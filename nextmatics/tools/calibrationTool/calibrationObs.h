#pragma once

namespace ssm
{
	struct CalibrationObsStruct
	{
	};

	class CalibrationObs
	{
	public:
		CalibrationObs();
		CalibrationObs(const CalibrationObs& newVal);
		void operator = (const CalibrationObs& newVal);

	private:

	};
}