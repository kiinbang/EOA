/**
* @file		lensCalibration.h
* @date		since 2020
* @author	kiinbang
* @brief	header file for a lens calibration class
*/

#pragma once

#include <ssm/include/SSMParameterCollection.h>

namespace ssm
{
	/**
	* @class	LensCalibration
	* @date		initial version: 2020
	* @author	kiinbang
	* @brief	class for lense calibration
	*/
	class __declspec (dllexport) LensCalibration
	{
	public:
		///

	private:
		///

	};
}