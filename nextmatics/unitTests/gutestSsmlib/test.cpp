#include "pch.h"

#include <ssmStr.h>
#include <ssmUuid.h>

#include <SSMMatrix.h>
#include <ssmBlockAdjustment.h>

#include "uuidTest.h"

///< SsmUuid class unit tests
///< not-equal operator test
TEST(UuidTest, operator_not_equal_test)
{
	util::SsmUuid id0;
	util::SsmUuid id = id0;

	ASSERT_FALSE(id != id0);

	auto uuid = id0.getId();

	ASSERT_FALSE(id != uuid);
}

///< equal operator test
TEST(UuidTest, operator_equal_test)
{
	util::SsmUuid id0;
	util::SsmUuid id = id0;

	ASSERT_TRUE(id == id0);

	auto uuid = id0.getId();

	ASSERT_TRUE(id == uuid);
}

///< unique id generation test
TEST(UuidTest, unique_id_gen_test)
{
	const unsigned int sizeArray = 1000;

	ASSERT_TRUE(uniqueTest(sizeArray));
}

///< SsmStr class unit tests
///< string operators test1
TEST(StrTest, operator_test)
{
	///< add strings
	util::SsmStr st = "abcdef";
	util::SsmStr st1 = "abc";
	util::SsmStr st2 = "def";
	util::SsmStr st3 = st1 + st2;
	ASSERT_TRUE(st3 == st);
}

///< string operators test2
TEST(StrTest, operator_test2)
{
	///< add to null string
	util::SsmStr st = "abcdef";
	util::SsmStr st3 = "abcdef";
	util::SsmStr st4;
	st4 = st4 + st3;
	ASSERT_TRUE(st4 == st);
}

///< string operators test3
TEST(StrTest, operator_test3)
{
	///< add to null strings
	util::SsmStr st5, st6;
	st5 = st5 + st6;
	ASSERT_TRUE(st5 == st6);
}

///< string operators test4
TEST(StrTest, operator_test4)
{
	char* tempStr = "abcedfg";
	util::SsmStr temp = tempStr;
	util::SsmStr temp2(tempStr);

	///< not equal test
	util::SsmStr st5, st6;
	ASSERT_FALSE(st5 != st6);
}

///< matrix resize test
TEST(MatrixTest, resize_test)
{
	math::Matrix<double> mat;
	const unsigned int cols = 5;
	const unsigned int rows = 5;
	mat.resize(rows, cols);

	unsigned int nR = 0;
	unsigned int nC;

	for (unsigned int r = 0; r < mat.getRows(); ++r)
	{
		nC = 0;
		
		for (unsigned int c = 0; c < mat.getCols(); ++c)
		{
			mat(r, c);
			++nC;
		}

		++nR;
	}

	ASSERT_TRUE(nC = cols);
	ASSERT_TRUE(nR = rows);
}

///< Block adjustment test1
TEST(BlockAdjustTest, test1)
{
	ssm::BlockAdjustment bAdj;
}