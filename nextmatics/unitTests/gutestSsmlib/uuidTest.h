#pragma once

#include <omp.h>
#include <iostream>
#include <vector>

#include <ssmUuid.h>

bool uniqueTest(const unsigned int sizeArray)
{
	std::vector<util::SsmUuid*> ids(sizeArray);

	for (auto& id : ids)
	{
		id = new util::SsmUuid;
	}

	for (unsigned int i = 0; i < sizeArray; ++i)
	{
		ids[i] = new util::SsmUuid;
	}

	bool retVal = true;

	for (unsigned int i = 0; i < sizeArray; ++i)
	{
		util::SsmUuid* id = ids[i];

		for (int j = 0; j < static_cast<int>(sizeArray); ++j)
		{
			if (*id == *(ids[j]) && i != j)
			{
				std::cout << "Same " << i << "th Uuid: \t" << j << std::endl;
				retVal = false;
			}
		}
	}

	return retVal;
}