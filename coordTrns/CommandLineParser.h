#pragma once

#include <iostream>

//#include <filesystem> /// Note that it need "ISO C++ 17" in C/C++ - Language - C++ Language Standart and it cause a compile error (byte ambiguous symbol).
/// Therefore, boost::filesystem is recommended with the default option of C/C++ Language Standard in the project property.
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
namespace bfs = boost::filesystem;
namespace bpo = boost::program_options;

std::string commandLineParser(const int argc, char** argv, const std::string& key)
{
    for (int i = 1; i < argc; ++i)
    {
        if (std::string(argv[i]).compare(key) == 0)
            return argv[i + 1];
    }

    return std::string("");
}

void onInFile(const std::string path)
{
    std::cout << "Input file: " << path << std::endl;
}

void onInSystem(const std::string name)
{
    std::cout << "Input coordinate system: " << name << std::endl;
}

void onInRefEllipsoid(const std::string name)
{
    std::cout << "Input reference ellipsoid: " << name << std::endl;
}

void onInA(const double semiMajor)
{
    std::cout << "Semi-Major of the input reference ellipsoid: " << semiMajor << std::endl;
}

void onInB(const double semiMinor)
{
    std::cout << "Semi-Minor of the input reference ellipsoid: " << semiMinor << std::endl;
}

void onOutFile(const std::string path)
{
    std::cout << "Output file: " << path << std::endl;
}

void onOutSystem(const std::string name)
{
    std::cout << "Output coordinate system: " << name << std::endl;
}

void onOutRefEllipsoid(const std::string name)
{
    std::cout << "Output reference ellipsoid: " << name << std::endl;
}

void onOutA(const double semiMajor)
{
    std::cout << "Semi-Major of the output reference ellipsoid: " << semiMajor << std::endl;
}

void onOutB(const double semiMinor)
{
    std::cout << "Semi-Minor of the output reference ellipsoid: " << semiMinor << std::endl;
}