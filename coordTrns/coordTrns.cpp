// coordTrns.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <fstream>
#include <iostream>
#include <vector>

#include <ssm/include/CoordinateTransformation.h>

#include "CommandLineParser.h"

const double rad2deg = 180.0 / 3.14159265358979;
const double deg2rad = 3.14159265358979 /180.0;

bool readTxt(const std::string& txtFilePath, std::vector<math::Arrayd>& inPts)
{
	std::fstream infile;
	infile.open(txtFilePath, std::ios::in);
	if (!infile)
	{
		std::cout << "Failed in opening a input file for reading" << std::endl;
		return false;
	}

	math::Arrayd pt(3);

	while (!infile.eof())
	{
		infile >> pt[0] >> pt[1] >> pt[2];

		inPts.push_back(pt);
	}

	return true;
}

bool writeTxt(const std::string& txtFilePath, const std::vector<math::Arrayd>& outPts, const std::string& outSystem)
{
	std::fstream outfile;
	outfile.open(txtFilePath, std::ios::out);
	if (!outfile)
	{
		std::cout << "Failed in opening a output file for writing" << std::endl;
		return false;
	}

	outfile.setf(std::ios::fixed, std::ios::floatfield);

	if (outSystem == math::geo::_LLH_DEG || outSystem == math::geo::_LLH_RAD)
		outfile.precision(9);	
	else
		outfile.precision(3);

	for (unsigned int i = 0; i < outPts.size(); ++i)
	{
		double x = outPts[i][0], y = outPts[i][1];
		/// If output angle unit is degree, change radian Lat an d Long into degrees 
		if (outSystem == math::geo::_LLH_DEG)
		{
			x = outPts[i][0] * rad2deg;
			y = outPts[i][1] * rad2deg;
		}

		outfile << x << "\t";
		outfile << y << "\t";
		outfile << outPts[i][2] << std::endl;
	}

	return true;
}

int main(int argc, char** argv)
{
	std::cout.precision(15);

	std::string inFile;// = commandLineParser(argc, argv, std::string("--inputFile"));
	std::string inSystem;// = commandLineParser(argc, argv, std::string("--inputSystem"));
	std::string inRefEllipsoid;// = commandLineParser(argc, argv, std::string("--inputReferenceEllipsoid"));
	double inA;// = std::atof(commandLineParser(argc, argv, std::string("--inputSemiMajor")).c_str());
	double inB;// = std::atof(commandLineParser(argc, argv, std::string("--inputSemiMinor")).c_str());
	std::string outFile;// = commandLineParser(argc, argv, std::string("--outputFile"));
	std::string outSystem;// = commandLineParser(argc, argv, std::string("--outputSystem"));
	std::string outRefEllipsoid;// = commandLineParser(argc, argv, std::string("--outputReferenceEllipsoid"));
	double outA;// = std::atof(commandLineParser(argc, argv, std::string("--outputSemiMajor")).c_str());
	double outB;// = std::atof(commandLineParser(argc, argv, std::string("--outputSemiMinor")).c_str());

	std::string sysNameList = math::geo::_ECEF + std::string(", ") 
		+ math::geo::_ENU + std::string(", ") 
		+ math::geo::_LLH_DEG + std::string(", ")
		+ math::geo::_LLH_RAD;
	std::string inputCoordSystems = std::string("Input Coordinate System: ") + sysNameList;
	std::string outputCoordSystems = std::string("Output Coordinate System: ") + sysNameList;

	bpo::options_description desc{ "Options" };
	desc.add_options()
		("help", "Help screen")
		("inFile,i", bpo::value<std::string>(&inFile)->notifier(onInFile), "Input File")
		("inSys", bpo::value<std::string>(&inSystem)->notifier(onInSystem), inputCoordSystems.c_str())
		("inEllip", bpo::value<std::string>(&inRefEllipsoid)->notifier(onInRefEllipsoid)->default_value("WGS84"), "Input reference ellipsoid")
		("inSMajor", bpo::value<double>(&inA)->notifier(onInA)->default_value(6378137.0), "Semimajor of a input reference ellipsoid")
		("inSMinor", bpo::value<double>(&inB)->notifier(onInB)->default_value(6356752.3142), "Semiminor of a input reference ellipsoid")

		("outFile,o", bpo::value<std::string>(&outFile)->notifier(onOutFile), "Output File")
		("outSys", bpo::value<std::string>(&outSystem)->notifier(onOutSystem)->default_value("WGS84"), outputCoordSystems.c_str())
		("outEllip", bpo::value<std::string>(&outRefEllipsoid)->notifier(onOutRefEllipsoid)->default_value("WGS84"), "Output reference ellipsoid")
		("outSMajor", bpo::value<double>(&outA)->notifier(onOutA)->default_value(6378137.0), "Semimajor of a output reference ellipsoid")
		("outSMinor", bpo::value<double>(&outB)->notifier(onOutB)->default_value(6356752.3142), "Semiminor of a output reference ellipsoid");

	bpo::variables_map vm;
	bpo::store(parse_command_line(argc, argv, desc), vm);
	bpo::notify(vm);

	if (vm.count("help"))
		std::cout << desc << std::endl;

	if (!(vm.count("inFile") &&
		vm.count("inSys") &&
		vm.count("inEllip") &&
		vm.count("inSMajor") &&
		vm.count("inSMinor") &&
		vm.count("outFile") &&
		vm.count("outSys") &&
		vm.count("outEllip") &&
		vm.count("outSMajor") &&
		vm.count("outSMinor")))

	{
		std::cout << desc << std::endl;
		return 0;
	}

	/// Check if the reference ellipsoides are same
	bool sameEllipsoid = false;
	if (inRefEllipsoid == outRefEllipsoid
		&& math::almostSame(inA, outA)
		&& math::almostSame(inB, outB))
	{
		sameEllipsoid = true;
	}

	math::geo::REP inRep;
	inRep.name = inRefEllipsoid;
	inRep.semiMajor = inA;
	inRep.semiMinor = inB;
	math::geo::ReferenceEllipsoid inEllips(inRep);

	math::geo::REP outRep;
	outRep.name = outRefEllipsoid;
	outRep.semiMajor = outA;
	outRep.semiMinor = outB;

	math::geo::ReferenceEllipsoid outEllips(outRep);

	if (sameEllipsoid)
		outEllips = inEllips;
	
	/// Read an input file
	std::vector<math::Arrayd> inPts;
	if (!readTxt(inFile, inPts))
	{
		std::cout << "Return false from readTxt." << std::endl;
		return 0;
	}

	/// Transform point coordinates
	std::vector<math::geo::PipeNode> pipeline;
	math::geo::setPipeline(inSystem, inEllips, outSystem, outEllips, pipeline);
	std::vector<math::Arrayd> outPts;
	for (const auto& inPt : inPts)
	{
		/// If input angle unit is degree, change it into radian
		if (inSystem == math::geo::_LLH_DEG)
		{
			inPt[0] = inPt[0] * deg2rad;
			inPt[1] = inPt[1] * deg2rad;
		}

		auto outPt = math::geo::sequentialTransform(inPt, pipeline);
		outPts.push_back(outPt);
	}

	// Writre an output file
	if (!writeTxt(outFile, outPts, outSystem))
	{
		std::cout << "Return false from writeTxt." << std::endl;
		return 0;
	}
	else
	{
		std::cout << "Succeed. \n Check the output file: " << outFile << std::endl;
	}

	return 0;
}