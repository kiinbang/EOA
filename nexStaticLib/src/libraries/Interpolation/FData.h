/*
* Copyright(c) 2021-2021 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <vector>

namespace math
{
	namespace geo
	{
		/// ToDo: check it: if FDataType is integer, then search is not correct.
		using FDataType = double;// unsigned long int;

		///  Flight data
		class FDEpoch
		{
		public:
			FDataType msecGpsTime;
			int year;
			int month;
			int day;
			int hour;
			int min;
			float sec;
			double msecSystemTime;
			double x;///lat
			double y;///lon
			double z;///height
			double rotX;///body rotation about x
			double rotY;///body rotation about y
			double rotZ;///body rotation about z
			double gimRotX;///gimbal rotation about x
			double gimRotY;///gimbal rotation about y
			double gimRotZ;///gimbal rotation about z
			int hotspotCount;

			int caseNo;
			double vn;///velocity N
			double ve;///velocity E
			double vd;///velocity D
			double s0;///distance s0
			double s1;///distance s1
			double s2;///distance s2
			double s3;///distance s3
			double s4;///distance s4
			int gpsNum;///gps number
			int syncFlag;///sync flag
			int solFlag;///solution flag
			int saveFlag;///save flag
			
			FDEpoch() {}

			FDEpoch(const FDEpoch& val)
			{
				copy(val);
			}

			FDEpoch operator = (const FDEpoch& val)
			{
				copy(val);
				return *this;
			}

			FDEpoch operator - (const FDEpoch& val) const
			{
				FDEpoch newVal = *this;
				newVal.msecGpsTime = this->msecGpsTime - val.msecGpsTime;
				newVal.msecSystemTime = this->msecSystemTime - val.msecSystemTime;
				newVal.x = this->x - val.x;
				newVal.y = this->y - val.y;
				newVal.z = this->z - val.z;
				newVal.rotX = this->rotX - val.rotX;
				newVal.rotY = this->rotY - val.rotY;
				newVal.rotZ = this->rotZ - val.rotZ;
				newVal.gimRotX = this->gimRotX - val.gimRotX;
				newVal.gimRotY = this->gimRotY - val.gimRotY;
				newVal.gimRotZ = this->gimRotZ - val.gimRotZ;
				newVal.hotspotCount = this->hotspotCount - val.hotspotCount;
				
				newVal.vn = this->vn - val.vn;
				newVal.ve = this->ve - val.ve;
				newVal.vd = this->vd - val.vd;
				newVal.s0 = this->s0 - val.s0;
				newVal.s1 = this->s1 - val.s1;
				newVal.s2 = this->s2 - val.s2;
				newVal.s3 = this->s3 - val.s3;
				newVal.s4 = this->s4 - val.s4;

				return newVal;
			}

			FDEpoch operator + (const FDEpoch& val) const
			{
				FDEpoch newVal = *this;
				newVal.msecGpsTime = this->msecGpsTime + val.msecGpsTime;
				newVal.msecSystemTime = this->msecSystemTime + val.msecSystemTime;
				newVal.x = this->x + val.x;
				newVal.y = this->y + val.y;
				newVal.z = this->z + val.z;
				newVal.rotX = this->rotX + val.rotX;
				newVal.rotY = this->rotY + val.rotY;
				newVal.rotZ = this->rotZ + val.rotZ;
				newVal.gimRotX = this->gimRotX - val.gimRotX;
				newVal.gimRotY = this->gimRotY - val.gimRotY;
				newVal.gimRotZ = this->gimRotZ - val.gimRotZ;
				newVal.hotspotCount = this->hotspotCount + val.hotspotCount;

				newVal.vn = this->vn + val.vn;
				newVal.ve = this->ve + val.ve;
				newVal.vd = this->vd + val.vd;
				newVal.s0 = this->s0 + val.s0;
				newVal.s1 = this->s1 + val.s1;
				newVal.s2 = this->s2 + val.s2;
				newVal.s3 = this->s3 + val.s3;
				newVal.s4 = this->s4 + val.s4;

				return newVal;
			}

			FDEpoch operator * (const FDataType& s) const
			{
				FDEpoch newVal = *this;
				newVal.msecGpsTime = this->msecGpsTime * s;
				newVal.msecSystemTime = this->msecSystemTime * s;
				newVal.x = this->x * s;
				newVal.y = this->y * s;
				newVal.z = this->z * s;
				newVal.rotX = this->rotX * s;
				newVal.rotY = this->rotY * s;
				newVal.rotZ = this->rotZ * s;
				newVal.gimRotX = this->gimRotX * s;
				newVal.gimRotY = this->gimRotY * s;
				newVal.gimRotZ = this->gimRotZ * s;
				newVal.hotspotCount = static_cast<int>(this->hotspotCount * s);

				newVal.vn = this->vn * s;
				newVal.ve = this->ve * s;
				newVal.vd = this->vd * s;
				newVal.s0 = this->s0 * s;
				newVal.s1 = this->s1 * s;
				newVal.s2 = this->s2 * s;
				newVal.s3 = this->s3 * s;
				newVal.s4 = this->s4 * s;
				
				return newVal;
			}

			private:
				void copy(const FDEpoch& val)
				{
					this->msecGpsTime = val.msecGpsTime;
					this->year = val.year;
					this->month = val.month;
					this->day = val.day;
					this->hour = val.hour;
					this->min = val.min;
					this->sec = val.sec;
					this->msecSystemTime = val.msecSystemTime;
					this->x = val.x;///lat
					this->y = val.y;///lon
					this->z = val.z;///height
					this->rotX = val.rotX;///body rotation about x
					this->rotY = val.rotY;///body rotation about y
					this->rotZ = val.rotZ;///body rotation about z
					this->gimRotX = val.gimRotX;///gimbal rotation about x
					this->gimRotY = val.gimRotY;///gimbal rotation about y
					this->gimRotZ = val.gimRotZ;///gimbal rotation about z
					this->hotspotCount = val.hotspotCount;

					this->caseNo = val.caseNo;
					this->vn = val.vn;///velocity N
					this->ve = val.ve;///velocity E
					this->vd = val.vd;///velocity D
					this->s0 = val.s0;///distance s0
					this->s1 = val.s1;
					this->s2 = val.s2;
					this->s3 = val.s3;
					this->s4 = val.s4;
					this->gpsNum = val.gpsNum;///gps number
					this->syncFlag = val.syncFlag;///sync flag
					this->solFlag = val.solFlag;///solution flag
					this->saveFlag = val.saveFlag;///save flag
				}
		};

		class FlightData
		{
		public:
			/// Must return the number of data points
			inline std::size_t kdtree_get_point_count() const { return epochs.size(); }

			/// Returns the distance between the vector "p1[0:size-1]" and the data point with index "idx_p2" stored in the class:
			inline FDataType kdtree_distance(const FDataType* p1, const size_t idx_p2, size_t size = 1) const
			{
				if (size != 1)
					throw std::runtime_error("size is not 1. EpochTree is a 1-dimsion-tree.");

				return static_cast<FDataType>(fabs(static_cast<double>(p1[0]) - static_cast<double>(epochs[idx_p2].msecGpsTime)));
			}

			/// Returns the dim'th component of the idx'th point in the class:
			inline FDataType kdtree_get_pt(const size_t idx, int dim = 0) const
			{
				if (dim != 0)
					throw std::runtime_error("dimension index is not 0. EpochTree is a 1-dimsion-tree.");

				return epochs[idx].msecGpsTime;
			}

			/// Optional bounding-box computation: return false to default to a standard bbox computation loop.
			/// Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
			/// Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
			template <class BBOX> bool kdtree_get_bbox(BBOX& /*bb*/) const
			{
				return false;
			}

		public:
			std::vector<FDEpoch> epochs;
		};
	}
}

