/*
* Copyright(c) 2021-2021 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <ssm/include/utilitygrocery.h>

#include "Interpolation.h"

namespace math
{
	namespace geo
	{
		void FDataTree::clear()
		{
			flightData.epochs.clear();
		}

		void FDataTree::buildTree(const unsigned int maxLeaf)
		{
			try
			{
				/// Larger maxLeaf result in fast search, but takes loing time for tree construction
				tree.reset(new KDTreeTime(dimensionT, flightData, nanoflann::KDTreeSingleIndexAdaptorParams(maxLeaf)));
				tree->buildIndex();
			}
			catch (...)
			{
				std::cerr << "Error in buildTree of SSMFlann" << std::endl;
			}
		}

		bool FDataTree::add(const FDEpoch& newVal)
		{
			try
			{
				flightData.epochs.push_back(newVal);
				return true;
			}
			catch (...)
			{
				std::cerr << "Error in setVertex of SSMFlann" << std::endl;
				return false;
			}
		}

		void FDataTree::reserve(const unsigned int size)
		{
			flightData.epochs.reserve(size);
		}

		bool FDataTree::search(const FDataType t0,
			std::vector<double>& distances,
			std::vector<std::size_t>& indices,
			const unsigned int numClosest,
			const double distThreshold) const
		{
			FDataType searchPt[] = { t0 };

			try
			{
				indices.resize(numClosest, 0);
				std::vector<FDataType> timeDiff(numClosest);
				size_t numFound = tree->knnSearch(searchPt, numClosest, indices.data(), timeDiff.data());

				distances.reserve(numFound);
				for (const auto& d : timeDiff)
				{
					distances.push_back(d);
				}

				if (numFound == 0)
					return false;
				else if (numFound < numClosest)
					indices.erase(indices.begin() + numFound, indices.end());
				
				if (numFound < numClosest && numClosest == 2)
					return false;
			}
			catch (...)
			{
				std::cerr << "Error in search of SSMFlann" << std::endl;
				return false;
			}

			if (distances[0] < distThreshold)
				return true;
			else
				return false;
		}

		const FDEpoch& FDataTree::getEpoch(const std::size_t index) const
		{
			return flightData.epochs[index];
		}

		FDEpoch FDataTree::linearInterpolate(const FDEpoch& epoch0, const FDEpoch& epoch1, const FDataType msecTime) const
		{
			FDEpoch interEpoch = epoch0;
			
			if (almostSame(epoch1.msecGpsTime, epoch0.msecGpsTime))
				return interEpoch;
			else if (almostSame(msecTime, epoch0.msecGpsTime))
			{
				return interEpoch;
			}
			else if (almostSame(msecTime, epoch1.msecGpsTime))
			{
				interEpoch = epoch1;
				return interEpoch;
			}
			else
			{
				auto rate = (msecTime - epoch0.msecGpsTime) / (epoch1.msecGpsTime - epoch0.msecGpsTime);
				interEpoch = ((epoch1 - epoch0) * rate) + epoch0;
				return interEpoch;
			}
		}

		std::vector<FDEpoch> FDataTree::linearInterpolate(std::vector<FDataType>& times, const FDataType searchThreshold, const unsigned int numClosest) const
		{
			std::vector<FDEpoch> interpolated;
			interpolated.reserve(times.size());

			for (unsigned int i = 0; i < times.size(); ++i)
			{
				double t0 = double(i) + 0.2;
				std::vector<double> distances;
				std::vector<size_t> indices;

				if (search(times[i], distances, indices, numClosest, searchThreshold))
				{
					if (indices.size() < 2)
						continue;
					interpolated.push_back(linearInterpolate(getEpoch(indices[0]), getEpoch(indices[1]), times[i]));
				}
			}

			return interpolated;
		}

		bool linearInterpolate(const FDEpoch& t0, const FDEpoch& t1, FDEpoch& t)
		{
			if (almostSame(t1.msecGpsTime, t0.msecGpsTime))
			{
				t = t0;
				return false;
			}
			else if (almostSame(t.msecGpsTime, t0.msecGpsTime))
			{
				t = t0;
				return true;
			}
			else if (almostSame(t.msecGpsTime, t1.msecGpsTime))
			{
				t = t1;
				return true;
			}
			else
			{
				auto rate = (t.msecGpsTime - t0.msecGpsTime) / (t1.msecGpsTime - t0.msecGpsTime);
				t = t0 + ((t1 - t0) * rate);/// t0 should be left to copy all non-calculable values to the result (refer to operators in FDEpoch class)
			}

			return true;
		}

		bool interpolateEpoch(const FDataTree& fdTree, FDEpoch& interpolatedData, const unsigned int numSearch, const double distThreshold)
		{
			std::vector<double> distances;
			std::vector<size_t> indices;

			if (fdTree.search(interpolatedData.msecGpsTime, distances, indices, numSearch, distThreshold))
			{
				if (indices.size() < 1)
					return false;
				if (indices.size() == 1)
				{
					interpolatedData = fdTree.getEpoch(indices[0]);
					return false;
				}
				else if (!linearInterpolate(fdTree.getEpoch(indices[0]), fdTree.getEpoch(indices[1]), interpolatedData))
					return false;
			}

			return true;
		}

		void getLinearInterpolation(const FDataTree& fdTree, std::vector<FDEpoch>& interpolatedData, const unsigned int numSearch, const double distThreshold)
		{
			for (unsigned int i = 0; i < interpolatedData.size(); ++i)
			{
				std::vector<double> distances;
				std::vector<size_t> indices;

				if(!interpolateEpoch(fdTree, interpolatedData[i], numSearch, distThreshold))
				{
#ifdef _DEBUG
					std::cout << "false return from getLinearInterpolation at time(msec) " << interpolatedData[i].msecGpsTime << " (" << i << "th)" << std::endl;
#endif
				}
			}
		}
	}
}