/*
* Copyright(c) 2021-2021 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <iostream>
#include <vector>

#include <nanoflann/nanoflann.hpp>

#include "FData.h"

#ifdef INTERPOLATION_EXPORT
#define INTERPOLATION_API __declspec(dllexport)
#define INTERPOLATION_TEMPLATE
#else
#define INTERPOLATION_API __declspec(dllimport)
#define INTERPOLATION_TEMPLATE extern
#endif

#define API INTERPOLATION_API

template<typename T>
inline bool almostSame(T a, T b)
{
    if (fabs(a - b) < std::numeric_limits<T>::epsilon())
        return true;
    else
        return false;
}

namespace math
{
	namespace geo
	{
		const unsigned int dimensionT = 1;

        using KDTreeTime = nanoflann::KDTreeSingleIndexAdaptor<nanoflann::L2_Simple_Adaptor<FDataType, FlightData>, FlightData, dimensionT>;

        INTERPOLATION_TEMPLATE template class API std::shared_ptr<KDTreeTime>;

        class API FDataTree
        {
        public:
            void clear();

            void buildTree(const unsigned int maxLeaf = 20);

            bool add(const FDEpoch& newVal);

            inline unsigned int size() const { return static_cast<unsigned int>(flightData.epochs.size());
			}

            void reserve(const unsigned int size);

            bool search(const FDataType t0,
                std::vector<double>& distances,
                std::vector<std::size_t>& indices,
                const unsigned int numClosest,
                const double distThreshold) const;

            const FDEpoch& getEpoch(const std::size_t index) const;

            std::vector<FDEpoch> linearInterpolate(std::vector<FDataType>& times, const FDataType searchThreshold, const unsigned int numClosest = 2) const;

            FDEpoch linearInterpolate(const FDEpoch& t0, const FDEpoch& t1, const FDataType time) const;

        private:
            FlightData flightData;
            std::shared_ptr<KDTreeTime> tree;
        };

		bool API linearInterpolate(const FDEpoch& t0, const FDEpoch& t1, FDEpoch& t);

        bool API interpolateEpoch(const FDataTree& fdTree, FDEpoch& interpolatedData, const unsigned int numSearch = 2, const double distThreshold = 6000.0);
		
        void API getLinearInterpolation(const FDataTree& fdTree, std::vector<FDEpoch>& interpolatedData, const unsigned int numSearch = 2, const double distThreshold = 6000.0);
	}
}
