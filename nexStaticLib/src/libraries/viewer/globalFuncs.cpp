#include "pch.h"

#include "BasicImage.h"
#include "gloablFuncs.h"

namespace image
{
	/******************************************************
						DIB와 관련된 전역 함수
		******************************************************/

	LPSTR FindDIBBits(LPSTR lpbi)
	{
		return (lpbi + *(LPDWORD)lpbi + PaletteSize(lpbi));
	}

	DWORD DIBWidth(LPSTR lpDIB)
	{
		LPBITMAPINFOHEADER lpbmi;  // pointer to a Win 3.0-style DIB
		LPBITMAPCOREHEADER lpbmc;  // pointer to an other-style DIB

		/* point to the header (whether Win 3.0 and old) */

		lpbmi = (LPBITMAPINFOHEADER)lpDIB;
		lpbmc = (LPBITMAPCOREHEADER)lpDIB;

		/* return the DIB width if it is a Win 3.0 DIB */
		if (IS_WIN30_DIB(lpDIB))
			return lpbmi->biWidth;
		else  /* it is an other-style DIB, so return its width */
			return (DWORD)lpbmc->bcWidth;
	}

	DWORD DIBHeight(LPSTR lpDIB)
	{
		LPBITMAPINFOHEADER lpbmi;  // pointer to a Win 3.0-style DIB
		LPBITMAPCOREHEADER lpbmc;  // pointer to an other-style DIB

		/* point to the header (whether old or Win 3.0 */

		lpbmi = (LPBITMAPINFOHEADER)lpDIB;
		lpbmc = (LPBITMAPCOREHEADER)lpDIB;

		/* return the DIB height if it is a Win 3.0 DIB */
		if (IS_WIN30_DIB(lpDIB))
			return lpbmi->biHeight;
		else  /* it is an other-style DIB, so return its height */
			return (DWORD)lpbmc->bcHeight;
	}

	WORD PaletteSize(LPSTR lpbi)
	{
		/* calculate the size required by the palette */
		if (IS_WIN30_DIB(lpbi))
			return (WORD)(DIBNumColors(lpbi) * sizeof(RGBQUAD));
		else
			return (WORD)(DIBNumColors(lpbi) * sizeof(RGBTRIPLE));
	}

	WORD DIBNumColors(LPSTR lpbi)
	{
		WORD wBitCount;  // DIB bit count

		/*  If this is a Windows-style DIB, the number of colors in the
		 *  color table can be less than the number of bits per pixel
		 *  allows for (i.e. lpbi->biClrUsed can be set to some value).
		 *  If this is the case, return the appropriate value.
		 */

		if (IS_WIN30_DIB(lpbi))
		{
			DWORD dwClrUsed;

			dwClrUsed = ((LPBITMAPINFOHEADER)lpbi)->biClrUsed;
			if (dwClrUsed != 0)
				return (WORD)dwClrUsed;
		}

		/*  Calculate the number of colors in the color table based on
		 *  the number of bits per pixel for the DIB.
		 */
		if (IS_WIN30_DIB(lpbi))
			wBitCount = ((LPBITMAPINFOHEADER)lpbi)->biBitCount;
		else
			wBitCount = ((LPBITMAPCOREHEADER)lpbi)->bcBitCount;

		/* return number of colors based on bits per pixel */
		switch (wBitCount)
		{
		case 1:
			return 2;

		case 4:
			return 16;

		case 8:
			return 256;

		default:
			return 0;
		}
	}

	/******************************************************
					클립보드를 위한 전역 함수
	******************************************************/

	HGLOBAL CopyHandle(HGLOBAL h)
	{
		if (h == NULL)
			return NULL;

		DWORD dwLen = static_cast<DWORD>(GlobalSize((HGLOBAL)h));
		HGLOBAL hCopy = GlobalAlloc(GHND, dwLen);

		if (hCopy != NULL)
		{
			void* lpCopy = GlobalLock((HGLOBAL)hCopy);
			void* lp = GlobalLock((HGLOBAL)h);
			memcpy(lpCopy, lp, dwLen);
			::GlobalUnlock(hCopy);
			::GlobalUnlock(h);
		}

		return hCopy;
	}
}