// BasicImage.cpp: implementation of the CBasicImage class.
//
//////////////////////////////////////////////////////////////////////
#include "pch.h"

#include <math.h>

#include "BasicImage.h"
#include "gloablFuncs.h"

#pragma warning(disable: 4996)

namespace image
{
	/******************************************************
	CBasicImage
	******************************************************/

	//////////////////////////////////////////////////////////////////////
	// Construction/Destruction
	//////////////////////////////////////////////////////////////////////

	CBasicImage::CBasicImage()
	{
		m_hImage = NULL;
		m_pPal = NULL;
		m_hUndoImage = NULL;
	}

	CBasicImage::CBasicImage(CBasicImage& Src)
	{
		m_Size = CSize(1, 1);
		m_hImage = NULL;
		m_pPal = NULL;
		m_hUndoImage = NULL;

		*this = Src;
	}

	void CBasicImage::SetHandle(void* hHandle)
	{
		m_hImage = (HDIB)hHandle;
	}

	CBasicImage::~CBasicImage()
	{
		Free();
	}

	CBasicImage* CBasicImage::operator=(CBasicImage* Src)  // Right side is the argument.
	{
		Free();
		m_hImage = (HDIB)CopyHandle(Src->GetHandle());
		m_hUndoImage = (HDIB)CopyHandle(Src->GetUndoHandle());
		m_Size = Src->GetSize();
		m_pPal = new CPalette;
		CreateDIBPalette();
		return (this);
	}

	CBasicImage& CBasicImage::operator=(CBasicImage& Src)  // Right side is the argument.
	{
		Free();
		m_hImage = (HDIB)CopyHandle(Src.GetHandle());
		m_hUndoImage = (HDIB)CopyHandle(Src.GetUndoHandle());
		m_Size = Src.GetSize();
		m_pPal = new CPalette;
		CreateDIBPalette();
		return (*this);
	}

	BOOL CBasicImage::Undo()
	{
		HDIB hTemp;

		// 백업이 존재하면 백업과 이미지 핸들을 교환
		if (m_hUndoImage)
		{
			hTemp = m_hImage;
			m_hImage = m_hUndoImage;
			m_hUndoImage = hTemp;
			return TRUE;
		}
		else
			return FALSE;
	}

	BOOL CBasicImage::PrepareUndo()
	{
		// 이미 백업이 존재하면 이를 해제함
		if (m_hUndoImage)
		{
			GlobalFree((HGLOBAL)m_hUndoImage);
			m_hUndoImage = NULL;
		}

		// 이미지를 통째로 복사하여 백업을 만듬
		if ((m_hUndoImage = (HDIB)CopyHandle((HGLOBAL)m_hImage)) == NULL)
		{
			m_hUndoImage = NULL;
			return FALSE;
		}
		return TRUE;
	}

	void CBasicImage::Free()
	{
		if (m_hImage)
		{
			if (GlobalFree(m_hImage) != NULL)
			{
				TRACE("Can't free handle in CImage::Free()");
			}
			m_hImage = NULL;
		}
		if (m_hUndoImage)
		{
			if (GlobalFree(m_hUndoImage) != NULL)
			{
				TRACE("Can't free handle in CRawImage::Free()");
			}
			m_hUndoImage = NULL;
		}

		if (m_pPal != NULL)
		{
			delete m_pPal;
			m_pPal = NULL;
		}
	}

	BOOL CBasicImage::Draw(HDC hDC, LPRECT lpDIBRect, LPRECT lpDCRect)
	{
		LPSTR	lpDIBHdr;	// BITMAPINFOHEADER를 가리킬 포인터
		LPSTR	lpDIBBits;	// DIB 비트를 가리킬 포인터
		BOOL		bSuccess = FALSE;	 // Success/fail 플래그
		HPALETTE 	hPal = NULL;		 // DIB 팔레트
		HPALETTE 	hOldPal = NULL;	 // 이전 팔레트

		// 메모리 고정
		lpDIBHdr = (LPSTR)GlobalLock((HGLOBAL)m_hImage);
		// DIB 비트가 저장되어 있는 곳의 주소를 얻음
		lpDIBBits = FindDIBBits(lpDIBHdr);

		// 팔레트를 얻어 DC에 선택
		if (m_pPal != NULL)
		{
			hPal = (HPALETTE)m_pPal->m_hObject;
			hOldPal = ::SelectPalette(hDC, hPal, TRUE);
		}

		::SetStretchBltMode(hDC, COLORONCOLOR);

		if ((RECTWIDTH(lpDCRect) == RECTWIDTH(lpDIBRect)) &&
			(RECTHEIGHT(lpDCRect) == RECTHEIGHT(lpDIBRect)))
			// 원래 크기로 그대로 출력하는 경우
			bSuccess = ::SetDIBitsToDevice(hDC, // hDC
				lpDCRect->left,		 			// DestX
				lpDCRect->top,		 			// DestY
				RECTWIDTH(lpDCRect),	 		// nDestWidth
				RECTHEIGHT(lpDCRect),			// nDestHeight
				lpDIBRect->left,		 		// SrcX
				(int)DIBHeight(lpDIBHdr) - lpDIBRect->top - RECTHEIGHT(lpDIBRect),   		// SrcY
				0,                          	// nStartScan
				(WORD)DIBHeight(lpDIBHdr),  	// nNumScans
				lpDIBBits,                  	// lpBits
				(LPBITMAPINFO)lpDIBHdr,			// lpBitsInfo
				DIB_RGB_COLORS);				// wUsage
		else	// 확대 또는 축소하여 출력하는 경우
			bSuccess = ::StretchDIBits(hDC, 	// hDC
				lpDCRect->left,					// DestX
				lpDCRect->top,					// DestY
				RECTWIDTH(lpDCRect),			// nDestWidth
				RECTHEIGHT(lpDCRect),			// nDestHeight
				lpDIBRect->left,				// SrcX
				(int)DIBHeight(lpDIBHdr) - lpDIBRect->top - RECTHEIGHT(lpDIBRect),   		// SrcY
				//lpDIBRect->top,					// SrcY
				RECTWIDTH(lpDIBRect),			// wSrcWidth
				RECTHEIGHT(lpDIBRect),			// wSrcHeight
				lpDIBBits,						// lpBits
				(LPBITMAPINFO)lpDIBHdr,			// lpBitsInfo
				DIB_RGB_COLORS,					// wUsage
				SRCCOPY);						// dwROP

		// 메모리 놓아줌
		::GlobalUnlock((HGLOBAL)m_hImage);
		// DC 복원
		if (hOldPal != NULL) ::SelectPalette(hDC, hOldPal, TRUE);
		return bSuccess;
	}

	BOOL CBasicImage::Create(int width, int height, int depth)
	{
		LPBITMAPINFOHEADER lpbi;
		unsigned char* lpPal;
		DWORD       dwSizeImage;
		int         i;

		dwSizeImage = height * (DWORD)((width * depth / 8 + 3) & ~3);

		if (depth == 24)
			m_hImage = (HDIB)GlobalAlloc(GHND, sizeof(BITMAPINFOHEADER) + dwSizeImage);
		else
			m_hImage = (HDIB)GlobalAlloc(GHND, sizeof(BITMAPINFOHEADER) + dwSizeImage + 1024);

		if (m_hImage == NULL)
			return FALSE;

		lpbi = (LPBITMAPINFOHEADER)GlobalLock(m_hImage);
		lpbi->biSize = sizeof(BITMAPINFOHEADER);
		lpbi->biWidth = width;
		lpbi->biHeight = height;
		lpbi->biPlanes = 1;
		lpbi->biBitCount = depth;
		lpbi->biCompression = BI_RGB;
		lpbi->biSizeImage = dwSizeImage;
		lpbi->biXPelsPerMeter = 0;
		lpbi->biYPelsPerMeter = 0;
		lpbi->biClrUsed = 0;
		lpbi->biClrImportant = 0;

		lpPal = (unsigned char*)lpbi;
		if (depth == 8)
		{
			lpbi->biClrUsed = 256;

			DWORD offDest = sizeof(BITMAPINFOHEADER);
			for (i = 0; i < 256; i++)
			{
				lpPal[offDest++] = (unsigned char)i;
				lpPal[offDest++] = (unsigned char)i;
				lpPal[offDest++] = (unsigned char)i;
				lpPal[offDest++] = 0x00;
			}
		}

		InitDIB(FALSE);
		return TRUE;
	}

	/******************************************************
					이미지 정보를 얻는 함수
	******************************************************/

	int CBasicImage::GetBitCount()
	{
		if (m_hImage == NULL) return -1;
		LPBITMAPINFOHEADER lpbi;
		lpbi = (LPBITMAPINFOHEADER) ::GlobalLock((HGLOBAL)m_hImage);
		return lpbi->biBitCount;
	}

	BOOL CBasicImage::InitDIB(BOOL bCreatePalette)
	{
		// 이미지의 가로, 세로 크기 설정
		LPSTR pDIB = (LPSTR)GlobalLock((HGLOBAL)m_hImage);
		m_Size = CSize((int)DIBWidth(pDIB), (int)DIBHeight(pDIB));
		::GlobalUnlock((HGLOBAL)m_hImage);

		if (bCreatePalette)
		{
			if (m_pPal != NULL) delete m_pPal;
			// 팔레트 생성
			m_pPal = new CPalette;
			if (CreateDIBPalette() == NULL)
			{
				// 팔레트를 가지지 않는 경우
				delete m_pPal;
				m_pPal = NULL;
				return FALSE;
			}
		}
		return TRUE;
	}

	BOOL CBasicImage::CreateDIBPalette()
	{
		LPLOGPALETTE lpPal;      // pointer to a logical palette
		HANDLE hLogPal;          // handle to a logical palette
		HPALETTE hPal = NULL;    // handle to a palette
		int i;                   // loop index
		WORD wNumColors;         // number of colors in color table
		LPSTR lpbi;              // pointer to packed-DIB
		LPBITMAPINFO lpbmi;      // pointer to BITMAPINFO structure (Win3.0)
		LPBITMAPCOREINFO lpbmc;  // pointer to BITMAPCOREINFO structure (old)
		BOOL bWinStyleDIB;       // flag which signifies whether this is a Win3.0 DIB
		BOOL bResult = FALSE;

		/* if handle to DIB is invalid, return FALSE */

		if (m_hImage == NULL)
			return FALSE;

		lpbi = (LPSTR) ::GlobalLock((HGLOBAL)m_hImage);

		/* get pointer to BITMAPINFO (Win 3.0) */
		lpbmi = (LPBITMAPINFO)lpbi;

		/* get pointer to BITMAPCOREINFO (old 1.x) */
		lpbmc = (LPBITMAPCOREINFO)lpbi;

		/* get the number of colors in the DIB */
		wNumColors = DIBNumColors(lpbi);

		if (wNumColors != 0)
		{
			/* allocate memory block for logical palette */
			hLogPal = ::GlobalAlloc(GHND, sizeof(LOGPALETTE)
				+ sizeof(PALETTEENTRY)
				* wNumColors);

			/* if not enough memory, clean up and return NULL */
			if (hLogPal == 0)
			{
				::GlobalUnlock((HGLOBAL)m_hImage);
				return FALSE;
			}

			lpPal = (LPLOGPALETTE) ::GlobalLock((HGLOBAL)hLogPal);

			/* set version and number of palette entries */
			lpPal->palVersion = PALVERSION;
			lpPal->palNumEntries = (WORD)wNumColors;

			/* is this a Win 3.0 DIB? */
			bWinStyleDIB = IS_WIN30_DIB(lpbi);
			for (i = 0; i < (int)wNumColors; i++)
			{
				if (bWinStyleDIB)
				{
					lpPal->palPalEntry[i].peRed = lpbmi->bmiColors[i].rgbRed;
					lpPal->palPalEntry[i].peGreen = lpbmi->bmiColors[i].rgbGreen;
					lpPal->palPalEntry[i].peBlue = lpbmi->bmiColors[i].rgbBlue;
					lpPal->palPalEntry[i].peFlags = 0;
				}
				else
				{
					lpPal->palPalEntry[i].peRed = lpbmc->bmciColors[i].rgbtRed;
					lpPal->palPalEntry[i].peGreen = lpbmc->bmciColors[i].rgbtGreen;
					lpPal->palPalEntry[i].peBlue = lpbmc->bmciColors[i].rgbtBlue;
					lpPal->palPalEntry[i].peFlags = 0;
				}
			}

			/* create the palette and get handle to it */
			bResult = m_pPal->CreatePalette(lpPal);
			::GlobalUnlock((HGLOBAL)hLogPal);
			::GlobalFree((HGLOBAL)hLogPal);
		}

		::GlobalUnlock((HGLOBAL)m_hImage);

		return bResult;
	}

	void CBasicImage::GetRainbowColor(unsigned char index, unsigned char& r, unsigned char& g, unsigned char& b)
	{
		if ((0 <= index) && (index <= 48))
		{
			r = (unsigned char)0;						g = (unsigned char)(index / 48 * 255);				b = (unsigned char)255;
			return;
		}
		else if ((48 < index) && (index <= 96))
		{
			r = (unsigned char)0;						g = (unsigned char)255;							b = (unsigned char)((96 - index) / 47 * 255);
			return;
		}
		else if ((96 < index) && (index <= 144))
		{
			b = (unsigned char)((index - 97) / 47 * 255);	g = (unsigned char)255;							b = (unsigned char)0;
			return;
		}
		else if ((144 < index) && (index <= 192))
		{
			b = (unsigned char)255;						g = (unsigned char)(255 - (index - 145) / 47 * 126);	b = (unsigned char)0;
			return;
		}
		else if ((192 < index) && (index <= 240))
		{
			b = (unsigned char)255;						g = (unsigned char)(126 - (index - 193) / 47 * 126);	b = (unsigned char)0;
			return;
		}
		else if ((192 < index) && (index <= 240))
		{
			b = (unsigned char)255;						g = (unsigned char)(126 - (index - 193) / 47 * 126);	b = (unsigned char)0;
			return;
		}
		else if ((241 < index) && (index <= 255))
		{
			b = (unsigned char)255;						g = (unsigned char)((index - 242) / 13 * 255);			b = (unsigned char)((index - 242) / 13 * 255);
			return;
		}
		else
		{
			b = (unsigned char)0;						g = (unsigned char)0;							b = (unsigned char)0;
			return;
		}
	}

	void CBasicImage::GetRainbowColor_new(unsigned char index, unsigned char& r, unsigned char& g, unsigned char& b)
	{
		if (index < 1)
		{
			r = 0; g = 0; b = 0;
			return;
		}
		else if (index < 20)
		{
			r = (unsigned char)(55 + g * 10); g = 0; b = 0;
			return;
		}
		else if (index < 48)
		{
			r = (unsigned char)(256 - (index - 20) * 9);
			g = (unsigned char)((index - 20) * 9);
			b = 0;
			return;
		}
		else if (index < 64)
		{
			r = 0;
			g = 255;
			b = (unsigned char)((index - 48) * 16);

			return;
		}
		else if (index < 128)
		{
			r = 0;
			g = (unsigned char)(256 - (index - 64) * 4);
			b = 255;

			return;
		}
		else
		{
			r = 0;
			g = 0;
			b = (unsigned char)(256 - (index - 128) * 2);

			return;
		}
	}

	void CBasicImage::GetRainbowColorfromXYZ(double X, double Y, double Z, unsigned char& r, unsigned char& g, unsigned char& b)
	{
		double var_X = X / 100;        //Where X = 0 ÷  95.047
		double var_Y = Y / 100;        //Where Y = 0 ÷ 100.000
		double var_Z = Z / 100;        //Where Z = 0 ÷ 108.883

		double var_R = var_X * 3.2406 + var_Y * -1.5372 + var_Z * -0.4986;
		double var_G = var_X * -0.9689 + var_Y * 1.8758 + var_Z * 0.0415;
		double var_B = var_X * 0.0557 + var_Y * -0.2040 + var_Z * 1.0570;

		if (var_R > 0.0031308) var_R = 1.055 * pow(var_R, (1 / 2.4)) - 0.055;
		else                     var_R = 12.92 * var_R;

		if (var_G > 0.0031308) var_G = 1.055 * pow(var_G, (1 / 2.4)) - 0.055;
		else                     var_G = 12.92 * var_G;

		if (var_B > 0.0031308) var_B = 1.055 * pow(var_B, (1 / 2.4)) - 0.055;
		else                     var_B = 12.92 * var_B;

		r = (unsigned char)(var_R * 255);
		g = (unsigned char)(var_G * 255);
		b = (unsigned char)(var_B * 255);
	}
}