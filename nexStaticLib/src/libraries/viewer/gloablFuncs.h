#pragma once

namespace image
{
	/******************************************************
							���� �Լ�
	******************************************************/
	LPSTR FindDIBBits(LPSTR lpbi);
	DWORD DIBWidth(LPSTR lpDIB);
	DWORD DIBHeight(LPSTR lpDIB);
	WORD PaletteSize(LPSTR lpbi);
	WORD DIBNumColors(LPSTR lpbi);
	HGLOBAL CopyHandle(HGLOBAL h);
}
