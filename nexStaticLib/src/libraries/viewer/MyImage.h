// MyImage.h: interface for the CMyImage class.
//
//////////////////////////////////////////////////////////////////////

//Derived from pic and silicon graphics library(JPEG, TIFF)
//Modified by Bang, Ki In

/// Modification for VS2019, Oct 2020

#pragma once

#define BYTE unsigned char
typedef int BOOL;


// DIB constants
#define PALVERSION   0x300

// DIB Macros
#define IS_WIN30_DIB(lpbi) ((*(LPDWORD)(lpbi)) == sizeof(BITMAPINFOHEADER))
#define RECTWIDTH(lpRect)  ((lpRect)->right - (lpRect)->left)
#define RECTHEIGHT(lpRect) ((lpRect)->bottom - (lpRect)->top)
#define WIDTHBYTES(bits)   (((bits) + 31) / 32 * 4)

#define DIB_HEADER_MARKER   ((WORD) ('M' << 8) | 'B')

class CMyImage
{
/*Member variable*/
protected:
	unsigned char* m_Image;		//image handle
	unsigned char* m_UndoImage;	//undo image handle
	CSize m_Size;		//image size
	CPalette * m_pPal;	//image palette
	CString filetype;   //image path

/*Member function*/
public:
	CMyImage();
    CMyImage( CMyImage & );
    CMyImage& operator=(CMyImage &);
	CMyImage* operator=(CMyImage *);
	BOOL InitDIB(BOOL bCreatePalette = TRUE);
	void SetHandle(HANDLE hHandle);
	BOOL Create(int width, int height, int depth);
	BOOL CreateDIBPalette();

	virtual ~CMyImage() { Free(); }
	void Free();


	// Image information
	int GetBitCount();
	unsigned char* GetHandle()		{return m_Image;}
	BOOL IsDataNull()		{return (m_Image == NULL);}
	CSize GetSize()			{return m_Size;}	
	int GetHeight()			{return m_Size.cy;}
	int GetWidth()			{return m_Size.cx;}
	int GetRealWidth()		{return WIDTHBYTES((GetWidth()*GetBitCount()));}
	unsigned char* GetUndoHandle()	{return m_UndoImage;}
	CPalette *GetPalette()	{return m_pPal;}

	// drawing
	BOOL Draw(HDC hDC, LPRECT sourceRect, LPRECT destRect);

	// Undo
	BOOL Undo();
	BOOL PrepareUndo();
	BOOL CanUndo()			{return (m_UndoImage!=NULL);}
	
	// load and save
	BOOL Save(LPCTSTR lpszFileName);
	BOOL Load(LPCTSTR lpszFileName);

	// path
	CString GetPathName() {return filetype;}

protected:
	// Load
	BOOL LoadBMP(LPCTSTR lpszFileName);
	BOOL LoadTIF(LPCTSTR lpszFileName);
	BOOL LoadJPG(LPCTSTR lpszFileName);
	
	// save
	BOOL SaveBMP(LPCTSTR lpszFileName);
	BOOL SaveJPG(LPCTSTR lpszFileName);
};

/*global function*/

LPSTR FindDIBBits(LPSTR lpbi);
DWORD DIBWidth(LPSTR lpDIB);
DWORD DIBHeight(LPSTR lpDIB);
WORD PaletteSize(LPSTR lpbi);
WORD DIBNumColors(LPSTR lpbi);
HGLOBAL CopyHandle (HGLOBAL h);
