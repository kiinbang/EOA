// MyImage.cpp: implementation of the CMyImage class.
//
//////////////////////////////////////////////////////////////////////

#include "pch.h"
#include "framework.h"
#include "MyImage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


typedef struct _tagCOLOR_
{
	BYTE B;
	BYTE G;
	BYTE R;
} _COLOR_;

typedef struct _tagCOLOR_* _LPCOLOR_;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMyImage::CMyImage()
{
	m_Size		 = CSize(1,1);
	m_Image	 = NULL;
	m_pPal		 = NULL;
	m_UndoImage = NULL;
}

CMyImage::CMyImage( CMyImage &Src )
{
	m_Size		 = CSize(1,1);
	m_Image	 = NULL;
	m_pPal		 = NULL;
	m_UndoImage = NULL;
	
	*this = Src;
}

CMyImage& CMyImage::operator=( CMyImage &Src )  // Right side is the argument.
{
	int m_Depth = GetBitCount();
	int m_size = m_Size.cx*m_Size.cy*m_Depth;
	
	Free();
	m_Image = (unsigned char*) ::CopyHandle( Src.GetHandle() );
	m_UndoImage = (unsigned char*) ::CopyHandle( Src.GetUndoHandle() );
	
	m_Size = Src.GetSize();
	m_pPal = new CPalette;
	CreateDIBPalette();
	return (*this);
}

CMyImage* CMyImage::operator=( CMyImage *Src )  // Right side is the argument.
{
	int m_Depth = GetBitCount();
	int m_size = m_Size.cx*m_Size.cy*m_Depth;
	
	Free();
	m_Image = (unsigned char*) ::CopyHandle( Src->GetHandle() );
	m_UndoImage = (unsigned char*) ::CopyHandle( Src->GetUndoHandle() );
	
	m_Size = Src->GetSize();
	m_pPal = new CPalette;
	CreateDIBPalette();
	return (this);
}

BOOL CMyImage::InitDIB(BOOL bCreatePalette)
{
	// image size
	LPSTR pDIB = (LPSTR)::GlobalLock((unsigned char*) m_Image);
	m_Size = CSize((int) ::DIBWidth(pDIB), (int) ::DIBHeight(pDIB));
	::GlobalUnlock((unsigned char*) m_Image);
	
	if(bCreatePalette)
	{
		if(m_pPal != NULL) delete m_pPal;
		// palette setting
		m_pPal = new CPalette;
		if (CreateDIBPalette() == NULL)
		{
			// no palette
			delete m_pPal;
			m_pPal = NULL;
			return FALSE;
		}
	}
	return TRUE;
}

void CMyImage::SetHandle(HANDLE hHandle)
{
	m_Image = (unsigned char*)hHandle;
}

BOOL CMyImage::Create(int width, int height, int depth)
{
    LPBITMAPINFOHEADER lpbi ;
	BYTE		*lpPal;
    DWORD       dwSizeImage;
    int         i;
	
    dwSizeImage = height*(DWORD)((width*depth/8+3)&~3);
	
	if(depth == 24)
		m_Image= (unsigned char*)GlobalAlloc(GHND,sizeof(BITMAPINFOHEADER)+dwSizeImage);
    else
		m_Image= (unsigned char*)GlobalAlloc(GHND,sizeof(BITMAPINFOHEADER)+dwSizeImage + 1024);
	
    if (m_Image == NULL)
        return FALSE;
	
	lpbi = (LPBITMAPINFOHEADER)GlobalLock(m_Image);
	lpbi->biSize            = sizeof(BITMAPINFOHEADER) ;
    lpbi->biWidth           = width;
    lpbi->biHeight          = height;
    lpbi->biPlanes          = 1;
    lpbi->biBitCount        = depth;
    lpbi->biCompression     = BI_RGB ;
    lpbi->biSizeImage       = dwSizeImage;
    lpbi->biXPelsPerMeter   = 0 ;
    lpbi->biYPelsPerMeter   = 0 ;
    lpbi->biClrUsed         = 0 ;
    lpbi->biClrImportant    = 0 ;
	
	lpPal = (BYTE *) lpbi;
	if (depth == 8)
	{
		lpbi->biClrUsed = 256;
		
		DWORD offDest = sizeof(BITMAPINFOHEADER);
		for(i = 0; i < 256; i++)
		{
			lpPal[offDest++] = (BYTE)i;
			lpPal[offDest++] = (BYTE)i;
			lpPal[offDest++] = (BYTE)i;
			lpPal[offDest++] = 0x00;
		}                  
	}
	
	InitDIB(FALSE);
	return TRUE;
}

BOOL CMyImage::CreateDIBPalette()
{
	LPLOGPALETTE lpPal;      // pointer to a logical palette
	HANDLE hLogPal;          // handle to a logical palette
	HPALETTE hPal = NULL;    // handle to a palette
	int i;                   // loop index
	WORD wNumColors;         // number of colors in color table
	LPSTR lpbi;              // pointer to packed-DIB
	LPBITMAPINFO lpbmi;      // pointer to BITMAPINFO structure (Win3.0)
	LPBITMAPCOREINFO lpbmc;  // pointer to BITMAPCOREINFO structure (old)
	BOOL bWinStyleDIB;       // flag which signifies whether this is a Win3.0 DIB
	BOOL bResult = FALSE;

	/* if handle to DIB is invalid, return FALSE */

	if (m_Image == NULL)
	  return FALSE;

   lpbi = (LPSTR) ::GlobalLock((unsigned char*) m_Image);

   /* get pointer to BITMAPINFO (Win 3.0) */
   lpbmi = (LPBITMAPINFO)lpbi;

   /* get pointer to BITMAPCOREINFO (old 1.x) */
   lpbmc = (LPBITMAPCOREINFO)lpbi;

   /* get the number of colors in the DIB */
   wNumColors = ::DIBNumColors(lpbi);

   if (wNumColors != 0)
   {
		/* allocate memory block for logical palette */
		hLogPal = ::GlobalAlloc(GHND, sizeof(LOGPALETTE)
									+ sizeof(PALETTEENTRY)
									* wNumColors);

		/* if not enough memory, clean up and return NULL */
		if (hLogPal == 0)
		{
			::GlobalUnlock((unsigned char*) m_Image);
			return FALSE;
		}

		lpPal = (LPLOGPALETTE) ::GlobalLock((unsigned char*) hLogPal);

		/* set version and number of palette entries */
		lpPal->palVersion = PALVERSION;
		lpPal->palNumEntries = (WORD)wNumColors;

		/* is this a Win 3.0 DIB? */
		bWinStyleDIB = IS_WIN30_DIB(lpbi);
		for (i = 0; i < (int)wNumColors; i++)
		{
			if (bWinStyleDIB)
			{
				lpPal->palPalEntry[i].peRed = lpbmi->bmiColors[i].rgbRed;
				lpPal->palPalEntry[i].peGreen = lpbmi->bmiColors[i].rgbGreen;
				lpPal->palPalEntry[i].peBlue = lpbmi->bmiColors[i].rgbBlue;
				lpPal->palPalEntry[i].peFlags = 0;
			}
			else
			{
				lpPal->palPalEntry[i].peRed = lpbmc->bmciColors[i].rgbtRed;
				lpPal->palPalEntry[i].peGreen = lpbmc->bmciColors[i].rgbtGreen;
				lpPal->palPalEntry[i].peBlue = lpbmc->bmciColors[i].rgbtBlue;
				lpPal->palPalEntry[i].peFlags = 0;
			}
		}

		/* create the palette and get handle to it */
		bResult = m_pPal->CreatePalette(lpPal);
		::GlobalUnlock((unsigned char*) hLogPal);
		::GlobalFree((unsigned char*) hLogPal);
	}

	::GlobalUnlock((unsigned char*) m_Image);

	return bResult;
}

void CMyImage::Free()
{
	if( m_Image )
	{
		if( GlobalFree( m_Image ) != NULL)
		{
			TRACE("Can't free handle in CMyImage::Free()");
		}
		m_Image = NULL;
	}
	if( m_UndoImage )
	{
		if( GlobalFree( m_UndoImage ) != NULL)
		{
			TRACE("Can't free handle in CRawImage::Free()");
		}
		m_UndoImage = NULL;
	}
	
	if(m_pPal != NULL)
	{
		delete m_pPal;
		m_pPal = NULL;
	}
}

int CMyImage::GetBitCount()
{
	if (m_Image == NULL) return -1;
	LPBITMAPINFOHEADER lpbi;
	lpbi = (LPBITMAPINFOHEADER) ::GlobalLock((unsigned char*) m_Image );
	return lpbi->biBitCount;
}

BOOL CMyImage::Draw(HDC hDC, LPRECT lpDIBRect, LPRECT lpDCRect)
{
	LPSTR	lpDIBHdr;	// BITMAPINFOHEADER
	LPSTR	lpDIBBits;	// DIB
	BOOL		bSuccess=FALSE;
	HPALETTE 	hPal=NULL;		 // DIB palette
	HPALETTE 	hOldPal=NULL;	 // old palette
	
	// GlobalLock
	lpDIBHdr  = (LPSTR) ::GlobalLock((unsigned char*) m_Image);
	lpDIBBits = ::FindDIBBits(lpDIBHdr);
	
	// Get palette
	if(m_pPal != NULL)
	{
		hPal = (HPALETTE) m_pPal->m_hObject;
		hOldPal = ::SelectPalette(hDC, hPal, TRUE);
	}
	
	::SetStretchBltMode(hDC, COLORONCOLOR);
	
	bSuccess = ::StretchDIBits(hDC, 	// hDC
		lpDCRect->left,					// DestX
		lpDCRect->top,					// DestY
		RECTWIDTH(lpDCRect),			// nDestWidth
		RECTHEIGHT(lpDCRect),			// nDestHeight
		lpDIBRect->left,				// SrcX
		(int)DIBHeight(lpDIBHdr) - lpDIBRect->top -	RECTHEIGHT(lpDIBRect),// SrcY
		RECTWIDTH(lpDIBRect),			// wSrcWidth
		RECTHEIGHT(lpDIBRect),			// wSrcHeight
		lpDIBBits,						// lpBits
		(LPBITMAPINFO)lpDIBHdr,			// lpBitsInfo
		DIB_RGB_COLORS,					// wUsage
		SRCCOPY);						// dwROP
	
	// Free memory
	::GlobalUnlock((unsigned char*) m_Image);
	// recover DC
	if (hOldPal != NULL) ::SelectPalette(hDC, hOldPal, TRUE);
	return bSuccess;
}

BOOL CMyImage::Undo()
{
	unsigned char* hTemp;	
	
	// exchange
	if(m_UndoImage)
	{
		hTemp = m_Image;
		m_Image = m_UndoImage;
		m_UndoImage = hTemp;
		return TRUE;
	}
	else
		return FALSE;
}

BOOL CMyImage::PrepareUndo()
{
	// remove old backup
	if(m_UndoImage) 
	{
		::GlobalFree((unsigned char*)m_UndoImage);
		m_UndoImage = NULL;
	}
	
	// copy data
	if ((m_UndoImage = (unsigned char*)::CopyHandle((unsigned char*)m_Image)) == NULL)
	{
		m_UndoImage = NULL;
		return FALSE;
	}
	return TRUE;
}

BOOL CMyImage::Save(LPCTSTR lpszFileName)
{
	CString filetype;
	filetype = lpszFileName;
	filetype.MakeUpper();

	if(filetype.Find(CString(".BMP")) > -1) return SaveBMP(lpszFileName);
	else if(filetype.Find(CString(".JPG")) > -1) return SaveJPG(lpszFileName);
	else return FALSE;
}

BOOL CMyImage::Load(LPCTSTR lpszFileName)
{
	filetype = lpszFileName;
	filetype.MakeUpper();
	
	if(filetype.Find(CString(".BMP")) > -1) return LoadBMP(lpszFileName);
	else if(filetype.Find(CString(".TIF")) > -1) return LoadTIF(lpszFileName);
	else if(filetype.Find(CString(".JPG")) > -1) return LoadJPG(lpszFileName);
	else return FALSE;
}

LPSTR FindDIBBits(LPSTR lpbi)
{
	return (lpbi + *(LPDWORD)lpbi + ::PaletteSize(lpbi));
}


DWORD DIBWidth(LPSTR lpDIB)
{
	LPBITMAPINFOHEADER lpbmi;  // pointer to a Win 3.0-style DIB
	LPBITMAPCOREHEADER lpbmc;  // pointer to an other-style DIB
	
	/* point to the header (whether Win 3.0 and old) */
	
	lpbmi = (LPBITMAPINFOHEADER)lpDIB;
	lpbmc = (LPBITMAPCOREHEADER)lpDIB;
	
	/* return the DIB width if it is a Win 3.0 DIB */
	if (IS_WIN30_DIB(lpDIB))
		return lpbmi->biWidth;
	else  /* it is an other-style DIB, so return its width */
		return (DWORD)lpbmc->bcWidth;
}


DWORD DIBHeight(LPSTR lpDIB)
{
	LPBITMAPINFOHEADER lpbmi;  // pointer to a Win 3.0-style DIB
	LPBITMAPCOREHEADER lpbmc;  // pointer to an other-style DIB
	
	/* point to the header (whether old or Win 3.0 */
	
	lpbmi = (LPBITMAPINFOHEADER)lpDIB;
	lpbmc = (LPBITMAPCOREHEADER)lpDIB;
	
	/* return the DIB height if it is a Win 3.0 DIB */
	if (IS_WIN30_DIB(lpDIB))
		return lpbmi->biHeight;
	else  /* it is an other-style DIB, so return its height */
		return (DWORD)lpbmc->bcHeight;
}



WORD PaletteSize(LPSTR lpbi)
{
	/* calculate the size required by the palette */
	if (IS_WIN30_DIB (lpbi))
		return (WORD)(::DIBNumColors(lpbi) * sizeof(RGBQUAD));
	else
		return (WORD)(::DIBNumColors(lpbi) * sizeof(RGBTRIPLE));
}



WORD DIBNumColors(LPSTR lpbi)
{
	WORD wBitCount;  // DIB bit count
	
					 /*  If this is a Windows-style DIB, the number of colors in the
					 *  color table can be less than the number of bits per pixel
					 *  allows for (i.e. lpbi->biClrUsed can be set to some value).
					 *  If this is the case, return the appropriate value.
	*/
	
	if (IS_WIN30_DIB(lpbi))
	{
		DWORD dwClrUsed;
		
		dwClrUsed = ((LPBITMAPINFOHEADER)lpbi)->biClrUsed;
		if (dwClrUsed != 0)
			return (WORD)dwClrUsed;
	}
	
	/*  Calculate the number of colors in the color table based on
	*  the number of bits per pixel for the DIB.
	*/
	if (IS_WIN30_DIB(lpbi))
		wBitCount = ((LPBITMAPINFOHEADER)lpbi)->biBitCount;
	else
		wBitCount = ((LPBITMAPCOREHEADER)lpbi)->bcBitCount;
	
	/* return number of colors based on bits per pixel */
	switch (wBitCount)
	{
	case 1:
		return 2;
		
	case 4:
		return 16;
		
	case 8:
		return 256;
		
	default:
		return 0;
	}
}

unsigned char* CopyHandle (unsigned char* h)
{
	if (h == NULL)
		return NULL;
	
	DWORD dwLen = ::GlobalSize((unsigned char*) h);
	unsigned char* hCopy = ::GlobalAlloc(GHND, dwLen);
	
	if (hCopy != NULL)
	{
		void* lpCopy = ::GlobalLock((unsigned char*) hCopy);
		void* lp     = ::GlobalLock((unsigned char*) h);
		memcpy(lpCopy, lp, dwLen);
		::GlobalUnlock(hCopy);
		::GlobalUnlock(h);
	}
	
	return hCopy;
}

BOOL CMyImage::SaveBMP(LPCTSTR lpszFileName)
{
	CFile file;
	CFileException fe;
	BITMAPFILEHEADER bmfHdr;
	LPBITMAPINFOHEADER lpBI;
	DWORD dwDIBSize;
	
	// file open (write)
	if (!file.Open(lpszFileName, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe)) return FALSE;
	
	// check memory handle
	if (m_Image == NULL) return FALSE;
	
	// GlobalLock
	lpBI = (LPBITMAPINFOHEADER)::GlobalLock((unsigned char*)m_Image);
	if (lpBI == NULL) return FALSE;
	
	// bitmap header
	bmfHdr.bfType = DIB_HEADER_MARKER;  // "BM"
	dwDIBSize = *(LPDWORD)lpBI + ::PaletteSize((LPSTR)lpBI);
	if((lpBI->biCompression==BI_RLE8) || (lpBI->biCompression==BI_RLE4))
		dwDIBSize += lpBI->biSizeImage;
	else 
	{
		DWORD dwBmBitsSize;  // Size of Bitmap Bits only
		dwBmBitsSize = WIDTHBYTES((lpBI->biWidth)*((DWORD)lpBI->biBitCount)) * lpBI->biHeight;
		dwDIBSize += dwBmBitsSize;
		lpBI->biSizeImage = dwBmBitsSize;
	}
	
	bmfHdr.bfSize = dwDIBSize + sizeof(BITMAPFILEHEADER);
	bmfHdr.bfReserved1 = 0;
	bmfHdr.bfReserved2 = 0;
	bmfHdr.bfOffBits=(DWORD)sizeof(BITMAPFILEHEADER)+lpBI->biSize + PaletteSize((LPSTR)lpBI);
	TRY
	{
		// write bitmap header on a file
		file.Write((LPSTR)&bmfHdr, sizeof(BITMAPFILEHEADER));
		// write other data on a file
		file.Write(lpBI, dwDIBSize);
	}
	CATCH (CFileException, e)
	{
		::GlobalUnlock((unsigned char*) m_Image);
		THROW_LAST();
	}
	END_CATCH
		
		// free memory
		::GlobalUnlock((unsigned char*) m_Image);
	return TRUE;
}


BOOL CMyImage::LoadBMP(LPCTSTR lpszFileName)
{
	CFile file;
	CFileException fe;
	LPSTR pDIB;
	DWORD dwBitsSize;
	BITMAPFILEHEADER bmfHeader;
	
	// file open (read)
	if(!file.Open(lpszFileName, CFile::modeRead|CFile::shareDenyWrite, &fe))
		return FALSE;
	
	// file length
	dwBitsSize = file.GetLength();
	
	// file header
	if(file.Read((LPSTR)&bmfHeader, sizeof(bmfHeader))!=sizeof(bmfHeader))
		return FALSE;
	
	// check "BM" marker
	if (bmfHeader.bfType != DIB_HEADER_MARKER)
		return FALSE;
	
	// memory allocation
	if((m_Image = (unsigned char*)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, dwBitsSize)) == NULL) return FALSE;
	
	// GlobalLock
	pDIB = (LPSTR) ::GlobalLock((unsigned char*) m_Image);
	
	// read a file
	if (file.Read(pDIB, dwBitsSize - sizeof(BITMAPFILEHEADER)) != dwBitsSize - sizeof(BITMAPFILEHEADER) ) 
	{
		::GlobalUnlock((unsigned char*) m_Image);
		::GlobalFree((unsigned char*) m_Image);
		return FALSE;
	}
	
	// free memory
	::GlobalUnlock((unsigned char*) m_Image);
	
	// initialize DIB
	InitDIB();
	
	return TRUE;
}

BOOL CMyImage::LoadTIF(LPCTSTR lpszFileName)
{
	m_Image = LoadTIFFinDIB((LPSTR)lpszFileName);
	InitDIB(FALSE);
	return TRUE;
}

/*
* Here's the routine that will replace the standard error_exit method:
*/

METHODDEF void
ima_jpeg_error_exit (j_common_ptr cinfo)
{
	/* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
	ima_error_ptr myerr = (ima_error_ptr) cinfo->err;
	
	char buffer[JMSG_LENGTH_MAX];
	
	/* Create the message */
	myerr->pub.format_message (cinfo, buffer);
	
	/* Send it to stderr, adding a newline */
	//        AfxMessageBox(buffer);
	
	/* Return control to the setjmp point */
	longjmp(myerr->setjmp_buffer, 1);
}

BOOL CMyImage::LoadJPG(LPCTSTR lpszFileName)
{
	
	BOOL bGray = FALSE;
	/* This struct contains the JPEG decompression parameters and pointers to
	* working space (which is allocated as needed by the JPEG library).
	*/
	struct jpeg_decompress_struct cinfo;
	/* We use our private extension JPEG error handler. */
	
	struct ima_error_mgr jerr;
	//  struct jpeg_error_mgr jerr;
	/* More stuff */
	FILE * infile;		/* source file */
	JSAMPARRAY buffer;		/* Output row buffer */
	int row_stride;		/* physical row width in output buffer */
	
						/* In this example we want to open the input file before doing anything else,
						* so that the setjmp() error recovery below can assume the file is open.
						* VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
						* requires it in order to read binary files.
	*/
	
	if ((infile = fopen(lpszFileName, "rb")) == NULL) {
		//fprintf(stderr, "can't open %s\n", filename);
		return 0;
	}
	
	/* Step 1: allocate and initialize JPEG decompression object */
	/* We set up the normal JPEG error routines, then override error_exit. */
	cinfo.err = jpeg_std_error(&jerr.pub);
	jerr.pub.error_exit = ima_jpeg_error_exit;
	
	/* Establish the setjmp return context for my_error_exit to use. */
	if (setjmp(jerr.setjmp_buffer)) {
	/* If we get here, the JPEG code has signaled an error.
	* We need to clean up the JPEG object, close the input file, and return.
		*/
		jpeg_destroy_decompress(&cinfo);
		fclose(infile);
		return 0;
	}
	/* Now we can initialize the JPEG decompression object. */
	jpeg_create_decompress(&cinfo);
	
	/* Step 2: specify data source (eg, a file) */
	jpeg_stdio_src(&cinfo, infile);
	
	/* Step 3: read file parameters with jpeg_read_header() */
	(void) jpeg_read_header(&cinfo, TRUE);
	
	/* Step 4: set parameters for decompression */
	//  printf("info %d %d %d CS %d ", cinfo.image_width, cinfo.image_height, cinfo.output_components, cinfo.jpeg_color_space);
	if (cinfo.jpeg_color_space!=JCS_GRAYSCALE) {
		cinfo.quantize_colors = TRUE;
		cinfo.desired_number_of_colors = 128;
	}
	/* Step 5: Start decompressor */
	jpeg_start_decompress(&cinfo);
	
	/* We may need to do some setup of our own at this point before reading
	* the data.  After jpeg_start_decompress() we have the correct scaled
	* output image dimensions available, as well as the output colormap
	* if we asked for color quantization.
	*/
	
	if (cinfo.jpeg_color_space==JCS_GRAYSCALE)
	{
		bGray = TRUE;
		//	  CreateImage(cinfo.image_width, cinfo.image_height, 8);
		Create(cinfo.image_width, cinfo.image_height, 8);
	}
	else
		//CreateImage(cinfo.image_width, cinfo.image_height, 24);
		Create(cinfo.image_width, cinfo.image_height, 24);
	
	/* JSAMPLEs per row in output buffer */
	row_stride = cinfo.output_width * cinfo.output_components;
	//  byte* buf2 = new byte[row_stride];
	//  printf("NCMPS cmp [%d %d %d]", cinfo.output_components, cinfo.actual_number_of_colors,row_stride);
	
	/* Make a one-row-high sample array that will go away when done with image */
	buffer = (*cinfo.mem->alloc_sarray)
		((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);
	
	/* Step 6: while (scan lines remain to be read) */
	/*           jpeg_read_scanlines(...); */
	
	/* Here we use the library's state variable cinfo.output_scanline as the
	* loop counter, so that we don't have to keep track ourselves.
	*/
	int line, col;
	
	if(bGray)
	{
		////////////////////////////////////////////////////////////////////////////
		int m_nHeight = GetHeight();		// 이미지의 가로 길이
		int nWidth = GetRealWidth();	// 이미지의 세로 길이
		
		// memory allocation
		BYTE** m_pPtr = new BYTE *[m_nHeight]; 
		
		// get memory handle
		unsigned char* m_hHandle = GetHandle();
		LPSTR lpDIBHdr  = (LPSTR) ::GlobalLock((unsigned char*) m_hHandle);
		
		// pointer of first pixel
		BYTE *lpDIBBits = (BYTE *) ::FindDIBBits(lpDIBHdr);
		
		// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
		// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
		for(int i=m_nHeight -1 ; i>=0 ; i--)
		{
			m_pPtr[i] = lpDIBBits;
			lpDIBBits += nWidth;
		}
		////////////////////////////////////////////////////////////////////////////////
		
		line = 0;
		while (cinfo.output_scanline < cinfo.output_height) 
		{
			(void) jpeg_read_scanlines(&cinfo, buffer, 1);
			/* Assume put_scanline_someplace wants a pointer and sample count. */
			memcpy(m_pPtr[line++], buffer[0], row_stride);
		}
	}
	else 
	{
		//////////////////////////////////////////////////////////////////////////////////////
		//image size
		int m_nHeight = GetHeight();
		int nWidth = GetRealWidth();
		
		// memory allocation
		_LPCOLOR_* m_pPtr = new _LPCOLOR_ [m_nHeight];
		
		// get memory handle
		unsigned char* m_hHandle = GetHandle();
		LPSTR lpDIBHdr  = (LPSTR)::GlobalLock((unsigned char*) m_hHandle);
		
		// pointer of first pixel
		LPSTR lpDIBBits = (LPSTR)::FindDIBBits(lpDIBHdr);
		
		// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
		// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
		for(int i=m_nHeight-1 ; i>=0 ; i--)
		{
			m_pPtr[i] = (_LPCOLOR_)lpDIBBits;
			lpDIBBits += nWidth;
		}
		//////////////////////////////////////////////////////////////////////////////////////////
		
		line = 0;
		
		if(cinfo.output_components == 3)
		{
			while (cinfo.output_scanline < cinfo.output_height) 
			{
				(void) jpeg_read_scanlines(&cinfo, buffer, 1);
				/* Assume put_scanline_someplace wants a pointer and sample count. */
				memcpy(m_pPtr[line++], buffer[0], row_stride);
			}
		}
		else
		{
			while (cinfo.output_scanline < cinfo.output_height) 
			{
				(void) jpeg_read_scanlines(&cinfo, buffer, 1);
				/* Assume put_scanline_someplace wants a pointer and sample count. */
				for(col=0 ; col<row_stride ; col++) 
				{
					m_pPtr[line][col].R = cinfo.colormap[0][buffer[0][col]];
					m_pPtr[line][col].G = cinfo.colormap[1][buffer[0][col]];
					m_pPtr[line][col].B = cinfo.colormap[2][buffer[0][col]];
				}
				line++;
			}
		}
	}
	
	
	/* Step 7: Finish decompression */
	(void) jpeg_finish_decompress(&cinfo);
	/* We can ignore the return value since suspension is not possible
	* with the stdio data source.
	*/
	
	/* Step 8: Release JPEG decompression object */
	
	/* This is an important step since it will release a good deal of memory. */
	jpeg_destroy_decompress(&cinfo);
	
	/* After finish_decompress, we can close the input file.
	* Here we postpone it until after no more JPEG errors are possible,
	* so as to simplify the setjmp error logic above.  (Actually, I don't
	* think that jpeg_destroy can do an error exit, but why assume anything...)
	*/
	fclose(infile);
	/* At this point you may want to check to see whether any corrupt-data
	* warnings occurred (test whether jerr.pub.num_warnings is nonzero).
	*/
	
	/* And we're done! */
	return 1;
}

BOOL CMyImage::SaveJPG(LPCTSTR lpszFileName)
{
	BOOL bGray = FALSE;

	int col, i;
		
	struct jpeg_compress_struct cinfo;
	/* This struct represents a JPEG error handler.  It is declared separately
	* because applications often want to supply a specialized error handler
	* (see the second half of this file for an example).  But here we just
	* take the easy way out and use the standard error handler, which will
	* print a message on stderr and call exit() if compression fails.
	* Note that this struct must live as long as the main JPEG parameter
	* struct, to avoid dangling-pointer problems.
	*/
	struct jpeg_error_mgr jerr;
	/* More stuff */
	FILE * outfile;		/* target file */
	int row_stride;		/* physical row width in image buffer */
	JSAMPARRAY buffer;		/* Output row buffer */
	
	/* Step 1: allocate and initialize JPEG compression object */
	
	/* We have to set up the error handler first, in case the initialization
	* step fails.  (Unlikely, but it could happen if you are out of memory.)
	* This routine fills in the contents of struct jerr, and returns jerr's
	* address which we place into the link field in cinfo.
	*/
	cinfo.err = jpeg_std_error(&jerr);
	/* Now we can initialize the JPEG compression object. */
	jpeg_create_compress(&cinfo);
	
	/* Step 2: specify data destination (eg, a file) */
	/* Note: steps 2 and 3 can be done in either order. */
	
	/* Here we use the library-supplied code to send compressed data to a
	* stdio stream.  You can also write your own code to do something else.
	* VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
	* requires it in order to write binary files.
	*/
	if ((outfile = fopen(lpszFileName, "wb")) == NULL) 
	{
		//fprintf(stderr, "can't open %s\n", filename);
		return FALSE;
	}
	jpeg_stdio_dest(&cinfo, outfile);
	
	/* Step 3: set parameters for compression */
	
	/* First we supply a description of the input image.
	* Four fields of the cinfo struct must be filled in:
	*/
	cinfo.image_width = GetWidth(); 	// image width and height, in pixels
	cinfo.image_height = GetHeight();
	if(GetBitCount() == 8)
	{
		cinfo.input_components = 1; 	// # of color components per pixel
		cinfo.in_color_space = JCS_GRAYSCALE; 	/* colorspace of input image */
		bGray = TRUE;
	}
	else
	{
		cinfo.input_components = 3; 	// # of color components per pixel
		cinfo.in_color_space = JCS_RGB; 	/* colorspace of input image */
	}
	
	//  printf("info %d %d %d %d ", cinfo.image_width, cinfo.image_height, cinfo.input_components, cinfo.in_color_space);
	/* Now use the library's routine to set default compression parameters.
	* (You must set at least cinfo.in_color_space before calling this,
	* since the defaults depend on the source color space.)
	*/
	jpeg_set_defaults(&cinfo);
	/* Now you can set any non-default parameters you wish to.
	* Here we just illustrate the use of quality (quantization table) scaling:
	*/
	//jpeg_set_quality(&cinfo, 30, TRUE /* limit to baseline-JPEG values */);
	
	/* Step 4: Start compressor */
	
	/* TRUE ensures that we will write a complete interchange-JPEG file.
	* Pass TRUE unless you are very sure of what you're doing.
	*/
	//  puts("begining");
	jpeg_start_compress(&cinfo, TRUE);
	
	/* Step 5: while (scan lines remain to be written) */
	/*           jpeg_write_scanlines(...); */
	
	/* Here we use the library's state variable cinfo.next_scanline as the
	* loop counter, so that we don't have to keep track ourselves.
	* To keep things simple, we pass one scanline per call; you can pass
	* more if you wish, though.
	*/
	row_stride = GetWidth()*cinfo.input_components;	/* JSAMPLEs per row in image_buffer */
	
	buffer = (*cinfo.mem->alloc_sarray)
		((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);
	
	int line = 0;
	if (bGray) 
	{
		////////////////////////////////////////////////////////////////////////////
		int m_nHeight = GetHeight();		// 이미지의 가로 길이
		int nWidth = GetRealWidth();	// 이미지의 세로 길이
		
		// 이미지의 세로 길이 만큼 포인터 배열 할당
		BYTE** m_pPtr = new BYTE *[m_nHeight]; 
		
		// 메모리 블록을 고정
		unsigned char* m_hHandle = GetHandle();
		LPSTR lpDIBHdr  = (LPSTR) ::GlobalLock((unsigned char*) m_hHandle);
		
		// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
		BYTE *lpDIBBits = (BYTE *) ::FindDIBBits(lpDIBHdr);
		
		// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
		// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
		for(int i=m_nHeight -1 ; i>=0 ; i--)
		{
			m_pPtr[i] = lpDIBBits;
			lpDIBBits += nWidth;
		}
		////////////////////////////////////////////////////////////////////////////////
		
		while (cinfo.next_scanline < cinfo.image_height) 
		{
			memcpy(buffer[0], m_pPtr[line++], row_stride);
			(void) jpeg_write_scanlines(&cinfo, buffer, 1);
		}
	}
	else
	{
		//////////////////////////////////////////////////////////////////////////////////////
		int m_nHeight = GetHeight();		// 이미지의 가로 길이
		int nWidth = GetRealWidth();	// 이미지의 세로 길이
		
		// 이미지의 세로 길이 만큼 포인터 배열 할당
		_LPCOLOR_* m_pPtr = new _LPCOLOR_ [m_nHeight];
		
		// 메모리 블록을 고정
		unsigned char* m_hHandle = GetHandle();
		LPSTR lpDIBHdr  = (LPSTR)::GlobalLock((unsigned char*) m_hHandle);
		
		// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
		LPSTR lpDIBBits = (LPSTR)::FindDIBBits(lpDIBHdr);
		
		// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
		// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
		for(int i=m_nHeight-1 ; i>=0 ; i--)
		{
			m_pPtr[i] = (_LPCOLOR_)lpDIBBits;
			lpDIBBits += nWidth;
		}
		//////////////////////////////////////////////////////////////////////////////////////////
		while (cinfo.next_scanline < cinfo.image_height) 
		{
			for(col=0, i=0 ; i<row_stride ; col++, i+=3)
			{
				buffer[0][i] = m_pPtr[line][col].R;
				buffer[0][i+1] = m_pPtr[line][col].G;
				buffer[0][i+2] = m_pPtr[line][col].B;
			}
			line++;
			(void) jpeg_write_scanlines(&cinfo, buffer, 1);
		}
		
	}
	
	/* Step 6: Finish compression */
	
	jpeg_finish_compress(&cinfo);
	/* After finish_compress, we can close the output file. */
	fclose(outfile);
	
	/* Step 7: release JPEG compression object */
	/* This is an important step since it will release a good deal of memory. */
	jpeg_destroy_compress(&cinfo);
	
	/* And we're done! */
	return TRUE;
}