
// viewerDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "viewer.h"
#include "viewerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
	ON_WM_MOUSEHWHEEL()
END_MESSAGE_MAP()


// CviewerDlg dialog



CviewerDlg::CviewerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_VIEWER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	leftMDown = false;
	zFactor = 1.0;
}

void CviewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CviewerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()


// CviewerDlg message handlers

BOOL CviewerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	CString imgPath = CString("C:\\Users\\bbarab\\Pictures\\black-holes-infographic-v2-1024x725.bmp");
	this->loadBMPFile(imgPath);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CviewerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CviewerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this);
		OnDraw(&dc);
		CDialogEx::OnPaint();
	}
}

void CviewerDlg::OnDraw(CDC* pDC)
{
	if (!basicImg->IsDataNull())
	{
		/// Image point coordinate located in the center of the display window
		GetClientRect(&rcDest);
		cx = cx + ((double)MovePosition.x / zFactor);
		cy = cy + ((double)MovePosition.y / zFactor);

		/// Size of the displayed image part
		subW = (double)rcDest.Width() / zFactor;
		subH = (double)rcDest.Height() / zFactor;

		/// Region(rectangle) of the displayed image part
		CRect rcDIB;
		
		rcDIB.left = (int)(cx - subW / 2. + 0.5);
		rcDIB.top = (int)(cy - subH / 2. + 0.5);

		rcDIB.right = rcDIB.left + int(subW + 0.5);
		rcDIB.bottom = rcDIB.top + int(subH + 0.5);

		basicImg->Draw(pDC->m_hDC, &rcDIB, &rcDest);

		//DrawMark(-100,-100,5, RGB(0,255,255),RGB(255,255,0));
	}
}

BOOL CviewerDlg::DrawMark(double col, double row, double lth, COLORREF color1, COLORREF color2)
{
	int wx, wy;

	if (!basicImg->IsDataNull())
	{
		GetClientRect(&rcDest);

		subW = (double)rcDest.Width() / zFactor;
		subH = (double)rcDest.Height() / zFactor;

		double left = (cx - subW / 2. + 0.5);//left
		double top = (cy - subH / 2. + 0.5);//top

		double right = left + int(subW + 0.5);
		double bottom = top + int(subH + 0.5);

		if ((col < left) || (col > right)) return FALSE;
		if ((row < top) || (row > bottom)) return FALSE;

		wx = (int)((col - left) * zFactor + 0.5);
		wy = (int)((row - top) * zFactor + 0.5);

		CDC* pDC = this->GetDC();
		CPen pen1, pen2;
		CPen* oldpen;
		pen2.CreatePen(PS_SOLID, 1, color1);
		pen1.CreatePen(PS_SOLID, 1, color2);


		oldpen = (CPen*)pDC->SelectObject(&pen1);

		pDC->SelectObject(&pen1);

		pDC->MoveTo(int(wx - lth), int(wy - lth));
		pDC->LineTo(int(wx + lth), int(wy + lth));
		pDC->LineTo(int(wx + lth), int(wy - lth));
		pDC->LineTo(int(wx - lth), int(wy + lth));
		pDC->LineTo(int(wx - lth), int(wy - lth));
		pDC->LineTo(int(wx + lth), int(wy - lth));
		pDC->MoveTo(int(wx - lth), int(wy + lth));
		pDC->LineTo(int(wx + lth), int(wy + lth));

		pDC->SelectObject(&pen2);

		pDC->Ellipse(int(wx - 1.), int(wy - 1.), int(wx + 1.), int(wy + 1.));

		pDC->SelectObject(oldpen);
	}

	return TRUE;
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CviewerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



BOOL CviewerDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	double zFactor0 = zFactor;
	double cx0 = cx;
	double cy0 = cy;

	if (zDelta > 0)
	{
		if (zFactor < rcDest.Width())
			zFactor *= 1.1;
	}
	else
	{
		if (zFactor > 1. / basicImg->GetWidth())
			zFactor /= 1.1;
	}

	Invalidate(TRUE);

	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}


void CviewerDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (leftMDown == true)
	{
		MovePosition = sPosition - point;
		sPosition = point;
		Invalidate(FALSE);
	}
	else
	{
		MovePosition.x = 0;
		MovePosition.y = 0;
	}

	CDialogEx::OnMouseMove(nFlags, point);
}


void CviewerDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	leftMDown = true;
	sPosition = point;

	CDialogEx::OnLButtonDown(nFlags, point);
}


void CviewerDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	leftMDown = false;
	Invalidate(TRUE);

	CDialogEx::OnLButtonUp(nFlags, point);
}
