#pragma once

#ifdef VIEWER_EXPORT
#define VIEWER_API __declspec(dllexport)
#else
#define VIEWER_API __declspec(dllimport)
#endif

#include "DataDef.h"

namespace image
{
	//
	//CPixel
	//
	class VIEWER_API CPixel
	{
	public:
		int I;
		CPixel::CPixel() {}

		CPixel::~CPixel() {}

		CPixel::CPixel(int i) { I = i; };

		inline CPixel operator>> (BYTE&);
		inline CPixel operator<< (BYTE);
		inline CPixel operator<< (int);
		inline operator BYTE() { return I; }
		inline operator int() { return (int)I; }
		inline CPixel operator= (int);
		inline CPixel operator* (int);
		inline CPixel operator/ (int);
		inline CPixel operator+ (int);
		inline CPixel operator- (int);
		inline CPixel operator*= (int);
		inline CPixel operator/= (int);
		inline CPixel operator+= (int);
		inline CPixel operator-= (int);

		inline CPixel operator= (double);
		inline CPixel operator* (double);
		inline CPixel operator/ (double);
		inline CPixel operator+ (double);
		inline CPixel operator- (double);
		inline CPixel operator*= (double);
		inline CPixel operator/= (double);
		inline CPixel operator+= (double);
		inline CPixel operator-= (double);

	};

	//
	//CColorPixel
	//
	class VIEWER_API CColorPixel
	{
	public:
		CColorPixel() { R = 0; G = 0; B = 0; }

		CColorPixel(COLOR c) { R = c.R; G = c.G; B = c.B; };

		~CColorPixel() {};

		inline int GetR() { return R; }
		inline int GetG() { return G; }
		inline int GetB() { return B; }

		inline void SetR(int value) { R = value; }
		inline void SetG(int value) { G = value; }
		inline void SetB(int value) { B = value; }

		inline CColorPixel operator= (COLOR);
		inline CColorPixel operator >>(COLOR&);
		inline CColorPixel operator <<(COLOR);
		inline CColorPixel operator= (CColorPixel);
		inline CColorPixel operator* (CColorPixel);
		inline CColorPixel operator/ (CColorPixel);
		inline CColorPixel operator+ (CColorPixel);
		inline CColorPixel operator- (CColorPixel);
		inline CColorPixel operator*= (CColorPixel);
		inline CColorPixel operator/= (CColorPixel);
		inline CColorPixel operator+= (CColorPixel);
		inline CColorPixel operator-= (CColorPixel);
		inline bool operator== (CColorPixel);

		inline CColorPixel operator= (int);
		inline CColorPixel operator* (int);
		inline CColorPixel operator/ (int);
		inline CColorPixel operator+ (int);
		inline CColorPixel operator- (int);
		inline CColorPixel operator*= (int);
		inline CColorPixel operator/= (int);
		inline CColorPixel operator+= (int);
		inline CColorPixel operator-= (int);

		inline CColorPixel operator= (double);
		inline CColorPixel operator* (double);
		inline CColorPixel operator/ (double);
		inline CColorPixel operator+ (double);
		inline CColorPixel operator- (double);
		inline CColorPixel operator*= (double);
		inline CColorPixel operator/= (double);
		inline CColorPixel operator+= (double);
		inline CColorPixel operator-= (double);

	protected:
		int B, G, R;
	};
}