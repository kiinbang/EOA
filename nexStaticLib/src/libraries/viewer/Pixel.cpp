#include "pch.h"

#include "Pixel.h"

namespace image
{
	/******************************************************
	CPixel
	******************************************************/

	CPixel CPixel::operator >> (BYTE& i)
	{
		i = (BYTE)__max(__min(255, I), 0);
		return *this;
	};

	CPixel CPixel::operator << (BYTE a)
	{
		I = a;
		return *this;
	};

	CPixel CPixel::operator << (int a)
	{
		I = a;
		return *this;
	};

	CPixel CPixel::operator= (int a)
	{
		I = a;
		return *this;
	};

	CPixel CPixel::operator* (int a)
	{
		CPixel r;
		r.I = I * a;
		return r;
	};

	CPixel CPixel::operator/ (int a)
	{
		CPixel r;
		r.I = (int)((double)I / a + 0.5);
		return r;
	};

	CPixel CPixel::operator+ (int a)
	{
		CPixel r;
		r.I = I + a;
		return r;
	};

	CPixel CPixel::operator- (int a)
	{
		CPixel r;
		r.I = I - a;
		return r;
	};

	CPixel CPixel::operator*= (int a)
	{
		I *= a;
		return *this;
	};

	CPixel CPixel::operator/= (int a)
	{
		I = (int)((double)I / a + 0.5);
		return *this;
	};

	CPixel CPixel::operator+= (int a)
	{
		I += a;
		return *this;
	};

	CPixel CPixel::operator-= (int a)
	{
		I -= a;
		return *this;
	};

	CPixel CPixel::operator= (double a)
	{
		I = (int)(a + 0.5);
		return *this;
	};

	CPixel CPixel::operator* (double a)
	{
		CPixel r;
		r.I = (int)((double)I * a + 0.5);
		return r;
	};

	CPixel CPixel::operator/ (double a)
	{
		CPixel r;
		r.I = (int)((double)I / a + 0.5);
		return r;
	};

	CPixel CPixel::operator+ (double a)
	{
		CPixel r;
		r.I = (int)((double)I + a + 0.5);
		return r;
	};

	CPixel CPixel::operator- (double a)
	{
		CPixel r;
		r.I = (int)((double)I - a + 0.5);
		return r;
	};

	CPixel CPixel::operator*= (double a)
	{
		I = (int)((double)I * a + 0.5);
		return *this;
	};

	CPixel CPixel::operator/= (double a)
	{
		I = (int)((double)I / a + 0.5);
		return *this;
	};

	CPixel CPixel::operator+= (double a)
	{
		I = (int)((double)I + a + 0.5);
		return *this;
	};

	CPixel CPixel::operator-= (double a)
	{
		I = (int)((double)I - a + 0.5);
		return *this;
	};

	/******************************************************
	CColorPixel
	******************************************************/

	CColorPixel CColorPixel::operator=(COLOR c)
	{
		R = c.R;
		G = c.G;
		B = c.B;
		return *this;
	};

	CColorPixel CColorPixel::operator <<(COLOR c)
	{
		R = c.R;
		G = c.G;
		B = c.B;
		return *this;
	};

	CColorPixel CColorPixel::operator >>(COLOR& c)
	{
		c.R = (BYTE)__max(__min(255, R), 0);
		c.G = (BYTE)__max(__min(255, G), 0);
		c.B = (BYTE)__max(__min(255, B), 0);
		return *this;
	};

	CColorPixel CColorPixel::operator= (CColorPixel c)
	{
		R = c.R;
		G = c.G;
		B = c.B;
		return *this;
	};

	CColorPixel CColorPixel::operator* (CColorPixel a)
	{
		CColorPixel r;
		r.R = R * a.R;
		r.G = G * a.G;
		r.B = B * a.B;
		return r;
	};

	CColorPixel CColorPixel::operator/ (CColorPixel a)
	{
		CColorPixel r;
		r.R = (int)((double)R / a.R + 0.5);
		r.G = (int)((double)G / a.G + 0.5);
		r.B = (int)((double)B / a.B + 0.5);
		return r;
	};

	CColorPixel CColorPixel::operator+ (CColorPixel a)
	{
		CColorPixel r;
		r.R = R + a.R;
		r.G = G + a.G;
		r.B = B + a.B;
		return r;
	};

	CColorPixel CColorPixel::operator- (CColorPixel a)
	{
		CColorPixel r;
		r.R = R - a.R;
		r.G = G - a.G;
		r.B = B - a.B;
		return r;
	};

	CColorPixel CColorPixel::operator*= (CColorPixel a)
	{
		R = R * a.R;
		G = G * a.G;
		B = B * a.B;
		return *this;
	};

	CColorPixel CColorPixel::operator/= (CColorPixel a)
	{
		R = (int)((double)R / a.R + 0.5);
		G = (int)((double)G / a.G + 0.5);
		B = (int)((double)B / a.B + 0.5);
		return *this;
	};

	CColorPixel CColorPixel::operator+= (CColorPixel a)
	{
		R = R + a.R;
		G = G + a.G;
		B = B + a.B;
		return *this;
	};

	CColorPixel CColorPixel::operator-= (CColorPixel a)
	{
		R = R - a.R;
		G = G - a.G;
		B = B - a.B;
		return *this;
	};

	CColorPixel CColorPixel::operator= (int a)
	{
		R = a;
		G = a;
		B = a;
		return *this;
	};

	CColorPixel CColorPixel::operator* (int a)
	{
		R = R * a;
		G = G * a;
		B = B * a;
		return *this;
	};

	CColorPixel CColorPixel::operator/ (int a)
	{
		R = (int)((double)R / a + 0.5);
		G = (int)((double)G / a + 0.5);
		B = (int)((double)B / a + 0.5);
		return *this;
	};

	CColorPixel CColorPixel::operator+ (int a)
	{
		CColorPixel r;
		r.R = R + a;
		r.G = G + a;
		r.B = B + a;
		return r;
	};

	CColorPixel CColorPixel::operator- (int a)
	{
		CColorPixel r;
		r.R = R - a;
		r.G = G - a;
		r.B = B - a;
		return r;
	};

	CColorPixel CColorPixel::operator*= (int a)
	{
		R = R * a;
		G = G * a;
		B = B * a;
		return *this;
	};

	CColorPixel CColorPixel::operator/= (int a)
	{
		R = (int)((double)R / a + 0.5);
		G = (int)((double)G / a + 0.5);
		B = (int)((double)B / a + 0.5);
		return *this;
	};

	CColorPixel CColorPixel::operator+= (int a)
	{
		R = R + a;
		G = G + a;
		B = B + a;
		return *this;
	};

	CColorPixel CColorPixel::operator-= (int a)
	{
		R = R - a;
		G = G - a;
		B = B - a;
		return *this;
	};

	CColorPixel CColorPixel::operator= (double a)
	{
		R = (int)(a + 0.5);
		G = (int)(a + 0.5);
		B = (int)(a + 0.5);
		return *this;
	};

	CColorPixel CColorPixel::operator* (double a)
	{
		R = (int)((double)R * a + 0.5);
		G = (int)((double)G * a + 0.5);
		B = (int)((double)B * a + 0.5);
		return *this;
	};

	CColorPixel CColorPixel::operator/ (double a)
	{
		R = (int)((double)R / a + 0.5);
		G = (int)((double)G / a + 0.5);
		B = (int)((double)B / a + 0.5);
		return *this;
	};

	CColorPixel CColorPixel::operator+ (double a)
	{
		CColorPixel r;
		r.R = (int)((double)R + a + 0.5);
		r.G = (int)((double)G + a + 0.5);
		r.B = (int)((double)B + a + 0.5);
		return r;
	};

	CColorPixel CColorPixel::operator- (double a)
	{
		CColorPixel r;
		r.R = (int)((double)R - a + 0.5);
		r.G = (int)((double)G - a + 0.5);
		r.B = (int)((double)B - a + 0.5);
		return r;
	};

	CColorPixel CColorPixel::operator*= (double a)
	{
		R = (int)((double)R * a + 0.5);
		G = (int)((double)G * a + 0.5);
		B = (int)((double)B * a + 0.5);
		return *this;
	};

	CColorPixel CColorPixel::operator/= (double a)
	{
		R = (int)((double)R / a + 0.5);
		G = (int)((double)G / a + 0.5);
		B = (int)((double)B / a + 0.5);

		return *this;
	};

	CColorPixel CColorPixel::operator+= (double a)
	{
		CColorPixel r;
		r.R = (int)((double)R + a + 0.5);
		r.G = (int)((double)G + a + 0.5);
		r.B = (int)((double)B + a + 0.5);
		return *this;
	};

	CColorPixel CColorPixel::operator-= (double a)
	{
		CColorPixel r;
		r.R = (int)((double)R - a + 0.5);
		r.G = (int)((double)G - a + 0.5);
		r.B = (int)((double)B - a + 0.5);
		return *this;
	};

	bool CColorPixel::operator== (CColorPixel p)
	{
		if ((R == p.R) && (G == p.G) && (B == p.B))
			return true;
		else return false;
	};
}