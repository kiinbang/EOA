
// viewerDlg.h : header file
//

#pragma once

#ifdef VIEWER_EXPORT
#define VIEWER_API __declspec(dllexport)
#else
#define VIEWER_API __declspec(dllimport)
#endif

#include <iostream>
#include <memory>
#include <string>

#include "BMPImage.h"

// CviewerDlg dialog
class VIEWER_API CviewerDlg : public CDialogEx
{
// Construction
public:
	CviewerDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_VIEWER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
	/// Image instance
	std::shared_ptr<image::CBasicImage> basicImg;
	/// Source image path
	CString imagePath;
	/// Sub-image width and height
	double subW, subH;
	/// Rect size
	double cx, cy;
	/// Left mouse button down
	bool leftMDown;
	/// Zoom factor
	double zFactor;
	/// Draw area
	CRect rcDest;
	///
	CPoint sPosition;
	///
	CPoint MovePosition;

private:
	bool loadBMPFile(const CString& path)
	{
		imagePath = path;

		std::shared_ptr<image::CBMPImage> image(new image::CBMPImage);

		if (!image->LoadBMP(imagePath))
			return false;

		int bitcount = image->GetBitCount();
		if (!(bitcount == 8 || bitcount == 24))
		{
			image->Free();
			throw std::runtime_error("In this module, only 1 byte(gray) and 3 byte (color) images are supported.\n");
			return false;
		}

		/// image center
		cx = (double)image->GetWidth() / 2.;
		cy = (double)image->GetHeight() / 2.;

		basicImg = std::static_pointer_cast<image::CBasicImage>(image);

		return false;
	}

private:
	/// Draw an image
	void OnDraw(CDC* pDC);

	///Draw marks (posCol, posRow, length, 1st color, 2nd color)
	BOOL DrawMark(double col, double row, double lth, COLORREF color1, COLORREF color2);

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
};
