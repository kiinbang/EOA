// BasicImage.h: interface for the CBasicImage class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#ifdef VIEWER_EXPORT
#define VIEWER_API __declspec(dllexport)
#else
#define VIEWER_API __declspec(dllimport)
#endif

#include "DataDef.h"

namespace image
{
	class VIEWER_API CBasicImage
	{
	public:
		CBasicImage();
		CBasicImage(CBasicImage& Src);
		virtual ~CBasicImage();
		void Free();

		void SetHandle(void* hHandle);

		BOOL InitDIB(BOOL bCreatePalette = TRUE);
		BOOL CreateDIBPalette();
		BOOL Create(int width, int height, int depth);

		/**GetRainbowColor
		* Description	    : build pseudo color value (256 color)
		*@param unsigned char index	: intensity value
		*@param unsigned char &r		: red
		*@param unsigned char &g		: green
		*@param unsigned char &b		: blue
		*@return void
		*/
		static void GetRainbowColor(unsigned char index, unsigned char& red, unsigned char& green, unsigned char& blue);
		static void GetRainbowColor_new(unsigned char index, unsigned char& red, unsigned char& green, unsigned char& blue);
		static void GetRainbowColorfromXYZ(double X, double Y, double Z, unsigned char& r, unsigned char& g, unsigned char& b);

		//Operator
		CBasicImage& operator=(CBasicImage&);
		CBasicImage* operator=(CBasicImage*);

		//Undo
		BOOL Undo();
		BOOL PrepareUndo();
		BOOL CanUndo() { return (m_hUndoImage != NULL); }

		BOOL Draw(HDC hDC, LPRECT sourceRect, LPRECT destRect);

		// 이미지 정보를 얻는 함수
		int GetBitCount();
		HDIB GetHandle() { return m_hImage; }
		BOOL IsDataNull() { return (m_hImage == NULL); }
		CSize GetSize() { return m_Size; }
		int GetHeight() { return m_Size.cy; }
		int GetWidth() { return m_Size.cx; }
		int GetRealWidth() { return WIDTHBYTES((GetWidth() * GetBitCount())); }
		HDIB GetUndoHandle() { return m_hUndoImage; }
		CPalette* GetPalette() { return m_pPal; }

		/******************************************************
								멤버 변수
		******************************************************/
	protected:
		HDIB m_hImage;		//image handle
		HDIB m_hUndoImage;	//undo image handle
		CSize m_Size;		//image size
		CPalette* m_pPal;	//image palette
		CString filetype;   //image path
	};
}