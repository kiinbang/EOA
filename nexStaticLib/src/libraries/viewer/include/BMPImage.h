#pragma once

#ifdef VIEWER_EXPORT
#define VIEWER_API __declspec(dllexport)
#else
#define VIEWER_API __declspec(dllimport)
#endif

#include "BasicImage.h"

namespace image
{
	//
	//CBMPImage
	//
	class VIEWER_API CBMPImage : public CBasicImage
	{
	public:
		CBMPImage();

		CBMPImage(CBMPImage& Src);

		virtual ~CBMPImage() {}

		BOOL LoadBMP(const CString& lpszFileName);

		BOOL SaveBMP(const CString& lpszFileName);
	};
}