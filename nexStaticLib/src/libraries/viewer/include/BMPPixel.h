#pragma once

#ifdef VIEWER_EXPORT
#define VIEWER_API __declspec(dllexport)
#else
#define VIEWER_API __declspec(dllimport)
#endif

#include "BMPImage.h"

namespace image
{
	//
	//CBMPPixelPtr
	//
	class VIEWER_API CBMPPixelPtr
	{
	public:
		BYTE* operator[](LONG index) {
			ASSERT(index >= 0 && index < m_nHeight);
			return m_pPtr[index];
		}
		BYTE* operator[](int index) {
			ASSERT(index >= 0 && index < m_nHeight);
			return m_pPtr[index];
		}
		CBMPPixelPtr() { m_pPtr = NULL; }
		CBMPPixelPtr(CBMPImage& Im);
		void SetImage(CBMPImage* Im);
		virtual ~CBMPPixelPtr();

	protected:
		HDIB m_hHandle;
		BYTE** m_pPtr;
		int m_nHeight;
	};

	class CBMPColorPixelPtr
	{
	public:
		LPCOLOR operator[](LONG index) {
			ASSERT(index >= 0 && index < m_nHeight);
			return m_pPtr[index];
		}
		LPCOLOR operator[](int index) {
			ASSERT(index >= 0 && index < m_nHeight);
			return m_pPtr[index];
		}
		CBMPColorPixelPtr() { m_pPtr = NULL; }
		CBMPColorPixelPtr(CBMPImage& Im);
		void SetImage(CBMPImage* Im);
		virtual ~CBMPColorPixelPtr();

	protected:
		LPCOLOR* m_pPtr;
		HDIB m_hHandle;
		int m_nHeight;
	};
}