#pragma once

namespace image
{
#define HDIB unsigned char*
#define _LPCOLOR_ LPCOLOR
// DIB constants
#define PALVERSION   0x300
// DIB Macros
#define IS_WIN30_DIB(lpbi) ((*(LPDWORD)(lpbi)) == sizeof(BITMAPINFOHEADER))
#define RECTWIDTH(lpRect)  ((lpRect)->right - (lpRect)->left)
#define RECTHEIGHT(lpRect) ((lpRect)->bottom - (lpRect)->top)
#define WIDTHBYTES(bits)   (((bits) + 31) / 32 * 4)

	typedef int BOOL;

	typedef struct tagCOLOR
	{
		unsigned char B = 0;
		unsigned char G = 0;
		unsigned char R = 0;
	} COLOR;

	typedef struct tagCOLOR* LPCOLOR;
}