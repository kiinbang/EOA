#include "pch.h"

#include "BMPPixel.h"
#include "gloablFuncs.h"

namespace image
{
	/******************************************************
	CBMPPixelPtr
	******************************************************/

	CBMPPixelPtr::CBMPPixelPtr(CBMPImage& Im)
	{

		m_nHeight = Im.GetHeight();		// 이미지의 가로 길이
		int nWidth = Im.GetRealWidth();	// 이미지의 세로 길이

		// 이미지의 세로 길이 만큼 포인터 배열 할당
		m_pPtr = new BYTE * [m_nHeight];

		// 메모리 블록을 고정
		m_hHandle = Im.GetHandle();
		LPSTR lpDIBHdr = (LPSTR) ::GlobalLock((HGLOBAL)m_hHandle);

		// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
		BYTE* lpDIBBits = (BYTE*) FindDIBBits(lpDIBHdr);

		// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
		// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
		for (int i = m_nHeight - 1; i >= 0; i--)
		{
			m_pPtr[i] = lpDIBBits;
			lpDIBBits += nWidth;
		}
	}

	void CBMPPixelPtr::SetImage(CBMPImage* Im)
	{

		m_nHeight = Im->GetHeight();		// 이미지의 가로 길이
		int nWidth = Im->GetRealWidth();	// 이미지의 세로 길이

		// 이미지의 세로 길이 만큼 포인터 배열 할당
		m_pPtr = new BYTE * [m_nHeight];

		// 메모리 블록을 고정
		m_hHandle = Im->GetHandle();
		LPSTR lpDIBHdr = (LPSTR) GlobalLock((HGLOBAL)m_hHandle);

		// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
		BYTE* lpDIBBits = (BYTE*) FindDIBBits(lpDIBHdr);

		// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
		// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
		for (int i = m_nHeight - 1; i >= 0; i--)
		{
			m_pPtr[i] = lpDIBBits;
			lpDIBBits += nWidth;
		}
	};

	CBMPPixelPtr::~CBMPPixelPtr()
	{
		if (m_pPtr != NULL)
		{
			// 메모리 블록을 풀어줌
			GlobalUnlock((HGLOBAL)m_hHandle);

			// 할당 받았던 포인터 배열을 해제
			delete[] m_pPtr;
		}
	}

	/******************************************************
	CBMPColorPixelPtr
	******************************************************/

	CBMPColorPixelPtr::CBMPColorPixelPtr(CBMPImage& Im)
	{
		m_nHeight = Im.GetHeight();		// 이미지의 가로 길이
		int nWidth = Im.GetRealWidth();	// 이미지의 세로 길이

		// 이미지의 세로 길이 만큼 포인터 배열 할당
		m_pPtr = new LPCOLOR[m_nHeight];

		// 메모리 블록을 고정
		m_hHandle = Im.GetHandle();
		LPSTR lpDIBHdr = (LPSTR)GlobalLock((HGLOBAL)m_hHandle);

		// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
		LPSTR lpDIBBits = (LPSTR)FindDIBBits(lpDIBHdr);

		// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
		// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
		for (int i = m_nHeight - 1; i >= 0; i--)
		{
			m_pPtr[i] = (LPCOLOR)lpDIBBits;
			lpDIBBits += nWidth;
		}
	};

	void CBMPColorPixelPtr::SetImage(CBMPImage* Im)
	{
		m_nHeight = Im->GetHeight();		// 이미지의 가로 길이
		int nWidth = Im->GetRealWidth();	// 이미지의 세로 길이

		// 이미지의 세로 길이 만큼 포인터 배열 할당
		m_pPtr = new LPCOLOR[m_nHeight];

		// 메모리 블록을 고정
		m_hHandle = Im->GetHandle();
		LPSTR lpDIBHdr = (LPSTR)GlobalLock((HGLOBAL)m_hHandle);

		// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
		LPSTR lpDIBBits = (LPSTR)FindDIBBits(lpDIBHdr);

		// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
		// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
		for (int i = m_nHeight - 1; i >= 0; i--)
		{
			m_pPtr[i] = (LPCOLOR)lpDIBBits;
			lpDIBBits += nWidth;
		}
	};

	CBMPColorPixelPtr::~CBMPColorPixelPtr()
	{
		if (m_pPtr != NULL)
		{
			// 메모리 블록을 풀어줌
			GlobalUnlock((HGLOBAL)m_hHandle);
			// 할당 받았던 포인터 배열을 해제
			delete[] m_pPtr;
		}
	};
}