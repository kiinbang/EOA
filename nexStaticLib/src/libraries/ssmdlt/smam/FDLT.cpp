#include "FDLT.h"

#include <sstream>

namespace smam
{
	namespace dlt
	{
		FrameDlt::FrameDlt()
		{
			SHIFT_X = 0.;
			SHIFT_Y = 0.;
			SHIFT_Z = 0.;

			SCALE_X = 1.0;
			SCALE_Y = 1.0;
			SCALE_Z = 1.0;

			bNormalized = false;
		}

		std::string FrameDlt::DoModeling(const double gcp_sd_th,
			const unsigned int num_max_iter,
			const double maximum_sd,
			const double maximum_diff,
			const double pixelsize,
			const bool normalize,
			const std::string& outfilename)
		{
			this->GCP_sd_threshold = gcp_sd_th;
			this->bNormalized = normalize;
			unsigned int num_param = NUMDLTPARAMS;

			std::string retval;

			retval = "\n\n[Frame DLT Modeling]\n";
			retval += "row = (A1X + A2Y + A3Z + A4)/(A9X + A10Y + A11Z + 1)\n";
			retval += "col = (A5X + A6Y + A7Z + A8)/(A9X + A10Y + A11Z + 1)\n";

			if (bNormalized)
				retval += "(***Data normalization: true***)\n";
			else
				retval += "(***Data normalization: false***)\n";

			/**
			*Calculate parameter approximation
			*/
			retval += "\n\n[Init_value of Parameters]\n";
			retval += "///////////////////////////////////////////////////////////////////\n";
			///////////////////////////////
			//Original Data backup
			///////////////////////////////
			retval += DoInitModeling(num_param);
			///////////////////////////////////////
			//Initial data copy for the iteration
			///////////////////////////////////////
			sceneDataOri = sceneData;
			gcpsOri = gcps;
			gclsOri = gcls;
			///////////////////////////////////////
			retval += "///////////////////////////////////////////////////////////////////\n";

			FileOut(outfilename, retval);

			retval += Run(num_param, num_max_iter, maximum_sd, maximum_diff, outfilename, pixelsize);

			std::vector<std::vector<double>> rigorousParams;
			retval += getEop(rigorousParams);

			return retval;
		}

		void FrameDlt::SetConfigData(const std::vector<_SCENE_>& sceneData_, const std::vector<_GCP_>& gcps_)
		{
			this->sceneData = sceneData_;
			/// Set number of parameters
			for (auto& scene : sceneData)
			{
				scene.param.num_param = NUMDLTPARAMS;
				scene.param.A.resize(scene.param.num_param);
			}

			this->gcps = gcps_;
		}

		std::vector<std::vector<double>> FrameDlt::getParameters()
		{
			const unsigned int num_param = 11;
			std::vector<std::vector<double>> params(sceneData.size());

			std::string outStr("");

			for (unsigned int scene_index = 0; scene_index < sceneData.size(); scene_index++)
			{
				if (this->bNormalized)
					params[scene_index] = sceneDataOri[scene_index].param.A;
				else
					params[scene_index] = sceneData[scene_index].param.A;
			}

			return params;
		}

		void FrameDlt::DataNormalization()
		{
			/// Normalized coordinates: -1.0 ~ +1.0
			/// SCALE = 2/(max - min)
			/// SHIFT = -(max + min)/2
			/// Normalized Coord = (Original Coord + SHIFT)*SCALE
			/// Original Coord = (Normalized Coord/SCALE) - SHIFT

			double maxcol = std::numeric_limits<double>::lowest();
			double mincol = std::numeric_limits<double>::max();
			double maxrow = std::numeric_limits<double>::lowest();
			double minrow = std::numeric_limits<double>::max();

			for (const auto& scene : sceneData)
			{
				for (const auto& icp : scene.ICP_List)
				{
					if (maxcol < icp.col)
						maxcol = icp.col;
					if (mincol > icp.col)
						mincol = icp.col;

					if (maxrow < icp.row)
						maxrow = icp.row;
					if (minrow > icp.row)
						minrow = icp.row;
				}

				for (const auto& icl : scene.ICL_List)
				{
					if (maxcol < icl.col)
						maxcol = icl.col;
					if (mincol > icl.col)
						mincol = icl.col;

					if (maxrow < icl.row)
						maxrow = icl.row;
					if (minrow > icl.row)
						minrow = icl.row;
				}
			}

			double shiftCol, shiftRow;
			double scaleCol, scaleRow;

			double maximg = maxcol > maxrow ? maxcol : maxrow;
			double minimg = mincol < minrow ? mincol : minrow;

			shiftCol = shiftRow = -(maximg + minimg) / 2;
			scaleCol = scaleRow = 2. / (maximg - minimg);

			/*
			/// row and col have different scales
			shiftCol = -(maxcol + mincol) / 2;
			scaleCol = 2. / (maxcol - mincol);
			shiftRow = -(maxrow + minrow) / 2;
			scaleRow = 2. / (maxrow - minrow);
			*/

			for (auto& scene : sceneData)
			{
				scene.param.SHIFT_col = shiftCol;
				scene.param.SHIFT_row = shiftRow;

				scene.param.SCALE_col = scaleCol;
				scene.param.SCALE_row = scaleRow;
			}

			double maxX = std::numeric_limits<double>::lowest();
			double minX = std::numeric_limits<double>::max();
			double maxY = std::numeric_limits<double>::lowest();
			double minY = std::numeric_limits<double>::max();
			double maxZ = std::numeric_limits<double>::lowest();
			double minZ = std::numeric_limits<double>::max();

			for (const auto& gp : gcps)
			{
				if (maxX < gp.X)
					maxX = gp.X;
				if (minX > gp.X)
					minX = gp.X;

				if (maxY < gp.Y)
					maxY = gp.Y;
				if (minY > gp.Y)
					minY = gp.Y;

				if (maxZ < gp.Z)
					maxZ = gp.Z;
				if (minZ > gp.Z)
					minZ = gp.Z;
			}

			for (const auto& gl : gcls)
			{
				if (maxX < gl.X)
					maxX = gl.X;
				if (minX > gl.X)
					minX = gl.X;

				if (maxY < gl.Y)
					maxY = gl.Y;
				if (minY > gl.Y)
					minY = gl.Y;

				if (maxZ < gl.Z)
					maxZ = gl.Z;
				if (minZ > gl.Z)
					minZ = gl.Z;
			}

			SHIFT_X = -(maxX + minX) / 2;
			SCALE_X = 2. / (maxX - minX);

			SHIFT_Y = -(maxY + minY) / 2;
			SCALE_Y = 2. / (maxY - minY);

			SHIFT_Z = -(maxZ + minZ) / 2;
			SCALE_Z = 2. / (maxZ - minZ);

			GenNormalizedData();
		}

		void FrameDlt::GenNormalizedData()
		{
			//Normalized Coord = (Original Coord + SHIFT)*SCALE

			size_t Num_Scene = sceneData.size();
			for (unsigned int scene_index = 0; scene_index < Num_Scene; scene_index++)
			{
				_SCENE_& scene = sceneData[scene_index];
				smam::SMParam param = scene.param;

				size_t Num_ICP = scene.ICP_List.size();

				for (unsigned int icp_index = 0; icp_index < Num_ICP; icp_index++)
				{
					double d = (scene.ICP_List[icp_index].row + param.SHIFT_row) * param.SCALE_row;
					scene.ICP_List[icp_index].row = d;
					d = (scene.ICP_List[icp_index].col + param.SHIFT_col) * param.SCALE_col;
					scene.ICP_List[icp_index].col = d;
					d = scene.ICP_List[icp_index].s[0] * param.SCALE_row;
					scene.ICP_List[icp_index].s[0] = d;
					d = scene.ICP_List[icp_index].s[3] * param.SCALE_col;
					scene.ICP_List[icp_index].s[3] = d;
				}

				size_t Num_ICL = scene.ICL_List.size();
				for (unsigned int icl_index = 0; icl_index < Num_ICL; icl_index++)
				{
					scene.ICL_List[icl_index].row = (scene.ICL_List[icl_index].row + param.SHIFT_row) * param.SCALE_row;
					scene.ICL_List[icl_index].col = (scene.ICL_List[icl_index].col + param.SHIFT_col) * param.SCALE_col;
					double d;
					d = scene.ICL_List[icl_index].s[0] * param.SCALE_row;
					scene.ICL_List[icl_index].s[0] = d;
					d = scene.ICL_List[icl_index].s[3] * param.SCALE_col;
					scene.ICL_List[icl_index].s[3] = d;
				}
			}

			size_t Num_GCP = gcps.size();
			for (unsigned int gcp_index = 0; gcp_index < Num_GCP; gcp_index++)
			{
				gcps[gcp_index].X = (gcps[gcp_index].X + SHIFT_X) * SCALE_X;
				gcps[gcp_index].Y = (gcps[gcp_index].Y + SHIFT_Y) * SCALE_Y;
				gcps[gcp_index].Z = (gcps[gcp_index].Z + SHIFT_Z) * SCALE_Z;

				gcps[gcp_index].s[0] = gcps[gcp_index].s[0] * SCALE_X;
				gcps[gcp_index].s[4] = gcps[gcp_index].s[4] * SCALE_Y;
				gcps[gcp_index].s[8] = gcps[gcp_index].s[8] * SCALE_Z;
			}

			size_t Num_GCL = gcls.size();
			for (unsigned int gcl_index = 0; gcl_index < Num_GCL; gcl_index++)
			{
				gcls[gcl_index].X = (gcls[gcl_index].X + SHIFT_X) * SCALE_X;
				gcls[gcl_index].Y = (gcls[gcl_index].Y + SHIFT_Y) * SCALE_Y;
				gcls[gcl_index].Z = (gcls[gcl_index].Z + SHIFT_Z) * SCALE_Z;

				gcls[gcl_index].s[0] = gcls[gcl_index].s[0] * SCALE_X;
				gcls[gcl_index].s[4] = gcls[gcl_index].s[4] * SCALE_Y;
				gcls[gcl_index].s[8] = gcls[gcl_index].s[8] * SCALE_Z;
			}
		}

		void FrameDlt::DeNormalizedCoord(_GCP_& G)
		{
			G.X = G.X / SCALE_X - SHIFT_X;
			G.Y = G.Y / SCALE_Y - SHIFT_Y;
			G.Z = G.Z / SCALE_Z - SHIFT_Z;
		}

		void FrameDlt::DeNormalizedCoord(_ICP_& I, const SMParam& param)
		{
			I.row = I.row / param.SCALE_row - param.SHIFT_row;
			I.col = I.col / param.SCALE_col - param.SHIFT_col;
		}

		void FrameDlt::DeNormalizedCoord(_GCL_& G)
		{
			G.X = G.X / SCALE_X - SHIFT_X;
			G.Y = G.Y / SCALE_Y - SHIFT_Y;
			G.Z = G.Z / SCALE_Z - SHIFT_Z;
		}

		double FrameDlt::Func_row(_GCP_ G, unsigned int image_index)
		{
			const SMParam& param = sceneData[image_index].param;
			double row = (param.A[0] * G.X + param.A[1] * G.Y + param.A[2] * G.Z + param.A[3])
				/ (param.A[8] * G.X + param.A[9] * G.Y + param.A[10] * G.Z + 1.);
			return row;
		}

		double FrameDlt::Func_row(_GCL_ G, unsigned int image_index)
		{
			const SMParam& param = sceneData[image_index].param;
			double row = (param.A[0] * G.X + param.A[1] * G.Y + param.A[2] * G.Z + param.A[3])
				/ (param.A[8] * G.X + param.A[9] * G.Y + param.A[10] * G.Z + 1.);
			return row;
		}

		double FrameDlt::Func_col(_GCP_ G, unsigned int image_index)
		{
			const SMParam& param = sceneData[image_index].param;
			double col = (param.A[4] * G.X + param.A[5] * G.Y + param.A[6] * G.Z + param.A[7])
				/ (param.A[8] * G.X + param.A[9] * G.Y + param.A[10] * G.Z + 1.);
			return col;
		}

		double FrameDlt::Func_col(_GCL_ G, unsigned int image_index)
		{
			const SMParam& param = sceneData[image_index].param;
			double col = (param.A[4] * G.X + param.A[5] * G.Y + param.A[6] * G.Z + param.A[7])
				/ (param.A[8] * G.X + param.A[9] * G.Y + param.A[10] * G.Z + 1.);
			return col;
		}

		void FrameDlt::Cal_PDs_linear(const _GCP_& G, const _ICP_& I, std::vector<double>& ret1, std::vector<double>& ret2, const unsigned int image_index)
		{
			const SMParam& param = sceneData[image_index].param;

			//row = (A1X + A2Y + A3Z + A4) - row*(A9X + A10Y + A11Z)
			//col = (A5X + A6Y + A7Z + A8) - col*(A9X + A10Y + A11Z)

			//A1~A4
			ret1[0] = G.X;
			ret1[1] = G.Y;
			ret1[2] = G.Z;
			ret1[3] = 1.;
			//A5~A8
			ret1[4] = 0.;
			ret1[5] = 0.;
			ret1[6] = 0.;
			ret1[7] = 0.;
			//A9~A11
			ret1[8] = -I.row * G.X;
			ret1[9] = -I.row * G.Y;
			ret1[10] = -I.row * G.Z;
			//X,Y,Z(row)
			ret1[11] = param.A[0] - I.row * param.A[8];
			ret1[12] = param.A[1] - I.row * param.A[9];
			ret1[13] = param.A[2] - I.row * param.A[10];

			//A1~A4
			ret2[0] = 0.;
			ret2[1] = 0.;
			ret2[2] = 0.;
			ret2[3] = 0.;
			//A5~A8
			ret2[4] = G.X;
			ret2[5] = G.Y;
			ret2[6] = G.Z;
			ret2[7] = 1.;
			//A9~A11
			ret2[8] = -I.col * G.X;
			ret2[9] = -I.col * G.Y;
			ret2[10] = -I.col * G.Z;
			//X,Y,Z(col)
			ret2[11] = param.A[4] - I.col * param.A[8];
			ret2[12] = param.A[5] - I.col * param.A[9];
			ret2[13] = param.A[6] - I.col * param.A[10];
		}

		void FrameDlt::Cal_PDs(const _GCP_& G, std::vector<double>& ret1, std::vector<double>& ret2, const unsigned int image_index)
		{
			const SMParam& param = sceneData[image_index].param;

			//      (A1X + A2Y + A3Z + A4)
			//row = ----------------------
			//      (A9X + A10Y + A11Z + 1)
			//
			//      (A5X + A6Y + A7Z + A8)
			//col = ----------------------
			//      (A9X + A10Y + A11Z + 1)

			double row_num = param.A[0] * G.X + param.A[1] * G.Y + param.A[2] * G.Z + param.A[3];
			double den = param.A[8] * G.X + param.A[9] * G.Y + param.A[10] * G.Z + 1.0;

			//ROW
			//A1~A4
			ret1[0] = G.X / den;
			ret1[1] = G.Y / den;
			ret1[2] = G.Z / den;
			ret1[3] = 1.0 / den;
			//A5~A8
			ret1[4] = 0.;
			ret1[5] = 0.;
			ret1[6] = 0.;
			ret1[7] = 0.;
			//A9~A11
			double dd = den * den;
			double n1dd = row_num / dd;
			ret1[8] = -G.X * n1dd;
			ret1[9] = -G.Y * n1dd;
			ret1[10] = -G.Z * n1dd;
			//X,Y,Z
			ret1[11] = (param.A[0] * den - param.A[8] * row_num) / dd;
			ret1[12] = (param.A[1] * den - param.A[9] * row_num) / dd;
			ret1[13] = (param.A[2] * den - param.A[10] * row_num) / dd;

			double col_num = param.A[4] * G.X + param.A[5] * G.Y + param.A[6] * G.Z + param.A[7];
			//COL
			//A1~A4
			ret2[0] = 0.;
			ret2[1] = 0.;
			ret2[2] = 0.;
			ret2[3] = 0.;
			//A5~A8
			ret2[4] = G.X / den;
			ret2[5] = G.Y / den;
			ret2[6] = G.Z / den;
			ret2[7] = 1.0 / den;
			//A9~A11
			double n2dd = col_num / dd;
			ret2[8] = -G.X * n2dd;
			ret2[9] = -G.Y * n2dd;
			ret2[10] = -G.Z * n2dd;
			//X,Y,Z
			ret2[11] = (param.A[4] * den - param.A[8] * col_num) / dd;
			ret2[12] = (param.A[5] * den - param.A[9] * col_num) / dd;
			ret2[13] = (param.A[6] * den - param.A[10] * col_num) / dd;

		}

		void FrameDlt::Cal_PDs_Line(const _GCL_& GL1, const _GCL_& GL2, const _ICL_& IL, double* ret, unsigned int image_index, unsigned int num_param)
		{
			const SMParam& param = sceneData[image_index].param;

			_ICP_ icp1, icp2;
			_GCP_ gcp1, gcp2;

			gcp1.X = GL1.X;
			gcp1.Y = GL1.Y;
			gcp1.Z = GL1.Z;

			gcp2.X = GL2.X;
			gcp2.Y = GL2.Y;
			gcp2.Z = GL2.Z;

			icp1.col = Func_col(gcp1, image_index);
			icp1.row = Func_row(gcp1, image_index);

			icp2.col = Func_col(gcp2, image_index);
			icp2.row = Func_row(gcp2, image_index);

			std::vector<double> d_row1(14), d_col1(14), d_row2(14), d_col2(14);
			Cal_PDs(gcp1, d_row1, d_col1, image_index);
			Cal_PDs(gcp2, d_row2, d_col2, image_index);
			//
			//FL=(row - row1)(col2 - col1) - (col - col1)(row2 - row1)
			//FL=(u - u1)(v2 - v1) - (v - v1)(u2 - u1)
			//
			//A1~A4
			double v_v2 = IL.col - icp2.col;
			double v1_v = icp1.col - IL.col;
			ret[0] = v1_v * d_row2[0] + v_v2 * d_row1[0];
			ret[1] = v1_v * d_row2[1] + v_v2 * d_row1[1];
			ret[2] = v1_v * d_row2[2] + v_v2 * d_row1[2];
			ret[3] = v1_v * d_row2[3] + v_v2 * d_row1[3];

			//A5~A8
			double u2_u = icp2.row - IL.row;
			double u_u1 = IL.row - icp1.row;
			ret[4] = u_u1 * d_col2[4] + u2_u * d_col1[4];
			ret[5] = u_u1 * d_col2[5] + u2_u * d_col1[5];
			ret[6] = u_u1 * d_col2[6] + u2_u * d_col1[6];
			ret[7] = u_u1 * d_col2[7] + u2_u * d_col1[7];

			//A9~A11
			double v2_v1 = icp2.col - icp1.col;
			double u_u2 = IL.row - icp2.row;
			double u2_u1 = icp2.row - icp1.row;
			ret[8] = u_u2 * (d_col2[8] - d_col1[8]) - v2_v1 * (d_row2[8]) + u2_u1 * (d_col2[8]) - v_v2 * (d_row2[8] - d_row1[8]);
			ret[9] = u_u2 * (d_col2[9] - d_col1[9]) - v2_v1 * (d_row2[9]) + u2_u1 * (d_col2[9]) - v_v2 * (d_row2[9] - d_row1[9]);
			ret[10] = u_u2 * (d_col2[10] - d_col1[10]) - v2_v1 * (d_row2[10]) + u2_u1 * (d_col2[10]) - v_v2 * (d_row2[10] - d_row1[10]);

			//X1,Y1,Z1
			double v2_v = icp2.col - IL.col;
			ret[11] = u2_u * d_col1[11] - v2_v * d_row1[11];
			ret[12] = u2_u * d_col1[12] - v2_v * d_row1[12];
			ret[13] = u2_u * d_col1[13] - v2_v * d_row1[13];
			//X2,Y2,Z2
			ret[14] = u_u1 * d_col2[11] + v1_v * d_row2[11];
			ret[15] = u_u1 * d_col2[12] + v1_v * d_row2[12];
			ret[16] = u_u1 * d_col2[13] + v1_v * d_row2[13];
		}

		void FrameDlt::PointOBSInit(const _SCENE_& scene, const unsigned int num_param, math::Matrix<double>& A, math::Matrix<double>& L, math::Matrix<double>& W, const unsigned int scene_index)
		{
			size_t GCP_Num = gcps.size();
			if (GCP_Num == 0) return;

			size_t ICP_Num = scene.ICP_List.size();

			if (ICP_Num == 0) return;

			for (size_t i = 0; i < GCP_Num; i++)
			{
				Matrix<double> l(2, 1, 0), a1(2, num_param, 0), w(2, 2, 0);
				_GCP_ GP = gcps[i];

				if (GP.s[0] > GCP_sd_threshold) continue;
				for (size_t j = 0; j < ICP_Num; j++)
				{
					_ICP_ IP = scene.ICP_List[j];
					if (IP.PID != GP.PID) continue;

					std::vector<double> ret1(num_param + 3);
					std::vector<double> ret2(num_param + 3);
					Cal_PDs_linear(GP, IP, ret1, ret2, scene_index);

					//row = (A1X + A2Y + A3Z + A4) - row*(A9X + A10Y + A11Z)
					//col = (A5X + A6Y + A7Z + A8) - col*(A9X + A10Y + A11Z)

					l(0, 0) = IP.row;
					l(1, 0) = IP.col;
					//u(row)
					//A1~A4
					for (unsigned int k = 0; k < num_param; k++)
					{
						//row
						a1(0, k) = ret1[k];
						//col
						a1(1, k) = ret2[k];
					}

					//weight
					w(0, 0) = 1. / IP.s[0] / IP.s[0]; //s(1,1)
					w(1, 1) = 1. / IP.s[3] / IP.s[3]; //s(2,2)

					unsigned int pos = A.getRows();
					A.insert(pos, scene_index * num_param, a1);
					L.insert(pos, 0, l);
					W.insert(pos, pos, w);
				}
			}
		}

		std::string FrameDlt::DoInitModeling(unsigned int num_param)
		{
			std::string retval;

			retval = "[Frame DLT Initial Parameters]\n";


			/*********************************
			* Data Normalization
			*********************************/

			if (bNormalized)
			{
				/// Data normalization
				retval += "(***Data is normalized***)\n";
				DataNormalization();
			}

			unsigned int SCENE_Num = (unsigned int)sceneData.size();

			std::string stParam;
			std::vector<_GCP_>& gcp = gcps;
			unsigned int GCP_Num = (unsigned int)gcp.size();

			math::Matrix<double> Amat, Lmat, Wmat, Xmat;
			Amat.resize(0, 0, 0.);
			Lmat.resize(0, 0, 0.);
			Wmat.resize(0, 0, 0.);

			for (unsigned int k = 0; k < SCENE_Num; k++)
			{
				_SCENE_ scene = sceneData[k];
				PointOBSInit(scene, num_param, Amat, Lmat, Wmat, k);
				CLeastSquare LS;
				Xmat = LS.RunLeastSquare_Fast(Amat, Lmat, Wmat);
			}

			for (unsigned int k = 0; k < SCENE_Num; k++)
			{
				for (unsigned int i = 0; i < num_param; i++)
				{
					sceneData[k].param.A[i] = Xmat(k * num_param + i, 0);
					stParam = "[A" + std::to_string(i) + "] " + std::to_string(Xmat(k * num_param + i, 0)) + "\t";
					retval += stParam;
				}
				stParam = "\n";
				retval += stParam;
			}

			return retval;
		}

		void FrameDlt::FileOut(const std::string& fname, const std::string& st)
		{
			FILE* outfile;
			outfile = NULL;
			outfile = fopen(fname.c_str(), "a");
			if (outfile == NULL)
				outfile = fopen(fname.c_str(), "w");
			fprintf(outfile, st.c_str());
			fclose(outfile);
		}

		void FrameDlt::GenerateFileName(const std::string& name, std::string& param_list, std::string& GCP_list, std::string& GCL_List)
		{
			param_list = name + "_params.csv";
			GCP_list = name + "_GCP.csv";
			GCL_List = name + "_GCL.csv";
		};

		void FrameDlt::FillMatrix_Point_i(const _GCP_& GP, const _ICP_& IP, unsigned image_index, Matrix<double>& a1, Matrix<double>& a2, Matrix<double>& l, Matrix<double>& w, const unsigned int num_param)
		{
			//**************************************
			//A point Data
			//**************************************
			l.resize(2, 1, 0.);
			a1.resize(2, num_param, 0.);
			a2.resize(2, 3, 0.);
			w.resize(2, 2, 0.);

			SMParam param = sceneData[image_index].param;

			/************************
			* Elements of L matrix.
			* (row - Fo) + v
			* (col - Go) + v
			************************/

			l(0, 0) = IP.row - Func_row(GP, image_index);
			l(1, 0) = IP.col - Func_col(GP, image_index);

			/************************
			* Elements of A matrix.
			* Jacobian  matrix
			************************/
			std::vector<double> PDs1(num_param + 3);
			std::vector<double> PDs2(num_param + 3);
			Cal_PDs(GP, PDs1, PDs2, image_index);

			for (unsigned int i = 0; i < num_param; i++)
			{
				a1(0, i) = PDs1[i];
				a1(1, i) = PDs2[i];
			}

			for (unsigned int i = 0; i < 3; i++)
			{
				a2(0, i) = PDs1[num_param + i];
				a2(1, i) = PDs2[num_param + i];
			}

			//weight
			w(0, 0) = 1. / IP.s[0] / IP.s[0]; //s(1,1)
			w(1, 1) = 1. / IP.s[3] / IP.s[3]; //s(2,2)
		}

		void FrameDlt::FillMatrix_Point(CLeastSquare& LS, unsigned int num_param)
		{
			unsigned int SCENE_Num = (unsigned int)sceneData.size();
			unsigned int GCP_Num = (unsigned int)gcps.size();

			for (unsigned int gcp_index = 0; gcp_index < GCP_Num; gcp_index++)
			{
				_GCP_ GP = gcps[gcp_index];

				for (unsigned int scene_index = 0; scene_index < SCENE_Num; scene_index++)
				{
					for (unsigned int point_index = 0;
						point_index < sceneData[scene_index].ICP_List.size();
						point_index++)
					{
						_ICP_ IP = sceneData[scene_index].ICP_List[point_index];
						if (GP.PID == IP.PID)
						{
							Matrix<double> a1, a2, l, w;
							l.resize(2, 1, 0.);
							a1.resize(2, num_param, 0.);
							a2.resize(2, 3, 0.);
							w.resize(2, 2, 0.);

							FillMatrix_Point_i(GP, IP, scene_index, a1, a2, l, w, num_param);

							LS.Fill_Nmat_Data_Point(a1, a2, w, l, point_index, scene_index);
						}
					}
				}
			}
		}

		double FrameDlt::Func_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, unsigned int image_index)
		{
			_ICP_ icp1, icp2;
			_GCP_ gcp1, gcp2;

			gcp1.X = GL1.X;
			gcp1.Y = GL1.Y;
			gcp1.Z = GL1.Z;

			gcp2.X = GL2.X;
			gcp2.Y = GL2.Y;
			gcp2.Z = GL2.Z;

			icp1.col = Func_col(gcp1, image_index);
			icp1.row = Func_row(gcp1, image_index);

			icp2.col = Func_col(gcp2, image_index);
			icp2.row = Func_row(gcp2, image_index);

			//Line constraint
			double H = ((icp2.col - icp1.col) * (IL.row - icp2.row)) - (icp2.row - icp1.row) * (IL.col - icp2.col);

			return H;
		}

		void FrameDlt::FillMatrix_Line_i(const _ICL_& IL, const _GCL_& GLA, const _GCL_& GLB, const unsigned scene_index, Matrix<double>& a1, Matrix<double>& a2, Matrix<double>& a3, Matrix<double>& l, Matrix<double>& w, const unsigned int num_param)
		{
			//*************************
			//A Line Data
			//*************************
			l.resize(1, 1, 0.);
			a1.resize(1, num_param, 0.);
			a2.resize(1, 3, 0.);
			a3.resize(1, 3, 0.);
			w.resize(1, 1, 0.);

			const SMParam& param = sceneData[scene_index].param;

			double* PDs = new double[num_param + 6];
			Cal_PDs_Line(GLA, GLB, IL, PDs, scene_index, num_param);

			l(0, 0) = -Func_Line(GLA, GLB, IL, scene_index);

			for (unsigned int i = 0; i < num_param; i++)
			{
				a1(0, i) = PDs[i];
			}
			for (unsigned int i = 0; i < 3; i++)
			{
				a2(0, i) = PDs[num_param + i];
			}
			for (unsigned int i = 0; i < 3; i++)
			{
				a3(0, i) = PDs[num_param + 3 + i];
			}

			/**
			*Equivalent weight
			*/
			Matrix<double> B(1, 2, 0.), Q(2, 2, 0.);

			double v_1 = Func_col(GLA, scene_index);
			double u_1 = Func_row(GLA, scene_index);

			double v_2 = Func_col(GLB, scene_index);
			double u_2 = Func_row(GLB, scene_index);

			Q(0, 0) = IL.s[0] * IL.s[0];
			Q(1, 1) = IL.s[3] * IL.s[3];

			B(0, 0) = v_2 - v_1;
			B(0, 1) = u_1 - u_2;

			w = B % Q % (B.transpose());
			w(0, 0) = 1. / w(0, 0);
		}

		void FrameDlt::FillMatrix_Line(CLeastSquare& LS, unsigned int num_param)
		{
			unsigned int SCENE_Num = (unsigned int)sceneData.size();
			unsigned int GCL_Num = (unsigned int)gcls.size();

			for (unsigned int gcl_index = 0; gcl_index < GCL_Num; gcl_index++)
			{
				_GCL_ GLA = gcls[gcl_index];
				if (GLA.flag == "A")
				{
					for (unsigned int gcl_index2 = 0; gcl_index2 < GCL_Num; gcl_index2++)
					{
						_GCL_ GLB = gcls[gcl_index2];
						if ((GLA.LID == GLB.LID) && (GLB.flag == "B"))
						{
							for (unsigned int scene_index = 0; scene_index < SCENE_Num; scene_index++)
							{
								for (unsigned int icl_index = 0;
									icl_index < sceneData[scene_index].ICL_List.size();
									icl_index++)
								{
									unsigned int line_index;
									_ICL_ IL = sceneData[scene_index].ICL_List[icl_index];
									if (GLA.LID == IL.LID)
									{
										Matrix<double> a1, a2, a3, l, w;

										if (IL.flag != "C")
										{
											_GCP_ GP;
											unsigned int index_ab;

											if (IL.flag == "A")
											{
												GP = copyGCL(GLA);
												index_ab = gcl_index;
											}
											else if (IL.flag == "B")
											{
												GP = copyGCL(GLB);
												index_ab = gcl_index2;
											}
											else { break; }

											_ICP_ IP;
											IP.col = IL.col;
											IP.row = IL.row;

											l.resize(2, 1, 0.);
											a1.resize(2, num_param, 0.);
											a2.resize(2, 3, 0.);
											w.resize(2, 2, 0.);

											FillMatrix_Point_i(GP, IP, scene_index, a1, a2, l, w, num_param);

											Matrix<double> a2_line;
											a2_line.resize(1, 6, 0.);

											if (IL.flag == "A")
											{
												a2_line.insert(0, 0, a2);
												line_index = index_ab / 2;
											}
											else if (IL.flag == "B")
											{
												a2_line.insert(0, 3, a2);
												line_index = (index_ab - 1) / 2;
											}

											LS.Fill_Nmat_Data_PointofLine(a1, a2_line, w, l, line_index, scene_index);
										}
										else
										{
											l.resize(1, 1, 0.);
											a1.resize(1, num_param, 0.);
											a2.resize(1, 3, 0.);
											a3.resize(1, 3, 0.);
											w.resize(1, 1, 0.);

											FillMatrix_Line_i(IL, GLA, GLB, scene_index, a1, a2, a3, l, w, num_param);

											a2.addCols(a3);
											LS.Fill_Nmat_Data_Line(a1, a2, w, l, line_index, scene_index);
										}
									}
								}
							}
						}
					}
				}
			}
		}

		void FrameDlt::FillMatrix_XYZ_i(CLeastSquare& LS, _GCP_ GP_init, _GCP_ GP_ori, unsigned scene_index, Matrix<double>& a2, Matrix<double>& l, Matrix<double>& w)
		{
			//**************************************
			//A point Data
			//**************************************
			//00:obs
			//0:init
			//X0+dX=X00+V
			//dx=(X00-X0)+V
			a2.resize(3, 3, 0.);
			l.resize(3, 1, 0.);
			w.resize(3, 3, 0.);

			const SMParam& param = sceneData[scene_index].param;

			a2.makeIdentityMat(1.);

			l(0, 0) = GP_ori.X - GP_init.X;
			l(1, 0) = GP_ori.Y - GP_init.Y;
			l(2, 0) = GP_ori.Z - GP_init.Z;

			w(0, 0) = 1 / GP_init.s[0] / GP_init.s[0];
			w(1, 1) = 1 / GP_init.s[4] / GP_init.s[4];
			w(2, 2) = 1 / GP_init.s[8] / GP_init.s[8];
		}

		void FrameDlt::FillMatrix_XYZ(CLeastSquare& LS)
		{
			unsigned int GCP_Num = (unsigned int)gcps.size();
			unsigned int GCL_Num = (unsigned int)gcls.size();
			unsigned int SCENE_Num = (unsigned int)sceneData.size();

			for (unsigned int gcp_index = 0; gcp_index < GCP_Num; gcp_index++)
			{
				const _GCP_& GP_init = gcps[gcp_index];
				const _GCP_& GP_ori = gcpsOri[gcp_index];
				Matrix<double> a2, l, w;
				l.resize(3, 1, 0.);
				a2.resize(3, 3, 0.);
				w.resize(3, 3, 0.);

				if ((GP_init.s[0] < 10) && (GP_init.s[4] < 10) && (GP_init.s[8] < 10))
				{
					FillMatrix_XYZ_i(LS, GP_init, GP_ori, 0, a2, l, w);
					LS.Fill_Nmat_Data_XYZ(a2, w, l, gcp_index);
				}

			}

			for (unsigned int gcl_index = 0; gcl_index < GCL_Num; gcl_index++)
			{
				_GCL_ GL_init = gcls[gcl_index];
				_GCL_ GL_ori = gclsOri[gcl_index];
				_GCP_ GP_init, GP_ori;
				GP_init = copyGCL(GL_init);
				GP_ori= copyGCL(GL_ori);

				Matrix<double> a2, l, w;
				l.resize(3, 1, 0.);
				a2.resize(3, 3, 0.);
				w.resize(3, 3, 0.);

				if ((GP_init.s[0] < 1) && (GP_init.s[4] < 1) && (GP_init.s[8] < 1))
				{

					FillMatrix_XYZ_i(LS, GP_init, GP_ori, 0, a2, l, w);

					unsigned int line_index;
					Matrix<double> a2_line;
					a2_line.resize(1, 6, 0.);

					if (GL_init.flag == "A")
					{
						a2_line.insert(0, 0, a2);
						line_index = gcl_index / 2;
					}
					else if (GL_init.flag == "B")
					{
						a2_line.insert(0, 3, a2);
						line_index = (gcl_index - 1) / 2;
					}

					LS.Fill_Nmat_Data_XYZofLine(a2_line, w, l, line_index);
				}
			}
		}

		CLeastSquare FrameDlt::Iteration(Matrix<double>& Xmat, const unsigned int num_param, double& var)
		{
			unsigned int n_image, n_point, n_line;
			n_image = static_cast<unsigned int>(sceneData.size());
			n_point = static_cast<unsigned int>(gcps.size());
			n_line = static_cast<unsigned int>(gcls.size());

			CLeastSquare LS;

			LS.SetDataConfig(n_image, num_param, n_point, n_line / 2);

			FillMatrix_Point(LS, num_param);
			FillMatrix_Line(LS, num_param);
			FillMatrix_XYZ(LS);

			Matrix<double> Nmat;
			std::string temp;
			Xmat = LS.RunLeastSquare_RN(var);

			return LS;
		}

		std::string FrameDlt::UpdateUnknownParam(Matrix<double>& Xmat, unsigned int num_param)
		{
			std::string temp;
			std::string ret = "";

			for (unsigned int param_index = 0; param_index < num_param; param_index++)
			{
				temp = "[" + std::to_string(param_index) + "], ";
				ret += temp;
			}
			temp = "\n";
			ret += temp;

			for (unsigned int scene_index = 0; scene_index < sceneData.size(); scene_index++)
			{
				temp = "[Parameters List(" + std::to_string(scene_index) + "): " + sceneData[scene_index].SID + "]\n";
				ret += temp;
				for (unsigned int param_index = 0; param_index < num_param; param_index++)
				{
					sceneData[scene_index].param.A[param_index] += Xmat(scene_index * num_param + param_index);
					temp = std::to_string(sceneData[scene_index].param.A[param_index]) + ", ";
					ret += temp;
				}
				temp = "\n";
				ret += temp;
			}
			return ret;
		}

		std::string FrameDlt::UpdateUnknownPoint(const Matrix<double>& Xmat, const unsigned int num_param)
		{
			std::string outStream;
			const unsigned int numScenes = (unsigned int)(sceneData.size());

			if (gcps.size() > 0)
			{ 
				outStream = std::string("[GCP List]");
				outStream += "\n";
			}
			
			for (unsigned int gcp_index = 0; gcp_index < gcps.size(); gcp_index++)
			{
				auto& gcp = gcps[gcp_index];
				gcp.X += Xmat(numScenes * num_param + gcp_index * 3 + 0);
				gcp.Y += Xmat(numScenes * num_param + gcp_index * 3 + 1);
				gcp.Z += Xmat(numScenes * num_param + gcp_index * 3 + 2);

				outStream += to_string(gcp);
			}

			return outStream;
		}

		std::string FrameDlt::UpdateUnknownLine(Matrix<double>& Xmat, unsigned int num_param)
		{
			std::string ret = "";
			std::string temp;
			const unsigned int numScenes = (unsigned int)(sceneData.size());
			const unsigned int numGcls = (unsigned int)(gcls.size());
			if (gcls.size() > 0) 
			{ 
				temp = "[GCL List]\n"; 
				ret += temp; 
			}

			for (unsigned int gcl_index = 0; gcl_index < gcls.size(); gcl_index++)
			{
				auto& gcl = gcls[gcl_index];
				gcl.X += Xmat(numScenes * num_param + numGcls * 3 + gcl_index * 3 + 0);
				gcl.Y += Xmat(numScenes * num_param + numGcls * 3 + gcl_index * 3 + 1);
				gcl.Z += Xmat(numScenes * num_param + numGcls * 3 + gcl_index * 3 + 2);

				temp = smam::to_string(gcl);
				ret += temp;
			}
			return ret;
		}

		SMParam FrameDlt::ParamDenormalization(SMParam& param, const unsigned int num_param)
		{
			//double* Nparam = SMData.DataNoramlization();
			//SHIFT_X[0], SCALE_X[1], 
			//SHIFT_Y[2], SCALE_Y[3], 
			//SHIFT_Z[4], SCALE_Z[5], 
			//SHIFT_col[6], SCALE_col[7], 
			//SHIFT_row[8], SCALE_row[9]

			double* A = new double[num_param];
			for (unsigned int i = 0; i < num_param; i++)
				A[i] = param.A[i];

			double L = A[8] * SHIFT_X * SCALE_X
				+ A[9] * SHIFT_Y * SCALE_Y
				+ A[10] * SHIFT_Z * SCALE_Z
				+ 1;

			double M1 = A[0] * SHIFT_X * SCALE_X / param.SCALE_row
				+ A[1] * SHIFT_Y * SCALE_Y / param.SCALE_row
				+ A[2] * SHIFT_Z * SCALE_Z / param.SCALE_row
				+ A[3] / param.SCALE_row;

			double M2 = A[4] * SHIFT_X * SCALE_X / param.SCALE_col
				+ A[5] * SHIFT_Y * SCALE_Y / param.SCALE_col
				+ A[6] * SHIFT_Z * SCALE_Z / param.SCALE_col
				+ A[7] / param.SCALE_col;

			param.A[0] = A[0] * SCALE_X / param.SCALE_row / L - A[8] * SCALE_X * param.SHIFT_row / L;
			param.A[1] = A[1] * SCALE_Y / param.SCALE_row / L - A[9] * SCALE_Y * param.SHIFT_row / L;
			param.A[2] = A[2] * SCALE_Z / param.SCALE_row / L - A[10] * SCALE_Z * param.SHIFT_row / L;
			param.A[3] = M1 / L - param.SHIFT_row;

			param.A[4] = A[4] * SCALE_X / param.SCALE_col / L - A[8] * SCALE_X * param.SHIFT_col / L;
			param.A[5] = A[5] * SCALE_Y / param.SCALE_col / L - A[9] * SCALE_Y * param.SHIFT_col / L;
			param.A[6] = A[6] * SCALE_Z / param.SCALE_col / L - A[10] * SCALE_Z * param.SHIFT_col / L;
			param.A[7] = M2 / L - param.SHIFT_col;

			param.A[8] = A[8] * SCALE_X / L;
			param.A[9] = A[9] * SCALE_Y / L;
			param.A[10] = A[10] * SCALE_Z / L;

			SHIFT_X = 0.;
			SCALE_X = 1.;
			SHIFT_Y = 0.;
			SCALE_Y = 1.;
			SHIFT_Z = 0.;
			SCALE_Z = 1.;
			param.SHIFT_col = 0.;
			param.SCALE_col = 1.;
			param.SHIFT_row = 0.;
			param.SCALE_row = 1.;

			delete[] A;

			return param;
		}

		std::string FrameDlt::getEop(std::vector <std::vector<double>>& rigorousParams)
		{
			const unsigned int num_param = 11;
			rigorousParams.resize(sceneData.size());

			std::string outStr("");

			for (unsigned int scene_index = 0; scene_index < sceneData.size(); scene_index++)
			{
				Matrix<double> X(num_param, 1);

				for (unsigned int param_index = 0; param_index < num_param; param_index++)
				{
					if (this->bNormalized)
						X(param_index) = sceneDataOri[scene_index].param.A[param_index];
					else
						X(param_index) = sceneData[scene_index].param.A[param_index];
				}

				/// Retrieve eop from dlt parameters
				/// https://doi.org/10.3846/gac.2018.1629
				Matrix<double> LA(3, 3), LC(3, 1);
				LA(0, 0) = X(0);	LA(0, 1) = X(1);	LA(0, 2) = X(2);
				LA(1, 0) = X(4);	LA(1, 1) = X(5);	LA(1, 2) = X(6);
				LA(2, 0) = X(8);	LA(2, 1) = X(9);	LA(2, 2) = X(10);

				LC(0) = -X(3);
				LC(1) = -X(7);
				LC(2) = -1.0;

				auto pos = LA.inverse() % LC;

				double temp = X(8) * X(8) + X(9) * X(9) + X(10) * X(10);
				double xp = (X(0) * X(8) + X(1) * X(9) + X(2) * X(10)) / temp;
				double yp = (X(4) * X(8) + X(5) * X(9) + X(6) * X(10)) / temp;
				double omg = atan(-X(9) / X(10));
				double phi = asin(-X(8) / sqrt(temp));
				double xpX8X0 = xp * X(8) - X(0);
				double xpX9X1 = xp * X(9) - X(1);
				double xpX10X2 = xp * X(10) - X(2);
				double temp2 = cos(phi) * sqrt(xpX8X0 * xpX8X0 + xpX9X1 * xpX9X1 + xpX10X2 * xpX10X2);
				double kap = acos(xpX8X0 / temp2);
				double f = xpX8X0 / (cos(kap) * cos(phi) * sqrt(temp));

				std::string msg;
				const double rad2deg = 180.0 / 3.14159265358979;
				msg = "Position: " 
					+ std::to_string(pos(0)) + "\t" 
					+ std::to_string(pos(1)) + "\t" 
					+ std::to_string(pos(2)) + "\n";
				outStr += msg;
				msg = "Orientation(opk) : " 
					+ std::to_string(omg * rad2deg) + "\t" 
					+ std::to_string(phi * rad2deg) + "\t" 
					+ std::to_string(kap * rad2deg) + "\n";
				outStr += msg;
				msg = "xp, py, f: "
					+ std::to_string(xp) + "\t"
					+ std::to_string(yp) + "\t"
					+ std::to_string(f) + "\n";
				outStr += msg;

				rigorousParams[scene_index].push_back(pos(0));
				rigorousParams[scene_index].push_back(pos(1));
				rigorousParams[scene_index].push_back(pos(2));
				rigorousParams[scene_index].push_back(omg);
				rigorousParams[scene_index].push_back(phi);
				rigorousParams[scene_index].push_back(kap);
				rigorousParams[scene_index].push_back(xp);
				rigorousParams[scene_index].push_back(yp);
				rigorousParams[scene_index].push_back(f);
			}

			return outStr;
		}

		std::string FrameDlt::Run(unsigned int num_param, unsigned int num_max_iter, double maximum_sd, double maximum_diff, std::string outfilename, double pixelsize)
		{
			std::string param_out, gcp_out, gcl_out;
			GenerateFileName(outfilename, param_out, gcp_out, gcl_out);

			std::string eachiterout = "";
			std::string retval;

			/**
			*Nonlinear Least square
			*AX = L + V
			*A: Jacobian matrix
			*/

			/*******************************************************************
			*[Modified Parallel Model]
			*x = A1X + A2Y + A3Z + A4
			*y = A5X + A6Y + A7Z + A8/(1+(alpha)(A5X + A6Y + A7Z + A8))
			********************************************************************/

			std::string temp;

			double before_var, current_var;
			double diff_var;
			bool var_increase = false;
			CLeastSquare LS;
			/*****************************************************************************************/
			//Iteration termination[criteria]
			/*****************************************************************************************/
			// 1) maximum iterations check
			// 2) maximum correction(elements of X matrix) check
			// 3) reference variance(or SD) increasing check : diverging
			// 4) checking the change in the reference variance between iterations (less than 1%)
			/*****************************************************************************************/
			for (unsigned int num_iter = 0; num_iter < num_max_iter; num_iter++)
			{
				eachiterout = "";
				Matrix<double> Xmat;
				temp = "\n****************************************";
				eachiterout += temp;
				temp = "\n[Iteration(" + std::to_string(num_iter + 1) + ")]\n";
				eachiterout += temp;
				temp = "****************************************\n";
				eachiterout += temp;

				/***********************************/
				double var;
				LS = Iteration(Xmat, num_param, var);
				double sd = sqrt(var);
				sd = sd * pixelsize; //mm
				/***********************************/

				/**************************************************************************************************************/
				std::string Paramst;
				Paramst = "\n[Iteration(" + std::to_string(num_iter) + ")]\n";
				Paramst += UpdateUnknownParam(Xmat, num_param);
				FileOut(param_out, Paramst);

				if (gcps.size() != 0)
				{
					std::string pointst;
					pointst = "\n[Iteration(" + std::to_string(num_iter) + ")]\n";
					pointst += UpdateUnknownPoint(Xmat, num_param);
					FileOut(gcp_out, pointst);
				}

				if (gcls.size() != 0)
				{
					std::string linest;
					linest = "\n[Iteration(" + std::to_string(num_iter) + ")]\n";
					linest += UpdateUnknownLine(Xmat, num_param);
					FileOut(gcl_out, linest);
				}

				/**************************************************************************************************************/

				double maxX;
				maxX = Xmat.getMaxabsElement();
				temp = "\n[Max Xmat element] " + std::to_string(maxX) + "\n";
				eachiterout += temp;

				temp = "\n[SD] " + std::to_string(sd) + "\n";
				eachiterout += temp;

				if (num_iter == 0)
				{
					current_var = var;
				}
				else
				{
					before_var = current_var;
					current_var = var;
					diff_var = current_var - before_var;
					//*
					//if(fabs(diff_var)<(0.001*current_var))
					if (fabs(diff_var) < fabs(maximum_sd))
					{
						temp = "\n[Iteration STOP: The change of the reference variance is less than threshold ( " 
							+ std::to_string(fabs(maximum_sd)) + " ) ]\n";
						eachiterout += temp;

						FileOut(outfilename, eachiterout);
						retval += eachiterout;
						break;
					}
					else
					{
						temp = "\nThe change of the reference variance is: " + std::to_string(diff_var) + "\n";
						eachiterout += temp;						
					}

					//*/

					if (diff_var > 0.)
					{
						if (var_increase == true)
						{
							temp = "\n[Iteration STOP: The reference variance is increasing.]\n";
							eachiterout += temp;
							//FileOut(outfilename, eachiterout);
							//retval += eachiterout;
							//break;
						}
						else
						{
							var_increase = true;
						}
					}
					else
					{
						var_increase = false;
						if (sd < maximum_sd)
						{
							temp = "\n[Iteration STOP: The SD is less than threshold ( " + std::to_string(maximum_sd) + " ) ]\n";
							eachiterout += temp;

							FileOut(outfilename, eachiterout);
							retval += eachiterout;
							break;
						}
					}
				}

				if (maximum_diff > maxX / (SCALE_X + SCALE_Y + SCALE_Z) * 3)
				{
					temp = "\nIteration STOP: The maximum correction( " 
						+ std::to_string(maxX / (SCALE_X + SCALE_Y + SCALE_Z) * 3) 
						+ " ) is less than threshold ( " 
						+ std::to_string(maximum_diff) + " ) ]\n";
					eachiterout += temp;

					FileOut(outfilename, eachiterout);
					retval += eachiterout;
					break;
				}

				FileOut(outfilename, eachiterout);
				retval += eachiterout;

			}//End of iteration

			/*****************************************************************************************/
			//Modeling Result[Points]
			/*****************************************************************************************/
			std::string deNormalCoord;
			std::string deNormalCoord_editbox;
			deNormalCoord_editbox = deNormalCoord = "\n[Modeling Result]\n";
			if (gcps.size() > 0)
			{
				temp = "\n[GCP List]\n";
				deNormalCoord += temp;
				temp = "\n[GCP List]\n";
				deNormalCoord_editbox += temp;

				for (unsigned int gcp_index = 0; gcp_index < gcps.size(); gcp_index++)
				{
					if (bNormalized)
					{
						DeNormalizedCoord(gcps[gcp_index]);
					}
					temp = smam::to_string(gcps[gcp_index]);					
					deNormalCoord += temp;
					deNormalCoord_editbox += temp;
				}
				/////////////////////////////////////////////
				FileOut(gcp_out, deNormalCoord);
				FileOut(outfilename, deNormalCoord_editbox);
				retval += deNormalCoord_editbox;
				/////////////////////////////////////////////
			}

			/*****************************************************************************************/
			//Modeling Result[Lines]
			/*****************************************************************************************/
			deNormalCoord_editbox = deNormalCoord = "\n[Modeling Result]\n";
			if (gcls.size() > 0)
			{
				temp = "\n[GCL List]\n";
				deNormalCoord += temp;
				deNormalCoord_editbox += temp;

				for (unsigned int gcl_index = 0; gcl_index < gcls.size(); gcl_index++)
				{
					if (bNormalized)
					{
						DeNormalizedCoord(gcls[gcl_index]);
					}
					temp = smam::to_string(gcls[gcl_index]);
					deNormalCoord += temp;
					deNormalCoord_editbox += temp;
				}
				//////////////////////////////////////////////
				FileOut(gcl_out, deNormalCoord);
				FileOut(outfilename, deNormalCoord_editbox);
				retval += deNormalCoord_editbox;
				//////////////////////////////////////////////
			}

			/*****************************************************************************************/
			//Modeling Result[Parameters]
			/*****************************************************************************************/
			std::string deNormalParam = "";
			std::string stParam;

			stParam = "\n------[Result: " + std::to_string(num_param) + " Parameters]------\n";
			deNormalParam += stParam;

			for (unsigned int param_index = 0; param_index < num_param; param_index++)
			{
				stParam = "[" + std::to_string(param_index) + "],\t";
				deNormalParam += stParam;
			}
			stParam = "\n";
			deNormalParam += stParam;

			sceneDataOri = sceneData;

			for (unsigned int n_scene = 0; n_scene < sceneData.size(); n_scene++)
			{
				/****************************
				*Parameters de-normalizing.
				****************************/
				if (bNormalized)
				{
					/// Parameters denormalization
					sceneDataOri[n_scene].param = ParamDenormalization(sceneData[n_scene].param, num_param);
				}
				else
				{
					sceneDataOri[n_scene] = sceneData[n_scene];
				}

				stParam = "[Parameters List(" + n_scene, sceneData[n_scene].SID + "): %s]\n";
				deNormalParam += stParam;
				for (unsigned int param_index = 0; param_index < num_param; param_index++)
				{
					stParam = std::to_string(sceneData[n_scene].param.A[param_index]) + ",\t";
					deNormalParam += stParam;
				}
				stParam = "\n";
				deNormalParam += stParam;
			}
			//////////////////////////////////////////////
			FileOut(param_out, deNormalParam);
			FileOut(outfilename, deNormalParam);
			retval += deNormalParam;

			return retval;
		}
	}
}