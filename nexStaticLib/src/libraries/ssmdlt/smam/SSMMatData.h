/*
* Copyright(c) 2001-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

namespace math
{
	template<typename S> class Matrix;
	template<typename T, size_t r, size_t c> class FixedSizeMat;
	template<typename T, size_t d> class VectorMat;

	/**
	*@class Matrix Data Class
	*@brief data for Matrix
	*/
	template<typename S>
	class MatData
	{
		//template<typename S>
		//friend class Matrix;

	public:
		MatData()
		{
			this->size = 0;
			resize(1, 1);
		}

		MatData(const unsigned int r, const unsigned int c)
		{
			this->size = 0;
			resize(r, c);
		}

		MatData(const unsigned int r, const unsigned int c, S val)
		{
			this->size = 0;
			allocateMem(r, c);
			initialize(val);
		}

		MatData(const MatData<S>& copy)
		{
			this->size = 0;
			allocateMem(copy.getRows(), copy.getCols());

			for (unsigned int i = 0; i < size; i++)
			{
				data[i] = copy.data[i];
			}
		}

		virtual ~MatData() { del(); }

		template<typename T>
		void operator = (const MatData<T>& copy)
		{
			del();

			allocateMem(copy.getRows(), copy.getCols());

			for (unsigned int i = 0; i < size; i++)
			{
				data[i] = static_cast<S>(copy.getAccessData()[i]);
			}
		}

		void del()
		{
			try
			{
				if (size >= 1 && data != nullptr)
					delete[] data;
			}
			catch (...)
			{
				throw std::runtime_error("Error in deleting memory in MatData");
			}
			
			rows = 0;
			cols = 0;
			size = 0;
			data = nullptr;
		}

		/**get the number of rows*/
		unsigned int getRows() const { return rows; }

		/**get the number of cols*/
		unsigned int getCols() const { return cols; }

		/**get the size of data*/
		unsigned int getSize() const { return size; }

		/**get the pointer of data*/
		S* getDataHandle() const { return data; }

		/**get the const pointer of data*/
		const S* getAccessData() const { return data; }

		/**resize a matrix with a default value*/
		void resize(const unsigned int r, const unsigned int c, S val = S(0.0))
		{
			del();
			allocateMem(r, c);
			initialize(val);
		}

	private:
		void allocateMem(const unsigned int r, const unsigned int c)
		{
			rows = r;
			cols = c;
			size = rows * cols;

			data = new S[size];
		}

		void initialize(S val)
		{
			for (unsigned int i = 0; i < size; i++)
			{
				data[i] = val;
			}
		}

	private:
		S* data;
		unsigned int rows;
		unsigned int cols;
		unsigned int size;
	};
}