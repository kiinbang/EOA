#pragma once

#include "LeastSquare.h"
#include "smam.h"

namespace smam { 
	namespace dlt
	{
		const unsigned int NUMDLTPARAMS = 11;

		class FrameDlt : public SMAM
		{
		public:
			FrameDlt();

			std::string DoModeling(const double gcp_sd_th,
				const unsigned int num_max_iter,
				const double maximum_sd,
				const double maximum_diff,
				const double pixelsize,
				const bool normalize,
				const std::string& outfilename) override;

			void SetConfigData(const std::vector<_SCENE_>& sceneData_, const std::vector<_GCP_>& gcps_) override;

			std::vector<std::vector<double>> getParameters() override;

		private:
			void DataNormalization();

			void GenNormalizedData();

			void DeNormalizedCoord(_GCP_& G);

			void DeNormalizedCoord(_ICP_& I, const SMParam& param);

			void DeNormalizedCoord(_GCL_& G);

			double Func_row(_GCP_ G, unsigned int image_index);

			double Func_row(_GCL_ G, unsigned int image_index);

			double Func_col(_GCP_ G, unsigned int image_index);

			double Func_col(_GCL_ G, unsigned int image_index);

			void Cal_PDs_linear(const _GCP_& G, const _ICP_& I, std::vector<double>& ret1, std::vector<double>& ret2, const unsigned int image_index);

			void Cal_PDs(const _GCP_& G, std::vector<double>& ret1, std::vector<double>& ret2, const unsigned int image_index);

			void Cal_PDs_Line(const _GCL_& GL1, const _GCL_& GL2, const _ICL_& IL, double* ret, unsigned int image_index, unsigned int num_param);

			void PointOBSInit(const _SCENE_& scene, const unsigned int num_param, math::Matrix<double>& A, math::Matrix<double>& L, math::Matrix<double>& W, const unsigned int scene_index);

			std::string DoInitModeling(unsigned int num_param);

			void FileOut(const std::string& fname, const std::string& st);

			void GenerateFileName(const std::string& name, std::string& param_list, std::string& GCP_list, std::string& GCL_List);

			void FillMatrix_Point_i(const _GCP_& GP,
				const _ICP_& IP,
				unsigned image_index,
				Matrix<double>& a1,
				Matrix<double>& a2,
				Matrix<double>& l,
				Matrix<double>& w,
				const unsigned int num_param);

			void FillMatrix_Point(CLeastSquare& LS, unsigned int num_param);

			double Func_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, unsigned int image_index);

			void FillMatrix_Line_i(const _ICL_& IL,
				const _GCL_& GLA,
				const _GCL_& GLB,
				const unsigned scene_index,
				Matrix<double>& a1,
				Matrix<double>& a2,
				Matrix<double>& a3,
				Matrix<double>& l,
				Matrix<double>& w,
				const unsigned int num_param);

			void FillMatrix_Line(CLeastSquare& LS, unsigned int num_param);

			void FillMatrix_XYZ_i(CLeastSquare& LS, _GCP_ GP_init, _GCP_ GP_ori, unsigned scene_index, Matrix<double>& a2, Matrix<double>& l, Matrix<double>& w);

			void FillMatrix_XYZ(CLeastSquare& LS);

			CLeastSquare Iteration(Matrix<double>& Xmat, const unsigned int num_param, double& var);

			std::string UpdateUnknownParam(Matrix<double>& Xmat, unsigned int num_param);

			std::string UpdateUnknownPoint(const Matrix<double>& Xmat, const unsigned int num_param);

			std::string UpdateUnknownLine(Matrix<double>& Xmat, unsigned int num_param);

			SMParam ParamDenormalization(SMParam& param, const unsigned int num_param);

			std::string getEop(std::vector <std::vector<double>>& rigorousParams);

			std::string Run(unsigned int num_param, unsigned int num_max_iter, double maximum_sd, double maximum_diff, std::string outfilename, double pixelsize);

		private:
			std::vector<_SCENE_> sceneData;
			std::vector<_SCENE_> sceneDataOri;
			std::vector<_GCP_> gcps;
			std::vector<_GCP_> gcpsOri;
			std::vector<_GCL_> gcls;
			std::vector<_GCL_> gclsOri;
			double SHIFT_X;/**<Shift of X*/
			double SCALE_X;/**<Scale of X*/
			double SHIFT_Y;/**<Shift of Y*/
			double SCALE_Y;/**<Scale of Y*/
			double SHIFT_Z;/**<Shift of Z*/
			double SCALE_Z;/**<Scale of Z*/
			bool bNormalized;
			double GCP_sd_threshold;/**<Threshold of SD of GCPs*/
		};
	}
}
