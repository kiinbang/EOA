#pragma once

#include <string>

#include "smparam.h"
#include "SSMMatrix.h"
#include "structuredData.h"

namespace smam
{
	class SMAM
	{
	public:
		virtual void SetConfigData(const std::vector<_SCENE_>& sceneData, const std::vector<_GCP_>& gcps) = 0;

		virtual std::string DoModeling(const double gcp_sd_th,
			const unsigned int num_max_iter,
			const double maximum_sd,
			const double maximum_diff,
			const double pixelsize,
			const bool normalize,
			const std::string& outfilename) = 0;

		virtual std::vector<std::vector<double>> getParameters() = 0;
	};

	/// Convert 11 dlt parameters to rigorous parameters
	static std::vector<double> dlt2rigorous(const std::vector<double>& dltParams)
	{
		const unsigned num_param = 11;
		const std::vector<double>& X = dltParams;

		/// Retrieve eop from dlt parameters
		/// https://doi.org/10.3846/gac.2018.1629
		math::Matrix<double> LA(3, 3), LC(3, 1);
		LA(0, 0) = X[0];	LA(0, 1) = X[1];	LA(0, 2) = X[2];
		LA(1, 0) = X[4];	LA(1, 1) = X[5];	LA(1, 2) = X[6];
		LA(2, 0) = X[8];	LA(2, 1) = X[9];	LA(2, 2) = X[10];

		LC(0) = -X[3];
		LC(1) = -X[7];
		LC(2) = -1.0;

		auto pos = LA.inverse() % LC;

		double temp = X[8] * X[8] + X[9] * X[9] + X[10] * X[10];
		double xp = (X[0] * X[8] + X[1] * X[9] + X[2] * X[10]) / temp;
		double yp = (X[4] * X[8] + X[5] * X[9] + X[6] * X[10]) / temp;
		double omg = atan(-X[9] / X[10]);
		double phi = asin(-X[8] / sqrt(temp));
		double xpX8X0 = xp * X[8] - X[0];
		double xpX9X1 = xp * X[9] - X[1];
		double xpX10X2 = xp * X[10] - X[2];
		double temp2 = cos(phi) * sqrt(xpX8X0 * xpX8X0 + xpX9X1 * xpX9X1 + xpX10X2 * xpX10X2);
		double kap = acos(xpX8X0 / temp2);
		double f = xpX8X0 / (cos(kap) * cos(phi) * sqrt(temp));

		std::vector<double> rigorousParams;
		rigorousParams.reserve(9);
		rigorousParams.push_back(pos(0));
		rigorousParams.push_back(pos(1));
		rigorousParams.push_back(pos(2));
		rigorousParams.push_back(omg);
		rigorousParams.push_back(phi);
		rigorousParams.push_back(kap);
		rigorousParams.push_back(xp);
		rigorousParams.push_back(yp);
		rigorousParams.push_back(f);

		return rigorousParams;
	}
}