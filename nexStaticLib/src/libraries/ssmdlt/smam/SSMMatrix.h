/*
* Copyright(c) 2001-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <iostream>
#include <stdexcept>
#include <string>

#include "SSMMatData.h"

namespace math
{
	/**
	*@class Matrix Class
	*@brief Mathematics class for manipulating a Matrix
	*/
	template<typename T>
	class Matrix : public MatData<T>
	{
		template<typename T, size_t r, size_t c>
		friend class FixedSizeMat;

		template<typename T, size_t d>
		friend class VectorMat;

	public:

		/**constructor*/
		Matrix() : MatData<T>() {}

		/**constructor*/
		Matrix(const unsigned int r, const unsigned int c) : MatData<T>(r, c) {}

		/**constructor*/
		Matrix(const unsigned int r, const unsigned int c, T val) : MatData<T>(r, c, val) {}

		/**copy constructor*/
		template<typename S>
		Matrix(const Matrix<S>& copy)
			: MatData<T>(copy.getRows(), copy.getCols())
		{
			for (unsigned int i = 0; i < this->getSize(); ++i)
			{
				this->getDataHandle()[i] = static_cast<T>(copy.getAccessData()[i]);
			}
		}

		/**destructor*/
		virtual ~Matrix() {}

		/**Add rows to existing matrix<br>
		*|A|addCols|B|=|A|
		*              |B|.
		*/
		void addRows(const Matrix<T>& copy)
		{
			if (this->getCols() != copy.getCols())
				throw std::runtime_error("addRows produces an error since the numbers of cols are not same.");

			Matrix<T> newMat(this->getRows() + copy.getRows(), this->getCols(), T(0));

			unsigned int i, j;

			for (i = 0; i < this->getRows(); ++i)
			{
				for (j = 0; j < this->getCols(); ++j)
				{
					newMat(i, j) = this->operator()(i, j);
				}
			}

			unsigned int size_r = copy.getRows();
			unsigned int size_c = copy.getCols();

			for (i = 0; i < size_r; ++i)
			{
				for (j = 0; j < size_c; ++j)
				{
					newMat(this->getRows() + i, j) = copy(i, j);
				}
			}

			*this = newMat;
		}

		/**Add columns to an existing matrix<br>
		*|A|addCols|B|=|A||B|.
		*/
		void addCols(const Matrix<T>& copy)
		{
			if (this->getRows() != copy.getRows())
				throw std::runtime_error("addCols produces an error since the numbers of rows are not same.");

			Matrix<T> newMat(this->getRows(), this->getCols() + copy.getCols(), T(0));

			unsigned int i, j;

			for (i = 0; i < this->getRows(); ++i)
			{
				for (j = 0; j < this->getCols(); ++j)
				{
					newMat(i, j) = this->operator()(i, j);
				}
			}

			unsigned int size_r = copy.getRows();
			unsigned int size_c = copy.getCols();

			for (i = 0; i < size_r; ++i)
			{
				for (j = 0; j < size_c; ++j)
				{
					newMat(i, this->getCols() + j) = copy(i, j);
				}
			}

			*this = newMat;
		}

		/**bind two matrices<br>
		*|A|add|B|=|A||0|<br>
		*          |0||B| = C.
		*/
		void addRowsCols(const Matrix<T>& copy)
		{
			Matrix<T> newMat(this->getRows() + copy.getRows(), this->getCols() + copy.getCols(), T(0));

			unsigned int i, j;

			for (i = 0; i < this->getRows(); ++i)
			{
				for (j = 0; j < this->getCols(); ++j)
				{
					newMat(i, j) = this->operator()(i, j);
				}
			}

			unsigned int size_r = copy.getRows();
			unsigned int size_c = copy.getCols();

			for (i = 0; i < size_r; ++i)
			{
				for (j = 0; j < size_c; ++j)
				{
					newMat(this->getRows() + i, this->getCols() + j) = copy(i, j);
				}
			}

			*this = newMat;
		}

		/**change specific part with given matrix<br>
		*if the size of an existing matrix is not available, resize the matrix.
		*/
		void insert(const unsigned int rIdx, const unsigned int cIdx, const Matrix<T>& copy)
		{
			if ((this->getRows() < (rIdx + copy.getRows())) || (this->getCols() < (cIdx + copy.getCols())))
			{
				unsigned int newrows = this->getRows(), newcols = this->getCols();
				if (this->getRows() < (rIdx + copy.getRows())) newrows = rIdx + copy.getRows();
				if (this->getCols() < (cIdx + copy.getCols())) newcols = cIdx + copy.getCols();

				Matrix<T> backup = *this;

				this->resize(newrows, newcols);

				for (unsigned int i = 0; i < backup.getRows(); ++i)
				{
					for (unsigned int j = 0; j < backup.getCols(); ++j)
					{
						this->operator()(i, j) = backup(i, j);
					}
				}
			}

			for (unsigned int i = 0; i < copy.getRows(); ++i)
			{
				for (unsigned int j = 0; j < copy.getCols(); ++j)
				{
					this->operator()(rIdx + i, cIdx + j) = copy(i, j);
				}
			}
		}

		/**using the given matrix and position accumulate existing matrix<br>
		*if the existing matrix size is not proper, throw error.
		*/
		void insertPlus(const unsigned int rIdx, const unsigned int cIdx, const Matrix<T> &copy)
		{
			if ((this->getRows() < (rIdx + copy.getRows())) || (this->getCols() < (cIdx + copy.getCols())))
			{
				throw std::runtime_error("The inserted matrix is too large.");
			}
			else
			{
				for (unsigned int i = 0; i < copy.getRows(); ++i)
				{
					for (unsigned int j = 0; j < copy.getCols(); ++j)
					{
						operator()(rIdx + i, cIdx + j) += copy(i, j);
					}
				}
			}
		}

		/**using the given matrix and position accumulate existing matrix<br>
		*if the existing matrix size is not proper, resize the matrix.
		*/
		void insertPlusResize(const unsigned int rIdx, const unsigned int cIdx, const Matrix<T> &copy)
		{
			if ((this->getRows() < (rIdx + copy.getRows())) || (this->getCols() < (cIdx + copy.getCols())))
			{
				unsigned int newrows = this->getRows(), newcols = this->getCols();
				if (this->getRows() < (rIdx + copy.getRows())) newrows = rIdx + copy.getRows();
				if (this->getCols() < (cIdx + copy.getCols())) newcols = cIdx + copy.getCols();

				Matrix<T> backup = *this;

				this->resize(newrows, newcols);

				for (unsigned int i = 0; i < backup.getRows(); ++i)
				{
					for (unsigned int j = 0; j < backup.getCols(); ++j)
					{
						this->operator()(i, j) = backup(i, j);
					}
				}
			}

			for (unsigned int i = 0; i < copy.getRows(); ++i)
			{
				for (unsigned int j = 0; j < copy.getCols(); ++j)
				{
					this->operator()(rIdx + i, cIdx + j) += copy(i, j);
				}
			}
		}

		/**get a subset from an existing matrix*/
		Matrix<T> getSubset(const unsigned int rIdx, const unsigned int cIdx, const unsigned int size_r, const unsigned int size_c) const
		{
			if ((this->getRows() < (rIdx + size_r)) || (this->getCols() < (cIdx + size_c)))
			{
				throw std::runtime_error("Out of range error from getSubset");
			}
			else
			{
				Matrix<T> ret(size_r, size_c, T(0));

				for (unsigned int i = 0; i < size_r; ++i)
				{
					for (unsigned int j = 0; j < size_c; ++j)
					{
						ret(i, j) = operator()(rIdx + i, cIdx + j);
					}
				}

				return ret;
			}
		}

		/**Get a abs-max value.*/
		T getMaxabsElement() const
		{
			double val = fabs(static_cast<double>(this->getAccessData()[0]));

			for (unsigned int i = 0; i < this->getSize(); ++i)
			{
				if (val < (fabs(static_cast<double>(this->getAccessData()[i]))))
					val = fabs(static_cast<double>(this->getAccessData()[i]));
			}

			return static_cast<T>(val);
		}

		/**Get a max value.*/
		T getMaxElement() const
		{
			T val = this->getAccessData()[0];

			for (unsigned int i = 0; i < this->getSize(); ++i)
			{
				if (val < this->getAccessData()[i])
					val = this->getAccessData()[i];
			}

			return val;
		}

		/**Get a abs-min value.*/
		T getMinabsElement() const
		{
			double val = fabs(this->getAccessData()[0]);

			for (unsigned int i = 0; i < this->getSize(); ++i)
			{
				if (val >(fabs(static_cast<double>(this->getAccessData()[i]))))
					val = fabs(static_cast<double>(this->getAccessData()[i]));
			}

			return static_cast<T>(val);
		}

		/**Get a min value.*/
		T getMinElement() const
		{
			T val = this->getAccessData()[0];

			for (unsigned int i = 0; i < this->getSize(); ++i)
			{
				if (val > this->getAccessData()[i])
					val = this->getAccessData()[i];
			}

			return val;
		}

		/**find 1d array index from row and col indices*/
		unsigned int findIndex(const unsigned int rIdx, const unsigned int cIdx) const
		{
			if ((this->getRows() <= rIdx) || (this->getCols() <= cIdx))
				throw std::runtime_error("OUt of range error from findIndex");

			unsigned int index = (rIdx*(this->getCols())) + cIdx;
			return index;
		}

		/**make an identity matrix*/
		void makeIdentityMat(const T & init_val = T(1))
		{
			if (this->getCols() != this->getRows())
				throw std::runtime_error("It is not a square matrix.");

			for (unsigned int i = 0; i < this->getRows(); ++i)
			{
				for (unsigned int j = 0; j < this->getCols(); ++j)
				{
					if (i == j) this->operator()(i, j) = init_val;
					else this->operator()(i, j) = T(0);
				}
			}
		};

		/**make a transpose matrix*/
		Matrix<T> transpose() const
		{
			Matrix<T> result(this->getCols(), this->getRows());

			for (unsigned int i = 0; i < this->getRows(); ++i)
			{
				for (unsigned int j = 0; j < this->getCols(); ++j)
				{
					result(j, i) = this->operator()(i, j);
				}
			}

			return result;
		};

		/**make a inverse matrix*/
		Matrix<T> inverse() const
		{
			if (this->getCols() != this->getRows())
				throw std::runtime_error("It is not a square matrix.");

			Matrix<T> copy(*this);

			if (this->getCols() == 1)
			{
				try
				{
					copy.getDataHandle()[0] = 1.0 / this->getAccessData()[0];
				}
				catch (...)
				{
					throw std::runtime_error("Divided by near zero in inverse function");
				}

				return copy;
			}
			else
			{
				Matrix<unsigned int> LUD;
				Matrix<T> mat_inv;

				try 
				{ 
					LUD = copy.LUDcompose(); 
				}
				catch (...) 
				{
					mat_inv.del();
					return mat_inv;
				}

				mat_inv = copy.LUInvert(LUD);

				return mat_inv;
			}

		}

	//operators
	public:
		void operator = (const Matrix<T>& copy)
		{
			this->resize(copy.getRows(), copy.getCols());

			for (unsigned int i = 0; i < this->getSize(); ++i)
			{
				this->getDataHandle()[i] = copy.getAccessData()[i];
			}
		}

		template<typename S>
		void operator = (const Matrix<S>& copy)
		{
			MatData::operator = (copy);
		}

		/**+ operator*/
		template<typename S>
		Matrix<T> operator + (const Matrix<S>& mat) const//Matrix + Matrix
		{
			if ((this->getRows() != mat.getRows()) || (this->getCols() != mat.getCols()))
				throw std::runtime_error("Different size error from operator +");

			Matrix<T> result(this->getRows(), this->getCols());

			for (unsigned int i = 0; i < this->getSize(); ++i)
				result.getDataHandle()[i] = static_cast<T>(this->getAccessData()[i] + mat.getAccessData()[i]);

			return result;
		}

		/**+= operator*/
		template<typename S>
		void operator += (const Matrix<S>& mat) //Matrix += Matrix
		{
			if ((this->getRows() != mat.getRows()) || (this->getCols() != mat.getCols()))
				throw std::runtime_error("Different size error from operator +=");

			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] += static_cast<T>(mat.getAccessData()[i]);
		}

		/**+ operator*/
		Matrix<T> operator + (const T& val) const//Matrix + val
		{
			Matrix<T> result(*this);

			for (unsigned int i = 0; i < this->getSize(); ++i)
				result.getDataHandle()[i] += val;

			return result;
		}

		/**+= operator*/
		void operator += (const T& val) //Matrix += val
		{
			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] += val;
		}

		/**- operator*/
		Matrix<T> operator - () const//-Matrix
		{
			Matrix<T> result(*this);

			for (unsigned int i = 0; i < this->getSize(); ++i)
				result.getDataHandle()[i] *= -1.0;

			return result;
		}

		/**- operator*/
		template<typename S>
		Matrix<T> operator - (const Matrix<S>& mat) const//Matrix - Matrix
		{
			if ((this->getRows() != mat.getRows()) || (this->getCols() != mat.getCols()))
				throw std::runtime_error("Different size error from operator -");

			Matrix<T> result(*this);

			for (unsigned int i = 0; i < this->getSize(); ++i)
				result.getDataHandle()[i] -= static_cast<T>(mat.getAccessData()[i]);

			return result;
		}

		/**-= operator*/
		template<typename S>
		void operator -= (const Matrix<S>& mat) //Matrix -= Matrix
		{
			if ((this->getRows() != mat.getRows()) || (this->getCols() != mat.getCols()))
				throw std::runtime_error("Different size error from operator -=");

			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] -= static_cast<T>(mat.getAccessData()[i]);
		}

		/**- operator*/
		Matrix<T> operator - (const T& val) const//Matrix - val
		{
			Matrix<T> result(*this);

			for (unsigned int i = 0; i < this->getSize(); ++i)
				result.getDataHandle()[i] -= val;

			return result;
		}

		/**-= operator*/
		void operator -= (const T& val) //Matrix -= val
		{
			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] -= val;
		}

		/** * operator*/
		template<typename S>
		Matrix<T> operator * (const Matrix<S>& mat) const//Matrix * Matrix
		{
			if ((this->getRows() != mat.getRows()) || (this->getCols() != mat.getCols()))
				throw std::runtime_error("Different size error from operator *");

			Matrix<T> result(*this);

			for (unsigned int i = 0; i < this->getSize(); ++i)
				result.getDataHandle()[i] *= static_cast<T>(mat.getAccessData()[i]);

			return result;
		}

		/** *= operator*/
		template<typename S>
		void operator *= (const Matrix<S>& mat) //Matrix *= Matrix
		{
			if ((this->getRows() != mat.getRows()) || (this->getCols() != mat.getCols()))
				throw std::runtime_error("Different size error from operator *=");

			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] *= static_cast<T>(mat.getAccessData()[i]);
		}

		/** * operator*/
		Matrix<T> operator * (const T& val) const//Matrix * val
		{
			Matrix<T> result(*this);

			for (unsigned int i = 0; i < this->getSize(); ++i)
				result.getDataHandle()[i] *= val;

			return result;
		}

		/** *= operator*/
		void operator *= (const T& val) //Matrix *= val
		{
			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] *= val;
		}

		/** / operator*/
		template<typename S>
		Matrix<T> operator / (const Matrix<S>& mat) const//Matrix / Matrix
		{
			if ((this->getRows() != mat.getRows()) || (this->getCols() != mat.getCols()))
				throw std::runtime_error("Different size error from operator /");

			Matrix<T> result(*this);
			try
			{
				for (unsigned int i = 0; i < this->getSize(); ++i)
					result.getDataHandle()[i] /= static_cast<T>(mat.getAccessData()[i]);
			}
			catch (...)
			{
				throw std::runtime_error("Divided by near zero in operator /");
			}

			return result;
		}

		/** /= operator*/
		template<typename S>
		void operator /= (const Matrix<S>& mat) //Matrix /= Matrix
		{
			if ((this->getRows() != mat.getRows()) || (this->getCols() != mat.getCols()))
				throw std::runtime_error("Different size error from operator /=");

			try
			{
				for (unsigned int i = 0; i < this->getSize(); ++i)
					this->getDataHandle()[i] /= static_cast<T>(mat.getAccessData()[i]);
			}
			catch (...)
			{
				throw std::runtime_error("Divided by near zero in operator /=");
			}
		}

		/** / operator*/
		Matrix<T> operator / (const T& val) const//Matrix / val
		{
			if (static_cast<double>(val) < DBL_EPSILON)
				throw std::runtime_error("Error from operator /(T val): Devided by near zero");

			Matrix<T> result(*this);

			try
			{
				for (unsigned int i = 0; i < this->getSize(); ++i)
					result.getDataHandle()[i] /= val;
			}
			catch (...)
			{
				throw std::runtime_error("Divided by near zero in operator /");
			}

			return result;
		}

		/**scalar /= operator*/
		void operator /= (const T& val) //Matrix / val
		{
			if (static_cast<double>(val) < DBL_EPSILON)
				throw std::runtime_error("Error from operator /=(T val): Devided by near zero");

			try
			{
				for (unsigned int i = 0; i < this->getSize(); ++i)
					this->getDataHandle()[i] /= val;
			}
			catch (...)
			{
				throw std::runtime_error("Divided by near zero in operator /=");
			}
		}

		/**matrix multiplication*/
		template<typename S>
		Matrix<T> operator % (const Matrix<S>& mat) const//Matrix % Matrix
		{
			if (this->getCols() != mat.getRows())
				throw std::runtime_error("The numbers of cols and rows are not correct for % operation.");

			T sum;
			Matrix<T> result(this->getRows(), mat.getCols());

			for (unsigned int i = 0; i < this->getRows(); ++i)
			{
				for (unsigned int j = 0; j < mat.getCols(); ++j)
				{
					sum = T(0);
					for (unsigned int k = 0; k < this->getCols(); ++k)
					{
						sum += operator()(i, k)*static_cast<T>(mat(k, j));
					}
					result(i, j) = sum;
				}
			}
			return result;
		}

		/**Get square sum*/
		double getSum() const
		{
			double sum = 0.0;
			for (unsigned int i = 0; i < this->getSize(); ++i)
				sum += static_cast<double>(this->getAccessData()[i]);

			return sum;
		}

		/**Get square sum*/
		double getSumOfSquares() const
		{
			Matrix<T> temp = (*this) * (*this);
			return temp.getSum();
		}

		/**() operator: access a element of matrix*/
		T& operator () (const unsigned int rIdx, const unsigned int cIdx = 0) const
		{
			unsigned int i;
			i = findIndex(rIdx, cIdx);
			return this->getDataHandle()[i];
		}

		bool operator == (const Matrix<T>& copy)
		{
			if (this->getRows() != copy.getRows()) return false;
			if (this->getCols() != copy.getCols()) return false;

			for (unsigned int i = 0; i < this->getSize(); ++i)
			{
				if(fabs(this->getAccessData()[i] - copy.getAccessData()[i]) > std::numeric_limits<double>::epsilon())
					return false;
			}

			return true;
		}

		bool operator != (const Matrix<T>& copy)
		{
			if (this->operator == (copy))
				return false;
			else
				return true;
		}

	private:
		/**LU Decompose*/
		Matrix<unsigned int> LUDcompose()
		{
			// LU decomposition
			unsigned int i, j, k, k2, t;
			T p, temp;
			// permutation matrix
			Matrix<unsigned int> perm(this->getRows(), 1);

			// initialize permutation
			for (i = 0; i < this->getRows(); ++i)
				perm(i, 0) = i;

			for (k = 0; k < (this->getRows() - 1); ++k)
			{
				p = T(0);

				for (i = k; i < this->getRows(); ++i)
				{
					temp = (this->getAccessData()[findIndex(i, k)] < 0 ? (-this->getAccessData()[findIndex(i, k)]) : this->getAccessData()[findIndex(i, k)]);

					if (temp > p)
					{
						p = temp;
						k2 = i;
					}
				}

				if (p == T(0))
					throw std::runtime_error("Inverse failed: SINGULAR_MATRIX");

				// exchange rows
				t = perm(k, 0);
				perm(k, 0) = perm(k2, 0);
				perm(k2, 0) = t;

				for (i = 0; i < this->getRows(); ++i)
				{
					temp = this->getAccessData()[findIndex(k, i)];
					this->getDataHandle()[findIndex(k, i)] = this->getAccessData()[findIndex(k2, i)];
					this->getDataHandle()[findIndex(k2, i)] = temp;
				}

				for (i = k + 1; i < this->getRows(); ++i)
				{
					this->getDataHandle()[findIndex(i, k)] /= this->getAccessData()[findIndex(k, k)];

					for (j = k + 1; j < this->getRows(); ++j)
						this->getDataHandle()[findIndex(i, j)] -= this->getAccessData()[findIndex(i, k)] * this->getAccessData()[findIndex(k, j)];
				}
			}
			return perm;
		}

		/**LU Invert*/
		Matrix<T> LUInvert(Matrix<unsigned int> &perm)
		{
			unsigned int i, j;
			Matrix<T> p(this->getRows(), 1, T(0));

			Matrix<T> result(this->getRows(), this->getRows(), T(0));

			for (j = 0; j < this->getRows(); ++j)
			{
				for (i = 0; i < this->getRows(); ++i)
					p(i, 0) = T(0);

				p(j, 0) = T(1);

				p = LUSolve(perm, p);

				for (i = 0; i < this->getRows(); ++i)
					result(i, j) = p(i, 0);
			}

			return result;
		}

		/**LU solve*/
		Matrix<T> LUSolve(Matrix<unsigned int> & perm, Matrix<T> & b)
		{
			unsigned int i, j, j2;
			T sum, u;
			Matrix<T> y(this->getRows(), 1, T(0)), x(this->getRows(), 1, T(0));

			for (i = 0; i < this->getRows(); ++i)
			{
				sum = T(0);
				j2 = 0;

				for (j = 1; j <= i; ++j)
				{
					sum += this->getAccessData()[findIndex(i, j2)] * y(j2, 0);
					++j2;
				}
				y(i, 0) = b(perm(i, 0), 0) - sum;
			}

			i = this->getRows() - 1;

			while (1)
			{
				sum = T(0);
				u = this->getAccessData()[findIndex(i, i)];

				for (j = i + 1; j < this->getRows(); ++j)
					sum += this->getAccessData()[findIndex(i, j)] * x(j, 0);

				x(i, 0) = (y(i, 0) - sum) / u;

				if (i == 0)
					break;

				--i;
			}

			return x;
		}

	};

	/**
	 * @brief It calculates a inverse matrix for a partitioned matrix.<br>
	 * Note that the quality of the result is not as good as the normal inverse function.
	 * @param a left top block (m11)
	 * @param b right top block (m12)
	 * @param c left bottom block (m21)
	 * @param d right bottom block (m22)
	 * @return inverse matrix
	*/
	template<typename T>
	Matrix<T> inversePartitionedMatrix(const Matrix<T>& a, const Matrix<T>& b, const Matrix<T>& c, const Matrix<T>& d)
	{
		if (a.getCols() != c.getCols()
			|| a.getRows() != b.getRows()
			|| c.getRows() != d.getRows()
			|| b.getCols() != d.getCols())
		{
			std::cout << "Error caused by improper matrix sizes in inversePartitionedMatrix" << std::endl;
			throw std::runtime_error("Error caused by improper matrix sizes in inversePartitionedMatrix");
		}
		/**
	 	* http://www.cfm.brown.edu/people/dobrush/cs52/python/Part2/partition.html
		*/
		
		// Schur complement: M/A
		auto aInv = a.inverse();
		auto ma = d - c % aInv % b;
		auto result22 = ma.inverse();//(M/A).inverse()
		auto temp = (aInv % b % result22);
		auto result12 = -temp;
		auto result11 = aInv + (temp % c % aInv);
		auto result21 = -result22 % c % aInv;

		Matrix<double> result(a.getRows() + c.getRows(), a.getCols() + b.getCols(), 0.0);
		result.insertPlus(0, 0, result11);
		result.insertPlus(0, result11.getCols(), result12);
		result.insertPlus(result11.getRows(), 0, result21);
		result.insertPlus(result11.getRows(), result11.getCols(), result22);

		return result;
	}

	template<typename T>
	std::string matrixout(const Matrix<T>& mat)
	{
		std::string contents;

		for (unsigned int r = 0; r < mat.getRows(); ++r)
		{
			for (unsigned int c = 0; c < mat.getCols(); ++c)
			{
				contents += std::to_string(mat(r, c)) + std::string("\t");
			}

			contents += std::string("\n");
		}

		return contents;
	}
}