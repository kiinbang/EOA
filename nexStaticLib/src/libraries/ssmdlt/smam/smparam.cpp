#include "smparam.h"

namespace smam
{
	SMParam::SMParam()
	{
		Initialize(0);
	}

	SMParam::SMParam(const unsigned int numparam)
	{
		Initialize(numparam);
	}

	SMParam::SMParam(const SMParam& copy)
	{
		Copy(copy);
	}

	void SMParam::Copy(const SMParam& copy)
	{
		num_param = copy.num_param;

		A = copy.A;

		SHIFT_col = copy.SHIFT_col;
		SCALE_col = copy.SCALE_col;
		SHIFT_row = copy.SHIFT_row;
		SCALE_row = copy.SCALE_row;
	}

	SMParam::~SMParam()
	{
		Initialize(0);
	}

	SMParam& SMParam::operator=(const SMParam& copy)
	{
		Copy(copy);
		return *this;
	}

	void SMParam::Initialize(const unsigned int numparam)
	{
		num_param = numparam;

		A.resize(num_param);

		for (unsigned int i = 0; i < num_param; i++)
		{
			A[i] = 0.;
		}

		SHIFT_col = 0.0;
		SCALE_col = 1.0;
		SHIFT_row = 0.0;
		SCALE_row = 1.0;
	}
}