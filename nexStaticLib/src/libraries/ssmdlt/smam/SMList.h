/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/


#if !defined(__SPACEMATICS_SMList_H__)
#define __SPACEMATICS_SMList_H__

/**
*@class DATA
*@brief data template class.
*/
template <typename T>
class DATA
{
public:
	DATA()
	{
		forward = nullptr;
		next = nullptr;
	}

	virtual ~DATA() {}

	DATA<T> *forward;/**<forward address*/
	DATA<T> *next;/**<next address*/
	T value;/**<this place data*/
};


/**@class CSMList
*@brief template class for list data[class DATA] management.
*/
template<typename T>
class CSMList
{
public:
	/**constuctor*/
	CSMList()
	{
		first = nullptr;
		next = nullptr;
		m_nNumData = 0;
	}

	/**copy constructor*/
	CSMList(CSMList& copy)
	{
		m_nNumData = 0;
		next = nullptr;
		first = nullptr;
			
		for(unsigned long i=0; i<copy.m_nNumData; i++)
		{
			//AddTail() 함수를 이용한 복사
			AddTail(copy.GetAt(i));
		}
	}

	/**destructor*/
	virtual ~CSMList()
	{
	}

private:
	//member variable
	DATA<T> *first;/**<pointer of first data*/
	DATA<T> *next; /**<pointer of next data*/
	unsigned long m_nNumData; /**<number of data(elements)*/

//member function
public:
	/**Get number of item*/
	unsigned long GetNumItem()
	{
		return m_nNumData;
	}

	/**Set void data*/
	void SetVoidData(unsigned int num)
	{
		T data;
		for(unsigned int i=0; i<num; i++) AddTail(data);
	}
	/**fill the data with default value*/
	void SetVoidData(unsigned int num, T data)
	{
		for(unsigned int i=0; i<num; i++) AddTail(data);
	}

	/**Add data to tail*/
	bool AddTail(T data)
	{
		if(m_nNumData == 0)
		{
			next = new DATA<T>;
			next->forward = nullptr;
			first = next;
		}
		
		m_nNumData++;
		next->value = data;
		next->next = new DATA<T>;
		//next->next->forward = new DATA<T>;
		next->next->forward = next;
		next = next->next;
		next->next = nullptr;
		return true;
	}
	
	/**Add data to head*/
	bool AddHead(T data)
	{
		if(m_nNumData == 0)
		{
			next = new DATA<T>;
			next->forward = nullptr;
			first = next;
			first->value = data;
			first->next = new DATA<T>;
			//next->next->forward = new DATA<T>;
			first->next->forward = first;
			next = first->next;
			next->next = nullptr;
			m_nNumData++;
		}
		else
		{
			DATA<T> *newfirst = new DATA<T>;
			newfirst->value = data;
			newfirst->forward = nullptr;
			first->forward = newfirst;
			newfirst->next = first;
			first = newfirst;
			m_nNumData++;
		}
		
		return true;
	}

	/**Delete item by index which starts from #0*/
	bool DeleteData(unsigned long sort)
	{
		if(sort >= m_nNumData)
			return false;

		DATA<T>* target;
		target = GetDataHandle(sort);
		
		if(target != nullptr)
		{
			if(target->forward == nullptr)//첫번째 데이터 인경우
			{
				target->next->forward = nullptr;
				first = target->next;
				delete target;
			}
			else
			{
				target->forward->next = target->next;
				target->next->forward = target->forward;
				delete target;
			}
			m_nNumData--;
			return true;
		}
		
		return false;
	}

	/**Empty list*/
	bool RemoveAll()
	{
		unsigned long i, num;
		num = m_nNumData;
		for(i=0; i<num; i++)
		{
			//RemoveHead();
			RemoveTail();
		}
		return true;
	}

	/**Remove first data*/
	bool RemoveHead(void)
	{
		DATA<T>* target;
		target = first;
		target->next->forward = nullptr;
		first = target->next;
		delete target;
		m_nNumData--;
		return true;
	}
	
	/**Remove last data*/
	bool RemoveTail(void)
	{
		DATA<T>* target;
		target = GetDataHandle(m_nNumData-1);
		if(target->forward == nullptr)//첫번째 데이터 인경우
		{
			target->next->forward = nullptr;
			first = target->next;
			delete target;
		}
		else
		{
			target->forward->next = target->next;
			target->next->forward = target->forward;
			delete target;
		}
		
		m_nNumData--;
		return true;
	}

	/**Get specific data by index(sort)*/
	T GetAt(unsigned long sort)
	{
		DATA<T>* target;
		target = first;
		for(unsigned long i=0; i<sort; i++)
		{
			target = target->next;
		}
		
		return target->value;
	}

	/**Set specific data by index(sort)*/
	bool SetAt(unsigned long sort, T newvalue)
	{
		DATA<T> *target = GetDataHandle(sort);
		target->value = newvalue;
		return true;
	}

	/**Get first data*/
	T GetHead()
	{
		DATA<T>* target;
		target = first;
		return target->value;
	}

	/**Get last data*/
	T GetTail()
	{
		DATA<T>* target;
		target = first;
		for(unsigned long i=0; i<(m_nNumData-1); i++)
		{
			target = target->next;
		}

		return target->value;
	}

	/**[] operator overloading*/
	T& operator [] (unsigned long sort)
	{
		DATA<T> *target = GetDataHandle(sort);
		return target->value;
	}

	/**= operator overloading*/
	void operator=(CSMList& copy)
	{
		RemoveAll();
		m_nNumData = 0;
		next = nullptr;
		first = nullptr;
		
		for(unsigned long i=0; i<copy.m_nNumData; i++)
		{
			//AddTail() 함수를 이용한 복사
			AddTail(copy.GetAt(i));
		}
	}

private:
	/**Get DATA handle*/
	DATA<T>* GetDataHandle(unsigned long sort)
	{
		DATA<T> *target;
		target = first;
		
		for(unsigned long i=0; i<sort; i++)
		{
			target = target->next;
		}
		return target;
	}
};

#endif // !defined(__SPACEMATICS_SMList_H__)
