// leastsquare.h: interface for the CLeastSquare class.
//
//////////////////////////////////////////////////////////////////////

/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/

//////////////////////////////////////////////////////////////////////
//This is the class for the adjustment solution
//
//Directly compose normal matrix
//N_dot, N_2dot, N_bar
//Reduced normal matrix are used for the better performance
//////////////////////////////////////////////////////////////////////

#if !defined(__BBARAB_LEAST_SQUARE_ADJUSTMENT_CLASS__)
#define __BBARAB_LEAST_SQUARE_ADJUSTMENT_CLASS__

#pragma warning(disable: 4996)

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>
#include <vector>

#include "SSMMatrix.h"
using namespace math;
/////////////////////////////////////////////////////////////////////////////////////
//This is the class for Least Square
//Made by Bang, Ki In
//2005-07-12: Start day
//[Mathematical Model]
//A%X = L+V
/////////////////////////////////////////////////////////////////////////////////////

template<typename T>
void setVoidData(std::vector<T>& list, const unsigned int num, const T& temp)
{
	for (unsigned int i = 0; i < num; ++i)
	{
		list.push_back(temp);
	}
}

/**@class RETMAT
*@brief class for return data of LS
*/
class RETMAT
{
public:
	Matrix<double> A; /**<Design matrix */
	Matrix<double> L; /**<Observation matrix */
	Matrix<double> W; /**<Weight matrix */
	Matrix<double> AT; /**<A transpose matrix */
	Matrix<double> N; /**<Normal matrix */
	Matrix<double> Ninv; /**<Normal inverse matrix */
	Matrix<double> VTV; /**<Sum of residual square*/
	Matrix<double> X; /**<Unknown matrix */
	Matrix<double> V; /**<Residual matrix */
	Matrix<double> XVarCov; /**<Variance-covariance matrix of unknown parameters */
	Matrix<double> LVarCov; /**<Variance-covariance matrix of observation */
	Matrix<double> LVar; /**<Variance matrix of observation */
	Matrix<double> XCorrelation; /**<Correlation matrix of unknown parameters */
	Matrix<double> VCorrelation; /**<Correlation matrix of observations */
	double SD; /**<Strandar deviation */
	double Variance; /**<Variance */
	double max_X_element;
};

/**@class MatrixData
*@brief for using the std::vector class, Matrix are wrapped with un-template class.
*/
class MatrixData
{
public:
	Matrix<double> mat;
};

/**@class CLeastSquare
*@brief class for least square and bundle adjustment including reduced normal matrix.
*/
class CLeastSquare  
{
public:
	/**constructor*/
	CLeastSquare(){Init();}
	/**destructor*/
	virtual ~CLeastSquare(){}
	/**initialized the object*/
	void Init()
	{
		Pos_Var = 0.0;
		num_eq = 0;
		num_unknown = 0;
	}
	
	/**simple LS without weight matrix*/
	RETMAT RunLeastSquare(Matrix<double> A, Matrix<double> L)
	{
		unsigned int _size_ = A.getRows();
		Matrix<double> W(_size_, _size_);
		W.makeIdentityMat();
		return RunLeastSquare(A, L, W);
	}

	/**simple and fast(poor result, only X matrix) LS*/
	Matrix<double> RunLeastSquare_Fast(Matrix<double> A, Matrix<double> L)
	{
		unsigned int _size_ = A.getRows();
		Matrix<double> W(_size_, _size_);
		W.makeIdentityMat();
		return RunLeastSquare_Fast(A, L, W);
	}

	/**LS using weight matrix, not using reduced normal matrix for simple modeling*/
	RETMAT RunLeastSquare(Matrix<double> A, Matrix<double> L, Matrix<double> W)
	{
		RETMAT RETVAL;
		RETVAL.A = A;
		RETVAL.L = L;
		RETVAL.W = W;
		RETVAL.AT = RETVAL.A.transpose();
		RETVAL.N = RETVAL.AT%RETVAL.W%RETVAL.A;
		RETVAL.Ninv = RETVAL.N.inverse();
		RETVAL.X = RETVAL.Ninv%RETVAL.AT%RETVAL.W%RETVAL.L;
		RETVAL.V = RETVAL.A%RETVAL.X - RETVAL.L;
		RETVAL.VTV = RETVAL.V.transpose()%RETVAL.W%RETVAL.V;
		RETVAL.Variance = RETVAL.VTV(0,0)/(RETVAL.A.getRows()-RETVAL.A.getCols());
		RETVAL.SD = sqrt(RETVAL.Variance);
		RETVAL.XVarCov = RETVAL.Ninv*RETVAL.Variance;
		RETVAL.LVarCov = (RETVAL.A%RETVAL.Ninv%RETVAL.AT)*RETVAL.Variance;
		
		return RETVAL;
	}

	/**LS using weight matrix, not using reduced normal matrix for simple modeling<br>
	*poor result, only X matrix, fast a little.
	*/
	Matrix<double> RunLeastSquare_Fast(Matrix<double> A, Matrix<double> L, Matrix<double> W)
	{
		RETMAT RETVAL;
		RETVAL.A = A;
		RETVAL.L = L;
		RETVAL.W = W;

		RETVAL.AT = RETVAL.A.transpose();
		RETVAL.N = RETVAL.AT%RETVAL.W%RETVAL.A;
		RETVAL.Ninv = RETVAL.N.inverse();
		RETVAL.X = RETVAL.Ninv%RETVAL.AT%RETVAL.W%RETVAL.L;
				
		return RETVAL.X;
	}

	Matrix<double> RunLeastSquare_Fast(Matrix<double> A, Matrix<double> L, Matrix<double> W, Matrix<double> &N)
	{
		RETMAT RETVAL;
		RETVAL.A = A;
		RETVAL.L = L;
		RETVAL.W = W;

		RETVAL.AT = RETVAL.A.transpose();
		RETVAL.N = RETVAL.AT%RETVAL.W%RETVAL.A;
		RETVAL.Ninv = RETVAL.N.inverse();
		RETVAL.X = RETVAL.Ninv%RETVAL.AT%RETVAL.W%RETVAL.L;

		N = RETVAL.N;
				
		return RETVAL.X;
	}
	
	/**Get correlation matrix from variance-covariance matrix*/
	Matrix<double> Correlation(Matrix<double> VarCov)
	{
		unsigned int i, j;
		//Correlation of Unknown Matrix
		Matrix<double> Correlation;
		Correlation.resize(VarCov.getRows(),VarCov.getCols(),0.);
		
		for(i=0; i<Correlation.getRows(); i++)
		{
			double sigma_i, sigma_j;
			sigma_i = sqrt(VarCov(i,i));
			
			for(j=i; j<Correlation.getCols();j++)
			{
				sigma_j = sqrt(VarCov(j,j));
				Correlation(i,j) = VarCov(i,j)/sigma_i/sigma_j;
			}
		}
		return Correlation;
	}

	/**Extract variance(diagonal element) matrix from variance-covariance matrix*/
	Matrix<double> ExtractVar(Matrix<double> VarCov)
	{
		unsigned int i;
		//Correlation of Unknown Matrix
		Matrix<double> Variance(VarCov.getRows(),1,0.);

		for(i=0; i<Variance.getRows(); i++)
		{
			Variance(i,0) = VarCov(i,i);
		}
		return Variance;
	}

	////////////////////////////
	///Reduced Normal Matrix////
	////////////////////////////
	/**LS for bundle using reduced normal matrix<br>
	*EO parameters, point data and straight line data.
	*/
	Matrix<double> RunLeastSquare_RN_old(double &var)
	{	
		//N_dot
		Matrix<double> N_dot; N_dot = Make_N_dot();
		//N_2dot_inverse
		Matrix<double> N_2dot_inv; N_2dot_inv.resize(0,0);
		//N_bar_transe
		Matrix<double> N_bar_trans; N_bar_trans = N_bar.transpose();
		//C_dot(K_dot)
		Matrix<double> C_dot; C_dot = Make_C_dot();
		//C_2dot(K_2dot)
		Matrix<double> C_2dot; C_2dot = Make_C_2dot();

		//Make N_2dot_inverse matrix (Points)
		for(unsigned int point_index=0; point_index<num_point; point_index++)
		{
			Matrix<double> inv;
			inv = N_2dot_point[point_index].mat.inverse();
			N_2dot_inv.insert(point_index*3,point_index*3,inv);
		}
		//Make N_2dot_inverse matrix (Lines)
		for(unsigned int line_index=0; line_index<num_line; line_index++)
		{
			Matrix<double> inv;
			inv = N_2dot_line[line_index].mat.inverse();
			N_2dot_inv.insert(num_point*3+line_index*6,num_point*3+line_index*6,inv);
		}
		
		//S=(N_dot - (N_bar*N_2dot_inverser*N_bar_trans))
		Matrix<double> S;
		S = (N_dot - (N_bar%N_2dot_inv%N_bar_trans));
		//E = (K_dot - N_bar*N_2dot_inverse*K_2dot)
		Matrix<double> E;
		E = (C_dot - N_bar%N_2dot_inv%C_2dot);

		///////////////////////////////////////////////
		Matrix<double> X_dot = S.inverse()%E;
		///////////////////////////////////////////////

		//i: image, j: point, k: line
		Matrix<double> X_2dot;
		X_2dot.resize(0,0);
		//Point
		for(unsigned int point_index=0; point_index<num_point; point_index++)
		{
			//X_2dot of point j
			Matrix<double> X_2dot_j, NX_ij(3,1);
			for(unsigned int image_index=0; image_index<num_image; image_index++)
			{
				//N_bar_trans of scene i, point j
				//X_dot of scene i
				Matrix<double> N_b_t_ij, X_dot_i;
				N_b_t_ij = N_bar_trans.getSubset(point_index*3,image_index*num_param,3,num_param);
				X_dot_i = X_dot.getSubset(image_index*num_param,0,num_param,1);
				//Sum of (N_bar_trans*X_dot) of scene i, point j
				NX_ij += N_b_t_ij%X_dot_i;
			}

			//K_2dot of point j
			Matrix<double> C_2dot_point_j;
			C_2dot_point_j = C_2dot_point[point_index].mat;
			Matrix<double> N_2dot_inv_j;
			//N_2dot_inverse of point j
			N_2dot_inv_j = N_2dot_inv.getSubset(point_index*3,point_index*3,3,3);
			/////////////////////////////////////////////////////
			//X_2dot of point j
			X_2dot_j = N_2dot_inv_j%(C_2dot_point_j - NX_ij);
			/////////////////////////////////////////////////////
			X_2dot.insert(X_2dot.getRows(),0,X_2dot_j);
		}

		//Line
		for(unsigned int line_index=0; line_index<num_line; line_index++)
		{
			//X_2dot(6,1) of line k
			Matrix<double> X_2dot_k, NX_ik(6,1);
			for(unsigned int image_index=0; image_index<num_image; image_index++)
			{
				//N_bar_trans of scene i, line k
				//X_dot of scene i
				Matrix<double> N_b_t_ik, X_dot_i;
				N_b_t_ik = N_bar_trans.getSubset(num_point*3+line_index*6,image_index*num_param,6,num_param);
				X_dot_i = X_dot.getSubset(image_index*num_param,0,num_param,1);
				NX_ik += N_b_t_ik%X_dot_i;
			}
			
			//K_2dot of line k
			Matrix<double> C_2dot_line_k;
			C_2dot_line_k = C_2dot_line[line_index].mat;
			//N_2dot_inverse of line k
			Matrix<double> N_2dot_inv_k;
			N_2dot_inv_k = N_2dot_inv.getSubset(num_point*3+line_index*6,num_point*3+line_index*6,6,6);
			/////////////////////////////////////////////////////
			//X_2dot of line k
			X_2dot_k = N_2dot_inv_k%(C_2dot_line_k - NX_ik);
			/////////////////////////////////////////////////////
			X_2dot.insert(X_2dot.getRows(),0,X_2dot_k);
		}

		//X_2dot add to X_dot
		X_dot.addRows(X_2dot);

		//standard deviation of unit weight
		var = Pos_Var/(num_eq-num_unknown);

		return X_dot;
	}

	Matrix<double> RunLeastSquare_RN(double &var)
	{	
		//
		//Sove X_dot
		//

		Matrix<double> S(num_image*num_param, num_image*num_param, 0.0);
		Matrix<double> E(num_image*num_param, 1, 0.0);
		
		for(unsigned long point_index=0; point_index<num_point; point_index++)
		{
			Matrix<double> N_bar_j = N_bar.getSubset(0, point_index*3, num_image*num_param, 3);
			Matrix<double> N_bar_j_T = N_bar_j.transpose();
			MatrixData N_2dot_j_inv;
			N_2dot_j_inv.mat = N_2dot_point[point_index].mat.inverse();
			N_2dot_point_inv.push_back(N_2dot_j_inv);
			
			S += N_bar_j % N_2dot_j_inv.mat % N_bar_j_T;

			Matrix<double> C_2dot_j = C_2dot_point[point_index].mat;
			E += N_bar_j % N_2dot_j_inv.mat % C_2dot_j;
		}

		for(unsigned int line_index=0; line_index<num_line; line_index++)
		{
			Matrix<double> N_bar_k = N_bar.getSubset(0, 3*num_point + line_index*6, num_image*num_param, 6);
			Matrix<double> N_bar_k_T = N_bar_k.transpose();
			MatrixData N_2dot_k_inv;
			N_2dot_k_inv.mat = N_2dot_line[line_index].mat.inverse();
			N_2dot_line_inv.push_back(N_2dot_k_inv);
			
			S += N_bar_k % N_2dot_k_inv.mat % N_bar_k_T;

			Matrix<double> C_2dot_k = C_2dot_line[line_index].mat;
			E += N_bar_k % N_2dot_k_inv.mat % C_2dot_k;
		}

		S = S*-1.0;

		E = E*-1.0;
		
		for(unsigned int i=0; i<num_image; i++)
		{
			S.insertPlus(i*num_param, i*num_param, N_dot[i].mat);

			E.insertPlus(i*num_param, 0, C_dot[i].mat);
		}

		///////////////////////////////////////////////
		Matrix<double> X_dot = S.inverse()%E;
		///////////////////////////////////////////////

		//
		//Sove points and lines
		//

		//i: image, j: point, k: line
		Matrix<double> X_2dot;
		X_2dot.resize(0,0);
		
		//Point
		for(unsigned int point_index=0; point_index<num_point; point_index++)
		{
			//X_2dot of point j
			Matrix<double> X_2dot_j, NX_ij(3,1);
			for(unsigned int image_index=0; image_index<num_image; image_index++)
			{
				//N_bar_trans of scene i, point j
				//X_dot of scene i
				Matrix<double> N_b_t_ij, X_dot_i;
				//N_b_t_ij = N_bar_trans.getSubset(point_index*3,image_index*num_param,3,num_param);
				N_b_t_ij = N_bar.getSubset(image_index*num_param, point_index*3, num_param, 3).transpose();
				
				X_dot_i = X_dot.getSubset(image_index*num_param,0,num_param,1);
				//Sum of (N_bar_trans*X_dot) of scene i, point j
				NX_ij += N_b_t_ij%X_dot_i;
			}

			//K_2dot of point j
			Matrix<double> C_2dot_point_j;
			C_2dot_point_j = C_2dot_point[point_index].mat;
			Matrix<double> N_2dot_inv_j;
			//N_2dot_inverse of point j
			N_2dot_inv_j = N_2dot_point_inv[point_index].mat;
			
			/////////////////////////////////////////////////////
			//X_2dot of point j
			X_2dot_j = N_2dot_inv_j%(C_2dot_point_j - NX_ij);
			/////////////////////////////////////////////////////
			
			X_2dot.insert(X_2dot.getRows(),0,X_2dot_j);
		}

		//Line
		for(unsigned int line_index=0; line_index<num_line; line_index++)
		{
			//X_2dot(6,1) of line k
			Matrix<double> X_2dot_k, NX_ik(6,1);
			for(unsigned int image_index=0; image_index<num_image; image_index++)
			{
				//N_bar_trans of scene i, line k
				//X_dot of scene i
				Matrix<double> N_b_t_ik, X_dot_i;
				//N_b_t_ik = N_bar_trans.getSubset(num_point*3+line_index*6,image_index*num_param,6,num_param);
				N_b_t_ik = N_bar.getSubset(image_index*num_param, num_point*3+line_index*6, num_param, 6).transpose();

				X_dot_i = X_dot.getSubset(image_index*num_param,0,num_param,1);
				NX_ik += N_b_t_ik%X_dot_i;
			}
			
			//K_2dot of line k
			Matrix<double> C_2dot_line_k;
			C_2dot_line_k = C_2dot_line[line_index].mat;
			//N_2dot_inverse of line k
			Matrix<double> N_2dot_inv_k;
			N_2dot_inv_k = N_2dot_line_inv[line_index].mat;
			/////////////////////////////////////////////////////
			
			//X_2dot of line k
			X_2dot_k = N_2dot_inv_k%(C_2dot_line_k - NX_ik);
			/////////////////////////////////////////////////////
			
			X_2dot.insert(X_2dot.getRows(),0,X_2dot_k);
		}

		//X_2dot add to X_dot
		X_dot.addRows(X_2dot);

		//standard deviation of unit weight
		var = Pos_Var/(num_eq-num_unknown);

		return X_dot;
	}

	/**fill the normal matrix for the point data*/
	void Fill_Nmat_Data_Point(Matrix<double> a1, Matrix<double> a2, Matrix<double> w, Matrix<double> l, int point_index, int image_index)
	{
		Matrix<double> temp;
		Matrix<double> a1_t; a1_t = a1.transpose();
		Matrix<double> a2_t; a2_t = a2.transpose();
		
		temp = a1_t%w%a1;
		MatrixData& N_dot_i = N_dot[image_index];
		N_dot_i.mat += temp;

		temp = a1_t%w%a2;
		N_bar.insertPlus(image_index*num_param,point_index*3,temp);

		temp = a2_t%w%a2;
		MatrixData& N_2dot_j = N_2dot_point[point_index];
		N_2dot_j.mat += temp;

		temp = a1_t%w%l;
		MatrixData& C_dot_i = C_dot[image_index];
		C_dot_i.mat += temp;

		temp = a2_t%w%l;
		MatrixData& C_2dot_j = C_2dot_point[point_index];
		C_2dot_j.mat += temp;

		//Posteriori variance
		Pos_Var +=(l.transpose()%w%l)(0,0);

		//Number of equations
		num_eq += 2;
	}

	/**fill the normal matrix for the ending point data of the line*/
	void Fill_Nmat_Data_PointofLine(Matrix<double> a1, Matrix<double> a2, Matrix<double> w, Matrix<double> l, int line_index, int image_index)
	{
		Matrix<double> temp;
		Matrix<double> a1_t; a1_t = a1.transpose();
		Matrix<double> a2_t; a2_t = a2.transpose();
		
		temp = a1_t%w%a1;
		MatrixData& N_dot_i = N_dot[image_index];
		N_dot_i.mat += temp;

		temp = a1.transpose()%w%a2;
		N_bar.insertPlus(image_index*num_param,num_point*3+line_index*6,temp);

		temp = a2_t%w%a2;
		MatrixData& N_2dot_k = N_2dot_line[line_index];
		N_2dot_k.mat += temp;

		temp = a1_t%w%l;
		MatrixData& C_dot_i = C_dot[image_index];
		C_dot_i.mat += temp;

		temp = a2_t%w%l;
		MatrixData& C_2dot_k = C_2dot_line[line_index];
		C_2dot_k.mat += temp;

		//Posteriori variance
		Pos_Var +=(l.transpose()%w%l)(0,0);

		//Number of equations
		num_eq += 2;
	}

	/**fill the normal matrix for the line data*/
	void Fill_Nmat_Data_Line(Matrix<double> a1, Matrix<double> a2, Matrix<double> w, Matrix<double> l, int line_index, int image_index)
	{
		Matrix<double> temp;
		Matrix<double> a1_t; a1_t = a1.transpose();
		Matrix<double> a2_t; a2_t = a2.transpose();
		
		temp = a1_t%w%a1;
		MatrixData& N_dot_i = N_dot[image_index];
		N_dot_i.mat += temp;

		temp = a1_t%w%a2;
		N_bar.insertPlus(image_index*num_param,num_point*3+line_index*6,temp);

		temp = a2_t%w%a2;
		MatrixData& N_2dot_k = N_2dot_line[line_index];
		N_2dot_k.mat += temp;

		temp = a1_t%w%l;
		MatrixData& C_dot_i = C_dot[image_index];
		C_dot_i.mat += temp;

		temp = a2_t%w%l;
		MatrixData& C_2dot_k = C_2dot_line[line_index];
		C_2dot_k.mat += temp;

		//Posteriori variance
		Pos_Var +=(l.transpose()%w%l)(0,0);
		//Number of equations
		num_eq += 1;
	}

	/**3D ground point constraints*/
	void Fill_Nmat_Data_XYZ(Matrix<double> a2, Matrix<double> w, Matrix<double> l, int point_index)
	{
		Matrix<double> temp;
		Matrix<double> a2_t; a2_t = a2.transpose();
		temp = a2_t%w%a2;
		MatrixData& N_2dot_j = N_2dot_point[point_index];
		N_2dot_j.mat += temp;

		temp = a2_t%w%l;
		MatrixData& C_2dot_j = C_2dot_point[point_index];
		C_2dot_j.mat += temp;

		//Posteriori variance
		//Pos_Var +=(l.transpose()%w%l)(0,0);

		//Number of equations
		num_eq += 3;
	}

	/**3D ending point of the line constraints*/
	void Fill_Nmat_Data_XYZofLine(Matrix<double> a2, Matrix<double> w, Matrix<double> l, int line_index)
	{
		Matrix<double> temp;
		Matrix<double> a2_t; a2_t = a2.transpose();
		temp = a2_t%w%a2;
		MatrixData& N_2dot_k = N_2dot_line[line_index];
		N_2dot_k.mat += temp;

		temp = a2_t%w%l;
		MatrixData& C_2dot_k = C_2dot_line[line_index];
		C_2dot_k.mat += temp;

		//Posteriori variance
		//Pos_Var +=(l.transpose()%w%l)(0,0);

		//Number of equations
		num_eq += 3;
	}

	/**Set configuration data for RunLeastSquare_RN*/
	void SetDataConfig(unsigned int n_image, unsigned int n_param, unsigned int n_point, unsigned int n_line)
	{
		num_image = n_image; 
		num_param = n_param; 
		num_point = n_point; 
		num_line = n_line;

		num_unknown = num_image*num_param 
			        + num_point*3
					+ num_line*6;

		MatrixData temp;

		temp.mat.resize(num_param,num_param,0.);
		setVoidData(N_dot, num_image, temp);
		//N_dot.resize(num_param*num_image,num_param*num_image);

		temp.mat.resize(3,3,0);
		setVoidData(N_2dot_point, num_point, temp);

		temp.mat.resize(6,6,0);
		setVoidData(N_2dot_line, num_line, temp);

		N_bar.resize(num_image*num_param,num_point*3+num_line*6,0.);

		temp.mat.resize(num_param,1,0.);
		setVoidData(C_dot, num_image, temp);

		temp.mat.resize(3,1,0.);
		setVoidData(C_2dot_point, num_point, temp);

		temp.mat.resize(6,1,0.);
		setVoidData(C_2dot_line, num_line, temp);
	}

protected:
	std::vector<MatrixData> N_dot;/**<N_dot(EOP(num_param)) matrix*/
	std::vector<MatrixData> N_2dot_point;/**<N_2dot_point(point(3)) matrix*/
	std::vector<MatrixData> N_2dot_point_inv;/**<N_2dot_point(point(3)) INVERSE matrix*/
	std::vector<MatrixData> N_2dot_line;/**<N_2dot_line(line(6)) matrix*/
	std::vector<MatrixData> N_2dot_line_inv;/**<N_2dot_line(line(6)) INVERSE matrix*/
	Matrix<double> N_bar;/**<N_bar matrix*/
	
	std::vector<MatrixData> C_dot;/**<C_dot(EOP) matrix*/
	std::vector<MatrixData> C_2dot_point;/**<C_2dot_point matrix*/
	std::vector<MatrixData> C_2dot_line;/**<C_2dot_line matrix*/

	unsigned int num_image;/**<number of scene data*/
	unsigned int num_param;/**<number of EO parameters*/
	unsigned int num_point;/**<number of points*/
	unsigned int num_line;/**<number of lines*/

	double Pos_Var;/**<posteriori variance*/
	unsigned int num_unknown;/**<number of all unknown parameters*/
	unsigned int num_eq;/**<number of equations and constraints*/

protected:
	/**Make N_dot matrix from N_dot elements list*/
	Matrix<double> Make_N_dot()
	{
		Matrix<double> retmat;
		retmat.resize(num_image*num_param,num_image*num_param,0.);
		for(unsigned int i=0; i<num_image; i++)
			retmat.insert(i*num_param,i*num_param,N_dot[i].mat);
		return retmat;
	}

	/**Make C_dot matrix from C_dot elements list*/
	Matrix<double> Make_C_dot()
	{
		Matrix<double> retmat;
		retmat.resize(num_image*num_param,1,0.);
		for(unsigned int i=0; i<num_image; i++)
			retmat.insert(i*num_param,0,C_dot[i].mat);
		return retmat;
	}

	/**Make C_2dot matrix from C_2dot_point and C_2dot_line elements list*/
	Matrix<double> Make_C_2dot()
	{
		Matrix<double> retmat;
		retmat.resize(num_point*3+num_line*6,1,0.);
		for(unsigned int i=0; i<num_point; i++)
			retmat.insert(i*3,0,C_2dot_point[i].mat);
		for(unsigned int i=0; i<num_line; i++)
			retmat.insert(3*num_point+i*6,0,C_2dot_line[i].mat);
		return retmat;
	}

	/**write given string on given file*/
	void FileOut(std::string fname, std::string st)
	{
		FILE* outfile;
		outfile = nullptr;
		outfile = fopen(fname.c_str(),"a");
		if(outfile == nullptr) 
			outfile = fopen(fname.c_str(),"w");
		fprintf(outfile,st.c_str());
		fclose(outfile);
	}
};

#endif // !defined(__BBARAB_LEAST_SQUARE_ADJUSTMENT_CLASS__)