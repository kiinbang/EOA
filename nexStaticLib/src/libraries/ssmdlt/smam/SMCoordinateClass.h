/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/

#ifndef _COORDINATE_CLASS_
#define _COORDINATE_CLASS_

#include<math.h>
#include <stdexcept>

/**@class Point2D
*@brief for 2D coordinate
*/
template<typename T>
class Point2D  // Declare 2D coordinate struct type
{
public:
	T x;			// x
	T y;			// y
	Point2D() { x = T(0); y = T(0); }
	Point2D(const Point2D<T>& coord)	{ x = coord.x; y = coord.y; }
	Point2D(const T& _x, const T& _y) { x = _x; y = _y; }
	Point2D(const int n) { x = T(n); y = T(n); }
	virtual ~Point2D() { }
	//operator
	Point2D<T>& operator=(const Point2D<T>& coord) { x = coord.x; y = coord.y; return *this; }
	Point2D<T> operator+(const Point2D<T>& coord) {
		Point2D<T> r;
		r.x = x + coord.x;
		r.y = y + coord.y;
		return r;
	}
	Point2D<T> operator-(const Point2D<T>& coord) {
		Point2D<T> r;
		r.x = x - coord.x;
		r.y = y - coord.y;
		return r;
	}
	Point2D<T> operator*(const double s) {
		Point2D<T> r;
		r.x = T(x*s);
		r.y = T(y*s);
		return r;
	}
	Point2D<T> operator/(const double s) {
		Point2D<T> r;
		if(s != 0.0)
		{
			r.x = T(x/s);
			r.y = T(y/s);
		}
		else
		{
			throw std::runtime_error("Point2D<T> Class: Divided zero");
		}
		return r;
	}
	
	void operator()(const T& _x, const T& _y)	{ x = _x; y = _y; }
	void operator()(const T value) { x = value; y = value; }
	
};

/**@class Point3D
*@brief for 3D coordinate
*/
template<class T>
class Point3D  // Declare 3D coordinate struct type
{
public:
	T x;			// x
	T y;			// y
	T z;			// z
	Point3D() { x = T(0); y = T(0); z = T(0); }
	Point3D(const Point3D<T>& coord)	{ x = coord.x; y = coord.y; z = coord.z; }
	Point3D(const T& _x, const T& _y, const T& _z) { x = _x; y = _y; z = _z; }
	Point3D(const int n) { x = T(n); y = T(n); z = T(n);}
	virtual ~Point3D() { }
	//operator
	Point3D<T>& operator=(const Point3D<T>& coord) { x = coord.x; y = coord.y; z = coord.z; return *this; }
	Point3D<T> operator+(const Point3D<T>& coord) {
		Point3D<T> r;
		r.x = x + coord.x;
		r.y = y + coord.y;
		r.z = z + coord.z;
		return r;
	}

	Point3D<T> operator-(const Point3D<T>& coord) {
		Point3D<T> r;
		r.x = x - coord.x;
		r.y = y - coord.y;
		r.z = z - coord.z;
		return r;
	}
	Point3D<T> operator*(const double s) {
		Point3D<T> r;
		r.x = T(x*s);
		r.y = T(y*s);
		r.z = T(z*s);
		return r;
	}
	Point3D<T> operator/(const double s) {
		Point3D<T> r;
		if(s != 0.0)
		{
			r.x = T(x/s);
			r.y = T(y/s);
			r.z = T(z/s);
		}
		else
		{
			throw std::runtime_error("Point3D<T> Class: Divided zero");
		}
		return r;
	}

	void operator()(const T& _x, const T& _y, const T& _z)	{ x = _x; y = _y; z = _z; }
	void operator()(const T value) { x = value; y = value; z = value; }
	
};

////////////////////////////////////////////////////////////////////
#endif