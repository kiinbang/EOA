#pragma once

#include <string>
#include <vector>

#include "SMCoordinateClass.h"
#include "smparam.h"

namespace smam
{
	/**
	*@struct _ICL_
	*@brief data structure for ICL(intermediate point in Control line).
	*/
	struct _ICL_
	{
		double row; /**<row*/
		double col; /**<col*/
		double s[4] = {1., 1., 1., 1.}; /**<sigma: s(1,1)s(1,2)s(2,1)s(2,2)*/
		std::string flag; /**<Point flag: A&B(ending point), C(intermediate point)*/
		std::string SID; /**<Scene ID*/
		std::string LID; /**< Line ID*/
	};

	/**
	*@struct _ICP_
	*@brief data structure for ICP(Image Control Point).
	*/
	struct _ICP_
	{
		double row; /**<row*/
		double col; /**<col*/
		double s[4]; /**<sigma: s(1,1)s(1,2)s(2,1)s(2,2)*/
		std::string SID; /**<Scene ID*/
		std::string PID;/**< Point ID*/
	};

	/**
	*@struct _GCL_
	*@brief data structure for GCL(Ground Control Line).
	*/
	struct _GCL_
	{
		double X, Y, Z; /**<Ground point coord*/
		double s[9]; /**<sigma: s(1,1)s(1,2)s(1,3)s(2,1)s(2,2)s(2,3)s(3,1)s(3,2)s(3,3)*/
		std::string LID; /**< Line ID*/
		std::string flag; /**<flag*/
	};

	/**
	*@struct _GCP_
	*@brief data structure for GCP(Ground Control Point).
	*/
	struct _GCP_
	{
		double X; /**<X*/
		double Y; /**<Y*/
		double Z; /**<Z*/
		double s[9]; /**<sigma: s(1,1)s(1,2)s(1,3)s(2,1)s(2,2)s(2,3)s(3,1)s(3,2)s(3,3)*/
		std::string PID; /**< Point ID*/
	};

	/**
	*@struct _SCENE_.
	*@brief data structure for Scene.
	*/
	struct _SCENE_
	{
		//property
		std::vector<_ICP_> ICP_List; /**<ICP Data List manager. */
		std::vector<_ICL_> ICL_List; /**<ICL Data List manager. */
		std::string SID; /**<scene id*/
		smam::SMParam param;
	};

	static _ICP_ copyICL(const _ICL_& IL)
	{
		_ICP_ icp;
		icp.row = IL.row;
		icp.col = IL.col;
		for (int i = 0; i < 4; i++)
			icp.s[i] = IL.s[i];
		icp.PID = (IL.LID + IL.flag);
		return icp;
	};

	static _GCP_ copyGCL(const _GCL_& GL)
	{
		_GCP_ gcp;
		gcp.X = GL.X;
		gcp.Y = GL.Y;
		gcp.Z = GL.Z;
		for (int i = 0; i < 9; i++)
			gcp.s[i] = GL.s[i];
		gcp.PID = (GL.LID + GL.flag);
		return gcp;
	};

	static _GCP_ copyGCL(const Point3D<double>& G)
	{
		_GCP_ gcp;
		gcp.X = G.x;
		gcp.Y = G.y;
		gcp.Z = G.z;
		return gcp;
	};

	inline std::string to_string(const smam::_GCP_& p)
	{
		std::string outstr;
		outstr = p.PID;
		outstr += ",\t";
		outstr += std::to_string(p.X);
		outstr += ",\t";
		outstr += std::to_string(p.Y);
		outstr += ",\t";
		outstr += std::to_string(p.Z);
		outstr += "\n";
		return outstr;
	}

	inline std::string to_string(const smam::_GCL_& l)
	{
		std::string outstr;
		outstr = l.LID;
		outstr += ",\t";
		outstr += "( ";
		outstr += l.flag;
		outstr += " )";
		outstr += ",\t";
		outstr += std::to_string(l.X);
		outstr += ",\t";
		outstr += std::to_string(l.Y);
		outstr += ",\t";
		outstr += std::to_string(l.Z);
		outstr += "\n";
		return outstr;
	}
}
