#pragma once

#include <vector>

namespace smam
{
	/**
	*@class SMParam
	*@brief This is the class for approximate model parameters.
	*/
	class SMParam
	{
	public:
		SMParam();
		SMParam(const unsigned int numparam);
		SMParam(const SMParam& copy);
		virtual ~SMParam();
		/**
		*Initialize class members.
		*/
		void Initialize();
		void Initialize(const unsigned int numparam);
		/**
		*Assignment operator.
		*/
		SMParam& operator=(const SMParam& copy);
		/**
		*Copy constructor.
		*/
		void Copy(const SMParam& copy);

	public:
		std::vector<double> A; /**<parameters*/

		unsigned int num_param;

		/**
		*Coord normalization coefficients<br>
		*Normalized Coord = (Original Coord + SHIFT)*SCALE<br>
		*Original Coord = (Normalized Coord/SCALE) - SHIFT.
		*/
		double SHIFT_col;/**<Shift of col*/
		double SCALE_col;/**<Scale of col*/
		double SHIFT_row;/**<Shift of row*/
		double SCALE_row;/**<Scale of row*/
	};
}