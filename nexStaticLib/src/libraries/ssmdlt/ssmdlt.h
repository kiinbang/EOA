#pragma once

#ifdef SSMDLT_EXPORTS
#define SSMDLT_LIB __declspec(dllexport)
#else
#define SSMDLT_LIB __declspec(dllimport)
#endif

#include "smam/smam.h"
#include "smam/structuredData.h"

std::shared_ptr<smam::SMAM> SSMDLT_LIB runDltFactory();