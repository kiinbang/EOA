#include "ssmdlt.h"

#include "smam/FDLT.h"


std::shared_ptr<smam::SMAM> runDltFactory()
{
	std::shared_ptr<smam::SMAM> dlt(new smam::dlt::FrameDlt);
	return dlt;
}