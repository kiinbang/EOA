#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
namespace bpo = boost::program_options;

#include <TinyEXIF/TinyEXIF.h>
#include "./src/libraries/Interpolation/Interpolation.h"
#include <ssm/include/CoordinateTransformation.h>

#define YYYY dt[0]
#define MM dt[1]
#define DD dt[2]
#define hh dt[3]
#define mm dt[4]
#define ss dt[5]

namespace math
{
	namespace geo
	{
		const unsigned int numDTArray = 6;
		using DTArray = std::array<unsigned int, numDTArray>;

		struct ImgInfo
		{
			std::string fineName = "";
			DTArray dt = {0, 0, 0, 0, 0, 0};
			FDEpoch epoch;
		};

		/** read a flight data file
		* @param path: file path
		* @param tree: KDtree for flight data
		* @param maxLeaves: maximum number of tree leaves
		* @return bool
		*/
		int readFDFile(const std::string& path, FDataTree& tree, const double headOffset, const unsigned int maxLeaves = 20);

		/**
		* @brief compare two std::string without case sensitivity
		* @param s1 : first string
		* @param s2 : second string
		* @return comparison result
		*/
		bool getCaseInsensitiveMatch(std::string s1, std::string s2);

		std::string splitFilename(const std::string& folderPath, std::string& parent);

		/** read all files in a folder
		* @param folderPath: folder path
		* @param filter: search filter, ex) *.*, *.dat, etc.
		* @return file list as a search result
		*/
		std::vector<std::string> getFileListInFolder(const std::string& folderPath, const std::string& filter);

		/** read entire binary file
		* @param path file path
		* @param data byte array
		* @param length file length
		*/
		void readBinFile(const std::string& path, std::vector<uint8_t>& data, std::streampos& length);

		/** import jpg files and parse exif info
		* @param folderPath: folder path
		* @param tree: KDtree
		* @param imgFileExt: image file extension (default: jpg)
		* @return bool
		*/
		std::vector<ImgInfo> importJpgs(const std::string& imgFolderPath, const FDataTree& tree, const unsigned int localTimeDiff, const std::string& imgFileExt = "jpg");

		void readFile(const std::string& path, std::vector<uint8_t>& data, std::streampos& length);

		TinyEXIF::EXIFInfo getExifInfo(std::string jpgPath);

		unsigned int getJulianDay(const unsigned int year, const unsigned int month, const unsigned int day);

		unsigned long int getDaySeconds(ImgInfo& imgInfo);

		double getDaySeconds(const unsigned int h, const unsigned int m, const float s);

		void setGpsSecond(ImgInfo& imgInfo, unsigned int timeOffset/*msec*/);


		/** parse date and time string
		* @param stTime: date and time in string
		* @param delimiters:
		* @param dt: array for storing date and time
		*/
		void parseTime(const std::string& stTime, const std::vector<char> delimiters, DTArray& dt);

		/** read a flight data file
		* @param path: file path
		* @param tree: KDtree for flight data
		* @param maxLeaves: maximum number of tree leaves
		* @return int
		*/
		int readFDFile(const std::string& path, FDataTree& tree, const double headOffset, const unsigned int maxLeaves)
		{
			const double deg2rad = 3.14159265358979 / 180.0;

			std::vector<FDEpoch> epochs;

			std::fstream infile;
			infile.open(path, std::ios::in);
			if (!infile)
				return -1;

			unsigned int caseno;
			std::string cFormat;
			std::getline(infile, cFormat);
			if (cFormat.find(
				"SystemTime_msec,PosX_mtr,PosY_mtr,PosZ_mtr,GpsTime_msec,Gimbal_Roll,Gimbal_Tilt,Gimbal_Pan,HotShoeCount,reserved_1,reserved_2,reserved_3,reserved_4,reserved_5,reserved_6,reserved_7,reserved_8,reserved_9,reserved_10,reserved_11")
				!= std::string::npos)
			{
				caseno = 0;
			}
			else if (cFormat.find(
				"SystemTime_msec,Lattitude,Longitude,Altitude,Roll,Pitch,Yaw,Year,Month,Day,Hours,Miniutes,Seconds,Gimbal_Roll,Gimbal_Tilt,Gimbal_Pan,HotShoeCount,reserved_1,reserved_2,reserved_3,reserved_4,reserved_5,reserved_6,reserved_7,reserved_8,reserved_9,reserved_10,reserved_11")
				!= std::string::npos ||
				cFormat.find(
				"SystemTime_msec,Latitude,Longitude,Altitude,Roll,Pitch,Yaw,Year,Month,Day,Hours,Miniutes,Seconds,Gimbal_Roll,Gimbal_Tilt,Gimbal_Pan,HotShoeCount,reserved1,reserved2,reserved3,reserved4,reserved5,reserved6,reserved7,reserved8,reserved9,reserved10,reserved11")
				!= std::string::npos)
			{
				caseno = 1;
			}
			else if ("SystemTime_msec,Latitude,Longitude,Altitude,Roll,Pitch,Yaw,Year,Month,Day,Hours,Miniutes,Seconds,Gimbal_Roll,Gimbal_Tilt,Gimbal_Pan,HotShoeCount,reserved1,reserved2,reserved3,reserved4,reserved5,reserved6,reserved7,reserved8,reserved9,reserved10,reserved11")
			{

			}
			else if (cFormat.find(
				"SystemTime_msec,Roll,Pitch,Yaw,Vn,Ve,Vd,Latitude,Longitude,Altitude,Distance_S0_mtr,Distance_S1_mtr,Distance_S2_mtr,Distance_S3_mtr,Distance_S4_mtr,GpsNum,SyncFlag,SolutionFlag,SaveFlag")
				!= std::string::npos)
			{
				caseno = 2;
			}
			else
				return -1;

			/// Transform point coordinates
			std::vector<math::geo::PipeNode> pipeline;
			math::geo::setPipeline(math::geo::_ECEF, math::geo::wgs84, math::geo::_LLH_RAD, math::geo::wgs84, pipeline);

			std::string strBuf;
			math::geo::FDEpoch epoch;
			if (caseno == 0)
			{
				epoch.caseNo = 0;

				while (std::getline(infile, strBuf, ',')) //SystemTime_msec
				{
					epoch.msecSystemTime = std::stod(strBuf);

					std::getline(infile, strBuf, ',');//PosX_mtr
					epoch.x = std::stod(strBuf);
					std::getline(infile, strBuf, ',');//PosY_mtr
					epoch.y = std::stod(strBuf);
					std::getline(infile, strBuf, ',');//PosZ_mtr
					epoch.z = std::stod(strBuf);

					std::getline(infile, strBuf, ',');//GpsTime_msec
					epoch.msecGpsTime = static_cast<math::geo::FDataType>(std::stod(strBuf));

					std::getline(infile, strBuf, ',');//Gimbal_Roll
					epoch.gimRotX = std::stod(strBuf) * deg2rad;
					epoch.rotX = 0.0;
					std::getline(infile, strBuf, ',');//Gimbal_Tilt
					epoch.gimRotY = std::stod(strBuf) * deg2rad;
					epoch.rotY = 0.0;
					std::getline(infile, strBuf, ',');//Gimbal_Pan
					epoch.gimRotZ = (std::stod(strBuf) + headOffset) * deg2rad;
					epoch.rotZ = 0.0;

					std::getline(infile, strBuf, ','); //HotShoeCount
					std::getline(infile, strBuf, ','); //reserved_1
					std::getline(infile, strBuf, ','); //reserved_2
					std::getline(infile, strBuf, ','); //reserved_3
					std::getline(infile, strBuf, ','); //reserved_4
					std::getline(infile, strBuf, ','); //reserved_5
					std::getline(infile, strBuf, ','); //reserved_6
					std::getline(infile, strBuf, ','); //reserved_7
					std::getline(infile, strBuf, ','); //reserved_8
					std::getline(infile, strBuf, ','); //reserved_9
					std::getline(infile, strBuf, ','); //reserved_10
					std::getline(infile, strBuf); //reserved_11

					/// ECEF to LLH
					math::Arrayd inCoord(3);
					inCoord[0] = epoch.x;
					inCoord[1] = epoch.y;
					inCoord[2] = epoch.z;
					auto outPt = math::geo::sequentialTransform(inCoord, pipeline);
					epoch.x = outPt[0];
					epoch.y = outPt[1];
					epoch.z = outPt[2];

					tree.add(epoch);
				}
			}
			else if (caseno == 1)
			{
				epoch.caseNo = 1;

				while (std::getline(infile, strBuf, ',')) //SystemTime_msec
				{
					epoch.msecSystemTime = std::stod(strBuf);

					std::getline(infile, strBuf, ',');//lat
					epoch.x = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');//lon
					epoch.y = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');//h
					epoch.z = std::stod(strBuf);

					std::getline(infile, strBuf, ',');//Imu_Roll
					epoch.rotX = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');//Imu_Tilt
					epoch.rotY = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');//Imu_yaw
					epoch.rotZ = std::stod(strBuf) * deg2rad;

					std::getline(infile, strBuf, ',');//year
					epoch.year = std::stoi(strBuf);
					std::getline(infile, strBuf, ',');//month
					epoch.month = std::stoi(strBuf);
					std::getline(infile, strBuf, ',');//day
					epoch.day = std::stoi(strBuf);
					std::getline(infile, strBuf, ',');//hr
					epoch.hour = std::stoi(strBuf);
					std::getline(infile, strBuf, ',');//min
					epoch.min = std::stoi(strBuf);
					std::getline(infile, strBuf, ',');//sec
					epoch.sec = std::stof(strBuf);

					epoch.msecGpsTime = getDaySeconds(epoch.hour, epoch.min, epoch.sec) * 1000.0; /// gps time miliseconds in a day

					std::getline(infile, strBuf, ',');//Gimbal_Roll
					epoch.gimRotX = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');//Gimbal_Tilt
					epoch.gimRotY = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');//Gimbal_Pan
					epoch.gimRotZ = (std::stod(strBuf) + headOffset) * deg2rad + epoch.rotZ; /// body-frame heading + gimbal pan-angle

					while (epoch.gimRotZ > (3.14159265358979 * 2.0))
					{
						epoch.gimRotZ -= (3.14159265358979 * 2.0);
					};

					std::getline(infile, strBuf, ','); //HotShoeCount
					std::getline(infile, strBuf, ','); //reserved_1
					std::getline(infile, strBuf, ','); //reserved_2
					std::getline(infile, strBuf, ','); //reserved_3
					std::getline(infile, strBuf, ','); //reserved_4
					std::getline(infile, strBuf, ','); //reserved_5
					std::getline(infile, strBuf, ','); //reserved_6
					std::getline(infile, strBuf, ','); //reserved_7
					std::getline(infile, strBuf, ','); //reserved_8
					std::getline(infile, strBuf, ','); //reserved_9
					std::getline(infile, strBuf, ','); //reserved_10
					std::getline(infile, strBuf); //reserved_11

					tree.add(epoch);
				}
			}
			else if (caseno == 2)
			{
				epoch.caseNo = 2;

				while (std::getline(infile, strBuf, ',')) ///SystemTime_msec
				{
					epoch.msecSystemTime = std::stod(strBuf);
					epoch.msecGpsTime = epoch.msecSystemTime; /// assign a system time to a gps time for kd-tree search, since the tree uses the gps time for searching.

					std::getline(infile, strBuf, ',');///Imu_Roll [deg]
					epoch.rotX = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');///Imu_Pitch [deg]
					epoch.rotY = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');///Imu_Yaw [deg]
					epoch.rotZ = std::stod(strBuf) * deg2rad;

					std::getline(infile, strBuf, ',');///Vn
					epoch.vn = std::stod(strBuf);
					std::getline(infile, strBuf, ',');///Ve
					epoch.ve = std::stod(strBuf);
					std::getline(infile, strBuf, ',');///Vd
					epoch.vd = std::stod(strBuf);

					std::getline(infile, strBuf, ',');///lat [deg]
					epoch.x = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');///lon [deg]
					epoch.y = std::stod(strBuf) * deg2rad;
					std::getline(infile, strBuf, ',');///h [m]
					epoch.z = std::stod(strBuf);

					std::getline(infile, strBuf, ',');///distance s0 mtr
					epoch.s0 = std::stod(strBuf);

					std::getline(infile, strBuf, ',');///distance s1 mtr
					epoch.s1 = std::stod(strBuf);

					std::getline(infile, strBuf, ',');///distance s2 mtr
					epoch.s2 = std::stod(strBuf);

					std::getline(infile, strBuf, ',');///distance s3 mtr
					epoch.s3 = std::stod(strBuf);

					std::getline(infile, strBuf, ',');///distance s4 mtr
					epoch.s4 = std::stod(strBuf);

					std::getline(infile, strBuf, ',');///gps number
					epoch.gpsNum = std::stoi(strBuf);

					std::getline(infile, strBuf, ',');///sync flag
					epoch.syncFlag = std::stoi(strBuf);

					std::getline(infile, strBuf, ',');///solution flag
					epoch.solFlag = std::stoi(strBuf);

					std::getline(infile, strBuf);///save flag
					epoch.saveFlag = std::stoi(strBuf);
										
					tree.add(epoch);
				}
			}

			infile.close();

			tree.buildTree(maxLeaves);

			return caseno;
		}

		/** read time list
		* @param path: file path
		* @param timeTags: time tags
		* @return bool
		*/
		bool readTimeFile(const std::string& path, std::vector<double>& timeTags)
		{
			std::fstream infile;
			infile.open(path, std::ios::in);
			if (!infile)
				return false;

			std::string cFormat;
			std::getline(infile, cFormat);
			std::string strBuf;
			if (cFormat.find("SystemTime_msec")	!= std::string::npos)
			{
				int c = 0;
				while (std::getline(infile, strBuf)) //SystemTime_msec
				{
					double msec = std::stod(strBuf);
					timeTags.push_back(msec);
					infile >> std::ws;
					//std::cout << c++ << "\t" << msec << "\n";
				}
			}

			infile.close();

			return  true;
		}

		/**
		* @brief compare two std::string without case sensitivity
		* @param s1 : first string
		* @param s2 : second string
		* @return comparison result
		*/
		bool getCaseInsensitiveMatch(std::string s1, std::string s2)
		{
			//convert s1 and s2 into lower case strings
			transform(s1.begin(), s1.end(), s1.begin(), ::tolower);
			transform(s2.begin(), s2.end(), s2.begin(), ::tolower);
			if (s1.compare(s2) == 0)
				return true; //The strings are same
			return false; //not matched
		}

		std::string splitFilename(const std::string& folderPath, std::string& parent)
		{
			size_t found;
			std::cout << "Splitting: " << folderPath << std::endl;
			found = folderPath.find_last_of("/\\");
			std::cout << " parent folder: " << folderPath.substr(0, found) << std::endl;
			parent = folderPath.substr(0, found);
			std::cout << " last folder: " << folderPath.substr(found + 1) << std::endl;

			return folderPath.substr(found + 1);
		}

		/** read all files in a folder
		* @param folderPath: folder path
		* @param filter: search filter, ex) *.*, *.dat, etc.
		* @return file list as a search result
		*/
		std::vector<std::string> getFileListInFolder(const std::string& folderPath, const std::string& filter)
		{
			std::vector<std::string> fileList;

			for (boost::filesystem::directory_iterator itr(folderPath); itr != boost::filesystem::directory_iterator(); ++itr)
			{
				std::string ext = itr->path().extension().string();

				if (!getCaseInsensitiveMatch(std::string(".") + filter, ext))
					continue;

				if (!is_regular_file(itr->status()))
					continue;

				std::string fname = itr->path().filename().string();
				std::cout << fname << ' ' << " [" << file_size(itr->path()) << ']';
				std::cout << '\n';

				//fileList.push_back(itr->path().string());
				fileList.push_back(fname);
			}

			return fileList;
		}

		/** read entire binary file
		* @param path file path
		* @param data byte array
		* @param length file length
		*/
		void readBinFile(const std::string& path, std::vector<uint8_t>& data, std::streampos& length)
		{
			std::ifstream file(path.c_str(), std::ifstream::in | std::ifstream::binary);
			file.seekg(0, std::ios::end);
			length = file.tellg();
			file.seekg(0, std::ios::beg);
			data.resize(length);
			file.read((char*)data.data(), length);
			file.close();
		}

		/** parse date and time string
		* @param stTime: date and time in string
		* @param delimiters:
		* @param dt: array for storing date and time
		*/
		void parseTime(const std::string& stTime, const std::vector<char> delimiters, DTArray& dt)
		{
			size_t length = stTime.length();
			std::stringstream str;
			bool foundDelimiter = false;

			unsigned int idx = 0;

			for (int i = 0; i < length; ++i)
			{
				const auto pick = stTime.at(i);

				for (const auto d : delimiters)
				{
					if (pick == d)
					{
						foundDelimiter = true;
						break;
					}
				}

				if (foundDelimiter)
				{
					dt[idx] = atoi(str.str().c_str());
					++idx;

					str.str("");
					foundDelimiter = false;
				}
				else
					str << pick;
			}

			dt[numDTArray - 1] = atoi(str.str().c_str());
		}

		/** import jpg files and parse exif info
		* @param folderPath: folder path
		* @param tree: KDtree
		* @param imgFileExt: image file extension (default: jpg)
		* @return bool
		*/
		std::vector<ImgInfo> importJpgs(const std::string& imgFolderPath, const FDataTree& tree, const unsigned int localTimeDiff, const std::string& imgFileExt)
		{
			std::vector<std::string> fileList = getFileListInFolder(imgFolderPath, imgFileExt);
			std::cout << "Total " << fileList.size() << " images" << std::endl;

			std::vector<char> delimiters;
			delimiters.push_back(':');
			delimiters.push_back(' ');

			std::vector<ImgInfo> imgInfoList;

			for (const auto& fname : fileList)
			{
				std::string jpgPath = imgFolderPath + std::string("\\") + fname;
				std::vector<uint8_t> data;
				std::streampos length;
				readBinFile(jpgPath, data, length);
				TinyEXIF::EXIFInfo imageEXIF(data.data(), static_cast<unsigned int>(length));
				ImgInfo imgInfo;
				imgInfo.fineName = fname;
				parseTime(imageEXIF.DateTime, delimiters, imgInfo.dt);
				imgInfoList.push_back(imgInfo);
			}

			return imgInfoList;
		}

		/** import jpg files and parse exif info
		* @param folderPath: folder path
		* @param imgFileExt: image file extension (default: jpg)
		* @return bool
		*/
		std::vector<ImgInfo> importJpgs(const std::string& imgFolderPath, const unsigned int localTimeDiff, const std::string& imgFileExt)
		{
			std::vector<std::string> fileList = getFileListInFolder(imgFolderPath, imgFileExt);
			std::cout << "Total " << fileList.size() << " images" << std::endl;

			std::vector<char> delimiters;
			delimiters.push_back(':');
			delimiters.push_back(' ');

			std::vector<ImgInfo> imgInfoList;

			for (const auto& fname : fileList)
			{
				std::string jpgPath = imgFolderPath + std::string("\\") + fname;
				std::vector<uint8_t> data;
				std::streampos length;
				readBinFile(jpgPath, data, length);
				TinyEXIF::EXIFInfo imageEXIF(data.data(), static_cast<unsigned int>(length));
				ImgInfo imgInfo;
				imgInfo.fineName = fname;
				parseTime(imageEXIF.DateTime, delimiters, imgInfo.dt);
				imgInfoList.push_back(imgInfo);
			}

			return imgInfoList;
		}

		void readFile(const std::string& path, std::vector<uint8_t>& data, std::streampos& length)
		{
			std::ifstream file(path.c_str(), std::ifstream::in | std::ifstream::binary);
			file.seekg(0, std::ios::end);
			length = file.tellg();
			file.seekg(0, std::ios::beg);
			data.resize(length);
			file.read((char*)data.data(), length);
			file.close();
		}

		TinyEXIF::EXIFInfo getExifInfo(std::string jpgPath)
		{
			std::vector<uint8_t> data;
			std::streampos length;
			readFile(jpgPath, data, length);

			TinyEXIF::EXIFInfo imageEXIF(data.data(), static_cast<unsigned int>(length));

			return imageEXIF;
		}

		unsigned int getJulianDay(const unsigned int year, const unsigned int month, const unsigned int day)
		{
			auto jDay = 367 * year
				      - 7 * (year + (month + 9) / 12) / 4
					  - 3 * ((year + (month - 9) / 7) / 100 + 1) / 4
					  + 275 * month / 9
					  + day
					  + 1721028
					  - 2400000;

			return jDay;
		}

		unsigned long int getDaySeconds(ImgInfo& imgInfo)
		{
			unsigned int seconds = imgInfo.hh * 3600 + imgInfo.mm * 60 + imgInfo.ss;
			return seconds;
		}

		double getDaySeconds(const unsigned int h, const unsigned int m, const float s)
		{
			double seconds = static_cast<double>(h * 3600 + m * 60) + static_cast<double>(s);
			return seconds;
		}

		void setGpsSecond(ImgInfo& imgInfo, unsigned int timeOffset/*msec*/)
		{
			unsigned long int msec = (imgInfo.hh * 3600 + imgInfo.mm * 60 + imgInfo.ss) * 1000;
			imgInfo.epoch.msecGpsTime = msec + timeOffset;
		}
	}
}

/*
 * Simple tool to convert GPS time and calendar dates.
 */

#include <stdio.h>
#include <stdlib.h>

 /*
  * Return Modified Julian Day given calendar year,
  * month (1-12), and day (1-31).
  * - Valid for Gregorian dates from 17-Nov-1858.
  * - Adapted from sci.astro FAQ.
  */

long DateToMjd(long Year, long Month, long Day)
{
	return
		367 * Year
		- 7 * (Year + (Month + 9) / 12) / 4
		- 3 * ((Year + (Month - 9) / 7) / 100 + 1) / 4
		+ 275 * Month / 9
		+ Day
		+ 1721028
		- 2400000;
}

/*
 * Convert Modified Julian Day to calendar date.
 * - Assumes Gregorian calendar.
 * - Adapted from Fliegel/van Flandern ACM 11/#10 p 657 Oct 1968.
 */

void getModifiedJulianDay(long Mjd, long* Year, long* Month, long* Day)
{
	long J, C, Y, M;

	J = Mjd + 2400001 + 68569;
	C = 4 * J / 146097;
	J = J - (146097 * C + 3) / 4;
	Y = 4000 * (J + 1) / 1461001;
	J = J - 1461 * Y / 4 + 31;
	M = 80 * J / 2447;
	*Day = J - 2447 * M / 80;
	J = M / 11;
	*Month = M + 2 - (12 * J);
	*Year = 100 * (C - 49) + Y + J;
}

/*
 * Convert GPS Week and Seconds to Modified Julian Day.
 * - Ignores UTC leap seconds.
 */

long GpsToMjd(long GpsCycle, long GpsWeek, long GpsSeconds)
{
	long GpsDays;

	GpsDays = ((GpsCycle * 1024) + GpsWeek) * 7 + (GpsSeconds / 86400);
	return DateToMjd(1980, 1, 6) + GpsDays;
}

/*
 * Test program.
 */

void mainfunc(int argc, char* argv[])
{
	long Year, Month, Day;
	long Mjd;
	long GpsCycle, GpsWeek, GpsSeconds;

	switch (argc - 1) {
		/* Given MJD */
	case 1:
		Mjd = atol(argv[1]);

		getModifiedJulianDay(Mjd, &Year, &Month, &Day);

		printf("Mjd %ld = %.4ld-%.2ld-%.2ld\n", Mjd, Year, Month, Day);
		break;

		/* Given GPS week and seconds */
	case 2:
		GpsWeek = atol(argv[1]);
		GpsSeconds = atol(argv[2]);
		for (GpsCycle = 0; GpsCycle < 3; GpsCycle += 1) {

			Mjd = GpsToMjd(GpsCycle, GpsWeek, GpsSeconds);
			getModifiedJulianDay(Mjd, &Year, &Month, &Day);

			printf("Gps Cycle %ld, Week %ld, Seconds %ld ",
				GpsCycle, GpsWeek, GpsSeconds);
			printf(" = Mjd %ld = %.4ld-%.2ld-%.2ld\n",
				Mjd, Year, Month, Day);
		}
		break;

		/* Given Calendar year, month, and day */
	case 3:
		Year = atol(argv[1]);
		Month = atol(argv[2]);
		Day = atol(argv[3]);

		Mjd = DateToMjd(Year, Month, Day);

		printf("%.4ld-%.2ld-%.2ld = Mjd %ld\n", Year, Month, Day, Mjd);
		break;

	default:
		fprintf(stderr, "\nThis tool converts calendar date, MJD, and GPS dates.\n\n");
		fprintf(stderr, "Use %s [Mjd]\n", argv[0]);
		fprintf(stderr, "Use %s [Gps Week] [Seconds]\n", argv[0]);
		fprintf(stderr, "Use %s [Year] [Month] [Day]\n", argv[0]);
		break;
	}
}