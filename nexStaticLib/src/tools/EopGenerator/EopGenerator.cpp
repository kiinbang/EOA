// EopGenerator.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <codecvt>
using convert_t = std::codecvt_utf8<wchar_t>;
const double rad2deg = 180.0 / 3.14159265358979;
const double deg2rad = 3.14159265358979 / 180.0;

#include <boost/filesystem.hpp>

#include "FlightDataImport.h"

/// Collecting proper epochs for images
bool collectEpochs(const std::string& fdPath,
	const std::string& imgFolderPath,
	const std::string& imgExt,
	math::geo::FDataTree& fdTree,
	const unsigned int maxLeaves,
	const unsigned int numSearch,
	const double headingOffset,
	const double timeThreshold,
	const unsigned int camType,
	const unsigned int epochOffset);

bool collectPairs(const std::string& fdPath1,
	const std::string& imgFolderPath1,
	const std::string& fdPath2,
	const std::string& imgFolderPath2,
	const std::string& imgExt,
	const double timeThreshold,
	const unsigned int camType1,
	const unsigned int camType2,
	const double headingOffset = 0.0,
	const unsigned int maxLeaves = 20);

bool eopInterpolation(const std::string& fdPath,
	const std::string& imgFolderPath,
	const std::string& imgExt,
	math::geo::FDataTree& fdTree,
	const unsigned int maxLeaves,
	const unsigned int numSearch,
	const double headingOffset,
	const double timeThreshold,
	const unsigned int camType);

bool interpolateIns(const std::string& fdPath,
	const std::string& timePath,
	math::geo::FDataTree& fdTree,
	const unsigned int maxLeaves,
	const unsigned int numSearch,
	const double timeThreshold);

bool exportEop(const std::string& imgFolderPath,
	const std::vector<math::geo::ImgInfo>& imgInfo,
	const unsigned int camType);

int main(int argc, char** argv)
{
	math::geo::FDataTree fdTree;
	const unsigned int maxLeaves = 20;
	double headingOffset = 0.0;
	const std::string imgExt("jpg"); /// image file extension
	unsigned int numSearch = 2; /// default: 2
	double timeThreshold = 60000.0; /// default: 60000.0 (1min)
	unsigned int camType = 2; /// camera type
	unsigned int camType2; /// second camera type
	unsigned int toolOption = 0; /// tool option: 0 epoch collector, 1 interpolation
	unsigned int epochOffset = 0; /// Epoch index of the first image

	std::string imgFolderPath;
	std::string imgFolderPath2;
	std::string fdPath; /// flight data path
	std::string fdPath2; /// second flight data path
	std::string tarTimeFilePath;

	bpo::options_description desc{ "Options" };
	desc.add_options()
		("help,h", "Help screen")
		("tool", bpo::value<unsigned int>(&toolOption)->default_value(toolOption), "tool option: 0 epoch collector, 1 interpolation, 2 INS interpolation")
		("camtype,c", bpo::value<unsigned int>(&camType), "camera type: 1 wide 2 tele")
		("camtype2", bpo::value<unsigned int>(&camType2), "second camera type: 1 wide 2 tele")
		("headingOff,o", bpo::value<double>(&headingOffset)->default_value(headingOffset), "heading offset)")
		("nsearch,n", bpo::value<unsigned int>(&numSearch)->default_value(numSearch), "Number search")
		("thresholdtime,t", bpo::value<double>(&timeThreshold)->default_value(timeThreshold), "Time threshold")
		("epochOffset,e", bpo::value<unsigned int>(&epochOffset), "Epoch offset")
		("fdpath,f", bpo::value<std::string>(&fdPath), "Input flight data path")
		("fdpath2", bpo::value<std::string>(&fdPath2), "Second input flight data path 2")
		("imgfolder,i", bpo::value<std::string>(&imgFolderPath), "Image folder path")
		("imgfolder2", bpo::value<std::string>(&imgFolderPath2), "Second image folder path")
		("timepath", bpo::value<std::string>(&tarTimeFilePath), "Target time list file for interpolation");

	bpo::variables_map vm;
	bpo::store(parse_command_line(argc, argv, desc), vm);
	bpo::notify(vm);

	//if (vm.count("help"))
	//    std::cout << desc << std::endl;

	if (!(vm.count("fdpath")))
	{
		std::cout << "\n\n fdpath is essential. \n\n";
		std::cout << desc << std::endl;
		return 0;
	}

	if (toolOption == 0 || toolOption == 1 || toolOption == 3)
	{
		if (!vm.count("camtype") && vm.count("imgfolder"))

		{
			std::cout << "\n\n camtype and imgfolder are essential for option 0 and 1. \n\n";
			std::cout << desc << std::endl;
			return 0;
		}
	}

	if (toolOption == 0)
	{
		if (!vm.count("epochOffset"))
		{
			std::cout << "Epoch collection requires 'epochOffset'." << std::endl << std::endl;
			std::cout << desc << std::endl;
			return 0;
		}
	}
	else if (toolOption == 1)
	{
		if (!vm.count("thresholdtime"))
		{
			std::cout << "Epoch interpolation requires 'thresholdtime'." << std::endl << std::endl;
			std::cout << desc << std::endl;
			return 0;
		}
	}
	else if (toolOption == 2)
	{
		if (!vm.count("timepath"))
		{
			std::cout << "option 2 requires timepath (time list file)." << std::endl << std::endl;
			std::cout << desc << std::endl;
			return 0;
		}
	}
	else if (toolOption == 3)
	{
		bool checkInputData = true;
		if (!vm.count("camtype2"))
		{
			std::cout << "option 3 requires second camera type (camtype2)." << std::endl << std::endl;
			std::cout << desc << std::endl;
			checkInputData = false;
		}

		if (!vm.count("fdpath2"))
		{
			std::cout << "option 3 requires second flight data path (fdpath2)." << std::endl << std::endl;
			std::cout << desc << std::endl;
			checkInputData = false;
		}

		if (!vm.count("imgfolder2"))
		{
			std::cout << "option 3 requires second image folder path (imgfolder2)." << std::endl << std::endl;
			std::cout << desc << std::endl;
			checkInputData = false;
		}

		if (!checkInputData)
			return 0;
	}

	switch (toolOption)
	{
	case 1:
		/// EOP interpolation
		if (!eopInterpolation(fdPath, imgFolderPath, imgExt, fdTree, maxLeaves, numSearch, headingOffset, timeThreshold, camType))
		{
			std::cout << std::endl << "!!! Failure from eopInterpolation" << std::endl;
			return 0;
		}
		break;
	case 0:
		if (!collectEpochs(fdPath, imgFolderPath, imgExt, fdTree, maxLeaves, numSearch, headingOffset, timeThreshold, camType, epochOffset))
		{
			std::cout << std::endl << "!!! Failure from collectEpochs" << std::endl;
			return 0;
		}
		break;
	case 2:
		if (!interpolateIns(fdPath, tarTimeFilePath, fdTree, maxLeaves, numSearch, timeThreshold))
		{
			std::cout << std::endl << "!!! Failure from collectEpochs" << std::endl;
			return 0;
		}
		break;
	case 3:
		if (!collectPairs(fdPath, imgFolderPath, fdPath2, imgFolderPath2, imgExt, timeThreshold, camType, camType2))
		{
			std::cout << std::endl << "!!! Failure from collectPairs" << std::endl;
			return 0;
		}
		break;
	}

	std::cout << "Done: exporting an eop file" << std::endl;

	return 1;
}

/// Collecting proper epochs for images
bool collectEpochs(const std::string& fdPath,
	const std::string& imgFolderPath,
	const std::string& imgExt,
	math::geo::FDataTree& fdTree,
	const unsigned int maxLeaves,
	const unsigned int numSearch,
	const double headingOffset,
	const double timeThreshold,
	const unsigned int camType,
	const unsigned int epochOffset)
{
	/// 1. Import flight data and build kdtree for gpstime tags
	auto caseno = math::geo::readFDFile(fdPath, fdTree, headingOffset, maxLeaves);
	if (-1 == caseno)
	{
		std::cout << "Error from readFDFile" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Done: importing flight data, case #: " << caseno << std::endl;
	}

	/// 2. Import image files
	auto imgInfoImported = math::geo::importJpgs(imgFolderPath, fdTree, 9, imgExt);
	if (imgInfoImported.size() == 0)
	{
		std::cout << "Image folder is empty" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Done: importing image files" << std::endl;
	}

	/// Images for exporting
	std::vector<math::geo::ImgInfo> imgInfoExport;
	imgInfoExport.reserve(imgInfoImported.size());

	/// 3. Search epoch
	for (unsigned int i = 0; i < imgInfoImported.size(); i++)
	{
		if (i == 0)
			imgInfoImported[i].epoch = fdTree.getEpoch(i + epochOffset);
		else
		{
			unsigned long int sec1 = getDaySeconds(imgInfoImported[i]);
			unsigned long int sec0 = getDaySeconds(imgInfoImported[i - 1]);
			if (sec1 < sec0)
			{
				std::cout << "Need to check image list order. Image exposure time looks wrong." << std::endl;
				return false;
			}

			unsigned int dSec = sec1 - sec0;
			imgInfoImported[i].epoch.msecGpsTime = imgInfoImported[i - 1].epoch.msecGpsTime + static_cast<double>(dSec * 1000);

			std::vector<double> distances;
			std::vector<size_t> indices;

			if (fdTree.search(imgInfoImported[i].epoch.msecGpsTime, distances, indices, numSearch, timeThreshold))
			{
				imgInfoImported[i].epoch = fdTree.getEpoch(indices[0]);
			}
			else
			{
				std::cout << i << "th image cannot find a proper epoch." << std::endl;
			}
		}

		/// Wrong second
		if (imgInfoImported[i].epoch.sec > 60.f)
		{
			int m = static_cast<int>(imgInfoImported[i].epoch.sec / 60.f);
			float s = imgInfoImported[i].epoch.sec - 60.f * static_cast<float>(m);
			imgInfoImported[i].epoch.min += m;
			imgInfoImported[i].epoch.sec = s;
		}

		/// Wrong angle
		if (fabs(imgInfoImported[i].epoch.gimRotX) > 360.0 * deg2rad ||
			fabs(imgInfoImported[i].epoch.gimRotY) > 360.0 * deg2rad ||
			fabs(imgInfoImported[i].epoch.gimRotZ) > 360.0 * deg2rad)
		{
			continue;
		}
		else
		{
			imgInfoExport.push_back(imgInfoImported[i]);
		}
	}

	std::cout << "Done: epoch collection" << std::endl;

	///  4. Export eop
	if (!exportEop(imgFolderPath, imgInfoExport, camType))
	{
		std::cout << "Failure from exportEop" << std::endl;
		return false;
	}

	return true;
}

inline void checkEpochSec(math::geo::FDEpoch& epoch)
{
	/// Wrong second
	if (epoch.sec > 60.f)
	{
		int m = static_cast<int>(epoch.sec / 60.f);
		float s = epoch.sec - 60.f * static_cast<float>(m);
		epoch.min += m;
		epoch.sec = s;
	}
}

/// Collecting proper epochs for images
bool collectPairs(const std::string& fdPath1,
	const std::string& imgFolderPath1,
	const std::string& fdPath2,
	const std::string& imgFolderPath2,
	const std::string& imgExt,
	const double timeThreshold,
	const unsigned int camType1,
	const unsigned int camType2,
	const double headingOffset,
	const unsigned int maxLeaves)
{
	/// 1. import geotag data of camera 1 and 2
	math::geo::FDataTree fdTree1;
	auto caseno1 = math::geo::readFDFile(fdPath1, fdTree1, headingOffset, maxLeaves);
	if (-1 == caseno1)
	{
		std::cout << "Error from readFDFile" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Done: importing flight data, case #: " << caseno1 << std::endl;
	}

	math::geo::FDataTree fdTree2;
	auto caseno2 = math::geo::readFDFile(fdPath2, fdTree2, headingOffset, maxLeaves);
	if (-1 == caseno2)
	{
		std::cout << "Error from readFDFile" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Done: importing flight data, case #: " << caseno2 << std::endl;
	}

	/// 2. Import image files
	auto imgInfoImported1 = math::geo::importJpgs(imgFolderPath1, 9, imgExt);
	if (imgInfoImported1.size() == 0)
	{
		std::cout << "Image folder of camera 1 is empty" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Done: importing image files in camera 1" << std::endl;
	}

	auto imgInfoImported2 = math::geo::importJpgs(imgFolderPath2, 9, imgExt);
	if (imgInfoImported2.size() == 0)
	{
		std::cout << "Image folder of camera 2 is empty" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Done: importing image files in camera 2" << std::endl;
	}

	/// 3. Check size of data of each camera and time-sort
	if (fdTree1.size() != imgInfoImported1.size())
	{
		std::cout << "Check number of image and geotag data of Camera 1." << std::endl;
	}

	for (unsigned int i = 0; i < fdTree1.size() - 1; ++i)
	{
		if (fdTree1.getEpoch(i).msecSystemTime > fdTree1.getEpoch(i + 1).msecGpsTime)
		{
			std::cout << "First flight data time tags are not properly sorted." << std::endl;
			return false;
		}
	}

	if (fdTree2.size() != imgInfoImported2.size())
	{
		std::cout << "Check number of image and geotag data of Camera 2." << std::endl;
	}

	for (unsigned int i = 0; i < fdTree2.size() - 1; ++i)
	{
		if (fdTree2.getEpoch(i).msecSystemTime > fdTree2.getEpoch(i + 1).msecGpsTime)
		{
			std::cout << "Second flight data time tags are not properly sorted." << std::endl;
			return false;
		}
	}

	/// 4. Search pairs
	std::vector<double> distances;
	std::vector<size_t> indices;
	const unsigned int numSearch = 1;

	std::vector<math::geo::ImgInfo> imgInfoExport1;
	imgInfoExport1.reserve(imgInfoImported1.size());
	std::vector<math::geo::ImgInfo> imgInfoExport2;
	imgInfoExport2.reserve(imgInfoImported2.size());
	
	unsigned int startJ = 0;
	std::vector<std::pair<unsigned int, unsigned int>> pairs;
	pairs.reserve(fdTree1.size());

	for (unsigned int i = 0; i < fdTree1.size(); i++)
	{
		double minTimeGap = timeThreshold * 2.0;
		std::pair<unsigned int, unsigned int> pair;
		pair.first = i;
		bool findEpoch = false;
		for (unsigned int j = startJ; j < fdTree2.size(); j++)
		{
#ifdef _DEBUG
			std::cout << "t1: " << fdTree1.getEpoch(i).msecSystemTime << std::endl;
			std::cout << "t2: " << fdTree2.getEpoch(j).msecSystemTime << std::endl;
#endif
			auto timeGap = fabs(fdTree1.getEpoch(i).msecSystemTime - fdTree2.getEpoch(j).msecSystemTime);

			if (timeGap < timeThreshold)
			{
				if (minTimeGap > timeGap)
					minTimeGap = timeGap;

				pair.second = j;
				findEpoch = true;
			}
			else
			{
				if (fdTree1.getEpoch(i).msecSystemTime < fdTree2.getEpoch(j).msecSystemTime)
					break;
			}
		}

		if(findEpoch)
		{
			imgInfoImported1[i].epoch = fdTree1.getEpoch(i);
			imgInfoImported2[pair.second].epoch = fdTree2.getEpoch(pair.second);

			pairs.push_back(pair);
			startJ = pair.second + 1;

			/// Wrong second
			checkEpochSec(imgInfoImported1[i].epoch);
			checkEpochSec(imgInfoImported2[pair.second].epoch);

			imgInfoExport1.push_back(imgInfoImported1[i]);
			imgInfoExport2.push_back(imgInfoImported2[pair.second]);
		}
	}

	std::cout << "Done: search epoch pairs" << std::endl;

	///  5. Export eop
	if (!exportEop(imgFolderPath1, imgInfoExport1, camType1))
	{
		std::cout << "Failure from exportEop of camera " << camType1 << std::endl;
		return false;
	}

	if (!exportEop(imgFolderPath2, imgInfoExport2, camType2))
	{
		std::cout << "Failure from exportEop of camera " << camType2 << std::endl;
		return false;
	}

	return true;
}

/// EOP interpolation
bool eopInterpolation(const std::string& fdPath,
	const std::string& imgFolderPath,
	const std::string& imgExt,
	math::geo::FDataTree& fdTree,
	const unsigned int maxLeaves,
	const unsigned int numSearch,
	const double headingOffset,
	const double timeThreshold,
	const unsigned int camType)
{
	/// 1. Import flight data and build kdtree for gpstime tags
	auto caseno = math::geo::readFDFile(fdPath, fdTree, headingOffset, maxLeaves);
	if (-1 == caseno)
	{
		std::cout << "Error from readFDFile" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Done: importing flight data, case #: " << caseno << std::endl;
	}

	/// 2. Import image files
	auto imgInfo = math::geo::importJpgs(imgFolderPath, fdTree, 9, imgExt);
	if (imgInfo.size() == 0)
	{
		std::cout << "Image folder is empty" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Done: importing image files" << std::endl;
	}

	/// 3. Gpstime conversion

	if (caseno == 0)
	{
		auto gpsTime0 = fdTree.getEpoch(0).msecGpsTime;
		auto imageTime0 = math::geo::getDaySeconds(imgInfo[0]) * 1000;
		unsigned long int timeOffset = static_cast<unsigned long int>(gpsTime0 - imageTime0);

		for (unsigned int i = 0; i < imgInfo.size(); ++i)
		{
			math::geo::setGpsSecond(imgInfo[i], timeOffset);
		}
	}

	std::cout << "Done: converting gps time" << std::endl;

	/// 4. Interpolation
	if (fdTree.size() == imgInfo.size())
	{
		for (unsigned int i = 0; i < imgInfo.size(); i++)
		{
			imgInfo[i].epoch = fdTree.getEpoch(i);
		}
	}
	else
	{
		for (auto& img : imgInfo)
		{
			math::geo::interpolateEpoch(fdTree, img.epoch, numSearch, timeThreshold);
		}
	}

	std::cout << "Done: interpolation" << std::endl;

	///  5. Export eop
	if (!exportEop(imgFolderPath, imgInfo, camType))
	{
		std::cout << "Failure from exportEop" << std::endl;
		return false;
	}

	return true;
}

bool interpolateIns(const std::string& fdPath,
	const std::string& timePath,
	math::geo::FDataTree& fdTree,
	const unsigned int maxLeaves,
	const unsigned int numSearch,
	const double timeThreshold)
{
	/// 1. Import flight data and build kdtree for gpstime tags
	auto caseno = math::geo::readFDFile(fdPath, fdTree, 0.0, maxLeaves);
	if (-1 == caseno)
	{
		std::cout << "Error from readFDFile" << std::endl;
		return false;
	}
	else
	{
		std::cout << "Done: importing flight data, case #: " << caseno << std::endl;
	}

	/// 2. Import target time files
	std::vector<double> timeTags;
	if (!math::geo::readTimeFile(timePath, timeTags))
		return false;

	/// 3. Interpolation
	std::vector<math::geo::FDEpoch> tarEpochs;
	tarEpochs.reserve(timeTags.size());

	for (auto& t : timeTags)
	{
		math::geo::FDEpoch epoch;
		epoch.msecGpsTime = t;
		if (!math::geo::interpolateEpoch(fdTree, epoch, numSearch, timeThreshold))
			continue;
		tarEpochs.push_back(epoch);
	}

	std::cout << "Done: interpolation\t" << tarEpochs.size() << " of " << timeTags.size() << std::endl;

	///  4. write result
	{
		boost::filesystem::path full_path(timePath);
		std::string outFolder = full_path.parent_path().string();
		//std::cout << "Output folder : " << outFolder << std::endl;
		std::string outfileTitle = full_path.stem().string();
		std::string outfilename = outfileTitle + "_interpolated.csv";
		//std::cout << "Output filename : " << outfilename << std::endl;
		std::string outfilepath = outFolder + "\\" + outfilename;
		//std::cout << "Output file path : " << outfilepath << std::endl;

		std::fstream outfile;
		outfile.open(outfilepath, std::ios::out);

		if (!outfile)
		{
			std::cout << "Cannot open a file for writing:\t" << outfilepath << std::endl;
			return false;
		}

		outfile << "SystemTime_msec,Roll,Pitch,Yaw,Vn,Ve,Vd,Latitude,Longitude,Altitude,Distance_S0_mtr,Distance_S1_mtr,Distance_S2_mtr,Distance_S3_mtr,Distance_S4_mtr,GpsNum,SyncFlag,SolutionFlag,SaveFlag" << std::endl;

		outfile.setf(std::ios::fixed, std::ios::floatfield);
		outfile.precision(9);

		const double r2d = 180 / 3.14159265358979;

		for (auto& e : tarEpochs)
		{
			outfile << e.msecSystemTime << ",";
			outfile << e.rotX * r2d << ",";///roll
			outfile << e.rotY * r2d << ",";///pitch
			outfile << e.rotZ * r2d << ",";///heading
			outfile << e.vn << ",";
			outfile << e.ve << ",";
			outfile << e.vd << ",";
			outfile << e.x * r2d << ",";///lon
			outfile << e.y * r2d << ",";///lat
			outfile << e.z << ",";
			outfile << e.s0 << ",";
			outfile << e.s1 << ",";
			outfile << e.s2 << ",";
			outfile << e.s3 << ",";
			outfile << e.s4 << ",";
			outfile << e.gpsNum << ",";
			outfile << e.syncFlag << ",";
			outfile << e.solFlag << ",";
			outfile << e.saveFlag << std::endl;
		}

		outfile.close();

		std::cout << std::endl << "Exported file:\t" << outfilepath << std::endl;
	}

	return true;
}

bool exportEop(const std::string& imgFolderPath,
	const std::vector<math::geo::ImgInfo>& imgInfo,
	const unsigned int camType)
{
	///  5. Export eop
	std::string parent;
	std::string currentFolder = math::geo::splitFilename(imgFolderPath, parent);

	std::fstream eopfile;
	std::string outpath = parent + "\\" + currentFolder + ".db";
	eopfile.open(outpath, std::ios::out);

	if (!eopfile)
	{
		std::cout << "Cannot open a file for writing:\t" << outpath << std::endl;
		return false;
	}

	eopfile.setf(std::ios::fixed, std::ios::floatfield);
	eopfile.precision(9);

	eopfile << "#filename,path,camtype,lat,lon,height,h_ortho,roll,pitch,heading,gimbal_roll,gimbal_pitch,gimbal_heading,gpstime,partnames" << std::endl;

	for (auto& img : imgInfo)
	{
		eopfile << img.fineName << ",";
		eopfile << imgFolderPath << ",";
		eopfile << camType << ",";
		eopfile << img.epoch.x * rad2deg << ",";///lat
		eopfile << img.epoch.y * rad2deg << ",";///lon
		eopfile << img.epoch.z << ",";
		eopfile << 0.0 << ",";/// orthometric height
		eopfile << img.epoch.rotX * rad2deg << ",";///r
		eopfile << img.epoch.rotY * rad2deg << ",";///p
		eopfile << img.epoch.rotZ * rad2deg << ",";///h
		eopfile << img.epoch.gimRotX * rad2deg << ",";/// gimbal rotX
		eopfile << img.epoch.gimRotY * rad2deg << ",";/// gimbal rotY
		eopfile << img.epoch.gimRotZ * rad2deg << ",";/// gimbal rotZ
		//eopfile << img.YYYY << "-" << img.MM << "-" << img.DD << " " << img.hh << ":" << img.mm << ":" << img.ss << ",";
		static char s[512];
		//sprintf_s(s, 20, "%04d-%02d-%02d %02d:%02d:%02d", img.YYYY, img.MM, img.DD, img.hh, img.mm, static_cast<unsigned int>(img.ss));
		//sprintf_s(s, 20, "%04d-%02d-%02d %02d:%02d:%02d", img.epoch.year, img.epoch.month, img.epoch.day, img.epoch.hour, img.epoch.min, static_cast<unsigned int>(img.epoch.sec + 0.5));
		sprintf_s(s, 20, "%04d-%02d-%02d %02d:%02d:%02d", img.epoch.year, img.epoch.month, img.epoch.day, img.epoch.hour, img.epoch.min, static_cast<unsigned int>(img.epoch.sec));
		eopfile << s << ",";

		eopfile << "NE" << std::endl;
	}
	eopfile.close();

	std::cout << std::endl << "Exported file:\t" << outpath << std::endl;

	return true;
}