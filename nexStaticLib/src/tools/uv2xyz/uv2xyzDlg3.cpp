// uv2xyzDig3.cpp : implementation file
//

#include "pch.h"
#include "uv2xyz.h"
#include "uv2xyzDlg3.h"
#include "afxdialogex.h"

#include "./OpenGL64bit/glaux.h"
#include "./OpenGL64bit/gl.h"
#include "./OpenGL64bit/glu.h"

// Cuv2xyzDlg3 dialog

const double deg2rad = 3.14159265358979 / 180.0;

IMPLEMENT_DYNAMIC(Cuv2xyzDlg3, CDialogEx)

Cuv2xyzDlg3::Cuv2xyzDlg3(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG2, pParent)
{
	std::vector<std::string> paths;
	std::vector<std::string> metas;

	{
		paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-12-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-12-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-13-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-13-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-14-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-14-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-15-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-15-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-16-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-16-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-17-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-17-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-18-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-18-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-12-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-12-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-13-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-13-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-14-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-14-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-15-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-15-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-16-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-16-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-17-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-17-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-18-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-18-01.gxxml");

		paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-19-01.obj");
		metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-19-01.gxxml");
	}

	int subSample = 4;
	int imgW = 5456;
	int imgH = 3632;
	double o, p, k;
	int winH;
	double bore_o, bore_p, bore_k;
	double pc[3];
	float fovH;

	{
		winH = imgH / subSample;
		/// APSC (25.1×16.7 mm, 1.503:1.0) with 18mm, fov = 46.32;
		fovH = 46.32f;

		/// boresight
		bore_o = 90.0 * deg2rad;
		bore_p = 0.0 * deg2rad;
		bore_k = 0.0 * deg2rad;

		/// perspective center
		pc[0] = 348727.134;
		pc[1] = 4049759.916;
		pc[2] = 53.851;

		/// orientation angles
		o = 0.302124 * deg2rad;
		p = 0.05518472 * deg2rad;
		k = 108.298326 * deg2rad;
	}

	obj.load(paths, metas);

	initColor = false;

	/// translation
	objTrans[0] = 0.0;
	objTrans[1] = 0.0;
	objTrans[2] = 0.0;
	/// quaternion (rotation)
	objRot.w = 1.0;
	objRot.x = 0.0;
	objRot.y = 0.0;
	objRot.z = 0.0;

	/// screen capture path
	outImgPath = "viewsimCapture.bmp";

	shadeFlag = 1;
}

void Cuv2xyzDlg3::init(const int imgW, const int imgH)
{
	width = imgW;
	height = imgH;

	/// vpX, vpY: lower left corner
	/// vpW, vpH: viewport widht and height
	int vpX = 0;
	int vpY = 0;
	int vpW = width + vpX;
	int vpH = height + vpY;
	glViewport(vpX, vpY, vpW, vpH);
	/// Create window with given title
	
	this->setupGLstate();
}

Cuv2xyzDlg3::~Cuv2xyzDlg3()
{
}

void Cuv2xyzDlg3::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Cuv2xyzDlg3, CDialogEx)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
END_MESSAGE_MAP()


// Cuv2xyzDlg3 message handlers


void Cuv2xyzDlg3::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	VERIFY(::wglMakeCurrent(m_hDC, m_hRC));
	glResize(cx, cy);

	VERIFY(::wglMakeCurrent(NULL, NULL));
}


int Cuv2xyzDlg3::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	int nPixelFormat;
	m_hDC = ::GetDC(m_hWnd);

	static PIXELFORMATDESCRIPTOR pfd =
	{

		sizeof(PIXELFORMATDESCRIPTOR),
	  1,
	  PFD_DRAW_TO_WINDOW |
	  PFD_SUPPORT_OPENGL |
	  PFD_DOUBLEBUFFER,
	  PFD_TYPE_RGBA,
	  24,
	  0,0,0,0,0,0,
	  0,0,
	  0,0,0,0,0,
	  32,
	  0,
	  0,
	  PFD_MAIN_PLANE,
	  0,
	  0,0,0
	};
	nPixelFormat = ChoosePixelFormat(m_hDC, &pfd);
	VERIFY(SetPixelFormat(m_hDC, nPixelFormat, &pfd));
	m_hRC = wglCreateContext(m_hDC);
	VERIFY(wglMakeCurrent(m_hDC, m_hRC));
	wglMakeCurrent(NULL, NULL);

	return 0;
}


void Cuv2xyzDlg3::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: Add your message handler code here
	wglDeleteContext(m_hRC);
	::ReleaseDC(m_hWnd, m_hDC);
}


void Cuv2xyzDlg3::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: Add your message handler code here
					   // Do not call CDialogEx::OnPaint() for painting messages

	::wglMakeCurrent(m_hDC, m_hRC);
	//glRenderScene();
	drawObject();
	::SwapBuffers(m_hDC);
	::wglMakeCurrent(m_hDC, NULL);
}

void Cuv2xyzDlg3::glResize(int cx, int cy)
{
	GLfloat fAspect;

	if (cy == 0) cy = 1;

	glViewport(0, 0, cx, cy);

	fAspect = (GLfloat)cx / (GLfloat)cy;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0f, fAspect, 1.0f, 10000.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Cuv2xyzDlg3::glRenderScene()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColor3f(1.0f, 0.0f, 0.0f);

	glPushMatrix();

	glEnable(GL_DEPTH_TEST);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	glShadeModel(GL_FLAT);
	glLoadIdentity();

	gluLookAt(0.0f, 0.0f, 1000.0f, 0.0f, 10.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	//To draw meshes of object of something you want, Just code here
	////////////////////////////////////////////////////////////////
	glBegin(GL_TRIANGLES);
	glVertex3f(-100, 0, 0);
	glVertex3f(0, 100, 0);
	glVertex3f(100, 0, 0);
	glEnd();
	////////////////////////////////////////////////////////////////
	glPopMatrix();

	glFlush();
}

void Cuv2xyzDlg3::drawObject()
{
	if (!initColor)
	{
		vtxColors.clear();
		vtxColors.resize(normalizedVtxs.size());
		for (auto& c : vtxColors)
		{
			c.r = c.g = c.b = 0.0f;
		}
	}

	for (size_t f = 0; f < faceNormals.size(); f++)
	{
		glBegin(GL_TRIANGLES);

		if (shadeFlag == 0)
		{
			glNormal3f(float(faceNormals[f][0]), float(faceNormals[f][1]), float(faceNormals[f][2]));
		}

		const tobj::FaceVtxIdx& vtxIds = fVtxIds[f];

		for (int v = 0; v < 3; v++)
		{
			const auto& vtxN = vtxNormalV[vtxIds[v]];

			if (shadeFlag == 1)
			{
				glNormal3f(GLfloat(vtxN[0]), GLfloat(vtxN[1]), GLfloat(vtxN[2]));
			}

			if (!initColor)
			{
				//glColor3f(235 / 255.0, 180 / 255.0, 173 / 255.0);
				float max = float(fabs(vtxN[0]) > fabs(vtxN[1]) ? fabs(vtxN[0]) : fabs(vtxN[1]));
				max = max > fabs(vtxN[2]) ? max : float(fabs(vtxN[2]));
				vtxColors[vtxIds[v]].r = GLfloat(fabs(vtxN[0])) / max;
				vtxColors[vtxIds[v]].g = GLfloat(fabs(vtxN[1])) / max;
				vtxColors[vtxIds[v]].b = GLfloat(fabs(vtxN[2])) / max;
			}

			glColor3f(vtxColors[vtxIds[v]].r, vtxColors[vtxIds[v]].g, vtxColors[vtxIds[v]].b);

			const auto& p = normalizedVtxs[vtxIds[v]];
			glVertex3d(p[0], p[1], p[2]);
		}

		glEnd();
	}

	if (!initColor)
		initColor = true;
}

/// setup GL states
void Cuv2xyzDlg3::setupGLstate()
{
	GLfloat lightOneColor[] = { 1, 1, 1, 1 };
	GLfloat globalAmb[] = { .1f, .1f, .1f, 1.f };
	GLfloat light1Position[] = { 0,  0, 1, 0 };

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	//glFrontFace(GL_CW);
	glEnable(GL_DEPTH_TEST);
	//glClearColor(0, 0, 0, 1); /// black bg
	glClearColor(1.0, 1.0, 1.0, 1.0); /// white bg
	glShadeModel(GL_SMOOTH);

	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	//glEnable(GL_LIGHT3);
	//glEnable(GL_LIGHT4);
	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightOneColor);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, lightOneColor);
	//glLightfv(GL_LIGHT3, GL_DIFFUSE, lightOneColor);
	//glLightfv(GL_LIGHT4, GL_DIFFUSE, lightOneColor);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmb);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	//glColorMaterial(GL_BACK, GL_AMBIENT_AND_DIFFUSE);

	glLightfv(GL_LIGHT1, GL_POSITION, light1Position);
}