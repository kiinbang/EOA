
// uv2xyzDlg.h : header file
//

#pragma once


// Cuv2xyzDlg dialog
class Cuv2xyzDlg : public CDialogEx
{
// Construction
public:
	Cuv2xyzDlg(CWnd* pParent = nullptr);	// standard constructor
	~Cuv2xyzDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_UV2XYZ_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);

protected:
	int m_PixelFormat;
	HGLRC m_hRC;
	HDC m_hDC;
	CDC* m_pDC;

	float m_vBackground[4];
	int m_vViewport[4];
	float m_fNearFactor, m_fFarFactor;
	float m_fZoomScale;
	float m_fViewingDistance;
	float m_fPanOffsetX, m_fPanOffsetY;
	enum ProjectionMode { Perspective = 1, Ortho } m_eProjectionMode;
	bool m_bHeadlight;
	enum ManipulMode { None = 0, Trackball, Zooming, Panning } m_eManipulationMode;
	bool m_bIsTracking;
	float m_fTrackingAngle;
	float m_vAxis[3];
	float m_mxTransform[4][4];

	// Collect information about the current OpenGL implementation
	const char* m_pDriverType;
	const unsigned char* m_pVendor, * m_pRenderer, * m_pGlVersion, * m_pGlExtensions;
	const unsigned char* m_pGluVersion, * m_pGluExtensions;
	const unsigned char* m_pGlError;

	static const char* const DRIVER_TYPES[];

protected:
	void init();
	void initializeOpenGL();
	void setupPixelFormat();
	void getOpenGLExtendedInformation();
	void drawDefaultScene();
	void renderScene();
	void drawWorkspace(int gridNum, float gridSpace);
	void setupOpenGLState();
	void setupViewingFrustum();
	void setupHeadLight();
	void setupViewingTransform();
	void drawAxes(float coordLength);
	void glRenderScene();
};
