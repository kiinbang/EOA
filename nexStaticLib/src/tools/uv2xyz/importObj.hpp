#pragma once

#define GOOGLE_GLOG_DLL_DECL
#define GLOG_NO_ABBREVIATED_SEVERITIES

#include <string>
#include <vector>

#include <glog/logging.h>

#include "datastructs.h"

namespace globj
{
	class ObjImporter
	{
	public:
		ObjImporter()
		{
			initColor = false;
		}

		bool load(const std::vector<std::string>& objPaths, const std::vector <std::string>& metaPaths)
		{
			/// clear all drawing data
			fVtxIds.clear();
			faceNormals.clear();
			vtxNormalV.clear();
			vertices.clear();
			nVertices.clear();
			normalizedVtxs.clear();

			size_t nFiles = objPaths.size();
			nVertices.resize(nFiles);

			for (size_t i = 0; i < nFiles; ++i)
			{
				nVertices[i] = 0;

				std::string name(objPaths[i]);

				size_t pos = name.find_last_of(".");
				std::string type = name.substr(pos + 1);

				if (type != "obj")
				{
					LOG(INFO) << "Not obj file." << std::endl;
					continue;
				}

				std::shared_ptr<tobj::ObjLoad> obj = tobj::createObjLoad(tobj::_TObjLoad_);

				obj = tobj::createObjLoad(tobj::_TObjLoad_);
				tobj::Meta meta = tobj::readModelConfigWithBOM(metaPaths[i].c_str());
				obj->load(name.c_str(), meta);

				/// previous size of vertices
				auto oldSize = vertices.size();
				/// extend vertex vector
				vertices.reserve(oldSize + obj->numVertices());
				/// new added vertices
				std::vector<tobj::Vertex> newVertices(obj->numVertices());

				for (size_t v = 0; v < obj->numVertices(); ++v)
				{
					newVertices[v] = obj->getVertex(v);
				}

				/// save the number of vertices for each file
				nVertices[i] = obj->numVertices();
				/// insert new vertices
				vertices.insert(vertices.end(), newVertices.begin(), newVertices.end());

				/// compuate face normal vectors
				compute_normal(obj, i);
			}

			/// compute normalization parameters: center and scale(length)
			computeNormalization();

			/// normalized vertices
			normalizedVtxs.resize(vertices.size());
			for (size_t v = 0; v < vertices.size(); ++v)
			{
				normalizedVtxs[v] = getNormalizedVtx(vertices[v]);
			}

			return true;
		}

		/// get normalized vertex coordinates
		void getNormalized(const double x0, const double y0, const double z0, double& nx, double& ny, double& nz)
		{
			nx = x0 - center[0];
			ny = y0 - center[1];
			nz = z0 - center[2];
		}

		tobj::Vertex getNormalizedVtx(const tobj::Vertex& vtx)
		{
			tobj::Vertex nVtx;
			getNormalized(vtx[0], vtx[1], vtx[2], nVtx[0], nVtx[1], nVtx[2]);

			return nVtx;
		}

	private:
		/// compute normalization parameters
		void computeNormalization()
		{
			double c[3];
			c[0] = c[1] = c[2] = 0.0;

			/// initialize min and max
			for (int j = 0; j < 3; ++j)
			{
				//max[j] = std::numeric_limits<float>::min;
				//max[j] = std::numeric_limits<float>::min();
				//min[j] = std::numeric_limits<float>::max;
				auto min0 = std::numeric_limits<float>::max;
			}

			for (size_t v = 0; v < vertices.size(); ++v)
			{
				for (int j = 0; j < 3; ++j)
				{
					c[j] += vertices[v][j];

					max[j] = (max[j] < vertices[v][j]) ? static_cast<float>(vertices[v][j]) : max[j];
					min[j] = (min[j] > vertices[v][j]) ? static_cast<float>(vertices[v][j]) : min[j];
				}
			}

			for (int j = 0; j < 3; ++j)
				center[j] = static_cast<float>(c[j] / vertices.size());
		}

		/// compute face normal vectors
		void compute_normal(const std::shared_ptr<tobj::ObjLoad>& obj, const size_t fileIdx)
		{
			size_t idxOffset = 0;
			for (int i = 0; i < fileIdx; ++i)
			{
				idxOffset += nVertices[i];
			}

			/// new face normal vectors
			std::vector<tobj::Vertex> newFaceNormals;
			newFaceNormals.reserve(obj->numFaces());
			/// new vertex normal vectors
			std::vector<tobj::Vertex> newVtxNormalV(obj->numVertices());
			/// initialize new vertex normals
			for (auto& n : newVtxNormalV)
			{
				n[0] = n[1] = n[2] = 0.0;
			}

			tobj::Vertex fVtx[3];
			std::vector<tobj::FaceVtxIdx> newFVtxIds;
			newFVtxIds.reserve(obj->numFaces());

			for (size_t f = 0; f < obj->numFaces(); ++f)
			{
				auto vtxIds = obj->getVtxIdx(f);

				for (int v = 0; v < 3; ++v)
					fVtx[v] = obj->getVertex(vtxIds[v]);

				tobj::Vertex p01, p02;
				p01 = fVtx[1] - fVtx[0];
				p02 = fVtx[2] - fVtx[0];

				/// cross product
				auto crossP = p01 ^ p02;

				/// area
				auto faceAreas = sqrt(crossP[0] * crossP[0] + crossP[1] * crossP[1] + crossP[2] * crossP[2]);

				if (faceAreas < std::numeric_limits<double>::epsilon())
				{
					continue;
				}

				/// face normal
				newFaceNormals.push_back(crossP * (1.0 / faceAreas));

				/// vertex normal vectors
				tobj::FaceVtxIdx fvtxid;
				for (int v = 0; v < 3; ++v)
				{
					newVtxNormalV[vtxIds[v]] = newVtxNormalV[vtxIds[v]] + crossP;
					/// save vertex indices of a face after adding an offset (previous number of vertices)
					fvtxid[v] = int(vtxIds[v] + idxOffset);
				}

				newFVtxIds.push_back(fvtxid);
			}

			/// previous number of faces
			auto nPreFace = faceNormals.size();
			/// extend vectors
			faceNormals.reserve(nPreFace + newFaceNormals.size());
			/// insert new face normlas
			faceNormals.insert(faceNormals.end(), newFaceNormals.begin(), newFaceNormals.end());

			/// normalize new vertex normals
			for (auto& n : newVtxNormalV)
			{
				auto size = sqrt(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
				if (size < std::numeric_limits<double>::epsilon())
					continue;
				n = n * (1.0 / size);
			}

			/// previous number of vertex normals
			auto nPreVNormals = vtxNormalV.size();
			/// extend
			vtxNormalV.reserve(nPreVNormals + newVtxNormalV.size());
			/// insert new vertex normlas
			vtxNormalV.insert(vtxNormalV.end(), newVtxNormalV.begin(), newVtxNormalV.end());

			/// previous number of face vertex Ids
			auto nPreFVtxIdx = fVtxIds.size();
			/// extend
			fVtxIds.reserve(fVtxIds.size() + newFVtxIds.size());
			/// insert new face vtx indices
			fVtxIds.insert(fVtxIds.end(), newFVtxIds.begin(), newFVtxIds.end());
		}

	private:
		/// vertex ids of a face
		std::vector<tobj::FaceVtxIdx> fVtxIds;
		/// face normal vectors
		std::vector<tobj::Vertex> faceNormals;
		///	vertex normal vectors
		std::vector<tobj::Vertex> vtxNormalV;
		/// vertices
		std::vector<tobj::Vertex> vertices;
		/// vertex colors
		std::vector<globj::VC> vtxColors;
		bool initColor;
		/// number of vertices of each file
		std::vector<size_t> nVertices;
		///	normalized vertices
		std::vector<tobj::Vertex> normalizedVtxs;
		/// min and max
		float min[3], max[3];
		/// obj space center
		float center[3];
	};
}
