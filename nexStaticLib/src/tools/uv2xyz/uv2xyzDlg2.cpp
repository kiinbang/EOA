// uv2xyzDlg2.cpp : implementation file
//

#include "pch.h"
#include "uv2xyz.h"
#include "uv2xyzDlg2.h"
#include "afxdialogex.h"

#include "./OpenGL/glaux.h"
#include "./OpenGL/gl.h"
#include "./OpenGL/glu.h"

// Cuv2xyzDlg2 dialog

IMPLEMENT_DYNAMIC(Cuv2xyzDlg2, CDialogEx)

Cuv2xyzDlg2::Cuv2xyzDlg2(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
{
	/*
	std::vector<std::string> paths;
	std::vector<std::string> metas;
	paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-12-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-12-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-13-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-13-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-14-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-14-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-15-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-15-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-16-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-16-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-17-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-17-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-P-18-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-P-18-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-12-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-12-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-13-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-13-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-14-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-14-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-15-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-15-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-16-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-16-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-17-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-17-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-18-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-18-01.gxxml");

	paths.push_back("../etc/Model/미호천하행/BR-D-00008-S-19-01.obj");
	metas.push_back("../etc/Model/미호천하행/BR-D-00008-S-19-01.gxxml");

	obj.load(paths, metas);
	*/
}

Cuv2xyzDlg2::~Cuv2xyzDlg2()
{
}

void Cuv2xyzDlg2::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Cuv2xyzDlg2, CDialogEx)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
END_MESSAGE_MAP()


// Cuv2xyzDlg2 message handlers


void Cuv2xyzDlg2::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	VERIFY(wglMakeCurrent(m_hDC, m_hRC));
	GLResize(cx, cy);

	VERIFY(wglMakeCurrent(NULL, NULL));
}

void Cuv2xyzDlg2::GLResize(int cx, int cy)
{
	GLfloat fAspect;

	if (cy == 0) cy = 1;

	glViewport(0, 0, cx, cy);

	fAspect = (GLfloat)cx / (GLfloat)cy;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0f, fAspect, 1.0f, 10000.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int Cuv2xyzDlg2::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here
	int nPixelFormat;
	m_hDC = ::GetDC(m_hWnd);

	static PIXELFORMATDESCRIPTOR pfd =
	{
	 
		sizeof(PIXELFORMATDESCRIPTOR),
	  1,
	  PFD_DRAW_TO_WINDOW |
	  PFD_SUPPORT_OPENGL |
	  PFD_DOUBLEBUFFER,
	  PFD_TYPE_RGBA,
	  24,
	  0,0,0,0,0,0,
	  0,0,
	  0,0,0,0,0,
	  32,
	  0,
	  0,
	  PFD_MAIN_PLANE,
	  0,
	  0,0,0
	};
	nPixelFormat = ChoosePixelFormat(m_hDC, &pfd);
	VERIFY(SetPixelFormat(m_hDC, nPixelFormat, &pfd));
	m_hRC = wglCreateContext(m_hDC);
	VERIFY(wglMakeCurrent(m_hDC, m_hRC));
	wglMakeCurrent(NULL, NULL);

	return 0;
}


void Cuv2xyzDlg2::OnDestroy()
{
	wglDeleteContext(m_hRC);
	::ReleaseDC(m_hWnd, m_hDC);

	CDialogEx::OnDestroy();
}

void Cuv2xyzDlg2::GLRenderScene()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColor3f(1.0f, 0.0f, 0.0f);

	glPushMatrix();

	glEnable(GL_DEPTH_TEST);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	glShadeModel(GL_FLAT);
	glLoadIdentity();

	gluLookAt(0.0f, 0.0f, 1000.0f, 0.0f, 10.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	//To draw meshes of object of something you want, Just code here
	////////////////////////////////////////////////////////////////
	glBegin(GL_TRIANGLES);
	glVertex3f(-100, 0, 0);
	glVertex3f(0, 100, 0);
	glVertex3f(100, 0, 0);
	glEnd();
	////////////////////////////////////////////////////////////////
	glPopMatrix();

	glFlush();
}

void Cuv2xyzDlg2::OnPaint()
{
	CDialogEx::OnPaint();

	wglMakeCurrent(m_hDC, m_hRC);
	GLRenderScene();
	SwapBuffers(m_hDC);
	wglMakeCurrent(m_hDC, NULL);
}
