#pragma once

#include "importObj.hpp"

// Cuv2xyzDlg2 dialog

class Cuv2xyzDlg2 : public CDialogEx
{
	DECLARE_DYNAMIC(Cuv2xyzDlg2)

public:
	Cuv2xyzDlg2(CWnd* pParent = nullptr);   // standard constructor
	virtual ~Cuv2xyzDlg2();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

private:
	HGLRC m_hRC;
	HDC m_hDC;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);

private:
	void GLResize(int cx, int cy);
	void GLRenderScene();
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();

private:
	globj::ObjImporter obj;
};
