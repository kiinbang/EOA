
// uv2xyzDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "uv2xyz.h"
#include "uv2xyzDlg.h"
#include "afxdialogex.h"

//#include "./OpenGL64bit/GLAUX.H"
#include "./OpenGL/glaux.h"
#include "./OpenGL/gl.h"
#include "./OpenGL/glu.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define INSTALLABLE_DRIVER_TYPE_MASK  (PFD_GENERIC_ACCELERATED|PFD_GENERIC_FORMAT)

#define PI 3.14159265358979
#define SCENE_SCALE 5000.0

// Cuv2xyzDlg dialog

const char* const Cuv2xyzDlg::DRIVER_TYPES[] =
{
	{"Generic Driver"},
	{"Mini Client Driver"},
	{"Installable Client Driver"}
};

Cuv2xyzDlg::Cuv2xyzDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_UV2XYZ_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

Cuv2xyzDlg::~Cuv2xyzDlg()
{
	CDialogEx::~CDialogEx();
	::wglDeleteContext(m_hRC);

	//	For Color-Index mode, reset the palette to the original here
	if (m_pDC)
	{
		delete m_pDC;
	}
}

void Cuv2xyzDlg::init()
{
	m_fPanOffsetX = 0.0f;
	m_fPanOffsetY = 0.0f;
	m_vBackground[0] = m_vBackground[1] = m_vBackground[2] = 0.0f;
	m_fNearFactor = 0.1f;
	m_fFarFactor = 10.0f;
	m_fViewingDistance = SCENE_SCALE * 2.0f;
	m_fZoomScale = m_fViewingDistance * m_fNearFactor;
	m_bHeadlight = true;
	m_eManipulationMode = None;
	m_bIsTracking = false;
	m_fTrackingAngle = 0.0f;
	// default view transform
	m_mxTransform[0][0] = 0.544677f;	m_mxTransform[0][1] = -0.362249f;	m_mxTransform[0][2] = 0.756375f;	m_mxTransform[0][3] = 0.f;
	m_mxTransform[1][0] = 0.838644f;	m_mxTransform[1][1] = 0.233635f;	m_mxTransform[1][2] = -0.492026f;	m_mxTransform[1][3] = 0.f;
	m_mxTransform[2][0] = 0.001520f;	m_mxTransform[2][1] = 0.902324f;	m_mxTransform[2][2] = 0.431054f;	m_mxTransform[2][3] = 0.f;
	m_mxTransform[3][0] = 0.f;			m_mxTransform[3][1] = 0.f;			m_mxTransform[3][2] = 0.f;			m_mxTransform[3][3] = 1.f;

	/*
	m_fTrackingAngle = 0.0f;
	m_vPrevVec[0] = m_vPrevVec[1] = m_vPrevVec[2] = 0.f;
	m_vCurVec[0] = m_vCurVec[1] = m_vCurVec[2] = 0.f;
	m_bIsTracking = false;
	m_bHeadlight = true;
	m_eManipulationMode = None;
	m_eShadeMode = Shading;
	m_eProjectionMode = Ortho;
	//m_eProjectionMode = Perspective;

	// Editables
	
	m_fFovy = 30.0f;
	m_fNearFactor = 0.1f;
	m_fFarFactor = 10.0f;

	m_fZoomScale = m_fViewingDistance * m_fNearFactor
		* (float)tan(m_fFovy * PI / 360.0);
	m_fPanOffsetX = 0.0f;
	m_fPanOffsetY = 0.0f;

	//m_vBackground[0] = m_vBackground[1] = m_vBackground[2] = 0.792f;
	//m_vBackground[3] = 1.0f;

	BG_Bright = 0.0f;
	m_vBackground[0] = m_vBackground[1] = m_vBackground[2] = BG_Bright;

	m_vBackground[0] = 0.0f;
	m_vBackground[1] = 0.0f;
	m_vBackground[2] = 0.0f;

	m_vBackground[3] = 0.7f;

	// default view transform
	m_mxTransform[0][0] = 0.544677f;	m_mxTransform[0][1] = -0.362249f;	m_mxTransform[0][2] = 0.756375f;	m_mxTransform[0][3] = 0.f;
	m_mxTransform[1][0] = 0.838644f;	m_mxTransform[1][1] = 0.233635f;	m_mxTransform[1][2] = -0.492026f;	m_mxTransform[1][3] = 0.f;
	m_mxTransform[2][0] = 0.001520f;	m_mxTransform[2][1] = 0.902324f;	m_mxTransform[2][2] = 0.431054f;	m_mxTransform[2][3] = 0.f;
	m_mxTransform[3][0] = 0.f;			m_mxTransform[3][1] = 0.f;			m_mxTransform[3][2] = 0.f;			m_mxTransform[3][3] = 1.f;

	m_nDefault3DFontID = 0;
	m_nDefaultBitmapFontID = 0;
	m_fDefaultTextExtrusion = 0.3f;

	m_bSelectionDone = false;

	bTrainingPixels = false;

	bDrawbox = false;

	m_Kappa = 0;//Kappa(-3600 ~ 3600) around Z axis (for ZX-plane display : profile display)

	bCollectedMode = false;

	rotCX = rotCY = rotCZ = 0.0;
	*/
}

void Cuv2xyzDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(Cuv2xyzDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &Cuv2xyzDlg::OnBnClickedOk)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// Cuv2xyzDlg message handlers

BOOL Cuv2xyzDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	initializeOpenGL();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Cuv2xyzDlg::initializeOpenGL()
{
	m_pDC = new CClientDC(this);
	m_hDC = m_pDC->GetSafeHdc();

	setupPixelFormat();
	m_hRC = ::wglCreateContext(m_hDC);

	::wglMakeCurrent(m_hDC, m_hRC);
	getOpenGLExtendedInformation();
	::wglMakeCurrent(NULL, NULL);

	return;
}

void Cuv2xyzDlg::setupPixelFormat()
{
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),  // size of this pfd
		1,                              // version number
		PFD_DRAW_TO_WINDOW |            // support window
		PFD_SUPPORT_OPENGL |            // support OpenGL
		PFD_DOUBLEBUFFER,               // double buffered
		PFD_TYPE_RGBA,                  // RGBA type
		24,                             // 24-bit color depth
		0, 0, 0, 0, 0, 0,               // color bits ignored
		0,                              // no alpha buffer
		0,                              // shift bit ignored
		0,                              // no accumulation buffer
		0, 0, 0, 0,                     // accum bits ignored
		32,                             // 32-bit z-buffer
		0,                              // no stencil buffer
		0,                              // no auxiliary buffer
		PFD_MAIN_PLANE,                 // main layer
		0,                              // reserved
		0, 0, 0                         // layer masks ignored
	};

	m_PixelFormat = ::ChoosePixelFormat(m_hDC, &pfd);
	::SetPixelFormat(m_hDC, m_PixelFormat, &pfd);

	return;
}

// GetOpenGLExtendedInformation
// Get information about the particular pixel format that's running. 
void Cuv2xyzDlg::getOpenGLExtendedInformation()
{
	// get driver type
	PIXELFORMATDESCRIPTOR pfd;
	::DescribePixelFormat(m_hDC, m_PixelFormat,
		sizeof(PIXELFORMATDESCRIPTOR), &pfd);
	if ((INSTALLABLE_DRIVER_TYPE_MASK & pfd.dwFlags) == 0)
		m_pDriverType = DRIVER_TYPES[2]; // InstallableClient - fully in hardware (fastest)
	else if (INSTALLABLE_DRIVER_TYPE_MASK == (INSTALLABLE_DRIVER_TYPE_MASK & pfd.dwFlags))
		m_pDriverType = DRIVER_TYPES[1]; // MiniClient - partially in hardware (pretty fast, maybe..)
	else  // plain old generic
		m_pDriverType = DRIVER_TYPES[0]; // Generic - software

	// Get general information about the OpenGL driver
	m_pVendor = ::glGetString(GL_VENDOR);
	m_pRenderer = ::glGetString(GL_RENDERER);
	m_pGlVersion = ::glGetString(GL_VERSION);
	m_pGlExtensions = ::glGetString(GL_EXTENSIONS);

	m_pGluVersion = ::gluGetString(GLU_VERSION);
	m_pGluExtensions = ::gluGetString(GLU_EXTENSIONS);

	GLenum glError;
	for (int i = 0; i < 6; i++)
	{
		glError = ::glGetError();
		m_pGlError = ::gluErrorString(glError);
		if (glError != GL_NO_ERROR)
			break;
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Cuv2xyzDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

		::wglMakeCurrent(m_hDC, m_hRC);

		setupOpenGLState();
		::glMatrixMode(GL_PROJECTION);
		::glLoadIdentity();
		setupViewingFrustum();

		::glMatrixMode(GL_MODELVIEW);
		::glLoadIdentity();
		setupHeadLight();
		setupViewingTransform();

		glRenderScene();

		::glPushMatrix();
	}	
}

void Cuv2xyzDlg::setupOpenGLState()
{
	::glClearColor(m_vBackground[0], m_vBackground[1], m_vBackground[2], m_vBackground[3]);
	::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	::glShadeModel(GL_SMOOTH);
	::glEnable(GL_DEPTH_TEST);
	::glEnable(GL_AUTO_NORMAL);

	::glFrontFace(GL_CCW);
	::glDisable(GL_CULL_FACE);
	::glCullFace(GL_BACK);

	::glEnable(GL_LIGHTING);
	::glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
	return;
}

void Cuv2xyzDlg::setupViewingFrustum()
{
	double aspectRatio = (double)m_vViewport[3] / (double)m_vViewport[2];
	float nearPlane = m_fNearFactor * m_fViewingDistance;
	float farPlane = m_fFarFactor * m_fViewingDistance;

	float left = -(float)(m_fZoomScale / aspectRatio) + m_fPanOffsetX;
	float right = (float)(m_fZoomScale / aspectRatio) + m_fPanOffsetX;
	float bottom = -m_fZoomScale + m_fPanOffsetY;
	float top = m_fZoomScale + m_fPanOffsetY;

	if (m_eProjectionMode == Perspective)
	{
		::glFrustum(left, right, bottom, top, nearPlane, farPlane);
	}
	else if (m_eProjectionMode == Ortho)
	{
		::glOrtho(left, right, bottom, top, nearPlane, farPlane);
	}

	return;
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Cuv2xyzDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void Cuv2xyzDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}

void Cuv2xyzDlg::renderScene()
{
	//////////////////////////////////////
	// TODO: Your Rendering Code is here !
	{
		drawDefaultScene();
		drawWorkspace(20, SCENE_SCALE / 20.0f);
		//DrawAxes(SCENE_SCALE / 10.0f);
	}


	// End here!
	//////////////////////////////////////
	return;
}

void Cuv2xyzDlg::drawDefaultScene()
{
	::glPushMatrix();

	float ambient[4];
	ambient[0] = 0.4275f;
	ambient[1] = 0.6745f;
	ambient[2] = 0.9216f;
	ambient[3] = 1.0f;

	float diffuse[4];
	diffuse[0] = 0.4275f;
	diffuse[1] = 0.6745f;
	diffuse[2] = 0.9216f;
	diffuse[3] = 1.0f;

	float specular[4];
	specular[0] = 0.2f;
	specular[1] = 0.1f;
	specular[2] = 0.1f;
	specular[3] = 1.0f;

	float emission[4];
	emission[0] = 0.0f;
	emission[1] = 0.0f;
	emission[2] = 0.0f;
	emission[3] = 1.0f;

	float shininess = 0.2f;
	::glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
	::glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
	::glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
	::glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emission);
	::glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
	GLUquadricObj* obj;
	obj = ::gluNewQuadric();
	::gluCylinder(obj, SCENE_SCALE / 4.0, 0.0, SCENE_SCALE / 3.0, 20, 20);
	::gluDeleteQuadric(obj);
	::glPopMatrix();

}

void Cuv2xyzDlg::drawWorkspace(int gridNum, float gridSpace)
{
	float gridColor[4];
	gridColor[0] = 0.6039f;
	gridColor[1] = 0.6039f;
	gridColor[2] = 0.6039f;
	gridColor[3] = 1.0f;

	int halfNum = gridNum / 2;
	float width = gridSpace * halfNum;

	GLboolean oldLight = ::glIsEnabled(GL_LIGHTING);
	::glDisable(GL_LIGHTING);

	::glColor3fv(gridColor);
	::glBegin(GL_LINES);
	for (int i = -halfNum; i <= halfNum; i++)
	{
		::glVertex3f(-width, i * gridSpace, 0.0f);
		::glVertex3f(width, i * gridSpace, 0.0f);
		::glVertex3f(i * gridSpace, -width, 0.0f);
		::glVertex3f(i * gridSpace, width, 0.0f);
	}
	::glEnd();

	if (oldLight)
		::glEnable(GL_LIGHTING);
}

void Cuv2xyzDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	m_vViewport[2] = cx;
	m_vViewport[3] = cy;

	::wglMakeCurrent(m_hDC, m_hRC);
	::glViewport(0, 0, m_vViewport[2], m_vViewport[3]);
	::wglMakeCurrent(NULL, NULL);
}

void Cuv2xyzDlg::setupHeadLight()
{
	if (!m_bHeadlight)
	{
		glDisable(GL_LIGHT7);
		return;
	}

	GLfloat  ambientLight[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat  diffuseLight[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat  specularLight[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat	 lightPos[] = { 0.0f, 0.0f, m_fViewingDistance };

	glLightfv(GL_LIGHT7, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT7, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT7, GL_SPECULAR, specularLight);
	glLightfv(GL_LIGHT7, GL_POSITION, lightPos);
	glEnable(GL_LIGHT7);

	return;
}

void Cuv2xyzDlg::setupViewingTransform()
{
	::glTranslatef(0.0f, 0.0f, -m_fViewingDistance);

	if (m_eManipulationMode == Trackball && m_bIsTracking == true)
	{
		::glPushMatrix();
		::glLoadIdentity();
		::glRotatef(m_fTrackingAngle, m_vAxis[0], m_vAxis[1], m_vAxis[2]);
		::glMultMatrixf((GLfloat*)m_mxTransform);
		::glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat*)m_mxTransform);
		::glPopMatrix();
	}
	::glMultMatrixf((GLfloat*)m_mxTransform);

	return;
}

void Cuv2xyzDlg::drawAxes(float coordLength)
{
	coordLength *= m_fZoomScale;
	GLboolean oldDepth = ::glIsEnabled(GL_DEPTH_TEST);
	glDisable(GL_DEPTH_TEST);
	GLboolean oldLight = ::glIsEnabled(GL_LIGHTING);
	::glDisable(GL_LIGHTING);

	//::glColor3f(1.0f, 1.0f, 1.0f);
	::glBegin(GL_LINES);

	float c_x = 0.0f;
	float c_y = 0.0f;
	float c_z = 0.0f;

	// X axis
	::glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(c_x, c_y, c_z);
	glVertex3f(c_x + coordLength, c_y, c_z);

	// X arrow
	glVertex3f(c_x + 0.9f * coordLength, c_y + 0.1f * coordLength, c_z + 0.0f);
	glVertex3f(c_x + 0.9f * coordLength, c_y - 0.1f * coordLength, c_z + 0.0f);

	glVertex3f(c_x + 0.9f * coordLength, c_y - 0.1f * coordLength, c_z + 0.0f);
	glVertex3f(c_x + coordLength, c_y, c_z);

	glVertex3f(c_x + coordLength, c_y, c_z);
	glVertex3f(c_x + 0.9f * coordLength, c_y + 0.1f * coordLength, c_z + 0.0f);

	//X  label

	glVertex3f(c_x + 1.1f * coordLength, c_y + 0.1f * coordLength, c_z + 0.0f);
	glVertex3f(c_x + 1.3f * coordLength, c_y - 0.1f * coordLength, c_z + 0.0f);

	glVertex3f(c_x + 1.3f * coordLength, c_y + 0.1f * coordLength, c_z + 0.0f);
	glVertex3f(c_x + 1.1f * coordLength, c_y - 0.1f * coordLength, c_z + 0.0f);

	// Y axis
	::glColor3f(0.0f, 1.0f, 0.0f);

	glVertex3f(c_x, c_y, c_z);
	glVertex3f(c_x, c_y + coordLength, c_z);

	// Y arrow
	glVertex3f(c_x - 0.1f * coordLength, c_y + 0.9f * coordLength, c_z + 0.0f);
	glVertex3f(c_x + 0.1f * coordLength, c_y + 0.9f * coordLength, c_z + 0.0f);

	glVertex3f(c_x + 0.1f * coordLength, c_y + 0.9f * coordLength, c_z + 0.0f);
	glVertex3f(c_x, c_y + coordLength, c_z);

	glVertex3f(c_x, c_y + coordLength, c_z);
	glVertex3f(c_x - 0.1f * coordLength, c_y + 0.9f * coordLength, c_z + 0.0f);

	//Y  label
	glVertex3f(c_x - 0.1f * coordLength, c_y + 1.3f * coordLength, c_z);
	glVertex3f(c_x, c_y + 1.2f * coordLength, c_z);

	glVertex3f(c_x + 0.1f * coordLength, c_y + 1.3f * coordLength, c_z);
	glVertex3f(c_x, c_y + 1.2f * coordLength, c_z);

	glVertex3f(c_x, c_y + 1.2f * coordLength, c_z);
	glVertex3f(c_x, c_y + 1.1f * coordLength, c_z);

	// Z axis
	::glColor3f(0.0f, 0.0f, 1.0f);

	glVertex3f(c_x, c_y, c_z);
	glVertex3f(c_x, c_y, c_z + coordLength);

	// Z arrow
	glVertex3f(c_x - 0.1f * coordLength, c_y, c_z + 0.9f * coordLength);
	glVertex3f(c_x + 0.1f * coordLength, c_y, c_z + 0.9f * coordLength);

	glVertex3f(c_x + 0.1f * coordLength, c_y, c_z + 0.9f * coordLength);
	glVertex3f(c_x, c_y, c_z + coordLength);

	glVertex3f(c_x, c_y, c_z + coordLength);
	glVertex3f(c_x - 0.1f * coordLength, c_y, c_z + 0.9f * coordLength);

	// Z label
	glVertex3f(c_x - 0.1f * coordLength, c_y, c_z + 1.2f * coordLength);
	glVertex3f(c_x + 0.1f * coordLength, c_y, c_z + 1.2f * coordLength);

	glVertex3f(c_x + 0.1f * coordLength, c_y, c_z + 1.2f * coordLength);
	glVertex3f(c_x - 0.1f * coordLength, c_y, c_z + 1.1f * coordLength);

	glVertex3f(c_x - 0.1f * coordLength, c_y, c_z + 1.1f * coordLength);
	glVertex3f(c_x + 0.1f * coordLength, c_y, c_z + 1.1f * coordLength);

	glEnd();

	// connecting lines
	::glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_LOOP);
	glVertex3f(c_x + 0.4f * coordLength, c_y, c_z);
	glVertex3f(c_x, c_y + 0.4f * coordLength, c_z);
	glVertex3f(c_x, c_y, c_z + 0.4f * coordLength);
	glEnd();

	if (oldDepth)
		glEnable(GL_DEPTH_TEST);
	if (oldLight)
		glEnable(GL_LIGHTING);
}

void Cuv2xyzDlg::glRenderScene()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glColor3f(1.0f, 0.0f, 0.0f);

	glPushMatrix();

	glEnable(GL_DEPTH_TEST);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	glShadeModel(GL_FLAT);
	glLoadIdentity();

	gluLookAt(0.0f, 0.0f, 1000.0f, 0.0f, 10.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	//To draw meshes of object of something you want, Just code here
	////////////////////////////////////////////////////////////////
	glBegin(GL_TRIANGLES);
	glVertex3f(-100, 0, 0);
	glVertex3f(0, 100, 0);
	glVertex3f(100, 0, 0);
	glEnd();
	////////////////////////////////////////////////////////////////
	glPopMatrix();

	glFlush();
}