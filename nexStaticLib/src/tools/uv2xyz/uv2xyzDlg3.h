#pragma once

#include "importObj.hpp"

// Cuv2xyzDlg3 dialog

class Cuv2xyzDlg3 : public CDialogEx
{
	DECLARE_DYNAMIC(Cuv2xyzDlg3)

public:
	Cuv2xyzDlg3(CWnd* pParent = nullptr);   // standard constructor
	virtual ~Cuv2xyzDlg3();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG2 };
#endif

	void glResize(int cx, int cy);
	void glRenderScene();
	void drawObject();
	void init(const int imgW, const int imgH);
	/// setup GL states
	void setupGLstate();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	globj::ObjImporter obj;

public:
	HGLRC m_hRC;
	HDC m_hDC;
	
	/// window width and height
	int width, height;
	/// shade flag
	int shadeFlag;
	/// vertex ids of a face
	std::vector<tobj::FaceVtxIdx> fVtxIds;
	/// face normal vectors
	std::vector<tobj::Vertex> faceNormals;
	///	vertex normal vectors
	std::vector<tobj::Vertex> vtxNormalV;
	/// vertices
	std::vector<tobj::Vertex> vertices;
	/// vertex colors
	std::vector<globj::VC> vtxColors;
	bool initColor;
	/// number of vertices of each file
	std::vector<size_t> nVertices;
	///	normalized vertices
	std::vector<tobj::Vertex> normalizedVtxs;
	/// shift, center of vertices
	tobj::Vertex center;
	/// translation
	globj::Pos3D objTrans;
	/// quaternion
	SSMATICS_EOA::CSMQuaternion objRot;
	/// screencapture image path
	std::string outImgPath;

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
};
