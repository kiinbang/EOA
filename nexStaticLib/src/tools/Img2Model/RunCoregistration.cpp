/*
* Copyright (c) 2020-2022, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#define GOOGLE_GLOG_DLL_DECL
#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <glog/logging.h>

#include <iostream>

#include <ImageSupporter.h>
#include <ObjRender.h>
#include <ssm/include/utilitygrocery.h>

#include "LineMatch.h"
#include "spr.h"

using LineSegment = std::shared_ptr<edge::LineSegmentPtr>;

//const double deg2rad = 3.14159265358979 / 180.0;

const int subSample = 4; /// <-- max size should be adjusted or use Frame buffer
const double resizeRatio = 1.0/ subSample;
const unsigned int blurMaskSize = 9;
const unsigned int cannyLowTh = 20;/// adjacent edge link
const unsigned int cannyHighTh = 80;/// edge threshold
//const double cannyLowHighRatio = double(cannyHighTh) / double(cannyLowTh);
const unsigned int cannyMaskSize = 3;

LineSegment imgLinesSegment = edge::getLineSegmentPtr();
LineSegment mdlLinesSegment = edge::getLineSegmentPtr();
const unsigned int waitTime = 1;// edge::infinitWait;
bool showResult = false;

/// camera specifications
const int imgW = 5456;
const int imgH = 3632;
//int winH = imgH / subSample;
/// APSC (25.1×16.7 mm, 1.503:1.0) with 18mm, fov = 46.32;
//float fovHDeg = 46.32f;
const float fovHDeg = float(45.92798521);
const double pixelPitch = 0.0042; /// mm
const double fl = 18.0;// mm;

/// boresight
double bore_o(90.0 * deg2rad);
double bore_p(0.0 * deg2rad);
double bore_k(0.0 * deg2rad);

/// for line primitive dispersion
const double frS = 1.0e+9; /// prj config: max sigma threshold
const double fxS = 1.0e-9; /// prj config: min sigma threshold
const double maxS = 1.0e12; /// for a line-constraint
const double minSChange = 1.0e-6; /// prj config: min sigma variation
const double colS = 1.0;
const double rowS = 1.0;
const double xS = 0.3;
const double yS = 0.3;
const double zS = 0.3;
const double minCorrelation = 0.9;

/// for image
double pc[3] = { 348713.8, 4049795.92, 53.58821559 };
//double pc[3] = { 348713.8, 4049795.92, 53.28821559 };

double o = -0.1943272 * deg2rad;
double p = -0.2761481 * deg2rad;
double k = -250.1265522 * deg2rad;


int main(int argc, char* argv[])
{
	char path[_MAX_PATH];
	const char* varName = "glogfolder";
	size_t len;
	getenv_s(&len, path, 80, varName);

	// Initialize Google's logging library.
	std::string argv0(argv[0]);
	std::string programName(argv0);
	google::InitGoogleLogging(programName.c_str());
	FLAGS_log_dir = path;

#ifdef _DEBUG
	if (false)
	{
		double x0 = 0.0, y0 = 0.0, z0 = 0.0;
		double x1 = 2.0, y1 = 1.0, z1 = 1.0;
		double sx = 0.3, sy = 0.3, sz = 0.3;
		auto disp2 = spr::getLineDispersion(x0, y0, x1, y1, sx, sy, maxS);
		auto disp3 = spr::getLineDispersion(x0, y0, z0, x1, y1, z1, sx, sy, sz, maxS);
	}
#endif

	if (false)
	{
		if (imgutil::rgb2gray("DSC00018.JPG", "DSC00018_Gray.JPG"))
			return 0;
		else
			return 1;
	}

	std::wstring srcImgName = L"DSC00018.JPG";
	std::string simPhoName = "DSC00018_Model.bmp";

	std::string outLineRecordName = "DSC00018_Line.txt";
	std::string outMdlLineRecordName = "DSC00018_Model_Line.txt";

	/// Extract lines from an image
	if(true)
	{
		std::wstring outEdgeName = L"DSC00018_Edge.JPG";
		std::string outLineName = "DSC00018_Line.JPG";
		
		unsigned int cannyLowTh = 20;/// adjacent edge link
		unsigned int cannyHighTh = 80;/// edge threshold
		if (!imgLinesSegment->extractLines(srcImgName, outEdgeName, resizeRatio, waitTime, blurMaskSize, cannyLowTh, cannyHighTh, cannyMaskSize, showResult))
			return 1;

		std::cout << imgLinesSegment->size() << " lines were found in an image.\n";

		std::vector<geo::Line2D> foundImgLines(imgLinesSegment->size());

		for (size_t i = 0; i < imgLinesSegment->size(); ++i)
		{
			foundImgLines[i] = imgLinesSegment->operator[](i);
		}

		image::draw2DLines(util::uni2multi(outEdgeName), outLineName, foundImgLines, 255, 0, 0);

		std::fstream outfile;
		outfile.open(outLineRecordName, ios::out);
		outfile.setf(ios::fixed, ios::floatfield);
		outfile.precision(3);

		for (const auto& l : foundImgLines)
		{
			outfile << l.s.x << "\t";
			outfile << l.s.y << "\t";
			outfile << l.e.x << "\t";
			outfile << l.e.y << std::endl;
		}

		outfile.close();
	}

	///  Extract lines from a model image
	if (true)
	{
		std::wstring outMdlEdgeName = L"DSC00018_Model_Edge.JPG";
		std::string outMdlLineName = "DSC00018_Model_Line.JPG";

		std::vector<std::string> paths;
		std::vector<std::string> metas;
		paths.push_back("../testData/Model/미호천하행/BR-D-00008-P-12-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-P-12-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-P-13-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-P-13-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-P-14-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-P-14-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-P-15-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-P-15-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-P-16-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-P-16-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-P-17-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-P-17-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-P-18-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-P-18-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-S-12-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-S-12-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-S-13-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-S-13-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-S-14-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-S-14-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-S-15-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-S-15-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-S-16-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-S-16-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-S-17-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-S-17-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-S-18-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-S-18-01.gxxml");

		paths.push_back("../testData/Model/미호천하행/BR-D-00008-S-19-01.obj");
		metas.push_back("../testData/Model/미호천하행/BR-D-00008-S-19-01.gxxml");

		objrend::loadObj(paths, metas);

		/// save sim-pho
		LOG(INFO) << "Generate a simulated photo from a model";
		objrend::simulatePhoto(imgW, imgH, fovHDeg,
			o, p, k, pc[0], pc[1], pc[2],
			//bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), simPhoName,
			bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, resizeRatio, simPhoName,
			false);

		unsigned int cannyLowTh = 50;/// adjacent edge link
		unsigned int cannyHighTh = 200;/// edge threshold
		LOG(INFO) << "Line segment extraction";
		if (!mdlLinesSegment->extractLines(util::multi2uni(simPhoName), outMdlEdgeName, 1.0, waitTime, blurMaskSize, cannyLowTh, cannyHighTh, cannyMaskSize, showResult))
			return 1;

		std::cout << mdlLinesSegment->size() << " lines were found in a model image.\n";

		std::vector<geo::Line2D> foundMdlLines(mdlLinesSegment->size());

		for (size_t i = 0; i < mdlLinesSegment->size(); ++i)
		{
			foundMdlLines[i] = mdlLinesSegment->operator[](i);
		}

		/// check found lines from a model
		linematch::checkMdlAvailability(foundMdlLines);

		if (!image::draw2DLines(util::uni2multi(outMdlEdgeName), outMdlLineName, foundMdlLines, 255, 0, 0))
		{
			return 1;
		}

		std::fstream outfile;
		outfile.open(outMdlLineRecordName, ios::out);
		outfile.setf(ios::fixed, ios::floatfield);
		outfile.precision(3);

		for (const auto& l : foundMdlLines)
		{
			outfile << l.s.x << "\t";
			outfile << l.s.y << "\t";
			outfile << l.e.x << "\t";
			outfile << l.e.y << std::endl;
		}

		outfile.close();
	}

	/// Line-match
	if (true)
	{
		std::fstream infile;
		infile.open(outLineRecordName, std::ios::in);
		if (!infile)
			return 1;

		geo::Line2D l;

		std::vector<geo::Line2D> foundImgLines;
		std::cerr << outLineRecordName << ": \n";
		while (infile >> l.s.x >> l.s.y >> l.e.x >> l.e.y >> std::skipws)
		{
			std::cerr << l.s.x << "\t" << l.s.y << "\t" << l.e.x << "\t" << l.e.y << std::endl;
			foundImgLines.push_back(l);
		}

		infile.close();

		std::vector<geo::Line2D> foundMdlLines;
		infile.open(outMdlLineRecordName, std::ios::in);
		if (!infile)
			return 1;

		std::cerr << outMdlLineRecordName << ": \n";
		while (infile >> l.s.x >> l.s.y >> l.e.x >> l.e.y >> std::skipws)
		{
			std::cerr << l.s.x << "\t" << l.s.y << "\t" << l.e.x << "\t" << l.e.y << std::endl;
			foundMdlLines.push_back(l);
		}

		infile.close();

		/// prepare CameraData
		const double fVariance = fxS * fxS;
		std::vector<double> pp(2); 
		pp[0] = pp[1] = 0.0;
		std::vector<double> ppVariance(4);
		ppVariance[0] = fxS * fxS;
		ppVariance[1] = 0.0;
		ppVariance[2] = 0.0;
		ppVariance[3] = fxS * fxS;
		std::vector<double> boresight(3);
		boresight[0] = bore_o;
		boresight[1] = bore_p;
		boresight[2] = bore_k;
		std::vector<double> boresightVariance(9);
		for (auto& ele : boresightVariance) ele = 0.0;
		boresightVariance[0] = fxS * fxS;
		boresightVariance[4] = fxS * fxS;
		boresightVariance[8] = fxS * fxS;
		std::vector<double> offset(3);
		for (auto& ele : offset) ele = 0.0;
		std::vector<double> offsetVariance(9);
		for (auto& ele : offsetVariance) ele = 0.0;
		offsetVariance[0] = fxS * fxS;
		offsetVariance[4] = fxS * fxS;
		offsetVariance[8] = fxS * fxS;
		const std::vector<double> distParams;
		const std::vector<double> distParamVariance;

		auto cameraData = data::Camera::getCameraData(0,
			data::_frame,
			fl,
			data::_milimeter,
			fVariance,
			imgW,
			imgH,
			pixelPitch,
			data::_milimeter,
			pp, 
			data::_phoCoordinates,
			data::_milimeter,
			ppVariance,
			boresight,
			data::_rad,
			boresightVariance,
			offset,
			data::_meter,
			offsetVariance,
			L"NA",
			L"NA",
			unsigned int(distParams.size()),
			distParams,
			distParamVariance);

		double finalAngle, finalTx, finalTy;
		double minDist;
		const double threshold = 1.0e-6;
		const double resizeRatioMdl = 1.0;
		const double angTh = 1.0 * deg2rad;
		const double distTh = 10.0;
		const double minMHDist = 100.0;
		const int minNumMatchPairs = 3;

		if(true) /// iterative process
		{
			//std::string temp = "D:\\미호천 시험데이터\\20220329_정확도테스트시험촬영\\model\\DSC00647_model.JPG";
			auto matchResult = linematch::lineMatch(simPhoName, util::uni2multi(srcImgName), foundImgLines, foundMdlLines, finalAngle, finalTx, finalTy, minDist, resizeRatioMdl, angTh, distTh, minMHDist, minNumMatchPairs, threshold, showResult, waitTime);

			size_t numPairs = matchResult.pairs.size();
			std::vector<int> imgCol(numPairs * 2), imgRow(numPairs * 2);
			std::vector<int> mdlCol(numPairs * 2), mdlRow(numPairs * 2);
			
			for (size_t i = 0; i < numPairs; ++i)
			{
				mdlCol[i * 2] = int(matchResult.pairs[i].first.s.x);
				mdlRow[i * 2] = int(matchResult.pairs[i].first.s.y);

				mdlCol[i * 2 + 1] = int(matchResult.pairs[i].first.e.x);
				mdlRow[i * 2 + 1] = int(matchResult.pairs[i].first.e.y);

				imgCol[i * 2] = int(matchResult.pairs[i].second.s.x);
				imgRow[i * 2] = int(matchResult.pairs[i].second.s.y);

				imgCol[i * 2 + 1] = int(matchResult.pairs[i].second.e.x);
				imgRow[i * 2 + 1] = int(matchResult.pairs[i].second.e.y);
			}

			std::cout << "Compute object coordinates of selected lines in a model space: " << std::endl;

			std::vector<double> Xa, Ya, Za;
			auto retVal = objrend::getObjCoord(mdlCol, mdlRow, Xa, Ya, Za);

			std::vector<double> opk(3);
			opk[0] = o;
			opk[1] = p;
			opk[2] = k;
			std::vector<double> opkVariance(9);
			opkVariance[0] = opkVariance[4] = opkVariance[8] = frS * frS;

			std::vector<double> pos(3);
			pos[0] = pc[0];
			pos[1] = pc[1];
			pos[2] = pc[2];
			std::vector<double> posVariance(9);
			posVariance[0] = posVariance[4] = posVariance[8] = frS * frS;

#ifdef _DEBUG
			for (const auto& r : retVal)
			{
				if (!r)
				{
					std::cerr << "Error from objrend::getObjCoord(mdlCol, mdlRow, Xa, Ya, Za)"  << std::endl;
				}
			}
#endif
			if (false) /// point primitive approach: not done yet. parameter class and numeric partial (+ equation function pointer) should be modified for accepting dispersion of 2D and 3D points
			{
				std::vector<spr::CRXYZ> crxyzs;
				crxyzs.reserve(static_cast<size_t>(numPairs * 2));
				spr::CRXYZ control;
				double phox0, phoy0;
				double phox1, phoy1;
				for (size_t i = 0; i < numPairs; ++i)
				{
					size_t idx0 = i * 2;
					size_t idx1 = i * 2 + 1;
#ifdef _DEBUG

					std::cout << imgCol[idx1] << "\t";
					std::cout << imgRow[idx1] << "\t";
#endif
					if (retVal[i])
					{
						spr::cr2xy(imgW, imgH, imgCol[idx0], imgRow[idx0], phox0, phoy0, pixelPitch);
						spr::cr2xy(imgW, imgH, imgCol[idx1], imgRow[idx1], phox1, phoy1, pixelPitch);

						/// line primitive dispersion
						auto xyDisp = spr::getLineDispersion(phox0, phoy0, phox1, phoy1, colS, rowS, maxS);
						auto xyzDisp = spr::getLineDispersion(Xa[idx0], Xa[idx0], Za[idx0], Xa[idx1], Xa[idx1], Za[idx1], xS, yS, zS, maxS);

						control.x = phox0;
						control.y = phoy0;
						control.xa = Xa[idx0];
						control.ya = Ya[idx0];
						control.za = Za[idx0];
						control.xyDisp = xyDisp;
						control.xyzDisp = xyzDisp;
						crxyzs.push_back(control);

						control.x = phox1;
						control.y = phoy1;
						control.xa = Xa[idx1];
						control.ya = Ya[idx1];
						control.za = Za[idx1];
						control.xyDisp = xyDisp;
						control.xyzDisp = xyzDisp;
						crxyzs.push_back(control);
#ifdef _DEBUG
						std::cout << imgCol[idx0] << "\t";
						std::cout << imgRow[idx0] << "\t";
						std::cout << phox0 << "\t";
						std::cout << imgRow[idx0] << "\t";

						std::cout << Xa[idx0] << "\t";
						std::cout << Ya[idx0] << "\t";
						std::cout << Za[idx0] << "\t";
						std::cout << Xa[idx1] << "\t";
						std::cout << Ya[idx1] << "\t";
						std::cout << Za[idx1] << std::endl;
#endif
					}
					else
					{
						std::cout << "Something wrong in checking availability" << std::endl;
						return 1;
					}
				}

				/// run spr
				if (spr::runSpr(cameraData, crxyzs, opk, opkVariance, pos, posVariance, fxS, frS, minSChange, minCorrelation, 20))
				{
				}
			}

			if (true)/// temporal implementation only for spr with line primitives
			{
				std::vector<spr::LinePair> linePairs;
				linePairs.reserve(numPairs);
				spr::LinePair lnPair;
				lnPair.sImgDisp(0, 0) = colS * colS;
				lnPair.sImgDisp(1, 1) = rowS * rowS;
				lnPair.eImgDisp = lnPair.sImgDisp;
				lnPair.sObjDisp(0, 0) = xS * xS;
				lnPair.sObjDisp(1, 1) = yS * yS;
				lnPair.sObjDisp(2, 2) = zS * zS;
				lnPair.eObjDisp = lnPair.sObjDisp;

#ifdef _DEBUG
				std::cout << math::matrixout(lnPair.sImgDisp) << std::endl;
				std::cout << math::matrixout(lnPair.sObjDisp) << std::endl;
#endif
				for (size_t i = 0; i < numPairs; ++i)
				{
					size_t idx0 = i * 2;
					size_t idx1 = i * 2 + 1;
#ifdef _DEBUG

					std::cout << imgCol[idx1] << "\t";
					std::cout << imgRow[idx1] << "\t";
#endif
					if (retVal[i])
					{
						spr::cr2xy(imgW, imgH, imgCol[idx0], imgRow[idx0], lnPair.sx, lnPair.sy, pixelPitch);
						spr::cr2xy(imgW, imgH, imgCol[idx1], imgRow[idx1], lnPair.ex, lnPair.ey, pixelPitch);

						lnPair.sXa = Xa[idx0];
						lnPair.sYa = Ya[idx0];
						lnPair.sZa = Za[idx0];

						lnPair.eXa = Xa[idx1];
						lnPair.eYa = Ya[idx1];
						lnPair.eZa = Za[idx1];

						linePairs.push_back(lnPair);
#ifdef _DEBUG
						std::cout << imgCol[idx0] << "\t";
						std::cout << imgRow[idx0] << "\t";
						std::cout << lnPair.sx << "\t";
						std::cout << lnPair.sy << "\t";
						std::cout << lnPair.sXa << "\t";
						std::cout << lnPair.sYa << "\t";
						std::cout << lnPair.sZa << "\t";

						std::cout << imgCol[idx1] << "\t";
						std::cout << imgRow[idx1] << "\t";
						std::cout << lnPair.ex << "\t";
						std::cout << lnPair.ey << "\t";
						std::cout << lnPair.eXa << "\t";
						std::cout << lnPair.eYa << "\t";
						std::cout << lnPair.eZa << std::endl;
#endif
					}
					else
					{
						std::cout << "Something wrong in checking availability" << std::endl;
						return 1;
					}
				}

				/// run spr
				if (spr::runSprLine(cameraData, linePairs, opk, opkVariance, pos, posVariance, fxS, frS, minSChange, minCorrelation, 20))
				{
				}
			}

			/// Only for checking another function
			/*
			std::vector<double> Xa2, Ya2, Za2;
			auto availabilities = objrend::getImg2ObjCoords(imgW, imgH, fovHDeg, o, p, k, pc[0], pc[1], pc[2], bore_o, bore_p, bore_k, 0, 0, 0, resizeRatio, imgCol, imgRow, Xa2, Ya2, Za2);

			std::cout << std::endl << std::endl;

			for (size_t i = 0; i < availabilities.size(); ++i)
			{
				std::cout << imgCol[i] << "\t";
				std::cout << imgRow[i] << "\t";

				if (retVal[i])
				{
					std::cout << Xa2[i] << "\t";
					std::cout << Ya2[i] << "\t";
					std::cout << Za2[i] << std::endl;
				}
				else
				{
					std::cout << std::endl;
				}
			}
			*/

			/*
			std::cout << i << "\t------------------------------------------" << std::endl;
			std::cout << "minDist: " << minDist << "\t";
			std::cout << "finalAngle: " << finalAngle / 3.14159265358979 * 180. << "\t";
			std::cout << "tx: " << finalTx << "\t";
			std::cout << "ty: " << finalTy << std::endl;
			std::cout << "\t------------------------------------------" << std::endl;

			auto rmat = math::rotation::getRMat(0., 0., finalAngle);
			std::vector<geo::Line2D> foundLinesModel_(foundLinesModel.size());
			for (int i = 0; i < foundLinesModel.size(); ++i)
			{
				foundLinesModel_[i].s = linematch::rigidBodyTransformation(foundLinesModel[i].s.x, foundLinesModel[i].s.y, rmat, finalTx, finalTy);
				foundLinesModel_[i].e = linematch::rigidBodyTransformation(foundLinesModel[i].e.x, foundLinesModel[i].e.y, rmat, finalTx, finalTy);
			}

			std::string inImgPath_edgeLine = outpathLine; //[In]
			std::wstring woutImgPath_adjustedLine = wmodelDir + L"\\" + imageName + L"_line_" + std::to_wstring(i) + L".JPG";
			std::string outImgPath_adjustedLine; outImgPath_adjustedLine.assign(woutImgPath_adjustedLine.begin(), woutImgPath_adjustedLine.end());

			image::draw2DLines(inImgPath_edgeLine, outImgPath_adjustedLine, foundLinesModel_, 0, 0, 255);
			*/
		}
	}

	return 0;
}