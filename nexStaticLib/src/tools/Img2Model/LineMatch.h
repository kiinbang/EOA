#pragma once

#include <string>
#include <vector>

#include <ssm/include/Define.h>

namespace linematch
{
	const double deg2rad = 3.14159265358979 / 180.0;
	const double rad2deg = 180.0 / 3.14159265358979;

	typedef struct tagMatchResult
	{
		std::vector<std::pair<geo::Line2D, geo::Line2D>> pairs;
		double tx, ty;
		double norError;
		double ang;
	} MatchResult;

	MatchResult lineMatch(const std::string& mdlImgPath,
		const std::string& imgPath,
		const std::vector<geo::Line2D>& foundLines,
		const std::vector<geo::Line2D>& foundLinesModel,
		double& finalAngle,
		double& finalTx,
		double& finalTy,
		double& minDist,
		const double resizeRatioMdl,// = 1.0,
		const double angTh,// = 1.0 * deg2rad,
		const double distTh,// = 100.0,
		const double minMHDist,// = 100.0,
		const int minNumMatchPair,// = 3,
		const double threshold,// = 1.0e-6,
		const bool showMatch,// = false,
		const unsigned int waitTime);

	void checkMdlAvailability(std::vector<geo::Line2D>& foundMdlLines);
}
