#include "LineMatch.h"

#include <algorithm>

#include <ImageSupporter.h>
#include <ObjRender.h>
#include <ssm/include/SSMRotation.h>

const double hlfPi = 0.5 * 3.14159265358979;
const double pi = 3.14159265358979;

struct LineLenAng
{
public:
	geo::Line2D l;
	double len;
	double bear;
	bool operator < (const LineLenAng& c)
	{
		return this->len < c.len;
	}
	bool operator > (const LineLenAng& c)
	{
		return this->len > c.len;
	}
};

/// ascending order
bool compareLen(LineLenAng a, LineLenAng b)
{
	return a < b;
}

/// descending order
bool inverseCompareLen(LineLenAng a, LineLenAng b)
{
	return a > b;
}

LineLenAng getLenAng(const double x0, const double y0, const double x1, const double y1)
{
	const auto esx = x1 - x0;
	const auto esy = y1 - y0;
	LineLenAng retval;
	retval.l.s.x = x0;
	retval.l.s.y = y0;
	retval.l.e.x = x1;
	retval.l.e.y = y1;
	retval.len = sqrt(esx * esx + esy * esy);
	retval.bear = atan2(esx, esy);
	return retval;  
}

/// sort lines by length
std::vector<LineLenAng> sortLines(const std::vector<geo::Line2D>& lines)
{
	std::vector<LineLenAng> lenang(lines.size());
	for (size_t i = 0; i < lines.size(); ++i)
	{
		lenang[i] = getLenAng(lines[i].s.x, lines[i].s.y, lines[i].e.x, lines[i].e.y);
	}

	sort(lenang.begin(), lenang.end(), inverseCompareLen);

	return lenang;
}

double computeAngleIn2DVectors(const double ax, const double ay, const double bx, const double by, bool acuteAngle)
{
	double sizeA = sqrt(ax * ax + ay * ay);
	double sizeB = sqrt(bx * bx + by * by);

	if (sizeA < std::numeric_limits<double>::epsilon() ||
		sizeB < std::numeric_limits<double>::epsilon())
	{
		return 0.0;
	}

	/// Determine the angular direction using cross product
	/// + : counter-clockwise
	/// - : clockwise
	double crspro = ax * by - ay * bx;

	/// Determine the size of angle between two vector Using inner product

	double inpro = ax * bx + ay * by;

	double angRad = acos(inpro / sizeA / sizeB);

	if (crspro < 0.0)
		angRad *= -1.0;

	if (acuteAngle && fabs(angRad) > hlfPi)
	{
		if (angRad > 0)
			return angRad - pi;
		else
			return angRad + pi;
	}
	else
		return angRad;
};

double computeAngleIn2DVectors(const geo::Line2D tar, const geo::Line2D& ref, const bool acuteAngle)
{
	/// ref = R(tar) + t
	geo::Point2f a, b;
	a.x = tar.e.x - tar.s.x;
	a.y = tar.e.y - tar.s.y;

	b.x = ref.e.x - ref.s.x;
	b.y = ref.e.y - ref.s.y;

	return computeAngleIn2DVectors(a.x, a.y, b.x, b.y, acuteAngle);
};

double evaluate(const LineLenAng& l0, const LineLenAng& l1, double& tx, double& ty, double& acuteAng, bool& availability)
{
	availability = false;

	auto rot = math::rotation::getRMat(math::AxisName(2), l0.bear - hlfPi);

	data::Point3D pt;
	pt(0) = l0.l.e.x - l0.l.s.x;
	pt(1) = l0.l.e.y - l0.l.s.y;
	pt(2) = 0.0;
	/// rotate end point E of l0
	auto ql0e = rot % pt;

	/// 1. End point S
	pt(0) = l1.l.s.x - l0.l.s.x;
	pt(1) = l1.l.s.y - l0.l.s.y;
	pt(2) = 0.0;
	/// rotate S
	auto qs = rot % pt;

	if (fabs(qs(0)) < std::numeric_limits<double>::epsilon())
	{
		availability = true;
	}
	else if (ql0e(0) / qs(0) > 1.0)
	{
		availability = true;
	}

	/// 2. End point E
	pt(0) = l1.l.e.x - l0.l.s.x;
	pt(1) = l1.l.e.y - l0.l.s.y;
	pt(2) = 0.0;
	/// rotate E
	auto qe = rot % pt;
	if (fabs(qe(0)) < std::numeric_limits<double>::epsilon())
	{
		availability = true;
	}
	else if (ql0e(0) / qe(0) > 1.0)
	{
		availability = true;
	}

	if (!availability) /// no overlap between two lines
		return 1.0e99;

	/// consider only v-coordinate
	auto dv = (qe(1) + qs(1)) * 0.5;

	data::Point3D q;
	q(0) = 0.0;
	q(1) = dv;
	q(2) = 0.0;
	/// 3. inverse-rotate dv
	/// compute tx and ty
	auto p = rot.transpose() % q;
	tx = p(0);
	ty = p(1);

	acuteAng = computeAngleIn2DVectors(l0.l, l1.l, true);

	return fabs(dv);
}

double evaluateL02L1(const std::string& imgPath,
	const double resizeRatio,
	const std::vector<LineLenAng>& l0,
	const std::vector<LineLenAng>& l1,
	const double angTh,
	const double distTh,
	double& sumTx, double& sumTy,
	std::vector<std::pair<int, int>>& matchPair,
	const bool showMatch,
	const unsigned int waitTime)
{
	matchPair.reserve(l1.size());

	int matchCount = 0;
	double eDistSum = 0.0;

	sumTx = sumTy = 0.0;

	for (size_t i0 = 0; i0 < l0.size(); ++i0)
	{
		const auto& ref = l0[i0];

		double minDist = 1.0e9;
		double finalTx, finalTy, ang;
		int found = -1;
		for (size_t i1 = 0; i1 < l1.size(); ++i1)
		{
			const auto& tar = l1[i1];

			double tx, ty;
			bool availability;
			auto dist = evaluate(ref, tar, tx, ty, ang, availability);

			if (!availability || fabs(ang) > angTh)
				continue;

			if (dist < distTh && tar.len > std::numeric_limits<double>::epsilon())
			{
				dist /= tar.len;
				if (minDist > dist)
				{
					found = int(i1);
					minDist = dist;
					finalTx = tx;
					finalTy = ty;
#ifdef _DEBUG
					if (showMatch)
					{
						std::vector<geo::Line2D> line0(1);
						std::vector<geo::Line2D> line1(1);

						line0[0].s = ref.l.s;
						line0[0].e = ref.l.e;
						line1[0].s = tar.l.s;
						line1[0].e = tar.l.e;

						edge::showLines(std::string("Show match line pair"), imgPath, line0, line1, waitTime, resizeRatio, 255, 0, 0, 0, 0, 255);

						auto temp = evaluate(ref, tar, tx, ty, ang, availability);
					}
#endif
				}
			}
		}

		if (found > -1)
		{
			std::pair<int, int> foundPair;
			foundPair.first = i0;
			foundPair.second = found;
			matchPair.push_back(foundPair);
			eDistSum += minDist;
			sumTx += finalTx;
			sumTy += finalTy;
			++matchCount;

#ifdef _DEBUG
			if (showMatch)
			{
				std::vector<geo::Line2D> line0(1);
				std::vector<geo::Line2D> line1(1);

				line0[0].s = l0[i0].l.s;
				line0[0].e = l0[i0].l.e;
				line1[0].s = l1[found].l.s;
				line1[0].e = l1[found].l.e;

				edge::showLines(std::string("[Final] Show match line pair"), imgPath, line0, line1, waitTime, resizeRatio, 255, 0, 0, 0, 0, 255);
			}
#endif
		}
	}

	return eDistSum;
}

/// similarity
double evaluate(const std::string& imgPath, const double resizeRatio, const std::vector<LineLenAng>& l0, const std::vector<LineLenAng>& l1, const double angTh, const double distTh, double& sumTx, double& sumTy, std::vector<std::pair<int, int>>& matchPair, const bool showMatch, const unsigned int waitTime)
{
	if (l0.size() < l1.size())
	{
		return evaluateL02L1(imgPath, resizeRatio, l0, l1, angTh, distTh, sumTx, sumTy, matchPair, showMatch, waitTime);
	}
	else
	{
		std::vector<std::pair<int, int>> invPair;
		auto e = evaluateL02L1(imgPath, resizeRatio, l1, l0, angTh, distTh, sumTx, sumTy, invPair, showMatch, waitTime);
		matchPair.resize(invPair.size());

		for (size_t i = 0; i < invPair.size(); ++i)
		{
			matchPair[i].second = invPair[i].first;
			matchPair[i].first = invPair[i].second;
		}

		return e;
	}
}

std::vector<LineLenAng> shift(const std::vector<LineLenAng>& lines, const double sft_x, const double sft_y)
{
	std::vector<LineLenAng> sftLines = lines;
	for (auto& l : sftLines)
	{
		l.l.s.x += sft_x;
		l.l.s.y += sft_y;

		l.l.e.x += sft_x;
		l.l.e.y += sft_y;
	}
	return sftLines;
}

std::vector<geo::Line2D> shift(const std::vector<geo::Line2D>& lines, const double sft_x, const double sft_y)
{
	std::vector<geo::Line2D> sftLines = lines;
	for (auto& l : sftLines)
	{
		l.s.x += sft_x;
		l.s.y += sft_y;

		l.e.x += sft_x;
		l.e.y += sft_y;
	}
	return sftLines;
}

namespace linematch
{
	MatchResult lineMatch(const std::string& mdlImgPath,
		const std::string& imgPath,
		const std::vector<geo::Line2D>& foundLines,
		const std::vector<geo::Line2D>& foundLinesModel,
		double& finalAngle,
		double& finalTx,
		double& finalTy,
		double& minDist,
		const double resizeRatioMdl,
		const double angTh,
		const double distTh,
		const double minMHDist,
		const int minNumMatchPair,
		const double threshold,
		const bool showMatch,
		const unsigned int waitTime)
	{
		/// [Todo]
		/// Bad scale factor (wrong distance derived from xy-position) can cause a failure.
		/// Improve for a scale invariant approach
		/// 
		size_t numModelLines = foundLinesModel.size();
		size_t numImgLines = foundLines.size();

		/// Initialize min-distance
		minDist = 1.0e99;
		int maxNumMatchPair = 0;

		/// brute force
		{
			/// 1. prefer long base lines
			std::vector<LineLenAng> sml = sortLines(foundLinesModel);
			std::vector<LineLenAng> sil = sortLines(foundLines);
			std::vector<std::pair<int, int>> bestMatchPairs;
			double betsAngle;
			double finalNorErr = 1.0e99;
			double finalTx, finalTy, FinalAcuteAng;

#ifdef _DEBUG
			std::vector<geo::Line2D> sml_(sml.size());
			std::vector<geo::Line2D> sil_(sil.size());

			for (int i = 0; i < sml.size(); ++i)
			{
				sml_[i].s = sml[i].l.s;
				sml_[i].e = sml[i].l.e;
			}

			for (int i = 0; i < sil.size(); ++i)
			{
				sil_[i].s = sil[i].l.s;
				sil_[i].e = sil[i].l.e;
			}

			if (showMatch)
				edge::showLines(std::string("raw mdl and img lines"), mdlImgPath, sml_, sil_, waitTime, resizeRatioMdl, 0, 255, 0, 0, 0, 255);

			std::vector<std::pair<int, int>> pairs;
			double sumTx, sumTy;
			auto edist = evaluate(mdlImgPath, resizeRatioMdl, sml, sil, angTh, distTh, sumTx, sumTy, pairs, showMatch, waitTime);
			if (pairs.size() > 0)
			{
				std::cout << "Initial error dist: " << edist << "\t tx, ty: " << sumTx / pairs.size() << "\t" << sumTy / pairs.size() << std::endl;

				std::vector<std::pair<geo::Line2D, geo::Line2D>> linesPairs(pairs.size());

				for (int i = 0; i < pairs.size(); ++i)
				{
					linesPairs[i].first.s = sml[pairs[i].first].l.s;
					linesPairs[i].first.e = sml[pairs[i].first].l.e;

					linesPairs[i].second.s = sil[pairs[i].second].l.s;
					linesPairs[i].second.e = sil[pairs[i].second].l.e;
				}

				if(showMatch)
					edge::showLines(std::string("selected mdl and shift-img lines"), mdlImgPath, linesPairs, waitTime, resizeRatioMdl, 255, 0, 0, 0, 0, 255);
			}
#endif
			for (size_t mdlIdx = 0; mdlIdx < numModelLines; ++mdlIdx)
			{
				if (sml[mdlIdx].len < std::numeric_limits<double>::epsilon())
					continue;

				for (size_t imgIdx = 0; imgIdx < numImgLines; ++imgIdx)
				{
					/// 2. corresponding line check
					double tx, ty, ang;
					bool availability;
					auto misclosure = evaluate(sml[mdlIdx], sil[imgIdx], tx, ty, ang, availability);

					if (!availability || fabs(ang) > angTh || fabs(tx) + fabs(ty) > minMHDist)
						continue;
#ifdef _DEBUG
					if (false)
					{
						std::vector<geo::Line2D> temp0(1);
						temp0[0].s = sml[mdlIdx].l.s;
						temp0[0].e = sml[mdlIdx].l.e;

						std::vector<geo::Line2D> temp(1);
						temp[0].s = sil[imgIdx].l.s;
						temp[0].e = sil[imgIdx].l.e;
						//if (showMatch)
							//edge::showLines(std::string("corresponding mdl and img lines"), mdlImgPath, temp0, temp, resizeRatioMdl, 0, 0, 0, 0, 0, 255);

						std::cout << "shift(dx and dy): " << -tx << "\t" << -ty << std::endl;
						auto sftLinesTmp = shift(sil, -tx, -ty);

						std::vector<geo::Line2D> sftImgLines(sftLinesTmp.size());
						for (int i = 0; i < sftImgLines.size(); ++i)
						{
							sftImgLines[i].s = sftLinesTmp[i].l.s;
							sftImgLines[i].e = sftLinesTmp[i].l.e;
						}

						//if (showMatch)
						{
							//edge::showLines(std::string("mdl and shift-img lines"), mdlImgPath, foundLinesModel, sftImgLines, resizeRatioMdl, 0, 0, 0, 0, 0, 255);
							//edge::showLines(std::string("raw img and shift-img lines"), mdlImgPath, foundLines, sftImgLines, resizeRatioMdl, 0, 0, 0, 0, 0, 255);
						}

						std::vector<std::pair<int, int>> matchPairsTmp;
						double sumTx, sumTy;
						double error = evaluate(mdlImgPath, resizeRatioMdl, sml, sftLinesTmp, angTh, distTh, sumTx, sumTy, matchPairsTmp, showMatch, waitTime);

						std::cout << "error: " << error << "\t tx, ty: " << sumTx / matchPairsTmp.size() << "\t" << sumTy / matchPairsTmp.size() << std::endl;

						std::vector<geo::Line2D> selectedMdlLine(matchPairsTmp.size());
						std::vector<geo::Line2D> selectedImgLinesft(matchPairsTmp.size());
						for (int i = 0; i < matchPairsTmp.size(); ++i)
						{
							selectedMdlLine[i].s = sml[matchPairsTmp[i].first].l.s;
							selectedMdlLine[i].e = sml[matchPairsTmp[i].first].l.e;
							selectedImgLinesft[i].s = sftLinesTmp[matchPairsTmp[i].second].l.s;
							selectedImgLinesft[i].e = sftLinesTmp[matchPairsTmp[i].second].l.e;
						}
					}
#endif

					/// 3. shift
					std::cout << "shift(dx and dy): " << -tx << "\t" << -ty << std::endl;
					auto sftLines = shift(sil, -tx, -ty);

					/// 4. similarity check
					std::vector<std::pair<int, int>> matchPairs;
					double sumTx, sumTy;
					double retError = evaluate(mdlImgPath, resizeRatioMdl, sml, sftLines, angTh, distTh, sumTx, sumTy, matchPairs, showMatch, waitTime);
					std::cout << "evaluate error: " << retError << "\t aveTx, aveTy: " << sumTx / matchPairs.size() << "\t" << sumTy / matchPairs.size() << std::endl;

					double norError;
					if (matchPairs.size() >= maxNumMatchPair && matchPairs.size() > minNumMatchPair)
					{
						/// L2 weighting error
						norError = retError / double(matchPairs.size() * matchPairs.size());

						/// 5. update best match
						if (matchPairs.size() > maxNumMatchPair || norError < finalNorErr)
						{
							maxNumMatchPair = matchPairs.size();

							finalTx = tx;
							finalTy = ty;
							FinalAcuteAng = ang;

							bestMatchPairs = matchPairs;
							finalNorErr = norError;

							std::cout << "FinalAcuteAng: " << FinalAcuteAng * rad2deg << "\tfinalTx: " << finalTx << "\tfinalTy: " << finalTy << std::endl;
							std::cout << "finalNorErr: " << finalNorErr << "\t";
							std::cout << "norError: " << retError << std::endl;

#ifdef _DEBUG
							std::vector<geo::Line2D> selectedMdlLine(matchPairs.size());
							std::vector<geo::Line2D> selectedImgLine0(matchPairs.size());
							std::vector<geo::Line2D> selectedImgLinesft(matchPairs.size());
							for (int i = 0; i < matchPairs.size(); ++i)
							{
								selectedMdlLine[i].s = sml[matchPairs[i].first].l.s;
								selectedMdlLine[i].e = sml[matchPairs[i].first].l.e;
								selectedImgLinesft[i].s = sftLines[matchPairs[i].second].l.s;
								selectedImgLinesft[i].e = sftLines[matchPairs[i].second].l.e;
								selectedImgLine0[i].s = sil[matchPairs[i].second].l.s;
								selectedImgLine0[i].e = sil[matchPairs[i].second].l.e;
							}

							if (showMatch)
							{
								//edge::showLines(std::string("mdl and selected-raw-img lines"), mdlImgPath, selectedMdlLine, selectedImgLine0, waitTime, resizeRatioMdl, 0, 0, 0, 0, 0, 255);
								edge::showLines(std::string("selected mdl and shift-img lines"), mdlImgPath, selectedMdlLine, selectedImgLinesft, waitTime, resizeRatioMdl, 0, 0, 0, 0, 0, 255);
								//edge::showLines(std::string("selected raw and shift-img lines"), mdlImgPath, selectedImgLine0, selectedImgLinesft, waitTime, resizeRatioMdl, 0, 0, 0, 0, 0, 255);
							}
#endif
						}
					}
				}
			}

			std::cout << "FinalAcuteAng: " << FinalAcuteAng * rad2deg << "\tfinalTx: " << finalTx << "\tfinalTy: " << finalTy << std::endl;
			std::cout << "finalNorErr: " << finalNorErr << "\t";

#ifdef _DEBUG
			if(showMatch)
				edge::showLines(std::string("all model lines and shift-img lines"), mdlImgPath, foundLinesModel, shift(foundLines, -finalTx, -finalTy), waitTime, resizeRatioMdl, 255, 0, 0, 0, 0, 255);

			/// 3. shift
			auto sftLines = shift(sil, -finalTx, -finalTy);

			std::vector<geo::Line2D> selectedMdlLine(bestMatchPairs.size());
			std::vector<geo::Line2D> selectedImgLine0(bestMatchPairs.size());
			std::vector<geo::Line2D> selectedImgLinesft(bestMatchPairs.size());
			for (int i = 0; i < bestMatchPairs.size(); ++i)
			{
				selectedMdlLine[i].s = sml[bestMatchPairs[i].first].l.s;
				selectedMdlLine[i].e = sml[bestMatchPairs[i].first].l.e;
				selectedImgLinesft[i].s = sftLines[bestMatchPairs[i].second].l.s;
				selectedImgLinesft[i].e = sftLines[bestMatchPairs[i].second].l.e;
				selectedImgLine0[i].s = sil[bestMatchPairs[i].second].l.s;
				selectedImgLine0[i].e = sil[bestMatchPairs[i].second].l.e;
			}

			edge::showLines(std::string("mdl and selected-raw-img lines"), mdlImgPath, selectedMdlLine, selectedImgLine0, 0, resizeRatioMdl, 255, 0, 0, 0, 0, 255);
			edge::showLines(std::string("selected mdl and shift-img lines"), mdlImgPath, selectedMdlLine, selectedImgLinesft, 0, resizeRatioMdl, 255, 0, 0, 0, 0, 255);

			if (showMatch)
			{
				std::vector<geo::Line2D> tempLines;
				edge::showLines(std::string("selected mdl"), mdlImgPath, selectedMdlLine, tempLines, waitTime, resizeRatioMdl, 255, 0, 0, 255, 0, 255);
				edge::showLines(std::string("mdl and selected-raw-img lines"), mdlImgPath, selectedMdlLine, selectedImgLine0, waitTime, resizeRatioMdl, 255, 0, 0, 0, 0, 255);
				edge::showLines(std::string("selected mdl and shift-img lines"), mdlImgPath, selectedMdlLine, selectedImgLinesft, waitTime, resizeRatioMdl, 255, 0, 0, 0, 0, 255);
				edge::showLines(std::string("selected raw and shift-img lines"), mdlImgPath, selectedImgLine0, selectedImgLinesft, waitTime, resizeRatioMdl, 0, 0, 0, 0, 0, 255);
			}
#endif
			MatchResult retval;
			retval.pairs.resize(bestMatchPairs.size());

			for (size_t i = 0; i < bestMatchPairs.size(); ++i)
			{
				retval.pairs[i].first.s = sml[bestMatchPairs[i].first].l.s;
				retval.pairs[i].first.e = sml[bestMatchPairs[i].first].l.e;
				retval.pairs[i].second.s = sil[bestMatchPairs[i].second].l.s;
				retval.pairs[i].second.e = sil[bestMatchPairs[i].second].l.e;
			}

			retval.tx = finalTx;
			retval.ty = finalTy;
			retval.norError = finalNorErr;
			retval.ang = FinalAcuteAng;

			return retval;
		}
	}

	bool checkAvailability(float& x, float& y, const float shiftVal)
	{
		/// adjust found lines
		std::vector<int> col(1), row(1);
		col[0] = x;
		row[0] = y;
		std::vector<double> Xa, Ya, Za;

		auto retVal = objrend::getObjCoord(col, row, Xa, Ya, Za);
		
		if (!retVal[0])
		{
			col[0] = x += shiftVal;
			retVal = objrend::getObjCoord(col, row, Xa, Ya, Za);
		}
		
		if (!retVal[0])
		{
			col[0] = x -= (2.0f * shiftVal);
			retVal = objrend::getObjCoord(col, row, Xa, Ya, Za);
		}
		
		if (!retVal[0])
		{
			col[0] = x += shiftVal;
			row[0] = y += shiftVal;
			retVal = objrend::getObjCoord(col, row, Xa, Ya, Za);
		}
		
		if (!retVal[0])
		{
			row[0] = y -= (2.0f * shiftVal);
			retVal = objrend::getObjCoord(col, row, Xa, Ya, Za);
		}
		
		if (!retVal[0])
		{
			row[0] = y += shiftVal;
			return false;
		}
		else
		{
			return true;
		}
	}

	void checkMdlAvailability(std::vector<geo::Line2D>& foundMdlLines)
	{
		const float falseVal = -999.9f;

		int c0 = 0;
		float shiftVal = 1.0f;
		const float maxShift = 3.0f;
		for (size_t i = 0; i < foundMdlLines.size(); ++i)
		{
			while(!checkAvailability(foundMdlLines[i].s.x, foundMdlLines[i].s.y, shiftVal) && shiftVal <= maxShift)
				shiftVal += 1.0f;

			if (shiftVal >= maxShift) foundMdlLines[i].s.x = foundMdlLines[i].s.y = falseVal;

			shiftVal = 1.0f;

			while (!checkAvailability(foundMdlLines[i].e.x, foundMdlLines[i].e.y, shiftVal) && shiftVal <= maxShift)
				shiftVal += 1.0f;

			if (shiftVal >= maxShift) foundMdlLines[i].e.x = foundMdlLines[i].e.y = falseVal;

			shiftVal = 1.0f;
		}

		int count = 0;
		/// erase unavailable vector-elements
		for ( std::vector<geo::Line2D>::iterator iter = foundMdlLines.begin(); iter != foundMdlLines.end(); )
		{
			if ((*iter).s.x == falseVal && (*iter).s.y == falseVal ||
				(*iter).e.x == falseVal && (*iter).e.y == falseVal)
			{
				iter = foundMdlLines.erase(iter);
				std::cout << count << "th pt is not available." << std::endl;
			}
			else
				++iter;

			++count;
		}
	}
}