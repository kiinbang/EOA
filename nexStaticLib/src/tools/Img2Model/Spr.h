#pragma once

#include <limits>
#include <vector>

#include <ssm/include/BundleBlockData.h>
#include <ssm/include/BundleBlockDataReader.hpp>
#include <ssm/include/SSMBundle.h>
#include <ssm/include/SSMFixedSizeMatd.h>
#include <ssm/include/SSMRotation.h>

#include "LineMatch.h"

namespace spr
{
	double maxDouble = (std::numeric_limits<double>::max)();
	const double pi = 3.14159265358979;
	const double hlfPi = 0.5 * pi;

	struct CRXYZ
	{
		double x, y;
		double xa, ya, za;
		math::FixedSizeMat<double, 2, 2> xyDisp;
		math::FixedSizeMat<double, 3, 3> xyzDisp;
	};

	struct LinePair
	{
		double ex, ey;
		double sx, sy;
		double sXa, sYa, sZa;
		double eXa, eYa, eZa;
		math::FixedSizeMat<double, 2, 2> sImgDisp;
		math::FixedSizeMat<double, 2, 2> eImgDisp;
		math::FixedSizeMat<double, 3, 3> sObjDisp;
		math::FixedSizeMat<double, 3, 3> eObjDisp;
	};

	void cr2xy(const unsigned int w, const unsigned int h, const double c, const double r, double& x, double& y, const double pixelPitch)
	{
		x = c - (w * 0.5);
		y = (h * 0.5) - r;
		x *= pixelPitch;
		y *= pixelPitch;
	}

	math::Matrix<double> getLineDispersion(const double x0, const double y0, const double z0,
		const double x1, const double y1, const double z1,
		const double sx, const double sy, const double sz,
		const double maxS)
	{
		/// rotate a line until it is parallel to x-axis

		const auto esx = x1 - x0;
		const auto esy = y1 - y0;
		const auto esz = z1 - z0;

		auto phi = -atan2(esz, esx);
		auto kap = -atan2(sqrt(esx*esx + esz*esz), esy);

		auto rot = math::rotation::getRMat(math::AxisName(2), -kap - hlfPi) % math::rotation::getRMat(math::AxisName(1), -phi);
		auto rotT = rot.transpose();

#ifdef _DEBUG
		data::Point3D pt;
		pt(0) = esx;
		pt(1) = esy;
		pt(2) = esz;
		std::cout << "pt(x, y, z) = " << pt(0, 0) << "\t" << pt(1, 0) << "\t" << pt(2, 0) << std::endl;
		auto qt = rot % pt;
		std::cout << "qt_caseI(u, v, w) = " << qt(0, 0) << "\t" << qt(1, 0) << "\t" << qt(2, 0) << std::endl;
		auto omg_ = -atan2(sqrt(esx * esx + esy * esy), esz);
		auto kap_ = -atan2(esx, esy);
		auto rotII = math::rotation::getRMat(math::AxisName(1), hlfPi) % math::rotation::getRMat(math::AxisName(0), -omg_) % math::rotation::getRMat(math::AxisName(2), -kap_);
		qt = rotII % pt;
		std::cout << "qt_caseII(u, v, w) = " << qt(0, 0) << "\t" << qt(1, 0) << "\t" << qt(2, 0) << std::endl;
#endif

		math::Matrix<double> lineDisp(3, 3);
		lineDisp(0, 0) = sx * sx;
		lineDisp(1, 1) = sy * sy;
		lineDisp(2, 2) = sz * sz;
		auto lineDispUVW = rot % lineDisp % rotT;
		auto lineDispUVW2 = lineDispUVW;
		lineDispUVW2(0, 0) = maxS;
		auto lineDisp2 = rotT % lineDispUVW2 % rot;

		return lineDisp2;
	}

	math::Matrix<double> getLineDispersion(const double x0, const double y0, const double x1, const double y1, const double sx, const double sy, const double maxS)
	{
		const auto esx = x1 - x0;
		const auto esy = y1 - y0;

		auto bear2 = atan2(esx, esy);
		auto kap = -bear2;

		/// make a line parallel to y-axis
		//auto rot = math::rotation::getRMat(math::AxisName(2), bear2);
		/// make a line parallel to x-axis
		auto rot = math::rotation::getRMat(-kap - hlfPi);
		auto rotT = rot.transpose();
#ifdef _DEBUG
		data::Point2D pt;
		pt(0) = esx;
		pt(1) = esy;
		auto qt = rot % pt;
		std::cout << "qt(u, v) = " << qt(0, 0) << "\t" << qt(1, 0) << std::endl;
#endif
		math::Matrix<double> lineDisp(2, 2);
		lineDisp(0, 0) = sx * sx;
		lineDisp(1, 1) = sy * sy;
		auto lineDispUV = rot % lineDisp % rotT;
		auto lineDispUV2 = lineDispUV;
		lineDispUV2(0, 0) = maxS * maxS;
		auto lineDisp2 = rotT % lineDispUV2 % rot;

		return lineDisp2;
	}

	bool runSpr(const data::Camera::CameraData& cam, 
		const std::vector<CRXYZ>& crxyzs,
		const std::vector<double>& opk,
		const std::vector<double>& opkVariance,
		const std::vector<double>& pos,
		const std::vector<double>& posVariance,
		const double fxS,
		const double frS,
		const double sTh,
		const double minCorrelation,
		const unsigned int maxIter)
	{
		std::vector<data::Camera::CameraData> camData(1);
		camData[0] = cam;

		std::vector<data::Pho::Image::ImgPt> imgPts(crxyzs.size());
		data::Obj objPtData;
		objPtData.unit = data::_meter;
		objPtData.objPts.resize(crxyzs.size());

		for (unsigned int i = 0; i < crxyzs.size(); ++i)
		{
			imgPts[i].pointId = i;
			imgPts[i].coordinates.resize(2);
			imgPts[i].coordinates[0] = crxyzs[i].x;
			imgPts[i].coordinates[1] = crxyzs[i].y;
			imgPts[i].dispersion.resize(4);
			const double* xyDisp = crxyzs[i].xyDisp.getAccessData();
			for(size_t j = 0; j<4; ++j) imgPts[i].dispersion[j] = xyDisp[j];

			objPtData.objPts[i].pointId = i;
			objPtData.objPts[i].xyz.resize(3);
			objPtData.objPts[i].xyz[0] = crxyzs[i].xa;
			objPtData.objPts[i].xyz[1] = crxyzs[i].ya;
			objPtData.objPts[i].xyz[2] = crxyzs[i].za;
			objPtData.objPts[i].variance.resize(9);
			const double* xyzVariance = crxyzs[i].xyzDisp.getAccessData();
			for (size_t j = 0; j < 9; ++j) objPtData.objPts[i].variance[j] = xyzVariance[j];
		}
		
		std::vector<data::Pho::Image> images(1);
		images[0] = data::Pho::getImage(int(0), camData[0].id, opk, data::_rad, opkVariance, pos, data::_meter, posVariance, imgPts, data::_phoCoordinates, data::_milimeter, L"NA", L"NA");

		data::Project::ProjectData prjData = data::Project::getPrjData(L"photo2model", L"SPR", fxS, frS, minCorrelation, maxIter, sTh);

		data::BundleBlockPrj baData;
		baData.setBundleData(prjData, camData, images, objPtData);

		ssm::Bundle ba(baData.getPhotos(),
			baData.getObjectPoints(),
			baData.getMinSigma(),
			baData.getMaxSigma(),
			baData.getMaxIteration(),
			baData.getSigmaThreshold(),
			baData.getMinCorrelation());

		LOG(INFO) << "Run adjustment for SPR";

		try
		{
			std::stringstream camParameters;
			std::stringstream eopParameters;
			std::stringstream objParameters;
			const double tryLM = false;
			/// Run
			std::string retmsg = ba.run(baData.getProjectName(), camParameters, eopParameters, objParameters, tryLM, ssm::SOLVEMETHOD::SIMPLE);
		}
		catch (...)
		{
			std::cout << "Exceptional error from runSpr(...)" << std::endl;
			std::cout << std::endl;
			std::cout << ba.getLogMsg();
		}

		return true;
	}

	double similarity(const data::Point2D& p, const data::Point2D& p0, const data::Point2D& p1)
	{
		auto a = p1 - p0;
		auto l = sqrt(a(0) * a(0) + a(1) * a(1));
		if (l <= std::numeric_limits<double>::epsilon())
			return 0.0;
		auto b = p - p0;
		return (a(0) * b(1) - a(1) * b(0)) / l;
	}

	std::vector<double> eq(const std::vector<data::Point2D>& sImgPts,
		const std::vector<data::Point2D>& eImgPts,
		const std::vector<data::Point3D>& sXaObjs,
		const std::vector<data::Point3D>& eXaObjs,
		const data::Point3D& leverarm,
		const std::vector<double>& boresight,
		const double f,
		const data::Point3D& pc,
		const std::vector<double>& opk)
	{
#ifdef _DEBUG
		if (!(sImgPts.size() == eImgPts.size() && eImgPts.size() == sXaObjs.size() && sXaObjs.size() == eXaObjs.size()))
			throw std::runtime_error("input data sizes are wrong; check this function eq(...) in spr.h");
#endif
		math::RotationMatrix rotBsM = math::rotation::getMMat(boresight[0], boresight[1], boresight[2]);
		math::RotationMatrix rotBodyM = math::rotation::getMMat(opk[0], opk[1], opk[2]);
		
		std::vector<double> e(sImgPts.size());

		for (size_t i = 0; i < sImgPts.size(); ++i)
		{
			auto sXaPho = ssm::collinearityEqWithAlignment(f, rotBsM, leverarm, pc, rotBodyM, sXaObjs[i]);
			auto eXaPho = ssm::collinearityEqWithAlignment(f, rotBsM, leverarm, pc, rotBodyM, eXaObjs[i]);
			auto retVal0 = similarity(sImgPts[i], sXaPho, eXaPho);
			auto retVal1 = similarity(eImgPts[i], sXaPho, eXaPho);
			e[i] = retVal0 + retVal1;
		}

		return e;
	}

	bool runSprLine(const data::Camera::CameraData& cam,
		const std::vector<LinePair>& linePairs,
		const std::vector<double>& opk,
		const std::vector<double>& opkVariance,
		const std::vector<double>& pos,
		const std::vector<double>& posVariance,
		const double fxS,
		const double frS,
		const double sTh,
		const double minCorrelation,
		const unsigned int maxIter)
	{
		data::Camera camInfo;
		camInfo.cameras.resize(1);
		camInfo.cameras[0] = cam;
		auto camPtrs = camInfo.getFrameCameras(fxS);
		auto& camPtr = camPtrs[0];

		std::vector<data::Point2D> sImgPts(linePairs.size());
		std::vector<data::Point2D> eImgPts(linePairs.size());
		std::vector<data::Point3D> sXaObjs(linePairs.size());
		std::vector<data::Point3D> eXaObjs(linePairs.size());
		data::Point2D distortedPhotoCoord;

		for (size_t i = 0; i < linePairs.size(); ++i)
		{
			distortedPhotoCoord(0) = linePairs[i].sx;
			distortedPhotoCoord(1) = linePairs[i].sy;			
			sImgPts[i] = camPtr->getRefinedPhotoCoord(distortedPhotoCoord);

			distortedPhotoCoord(0) = linePairs[i].ex;
			distortedPhotoCoord(1) = linePairs[i].ey;
			eImgPts[i] = camPtr->getRefinedPhotoCoord(distortedPhotoCoord);

			sXaObjs[i](0) = linePairs[i].sXa;
			sXaObjs[i](1) = linePairs[i].sYa;
			sXaObjs[i](2) = linePairs[i].sZa;

			eXaObjs[i](0) = linePairs[i].eXa;
			eXaObjs[i](1) = linePairs[i].eYa;
			eXaObjs[i](2) = linePairs[i].eZa;
		}

		data::Point3D leverarm;
		leverarm(0) = cam.offset[0];
		leverarm(1) = cam.offset[1];
		leverarm(2) = cam.offset[2];
		data::Point3D pc;
		pc(0) = pos[0];
		pc(1) = pos[1];
		pc(2) = pos[2];

		do
		{
			std::vector<double> e = eq(sImgPts, eImgPts, sXaObjs, eXaObjs, leverarm, cam.boresight, cam.f, pc, opk);

		} while (1);





		/*

		for (unsigned int i = 0; i < crxyzs.size(); ++i)
		{
			imgPts[i].pointId = i;
			imgPts[i].coordinates.resize(2);
			imgPts[i].coordinates[0] = crxyzs[i].x;
			imgPts[i].coordinates[1] = crxyzs[i].y;
			imgPts[i].dispersion.resize(4);
			const double* xyDisp = crxyzs[i].xyDisp.getAccessData();
			for (size_t j = 0; j < 4; ++j) imgPts[i].dispersion[j] = xyDisp[j];

			objPtData.objPts[i].pointId = i;
			objPtData.objPts[i].xyz.resize(3);
			objPtData.objPts[i].xyz[0] = crxyzs[i].xa;
			objPtData.objPts[i].xyz[1] = crxyzs[i].ya;
			objPtData.objPts[i].xyz[2] = crxyzs[i].za;
			objPtData.objPts[i].variance.resize(9);
			const double* xyzVariance = crxyzs[i].xyzDisp.getAccessData();
			for (size_t j = 0; j < 9; ++j) objPtData.objPts[i].variance[j] = xyzVariance[j];
		}

		

		data::Project::ProjectData prjData = data::Project::getPrjData(L"photo2model", L"SPR", fxS, frS, minCorrelation, maxIter, sTh);

		data::BundleBlockPrj baData;
		baData.setBundleData(prjData, camData, images, objPtData);

		ssm::Bundle ba(baData.getPhotos(),
			baData.getObjectPoints(),
			baData.getMinSigma(),
			baData.getMaxSigma(),
			baData.getMaxIteration(),
			baData.getSigmaThreshold(),
			baData.getMinCorrelation());

		LOG(INFO) << "Run adjustment for SPR";

		try
		{
			std::stringstream camParameters;
			std::stringstream eopParameters;
			std::stringstream objParameters;
			const double tryLM = false;
			/// Run
			std::string retmsg = ba.run(baData.getProjectName(), camParameters, eopParameters, objParameters, tryLM, ssm::SOLVEMETHOD::SIMPLE);
		}
		catch (...)
		{
			std::cout << "Exceptional error from runSpr(...)" << std::endl;
			std::cout << std::endl;
			std::cout << ba.getLogMsg();
		}
		*/

		return true;
	}
}