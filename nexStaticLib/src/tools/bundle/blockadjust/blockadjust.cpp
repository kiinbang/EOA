﻿// blockadjust.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#define GOOGLE_GLOG_DLL_DECL
#define GLOG_NO_ABBREVIATED_SEVERITIES

#include <glog/logging.h>

#include <chrono>

#include <ssm/include/BundleBlockData.h>
#include <ssm/include/SSMBundle.h>
#include <ssm/include/utilitygrocery.h>

std::string getCamParamStr(const std::vector<std::vector<param::Parameter>>& cameras, const double minSigma = 0.0 , const unsigned int precision = 9, const std::ios::fmtflags format = std::ios::scientific)
{
	std::stringstream stResult;
	stResult.str("");

	std::stringstream names;	
	std::stringstream values;	
	values.precision(precision);
	values.setf(format, std::ios::fixed);

	for(unsigned int camIdx = 0; camIdx < cameras.size(); ++camIdx)
	{
		stResult << std::to_string(camIdx) << std::string("th Camera: ") << std::endl;

		names.str("");
		values.str("");

		for (unsigned int p = 0; p < cameras[camIdx].size(); ++p)
		{
			auto nameP = cameras[camIdx][p].getName();
			names << nameP;

			if (cameras[camIdx][p].getStDev() <= minSigma)
			{
				names << std::string("(Fixed)");
			}

			auto valueP = cameras[camIdx][p].get();
			if (nameP == constant::camera::_omega_bs_ ||
				nameP == constant::camera::_phi_bs_ ||
				nameP == constant::camera::_kappa_bs_)
			{
				valueP = util::Rad2Deg(valueP);
				names << std::string("[deg]");
			}
			names << std::string("\t");
			values << valueP << std::string("\t");
		}
		
		stResult << names.str() << std::endl;
		stResult << values.str() << std::endl;
	}

	return stResult.str();
}

std::string getEopStr(const std::vector<std::vector<param::Parameter>>& eops, const double minSigma = 0.0, const unsigned int precision = 9, const std::ios::fmtflags format = std::ios::scientific)
{
	std::stringstream stResult;
	stResult.str("");

	std::stringstream names;
	std::stringstream values;
	values.precision(precision);
	values.setf(format, std::ios::fixed);

	for (unsigned int phoIdx = 0; phoIdx < eops.size(); ++phoIdx)
	{
		stResult << std::to_string(phoIdx) << std::string("th Photo EOPs: ") << std::endl;

		names.str("");
		values.str("");

		for (unsigned int p = 0; p < eops[phoIdx].size(); ++p)
		{
			auto nameP = eops[phoIdx][p].getName();
			names << nameP;

			if (eops[phoIdx][p].getStDev() <= minSigma)
			{
				names << std::string("(Fixed)");
			}

			auto valueP = eops[phoIdx][p].get();
			if (nameP == constant::photo::_omega_ ||
				nameP == constant::photo::_phi_ ||
				nameP == constant::photo::_kappa_)
			{
				valueP = util::Rad2Deg(valueP);
				names << std::string("[deg]");
			}

			names << std::string("\t");
			values << valueP << std::string("\t");
		}

		stResult << names.str() << std::endl;
		stResult << values.str() << std::endl;
	}

	return stResult.str();
}

std::string getObjPtrStr(const std::vector<std::vector<param::Parameter>>& objPts, const double minSigma = 0.0, const unsigned int precision = 9, const std::ios::fmtflags format = std::ios::scientific)
{
	std::stringstream stResult;
	stResult.str("");

	std::stringstream names;
	std::stringstream values;
	values.precision(precision);
	values.setf(format, std::ios::fixed);

	for (unsigned int objIdx = 0; objIdx < objPts.size(); ++objIdx)
	{
		stResult << std::to_string(objIdx) << std::string("th object point: ") << std::endl;

		names.str("");
		values.str("");

		for (unsigned int p = 0; p < objPts[objIdx].size(); ++p)
		{
			names << objPts[objIdx][p].getName();

			if (objPts[objIdx][p].getStDev() <= minSigma)
			{
				names << std::string("(Fixed)");
			}

			names << std::string("\t");
			values << objPts[objIdx][p].get() << std::string("\t");
		}

		stResult << names.str() << std::endl;
		stResult << values.str() << std::endl;
	}

	return stResult.str();
}

bool fileExport(const std::wstring& fname, const std::string& msg)
{
	fstream exportfile;
	exportfile.open(fname, std::ios::out);
	if (!exportfile)
	{
		std::wcout << "Failed in creating a output file: " << fname << std::endl;
		return false;
	}
	exportfile << msg;
	exportfile.close();

	return true;
}

bool fileExportResiduals(const std::wstring& fname,
	const std::vector<std::vector<std::vector<double>>>& obsResiduals,
	const std::vector<std::vector<std::vector<double>>>& paramResiduals)
{
	fstream exportfile;
	exportfile.open(fname, std::ios::out);
	if (!exportfile)
	{
		std::wcout << "Failed in creating a output file: " << fname << std::endl;
		return false;
	}
	
	exportfile.precision(9);
	exportfile.setf(std::ios::scientific, std::ios::fixed);
	exportfile << "Residuals of image observations: " << std::endl << std::endl;
	
	unsigned int phoIdx = 0;
	for (const auto& pho : obsResiduals)
	{
		exportfile << phoIdx ++ << "th photo: " << std::endl;
		for (const auto& img : pho)
		{
			for (const auto& res : img)
			{
				exportfile << res << "\t";
			}
			exportfile << std::endl;
		}
	}

	exportfile << std::endl;

	exportfile.precision(12);
	exportfile.setf(std::ios::scientific, std::ios::fixed);
	exportfile << "Residuals of parameter observations: " << std::endl << std::endl;

	unsigned int camIdx = 0;
	for (const auto& cam : paramResiduals[0])
	{
		exportfile << camIdx++ << "th camera: " << std::endl;
		for (const auto& res : cam)
		{
			exportfile << res << std::endl;
		}
	}

	phoIdx = 0;
	for (const auto& pho : paramResiduals[1])
	{
		exportfile << phoIdx++ << "th photo: " << std::endl;
		for (const auto& res : pho)
		{
			exportfile << res << std::endl;
		}
	}

	unsigned int objIdx = 0;
	for (const auto& obj : paramResiduals[2])
	{
		exportfile << objIdx++ << "th object-point: " << std::endl;
		for (const auto& res : obj)
		{
			exportfile << res << "\t";
		}
		exportfile << std::endl;
	}

	exportfile.close();

	return true;
}

int wmain(int argc, wchar_t* argv[])
{
	char path[_MAX_PATH];
    const char* varName = "glogfolder";
    size_t len;
    getenv_s(&len, path, 80, varName);

    // Initialize Google's logging library.
    std::wstring argv0(argv[0]);
    std::string programName(util::uni2multi(argv0));
    google::InitGoogleLogging(programName.c_str());
    FLAGS_log_dir = path;

    if (argc != 2)
    {
        LOG(INFO) << "Missing input file path (*.prj)";
        std::cout << "Missing input file path (*.prj)";
        return 0;
    }

	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    data::BundleBlockPrj baData;
    LOG(INFO) << "Read project configuration: " << util::uni2multi(std::wstring(argv[1]));
    
    if (!baData.readData(argv[1]))
    {
        LOG(INFO) << "data::BundleBlockData::readData() returns false. Check the input data";
        std::cout << "data::BundleBlockData::readData() returns false. Check the input data";
        return 0;
    }

    ssm::Bundle ba(baData.getPhotos(), 
        baData.getObjectPoints(), 
        baData.getMinSigma(), 
        baData.getMaxSigma(), 
        baData.getMaxIteration(),
        baData.getSigmaThreshold(),
        baData.getMinCorrelation());
    
    LOG(INFO) << "Run bundle block adjustment";
    
    try
    {
		std::stringstream camParameters;
		std::stringstream eopParameters;
		std::stringstream objParameters;
		const double tryLM = false;
		/// Run
        std::string retmsg = ba.run(baData.getProjectName(), camParameters, eopParameters, objParameters, tryLM, ssm::SOLVEMETHOD::REDUCED);

		/// Get final parameters
        auto camParams = ba.getCameraParameters();
        auto eops = ba.getEopParameters();
        auto objPts = ba.getObjPoints();

        /// Export log
        std::wstring outFilePath = baData.getPhotoDataPath();
        boost::replace_all(outFilePath, ".pho", ".ret");
        fstream outfile;
        outfile.open(outFilePath, std::ios::out);
        if (!outfile)
            std::cout << "Failed in creating a output file" << std::endl;
        outfile << retmsg;
        outfile.close();

		unsigned int precision12 = 12;
		unsigned int precision9 = 9;
		unsigned int precision3 = 3;
		std::ios::fmtflags formatSci = std::ios::scientific;
		std::ios::fmtflags formatFix = std::ios::fixed;

		/// Export camera parameters
		std::wstring camFilePath = baData.getPhotoDataPath();
		boost::replace_all(camFilePath, ".pho", "_Cam.ret");
		if(!fileExport(camFilePath, getCamParamStr(camParams, baData.getMinSigma(), precision12, formatSci)))
			std::cout << "Failed in exporting camera parameters" << std::endl;

		std::wstring allCamFilePath = baData.getPhotoDataPath();
		boost::replace_all(allCamFilePath, ".pho", "_AllCam.ret");
		if (!fileExport(allCamFilePath, camParameters.str()))
			std::cout << "Failed in exporting all camera parameters" << std::endl;

		/// Export Eops
		std::wstring eopFilePath = baData.getPhotoDataPath();
		boost::replace_all(eopFilePath, ".pho", "_Eop.ret");
		if (!fileExport(eopFilePath, getEopStr(eops, baData.getMinSigma(), precision9, formatFix)))
			std::cout << "Failed in exporting EO parameters" << std::endl;

		std::wstring allEopFilePath = baData.getPhotoDataPath();
		boost::replace_all(allEopFilePath, ".pho", "_AllEop.ret");
		if (!fileExport(allEopFilePath, eopParameters.str()))
			std::cout << "Failed in exporting all EO parameters" << std::endl;

		/// Export object points
		std::wstring objPtsFilePath = baData.getPhotoDataPath();
		boost::replace_all(objPtsFilePath, ".pho", "_ObjPts.ret");
		if (!fileExport(objPtsFilePath, getEopStr(objPts, baData.getMinSigma(), precision3, formatFix)))
			std::cout << "Failed in exporting object points" << std::endl;

		std::wstring allObjPtsFilePath = baData.getPhotoDataPath();
		boost::replace_all(allObjPtsFilePath, ".pho", "_AllObjPts.ret");
		if (!fileExport(allObjPtsFilePath, objParameters.str()))
			std::cout << "Failed in exporting all object points" << std::endl;

		/// Export residuals
		std::wstring residualPath = baData.getPhotoDataPath();
		boost::replace_all(residualPath, ".pho", "_residuals.ret");
		std::vector<std::vector<std::vector<double>>> obsResiduals = ba.getResidualsOfCollinearity();
		std::vector<std::vector<std::vector<double>>> paramResiduals = ba.getResidualsOfParameters();
		fileExportResiduals(residualPath, obsResiduals, paramResiduals);

		/// Correlation analysis
		std::cout << ba.getCorrelation() << std::endl;

        std::wcout << std::endl << L"Result message: " << outFilePath << std::endl;
    }
    catch (...)
    {
        std::cout << "Exceptional error from the Block adjustment execution" << std::endl;

        std::cout << std::endl;
        std::cout << ba.getLogMsg();
    }

	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

	std::cout << "Elapsed time = " << std::chrono::duration_cast<std::chrono::milliseconds> (end - begin).count() << "[milsec]" << std::endl;

	return 0;
}