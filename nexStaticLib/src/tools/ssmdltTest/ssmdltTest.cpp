// ssmdltTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <sstream>
#include "../../../src/libraries/ssmdlt/ssmdlt.h"

int main()
{
    std::cout << "ssmdltTest ... \n";

    std::vector<smam::_SCENE_> sceneData;
    sceneData.resize(1);
    smam::_ICP_ icp;
    icp.s[0] = 1.0;
    icp.s[1] = 0.0;
    icp.s[2] = 0.0;
    icp.s[3] = 1.0;
    icp.SID = std::string("457");	icp.PID = std::string("4639");	icp.row = 599;	icp.col = 1097; sceneData[0].ICP_List.push_back(icp);
    icp.SID = std::string("457");	icp.PID = std::string("37");	icp.row = 1757;	icp.col = 1101; sceneData[0].ICP_List.push_back(icp);
    icp.SID = std::string("457");	icp.PID = std::string("4636");	icp.row = 2876;	icp.col = 1074; sceneData[0].ICP_List.push_back(icp);
    icp.SID = std::string("457");	icp.PID = std::string("4692");	icp.row = 378;	icp.col = 2758; sceneData[0].ICP_List.push_back(icp);
    icp.SID = std::string("457");	icp.PID = std::string("41");	icp.row = 1769;	icp.col = 2735; sceneData[0].ICP_List.push_back(icp);
    icp.SID = std::string("457");	icp.PID = std::string("4711");	icp.row = 3112;	icp.col = 2682; sceneData[0].ICP_List.push_back(icp);

    std::vector<smam::_GCP_> gcps;
    smam::_GCP_ gcp;
    gcp.s[0] = sqrt(1.0e-6);
    gcp.s[1] = 0.0;
    gcp.s[2] = 0.0;
    gcp.s[3] = 0.0;
    gcp.s[4] = sqrt(1.0e-6);
    gcp.s[5] = 0.0;
    gcp.s[6] = 0.0;
    gcp.s[7] = 0.0;
    gcp.s[8] = sqrt(1.0e-6);

    gcp.PID = std::string("4639");	gcp.X = 2.150073242;	gcp.Y = 3.083609619; gcp.Z = -8.273751953; gcps.push_back(gcp);
    gcp.PID = std::string("37");	    gcp.X = 0.003294769;	gcp.Y = 3.11161499; gcp.Z = -8.307868164; gcps.push_back(gcp);
    gcp.PID = std::string("4636");   gcp.X = -2.183121826;	gcp.Y = 3.081416504; gcp.Z = -8.249383789; gcps.push_back(gcp);
    gcp.PID = std::string("4692");	gcp.X = 2.537445557;	gcp.Y = 3.465658936; gcp.Z = -12.51874512; gcps.push_back(gcp);
    gcp.PID = std::string("41");     gcp.X = -0.008881502;	gcp.Y = 3.494376465; gcp.Z = -12.53336621; gcps.push_back(gcp);
    gcp.PID = std::string("4711");   gcp.X = -2.556125244;	gcp.Y = 3.463904541; gcp.Z = -12.49925098; gcps.push_back(gcp);

    /// Create a dlt instance (shared_ptr)
    auto dltPtr = runDltFactory();
    dltPtr->SetConfigData(sceneData, gcps);
    double gcp_sd_th = 0.1;
    unsigned int num_max_iter = 100;
    double maximum_sd = 1.0e-6;
    double maximum_diff = 1.0e-6;
    double pixelsize = 0.004246;//mm
    bool normalize = true;
    std::string outfilename = "F:\\Users\\bbarab\\Downloads\\powerTower\\ssmdltTest_457.out";
    
    std::cout << dltPtr->DoModeling(gcp_sd_th, num_max_iter, maximum_sd, maximum_diff, pixelsize, normalize, outfilename) << std::endl;

    auto parameters = dltPtr->getParameters();

    std::cout.precision(9);
    std::cout.setf(std::ios::floatfield, std::ios::fixed);

    std::cout << "DLT parameters" << std::endl;

    for (const auto& scene : parameters)
    {
        for (const auto& p : scene)
        {
            std::cout << p << "\t";
        }

        std::cout << std::endl;
    }

    std::cout << "Rigorous parameters [x, y, z, omg, phi, kap, xp, yp, f]" << std::endl;

    for (const auto& p : parameters)
    {
        /// x, y, z, omg, phi, kap, xp, yp, f
        auto rp = smam::dlt2rigorous(p);

        for (const auto& p : rp)
        {
            std::cout << p << "\t";
        }

        std::cout << std::endl;
    }
}