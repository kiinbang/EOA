#include "pch.h"

#include <fcntl.h> // for _O_U16TEXT
#include <wchar.h>
#include <vector>

#define GOOGLE_GLOG_DLL_DECL
#define GLOG_NO_ABBREVIATED_SEVERITIES

#include <ssm/include/BundleBlockDataReader.hpp>
#include <ssm/include/SSMCollinearity.h>
#include <ssm/include/SSMIntersectionLinear.h>
#include <ssm/include/NumericPDE.h>
#include <ssm/include/SSMDerivedMatrix.h>
#include <ssm/include/SsmMatrix.h>
#include <ssm/include/SsmMatrixd.h>
#include <ssm/include/SSMFixedSizeMatd.h>
#include <ssm/include/SSMVectorMatd.h>
//#include <ssm/include/Texture.h>

#include <ssm/include/BundleBlockData.h>
#include <ssm/include/SSMBundle.h>
#include <ssm/include/utilitygrocery.h>

data::Point3D grdPt;
const double pi = util::GetPI();
const double f = 60.58;
data::Point2D pp;
unsigned int w = 8984;
unsigned int h = 6732;
double pixSize = 0.006;
double k[4] = { 0., 0., 0., 0. }, p[3] = { 0., 0., 0. };
std::vector<std::shared_ptr<data::PhotoData>> photos;
std::shared_ptr<sensor::DistortionModel> distortion(new sensor::SMACModel(k, p));
std::vector<std::shared_ptr<data::EOP>> eop;
std::vector<data::Point2D> imgPts; /// image points coordinates

void setupTestDataWithDistortion_NEW()
{
	/// no. 317 object point
	grdPt(0) = 100136.39610;
	grdPt(1) = 99777.81746;
	grdPt(2) = 92.06897;

	pp(0) = 0.040527f;
	pp(1) = 0.018668f;

	k[0] = 0.000000;
	k[1] = 0.360236;
	k[2] = -0.854961;
	k[3] = 2.164434;

	p[0] = 0.000497;
	p[1] = -0.000579;
	p[2] = 0.0;

	distortion.reset(new sensor::SMACModel(k, p));

	std::shared_ptr<sensor::FrameCamera> cam(new sensor::FrameCamera(0, 1.0, pp, 640, 480, 0.001886, distortion));
	std::shared_ptr<sensor::FrameCamera> cam2(new sensor::FrameCamera(1, 1.0, pp, 640, 480, 0.001886, distortion));

	/// EOPs
	eop.resize(2);
	eop[0] = make_shared<data::EOP>();
	eop[1] = make_shared<data::EOP>();

	std::vector<data::EulerAngles> opk(2);
	opk[0](0) = 0.0 / 180. * pi;
	opk[0](1) = 0.0 / 180. * pi;
	opk[0](2) = -90.0 / 180. * pi;

	opk[1](0) = 0.0 / 180. * pi;
	opk[1](1) = 0.0 / 180. * pi;
	opk[1](2) = -90.0 / 180. * pi;

	std::vector <data::Point3D> pc(2);
	pc[0](0) = 100133.2;
	pc[0](1) = 100000.0;
	pc[0](2) = 500.0;

	data::Point3D pc2;
	pc[1](0) = 100266.4;
	pc[1](1) = 100000.0;
	pc[1](2) = 500.0;

	eop[0]->setPC(pc[0]);
	eop[0]->setEulerAngles(opk[0]);
	eop[1]->setPC(pc[1]);
	eop[1]->setEulerAngles(opk[1]);

	/// object point
	std::vector<std::shared_ptr<data::ImagePointData>> pt(2);
	pt[0] = std::make_shared<data::ImagePointData>();
	pt[1] = std::make_shared<data::ImagePointData>();

	pt[0]->pts.resize(1);
	pt[1]->pts.resize(1);

	/// distorted photo coordinates of no. 317 point in no. 0 and no. 1 images
	pt[0]->pts[0](0) = 0.50727;
	pt[0]->pts[0](1) = 0.00744;

	pt[1]->pts[0](0) = 0.49369;
	pt[1]->pts[0](1) = -0.28878;

	data::Point2D photoPt;
	photoPt(0) = pt[0]->pts[0](0);
	photoPt(1) = pt[0]->pts[0](1);
	auto imgPt0 = cam->getPhoto2ImgCoord(photoPt);
	photoPt(0) = pt[1]->pts[0](0);
	photoPt(1) = pt[1]->pts[0](1);
	auto imgPt1 = cam->getPhoto2ImgCoord(photoPt);

	imgPts.resize(2);
	imgPts[0](0) = imgPt0(0);/// col
	imgPts[0](1) = imgPt0(1);///row
	imgPts[1](0) = imgPt1(0);///col
	imgPts[1](1) = imgPt1(1);///row	

	/// create photos
	photos.resize(2);
	photos[0] = make_shared<data::PhotoData>(cam, eop[0], pt[0], 0);
	photos[1] = make_shared<data::PhotoData>(cam2, eop[1], pt[1], 1);
}

/**< Test for List class*/
TEST(TestSsmList, resize)
{

	unsigned int listSize = 10;
	std::vector<int> list(10);

	auto temp1 = list[1];
	auto temp0 = list[0];

	for (unsigned int i = 0; i < listSize; ++i)
	{
		list[i] = i;
	}

	auto sizeOfList = list.size();
	EXPECT_EQ(listSize, sizeOfList);

	list.push_back(int(10));
	sizeOfList = list.size();
	EXPECT_EQ(++listSize, sizeOfList);

	std::vector<std::shared_ptr<int>> listPtr(10);
	for (unsigned int i = 0; i < listPtr.size(); ++i)
	{
		listPtr[i] = std::make_shared<int>(i);
	}

	std::vector<std::shared_ptr<int>> listPtrCopy = listPtr;

	EXPECT_EQ(listPtr.size(), listPtrCopy.size());

	for (unsigned int i = 0; i < listPtr.size(); ++i)
	{
		EXPECT_EQ(listPtr[i], listPtrCopy[i]);
		EXPECT_EQ(*listPtr[i], *listPtrCopy[i]);
	}
}

TEST(TestSsmList, removeElements)
{

	unsigned int listSize = 10;
	std::vector<int> list(10);

	for (unsigned int i = 0; i < listSize; ++i)
	{
		list[i] = i;
	}

	auto sizeOfList = list.size();
	EXPECT_EQ(listSize, sizeOfList);

	unsigned int n = 0;
	for (unsigned int i = 0; i < list.size(); ++i)
	{
		if (list[i] % 2 == 0)
		{
			list[i] = -999;
			++n;
		}
	}

	list.erase(std::remove_if(list.begin(), list.end(), [](int& test) { return (test == -999); }), list.end());

	for (unsigned int i = 0; i < list.size(); ++i)
	{
		std::cout << list[i] << std::endl;
	}

	unsigned int newListSize = listSize - n;
	sizeOfList = list.size();
	EXPECT_EQ(newListSize, sizeOfList);
}

/**< Test for Matrix template class*/
TEST(TestMatrix, inverse)
{

	math::Matrix<double> a(4, 4);
	a(0, 0) = 3.0;
	a(0, 1) = 5.0;
	a(0, 2) = 7.0;
	a(0, 3) = 2.0;

	a(1, 0) = 1.0;
	a(1, 1) = 4.0;
	a(1, 2) = 7.0;
	a(1, 3) = 2.0;

	a(2, 0) = 6.0;
	a(2, 1) = 3.0;
	a(2, 2) = 9.0;
	a(2, 3) = 17.0;

	a(3, 0) = 13.0;
	a(3, 1) = 5.0;
	a(3, 2) = 4.0;
	a(3, 3) = 16.0;

	std::cout << "a matrix: " << std::endl;
	std::cout << math::matrixout(a) << std::endl;

	auto aInv = a.inverse();

	std::cout << "Inverse matrix of a: " << std::endl;
	std::cout << math::matrixout(aInv) << std::endl;

	auto check = a % aInv;
	std::cout << "check: a % aInv: " << std::endl;
	std::cout << math::matrixout(check) << std::endl;

	double sum = check.getSum();

	double diff = fabs(sum - static_cast<double>(check.getRows())) / check.getSize();
	EXPECT_LT(diff, std::numeric_limits<double>::epsilon());

	if (diff < std::numeric_limits<double>::epsilon())
		std::cout << "Inverse result is OK!!!" << std::endl;
	else
	{
		std::cout << "diff:/t" << diff << std::endl;
		std::cout << "epsilon:/t" << std::numeric_limits<double>::epsilon() << std::endl;
	}

	a.resize(3, 3);
	a(0, 0) = 1.0;
	a(0, 1) = -1.0;
	a(0, 2) = 3.0;

	a(1, 0) = 2.0;
	a(1, 1) = 1.0;
	a(1, 2) = 2.0;

	a(2, 0) = -2.0;
	a(2, 1) = -2.0;
	a(2, 2) = 1.0;

	std::cout << "a matrix: " << std::endl;
	std::cout << math::matrixout(a) << std::endl;

	aInv = a.inverse();

	std::cout << "Inverse matrix of a: " << std::endl;
	std::cout << math::matrixout(aInv) << std::endl;

	check = a % aInv;
	std::cout << "check: a % aInv: " << std::endl;
	std::cout << math::matrixout(check) << std::endl;

	sum = check.getSum();

	diff = fabs(sum - static_cast<double>(check.getRows())) / check.getSize();
	EXPECT_LT(diff, std::numeric_limits<double>::epsilon());

	if (diff < std::numeric_limits<double>::epsilon())
		std::cout << "Inverse result is OK!!!" << std::endl;
	else
	{
		std::cout << "diff:/t" << diff << std::endl;
		std::cout << "epsilon:/t" << std::numeric_limits<double>::epsilon() << std::endl;
	}

	a.resize(2, 2);
	a(0, 0) = 3.0;
	a(0, 1) = 4.0;

	a(1, 0) = 1.0;
	a(1, 1) = 2.0;

	std::cout << "a matrix: " << std::endl;
	std::cout << math::matrixout(a) << std::endl;

	aInv = a.inverse();

	std::cout << "Inverse matrix of a: " << std::endl;
	std::cout << math::matrixout(aInv) << std::endl;

	check = a % aInv;
	std::cout << "check: a % aInv: " << std::endl;
	std::cout << math::matrixout(check) << std::endl;

	sum = check.getSum();

	diff = fabs(sum - static_cast<double>(check.getRows())) / check.getSize();
	EXPECT_LT(diff, std::numeric_limits<double>::epsilon());

	if (diff < std::numeric_limits<double>::epsilon())
		std::cout << "Inverse result is OK!!!" << std::endl;
	else
	{
		std::cout << "diff:/t" << diff << std::endl;
		std::cout << "epsilon:/t" << std::numeric_limits<double>::epsilon() << std::endl;
	}
}

TEST(TestMatrix, inversePartitionedMatrix)
{

	math::Matrix<double> a(4, 4);
	a(0, 0) = 3.0;
	a(0, 1) = 5.0;
	a(0, 2) = 7.0;
	a(0, 3) = 2.0;

	a(1, 0) = 1.0;
	a(1, 1) = 4.0;
	a(1, 2) = 7.0;
	a(1, 3) = 2.0;

	a(2, 0) = 6.0;
	a(2, 1) = 3.0;
	a(2, 2) = 9.0;
	a(2, 3) = 17.0;

	a(3, 0) = 13.0;
	a(3, 1) = 5.0;
	a(3, 2) = 4.0;
	a(3, 3) = 16.0;

	std::cout << "a matrix: " << std::endl;
	std::cout << math::matrixout(a) << std::endl;

	auto aInv = a.inverse();

	std::cout << "aInv matrix: " << std::endl;
	std::cout << math::matrixout(aInv) << std::endl;

	auto check0 = a % aInv;
	auto sum0 = check0.getSum();

	auto diff0 = fabs(sum0 - static_cast<double>(check0.getRows())) / check0.getSize();
	EXPECT_LT(diff0, std::numeric_limits<double>::epsilon());

	math::Matrix<double> m11, m12, m21, m22;
	m11 = a.getSubset(0, 0, 2, 2);
	m12 = a.getSubset(0, 2, 2, 2);
	m21 = a.getSubset(2, 0, 2, 2);
	m22 = a.getSubset(2, 2, 2, 2);

	std::cout << "m11 matrix: " << std::endl;
	std::cout << math::matrixout(m11) << std::endl;
	std::cout << "m12 matrix: " << std::endl;
	std::cout << math::matrixout(m12) << std::endl;
	std::cout << "m21 matrix: " << std::endl;
	std::cout << math::matrixout(m21) << std::endl;
	std::cout << "m22 matrix: " << std::endl;
	std::cout << math::matrixout(m22) << std::endl;

	auto check = a % aInv;
	std::cout << "check: a % aInv: " << std::endl;
	std::cout << math::matrixout(check) << std::endl;

	auto sum = check.getSum();

	auto diff = fabs(sum - static_cast<double>(check.getRows())) / check.getSize();
	EXPECT_LT(diff, std::numeric_limits<double>::epsilon());

	auto aInv2 = math::inversePartitionedMatrix(m11, m12, m21, m22);
	std::cout << "aInv2 matrix: " << std::endl;
	std::cout << math::matrixout(aInv2) << std::endl;

	auto check2 = a % aInv2;
	std::cout << "check: a % aInv2: " << std::endl;
	std::cout << math::matrixout(check2) << std::endl;

	auto sum2 = check2.getSum();

	auto diff2 = fabs(sum2 - static_cast<double>(check2.getRows())) / check2.getSize();
	EXPECT_LT(diff2, std::numeric_limits<double>::epsilon() * 10.0);
}

/**< Test for matrixd (double type) class*/
TEST(TestMatrixd, inverse)
{

	math::Matrixd a(4, 4);
	a(0, 0) = 3.0;
	a(0, 1) = 5.0;
	a(0, 2) = 7.0;
	a(0, 3) = 2.0;

	a(1, 0) = 1.0;
	a(1, 1) = 4.0;
	a(1, 2) = 7.0;
	a(1, 3) = 2.0;

	a(2, 0) = 6.0;
	a(2, 1) = 3.0;
	a(2, 2) = 9.0;
	a(2, 3) = 17.0;

	a(3, 0) = 13.0;
	a(3, 1) = 5.0;
	a(3, 2) = 4.0;
	a(3, 3) = 16.0;

	std::cout << "a matrix: " << std::endl;
	std::cout << math::matrixout(a) << std::endl;

	auto aInv = a.inverse();

	std::cout << "Inverse matrix of a: " << std::endl;
	std::cout << math::matrixout(aInv) << std::endl;

	auto check = a % aInv;
	std::cout << "check: a % aInv: " << std::endl;
	std::cout << math::matrixout(check) << std::endl;

	double sum = check.getSum();

	double diff = fabs(sum - static_cast<double>(check.getRows())) / check.getSize();
	EXPECT_LT(diff, std::numeric_limits<double>::epsilon());

	if (diff < std::numeric_limits<double>::epsilon())
		std::cout << "Inverse result is OK!!!" << std::endl;
	else
	{
		std::cout << "diff:/t" << diff << std::endl;
		std::cout << "epsilon:/t" << std::numeric_limits<double>::epsilon() << std::endl;
	}

	a.resize(3, 3);
	a(0, 0) = 1.0;
	a(0, 1) = -1.0;
	a(0, 2) = 3.0;

	a(1, 0) = 2.0;
	a(1, 1) = 1.0;
	a(1, 2) = 2.0;

	a(2, 0) = -2.0;
	a(2, 1) = -2.0;
	a(2, 2) = 1.0;

	std::cout << "a matrix: " << std::endl;
	std::cout << math::matrixout(a) << std::endl;

	aInv = a.inverse();

	std::cout << "Inverse matrix of a: " << std::endl;
	std::cout << math::matrixout(aInv) << std::endl;

	check = a % aInv;
	std::cout << "check: a % aInv: " << std::endl;
	std::cout << math::matrixout(check) << std::endl;

	sum = check.getSum();

	diff = fabs(sum - static_cast<double>(check.getRows())) / check.getSize();
	EXPECT_LT(diff, std::numeric_limits<double>::epsilon());

	if (diff < std::numeric_limits<double>::epsilon())
		std::cout << "Inverse result is OK!!!" << std::endl;
	else
	{
		std::cout << "diff:/t" << diff << std::endl;
		std::cout << "epsilon:/t" << std::numeric_limits<double>::epsilon() << std::endl;
	}

	a.resize(2, 2);
	a(0, 0) = 3.0;
	a(0, 1) = 4.0;

	a(1, 0) = 1.0;
	a(1, 1) = 2.0;

	std::cout << "a matrix: " << std::endl;
	std::cout << math::matrixout(a) << std::endl;

	aInv = a.inverse();

	std::cout << "Inverse matrix of a: " << std::endl;
	std::cout << math::matrixout(aInv) << std::endl;

	check = a % aInv;
	std::cout << "check: a % aInv: " << std::endl;
	std::cout << math::matrixout(check) << std::endl;

	sum = check.getSum();

	diff = fabs(sum - static_cast<double>(check.getRows())) / check.getSize();
	EXPECT_LT(diff, std::numeric_limits<double>::epsilon());

	if (diff < std::numeric_limits<double>::epsilon())
		std::cout << "Inverse result is OK!!!" << std::endl;
	else
	{
		std::cout << "diff:/t" << diff << std::endl;
		std::cout << "epsilon:/t" << std::numeric_limits<double>::epsilon() << std::endl;
	}
}

TEST(TestMatrixd, inversePartitionedMatrix)
{

	math::Matrixd a(4, 4);
	a(0, 0) = 3.0;
	a(0, 1) = 5.0;
	a(0, 2) = 7.0;
	a(0, 3) = 2.0;

	a(1, 0) = 1.0;
	a(1, 1) = 4.0;
	a(1, 2) = 7.0;
	a(1, 3) = 2.0;

	a(2, 0) = 6.0;
	a(2, 1) = 3.0;
	a(2, 2) = 9.0;
	a(2, 3) = 17.0;

	a(3, 0) = 13.0;
	a(3, 1) = 5.0;
	a(3, 2) = 4.0;
	a(3, 3) = 16.0;

	std::cout << "a matrix: " << std::endl;
	std::cout << math::matrixout(a) << std::endl;

	auto aInv = a.inverse();

	std::cout << "aInv matrix: " << std::endl;
	std::cout << math::matrixout(aInv) << std::endl;

	auto check0 = a % aInv;
	auto sum0 = check0.getSum();

	auto diff0 = fabs(sum0 - static_cast<double>(check0.getRows())) / check0.getSize();
	EXPECT_LT(diff0, std::numeric_limits<double>::epsilon());

	math::Matrixd m11, m12, m21, m22;
	m11 = a.getSubset(0, 0, 2, 2);
	m12 = a.getSubset(0, 2, 2, 2);
	m21 = a.getSubset(2, 0, 2, 2);
	m22 = a.getSubset(2, 2, 2, 2);

	std::cout << "m11 matrix: " << std::endl;
	std::cout << math::matrixout(m11) << std::endl;
	std::cout << "m12 matrix: " << std::endl;
	std::cout << math::matrixout(m12) << std::endl;
	std::cout << "m21 matrix: " << std::endl;
	std::cout << math::matrixout(m21) << std::endl;
	std::cout << "m22 matrix: " << std::endl;
	std::cout << math::matrixout(m22) << std::endl;

	auto check = a % aInv;
	std::cout << "check: a % aInv: " << std::endl;
	std::cout << math::matrixout(check) << std::endl;

	auto sum = check.getSum();

	auto diff = fabs(sum - static_cast<double>(check.getRows())) / check.getSize();
	EXPECT_LT(diff, std::numeric_limits<double>::epsilon());

	auto aInv2 = math::inversePartitionedMatrixd(m11, m12, m21, m22);
	std::cout << "aInv2 matrix: " << std::endl;
	std::cout << math::matrixout(aInv2) << std::endl;

	auto check2 = a % aInv2;
	std::cout << "check: a % aInv2: " << std::endl;
	std::cout << math::matrixout(check2) << std::endl;

	auto sum2 = check2.getSum();

	auto diff2 = fabs(sum2 - static_cast<double>(check2.getRows())) / check2.getSize();
	EXPECT_LT(diff2, std::numeric_limits<double>::epsilon() * 10.0);
}

/**< Test for fixed-size matrix template class*/
TEST(TestFixedSizeMatrix, BasicOperation)
{

	math::FixedSizeMat<double, 2, 2> a(1.0);
	math::FixedSizeMat<double, 2, 2> b(2.0);

	auto c = a + b;

	std::cout << "c = a + b" << std::endl;
	std::cout << math::matrixout(c) << std::endl;
}

/**< Test for vector template class*/
TEST(TestVectorMatrix, BasicOperation)
{

	math::VectorMat<double, 3> a(1.0);
	math::VectorMat<double, 3> b(2.0);

	auto c = a + b;

	std::cout << "c = a + b" << std::endl;
	std::cout << math::matrixout(c) << std::endl;
}

/**< Test for fixed-size matrix (double type) class*/
TEST(TestFixedSizeMatrixDouble, BasicOperation)
{

	math::FixedSizeMatd<2, 2> a(1.0);
	math::FixedSizeMatd<2, 2> b(2.0);

	auto c = a + b;

	std::cout << "c = a + b" << std::endl;
	std::cout << math::matrixout(c) << std::endl;
}

/**< Test for vector (double type) class*/
TEST(TestVectorMatrixDouble, BasicOperation)
{
	try
	{
		math::VectorMatd<3> a(1.0);
		math::VectorMatd<3> b(2.0);

		auto c = a + b;

		std::cout << "c = a + b" << std::endl;
		std::cout << math::matrixout(c) << std::endl;
	}
	catch (...)
	{
		std::cout << "exceptional error" << std::endl;
	}
}

/**< Test for utilitygrocery */
TEST(TestUtilitygrocery, PtLineIntersection2D)
{
	double p0x = 1.0;
	double p0y = 1.0;
	double p0z = 1.0;

	double pax = 0.0;
	double pay = 0.0;
	double paz = 0.0;

	double pbx = 2.0;
	double pby = 0.0;
	double pbz = 0.0;

	double px, py, pz, norm;

	util::IntersectPointBetweenOnePointandOneLine(p0x, p0y, p0z,
		pax, pay, paz,
		pbx, pby, pbz,
		px, py, pz,
		norm);

	double normRef = sqrt(2.0);

	EXPECT_EQ(norm, normRef);

	EXPECT_EQ(px, 1.0);
	EXPECT_EQ(py, 0.0);
	EXPECT_EQ(pz, 0.0);
}

/**< Test for utilitygrocery */
TEST(TestUtilitygrocery, unicodeConversionTest)
{
	std::wstring uniStr;
	std::string multiStr;

	std::string msg = "It is string";

	std::cout << msg << std::endl;

	uniStr = util::multi2uni(msg);

	std::wcout << uniStr << std::endl;

	multiStr = util::uni2multi(uniStr);

	std::cout << multiStr << std::endl;

	EXPECT_EQ((multiStr == msg), true);
}

math::Matrixd myFunc(const std::vector<std::shared_ptr<param::Parameter>>& givenParams, const std::vector<std::shared_ptr<param::Parameter>>& givenObs)
{
	/// y = a*x*x + b*sin(x) + c
	math::Matrixd f(1, 1);

	double a = givenParams[0]->get();
	double b = givenParams[1]->get();
	double c = givenParams[2]->get();
	auto x = givenObs[0]->get();
	f(0) = a * x * x + b * sin(x) + c;

	return f;
}

math::Matrixd myFunc_PDE(const std::vector<std::shared_ptr<param::Parameter>>& givenParams, const std::vector<std::shared_ptr<param::Parameter>>& givenObs)
{
	/// y = a*x*x + b*sin(x) + c
	/// dy/da = x*x
	/// dy/db = sin(x)
	/// dy/dc = 1.0
	/// dy/dx = 2*a*x + b*cos(x)

	double a = givenParams[0]->get();
	double b = givenParams[1]->get();
	double c = givenParams[2]->get();
	auto x = givenObs[0]->get();

	math::Matrixd dy(4, 1);
	dy(0, 0) = x * x;
	dy(1, 0) = sin(x);
	dy(2, 0) = 1.0;
	dy(3, 0) = 2. * a * x + b * cos(x);

	return dy;
}

/**< Test for numeric partial derivative equation(PDE) */
TEST(TestMath, NumericPDE)
{
	std::vector<std::shared_ptr<param::Parameter>> givenParams(3);
	givenParams[0] = std::make_shared<param::Parameter>();
	givenParams[0]->set(1.0);
	givenParams[1] = std::make_shared<param::Parameter>();
	givenParams[1]->set(2.0);
	givenParams[2] = std::make_shared<param::Parameter>();
	givenParams[2]->set(3.0);

	std::vector<std::shared_ptr<param::Parameter>> givenObs(1);
	givenObs[0] = std::make_shared<param::Parameter>();
	givenObs[0]->set(1.0);

	math::Matrixd a, b, l;
	math::NPDETYPE npdeType = math::NPDETYPE::FourthOrder;

	math::getNumericPDEWithParameters(myFunc, givenParams, givenObs, a, b, l, npdeType);

	math::Matrixd dy = myFunc_PDE(givenParams, givenObs);

	std::cout << fabs(dy(0, 0) - a(0, 0)) << std::endl;
	std::cout << fabs(dy(1, 0) - a(0, 1)) << std::endl;
	std::cout << fabs(dy(2, 0) - a(0, 2)) << std::endl;
	std::cout << fabs(dy(3, 0) - b(0, 0)) << std::endl;

	EXPECT_LT(fabs(dy(0) - a(0, 0)), 0.000001);
	EXPECT_LT(fabs(dy(1) - a(0, 1)), 0.000001);
	EXPECT_LT(fabs(dy(2) - a(0, 2)), 0.000001);
	EXPECT_LT(fabs(dy(3) - b(0, 0)), 0.000001);
}

/**< Test for reading a xml files */
TEST(TestXml, calibrationPrjFile)
{
	std::wstring prjPath = L".\\..\\..\\..\\testData\\calibration.prj";
	data::Project prj;
	EXPECT_EQ(prj.readProject(prjPath), true);
	EXPECT_LT(fabs(prj.project.config.minSigma - 1.0e-6), std::numeric_limits<double>::epsilon());
	EXPECT_LT(fabs(prj.project.config.maxSigma - 1.0e+6), std::numeric_limits<double>::epsilon());
	EXPECT_LT(fabs(prj.project.config.minCorrelation - 0.95), std::numeric_limits<double>::epsilon());
	EXPECT_EQ(prj.project.iteration.maxIteration, 30);
	EXPECT_LT(fabs(prj.project.iteration.sigma_threshold - 1.0e-9), std::numeric_limits<double>::epsilon());
}

/**< math::Matrix test */
TEST(Matrix, TestForInsertPlus)
{
	std::vector<double> vec;

	math::Matrix<double> emptyMat(0, 0, 0.0);
	std::cout << "Empty-Mat.getRows() returns ... " << emptyMat.getRows() << std::endl;
	EXPECT_TRUE(0 == emptyMat.getRows());
	math::Matrix<double> temp(3, 3, 1.2);
	std::cout << "Run insertPlusResize: (EmptyMat + 3x3 mat) ... " << std::endl;
	emptyMat.insertPlusResize(0, 0, temp);
	std::cout << "(EmptyMat + 3x3 mat).getRows() returns ... " << emptyMat.getRows() << std::endl;
	EXPECT_TRUE(3 == emptyMat.getRows());
	EXPECT_TRUE(3 == emptyMat.getCols());
}

/**< math::Matrix test */
TEST(Matrix, TestForInsert)
{
	math::Matrix<double> emptyMat(0, 0, 0.0);
	std::cout << "math::Matrix<double> emptyMat(0, 0, 0.0)" << std::endl;
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;
	std::cout << "emptyMat.getCols(): " << emptyMat.getCols() << std::endl;
	EXPECT_TRUE(0 == emptyMat.getRows());
	EXPECT_TRUE(0 == emptyMat.getCols());

	math::Matrix<double> temp(3, 3, 1.2);
	std::cout << "math::Matrix<double> temp(3, 3, 1.2)" << std::endl;
	std::cout << "temp.getRows(): " << emptyMat.getRows() << std::endl;
	std::cout << "temp.getCols(): " << emptyMat.getCols() << std::endl;

	emptyMat.insert(0, 0, temp * 10.);
	std::cout << "emptyMat.insert(0, 0, temp * 10.)" << std::endl;
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;
	std::cout << "emptyMat.getCols(): " << emptyMat.getCols() << std::endl;

	EXPECT_TRUE(emptyMat.getRows() == temp.getRows());
	EXPECT_TRUE(emptyMat.getCols() == temp.getCols());

	for (unsigned int r = 0; r < emptyMat.getRows(); ++r)
	{
		for (unsigned int c = 0; c < emptyMat.getCols(); ++c)
		{
			EXPECT_TRUE(emptyMat(r, c) == temp(r, c) * 10.0);
		}
	}
}

/* math::Matrix test for the partitioned inverse matrix */
TEST(Matrix, PatitionedInverse)
{
	unsigned int sizer = 2;
	unsigned int sizec = 2;
	math::Matrix<double> invTest(sizer * 2, sizec * 2, 0.0);
	int idx = 0;
	for (unsigned int r = 0; r < sizer * 2; ++r)
	{
		for (unsigned int c = 0; c < sizec * 2; ++c)
		{
			invTest(r, c) = ((++idx) * (c + 1) * (r + 1)) % 7;
		}
	}

	std::cout << "invTest" << std::endl;
	std::cout << math::matrixout(invTest) << std::endl;

	auto invmat = invTest.inverse();
	if (invmat.getRows() == 0 && invmat.getCols() == 0)
	{
		std::cout << "Error in getting an inverse matrix" << std::endl;
		//throw std::runtime_error("Error in inverse");
		EXPECT_TRUE(false);
	}
	else
	{
		std::cout << "invmat" << std::endl;
		std::cout << math::matrixout(invmat) << std::endl;
		std::cout << "invmat % invTest should result in a identity matrix" << std::endl;
		std::cout << math::matrixout(invmat % invTest) << std::endl;

		auto m11 = invTest.getSubset(0, 0, sizer, sizec);
		auto m12 = invTest.getSubset(0, sizec, sizer, sizec);
		auto m21 = invTest.getSubset(sizer, 0, sizer, sizec);
		auto m22 = invTest.getSubset(sizer, sizec, sizer, sizec);

		auto invmat2 = math::inversePartitionedMatrix(m11, m12, m21, m22);
		std::cout << std::endl;
		std::cout << "invmat2" << std::endl;
		std::cout << math::matrixout(invmat2) << std::endl;

		std::cout << std::endl;
		std::cout << "invmat2 % invTest result in a identity matrix" << std::endl;
		auto checkMat = invmat2 % invTest;
		auto squareSum = checkMat.getSumOfSquares();
		std::cout << "squareSum of mat_inv % mat: " << squareSum << std::endl;
		EXPECT_TRUE(squareSum < (0.001 + checkMat.getRows()));

		std::cout << math::matrixout(checkMat) << std::endl;

		std::cout << std::endl;
		std::cout << "invmat - invmat2 shows the difference between the ordinary inverse matrix and the partitioned matrix inverse " << std::endl;
		checkMat = invmat - invmat2;
		squareSum = checkMat.getSumOfSquares();
		std::cout << "squareSum of (invmat - invmat2): " << squareSum << std::endl;
		std::cout << math::matrixout(invmat - invmat2) << std::endl;
		EXPECT_TRUE(squareSum < (0.001));
	}
}

/* math::Matrix test for matrix sum and squaresum */
TEST(Matrix, testForMatrixElementSum)
{
	//
	// Matrix tests
	//

	math::Matrix<double> a(3, 3, 9);
	std::cout << "sum of matrix" << std::endl;
	std::cout << a.getSum() << std::endl;
	EXPECT_EQ(a.getSum(), double(9 * 9));

	std::cout << "square sum" << std::endl;
	std::cout << a.getSumOfSquares() << std::endl;
	EXPECT_EQ(a.getSumOfSquares(), double(9 * 9 * 9));

	auto aa = a * a;
	std::cout << "sum of matrix" << std::endl;
	std::cout << aa.getSum() << std::endl;
	EXPECT_EQ(aa.getSum(), double(9 * 9 * 9));

}

/* math::Matrix test for assigning matrix to Point3D */
TEST(Matrix, assignMatrixToPoint3D)
{
	math::Matrix<double> vec0(3, 1, 1.1);
	data::Point3D pt3d(vec0);
	std::cout << "pt3d(vec0)" << std::endl;
	std::cout << pt3d(0) << "\t" << pt3d(1) << "\t" << pt3d(2) << std::endl;
	EXPECT_EQ(vec0.getSum(), pt3d.getSum());

	data::Point3D pt3d2;
	pt3d2 = vec0;
	std::cout << "pt3d2 = vec0" << std::endl;
	std::cout << pt3d2(0) << "\t" << pt3d2(1) << "\t" << pt3d2(2) << std::endl;
	EXPECT_EQ(vec0.getSum(), pt3d2.getSum());

	math::Matrix<double> vec1(vec0);
	std::cout << "vec1(vec0)" << std::endl;
	std::cout << vec1(0) << "\t" << vec1(1) << "\t" << vec1(2) << std::endl;
	EXPECT_EQ(vec1.getSum(), vec0.getSum());

	vec0 *= 2.0;
	vec1 = vec0;
	std::cout << "vec1 = vec0" << std::endl;
	std::cout << vec1(0) << "\t" << vec1(1) << "\t" << vec1(2) << std::endl;
	EXPECT_EQ(vec1.getSum(), vec0.getSum());
}

/* math::Matrix test for matrix minus operator */
TEST(Matrix, testMinusOperatorOfMatrix)
{
	math::Matrix<double> A(2, 2);
	A(0, 0) = 1.0;
	A(0, 1) = -2.0;
	A(1, 0) = 3.0;
	A(1, 1) = -4.0;

	math::Matrix<double> B = -A;

	std::cout << "A matrix" << std::endl;
	std::cout << math::matrixout(A) << std::endl;
	std::cout << "-A matrix" << std::endl;
	std::cout << math::matrixout(B) << std::endl;
	std::cout << "A + B should be Zero Matrix" << std::endl;
	std::cout << math::matrixout(A + B) << std::endl;

	auto AB = A + B;

	for (unsigned int r = 0; r < AB.getRows(); ++r)
	{
		for (unsigned int c = 0; c < AB.getCols(); ++c)
		{
			EXPECT_TRUE(AB(r, c) == 0.0);
		}
	}
}

/* collinearity intersection */
TEST(ssm_Collinearity, utestIntersectionCollinearityEq)
{
	cout.precision(4);
	cout.setf(ios::fixed, ios::floatfield);

	//
	// Intersection tests
	//

	std::cout << "-----Intersection test using distorted photo coordinates------" << std::endl << std::endl;

	//setupTestDataWithoutDistortion();
	setupTestDataWithDistortion_NEW();

	auto eop0 = photos[0]->getEop();
	auto eop1 = photos[1]->getEop();

	std::cout << "Ground(object) point coordinates (reference values): " << std::endl << grdPt(0) << "\t" << grdPt(1) << "\t" << grdPt(2) << std::endl;

	data::Point3D objPt = ssm::runLinearIntersectionStereo(*photos[0], *photos[1]);
	std::cout << "runLinearIntersectionStereo" << std::endl;
	cout << objPt(0) << "\t" << objPt(1) << "\t" << objPt(2) << std::endl;
	cout << "Diff (computed Pt - grdPt)\t" << objPt(0) - grdPt(0) << "\t" << objPt(1) - grdPt(1) << "\t" << objPt(2) - grdPt(2) << std::endl;
	cout << "B/H:\t" << (eop0->getPC()(0) - eop1->getPC()(0)) / (eop0->getPC()(2) - grdPt(2)) << std::endl;

	EXPECT_TRUE(fabs(objPt(0) - grdPt(0)) < 0.01);
	EXPECT_TRUE(fabs(objPt(1) - grdPt(1)) < 0.01);
	EXPECT_TRUE(fabs(objPt(2) - grdPt(2)) < 0.02);

	objPt = ssm::runLinearIntersectionMultiNormalMat(photos);
	std::cout << "runLinearIntersectionMultiNormalMat" << std::endl;
	cout << objPt(0) << "\t" << objPt(1) << "\t" << objPt(2) << std::endl;
	cout << "Diff (computed Pt - grdPt)" << objPt(0) - grdPt(0) << "\t" << objPt(1) - grdPt(1) << "\t" << objPt(2) - grdPt(2) << std::endl;
	cout << "B/H:\t" << (eop0->getPC()(0) - eop1->getPC()(0)) / (eop0->getPC()(2) - grdPt(2)) << std::endl;

	EXPECT_TRUE(fabs(objPt(0) - grdPt(0)) < 0.01);
	EXPECT_TRUE(fabs(objPt(1) - grdPt(1)) < 0.01);
	EXPECT_TRUE(fabs(objPt(2) - grdPt(2)) < 0.02);
}

/* math::Matrix test for intersection */
TEST(ssm_Collinearity, utestLensDistortion)
{
	cout.precision(9);
	cout.setf(ios::fixed, ios::floatfield);

	std::cout << "-----Collinearity test using distorted photo coordinates" << std::endl << std::endl;

	double fx = 530.1620896;
	double fy = 530.2110354;
	double s = (fx + fy) / 2.0;
	double s2 = s * s;
	double s4 = s2 * s2;
	double s6 = s2 * s4;
	int w = 640;
	int h = 480;
	double pixSize = 1.0 / s;
	double f = 1.0;

	pp(0) = (341.4870318 - w / 2) / s;
	pp(1) = (h / 2 - 230.1026168) / s;

	k[0] = 0.0;
	k[1] = 0.360235911;// / s2;
	k[2] = -0.854960566;// / s4;
	k[3] = 2.164433901;// / s6;

	p[0] = 0.000497447;// / s2;
	p[1] = -0.000579024; // s2;
	p[2] = 0.0;

	std::shared_ptr<sensor::DistortionModel> distortion(new sensor::SMACModel(k, p));
	std::shared_ptr<sensor::FrameCamera> cam(new sensor::FrameCamera(0, f, pp, w, h, pixSize, distortion));

	data::Point2D imgPt;
	imgPt(0) = 415.9795;
	imgPt(1) = 57.382168;

	auto photoPt = cam->getImg2PhotoCoord(imgPt);
	auto temp = photoPt - pp;
	auto refinedPhotoCoord = cam->getRefinedPhotoCoord(photoPt);
	auto refinedImgCoord = cam->getPhoto2ImgCoord(refinedPhotoCoord);

	EXPECT_TRUE(fabs(refinedImgCoord(0) - 418.685195) < 0.001);
	EXPECT_TRUE(fabs(refinedImgCoord(1) - 51.224274) < 0.001);

	auto distortedImgCoord = cam->getDistortedImgCoordFromRefinedPhotoCoord(refinedPhotoCoord);

	EXPECT_TRUE(fabs(distortedImgCoord(0) - imgPt(0)) < 0.001);
	EXPECT_TRUE(fabs(distortedImgCoord(1) - imgPt(1)) < 0.001);
}

/* CollinearityEq */
TEST(ssm_Collinearity, utestCollinearityEq)
{
	unsigned int cameraId = 0;
	double pixPitch = 0.004401408;
	double f = 58.0;// *pixPitch;
	unsigned int imgW = 7952;
	unsigned int imgH = 5304;
	data::Point2D pp;
	//pp(0) = 17.418 *pixPitch;
	//pp(1) = 11.871 *pixPitch;

	pp(0) = 0.0764;
	pp(1) = -0.2022;

	double k[4];
	k[0] = 0.0;
	k[1] = 0.0;
	k[2] = 0.0;
	k[3] = 0.0;
	double p[3];
	p[0] = 0.0;
	p[1] = 0.0;
	p[2] = 0.0;

	distortion.reset(new sensor::SMACModel(k, p));

	std::shared_ptr<sensor::FrameCamera> cam(new sensor::FrameCamera(cameraId,
		f,
		pp,
		imgW,
		imgH,
		pixPitch,
		distortion));

	/// PhotoID			X(east)			Y(north)		Z			omega		phi			Kappa
	/// DSC01514.JPG	-5.971626		-129.667153		22.686798	90.85722	-2.458432	-0.168669
	/// DSC01515.JPG	-5.83449		-128.079886		15.035602	90.890882	-2.415211	-0.192187

	double omgDeg = 90.890882;
	double phiDeg = -2.415211;
	double kapDeg = -0.192187;

	data::Point3D pc;
	pc(0) = -5.83449;
	pc(1) = -128.079886;
	pc(2) = 15.035602;

	/// Setup required initial information
	//setupTestDataWithDistortion(f, imgW, imgH, pp, pixPitch, k1, k2, k3, k4, p1, p2, p3, omgDeg, phiDeg, kapDeg, pc);

	unsigned int numObjPts = 44;
	double** objPts = new double* [numObjPts];
	objPts[0] = new double[3]{ -12.19789457	,	-17.80235819	,	39.13637162 };
	objPts[1] = new double[4]{ -12.19789457	,	-17.80235819	,	39.13637162 };
	objPts[2] = new double[5]{ -12.19789457	,	-17.80235819	,	29.29159291 };
	objPts[3] = new double[6]{ -12.19789457	,	-17.80235819	,	29.29159291 };
	objPts[4] = new double[7]{ -12.65833808	,	-21.05148789	,	26.01 };
	objPts[5] = new double[8]{ -12.65833808	,	-21.05148789	,	26.01 };
	objPts[6] = new double[9]{ -16.46	,	-24.49	,	26.01 };
	objPts[7] = new double[10]{ -16.46	,	-24.49	,	26.01 };
	objPts[8] = new double[11]{ -16.46	,	-24.49	,	-58.19567397 };
	objPts[9] = new double[12]{ -16.46	,	-24.49	,	-58.19567397 };
	objPts[10] = new double[13]{ -38.76948941	,	5.186585693	,	-58.19567397 };
	objPts[11] = new double[14]{ -38.76948941	,	5.186585693	,	-58.19567397 };
	objPts[12] = new double[15]{ -38.76948941	,	5.186585693	,	-77.8852314 };
	objPts[13] = new double[16]{ -38.6774007	,	5.836411632	,	-77.8852314 };
	objPts[14] = new double[17]{ -38.6774007	,	5.836411632	,	-77.8852314 };
	objPts[15] = new double[18]{ -38.6774007	,	5.836411632	,	-77.8852314 };
	objPts[16] = new double[19]{ -38.6774007	,	5.836411632	,	-78.54154998 };
	objPts[17] = new double[20]{ -38.6774007	,	5.836411632	,	-78.54154998 };
	objPts[18] = new double[21]{ -50.83310958	,	-79.94061228	,	-78.54154998 };
	objPts[19] = new double[22]{ -50.83310958	,	-79.94061228	,	-78.54154998 };
	objPts[20] = new double[23]{ -50.83310958	,	-79.94061228	,	-77.8852314 };
	objPts[21] = new double[24]{ -50.83310958	,	-79.94061228	,	-77.8852314 };
	objPts[22] = new double[25]{ -50.83310958	,	-79.94061228	,	-77.8852314 };
	objPts[23] = new double[26]{ -50.74102087	,	-79.29078634	,	-77.8852314 };
	objPts[24] = new double[27]{ -50.74102087	,	-79.29078634	,	-58.19567397 };
	objPts[25] = new double[28]{ -50.74102087	,	-79.29078634	,	-58.19567397 };
	objPts[26] = new double[29]{ -21.06443518	,	-56.98129694	,	-58.19567397 };
	objPts[27] = new double[30]{ -21.06443518	,	-56.98129694	,	-58.19567397 };
	objPts[28] = new double[31]{ -21.06443518	,	-56.98129694	,	26.01 };
	objPts[29] = new double[32]{ -21.06443518	,	-56.98129694	,	26.01 };
	objPts[30] = new double[33]{ -18.36783771	,	-61.34069609	,	26.01 };
	objPts[31] = new double[34]{ -18.36783771	,	-61.34069609	,	26.01 };
	objPts[32] = new double[35]{ -18.82828122	,	-64.58982578	,	29.29159291 };
	objPts[33] = new double[36]{ -18.82828122	,	-64.58982578	,	29.29159291 };
	objPts[34] = new double[37]{ -18.82828122	,	-64.58982578	,	39.13637162 };
	objPts[35] = new double[38]{ -18.82828122	,	-64.58982578	,	39.13637162 };
	objPts[36] = new double[39]{ -15.05264438	,	-37.94696229	,	39.13637162 };
	objPts[37] = new double[40]{ -15.05264438	,	-37.94696229	,	39.13637162 };
	objPts[38] = new double[41]{ -15.05264438	,	-37.94696229	,	32.57318581 };
	objPts[39] = new double[42]{ -15.05264438	,	-37.94696229	,	32.57318581 };
	objPts[40] = new double[43]{ -15.97353141	,	-44.44522168	,	32.57318581 };
	objPts[41] = new double[44]{ -15.97353141	,	-44.44522168	,	32.57318581 };
	objPts[42] = new double[45]{ -15.97353141	,	-44.44522168	,	39.13637162 };
	objPts[43] = new double[46]{ -15.97353141	,	-44.44522168	,	39.13637162 };

	/// UID		IDX		X				Y				Z
	/// 12		0		-12.19789457	-17.80235819	39.13637162

	std::cout << "Measured photo coordinates" << std::endl;

	bool retAvailability;
	float ratio = 0.90f; /// available area ratio
	unsigned int widthMargin = static_cast<unsigned int>((1.0f - ratio) * 0.5f * static_cast<float>(cam->getSensorSize()(0)) + 0.5);
	unsigned int heightMargin = static_cast<unsigned int>((1.0f - ratio) * 0.5f * static_cast<float>(cam->getSensorSize()(1)) + 0.5);

	for (unsigned int i = 0; i < numObjPts; ++i)
	{
		data::ObjectPoint objPt;
		//objPt(0) = -12.19789457;
		//objPt(1) = -17.80235819;
		//objPt(2) = 39.13637162;
		objPt(0) = objPts[i][0];
		objPt(1) = objPts[i][1];
		objPt(2) = objPts[i][2];

		double omg = omgDeg / 180. * pi;
		double phi = phiDeg / 180. * pi;
		double kap = kapDeg / 180. * pi;

		auto rMat = math::rotation::getMMat(omg, phi, kap);

		data::Point3D objVec = objPt.val - pc;
		data::Point3D rotatedObjVec = rMat % objVec;
		double x0 = -cam->getFL() * rotatedObjVec(0) / rotatedObjVec(2);
		double y0 = -cam->getFL() * rotatedObjVec(1) / rotatedObjVec(2);
		double x = x0 + pp(0);
		double y = y0 + pp(1);
		double c_ = x / cam->getPixPitch();
		double r_ = y / cam->getPixPitch();
		double c = c_ + cam->getSensorSize()(0) / 2.0;
		double r = cam->getSensorSize()(1) / 2.0 - r_;

		data::Point2D refinedPhotoCoord = ssm::Collinearity::getUndistortedPhotoCoordinates(omg, phi, kap, pc, cam, objPt.val);
		data::Point2D measuredPhotoCoord = ssm::Collinearity::getDistortedPhotoCoordinates(omg, phi, kap, pc, cam, objPt.val);

		data::Point2D measuredImagePt = ssm::Collinearity::getDistortedImageCoordinates(
			omg, phi, kap,
			pc,
			cam,
			objPt.val,
			retAvailability,
			widthMargin,
			heightMargin);

		if (retAvailability)
		{
			//std::cout << "Inside point\t";
			//std::cout << measuredImagePt(0) << "\t" << measuredImagePt(1) << "\t";
			std::cout << measuredImagePt(0) - c << "\t" << measuredImagePt(1) - r << std::endl;
			EXPECT_TRUE(fabs(measuredImagePt(0) - c) < 0.0001);
			EXPECT_TRUE(fabs(measuredImagePt(1) - r) < 0.0001);
		}
		else
		{
			//std::cout << "Outside point\t";
			std::cout << measuredImagePt(0) << "\t" << measuredImagePt(1) << std::endl;
		}
	}

	for (unsigned int i = 0; i < numObjPts; ++i)
		delete[] objPts[i];

	delete[] objPts;
}