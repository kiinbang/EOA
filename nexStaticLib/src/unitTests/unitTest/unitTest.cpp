/*
* Copyright (c) 2019-2019, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>

#include <ssm/include/ROCoplanarity.h>
#include <ssm/include/RONonlinear.h>

#include "unitTestCollinearity.h"
#include "unitTestBundle.h"

void vectorEraseTest(std::vector<double>& vec)
{
	math::Matrix<double> emptyMat(0, 0, 0.0);
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;
	math::Matrix<double> temp(3, 3, 1.2);
	emptyMat.insertPlusResize(0, 0, temp);
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;

	emptyMat.resize(0, 0);
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;
	if (emptyMat.getCols() < 1)
		emptyMat = temp;
	else
		emptyMat.addRows(temp);
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;

	emptyMat.resize(0, emptyMat.getCols());
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;
	if (emptyMat.getCols() < 1)
		emptyMat = temp;
	else
		emptyMat.addRows(temp);
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;
	std::cout << "emptyMat.getCols(): " << emptyMat.getCols() << std::endl;

	emptyMat.resize(0, 0);
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;
	emptyMat.insert(0, 0, temp*10.);
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;
	emptyMat.insert(0, 0, temp);
	std::cout << "emptyMat.getRows(): " << emptyMat.getRows() << std::endl;

	std::cout << "math::matrixout(temp)" << emptyMat.getRows() << std::endl;
	std::cout << math::matrixout(temp) << std::endl;

	emptyMat.del();
	emptyMat.insert(0, 0, temp*10.);
	std::cout << "math::matrixout(emptyMat)" << emptyMat.getRows() << std::endl;
	std::cout << math::matrixout(emptyMat) << std::endl;

	vec.push_back(1.0);
	vec.push_back(2.0);
	vec.push_back(3.0);

	std::vector<int> v;
	for (int i = 0; i < 10; i++)
	{
		v.push_back(i);
	}

	std::cout << v.front() << std::endl;
	std::cout << v.back() << std::endl;

	unsigned int idx = 3;
	//v.erase(v.begin() + idx);

	for (size_t i = 0; i < v.size(); )
	{
		std::cout << "i: " << i << "current v[i]: " << v[i] << std::endl;
		if (v[i] % 2 == 0)
		{
			v.erase(v.begin() + i);
			std::cout << "delete " << i << "current size: " << v.size() << std::endl;
			continue;
		}
		++i;
		//std::cout << v[i] << std::endl;
	}

	//v.erase(v.begin() + 1, v.begin() + 4);

	for (size_t i = 0; i < v.size(); i++)
	{
		std::cout << v[i] << std::endl;
	}
}

int main(void)
{

	if (true)
	{
		std::vector<double> vec;
		vectorEraseTest(vec);

		std::cout << vec[0] << std::endl;
		std::cout << vec[1] << std::endl;
		std::cout << vec[2] << std::endl;
	}

	if (true)
	{
		/// Unit test for the partitioned inverse matrix
		std::cout << "-----[Unit test for math::Matrix ---> Partitioned Matrix Inverse]-----" << std::endl << std::endl;
		testPartitionedMatInverse();
	}

	if (true)
	{
		/// Unit test for the minus operator of the matrix class
		std::cout << "-----[Unit test for math::Matrix ---> minus operator, -A]-----" << std::endl << std::endl;
		testMinusOperatorOfMatrix();
	}

	if (true)
	{
		/// Unit test for matrix and collinearity
		std::cout << "-----[Unit test for math::Matrix]-----" << std::endl << std::endl;
		utestForMatrix();
		std::cout << std::endl << "-----[Unit test for ssm::Intersection and CollinearityEq]-----" << std::endl << std::endl;
		utestIntersectionCollinearityEq();
	}

	if (true)
	{
		/// Relative orientation
		CModel model(".\\testData\\RODataSample.txt");
		model.RO_dependent();
		model.FilePrintROResult(".\\testData\\RODataSample_Result.txt");
	}

	if (true)
	{
		CModelCoplanarity copModel(".\\testData\\RODataSample.txt");
		copModel.RO_dependent_General();
		copModel.FilePrintROResult(".\\testData\\RODataSample_Result_Coplanarity.txt");
	}

	if (true)
	{
		testBundleAdjustment();
	}

	if (true)
	{
		runCollinearityTest();
	}

	if (true)
	{
		runNegativePhotoCoordTest();
	}

	std::cout << "--- DONE ---" << std::endl;

	return 0;
}