/*
* Copyright (c) 2019-2019, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <memory>

#include <ssm/include/SSMCollinearity.h>
#include <ssm/include/SSMFrameCamera.h>
#include <ssm/include/SSMIntersectionLinear.h>
#include <ssm/include/SSMRotation.h>
#include <ssm/include/SSMSMACModel.h>
#include <ssm/include/SSMParameterCollection.h>
#include <utilitygrocery_2.0.h>

int utestForMatrix(void)
{
	//
	// Matrix tests
	//

	math::Matrix<double> a(3, 3, 9);
	std::cout << "sum of matrix" << std::endl;
	std::cout << a.getSum() << std::endl;
	std::cout << "square sum" << std::endl;
	std::cout << a.getSumOfSquares() << std::endl;
	auto aa = a*a;
	std::cout << "sum of matrix" << std::endl;
	std::cout << aa.getSum() << std::endl;

	math::Matrix<double> vec0(3, 1, 1.1);
	data::Point3D pt3d(vec0);
	std::cout << "pt3d(vec0)" << std::endl;
	std::cout << pt3d(0) << "\t" << pt3d(1) << "\t" << pt3d(2) << std::endl;

	data::Point3D pt3d2;
	pt3d2 = vec0;
	std::cout << "pt3d2 = vec0" << std::endl;
	std::cout << pt3d2(0) << "\t" << pt3d2(1) << "\t" << pt3d2(2) << std::endl;

	math::Matrix<int> vec1(vec0);
	vec1 = vec0;
	std::cout << "vec1(vec0)" << std::endl;
	std::cout << vec1(0) << "\t" << vec1(1) << "\t" << vec1(2) << std::endl;

	vec0 *= 2.0;
	vec1 = vec0;
	std::cout << "vec1 = vec0" << std::endl;
	std::cout << vec1(0) << "\t" << vec1(1) << "\t" << vec1(2) << std::endl;

	return 0;
}

data::Point3D grdPt;
const double pi = util::GetPI();
const double f = 60.58;
data::Point2D pp;
unsigned int w = 8984;
unsigned int h = 6732;
double pixSize = 0.006;
double k[4] = { 0., 0., 0., 0. }, p[3] = { 0., 0., 0. };
std::shared_ptr<sensor::DistortionModel> distortion(new sensor::SMACModel(k, p));

std::vector<std::shared_ptr<data::PhotoData>> photos;
std::vector<std::shared_ptr<data::EOP>> eop;
std::vector<data::EulerAngles> opk;
std::vector<data::Point3D> pc;
std::vector<shared_ptr<data::ImagePointData>> pts;
std::vector<unsigned int> ids;

void setupTestDataWithoutDistortion()
{
	const int numPhotos = 2;
	/// pt# 345	100136.39610	99848.52814	82.41379
	/// photo# 0	pt# 345
	/// (x,y) 22.00530	0.61566
	/// (o, p, k, x, y, z) -0.000000	0.000000	-90.000000	100133.2000	100000.0000	500.0000
	/// photo# 1	pt# 345
	/// (x,y) 22.00530	-18.70790
	/// (o, p, k, x, y, z) -0.000000	0.000000	-90.000000	100266.4000	100000.0000	500.0000

	/// object point
	grdPt(0) = 100136.39610;
	grdPt(1) = 99848.52814;
	grdPt(2) = 82.41379;

	/// create cameras
	pp(0) = 0.031;
	pp(1) = 0.152;
	
	k[0] = 0.0;
	k[1] = 0.0;
	k[2] = 0.0;
	k[3] = 0.0;

	p[0] = 0.0;
	p[1] = 0.0;
	p[2] = 0.0;

	distortion.reset(new sensor::SMACModel(k, p));

	std::vector<std::shared_ptr<sensor::FrameCamera>> cam(numPhotos);
	cam[0] = make_shared<sensor::FrameCamera>(0, f, pp, w, h, pixSize, distortion);
	cam[1] = make_shared<sensor::FrameCamera>(1, f, pp, w, h, pixSize, distortion);

	/// eops
	opk.resize(numPhotos);
	opk[0](0) = 0.0 / 180.*pi;
	opk[0](1) = 0.0 / 180.*pi;
	opk[0](2) = -90.0 / 180.*pi;

	opk[1](0) = 0.0 / 180.*pi;
	opk[1](1) = 0.0 / 180.*pi;
	opk[1](2) = -90.0 / 180.*pi;

	pc.resize(numPhotos);
	pc[0](0) = 100133.2;
	pc[0](1) = 100000.0;
	pc[0](2) = 500.0;

	pc[1](0) = 100266.4;
	pc[1](1) = 100000.0;
	pc[1](2) = 500.0;

	eop.resize(numPhotos);
	eop[0] = make_shared<data::EOP>();
	eop[1] = make_shared<data::EOP>();

	eop[0]->setPC(pc[0]);
	eop[0]->setEulerAngles(opk[0]);
	eop[1]->setPC(pc[1]);
	eop[1]->setEulerAngles(opk[1]);

	/// image points without noise
	pts.resize(numPhotos);
	pts[0] = std::make_shared<data::ImagePointData>();
	pts[1] = std::make_shared<data::ImagePointData>();

	pts[0]->pts.resize(1);
	pts[0]->pts[0].coord[0]->set(22.00530);
	pts[0]->pts[0].coord[1]->set(0.61566);

	pts[1]->pts.resize(1);	
	pts[1]->pts[0](0) = 22.00530;
	pts[1]->pts[0](1) = -18.70790;

	/// photo id
	ids.resize(numPhotos);
	ids[0] = 0;
	ids[1] = 1;

	/// create photos
	photos.resize(numPhotos);
	photos[0] = std::make_shared<data::PhotoData>(cam[0], eop[0], pts[0], ids[0]);
	photos[1] = std::make_shared<data::PhotoData>(cam[1], eop[1], pts[1], ids[1]);
}

void setupTestDataWithDistortion(const unsigned int cameraId,
	const double focalLength,
	const unsigned int imgW,
	const unsigned int imgH,
	const data::Point2D& principalPt,
	const double pixPitch,
	const double k1,
	const double k2,
	const double k3,
	const double k4,
	const double p1,
	const double p2,
	const double p3,
	const double omgDeg,
	const double phiDeg,
	const double kapDeg,
	const data::Point3D& perspectiveCenter)
{
	/// create cameras
	k[0] = k1;
	k[1] = k2;
	k[2] = k3;
	k[3] = k4;

	p[0] = p1;
	p[1] = p2;
	p[2] = p3;

	distortion.reset(new sensor::SMACModel(k, p));

	std::shared_ptr<sensor::FrameCamera> cam(new sensor::FrameCamera(cameraId,
		focalLength,
		principalPt,
		imgW,
		imgH,
		pixPitch,
		distortion));

	/// eops
	opk.resize(1);
	opk[0](0) = omgDeg / 180. * pi;
	opk[0](1) = phiDeg / 180. * pi;
	opk[0](2) = kapDeg / 180. * pi;

	eop.resize(1);
	eop[0] = make_shared<data::EOP>();
	eop[0]->setPC(perspectiveCenter);
	eop[0]->setEulerAngles(opk[0]);

	photos.resize(1);
	photos[0] = make_shared<data::PhotoData>(cam, eop[0], pts[0], ids[0]);
}

void runCollinearityTest()
{
	unsigned int cameraId = 0;
	double pixPitch = 0.004401408;
	double f = 58.0;// *pixPitch;
	unsigned int imgW = 7952;
	unsigned int imgH = 5304;
	data::Point2D pp;
	//pp(0) = 17.418 *pixPitch;
	//pp(1) = 11.871 *pixPitch;

	pp(0) = 0.0764;
	pp(1) = -0.2022;
	
	double k[4];	
	k[0] = 0.0;
	k[1] = 0.0;
	k[2] = 0.0;
	k[3] = 0.0;
	double p[3];
	p[0] = 0.0;
	p[1] = 0.0;
	p[2] = 0.0;

	distortion.reset(new sensor::SMACModel(k, p));

	std::shared_ptr<sensor::FrameCamera> cam(new sensor::FrameCamera(cameraId,
		f,
		pp,
		imgW,
		imgH,
		pixPitch,
		distortion));

	/// PhotoID			X(east)			Y(north)		Z			omega		phi			Kappa
	/// DSC01514.JPG	-5.971626		-129.667153		22.686798	90.85722	-2.458432	-0.168669
	/// DSC01515.JPG	-5.83449		-128.079886		15.035602	90.890882	-2.415211	-0.192187

	double omgDeg = 90.890882;
	double phiDeg = -2.415211;
	double kapDeg = -0.192187;

	data::Point3D pc;
	pc(0) = -5.83449;
	pc(1) = -128.079886;
	pc(2) = 15.035602;

	/// Setup required initial information
	//setupTestDataWithDistortion(f, imgW, imgH, pp, pixPitch, k1, k2, k3, k4, p1, p2, p3, omgDeg, phiDeg, kapDeg, pc);

	unsigned int numObjPts = 44;
	double** objPts = new double*[numObjPts];
	objPts[0] = new double[3]{ -12.19789457	,	-17.80235819	,	39.13637162 };
	objPts[1] = new double[4]{ -12.19789457	,	-17.80235819	,	39.13637162 };
	objPts[2] = new double[5]{ -12.19789457	,	-17.80235819	,	29.29159291 };
	objPts[3] = new double[6]{ -12.19789457	,	-17.80235819	,	29.29159291 };
	objPts[4] = new double[7]{ -12.65833808	,	-21.05148789	,	26.01 };
	objPts[5] = new double[8]{ -12.65833808	,	-21.05148789	,	26.01 };
	objPts[6] = new double[9]{ -16.46	,	-24.49	,	26.01 };
	objPts[7] = new double[10]{ -16.46	,	-24.49	,	26.01 };
	objPts[8] = new double[11]{ -16.46	,	-24.49	,	-58.19567397 };
	objPts[9] = new double[12]{ -16.46	,	-24.49	,	-58.19567397 };
	objPts[10] = new double[13]{ -38.76948941	,	5.186585693	,	-58.19567397 };
	objPts[11] = new double[14]{ -38.76948941	,	5.186585693	,	-58.19567397 };
	objPts[12] = new double[15]{ -38.76948941	,	5.186585693	,	-77.8852314 };
	objPts[13] = new double[16]{ -38.6774007	,	5.836411632	,	-77.8852314 };
	objPts[14] = new double[17]{ -38.6774007	,	5.836411632	,	-77.8852314 };
	objPts[15] = new double[18]{ -38.6774007	,	5.836411632	,	-77.8852314 };
	objPts[16] = new double[19]{ -38.6774007	,	5.836411632	,	-78.54154998 };
	objPts[17] = new double[20]{ -38.6774007	,	5.836411632	,	-78.54154998 };
	objPts[18] = new double[21]{ -50.83310958	,	-79.94061228	,	-78.54154998 };
	objPts[19] = new double[22]{ -50.83310958	,	-79.94061228	,	-78.54154998 };
	objPts[20] = new double[23]{ -50.83310958	,	-79.94061228	,	-77.8852314 };
	objPts[21] = new double[24]{ -50.83310958	,	-79.94061228	,	-77.8852314 };
	objPts[22] = new double[25]{ -50.83310958	,	-79.94061228	,	-77.8852314 };
	objPts[23] = new double[26]{ -50.74102087	,	-79.29078634	,	-77.8852314 };
	objPts[24] = new double[27]{ -50.74102087	,	-79.29078634	,	-58.19567397 };
	objPts[25] = new double[28]{ -50.74102087	,	-79.29078634	,	-58.19567397 };
	objPts[26] = new double[29]{ -21.06443518	,	-56.98129694	,	-58.19567397 };
	objPts[27] = new double[30]{ -21.06443518	,	-56.98129694	,	-58.19567397 };
	objPts[28] = new double[31]{ -21.06443518	,	-56.98129694	,	26.01 };
	objPts[29] = new double[32]{ -21.06443518	,	-56.98129694	,	26.01 };
	objPts[30] = new double[33]{ -18.36783771	,	-61.34069609	,	26.01 };
	objPts[31] = new double[34]{ -18.36783771	,	-61.34069609	,	26.01 };
	objPts[32] = new double[35]{ -18.82828122	,	-64.58982578	,	29.29159291 };
	objPts[33] = new double[36]{ -18.82828122	,	-64.58982578	,	29.29159291 };
	objPts[34] = new double[37]{ -18.82828122	,	-64.58982578	,	39.13637162 };
	objPts[35] = new double[38]{ -18.82828122	,	-64.58982578	,	39.13637162 };
	objPts[36] = new double[39]{ -15.05264438	,	-37.94696229	,	39.13637162 };
	objPts[37] = new double[40]{ -15.05264438	,	-37.94696229	,	39.13637162 };
	objPts[38] = new double[41]{ -15.05264438	,	-37.94696229	,	32.57318581 };
	objPts[39] = new double[42]{ -15.05264438	,	-37.94696229	,	32.57318581 };
	objPts[40] = new double[43]{ -15.97353141	,	-44.44522168	,	32.57318581 };
	objPts[41] = new double[44]{ -15.97353141	,	-44.44522168	,	32.57318581 };
	objPts[42] = new double[45]{ -15.97353141	,	-44.44522168	,	39.13637162 };
	objPts[43] = new double[46]{ -15.97353141	,	-44.44522168	,	39.13637162 };

	/// UID		IDX		X				Y				Z
	/// 12		0		-12.19789457	-17.80235819	39.13637162

	std::cout << "Measured photo coordinates" << std::endl;
	
	bool retAvailability;
	float ratio = 0.90f; /// available area ratio
	unsigned int widthMargin = static_cast<unsigned int>((1.0f - ratio)*0.5f * static_cast<float>(cam->getSensorSize()(0)) + 0.5);
	unsigned int heightMargin = static_cast<unsigned int>((1.0f - ratio)*0.5f * static_cast<float>(cam->getSensorSize()(1)) + 0.5);

	for (unsigned int i = 0; i < numObjPts; ++i)
	{
		data::ObjectPoint objPt;
		//objPt(0) = -12.19789457;
		//objPt(1) = -17.80235819;
		//objPt(2) = 39.13637162;
		objPt(0) = objPts[i][0];
		objPt(1) = objPts[i][1];
		objPt(2) = objPts[i][2];

		double omg = omgDeg / 180.*pi;
		double phi = phiDeg / 180.*pi;
		double kap = kapDeg / 180.*pi;

		auto rMat = math::rotation::getMMat(omg, phi, kap);

		data::Point3D objVec = objPt.val - pc;
		data::Point3D rotatedObjVec = rMat % objVec;
		double x0 = -cam->getFL() * rotatedObjVec(0) / rotatedObjVec(2);
		double y0 = -cam->getFL() * rotatedObjVec(1) / rotatedObjVec(2);
		double x = x0 + pp(0);
		double y = y0 + pp(1);
		double c_ = x / cam->getPixPitch();
		double r_ = y / cam->getPixPitch();
		double c = c_ + cam->getSensorSize()(0) / 2.0;
		double r = cam->getSensorSize()(1) / 2.0 - r_;

		data::Point2D refinedPhotoCoord = ssm::Collinearity::getUndistortedPhotoCoordinates(omg, phi, kap, pc, cam, objPt.val);
		data::Point2D measuredPhotoCoord = ssm::Collinearity::getDistortedPhotoCoordinates(omg, phi, kap, pc, cam, objPt.val);

		data::Point2D measuredImagePt = ssm::Collinearity::getDistortedImageCoordinates(
			omg, phi, kap,
			pc,
			cam,
			objPt.val,
			retAvailability,
			widthMargin,
			heightMargin);

		if (retAvailability)
		{
			//std::cout << "Inside point\t";
			std::cout << measuredImagePt(0) << "\t" << measuredImagePt(1) << std::endl;
			//std::cout << "\t\t" << c << "\t" << r << std::endl;
		}
		else
		{
			//std::cout << "Outside point\t";
			std::cout << measuredImagePt(0) << "\t" << measuredImagePt(1) << std::endl;
			//std::cout << "\t\t" << c << "\t" << r << std::endl;
		}
	}

	for (unsigned int i = 0; i < numObjPts; ++i)
		delete[] objPts[i];

	delete[] objPts;
}

void runNegativePhotoCoordTest()
{
	const unsigned int cameraId = 0;
	double pixPitch = 0.001;
	double f = 100.0;/// mm
	unsigned int imgW = 7952;
	unsigned int imgH = 5304;
	data::Point2D pp;/// mm
	pp(0) = 0.0764;
	pp(1) = -0.2022;

	double k[4];
	k[0] = 0.0;
	k[1] = 0.0;
	k[2] = 0.0;
	k[3] = 0.0;
	double p[3];
	p[0] = 0.0;
	p[1] = 0.0;
	p[2] = 0.0;

	distortion.reset(new sensor::SMACModel(k, p));

	std::shared_ptr<sensor::FrameCamera> cam(new sensor::FrameCamera(cameraId,
		f,
		pp,
		imgW,
		imgH,
		pixPitch,
		distortion));

	double omgDeg = 10.0;
	double phiDeg = -10.0;
	double kapDeg = 45.0;

	data::Point3D pc;
	pc(0) = 0.0;
	pc(1) = 0.0;
	pc(2) = 100.0;

	data::ObjectPoint objPt;
	objPt(0) = 5.0;
	objPt(1) = 5.0;
	objPt(2) = 0.0;

	std::cout << "Measured photo coordinates" << std::endl;

	bool retAvailability;
	float ratio = 1.0f; /// available area ratio
	unsigned int widthMargin = static_cast<unsigned int>((1.0f - ratio)*0.5f * static_cast<float>(cam->getSensorSize()(0)) + 0.5);
	unsigned int heightMargin = static_cast<unsigned int>((1.0f - ratio)*0.5f * static_cast<float>(cam->getSensorSize()(1)) + 0.5);

	double omg = omgDeg / 180.*pi;
	double phi = phiDeg / 180.*pi;
	double kap = kapDeg / 180.*pi;

	auto rMat = math::rotation::getMMat(omg, phi, kap);

	data::Point3D objVec = objPt.val - pc;
	data::Point3D rotatedObjVec = rMat % objVec;
	double x0 = -cam->getFL() * rotatedObjVec(0) / rotatedObjVec(2);
	double y0 = -cam->getFL() * rotatedObjVec(1) / rotatedObjVec(2);
	double x = x0 + pp(0);
	double y = y0 + pp(1);
	double c_ = x / cam->getPixPitch();
	double r_ = y / cam->getPixPitch();
	double c = c_ + cam->getSensorSize()(0) / 2.0;
	double r = cam->getSensorSize()(1) / 2.0 - r_;

	data::Point2D refinedPhotoCoord = ssm::Collinearity::getUndistortedPhotoCoordinates(omg, phi, kap, pc, cam, objPt.val);
	data::Point2D measuredPhotoCoord = ssm::Collinearity::getDistortedPhotoCoordinates(omg, phi, kap, pc, cam, objPt.val);
	double targetGSD = 0.005;/// meters

	data::Point2D measuredImagePt = ssm::Collinearity::getDistortedImageCoordinates(
		omg, phi, kap,
		pc,
		cam,
		objPt.val,
		retAvailability,
		widthMargin,
		heightMargin,
		targetGSD);

	if (retAvailability)
	{
		//std::cout << "Inside point\t";
		std::cout << measuredImagePt(0) << "\t" << measuredImagePt(1) << std::endl;
		std::cout << c << "\t" << r << std::endl;
		std::cout << refinedPhotoCoord(0) << "\t" << refinedPhotoCoord(1) << std::endl;
	}
	else
	{
		//std::cout << "Outside point\t";
		std::cout << measuredImagePt(0) << "\t" << measuredImagePt(1) << std::endl;
		std::cout << c << "\t" << r << std::endl;
		std::cout << refinedPhotoCoord(0) << "\t" << refinedPhotoCoord(1) << std::endl;
	}

	data::ObjectPoint objPt2;
	objPt2(0) = -50.0;
	objPt2(1) = -50.0;
	objPt2(2) = 1100.0;

	data::Point2D measuredImagePt2 = ssm::Collinearity::getDistortedImageCoordinates(
		omg, phi, kap,
		pc,
		cam,
		objPt2.val,
		retAvailability,
		widthMargin,
		heightMargin,
		targetGSD);

	data::Point2D refinedPhotoPt2 = ssm::Collinearity::getUndistortedPhotoCoordinates(omg, phi, kap, pc, cam, objPt2.val);

	if (retAvailability)
	{
		//std::cout << "Inside point\t";
		std::cout << measuredImagePt2(0) << "\t" << measuredImagePt2(1) << std::endl;
		std::cout << refinedPhotoPt2(0) << "\t" << refinedPhotoPt2(1) << std::endl;
	}
	else
	{
		//std::cout << "Outside point\t";
		std::cout << measuredImagePt2(0) << "\t" << measuredImagePt2(1) << std::endl;
		std::cout << refinedPhotoPt2(0) << "\t" << refinedPhotoPt2(1) << std::endl;
	}
}

void setupTestDataWithDistortion()
{
	/// pt# 345	100136.39610	99848.52814	82.41379
	/// photo# 0	pt# 345
	/// (x,y) 21.99326	0.61542
	/// (o, p, k, x, y, z) - 0.000000	0.000000 - 90.000000	100133.2000	100000.0000	500.0000
	/// photo# 1	pt# 345
	/// (x,y) 21.98512 - 18.69130
	/// (o, p, k, x, y, z) -0.000000	0.000000	-90.000000	100266.4000	100000.0000	500.0000

	grdPt(0) = 100136.39610;
	grdPt(1) = 99848.52814;
	grdPt(2) = 82.41379;

	pp(0) = 0.031f;
	pp(1) = 0.152f;

	k[0] = 0.0;
	k[1] = 0.000001;
	k[2] = 0.0;
	k[3] = 0.0;

	p[0] = 0.000001;
	p[1] = 0.0;
	p[2] = 0.0;

	distortion.reset(new sensor::SMACModel(k, p));

	std::shared_ptr<sensor::FrameCamera> cam(new sensor::FrameCamera(0, f, pp, w, h, pixSize, distortion));
	std::shared_ptr<sensor::FrameCamera> cam2(new sensor::FrameCamera(1, f, pp, w, h, pixSize, distortion));

	/// EOPs
	eop.resize(2);
	eop[0] = make_shared<data::EOP>();
	eop[1] = make_shared<data::EOP>();

	std::vector<data::EulerAngles> opk(2);
	opk[0](0) = 0.0 / 180. * pi;
	opk[0](1) = 0.0 / 180. * pi;
	opk[0](2) = -90.0 / 180. * pi;

	opk[1](0) = 0.0 / 180. * pi;
	opk[1](1) = 0.0 / 180. * pi;
	opk[1](2) = -90.0 / 180. * pi;

	std::vector <data::Point3D> pc(2);
	pc[0](0) = 100133.2;
	pc[0](1) = 100000.0;
	pc[0](2) = 500.0;
	
	data::Point3D pc2;
	pc[1](0) = 100266.4;
	pc[1](1) = 100000.0;
	pc[1](2) = 500.0;

	eop[0]->setPC(pc[0]);
	eop[0]->setEulerAngles(opk[0]);
	eop[1]->setPC(pc[1]);
	eop[1]->setEulerAngles(opk[1]);

	/// object point
	std::vector<std::shared_ptr<data::ImagePointData>> pt(2);
	pt[0] = std::make_shared<data::ImagePointData>();
	pt[1] = std::make_shared<data::ImagePointData>();

	pt[0]->pts.resize(1);
	pt[1]->pts.resize(1);

	pt[0]->pts[0](0) = 21.99326;
	pt[0]->pts[0](1) = 0.61542;

	pt[1]->pts[0](0) = 21.98512;
	pt[1]->pts[0](1) = -18.69130;

	/// create photos
	photos.resize(2);
	photos[0] = make_shared<data::PhotoData>(cam, eop[0], pt[0], 0);
	photos[1] = make_shared<data::PhotoData>(cam2, eop[1], pt[1], 1);
}

void CollinearityEq(void)
{
	for (size_t n = 0; n < photos.size(); ++n)
	{
		std::cout << "Measured photo coordinates(Truth) [" << n << "]" << std::endl;
		std::cout << photos[n]->getPts()[0].val(0) << "\t" << photos[n]->getPts()[0].val(1) << "\t" << std::endl;
		double omega, phi, kappa;
		photos[n]->getEop()->getEulerAngles(omega, phi, kappa);
		auto pc = photos[n]->getEop()->getPC();
		auto cam = photos[n]->getCam();
		data::Point2D measuredPhotoPt = ssm::Collinearity::getDistortedPhotoCoordinates(omega, phi, kappa, pc, cam, grdPt);

		std::cout << "getMeasuredPhotoCoordinates" << std::endl;
		std::cout << measuredPhotoPt(0) << "\t" << measuredPhotoPt(1) << std::endl;

		data::RotationMatrix mmat = math::rotation::getMMat(omega, phi, kappa);

		measuredPhotoPt = ssm::Collinearity::getDistortedPhotoCoordinates(mmat, photos[n]->getEop()->getPC(), photos[n]->getCam(), grdPt);
		std::cout << "getMeasuredPhotoCoordinates" << std::endl;
		std::cout << measuredPhotoPt(0) << "\t" << measuredPhotoPt(1) << std::endl;

		bool retAvailability;
		float ratio = 0.1f;
		unsigned int widthMargin = static_cast<unsigned int>((1.0f - ratio)*0.5f * static_cast<float>(photos[n]->getCam()->getSensorSize()(0)) + 0.5);
		unsigned int heightMargin = static_cast<unsigned int>((1.0f - ratio)*0.5f * static_cast<float>(photos[n]->getCam()->getSensorSize()(1)) + 0.5);

		data::Point2D measuredImagePt = ssm::Collinearity::getDistortedImageCoordinates(
			omega, phi, kappa, 
			photos[n]->getEop()->getPC(), 
			photos[n]->getCam(), 
			grdPt, 
			retAvailability, widthMargin, heightMargin);

		std::cout << "getMeasuredImageCoordinates" << std::endl;
		std::cout << measuredImagePt(0) << "\t" << measuredImagePt(1) << std::endl;

		if (retAvailability == false)
		{
			std::cout << "out of boundary for the ratio (" << ratio;
			std::cout << ") and sensor size (" << photos[n]->getCam()->getSensorSize()(0);
			std::cout << ", " << photos[n]->getCam()->getSensorSize()(1) << ")" << " boundary size (";
			std::cout << photos[n]->getCam()->getSensorSize()(0) * ratio << ", ";
			std::cout << photos[n]->getCam()->getSensorSize()(1) * ratio << ")" << std::endl;
		}
	}
}

int utestIntersectionCollinearityEq(void)
{
	cout.precision(4);
	cout.setf(ios::fixed, ios::floatfield);

	//
	// Intersection tests
	//

	std::cout << "-----Intersection test using distorted photo coordinates------" << std::endl << std::endl;

	setupTestDataWithDistortion();

	std::cout << "Ground(object) point coordinates (reference values): " << std::endl << grdPt(0) << "\t" << grdPt(1) << "\t" << grdPt(2) << std::endl;
	
	data::Point3D objPt = ssm::runLinearIntersectionStereo(*photos[0], *photos[1]);
	std::cout << "runLinearIntersectionStereo" << std::endl;
	cout << objPt(0) << "\t" << objPt(1) << "\t" << objPt(2) << std::endl;

	objPt = ssm::runLinearIntersectionMultiNormalMat(photos);
	std::cout << "runLinearIntersectionMultiNormalMat" << std::endl;
	cout << objPt(0) << "\t" << objPt(1) << "\t" << objPt(2) << std::endl;

	std::cout << "-----Collinearity test using distorted photo coordinates" << std::endl << std::endl;

	CollinearityEq();

	std::cout << std::endl << std::endl << "-----Intersection test without lens distortion------" << std::endl << std::endl;

	setupTestDataWithoutDistortion();

	std::cout << "Ground(object) point coordinates (reference values): " << std::endl << grdPt(0) << "\t" << grdPt(1) << "\t" << grdPt(2) << std::endl;

	objPt = ssm::runLinearIntersectionStereo(*photos[0], *photos[1]);
	std::cout << "runLinearIntersectionStereo" << std::endl;
	cout << objPt(0) << "\t" << objPt(1) << "\t" << objPt(2) << std::endl;

	objPt = ssm::runLinearIntersectionMultiNormalMat(photos);
	std::cout << "runLinearIntersectionMultiNormalMat" << std::endl;
	cout << objPt(0) << "\t" << objPt(1) << "\t" << objPt(2) << std::endl;

	std::cout << std::endl << "-----Collinearity test without lens distortion" << std::endl << std::endl;

	CollinearityEq();

	return 0;
}

int testMinusOperatorOfMatrix()
{
	math::Matrix<double> A(2, 2);
	A(0, 0) = 1.0;
	A(0, 1) = -2.0;
	A(1, 0) = 3.0;
	A(1, 1) = -4.0;

	math::Matrix<double> B = -A;

	std::cout << "A matrix" << std::endl;
	std::cout << math::matrixout(A) << std::endl;
	std::cout << "-A matrix" << std::endl;
	std::cout << math::matrixout(B) << std::endl;
	std::cout << "A + B should be Zero Matrix" << std::endl;
	std::cout << math::matrixout(A + B) << std::endl;

	return 0;
}

int testPartitionedMatInverse()
{
	unsigned int sizer = 2;
	unsigned int sizec = 2;
	math::Matrix<double> invTest(sizer * 2, sizec * 2, 0.0);
	int idx = 0;
	for (unsigned int r = 0; r < sizer * 2; ++r)
	{
		for (unsigned int c = 0; c < sizec * 2; ++c)
		{
			invTest(r, c) = ((++idx)*(c + 1)*(r + 1)) % 7;
		}
	}

	std::cout << "invTest" << std::endl;
	std::cout << math::matrixout(invTest) << std::endl;

	auto invmat = invTest.inverse();
	if (invmat.getRows() == 0 && invmat.getCols() == 0)
	{
		std::cout << "Error in getting an inverse matrix" << std::endl;
		throw std::runtime_error("Error in inverse");
	}
	else
	{
		std::cout << "invmat" << std::endl;
		std::cout << math::matrixout(invmat) << std::endl;
		std::cout << "invmat % invTest should result in a identity matrix" << std::endl;
		std::cout << math::matrixout(invmat % invTest) << std::endl;

		auto m11 = invTest.getSubset(0, 0, sizer, sizec);
		auto m12 = invTest.getSubset(0, sizec, sizer, sizec);
		auto m21 = invTest.getSubset(sizer, 0, sizer, sizec);
		auto m22 = invTest.getSubset(sizer, sizec, sizer, sizec);

		auto invmat2 = math::inversePartitionedMatrix(m11, m12, m21, m22);
		std::cout << std::endl;
		std::cout << "invmat2" << std::endl;
		std::cout << math::matrixout(invmat2) << std::endl;

		std::cout << std::endl;
		std::cout << "invmat2 % invTest result in a identity matrix" << std::endl;
		std::cout << math::matrixout(invmat2 % invTest) << std::endl;

		std::cout << std::endl;
		std::cout << "invmat - invmat2 shows the difference between the ordinary inverse matrix and the partitioned matrix inverse " << std::endl;
		std::cout << math::matrixout(invmat - invmat2) << std::endl;
	}

	return 0;
}
