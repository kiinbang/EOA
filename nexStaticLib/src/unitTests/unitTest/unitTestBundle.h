#pragma once

int testBundleAdjustment()
{
	/// pt# 345	100136.39610	99848.52814	82.41379
	/// photo# 0	pt# 345
	/// (x,y) 21.99326	0.61542
	/// (o, p, k, x, y, z) - 0.000000	0.000000 - 90.000000	100133.2000	100000.0000	500.0000
	/// photo# 1	pt# 345
	/// (x,y) 21.98512 - 18.69130
	/// (o, p, k, x, y, z) -0.000000	0.000000	-90.000000	100266.4000	100000.0000	500.0000

	data::Point3D grdPt;
	grdPt(0) = 100136.39610;
	grdPt(1) = 99848.52814;
	grdPt(2) = 82.41379;

	data::Point2D pp;
	pp(0) = 0.031f;
	pp(1) = 0.152f;

	std::vector<shared_ptr<data::PhotoData>> photos;
	photos.resize(2);

	double k[4] = { 0., 0., 0., 0. }, p[3] = { 0., 0., 0. };
	
	k[0] = 0.0;
	k[1] = 0.000001;
	k[2] = 0.0;
	k[3] = 0.0;

	p[0] = 0.000001;
	p[1] = 0.0;
	p[2] = 0.0;

	std::shared_ptr<sensor::DistortionModel> distortion(std::make_shared<sensor::SMACModel>(k, p));

	std::shared_ptr<sensor::Camera> cam(std::make_shared<sensor::FrameCamera>(0, f, pp, w, h, pixSize, distortion));
	
	double** opk = new double*[2];
	opk[0] = new double[3];
	opk[1] = new double[3];

	opk[0][0] = 0.0 / 180.*pi;
	opk[0][1] = 0.0 / 180.*pi;
	opk[0][2] = -90.0 / 180.*pi;

	opk[1][0] = 0.0 / 180.*pi;
	opk[1][1] = 0.0 / 180.*pi;
	opk[1][2] = -90.0 / 180.*pi;	

	data::Point3D pc;
	pc(0) = 100133.2;
	pc(1) = 100000.0;
	pc(2) = 500.0;

	shared_ptr<data::EOP> eop(std::make_shared<data::EOP>());
	eop->setEulerAngles(opk[0][0], opk[0][1], opk[0][2]);
	eop->setPC(pc);

	std::shared_ptr<data::ImagePointData> points(std::make_shared<data::ImagePointData>());
	points->pts.resize(1);
	// without noise
	points->pts[0](0) = 21.99326;
	points->pts[0](1) = 0.61542;

	photos[0] = std::make_shared<data::PhotoData>(cam, eop, points, 0);

	std::shared_ptr<sensor::Camera> cam2(std::make_shared<sensor::FrameCamera>(cam->getId(), f, pp, w, h, pixSize, distortion));

	data::Point3D pc2;
	pc2(0) = 100266.4;
	pc2(1) = 100000.0;
	pc2(2) = 500.0;

	shared_ptr<data::EOP> eop2(std::make_shared<data::EOP>());
	eop2->setEulerAngles(opk[1][0], opk[1][1], opk[1][2]);
	eop2->setPC(pc2);

	std::shared_ptr<data::ImagePointData> points2(std::make_shared<data::ImagePointData>());
	points2->pts.resize(1);
	// without noise
	points2->pts[0](0) = 21.98512;
	points2->pts[0](1) = -18.69130;

	photos[1] = std::make_shared<data::PhotoData>(cam2, eop2, points2, 1);
	
	std::vector<data::ObjectPoint> objPoints;
	data::ObjectPoint objPt;
	objPt.val = grdPt;
	objPoints.push_back(objPt);

	const double minVarThreshold = 1.0e-12;
	const double maxVarThreshold = 1.0e+12;

	//ssm::Bundle bundle(photos, objPoints, minVarThreshold, maxVarThreshold);

	return 0;
}
