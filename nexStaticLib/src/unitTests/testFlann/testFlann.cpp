// testFlann.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <fstream>
#include <iostream>

#include <ssm/include/MeshDataTree.h>

void nanoflannTest()
{
    mesh::SSMFlann mytree(0.5, mesh::PSEUDOTYPE::CENTROID);
    mytree.setNumberOfPoints(6);
    mytree.setVertex(0, 0.0, 0.0, 0.0);
    mytree.setVertex(1, 1.0, 0.0, 0.0);
    mytree.setVertex(2, 0.0, 1.0, 0.0);

    mytree.setVertex(3, 0.0, 0.0, -10.0);
    mytree.setVertex(4, 1.0, 0.0, -10.0);
    mytree.setVertex(5, 0.0, 1.0, -10.0);
    
    mytree.setNumberOfFaces(2);
    std::vector<unsigned int> vertexIndices;
    vertexIndices.push_back(0);
    vertexIndices.push_back(1);
    vertexIndices.push_back(2);
    mytree.setFace(0, vertexIndices);

    vertexIndices.clear();
    vertexIndices.push_back(3);
    vertexIndices.push_back(4);
    vertexIndices.push_back(5);
    mytree.setFace(1, vertexIndices);

    mytree.buildTree(10);
    std::vector<double> distances;
    std::vector<std::size_t> indices;
    unsigned int numClosest = 10;
    double distThreshold = 1.1;

    if (mytree.search(0.0, 0.0, 1.0,
        distances,
        indices,
        numClosest,
        distThreshold))
    {
        for (size_t i = 0; i < indices.size(); ++i)
        {
            auto vertex = mytree.getVertex(indices[i]);
            std::cout << indices[i] << "\t" << vertex.x() << "\t" << vertex.y() << "\t" <<  vertex.z() << "\tdist: "<< distances[i]<< std::endl;

            /// Number of faces
            auto numFaces = vertex.getFaceId().size();

            for (size_t j = 0; j < numFaces; ++j)
            {
                std::cout << "face id: " << vertex.getFaceId()[j] << std::endl;
            }
        }  
    }

    std::string idxPath = std::string("C:\\repository\\mygit\\test_nanoflann_index.idx");
    if(!mytree.exportIndex(idxPath))
        std::cout << "Failure in exportIndex" << std::endl;

	if (!mytree.buildTree(idxPath))
		std::cout << "Failure in buildTree(import an idx file)" << std::endl;

	if (mytree.search(0.0, 0.0, 1.0,
		distances,
		indices,
		numClosest,
		distThreshold))
	{
		for (size_t i = 0; i < indices.size(); ++i)
		{
			auto vertex = mytree.getVertex(indices[i]);
			std::cout << indices[i] << "\t" << vertex.x() << "\t" << vertex.y() << "\t" << vertex.z() << "\tdist: " << distances[i] << std::endl;

			/// Number of faces
			auto numFaces = vertex.getFaceId().size();

			for (size_t j = 0; j < numFaces; ++j)
			{
				std::cout << "face id: " << vertex.getFaceId()[j] << std::endl;
			}
		}
	}
}

void nanoflannPseudoPtsTest()
{
	mesh::SSMFlann mytree(0.07, mesh::PSEUDOTYPE::MIDPOINT);
	mytree.setNumberOfPoints(3);
	mytree.setVertex(0, 0.0, 0.0, 0.0);
	mytree.setVertex(1, 0.0, 0.2, 0.0);
	mytree.setVertex(2, 1.0, 0.0, 0.0);

	mytree.setNumberOfFaces(1);
	std::vector<unsigned int> vertexIndices;
	vertexIndices.push_back(0);
	vertexIndices.push_back(1);
	vertexIndices.push_back(2);
	mytree.setFace(0, vertexIndices);

	mytree.buildTree(10);

    const std::vector<mesh::Vertex3D>&  vertices = mytree.getVertices();
    const std::vector<mesh::Vertex3D>& pseudoPts = mytree.getPseudoPts();

	for (const auto& vtx : vertices)
		std::cout << vtx.x() << "\t" << vtx.y() << "\t" << vtx.z() << std::endl;
}

void exportTest()
{
	mesh::SSMFlann mytree(0.07, mesh::PSEUDOTYPE::MIDPOINT);
	mytree.setNumberOfPoints(3);
	mytree.setVertex(0, 0.0, 0.0, 0.0);
	mytree.setVertex(1, 0.0, 0.2, 0.0);
	mytree.setVertex(2, 1.0, 0.0, 0.0);

	mytree.setNumberOfFaces(1);
	std::vector<unsigned int> vertexIndices;
	vertexIndices.push_back(0);
	vertexIndices.push_back(1);
	vertexIndices.push_back(2);
	mytree.setFace(0, vertexIndices);

	mytree.buildTree(10);

	const std::vector<mesh::Vertex3D>& vertices = mytree.getVertices();
	const std::vector<mesh::Vertex3D>& pseudoPts = mytree.getPseudoPts();

	for (const auto& vtx : vertices)
		std::cout << vtx.x() << "\t" << vtx.y() << "\t" << vtx.z() << std::endl;

	/// File export
	std::string idxPath = std::string("C:\\repository\\mygit\\vertices.txt");
	std::fstream vtxFile(idxPath, std::ios::out);
	if (!vtxFile)
		std::cout << "Failed in opening a vertex file for writing" << std::endl;

	vtxFile.precision(9);
	vtxFile.setf(std::ios::fixed, std::ios::floatfield);

	for (const auto& vtx : vertices)
		vtxFile << vtx.x() << "\t" << vtx.y() << "\t" << vtx.z() << std::endl;

	vtxFile.close();
}

void radiusSearchTest()
{
	mesh::SSMFlann mytree(0.07, mesh::PSEUDOTYPE::MIDPOINT);
	mytree.setNumberOfPoints(3);
	mytree.setVertex(0, 0.0, 0.0, 0.0);
	mytree.setVertex(1, 0.0, 0.2, 0.0);
	mytree.setVertex(2, 1.0, 0.0, 0.0);

	mytree.setNumberOfFaces(1);
	std::vector<unsigned int> vertexIndices;
	vertexIndices.push_back(0);
	vertexIndices.push_back(1);
	vertexIndices.push_back(2);
	mytree.setFace(0, vertexIndices);

	mytree.buildTree(10);

	double sx = 0.4, sy = 0.03, sz = 10.0;
	double ex = 0.4, ey = 0.03, ez = -10.0;
	double radius = 0.07;
	std::vector<std::pair<size_t, double>> retVal;
	mytree.raytrace(sx, sy, sz,
		ex, ey, ez,
		radius,
		retVal);

	const std::vector<mesh::Vertex3D>& vertices = mytree.getVertices();
	for (int i=0; i<retVal.size(); ++i)
	{
		std::cout << "index: " << retVal[i].first << std::endl;
		std::cout << "dist: " << retVal[i].second << std::endl;
		std::cout << vertices[retVal[i].first].x() << "\t" << vertices[retVal[i].first].y() << "\t" << vertices[retVal[i].first].z() << std::endl;
		auto faceIds = vertices[retVal[i].first].getFaceId();
		for (auto id : faceIds)
			std::cout << "face id: " << id << std::endl;
	}
}

int main()
{
    nanoflannTest();

    nanoflannPseudoPtsTest();

    //exportTest();

    radiusSearchTest();
}