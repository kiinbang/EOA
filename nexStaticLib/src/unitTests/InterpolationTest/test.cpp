#include "pch.h"

#include "../../libraries/Interpolation/Interpolation.h"
#include <iostream>	
#include <fstream> 
#include <string>

math::geo::FDataTree epochTree;
math::geo::FDEpoch epoch;
const int numEpochs = 162;
const std::string flightdatapath = "F:\\Users\\bbarab\\Downloads\\interpolation\\";
const std::string step2path = "F:\\Users\\bbarab\\Downloads\\interpolation\\step2\\";
const std::string step1path = "F:\\Users\\bbarab\\Downloads\\interpolation\\step1\\";

TEST(FDataTree, BuildTree)
{
	epochTree.clear();
	epochTree.reserve(numEpochs);

	std::fstream file(flightdatapath + "20210324_105704_LOG_RAIL_GEOTAG_LLH.csv");
	
	if(!file)
		EXPECT_TRUE(false);

	std::string cFormat;
	
	std::getline(file, cFormat);
	if (cFormat.find("SystemTime_msec,Lat,Lon,H,GpsTime_msec,Gimbal_Roll,Gimbal_Tilt,Gimbal_Pan,HotShoeCount,reserved_1,reserved_2,reserved_3,reserved_4,reserved_5,reserved_6,reserved_7,reserved_8,reserved_9,reserved_10,reserved_11") != std::string::npos)
	{
		std::string strBuf;
		int row = 0;
		while (std::getline(file, strBuf, ',')) //SystemTime_msec
		{

			std::getline(file, strBuf, ',');//PosX_mtr
			epoch.x = std::stod(strBuf);
			std::getline(file, strBuf, ',');//PosY_mtr
			epoch.y = std::stod(strBuf);
			std::getline(file, strBuf, ',');//PosZ_mtr
			epoch.z = std::stod(strBuf);

			std::getline(file, strBuf, ',');//GpsTime_msec
			epoch.msecGpsTime = static_cast<math::geo::FDataType>(std::stod(strBuf));

			std::getline(file, strBuf, ',');//Gimbal_Roll
			epoch.rotX = std::stod(strBuf);
			std::getline(file, strBuf, ',');//Gimbal_Tilt
			epoch.rotY = std::stod(strBuf);
			std::getline(file, strBuf, ',');//Gimbal_Pan
			epoch.rotZ = std::stod(strBuf);

			std::getline(file, strBuf, ','); //HotShoeCount
			std::getline(file, strBuf, ','); //reserved_1
			std::getline(file, strBuf, ','); //reserved_2
			std::getline(file, strBuf, ','); //reserved_3
			std::getline(file, strBuf, ','); //reserved_4
			std::getline(file, strBuf, ','); //reserved_5
			std::getline(file, strBuf, ','); //reserved_6
			std::getline(file, strBuf, ','); //reserved_7
			std::getline(file, strBuf, ','); //reserved_8
			std::getline(file, strBuf, ','); //reserved_9
			std::getline(file, strBuf, ','); //reserved_10
			std::getline(file, strBuf); //reserved_11

			epochTree.add(epoch);
			row++;
		}

		try
		{
			epochTree.buildTree(row);
		}
		catch (...)
		{
			EXPECT_TRUE(false);
		}

		file.close();
		
		EXPECT_TRUE(true);
	}
	else
		EXPECT_TRUE(false);	
}

std::vector<math::geo::FDataType> readTimes(const std::string& path)
{
	std::vector<math::geo::FDataType> times;
	std::fstream file(path);
	
	if (!file) 
		return times;

	std::string cFormat;
	
	std::getline(file, cFormat);
	if (cFormat.find("filepath,gpstime") != std::string::npos)
	{
		std::string strBuf;
		while (std::getline(file, strBuf, ',')) //filepath
		{
			std::getline(file, strBuf);//gpstime
			times.push_back(static_cast<math::geo::FDataType>(std::stod(strBuf)));
		}
	}

	file.close();

	return times;
}

void writeEpochs(const std::string& path, const std::vector<math::geo::FDEpoch>& epochs)
{
	std::wofstream outfile(path);

	if (!outfile)
		return;

	outfile << L"gpstime,PosX_mtr,PosY_mtr,PosZ_mtr,Gimbal_Roll,Gimbal_Tilt,Gimbal_Pan" << std::endl;

	for (size_t i = 0; i < epochs.size(); i++)
	{
		outfile << std::to_wstring(epochs[i].msecGpsTime) << L',';
		outfile << std::to_wstring(epochs[i].x) << L',';
		outfile << std::to_wstring(epochs[i].y) << L',';
		outfile << std::to_wstring(epochs[i].z) << L',';
		outfile << std::to_wstring(epochs[i].rotX) << L',';
		outfile << std::to_wstring(epochs[i].rotY) << L',';

		outfile << std::to_wstring(epochs[i].rotZ) << std::endl;
	}
	outfile.close();
}

TEST(ReadImgGpstime_and_Search, ReadImgGpstime_and_Search)
{
	///1 sortie Map
	auto sortie1mapTimes = readTimes(step1path + "1 sortie Map_edit.csv");
	auto sortie1map = epochTree.linearInterpolate(sortie1mapTimes, static_cast<math::geo::FDataType>(6000.0), 2);
	writeEpochs((step2path + "1 sortie Map_out_new.csv"), sortie1map);	
	
	///1 sortie Sony
	auto sortie1sonyTimes = readTimes(step1path + "1 sotie sony_edit.csv");
	auto sortie1sony = epochTree.linearInterpolate(sortie1sonyTimes, static_cast<math::geo::FDataType>(6000.0), 2);
	writeEpochs((step2path + "1 sotie sony_out_new.csv"), sortie1sony);

	///2 sortie Sony
	auto sortie2sonyTimes = readTimes(step1path + "2 sotie sony_edit.csv");
	auto sortie2sony = epochTree.linearInterpolate(sortie2sonyTimes, static_cast<math::geo::FDataType>(6000.0), 2);
	writeEpochs((step2path + "2 sotie sony_out_new.csv"), sortie2sony);

	EXPECT_TRUE(true);	
}

//TEST(FDataTree, BuildTree)
//{
//	epochTree.clear();
//	epochTree.reserve(numEpochs);
//
//	for (int i = 0; i < numEpochs; ++i)
//	{
//		epoch.time = static_cast<math::geo::FDataType>(i);
//		epoch.x = double(i * 100. + 1.0);
//		epoch.y = double(i * 100. + 2.0);
//		epoch.z = double(i * 100. + 3.0);
//		epoch.rotX = double(i * 0.1 + 0.01);
//		epoch.rotY = double(i * 0.1 + 0.02);
//		epoch.rotZ = double(i * 0.1 + 0.03);
//
//		epochTree.add(epoch);
//	}
//
//	try 
//	{
//		epochTree.buildTree(10);
//	}
//	catch(...)
//	{
//		EXPECT_TRUE(false);
//	}
//
//	EXPECT_TRUE(true);
//}

//TEST(FDataTree, CheckTreeData)
//{
//	for (int i = 0; i < numEpochs; ++i)
//	{
//		EXPECT_TRUE(almostSame(epochTree.getEpoch(i).time, static_cast<math::geo::FDataType>(i)));
//		EXPECT_TRUE(almostSame(epochTree.getEpoch(i).x, double(i * 100. + 1.0)));
//		EXPECT_TRUE(almostSame(epochTree.getEpoch(i).y, double(i * 100. + 2.0)));
//		EXPECT_TRUE(almostSame(epochTree.getEpoch(i).z, double(i * 100. + 3.0)));
//		EXPECT_TRUE(almostSame(epochTree.getEpoch(i).rotX, double(i * 0.1 + 0.01)));
//		EXPECT_TRUE(almostSame(epochTree.getEpoch(i).rotY, double(i * 0.1 + 0.02)));
//		EXPECT_TRUE(almostSame(epochTree.getEpoch(i).rotZ, double(i * 0.1 + 0.03)));
//	}
//}

//TEST(FDataTree, SearchTest)
//{
	//for (int i = 0; i < numEpochs; ++i)
	//{
	//	double t0 = double(i) + 0.2;
	//	std::vector<double> distances;
	//	std::vector<size_t> indices;
	//	const unsigned int numClosest = 3;
	//	double distThreshold = 1.0;
	//	epochTree.search(t0, distances, indices, numClosest, distThreshold);

	//	// interpolation
	//	;
	//	getLinearInterpolation(epochTree, std::vector<math::geo::FDEpoch> &output);

	//	//EXPECT_EQ(indices[0], i);
	//}
//}
