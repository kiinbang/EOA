#include "TrackerSubRegion.h"

namespace feature
{
	TrackerSubRegion::TrackerSubRegion(cv::Ptr<cv::Feature2D> _detector, cv::Ptr<cv::DescriptorMatcher> _matcher)
		: Tracker(_detector, _matcher)
	{}

	void TrackerSubRegion::setSourceFrame(cv::Mat& frame, std::vector< std::vector<std::vector<cv::Point>>>& ptMask)
	{
		ptMask.resize(numDividFactor);
		for (unsigned int i = 0; i < numDividFactor; ++i)
		{
			ptMask[i].resize(numDividFactor);
			for (unsigned int j = 0; j < numDividFactor; ++j)
			{
				ptMask[i][j].resize(4);//4 corners (LT, RT, RB, LB)
			}
		}

		auto size = frame.size();
		int dW = size.width / numDividFactor;
		int dH = size.height / numDividFactor;

		for (unsigned int r = 0; r < numDividFactor; ++r)
		{
			for (unsigned int c = 0; c < numDividFactor; ++c)
			{
				int h = dH * r;
				int w = dW * c;
				ptMask[r][c][0].x = w;
				ptMask[r][c][0].y = h;
				ptMask[r][c][1].x = ptMask[r][c][0].x + dW;
				ptMask[r][c][1].y = ptMask[r][c][0].y;
				ptMask[r][c][2].x = ptMask[r][c][0].x + dW;
				ptMask[r][c][2].y = ptMask[r][c][0].y + dH;
				ptMask[r][c][3].x = ptMask[r][c][0].x;
				ptMask[r][c][3].y = ptMask[r][c][0].y + dH;
			}
		}

		first_frame = frame.clone();
	}

	void TrackerSubRegion::setRegion(Stats& stats, const std::string& title, std::vector<cv::Point>& mask)
	{
		const int iSize = 4;//4 corners

		const cv::Point* ptContain = { &mask[0] };

		cv::Mat matMask = cv::Mat::zeros(first_frame.size(), CV_8UC1);
		fillPoly(matMask, &ptContain, &iSize, 1, cv::Scalar::all(255));
		detector->detectAndCompute(first_frame, matMask, first_kp, first_desc);
		stats.keypoints = (int)first_kp.size();

		std::vector<cv::Point2f> bbf;
		bbf.reserve(iSize);
		for (const auto& p : mask)
		{
			cv::Point pf(p.x, p.y);
			bbf.push_back(pf);
		}

#ifdef _DEBUG
		drawBoundingBox(first_frame, bbf);
#endif
		putText(first_frame, title, cv::Point(0, 60), cv::FONT_HERSHEY_PLAIN, 5, cv::Scalar::all(0), 4);
		object_bb = bbf;
	}

	void TrackerSubRegion::processTargetKp(const cv::Mat& frame, Stats& targetStats, std::vector<cv::KeyPoint>& targetKp, cv::Mat& desc)
	{
		detector->detectAndCompute(frame, cv::noArray(), targetKp, desc);
		targetStats.keypoints = (int)targetKp.size();
	}

	void TrackerSubRegion::getDistinctPtsFromBinaryImg(const cv::Mat& frame, Stats& targetStats, std::vector<cv::KeyPoint>& targetKp, cv::Mat& desc, int thresh)
	{
		/// Blur
		cv::Mat imgBlur;
		cv::GaussianBlur(frame, imgBlur, cv::Size(3, 3), 0.0);

		/// Binalization
		cv::Mat imgBinary;
		cv::adaptiveThreshold(imgBlur, imgBinary, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 15, 2);

		/// Harris corner
		int blockSize = 3;
		int apertureSize = 3;
		double k = 0.04;
		cv::Mat dst = cv::Mat::zeros(imgBinary.size(), CV_32FC1);
		cv::cornerHarris(imgBinary, dst, blockSize, apertureSize, k);
		cv::Mat dst_norm;
		cv::normalize(dst, dst_norm, 0, 255, cv::NORM_MINMAX, CV_32FC1, cv::Mat());
		cv::Mat dst_norm_scaled;
		cv::convertScaleAbs(dst_norm, dst_norm_scaled);

		for (int i = 0; i < dst_norm.rows; i++)
		{
			for (int j = 0; j < dst_norm.cols; j++)
			{
				if ((int)dst_norm.at<float>(i, j) > thresh)
				{
					//cv::circle(dst_norm_scaled, cv::Point(j, i), 10, cv::Scalar(255), 2, 8, 0);
					cv::circle(dst_norm_scaled, cv::Point(j, i), 10, cv::Scalar(255), 2, 8, 0);
				}
			}
		}

		image::ImageDisp::show("dst_norm_scaled", dst_norm_scaled, 0);
	}

	int TrackerSubRegion::processMatch(const cv::Mat& frame,
		const std::vector<cv::KeyPoint>& targetKp,
		const cv::Mat& targetDesc,
		std::vector<cv::KeyPoint>& matched1,
		std::vector<cv::KeyPoint>& matched2)
	{
		std::vector<std::vector<cv::DMatch>> matches;
		matcher->knnMatch(first_desc, targetDesc, matches, 2);
		for (unsigned i = 0; i < matches.size(); i++)
		{
			if (matches[i][0].distance < nn_match_ratio * matches[i][1].distance)
			{
				matched1.push_back(first_kp[matches[i][0].queryIdx]);
				matched2.push_back(targetKp[matches[i][0].trainIdx]);
			}
		}

		return (int)matched1.size();
	}

	cv::Mat TrackerSubRegion::processHomography(const cv::Mat& frame,
		const std::vector<cv::KeyPoint>& matched1,
		const std::vector<cv::KeyPoint>& matched2,
		Stats& targetStats,
		std::vector<cv::KeyPoint>& inliers1,
		std::vector<cv::KeyPoint>& inliers2,
		std::vector<cv::DMatch>& inlier_matches)
	{
#ifdef _DEBUG
		if (matched1.size() != matched2.size())
		{
			std::cout << "Wrong sizes of matched 1 and 2" << std::endl;
			cv::Mat res;
			hconcat(first_frame, frame, res);
			return res;
		}
#endif
		cv::Mat inlier_mask, homography;
		std::vector<cv::Point2f> obj(matched1.size());
		std::vector<cv::Point2f> scene(matched2.size());
		for (unsigned i = 0; i < matched1.size(); i++)
		{
			obj[i] = matched1[i].pt;
			scene[i] = matched2[i].pt;
		}

		if (obj.size() >= 4)
		{
			homography = findHomography(obj, scene, cv::RANSAC, ransac_thresh, inlier_mask);

			if (homography.empty())
			{
				cv::Mat res;
				hconcat(first_frame, frame, res);
				targetStats.inliers = 0;
				targetStats.ratio = 0;
				return res;
			}
		}

		for (unsigned i = 0; i < matched1.size(); i++)
		{
			if (inlier_mask.at<uchar>(i))
			{
				int new_i = static_cast<int>(inliers1.size());
				inliers1.push_back(matched1[i]);
				inliers2.push_back(matched2[i]);
				inlier_matches.push_back(cv::DMatch(new_i, new_i, 0));
			}
		}

		targetStats.inliers = (int)inliers1.size();
		targetStats.ratio = targetStats.inliers * 1.0 / targetStats.matches;
		std::vector<cv::Point2f> new_bb;
		perspectiveTransform(object_bb, new_bb, homography);
		cv::Mat frame_with_bb = frame.clone();
		cv::Mat res;
		cv::Mat outImg0, outImg1;
		drawMatchedPointPairs(first_frame, frame_with_bb, res, outImg0, outImg1, inliers1, inliers2, inlier_matches, false);
		if (targetStats.inliers >= bb_min_inliers)
		{
			drawBoundingBox(frame_with_bb, new_bb);
		}

		return res;
	}
}