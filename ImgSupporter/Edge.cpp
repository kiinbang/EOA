#include "Edge.h"
#include "ImageDisp.h"

#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <codecvt>
#include <iostream>
#include <math.h>
#include <omp.h>
#include <locale>
#include <vector>

namespace edge
{
	/// wstring to string
	std::string uni2multi(const std::wstring& wst)
	{
		std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> strconverter;
		return strconverter.to_bytes(wst);
	}

	void extractEdge(const std::wstring& wImgPath, const bool show, const int lowTh, const int highTh)
	{
		std::string imgPath = uni2multi(wImgPath);
		cv::Mat img0 = cv::imread(imgPath);
		cv::Mat imgEdge;
		cv::Canny(img0, imgEdge, lowTh, highTh);

		if (show)
		{
			image::ImageDisp::show("Edge", imgEdge, 0);
		}
	}

	/*
	* dst: Output of the edge detector.It should be a grayscale image(although in fact it is a binary one)
	* lines : A vector that will store the parameters(xstart, ystart, xend, yend) of the detected lines
	* rho : The resolution of the parameter r in pixels.We use 1 pixel.
	* theta : The resolution of the parameter �� in radians.We use 1 degree(CV_PI / 180)
	* threshold : The minimum number of intersections to "*detect*" a line
	* srn and stn : Default parameters to zero.Check OpenCV reference for more info.
	*/
	std::vector<cv::Vec2f> extractLines(const cv::Mat& imgEdge, const bool showResult, const unsigned int waitTime)
	{
		std::vector<cv::Vec2f> lines;
		cv::HoughLines(imgEdge, lines, 1, CV_PI / 180, 150, 0, 0);

		if (showResult)
		{
			cv::Mat imgRet;

			cv::cvtColor(imgEdge, imgRet, cv::COLOR_GRAY2BGR);

			/// Draw the lines
			for (size_t i = 0; i < lines.size(); i++)
			{
				float rho = lines[i][0], theta = lines[i][1];
				cv::Point pt1, pt2;
				double a = cos(theta), b = sin(theta);
				double x0 = a * rho, y0 = b * rho;
				pt1.x = cvRound(x0 + 1000 * (-b));
				pt1.y = cvRound(y0 + 1000 * (a));
				pt2.x = cvRound(x0 - 1000 * (-b));
				pt2.y = cvRound(y0 - 1000 * (a));
				line(imgRet, pt1, pt2, cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
			}

			/// Show results		
			image::ImageDisp::show("Lines from Hough Transform", imgRet, waitTime);
		}		

		return lines;
	}

	/*
	* dst: Output of the edge detector.It should be a grayscale image(although in fact it is a binary one)
	* lines : A vector that will store the parameters(xstart, ystart, xend, yend) of the detected lines
	* rho : The resolution of the parameter r in pixels.We use 1 pixel.
	* theta : The resolution of the parameter �� in radians.We use 1 degree(CV_PI / 180)
	* threshold : The minimum number of intersections to "*detect*" a line
	* minLineLength : The minimum number of points that can form a line.Lines with less than this number of points are disregarded.
	* maxLineGap : The maximum gap between two points to be considered in the same line.
	*/
	std::vector<cv::Vec4i> extractLinesProbabilistic(const cv::Mat& imgEdge, const bool showResult, const unsigned int time)
	{
		std::vector<cv::Vec4i> linesP;

		const double rhoTh = 1.0;/// pixel
		const double thetaTh = 0.1;//deg
		const int minVotes = 50;
		const double minLineLength = 50.0;
		const double maxLineGap = 10.0;
		cv::HoughLinesP(imgEdge, linesP, rhoTh, CV_PI/180*thetaTh, minVotes, minLineLength, 10);
		if (showResult)
		{
			cv::Mat imgRet;

			cv::cvtColor(imgEdge, imgRet, cv::COLOR_GRAY2BGR);

			/// Draw the lines
			for (size_t i = 0; i < linesP.size(); i++)
			{
				cv::Vec4i l = linesP[i];
				line(imgRet, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
			}

			/// Show results
			image::ImageDisp::show("Result from Probabilistic Hough Transform", imgRet, time);
		}					

		return linesP;
	}

	/// Resize
	bool showLines(const std::string& title, const std::string& imgPath, const std::vector<geo::Line2D>& lines0, const std::vector<geo::Line2D>& lines1, const int waitTime, const double resizeRatio, const int B0, const int G0, const int R0, const int B1, const int G1, const int R1)
	{
		/// Load an image
		cv::Mat img0 = cv::imread(imgPath);

		if (img0.empty())
		{
			std::cout << "Failed to open the image: " << imgPath << std::endl;
			return false;
		}

		if (resizeRatio != 1.0)
			cv::resize(img0, img0, cv::Size(static_cast<int>(img0.size().width * resizeRatio), static_cast<int>(img0.size().height * resizeRatio)));

		/// Color to gray
		cv::Mat imgGray;
		if (img0.channels() == 3)
			cv::cvtColor(img0, imgGray, cv::COLOR_BGR2GRAY);
		else
			imgGray = img0;

		cv::Mat imgRet;
		cv::cvtColor(imgGray, imgRet, cv::COLOR_GRAY2BGR);

		/// Draw the lines
		for (size_t i = 0; i < lines0.size(); i++)
		{
			cv::Vec4i l;
			l[0] = static_cast<int>(lines0[i].s.x);
			l[1] = static_cast<int>(lines0[i].s.y);
			l[2] = static_cast<int>(lines0[i].e.x);
			l[3] = static_cast<int>(lines0[i].e.y);
			line(imgRet, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(R0, G0, B0), 3, cv::LINE_AA);
		}

		for (size_t i = 0; i < lines1.size(); i++)
		{
			cv::Vec4i l;
			l[0] = static_cast<int>(lines1[i].s.x);
			l[1] = static_cast<int>(lines1[i].s.y);
			l[2] = static_cast<int>(lines1[i].e.x);
			l[3] = static_cast<int>(lines1[i].e.y);
			line(imgRet, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(R1, G1, B1), 3, cv::LINE_AA);
		}

		cv::imshow(title, imgRet);
		cv::waitKey(waitTime);

		if(!imgRet.empty())
			imgRet.release();

		return true;
	}

	/// Resize
	bool showLines(const std::string& title, const std::string& imgPath, const std::vector<std::pair<geo::Line2D, geo::Line2D>>& linesPairs, const int waitTime, const double resizeRatio, const int B0, const int G0, const int R0, const int B1, const int G1, const int R1)
	{
		/// Load an image
		cv::Mat img0 = cv::imread(imgPath);

		if (img0.empty())
		{
			std::cout << "Failed to open the image: " << imgPath << std::endl;
			return false;
		}

		if (resizeRatio != 1.0)
			cv::resize(img0, img0, cv::Size(static_cast<int>(img0.size().width * resizeRatio), static_cast<int>(img0.size().height * resizeRatio)));

		/// Color to gray
		cv::Mat imgGray;
		if (img0.channels() == 3)
			cv::cvtColor(img0, imgGray, cv::COLOR_BGR2GRAY);
		else
			imgGray = img0;

		cv::Mat imgRet;
		cv::cvtColor(imgGray, imgRet, cv::COLOR_GRAY2BGR);

		/// Draw the lines
		for (size_t i = 0; i < linesPairs.size(); i++)
		{
			cv::Vec4i l;
			l[0] = static_cast<int>(linesPairs[i].first.s.x);
			l[1] = static_cast<int>(linesPairs[i].first.s.y);
			l[2] = static_cast<int>(linesPairs[i].first.e.x);
			l[3] = static_cast<int>(linesPairs[i].first.e.y);
			line(imgRet, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(R0, G0, B0), 3, cv::LINE_AA);

			l[0] = static_cast<int>(linesPairs[i].second.s.x);
			l[1] = static_cast<int>(linesPairs[i].second.s.y);
			l[2] = static_cast<int>(linesPairs[i].second.e.x);
			l[3] = static_cast<int>(linesPairs[i].second.e.y);
			line(imgRet, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(R1, G1, B1), 3, cv::LINE_AA);

			l[0] = static_cast<int>(linesPairs[i].first.s.x);
			l[1] = static_cast<int>(linesPairs[i].first.s.y);
			l[2] = static_cast<int>(linesPairs[i].second.e.x);
			l[3] = static_cast<int>(linesPairs[i].second.e.y);
			line(imgRet, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(R0+R1, G0+G1, B0+B1), 3, cv::LINE_AA);

			l[0] = static_cast<int>(linesPairs[i].second.s.x);
			l[1] = static_cast<int>(linesPairs[i].second.s.y);
			l[2] = static_cast<int>(linesPairs[i].first.e.x);
			l[3] = static_cast<int>(linesPairs[i].first.e.y);
			line(imgRet, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(R0 + R1, G0 + G1, B0 + B1), 3, cv::LINE_AA);
		}

		cv::imshow(title, imgRet);
		cv::waitKey(waitTime);

		if (!imgRet.empty())
			imgRet.release();

		return true;
	}

	/// use class LineSegment instead of this function because there is an error caused by alocating memory inside dll for vector template
	/// https://stackoverflow.com/questions/5373553/how-to-return-a-vector-in-a-dll
	bool extractLines(const std::wstring& wImgPath,
		std::vector<geo::Line2D>& found0,
		const std::wstring& wOutPath,
		const double resizeRatio,
		const unsigned int time,
		const unsigned int blurMaskSize,
		const unsigned int cannyLowTh,
		const double cannyLowHighRatio,
		const unsigned int cannyMaskSize,
		const bool showResult)
	{
		std::string imgPath = uni2multi(wImgPath.c_str());
		std::string outPath = uni2multi(wOutPath.c_str());

		/// Load an image
		cv::Mat img0 = cv::imread(imgPath);
		
		if (img0.empty())
		{
			std::cout << "Failed to open the image: " << imgPath << std::endl;
			return false;
		}

		/// Resize
		if(resizeRatio != 1.0)
			cv::resize(img0, img0, cv::Size(static_cast<int>(img0.size().width * resizeRatio), static_cast<int>(img0.size().height * resizeRatio)));

		/// Color to gray
		cv::Mat imgGray;
		if (img0.channels() == 3)
			cv::cvtColor(img0, imgGray, cv::COLOR_BGR2GRAY);
		else
			imgGray = img0;

		/// Median filter (blurring)
		cv::Mat imgMedian;
		if (blurMaskSize % 2 == 1 && blurMaskSize > 1)
			cv::medianBlur(imgGray, imgMedian, blurMaskSize);
		else
			imgMedian = imgGray;

		/// Canny
		if (cannyMaskSize % 2 != 1 && blurMaskSize < 3)
			return false;

		int highThreshold = static_cast<int>(cannyLowTh * cannyLowHighRatio);
		cv::Mat imgCanny;
		cv::Canny(imgMedian, imgCanny, cannyLowTh, highThreshold, cannyMaskSize);

		if(showResult)
			image::ImageDisp::show("Canny result: ", imgCanny, time);

		if (!cv::imwrite(outPath, imgCanny))
		{
			std::cout << "Error in writing an image: " << outPath << std::endl;
			return false;
		}

		/// Extract lines
		std::vector<cv::Vec4i> lines = extractLinesProbabilistic(imgCanny, showResult, time);

		std::vector<geo::Line2D> found;
		found.reserve(lines.size());

		for (const auto& l : lines)
		{
			geo::Line2D linef;
			linef.s.x = static_cast<float>(l[0]);
			linef.s.y = static_cast<float>(l[1]);
			linef.e.x = static_cast<float>(l[2]);
			linef.e.y = static_cast<float>(l[3]);
			found.push_back(linef);
		}

		// check it later,
		// copy found to found0, unless an error occurs
		// Figure it out, why found0 cannot be directly used for saving extracted lines
		found0 = found;

		return true;
	}

	bool runClosing(const std::wstring& wImgPath, 
		const std::string& outPath,
		const double resizeRatio)
	{
		std::string imgPath = uni2multi(wImgPath);

		/// Load an image
		cv::Mat img0 = cv::imread(imgPath);

		if (img0.empty())
		{
			std::wcout << L"Failed to open the image: " << wImgPath << std::endl;
			return false;
		}

		/// Resize
		cv::resize(img0, img0, cv::Size(static_cast<int>(img0.size().width * resizeRatio), static_cast<int>(img0.size().height * resizeRatio)));

		/// Color to gray
		cv::Mat imgGray;
		if (img0.channels() == 3)
			cv::cvtColor(img0, imgGray, cv::COLOR_BGR2GRAY);
		else
			imgGray = img0;

		/// Median blur
		cv::Mat imgMedian;
		cv::medianBlur(imgGray, imgMedian, 3);

		/// adaptive binarization
		cv::Mat imgBin;
		//cv::adaptiveThreshold(imgMedian, imgBin, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, blurMaskSize, 2);
		//cv::adaptiveThreshold(imgMedian, imgBin, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, blurMaskSize, 2);
		cv::adaptiveThreshold(imgMedian, imgBin, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 9, 2);
		//cv::threshold(imgMedian, imgBin, 127, 255, cv::THRESH_BINARY);

		/// Closing
		cv::Mat imgClose;
		cv::dilate(imgBin, imgClose, cv::Mat());
		cv::erode(imgClose, imgClose, cv::Mat());

		cv::imwrite(outPath, imgClose);

		return true;
	}

	bool runOpening(const std::wstring& wImgPath,
		const std::string& outPath,
		const double resizeRatio)
	{
		std::string imgPath = uni2multi(wImgPath);

		/// Load an image
		cv::Mat img0 = cv::imread(imgPath);

		if (img0.empty())
		{
			std::wcout << L"Failed to open the image: " << wImgPath << std::endl;
			return false;
		}

		/// Resize
		cv::resize(img0, img0, cv::Size(static_cast<int>(img0.size().width * resizeRatio), static_cast<int>(img0.size().height * resizeRatio)));

		/// Color to gray
		cv::Mat imgGray;
		if (img0.channels() == 3)
			cv::cvtColor(img0, imgGray, cv::COLOR_BGR2GRAY);
		else
			imgGray = img0;

		/// Median blur
		cv::Mat imgMedian;
		cv::medianBlur(imgGray, imgMedian, 3);

		/// adaptive binarization
		cv::Mat imgBin;
		//cv::adaptiveThreshold(imgMedian, imgBin, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, blurMaskSize, 2);
		//cv::adaptiveThreshold(imgMedian, imgBin, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, blurMaskSize, 2);
		cv::adaptiveThreshold(imgMedian, imgBin, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 9, 2);
		//cv::threshold(imgMedian, imgBin, 127, 255, cv::THRESH_BINARY);

		/// Opening
		cv::Mat imgOpen;
		cv::erode(imgBin, imgOpen, cv::Mat());
		cv::dilate(imgOpen, imgOpen, cv::Mat());

		cv::imwrite(outPath, imgOpen);

		return true;
	}

	int lowTh = 0;
	int highTh = 0;
	int kernelSize = 3;
	const int maxLowTh = 100;
	const int maxHighTh = 250;
	const int maxKernelSize = 19;
	cv::Mat imgGray;
	int maskSize = 3;
	const int maskSize0 = 3;
	int maskSizeMax = 19;
	int size = 0;
	int sizeCanny = 0;

	static void ontrackbar(int, void*)
	{
		cv::Mat imgBlur;
		cv::Mat imgEdge;

		/// Median blur
		maskSize = (size * 2) + maskSize0;
		if (maskSize < 3) maskSize = 3;
		if (maskSize > maskSizeMax) maskSize = maskSizeMax;
		cv::medianBlur(imgGray, imgBlur, maskSize);

		/// adaptive binarization
		//cv::adaptiveThreshold(imgBlur, imgBin, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, maskSize2, 2);

		/// Canny
		int kernelSize = (sizeCanny * 2) + maskSize0;
		if (kernelSize < 3) kernelSize = 3;
		if (kernelSize > maskSizeMax) kernelSize = maskSizeMax;
		const int ratio = 3;
		//cv::Canny(imgBlur, imgEdge, lowTh, lowTh * ratio, kernelSize);
		cv::Canny(imgBlur, imgEdge, lowTh, highTh, kernelSize);
		cv::imshow("Canny", imgEdge);
	}

	bool runCannyTrackbar(const std::wstring& wImgPath, const double resizeRatio)
	{
		std::string imgPath = uni2multi(wImgPath);

		/// Load an image
		cv::Mat img0 = cv::imread(imgPath);

		if (img0.empty())
		{
			std::wcout << L"Failed to open the image: " << wImgPath << std::endl;
			return false;
		}

		/// Resize: 10% size
		cv::resize(img0, img0, cv::Size(static_cast<int>(img0.size().width * 0.3), static_cast<int>(img0.size().height * 0.3)));

		/// Color to gray
		cv::cvtColor(img0, imgGray, cv::COLOR_BGR2GRAY);

		cv::namedWindow("Canny", cv::WINDOW_AUTOSIZE); // Create Window
		cv::createTrackbar("MaskSize", "Canny", &size, maskSizeMax, ontrackbar);
		//cv::createTrackbar("MaskSize(Binarization)", "Canny", &size2, maskSizeMax, ontrackbar);
		cv::createTrackbar("LowTh", "Canny", &lowTh, maxLowTh, ontrackbar);
		cv::createTrackbar("HighTh", "Canny", &highTh, maxHighTh, ontrackbar);
		cv::createTrackbar("KernelSize", "Canny", &sizeCanny, maskSizeMax, ontrackbar);

		ontrackbar(0, 0);

		cv::waitKey(0);

		return true;
	}

	class LineSegment : public LineSegmentPtr
	{
	public:
		LineSegment();
		~LineSegment();

		bool extractLines(const std::wstring& imgPath,
			const std::wstring& outPath,
			const double resizeRatio,// = 0.3,
			const unsigned int time,// = 0,
			const unsigned int blurMaskSize,// = 3,
			const unsigned int cannyLowTh,// = 5,
			const unsigned int cannyHighTh,// = 30,
			const unsigned int cannyMaskSize,// = 3,
			const bool showResult) override;// = true);

		geo::Line2D& operator[](const size_t idx) override;
		const geo::Line2D& operator[](const size_t idx) const override;
		const size_t size() const override;
	private:
		std::vector<geo::Line2D> lines;
	};

	LineSegment::LineSegment()
	{
	}

	LineSegment::~LineSegment()
	{
	}
	
	bool LineSegment::extractLines(const std::wstring& wImgPath,
		const std::wstring& wOutPath,
		const double resizeRatio,
		const unsigned int time,
		const unsigned int blurMaskSize,
		const unsigned int cannyLowTh,/// adjacent edge link
		const unsigned int cannyHighTh,/// edge threshold
		const unsigned int cannyMaskSize,
		const bool showResult)
	{
		std::string imgPath = uni2multi(wImgPath.c_str());
		std::string outPath = uni2multi(wOutPath.c_str());

		/// Load an image
		cv::Mat img0 = cv::imread(imgPath);

		if (img0.empty())
		{
			std::cout << "Failed to open the image: " << imgPath << std::endl;
			return false;
		}

		/// Resize
		if (resizeRatio != 1.0)
			cv::resize(img0, img0, cv::Size(static_cast<int>(img0.size().width * resizeRatio), static_cast<int>(img0.size().height * resizeRatio)));

		/// Color to gray
		cv::Mat imgGray;
		if (img0.channels() == 3)
			cv::cvtColor(img0, imgGray, cv::COLOR_BGR2GRAY);
		else
			imgGray = img0;

		/// Median filter (blurring)
		cv::Mat imgMedian;
		if (blurMaskSize % 2 == 1 && blurMaskSize > 1)
			cv::medianBlur(imgGray, imgMedian, blurMaskSize);
		else
			imgMedian = imgGray;

		if (false)
		{
			cv::adaptiveThreshold(imgMedian, imgMedian, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 5, 0);
			if (showResult)
				image::ImageDisp::show("adaptiveThreshold: ", imgMedian, time);

			if (blurMaskSize % 2 == 1 && blurMaskSize > 1)
				cv::medianBlur(imgMedian, imgMedian, blurMaskSize);

			if (showResult)
				image::ImageDisp::show("adaptiveThreshold: ", imgMedian, time);
		}

		if (false)
		{
			/// sobel test
			cv::Mat imgSobelH, imgSobelV;
			int ddepth = -1;
			int dx = 1;
			int dy = 0;
			int ksize = 3;
			double scale = 1.0;
			double delta = 0.0;
			int borderType = cv::BORDER_DEFAULT;

			cv::Sobel(imgGray, imgSobelV, ddepth, dx, dy, ksize, scale, delta, borderType);
			dx = 0;
			dy = 1;
			cv::Sobel(imgGray, imgSobelH, ddepth, dx, dy);
			cv::convertScaleAbs(imgSobelV, imgSobelV);
			cv::convertScaleAbs(imgSobelH, imgSobelH);

			cv::Mat dstAdd = imgSobelV + imgSobelH;
			cv::addWeighted(imgSobelV, 0.5, imgSobelH, 0.5, 0.0, dstAdd);

			image::ImageDisp::show("Sobel result: ", dstAdd, time);
		}

		/// Canny
		if (cannyMaskSize % 2 != 1 && blurMaskSize < 3)
			return false;

		cv::Mat imgCanny;
		cv::Canny(imgMedian, imgCanny, cannyLowTh, cannyHighTh, cannyMaskSize);

		if (showResult)
			image::ImageDisp::show("Canny result: ", imgCanny , time);

		if (!cv::imwrite(outPath, imgCanny))
		{
			std::cout << "Error in writing an image: " << outPath << std::endl;
			return false;
		}

		/// Extract lines
		std::vector<cv::Vec4i> linesVec4i = extractLinesProbabilistic(imgCanny, showResult, time);

		this->lines.clear();
		this->lines.resize(linesVec4i.size());

		for (size_t i = 0; i < linesVec4i.size(); ++i)
		{
			const auto& l = linesVec4i[i];
			geo::Line2D& linef = this->lines[i];

			linef.s.x = static_cast<float>(l[0]);
			linef.s.y = static_cast<float>(l[1]);
			linef.e.x = static_cast<float>(l[2]);
			linef.e.y = static_cast<float>(l[3]);
		}

		return true;
	}

	geo::Line2D& LineSegment::operator[](const size_t idx)
	{
		return lines[idx];
	}

	const geo::Line2D& LineSegment::operator[](const size_t idx) const
	{
		return lines[idx];
	}

	const size_t LineSegment::size() const
	{
		return lines.size();
	}

	std::shared_ptr<LineSegmentPtr> IMAGESUPPORTER_LIB getLineSegmentPtr()
	{
		std::shared_ptr<LineSegmentPtr> ptr(new LineSegment());
		return ptr;
	}
}