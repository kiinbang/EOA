#include "Feature.h"
#include "ImageDisp.h"

#include <iostream>
#include <fstream>

#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
//#include <opencv2/xfeatures2d.hpp> // Not free

namespace feature
{
	///Refer to
	///https://docs.opencv.org/3.4/dc/d16/tutorial_akaze_tracking.html
	
	void drawBoundingBox(cv::Mat& image, const std::vector<cv::Point2f>& bb);

	void copyImg(cv::InputArray src, const cv::Mat& dst);

	void drawMatchedPointPairs(cv::InputArray img0,
		cv::InputArray img1,
		cv::InputOutputArray _outImg,
		cv::Mat& outImg0,
		cv::Mat& outImg1,
		const std::vector<cv::KeyPoint>& pts,
		const std::vector<cv::KeyPoint>& pts1,
		const std::vector<cv::DMatch>& matches,
		const bool drawKeypoints);

	class TrackerSubRegion;

	class Tracker
	{
		friend class TrackerSubRegion;
	public:
		Tracker(cv::Ptr<cv::Feature2D> _detector, cv::Ptr<cv::DescriptorMatcher> _matcher);

		void setFirstFrame(cv::Mat& frame, std::vector<cv::Point2f>& bb, const std::string& title, Stats& stats);

		void setCenter(cv::Mat& frame, const std::string& title, Stats& stats);

		cv::Mat process(const cv::Mat& frame, Stats& stats);
		
		cv::Ptr<cv::Feature2D> getDetector();

	protected:
		cv::Ptr<cv::Feature2D> detector;
		cv::Ptr<cv::DescriptorMatcher> matcher;
		cv::Mat first_frame, first_desc;
		std::vector<cv::KeyPoint> first_kp;
		std::vector<cv::Point2f> object_bb;
	};
}