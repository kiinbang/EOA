#pragma once

#ifdef IMAGESUPPORTER_EXPORTS
#define IMAGESUPPORTER_LIB __declspec(dllexport)
#else
#define IMAGESUPPORTER_LIB __declspec(dllimport)
#endif

#include <ssm/include/Define.h>

namespace cv
{
	class Mat;
}

namespace image
{
	class IMAGESUPPORTER_LIB ImageDisp
	{
	public:
		ImageDisp();

		/**ImageDisp
		* Description : constructor
		* @param title : title of a display window
		*/
		ImageDisp(const char* title);

		/**ImageDisp
		* Description : constructor
		* @param title : title of a display window
		* @param path : image path
		* @param time : wait time
		*/
		ImageDisp(const char* title, const char* path, const unsigned int time);

		/**show
		* Description : show a image display window
		* @param path : image path
		* @param time : wait time
		* @return bool
		*/
		bool show(const char* path, const unsigned int time);

		/**show
		* Description : show a image display window
		* @param path : image path
		* @param posX : window LT position x
		* @param posY : window LT position y
		* @param w : window width
		* @param h : window height
		* @return bool
		*/
		bool show(const char* path, const int posX, const int posY, const int w, const int h);

		/**show
		* Description : show a given img
		* @param title0: display windows title
		* @param time : waitKey delay time (miliseconds)
		* @return bool
		*/
		bool show(cv::Mat& img0, const char* title0, const unsigned int time);

		/**show
		* Description : static function for showing a image display window
		* @param title : title of a display window
		* @param img : image(cv::Mat) instance
		* @param posX : window LT position x
		* @param posY : window LT position y
		* @param w : window width
		* @param h : window height
		* @param time : waitKey delay time (miliseconds)
		*/
		static void show(const char* title, cv::Mat& img, const unsigned int time, const int posX=0, const int posY=0, const int w=0, const int h=0);

		/**size
		* Description : static function for getting image size
		* @param path : image path
		* @return size
		*/
		static geo::Size size(const char* path);

	private:
		/**< window title */
		char winTitle[256];
	};
}