#include "ImageDisp.h"

#include <iostream>
#include <windows.h>

#include <opencv2/highgui.hpp>

#pragma warning(disable: 4996)
#pragma warning(suppress : 4996)

namespace image
{
	void callBackFunc(int event, int x, int y, int flags, void* userdata)
	{
		if (event == cv::EVENT_LBUTTONDOWN)
		{
			cv::Mat* img = static_cast<cv::Mat*>(((void**)userdata)[0]);
			int* ch = static_cast<int*>(((void**)userdata)[1]);

			std::cout << "(" << x << ", " << y << ")\t";

			uchar* pVal = new uchar[*ch];
			for (int i = 0; i < *ch; ++i)
			{
				pVal[i] = img->data[(y * img->size().width + x) * (*ch) + i];
				std::cout << static_cast<int>(pVal[i]) << " ";
			}

			std::cout << std::endl;

			delete[] pVal;
		}
		else if (event == cv::EVENT_RBUTTONDOWN)
		{
		}
		else if (event == cv::EVENT_MBUTTONDOWN)
		{
		}
		else if (event == cv::EVENT_MOUSEMOVE)
		{
		}
	}

	ImageDisp::ImageDisp()
	{
		strcpy(winTitle, "noname");
		cv::namedWindow("winTitle", cv::WINDOW_NORMAL);
	}

	ImageDisp::ImageDisp(const char* title)
	{
		strcpy(winTitle, title);
		cv::namedWindow(title, cv::WINDOW_NORMAL);
	}

	ImageDisp::ImageDisp(const char* title, const char* path, const unsigned int time)
	{
		strcpy(winTitle, title);
		cv::namedWindow(title, cv::WINDOW_NORMAL);
		show(path, time);
	}

	bool ImageDisp::show(const char* path, const unsigned int time)
	{
		cv::Mat img = cv::imread(path);
		if (img.empty())
			return false;

		//set the callback function for any mouse event
		int numChannels = img.channels();
		void* userdata[] = { &img, &numChannels };
		cv::setMouseCallback(winTitle, callBackFunc, userdata);

		cv::imshow(winTitle, img);
		cv::waitKey(time);

		if (!img.empty())
			img.release();

		return true;
	}

	bool ImageDisp::show(cv::Mat& img0, const char* title0, const unsigned int time)
	{
		if (img0.empty())
			return false;

		if (std::string(winTitle) != std::string(title0))
			cv::namedWindow(title0, cv::WINDOW_NORMAL);

		//set the callback function for any mouse event
		int numChannels = img0.channels();
		void* userdata[] = { &img0, &numChannels };
		cv::setMouseCallback(title0, callBackFunc, userdata);

		cv::imshow(title0, img0);
		cv::waitKey(time);

		return true;
	}

	void ImageDisp::show(const char* title, cv::Mat& img, const unsigned int time, const int posX, const int posY, const int w, const int h)
	{
		int halfScrW = static_cast<int>(GetSystemMetrics(SM_CXSCREEN) * 0.5);
		int halfScrH = static_cast<int>(GetSystemMetrics(SM_CYSCREEN) * 0.5);

		cv::Size winSize;
		if (w == 0)
			winSize.width = static_cast<int>(img.size().width * 0.5f);
		else
			winSize.width = w;

		if (winSize.width > halfScrW)
			winSize.width = halfScrW;

		winSize.height = winSize.width * img.size().height / img.size().width;

		cv::namedWindow(title, cv::WINDOW_NORMAL);
		//set the callback function for any mouse event
		int numChannels = img.channels();
		void* userdata[] = { &img, &numChannels };
		cv::setMouseCallback(title, callBackFunc, userdata);
		cv::moveWindow(title, posX, posY);
		cv::resizeWindow(title, winSize.width, winSize.height);

		cv::imshow(title, img);
		cv::waitKey(time);
	}

	bool ImageDisp::show(const char* path, const int posX, const int posY, const int w, const int h)
	{
		cv::Mat img = cv::imread(path);
		if (img.empty())
			return false;

		//set the callback function for any mouse event
		int numChannels = img.channels();
		void* userdata[] = { &img, &numChannels };
		cv::setMouseCallback(winTitle, callBackFunc, userdata);

		cv::Point2i winPos;
		winPos.x = posX;
		winPos.y = posY;

		cv::Size winSize;
		winSize.width = w;
		winSize.height = h;

		show(winTitle, img, winPos.x, winPos.y, winSize.width, winSize.height, 0);

		return true;
	}

	geo::Size ImageDisp::size(const char* path)
	{
		geo::Size size;
		size.w = 0;
		size.h = 0;

		cv::Mat img = cv::imread(path);
		if (img.empty())
			return size;

		size.w = img.size().width;
		size.h = img.size().height;

		if (!img.empty())
			img.release();

		return size;
	}
}