#pragma once

#ifdef IMAGESUPPORTER_EXPORTS
#define IMAGESUPPORTER_LIB __declspec(dllexport)
#else
#define IMAGESUPPORTER_LIB __declspec(dllimport)
#endif

#include <string>
#include <vector>

#include <ssm/include/Define.h>

namespace detect
{
	void IMAGESUPPORTER_LIB drawTargets(const std::string& imgPath, const std::vector<geo::Point2f>& targets, const std::string& title, const int time);

	std::vector<geo::Point2f> IMAGESUPPORTER_LIB detectTargets(const std::string& imgPath, const int boardW, const int boardH, const bool refineFound, bool& found);

	geo::Point2f IMAGESUPPORTER_LIB detectRedAreaCenter(const std::string& imgPath, const bool show);

	std::vector<std::vector<geo::Point2f>> IMAGESUPPORTER_LIB detectTargets(const std::vector<std::string>& imgPaths, const std::vector<geo::Size>& sizes, const bool refineFound, const bool show);

	std::vector<std::vector<geo::Point2f>> IMAGESUPPORTER_LIB detectTargets(const std::vector<std::string>& imgPaths, const std::vector<geo::Size>& sizes, std::vector<std::vector<geo::Point2f>>& centersInRed, const bool refineFound, const bool show);

	std::vector<std::vector<geo::Point2f>> IMAGESUPPORTER_LIB detectTargets(const std::vector<std::string>& imgPaths,
		const std::vector<geo::Size>& sizes,
		const bool refineFound,
		const bool show,
		std::vector<std::vector<float>>& redAreaOrientation);

	std::vector<std::vector<geo::Point2f>> IMAGESUPPORTER_LIB detectTargets(const std::vector<std::string>& imgPaths,
		const std::vector<geo::Size>& sizes,
		std::vector<std::vector<geo::Point2f>>& centers,
		std::vector<std::vector<geo::Point2f>>& centersInRed,
		const bool refineFound,
		const bool show,
		const bool organize);

	std::vector<geo::Point2f> IMAGESUPPORTER_LIB detectTargets(const std::string& imgPath,
		const std::vector<geo::Size>& sizes,
		std::vector<geo::Point2f>& centers,
		std::vector<geo::Point2f>& centersInRed,
		const bool refineFound,
		const bool show,
		bool organize);

	void IMAGESUPPORTER_LIB detectLines(const std::vector<std::string>& imgPaths, const bool refineFound, const bool show, const unsigned int time);
}