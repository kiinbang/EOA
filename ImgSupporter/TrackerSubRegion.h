#pragma once

#include "Tracker.h"

namespace feature
{
	class TrackerSubRegion : public Tracker
	{
	public:
		TrackerSubRegion(cv::Ptr<cv::Feature2D> _detector, cv::Ptr<cv::DescriptorMatcher> _matcher);

		void setSourceFrame(cv::Mat& frame, std::vector< std::vector<std::vector<cv::Point>>>& ptMask);

		void setRegion(Stats& stats, const std::string& title, std::vector<cv::Point>& mask);

		void processTargetKp(const cv::Mat& frame, Stats& targetStats, std::vector<cv::KeyPoint>& targetKp, cv::Mat& desc);

		void getDistinctPtsFromBinaryImg(const cv::Mat& frame, Stats& targetStats, std::vector<cv::KeyPoint>& targetKp, cv::Mat& desc, int thresh = 50);

		int processMatch(const cv::Mat& frame,
			const std::vector<cv::KeyPoint>& targetKp,
			const cv::Mat& targetDesc,
			std::vector<cv::KeyPoint>& matched1,
			std::vector<cv::KeyPoint>& matched2);

		cv::Mat processHomography(const cv::Mat& frame,
			const std::vector<cv::KeyPoint>& matched1,
			const std::vector<cv::KeyPoint>& matched2,
			Stats& targetStats,
			std::vector<cv::KeyPoint>& inliers1,
			std::vector<cv::KeyPoint>& inliers2,
			std::vector<cv::DMatch>& inlier_matches);
	};
}
