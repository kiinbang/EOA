#pragma once

#ifdef IMAGESUPPORTER_EXPORTS
#define IMAGESUPPORTER_LIB __declspec(dllexport)
#else
#define IMAGESUPPORTER_LIB __declspec(dllimport)
#endif

#include <string>

namespace imgutil
{
	bool IMAGESUPPORTER_LIB rgb2gray(const std::string& inPath, const std::string& outPath);
}
