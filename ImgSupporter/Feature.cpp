#include "Feature.h"
#include "ImageDisp.h"
#include "TrackerSubRegion.h"

#include <iostream>
#include <fstream>

#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
//#include <opencv2/xfeatures2d.hpp> // Not free

namespace feature
{
	void drawMatchedPointPairs(cv::InputArray img0,
		cv::InputArray img1,
		cv::InputOutputArray _outImg,
		cv::Mat& outImg0,
		cv::Mat& outImg1,
		const std::vector<cv::KeyPoint>& pts,
		const std::vector<cv::KeyPoint>& pts1)
	{
		if (img0.empty() || img1.empty())
			return;

		if (pts.size() != pts1.size())
			return;

		cv::Mat outImg;
		cv::Size img0size = img0.size();
		cv::Size img1size = img1.size();
		cv::Size size(img0size.width + img1size.width, MAX(img0size.height, img1size.height));
		const int cn0 = img0.channels();
		const int cn1 = img1.channels();
		const int outCn = std::max(3, std::max(cn0, cn1));
		_outImg.create(size, CV_MAKETYPE(img0.depth(), outCn));
		outImg = _outImg.getMat();
		outImg = cv::Scalar::all(255);

		outImg0 = outImg(cv::Rect(0, 0, img0size.width, img0size.height));
		outImg1 = outImg(cv::Rect(img0size.width, 0, img1size.width, img1size.height));

		copyImg(img0, outImg0);
		copyImg(img1, outImg1);

		/// Draw keypoints
		int thick = 2;
		int rad = 20;
		int R = 255, G = 255, B = 0;
		int cR0 = 0, cG0 = 0, cB0 = 255;
		int cR1 = 255, cG1 = 0, cB1 = 0;
		int cThick = -1;

		for (int i = 0; i < pts.size(); ++i)
		{
			const auto& p0 = pts[i];
			const auto& p1 = pts1[i];
			auto dpt2 = cv::Point2f(std::min(p1.pt.x + float(img0.size().width), float(outImg.size().width - 1)), p1.pt.y);			

			cv::line(outImg, cv::Point(static_cast<int>(p0.pt.x),
				static_cast<int>(p0.pt.y)),
				cv::Point(static_cast<int>(dpt2.x), static_cast<int>(dpt2.y)),
				cv::Scalar(B, G, R),
				thick);
			cv::circle(outImg, cv::Point(static_cast<int>(p0.pt.x), static_cast<int>(p0.pt.y)), rad, cv::Scalar(cB0, cG0, cR0), cThick);
			cv::circle(outImg, cv::Point(static_cast<int>(dpt2.x), static_cast<int>(dpt2.y)), rad, cv::Scalar(cB1, cG1, cR1), cThick);
		}
	}

	bool findFeatures(const std::string& input1, const std::string& input2, const unsigned int time, const DESCRIPTOR method, const bool show)
	{
		cv::Mat img_object = cv::imread(cv::samples::findFile(input1), cv::IMREAD_GRAYSCALE);
		cv::Mat img_scene = cv::imread(cv::samples::findFile(input2), cv::IMREAD_GRAYSCALE);
#ifdef _DEBUG
		image::ImageDisp::show("img1", img_object, time);
		image::ImageDisp::show("img2", img_scene, time);
#endif
		if (img_object.empty() || img_scene.empty())
		{
			std::cout << "Could not open or find the image!\n" << std::endl;
			return false;
		}
		//-- Step 1: Detect the keypoints using SURF or SIFT Detector, compute the descriptors
		cv::Mat descriptors_object, descriptors_scene;
		std::vector<cv::KeyPoint> keypoints_object, keypoints_scene;
		cv::Ptr<cv::Feature2D> detector;

		switch (method)
		{
		case DESCRIPTOR::SURF:
		{
			/* not free
			int minHessian = 400;
			detector = cv::xfeatures2d::SURF::create(minHessian);
			*/

			std::cout << "SURF is not supported now. Select other descriptors." << std::endl;
			return false;
		}
		break;
		case DESCRIPTOR::SIFT:
			detector = cv::SIFT::create();
			break;
		default:
			return false;
		}

		detector->detectAndCompute(img_object, cv::noArray(), keypoints_object, descriptors_object);
		detector->detectAndCompute(img_scene, cv::noArray(), keypoints_scene, descriptors_scene);

		//-- Step 2: Matching descriptor vectors with a FLANN based matcher
		// Since SUR or SIFT is a floating-point descriptor NORM_L2 is used
		cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
		std::vector< std::vector<cv::DMatch> > knn_matches;
		matcher->knnMatch(descriptors_object, descriptors_scene, knn_matches, 2);
		//-- Filter matches using the Lowe's ratio test
		const float ratio_thresh = 0.15f;//75f;
		std::vector<cv::DMatch> good_matches;
		for (size_t i = 0; i < knn_matches.size(); i++)
		{
			if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
			{
				good_matches.push_back(knn_matches[i][0]);
			}
		}

		//-- Localize the object
		std::cout << "Number of good matches: " << good_matches.size() << std::endl;
		if (good_matches.size() > 4)
		{
			//-- Draw matches
			cv::Mat outImg0, outImg1;
			cv::Mat img_matches;
			drawMatchedPointPairs(img_object, img_scene, img_matches, outImg0, outImg1, keypoints_object, keypoints_scene, good_matches, false);
			//
			//drawMatches(img_object, keypoints_object, img_scene, keypoints_scene, good_matches, img_matches, cv::Scalar::all(-1),
			//	cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
			//

			std::vector<cv::Point2f> obj;
			std::vector<cv::Point2f> scene;
			for (size_t i = 0; i < good_matches.size(); i++)
			{
				//-- Get the keypoints from the good matches
				obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
				scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
			}

			cv::Mat H = findHomography(obj, scene, cv::RANSAC);

			std::cout << "---Estimated homography---" << std::endl;
			std::cout << H << std::endl;

			//-- Get the corners from the image_1 ( the object to be "detected" )
			std::vector<cv::Point2f> obj_corners(4);
			obj_corners[0] = cv::Point2f(0, 0);
			obj_corners[1] = cv::Point2f((float)img_object.cols, 0);
			obj_corners[2] = cv::Point2f((float)img_object.cols, (float)img_object.rows);
			obj_corners[3] = cv::Point2f(0, (float)img_object.rows);
			std::vector<cv::Point2f> scene_corners(4);
			perspectiveTransform(obj_corners, scene_corners, H);
			//-- Draw lines between the corners (the mapped object in the scene - image_2 )
			line(img_matches, scene_corners[0] + cv::Point2f((float)img_object.cols, 0),
				scene_corners[1] + cv::Point2f((float)img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
			line(img_matches, scene_corners[1] + cv::Point2f((float)img_object.cols, 0),
				scene_corners[2] + cv::Point2f((float)img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
			line(img_matches, scene_corners[2] + cv::Point2f((float)img_object.cols, 0),
				scene_corners[3] + cv::Point2f((float)img_object.cols, 0), cv::Scalar(0, 255, 0), 4);
			line(img_matches, scene_corners[3] + cv::Point2f((float)img_object.cols, 0),
				scene_corners[0] + cv::Point2f((float)img_object.cols, 0), cv::Scalar(0, 255, 0), 4);

			//-- Show detected matches
			if(show)
				image::ImageDisp::show("Good Matches & Object detection", img_matches, 0);

			return true;
		}
		else
		{
			//-- Draw matches
			cv::Mat outImg0, outImg1;
			cv::Mat img_matches;
			drawMatchedPointPairs(img_object, img_scene, img_matches, outImg0, outImg1, keypoints_object, keypoints_scene, good_matches, true);

			//-- Show detected key points
			image::ImageDisp::show("Good Matches & Object detection", img_matches, 0);
			std::cout << "Lack of matched key points" << std::endl;

			return false;
		}
	}

	void drawStatistics(cv::Mat image, const Stats& stats)
	{
		static const int font = cv::FONT_HERSHEY_PLAIN;
		std::stringstream str1, str2, str3, str4;

		str1 << "Matches: " << stats.matches;
		str2 << "Inliers: " << stats.inliers;
		str3.precision(2);
		str3 << "Inlier ratio: " << stats.ratio;
		str4.precision(2);
		str4 << "FPS: " << std::fixed<< stats.fps;

		putText(image, str1.str(), cv::Point(0, image.rows - 120), font, 2, cv::Scalar::all(255), 3);
		putText(image, str2.str(), cv::Point(0, image.rows - 90), font, 2, cv::Scalar::all(255), 3);
		putText(image, str3.str(), cv::Point(0, image.rows - 60), font, 2, cv::Scalar::all(255), 3);
		putText(image, str4.str(), cv::Point(0, image.rows - 30), font, 2, cv::Scalar::all(255), 3);
	}

	void printStatistics(const std::string& name, const Stats& stats)
	{
		std::cout << name << std::endl;
		std::cout << "----------" << std::endl;

		std::cout << "Matches " << stats.matches << std::endl;
		std::cout << "Inliers " << stats.inliers << std::endl;
		std::cout.precision(2);
		std::cout << "Inlier ratio " << stats.ratio << std::endl;
		std::cout << "Keypoints " << stats.keypoints << std::endl;
		std::cout.precision(2);
		std::cout << "FPS " << std::fixed << stats.fps << std::endl;
		std::cout << std::endl;
	}

	bool trackFeatures(const std::vector<std::string>& imgPaths, const DESCRIPTOR method)
	{
		if (imgPaths.size() < 1)
		{
			std::cout << "Lack of image file paths" << std::endl;
			return false;
		}

		/// Descriptor
		cv::Ptr<cv::Feature2D> descriptor;
		std::string title;
		MATCHER matcherType = MATCHER::BF;

		switch (method)
		{
		case DESCRIPTOR::AKAZE:
			{
				cv::Ptr<cv::AKAZE> akaze = cv::AKAZE::create();
				akaze->setThreshold(akaze_thresh);
				descriptor = akaze;
				title = "AKAZE";
				matcherType = MATCHER::BF;
			}
			break;
		case DESCRIPTOR::ORB:
			descriptor = cv::ORB::create();
			title = "ORB";
			matcherType = MATCHER::BF;
			break;
		case DESCRIPTOR::SIFT:
			descriptor = cv::SIFT::create();
			matcherType = MATCHER::FLANN;
			title = "SIFT";
			break;
		case DESCRIPTOR::SURF:
		{
			title = "SURF";
			/*
			int minHessian = 400;
			descriptor = cv::xfeatures2d::SURF::create(minHessian);
			matcherType = MATCHER::FLANN;
			*/
			std::cout << "SURF is not supported now. Select other descriptors." << std::endl;
			return false;
		}
		break;
		default:
			return false;
		}

		/// Matcher
		cv::Ptr<cv::DescriptorMatcher> matcher;

		switch (matcherType)
		{
		case MATCHER::BF:
			matcher = cv::DescriptorMatcher::create("BruteForce-Hamming");
			break;
		case MATCHER::FLANN:
			// Since SUR or SIFT is a floating-point descriptor NORM_L2 is used
			matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
			break;
		default:
			return false;
		}

		Tracker tracker(descriptor, matcher);

		cv::Mat frame0 = cv::imread(imgPaths[0]);

		std::vector<cv::Point2f> bb;
		/// User selects ROI
		cv::Rect uBox = cv::selectROI(frame0);
		bb.push_back(cv::Point2f(static_cast<float>(uBox.x), static_cast<float>(uBox.y)));
		bb.push_back(cv::Point2f(static_cast<float>(uBox.x + uBox.width), static_cast<float>(uBox.y)));
		bb.push_back(cv::Point2f(static_cast<float>(uBox.x + uBox.width), static_cast<float>(uBox.y + uBox.height)));
		bb.push_back(cv::Point2f(static_cast<float>(uBox.x), static_cast<float>(uBox.y + uBox.height)));

		Stats stats;
		cv::Mat res;
		tracker.setFirstFrame(frame0, bb, title, stats);

		for (int i = 1; i < static_cast<int>(imgPaths.size()); ++i)
		{
			cv::Mat frame = cv::imread(imgPaths[i]);
			
			if (method == DESCRIPTOR::ORB)
			{
				cv::Ptr<cv::ORB> orb = std::dynamic_pointer_cast<cv::ORB>(descriptor);
				orb->setMaxFeatures(stats.keypoints);
			}

			res = tracker.process(frame, stats);
			drawStatistics(res, stats);
			image::ImageDisp::show("Result", res, 10);
			printStatistics(title, stats);
		}

		return true;
	}

	bool trackCapturePath(const std::vector<std::string>& imgPaths, const DESCRIPTOR method)
	{
		if (imgPaths.size() < 1)
		{
			std::cout << "Lack of image file paths" << std::endl;
			return false;
		}

		/// Descriptor
		cv::Ptr<cv::Feature2D> descriptor;
		std::string title;
		MATCHER matcherType = MATCHER::BF;

		switch (method)
		{
		case DESCRIPTOR::AKAZE: /// Need to test; it does not work well so far
		{
			cv::Ptr<cv::AKAZE> akaze = cv::AKAZE::create();
			akaze->setThreshold(akaze_thresh);
			descriptor = akaze;
			title = "AKAZE";
			matcherType = MATCHER::BF;
		}
		break;
		case DESCRIPTOR::ORB: /// Need to test; it does not work well so far
			descriptor = cv::ORB::create();
			title = "ORB";
			matcherType = MATCHER::BF;
			break;
		case DESCRIPTOR::SIFT:
			title = "SIFT";
			descriptor = cv::SIFT::create();
			matcherType = MATCHER::FLANN;
			break;
		case DESCRIPTOR::SURF: /// Not tried yet.
		{
			title = "SURF";
			/*
			int minHessian = 400;
			descriptor = cv::xfeatures2d::SURF::create(minHessian);
			matcherType = MATCHER::FLANN;
			*/
			std::cout << "SURF is not supported now. Select other descriptors." << std::endl;
			return false;
		}
		break;
		default:
			return false;
		}

		/// Matcher
		cv::Ptr<cv::DescriptorMatcher> matcher;

		switch (matcherType)
		{
		case MATCHER::BF:
			matcher = cv::DescriptorMatcher::create("BruteForce-Hamming");
			break;
		case MATCHER::FLANN:
			/// Since SUR or SIFT is a floating-point descriptor NORM_L2 is used
			matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
			break;
		default:
			return false;
		}

		Tracker tracker(descriptor, matcher);
		cv::Mat firsFrame = cv::imread(imgPaths[0]);
		cv::Mat& frame0 = firsFrame;
		for (int i = 1; i < static_cast<int>(imgPaths.size()); ++i)
		{
			Stats stats;
			cv::Mat res;
			tracker.setCenter(frame0, title, stats);

			cv::Mat frame = cv::imread(imgPaths[i]);

			if (method == DESCRIPTOR::ORB)
			{
				cv::Ptr<cv::ORB> orb = std::dynamic_pointer_cast<cv::ORB>(descriptor);
				orb->setMaxFeatures(stats.keypoints);
			}

			res = tracker.process(frame, stats);
			drawStatistics(res, stats);
			image::ImageDisp::show("Result", res, 10);
			printStatistics(title, stats);

			frame0 = frame;
		}

		return true;

	}
}

namespace feature
{
	class Tracker2// : public Tracker
	{
	public:
		Tracker2(cv::Ptr<cv::Feature2D> _detector, cv::Ptr<cv::DescriptorMatcher> _matcher) :
			detector(_detector),
			matcher(_matcher)
			//Tracker(_detector, _matcher)
		{}

		void setSourceFrame(cv::Mat& frame, std::vector< std::vector<std::vector<cv::Point>>>& ptMask)
		{
			ptMask.resize(numDividFactor);
			for (unsigned int i = 0; i < numDividFactor; ++i)
			{
				ptMask[i].resize(numDividFactor);
				for (unsigned int j = 0; j < numDividFactor; ++j)
				{
					ptMask[i][j].resize(4);//4 corners (LT, RT, RB, LB)
				}
			}

			auto size = frame.size();
			int dW = size.width / numDividFactor;
			int dH = size.height / numDividFactor;

			for (unsigned int r = 0; r < numDividFactor; ++r)
			{
				for (unsigned int c = 0; c < numDividFactor; ++c)
				{
					int h = dH * r;
					int w = dW * c;
					ptMask[r][c][0].x = w;
					ptMask[r][c][0].y = h;
					ptMask[r][c][1].x = ptMask[r][c][0].x + dW;
					ptMask[r][c][1].y = ptMask[r][c][0].y;
					ptMask[r][c][2].x = ptMask[r][c][0].x + dW;
					ptMask[r][c][2].y = ptMask[r][c][0].y + dH;
					ptMask[r][c][3].x = ptMask[r][c][0].x;
					ptMask[r][c][3].y = ptMask[r][c][0].y + dH;
				}
			}

			first_frame = frame.clone();
		}

		void setRegion(Stats& stats, const std::string& title, std::vector<cv::Point>& mask)
		{
			const int iSize = 4;//4 corners

			const cv::Point* ptContain = { &mask[0] };

			cv::Mat matMask = cv::Mat::zeros(first_frame.size(), CV_8UC1);
			fillPoly(matMask, &ptContain, &iSize, 1, cv::Scalar::all(255));
			detector->detectAndCompute(first_frame, matMask, first_kp, first_desc);
			stats.keypoints = (int)first_kp.size();

			std::vector<cv::Point2f> bbf;
			bbf.reserve(iSize);
			for (const auto& p : mask)
			{
				cv::Point pf(p.x, p.y);
				bbf.push_back(pf);
			}

#ifdef _DEBUG
			drawBoundingBox(first_frame, bbf);
#endif
			putText(first_frame, title, cv::Point(0, 60), cv::FONT_HERSHEY_PLAIN, 5, cv::Scalar::all(0), 4);
			object_bb = bbf;
		}

		void processTargetKp(const cv::Mat& frame, Stats& targetStats, std::vector<cv::KeyPoint>& targetKp, cv::Mat& desc)
		{
			detector->detectAndCompute(frame, cv::noArray(), targetKp, desc);
			targetStats.keypoints = (int)targetKp.size();
		}

		void getDistinctPtsFromBinaryImg(const cv::Mat& frame, Stats& targetStats, std::vector<cv::KeyPoint>& targetKp, cv::Mat& desc, int thresh = 50)
		{
			/// Blur
			cv::Mat imgBlur;
			cv::GaussianBlur(frame, imgBlur, cv::Size(3, 3), 0.0);

			/// Binalization
			cv::Mat imgBinary;
			cv::adaptiveThreshold(imgBlur, imgBinary, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 15, 2);

			/// Harris corner
			int blockSize = 3;
			int apertureSize = 3;
			double k = 0.04;
			cv::Mat dst = cv::Mat::zeros(imgBinary.size(), CV_32FC1);
			cv::cornerHarris(imgBinary, dst, blockSize, apertureSize, k);
			cv::Mat dst_norm;
			cv::normalize(dst, dst_norm, 0, 255, cv::NORM_MINMAX, CV_32FC1, cv::Mat());
			cv::Mat dst_norm_scaled;
			cv::convertScaleAbs(dst_norm, dst_norm_scaled);

			for (int i = 0; i < dst_norm.rows; i++)
			{
				for (int j = 0; j < dst_norm.cols; j++)
				{
					if ((int)dst_norm.at<float>(i, j) > thresh)
					{
						//cv::circle(dst_norm_scaled, cv::Point(j, i), 10, cv::Scalar(255), 2, 8, 0);
						cv::circle(dst_norm_scaled, cv::Point(j, i), 10, cv::Scalar(255), 2, 8, 0);
					}
				}
			}

			image::ImageDisp::show("dst_norm_scaled", dst_norm_scaled, 0);
		}

		int processMatch(const cv::Mat& frame,
			const std::vector<cv::KeyPoint>& targetKp,
			const cv::Mat& targetDesc,
			std::vector<cv::KeyPoint>& matched1,
			std::vector<cv::KeyPoint>& matched2)
		{
			std::vector<std::vector<cv::DMatch>> matches;
			matcher->knnMatch(first_desc, targetDesc, matches, 2);
			for (unsigned i = 0; i < matches.size(); i++)
			{
				if (matches[i][0].distance < nn_match_ratio * matches[i][1].distance)
				{
					matched1.push_back(first_kp[matches[i][0].queryIdx]);
					matched2.push_back(targetKp[matches[i][0].trainIdx]);
				}
			}

			return (int)matched1.size();
		}

		cv::Mat processHomography(const cv::Mat& frame,
			const std::vector<cv::KeyPoint>& matched1,
			const std::vector<cv::KeyPoint>& matched2,
			Stats& targetStats,
			std::vector<cv::KeyPoint>& inliers1,
			std::vector<cv::KeyPoint>& inliers2,
			std::vector<cv::DMatch>& inlier_matches)
		{
#ifdef _DEBUG
			if (matched1.size() != matched2.size())
			{
				std::cout << "Wrong sizes of matched 1 and 2" << std::endl;
				cv::Mat res;
				hconcat(first_frame, frame, res);
				return res;
			}
#endif
			cv::Mat inlier_mask, homography;
			std::vector<cv::Point2f> obj(matched1.size());
			std::vector<cv::Point2f> scene(matched2.size());
			for (unsigned i = 0; i < matched1.size(); i++)
			{
				obj[i] = matched1[i].pt;
				scene[i] = matched2[i].pt;
			}

			if (obj.size() >= 4)
			{
				homography = findHomography(obj, scene, cv::RANSAC, ransac_thresh, inlier_mask);

				if (homography.empty())
				{
					cv::Mat res;
					hconcat(first_frame, frame, res);
					targetStats.inliers = 0;
					targetStats.ratio = 0;
					return res;
				}
			}

			for (unsigned i = 0; i < matched1.size(); i++)
			{
				if (inlier_mask.at<uchar>(i))
				{
					int new_i = static_cast<int>(inliers1.size());
					inliers1.push_back(matched1[i]);
					inliers2.push_back(matched2[i]);
					inlier_matches.push_back(cv::DMatch(new_i, new_i, 0));
				}
			}

			targetStats.inliers = (int)inliers1.size();
			targetStats.ratio = targetStats.inliers * 1.0 / targetStats.matches;
			std::vector<cv::Point2f> new_bb;
			perspectiveTransform(object_bb, new_bb, homography);
			cv::Mat frame_with_bb = frame.clone();
			cv::Mat res;
			cv::Mat outImg0, outImg1;
			drawMatchedPointPairs(first_frame, frame_with_bb, res, outImg0, outImg1, inliers1, inliers2, inlier_matches, false);
			if (targetStats.inliers >= bb_min_inliers)
			{
				drawBoundingBox(frame_with_bb, new_bb);
			}

			return res;
		}

	protected:
		cv::Ptr<cv::Feature2D> detector;
		cv::Ptr<cv::DescriptorMatcher> matcher;
		cv::Mat first_frame, first_desc;
		std::vector<cv::KeyPoint> first_kp;
		std::vector<cv::Point2f> object_bb;
	};

	int lowThreshold = 0;
	int highThreshold = 50;
	cv::Mat img, imggray, detectedEdges, imgblur;

	void showCanny(int, void*)
	{
		cv::Mat detectedEdges;
		cv::Canny(imgblur, detectedEdges, lowThreshold, highThreshold, 3);
		image::ImageDisp::show("canny", detectedEdges, 0);
	}

	int thresh = 200;
	int max_thresh = 255;

	void showHarris(int, void*)
	{
		int blockSize = 3;
		int apertureSize = 3;
		double k = 0.04;

		cv::Mat dst = cv::Mat::zeros(img.size(), CV_32FC1);
		cv::cornerHarris(detectedEdges, dst, blockSize, apertureSize, k);
		cv::Mat dst_norm, dst_norm_scaled;
		cv::normalize(dst, dst_norm, 0, 255, cv::NORM_MINMAX, CV_32FC1, cv::Mat());
		cv::convertScaleAbs(dst_norm, dst_norm_scaled);

		for (int i = 0; i < dst_norm.rows; i++)
		{
			for (int j = 0; j < dst_norm.cols; j++)
			{
				if ((int)dst_norm.at<float>(i, j) > thresh)
				{
					circle(dst_norm_scaled, cv::Point(j, i), 5, cv::Scalar(0), 2, 8, 0);
				}
			}
		}

		image::ImageDisp::show("harris", dst_norm_scaled, 0);
	}

	cv::Mat detectCorners(const std::string& path)
	{
		//cv::Mat img = cv::imread(path, cv::IMREAD_GRAYSCALE);
		img = cv::imread(path);
		cvtColor(img, imggray, cv::COLOR_BGRA2GRAY);

		/// Blur
		cv::GaussianBlur(imggray, imgblur, cv::Size(5, 5), 0.0);
		image::ImageDisp::show("blur", imgblur, 0);

		/// Img merge
		cv::Mat dst = imgblur;
		dst = cv::Scalar::all(0);
		cv::Mat addweight;
		cv::copyTo(imggray, dst, imgblur);
		image::ImageDisp::show("copyTo", dst, 0);

		/// Canny edges
		cv::namedWindow("canny", cv::WINDOW_NORMAL);
		cv::createTrackbar("min_threshold", "canny", &lowThreshold, 1000, showCanny);
		cv::createTrackbar("max_threshold", "canny", &highThreshold, 1000, showCanny);
		showCanny(0, 0);

		/// Harris corners
		cv::namedWindow("harris", cv::WINDOW_NORMAL);
		showHarris(0, 0);

		return img;
	}

	void printTies(const std::string& path, const std::vector<cv::KeyPoint>& pts1, const std::vector<cv::KeyPoint>& pts2)
	{
		if (pts1.size() != pts2.size())
			return;

		std::fstream tie;
		tie.open(path, std::ios::out);

		for (int i = 0; i < pts1.size(); ++i)
		{
			const auto& obj = pts1[i];
			const auto& sce = pts2[i];

			tie << obj.pt.x << "\t";
			tie << obj.pt.y << "\t";
			tie << obj.angle << "\t";
			tie << obj.octave << "\t";
			tie << obj.size << "\t";
			tie << obj.response << "\t";

			tie << sce.pt.x << "\t";
			tie << sce.pt.y << "\t";
			tie << sce.angle << "\t";
			tie << sce.octave << "\t";
			tie << sce.size << "\t";
			tie << sce.response << std::endl;
		}

		tie.close();
	}

	bool trackCapturePathGlobal(const std::string& folder, const std::vector<std::string>& filenames, const DESCRIPTOR method, const bool show, const int time)
	{
		if (filenames.size() < 1)
		{
			std::cout << "Lack of image file paths" << std::endl;
			return false;
		}

		/// Descriptor
		cv::Ptr<cv::Feature2D> descriptor;
		std::string title;
		MATCHER matcherType = MATCHER::BF;

		switch (method)
		{
		case DESCRIPTOR::AKAZE: /// Need to test; it does not work well so far
		{
			cv::Ptr<cv::AKAZE> akaze = cv::AKAZE::create();
			akaze->setThreshold(akaze_thresh);
			descriptor = akaze;
			title = "AKAZE";
			matcherType = MATCHER::BF;
		}
		break;
		case DESCRIPTOR::ORB: /// Need to test; it does not work well so far
		{
			descriptor = cv::ORB::create();
			title = "ORB";
			matcherType = MATCHER::BF;
		}
		break;
		case DESCRIPTOR::SIFT:
		{
			const int nfeatures = 0;///default=0; The number of best features to retain.
			const int nOctaveLayers = 3;///default=3; number of layers in each octave
			const double contrastThreshold = 0.04;///default = 0.04; The contrast threshold used to filter out weak features in semi - uniform (low - contrast) regions. The larger the threshold, the less features are produced by the detector.
			const double edgeThreshold = 10;///default=10; larger the edgeThreshold, the less features are filtered out(more features are retained)
			const double sigma = 1.6; ///default=1.6;The sigma of the Gaussian applied to the input image at the octave. If your image is captured with a weak camera with soft lenses, you might want to reduce the number.
			descriptor = cv::SIFT::create(nfeatures, nOctaveLayers, contrastThreshold, edgeThreshold, sigma);
			//descriptor = cv::SIFT::create();
			matcherType = MATCHER::FLANN;
		}
		break;
		case DESCRIPTOR::SURF: /// Not tried yet.
		{
			/*
			int minHessian = 400;
			descriptor = cv::xfeatures2d::SURF::create(minHessian);
			if (!descriptor)
			{
				std::cout << "SURF is not supported now. Select other descriptors." << std::endl;
				return false;
			}
			matcherType = MATCHER::FLANN;
			*/
			std::cout << "SURF is not supported now. Select other descriptors." << std::endl;
			return false;
		}
		break;
		default:
			return false;
		}

		if (!descriptor)
			return false;

		/// Matcher
		cv::Ptr<cv::DescriptorMatcher> matcher;

		switch (matcherType)
		{
		case MATCHER::BF:
			matcher = cv::DescriptorMatcher::create("BruteForce-Hamming");
			break;
		case MATCHER::FLANN:
			/// Since SUR or SIFT is a floating-point descriptor NORM_L2 is used
			matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
			break;
		default:
			return false;
		}

		TrackerSubRegion tracker(descriptor, matcher);
		cv::Mat firsFrame = cv::imread(folder + "\\" + filenames[0]);
		cv::Mat& frame0 = firsFrame;
		for (int i = 1; i < static_cast<int>(filenames.size()); ++i)
		{
			cv::TickMeter tm;
			tm.start();

			std::vector< std::vector<std::vector<cv::Point>>> ptMask;
			tracker.setSourceFrame(frame0, ptMask);
			cv::Mat frame = cv::imread(folder + "\\" + filenames[i]);
			Stats targetStats;
			std::vector<cv::KeyPoint> targetKp;
			cv::Mat targetDesc;
			std::vector<cv::KeyPoint> matchedObjAll, matchedSceAll;
			tracker.processTargetKp(frame, targetStats, targetKp, targetDesc);
			for (auto& rowMask : ptMask)
			{
				for (auto& mask : rowMask)
				{
					Stats stats0;
					tracker.setRegion(stats0, title, mask);

					if (method == DESCRIPTOR::ORB)
					{
						cv::Ptr<cv::ORB> orb = std::dynamic_pointer_cast<cv::ORB>(descriptor);
						orb->setMaxFeatures(stats0.keypoints);
					}

					std::vector<cv::KeyPoint> matchedObj, matchedSce;
					targetStats.matches += tracker.processMatch(frame, targetKp, targetDesc, matchedObj, matchedSce);
					matchedObjAll.insert(matchedObjAll.end(), matchedObj.begin(), matchedObj.end());
					matchedSceAll.insert(matchedSceAll.end(), matchedSce.begin(), matchedSce.end());
				}
			}

			cv::Mat res0;
			cv::Mat outImg0, outImg1;
			drawMatchedPointPairs(frame0, frame, res0, outImg0, outImg1, matchedObjAll, matchedSceAll);

			if (show)
				image::ImageDisp::show("Result", res0, time);
			else
				cv::imwrite(folder + "\\temp\\" + filenames[i] + "_tie.jpg", res0);

			printTies(folder + "\\temp\\" + filenames[i] + "_tie.txt", matchedObjAll, matchedSceAll);

			std::vector<cv::KeyPoint> inliers1;
			std::vector<cv::KeyPoint> inliers2;
			std::vector<cv::DMatch> inlier_matches;
			cv::Mat res = tracker.processHomography(frame, matchedObjAll, matchedSceAll, targetStats, inliers1, inliers2, inlier_matches);
			drawStatistics(res, targetStats);

			printTies(folder + "\\temp\\" + filenames[i] + "_tie_inlier.txt", inliers1, inliers2);

			tm.stop();

			targetStats.fps = 1. / (tm.getTimeMilli() * 0.001);
			if (show)
				image::ImageDisp::show("Result", res, time);
			else
				cv::imwrite(folder + "\\temp\\" + filenames[i] + "_tie_inliers.jpg", res);

			printStatistics(title, targetStats);
			std::cout << "Elapsed time for the current frame: " << tm.getTimeMilli() / 1000.0 << "sec" << std::endl;

			frame0 = frame;
		}

		return true;
	}
}