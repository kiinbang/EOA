#include "ImgUtil.h"

#include <iostream>
#include <fstream>

#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>

namespace imgutil
{
	bool rgb2gray(const std::string& inPath, const std::string& outPath)
	{
		/// Load an image
		cv::Mat img0 = cv::imread(inPath);

		if (img0.empty())
		{
			std::cout << "Failed to open the image: " << inPath << std::endl;
			return false;
		}

		/// Color to gray
		cv::Mat imgGray;
		if (img0.channels() == 3)
			cv::cvtColor(img0, imgGray, cv::COLOR_BGR2GRAY);
		else
			return false;

		if (cv::imwrite(outPath, imgGray))
			return true;
		else
			return false;
	}
}