#pragma once

#ifdef IMAGESUPPORTER_EXPORTS
#define IMAGESUPPORTER_LIB __declspec(dllexport)
#else
#define IMAGESUPPORTER_LIB __declspec(dllimport)
#endif

#include <string>
#include <vector>

#include <ssm/include/Define.h>
#include <ssm/include/SSMMatrix.h>

namespace edge
{
	const unsigned int infinitWait = 0;
	const unsigned int wait500 = 500;
	const unsigned int wait2000 = 2000;

	void IMAGESUPPORTER_LIB extractEdge(const std::wstring& imgPath, const bool show, const int lowTh, const int highTh);

	bool IMAGESUPPORTER_LIB extractLines(const std::wstring& imgPath,
		std::vector<geo::Line2D>& found,
		const std::wstring& outPath,
		const double resizeRatio,// = 0.3,
		const unsigned int time,
		const unsigned int blurMaskSize,// = 3,
		const unsigned int cannyLowTh,// = 25,
		const double cannyLowHighRatio,// = 3.0,
		const unsigned int cannyMaskSize,// = 3,
		const bool showResult);// = true);

	bool IMAGESUPPORTER_LIB runCannyTrackbar(const std::wstring& imgPath, const double resizeRatio);

	bool IMAGESUPPORTER_LIB runClosing(const std::wstring& imgPath,
		const std::string& outPath,
		const double resizeRatio);

	bool IMAGESUPPORTER_LIB runOpening(const std::wstring& imgPath,
		const std::string& outPath,
		const double resizeRatio);

	bool IMAGESUPPORTER_LIB showLines(const std::string& title,
		const std::string& imgPath, 
		const std::vector<geo::Line2D>& lines0, 
		const std::vector<geo::Line2D>& lines1, 
		const int waitTime,
		const double resizeRatio, 
		const int B0, const int G0, const int R0, 
		const int B1, const int G1, const int R1);

	bool IMAGESUPPORTER_LIB showLines(const std::string& title, 
		const std::string& imgPath, 
		const std::vector<std::pair<geo::Line2D, geo::Line2D>>& linesPairs,
		const int waitTime,
		const double resizeRatio, 
		const int B0, const int G0, const int R0, 
		const int B1, const int G1, const int R1);

	class IMAGESUPPORTER_LIB LineSegmentPtr
	{
	public:
		virtual bool extractLines(const std::wstring& imgPath,
			const std::wstring& outPath,
			const double resizeRatio,// = 0.3,
			const unsigned int time,// = 0,
			const unsigned int blurMaskSize,// = 3,
			const unsigned int cannyLowTh,// = 5,
			const unsigned int cannyHighTh,// = 30,
			const unsigned int cannyMaskSize,// = 3,
			const bool showResult) = 0;// = true);

		virtual geo::Line2D& operator[](const size_t idx) = 0;
		virtual const geo::Line2D& operator[](const size_t idx) const = 0;
		virtual const size_t size() const = 0;
	};

	std::shared_ptr<LineSegmentPtr> IMAGESUPPORTER_LIB getLineSegmentPtr();
}