#include "Tracker.h"

namespace feature
{
	void drawBoundingBox(cv::Mat& image, const std::vector<cv::Point2f>& bb)
	{
		for (unsigned i = 0; i < bb.size() - 1; i++)
		{
			line(image, bb[i], bb[i + 1], cv::Scalar(0, 0, 255), 2);
		}

		line(image, bb[bb.size() - 1], bb[0], cv::Scalar(0, 0, 255), 2);
	}

	void copyImg(cv::InputArray src, const cv::Mat& dst)
	{
		CV_CheckType(src.type(), src.type() == CV_8UC1 || src.type() == CV_8UC3 || src.type() == CV_8UC4, "Unsupported source image");
		CV_CheckType(dst.type(), dst.type() == CV_8UC3 || dst.type() == CV_8UC4, "Unsupported destination image");

		const int src_cn = src.channels();
		const int dst_cn = dst.channels();

		if (src_cn == dst_cn)
			src.copyTo(dst);
		else if (src_cn == 1 && dst_cn == 3)
			cvtColor(src, dst, cv::COLOR_GRAY2BGR);
		else if (src_cn == 1 && dst_cn == 4)
			cvtColor(src, dst, cv::COLOR_GRAY2BGRA);
		else if (src_cn == 3 && dst_cn == 4)
			cvtColor(src, dst, cv::COLOR_BGR2BGRA);
		else if (src_cn == 4 && dst_cn == 3)
			cvtColor(src, dst, cv::COLOR_BGRA2BGR);
		else
			cv::error(cv::Error::StsInternal, "", CV_Func, __FILE__, __LINE__);
	}

	void drawMatchedPointPairs(cv::InputArray img0,
		cv::InputArray img1,
		cv::InputOutputArray _outImg,
		cv::Mat& outImg0,
		cv::Mat& outImg1,
		const std::vector<cv::KeyPoint>& pts,
		const std::vector<cv::KeyPoint>& pts1,
		const std::vector<cv::DMatch>& matches,
		const bool drawKeypoints)
	{
		if (img0.empty() || img1.empty())
			return;

		cv::Mat outImg;
		cv::Size img0size = img0.size();
		cv::Size img1size = img1.size();
		cv::Size size(img0size.width + img1size.width, MAX(img0size.height, img1size.height));
		const int cn0 = img0.channels();
		const int cn1 = img1.channels();
		const int outCn = std::max(3, std::max(cn0, cn1));
		_outImg.create(size, CV_MAKETYPE(img0.depth(), outCn));
		outImg = _outImg.getMat();
		outImg = cv::Scalar::all(255);

		outImg0 = outImg(cv::Rect(0, 0, img0size.width, img0size.height));
		outImg1 = outImg(cv::Rect(img0size.width, 0, img1size.width, img1size.height));

		copyImg(img0, outImg0);
		copyImg(img1, outImg1);

		/// Draw keypoints
		int thick = 2;
		int rad = 20;
		int cR0 = 0, cG0 = 0, cB0 = 255;
		int cR1 = 255, cG1 = 0, cB1 = 0;
		int cThick = -1;
		int R = 255, G = 255, B = 0;

		/// Draw matched points
		for (auto match : matches)
		{
#ifdef _DEBUG
			std::cout << match.queryIdx << "\t";
			std::cout << match.trainIdx << "\t";
			std::cout << match.distance << "\t";
			std::cout << match.imgIdx << std::endl;
#endif

			const auto& p0 = pts[match.queryIdx];
			const auto& p1 = pts1[match.trainIdx];
			const auto dpt2 = cv::Point2f(std::min(p1.pt.x + float(img0.size().width), float(outImg.size().width - 1)), p1.pt.y);

			cv::line(outImg, cv::Point(static_cast<int>(p0.pt.x),
				static_cast<int>(p0.pt.y)),
				cv::Point(static_cast<int>(dpt2.x), static_cast<int>(dpt2.y)),
				cv::Scalar(B, G, R),
				thick);

			if (!drawKeypoints)
			{
				cv::circle(outImg, cv::Point(static_cast<int>(p0.pt.x), static_cast<int>(p0.pt.y)), rad, cv::Scalar(cB0, cG0, cR0), cThick);
				cv::circle(outImg, cv::Point(static_cast<int>(dpt2.x), static_cast<int>(dpt2.y)), rad, cv::Scalar(cB1, cG1, cR1), cThick);
			}
		}

		if (drawKeypoints)
		{
			for (int i = 0; i < pts.size(); ++i)
			{
				const auto& p0 = pts[i];
				cv::circle(outImg, cv::Point(static_cast<int>(p0.pt.x), static_cast<int>(p0.pt.y)), rad, cv::Scalar(cB0, cG0, cR0), cThick);
			}

			for (int i = 0; i < pts1.size(); ++i)
			{
				const auto& p1 = pts1[i];
				auto dpt2 = cv::Point2f(std::min(p1.pt.x + float(img0.size().width), float(outImg.size().width - 1)), p1.pt.y);
				cv::circle(outImg, cv::Point(static_cast<int>(dpt2.x), static_cast<int>(dpt2.y)), rad, cv::Scalar(cB1, cG1, cR1), cThick);
			}
		}
	}

	Tracker::Tracker(cv::Ptr<cv::Feature2D> _detector, cv::Ptr<cv::DescriptorMatcher> _matcher) :
		detector(_detector),
		matcher(_matcher)
	{}

	void Tracker::setFirstFrame(cv::Mat& frame, std::vector<cv::Point2f>& bb, const std::string& title, Stats& stats)
	{
		cv::Point* ptMask = new cv::Point[bb.size()];
		const cv::Point* ptContain = { &ptMask[0] };
		int iSize = static_cast<int>(bb.size());
		for (size_t i = 0; i < bb.size(); i++) {
			ptMask[i].x = static_cast<int>(bb[i].x);
			ptMask[i].y = static_cast<int>(bb[i].y);
		}
		first_frame = frame.clone();
		cv::Mat matMask = cv::Mat::zeros(frame.size(), CV_8UC1);
		fillPoly(matMask, &ptContain, &iSize, 1, cv::Scalar::all(255));
		detector->detectAndCompute(first_frame, matMask, first_kp, first_desc);
		stats.keypoints = (int)first_kp.size();
		drawBoundingBox(first_frame, bb);
		putText(first_frame, title, cv::Point(0, 60), cv::FONT_HERSHEY_PLAIN, 5, cv::Scalar::all(0), 4);
		object_bb = bb;
		delete[] ptMask;
	}

	void Tracker::setCenter(cv::Mat& frame, const std::string& title, Stats& stats)
	{
		const int iSize = 4;
		std::vector<cv::Point> ptMask(iSize);
		cv::Point center;
		auto size = frame.size();
		center.x = size.width / 2;
		center.y = size.height / 2;

		int halfWidthROI = static_cast<int>((size.width * 0.3f) / 2.f);
		int halfHeightROI = static_cast<int>((size.height * 0.3f) / 2.f);
		ptMask[0].x = center.x - halfWidthROI;
		ptMask[0].y = center.y - halfHeightROI;
		ptMask[1].x = center.x + halfWidthROI;
		ptMask[1].y = center.y - halfHeightROI;
		ptMask[2].x = center.x + halfWidthROI;
		ptMask[2].y = center.y + halfHeightROI;
		ptMask[3].x = center.x - halfWidthROI;
		ptMask[3].y = center.y + halfHeightROI;

		const cv::Point* ptContain = { &ptMask[0] };

		first_frame = frame.clone();
		cv::Mat matMask = cv::Mat::zeros(frame.size(), CV_8UC1);
		fillPoly(matMask, &ptContain, &iSize, 1, cv::Scalar::all(255));
		detector->detectAndCompute(first_frame, matMask, first_kp, first_desc);
		stats.keypoints = (int)first_kp.size();

		std::vector<cv::Point2f> bbf;
		bbf.reserve(iSize);
		for (const auto& p : ptMask)
		{
			cv::Point pf(p.x, p.y);
			bbf.push_back(pf);
		}

		drawBoundingBox(first_frame, bbf);
		putText(first_frame, title, cv::Point(0, 60), cv::FONT_HERSHEY_PLAIN, 5, cv::Scalar::all(0), 4);
		object_bb = bbf;
	}

	cv::Mat Tracker::process(const cv::Mat& frame, Stats& stats)
	{
		cv::TickMeter tm;
		std::vector<cv::KeyPoint> kp;
		cv::Mat desc;
		tm.start();
		detector->detectAndCompute(frame, cv::noArray(), kp, desc);
		stats.keypoints = (int)kp.size();
		std::vector<std::vector<cv::DMatch>> matches;
		std::vector<cv::KeyPoint> matched1, matched2;
		matcher->knnMatch(first_desc, desc, matches, 2);
		for (unsigned i = 0; i < matches.size(); i++)
		{
			if (matches[i][0].distance < nn_match_ratio * matches[i][1].distance)
			{
				matched1.push_back(first_kp[matches[i][0].queryIdx]);
				matched2.push_back(kp[matches[i][0].trainIdx]);
			}
		}
		stats.matches = (int)matched1.size();
		cv::Mat inlier_mask, homography;

		std::vector<cv::Point2f> obj;
		std::vector<cv::Point2f> scene;
		for (unsigned i = 0; i < matches.size(); i++)
		{
			if (matches[i][0].distance < nn_match_ratio * matches[i][1].distance)
			{
				//-- Get the keypoints from the good matches
				obj.push_back(first_kp[matches[i][0].queryIdx].pt);
				scene.push_back(kp[matches[i][0].trainIdx].pt);
			}
		}

		if (matched1.size() >= 4)
		{
			homography = findHomography(obj, scene, cv::RANSAC, ransac_thresh, inlier_mask);
		}

		tm.stop();
		stats.fps = 1. / tm.getTimeSec();
		if (matched1.size() < 4 || homography.empty()) {
			cv::Mat res;
			hconcat(first_frame, frame, res);
			stats.inliers = 0;
			stats.ratio = 0;
			return res;
		}

		std::vector<cv::KeyPoint> inliers1, inliers2;
		std::vector<cv::DMatch> inlier_matches;

		for (unsigned i = 0; i < matched1.size(); i++) {
			if (inlier_mask.at<uchar>(i)) {
				int new_i = static_cast<int>(inliers1.size());
				inliers1.push_back(matched1[i]);
				inliers2.push_back(matched2[i]);
				inlier_matches.push_back(cv::DMatch(new_i, new_i, 0));
			}
		}
		stats.inliers = (int)inliers1.size();
		stats.ratio = stats.inliers * 1.0 / stats.matches;
		std::vector<cv::Point2f> new_bb;
		perspectiveTransform(object_bb, new_bb, homography);
		cv::Mat frame_with_bb = frame.clone();
		if (stats.inliers >= bb_min_inliers)
		{
			drawBoundingBox(frame_with_bb, new_bb);
		}

		cv::Mat res;
		//drawMatches(first_frame, inliers1, frame_with_bb, inliers2,
		//	inlier_matches, res,
		//	cv::Scalar(255, 0, 0), cv::Scalar(255, 0, 0));

		cv::Mat outImg0, outImg1;
		drawMatchedPointPairs(first_frame, frame_with_bb, res, outImg0, outImg1, inliers1, inliers2, inlier_matches, false);

		return res;
	}

	cv::Ptr<cv::Feature2D> Tracker::getDetector()
	{
		return detector;
	}
}