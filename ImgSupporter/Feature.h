#pragma once

#ifdef IMAGESUPPORTER_EXPORTS
#define IMAGESUPPORTER_LIB __declspec(dllexport)
#else
#define IMAGESUPPORTER_LIB __declspec(dllimport)
#endif

#include <string>
#include <vector>

#include <ssm/include/Define.h>

namespace feature
{
	/**houghtransforma
	* Description : find linear feature using hough transform
	* @param filename : image path
	* @return int
	*/
	int IMAGESUPPORTER_LIB houghtransforma(const std::string& filename);
    
	/**findFeatures
	* Description : find matched features between two images
	* @param input1 : first image path
	* @param input2 : second image path
	* @param method : descriptor type
	* @return bool
	*/
    bool IMAGESUPPORTER_LIB findFeatures(const std::string& input1, const std::string& input2, const unsigned int time, const DESCRIPTOR method = DESCRIPTOR::SIFT, const bool = true);

	/**trackFeatures
	* Description : find matched features between adjacent images
	* @param imgPaths : image file paths
	* @param method : descriptor type
	* @return bool
	*/
    bool IMAGESUPPORTER_LIB trackFeatures(const std::vector<std::string>& imgPaths, const DESCRIPTOR method = DESCRIPTOR::SIFT);

	/**trackCapturePath
	* Description : track camera move
	* @param imgPaths : image file paths
	* @param method : descriptor type
	* @return bool
	*/
	bool IMAGESUPPORTER_LIB trackCapturePath(const std::vector<std::string>& imgPaths, const DESCRIPTOR method = DESCRIPTOR::SIFT);

	/**trackCapturePathGlobal
	* Description : track camera move based on divided sub-regions
	* @param folder : current input directory
	* @param imgPaths : image file paths
	* @param method : descriptor type
	* @param show : option for showing result images
	* @param time : image view time
	* @param resval : matched points as results
	* @return bool
	*/
	bool IMAGESUPPORTER_LIB trackCapturePathGlobal(const std::string& folder, const std::vector<std::string>& filenames, const DESCRIPTOR method = DESCRIPTOR::SIFT, const bool show = false, const int time = 0);
}