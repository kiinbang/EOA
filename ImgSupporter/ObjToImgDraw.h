#pragma once

#ifdef IMAGESUPPORTER_EXPORTS
#define IMAGESUPPORTER_LIB __declspec(dllexport)
#define IMAGESUPPORTER_TEMPLATE
#else
#define IMAGESUPPORTER_LIB __declspec(dllimport)
#define IMAGESUPPORTER_TEMPLATE extern
#endif

#include <array>
#include <memory>
#include <string>
#include <vector>

#include <ssm/include/Define.h>
#include <ssm/include/SSMDataStructures.h>

#include "ImageDisp.h"

namespace cv
{
	class Mat;
}

namespace image
{
	struct RGB
	{
	public:
		unsigned char b;
		unsigned char g;
		unsigned char r;
	};

	class  IMAGESUPPORTER_LIB ObjToImg
	{
	public:
		virtual void DrawProjectiedLines(const std::vector<geo::Line2D>& lines) = 0;

		virtual void show() = 0;

		virtual bool createEmpty8UC3(const unsigned int w, const unsigned int h, const RGB& pixVal) = 0;

		virtual void fillPolygon(const std::vector<geo::Point2f>& poly, const RGB& inPix) = 0;

		virtual void fillPolygon(const std::vector<data::Point2D>& poly, const RGB& inPix) = 0;

		virtual void saveImg(const std::string& path) const = 0;
	};

	std::shared_ptr<ObjToImg> IMAGESUPPORTER_LIB createBasicObjToImg();

	bool IMAGESUPPORTER_LIB draw2DLines(const std::string& inImgPath, const std::string& outImgPath, const std::vector<geo::Line2D>& lines, const int R = 0, const int G = 0, const int B = 255);
}