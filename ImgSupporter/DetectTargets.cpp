#include "DetectTargets.h"
#include "ImageDisp.h"

#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <ssm/include/MeshDataTree.h>

#include <iostream>
#include <math.h>
#include <omp.h>
#include <vector>

namespace detect
{
	const int winSize = 11;
	const int type = cv::TermCriteria::EPS + cv::TermCriteria::COUNT;
	const int maxCount = 30;
	const double epsilon = 0.0001;
	cv::Scalar redRangeLower = cv::Scalar(0, 0, 100);
	cv::Scalar redRangeUpper = cv::Scalar(50, 50, 255);

	bool renumbering_2(const geo::Point2f centerInRed, const geo::Point2f center, std::vector<geo::Point2f>& targets);

	bool renumbering_3(const geo::Point2f centerInRed, const geo::Point2f center, std::vector<geo::Point2f>& targets, const unsigned int boardW, const unsigned int boardH);

	/// angle between two vectors
	double angleBetweenTwoVectorsInnerProduct(const double x1,
		const double y1,
		const double z1,
		const double x2,
		const double y2,
		const double z2)
	{
		double angle;
		double innerProduct = x1 * x2 + y1 * y2 + z1 * z2;
		double size1 = sqrt(x1 * x1 + y1 * y1 + z1 * z1);
		double size2 = sqrt(x2 * x2 + y2 * y2 + z2 * z2);

		if (size1 < std::numeric_limits<double>::epsilon() || size2 < std::numeric_limits<double>::epsilon())
			throw std::string("Error: a vector size is zero.");

		/// ang = acos(inner-product / |a||b|)
		angle = acos(innerProduct / size1 / size2);

		return angle;
	}

	/// angle between two vectors
	double angleBetweenTwoVectorsCrossProduct(const double x1,
		const double y1,
		const double z1,
		const double x2,
		const double y2,
		const double z2)
	{
		double angle;
		double crossProduct
			= x1 * y2 - x2 * y1
			+ y1 * z2 - y2 * z1
			+ z1 * x2 - z2 * x1;
		double size1 = sqrt(x1 * x1 + y1 * y1 + z1 * z1);
		double size2 = sqrt(x2 * x2 + y2 * y2 + z2 * z2);

		if (size1 < std::numeric_limits<double>::epsilon() || size2 < std::numeric_limits<double>::epsilon())
			throw std::string("Error: a vector size is zero.");

		/// ang = asin(cross-product / |a||b|)
		/// + : CCW
		/// - : CW
		angle = asin(crossProduct / size1 / size2);

		return angle;
	}

	void IMAGESUPPORTER_LIB drawTargets(const std::string& imgPath,
		const std::vector<geo::Point2f>& targets,
		const std::string& title,
		const int time)
	{
		int R = 0, G = 0, B = 255;
		int thick = 5;
		int rad = 20;
		int cR = 0, cG = 255, cB = 0;
		int cThick = -1;

		cv::Mat img = cv::imread(imgPath);
		if (!img.empty() && targets.size() > 0)
		{
			for (int i = 0; i < static_cast<int>(targets.size()) - 1; ++i)
			{
				cv::line(img, cv::Point(static_cast<int>(targets[i].x), static_cast<int>(targets[i].y)), cv::Point(static_cast<int>(targets[i + 1].x), static_cast<int>(targets[i + 1].y)), cv::Scalar(R, G, B), thick);
				cv::circle(img, cv::Point(static_cast<int>(targets[i].x), static_cast<int>(targets[i].y)), rad, (cR, cG, cB), cThick);
			}

			cv::circle(img, cv::Point(static_cast<int>(targets[targets.size() - 1].x), static_cast<int>(targets[targets.size() - 1].y)), rad, (cR, cG, cB), cThick);
		}

		std::cout << "show a detection result image with circles and lines" << std::endl;

		// Show results
		cv::namedWindow(title, cv::WINDOW_NORMAL);
		imshow(title, img);
		// Wait and Exit
		cv::waitKey(time);

		std::cout << "Done: drawTargets()" << std::endl;
	}

	std::vector<geo::Point2f> IMAGESUPPORTER_LIB detectTargets(const std::string& imgPath, const int boardW, const int boardH, const bool refineFound, bool& found)
	{
		std::vector<cv::Point2f> pointbuf;

		cv::Mat view = cv::imread(imgPath);
		if (!view.empty())
		{
			std::vector<cv::Point2f> pointbuf0;

			const auto& channels = view.channels();

			if (channels == 3)
			{
				//found = cv::findChessboardCorners(view, cv::Size(boardW, boardH), pointbuf0, cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FAST_CHECK | cv::CALIB_CB_NORMALIZE_IMAGE);
				found = cv::findChessboardCorners(view, cv::Size(boardW, boardH), pointbuf0, cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_FILTER_QUADS + cv::CALIB_CB_NORMALIZE_IMAGE);

				if (found)
				{
					pointbuf = pointbuf0;

					if (refineFound)
					{
						// improve the accuracy of the found corners
						cv::Mat viewGray;
						cv::cvtColor(view, viewGray, cv::COLOR_BGR2GRAY);
						cv::cornerSubPix(viewGray, pointbuf, cv::Size(winSize, winSize), cv::Size(-1, -1), cv::TermCriteria(type, maxCount, epsilon));
					}
				}
			}
		}

		std::vector<geo::Point2f> targets;
		targets.reserve(pointbuf.size());

		for (const auto&  cvp : pointbuf)
		{
			geo::Point2f p;
			p.x = cvp.x;
			p.y = cvp.y;
			targets.push_back(p);
		}

		return targets;
	}

	geo::Point2f IMAGESUPPORTER_LIB detectRedAreaCenter(const std::string& imgPath, const bool show)
	{
		geo::Point2f center;
		center.x = -1.;
		center.y = -1.;

		cv::Mat view0 = cv::imread(imgPath);

		if (view0.empty())
			return center;

		std::vector<cv::Point2f> pointbuf0;

		if (view0.channels() != 3)
			return center;

		cv::Mat mask;
		cv::inRange(view0, redRangeLower, redRangeUpper, mask);

		const auto size = mask.size();

#ifdef _DEBUG
		if (show)
		{
			cv::Mat view;
			cv::copyTo(view0, view, mask);
			image::ImageDisp::show("Mask", mask, 0);
			image::ImageDisp::show("Filtered-Red", view, 0);
		}
#endif
		int numChannels = mask.channels();
		uchar* data = mask.data;
		unsigned int count = 0;
		double sumR = 0.;
		double sumC = 0.;

		for (int r = 0; r < size.height; ++r)
		{
			for (int c = 0; c < size.width; ++c)
			{
				uchar pVal = 0;

				for (int ch = 0; ch < numChannels; ++ch)
				{
					try
					{
						pVal += (data[(r * size.width + c) * numChannels + ch]) / numChannels;
					}
					catch (...)
					{
						std::cout << "num channels: "<< numChannels << std::endl;
						throw std::runtime_error("Error from detectRedAreaCenter function in DetectTargets.cpp");
					}
				}

				if (pVal > 0)
				{
					sumR += static_cast<double>(r);
					sumC += static_cast<double>(c);
					++count;
				}
			}
		}

		if (count < 1)
			return center;

		center.x = static_cast<float>(sumC / static_cast<double>(count));
		center.y = static_cast<float>(sumR / static_cast<double>(count));

		std::cout << "center(col, row): " << center.x << ",\t" << center.y << std::endl;

		return center;
	}

	std::vector<std::vector<geo::Point2f>> IMAGESUPPORTER_LIB detectTargets(const std::vector<std::string>& imgPaths, const std::vector<geo::Size>& sizes, const bool refineFound, const bool show)
	{
		std::vector<std::vector<geo::Point2f>> foundTargets(imgPaths.size());

//#pragma omp parallel for
		for (int i=0; i<static_cast<int>(imgPaths.size()); ++i)
		{
			const auto& path = imgPaths[i];

			cv::Mat resultView;
			
			if (show)
			{
				resultView = cv::imread(path, cv::IMREAD_COLOR);
				if (resultView.empty())
				{
					std::cout << "Error opening image: " << imgPaths[i] << std::endl;
					continue;
				}
			}

#ifdef _DEBUG
			std::cout << "\n" << "---" << imgPaths[i] << "---" << std::endl;
#endif

			for (const auto& size : sizes)
			{
				bool found;
				std::vector<geo::Point2f> targets = detectTargets(path, size.w, size.h, refineFound, found);

				if (static_cast<int>(targets.size()) != (size.w * size.h))
				{
					found = false;
#ifdef _DEBUG
					std::cout << "Wrong number of detected targets." << std::endl;
#endif
				}

				if (show)
				{
					std::vector<cv::Point2f> pointbuf;
					pointbuf.reserve(targets.size());

					for (const auto& p : targets)
					{
						cv::Point2f cvp;
						cvp.x = p.x;
						cvp.y = p.y;
						pointbuf.push_back(cvp);

#ifdef _DEBUG
						std::cout << "\t" << p.x << "\t" << p.y << std::endl;
#endif
					}

					drawChessboardCorners(resultView, cv::Size(size.w, size.h), cv::Mat(pointbuf), found);
				}

				foundTargets[i].insert(foundTargets[i].end(), targets.begin(), targets.end());
			}

			if (show)
			{
				image::ImageDisp::show(
					std::string(std::string("Found targets: ") + std::to_string(i) + std::string("th")).c_str(), 
					resultView, 
					0);
			}
		}

		return foundTargets;
	}

	std::vector<std::vector<geo::Point2f>> IMAGESUPPORTER_LIB detectTargets(const std::vector<std::string>& imgPaths, const std::vector<geo::Size>& sizes, std::vector<std::vector<geo::Point2f>>& centersInRed, const bool refineFound, const bool show)
	{
		std::vector<std::vector<geo::Point2f>> foundTargets(imgPaths.size());
		centersInRed.resize(imgPaths.size());

		//#pragma omp parallel for
		for (int i = 0; i < static_cast<int>(imgPaths.size()); ++i)
		{
			const auto& path = imgPaths[i];

			cv::Mat resultView;

			if (show)
			{
				resultView = cv::imread(path, cv::IMREAD_COLOR);
				if (resultView.empty())
				{
					std::cout << "Error opening image: " << imgPaths[i] << std::endl;
					continue;
				}
			}

#ifdef _DEBUG
			std::cout << "\n" << "---" << imgPaths[i] << "---" << std::endl;
#endif

			centersInRed[i].resize(sizes.size());

			for (int j = 0; j < static_cast<int>(sizes.size()); ++j)
			{
				centersInRed[i][j] = detectRedAreaCenter(path, show);

				bool found;
				std::vector<geo::Point2f> targets = detectTargets(path, sizes[j].w, sizes[j].h, refineFound, found);

				if (static_cast<int>(targets.size()) != (sizes[j].w * sizes[j].h))
				{
					found = false;
#ifdef _DEBUG
					std::cout << "Wrong number of detected targets." << std::endl;
#endif
				}

				if (show)
				{
					std::vector<cv::Point2f> pointbuf;
					pointbuf.reserve(targets.size());

					for (const auto& p : targets)
					{
						cv::Point2f cvp;
						cvp.x = p.x;
						cvp.y = p.y;
						pointbuf.push_back(cvp);

#ifdef _DEBUG
						std::cout << "\t" << p.x << "\t" << p.y << std::endl;
#endif
					}

					drawChessboardCorners(resultView, cv::Size(sizes[j].w, sizes[j].h), cv::Mat(pointbuf), found);
				}

				foundTargets[i].insert(foundTargets[i].end(), targets.begin(), targets.end());
			}

			if (show)
			{
				image::ImageDisp::show(
					std::string(std::string("Found targets: ") + std::to_string(i) + std::string("th")).c_str(),
					resultView,
					0);
			}
		}

		return foundTargets;
	}

	
	std::vector<std::vector<geo::Point2f>> IMAGESUPPORTER_LIB detectTargets(const std::vector<std::string>& imgPaths,
		const std::vector<geo::Size>& sizes, 
		const bool refineFound, 
		const bool show,
		std::vector<std::vector<float>>& redAreaOrientation)
	{
		std::vector<std::vector<geo::Point2f>> foundTargets(imgPaths.size());
		std::vector<std::vector<geo::Point2f>> centers(imgPaths.size());
		std::vector<std::vector<geo::Point2f>> centersInRed(imgPaths.size());
		redAreaOrientation.resize(imgPaths.size());

		//#pragma omp parallel for
		for (int i = 0; i < static_cast<int>(imgPaths.size()); ++i)
		{
			const auto& path = imgPaths[i];

			cv::Mat resultView;

			if (show)
			{
				resultView = cv::imread(path, cv::IMREAD_COLOR);
				if (resultView.empty())
				{
					std::cout << "Error opening image: " << imgPaths[i] << std::endl;
					continue;
				}
			}

#ifdef _DEBUG
			std::cout << "\n" << "---" << imgPaths[i] << "---" << std::endl;
#endif

			centersInRed[i].resize(sizes.size());
			centers[i].resize(sizes.size());
			redAreaOrientation[i].resize(sizes.size());

			for (int j = 0; j < static_cast<int>(sizes.size()); ++j)
			{
				centersInRed[i][j] = detectRedAreaCenter(path, show);

				bool found;
				std::vector<geo::Point2f> targets = detectTargets(path, sizes[j].w, sizes[j].h, refineFound, found);

				if (static_cast<int>(targets.size()) != (sizes[j].w * sizes[j].h))
				{
					found = false;
#ifdef _DEBUG
					std::cout << "Wrong number of detected targets." << std::endl;
#endif
				}

				/// cneter of the target area
				geo::Point2f center;
				{
					double sum_x = 0.0;
					double sum_y = 0.0;
					for (auto t : targets)
					{
						sum_x += t.x;
						sum_y += t.y;
					}

					center.x = static_cast<float>(sum_x / targets.size());
					center.y = static_cast<float>(sum_y / targets.size());
				}

				centers[i][j] = center;

				if (show)
				{
					std::vector<cv::Point2f> pointbuf;
					pointbuf.reserve(targets.size());

					for (const auto& p : targets)
					{
						cv::Point2f cvp;
						cvp.x = p.x;
						cvp.y = p.y;
						pointbuf.push_back(cvp);

#ifdef _DEBUG
						std::cout << "\t" << p.x << "\t" << p.y << std::endl;
#endif
					}

					drawChessboardCorners(resultView, cv::Size(sizes[j].w, sizes[j].h), cv::Mat(pointbuf), found);
				}

				/// Orientation of a red area
				float dc = centersInRed[i][j].x - centers[i][j].x;
				float dr = centersInRed[i][j].y - centers[i][j].y;
				float dx = dc;
				float dy = -dr;
				redAreaOrientation[i][j] = -atan2(dx, dy);

				/// Accumulate all found targets
				foundTargets[i].insert(foundTargets[i].end(), targets.begin(), targets.end());
			}

			if (show)
			{
				image::ImageDisp::show(
					std::string(std::string("Found targets: ") + std::to_string(i) + std::string("th")).c_str(),
					resultView,
					0);
			}
		}

		return foundTargets;
	}

	std::vector<std::vector<geo::Point2f>> IMAGESUPPORTER_LIB detectTargets(const std::vector<std::string>& imgPaths,
		const std::vector<geo::Size>& sizes,
		std::vector<std::vector<geo::Point2f>>& centers,
		std::vector<std::vector<geo::Point2f>>& centersInRed,
		const bool refineFound,
		const bool show,
		bool organize)
	{
		std::vector<std::vector<geo::Point2f>> foundTargets(imgPaths.size());
		centersInRed.resize(imgPaths.size());
		centers.resize(imgPaths.size());

		//#pragma omp parallel for
		for (int i = 0; i < static_cast<int>(imgPaths.size()); ++i)
		{
			const auto& path = imgPaths[i];

			cv::Mat resultView;

			if (show)
			{
				resultView = cv::imread(path, cv::IMREAD_COLOR);
				if (resultView.empty())
				{
					std::cout << "Error opening image: " << imgPaths[i] << std::endl;
					continue;
				}
			}

#ifdef _DEBUG
			std::cout << "\n" << "---" << imgPaths[i] << "---" << std::endl;
#endif

			centersInRed[i].resize(sizes.size());
			centers[i].resize(sizes.size());

			for (int j = 0; j < static_cast<int>(sizes.size()); ++j)
			{
				centersInRed[i][j] = detectRedAreaCenter(path, show);

				bool found;
				std::vector<geo::Point2f> targets = detectTargets(path, sizes[j].w, sizes[j].h, refineFound, found);

				if (static_cast<int>(targets.size()) != (sizes[j].w * sizes[j].h))
				{
					found = false;
#ifdef _DEBUG
					std::cout << "Wrong number of detected targets." << std::endl;
#endif
				}

				/// center of the target area
				geo::Point2f center;
				{
					double sum_x = 0.0;
					double sum_y = 0.0;
					for (auto t : targets)
					{
						sum_x += t.x;
						sum_y += t.y;
					}

					center.x = static_cast<float>(sum_x / targets.size());
					center.y = static_cast<float>(sum_y / targets.size());
				}

				centers[i][j] = center;

				if (show)
				{
					std::vector<cv::Point2f> pointbuf;
					pointbuf.reserve(targets.size());

					for (const auto& p : targets)
					{
						cv::Point2f cvp;
						cvp.x = p.x;
						cvp.y = p.y;
						pointbuf.push_back(cvp);

#ifdef _DEBUG
						std::cout << "\t" << p.x << "\t" << p.y << std::endl;
#endif
					}

					drawChessboardCorners(resultView, cv::Size(sizes[j].w, sizes[j].h), cv::Mat(pointbuf), found);
				}

				/// Target re-numbering
				if (organize)
				{
					if (sizes[j].w == 15 && sizes[j].h == 23)
					{
						if (!renumbering_2(centersInRed[i][j], centers[i][j], targets))
							throw std::string("Error in target re-numbering: the red area is at neither the start nor the end.");
					}
					else
					{
						if (!renumbering_3(centersInRed[i][j], centers[i][j], targets, sizes[j].w, sizes[j].h))
							throw std::string("Error in target re-numbering: the red area is at neither the start nor the end.");
					}
				}

				/// Accumulate all found targets
				foundTargets[i].insert(foundTargets[i].end(), targets.begin(), targets.end());
			}

			if (show)
			{
				image::ImageDisp::show(
					std::string(std::string("Found targets: ") + std::to_string(i) + std::string("th")).c_str(),
					resultView,
					0);
			}
		}

		return foundTargets;
	}

	std::vector<geo::Point2f> IMAGESUPPORTER_LIB detectTargets(const std::string& imgPath,
		const std::vector<geo::Size>& sizes,
		std::vector<geo::Point2f>& centers,
		std::vector<geo::Point2f>& centersInRed,
		const bool refineFound,
		const bool show,
		bool organize)
	{
		std::vector<geo::Point2f> foundTargets;

		cv::Mat resultView;

		if (show)
		{
			resultView = cv::imread(imgPath, cv::IMREAD_COLOR);
			if (resultView.empty())
			{
				std::cout << "Error opening image: " << imgPath << std::endl;
				throw std::string("Error opening image: ") + imgPath;
			}
		}

#ifdef _DEBUG
		std::cout << "\n" << "---" << imgPath << "---" << std::endl;
#endif

		centersInRed.resize(sizes.size());
		centers.resize(sizes.size());

		for (int j = 0; j < static_cast<int>(sizes.size()); ++j)
		{
			centersInRed[j] = detectRedAreaCenter(imgPath, show);

			bool found;
			std::vector<geo::Point2f> targets = detectTargets(imgPath, sizes[j].w, sizes[j].h, refineFound, found);

			if (static_cast<int>(targets.size()) != (sizes[j].w * sizes[j].h))
			{
				found = false;
#ifdef _DEBUG
				std::cout << "Wrong number of detected targets." << std::endl;
#endif
			}

			/// cneter of the target area
			geo::Point2f center;
			{
				double sum_x = 0.0;
				double sum_y = 0.0;
				for (auto t : targets)
				{
					sum_x += t.x;
					sum_y += t.y;
				}

				center.x = static_cast<float>(sum_x / targets.size());
				center.y = static_cast<float>(sum_y / targets.size());
			}

			centers[j] = center;

			if (show)
			{
				std::vector<cv::Point2f> pointbuf;
				pointbuf.reserve(targets.size());

				for (const auto& p : targets)
				{
					cv::Point2f cvp;
					cvp.x = p.x;
					cvp.y = p.y;
					pointbuf.push_back(cvp);

#ifdef _DEBUG
					std::cout << "\t" << p.x << "\t" << p.y << std::endl;
#endif
				}

				drawChessboardCorners(resultView, cv::Size(sizes[j].w, sizes[j].h), cv::Mat(pointbuf), found);
			}

			/// Target re-numbering
			if (organize)
			{
				if (sizes[j].w == 15 && sizes[j].h == 23)
				{
					if (!renumbering_2(centersInRed[j], centers[j], targets))
						throw std::string("Error in target re-numbering: the red area is at neither the start nor the end.");
				}
				else
				{
					if (!renumbering_3(centersInRed[j], centers[j], targets, sizes[j].w, sizes[j].h))
						throw std::string("Error in target re-numbering: the red area is at neither the start nor the end.");
				}
			}

			/// Accumulate all found targets
			foundTargets.insert(foundTargets.end(), targets.begin(), targets.end());
			}

		if (show)
		{
			image::ImageDisp::show((std::string("Found targets in ") + imgPath).c_str(),
				resultView,
				0);
		}

		return foundTargets;
	}

	void IMAGESUPPORTER_LIB detectLines(const std::vector<std::string>& imgPaths, const bool refineFound, const bool show, const unsigned int time)
	{		
		for (unsigned int i = 0; i < static_cast<unsigned int>(imgPaths.size()); ++i)
		{
			const auto& path = imgPaths[i];

			cv::Mat src = cv::imread(path, cv::IMREAD_GRAYSCALE);

			if (src.empty())
			{
				std::cout << "Error opening image: " << imgPaths[i] << std::endl;
				continue;
			}

			/// Edge detection
			cv::Mat dst;
			Canny(src, dst, 50, 200, 3);

#ifdef _DEBUG
			if (show)
			{
				cv::namedWindow("Source", cv::WINDOW_NORMAL);
				imshow("Source", src);
				cv::waitKey(time);

				cv::namedWindow("Canny", cv::WINDOW_NORMAL);
				imshow("Canny", dst);
				cv::waitKey(time);
			}
#endif
			
			/// Copy edges to the images that will display the results in BGR
			cv::Mat cdst;
			cvtColor(dst, cdst, cv::COLOR_GRAY2BGR);

			cv::Mat cdstP = cdst.clone();

			/// Standard Hough Line Transform
			std::vector<cv::Vec2f> lines; // will hold the results of the detection
			HoughLines(dst, lines, 1, CV_PI / 180, 150, 0, 0); // runs the actual detection
			
			/// Draw the lines
			for (size_t i = 0; i < lines.size(); i++)
			{
				float rho = lines[i][0], theta = lines[i][1];
				cv::Point pt1, pt2;
				double a = cos(theta), b = sin(theta);
				double x0 = a * rho, y0 = b * rho;
				pt1.x = cvRound(x0 + 1000 * (-b));
				pt1.y = cvRound(y0 + 1000 * (a));
				pt2.x = cvRound(x0 - 1000 * (-b));
				pt2.y = cvRound(y0 - 1000 * (a));
				line(cdst, pt1, pt2, cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
			}

			/// Probabilistic Line Transform
			std::vector<cv::Vec4i> linesP; // will hold the results of the detection
			HoughLinesP(dst, linesP, 1, CV_PI / 180, 50, 50, 10); // runs the actual detection
			/// Draw the lines
			for (size_t i = 0; i < linesP.size(); i++)
			{
				cv::Vec4i l = linesP[i];
				line(cdstP, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
			}
			
			if (show)
			{
				/// Show results
				cv::namedWindow("Detected Lines (in red) - Standard Hough Line Transform", cv::WINDOW_NORMAL);
				imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst);
				cv::waitKey(time);

				cv::namedWindow("Detected Lines (in red) - Probabilistic Line Transform", cv::WINDOW_NORMAL);
				imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP);
				/// Wait and Exit
				cv::waitKey(time);
			}
		}
	}

	bool renumbering_2(const geo::Point2f centerInRed, const geo::Point2f center, std::vector<geo::Point2f>& targets)
	{
		double xTemp = 0.f;
		double yTemp = 1.f;

		/// Orientation of a red area
		float dc = centerInRed.x - center.x;
		float dr = centerInRed.y - center.y;
		float dx = dc;
		float dy = -dr;
		const float boardOrientationRed = -atan2(dx, dy);
		double xRed = xTemp * cos(boardOrientationRed) - yTemp * sin(boardOrientationRed);
		double yRed = xTemp * sin(boardOrientationRed) + yTemp * cos(boardOrientationRed);

		/// Orientation of the first mark
		dc = targets[0].x - center.x;
		dr = targets[0].y - center.y;
		dx = dc;
		dy = -dr;
		const float boardOrientationFirst = -atan2(dx, dy);
		double xFirst = xTemp * cos(boardOrientationFirst) - yTemp * sin(boardOrientationFirst);
		double yFirst = xTemp * sin(boardOrientationFirst) + yTemp * cos(boardOrientationFirst);

		/// Orientation of the last mark
		dc = targets[targets.size() - 1].x - center.x;
		dr = targets[targets.size() - 1].y - center.y;
		dx = dc;
		dy = -dr;
		const float boardOrientationLast = -atan2(dx, dy);
		double xLast = xTemp * cos(boardOrientationLast) - yTemp * sin(boardOrientationLast);
		double yLast = xTemp * sin(boardOrientationLast) + yTemp * cos(boardOrientationLast);

		double angleRedFirst = angleBetweenTwoVectorsInnerProduct(xRed, yRed, 0.0, xFirst, yFirst, 0.0);
		double angleRedLast = angleBetweenTwoVectorsInnerProduct(xRed, yRed, 0.0, xLast, yLast, 0.0);
		double angleFirstLast = angleBetweenTwoVectorsInnerProduct(xFirst, yFirst, 0.0, xLast, yLast, 0.0);

		double Threshold = 15.0 / 180.0 * 3.14159265358979;

		if (fabs(angleFirstLast) < fabs(3.14159265358979 - Threshold))
			throw std::string("Error in finding a red area or calculating an orientation angle");
		else if (fabs(angleRedFirst) < Threshold)
		{
			/// keep current target point order
		}
		else if (fabs(angleRedLast) < Threshold)
		{
			/// Reverse current target point order
			auto tempArray = targets;
			for (int i = 0; i < tempArray.size(); ++i)
			{
				targets[targets.size() - 1 - i] = tempArray[i];
			}
		}
		else
			return false;

		return true;
	}

	bool renumbering_3(const geo::Point2f centerInRed, const geo::Point2f center, std::vector<geo::Point2f>& targets, const unsigned int boardW, const unsigned int boardH)
	{
		double xTemp = 0.f;
		double yTemp = 1.f;

		/// Orientation of a red area
		float dc = centerInRed.x - center.x;
		float dr = centerInRed.y - center.y;
		float dx = dc;
		float dy = -dr;
		const float boardOrientationRed = -atan2(dx, dy);
		double xRed = xTemp * cos(boardOrientationRed) - yTemp * sin(boardOrientationRed);
		double yRed = xTemp * sin(boardOrientationRed) + yTemp * cos(boardOrientationRed);

		/// Orientation of the first mark
		dc = targets[0].x - center.x;
		dr = targets[0].y - center.y;
		dx = dc;
		dy = -dr;
		const float boardOrientationFirst = -atan2(dx, dy);
		double xFirst = xTemp * cos(boardOrientationFirst) - yTemp * sin(boardOrientationFirst);
		double yFirst = xTemp * sin(boardOrientationFirst) + yTemp * cos(boardOrientationFirst);

		/// Orientation of the last mark
		dc = targets[targets.size() - 1].x - center.x;
		dr = targets[targets.size() - 1].y - center.y;
		dx = dc;
		dy = -dr;
		const float boardOrientationLast = -atan2(dx, dy);
		double xLast = xTemp * cos(boardOrientationLast) - yTemp * sin(boardOrientationLast);
		double yLast = xTemp * sin(boardOrientationLast) + yTemp * cos(boardOrientationLast);

		double angleRedFirst = angleBetweenTwoVectorsCrossProduct(xRed, yRed, 0.0, xFirst, yFirst, 0.0);
		double angleRedLast = angleBetweenTwoVectorsCrossProduct(xRed, yRed, 0.0, xLast, yLast, 0.0);
		
		if (fabs(angleRedFirst) < 30.0/180*3.14159265358979)
		{
			auto tempArray = targets;
			for (int i = 0; i < tempArray.size(); ++i)
			{
				unsigned int r = unsigned int(i % boardH);
				r = boardH - 1 - r;
				unsigned int c = unsigned int(i / boardH);
				unsigned int newIdx = r * boardW + c;
				targets[newIdx] = tempArray[i];
			}
		}
		else if (fabs(angleRedLast) < 30.0 / 180 * 3.14159265358979)
		{
			auto tempArray = targets;
			for (int i = 0; i < tempArray.size(); ++i)
			{
				unsigned int r = unsigned int(i % boardH);
				unsigned int c = unsigned int(i / boardH);
				c = boardW - 1 - c;
				unsigned int newIdx = r * boardW + c;
				targets[newIdx] = tempArray[i];
			}
		}
		if (angleRedFirst < 0.0 && fabs(angleRedFirst) < 3.14159265358979)/// Normal orientation
		{
			/// keep current target point order
		}
		else if (angleRedFirst > 0.0 && fabs(angleRedFirst) < 3.14159265358979)/// Reverse current target point order
		{
			auto tempArray = targets;
			for (int i = 0; i < tempArray.size(); ++i)
			{
				targets[targets.size() - 1 - i] = tempArray[i];
			}
		}
		else
			return false;

		return true;
	}
}