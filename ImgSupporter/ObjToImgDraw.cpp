#include "ObjToImgDraw.h"

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/mat.hpp>

#include <ssm/include/utilitygrocery.h>

namespace image
{
	int R = 0, G = 0, B = 255;
	int thick = 5;
	int rad = 20;
	int cR = 0, cG = 255, cB = 0;
	int cThick = -1;

	const double epsilon = std::numeric_limits<double>::epsilon();

	double dummyMinCol = -1.0e10;

	/// How to get and set pixel color to Mat
	/// https://newbedev.com/c-and-opencv-get-and-set-pixel-color-to-mat

	/**Line2DIntersect
	* @brief :  find intersect point between two lines (https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection)
	* @param line1 : first line
	* @param line2 : second line
	* @param intPt : intersection point
	* @return bool
	*/
	bool get2DLineIntersection(const geo::Line2D& l1, const geo::Line2D& l2, geo::Point2f& intPt)
	{
		const double& x1 = l1.s.x;
		const double& x2 = l1.e.x;
		const double& y1 = l1.s.y;
		const double& y2 = l1.e.y;
		const double& x3 = l2.s.x;
		const double& x4 = l2.e.x;
		const double& y3 = l2.s.y;
		const double& y4 = l2.e.y;

		double D = (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4);
		if (D < epsilon)
			return false;

		double Nt = (x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4);
		double t = Nt / D;
		double Nu = (x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3);
		double u = Nu / D;
		intPt.x = static_cast<float>(x1 + t * (x2 - x1));
		intPt.y = static_cast<float>(y1 + t * (y2 - y1));

		if (t < 0. || t > 1.0 || u < 0. || u > 1.0)
			return false;
		else
			return true;
	};

	/**isInside
	*@brief check if a point is inside a polygon
	*@param poly: polygon vertices
	*@param pt	: a point in query
	*@return bool
	*/
	bool isInsdide(const std::vector<geo::Point2f>& poly, const geo::Point2f& pt)
	{
		const size_t n = poly.size();
		if (n < 3) return false;

		int count = 0;

		double sameY = pt.y;
		double minX = dummyMinCol;

		int n_end_point = 0;
		for (size_t i = 0; i < n; i++)
		{
			size_t j = i + 1;
			if (i == n - 1) j = 0;
			double x3, y3;
			x3 = poly[i].x;
			y3 = poly[i].y;
			double x4, y4;
			x4 = poly[j].x;
			y4 = poly[j].y;

			geo::Point2f intersection;
			geo::Line2D l1, l2;
			l1.s.x = pt.x;
			l1.s.y = pt.y;
			l1.e.x = static_cast<float>(minX);
			l1.e.y = static_cast<float>(sameY);
			l2.s.x = static_cast<float>(x3);
			l2.s.y = static_cast<float>(y3);
			l2.e.x = static_cast<float>(x4);
			l2.e.y = static_cast<float>(y4);
			if(get2DLineIntersection(l1, l2, intersection))
				++count;
		}

		if (count % 2 == 0)
			return false;
		else
			return true;
	}

	class  BasicObjToImg : public ObjToImg
	{
	private:
		cv::Mat img;
		unsigned int width;
		unsigned int height;

	public:
		BasicObjToImg()
		{
			width = 0;
			height = 0;
		}

		~BasicObjToImg()
		{
			if(!img.empty())
				img.release();
		}

		void DrawProjectiedLines(const std::vector<geo::Line2D>& lines) override
		{
			for (const auto& l : lines)
			{
				cv::line(img, cv::Point(static_cast<int>(l.s.x + 0.5), static_cast<int>(l.s.y + 0.5)),
					cv::Point(static_cast<int>(l.e.x + 0.5), static_cast<int>(l.e.y + 0.5)),
					cv::Scalar(R, G, B), thick);
			}
		}

		void show() override
		{
			image::ImageDisp disp;
			disp.show(img, "ObjToDraw", 0);
		}

		bool createEmpty8UC3(const unsigned int w, const unsigned int h, const RGB& pixVal) override
		{
			if (w < 1 || h < 1)
				return false;

			if(!img.empty())
				img.release();
			img = cv::Mat(h, w, CV_8UC3, cv::Scalar(pixVal.b, pixVal.g, pixVal.r));

			dummyMinCol = -static_cast<double>(width);

			if (img.empty())
				return false;

			width = w;
			height = h;

			return true;
		}

		void fillPolygon(const std::vector<geo::Point2f>& poly, const RGB& inPix) override
		{
			unsigned int maxC = 0, maxR = 0;
			unsigned int minC = width-1, minR = height-1;

			bool inPoly = false;
			/// MBR (minimum bounding retangle)
			for (unsigned int i = 0; i < poly.size(); i++)
			{
				unsigned int c = static_cast<unsigned int>(poly[i].x + 0.5f);
				unsigned int r = static_cast<unsigned int>(poly[i].y + 0.5f);

				if (!inPoly)
				{
					if (c < width && c >= 0 && r < height && r >= 0)
						inPoly = true;
				}

				if (c > maxC) maxC = c;
				if (c < minC) minC = c;
				if (r > maxR) maxR = r;
				if (r < minR) minR = r;
			}

			if (!inPoly)
				return;

			if (maxC >= width) maxC = width - 1;
			if (maxR >= height) maxR = height - 1;
			if (minC < 0) minC = 0;
			if (minR < 0) minR = 0;

			/// Inside polygon
			for (unsigned int r = minR; r <= maxR; ++r)
			{
				for (unsigned int c = minC; c <= maxC; ++c)
				{
					geo::Point2f pt;
					pt.x = static_cast<float>(c);
					pt.y = static_cast<float>(r);
					if (isInsdide(poly, pt))
					{
						img.ptr<RGB>(r)[c] = inPix;
					}
				}
			}
		}

		void fillPolygon(const std::vector<data::Point2D>& poly, const RGB& inPix) override
		{
			std::vector<geo::Point2f> poly_(poly.size());
			for (unsigned int i = 0; i < poly.size(); ++i)
			{
				poly_[i].x = static_cast<float>(poly[i](0));
				poly_[i].y = static_cast<float>(poly[i](1));
			}

			fillPolygon(poly_, inPix);
		}

		void saveImg(const std::string& path) const override
		{
			cv::imwrite(path, img);
		}
	};

	std::shared_ptr<ObjToImg> createBasicObjToImg()
	{
		std::shared_ptr<ObjToImg> retVal(new BasicObjToImg());
		return retVal;
	}

	bool draw2DLines(const std::string& inImgPath, const std::string& outImgPath, const std::vector<geo::Line2D>& lines, const int R, const int G, const int B)
	{
		int thick = 5;

		cv::Mat img = cv::imread(inImgPath);

		if (img.empty())
		{
			std::cout << "Error in opening an image: " << inImgPath << std::endl;
			return false;
		}

		for (const auto& l : lines)
		{
			cv::line(img, cv::Point(static_cast<int>(l.s.x + 0.5), static_cast<int>(l.s.y + 0.5)),
				cv::Point(static_cast<int>(l.e.x + 0.5), static_cast<int>(l.e.y + 0.5)),
				cv::Scalar(B, G, R), thick);
		}

		if (false == cv::imwrite(outImgPath, img))
		{
			std::cout << "Error in writing an image: " << outImgPath << std::endl;
			return false;
		}
		else
			return true;
	}
}