#include "FreeFormSurf.h"

#include <ssm/include/MeshDataTree.h>
#include <ssm/include/utilitygrocery.h>
#include <ssm/include/viewsim.hpp>

namespace obj
{
	const double pi = 3.14159265358979;
	const double hlfPi = pi * 0.5;
	
	const int SHOWGLWIN = 0;
	const int SAVERENDER = 1;
	const int PREPAREOBJPT = 2;

	const int baseW = 1432;
	const int baseH = 954;

	struct RenderInfo
	{
		double sizeRatio;
		double rendPixPitch;
		int rendW;
		int rendH;
	};

	double computeAngleIn3Vectors(const double ax, const double ay, const double az, const double bx, const double by, const double bz, bool acuteAngle)
	{
		double sizeA = sqrt(ax * ax + ay * ay + az * az);
		double sizeB = sqrt(bx * bx + by * by + bz * bz);

		if (sizeA < std::numeric_limits<double>::epsilon() ||
			sizeB < std::numeric_limits<double>::epsilon())
		{
			return 0.0;
		}

		/// Determine the size of angle between two vector Using inner product
		double inpro = ax * bx + ay * by + az * bz;

		double angRad = acos(inpro / sizeA / sizeB);

		if (acuteAngle && fabs(angRad) > hlfPi)
		{
			if (angRad > 0)
				return angRad - pi;
			else
				return angRad + pi;
		}
		else
			return angRad;
	};

	class CompObjSpace : public CompObjSpaceApi
	{
	public:
		CompObjSpace();

	public:/// override functions
		/**setObjData
		*@brief put obj and meta data paths
		*@param objPaths: obj files' paths (.obj)
		*@param metaPaths: meta files' paths (.gxxml)
		*/
		void setObjData(const std::vector<std::string>& objPaths, const std::vector <std::string>& metaPaths) override;

		/**setImgDataForObjCoord
		*@brief put camera and image info (position and pose information)
		*@param pos: camera position (x, y, z)
		*@param opk: camera orientation(o, p, k)
		*@param cam: camera smart pointer
		*@return bool
		*/
		bool setImgDataForObjCoord(const std::vector<double>& pos, const std::vector<double>& opk, const std::shared_ptr<sensor::Camera>& cam) override;

		/**getObjPts
		*@brief get object space coordinates of a given pixel, using a existing simulated image
		*@param cam: camera 
		*@param col: col coordinate (integer)
		*@param row: row coordinate (integer)
		*@param x: computed x coord
		*@param y: computed y coord
		*@param z: computed z coord
		*@return bool
		*/
		bool getObjPts(const std::shared_ptr<sensor::Camera>& cam, const unsigned int col, unsigned int row, double& x, double& y, double& z) override;

		/**getObjPts
		*@brief get object space coordinates of polygons (damages)
		*@param damageInfo: damage information
		*@param maxDist: maximum distance to an object surface
		*@return bool
		*/
		bool getObjPts(DamageInfoGroup& damageGroup, const double maxDist) override;

		/**determineObj
		*@brief determine an object of a given image point
		*@param nCols: number of columns
		*@param nRows: number of rows
		*@param pos: camera positions (x, y, z) for n images
		*@param opk: camera orientations(o, p, k) for n images
		*@param cam: camera smart pointer
		*@param maxDist: max-distance between an image and a model center
		*@param objPts: object coordinates of the given pixels for n images
		*@return no of objects: -1 means failure
		*/
		std::vector<std::vector<int>> determineObj(const unsigned int nCols,
			const unsigned int nRows,
			const std::vector<std::vector<double>>& pos,
			const std::vector<std::vector<double>>& opk,
			const std::shared_ptr<sensor::Camera>& cam,
			const double maxDist,
			std::vector<std::vector<data::Point3D>>& objPts) override;

		/**getObjPlyArea
		*@brief get object space areas of a given polygon
		*@param imgColRow: image coordinates (col, row) of vertex-pixels
		*@param sumArea: computed area
		*@return bool
		*/
		bool getObjPlyArea(const std::vector<data::Point2di>& imgColRow, double& sumArea) override;

	private:
		inline void init();

		bool loadObj(const bool buildTree = false);

		bool loadObj(const std::vector<std::string>& objPaths, const std::vector<std::string>& metaPaths);

		bool getObjAreasGrid(const std::vector<data::Point2di>& imgColRow,
			std::vector<double>& areas,
			std::vector<bool>& failedPixs,
			double& sumAreas);

		RenderInfo simulatePhoto(const int imgW, const int imgH, const double f, const double pixPitch0,
			const double omg, const double phi, const double kap,
			const double x0, const double y0, const double z0,
			const double domg, const double dphi, const double dkap,
			const double dx, const double dy, const double dz,
			const std::string& outPath,
			const int menu);

		std::vector<std::vector<int>> determineObj(const std::vector<std::vector<data::Point2di>>& imgColRow,
			const std::vector<std::vector<double>>& pos,
			const std::vector<std::vector<double>>& opk,
			const std::shared_ptr<sensor::Camera>& cam,
			const double maxDist,
			std::vector<std::vector<data::Point3D>>& objPts);

		void getRendImgCoordinates(const std::vector<data::Point2di>& imgColRow,
			const RenderInfo& rendInfo,
			const std::shared_ptr<sensor::Camera>& cam,
			std::vector<int>& imgCol,
			std::vector<int>& imgRow);

		void buildTreeForSearch();

		void updatePolygon3D(Polygon& plgn, 
			const std::vector<double>& pos, 
			const std::vector<bool>& currentUsage, 
			const std::vector<double>& Xa, 
			const std::vector<double>& Ya, 
			const std::vector<double>& Za, 
			const size_t objIdx)
		{
			/// update corresponding surface point, comparing distance to object
			bool update = false;
			size_t numPts = plgn.node2D.size();
			double firstDist = -999.999;

			for (int pt = 0; pt < numPts; ++pt)
			{
				if (currentUsage[pt])
				{
					double dx = pos[0] - Xa[pt];
					double dy = pos[1] - Ya[pt];
					double dz = pos[2] - Za[pt];
					firstDist = sqrt(dx * dx + dy * dy + dz * dz);
					break;
				}
			}

			if (plgn.distFirstNode < 0.0 && firstDist > 0.0)
			{
				plgn.distFirstNode = firstDist;
				update = true;
			}
			else
			{
				if (plgn.distFirstNode > firstDist)
				{
					plgn.distFirstNode = firstDist;
					update = true;
				}
			}

			if (update)
			{
				plgn.objIdx = static_cast<int>(objIdx);
				plgn.usage = currentUsage;
				plgn.node3D.clear();
				plgn.node3D.resize(numPts);

				for (int pt = 0; pt < numPts; ++pt)
				{
					plgn.node3D[pt](0) = Xa[pt];
					plgn.node3D[pt](1) = Ya[pt];
					plgn.node3D[pt](2) = Za[pt];
				}
			}
		}

	private:
		globj::ViewSim viewRender;
		bool searchTree;
		mesh::SSMFlann ssmFlann;
		std::vector<std::string> objPaths;
		std::vector<std::string> metaPaths;
		bool objIsLoaded;
		bool simulated;
		data::Point3D curPos;
		data::Point3D curOpk;
		std::shared_ptr<sensor::Camera> curCam;
		RenderInfo curRendInfo;
		double shiftCol;
		double shiftRow;
	};

	CompObjSpace::CompObjSpace()
	{
		init();
	}

	void CompObjSpace::init()
	{
		this->searchTree = false;
		this->objIsLoaded = false;
		this->simulated = false;
		this->shiftCol = this->shiftRow = 0.0;
		this->curCam = nullptr;
	}

	void CompObjSpace::setObjData(const std::vector<std::string>& objPaths, const std::vector <std::string>& metaPaths)
	{
		init();

		this->objPaths = objPaths;
		this->metaPaths = metaPaths;
	}

	bool CompObjSpace::setImgDataForObjCoord(const std::vector<double>& pos,
		const std::vector<double>& opk,
		const std::shared_ptr<sensor::Camera>& cam)
	{
		try
		{
			if (!objIsLoaded)
				loadObj();

			auto sensorSize = cam->getSensorSize();
			auto bore = cam->getBoresight();
			auto lever = cam->getLeverArm();

			this->curRendInfo = simulatePhoto(sensorSize(0), sensorSize(1), cam->getFL(), cam->getPixPitch(),
				opk[0], opk[1], opk[2],
				pos[0], pos[1], pos[2],
				bore(0), bore(1), bore(2),
				lever(0), lever(1), lever(2),
				"temp.bmp",
				PREPAREOBJPT);

			this->shiftCol = (double(baseW) - double(this->curRendInfo.rendW)) * 0.5;
			this->shiftRow = (double(baseH) - double(this->curRendInfo.rendH)) * 0.5;

			curPos(0) = pos[0];
			curPos(1) = pos[1];
			curPos(2) = pos[2];

			curOpk(0) = opk[0];
			curOpk(1) = opk[1];
			curOpk(2) = opk[2];

			curCam = cam;

			this->simulated = true;

			return true;
		}
		catch(...)
		{
			return false;
		}
	}

	bool CompObjSpace::getObjPts(const std::shared_ptr<sensor::Camera>& cam, const unsigned int col, unsigned int row, double& x, double& y, double& z)
	{
		if (!this->objIsLoaded || !this->simulated)
			return false;

		data::Point2D distortedImgCoord, refinedPhotoCoord, refinedImgCoord;

		distortedImgCoord(0) = col;
		distortedImgCoord(1) = row;

		refinedPhotoCoord = cam->getRefinedPhotoCoordFromImgCoord(distortedImgCoord);
		refinedImgCoord = cam->getPhoto2ImgCoord(refinedPhotoCoord);

		/// compute col and row for the rendered image scale
		int imgCol = int(refinedImgCoord(0) * this->curRendInfo.sizeRatio + this->shiftCol + 0.5);
		int imgRow = int(refinedImgCoord(1) * this->curRendInfo.sizeRatio + this->shiftRow + 0.5);

		return viewRender.computeObjCoord(imgCol, imgRow, x, y, z);
	}

	bool CompObjSpace::getObjPts(DamageInfoGroup& damageGroup, const double maxDist)
	{
		if (!damageGroup.cam)
		{
			std::clog << "getObjPts: nullptr camera" << std::endl;
			return false;
		}

		if (this->objPaths.size() != this->metaPaths.size())
		{
			std::clog << "getObjPts: this->objPaths.size() != this->metaPaths.size()" << std::endl;
			return false;
		}

		const size_t nObjs = this->objPaths.size();
		const size_t nImgs = damageGroup.damages.size();

		/// Initialize firstNodeDist and corresponding obj index
		for (size_t i = 0; i < nImgs; ++i)
		{
			auto& imgDamage = damageGroup.damages[i];
			size_t nPl = imgDamage.plgns.size(); /// number of polygons

			for (size_t pl = 0; pl < nPl; ++pl)
			{
				imgDamage.plgns[pl].distFirstNode = -999.999;
				imgDamage.plgns[pl].objIdx = -1;
				size_t nNodes = imgDamage.plgns[pl].node2D.size();
				imgDamage.plgns[pl].node3D.clear();
				imgDamage.plgns[pl].node3D.resize(nNodes);
				imgDamage.plgns[pl].usage.clear();
				imgDamage.plgns[pl].usage.resize(nNodes);
				for (auto& pt3D : imgDamage.plgns[pl].node3D)
				{
					pt3D(0) = FALSECOORD;
					pt3D(1) = FALSECOORD;
					pt3D(2) = FALSECOORD;
				}
			}
		}

		std::vector<std::string> objPathi(1);
		std::vector<std::string> metaPathi(1);

		for (size_t objIdx = 0; objIdx < nObjs; ++objIdx)
		{
			/// load each object file
			objPathi[0] = this->objPaths[objIdx];
			metaPathi[0] = this->metaPaths[objIdx];

			/// load each obj file
			if (!loadObj(objPathi, metaPathi))
			{
#ifdef _DEBUG
				std::clog << "\n" << "Failed in opening a object file, # " << objIdx << " of " << objPaths.size() << std::endl;
#endif
				continue;
			}

			const auto sensorSize = damageGroup.cam->getSensorSize();
			const auto bore = damageGroup.cam->getBoresight();
			const auto lever = damageGroup.cam->getLeverArm();
			const auto fl = damageGroup.cam->getFL();
			const auto pxPitch = damageGroup.cam->getPixPitch();

			double mdlHalfSize = sqrt(pow(viewRender.sizeX(), 2) + pow(viewRender.sizeY(), 2) + pow(viewRender.sizeZ(), 2)) * 0.5;

			for (size_t i = 0; i < nImgs; ++i)
			{
				auto& imgDamage = damageGroup.damages[i];
				/// check a distance to a model
				double dx = viewRender.centerX() - imgDamage.pos[0];
				double dy = viewRender.centerY() - imgDamage.pos[1];
				double dz = viewRender.centerZ() - imgDamage.pos[2];
				double dist = sqrt(dx * dx + dy * dy + dz * dz);
				if ((dist - mdlHalfSize) > maxDist)
					continue;

#ifdef _DEBUG
				std::clog << "image index: " << i << " of " << damageGroup.damages.size() << std::endl;
#endif

				/// render photo
				const auto rendInfo = simulatePhoto(sensorSize(0), sensorSize(1), fl, pxPitch,
					imgDamage.opk[0], imgDamage.opk[1], imgDamage.opk[2],
					imgDamage.pos[0], imgDamage.pos[1], imgDamage.pos[2],
					bore(0), bore(1), bore(2),
					lever(0), lever(1), lever(2),
					"temp.bmp",
					PREPAREOBJPT);

				/// recompute image coordinate for the render image
				size_t nPl = imgDamage.plgns.size(); /// number of polygons
				for (size_t pl = 0; pl < nPl; ++pl)
				{
					size_t numPts = imgDamage.plgns[pl].node2D.size(); /// number of nodes in a polygon
					std::vector<int> imgCol, imgRow;
					getRendImgCoordinates(imgDamage.plgns[pl].node2D, rendInfo, damageGroup.cam, imgCol, imgRow);

					/// compute object point coordinates
					std::vector<double> Xa, Ya, Za;
					auto currentUsage = viewRender.getObjCoords(imgCol, imgRow, Xa, Ya, Za);

					bool availability = false;
					for (const auto& u : currentUsage)
					{
						if (u)
						{
							availability = true;
							break;
						}
					}

					if (!availability)
						continue;

					/// update corresponding surface point, comparing distance to object
					updatePolygon3D(imgDamage.plgns[pl],
						imgDamage.pos,
						currentUsage,
						Xa, Ya, Za,
						objIdx);
				}
			}
		}

		this->objIsLoaded = false;
		this->simulated = false;

		return true;
	}

	bool CompObjSpace::loadObj(const bool buildTree)
	{
		if (!viewRender.load(this->objPaths, this->metaPaths))
			return false;

		this->searchTree = buildTree;
		if (this->searchTree)
			buildTreeForSearch();

		this->objIsLoaded = true;

		return true;
	}

	bool CompObjSpace::loadObj(const std::vector<std::string>& objPaths, const std::vector<std::string>& metaPaths)
	{
		if (!viewRender.load(objPaths, metaPaths))
			return false;
		else
			return true;
	}

	void CompObjSpace::getRendImgCoordinates(const std::vector<data::Point2di>& imgColRow,
		const RenderInfo& rendInfo,
		const std::shared_ptr<sensor::Camera>& cam,
		std::vector<int>& imgCol,
		std::vector<int>& imgRow)
	{
		size_t numPts = imgColRow.size();
		imgCol.resize(numPts);
		imgRow.resize(numPts);

		data::Point2D distortedImgCoord, refinedPhotoCoord, refinedImgCoord;

		double shiftCol = (double(baseW) - double(rendInfo.rendW)) * 0.5;
		double shiftRow = (double(baseH) - double(rendInfo.rendH)) * 0.5;

		for (size_t i = 0; i < numPts; ++i)
		{
			distortedImgCoord(0) = imgColRow[i](0);
			distortedImgCoord(1) = imgColRow[i](1);

			refinedPhotoCoord = cam->getRefinedPhotoCoordFromImgCoord(distortedImgCoord);
			refinedImgCoord = cam->getPhoto2ImgCoord(refinedPhotoCoord);

			imgCol[i] = int(refinedImgCoord(0) * rendInfo.sizeRatio + shiftCol + 0.5);
			imgRow[i] = int(refinedImgCoord(1) * rendInfo.sizeRatio + shiftRow + 0.5);
		}
	};

	void CompObjSpace::buildTreeForSearch()
	{
		ssmFlann.setPseudoType(mesh::PSEUDOTYPE::MIDPOINT);
		ssmFlann.setDistThreshold(treeDistThr);

		unsigned int nVtx = static_cast<unsigned int>(viewRender.getNumVertices());
		ssmFlann.setNumberOfPoints(nVtx);
		for (unsigned int i = 0; i < nVtx; ++i)
		{
			const auto& vtx = viewRender.getVtx(i);
			ssmFlann.setVertex(i, vtx[0], vtx[1], vtx[2]);
		}

		unsigned int nFaces = static_cast<unsigned int>(viewRender.getNumFaces());
		ssmFlann.setNumberOfFaces(nFaces);
		std::vector<unsigned int> vId(3);
		for (unsigned int i = 0; i < nFaces; ++i)
		{
			const auto& fVtxIds = viewRender.getFVtxId(i);
			const auto& fnormal = viewRender.getFNormal(i);
			vId[0] = fVtxIds[0];
			vId[1] = fVtxIds[1];
			vId[2] = fVtxIds[2];
			ssmFlann.setFace(i, vId);
			ssmFlann.setFaceNormal(i, fnormal[0], fnormal[1], fnormal[2]);
		}

		///build tree
		ssmFlann.buildTree();

		searchTree = true;
	}

	bool CompObjSpace::getObjAreasGrid(const std::vector<data::Point2di>& imgColRow,
		std::vector<double>& areas,
		std::vector<bool>& failedPixs,
		double& sumAreas)
	{
		if (!this->objIsLoaded || !this->simulated)
			return false;

		if (!searchTree)
			buildTreeForSearch();

		/// number nodes
		size_t numPts = imgColRow.size();

		/// object coordinates
		std::vector<data::Point3D> objPts(numPts);
		/// object coordinates
		std::vector<data::Point3D> obj2pc(numPts);
		/// object distances
		std::vector<double> objDists(numPts);
		/// photo distances
		std::vector<double> phoDists(numPts);

		failedPixs.resize(numPts);
		
		auto pitch = this->curCam->getPixPitch();///mm
		auto fl = this->curCam->getFL();///mm

		double x, y, z;
		int falseIdx = -1;
		data::Point2D distortedImgCoord;

		for (size_t i = 0; i < numPts; ++i)
		{
			/// compute refined photo coordinates
			const auto& imgP = imgColRow[i];
			distortedImgCoord(0) = imgColRow[i](0);
			distortedImgCoord(1) = imgColRow[i](1);
			auto xy = this->curCam->getRefinedPhotoCoordFromImgCoord(distortedImgCoord); /// mm
			/// compute a photo distance
			phoDists[i] = sqrt(xy(0) * xy(0) + xy(1) * xy(1) + fl * fl) / 1000.0;/// meter			
			/// compute object coordinates
			failedPixs[i] = getObjPts(this->curCam, imgColRow[i](0), imgColRow[i](1), x, y, z);
			if (failedPixs[i])
			{
				/// obj coordinates
				objPts[i](0) = x;
				objPts[i](1) = y;
				objPts[i](2) = z;

				/// distance to the surface
				obj2pc[i] = objPts[i] - this->curPos;
				double dist = sqrt(obj2pc[i].getSumOfSquares()); /// meter
				objDists[i] = dist;

				/// set the current distance to the failed points
				for (size_t j = falseIdx; j < i; ++j)
				{
					if (objDists[j] < 0.0)
						objDists[j] = objDists[i];
				}

				falseIdx = -1;
			}
			else
			{
				if(falseIdx < 0)
					falseIdx = int(i);

				objDists[i] = -1.0;
			}
		}

		/// set the last available distance to the remaining false pixels
		if (falseIdx >= 1)
		{
			const int lastIdx = int(falseIdx) - 1;

			for (size_t j = falseIdx; j < numPts; ++j)
			{
				objDists[j] = objDists[lastIdx];
			}
		}
		else
		{
			/// Computating obj coordinates is failed in all pixels
			return false;
		}

		/// compute areas of pixels within a polygon
		areas.resize(numPts);
		sumAreas = 0.0;
		auto area0 = pitch * pitch / 1000000.0;///m^2

		for (int i = 0; i < numPts; ++i)
		{
			if (phoDists[i] > std::numeric_limits<double>::epsilon())
			{
				/// scale
				double s = objDists[i] / phoDists[i];
				areas[i] = area0 * (s * s);

				/// search face for computing surface aspect
				std::vector<double> dists;
				std::vector<size_t> indices;

				if (ssmFlann.search(objPts[i](0), objPts[i](1), objPts[i](2), dists, indices, 1, 0.0))
				{
					auto v0 = ssmFlann.getVertex(indices[0]);
					auto faceIds = v0.getFaceId();
					auto fn = ssmFlann.getFaceNormal(faceIds[0]);
					auto ang = computeAngleIn3Vectors(obj2pc[i](0), obj2pc[i](1), obj2pc[i](2), fn[0], fn[1], fn[2], true);

					/// applying surface aspect
					if (cos(ang) > std::numeric_limits<double>::epsilon())
					{
						areas[i] /= cos(ang);
					}
				}

				/// accumulate areas
				sumAreas += areas[i];
			}
			else
			{
				areas[i] = 0.0;
			}
		}

		return true;
	}

	bool CompObjSpace::getObjPlyArea(const std::vector<data::Point2di>& imgColRow, double& sumArea)
	{
		if (imgColRow.size() < 1)
			return  0.0;

		/// collect pixels
		unsigned int minCol = imgColRow[0](0);
		unsigned int maxCol = imgColRow[0](0);
		unsigned int minRow = imgColRow[0](1);
		unsigned int maxRow = imgColRow[0](1);

		std::vector<double> imgCol(imgColRow.size());
		std::vector<double> imgRow(imgColRow.size());

		for (int i = 1; i < imgColRow.size(); ++i)
		{
			if (minCol > imgColRow[i](0)) minCol = imgColRow[i](0);
			if (maxCol < imgColRow[i](0)) maxCol = imgColRow[i](0);
			if (minRow > imgColRow[i](1)) minRow = imgColRow[i](1);
			if (maxRow < imgColRow[i](1)) maxRow = imgColRow[i](1);

			imgCol[i] = double(imgColRow[i](0));
			imgRow[i] = double(imgColRow[i](1));
		}

		std::vector<data::Point2di> imgColRowArea;
		imgColRowArea.reserve(static_cast<size_t>((static_cast<int>(maxCol) - static_cast<int>(minCol) + 1) * (static_cast<int>(maxRow) - static_cast<int>(minRow) + 1)));

		for (unsigned int r = minRow; r <= maxRow; ++r)
		{
			for (unsigned int c = minCol; c <= maxCol; ++c)
			{
				if (1 == util::Inside2DPolygon(imgCol, imgRow, double(c), double(r)))
				{
					data::Point2di pix;
					pix(0) = c;
					pix(1) = r;
					imgColRowArea.push_back(pix);
				}
			}
		}

		std::vector<double> areas;
		std::vector<bool> failedPixs;

		return getObjAreasGrid(imgColRowArea, areas, failedPixs, sumArea);
	}

	std::vector<std::vector<int>> CompObjSpace::determineObj(const std::vector<std::vector<data::Point2di>>& imgColRow,
		const std::vector<std::vector<double>>& pos,
		const std::vector<std::vector<double>>& opk,
		const std::shared_ptr<sensor::Camera>& cam,
		const double maxDist,
		std::vector<std::vector<data::Point3D>>& objPts)
	{
		/// return values: object indices of each image point in each image
		std::vector<vector<int>> retVal;

		/// check data sizes
		if (imgColRow.size() < 1 || imgColRow.size() != pos.size() || pos.size() != opk.size())
		{
			std::cerr << "Note: wrong sizes(imgColRow, pos, opk) in CompObjSpace::determineObj()"  << std::endl;
			return retVal;
		}

		/// distances record
		std::vector<vector<double>> dists(imgColRow.size());
		/// object coordinates of each image point in each image
		objPts.resize(imgColRow.size());
		/// initialize objPts
		data::Point3D falsePt;
		falsePt(0) = FALSECOORD;
		falsePt(1) = FALSECOORD;
		falsePt(2) = FALSECOORD;
		for (int i = 0; i < objPts.size(); ++i)
		{
			objPts[i].resize(imgColRow[0].size());

			for (int j = 0; j < imgColRow[0].size(); ++j)
				objPts[i][j] = falsePt;
		}
		/// object indices for each of images
		retVal.resize(imgColRow.size());
		/// Initialize obj file indices and distances
		for (size_t i = 0; i < retVal.size(); ++i)
		{
			retVal[i].resize(imgColRow[i].size());
			dists[i].resize(imgColRow[i].size());

			for (size_t cellIdx = 0; cellIdx < dists[i].size(); ++cellIdx)
			{
				retVal[i][cellIdx] = -1; /// -1 means unavailable
				dists[i][cellIdx] = std::numeric_limits<double>::max();
			}
		}

		const auto sensorSize = cam->getSensorSize();
		const auto bore = cam->getBoresight();
		const auto lever = cam->getLeverArm();

		std::vector<std::string> objPathi(1);
		std::vector<std::string> metaPathi(1);

		for (size_t objIdx = 0; objIdx < this->objPaths.size(); ++objIdx)
		{
			/// load each object file
			objPathi[0] = this->objPaths[objIdx];
			metaPathi[0] = this->metaPaths[objIdx];

			/// load each obj file
			if (!loadObj(objPathi, metaPathi))
			{
#ifdef _DEBUG
				std::clog << "\n" << "Failed in opening a object file, # " << objIdx << " of " << objPaths.size() << std::endl;
#endif
				continue;
			}

			double mdlHalfSize = sqrt(pow(viewRender.sizeX(), 2) + pow(viewRender.sizeY(), 2) + pow(viewRender.sizeZ(), 2)) * 0.5;

			/// compute object coordinates for each of images
			for (size_t imgIdx = 0; imgIdx < imgColRow.size(); ++imgIdx)
			{
				/// check a distance to a model
				double dx = viewRender.centerX() - pos[imgIdx][0];
				double dy = viewRender.centerY() - pos[imgIdx][1];
				double dz = viewRender.centerZ() - pos[imgIdx][2];
				double dist = sqrt(dx * dx + dy * dy + dz * dz);
				if((dist - mdlHalfSize) > maxDist)
					continue;

#ifdef _DEBUG
				std::clog << "image index: " << imgIdx << " of " << imgColRow.size() << std::endl;
#endif
				/// render photo
				const auto rendInfo = simulatePhoto(sensorSize(0), sensorSize(1), cam->getFL(), cam->getPixPitch(),
					opk[imgIdx][0], opk[imgIdx][1], opk[imgIdx][2],
					pos[imgIdx][0], pos[imgIdx][1], pos[imgIdx][2],
					bore(0), bore(1), bore(2),
					lever(0), lever(1), lever(2),
					"temp.bmp",
					PREPAREOBJPT);

				/// recompute image coordinate for the render image
				size_t numPts = imgColRow[imgIdx].size();
				std::vector<int> imgCol, imgRow;
				getRendImgCoordinates(imgColRow[imgIdx], rendInfo, cam, imgCol, imgRow);

				/// compute object point coordinates
				std::vector<double> Xa, Ya, Za;
				std::vector<bool> availability = viewRender.getObjCoords(imgCol, imgRow, Xa, Ya, Za);

				std::vector<data::Point3D> pt3d(numPts);
				for (int i = 0; i < numPts; ++i)
				{
					pt3d[i](0) = Xa[i];
					pt3d[i](1) = Ya[i];
					pt3d[i](2) = Za[i];
				}

				/// update closest surface point, comparing distance to object
				for (size_t cellIdx = 0; cellIdx < availability.size(); ++cellIdx)
				{
					if (availability[cellIdx])
					{
						auto dx = pos[imgIdx][0] - pt3d[cellIdx](0);
						auto dy = pos[imgIdx][1] - pt3d[cellIdx](1);
						auto dz = pos[imgIdx][2] - pt3d[cellIdx](2);
						auto distk = sqrt(dx * dx + dy * dy + dz * dz);
						
						/// check a distance to a point
						if (distk > maxDist)
							continue;

						/// record point coordinates
						objPts[imgIdx][cellIdx] = pt3d[cellIdx];

						if (dists[imgIdx][cellIdx] > distk)
						{
							/// update object index and distance to the object
							retVal[imgIdx][cellIdx] = int(objIdx);
							dists[imgIdx][cellIdx] = distk;
						}
					}
				}
			}
		}

		this->objIsLoaded = false;

		return retVal;
	}

	std::vector<std::vector<int>> CompObjSpace::determineObj(const unsigned int nCols,
		const unsigned int nRows,
		const std::vector<std::vector<double>>& pos,
		const std::vector<std::vector<double>>& opk,
		const std::shared_ptr<sensor::Camera>& cam,
		const double maxDist,
		std::vector<std::vector<data::Point3D>>& objPts)
	{
		/// return values: object indices of each image point in each image
		std::vector<std::vector<int>> objParts;
		if (nCols < 1 || nRows < 1)
		{
#ifdef _DEBUG
			std::cerr << "Wrong grid size in CompObjSpace::determineObj()";
#endif
			return objParts;
		}

		if (pos.size() != opk.size())
		{
#ifdef _DEBUG
			std::cerr << "Wrong number of pos and opk data in CompObjSpace::determineObj()";
#endif
			return objParts;
		}

		auto imgSize = cam->getSensorSize();
		double wGap = double(imgSize(0)) / double(nCols);
		double hGap = double(imgSize(1)) / double(nRows);

		std::vector<std::vector<data::Point2di>> imgColRow(pos.size());
		std::vector<data::Point2di> grid(nCols * nRows);

		double x = 0, y = 0;
		for (size_t r = 0; r < nRows; ++r)
		{
			y = r * hGap + hGap * 0.5;

			for (size_t c = 0; c < nCols; ++c)
			{
				x = c * wGap + wGap * 0.5;

				grid[c + nCols * r](0) = unsigned int(x + 0.5);
				grid[c + nCols * r](1) = unsigned int(y + 0.5);
			}
		}

		for (auto& img : imgColRow)
		{
			img = grid;
		}

		/// Search captured parts
		auto cellIdxMap = determineObj(imgColRow, pos, opk, cam, maxDist, objPts);
		
		/// Collect corresponding part indices
		objParts.resize(cellIdxMap.size());

		for (unsigned int i=0; i< cellIdxMap.size(); ++i)
		{
			for (unsigned int j = 0; j < cellIdxMap[i].size(); ++j)
			{
				if (cellIdxMap[i][j] < 0) /// -1
					continue;

				/// Search an ID in the existing part list
				bool found = false;
				for (unsigned int k = 0; k < objParts[i].size(); ++k)
				{
					if (cellIdxMap[i][j] == objParts[i][k])
					{
						found = true;
						break;
					}
				}

				/// Add a new part index
				if(!found)
					objParts[i].push_back(cellIdxMap[i][j]);
			}
		}

#ifdef _DEBUG
		if (savePartsForEachImgInDebug)
		{
			const auto sensorSize = cam->getSensorSize();
			const auto bore = cam->getBoresight();
			const auto lever = cam->getLeverArm();

			for (size_t imgIdx = 0; imgIdx < objParts.size(); ++imgIdx)
			{
				std::string outname;
				outname = std::to_string(imgIdx) + std::string("_IMG");

				std::vector<std::string> objs;
				std::vector<std::string> metas;

				for (const auto& objIdx : objParts[imgIdx])
				{
					objs.push_back(objPaths[objIdx]);
					metas.push_back(metaPaths[objIdx]);

					outname += std::string("_") + std::to_string(objIdx);
				}

				if (!loadObj(objs, metas))
					continue;

				auto rendInfo = simulatePhoto(sensorSize(0), sensorSize(1), cam->getFL(), cam->getPixPitch(),
					opk[imgIdx][0], opk[imgIdx][1], opk[imgIdx][2],
					pos[imgIdx][0], pos[imgIdx][1], pos[imgIdx][2],
					bore(0), bore(1), bore(2),
					lever(0), lever(1), lever(2),
					"temp.bmp",
					PREPAREOBJPT);

				outname += std::string(".bmp");

				globj::saveScreen(outname);
			}
		}
#endif

		return objParts;
	}

	RenderInfo CompObjSpace::simulatePhoto(const int imgW, const int imgH, const double f, const double pixPitch0,
		const double omg, const double phi, const double kap,
		const double x0, const double y0, const double z0,
		const double domg, const double dphi, const double dkap,
		const double dx, const double dy, const double dz,
		const std::string& outPath,
		const int menu)
	{
		/// geo-referencing with alignment (boresight(Rb2m), leverarm(T)) 
		/// Xa = X0 + Rb2m * T + S * Rb2m * Rc2b * (x, y, -f) 
		/// S * Rb2m * Rc2b * (x, y, -f) = Xa -  X0 - Rb2m * T 
		/// S *  (x, y, -f) = (Rb2c * Rm2b)(Xa -  X0) - (Rb2c * Rm2b) * Rb2m * T 
		/// S *  (x, y, -f) = (q, r, s) = (Rb2c * Rm2b)(Xa -  X0) - Rb2c * T 
		/// x = -f (r/q) 
		/// y = -f (s/q)

		/// EOP derivation 
		/// S* (x, y, -f) = (Rb2c * Rm2b)(Xa - X0) - Rb2c * T 
		/// S * (x, y, -f) = (Rb2c * Rm2b)((Xa - X0) - Rb2m * T) 
		/// S * (x, y, -f) = (Rb2c * Rm2b)(Xa - (X0 - Rb2m * T)) 
		/// Orientation Matrix: Rb2c * Rm2b 
		/// Perspective center: (X0 - Rb2m * T)

		/// render image scale and size
		RenderInfo& rendInfo = this->curRendInfo;
		
		try
		{
			double sW = double(baseW) / double(imgW);
			double sH = double(baseH) / double(imgH);
			
			if (sW < sH)
			{
				rendInfo.sizeRatio = sW;
				rendInfo.rendW = baseW;
				rendInfo.rendH = static_cast<int>(double(imgH) * rendInfo.sizeRatio);				
			}
			else
			{
				rendInfo.sizeRatio = sH;
				rendInfo.rendH = baseH;
				rendInfo.rendW = static_cast<int>(double(imgW) * rendInfo.sizeRatio);				
			}

			rendInfo.rendPixPitch = pixPitch0 / rendInfo.sizeRatio;

			float fovHDeg = static_cast<float>(atan(baseH * 0.5 * rendInfo.rendPixPitch / f) * 114.591559);/// deg

			double nx, ny, nz;
			globj::getNormalized(x0, y0, z0, nx, ny, nz);

			/// body rotation
			auto bodyR = math::rotation::getRMat(omg, phi, kap);
			/// lever-arm
			math::Matrix<double> T(3, 1);
			T(0, 0) = dx;
			T(1, 0) = dy;
			T(2, 0) = dz;
			/// perspective center
			math::Matrix<double> pc(3, 1);
			pc(0, 0) = nx;
			pc(1, 0) = ny;
			pc(2, 0) = nz;
			pc = pc + bodyR % T;
			/// boresight
			auto boreR = math::rotation::getRMat(domg, dphi, dkap);
			/// camera rotation w.r.t a mapping frame
			auto R = bodyR % boreR;

			math::Matrix<double> camYDir(3, 1);
			camYDir(0, 0) = 0.0;
			camYDir(1, 0) = 1.0;
			camYDir(2, 0) = 0.0;

			camYDir = R % camYDir;

			/// camera direction
			math::Matrix<double> camDir(3, 1);
			camDir(0, 0) = 0.0;
			camDir(1, 0) = 0.0;
			camDir(2, 0) = -1.0;

			camDir = R % camDir * 5.0;

			/// looking at
			auto tar = camDir + pc;

			/// set view direction
			globj::setupEye(pc(0, 0), pc(1, 0), pc(2, 0),
				tar(0, 0), tar(1, 0), tar(2, 0),
				camYDir(0, 0), camYDir(1, 0), camYDir(2, 0));

			/// set fov along the image height
			viewRender.setFov(fovHDeg);

			int winW = baseW;
			int winH = baseH;
			/*
			/// legacy codes for calculating proper redering image size
			double apsc_w_ratio = double(imgW) / double(imgH);
			winH = int(float(imgH) * sizeRatio + 0.5);

			if (winH % 2 != 0)
				winH -= 1; /// even number

			winW = int(apsc_w_ratio * double(winH) + 0.5);

			if (winW % 2 != 0)
				winW -= 1; /// even number
			*/

			bool offScreen;

			switch (menu)
			{
			case 0: /// show obj
				/// initialize glut
				offScreen = true;
				viewRender.init(winW, winH, "glrender", offScreen);
				viewRender.showObj();
				break;
			case 1: /// save render
				/// initialize glut
				offScreen = true;
				viewRender.init(winW, winH, "glrender", offScreen);
				viewRender.saveRender(std::string(outPath));
				break;
			case 2: /// prepare for computing object space coordinates
				/// initialize glut
				offScreen = true;
				viewRender.init(winW, winH, "glrender", offScreen);
				viewRender.prepareComputingObjPts();
				break;
			default:
				break;
			}
		}
		catch (...)
		{
			throw std::runtime_error("Error in simulatePhoto");
		}

		return rendInfo;
	};

	std::shared_ptr<CompObjSpaceApi> createCompObjSpace()
	{
		std::shared_ptr<CompObjSpaceApi> ptr(new CompObjSpace);
		if (!ptr)
		{
			std::clog << "Fail in creating CompObjSpaceApi";
		}
		else
		{
			std::clog << "Succeed in creating CompObjSpaceApi";
		}

		return ptr;
	}
}