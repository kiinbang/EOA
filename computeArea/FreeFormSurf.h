#pragma once

#ifdef COMAREA_EXPORT
#define COMAREA_API __declspec(dllexport)
#else
#define COMAREA_API __declspec(dllimport)
#endif

#include <string>
#include <vector>

#include <ssm/include/SSMCamera.h>
#include <ssm/include/SSMDataStructures.h>

namespace obj
{
	const double treeDistThr = 0.1;

	struct Polygon
	{
		std::vector<data::Point2di> node2D;
		std::vector<data::Point3D> node3D;
		std::vector<bool> usage;
		double distFirstNode;
		int objIdx;
	};

	struct DamageInfo
	{
		std::vector<double> pos;
		std::vector<double> opk;
		std::vector<Polygon> plgns;
	};

	struct DamageInfoGroup
	{
		std::shared_ptr<sensor::Camera> cam;
		std::vector<DamageInfo> damages;
	};

	class COMAREA_API CompObjSpaceApi
	{
	public:
		/**setObjData
		*@brief put obj and meta data paths
		*@param objPaths: obj files' paths (.obj)
		*@param metaPaths: meta files' paths (.gxxml)
		*/
		virtual void setObjData(const std::vector<std::string>& objPaths, const std::vector <std::string>& metaPaths) = 0;

		/**setImgDataForObjCoord
		*@brief put camera and image info (position and pose information)
		*@param pos: camera position (x, y, z)
		*@param opk: camera orientation(o, p, k)
		*@param cam: camera smart pointer
		*@return bool
		*/
		virtual bool setImgDataForObjCoord(const std::vector<double>& pos,
			const std::vector<double>& opk,
			const std::shared_ptr<sensor::Camera>& cam) = 0;

		/**getObjPts
		*@brief get object space coordinates of polygons (damages)
		*@param damageInfo: damage information
		*@param maxDist: maximum distance to an object surface 
		*@return bool
		*/
		virtual bool getObjPts(DamageInfoGroup& damageGroup, const double maxDist) = 0;

		/**getObjPts
		*@brief get object space coordinates of a given pixel, using a existing simulated image
		*@param cam: camera
		*@param col: col coordinate (integer)
		*@param row: row coordinate (integer)
		*@param x: computed x coord
		*@param y: computed y coord
		*@param z: computed z coord
		*@return bool
		*/
		virtual bool getObjPts(const std::shared_ptr<sensor::Camera>& cam, const unsigned int col, unsigned int row, double& x, double& y, double& z) = 0;

		/**getObjPlyArea
		*@brief get object space areas of a given polygon
		*@param imgColRow: image coordinates (col, row) of vertex-pixels
		*@param sumArea: computed area
		*@return bool
		*/
		virtual bool getObjPlyArea(const std::vector<data::Point2di>& imgColRow, double& sumArea) = 0;

		/**determineObj
		*@brief determine an object of a given image point
		*@param nCols: number of columns
		*@param nRows: number of rows
		*@param pos: camera positions (x, y, z) for n images
		*@param opk: camera orientations(o, p, k) for n images
		*@param cam: camera smart pointer
		*@param maxDist: max-distance between an image and a model center
		*@param objPts: object coordinates of the given pixels for n images
		*@return no of objects: -1 means failure
		*/
		virtual std::vector<std::vector<int>> determineObj(const unsigned int nCols, 
			const unsigned int nRows,
			const std::vector<std::vector<double>>& pos,
			const std::vector<std::vector<double>>& opk,
			const std::shared_ptr<sensor::Camera>& cam,
			const double maxDist,
			std::vector<std::vector<data::Point3D>>& objPts) = 0;
	};

	std::shared_ptr<CompObjSpaceApi> COMAREA_API createCompObjSpace();

	double COMAREA_API computeAngleIn3Vectors(const double ax, const double ay, const double az, const double bx, const double by, const double bz, bool acuteAngle);
}