#include "../ViewSimulation/ObjRender.h"

#include "fileInOut.hpp"

int main(int argc, char** argv)
{
	const std::vector<std::string> paths;
	const std::vector<std::string> metas;
	
	std::string inputPath;
	std::vector<int> id;
	/// img size
	int imgW, imgH;
	/// pc
	double E, N, U;
	/// orientation angles
	double omg0, phi0, kap0;
	/// lever-arm
	double dx, dy, dz;
	/// boresight
	double domg, dphi, dkap;
	/// img col and row coordinates
	std::vector<int> col, row;

	if (!readInput(inputPath, imgW, imgH, id, col, row, E, N, U, omg0, phi0, kap0, dx, dy, dz, domg, dphi, dkap))
		return 1;

	objrend::loadObj(paths, metas);

	const int subSample = 4; /// <-- max size should be adjusted or use Frame buffer
	imgW = 5456;
	imgH = 3632;
	int winH = imgH / subSample;
	/// APSC (25.1��16.7 mm, 1.503:1.0) with 18mm, fov = 46.32;
	float fovH = 46.32f;

	

	double pc[3];
	pc[0] = 348727.134;
	pc[1] = 4049759.916;
	pc[2] = 53.851;

	//double nx = pc[0], ny = pc[1], nz = pc[2];
	//objrend::normalizePt(nx, ny, nz);

	double o, p, k;
	o = 0.302124 * deg2rad;
	p = 0.05518472 * deg2rad;
	k = 108.298326 * deg2rad;

	/// boresight
	double bore_o(90.0 * deg2rad);
	double bore_p(0.0 * deg2rad);
	double bore_k(0.0 * deg2rad);

	objrend::simulatePhoto(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_017_new.bmp",
		true);

	return 0;
}