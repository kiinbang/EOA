#pragma once

#include <fstream>
#include <string>
#include <vector>

const double deg2rad = 3.14159265358979 / 180.0;

bool readInput(const std::string& uvPath, 
	int& imgW, int& imgH,
	std::vector<int>& id,
	std::vector<int>& col, 
	std::vector<int>& row,
	double& E, double& N, double& U,
	double& omg0, double& phi0, double& kap0,
	double& dx, double& dy, double& dz,
	double& domg, double& dphi, double& dkap)
{
	std::fstream uvFile;
	uvFile.open(uvPath, std::ios::in);

	if (!uvFile)
		return false;

	col.clear();
	row.clear();

	try
	{
		uvFile >> E >> N >> U;
		uvFile >> omg0 >> phi0 >> kap0;
		omg0 *= deg2rad;
		phi0 *= deg2rad;
		kap0 *= deg2rad;
		uvFile >> dx >> dy >> dz;
		uvFile >> domg >> dphi >> dkap;
		domg *= deg2rad;
		dphi *= deg2rad;
		dkap *= deg2rad;

		int n;
		double u, v;
		while (!uvFile.eof())
		{
			uvFile >> n;
			uvFile >> u >> v;
			id.push_back(n);
			col.push_back(u);
			row.push_back(v);
		}
	}
	catch (...)
	{
		throw std::runtime_error("Error in readColRow");
	}
	
	return true;
}

bool writeOutput(const std::string& xyzPath, 
	std::vector<bool>& availability,
	std::vector<int>& id,
	std::vector<int>& col,
	std::vector<int>& row,
	const std::vector<double>& x, 
	const std::vector<double>& y, 
	const std::vector<double>& z)
{
	std::fstream xyzFile;
	xyzFile.open(xyzPath, std::ios::out);

	if (!xyzFile)
		return false;
	try
	{
		for (size_t i = 0; i < x.size(); ++i)
		{
			if (!availability[i])
				continue;

			xyzFile << id[i] << "\t";
			xyzFile << col[i] << "\t";
			xyzFile << row[i] << "\t";
			xyzFile << x[i] << "\t";
			xyzFile << y[i] << "\t";
			xyzFile << z[i] << std::endl;
		}
	}
	catch (...)
	{
		throw std::runtime_error("Error in writeXyz");
	}

	return true;
}