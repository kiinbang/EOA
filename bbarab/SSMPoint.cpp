/*
 * Copyright (c) 1999-2007, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// SSMPoint.cpp: implementation of the CSSMPoint class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SSMPoint.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSSMPoint::CSSMPoint()
{
	m_nDimension = 0;
	m_p = NULL;
}

CSSMPoint::CSSMPoint(CSSMPoint &copy)
{
	m_nDimension = 0;
	m_p = NULL;

	Copy(copy);
}

CSSMPoint CSSMPoint::operator=(CSSMPoint &copy)
{
	Copy(copy);
	return *this;
}

void CSSMPoint::Copy(CSSMPoint &copy)
{
	strcpy(this->m_ID, copy.m_ID);
	this->m_nDimension = copy.m_nDimension;
	this->SetDimension(this->m_nDimension);
	this->m_Dispersion = copy.m_Dispersion;
	memcpy(this->m_p, copy.m_p, sizeof(double)*m_nDimension);
}

CSSMPoint::CSSMPoint(int dimension)
{
	m_nDimension = 0;
	m_p = NULL;

	SetDimension(dimension);
}

CSSMPoint::~CSSMPoint()
{
	if(m_p != NULL)
		delete[] m_p;
}

void CSSMPoint::SetDimension(int dimension)
{
	if(m_p != NULL)
		delete[] m_p;
	m_nDimension = dimension;
	m_p = new double[m_nDimension];

	m_Dispersion.Resize(m_nDimension, m_nDimension);
	m_Dispersion.Identity(1.0);
}

double& CSSMPoint::operator[](int i)
{
	return m_p[i];
}