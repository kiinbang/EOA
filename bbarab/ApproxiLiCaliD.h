// ApproxiLiCaliD.h: interface for the ApproxiLiCaliD class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_APPROXILICALID_H__913B71B3_1BBE_4EC7_B0B6_C07504511FB9__INCLUDED_)
#define AFX_APPROXILICALID_H__913B71B3_1BBE_4EC7_B0B6_C07504511FB9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_NUM_OVERLAPS 5

#include <math.h>
#include "SMList.h"
#include "SMMatrix.h"
#include "SMRotationMatrix.h"
#include "KDTree.h"
using namespace SMATICS_BBARAB;

#include <fstream.h>
#include <iomanip.h>

typedef struct
{
	double x;
	double y;
	double z;
	int    Patch_ID;
}VERTEX;

class Triangle
{
public:
	double A[4];//XYZtime
	double B[4];
	double C[4];
	double N[4];//Plane parameters(a, b, c, d)
};

class Triangle2
{
public:
	double A[4];//XYZtime
	double B[4];
	double C[4];
	double R[3][3];
	double dist;
};

class Point
{
public:
	double East;
	double North;
	double EH;
};

class Point3D : public Point
{
public:
	
	Point3D operator +(const Point3D val1)
	{
		Point3D res;
		res.East = val1.East + this->East;
		res.North = val1.North + this->North;
		res.EH = val1.EH + this->EH;
		return res;
	}
	
	Point3D operator -(const Point3D val1)
	{
		Point3D res;
		res.East = -val1.East + this->East;
		res.North = -val1.North + this->North;
		res.EH = -val1.EH + this->EH;
		return res;
	}
};

class CorrectionTerm
{
public:
	double dPx, dPy, dDO, dDP, dDK, dp, dS;
	double sdPx, sdPy, sdDO, sdDP, sdDK, sdp, sdS;

	CorrectionTerm()
	{
		dPx=dPy=dDO=dDP=dDK=dp=dS=0.0;
		sdPx=sdPy=sdDO=sdDP=sdDK=sdp=sdS=0.0;
	}

	void Init()
	{
		dPx=dPy=dDO=dDP=dDK=dp=dS=0.0;
	}
};

class Triangle_Point
{
public:
	Point3D PA;
	double xA;
	double zA;
	double Kappa;
	
	Point3D PB;
	double A_xB;
	double A_zB;
	double A_KappaB;
	
	CSMMatrix<double> R;
	
	double VA[4];
	double VB[4];
	double VC[4];
	
	Triangle_Point(){}
	
	Triangle_Point(const Triangle_Point &copy)
	{
		Copy(copy);		
	}
	
	Triangle_Point& operator = (const Triangle_Point &copy)
	{
		Copy(copy);
		return *this;
	}
	
	void Copy(const Triangle_Point &copy)
	{
		this->PA = copy.PA;
		
		this->xA = copy.xA;
		this->zA = copy.zA;
		
		this->PB = copy.PB;
		
		this->A_xB = copy.A_xB;
		this->A_zB = copy.A_zB;
		
		this->Kappa = copy.Kappa;
		this->A_KappaB = copy.A_KappaB;
		
		this->VA[0] = copy.VA[0];
		this->VA[1] = copy.VA[1];
		this->VA[2] = copy.VA[2];
		this->VA[3] = copy.VA[3];
		
		this->VB[0] = copy.VB[0];
		this->VB[1] = copy.VB[1];
		this->VB[2] = copy.VB[2];
		this->VB[3] = copy.VB[3];
		
		this->VC[0] = copy.VC[0];
		this->VC[1] = copy.VC[1];
		this->VC[2] = copy.VC[2];
		this->VC[3] = copy.VC[3];
		
		R = copy.R;
	}
};

class TrjRecord
{
public:
	
	//[Trajectory file contents]
	//TIME, 
	//DISTANCE, 
	//EASTING, NORTHING, ELLIPSOID HEIGHT, 
	//LATITUDE, LONGITUDE, ELLIPSOID HEIGHT, 
	//ROLL, PITCH, HEADING, 
	//EAST VELOCITY, NORTH VELOCITY, UP VELOCITY, 
	//EAST SD, NORTH SD, HEIGHT SD, 
	//ROLL SD, PITCH SD, HEADING SD
	double time;//sec
	//double Distance;//meter
	double East;//meter
	double North;//meter
	double EH1;//meter
	//double Lat;//deg
	//double Lon;//deg
	//double EH2;//meter
	//double Roll;//deg
	//double Pitch;//deg
	//double Heading;//deg
	double V_East;//m/sec
	double V_North;//m/sec
	double V_Up;//m/sec
	//double SD_East;//meter
	//double SD_North;//meter
	//double SD_EH;//meter
	//double SD_Roll;//deg
	//double SD_Pitch;//deg
	//double SD_Heading;//deg
};

class BrazilTXYZI
{
public:
	//[Brazil data contents]
	//GPS time,
	//East_first, North_first, EH_first, Intensity_first
	//East_last, North_last, EH_last, Internsity_last
	double time;
	//double East_First;
	//double North_First;
	//double EH_First;
	//int Intensity_First;
	double East_Last;
	double North_Last;
	double EH_Last;
	int Intensity_Last;
};

class ApproxiLiCaliD  
{
public:
	double radius;//KD-tree search radius
	CSMList<double> ave_side_List;
	double max_ND;
	int option_printout_sf1_sf2;
	int option_corresponding_point;//Ki In Dec-15 2008
	CString PrjPath;
	CString TrajectoryPath;
	CSMList<CString> BrazilFormatDataPath;//Ki In Dec-14 2008
	CSMList<CString> TINPath;//Ki In Dec-14 2008
	int num_overlap_pairs;//Ki In Dec-14 2008
	CString UnMatchPointPath;
	CString MatchPointPath;
	CString MatchTriPath;
	CString KappaPath;

	CSMList<double> sum_ND_List;//Ki In Dec-14 2008
	CSMList<int> match_count_List;//Ki In Dec-14 2008
	
	CorrectionTerm dParams;
	CorrectionTerm old_dParams;

	double Max_Tri_Side;
	
	double sX, sY, sZ;
	double priori_var;
	int max_iteration;
	double diff_sigma;
	double max_sigma;
	double min_sigma;
	double East_Offset_TRJ;
	CSMList<TrjRecord> Trajectory;
	KDTree KDTrjTime;
	CSMList<BrazilTXYZI> *PointCloud;//Ki In Dec-14 2008
	CSMList<Triangle_Point> TriPointList;
	//CSMList<KDTree> TINList;
	KDTree TINList[MAX_NUM_OVERLAPS];//Ki In Dec-14 2008

	bool bLogFiles;

public:
		
	ApproxiLiCaliD();
	virtual ~ApproxiLiCaliD();

	bool RunCalibration(CString prjpath, CString outpath);

private:
	bool ReadPrj(int &option, CString PrjPath);
	
	bool ReadTrj(CString TrjPath, CSMList<TrjRecord> &Trajectory);

	
	bool ReadBrazilFormat(CString BrazilFormatPath, int iOverlap);

	bool ReadTXYZ(CString TXYZFormatPath, int iOverlap);
	
	bool ReadTINFormat(CString TINPath, int iOverlap);
	
	bool ReadTrj2(CString TrjPath, CSMList<TrjRecord> &Trajectory);
	
	void FindPseudoConjugate(int nOverlap, fstream &ResFile);
	
	double CalTriangleArea(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3);

	
	bool SearchTrajectory(double time, Point3D point, Point3D &pos, double &Kappa, double &Kappa2, double &lateral_coord);
	bool SearchTrajectory(Point3D point, Point3D pos0, Point3D pos1, Point3D &pos, double &Kappa, double &Kappa2, double &lateral_coord);
		
	bool DiscrepancyFunction(CSMMatrix<double> &ai, CSMMatrix<double> &yi, Triangle_Point TriPointPair);
	
	void MatrixOut(fstream &ResFile, CSMMatrix<double> M, CString title, int pre, int width);
	
	void UpdateUnknowns(CSMMatrix<double> X);
	
	
	void ReconstructCoordinate(double oldX, double oldY, double oldZ, double Kappa, double zA, double xA, double SB, double &newX, double &newY, double &newZ);
	
	double Solve(fstream &ResFile, int num_repeat);
	
	void UpdateCoordinates();

	bool FindString(fstream fin, char target[], int &result);
	void PrintOutSf1andSf2();
	
};

#endif // !defined(AFX_APPROXILICALID_H__913B71B3_1BBE_4EC7_B0B6_C07504511FB9__INCLUDED_)
