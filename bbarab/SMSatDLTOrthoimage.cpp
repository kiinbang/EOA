#include "stdafx.h"
#include "SMSatDLTOrthoimage.h"
#include <ProgressBar.h>

#define PATCH_ORTHO_RESOLUTION 10

#define _MAXDIFFROW_ 0.5
#define BACKGROUND 200
#define WALLINDEX -888

/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////
//
//
// CSatDLTOrthoImage
//
//
//
//
//
/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////

CSatDLTOrthoImage::CSatDLTOrthoImage()
{
}

CSatDLTOrthoImage::~CSatDLTOrthoImage()
{
}

bool CSatDLTOrthoImage::SetOriginImage(char* FileName)
{
	OriginImage.Load(FileName);
	
	return TRUE;
	
}

bool CSatDLTOrthoImage::MakeOrtho(DWORD Start_C, DWORD Start_R, DWORD End_C, DWORD End_R, char* OrthoName)
{
	CImage OrthoImage;
	OrthoImage.Create(End_C-Start_C,End_R-Start_R,8);
	CPixelPtr OrthoPixel(OrthoImage);
	CPixelPtr OriginPixel(OriginImage);
	DWORD W, H;
	W = (DWORD)OriginImage.GetWidth();
	H = (DWORD)OriginImage.GetHeight();
	
	for(DWORD i=Start_R;i<End_R;i++)
	{
		for(DWORD j=Start_C;j<End_C;j++)
		{
			double X, Y, Z;
			Point2D<double> a;
			
			X = LIDAR.GetX(j);
			Y = LIDAR.GetY(i);
			Z = LIDAR.GetZ(j,i);
			
			a = SatDLT.GetGround2Image(X, Y, Z);
			//Nearest Neighborhood
			/*
			if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
			{
			if(Z == -1)
			{
			OrthoPixel[LONG(i-Start_R)][LONG(j-Start_C)] = (BYTE)0;
			}
			else
			{
			OrthoPixel[LONG(i-Start_R)][LONG(j-Start_C)] = OriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
			}
			}
			else
			{
			OrthoPixel[LONG(i-Start_R)][LONG(j-Start_C)] = (BYTE)0;
			}
			*/
			
			//Bilinear Interpolation
			if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
			{
				double value1, value2, value3, value4;
				BYTE value;
				value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
				value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
				value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
				value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
				
				double A, B, C, D;
				A = (double)((int)a.x + 1) - a.x;
				B = 1 - A;
				C = (double)((int)a.y + 1) - a.y;
				D = 1 - C;
				if(Z == -1)
				{
					value = 0;
				}
				else
				{
					value = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
				}
				OrthoPixel[LONG(i-Start_R)][LONG(j-Start_C)] = value;
			}
			else
				OrthoPixel[LONG(i-Start_R)][LONG(j-Start_C)] = (BYTE)0;
			
		}
	}
	
	OrthoImage.Save(OrthoName);
	
	return TRUE;
}

bool CSatDLTOrthoImage::MakeOrtho_Affine(DWORD Start_C, DWORD Start_R, DWORD End_C, DWORD End_R, char* OrthoName)
{
	CImage OrthoImage;
	LONG size;
	size = (LONG)LIDAR.offset/PATCH_ORTHO_RESOLUTION;
	OrthoImage.Create((End_C-Start_C)*size,(End_R-Start_R)*size,8);
	CPixelPtr OrthoPixel(OrthoImage);
	CPixelPtr OriginPixel(OriginImage);
	DWORD W, H;
	W = (DWORD)OriginImage.GetWidth();
	H = (DWORD)OriginImage.GetHeight();
	DWORD Orow, Ocol;
	
	Orow = 0;
	for(DWORD i=Start_R;i<End_R-1;i++)
	{
		Ocol = 0;
		for(DWORD j=Start_C;j<End_C-1;j++)
		{
			KnownPoint Known_P(4);
			Affine affine;
			double X, Y, Z;
			Point2D<double> a;
			
			X = LIDAR.GetX(j);
			Y = LIDAR.GetY(i);
			Z = LIDAR.GetZ(j,i);
			a = SatDLT.GetGround2Image(X, Y, Z);
			Known_P.SetPoint(0,Ocol,Orow,a.x,a.y);
			if(Z == -1) { Ocol = Ocol + size;	 continue;}
			
			X = LIDAR.GetX(j+1);
			Y = LIDAR.GetY(i);
			Z = LIDAR.GetZ(j+1,i);
			a = SatDLT.GetGround2Image(X, Y, Z);
			Known_P.SetPoint(1,Ocol+size,Orow,a.x,a.y);
			if(Z == -1) { Ocol = Ocol + size;	 continue;}
			
			X = LIDAR.GetX(j);
			Y = LIDAR.GetY(i+1);
			Z = LIDAR.GetZ(j,i+1);
			a = SatDLT.GetGround2Image(X, Y, Z);
			Known_P.SetPoint(2,Ocol,Orow+size,a.x,a.y);
			if(Z == -1) { Ocol = Ocol + size;	 continue;}
			
			X = LIDAR.GetX(j+1);
			Y = LIDAR.GetY(i+1);
			Z = LIDAR.GetZ(j+1,i+1);
			a = SatDLT.GetGround2Image(X, Y, Z);
			Known_P.SetPoint(3,Ocol+size,Orow+size,a.x,a.y);
			if(Z == -1) { Ocol = Ocol + size;	 continue;}
			
			affine.Affine_Transform(Known_P);
			
			for(int c1=0;c1<size;c1++)
			{
				for(int c2=0;c2<size;c2++)
				{
					Point2D<double> p, q;
					
					p.x = Ocol + c1;
					p.y = Orow + c2;
					
					q = affine.CoordTransform(p);
					
					//Bilinear Interpolation
					if(((q.x > 0) && (q.y >0)) && ((q.x < W-1) && (q.y < H-1)))
					{
						double value1, value2, value3, value4;
						BYTE value;
						value1 = (double)OriginPixel[LONG(q.y)][LONG(q.x)];
						value2 = (double)OriginPixel[LONG(q.y)][LONG(q.x+1)];
						value3 = (double)OriginPixel[LONG(q.y+1)][LONG(q.x)];
						value4 = (double)OriginPixel[LONG(q.y+1)][LONG(q.x+1)];
						
						double A, B, C, D;
						A = (double)((int)q.x + 1) - q.x;
						B = 1 - A;
						C = (double)((int)q.y + 1) - q.y;
						D = 1 - C;
						value = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
						
						OrthoPixel[LONG(p.y)][LONG(p.x)] = value;
					}
					else
					{
						OrthoPixel[LONG(p.y)][LONG(p.x)] = (BYTE)0;
					}
				}				
			}
			
			
			//Nearest Neighborhood
			//if((int(a.x+0.5) < OriginImage.GetWidth()) && (int(a.y+0.5) < OriginImage.GetHeight()))
			//{
			//	OrthoPixel[LONG(i-Start_R)][LONG(j-Start_C)] = OriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
			//}
			//else
			//	OrthoPixel[LONG(i-Start_R)][LONG(j-Start_C)] = (BYTE)0;
			
			
			Ocol = Ocol + size;				
		}
		
		Orow = Orow + size;
	}
	
	OrthoImage.Save(OrthoName);
	
	return TRUE;
}

bool CSatDLTOrthoImage::MakeOrtho_Convex(DWORD Start_C, DWORD Start_R, DWORD End_C, DWORD End_R, char* OrthoName)
{	
	CImage OrthoImage;
	LONG size;
	size = (LONG)LIDAR.offset/PATCH_ORTHO_RESOLUTION;
	OrthoImage.Create((End_C-Start_C)*size,(End_R-Start_R)*size,8);
	CPixelPtr OrthoPixel(OrthoImage);
	CPixelPtr OriginPixel(OriginImage);
	DWORD W, H;
	W = (DWORD)OriginImage.GetWidth();
	H = (DWORD)OriginImage.GetHeight();
	
	double TA = size*size/2; //Whole Area
	
	//area ratio table
	double *ratio;
	DWORD i,j, m, n;
	ratio = new double[size*size*3];
	Point2D<double> oip[4];
	oip[0].x = 0.0;
	oip[0].y = 0.0;
	oip[1].x = (double)size;
	oip[1].y = 0.0;
	oip[2].x = 0.0;
	oip[2].y = (double)size;
	oip[3].x = (double)size;
	oip[3].y = (double)size;
	
	for(i=0;i<(DWORD)size;i++)
	{
		//upper
		for(j=0;j<(size-i);j++)
		{
			//Area
			double A[3];
			//simplicial coordinates
			Point2D<double> P;
			P.x = j;
			P.y = i;
			A[0] = TriArea(P,oip[1],oip[2]);
			A[1] = TriArea(P,oip[0],oip[2]);
			A[2] = TriArea(P,oip[0],oip[1]);
			//assignment ratio to table
			ratio[(i*size+j)*3+0] = A[0]/TA;
			ratio[(i*size+j)*3+1] = A[1]/TA;
			ratio[(i*size+j)*3+2] = A[2]/TA;
		}
		
		//lower
		for(j=((DWORD)size-i);j<(DWORD)size;j++)
		{
			//Area
			double A[3];
			//simplicial coordinates
			Point2D<double> P;
			P.x = j;
			P.y = i;
			A[0] = TriArea(P,oip[1],oip[2]);
			A[1] = TriArea(P,oip[1],oip[3]);
			A[2] = TriArea(P,oip[2],oip[3]);
			//assignment ratio to table
			ratio[(i*size+j)*3+0] = A[0]/TA;
			ratio[(i*size+j)*3+1] = A[1]/TA;
			ratio[(i*size+j)*3+2] = A[2]/TA;
		}
	}
	
	//ortho-image resampling
	for(i=Start_R;i<End_R-1;i++)
	{
		for(j=Start_C;j<End_C-1;j++)
		{
			double X1, Y1, Z1;
			double X2, Y2, Z2;
			double X3, Y3, Z3;
			double X4, Y4, Z4;
			Point2D<double> a1, a2, a3, a4;
			Point2D<double> imagePoint[3];
			
			X1 = LIDAR.GetX(j);
			Y1 = LIDAR.GetY(i);
			Z1 = LIDAR.GetZ(j,i);
			a1 = SatDLT.GetGround2Image(X1, Y1, Z1);
			if(Z1 == -1) { continue;}
			
			X2 = LIDAR.GetX(j+1);
			Y2 = LIDAR.GetY(i);
			Z2 = LIDAR.GetZ(j+1,i);
			a2 = SatDLT.GetGround2Image(X2, Y2, Z2);
			if(Z2 == -1) { continue;}
			
			X3 = LIDAR.GetX(j);
			Y3 = LIDAR.GetY(i+1);
			Z3 = LIDAR.GetZ(j,i+1);
			a3 = SatDLT.GetGround2Image(X3, Y3, Z3);
			if(Z3 == -1) { continue;}
			
			X4 = LIDAR.GetX(j+1);
			Y4 = LIDAR.GetY(i+1);
			Z4 = LIDAR.GetZ(j+1,i+1);
			a4 = SatDLT.GetGround2Image(X4, Y4, Z4);
			if(Z4 == -1) { continue;}
			
			/////////////////
			//Upper Triangle
			/////////////////
			double za1 = Z1;
			double za2 = Z2;
			double za3 = Z3;
			
			double xa1 = X1,		ya1 = Y1;//Ground Coord
			double xa2 = X2,		ya2 = Y2;//Ground Coord
			double xa3 = X3,		ya3 = Y3;//Ground Coord
			
			//Image Point
			imagePoint[0] = a1;
			imagePoint[1] = a2;
			imagePoint[2] = a3;
			
			//Assign Greyvalue
			for(m=0;m<(DWORD)size;m++)
			{
				for(n=0;n<(DWORD)(size-m);n++)
				{
					//convex dombination
					
					/**********************************
					//Nearest Image row & col
					Point2D<int> Q;
					Q.x = (int)(ratio[(m*size+n)*3+0]*imagePoint[0].x+ratio[(m*size+n)*3+1]*imagePoint[1].x+ratio[(m*size+n)*3+2]*imagePoint[2].x + 0.5);
					Q.y = (int)(ratio[(m*size+n)*3+0]*imagePoint[0].y+ratio[(m*size+n)*3+1]*imagePoint[1].y+ratio[(m*size+n)*3+2]*imagePoint[2].y + 0.5);
					
					  if(((Q.x > 0) && (Q.y >0)) && ((Q.x < (int)(W-1)) && (Q.y < (int)(H-1))))
					  {
					  //OrthoPixel[LONG(p.y)][LONG(p.x)] = value;
					  OrthoPixel[LONG(i*size+m)][LONG(j*size+n)] = (BYTE)OriginPixel[Q.y][Q.x];
					  }
					  else
					  {
					  OrthoPixel[LONG(i*size+m)][LONG(j*size+n)] = (BYTE)0;
					  }
					*****************************/
					
					/*********************************/
					//Bilinear Interpolation
					Point2D<int> Q;
					
					Q.x = (int)(ratio[(m*size+n)*3+0]*imagePoint[0].x+ratio[(m*size+n)*3+1]*imagePoint[1].x+ratio[(m*size+n)*3+2]*imagePoint[2].x);
					Q.y = (int)(ratio[(m*size+n)*3+0]*imagePoint[0].y+ratio[(m*size+n)*3+1]*imagePoint[1].y+ratio[(m*size+n)*3+2]*imagePoint[2].y);
					
					if(((Q.x > 0) && (Q.y >0)) && ((Q.x < (int)(W-1)) && (Q.y < (int)(H-1))))
					{
						double value1, value2, value3, value4;
						BYTE value;
						value1 = (double)OriginPixel[LONG(Q.y)][LONG(Q.x)];
						value2 = (double)OriginPixel[LONG(Q.y)][LONG(Q.x+1)];
						value3 = (double)OriginPixel[LONG(Q.y+1)][LONG(Q.x)];
						value4 = (double)OriginPixel[LONG(Q.y+1)][LONG(Q.x+1)];
						
						double A, B, C, D;
						A = (double)((int)Q.x + 1) - Q.x;
						B = 1 - A;
						C = (double)((int)Q.y + 1) - Q.y;
						D = 1 - C;
						value = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
						
						OrthoPixel[LONG(i*size+m)][LONG(j*size+n)] = value;
					}
					else
					{
						OrthoPixel[LONG(i*size+m)][LONG(j*size+n)] = (BYTE)0;
					}
					/**************************/
					
				}//for(n)
			}//for(m)
			
			/////////////////
			//Lower Triangle
			/////////////////
			za1 = Z2;
			za2 = Z3;
			za3 = Z4;
			
			xa1 = X2;		ya1 = Y2;//Ground Coord
			xa2 = X3;		ya2 = Y3;//Ground Coord
			xa3 = X4;		ya3 = Y4;//Ground Coord
			
			//Image Point
			imagePoint[0] = a4;
			imagePoint[1] = a3;
			imagePoint[2] = a2;
			
			//Assign Greyvalue
			for(m=0;m<(DWORD)size;m++)
			{
				for(n=(DWORD)(size-m);n<(DWORD)(size);n++)
				{
					//convex dombination
					Point2D<int> Q;
					Q.x = (int)(ratio[(m*size+n)*3+0]*imagePoint[0].x+ratio[(m*size+n)*3+1]*imagePoint[1].x+ratio[(m*size+n)*3+2]*imagePoint[2].x + 0.5);
					Q.y = (int)(ratio[(m*size+n)*3+0]*imagePoint[0].y+ratio[(m*size+n)*3+1]*imagePoint[1].y+ratio[(m*size+n)*3+2]*imagePoint[2].y + 0.5);
					
					//Nearest Image row & col
					if(((Q.x > 0) && (Q.y >0)) && ((Q.x < (int)(W-1)) && (Q.y < (int)(H-1))))
					{
						//OrthoPixel[LONG(p.y)][LONG(p.x)] = value;
						OrthoPixel[LONG(i*size+m)][LONG(j*size+n)] = (BYTE)OriginPixel[Q.y][Q.x];
					}
					else
					{
						OrthoPixel[LONG(i*size+m)][LONG(j*size+n)] = (BYTE)0;
					}
				}//for(n)
			}//for(m)
			
		}//for(j)
	}//for(i)
	
	OrthoImage.Save(OrthoName);
	
	return true;
}

double CSatDLTOrthoImage::TriArea(Point2D<double> p, Point2D<double> p1, Point2D<double> p2)
{
	double area;
	area = fabs((p2.x-p1.x)*p.y+(p1.y-p2.y)*p.x+(p2.y*p1.x-p1.y*p2.x))/2.0;
	return area;
}
