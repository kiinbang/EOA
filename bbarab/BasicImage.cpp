// BasicImage.cpp: implementation of the CBasicImage class.
//
//////////////////////////////////////////////////////////////////////

#include "BasicImage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define DIB_HEADER_MARKER   ((WORD) ('M' << 8) | 'B')

#include <math.h>

/******************************************************
CBasicImage
******************************************************/

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBasicImage::CBasicImage()
{
	m_hImage	 = NULL;
	m_pPal		 = NULL;
	m_hUndoImage = NULL;
}

CBasicImage::CBasicImage( CBasicImage &Src )
{
	m_Size		 = CSize(1,1);
	m_hImage	 = NULL;
	m_pPal		 = NULL;
	m_hUndoImage = NULL;

	*this = Src;
}

void CBasicImage::SetHandle(HANDLE hHandle)
{
	m_hImage = (HDIB)hHandle;
}

CBasicImage::~CBasicImage()
{
	Free();
}

CBasicImage* CBasicImage::operator=( CBasicImage *Src )  // Right side is the argument.
{
	Free();
	m_hImage = (HDIB) ::CopyHandle( Src->GetHandle() );
	m_hUndoImage = (HDIB) ::CopyHandle( Src->GetUndoHandle() );
	m_Size = Src->GetSize();
	m_pPal = new CPalette;
 	CreateDIBPalette();
	return (this);
}

CBasicImage& CBasicImage::operator=( CBasicImage &Src )  // Right side is the argument.
{
	Free();
	m_hImage = (HDIB) ::CopyHandle( Src.GetHandle() );
	m_hUndoImage = (HDIB) ::CopyHandle( Src.GetUndoHandle() );
	m_Size = Src.GetSize();
	m_pPal = new CPalette;
 	CreateDIBPalette();
	return (*this);
}

BOOL CBasicImage::Undo()
{
	HDIB hTemp;	
	
	// 백업이 존재하면 백업과 이미지 핸들을 교환
	if(m_hUndoImage)
	{
		hTemp = m_hImage;
		m_hImage = m_hUndoImage;
		m_hUndoImage = hTemp;
		return TRUE;
	}
	else
		return FALSE;
}

BOOL CBasicImage::PrepareUndo()
{
	// 이미 백업이 존재하면 이를 해제함
	if(m_hUndoImage) 
	{
		::GlobalFree((HGLOBAL)m_hUndoImage);
		m_hUndoImage = NULL;
	}

	// 이미지를 통째로 복사하여 백업을 만듬
	if ((m_hUndoImage = (HDIB)::CopyHandle((HGLOBAL)m_hImage)) == NULL)
	{
		m_hUndoImage = NULL;
		return FALSE;
	}
	return TRUE;
}

void CBasicImage::Free()
{
	if( m_hImage )
	{
		if( GlobalFree( m_hImage ) != NULL)
		{
			TRACE("Can't free handle in CImage::Free()");
		}
		m_hImage = NULL;
	}
	if( m_hUndoImage )
	{
		if( GlobalFree( m_hUndoImage ) != NULL)
		{
			TRACE("Can't free handle in CRawImage::Free()");
		}
		m_hUndoImage = NULL;
	}

	if(m_pPal != NULL)
	{
		delete m_pPal;
		m_pPal = NULL;
	}
}

BOOL CBasicImage::Draw(HDC hDC, LPRECT lpDIBRect, LPRECT lpDCRect)
{
	LPSTR	lpDIBHdr;	// BITMAPINFOHEADER를 가리킬 포인터
	LPSTR	lpDIBBits;	// DIB 비트를 가리킬 포인터
	BOOL		bSuccess=FALSE;	 // Success/fail 플래그
	HPALETTE 	hPal=NULL;		 // DIB 팔레트
	HPALETTE 	hOldPal=NULL;	 // 이전 팔레트

	// 메모리 고정
	lpDIBHdr  = (LPSTR) ::GlobalLock((HGLOBAL) m_hImage);
	// DIB 비트가 저장되어 있는 곳의 주소를 얻음
	lpDIBBits = ::FindDIBBits(lpDIBHdr);

	// 팔레트를 얻어 DC에 선택
	if(m_pPal != NULL)
	{
		hPal = (HPALETTE) m_pPal->m_hObject;
		hOldPal = ::SelectPalette(hDC, hPal, TRUE);
	}

	::SetStretchBltMode(hDC, COLORONCOLOR);

	if ((RECTWIDTH(lpDCRect)  == RECTWIDTH(lpDIBRect)) &&
	   (RECTHEIGHT(lpDCRect) == RECTHEIGHT(lpDIBRect)))
		// 원래 크기로 그대로 출력하는 경우
		bSuccess = ::SetDIBitsToDevice(hDC, // hDC
			lpDCRect->left,		 			// DestX
			lpDCRect->top,		 			// DestY
			RECTWIDTH(lpDCRect),	 		// nDestWidth
			RECTHEIGHT(lpDCRect),			// nDestHeight
			lpDIBRect->left,		 		// SrcX
			(int)DIBHeight(lpDIBHdr) - lpDIBRect->top -	RECTHEIGHT(lpDIBRect),   		// SrcY
			0,                          	// nStartScan
			(WORD)DIBHeight(lpDIBHdr),  	// nNumScans
			lpDIBBits,                  	// lpBits
			(LPBITMAPINFO)lpDIBHdr,			// lpBitsInfo
			DIB_RGB_COLORS);				// wUsage
	else	// 확대 또는 축소하여 출력하는 경우
		bSuccess = ::StretchDIBits(hDC, 	// hDC
			lpDCRect->left,					// DestX
			lpDCRect->top,					// DestY
			RECTWIDTH(lpDCRect),			// nDestWidth
			RECTHEIGHT(lpDCRect),			// nDestHeight
			lpDIBRect->left,				// SrcX
			(int)DIBHeight(lpDIBHdr) - lpDIBRect->top -	RECTHEIGHT(lpDIBRect),   		// SrcY
			//lpDIBRect->top,					// SrcY
			RECTWIDTH(lpDIBRect),			// wSrcWidth
			RECTHEIGHT(lpDIBRect),			// wSrcHeight
			lpDIBBits,						// lpBits
			(LPBITMAPINFO)lpDIBHdr,			// lpBitsInfo
			DIB_RGB_COLORS,					// wUsage
			SRCCOPY);						// dwROP

	// 메모리 놓아줌
   ::GlobalUnlock((HGLOBAL) m_hImage);
	// DC 복원
	if (hOldPal != NULL) ::SelectPalette(hDC, hOldPal, TRUE);
	return bSuccess;
}

BOOL CBasicImage::Create(int width, int height, int depth)
{
    LPBITMAPINFOHEADER lpbi ;
	BYTE		*lpPal;
    DWORD       dwSizeImage;
    int         i;

    dwSizeImage = height*(DWORD)((width*depth/8+3)&~3);

	if(depth == 24)
		m_hImage= (HDIB)GlobalAlloc(GHND,sizeof(BITMAPINFOHEADER)+dwSizeImage);
    else
		m_hImage= (HDIB)GlobalAlloc(GHND,sizeof(BITMAPINFOHEADER)+dwSizeImage + 1024);

    if (m_hImage == NULL)
        return FALSE;

	lpbi = (LPBITMAPINFOHEADER)GlobalLock(m_hImage);
	lpbi->biSize            = sizeof(BITMAPINFOHEADER) ;
    lpbi->biWidth           = width;
    lpbi->biHeight          = height;
    lpbi->biPlanes          = 1;
    lpbi->biBitCount        = depth;
    lpbi->biCompression     = BI_RGB ;
    lpbi->biSizeImage       = dwSizeImage;
    lpbi->biXPelsPerMeter   = 0 ;
    lpbi->biYPelsPerMeter   = 0 ;
    lpbi->biClrUsed         = 0 ;
    lpbi->biClrImportant    = 0 ;

	lpPal = (BYTE *) lpbi;
	if (depth == 8)
	{
		lpbi->biClrUsed = 256;

		DWORD offDest = sizeof(BITMAPINFOHEADER);
		for(i = 0; i < 256; i++)
		{
			lpPal[offDest++] = (BYTE)i;
			lpPal[offDest++] = (BYTE)i;
			lpPal[offDest++] = (BYTE)i;
			lpPal[offDest++] = 0x00;
		}                  
	}

	InitDIB(FALSE);
	return TRUE;
}

/******************************************************
				이미지 정보를 얻는 함수
******************************************************/

int CBasicImage::GetBitCount()
{
	if (m_hImage == NULL) return -1;
	LPBITMAPINFOHEADER lpbi;
	lpbi = (LPBITMAPINFOHEADER) ::GlobalLock((HGLOBAL) m_hImage );
	return lpbi->biBitCount;
}

BOOL CBasicImage::InitDIB(BOOL bCreatePalette)
{
	// 이미지의 가로, 세로 크기 설정
	LPSTR pDIB = (LPSTR)::GlobalLock((HGLOBAL) m_hImage);
	m_Size = CSize((int) ::DIBWidth(pDIB), (int) ::DIBHeight(pDIB));
	::GlobalUnlock((HGLOBAL) m_hImage);

	if(bCreatePalette)
	{
		if(m_pPal != NULL) delete m_pPal;
		// 팔레트 생성
		m_pPal = new CPalette;
		if (CreateDIBPalette() == NULL)
		{
			// 팔레트를 가지지 않는 경우
			delete m_pPal;
			m_pPal = NULL;
			return FALSE;
		}
	}
	return TRUE;
}

BOOL CBasicImage::CreateDIBPalette()
{
	LPLOGPALETTE lpPal;      // pointer to a logical palette
	HANDLE hLogPal;          // handle to a logical palette
	HPALETTE hPal = NULL;    // handle to a palette
	int i;                   // loop index
	WORD wNumColors;         // number of colors in color table
	LPSTR lpbi;              // pointer to packed-DIB
	LPBITMAPINFO lpbmi;      // pointer to BITMAPINFO structure (Win3.0)
	LPBITMAPCOREINFO lpbmc;  // pointer to BITMAPCOREINFO structure (old)
	BOOL bWinStyleDIB;       // flag which signifies whether this is a Win3.0 DIB
	BOOL bResult = FALSE;

	/* if handle to DIB is invalid, return FALSE */

	if (m_hImage == NULL)
	  return FALSE;

   lpbi = (LPSTR) ::GlobalLock((HGLOBAL) m_hImage);

   /* get pointer to BITMAPINFO (Win 3.0) */
   lpbmi = (LPBITMAPINFO)lpbi;

   /* get pointer to BITMAPCOREINFO (old 1.x) */
   lpbmc = (LPBITMAPCOREINFO)lpbi;

   /* get the number of colors in the DIB */
   wNumColors = ::DIBNumColors(lpbi);
   
   if (wNumColors != 0)
   {
	   /* allocate memory block for logical palette */
	   hLogPal = ::GlobalAlloc(GHND, sizeof(LOGPALETTE)
		   + sizeof(PALETTEENTRY)
		   * wNumColors);
	   
	   /* if not enough memory, clean up and return NULL */
	   if (hLogPal == 0)
	   {
		   ::GlobalUnlock((HGLOBAL) m_hImage);
		   return FALSE;
	   }
	   
	   lpPal = (LPLOGPALETTE) ::GlobalLock((HGLOBAL) hLogPal);
	   
	   /* set version and number of palette entries */
	   lpPal->palVersion = PALVERSION;
	   lpPal->palNumEntries = (WORD)wNumColors;
	   
	   /* is this a Win 3.0 DIB? */
	   bWinStyleDIB = IS_WIN30_DIB(lpbi);
	   for (i = 0; i < (int)wNumColors; i++)
	   {
		   if (bWinStyleDIB)
		   {
			   lpPal->palPalEntry[i].peRed = lpbmi->bmiColors[i].rgbRed;
			   lpPal->palPalEntry[i].peGreen = lpbmi->bmiColors[i].rgbGreen;
			   lpPal->palPalEntry[i].peBlue = lpbmi->bmiColors[i].rgbBlue;
			   lpPal->palPalEntry[i].peFlags = 0;
		   }
		   else
		   {
			   lpPal->palPalEntry[i].peRed = lpbmc->bmciColors[i].rgbtRed;
			   lpPal->palPalEntry[i].peGreen = lpbmc->bmciColors[i].rgbtGreen;
			   lpPal->palPalEntry[i].peBlue = lpbmc->bmciColors[i].rgbtBlue;
			   lpPal->palPalEntry[i].peFlags = 0;
		   }
	   }
	   
	   /* create the palette and get handle to it */
	   bResult = m_pPal->CreatePalette(lpPal);
	   ::GlobalUnlock((HGLOBAL) hLogPal);
	   ::GlobalFree((HGLOBAL) hLogPal);
   }
   
   ::GlobalUnlock((HGLOBAL) m_hImage);
   
   return bResult;
}

void CBasicImage::GetRainbowColor(BYTE index, BYTE &r, BYTE &g, BYTE &b)
{
	if((0<=index)&&(index<=48))
	{
		r = (BYTE)0;						g = (BYTE)(index/48*255);				b = (BYTE)255;
		return;
	}
	else if((48<index)&&(index<=96))
	{
		r = (BYTE)0;						g = (BYTE)255;							b = (BYTE)((96-index)/47*255);
		return;
	}
	else if((96<index)&&(index<=144))
	{
		b = (BYTE)((index - 97)/47*255);	g = (BYTE)255;							b = (BYTE)0;
		return;
	}
	else if((144<index)&&(index<=192))
	{
		b = (BYTE)255;						g = (BYTE)(255 - (index-145)/47*126);	b = (BYTE)0;
		return;
	}
	else if((192<index)&&(index<=240))
	{
		b = (BYTE)255;						g = (BYTE)(126 - (index-193)/47*126);	b = (BYTE)0;
		return;
	}
	else if((192<index)&&(index<=240))
	{
		b = (BYTE)255;						g = (BYTE)(126 - (index-193)/47*126);	b = (BYTE)0;
		return;
	}
	else if((241<index)&&(index<=255))
	{
		b = (BYTE)255;						g = (BYTE)((index-242)/13*255);			b = (BYTE)((index-242)/13*255);
		return;
	}
	else
	{
		b = (BYTE)0;						g = (BYTE)0;							b = (BYTE)0;
		return;
	}
}

void CBasicImage::GetRainbowColor_new(BYTE index, BYTE &r, BYTE &g, BYTE &b)
{
	if(index < 1)
	{
		r = 0; g = 0; b = 0;
		return;
	}
	else if(index < 20)
	{
		r = (BYTE)(55 + g * 10); g = 0; b = 0;
		return;
	}
	else if(index < 48)
	{
		r = (BYTE)(256 - (index - 20) * 9);
		g = (BYTE)((index - 20) * 9);
		b = 0;
		return;
	}
	else if(index < 64)
	{
		r = 0;
		g = 255;
		b = (BYTE)((index - 48) * 16);

		return;
	}
	else if(index < 128)
	{
		r = 0;
		g = (unsigned char)(256 - (index - 64) * 4);
		b = 255;

		return;
	}
	else
	{
		r = 0;
		g = 0;
		b = (unsigned char)(256 - (index - 128) * 2);

		return;
	}	
}

void CBasicImage::GetRainbowColorfromXYZ(double X, double Y, double Z, BYTE &r, BYTE &g, BYTE &b)
{
	double var_X = X / 100;        //Where X = 0 ÷  95.047
	double var_Y = Y / 100;        //Where Y = 0 ÷ 100.000
	double var_Z = Z / 100;        //Where Z = 0 ÷ 108.883

	double var_R = var_X *  3.2406 + var_Y * -1.5372 + var_Z * -0.4986;
	double var_G = var_X * -0.9689 + var_Y *  1.8758 + var_Z *  0.0415;
	double var_B = var_X *  0.0557 + var_Y * -0.2040 + var_Z *  1.0570;

	if ( var_R > 0.0031308 ) var_R = 1.055 * pow( var_R , ( 1 / 2.4 ) ) - 0.055;
	else                     var_R = 12.92 * var_R;

	if ( var_G > 0.0031308 ) var_G = 1.055 * pow( var_G , ( 1 / 2.4 ) ) - 0.055;
	else                     var_G = 12.92 * var_G;

	if ( var_B > 0.0031308 ) var_B = 1.055 * pow( var_B , ( 1 / 2.4 ) ) - 0.055;
	else                     var_B = 12.92 * var_B;

	r = (BYTE)(var_R * 255);
	g = (BYTE)(var_G * 255);
	b = (BYTE)(var_B * 255);
}
//
//
//
//
//
//
//
//
//
////////////////////////////////////////////////////////
/******************************************************
				DIB와 관련된 전역 함수
******************************************************/

LPSTR WINAPI FindDIBBits(LPSTR lpbi)
{
	return (lpbi + *(LPDWORD)lpbi + ::PaletteSize(lpbi));
}


DWORD WINAPI DIBWidth(LPSTR lpDIB)
{
	LPBITMAPINFOHEADER lpbmi;  // pointer to a Win 3.0-style DIB
	LPBITMAPCOREHEADER lpbmc;  // pointer to an other-style DIB

	/* point to the header (whether Win 3.0 and old) */

	lpbmi = (LPBITMAPINFOHEADER)lpDIB;
	lpbmc = (LPBITMAPCOREHEADER)lpDIB;

	/* return the DIB width if it is a Win 3.0 DIB */
	if (IS_WIN30_DIB(lpDIB))
		return lpbmi->biWidth;
	else  /* it is an other-style DIB, so return its width */
		return (DWORD)lpbmc->bcWidth;
}


DWORD WINAPI DIBHeight(LPSTR lpDIB)
{
	LPBITMAPINFOHEADER lpbmi;  // pointer to a Win 3.0-style DIB
	LPBITMAPCOREHEADER lpbmc;  // pointer to an other-style DIB

	/* point to the header (whether old or Win 3.0 */

	lpbmi = (LPBITMAPINFOHEADER)lpDIB;
	lpbmc = (LPBITMAPCOREHEADER)lpDIB;

	/* return the DIB height if it is a Win 3.0 DIB */
	if (IS_WIN30_DIB(lpDIB))
		return lpbmi->biHeight;
	else  /* it is an other-style DIB, so return its height */
		return (DWORD)lpbmc->bcHeight;
}



WORD WINAPI PaletteSize(LPSTR lpbi)
{
   /* calculate the size required by the palette */
   if (IS_WIN30_DIB (lpbi))
	  return (WORD)(::DIBNumColors(lpbi) * sizeof(RGBQUAD));
   else
	  return (WORD)(::DIBNumColors(lpbi) * sizeof(RGBTRIPLE));
}



WORD WINAPI DIBNumColors(LPSTR lpbi)
{
	WORD wBitCount;  // DIB bit count

	/*  If this is a Windows-style DIB, the number of colors in the
	 *  color table can be less than the number of bits per pixel
	 *  allows for (i.e. lpbi->biClrUsed can be set to some value).
	 *  If this is the case, return the appropriate value.
	 */

	if (IS_WIN30_DIB(lpbi))
	{
		DWORD dwClrUsed;

		dwClrUsed = ((LPBITMAPINFOHEADER)lpbi)->biClrUsed;
		if (dwClrUsed != 0)
			return (WORD)dwClrUsed;
	}

	/*  Calculate the number of colors in the color table based on
	 *  the number of bits per pixel for the DIB.
	 */
	if (IS_WIN30_DIB(lpbi))
		wBitCount = ((LPBITMAPINFOHEADER)lpbi)->biBitCount;
	else
		wBitCount = ((LPBITMAPCOREHEADER)lpbi)->bcBitCount;

	/* return number of colors based on bits per pixel */
	switch (wBitCount)
	{
		case 1:
			return 2;

		case 4:
			return 16;

		case 8:
			return 256;

		default:
			return 0;
	}
}

/******************************************************
CBMPImage
******************************************************/
CBMPImage::CBMPImage()
{
	m_Size		 = CSize(1,1);
	m_hImage	 = NULL;
	m_pPal		 = NULL;
	m_hUndoImage = NULL;
}

CBMPImage::CBMPImage( CBMPImage &Src ) : CBasicImage(Src)
{
	*this = Src;
}

BOOL CBMPImage::LoadBMP(LPCTSTR lpszFileName)
{
	CFile file;
	CFileException fe;
	LPSTR pDIB;
	DWORD dwBitsSize;
	BITMAPFILEHEADER bmfHeader;

	// 읽기 모드로 파일 열기
	if(!file.Open(lpszFileName, CFile::modeRead|CFile::shareDenyWrite, &fe))
		return FALSE;

	// 파일의 길이를 구함
	dwBitsSize = file.GetLength();

	// 파일 헤더 읽기
	if(file.Read((LPSTR)&bmfHeader, sizeof(bmfHeader))!=sizeof(bmfHeader))
		return FALSE;

	// BMP 파일임을 나타내는 "BM" 마커가 있는지 확인
	if (bmfHeader.bfType != DIB_HEADER_MARKER)
		return FALSE;

	// 메모리 할당
	if((m_hImage = (HDIB)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, dwBitsSize)) == NULL) return FALSE;

	// 메모리 고정
	pDIB = (LPSTR) ::GlobalLock((HGLOBAL) m_hImage);

	// 파일 읽기
	if (file.Read(pDIB, dwBitsSize - sizeof(BITMAPFILEHEADER)) != dwBitsSize - sizeof(BITMAPFILEHEADER) ) 
	{
		::GlobalUnlock((HGLOBAL) m_hImage);
		::GlobalFree((HGLOBAL) m_hImage);
		return FALSE;
	}

	// 메모리 풀어줌
	::GlobalUnlock((HGLOBAL) m_hImage);

	// DIB 초기화
	InitDIB();

	return TRUE;
}

BOOL CBMPImage::SaveBMP(LPCTSTR lpszFileName)
{
	CFile file;
	CFileException fe;
	BITMAPFILEHEADER bmfHdr;
	LPBITMAPINFOHEADER lpBI;
	DWORD dwDIBSize;

	// 쓰기 모드로 파일 열기
	if (!file.Open(lpszFileName, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe)) return FALSE;

	// 메모리 핸들이 유효한지 확인
	if (m_hImage == NULL) return FALSE;

	// 메모리 고정
	lpBI = (LPBITMAPINFOHEADER)::GlobalLock((HGLOBAL)m_hImage);
	if (lpBI == NULL) return FALSE;

	// 비트맵 파일 헤더 정보를 설정
	bmfHdr.bfType = DIB_HEADER_MARKER;  // "BM"
	dwDIBSize = *(LPDWORD)lpBI + ::PaletteSize((LPSTR)lpBI);
	if((lpBI->biCompression==BI_RLE8) || (lpBI->biCompression==BI_RLE4))
		dwDIBSize += lpBI->biSizeImage;
	else 
	{
		DWORD dwBmBitsSize;  // Size of Bitmap Bits only
		dwBmBitsSize = WIDTHBYTES((lpBI->biWidth)*((DWORD)lpBI->biBitCount)) * lpBI->biHeight;
		dwDIBSize += dwBmBitsSize;
		lpBI->biSizeImage = dwBmBitsSize;
	}

	bmfHdr.bfSize = dwDIBSize + sizeof(BITMAPFILEHEADER);
	bmfHdr.bfReserved1 = 0;
	bmfHdr.bfReserved2 = 0;
	bmfHdr.bfOffBits=(DWORD)sizeof(BITMAPFILEHEADER)+lpBI->biSize + PaletteSize((LPSTR)lpBI);
	TRY
	{
		// 비트맵 파일 헤더를 파일에 쓰기
		file.Write((LPSTR)&bmfHdr, sizeof(BITMAPFILEHEADER));
		// 나머지 데이터를 파일에 쓰기
		file.Write(lpBI, dwDIBSize);
	}
	CATCH (CFileException, e)
	{
		::GlobalUnlock((HGLOBAL) m_hImage);
		THROW_LAST();
	}
	END_CATCH

	// 메모리 풀어줌
	::GlobalUnlock((HGLOBAL) m_hImage);
	return TRUE;
}


/******************************************************
CBMPPixelPtr
******************************************************/

CBMPPixelPtr::CBMPPixelPtr(CBMPImage &Im)
{
	
	m_nHeight = Im.GetHeight();		// 이미지의 가로 길이
	int nWidth = Im.GetRealWidth();	// 이미지의 세로 길이
	
	// 이미지의 세로 길이 만큼 포인터 배열 할당
	m_pPtr = new BYTE *[m_nHeight]; 
	
	// 메모리 블록을 고정
	m_hHandle = Im.GetHandle();
	LPSTR lpDIBHdr  = (LPSTR) ::GlobalLock((HGLOBAL) m_hHandle);
	
	// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
	BYTE *lpDIBBits = (BYTE *) ::FindDIBBits(lpDIBHdr);
	
	// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
	// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
	for(int i=m_nHeight -1 ; i>=0 ; i--)
	{
		m_pPtr[i] = lpDIBBits;
		lpDIBBits += nWidth;
	}
}

void CBMPPixelPtr::SetImage(CBMPImage *Im)
{
	
	m_nHeight = Im->GetHeight();		// 이미지의 가로 길이
	int nWidth = Im->GetRealWidth();	// 이미지의 세로 길이
	
	// 이미지의 세로 길이 만큼 포인터 배열 할당
	m_pPtr = new BYTE *[m_nHeight]; 
	
	// 메모리 블록을 고정
	m_hHandle = Im->GetHandle();
	LPSTR lpDIBHdr  = (LPSTR) ::GlobalLock((HGLOBAL) m_hHandle);
	
	// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
	BYTE *lpDIBBits = (BYTE *) ::FindDIBBits(lpDIBHdr);
	
	// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
	// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
	for(int i=m_nHeight -1 ; i>=0 ; i--)
	{
		m_pPtr[i] = lpDIBBits;
		lpDIBBits += nWidth;
	}
};

CBMPPixelPtr::~CBMPPixelPtr()
{
	if(m_pPtr != NULL)
	{
		// 메모리 블록을 풀어줌
		GlobalUnlock((HGLOBAL) m_hHandle);
		
		TRY
		{
			// 할당 받았던 포인터 배열을 해제
			delete [] m_pPtr;
		}
		CATCH (CMemoryException, e)
		{
			THROW_LAST();
		}
		END_CATCH
	}
}

/******************************************************
CBMPColorPixelPtr
******************************************************/

CBMPColorPixelPtr::CBMPColorPixelPtr(CBMPImage &Im)
{
	m_nHeight = Im.GetHeight();		// 이미지의 가로 길이
	int nWidth = Im.GetRealWidth();	// 이미지의 세로 길이
	
	// 이미지의 세로 길이 만큼 포인터 배열 할당
	m_pPtr = new LPCOLOR [m_nHeight];
	
	// 메모리 블록을 고정
	m_hHandle = Im.GetHandle();
	LPSTR lpDIBHdr  = (LPSTR)::GlobalLock((HGLOBAL) m_hHandle);
	
	// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
	LPSTR lpDIBBits = (LPSTR)::FindDIBBits(lpDIBHdr);
	
	// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
	// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
	for(int i=m_nHeight-1 ; i>=0 ; i--)
	{
		m_pPtr[i] = (LPCOLOR)lpDIBBits;
		lpDIBBits += nWidth;
	}
};

void CBMPColorPixelPtr::SetImage(CBMPImage *Im)
{
	m_nHeight = Im->GetHeight();		// 이미지의 가로 길이
	int nWidth = Im->GetRealWidth();	// 이미지의 세로 길이
	
	// 이미지의 세로 길이 만큼 포인터 배열 할당
	m_pPtr = new LPCOLOR [m_nHeight];
	
	// 메모리 블록을 고정
	m_hHandle = Im->GetHandle();
	LPSTR lpDIBHdr  = (LPSTR)::GlobalLock((HGLOBAL) m_hHandle);
	
	// 헤더 다음에 나오는 첫번째 픽셀의 포인터를 얻음
	LPSTR lpDIBBits = (LPSTR)::FindDIBBits(lpDIBHdr);
	
	// 가로 길이 만큼씩 진행하며 m_pPtr에 이미지의 
	// 각 줄의 첫번째 픽셀의 주소를 뒤집어서 저장
	for(int i=m_nHeight-1 ; i>=0 ; i--)
	{
		m_pPtr[i] = (LPCOLOR)lpDIBBits;
		lpDIBBits += nWidth;
	}
};

CBMPColorPixelPtr::~CBMPColorPixelPtr()
{ 
	if(m_pPtr != NULL)
	{
		// 메모리 블록을 풀어줌
		GlobalUnlock((HGLOBAL) m_hHandle);
		TRY
		{
			// 할당 받았던 포인터 배열을 해제
			delete [] m_pPtr;
		}
		CATCH (CMemoryException, e)
		{
			THROW_LAST();
		}
		END_CATCH
	}
};

/******************************************************
				클립보드를 위한 전역 함수
******************************************************/

HGLOBAL WINAPI CopyHandle (HGLOBAL h)
{
	if (h == NULL)
		return NULL;

	DWORD dwLen = ::GlobalSize((HGLOBAL) h);
	HGLOBAL hCopy = ::GlobalAlloc(GHND, dwLen);

	if (hCopy != NULL)
	{
		void* lpCopy = ::GlobalLock((HGLOBAL) hCopy);
		void* lp     = ::GlobalLock((HGLOBAL) h);
		memcpy(lpCopy, lp, dwLen);
		::GlobalUnlock(hCopy);
		::GlobalUnlock(h);
	}

	return hCopy;
}
