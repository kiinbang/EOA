#pragma once

#pragma warning(disable: 4996)

#include <vector>

#define DATA_OFFSET_OFFSET 0x000A
#define WIDTH_OFFSET 0x0012
#define HEIGHT_OFFSET 0x0016
#define BITS_PER_PIXEL_OFFSET 0x001C
#define HEADER_SIZE 14
#define INFO_HEADER_SIZE 40
#define NO_COMPRESION 0
#define MAX_NUMBER_OF_COLORS 0
#define ALL_COLORS_REQUIRED 0

namespace tinybmp
{
    typedef unsigned int int32;
    typedef short int16;
    typedef unsigned char byte;

    struct BmpInfo
    {
        int32 w;
        int32 h;
        int32 nBands;/// bytes per pixel
        std::vector<byte> pixels;
        const int32 depthInBits = 8;/// Note: Pixel depth is 1-byte;

        void setPixel(const int32 r, const int32 c, const std::vector<byte>& pix)
        {
            if (pix.size() != nBands)
                return;
            else
            {
                const auto pos0 = (r * this->w + c) * nBands;
                for (int32 i = 0; i < pix.size(); ++i)
                {
                    this->pixels[pos0 + i] = pix[0];
                }
            }
        }

        std::vector<byte> getPixel(const int32 r, const int32 c)
        {
            std::vector<byte> pix(nBands);
            auto pos0 = (r * this->w + c) * nBands;
            for (int32 i = 0; i < this->nBands; ++i)
            {
                pix[0] = this->pixels[pos0 + i];
            }

            return pix;
        }
    };

    void readBmp(const char* fileName, BmpInfo& bmpInfo)
    {
        FILE* imageFile = fopen(fileName, "rb");
        int32 dataOffset;
        fseek(imageFile, DATA_OFFSET_OFFSET, SEEK_SET);
        fread(&dataOffset, 4, 1, imageFile);
        fseek(imageFile, WIDTH_OFFSET, SEEK_SET);
        fread(&bmpInfo.w, 4, 1, imageFile);
        fseek(imageFile, HEIGHT_OFFSET, SEEK_SET);
        fread(&bmpInfo.h, 4, 1, imageFile);
        int16 bitsPerPixel;
        fseek(imageFile, BITS_PER_PIXEL_OFFSET, SEEK_SET);
        fread(&bitsPerPixel, 2, 1, imageFile);
        bmpInfo.nBands = ((int32)bitsPerPixel) / bmpInfo.depthInBits;

        int paddedRowSize = (int)(4 * ceil((float)(bmpInfo.w) / 4.0f)) * (bmpInfo.nBands);
        int unpaddedRowSize = (bmpInfo.w) * (bmpInfo.nBands);
        int totalSize = unpaddedRowSize * (bmpInfo.h);
        bmpInfo.pixels.resize(totalSize);
        byte* pixels = bmpInfo.pixels.data();
        byte* currentRowPointer = pixels + ((bmpInfo.h - 1) * unpaddedRowSize);
        for (int32 i = 0; i < bmpInfo.h; i++)
        {
            fseek(imageFile, dataOffset + (i * paddedRowSize), SEEK_SET);
            fread(currentRowPointer, 1, unpaddedRowSize, imageFile);
            currentRowPointer -= unpaddedRowSize;
        }

        fclose(imageFile);
    }

    void writeBmp(const char* fileName, const BmpInfo& bmpInfo)
    {
        FILE* outputFile = fopen(fileName, "wb");
        //*****HEADER************//
        const char* BM = "BM";
        fwrite(&BM[0], 1, 1, outputFile);
        fwrite(&BM[1], 1, 1, outputFile);
        int paddedRowSize = (int)(4 * ceil((float)bmpInfo.w / 4.0f)) * bmpInfo.nBands;
        int32 fileSize = paddedRowSize * bmpInfo.h + HEADER_SIZE + INFO_HEADER_SIZE;
        fwrite(&fileSize, 4, 1, outputFile);
        int32 reserved = 0x0000;
        fwrite(&reserved, 4, 1, outputFile);
        int32 dataOffset = HEADER_SIZE + INFO_HEADER_SIZE;
        fwrite(&dataOffset, 4, 1, outputFile);

        //*******INFO*HEADER******//
        int32 infoHeaderSize = INFO_HEADER_SIZE;
        fwrite(&infoHeaderSize, 4, 1, outputFile);
        fwrite(&bmpInfo.w, 4, 1, outputFile);
        fwrite(&bmpInfo.h, 4, 1, outputFile);
        int16 planes = 1; //always 1
        fwrite(&planes, 2, 1, outputFile);
        int16 bitsPerPixel = bmpInfo.nBands * bmpInfo.depthInBits;
        fwrite(&bitsPerPixel, 2, 1, outputFile);
        //write compression
        int32 compression = NO_COMPRESION;
        fwrite(&compression, 4, 1, outputFile);
        //write image size(in bytes)
        int32 imageSize = bmpInfo.w * bmpInfo.h * bmpInfo.nBands;
        fwrite(&imageSize, 4, 1, outputFile);
        int32 resolutionX = 11811; //300 dpi
        int32 resolutionY = 11811; //300 dpi
        fwrite(&resolutionX, 4, 1, outputFile);
        fwrite(&resolutionY, 4, 1, outputFile);
        int32 colorsUsed = MAX_NUMBER_OF_COLORS;
        fwrite(&colorsUsed, 4, 1, outputFile);
        int32 importantColors = ALL_COLORS_REQUIRED;
        fwrite(&importantColors, 4, 1, outputFile);
        int unpaddedRowSize = bmpInfo.w * bmpInfo.nBands;
        for (int32 i = 0; i < bmpInfo.h; i++)
        {
            int pixelOffset = ((bmpInfo.h - i) - 1) * unpaddedRowSize;
            fwrite(&bmpInfo.pixels[pixelOffset], 1, paddedRowSize, outputFile);
        }
        fclose(outputFile);
    }
}