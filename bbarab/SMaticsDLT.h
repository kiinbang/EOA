/*
 * Copyright (c) 2001-2001, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/////////////////////////////////////////////////////////////////////
//DLT.h
//made by BangBaraBang
//revision: 2001-09-17
///////////////////////////////////////////////////////////////////////
//Pushboom Satellite DLT Sensor Model
//(Sat)DLT delived from collinearity condition equations
/////////////////////////////////////////////////////////////////////////
//Level 1 Sensor Model
//Level 1 is preserved CCD array order.
/////////////////////////////////////////////////////////////////////////
//Level 2(2D transformed image) Sensor Model
//Level 2 is rearranged CCD array.
//This program assumed that rearrange function is affine transformation
//////////////////////////////////////////////////////////////////////////
//revision: 2001-12-04
///////////////////////////////////////////////////////////////////////
//GCP Management update
/////////////////////////////////////////////////////////////////////////

#if !defined(__SM_DLT_BBARAB__)
#define __SM_DLT_BBARAB__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <SMMatrix.h>
using namespace SMATICS_BBARAB;

#include "SMCoordinateclass.h"
#include "GCPManage.h"





/////////////////////////////////////////////////////////////////////////////
// DLTParam
//(DLT parameters)
/////////////////////////////////////////////////////////////////////////////
struct DLTParam
{
public:
	double A1, B1, C1, D1;
	double A2, B2, C2, D2;
	double A3, B3, C3;

	double A4, B4, C4;
	double A5, B5, C5;
	double alpha, beta;
	double alpha2, beta2;
	DLTParam() {A4 = B4 = C4 = A5 = B5 = C5 = alpha = beta = 0.0;}
};

/////////////////////////////////////////////////////////////////////////////
// CDLT
// DLT sensor model class
/////////////////////////////////////////////////////////////////////////////
class CDLT
{
// Construction
public:
	CDLT();
	virtual ~CDLT();

//Attribute
protected:
	//mumber of GCP
	long num_GCP;
	
	//GCP image coordinate
	double *ImageCoord;
	double *ImageCoord_Old; //bakcup coord
	
	//GCP Ground Coordinate
	double *GCPCoord;
	double *GCPCoord_Old; //backup coord
	
	//coordinates normalization check
	bool enable_normalization;
	
	//iteration number at finish-time
	int end_iter;


	///////////////////////////
	//Coordinate Normalization
	///////////////////////////
	//Image Coordinate Scale
	double Scale_I_X, Scale_I_Y;
	//Image Cordinate Offset
	double Offset_I_X, Offset_I_Y;
	//Ground Coordinate Scale
	double Scale_G_X, Scale_G_Y, Scale_G_Z;
	//Ground Coordinate Offset
	double Offset_G_X, Offset_G_Y, Offset_G_Z;

	////////////////////////////////////////////////////////////
	//Frame image DLT 11 parameters (11)
	//[nonlinear]
	//c(column) = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)
	//r(row) = (A2X + B2Y + C2Z + D2)/(A3X + B3Y + C3Z + 1)
	////////////////////////////////////////////////////////////
	//[linear]
	//c(column) = (A1X + B1Y + C1Z + D1) - (A3cX + B3cY + C3cZ)
	//r(row) = (A2X + B2Y + C2Z + D2) - (A3rX + B3rY + C3rZ)
	////////////////////////////////////////////////////////////
	DLTParam param;
	///////////////////////
	//Calculation matrix
	///////////////////////
	CSMMatrix<double> A, X, L, V;
	CSMMatrix<double> var_La, var_X;
	double posteriori_var, SD;
	double *correlation_coefficient;
	long DF;

//Member function
public:
	void InitDLTModule(void);
	void InsertGCP(double col, double row, double X, double Y, double Z, long ID);
	bool SetGCPCoord(unsigned short ID, double col, double row, double X, double Y, double Z);
	bool GetGCPCoord(unsigned short ID, double *col, double *row, double *X, double *Y, double *Z);
	void Solve(bool nonlinear=true, bool coordnormal=true, unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image(double X, double Y, double Z);
	CSMMatrix<double> Getvar_X(void) {return var_X;}
	CSMMatrix<double> Getvar_La(void) {return var_La;}
	double Getposteriori_var(void) {return posteriori_var;}
	double GetSD(void) {return SD;}
	virtual double *Getcorrelation_coefficient(void);
	int GetEndIteration(void) {return end_iter;}
	DLTParam GetDLTParam(void);

//Member variable
private:
	CGCPManage gcp;
	CICPManage icp;

//Member function
private:
	void SetGCP();
	void SetCoordNormalize(bool value);
	void GCPNormalization(void);
	void Solve_linear();
	void Solve_nonlinear(unsigned int maxiteration, double maxcorrection);
};





/////////////////////////////////////////////////////////////////////////////
// CSatDLT
/////////////////////////////////////////////////////////////////////////////
class CSatDLT:public CDLT
{
//Attribute
public:
	////////////////////////////////////////////////////////////
	//satellite DLT parameters (11)
	//Level 1
	//[nonlinear]
	//c(column) = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)
	//l(scan line) = (A2X + B2Y + C2Z + D2)
	////////////////////////////////////////////////////////////
	//[linear]
	//c(column) = (A1X + B1Y + C1Z + D1) - (A3cX + B3cY + C3cZ)
	//l(scan line) = (A2X + B2Y + C2Z + D2)
	////////////////////////////////////////////////////////////

//Member function	
public:
	CSatDLT();
	~CSatDLT();
	void Solve(bool nonlinear=true, unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image(double X, double Y, double Z);
	double *Getcorrelation_coefficient(void);
//Memberfunction
protected:
	void Solve_linear();
	void Solve_nonlinear(unsigned int maxiteration, double maxcorrection);
};

/////////////////////////////////////////////////////////////////////////////
// CSatDLT for Level 2 image
/////////////////////////////////////////////////////////////////////////////
class CSatDLT_Level2:public CSatDLT
{
//Attribute
public:
	//////////////////////////////////////////////////////////////////////////////////
	//satellite DLT parameters (17)
	//Level 2
	//[nonlinear]
	//c(column) = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + A4X + B4Y + C4Z
	//l(scan line) = (A2X + B2Y + C2Z + D2)/(A3X + B3Y + C3Z + 1) + A5X + B5Y + C5Z
	//////////////////////////////////////////////////////////////////////////////////
	

//Member function	
public:
	CSatDLT_Level2();
	~CSatDLT_Level2();
	
	double *Getcorrelation_coefficient(void);
	double *Getcorrelation_coefficient2(void);
	double *Getcorrelation_coefficient3(void);
	
	void Solve(unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image(double X, double Y, double Z);
	
	//Affine SatDLT
	void Solve2(unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image2(double X, double Y, double Z);

	void Solve3(unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image3(double X, double Y, double Z);

	void Solve4(unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image4(double X, double Y, double Z);

	void Solve5(unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image5(double X, double Y, double Z);

	//Polynomial I
	void Solve6(unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image6(double X, double Y, double Z);

	//Polynomial II
	void Solve7(unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image7(double X, double Y, double Z);

	//Okamoto's Extended DLT
	void Solve8(unsigned int maxiteration = 10, double maxcorrection = 0.000001);
	Point2D<double> GetGround2Image8(double X, double Y, double Z);

//Memberfunction
protected:
};





/////////////////////////////////////////////////////////////////////////////
#endif // !defined(__SM_DLT_BBARAB__)
