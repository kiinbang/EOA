// FrameIntersection.h
//////////////////////////////////////////////////////////////////////
//Description:
//stereo frame image intersection
//Made By Spacematics Lab(wcho).
//Bang Ki-In
//////////////////////////////////////////////////////////////////////
//revision date: 2001/09/10
//////////////////////////////////////////////////////////////////////
//revision date: 2001/09/10
//초기값 결정 방법 변경
//시차방정식을 사용하지 않음...
//////////////////////////////////////////////////////////////////////
//revision date: 2002/07/15
//초기값 결정 방법 변경(DLT 를 사용)
//주점의 위치를 세팅하는 함수추가
//입력된 주점을 사용하도록 코드 수정
//////////////////////////////////////////////////////////////////////


#ifndef __FRAMEINTERSECTION_H_
#define __FRAMEINTERSECTION_H_

#include "SMCoordinateClass.h"
#include "matrix.h"
/////////////////////////////////////////////////////////////////////////////
// CFrameIntersection
class CFrameIntersection
{
public:
	CFrameIntersection()
	{
		LPPA.x = 0.0;
		LPPA.y = 0.0;

		RPPA.x = 0.0;
		RPPA.y = 0.0;
	}

// CFrameIntersection Member Variable
private:
	double Lomega, Lphi, Lkappa;
	double Romega, Rphi, Rkappa;
	Point3D<double> LPC, RPC;
	double B, H;
	double focal_length_left, focal_length_right;
	Point2D<double> LPPA, RPPA;

	Point3D<double> GP; //Ground Point
	
	double b14,b15,b16;
	double b24,b25,b26;
	double J,K;

	Matrix<double> A, L, V, X;

	double maxcorrection;
	unsigned char maxiteration;


// CFrameIntersection Member Function
private:
	void Make_b_J_K_Left(Point2D<double> La);
	void Make_b_J_K_Right(Point2D<double> Ra);
	void Make_A_L(Point2D<double> La, Point2D<double> Ra);
	Point3D<double> init(Point2D<double> Left);
	Point3D<double> initByDLT(Point2D<double> La, Point2D<double> Ra);
	
// IFrameIntersection
public:
	void SetEOParameter(double L_omega, double L_phi, double L_kappa, Point3D<double> lpc,
		                 double R_omega, double R_phi, double R_kappa, Point3D<double> rpc, 
						 double LFL, double RFL, double maxcor = 1.0e-6, unsigned char maxiter = 10);
	
	Point3D<double> RunIntersection(Point2D<double> La, Point2D<double> Ra);
	Point3D<double> RunIntersection(double La_x, double La_y, double Ra_x, double Ra_y);

	void SetPPA(Point2D<double> LeftPPA, Point2D<double>RightPPA) {LPPA = LeftPPA; RPPA = RightPPA;}
	void SetPPA(double LPPA_x, double LPPA_y, double RPPA_x, double RPPA_y)
	{LPPA.x = LPPA_x; LPPA.y = LPPA_y; RPPA.x = RPPA_x; RPPA.y = RPPA_y;}

};

#endif //__FRAMEINTERSECTION_H_
