/////////////////////////////////////////////////////////
//SpaceResection.h
//made by bbarab
/////////////////////////////////////////////////////////
//revision: 2000-08-02
//항공사진과 pushbroom 위성영상의 공간후방교회(공선조건식)
/////////////////////////////////////////////////////////
//revision: 2001-06-25
//pushbroom위성영상의 공간후방교회를 제거
//항공사진 공간후방교회만으로 구성
//////////////////////////////////////////////////////////
//인하대학교 토목공학과 지형정보연구실
//항공사진의 공간후방교회법
///////////////////////////////////////////////////////////////
#if !defined(__SPACEMATICS_SPACE_RESECTION__)
#define __SPACEMATICS_SPACE_RESECTION__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Collinearity.h"

class CSpaceResection
{
public:

	CSpaceResection();
	CSpaceResection(char *fname);
	virtual ~CSpaceResection();
	Point2D<double> PPA;//Principal Point Autocollimation
	Point2D<double> PPBS;//Principal Point Best Symmetry

	double f;//focal length
	double FH;//flying height
	int *ID;//Point ID
	Point2D<double> *a;//image point
	Point3D<double> *GCP;//Ground Control Point
	int num_Check;//number of check point
	int num_GCP;//number of GCP

	//편미분 계수
	double b11, b12, b13, b14, b15, b16;
	double b21, b22, b23, b24, b25, b26;
	double J, K;
	//외부표정요소
	Point3D<double> PC;//Perspective Center
	double Omega, Phi, Kappa;
	//Matrix
	Matrix<double> A;//design matrix
	Matrix<double> AT;
	Matrix<double> N;//normal matrix
	Matrix<double> Ninv;
	Matrix<double> L;
	Matrix<double> V;
	Matrix<double> X;
	Matrix<double> VTV;
	Matrix<double> Var;
	Matrix<double> Covar;
	
	//Statistics
	double DF;//degree of freedom
	double SD;//standard deviation
	double Posteriori_Var;//posteriori variance

	int maxiteration;
	double maxcorrection;
	CString result;

	//Member Function
	BOOL init(void);
	BOOL make_A_L(void);
	BOOL make_b_J_K(Point3D<double> PP, Point3D<double> GP, Point2D<double> p);
	BOOL solve(void);

};

#endif // !defined(__SPACEMATICS_SPACE_RESECTION__)
