//////////////////////////
//SpacematicsCamera.cpp
//made by BbaraB
//revision date 2001-03-09
//////////////////////////
#include "stdafx.h"
#include "SpacematicsCamera.h"

CCamera::CCamera()
{
	fiducial = NULL;
	num_fiducial = 0;
	ID = -1;
	Name = "";
	PPA.x = 0.0;
	PPA.y = 0.0;
	PPBS.x = 0.0;
	PPBS.y = 0.0;
	k1 = k2 = k3 = k4 = 0.0;
	LensDistortionEnable = false;
	SMACEnable = false;
}

CCamera::CCamera(CCamera& copy)
{
	f = copy.f;
	ID = copy.ID;
	Name = copy.Name;
	num_fiducial = copy.num_fiducial;
	fiducial = NULL;
	fiducial = new Point2D<double> [num_fiducial];
	for(int i=0;i<num_fiducial;i++)
		fiducial[i] = copy.fiducial[i];
	PPA = copy.PPA;
	PPBS = copy.PPBS;
	k1 = copy.k1;
	k2 = copy.k2;
	k3 = copy.k3;
	k4 = copy.k4;
	LensDistortionEnable = copy.LensDistortionEnable;
	SMACEnable = false;
}

void CCamera::operator = (CCamera& copy)
{
	f = copy.f;
	ID = copy.ID;
	Name = copy.Name;
	num_fiducial = copy.num_fiducial;
	if(fiducial!=NULL)
	{
		delete[] fiducial;
		fiducial = NULL;
	}
	fiducial = new Point2D<double> [num_fiducial];
	for(int i=0;i<num_fiducial;i++)
		fiducial[i] = copy.fiducial[i];
	PPA = copy.PPA;
	PPBS = copy.PPBS;
	k1 = copy.k1;
	k2 = copy.k2;
	k3 = copy.k3;
	k4 = copy.k4;
	LensDistortionEnable = copy.LensDistortionEnable;
	SMACEnable = copy.SMACEnable;
}

CCamera::~CCamera()
{
	if(fiducial!=NULL)
	{
		delete[] fiducial;
		fiducial = NULL;
	}
}

bool CCamera::ReadCameraFile(CString fname)
{
	char title[128];
	
	fstream camfile;
	camfile.open(fname, ios::in);

	if(camfile.eof())
		return false;
	camfile>>title>>f;
	if(camfile.eof())
		return false;
	camfile>>title>>PPA.x>>PPA.y;
	if(camfile.eof())
		return false;
	camfile>>title>>num_fiducial;
	
	SetNumFM(num_fiducial);
	for(int i=0; i<num_fiducial; i++)
	{
		if(camfile.eof())
		return false;
		camfile>>fiducial[i].x>>fiducial[i].y;
	}

	camfile>>title;
	
	if(camfile.eof())
		return false;
	
	camfile>>smac.k0>>smac.k1>>smac.k2>>smac.k3>>smac.k4;
	
	if(camfile.eof())
		return false;
	
	camfile>>smac.p1>>smac.p2>>smac.p3>>smac.p4;
	
	return true;
}