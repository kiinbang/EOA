// FrameIntersection.cpp : Implementation of CFrameIntersection
#include "stdafx.h"
#include "SMFrameIntersection.h"
#include "SMFrameIntersection.h"

#include "Collinearity.h"

/////////////////////////////////////////////////////////////////////////////
// CFrameIntersection
Point3D<double> CFrameIntersection::init(Point2D<double> Left)
{

	CRotationcoeff LR(Lomega,Lphi,Lkappa);
	Point3D<double> G;
	
	Matrix<double> A(2,2,0.0),X(2,1,0.0),C(2,1,0.0);
	C(0,0) = -focal_length_left*(LR.Rmatrix(0,0)*(-LPC.x) + LR.Rmatrix(0,1)*(-LPC.y) + LR.Rmatrix(0,2)*(-LPC.z))
			 -Left.x*(LR.Rmatrix(2,0)*(-LPC.x) + LR.Rmatrix(2,1)*(-LPC.y) + LR.Rmatrix(2,2)*(-LPC.z));
	C(1,0) = -focal_length_left*(LR.Rmatrix(1,0)*(-LPC.x) + LR.Rmatrix(1,1)*(-LPC.y) + LR.Rmatrix(1,2)*(-LPC.z))
			 -Left.y*(LR.Rmatrix(2,0)*(-LPC.x) + LR.Rmatrix(2,1)*(-LPC.y) + LR.Rmatrix(2,2)*(-LPC.z));

	A(0,0) =(Left.x*LR.Rmatrix(2,0)+focal_length_left*LR.Rmatrix(0,0));
	A(0,1) =(Left.x*LR.Rmatrix(2,1)+focal_length_left*LR.Rmatrix(0,1));
	A(1,0) =(Left.y*LR.Rmatrix(2,0)+focal_length_left*LR.Rmatrix(1,0));
	A(1,1) =(Left.y*LR.Rmatrix(2,0)+focal_length_left*LR.Rmatrix(1,1));

	X = A.Inverse()%C;

	G.x = X(0);
	G.y = X(1);
	G.z = 0.0;

	return G;
}

Point3D<double> CFrameIntersection::initByDLT(Point2D<double> La, Point2D<double> Ra)
{
	Point3D<double> G;

	Matrix<double> A(4,3,0.0);
	Matrix<double> L(4,1,0.0);
	Matrix<double> V, X;
	
	double LNUM_x[4], LNUM_y[4], LDEN[3];
	double RNUM_x[4], RNUM_y[4], RDEN[3];

	CRotationcoeff LeftR(Lomega,Lphi,Lkappa);
	Matrix<double> LR = LeftR.Rmatrix;
	CRotationcoeff RightR(Romega,Rphi,Rkappa);
	Matrix<double> RR = RightR.Rmatrix;

	double Ltemp, Rtemp;
	double temp;
	temp = LR(2,0)*LPC.x + LR(2,1)*LPC.y + LR(2,2)*LPC.z;
	if(temp ==0.0)
	{
		temp = 1.0e-99;
	}
	Ltemp = 1/-temp;
	
	temp = RR(2,0)*RPC.x + RR(2,1)*RPC.y + RR(2,2)*RPC.z;
	if(temp == 0.0)
	{
		temp = 1.0e-99;
	}
	Rtemp = 1/-temp;

	LNUM_x[0] = Ltemp*(LR(2,0)*LPPA.x-LR(0,0)*focal_length_left);
	LNUM_x[1] = Ltemp*(LR(2,1)*LPPA.x-LR(0,1)*focal_length_left);
	LNUM_x[2] = Ltemp*(LR(2,2)*LPPA.x-LR(0,2)*focal_length_left);
	LNUM_x[3] = LPPA.x + Ltemp*focal_length_left*(LR(0,0)*LPC.x+LR(0,1)*LPC.y+LR(0,2)*LPC.z);

	LNUM_y[0] = Ltemp*(LR(2,0)*LPPA.y-LR(1,0)*focal_length_left);
	LNUM_y[1] = Ltemp*(LR(2,1)*LPPA.y-LR(1,1)*focal_length_left);
	LNUM_y[2] = Ltemp*(LR(2,2)*LPPA.y-LR(1,2)*focal_length_left);
	LNUM_y[3] = LPPA.y + Ltemp*focal_length_left*(LR(1,0)*LPC.x+LR(1,1)*LPC.y+LR(1,2)*LPC.z);

	LDEN[0] = Ltemp*LR(2,0);
	LDEN[1] = Ltemp*LR(2,1);
	LDEN[2] = Ltemp*LR(2,2);

	RNUM_x[0] = Rtemp*(RR(2,0)*RPPA.x-RR(0,0)*focal_length_right);
	RNUM_x[1] = Rtemp*(RR(2,1)*RPPA.x-RR(0,1)*focal_length_right);
	RNUM_x[2] = Rtemp*(RR(2,2)*RPPA.x-RR(0,2)*focal_length_right);
	RNUM_x[3] = RPPA.x + Rtemp*focal_length_right*(RR(0,0)*RPC.x+RR(0,1)*RPC.y+RR(0,2)*RPC.z);

	RNUM_y[0] = Rtemp*(RR(2,0)*RPPA.y-RR(1,0)*focal_length_right);
	RNUM_y[1] = Rtemp*(RR(2,1)*RPPA.y-RR(1,1)*focal_length_right);
	RNUM_y[2] = Rtemp*(RR(2,2)*RPPA.y-RR(1,2)*focal_length_right);
	RNUM_y[3] = RPPA.y + Rtemp*focal_length_right*(RR(1,0)*RPC.x+RR(1,1)*RPC.y+RR(1,2)*RPC.z);

	RDEN[0] = Rtemp*RR(2,0);
	RDEN[1] = Rtemp*RR(2,1);
	RDEN[2] = Rtemp*RR(2,2);

	A(0,0) = LNUM_x[0] - La.x*LDEN[0];
	A(0,1) = LNUM_x[1] - La.x*LDEN[1];
	A(0,2) = LNUM_x[2] - La.x*LDEN[2];

	L(0,0) = La.x - LNUM_x[3];

	A(1,0) = LNUM_y[0] - La.y*LDEN[0];
	A(1,1) = LNUM_y[1] - La.y*LDEN[1];
	A(1,2) = LNUM_y[2] - La.y*LDEN[2];

	L(1,0) = La.y - LNUM_y[3];

	A(2,0) = RNUM_x[0] - Ra.x*RDEN[0];
	A(2,1) = RNUM_x[1] - Ra.x*RDEN[1];
	A(2,2) = RNUM_x[2] - Ra.x*RDEN[2];

	L(2,0) = Ra.x - RNUM_x[3];

	A(3,0) = RNUM_y[0] - Ra.y*RDEN[0];
	A(3,1) = RNUM_y[1] - Ra.y*RDEN[1];
	A(3,2) = RNUM_y[2] - Ra.y*RDEN[2];

	L(3,0) = Ra.y - RNUM_y[3];
	
	X = (A.Transpose()%A).Inverse()%A.Transpose()%L;

	G.x = X(0,0);
	G.y = X(1,0);
	G.z = X(2,0);

	return G;
}

Point3D<double> CFrameIntersection::RunIntersection(double La_x, double La_y, double Ra_x, double Ra_y)
{
	Point2D<double> La(La_x,La_y);
	Point2D<double> Ra(Ra_x,Ra_y);
	
	Point3D<double> ans;

	//Init Approximation
	Point3D<double>gp;

	//gp = ParallaxEquation(H,B,focal_length_left, focal_length_right,La,Ra,3);
	
	//gp = init(La);
	gp = initByDLT(La, Ra);
	
	GP.x = gp.x;
	GP.y = gp.y;
	GP.z = gp.z;
	
	unsigned char iteration = 0;
	bool stop = true;
	
	do
	{
		iteration ++;
			
		Make_A_L(La, Ra);
		
		Matrix<double> AT, N, Ninv;
		AT = A.Transpose();
		N = AT%A;
		Ninv = N.Inverse();
		X = Ninv%AT%L;

		GP.x += X(0,0);
		GP.y += X(1,0);
		GP.z += X(2,0);
		
		double max = fabs(X(0));
		if(fabs(max)<fabs(X(1)))
		{	max = fabs(X(1));	}
		if(fabs(max)<fabs(X(2)))
		{	max = fabs(X(2));	}
		
		//iteration condition
		if(fabs(max)<fabs(maxcorrection))
			stop = false;
		
		if(iteration == maxiteration)
		{
			stop = false;
			::MessageBox(NULL,"execss iteration num!","",NULL);
		}

	}while(stop);
	
	ans.x = GP.x;
	ans.y = GP.y;
	ans.z = GP.z;
	
	return ans;
}

Point3D<double> CFrameIntersection::RunIntersection(Point2D<double> La, Point2D<double> Ra)
{
	// TODO: Add your implementation code here

	Point3D<double> ans;

	//Init Approximation
	Point3D<double>gp;

	//gp = ParallaxEquation(H,B,focal_length_left, focal_length_right,La,Ra,3);
	
	//gp = init(La);
	gp = initByDLT(La, Ra);
	
	GP.x = gp.x;
	GP.y = gp.y;
	GP.z = gp.z;
	
	unsigned char iteration = 0;
	bool stop = true;
	
	do
	{
		iteration ++;
			
		Make_A_L(La, Ra);
		
		Matrix<double> AT, N, Ninv;
		AT = A.Transpose();
		N = AT%A;
		Ninv = N.Inverse();
		X = Ninv%AT%L;

		GP.x += X(0,0);
		GP.y += X(1,0);
		GP.z += X(2,0);
		
		double max = fabs(X(0));
		if(fabs(max)<fabs(X(1)))
		{	max = fabs(X(1));	}
		if(fabs(max)<fabs(X(2)))
		{	max = fabs(X(2));	}
		
		//iteration condition
		if(fabs(max)<fabs(maxcorrection))
			stop = false;
		
		if(iteration == maxiteration)
		{
			stop = false;
			::MessageBox(NULL,"execss iteration num!","",NULL);
		}

	}while(stop);
	
	ans.x = GP.x;
	ans.y = GP.y;
	ans.z = GP.z;
	
	return ans;
}

void CFrameIntersection::SetEOParameter(double L_omega, double L_phi, double L_kappa, Point3D<double> lpc,
										double R_omega, double R_phi, double R_kappa, Point3D<double> rpc, 
										double LFL, double RFL, double maxcor, unsigned char maxiter)
{
	// TODO: Add your implementation code here
	Lomega = L_omega;
	Lphi = L_phi;
	Lkappa = L_kappa;
	
	LPC.x = lpc.x;
	LPC.y = lpc.y;
	LPC.z = lpc.z;

	Romega = R_omega;
	Rphi = R_phi;
	Rkappa = R_kappa;

	RPC.x = rpc.x;
	RPC.y = rpc.y;
	RPC.z = rpc.z;

	H = (LPC.z+RPC.z)/2;
	B = sqrt((RPC.x-LPC.x)*(RPC.x-LPC.x)+(RPC.y-LPC.y)*(RPC.y-LPC.y));

	focal_length_left = LFL;
	focal_length_right = RFL;

	maxcorrection = maxcor;
	maxiteration = maxiter;
}

void CFrameIntersection::Make_A_L(Point2D<double> La, Point2D<double> Ra)
{
	A.Resize(4,3,0.0);
	L.Resize(4,1,0.0);

	Make_b_J_K_Left(La);
	
	A(0,0) = b14;
	A(0,1) = b15;
	A(0,2) = b16;
		
	L(0,0) = J;

	A(1,0) = b24;
	A(1,1) = b25;
	A(1,2) = b26;
		
	L(1,0) = K;

	Make_b_J_K_Right(Ra);
	
	A(2,0) = b14;
	A(2,1) = b15;
	A(2,2) = b16;
		
	L(2,0) = J;

	A(3,0) = b24;
	A(3,1) = b25;
	A(3,2) = b26;
		
	L(3,0) = K;
}

void CFrameIntersection::Make_b_J_K_Left(Point2D<double> La)
{
	CRotationcoeff M(Lomega,Lphi,Lkappa);
	double q, r, s;
	double dX, dY, dZ;
	double fqq;
	dX = GP.x - LPC.x;
	dY = GP.y - LPC.y;
	dZ = GP.z - LPC.z;

	q = M.Rmatrix(2,0)*dX + M.Rmatrix(2,1)*dY + M.Rmatrix(2,2)*dZ;
	r = M.Rmatrix(0,0)*dX + M.Rmatrix(0,1)*dY + M.Rmatrix(0,2)*dZ;
	s = M.Rmatrix(1,0)*dX + M.Rmatrix(1,1)*dY + M.Rmatrix(1,2)*dZ;
	
	fqq = focal_length_left/(q*q);

	b14 = fqq*(r*M.Rmatrix(2,0)-q*M.Rmatrix(0,0));
	b15 = fqq*(r*M.Rmatrix(2,1)-q*M.Rmatrix(0,1));
	b16 = fqq*(r*M.Rmatrix(2,2)-q*M.Rmatrix(0,2));
	
	J = La.x - LPPA.x + focal_length_left*r/q;

	b24 = fqq*(s*M.Rmatrix(2,0)-q*M.Rmatrix(1,0));
	b25 = fqq*(s*M.Rmatrix(2,1)-q*M.Rmatrix(1,1));
	b26 = fqq*(s*M.Rmatrix(2,2)-q*M.Rmatrix(1,2));

	K = La.y - LPPA.y + focal_length_left*s/q;
}

void CFrameIntersection::Make_b_J_K_Right(Point2D<double> Ra)
{
	CRotationcoeff M(Romega,Rphi,Rkappa);
	double q, r, s;
	double dX, dY, dZ;
	double fqq;
	dX = GP.x - RPC.x;
	dY = GP.y - RPC.y;
	dZ = GP.z - RPC.z;

	q = M.Rmatrix(2,0)*dX + M.Rmatrix(2,1)*dY + M.Rmatrix(2,2)*dZ;
	r = M.Rmatrix(0,0)*dX + M.Rmatrix(0,1)*dY + M.Rmatrix(0,2)*dZ;
	s = M.Rmatrix(1,0)*dX + M.Rmatrix(1,1)*dY + M.Rmatrix(1,2)*dZ;
	
	fqq = focal_length_right/(q*q);

	b14 = fqq*(r*M.Rmatrix(2,0)-q*M.Rmatrix(0,0));
	b15 = fqq*(r*M.Rmatrix(2,1)-q*M.Rmatrix(0,1));
	b16 = fqq*(r*M.Rmatrix(2,2)-q*M.Rmatrix(0,2));
	
	J = Ra.x - RPPA.x + focal_length_right*r/q;

	b24 = fqq*(s*M.Rmatrix(2,0)-q*M.Rmatrix(1,0));
	b25 = fqq*(s*M.Rmatrix(2,1)-q*M.Rmatrix(1,1));
	b26 = fqq*(s*M.Rmatrix(2,2)-q*M.Rmatrix(1,2));

	K = Ra.y - RPPA.y + focal_length_right*s/q;
}
