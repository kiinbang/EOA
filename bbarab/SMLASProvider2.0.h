﻿/*
 * Copyright (c) 2005-2015, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#define RAD2DEG 57.295779513082
#define DEG2RAD 0.017453292520


#include <fstream>

#include "UTM_Transform.h"

#pragma pack(push, 1)

enum ErrorMsg{ ERROR_FREE, 
	           HEADER_READ_ERROR, 
			   LASFILE_OPEN_ERROR, 
			   VERSION_READ_ERROR, 
			   NOT_SUPPORTED_VERSION, 
	           SCALEFACTOR_ERROR, 
			   NOT_SUPPORTED_FORMAT, 
			   READ_POINTDATA_ERROR, 
			   OUT_OF_POINT_NUMBER, 
			   VLR_READ_ERROR,
               LASFILE_WRITE_OPEN_ERROR, 
			   POINT_START_SIGN_READ_ERROR, 
			   HEADER_WRITE_ERROR, 
			   DIFFERENT_VERSION_ERROR, 
			   DIFFERENT_TYPE_ERROR,
               POINT_INDEX_ERROR };

enum LAS_VERSION {V10, V12};

class LASHeader_10
{
public:
	char			FileSignature[4];
	unsigned long	Reserved;
	unsigned long	GUIDdata_1;
	unsigned short	GUIDdata_2;
	unsigned short	GUIDdata_3;
	unsigned char	GUIDdata_4[8];
	unsigned char	VersionMajor;
	unsigned char	VersionMinor;
	char			SystemIdentifier[32];
	char			GeneratingSoftware[32];
	unsigned short	FlightDateJulian;
	unsigned short	Year;
	unsigned short	HeaderSize;
	unsigned long	OffsettoData;
	unsigned long	NumberOfVariableLengthRecords;
	unsigned char	PointDataFormatID;
	unsigned short	PointDataRecordLength;
	unsigned long	NumberOfPointRecords;
	unsigned long	NumberOfPointsbyReturn[5];
	double			ScaleFactor_X;
	double			ScaleFactor_Y;
	double			ScaleFactor_Z;
	double			Offset_X;
	double			Offset_Y;
	double			Offset_Z;
	double			Max_X;
	double			Min_X;
	double			Max_Y;
	double			Min_Y;
	double			Max_Z;
	double			Min_Z;
public:
	LASHeader_10(){}

	LASHeader_10(LASHeader_10 &copy) { DataCopy(copy); }

	LASHeader_10& operator = (const LASHeader_10 &copy) { DataCopy(copy); return (*this);}

	void WriteHeader(std::fstream &lasfile)
	{
		lasfile.write(FileSignature,									4);
		lasfile.write((char*)&Reserved,						4);
		lasfile.write((char*)&GUIDdata_1,						4);
		lasfile.write((char*)&GUIDdata_2,						2);
		lasfile.write((char*)&GUIDdata_3,						2);
		lasfile.write((char*)GUIDdata_4,						8);
		lasfile.write((char*)&VersionMajor,						1);
		lasfile.write((char*)&VersionMinor,						1);
		lasfile.write((char*)SystemIdentifier,					32);
		lasfile.write((char*)GeneratingSoftware,				32);
		lasfile.write((char*)&FlightDateJulian,					2);
		lasfile.write((char*)&Year,								2);
		lasfile.write((char*)&HeaderSize,						2);
		lasfile.write((char*)&OffsettoData,						4);
		lasfile.write((char*)&NumberOfVariableLengthRecords,	4);
		lasfile.write((char*)&PointDataFormatID,				1);//0-99 for spec
		lasfile.write((char*)&PointDataRecordLength,			2);
		lasfile.write((char*)&NumberOfPointRecords,				4);
		lasfile.write((char*)NumberOfPointsbyReturn,			20);
		lasfile.write((char*)&ScaleFactor_X,					8);
		lasfile.write((char*)&ScaleFactor_Y,					8);
		lasfile.write((char*)&ScaleFactor_Z,					8);
		lasfile.write((char*)&Offset_X,							8);
		lasfile.write((char*)&Offset_Y,							8);
		lasfile.write((char*)&Offset_Z,							8);
		lasfile.write((char*)&Max_X,							8);
		lasfile.write((char*)&Min_X,							8);
		lasfile.write((char*)&Max_Y,							8);
		lasfile.write((char*)&Min_Y,							8);
		lasfile.write((char*)&Max_Z,							8);
		lasfile.write((char*)&Min_Z,							8);
	}

	ErrorMsg ReadHeader(std::fstream &lasfile)
	{
		lasfile.seekg(0, std::ios::beg);

		if(!lasfile.read((char*)FileSignature,						4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Reserved,							4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&GUIDdata_1,						4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&GUIDdata_2,						2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&GUIDdata_3,						2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)GUIDdata_4,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&VersionMajor,						1))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&VersionMinor,						1))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)SystemIdentifier,					32))	return HEADER_READ_ERROR;
		if(!lasfile.read((char*)GeneratingSoftware,					32))	return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&FlightDateJulian,					2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Year,								2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&HeaderSize,						2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&OffsettoData,						4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&NumberOfVariableLengthRecords,		4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&PointDataFormatID,					1))		return HEADER_READ_ERROR;//0-99 for spec
		if(!lasfile.read((char*)&PointDataRecordLength,				2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&NumberOfPointRecords,				4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)NumberOfPointsbyReturn,				20))	return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&ScaleFactor_X,						8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&ScaleFactor_Y,						8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&ScaleFactor_Z,						8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Offset_X,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Offset_Y,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Offset_Z,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Max_X,								8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Min_X,								8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Max_Y,								8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Min_Y,								8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Max_Z,								8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Min_Z,								8))		return HEADER_READ_ERROR;

		return ERROR_FREE;
	}

	void DataCopy(const LASHeader_10 &copy)
	{
		memcpy(FileSignature, copy.FileSignature, 4);
		Reserved						= copy.Reserved;
		GUIDdata_1						= copy.GUIDdata_1;
		GUIDdata_2						= copy.GUIDdata_2;
		GUIDdata_3						= copy.GUIDdata_3;
		memcpy(GUIDdata_4, copy.GUIDdata_4, 8);
		VersionMajor					= copy.VersionMajor;
		VersionMinor					= copy.VersionMinor;
		memcpy(SystemIdentifier, copy.SystemIdentifier, 32);
		memcpy(GeneratingSoftware, copy.GeneratingSoftware, 32);
		FlightDateJulian				= copy.FlightDateJulian;
		Year							= copy.Year;
		HeaderSize						= copy.HeaderSize;
		OffsettoData					= copy.OffsettoData;
		NumberOfVariableLengthRecords	= copy.NumberOfVariableLengthRecords;
		PointDataFormatID				= copy.PointDataFormatID;
		PointDataRecordLength			= copy.PointDataRecordLength;
		NumberOfPointRecords			= copy.NumberOfPointRecords;
		memcpy(NumberOfPointsbyReturn, copy.NumberOfPointsbyReturn, sizeof(unsigned long)*5);
		ScaleFactor_X					= copy.ScaleFactor_X;
		ScaleFactor_Y					= copy.ScaleFactor_Y;
		ScaleFactor_Z					= copy.ScaleFactor_Z;
		Offset_X						= copy.Offset_X;
		Offset_Y						= copy.Offset_Y;
		Offset_Z						= copy.Offset_Z;
		Max_X							= copy.Max_X;
		Min_X							= copy.Min_X;
		Max_Y							= copy.Max_Y;
		Min_Y							= copy.Min_Y;
		Max_Z							= copy.Max_Z;
		Min_Z							= copy.Min_Z;
	}
};

class LASPoint_10_0//size: 20 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		FileMarker;
	unsigned short		UserBitField;
public:
	LASPoint_10_0(){}

	LASPoint_10_0(LASPoint_10_0 &copy){ DataCopy(copy); }

	LASPoint_10_0& operator = (LASPoint_10_0 &copy){ DataCopy(copy); return (*this); }

	void DataCopy(LASPoint_10_0 &copy)
	{
		memcpy(this, &copy, sizeof(LASPoint_10_0));
	}
};

class LASPoint_10_1//size: 28 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		FileMarker;
	unsigned short		UserBitField;
	//char				GPSTime[8];//should be changed as double when it is used
	double				GPSTime;
public:
	LASPoint_10_1(){}

	LASPoint_10_1(LASPoint_10_1 &copy){ DataCopy(copy); }

	LASPoint_10_1& operator = (LASPoint_10_1 &copy){ DataCopy(copy); return (*this); }

	void DataCopy(LASPoint_10_1 &copy)
	{
		memcpy(this, &copy, sizeof(LASPoint_10_1));
	}
};

class LASHeader_12
{
public:
	char			FileSignature[4];
	unsigned short	FileSourceID;
	unsigned short	GlobalEncoding;
	unsigned long	GUIDdata_1;
	unsigned short	GUIDdata_2;
	unsigned short	GUIDdata_3;
	unsigned char	GUIDdata_4[8];
	unsigned char	VersionMajor;
	unsigned char	VersionMinor;
	char			SystemIdentifier[32];
	char			GeneratingSoftware[32];
	unsigned short	FileCreationDayOfYear;
	unsigned short	FileCreationYear;
	unsigned short	HeaderSize;
	unsigned long	OffsettoData;
	unsigned long	NumberOfVariableLengthRecords;
	unsigned char	PointDataFormatID;//0-99 for spec
	unsigned short	PointDataRecordLength;
	unsigned long	NumberOfPointRecords;
	unsigned long	NumberOfPointsbyReturn[5];
	double			ScaleFactor_X;
	double			ScaleFactor_Y;
	double			ScaleFactor_Z;
	double			Offset_X;
	double			Offset_Y;
	double			Offset_Z;
	double			Max_X;
	double			Min_X;
	double			Max_Y;
	double			Min_Y;
	double			Max_Z;
	double			Min_Z;
public:
	LASHeader_12(){}

	LASHeader_12(LASHeader_12 &copy) { DataCopy(copy); }

	LASHeader_12& operator = (const LASHeader_12 &copy) { DataCopy(copy); return (*this);}

	void WriteHeader(std::fstream &lasfile)
	{
		lasfile.write((char*)FileSignature,						4);
		lasfile.write((char*)&FileSourceID,						2);
		lasfile.write((char*)&GlobalEncoding,					2);
		lasfile.write((char*)&GUIDdata_1,						4);
		lasfile.write((char*)&GUIDdata_2,						2);
		lasfile.write((char*)&GUIDdata_3,						2);
		lasfile.write((char*)GUIDdata_4,						8);
		lasfile.write((char*)&VersionMajor,						1);
		lasfile.write((char*)&VersionMinor,						1);
		lasfile.write((char*)SystemIdentifier,					32);
		lasfile.write((char*)GeneratingSoftware,				32);
		lasfile.write((char*)&FileCreationDayOfYear,			2);
		lasfile.write((char*)&FileCreationYear,					2);
		lasfile.write((char*)&HeaderSize,						2);
		lasfile.write((char*)&OffsettoData,						4);
		lasfile.write((char*)&NumberOfVariableLengthRecords,	4);
		lasfile.write((char*)&PointDataFormatID,				1);//0-99 for spec
		lasfile.write((char*)&PointDataRecordLength,			2);
		lasfile.write((char*)&NumberOfPointRecords,				4);
		lasfile.write((char*)NumberOfPointsbyReturn,			20);
		lasfile.write((char*)&ScaleFactor_X,					8);
		lasfile.write((char*)&ScaleFactor_Y,					8);
		lasfile.write((char*)&ScaleFactor_Z,					8);
		lasfile.write((char*)&Offset_X,							8);
		lasfile.write((char*)&Offset_Y,							8);
		lasfile.write((char*)&Offset_Z,							8);
		lasfile.write((char*)&Max_X,							8);
		lasfile.write((char*)&Min_X,							8);
		lasfile.write((char*)&Max_Y,							8);
		lasfile.write((char*)&Min_Y,							8);
		lasfile.write((char*)&Max_Z,							8);
		lasfile.write((char*)&Min_Z,							8);
	}

	ErrorMsg ReadHeader(std::fstream &lasfile)
	{
		lasfile.seekg(0, std::ios::beg);

		//if(!lasfile.Open(m_stInfilePath, CFile::modeRead|CFile::shareDenyRead))
		//	return false;

		if(!lasfile.read((char*)FileSignature,						4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&FileSourceID,						2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&GlobalEncoding,					2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&GUIDdata_1,						4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&GUIDdata_2,						2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&GUIDdata_3,						2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)GUIDdata_4,						8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&VersionMajor,						1))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&VersionMinor,						1))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)SystemIdentifier,					32))	return HEADER_READ_ERROR;
		if(!lasfile.read((char*)GeneratingSoftware,				32))	return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&FileCreationDayOfYear,			2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&FileCreationYear,					2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&HeaderSize,						2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&OffsettoData,						4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&NumberOfVariableLengthRecords,	4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&PointDataFormatID,				1))		return HEADER_READ_ERROR;//0-99 for spec
		if(!lasfile.read((char*)&PointDataRecordLength,			2))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&NumberOfPointRecords,				4))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)NumberOfPointsbyReturn,			20))	return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&ScaleFactor_X,					8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&ScaleFactor_Y,					8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&ScaleFactor_Z,					8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Offset_X,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Offset_Y,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Offset_Z,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Max_X,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Min_X,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Max_Y,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Min_Y,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Max_Z,							8))		return HEADER_READ_ERROR;
		if(!lasfile.read((char*)&Min_Z,							8))		return HEADER_READ_ERROR;

		return ERROR_FREE;
	}

	void DataCopy(const LASHeader_12 &copy)
	{
		memcpy(FileSignature, copy.FileSignature, 4);
		FileSourceID					= copy.FileSourceID;
		GlobalEncoding					= copy.GlobalEncoding;
		GUIDdata_1						= copy.GUIDdata_1;
		GUIDdata_2						= copy.GUIDdata_2;
		GUIDdata_3						= copy.GUIDdata_3;
		memcpy(GUIDdata_4, copy.GUIDdata_4, 8);
		VersionMajor					= copy.VersionMajor;
		VersionMinor					= copy.VersionMinor;
		memcpy(SystemIdentifier, copy.SystemIdentifier, 32);
		memcpy(GeneratingSoftware, copy.GeneratingSoftware, 32);
		FileCreationDayOfYear			= copy.FileCreationDayOfYear;
		FileCreationYear				= copy.FileCreationYear;
		HeaderSize						= copy.HeaderSize;
		OffsettoData					= copy.OffsettoData;
		NumberOfVariableLengthRecords	= copy.NumberOfVariableLengthRecords;
		PointDataFormatID				= copy.PointDataFormatID;
		PointDataRecordLength			= copy.PointDataRecordLength;
		NumberOfPointRecords			= copy.NumberOfPointRecords;
		memcpy(NumberOfPointsbyReturn, copy.NumberOfPointsbyReturn, sizeof(unsigned long)*5);
		ScaleFactor_X					= copy.ScaleFactor_X;
		ScaleFactor_Y					= copy.ScaleFactor_Y;
		ScaleFactor_Z					= copy.ScaleFactor_Z;
		Offset_X						= copy.Offset_X;
		Offset_Y						= copy.Offset_Y;
		Offset_Z						= copy.Offset_Z;
		Max_X							= copy.Max_X;
		Min_X							= copy.Min_X;
		Max_Y							= copy.Max_Y;
		Min_Y							= copy.Min_Y;
		Max_Z							= copy.Max_Z;
		Min_Z							= copy.Min_Z;
	}
};

class LASPoint_12_0//size: 20 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		UserData;
	unsigned short		PointSourceID;
public:
	LASPoint_12_0(){}

	LASPoint_12_0(LASPoint_12_0 &copy){ DataCopy(copy); }

	LASPoint_12_0& operator = (LASPoint_12_0 &copy){ DataCopy(copy); return (*this); }

	void DataCopy(LASPoint_12_0 &copy)
	{
		memcpy(this, &copy, sizeof(LASPoint_12_0));
	}
};

class LASPoint_12_1//size: 28 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		UserData;
	unsigned short		PointSourceID;
	//char				GPSTime[8];//should be changed as double when it is used
	double				GPSTime;
public:
	LASPoint_12_1(){}

	LASPoint_12_1(LASPoint_12_1 &copy){ DataCopy(copy); }

	LASPoint_12_1& operator = (LASPoint_12_1 &copy){ DataCopy(copy); return (*this); }

	void DataCopy(LASPoint_12_1 &copy)
	{
		memcpy(this, &copy, sizeof(LASPoint_12_1));
	}
};

class LASPoint_12_2//size: 26 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		UserData;
	unsigned short		PointSourceID;
	unsigned short		Red;
	unsigned short		Green;
	unsigned short		Blue;
public:
	LASPoint_12_2(){}

	LASPoint_12_2(LASPoint_12_2 &copy){ DataCopy(copy); }

	LASPoint_12_2& operator = (LASPoint_12_2 &copy){ DataCopy(copy); return (*this); }

	void DataCopy(LASPoint_12_2 &copy)
	{
		memcpy(this, &copy, sizeof(LASPoint_12_2));
	}
};

class LASPoint_12_3//size: 34 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		UserData;
	unsigned short		PointSourceID;
	//char				GPSTime[8];//should be changed as double when it is used
	double				GPSTime;
	unsigned short		Red;
	unsigned short		Green;
	unsigned short		Blue;
public:
	LASPoint_12_3(){}

	LASPoint_12_3(LASPoint_12_3 &copy){ DataCopy(copy); }

	LASPoint_12_3& operator = (LASPoint_12_3 &copy){ DataCopy(copy); return (*this); }

	void DataCopy(LASPoint_12_3 &copy)
	{
		memcpy(this, &copy, sizeof(LASPoint_12_3));
	}
};

//It is not recommended to use this type for all points because of the size of this class. 
//Use this class for point data exchange between different types
class LASPoint_Common 
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		FileMarker;
	unsigned short		UserBitField;
	unsigned char		UserData;
	unsigned short		PointSourceID;
	//char				GPSTime[8];//should be changed as double when it is used
	double				GPSTime;
	unsigned short		Red;
	unsigned short		Green;
	unsigned short		Blue;
public:
	LASPoint_Common()
	{
		FileMarker = 0;
		NumberOfReturns = 1;
		PointSourceID = 0;
		ReturnNumber = 1;
		ScanAngleRank = 0;
		Classification = 0;
		ScanDirectionFlag = 0;
		UserBitField = 0;
		UserData = 0;
	}

	LASPoint_Common(LASPoint_Common &copy){ DataCopy(copy); }
	LASPoint_Common(LASPoint_10_0 &copy){ SetPointData(copy); }
	LASPoint_Common(LASPoint_10_1 &copy){ SetPointData(copy); }
	LASPoint_Common(LASPoint_12_0 &copy){ SetPointData(copy); }
	LASPoint_Common(LASPoint_12_1 &copy){ SetPointData(copy); }
	LASPoint_Common(LASPoint_12_2 &copy){ SetPointData(copy); }
	LASPoint_Common(LASPoint_12_3 &copy){ SetPointData(copy); }

	LASPoint_Common& operator = (LASPoint_Common &copy){ DataCopy(copy); return (*this); }
	LASPoint_Common& operator = (LASPoint_10_0 &copy){ SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (LASPoint_10_1 &copy){ SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (LASPoint_12_0 &copy){ SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (LASPoint_12_1 &copy){ SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (LASPoint_12_2 &copy){ SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (LASPoint_12_3 &copy){ SetPointData(copy); return (*this); }

	void DataCopy(LASPoint_Common &copy)
	{
		memcpy(this, &copy, sizeof(LASPoint_Common));
	}

	void SetPointData(LASPoint_10_0 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		FileMarker			= copy.FileMarker;
		UserBitField		= copy.UserBitField;
	}

	void SetPointData(LASPoint_10_1 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		FileMarker			= copy.FileMarker;
		UserBitField		= copy.UserBitField;
		memcpy(&GPSTime, &(copy.GPSTime), 8);
	}

	void SetPointData(LASPoint_12_0 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		UserData			= copy.UserData;
		PointSourceID		= copy.PointSourceID;
	}

	void SetPointData(LASPoint_12_1 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		UserData			= copy.UserData;
		PointSourceID		= copy.PointSourceID;
		memcpy(&GPSTime, &(copy.GPSTime), 8);
	}

	void SetPointData(LASPoint_12_2 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		UserData			= copy.UserData;
		PointSourceID		= copy.PointSourceID;
		Red					= copy.Red;
		Green				= copy.Green;
		Blue				= copy.Blue;
	}

	void SetPointData(LASPoint_12_3 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		UserData			= copy.UserData;
		PointSourceID		= copy.PointSourceID;
		memcpy(&GPSTime, &(copy.GPSTime), 8);
		Red					= copy.Red;
		Green				= copy.Green;
		Blue				= copy.Blue;
	}

	void GetPointData(LASPoint_10_0 &set)
	{
		set.X						= X;
		set.Y						= Y;
		set.Z						= Z;
		set.Intensity				= Intensity;
		set.ReturnNumber			= ReturnNumber;
		set.NumberOfReturns			= NumberOfReturns;
		set.ScanDirectionFlag		= ScanDirectionFlag;
		set.EdgeofFlightLine		= EdgeofFlightLine;
		set.Classification			= Classification;
		set.ScanAngleRank			= ScanAngleRank;
		set.FileMarker				= FileMarker;
		set.UserBitField			= UserBitField;
	}

	void GetPointData(LASPoint_10_1 &set)
	{
		set.X						= X;
		set.Y						= Y;
		set.Z						= Z;
		set.Intensity				= Intensity;
		set.ReturnNumber			= ReturnNumber;
		set.NumberOfReturns			= NumberOfReturns;
		set.ScanDirectionFlag		= ScanDirectionFlag;
		set.EdgeofFlightLine		= EdgeofFlightLine;
		set.Classification			= Classification;
		set.ScanAngleRank			= ScanAngleRank;
		set.FileMarker				= FileMarker;
		set.UserBitField			= UserBitField;
		memcpy(&(set.GPSTime), &GPSTime, 8);
	}

	void GetPointData(LASPoint_12_0 &set)
	{
		set.X						= X;
		set.Y						= Y;
		set.Z						= Z;
		set.Intensity				= Intensity;
		set.ReturnNumber			= ReturnNumber;
		set.NumberOfReturns			= NumberOfReturns;
		set.ScanDirectionFlag		= ScanDirectionFlag;
		set.EdgeofFlightLine		= EdgeofFlightLine;
		set.Classification			= Classification;
		set.ScanAngleRank			= ScanAngleRank;
		set.UserData				= UserData;
		set.PointSourceID			= PointSourceID;
	}

	void GetPointData(LASPoint_12_1 &set)
	{
		set.X						= X;
		set.Y						= Y;
		set.Z						= Z;
		set.Intensity				= Intensity;
		set.ReturnNumber			= ReturnNumber;
		set.NumberOfReturns			= NumberOfReturns;
		set.ScanDirectionFlag		= ScanDirectionFlag;
		set.EdgeofFlightLine		= EdgeofFlightLine;
		set.Classification			= Classification;
		set.ScanAngleRank			= ScanAngleRank;
		set.UserData				= UserData;
		set.PointSourceID			= PointSourceID;
		memcpy(&(set.GPSTime), &GPSTime, 8);
	}

	void GetPointData(LASPoint_12_2 &set)
	{
		set.X						= X;
		set.Y						= Y;
		set.Z						= Z;
		set.Intensity				= Intensity;
		set.ReturnNumber			= ReturnNumber;
		set.NumberOfReturns			= NumberOfReturns;
		set.ScanDirectionFlag		= ScanDirectionFlag;
		set.EdgeofFlightLine		= EdgeofFlightLine;
		set.Classification			= Classification;
		set.ScanAngleRank			= ScanAngleRank;
		set.UserData				= UserData;
		set.PointSourceID			= PointSourceID;
		set.Red						= Red;
		set.Green					= Green;
		set.Blue					= Blue;
	}

	void GetPointData(LASPoint_12_3 &set)
	{
		set.X						= X;
		set.Y						= Y;
		set.Z						= Z;
		set.Intensity				= Intensity;
		set.ReturnNumber			= ReturnNumber;
		set.NumberOfReturns			= NumberOfReturns;
		set.ScanDirectionFlag		= ScanDirectionFlag;
		set.EdgeofFlightLine		= EdgeofFlightLine;
		set.Classification			= Classification;
		set.ScanAngleRank			= ScanAngleRank;
		set.UserData				= UserData;
		set.PointSourceID			= PointSourceID;
		memcpy(&(set.GPSTime), &GPSTime, 8);
		set.Red						= Red;
		set.Green					= Green;
		set.Blue					= Blue;
	}
};

typedef class VARIABLE_LENGTH_RECORDS
{
public:
	unsigned short	RecordSignature;
	char			UserID[16];
	unsigned short	RecordID;
	unsigned short	RecordLengthAfterHeader;
	char			Description[32];
public:
	VARIABLE_LENGTH_RECORDS(){}

	VARIABLE_LENGTH_RECORDS(const VARIABLE_LENGTH_RECORDS& copy){ DataCopy(copy);}

	VARIABLE_LENGTH_RECORDS& operator = (const VARIABLE_LENGTH_RECORDS& copy){ DataCopy(copy); return (*this);}

	ErrorMsg ReadVLR(std::fstream &lasfile)
	{
		if(!lasfile.read((char*)&RecordSignature,					2))		return VLR_READ_ERROR;
		if(!lasfile.read((char*)UserID,								16))	return VLR_READ_ERROR;
		if(!lasfile.read((char*)&RecordID,							2))		return VLR_READ_ERROR;
		if(!lasfile.read((char*)&RecordLengthAfterHeader,			2))		return VLR_READ_ERROR;
		if(!lasfile.read((char*)Description,						32))	return VLR_READ_ERROR;

		return ERROR_FREE;
	}

	void DataCopy(const VARIABLE_LENGTH_RECORDS& copy)
	{
		RecordSignature					= copy.RecordSignature;
		memcpy(UserID, copy.UserID, 16);
		RecordID						= copy.RecordID;
		RecordLengthAfterHeader			= copy.RecordLengthAfterHeader;
		memcpy(Description, copy.Description, 16);
	}
} VLR;

class CSMLASProvider
{
public:
	std::fstream		m_LASFile;
	bool				m_bOpen;

	unsigned char		m_VerMajor;
	unsigned char		m_VerMinor;
	LAS_VERSION			m_Version;
	unsigned char		m_FormatType;

	unsigned long		m_HeaderSize;
	unsigned long		m_OffsetToPointData;
	unsigned long		*m_DataSizeAfterVLR;

	LASHeader_10		m_Header_10;
	LASHeader_12		m_Header_12;

	unsigned long		m_NumVLR;
	VLR					*m_VLR;

	unsigned long		m_NumPoints;

	LASPoint_10_0		*m_Points_10_0;
	LASPoint_10_1		*m_Points_10_1;
	LASPoint_12_0		*m_Points_12_0;
	LASPoint_12_1		*m_Points_12_1;
	LASPoint_12_2		*m_Points_12_2;
	LASPoint_12_3		*m_Points_12_3;

	double				ScaleX;
	double				ScaleY;
	double				ScaleZ;
	double				OffsetX;
	double				OffsetY;
	double				OffsetZ;

	double				MinX;
	double				MinY;
	double				MinZ;

	double				MaxX;
	double				MaxY;
	double				MaxZ;

	unsigned char		**m_AfterVLR;//Data size after Variable Length Records

	unsigned short		PointDataStartSignature; //For LAS Ver 1.0

	unsigned long		begin_idx;
	unsigned long		end_idx;


public:

	CSMLASProvider(void)
	{
		m_Points_10_0				= NULL;
		m_Points_10_1				= NULL;
		m_Points_12_0				= NULL;
		m_Points_12_1				= NULL;
		m_Points_12_2				= NULL;
		m_Points_12_3				= NULL;
		m_AfterVLR					= NULL;
		m_DataSizeAfterVLR			= NULL;
		m_VLR						= NULL;
		m_AfterVLR					= NULL;

		m_bOpen						= false;

		PointDataStartSignature		=0xCCDD; //For LAS Ver 1.0
	}

	~CSMLASProvider(void)
	{
		DeletePointData();

		DeleteVLR();

		Close();
	}

	ErrorMsg CreateNewLAS(const LASHeader_10 &header, const VLR *pVLR=NULL, const unsigned char **pAfterVLR=NULL)
	{
		ErrorMsg retval = ERROR_FREE;

		m_Header_10 = header;

		if(pVLR==NULL||pAfterVLR==NULL)
			m_Header_10.NumberOfVariableLengthRecords = 0;

		SetConfigFromHeader(m_Header_10);

		SetVLR(this->m_NumVLR, pVLR, pAfterVLR);

		retval = SetDummyPoints(this->m_NumPoints, this->m_Version, this->m_FormatType);

		if(retval != ERROR_FREE)
			return retval;

		return retval;
	}

	ErrorMsg CreateNewLAS(const LASHeader_12 &header, const VLR *pVLR=NULL, const unsigned char **pAfterVLR=NULL)
	{
		ErrorMsg retval = ERROR_FREE;

		m_Header_12 = header;

		if(pVLR==NULL||pAfterVLR==NULL)
			m_Header_12.NumberOfVariableLengthRecords = 0;

		SetConfigFromHeader(m_Header_12);

		SetVLR(this->m_NumVLR, pVLR, pAfterVLR);

		retval = SetDummyPoints(this->m_NumPoints, this->m_Version, this->m_FormatType);

		if(retval != ERROR_FREE)
			return retval;

		return retval;
	}

	void DeletePointData()
	{
		if(m_Points_10_0 != NULL)
		{
			delete[] m_Points_10_0;
			m_Points_10_0 = NULL;
		}

		if(m_Points_10_1 != NULL)
		{
			delete[] m_Points_10_1;
			m_Points_10_1 = NULL;
		}

		if(m_Points_12_0 != NULL)
		{
			delete[] m_Points_12_0;
			m_Points_12_0 = NULL;
		}

		if(m_Points_12_1 != NULL)
		{
			delete[] m_Points_12_1;
			m_Points_12_1 = NULL;
		}

		if(m_Points_12_2 != NULL)
		{
			delete[] m_Points_12_2;
			m_Points_12_2 = NULL;
		}

		if(m_Points_12_3 != NULL)
		{
			delete[] m_Points_12_3;
			m_Points_12_3 = NULL;
		}
	}

	void DeleteVLR()
	{
		if(m_VLR != NULL)
		{
			delete[] m_VLR;
			m_VLR = NULL;
		}

		if(m_DataSizeAfterVLR != NULL)
		{
			delete[] m_DataSizeAfterVLR;
			m_DataSizeAfterVLR = NULL;
		}

		if(this->m_AfterVLR != NULL)
		{
			for(unsigned long i=0; i<m_NumVLR; i++)
			{
				delete[] m_AfterVLR[i];
			}

			delete[] m_AfterVLR;

			m_AfterVLR = NULL;
		}
	}

	void Close()
	{
		if(m_bOpen == true)
		{
			m_LASFile.close();

			m_bOpen = false;
		}
	}

	ErrorMsg Open(const char* inFilePath)
	{
		m_LASFile.open(inFilePath, std::ios::in | std::ios::binary);

		if(!m_LASFile) return LASFILE_OPEN_ERROR;

		m_bOpen = true;

		ErrorMsg emsg;
		//
		//Check version of LAS format
		//
		emsg = CheckVersion();

		if(ERROR_FREE != emsg)
			return emsg;

		//
		//Move to file begin
		//
		m_LASFile.seekg(0, std::ios::beg);

		//
		//Read header data
		//
		switch(m_Version)
		{
		case V10:
			emsg = m_Header_10.ReadHeader(m_LASFile);
			if(emsg != ERROR_FREE) return emsg;
			SetConfigFromHeader(m_Header_10);
			break;
		case V12:
			emsg = m_Header_12.ReadHeader(m_LASFile);
			if(emsg != ERROR_FREE) return emsg;
			SetConfigFromHeader(m_Header_12);
			break;
		default:
			return NOT_SUPPORTED_VERSION;
			break;
		}

		if(ScaleX == 0.||ScaleY == 0.||ScaleZ == 0.)
			return SCALEFACTOR_ERROR;

		//
		//Read Variable Length Records
		//

		if(m_NumVLR > 0)
		{
			unsigned long m_VLRSize = sizeof(VLR);

			m_VLR = new VLR[m_NumVLR];
			m_DataSizeAfterVLR = new unsigned long[m_NumVLR];
			m_AfterVLR = new unsigned char*[m_NumVLR];

			for(unsigned long i=0; i<m_NumVLR; i++)
			{
				m_VLR[i].ReadVLR(m_LASFile);
				m_DataSizeAfterVLR[i] = m_VLR[i].RecordLengthAfterHeader;

				m_AfterVLR[i] = new unsigned char[m_DataSizeAfterVLR[i]];
				m_LASFile.read((char*)m_AfterVLR[i], m_DataSizeAfterVLR[i]);
			}
		}

		if(m_Version == V10)
		{
			if(!m_LASFile.read((char*)&PointDataStartSignature, 2)) 
				return POINT_START_SIGN_READ_ERROR;
		}
		
		return ERROR_FREE;
	}

	ErrorMsg CheckVersion()
	{
		m_LASFile.seekg(0, std::ios::beg);

		char temp[26];

		if(!m_LASFile.read(temp, 26))
			return VERSION_READ_ERROR;

		m_VerMajor = temp[24];
		m_VerMinor = temp[25];

		if(m_VerMajor == 1)
		{
			if(m_VerMinor == 0)
			{
				m_Version = V10;
			}
			else if(m_VerMinor == 2)
			{
				m_Version = V12;
			}
		}

		return ERROR_FREE;
	}

	ErrorMsg ReadAllPoints()
	{
		m_LASFile.seekg(m_OffsetToPointData, std::ios::beg);

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				m_Points_10_0 = new LASPoint_10_0[m_NumPoints];
				if(!m_LASFile.read((char*)m_Points_10_0, sizeof(LASPoint_10_0)*m_NumPoints)) return READ_POINTDATA_ERROR;
				break;
			case 1:
				m_Points_10_1 = new LASPoint_10_1[m_NumPoints];
				if(!m_LASFile.read((char*)m_Points_10_1, sizeof(LASPoint_10_1)*m_NumPoints)) return READ_POINTDATA_ERROR;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				m_Points_12_0 = new LASPoint_12_0[m_NumPoints];
				if(!m_LASFile.read((char*)m_Points_12_0, sizeof(LASPoint_12_0)*m_NumPoints)) return READ_POINTDATA_ERROR;
				break;
			case 1:
				m_Points_12_1 = new LASPoint_12_1[m_NumPoints];
				if(!m_LASFile.read((char*)m_Points_12_1, sizeof(LASPoint_12_1)*m_NumPoints)) return READ_POINTDATA_ERROR;
				break;
			case 2:
				m_Points_12_2 = new LASPoint_12_2[m_NumPoints];
				if(!m_LASFile.read((char*)m_Points_12_2, sizeof(LASPoint_12_2)*m_NumPoints)) return READ_POINTDATA_ERROR;
				break;
			case 3:
				m_Points_12_3 = new LASPoint_12_3[m_NumPoints];
				if(!m_LASFile.read((char*)m_Points_12_3, sizeof(LASPoint_12_3)*m_NumPoints)) return READ_POINTDATA_ERROR;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetClassification(const unsigned long i, unsigned char& classification)
	{
		if (i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch (m_Version)
		{
		case V10:
			switch (m_FormatType)
			{
			case 0:
				classification = m_Points_10_0[i].Classification;
				break;
			case 1:
				classification = m_Points_10_1[i].Classification;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch (m_FormatType)
			{
			case 0:
				classification = m_Points_12_0[i].Classification;
				break;
			case 1:
				classification = m_Points_12_1[i].Classification;
				break;
			case 2:
				classification = m_Points_12_2[i].Classification;
				break;
			case 3:
				classification = m_Points_12_3[i].Classification;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetClassification(const unsigned long i, const unsigned char classification)
	{
		if (i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch (m_Version)
		{
		case V10:
			switch (m_FormatType)
			{
			case 0:
				m_Points_10_0[i].Classification = classification;
				break;
			case 1:
				m_Points_10_1[i].Classification = classification;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch (m_FormatType)
			{
			case 0:
				m_Points_12_0[i].Classification = classification;
				break;
			case 1:
				m_Points_12_1[i].Classification = classification;
				break;
			case 2:
				m_Points_12_2[i].Classification = classification;
				break;
			case 3:
				m_Points_12_3[i].Classification = classification;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetScanAngleRank(const unsigned long i, unsigned char& classification)
	{
		if (i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch (m_Version)
		{
		case V10:
			switch (m_FormatType)
			{
			case 0:
				classification = m_Points_10_0[i].Classification;
				break;
			case 1:
				classification = m_Points_10_1[i].Classification;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch (m_FormatType)
			{
			case 0:
				classification = m_Points_12_0[i].Classification;
				break;
			case 1:
				classification = m_Points_12_1[i].Classification;
				break;
			case 2:
				classification = m_Points_12_2[i].Classification;
				break;
			case 3:
				classification = m_Points_12_3[i].Classification;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetScanDirectionFlag(const unsigned long idx, unsigned char& directionFlag)
	{
		switch (m_Version)
		{
		case V10:
			switch (m_FormatType)
			{
			case 0:
				directionFlag = m_Points_10_0[idx].ScanDirectionFlag;
				break;
			case 1:
				directionFlag = m_Points_10_1[idx].ScanDirectionFlag;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch (m_FormatType)
			{
			case 0:
				directionFlag = m_Points_12_0[idx].ScanDirectionFlag;
				break;
			case 1:
				directionFlag = m_Points_12_1[idx].ScanDirectionFlag;
				break;
			case 2:
				directionFlag = m_Points_12_2[idx].ScanDirectionFlag;
				break;
			case 3:
				directionFlag = m_Points_12_3[idx].ScanDirectionFlag;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetXYZ(const unsigned long i, double *X, double *Y, double *Z)
	{
		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		double X0, Y0, Z0;

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				X0 = m_Points_10_0[i].X;
				Y0 = m_Points_10_0[i].Y;
				Z0 = m_Points_10_0[i].Z;
				break;
			case 1:
				X0 = m_Points_10_1[i].X;
				Y0 = m_Points_10_1[i].Y;
				Z0 = m_Points_10_1[i].Z;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			*X = X0*m_Header_10.ScaleFactor_X + m_Header_10.Offset_X;
			*Y = Y0*m_Header_10.ScaleFactor_Y + m_Header_10.Offset_Y;
			*Z = Z0*m_Header_10.ScaleFactor_Z + m_Header_10.Offset_Z;
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				X0 = m_Points_12_0[i].X;
				Y0 = m_Points_12_0[i].Y;
				Z0 = m_Points_12_0[i].Z;
				break;
			case 1:
				X0 = m_Points_12_1[i].X;
				Y0 = m_Points_12_1[i].Y;
				Z0 = m_Points_12_1[i].Z;
				break;
			case 2:
				X0 = m_Points_12_2[i].X;
				Y0 = m_Points_12_2[i].Y;
				Z0 = m_Points_12_2[i].Z;
				break;
			case 3:
				X0 = m_Points_12_3[i].X;
				Y0 = m_Points_12_3[i].Y;
				Z0 = m_Points_12_3[i].Z;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			*X = X0*m_Header_12.ScaleFactor_X + m_Header_12.Offset_X;
			*Y = Y0*m_Header_12.ScaleFactor_Y + m_Header_12.Offset_Y;
			*Z = Z0*m_Header_12.ScaleFactor_Z + m_Header_12.Offset_Z;
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetXYZ(unsigned long i, double X, double Y, double Z)
	{
		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		double X0, Y0, Z0;

		switch(m_Version)
		{
		case V10:
			X0 = (X - m_Header_10.Offset_X)/m_Header_10.ScaleFactor_X;
			Y0 = (Y - m_Header_10.Offset_Y)/m_Header_10.ScaleFactor_Y;
			Z0 = (Z - m_Header_10.Offset_Z)/m_Header_10.ScaleFactor_Z;
			switch(m_FormatType)
			{
			case 0:
				m_Points_10_0[i].X = (long)X0;
				m_Points_10_0[i].Y = (long)Y0;
				m_Points_10_0[i].Z = (long)Z0;
				break;
			case 1:
				m_Points_10_1[i].X = (long)X0;
				m_Points_10_1[i].Y = (long)Y0;
				m_Points_10_1[i].Z = (long)Z0;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			X0 = (X - m_Header_12.Offset_X)/m_Header_12.ScaleFactor_X;
			Y0 = (Y - m_Header_12.Offset_Y)/m_Header_12.ScaleFactor_Y;
			Z0 = (Z - m_Header_12.Offset_Z)/m_Header_12.ScaleFactor_Z;
			switch(m_FormatType)
			{
			case 0:
				m_Points_12_0[i].X = (long)X0;
				m_Points_12_0[i].Y = (long)Y0;
				m_Points_12_0[i].Z = (long)Z0;
				break;
			case 1:
				m_Points_12_1[i].X = (long)X0;
				m_Points_12_1[i].Y = (long)Y0;
				m_Points_12_1[i].Z = (long)Z0;
				break;
			case 2:
				m_Points_12_2[i].X = (long)X0;
				m_Points_12_2[i].Y = (long)Y0;
				m_Points_12_2[i].Z = (long)Z0;
				break;
			case 3:
				m_Points_12_3[i].X = (long)X0;
				m_Points_12_3[i].Y = (long)Y0;
				m_Points_12_3[i].Z = (long)Z0;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetIntensity(unsigned long i, unsigned short *Intensity)
	{
		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				*Intensity = m_Points_10_0[i].Intensity;
				break;
			case 1:
				*Intensity = m_Points_10_1[i].Intensity;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				*Intensity = m_Points_12_0[i].Intensity;
				break;
			case 1:
				*Intensity = m_Points_12_1[i].Intensity;
				break;
			case 2:
				*Intensity = m_Points_12_2[i].Intensity;
				break;
			case 3:
				*Intensity = m_Points_12_3[i].Intensity;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetIntensity(unsigned long i, unsigned short Intensity)
	{
		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				m_Points_10_0[i].Intensity = Intensity;
				break;
			case 1:
				m_Points_10_1[i].Intensity = Intensity;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				m_Points_12_0[i].Intensity = Intensity;
				break;
			case 1:
				m_Points_12_1[i].Intensity = Intensity;
				break;
			case 2:
				m_Points_12_2[i].Intensity = Intensity;
				break;
			case 3:
				m_Points_12_3[i].Intensity = Intensity;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetRGB(unsigned long i, unsigned short *R, unsigned short *G, unsigned short *B)
	{
		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				return NOT_SUPPORTED_FORMAT;
			case 1:
				return NOT_SUPPORTED_FORMAT;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				return NOT_SUPPORTED_FORMAT;
			case 1:
				return NOT_SUPPORTED_FORMAT;
			case 2:
				*R = m_Points_12_2[i].Red;
				*G = m_Points_12_2[i].Green;
				*B = m_Points_12_2[i].Blue;
				break;
			case 3:
				*R = m_Points_12_3[i].Red;
				*G = m_Points_12_3[i].Green;
				*B = m_Points_12_3[i].Blue;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetRGB(unsigned long i, unsigned short R, unsigned short G, unsigned short B)
	{
		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				return NOT_SUPPORTED_FORMAT;
			case 1:
				return NOT_SUPPORTED_FORMAT;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				return NOT_SUPPORTED_FORMAT;
			case 1:
				return NOT_SUPPORTED_FORMAT;
			case 2:
				m_Points_12_2[i].Red	= R;
				m_Points_12_2[i].Green	= G;
				m_Points_12_2[i].Blue	= B;
				break;
			case 3:
				m_Points_12_3[i].Red	= R;
				m_Points_12_3[i].Green	= G;
				m_Points_12_3[i].Blue	= B;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetPoint(unsigned long i, LASPoint_Common &point)//Copy FROM this TO point(common)
	{
		//This GetPoint function call "Set" function of LASPoint_Common to return LAS point value through "&point".
		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				point.SetPointData(m_Points_10_0[i]);
				break;
			case 1:
				point.SetPointData(m_Points_10_1[i]);
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				point.SetPointData(m_Points_12_0[i]);
				break;
			case 1:
				point.SetPointData(m_Points_12_1[i]);
				break;
			case 2:
				point.SetPointData(m_Points_12_2[i]);
				break;
			case 3:
				point.SetPointData(m_Points_12_3[i]);
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetPoint(unsigned long i, LASPoint_Common &point)//Copy FROM point(common) TO this
	{
		//This SetPoint function call "Get" function of LASPoint_Common to copy "&point" value to LAS point.

		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				point.GetPointData(m_Points_10_0[i]);
				break;
			case 1:
				point.GetPointData(m_Points_10_1[i]);
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				point.GetPointData(m_Points_12_0[i]);
				break;
			case 1:
				point.GetPointData(m_Points_12_1[i]);
				break;
			case 2:
				point.GetPointData(m_Points_12_2[i]);
				break;
			case 3:
				point.GetPointData(m_Points_12_3[i]);
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetReturnNumber(unsigned long i, unsigned char *ReturnNumber)
	{
		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				*ReturnNumber = m_Points_10_0[i].ReturnNumber;
				break;
			case 1:
				*ReturnNumber = m_Points_10_1[i].ReturnNumber;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				*ReturnNumber = m_Points_12_0[i].ReturnNumber;
				break;
			case 1:
				*ReturnNumber = m_Points_12_1[i].ReturnNumber;
				break;
			case 2:
				*ReturnNumber = m_Points_12_2[i].ReturnNumber;
				break;
			case 3:
				*ReturnNumber = m_Points_12_3[i].ReturnNumber;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetReturnNumber(unsigned long i, unsigned char ReturnNumber)
	{
		if(i >= m_NumPoints)
			return OUT_OF_POINT_NUMBER;

		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				m_Points_10_0[i].ReturnNumber = ReturnNumber;
				break;
			case 1:
				m_Points_10_1[i].ReturnNumber = ReturnNumber;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				m_Points_12_0[i].ReturnNumber = ReturnNumber;
				break;
			case 1:
				m_Points_12_1[i].ReturnNumber = ReturnNumber;
				break;
			case 2:
				m_Points_12_2[i].ReturnNumber = ReturnNumber;
				break;
			case 3:
				m_Points_12_3[i].ReturnNumber = ReturnNumber;
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	/////////////////////////////////////////////////////////////
	//
	//For creating a new las from an old one
	//
	/////////////////////////////////////////////////////////////
	
	//
	//Copy header & VLR
	//
	ErrorMsg CopyHeaderAndVLR(CSMLASProvider &copy)
	{
		//
		//Copy header
		//
		m_Version = copy.m_Version;

		switch(m_Version)
		{
		case V10:
			m_Header_10 = copy.m_Header_10;
			SetConfigFromHeader(m_Header_10);
			break;
		case V12:
			m_Header_12 = copy.m_Header_12;
			SetConfigFromHeader(m_Header_12);
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		//
		//Copy Variable Length Records
		//
		DeleteVLR();

		m_NumVLR = copy.m_NumVLR;

		if(m_NumVLR > 0)
		{
			unsigned long m_VLRSize = sizeof(VLR);

			m_VLR = new VLR[m_NumVLR];
			m_DataSizeAfterVLR = new unsigned long[m_NumVLR];
			m_AfterVLR = new unsigned char*[m_NumVLR];

			for(unsigned long i=0; i<m_NumVLR; i++)
			{
				m_VLR[i] = copy.m_VLR[i];
				m_DataSizeAfterVLR[i] = m_VLR[i].RecordLengthAfterHeader;

				m_AfterVLR[i] = new unsigned char[m_DataSizeAfterVLR[i]];
				memcpy(m_AfterVLR[i], copy.m_AfterVLR[i], m_DataSizeAfterVLR[i]);
			}
		}

		if(m_Version == V10)
		{
			PointDataStartSignature = copy.PointDataStartSignature;
		}

		return ERROR_FREE;
	}

	//
	//Set dummy point data
	//
	ErrorMsg SetDummyPoints(unsigned long numPoints, LAS_VERSION ver, unsigned char Formattype)
	{
		if(m_Version != ver)
			return DIFFERENT_VERSION_ERROR;

		if(m_FormatType != Formattype)
			return DIFFERENT_TYPE_ERROR;

		//
		//Delete old data if exist
		//
		DeletePointData();

		//
		//Write Point Data
		//
		switch(ver)
		{
		case V10:
			switch(Formattype)
			{
			case 0:
				m_Points_10_0 = new LASPoint_10_0[numPoints];
				break;
			case 1:
				m_Points_10_1 = new LASPoint_10_1[numPoints];
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			m_NumPoints = numPoints;
			m_Header_10.NumberOfPointRecords = numPoints;
			break;
		case V12:
			switch(Formattype)
			{
			case 0:
				m_Points_12_0 = new LASPoint_12_0[numPoints];
				break;
			case 1:
				m_Points_12_1 = new LASPoint_12_1[numPoints];
				break;
			case 2:
				m_Points_12_2 = new LASPoint_12_2[numPoints];
				break;
			case 3:
				m_Points_12_3 = new LASPoint_12_3[numPoints];
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			m_NumPoints = numPoints;
			m_Header_12.NumberOfPointRecords = numPoints;
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	//
	//Count number of points by return and Min/Max X and Y coordinates
	//
	ErrorMsg RecalNumberOfPointsByReturnAndMinMaxValues()
	{
		//
		//Count number of points by return
		//
		switch(m_Version)
		{
		case V10:
			for(unsigned int i=0; i<5; i++)m_Header_10.NumberOfPointsbyReturn[i] = 0;

			switch(m_FormatType)
			{
			case 0:
				for(unsigned long i=0; i<m_NumPoints; i++)
				{
					unsigned char idx = m_Points_10_0[i].ReturnNumber - 1;
					if(idx>=0 && idx<5)
						m_Header_10.NumberOfPointsbyReturn[idx] ++;
				}
				break;
			case 1:
				for(unsigned long i=0; i<m_NumPoints; i++)
				{
					unsigned char idx = m_Points_10_1[i].ReturnNumber - 1;
					if(idx>=0 && idx<5)
						m_Header_10.NumberOfPointsbyReturn[idx] ++;
				}
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			for(unsigned int i=0; i<5; i++)m_Header_12.NumberOfPointsbyReturn[i] = 0;

			switch(m_FormatType)
			{
			case 0:
				for(unsigned long i=0; i<m_NumPoints; i++)
				{
					unsigned char idx = m_Points_12_0[i].ReturnNumber - 1;
					if(idx>=0 && idx<5)
						m_Header_12.NumberOfPointsbyReturn[idx] ++;
				}
				break;
			case 1:
				for(unsigned long i=0; i<m_NumPoints; i++)
				{
					unsigned char idx = m_Points_12_1[i].ReturnNumber - 1;
					if(idx>=0 && idx<5)
						m_Header_12.NumberOfPointsbyReturn[idx] ++;
				}
				break;
			case 2:
				for(unsigned long i=0; i<m_NumPoints; i++)
				{
					unsigned char idx = m_Points_12_2[i].ReturnNumber - 1;
					if(idx>=0 && idx<5)
						m_Header_12.NumberOfPointsbyReturn[idx] ++;
				}
				break;
			case 3:
				for(unsigned long i=0; i<m_NumPoints; i++)
				{
					unsigned char idx = m_Points_12_3[i].ReturnNumber - 1;
					if(idx>=0 && idx<5)
						m_Header_12.NumberOfPointsbyReturn[idx] ++;
				}
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		//
		//Min and Max
		//
		double X, Y, Z, Min_X, Max_X, Min_Y, Max_Y;

		this->GetXYZ(0, &X, &Y, &Z);
		Min_X = Max_X = X;
		Min_Y = Max_Y = Y;

		for(unsigned long i=1; i<m_NumPoints; i++)
		{
			this->GetXYZ(i, &X, &Y, &Z);

			if(Min_X > X) Min_X = X;
			if(Max_X < X) Max_X = X;

			if(Min_Y > Y) Min_Y = Y;
			if(Max_Y < Y) Max_Y = Y;
		}

		switch(m_Version)
		{
		case V10:
			m_Header_10.Min_X = Min_X;
			m_Header_10.Max_X = Max_X;
			m_Header_10.Min_Y = Min_Y;
			m_Header_10.Max_Y = Max_Y;
			break;
			m_Header_12.Min_X = Min_X;
			m_Header_12.Max_X = Max_X;
			m_Header_12.Min_Y = Min_Y;
			m_Header_12.Max_Y = Max_Y;
		case V12:
			break;
		}

		return ERROR_FREE;
	}

	//
	//Save current data as a new las
	//
	ErrorMsg SaveAs(const char* OutLASFilePath)
	{
		std::fstream NewLASFile;
		NewLASFile.open(OutLASFilePath, std::ios::out | std::ios::binary);
		if(!NewLASFile)
			return LASFILE_WRITE_OPEN_ERROR;

		//
		//Recalculate "OffsettoData"
		//
		unsigned long sizeofheader = sizeof(VLR)*m_NumVLR;

		for(unsigned long i=0; i<m_NumVLR; i++)
		{
			sizeofheader += m_DataSizeAfterVLR[i];
		}

		switch(m_Version)
		{
		case V10:
			sizeofheader += sizeof(m_Header_10);
			sizeofheader += 2;// Point start signature
			break;
		case V12:
			sizeofheader += sizeof(m_Header_12);
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		//
		//Write Header Data
		//
		switch(m_Version)
		{
		case V10:
			m_Header_10.OffsettoData = sizeofheader;
			m_Header_10.WriteHeader(NewLASFile);
			break;
		case V12:
			m_Header_12.OffsettoData = sizeofheader;
			m_Header_12.WriteHeader(NewLASFile);
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		//
		//Write VLR
		//
		//
		unsigned long m_VLRSize = sizeof(VLR);

		for(unsigned long i=0; i<m_NumVLR; i++)
		{
			NewLASFile.write((char*)&m_VLR[i], m_VLRSize);

			NewLASFile.write((char*)m_AfterVLR[i], m_DataSizeAfterVLR[i]);
		}

		//
		// Point start signature♥
		//
		if(m_Version == V10)
		{
			NewLASFile.write((char*)&PointDataStartSignature, 2);
		}

		//
		//Write Point Data
		//
		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				NewLASFile.write((char*)m_Points_10_0, sizeof(LASPoint_10_0)*m_NumPoints);
				break;
			case 1:
				NewLASFile.write((char*)m_Points_10_1, sizeof(LASPoint_10_1)*m_NumPoints);
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				NewLASFile.write((char*)m_Points_12_0, sizeof(LASPoint_12_0)*m_NumPoints);
				break;
			case 1:
				NewLASFile.write((char*)m_Points_12_1, sizeof(LASPoint_12_1)*m_NumPoints);
				break;
			case 2:
				NewLASFile.write((char*)m_Points_12_2, sizeof(LASPoint_12_2)*m_NumPoints);
				break;
			case 3:
				NewLASFile.write((char*)m_Points_12_3, sizeof(LASPoint_12_3)*m_NumPoints);
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		NewLASFile.close();

		return ERROR_FREE;
	}

	//
	//Point copy from source data
	//
	ErrorMsg PointCopyFromSource(unsigned long index, unsigned long source_index, CSMLASProvider &source, bool bCheckVersionIndex = true)
	{
		if(bCheckVersionIndex == true)
		{
			if( (m_Version != source.m_Version) | (m_FormatType != source.m_FormatType) )
				return DIFFERENT_TYPE_ERROR;

			if(index >= m_NumPoints)
				return POINT_INDEX_ERROR;

			if(source_index >= source.m_NumPoints)
				return POINT_INDEX_ERROR;
		}

		//
		//Copy Point Data
		//
		switch(m_Version)
		{
		case V10:
			switch(m_FormatType)
			{
			case 0:
				memcpy(&(m_Points_10_0[index]), &(source.m_Points_10_0[source_index]), sizeof(LASPoint_10_0));
				break;
			case 1:
				memcpy(&(m_Points_10_1[index]), &(source.m_Points_10_1[source_index]), sizeof(LASPoint_10_1));
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		case V12:
			switch(m_FormatType)
			{
			case 0:
				memcpy(&(m_Points_12_0[index]), &(source.m_Points_12_0[source_index]), sizeof(LASPoint_12_0));
				break;
			case 1:
				memcpy(&(m_Points_12_1[index]), &(source.m_Points_12_1[source_index]), sizeof(LASPoint_12_1));
				break;
			case 2:
				memcpy(&(m_Points_12_2[index]), &(source.m_Points_12_2[source_index]), sizeof(LASPoint_12_2));
				break;
			case 3:
				memcpy(&(m_Points_12_3[index]), &(source.m_Points_12_3[source_index]), sizeof(LASPoint_12_3));
				break;
			default:
				return NOT_SUPPORTED_FORMAT;
			}
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	static void CopyHeader(LASHeader_12 &dst, LASHeader_10 &src)
	{
		memcpy(dst.FileSignature, src.FileSignature, 4);
		dst.FileSourceID = 0;
		dst.GlobalEncoding = 0;
		dst.GUIDdata_1						= src.GUIDdata_1;
		dst.GUIDdata_2						= src.GUIDdata_2;
		dst.GUIDdata_3						= src.GUIDdata_3;
		memcpy(dst.GUIDdata_4, src.GUIDdata_4, 8);
		dst.VersionMajor					= src.VersionMajor;
		dst.VersionMinor					= src.VersionMinor;
		memcpy(dst.SystemIdentifier, src.SystemIdentifier, 32);
		memcpy(dst.GeneratingSoftware, src.GeneratingSoftware, 32);
		dst.FileCreationDayOfYear			= src.FlightDateJulian;
		dst.FileCreationYear				= src.Year;
		dst.HeaderSize						= sizeof(LASHeader_12);
		dst.OffsettoData					= src.OffsettoData;
		dst.NumberOfVariableLengthRecords	= src.NumberOfVariableLengthRecords;
		dst.PointDataFormatID				= src.PointDataFormatID;
		dst.PointDataRecordLength			= src.PointDataRecordLength;
		memcpy(dst.NumberOfPointsbyReturn, src.NumberOfPointsbyReturn, 20);
		dst.ScaleFactor_X					= src.ScaleFactor_X;
		dst.ScaleFactor_Y					= src.ScaleFactor_Y;
		dst.ScaleFactor_Z					= src.ScaleFactor_Z;
		dst.Offset_X						= src.Offset_X;
		dst.Offset_Y						= src.Offset_Y;
		dst.Offset_Z						= src.Offset_Z;
		dst.Max_X							= src.Max_X;
		dst.Min_X							= src.Min_X;
		dst.Max_Y							= src.Max_Y;
		dst.Min_Y							= src.Min_Y;
		dst.Max_Z							= src.Max_Z;
		dst.Min_Z							= src.Min_Z;
	}
	
	bool AddPoints(const unsigned int numPts, const LASPoint_10_0* addedPoints, const double* scale, const double* offset)
	{
		//Temporarily save old data
		unsigned int numOldPts = m_NumPoints;
		LASPoint_10_0* temp = new LASPoint_10_0[numOldPts];
		memcpy(temp, m_Points_10_0, sizeof(LASPoint_10_0)*numOldPts);

		//Delete old data
		if (numOldPts > 0) delete[] m_Points_10_0;

		//Create new data bucket
		m_Points_10_0 = new LASPoint_10_0[numOldPts + numPts];

		//Recover the old data
		memcpy(m_Points_10_0, temp, sizeof(LASPoint_10_0)*numOldPts);

		//Add new data
		for (unsigned int i = 0; i < numPts; ++i)
		{
			m_Points_10_0[i + numOldPts].X = ( (addedPoints[i].X * scale[0] + offset[0]) - this->OffsetX ) / this->ScaleX;
			m_Points_10_0[i + numOldPts].Y = ( (addedPoints[i].Y * scale[1] + offset[1]) - this->OffsetY ) / this->ScaleY;
			m_Points_10_0[i + numOldPts].Z = ( (addedPoints[i].Z * scale[2] + offset[2]) - this->OffsetZ ) / this->ScaleZ;
		}

		m_NumPoints += numPts;
		m_Header_10.NumberOfPointRecords = m_NumPoints;

		delete[] temp;

		return true;
	}

	bool AddPoints(const unsigned int numPts, const LASPoint_10_1* addedPoints, const double* scale, const double* offset)
	{
		//Temporarily save old data
		unsigned int numOldPts = m_NumPoints;
		LASPoint_10_1* temp = new LASPoint_10_1[numOldPts];
		memcpy(temp, m_Points_10_1, sizeof(LASPoint_10_1)*numOldPts);

		//Delete old data
		if (numOldPts > 0) delete[] m_Points_10_1;

		//Create new data bucket
		m_Points_10_1 = new LASPoint_10_1[numOldPts + numPts];

		//Recover the old data
		memcpy(m_Points_10_1, temp, sizeof(LASPoint_10_1)*numOldPts);

		//Add new data
		for (unsigned int i = 0; i < numPts; ++i)
		{
			m_Points_10_1[i + numOldPts].X = ((addedPoints[i].X * scale[0] + offset[0]) - this->OffsetX) / this->ScaleX;
			m_Points_10_1[i + numOldPts].Y = ((addedPoints[i].Y * scale[1] + offset[1]) - this->OffsetY) / this->ScaleY;
			m_Points_10_1[i + numOldPts].Z = ((addedPoints[i].Z * scale[2] + offset[2]) - this->OffsetZ) / this->ScaleZ;
		}

		m_NumPoints += numPts;
		m_Header_10.NumberOfPointRecords = m_NumPoints;

		delete[] temp;

		return true;
	}

	bool AddPoints(const unsigned int numPts, const LASPoint_12_0* addedPoints, const double* scale, const double* offset)
	{
		//Temporarily save old data
		unsigned int numOldPts = m_NumPoints;
		LASPoint_12_0* temp = new LASPoint_12_0[numOldPts];
		memcpy(temp, m_Points_12_0, sizeof(LASPoint_12_0)*numOldPts);

		//Delete old data
		if (numOldPts > 0) delete[] m_Points_12_0;

		//Create new data bucket		
		m_Points_12_0 = new LASPoint_12_0[numOldPts + numPts];

		//Recover the old data
		memcpy(m_Points_12_0, temp, sizeof(LASPoint_12_0)*numOldPts);

		//Add new data
		for (unsigned int i = 0; i < numPts; ++i)
		{
			m_Points_12_0[i + numOldPts].X = ((addedPoints[i].X * scale[0] + offset[0]) - this->OffsetX) / this->ScaleX;
			m_Points_12_0[i + numOldPts].Y = ((addedPoints[i].Y * scale[1] + offset[1]) - this->OffsetY) / this->ScaleY;
			m_Points_12_0[i + numOldPts].Z = ((addedPoints[i].Z * scale[2] + offset[2]) - this->OffsetZ) / this->ScaleZ;
		}
		
		m_NumPoints += numPts;
		m_Header_12.NumberOfPointRecords = m_NumPoints;

		delete[] temp;

		return true;
	}

	bool AddPoints(const unsigned int numPts, const LASPoint_12_1* addedPoints, const double* scale, const double* offset)
	{
		//Temporarily save old data
		unsigned int numOldPts = m_NumPoints;
		LASPoint_12_1* temp = new LASPoint_12_1[numOldPts];
		memcpy(temp, m_Points_12_1, sizeof(LASPoint_12_1)*numOldPts);

		//Delete old data
		if (numOldPts > 0) delete[] m_Points_12_1;

		//Create new data bucket
		m_Points_12_1 = new LASPoint_12_1[numOldPts + numPts];

		//Recover the old data
		memcpy(m_Points_12_1, temp, sizeof(LASPoint_12_1)*numOldPts);

		//Add new data
		for (unsigned int i = 0; i < numPts; ++i)
		{
			m_Points_12_1[i + numOldPts].X = ((addedPoints[i].X * scale[0] + offset[0]) - this->OffsetX) / this->ScaleX;
			m_Points_12_1[i + numOldPts].Y = ((addedPoints[i].Y * scale[1] + offset[1]) - this->OffsetY) / this->ScaleY;
			m_Points_12_1[i + numOldPts].Z = ((addedPoints[i].Z * scale[2] + offset[2]) - this->OffsetZ) / this->ScaleZ;
		}

		m_NumPoints += numPts;
		m_Header_12.NumberOfPointRecords = m_NumPoints;

		delete[] temp;

		return true;
	}

	bool AddPoints(const unsigned int numPts, const LASPoint_12_2* addedPoints, const double* scale, const double* offset)
	{
		//Temporarily save old data
		unsigned int numOldPts = m_NumPoints;
		LASPoint_12_2* temp = new LASPoint_12_2[numOldPts];
		memcpy(temp, m_Points_12_2, sizeof(LASPoint_12_2)*numOldPts);

		//Delete old data
		if (numOldPts > 0) delete[] m_Points_12_2;

		//Create new data bucket
		m_Points_12_2 = new LASPoint_12_2[numOldPts + numPts];

		//Recover the old data
		memcpy(m_Points_12_2, temp, sizeof(LASPoint_12_2)*numOldPts);

		//Add new data
		for (unsigned int i = 0; i < numPts; ++i)
		{
			m_Points_12_2[i + numOldPts].X = ((addedPoints[i].X * scale[0] + offset[0]) - this->OffsetX) / this->ScaleX;
			m_Points_12_2[i + numOldPts].Y = ((addedPoints[i].Y * scale[1] + offset[1]) - this->OffsetY) / this->ScaleY;
			m_Points_12_2[i + numOldPts].Z = ((addedPoints[i].Z * scale[2] + offset[2]) - this->OffsetZ) / this->ScaleZ;
		}

		m_NumPoints += numPts;
		m_Header_12.NumberOfPointRecords = m_NumPoints;

		delete[] temp;

		return true;
	}

	bool AddPoints(const unsigned int numPts, const LASPoint_12_3* addedPoints, const double* scale, const double* offset)
	{
		//Temporarily save old data
		unsigned int numOldPts = m_NumPoints;
		LASPoint_12_3* temp = new LASPoint_12_3[numOldPts];
		memcpy(temp, m_Points_12_3, sizeof(LASPoint_12_3)*numOldPts);

		//Delete old data
		if (numOldPts > 0) delete[] m_Points_12_3;

		//Create new data bucket

		m_Points_12_3 = new LASPoint_12_3[numOldPts + numPts];

		//Recover the old data
		memcpy(m_Points_12_3, temp, sizeof(LASPoint_12_3)*numOldPts);

		//Add new data
		for (unsigned int i = 0; i < numPts; ++i)
		{
			m_Points_12_3[i + numOldPts].X = ((addedPoints[i].X * scale[0] + offset[0]) - this->OffsetX) / this->ScaleX;
			m_Points_12_3[i + numOldPts].Y = ((addedPoints[i].Y * scale[1] + offset[1]) - this->OffsetY) / this->ScaleY;
			m_Points_12_3[i + numOldPts].Z = ((addedPoints[i].Z * scale[2] + offset[2]) - this->OffsetZ) / this->ScaleZ;
		}

		m_NumPoints += numPts;
		m_Header_12.NumberOfPointRecords = m_NumPoints;

		delete[] temp;

		return true;
	}

	bool AddPoints(const CSMLASProvider& copy)
	{
		double scale[3], offset[3];
		scale[0] = copy.ScaleX;
		scale[1] = copy.ScaleY;
		scale[2] = copy.ScaleZ;

		offset[0] = copy.OffsetX;
		offset[1] = copy.OffsetY;
		offset[2] = copy.OffsetZ;

		if (copy.m_Version == V10)
		{
			if (copy.m_FormatType == (unsigned char)0) return AddPoints(copy.m_NumPoints, copy.m_Points_10_0, scale, offset);
			else if (copy.m_FormatType == (unsigned char)1) return AddPoints(copy.m_NumPoints, copy.m_Points_10_1, scale, offset);
			else return false;
		}
		else if (copy.m_Version == V12)
		{
			if (copy.m_FormatType == (unsigned char)0) return AddPoints(copy.m_NumPoints, copy.m_Points_12_0, scale, offset);
			else if (copy.m_FormatType == (unsigned char)1) return AddPoints(copy.m_NumPoints, copy.m_Points_12_1, scale, offset);
			else if (copy.m_FormatType == (unsigned char)2) return AddPoints(copy.m_NumPoints, copy.m_Points_12_2, scale, offset);
			else if (copy.m_FormatType == (unsigned char)3) return AddPoints(copy.m_NumPoints, copy.m_Points_12_3, scale, offset);
			else return false;
		}
		else return false;
	}

	void SetScaleOffset(const double* newScale, const double*newOffset)
	{
		switch (m_Version)
		{
		case V10:
			m_Header_10.ScaleFactor_X = newScale[0];
			m_Header_10.ScaleFactor_Y = newScale[1];
			m_Header_10.ScaleFactor_Z = newScale[2];

			m_Header_10.Offset_X = newOffset[0];
			m_Header_10.Offset_Y = newOffset[1];
			m_Header_10.Offset_Z = newOffset[2];
			break;
		case V12:
			m_Header_12.ScaleFactor_X = newScale[0];
			m_Header_12.ScaleFactor_Y = newScale[1];
			m_Header_12.ScaleFactor_Z = newScale[2];

			m_Header_12.Offset_X = newOffset[0];
			m_Header_12.Offset_Y = newOffset[1];
			m_Header_12.Offset_Z = newOffset[2];
			break;
		}
	}

	void GetMinMax(double* min, double* max)
	{
		switch (m_Version)
		{
		case V10:
			min[0] = m_Header_10.Min_X;
			min[1] = m_Header_10.Min_Y;
			min[2] = m_Header_10.Min_Z;

			max[0] = m_Header_10.Max_X;
			max[1] = m_Header_10.Max_Y;
			max[2] = m_Header_10.Max_Z;
			break;
		case V12:
			min[0] = m_Header_12.Min_X;
			min[1] = m_Header_12.Min_Y;
			min[2] = m_Header_12.Min_Z;

			max[0] = m_Header_12.Max_X;
			max[1] = m_Header_12.Max_Y;
			max[2] = m_Header_12.Max_Z;
			break;
		}
	}

	void SetMinMax(double* min, double* max)
	{
		switch (m_Version)
		{
		case V10:
			m_Header_10.Min_X = min[0];
			m_Header_10.Min_Y = min[1];
			m_Header_10.Min_Z = min[2];

			m_Header_10.Max_X = max[0];
			m_Header_10.Max_Y = max[1];
			m_Header_10.Max_Z = max[2];
			break;
		case V12:
			m_Header_12.Min_X = min[0];
			m_Header_12.Min_Y = min[1];
			m_Header_12.Min_Z = min[2];

			m_Header_12.Max_X = max[0];
			m_Header_12.Max_Y = max[1];
			m_Header_12.Max_Z = max[2];
			break;
		}
	}

private:
	void SetConfigFromHeader(LASHeader_10 &m_Header_10)
	{
		m_FormatType		= m_Header_10.PointDataFormatID;
		m_NumPoints			= m_Header_10.NumberOfPointRecords;
		m_OffsetToPointData	= m_Header_10.OffsettoData;
		ScaleX				= m_Header_10.ScaleFactor_X;
		ScaleY				= m_Header_10.ScaleFactor_Y;
		ScaleZ				= m_Header_10.ScaleFactor_Z;
		OffsetX				= m_Header_10.Offset_X;
		OffsetY				= m_Header_10.Offset_Y;
		OffsetZ				= m_Header_10.Offset_Z;
		m_HeaderSize		= m_Header_10.HeaderSize;
		m_NumVLR			= m_Header_10.NumberOfVariableLengthRecords;

		MinX = m_Header_10.Min_X;
		MinY = m_Header_10.Min_Y;
		MinZ = m_Header_10.Min_Z;

		MaxX = m_Header_10.Max_X;
		MaxY = m_Header_10.Max_Y;
		MaxZ = m_Header_10.Max_Z;

		m_VerMajor			= m_Header_10.VersionMajor;
		m_VerMinor			= m_Header_10.VersionMinor;

		if(m_VerMajor == 1)
		{
			if(m_VerMinor == 0)
			{
				m_Version = V10;
			}
			else if(m_VerMinor == 2)
			{
				m_Version = V12;
			}
		}
	}

	void SetConfigFromHeader(LASHeader_12 &m_Header_12)
	{
		m_FormatType		= m_Header_12.PointDataFormatID;
		m_NumPoints			= m_Header_12.NumberOfPointRecords;
		m_OffsetToPointData = m_Header_12.OffsettoData;
		ScaleX				= m_Header_12.ScaleFactor_X;
		ScaleY				= m_Header_12.ScaleFactor_Y;
		ScaleZ				= m_Header_12.ScaleFactor_Z;
		OffsetX				= m_Header_12.Offset_X;
		OffsetY				= m_Header_12.Offset_Y;
		OffsetZ				= m_Header_12.Offset_Z;
		m_HeaderSize		= m_Header_12.HeaderSize;
		m_NumVLR			= m_Header_12.NumberOfVariableLengthRecords;

		m_VerMajor			= m_Header_12.VersionMajor;
		m_VerMinor			= m_Header_12.VersionMinor;

		if(m_VerMajor == 1)
		{
			if(m_VerMinor == 0)
			{
				m_Version = V10;
			}
			else if(m_VerMinor == 2)
			{
				m_Version = V12;
			}
		}
	}

	void SetVLR(const unsigned long NumVLR, const VLR *pVLR, const unsigned char **pAfterVLR)
	{
		if(NumVLR > 0)
		{
			unsigned long m_VLRSize = sizeof(VLR);

			m_VLR = new VLR[m_NumVLR];
			m_DataSizeAfterVLR = new unsigned long[m_NumVLR];
			m_AfterVLR = new unsigned char*[m_NumVLR];

			for(unsigned long i=0; i<m_NumVLR; i++)
			{
				m_VLR[i] = pVLR[i];
				m_DataSizeAfterVLR[i] = pVLR[i].RecordLengthAfterHeader;

				m_AfterVLR[i] = new unsigned char[m_DataSizeAfterVLR[i]];
				memcpy(m_AfterVLR[i], pAfterVLR[i], m_DataSizeAfterVLR[i]);
			}
		}
	}
	
	void GetHeader(const CSMLASProvider &srcLAS)
	{
		m_Header_10 = srcLAS.m_Header_10;
		m_Header_12 = srcLAS.m_Header_12;
	}
};

bool AddPoints(CSMLASProvider& las0, const CSMLASProvider& addedLAS)
{
	if (las0.m_VerMajor != addedLAS.m_VerMajor) return false;
	if (las0.m_VerMinor != addedLAS.m_VerMinor) return false;

	if (false == las0.AddPoints(addedLAS)) return false;

	return true;
}

#include <vector>
void LLH2UTM(CSMLASProvider& las, std::vector<double>& E, std::vector<double>& N, std::vector<double>& U)
{
	double lat, lon, h;
	double x, y;

	E.clear();
	N.clear();
	U.clear();
	E.resize(las.m_NumPoints);
	N.resize(las.m_NumPoints);
	U.resize(las.m_NumPoints);

	for (unsigned int i = 0; i < las.m_NumPoints; ++i)
	{
		las.GetXYZ(i, &lon, &lat, &h);
		UTM(lat, lon, &x, &y);
		E[i] = x;
		N[i] = y;
		U[i] = h;
	}

	double newScale[3];
	newScale[0] = 0.001;
	newScale[1] = 0.001;
	newScale[2] = las.ScaleZ;
	double newOffset[3];
	newOffset[0] = E[0];
	newOffset[1] = N[1];
	newOffset[2] = las.OffsetZ;

	las.SetScaleOffset(newScale, newOffset);

	for (unsigned int i = 0; i < las.m_NumPoints; ++i)
	{
		las.SetXYZ(i, E[i], N[i], U[i]);
	}

	las.SetScaleOffset(newScale, newOffset);

	double min[3], max[3];
	double minUTM[3], maxUTM[3];
	las.GetMinMax(min, max);

	UTM(min[1], min[0], &minUTM[0], &minUTM[1]);
	minUTM[2] = min[2];
	UTM(max[1], max[0], &maxUTM[0], &maxUTM[1]);
	maxUTM[2] = max[2];

	las.SetMinMax(minUTM, maxUTM);
}

#pragma pack(pop)// #pragma pack(push, 1) ~ #pragma pack(pop) : handle binary data per 1 byte to error in "avoid sizeof(struct or class)"