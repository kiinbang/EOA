/*
 * Copyright (c) 2002-2005, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(_SPACEMATICS_TEMPLATE_DEM)
#define _SPACEMATICS_TEMPLATE_DEM

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CTemplateDEM
template<typename T>
class CTemplateDEM
{
	//member function
public:
	CTemplateDEM()
	{
		falsedem = -999.999;
	}
	
	CTemplateDEM(CTemplateDEM &copy)
	{
		Copy(copy);
	}
	
	CTemplateDEM& operator =(CTemplateDEM &copy)
	{
		return Copy(copy);
	}
	
	CTemplateDEM& Copy(CTemplateDEM &copy)
	{
		dwBitsSize = copy.dwBitsSize;
		FileName = copy.FileName;
		height = copy.height;
		width = copy.width;
		m_Mem = copy.m_Mem;
		offset = copy.offset;
		start_coordX = copy.start_coordX;
		start_coordY = copy.start_coordY;
		
		return *this;
	}
	
	virtual ~CTemplateDEM()
	{
		GlobalFree(m_Mem);
	}
	
	bool ReadDEM(CString DEMPath, CString DEMHDRPath)
	{
		//헤더파일 읽기
		FILE *hdr;
		hdr = fopen(DEMHDRPath,"r");
		double interval, sX, sY;
		int w, h;
		
		//width, height, offset, start_X(left), start_Y(top)
		if(EOF == fscanf(hdr,"%d",&w)) return false;
		if(EOF == fscanf(hdr,"%d",&h)) return false;
		if(EOF == fscanf(hdr,"%lf",&interval)) return false;
		if(EOF == fscanf(hdr,"%lf",&sX)) return false;
		if(EOF == fscanf(hdr,"%lf",&sY)) return false;
		
		fclose(hdr);
		
		FileName = DEMPath;
		height = h;
		width = w;
		offset = interval;
		start_coordX = sX;
		start_coordY = sY;
		
		DEMPath.MakeUpper();
		if(DEMPath.Find(".DEM") > -1) return ReadBINDEM();
		else if(DEMPath.Find(".TXT") > -1) return ReadDoubleTextXYZDEM();
		else return false;
	}

	bool ReadBINDEM()
	{
		CFile file;
		CFileException fe;
		T * pData;
		
		// 읽기 모드로 파일 열기
		if(!file.Open(FileName, CFile::modeRead|CFile::shareDenyWrite, &fe))
		{
			AfxMessageBox("파일읽기 에러");
			return 0;
		}
		
		// 파일의 길이를 구함
		dwBitsSize = file.GetLength();
		
		// 메모리 할당
		if((m_Mem = (HGLOBAL)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, (SIZE_T)dwBitsSize)) == NULL)
		{
			AfxMessageBox("메모리 할당 에러");
			return 0;
		}
		
		// 메모리 고정
		pData = (T*) ::GlobalLock((HGLOBAL) m_Mem);
		
		// 파일 읽기
		if (file.Read(pData, (UINT)dwBitsSize) != dwBitsSize) 
		{
			::GlobalUnlock((HGLOBAL) m_Mem);
			::GlobalFree((HGLOBAL) m_Mem);
			AfxMessageBox("파읽기에러");
			return 0;
		}
		
		// 메모리 풀어줌
		::GlobalUnlock((HGLOBAL) m_Mem);
		
		return 1;
	}
	
	bool CreatEmptyBuf(DWORD w, DWORD h, T value)
	{
		width = w; height = h;
		//메모리 크기 설정
		dwBitsSize = sizeof(T)*width*height;
		
		// 메모리 할당
		if((m_Mem = (HGLOBAL)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, dwBitsSize)) == NULL)
		{
			AfxMessageBox("메모리 할당 에러");
			return 0;
		}
		
		// 메모리 고정
		T *data;
		data = (T*) ::GlobalLock((HGLOBAL) m_Mem);
		
		// Initialize
		for(long j=0; j<(long)height; j++)
		{
			for(long i=0; i<(long)width; i++)
			{
				data[(height-1-j)*width + i] = value;
			}
		}
		
		// 메모리 풀어줌
		::GlobalUnlock((HGLOBAL) m_Mem);
		
		return 1;
	}
		
	inline DWORD FindIndex(DWORD x, DWORD y)
	{
		DWORD index;
		index = y*width + x;
		return index;
	}
	
	bool GetX(DWORD x, double &value)
	{
		if(x<0||x>=width) return false;
		value = start_coordX + offset*x;
		
		return true;
	}

	bool GetIndexX(double X, DWORD &value)
	{
		value = DWORD((X-start_coordX)/offset+0.5);
		if(value>=width||value<0) return false;
		
		return true;
	}
	
	bool GetY(DWORD y, double &value)
	{
		if(y<0||y>=height) return false;
		value = start_coordY - offset*y;
		return true;
	}

	bool GetIndexY(double Y, DWORD &value)
	{
		value = DWORD((start_coordY-Y)/offset+0.5);
		if(value>=height||value<0) return false;
		return true;
	}
	
	bool GetZ(DWORD x, DWORD y, T &value)
	{
		if(x<0||x>=width) return false;
		if(y<0||y>=height) return false;
		
		T *data;
		data = (T*)::GlobalLock(m_Mem);
		value =  data[FindIndex(x,y)];
		::GlobalUnlock(m_Mem);
		
		return true;
	}

	bool GetBilinearZ(double X, double Y, DWORD &indexX, DWORD &indexY, T &value)
	{
		double dindexX, dindexY;
		dindexX = (X-start_coordX)/offset;
		dindexY = (start_coordY-Y)/offset;
		//Round down
		indexX = (DWORD)(dindexX);
		indexY = (DWORD)(dindexY);
		
		//Boundary check
		if(indexX<0||indexX>=width) return false;
		if(indexY<0||indexY>=height) return false;

		T *data;
		data = (T*)::GlobalLock(m_Mem);

		//Boundary check for the bilinear interpolation
		if((indexX ==(width-1))||(indexY ==(height-1)))
		{
			value =  data[FindIndex(indexX,indexY)];
			return true;
		}
		
		//Bilinear interpolation
		double value1 =  data[FindIndex(indexX,indexY)];
		double value2 =  data[FindIndex(indexX+1,indexY)];
		double value3 =  data[FindIndex(indexX,indexY+1)];
		double value4 =  data[FindIndex(indexX+1,indexY+1)];

		double A, B, C, D;
		A = (double)(indexX + 1) - dindexX;
		B = 1 - A;
		C = (double)(indexY + 1) - dindexY;
		D = 1 - C;

		value = value1*A*C + value2*B*C + value3*A*D + value4*B*D;
		
		::GlobalUnlock(m_Mem);
		
		return true;
	}

	bool GetNearestZ(double X, double Y, DWORD &indexX, DWORD &indexY, T &value)
	{
		double dindexX, dindexY;
		dindexX = (X-start_coordX)/offset;
		dindexY = (start_coordY-Y)/offset;
		//Round
		indexX = (DWORD)(dindexX+(dindexX>0 ? 0.5:-0.5));
		indexY = (DWORD)(dindexY+(dindexY>0 ? 0.5:-0.5));
		
		//Boundary check
		if(indexX<0||indexX>=width) return false;
		if(indexY<0||indexY>=height) return false;

		T *data;
		data = (T*)::GlobalLock(m_Mem);

		value =  data[FindIndex(indexX,indexY)];

		if(value <= falsedem) return false;
		
		::GlobalUnlock(m_Mem);
		
		return true;
	}

	bool GetNearestZ(double X, double Y, DWORD &indexX, DWORD &indexY, T &value, double &DEM_X, double &DEM_Y)
	{
		double dindexX, dindexY;
		dindexX = (X-start_coordX)/offset;
		dindexY = (start_coordY-Y)/offset;
		//Round
		indexX = (DWORD)(dindexX+(dindexX>0 ? 0.5:-0.5));
		indexY = (DWORD)(dindexY+(dindexY>0 ? 0.5:-0.5));
		
		//Boundary check
		if(indexX<0||indexX>=width) return false;
		if(indexY<0||indexY>=height) return false;

		T *data;
		data = (T*)::GlobalLock(m_Mem);

		value =  data[FindIndex(indexX,indexY)];

		DEM_Y = start_coordY - offset*indexY;
		DEM_X = start_coordX + offset*indexX;

		if(value <= falsedem) return false;
		
		::GlobalUnlock(m_Mem);
		
		return true;
	}

	bool GetXYZ(DWORD x, DWORD y, double &X, double &Y, T &Z)
	{
		if(x<0||x>=width) return false;
		if(y<0||y>=height) return false;
		
		T *data;
		data = (T*)::GlobalLock(m_Mem);
		Z =  data[FindIndex(x,y)];
		::GlobalUnlock(m_Mem);

		Y = start_coordY - offset*y;

		X = start_coordX + offset*x;
		
		return true;
	}
	
	bool SetZ(DWORD x, DWORD y, T value)
	{
		if(x<0||x>=width) return false;
		if(y<0||y>=height) return false;

		T *data;
		data = (T *)::GlobalLock(m_Mem);
		data[FindIndex(x,y)] = value;
		::GlobalUnlock(m_Mem);
		
		return true;
	}

	bool ReadDoubleTextXYZDEM()
	{
		//메모리 크기 설정
		dwBitsSize = sizeof(double)*width*height;
		
		// 메모리 할당
		if((m_Mem = (HGLOBAL)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, (SIZE_T)dwBitsSize)) == NULL)
		{
			AfxMessageBox("메모리 할당 에러");
			return 0;
		}
		
		// 메모리 고정
		double *data;
		data = (double*) ::GlobalLock((HGLOBAL) m_Mem);
		
		// Text DEM 파일 읽기
		FILE *file;
		file = fopen(FileName, "r");
		double X, Y; double Z;
		
		for(long j=0; j<(long)height; j++)
		{
			for(long i=0; i<(long)width; i++)
			{
				if(EOF == fscanf(file,"%lf",&X)) break;
				if(EOF == fscanf(file,"%lf",&Y)) break;
				if(EOF == fscanf(file,"%lf",&Z)) break;

				if(Z > 1.0e5) Z = -9999.0; //<--false value
				if(Z < 0.0) Z = -9999.0; //<--false value
				
				data[j*width + i] = Z;
				//상하(Y축)를 뒤집어서 저장...
				//data[(height-1-j)*width + i] = Z;
			}
		}
		
		fclose(file);
		
		// 메모리 풀어줌
		::GlobalUnlock((HGLOBAL) m_Mem);
		
		return 1;
	}

	void SaveTXT2BINTDEM(CString path)
	{
		double* newdata = (double*)::GlobalLock(m_Mem);
		CFile newfile;
		CFileException fe;
		// 쓰기 모드로 파일 열기

		for(long j=0; j<(long)height; j++)
		{
			for(long i=0; i<(long)width; i++)
			{
				double Z;
				this->GetZ(i,j,Z);
				double dz = Z-newdata[j*width+i];
			}
		}
		
		if(TRUE == newfile.Open(path, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe))
		{
			newfile.Write(newdata, DWORD(width)*DWORD(height)*sizeof(double));
		
			newfile.Close();
		}

		// 메모리 풀어줌
		::GlobalUnlock((HGLOBAL) m_Mem);
	}

	void ExportShortTDEM(CString path)
	{
		//메모리 설정
		short *newdata;
		newdata = new short[width*height];
		
		// copy
		for(long j=0; j<(long)height; j++)
		{
			for(long i=0; i<(long)width; i++)
			{
				T value;
				if(false == GetZ(i, j, value)) continue;
				newdata[j*width + i] = (short)(value+0.5);
			}
		}
		
		CFile newfile;
		CFileException fe;
		// 쓰기 모드로 파일 열기
		if(TRUE == newfile.Open(path, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe))
		{
			newfile.Write(newdata, DWORD(width)*DWORD(height)*sizeof(short));
		
			newfile.Close();
		}
				
		// 메모리 풀어줌
		delete[] newdata;
	}

	void SaveDEM(CString path)
	{
		T *newdata;
		newdata = (T*) ::GlobalLock((HGLOBAL) m_Mem);

		CFile newfile;
		CFileException fe;
		// 쓰기 모드로 파일 열기
		if(TRUE == newfile.Open(path, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe))
		{
			newfile.Write(newdata, DWORD(width)*DWORD(height)*sizeof(T));
		
			newfile.Close();
		}

		// 메모리 풀어줌
		::GlobalUnlock((HGLOBAL) m_Mem);
	}

	void DEMInverse(CString path)
	{
		//메모리 크기 설정
		DWORD memorysize = sizeof(T)*width*height;
		HGLOBAL new_Mem;//메모리핸들
		
		// 메모리 할당
		if((new_Mem = (HGLOBAL)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, memorysize)) == NULL)
		{
			AfxMessageBox("메모리 할당 에러");
			return;
		}

		// 메모리 고정
		T *newdata;
		newdata = (T*) ::GlobalLock((HGLOBAL) new_Mem);
		
		// copy
		long i, j;
		for(j=0; j<(long)height; j++)
		{
			for(i=0; i<(long)width; i++)
			{
				T value;
				if(false == GetZ(i, j, value)) continue;
				newdata[j*width + i] = value;
			}
		}

		// Inverse
		for(j=0; j<(long)height; j++)
		{
			for(i=0; i<(long)width; i++)
			{
				T value = newdata[(height-1-j)*width + i];
				if(false == SetZ(i,j,value)) continue;
			}
		}

		SaveDEM(path);
	}

private:
	//member variable
public:
	CString FileName;//파일 이름
	//DWORD dwBitsSize;//DEM size
	ULONGLONG dwBitsSize;//DEM size
	HGLOBAL m_Mem;//메모리핸들
	double start_coordX;//시작좌표(X)
	double start_coordY;//시작좌표(Y)
	double offset;//DEM 간격
	DWORD width, height;//DEM width and height
	double falsedem;//false dem value
	
};

#endif // !defined(_SPACEMATICS_TEMPLATE_DEM)
