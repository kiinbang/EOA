//////////////////////////////////////
//SpacematicsModel.h(RO에서 AO까지)
//made by BbaraB
//revision date 2000-6-15
//revision date 2001-3-16
//revision date 2001-5-1 Model class 복사생성자, 대입연산자 추가
//revision date 2002-10-29 종속적 상대표정 기존소스에서 복사하여 붙여 넣음.
///////////////////////////////////////
#ifndef __SPACEMATICS_MODEL_H__
#define __SPACEMATICS_MODEL_H__

#include "Collinearity.h"
#include "Transformation.h"
#include "SpacematicsPhoto.h"

//////////////
//절대표정 초기값 계산부분 수정중 멈춤...
//모델 좌표 계산을 위한 공간전방교회...모듈 삽입이 필요
/////////////////////////////////////////////////////////

//////////////////////////////
//class for RO_EQ Coefficient
//////////////////////////////
class coefficient
{
public:
	double q, r, s;
	double b11, b12, b13, b14, b15, b16;
	double b21, b22, b23, b24, b25, b26;
	double J, K;
};

/////////////////////////
//class for AO Parameter
/////////////////////////
class CAOParameter
{
public:
	double S, omega, phi, kappa;//Scale, Rotation
	Matrix<double> T;//Origin Transfer
	double Lomega, Lphi, Lkappa;//Left Photo Rotation
	double Romega, Rphi, Rkappa;//Right Photo Rotation
	double Xl,Yl,Zl;//Left Photo Perspective Center
	double Xr,Yr,Zr;//Right Photo Perspective Center
};

///////////////////////////
//class for Compute Matrix
///////////////////////////
class Cal_Matrix
{
public:
	Matrix<double> A, L, X, V, N, Ninv, VTV, Var_Co;
	Matrix<double> B, We, Ve;
	Matrix<double> Var;
	double pos_var, SD;
	double maxX;//maximum element fo X matrix
	int DF;
};
////////////////////////////////////////////
//class for Relative Orientation Parameters
////////////////////////////////////////////
class CROParameter
{
public:
	CSMStr ID;							//Parameter ID
	CSMStr LPhotoID, RPhotoID;			//L & R Photo ID
	double Lomega, Lphi, Lkappa;		//Left Photo Rotation Angle
	double Xl, Yl, Zl;					//Left Photo PC Coordinate
	double Romega, Rphi, Rkappa;		//Right Photo Rotation Angle
	double Xr, Yr, Zr;					//Right Photo PC Coordinate
};
//////////////////
//class for model
//////////////////
class CModel
{
public:
	//Data & Member Variable
	int ID;						//model ID
	CSMStr Name;				//model Name

	//4 column: ID, X, Y, Z
	Matrix<double> ROMPoint;	//RO Model Point(Matrix)
	//4 column: ID, X, Y, Z
	Matrix<double> AOMPoint;	//AO Model Point(Matrix)
	int num_GCP;				//Number of GCP	
	Matrix<double> GCP;			//GCP Point(Matrix)
	int num_ROPoint;			//Number Of RO Point
	CROParameter Param_de;		//RO Parameter(dependent)
	CROParameter Param_inde;	//RO Parameter(independent)
	CAOParameter Param_AO;		//RO Parameter(AO)
	coefficient b;				//RO_EQ Coefficient
	Cal_Matrix mat;				//matrix for RO compute
	int maxIteration;			//maximum iteration limit
	double maxCorrection;		//maximum correction term limit
	double maxSD;				//standard deviation limits
	double diffVar;				//variance dirrerence between old_variance and new_variance
	double maxdiffVar;			//maximum variance dirrerence between old_variance and new_variance limit
	enum optionApproximation {PARALLAXEQ, LPHOTOCOORDINATE, RPHOTOCOORDINATE, TWOPHOTOMEAN}; //Option For Ga(model space 3D-coord) Approximation
	enum optionApproximation GaApproximation_Option;
	enum optionApproximation ParallaxEQ_Option;
	//enum ROMethod {INDEPENDENT,DEPENDENT};
	//ROMethod romethod;			//RO Method (dependent, independent)
	CSMStr result;				//string for resut out
	BOOL ROComplete;			//Is RO a success?
	BOOL ROMethod_inde, ROMethod_de;//RO Method (dependent, independent)
	BOOL AOComplete_General;//Is AO a success?

private:
	CPhoto LPhoto, RPhoto;		//Photo(pair)
	
public:
	//Constructor
	CModel();
	CModel(CModel &copy);
	void operator = (CModel &copy);
	CModel(char* infile);
	CModel(int modelID, CSMStr modelName, CPhoto L, CPhoto R, int maxi = 10, double correction = 0.000001, double sd = 0.000001, double diffvar = 0.0);
	~CModel();
	
	//Member Function for Relative Orientation
	void SetLeftPhoto(CPhoto L) { LPhoto = L; } //Set Left Photo
	void SetRightPhoto(CPhoto R) { RPhoto = R; } //Set Right Photo
	CPhoto GetLeftPhoto(void) { return LPhoto; } //Get Left Photo
	CPhoto GetRightPohto(void) { return RPhoto; } //Get Right Photo
	BOOL RO_independent(void);		//Independent Method
	BOOL RO_dependent(void);		//Dependent Method
	BOOL RO_dependent(double LO, double LP, double LK, double LX, double LY, double LZ);		//Dependent Method
	BOOL IndependentTODependent(void);//From Independent To Dependent
	BOOL DependentTOIndependent(void);//From Dependent To Independent
	//Member Function for Absolute Orientation
	BOOL AO_General(void);
	//File out
	void FilePrintResult(char* outfile);	//Result File Print
	
private:
	void InitApp_inde(void);			//Initial Approximation Function(independent RO)
	void InitApp_de(void);				//Initial Approximation Function(dependent RO)
	void InitApp_de(double O, double P, double K, double Lx, double Ly, double Lz);//Initial Approximation Function(dependent RO)
	void make_A_L_RO_independent(void);	//Make A & L Matrix For RO Independent
	void make_A_L_RO_dependent(void);	//Make A & L Matrix For RO Dependent
	void InitApp_AO(void);				//Initial Approximation Function AO Parameter
	void make_A_L_B_AO_General(void);	//Make A & L & B Matrix For AO
	BOOL CalPhotoAttitude(void);		//calculate photo rotation and perspective center
	Point3D<double> FindMPoint(int n);	//Find Model Point
	CSMStr PrintResult_inde();			//Store Compute_Result to string(RO Independent)
	CSMStr PrintResult_de();			//Store Compute_Result to string(RO Dependent)
	CSMStr PrintResult_AO_General();	//Store Compute_Result to string(AO General)
	CSMStr PrintResult_AO_Parameter();	//Store Compute_Result to string(AO Parameter)
	
};

#endif