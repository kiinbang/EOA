#ifndef _ABS_GHAPI
#define _ABS_GHAPI 

#include "./ANN/ANN.h"
#include <afxtempl.h>

#define STR_LEN 200

extern double detx;
extern double dety;
extern double detz;
extern double det;

extern double XT;
extern double YT;
extern double ZT;
extern double S;
extern double om;
extern double phi;
extern double kap;
extern double **R;

extern double DX, DY, DZ;
extern double delta_x, delta_y, delta_z;

extern double Distance_Threshold;
extern double Angle_Threshold;
extern char	  dir_name[200];

typedef struct
{
	char	pt_id[20];
	double	x;
	double	y;
	double	z;
} P_SPI;

typedef struct
{
	char	patch_id[20];
	int		WTXY;	
	int		WTZ;	
	double	X[3];
	double	Y[3];
	double	Z[3];
	double	min_x;
	double  min_y;
	double  max_x;
	double  max_y;
} SPII;

// build the vertex structure;
typedef struct
{
	double x;
	double y;
	double z;
	int    Patch_ID;
}VERTEX;

extern	P_SPI	*p_sp1, *p_sp2;
extern	SPII	*sp2;
extern	int		no_of_p_sp1, no_of_p_sp2;
extern	int		no_of_sp2;

extern	double	xa, ya, za;
extern	double	XA, YA, ZA;
extern	double	Xa, Ya, Za;
extern	double	BZ1, BZA, BZB, BZC;
extern  double	BX1, BXA, BXB, BXC;
extern  double	BY1, BYA, BYB, BYC;
extern	double	V1[3], V2[3], V3[3];

int		abs_ort			(char *file_name, FILE *inp, FILE *acc, FILE *inp1, FILE *inp2, FILE *out);
int		abs_ort_icpoint	(char *file_name, FILE *inp, FILE *inp1, FILE *inp2, FILE *out);	
int		abs_ort_icpatch	(char *file_name, FILE *inp, FILE *inp1, FILE *inp2, FILE *out);	
int     abs_ort_icpatch_KdTree(char *file_name, FILE *inp, FILE *inp1, FILE *inp2, FILE *out, FILE *out_transformed);
int     abs_ort_icpoint_KdTree(char *file_name, FILE *inp, FILE *inp1, FILE *inp2,  FILE *out, FILE *out_transformed, double &var, double &NDIST);

int		rotation	(void);
int		compute_det (void);
int		compute_BZ	(void);
int		compute_BY	(void);
int		compute_BX	(void);

int     rotation_Matrix(double omega, double phi, double kappa, double R[9]);

int		match_XT	(FILE *out, FILE *fpx, FILE *fpx_a, int flag);
int		match_YT	(FILE *out, FILE *fpy, FILE *fpy_a, int flag);
int		match_ZT	(FILE *out, FILE *fpz, FILE *fpz_a, int flag);
int		match_S		(FILE *out, FILE *fps, FILE *fps_a, int flag);
int		match_om	(FILE *out, FILE *fpo, FILE *fpo_a, int flag);
int		match_phi	(FILE *out, FILE *fpp, FILE *fpp_a, int flag);
int		match_kap	(FILE *out, FILE *fpk, FILE *fpk_a, int flag);

double	solve_XT			(void);
double	solve_YT			(void);
double	solve_ZT			(void);
double	solve_S				(void);
double	solve_om			(void);
double	solve_phi			(void);
double	solve_kap			(void);

int		compute_XYZA		(void);
int		compute_XYZa		(void);

int		prj_fnames			(char *i_name);
int		prj_dir				(char *i_name);

double	check_coplanarity	(void);
double	Compute_In_Out		(void);
int  	Compute_In_Out_NTH	(double *Normal_distance, double *th);
double	Compute_Normal_Distance (void);

int		find_max			(int *vec, int n);
int		find_min_max		(int	i);
int		ZERO_ARRAY			(int *vec, int n);

int compare_x(const void*a, const void*b);
int compare_y(const void*a, const void*b);
int compare_z(const void*a, const void*c);

extern	double	XT_O, YT_O, ZT_O, S_O, om_O, phi_O, kap_O;

extern	double	XT_MIN, YT_MIN, ZT_MIN, S_MIN, om_MIN, phi_MIN, kap_MIN;
extern	double	XT_MAX, YT_MAX, ZT_MAX, S_MAX, om_MAX, phi_MAX, kap_MAX;

extern	double	XT_pix_size, YT_pix_size, ZT_pix_size, S_pix_size, om_pix_size, phi_pix_size, kap_pix_size;
extern	int		XT_no_pixels, YT_no_pixels, ZT_no_pixels, S_no_pixels, om_no_pixels, phi_no_pixels, kap_no_pixels;     
extern	int		*XT_array, *YT_array, *ZT_array, *S_array, *om_array, *phi_array, *kap_array;

extern	int		ITX, ITY, ITZ, ITS, ITO, ITP, ITK;
      
double	**dmatrix		( int nrl , int nrh , int ncl , int nch);
void	free_dmatrix	(double **m , int nrl , int nrh , int ncl , int nch);
void	mult_AV		( double **a , double *y , double *c , int n , int m);
void	mult_ATV	( double **a , double *y , double *c , int n , int m);
void	mult_ABT ( double **a , double **b , double **abt , int n , int m , int k );
void	mult_ATB ( double **a , double **b , double **atb , int n , int m , int k );


void	mult_VVT (double *A, double *B, double **C, int n);
void	mult_VTV (double *A, double *B, double *C, int n);

void	dsvdcmp  (double **a, int m, int n, double *w, double **v);
double	pythag( double a, double  b);

double	Compute_Parameters			(FILE *out, int t);
double	Compute_Parameters_points	(FILE *out, int t, double &NDIST);

double	Match_Surfaces     (FILE *out);
double	Match_Surfaces_a   (FILE *out);
double	Match_Surfaces_b   (FILE *out);
double	Match_Surfaces_c   (FILE *out);
double	Match_Surfaces_c   (FILE *out, ANNkd_tree* kdTree, CArray<int,int> *pTriIndexes, int k);


typedef struct
{
	int		*surf1_index;
	int		*surf2_index;
	int	count;
} MATCH_RES_S;

extern MATCH_RES_S match_res_s;


#endif

