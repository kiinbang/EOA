// Surface Patches Matching Through the Coplanarity Condition


#include	"stdafx.h"
#include	<math.h>
//#include    <afxtempl.h>

#include        "nrutil.h"

#define	RO		206265
#define	LARGE	1.0e+9

#include		"ABS_ORT.h"



double detx;
double dety;
double detz;
double det;

double XT;
double YT;
double ZT;

double S;

double om;
double phi;
double kap;

double **R = dmatrix (0, 2, 0, 2);

P_SPI	*p_sp1, *p_sp2;
SPII	*sp2;

double	xa, ya, za;
double	XA, YA, ZA;
double	Xa, Ya, Za;
double	V1[3], V2[3], V3[3];
double	BZ1, BZA, BZB, BZC;
double	BX1, BXA, BXB, BXC;
double	BY1, BYA, BYB, BYC;

int		no_of_p_sp1, no_of_p_sp2;
int		no_of_sp2;

double	XT_O  , YT_O  , ZT_O  , S_O  , om_O  , phi_O  , kap_O  ;

double	XT_MIN, YT_MIN, ZT_MIN, S_MIN, om_MIN, phi_MIN, kap_MIN;
double	XT_MAX, YT_MAX, ZT_MAX, S_MAX, om_MAX, phi_MAX, kap_MAX;

int		ITX, ITY, ITZ, ITS, ITO, ITP, ITK;

double	XT_pix_size , YT_pix_size , ZT_pix_size , S_pix_size , om_pix_size , phi_pix_size , kap_pix_size ;
int		XT_no_pixels, YT_no_pixels, ZT_no_pixels, S_no_pixels, om_no_pixels, phi_no_pixels, kap_no_pixels;     
int		*XT_array   , *YT_array   , *ZT_array   , *S_array   , *om_array   , *phi_array   , *kap_array   ;

double	Distance_Threshold;
double	Angle_Threshold;
char	dir_name[200];

double DX, DY, DZ;
double	delta_x, delta_y, delta_z;

MATCH_RES_S match_res_s;

int abs_ort (char *file_name, FILE *inp, FILE *acc, FILE *inp1, FILE *inp2, FILE *out)
{
	double  pi;
	char	line[200];
	int		no_of_it;
	int i, j;
	
	pi = 2.0 * asin(1.0);
	
	fgets  (line, 200, inp);	//Description line (Threshold Values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf",&Angle_Threshold , &Distance_Threshold  );
	
	fgets  (line, 200, inp);	//Description line (Approximate Values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf %lf %lf %lf %lf %lf",&XT  , &YT  , &ZT  , &S  , &om  , &phi  , &kap  );
	
	om	*= pi / 180.0;
	phi *= pi / 180.0;
	kap *= pi / 180.0;
	
	fgets  (line, 200, inp);	//Description line (Central Values - Should be exactly equal to the approximate values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf %lf %lf %lf %lf %lf",&XT_O, &YT_O, &ZT_O, &S_O, &om_O, &phi_O, &kap_O);
	
	om_O	*= pi / 180.0;
	phi_O	*= pi / 180.0;
	kap_O	*= pi / 180.0;
	
	fgets  (line, 200, inp);	//Description line (Min. Values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf %lf %lf %lf %lf %lf",&XT_MIN, &YT_MIN, &ZT_MIN, &S_MIN, &om_MIN, &phi_MIN, &kap_MIN);
	
	om_MIN	*= pi / 180.0;
	phi_MIN	*= pi / 180.0;
	kap_MIN	*= pi / 180.0;
	
	fgets  (line, 200, inp);	//Description line (Max. Values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf %lf %lf %lf %lf %lf",&XT_MAX, &YT_MAX, &ZT_MAX, &S_MAX, &om_MAX, &phi_MAX, &kap_MAX);
	
	om_MAX	*= pi / 180.0;
	phi_MAX	*= pi / 180.0;
	kap_MAX	*= pi / 180.0;
	
	fgets  (line, 200, inp);	//Description line (# of pixels)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%d %d %d %d %d %d %d", &XT_no_pixels, &YT_no_pixels, &ZT_no_pixels, &S_no_pixels, &om_no_pixels, &phi_no_pixels, &kap_no_pixels);
	
	XT_array	= (int *) malloc (XT_no_pixels  * sizeof(int));
	YT_array	= (int *) malloc (YT_no_pixels  * sizeof(int));
	ZT_array	= (int *) malloc (ZT_no_pixels  * sizeof(int));
	
	S_array		= (int *) malloc (S_no_pixels   * sizeof(int));
	
	om_array	= (int *) malloc (om_no_pixels  * sizeof(int));
	phi_array	= (int *) malloc (phi_no_pixels * sizeof(int));
	kap_array	= (int *) malloc (kap_no_pixels * sizeof(int));
	
	char blunder_name[50];
	
	fgets  (line, 200, inp);	//Description line (Blunder File)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%s ", blunder_name);
	
	fgets  (line, 200, acc);	//Read the Pixel size for the different arrays from ACC File
	sscanf (line, "%d", &no_of_it);
	
	double		*XT_pix_array, *YT_pix_array, *ZT_pix_array, *S_pix_array, *om_pix_array, *phi_pix_array, *kap_pix_array;
	
	XT_pix_array  = (double *) malloc (no_of_it * sizeof(double));
	YT_pix_array  = (double *) malloc (no_of_it * sizeof(double));
	ZT_pix_array  = (double *) malloc (no_of_it * sizeof(double));
	
	S_pix_array   = (double *) malloc (no_of_it * sizeof(double));
	
	om_pix_array  = (double *) malloc (no_of_it * sizeof(double));
	phi_pix_array = (double *) malloc (no_of_it * sizeof(double));
	kap_pix_array = (double *) malloc (no_of_it * sizeof(double));
	
	for (int k = 0; k < no_of_it; k++)
	{	
		fgets (line, 200, acc);
		sscanf (line, "%lf %lf %lf %lf %lf %lf %lf", &XT_pix_array[k], &YT_pix_array[k], &ZT_pix_array[k], &S_pix_array[k], &om_pix_array[k], &phi_pix_array[k], &kap_pix_array[k]);
	}
	
	char	pt_id[20], patch_id[20];
	double	x, y, z;
	double  X[3], Y[3], Z[3];
	
	no_of_p_sp1 = 0;
	no_of_sp2	= 0;
	
	//AH Warning Hard Coded Fixed Values.
	//To Avoid dealing with large numbers.
	
	DX = 700355;	
	DY = 5661844;
	DZ = 1101;
	
	delta_x = delta_y = 15.0;	//To limit the search space
	
	FILE	*pnt_dat, *tin_dat;
	char	pnt_dat_name[200], tin_dat_name[200];
	
	strcpy	(pnt_dat_name, file_name);
	prj_dir	(pnt_dat_name);
	strcat	(pnt_dat_name, "pnt.dat");
	pnt_dat = fopen(pnt_dat_name, "w");
	
	strcpy	(tin_dat_name, file_name);
	prj_dir	(tin_dat_name);
	strcat	(tin_dat_name, "tin.dat");
	tin_dat = fopen(tin_dat_name, "w");
	
	fprintf (pnt_dat, "Easting\tNorthing\tUp\n");
	fprintf (tin_dat, "Easting\tNorthing\tUp\n");
	
	//Reading the data associated with the first surface points.
	
	while (fgets(line, 200, inp1) != NULL)
	{
		no_of_p_sp1++;
		
		if (no_of_p_sp1 == 1)
			p_sp1 = (P_SPI *) malloc (no_of_p_sp1 * sizeof(P_SPI));
		else
			p_sp1 = (P_SPI *) realloc (p_sp1, no_of_p_sp1 * sizeof(P_SPI));
		
		sscanf (line, "%s %lf %lf %lf", pt_id, &x, &y, &z);
		
		i = no_of_p_sp1 - 1;
		
		strcpy (p_sp1[i].pt_id, pt_id);
		
		p_sp1[i].x = x - DX;
		p_sp1[i].y = y - DY;
		p_sp1[i].z = z - DZ;
		
		fprintf (pnt_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", p_sp1[i].x, p_sp1[i].y, p_sp1[i].z);
	};
	
	fclose (pnt_dat);
	
	//Reading The Data associated with the second surface patches.
	
	int		WTXY, WTZ;
	while (fgets(line, 200, inp2) != NULL)
	{
		no_of_sp2++;
		
		if (no_of_sp2 == 1)
			sp2 = (SPII *) malloc  (no_of_sp2 * sizeof(SPII));
		else
			sp2 = (SPII *) realloc (sp2, no_of_sp2 * sizeof(SPII));
		
		sscanf (line, "%s %d %d", patch_id, &WTXY, &WTZ);
		
		fgets (line, 200, inp2);
		sscanf (line, "%lf %lf %lf ", &X[0], &Y[0], &Z[0]);
		
		fgets (line, 200, inp2);
		sscanf (line, "%lf %lf %lf ", &X[1], &Y[1], &Z[1]);
		
		fgets (line, 200, inp2);
		sscanf (line, "%lf %lf %lf ", &X[2], &Y[2], &Z[2]);
		
		int i = no_of_sp2 - 1;
		
		strcpy (sp2[i].patch_id, patch_id);
		
		sp2[i].WTXY = WTXY;
		sp2[i].WTZ  = WTZ;
		
		sp2[i].X[0] = X[0] - DX;
		sp2[i].Y[0] = Y[0] - DY;
		sp2[i].Z[0] = Z[0] - DZ;
		
		sp2[i].X[1] = X[1] - DX;
		sp2[i].Y[1] = Y[1] - DY;
		sp2[i].Z[1] = Z[1] - DZ;
		
		sp2[i].X[2] = X[2] - DX;
		sp2[i].Y[2] = Y[2] - DY;
		sp2[i].Z[2] = Z[2] - DZ;
		
		fprintf (tin_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", sp2[i].X[0], sp2[i].Y[0], sp2[i].Z[0]);
		fprintf (tin_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", sp2[i].X[1], sp2[i].Y[1], sp2[i].Z[1]);
		fprintf (tin_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", sp2[i].X[2], sp2[i].Y[2], sp2[i].Z[2]);
	};
	
	fclose (tin_dat);
	
	// Find the range for each patch
	
	for (i = 0; i < no_of_sp2; i++)
		find_min_max (i);
	
	fprintf (out, "Number of Surface I  Points  = %12d\n", no_of_p_sp1);
	fprintf (out, "Number of Surface II Patches = %12d\n", no_of_sp2);
	fprintf (out, "__________________________________________\n");
	
	char str[200];
	
	sprintf (str, "Number of Surface I  Points  = %12d\nNumber of Surface II Patches = %12d\n", no_of_p_sp1, no_of_sp2);
	AfxMessageBox( str, MB_OK, 0);
	
	//Files for writing the accumulator arrays and the progress of the estimated parameters with iterations
	
	FILE	*fpx  , *fpy  , *fpz  , *fps  , *fpo  , *fpp  , *fpk;
	//	FILE	*fpx_a, *fpy_a, *fpz_a, *fps_a, *fpo_a, *fpp_a, *fpk_a;
	
	char	fpx_name[200];
	char	fpy_name[200];
	char	fpz_name[200];
	
	char	fps_name[200];
	
	char	fpo_name[200];
	char	fpp_name[200];
	char	fpk_name[200];
	
	char	fpx_a_name[200];
	char	fpy_a_name[200];
	char	fpz_a_name[200];
	
	char	fps_a_name[200];
	
	char	fpo_a_name[200];
	char	fpp_a_name[200];
	char	fpk_a_name[200];
	
	strcpy	(fpx_name, file_name);
	prj_dir	(fpx_name);
	strcpy  (dir_name, fpx_name);
	strcat	(fpx_name, "XT.out");
	
	strcpy	(fpy_name, file_name);
	prj_dir	(fpy_name);
	strcat	(fpy_name, "YT.out");
	
	strcpy	(fpz_name, file_name);
	prj_dir	(fpz_name);
	strcat	(fpz_name, "ZT.out");
	
	strcpy	(fps_name, file_name);
	prj_dir	(fps_name);
	strcat	(fps_name, "SF.out");
	
	strcpy	(fpo_name, file_name);
	prj_dir	(fpo_name);
	strcat	(fpo_name, "OM.out");
	
	strcpy	(fpp_name, file_name);
	prj_dir	(fpp_name);
	strcat	(fpp_name, "PH.out");
	
	strcpy	(fpk_name, file_name);
	prj_dir	(fpk_name);
	strcat	(fpk_name, "KP.out");
	
	
	strcpy	(fpx_a_name, file_name);
	prj_dir	(fpx_a_name);
	strcat	(fpx_a_name, "XT_a.out");
	
	strcpy	(fpy_a_name, file_name);
	prj_dir	(fpy_a_name);
	strcat	(fpy_a_name, "YT_a.out");
	
	strcpy	(fpz_a_name, file_name);
	prj_dir	(fpz_a_name);
	strcat	(fpz_a_name, "ZT_a.out");
	
	strcpy	(fps_a_name, file_name);
	prj_dir	(fps_a_name);
	strcat	(fps_a_name, "SF_a.out");
	
	strcpy	(fpo_a_name, file_name);
	prj_dir	(fpo_a_name);
	strcat	(fpo_a_name, "OM_a.out");
	
	strcpy	(fpp_a_name, file_name);
	prj_dir	(fpp_a_name);
	strcat	(fpp_a_name, "PH_a.out");
	
	strcpy	(fpk_a_name, file_name);
	prj_dir	(fpk_a_name);
	strcat	(fpk_a_name, "KP_a.out");
	
	fpx = fopen (fpx_name, "w");
	fpy = fopen (fpy_name, "w");
	fpz = fopen (fpz_name, "w");
	
	fps = fopen (fps_name, "w");
	
	fpo = fopen (fpo_name, "w");
	fpp = fopen (fpp_name, "w");
	fpk = fopen (fpk_name, "w");
	
	//The matching process
	int flag = 0;
	
	CTime t;
	
	double check;
	CTime time1, time2;
	
	// Beginning Time
	
	time1		= CTime::GetCurrentTime();;
	int hr1		= time1.GetHour();
	int	min1	= time1.GetMinute();
	int sec1	= time1.GetSecond();
	
	ITX = ITY = ITZ = ITS = ITO = ITP = ITK = 0;
	
	double  seg_bef = 100000.0;
	int		I_NO_IT = 0;
	
	for (int IT = 0; IT < 1; IT++)
	{
		
		int II;
		
		if (IT >= 1)
			II = 55;
		else
			II = 0;
		
		II = no_of_it - 1;		//AH 07 (no_of_it - 1)
		
		fprintf (out, "\n\n IT iteration # %4d\n", IT);
		fprintf (out, "****************************\n");
		for (int i = II; i < no_of_it; i++)
		{
			
			fprintf (out, "\n\n iteration # %4d\n", i);
			fprintf (out, "+++++++++++++++++++++\n");
			
			XT_pix_size  =  XT_pix_array[i];
			YT_pix_size  =  YT_pix_array[i];
			ZT_pix_size  =  ZT_pix_array[i];
			
			S_pix_size   =   S_pix_array[i];
			
			om_pix_size  =  om_pix_array[i];
			phi_pix_size = phi_pix_array[i];
			kap_pix_size = kap_pix_array[i];
			
			if ((IT >= 0) && (i == (no_of_it - 1)))	//AH
				flag = 1;
			else
				flag = 0;
			
				/*			
				fpx_a = fopen  (fpx_a_name, "w"); 
				ITX++;
				fprintf (fpx_a, "THE # %5d ITERTAION of XT\n", ITX);
				fprintf (fpx_a, "PIXEL NUMBER : %5d\n", XT_no_pixels);
				fprintf (fpx_a, "PIXEL SIZE : %12.5lf\n", XT_pix_size);
				fprintf (fpx_a, "THE MINIMUM XT : %12.5f\n", XT_MIN); 
				fprintf (fpx_a, "THE MAXIMUM XT : %12.5f\n", XT_MAX); 
				match_XT	(out, fpx, fpx_a, 0);
				
				  fpy_a = fopen  (fpy_a_name, "w"); 
				  ITY++;
				  fprintf (fpy_a, "THE # %5d ITERTAION of YT\n", ITY);
				  fprintf (fpy_a, "PIXEL NUMBER : %5d\n", YT_no_pixels);
				  fprintf (fpy_a, "PIXEL SIZE : %12.5lf\n", YT_pix_size);
				  fprintf (fpy_a, "THE MINIMUM YT : %12.5f\n", YT_MIN); 
				  fprintf (fpy_a, "THE MAXIMUM YT : %12.5f\n", YT_MAX); 
				  match_YT	(out, fpy, fpy_a, 0);
				  
					fpz_a = fopen  (fpz_a_name, "w"); 
					ITZ++;
					fprintf (fpz_a, "THE # %5d ITERTAION of ZT\n", ITZ);
					fprintf (fpz_a, "PIXEL NUMBER : %5d\n", ZT_no_pixels);
					fprintf (fpz_a, "PIXEL SIZE : %12.5lf\n", ZT_pix_size);
					fprintf (fpz_a, "THE MINIMUM ZT : %12.5f\n", ZT_MIN); 
					fprintf (fpz_a, "THE MAXIMUM ZT : %12.5f\n", ZT_MAX); 
					match_ZT	(out, fpz, fpz_a, 0);
					
					  fps_a = fopen  (fps_a_name, "w"); 
					  ITS++;
					  fprintf (fps_a, "THE # %5d ITERTAION of S\n", ITS);
					  fprintf (fps_a, "PIXEL NUMBER : %5d\n", S_no_pixels);
					  fprintf (fps_a, "PIXEL SIZE : %12.5lf\n", S_pix_size);
					  fprintf (fps_a, "THE MINIMUM S : %12.5f\n", S_MIN); 
					  fprintf (fps_a, "THE MAXIMUM S : %12.5f\n", S_MAX); 
					  match_S		(out, fps, fps_a, 0);
					  
						fpo_a = fopen  (fpo_a_name, "w"); 
						ITO++;
						fprintf (fpo_a, "THE # %5d ITERTAION of OM\n", ITO);
						fprintf (fpo_a, "PIXEL NUMBER : %5d\n", om_no_pixels);
						fprintf (fpo_a, "PIXEL SIZE : %12.5lf\n", om_pix_size);
						fprintf (fpo_a, "THE MINIMUM OM : %12.5f\n", om_MIN * 180.0 /pi); 
						fprintf (fpo_a, "THE MAXIMUM OM : %12.5f\n", om_MAX * 180.0 /pi); 
						match_om	(out, fpo, fpo_a, 0);
						
						  fpp_a = fopen  (fpp_a_name, "w"); 
						  ITP++;
						  fprintf (fpp_a, "THE # %5d ITERTAION of PH\n", ITP);
						  fprintf (fpp_a, "PIXEL NUMBER : %5d\n", phi_no_pixels);
						  fprintf (fpp_a, "PIXEL SIZE : %12.5lf\n", phi_pix_size);
						  fprintf (fpp_a, "THE MINIMUM PH : %12.5f\n", phi_MIN * 180.0 /pi); 
						  fprintf (fpp_a, "THE MAXIMUM PH : %12.5f\n", phi_MAX * 180.0 /pi); 
						  match_phi	(out, fpp, fpp_a, 0);
						  
							fpk_a = fopen  (fpk_a_name, "w"); 
							ITK++;
							fprintf (fpk_a, "THE # %5d ITERTAION of KP\n", ITK);
							fprintf (fpk_a, "PIXEL NUMBER : %5d\n", kap_no_pixels);
							fprintf (fpk_a, "PIXEL SIZE : %12.5lf\n", kap_pix_size);
							fprintf (fpk_a, "THE MINIMUM KP : %12.5f\n", kap_MIN * 180.0 /pi); 
							fprintf (fpk_a, "THE MAXIMUM KP : %12.5f\n", kap_MAX * 180.0 /pi); 
							match_kap	(out, fpk, fpk_a, 0);
			//*/			
			
			
			if (flag == 1)
			{
				for (int NNN = 0; NNN < 40; NNN++)
				{
					ITX++;
					ITY++;
					ITZ++;
					ITS++;
					ITO++;
					ITP++;
					ITK++;
					
					Match_Surfaces_c (out);
					// Least Squares Adjustment
					
					fprintf (out, "\n\nLeast Squares Adjustment\n");
					fprintf (out, "-------------------------\n");
					
					fprintf (out, "\n\n");
					// t == 3	Solve for XT, YT, ZT
					// t == 4	Solve for XT, YT, ZT, OM
					// t == 5	Solve for XT, YT, ZT, OM, PH
					// t == 6	Solve for XT, YT, ZT, OM, PH, KP
					// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					
					check = Compute_Parameters(out, 7);
					XT_O	= XT;
					YT_O	= YT;
					ZT_O	= ZT;
					
					S_O		= S;
					om_O	= om  * 180.0 / pi;
					phi_O	= phi * 180.0 / pi;
					kap_O	= kap * 180.0 / pi;
					
					om_O  *= pi / 180.0;
					phi_O *= pi / 180.0;
					kap_O *= pi / 180.0;
					
					fprintf (fpx,"%6d %14.4lf\n", ITX, XT);
					fprintf (fpy,"%6d %14.4lf\n", ITY, YT);
					fprintf (fpz,"%6d %14.4lf\n", ITZ, ZT);
					
					fprintf (fps,"%6d %14.4lf\n", ITS, S);
					
					fprintf (fpo,"%6d %14.4lf\n", ITO, om  * 180.0 / pi);
					fprintf (fpp,"%6d %14.4lf\n", ITP, phi * 180.0 / pi);
					fprintf (fpk,"%6d %14.4lf\n", ITK, kap * 180.0 / pi);
					
					double dseg = check - seg_bef;
					seg_bef = check;
					if (fabs (dseg) < 0.001)
						I_NO_IT++;
					else
						I_NO_IT = 0;
				}
				
			}
		}
		
		if (I_NO_IT >= 9)
			break;
		
	}
	
	fclose (fpx);
	fclose (fpy);
	fclose (fpz);
	fclose (fps);
	fclose (fpo);
	fclose (fpp);
	fclose (fpk);
	
	
	int n_m, n_nm;
	n_m = n_nm = 0;
	
	if (check != 0.0)
	{
		char fpm_name[200];
		FILE *fpm;
		
		strcpy	(fpm_name, file_name);
		prj_dir	(fpm_name);
		strcat	(fpm_name, "Matched.dat");
		
		fpm = fopen (fpm_name, "w");
		fprintf (fpm,"Easting\t\tNorthing\t\tUp\n");
		
		char fpmn_name[200];
		FILE *fpmn;
		
		strcpy	(fpmn_name, file_name);
		prj_dir	(fpmn_name);
		strcat	(fpmn_name, "NMatched.dat");
		
		fpmn = fopen (fpmn_name, "w");
		fprintf (fpmn,"Easting\t\tNorthing\t\tUp\n");
		
		char fp_nm_name[200];
		FILE *fp_nm;
		
		strcpy	(fp_nm_name, file_name);
		prj_dir	(fp_nm_name);
		strcat	(fp_nm_name, "Non_NMatched.dat");
		
		fp_nm = fopen (fp_nm_name, "w");
		fprintf (fp_nm,"Easting\t\tNorthing\t\tUp\n");
		
		char fp_mat_name[200];
		FILE *fp_mt_id;
		
		strcpy	(fp_mat_name, file_name);
		prj_dir	(fp_mat_name);
		strcat	(fp_mat_name, "Matched_IDS.out");
		
		fp_mt_id = fopen (fp_mat_name, "w");
		fprintf (fp_mt_id,"Points\t\tMatched Patches\t\tNormal Distance\n");
		
		char fp_nmat_name[200];
		FILE *fp_nmt_id;
		
		strcpy	(fp_nmat_name, file_name);
		prj_dir	(fp_nmat_name);
		strcat	(fp_nmat_name, "Non_Matched_IDS.out");
		
		fp_nmt_id = fopen (fp_nmat_name, "w");
		fprintf (fp_nmt_id,"Non Matched Points\t\tNormal Distance\t\tAngular Difference\t\tClosest Patch\n");
		
		/****/
		char fp_blunder_name[200];
		FILE *fp_blunder;
		
		strcpy	(fp_blunder_name, file_name);
		prj_dir	(fp_blunder_name);
		strcat	(fp_blunder_name, blunder_name);
		
		fp_blunder = fopen (fp_blunder_name, "r");
		typedef struct
		{
			char ID[20];
		} BLUNDER_ID;
		
		BLUNDER_ID *bld_id;
		
		bld_id = NULL;
		
		int no_of_bld = 0;
		while (fgets(line, 200, fp_blunder) != NULL)
		{
			no_of_bld++;
			if (no_of_bld == 1)
				bld_id = (BLUNDER_ID *) malloc (no_of_bld * sizeof (BLUNDER_ID));
			else
				bld_id = (BLUNDER_ID *) realloc (bld_id, no_of_bld * sizeof (BLUNDER_ID));
			
			sscanf(line, " %s %s", bld_id[no_of_bld - 1].ID, bld_id[no_of_bld - 1].ID);
		}
		fclose (fp_blunder);
		
		char fp_out_blunder_name[200];
		FILE *fp_out_blunder;
		
		strcpy	(fp_out_blunder_name, file_name);
		prj_dir	(fp_out_blunder_name);
		strcat	(fp_out_blunder_name, "out_blunder.out");
		
		fp_out_blunder = fopen (fp_out_blunder_name, "w");
		int no_found_bld = 0;
		int no_of_miss_bld = 0;
		/****/
		
		fprintf (out, "Matched Points\n");
		fprintf (out, "***************\n\n");
		
		char matched_patch[20];
		
		rotation ();
		
		double avg_dist_m, avg_dist_nm;
		
		avg_dist_m = avg_dist_nm = 0.0;
		int n_wr_mat = 0;
		
		for (i = 0; i < no_of_p_sp1; i++)
		{
			xa = p_sp1[i].x;
			ya = p_sp1[i].y;
			za = p_sp1[i].z;
			
			compute_XYZA();
			
			int flag_matched = 0;
			
			double checkA; 
			double checkB; 
			
			double dist, min_ang_diff;
			
			min_ang_diff = 10000.0;
			
			for (j = 0; j < no_of_sp2; j++)
			{
				V1[0] = sp2[j].X[0];
				V1[1] = sp2[j].Y[0];
				V1[2] = sp2[j].Z[0];
				
				V2[0] = sp2[j].X[1];
				V2[1] = sp2[j].Y[1];
				V2[2] = sp2[j].Z[1];
				
				V3[0] = sp2[j].X[2];
				V3[1] = sp2[j].Y[2];
				V3[2] = sp2[j].Z[2];
				
				checkA = 0.0;
				checkB = 0.0;
				
				Compute_In_Out_NTH (&checkA, &checkB);
				if (fabs(360.0 - checkB) < min_ang_diff)
				{
					dist = fabs(checkA);
					min_ang_diff = fabs(360.0 - checkB);
					strcpy (matched_patch, sp2[j].patch_id);
				}
				
				if (checkA < Distance_Threshold)
				{
					if (fabs(checkB - 360.0) < Angle_Threshold)
					{
						fprintf (out , "point %5s and surface patch %5s are coplananr - NDistance = %12.4lf Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, checkA, checkB);
						fprintf (fpm , "%10s %12.4lf %12.4lf %12.4lf\n", p_sp1[i].pt_id, xa, ya, za);
						fprintf (fpmn, "%10s %12.4lf %12.4lf %12.4lf\n", p_sp1[i].pt_id, XA, YA, ZA);
						int len = strlen(sp2[j].patch_id);
						int mat = strncmp(sp2[j].patch_id, p_sp1[i].pt_id, len);
						if (mat == 0)
							fprintf (fp_mt_id, "%12s\t%12s\t%12.7lf Correct Match\n", p_sp1[i].pt_id, sp2[j].patch_id, checkA);
						else
						{
							fprintf (fp_mt_id, "%12s\t%12s\t%12.7lf Wrong Match\n", p_sp1[i].pt_id, sp2[j].patch_id, checkA);
							n_wr_mat++;
						}
						flag_matched = 1;
						avg_dist_m += checkA;
						n_m++;
					}
				}
				
			}
			
			if (flag_matched == 0)
			{
				fprintf (fp_nmt_id, "There is no match for point %12s\t%12.7lf\t%12.7lf\t%12s\n", p_sp1[i].pt_id, dist, min_ang_diff, matched_patch);
				fprintf (fp_nm, "%10s %12.4lf %12.4lf %12.4lf\n", p_sp1[i].pt_id, XA, YA, ZA);
				avg_dist_nm += dist;
				n_nm++;
				int flag_found = 0;
				for (int K = 0; K < no_of_bld; K++)
				{
					if (!strcmp(p_sp1[i].pt_id, bld_id[K].ID))
					{
						fprintf (fp_out_blunder, "%20s is a TRUE Blunder\n", p_sp1[i].pt_id);
						flag_found = 1;
						no_found_bld++;
						continue;
					}
				}
				if (flag_found == 0)
				{
					fprintf (fp_out_blunder, "%20s is a Misscalssified Blunder\n", p_sp1[i].pt_id);
					no_of_miss_bld++;
				}
			}
		}
		
		fclose  (fpm );
		fclose  (fpmn);
		fprintf (fp_mt_id, "Total Number of Points = %7d\n", no_of_p_sp1); 
		fprintf (fp_mt_id, "Number of Matched Points = %7d\n", n_m);
		fprintf (fp_mt_id, "Number of Wrong Matches = %12d\n", n_wr_mat);
		fprintf (fp_mt_id, "Percentage of Matched Points = %7.3lf\n", (double) n_m / (double) no_of_p_sp1 * 100.0);
		fprintf (fp_mt_id, "Average distance = %12.7lf\n", avg_dist_m/n_m);
		fclose  (fp_mt_id);
		fprintf (fp_nmt_id, "Total Number of Points = %7d\n", no_of_p_sp1); 
		fprintf (fp_nmt_id, "Number of Non Matched Points = %7d\n", n_nm);
		fprintf (fp_nmt_id, "Percentage of Non_Matched Points = %7.3lf\n", (double) n_nm / (double) no_of_p_sp1 * 100.0);
		fprintf (fp_nmt_id, "Average distance = %12.7lf\n", avg_dist_nm/n_nm);
		fclose  (fp_nmt_id);
		fclose	(fp_nm);
		
		fprintf (fp_out_blunder, "Total Number of Points            = %7d\n"   , no_of_p_sp1);
		fprintf (fp_out_blunder, "Total Number of Blunders          = %7d\n"   , no_of_bld);
		fprintf (fp_out_blunder, "Percentage of Blunders            = %7.3lf\n", (double) no_of_bld / (double) no_of_p_sp1 * 100.0);
		fprintf (fp_out_blunder, "Number of Truly Detected Blunders = %7d\n"   , no_found_bld);
		fprintf (fp_out_blunder, "Number of Undetected Blunders     = %7d\n"   , no_of_bld - no_found_bld);
		fprintf (fp_out_blunder, "Number of Missclassified Blunders = %7d\n"   , no_of_miss_bld);
		if (no_of_bld != 0)
			fprintf (fp_out_blunder, "Percentage of Detected Blunders   = %7.3lf\n"  , (double) (no_found_bld)/ (double) (no_of_bld) * 100.0);
		fclose  (fp_out_blunder);
		
		if (bld_id != NULL)
			free (bld_id);
	}
	
	
	time2 = CTime::GetCurrentTime();;
	int hr2  = time2.GetHour();
	int	min2 = time2.GetMinute();
	int sec2 = time2.GetSecond();
	
	fprintf (out, "Starting   Time \t\t\t%2d %2d %2d\n", hr1, min1, sec1);
	fprintf (out, "Ending     Time \t\t\t%2d %2d %2d\n", hr2, min2, sec2);
	
	int dhr, dmin, dsec;
	
	if (sec2 >= sec1)
		dsec = sec2 - sec1;
	else
	{
		dsec = sec2 + 60 - sec1;
		min2 = min2 - 1;
	}
	
	if (min2 >= min1)
		dmin = min2 - min1;
	else
	{
		dmin = min2 + 60 - min1;
		hr2 = hr2 - 1;
	}
	
	if (hr2 >= hr1)
		dhr = hr2 - hr1;
	else
	{
		dhr = hr2 + 24 - hr1;
	}
	
	fprintf (out, "Processing Time \t\t\t%2d %2d %2d\n", dhr, dmin, dsec);
	
	fclose (inp );
	fclose (acc );
	
	fclose (inp1);
	fclose (inp2);
	
	fclose (out );
	
	free ( XT_array);
	free ( YT_array);
	free ( ZT_array);
	free (  S_array);
	free ( om_array);
	free (phi_array);
	free (kap_array);
	
	free_dmatrix (R, 0, 2, 0, 2);
	
	free (sp2);
	free (p_sp1);
	
	free ( XT_pix_array);
	free ( YT_pix_array);
	free ( ZT_pix_array);
	free (  S_pix_array);
	free ( om_pix_array);
	free (phi_pix_array);
	free (kap_pix_array);
	
	return (1);
}

double Match_Surfaces (FILE *out)
{
	//NOTE: we are allowing multiple matches
	int		i, j;
	
	rotation ();
	
	match_res_s.count = 0;
	
	/*******************/
	
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();
			
			compute_XYZA();
			compute_XYZa();		//no scale or shift is applied 
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			check =  Compute_Normal_Distance();
			check2 = check_coplanarity(); 
			check1 = Compute_In_Out();
			
			if ((fabs(check) > Distance_Threshold) || (fabs(check1 - 360.0) > Angle_Threshold))
				continue;
			
			fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, check1);
			fprintf (out, "Normal Distance   = %12.4lf\n", check);
			fprintf (out, "Coplanarity Check = %12.4lf\n", check2);
			fprintf (out, "Compute In Out = %12.4lf\n", check1);
			
			match_res_s.count++;
			if (match_res_s.count == 1)
			{
				match_res_s.surf1_index = (int *) malloc (match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) malloc (match_res_s.count * sizeof(int));
			}
			else
			{
				match_res_s.surf1_index = (int *) realloc (match_res_s.surf1_index, match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) realloc (match_res_s.surf2_index, match_res_s.count * sizeof(int));
			}
			
			int I =  match_res_s.count - 1;
			match_res_s.surf1_index[I] = i;
			match_res_s.surf2_index[I] = j;
		}
	}
	
	return (1.0);
}


double Match_Surfaces_a (FILE *out)
{
	//NOTE: we are not allowing multiple matches
	int		i, j;
	double	CHECK, CHECK1, CHECK2;
	
	rotation ();
	match_res_s.count = 0;
	
	/*******************/
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		double	min_ang_dif = 50.0;
		int		patch_id;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();
			
			compute_XYZA();
			compute_XYZa();		//no scale or shift is applied 
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			check =  Compute_Normal_Distance();
			check2 = check_coplanarity(); 
			check1 = Compute_In_Out();
			
			if (fabs(check1 - 360.0) < min_ang_dif)
			{
				min_ang_dif = fabs(check1 - 360.0);
				patch_id = j;
				CHECK1 = check1;
				CHECK  = check;
				CHECK2 = check2;
			}
		}
		
		if ((min_ang_dif < Angle_Threshold) && (fabs(CHECK) < Distance_Threshold))
		{
			fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[patch_id].patch_id, CHECK1);
			fprintf (out, "Normal Distance   = %12.4lf\n", CHECK);
			fprintf (out, "Coplanarity Check = %12.4lf\n", CHECK2);
			fprintf (out, "Compute In Out = %12.4lf\n", CHECK1);
			
			match_res_s.count++;
			if (match_res_s.count == 1)
			{
				match_res_s.surf1_index = (int *) malloc (match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) malloc (match_res_s.count * sizeof(int));
			}
			else
			{
				match_res_s.surf1_index = (int *) realloc (match_res_s.surf1_index, match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) realloc (match_res_s.surf2_index, match_res_s.count * sizeof(int));
			}
			
			int I =  match_res_s.count - 1;
			match_res_s.surf1_index[I] = i;
			match_res_s.surf2_index[I] = patch_id;
		}
		
	}
	
	return (1.0);
}

double Match_Surfaces_b (FILE *out)
{
	//NOTE: we are not allowing multiple matches
	int		i, j;
	double	CHECK, CHECK1, CHECK2;
	
	rotation ();
	match_res_s.count = 0;
	
	/*******************/
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		double	min_dist_dif = 50.0;
		int		patch_id;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();
			
			compute_XYZA();
			compute_XYZa();		//no scale or shift is applied 
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			double checkA = 0.0;
			double checkB = 0.0;
			
			check =  Compute_Normal_Distance();
			check2 = check_coplanarity(); 
			check1 = Compute_In_Out();
			
			Compute_In_Out_NTH (&checkA, &checkB);
			
			if (!strcmp(p_sp1[i].pt_id, "1_8") && !strcmp(sp2[j].patch_id, "1"))
				double test = 1.0;
			
			if ((fabs(checkA) < min_dist_dif) && (fabs(checkB - 360) < Angle_Threshold))
			{
				min_dist_dif = fabs(checkA);
				patch_id = j;
				CHECK1 = checkB;
				CHECK  = checkA;
				CHECK2 = check2;
			}
		}
		
		if ((min_dist_dif < Distance_Threshold) && (fabs(CHECK1 - 360) < Angle_Threshold))
		{
			fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[patch_id].patch_id, CHECK1);
			fprintf (out, "Normal Distance   = %12.4lf\n", CHECK);
			fprintf (out, "Coplanarity Check = %12.4lf\n", CHECK2);
			fprintf (out, "Compute In Out = %12.4lf\n", CHECK1);
			
			match_res_s.count++;
			if (match_res_s.count == 1)
			{
				match_res_s.surf1_index = (int *) malloc (match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) malloc (match_res_s.count * sizeof(int));
			}
			else
			{
				match_res_s.surf1_index = (int *) realloc (match_res_s.surf1_index, match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) realloc (match_res_s.surf2_index, match_res_s.count * sizeof(int));
			}
			
			int I =  match_res_s.count - 1;
			match_res_s.surf1_index[I] = i;
			match_res_s.surf2_index[I] = patch_id;
		}
		
	}
	
	return (1.0);
}

double Match_Surfaces_c (FILE *out)
{
	//NOTE: we are not allowing multiple matches
	int		i, j;
	double	CHECK, CHECK1;
	
	rotation ();
	match_res_s.count = 0;
	
	/*******************/
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		compute_XYZA();
		
		double	min_dist_dif = 50.0;
		int		patch_id;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			double checkA = 0.0;
			double checkB = 0.0;
			
			Compute_In_Out_NTH (&checkA, &checkB);
			
			if ((fabs(checkA) < min_dist_dif) && (fabs(checkB - 360) < Angle_Threshold))
			{
				min_dist_dif = fabs(checkA);
				patch_id = j;
				CHECK1 = checkB;
				CHECK  = checkA;
			}
		}
		
		if ((min_dist_dif < Distance_Threshold) && (fabs(CHECK1 - 360) < Angle_Threshold))
		{
			fprintf (out, "point %5s is inside surface patch %5s - Normal Distance = %12.4lf\n", p_sp1[i].pt_id, sp2[patch_id].patch_id, CHECK);
			
			match_res_s.count++;
			if (match_res_s.count == 1)
			{
				match_res_s.surf1_index = (int *) malloc (match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) malloc (match_res_s.count * sizeof(int));
			}
			else
			{
				match_res_s.surf1_index = (int *) realloc (match_res_s.surf1_index, match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) realloc (match_res_s.surf2_index, match_res_s.count * sizeof(int));
			}
			
			int I =  match_res_s.count - 1;
			match_res_s.surf1_index[I] = i;
			match_res_s.surf2_index[I] = patch_id;
		}
		
	}
	
	return (1.0);
}

int match_XT(FILE *out, FILE *fpx, FILE *fpx_a, int flag)
{
	int		i, j;
	double	dxt;
	double	XT_;
	
	fprintf (out, "\n\n");
	fprintf (out, "Initial XT = %12.6lf(m)\n", XT);
	fprintf (out, "----------------------\n");
	
	rotation ();
	
	ZERO_ARRAY (XT_array, XT_no_pixels);
	
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			if (sp2[j].WTXY == 0)
				continue;
			
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			if (flag == 1)
			{
				check  = Compute_Normal_Distance();
				check2 = check_coplanarity();
				check1 = Compute_In_Out();
			}
			
			if (flag == 1)
				if (fabs(check1 - 360.0) < Angle_Threshold)
				{
					fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, check1);
					fprintf (out, "Normal Distance   = %12.4lf\n", check);
					fprintf (out, "Coplanarity Check = %12.4lf\n", check2);
				}
				
				if (flag == 1)
					if (fabs(check1 - 360.0) > Angle_Threshold)
						continue;
					
					dxt = solve_XT();	//correction increment (meters)
					
					if (dxt == 1000000.0)
						continue;
					
					XT_ = XT + dxt;;
					
					if ( (XT_ < XT_MIN) || (XT_ > XT_MAX))
						continue;
					
					double	dx = (XT_ - XT_O) / XT_pix_size + (double) XT_no_pixels / 2.0;
					int		ix = (int) (dx + 0.5);
					if ((ix > 0.0) && (ix < XT_no_pixels))
						XT_array[ix] += (1 * sp2[j].WTXY);
		}
	}
	
	int max = find_max (XT_array, XT_no_pixels);
	
	if (max != 0)
	{
		
		XT = ((double) (max) - XT_no_pixels / 2.0) * XT_pix_size + XT_O;	//Newly Estimated XT
		
		fprintf (out, "Maximum Value of %12d in the Accumulator Array has been detected at %4d\n", XT_array[max], max);
		fprintf (out, "Newly Estimated XT = %12.6lf(m)\n", XT);
		
		fprintf (fpx,"%6d %14.4lf\n", ITX, XT);
		
		
		for (i = 0; i < XT_no_pixels; i++)
		{
			double xt = ((double) (i) - XT_no_pixels / 2.0) * XT_pix_size + XT_O;
			fprintf (fpx_a,"%12.6lf %6d\n", xt, XT_array[i]);
		}
		
	}
	else
		fprintf (out,"No Maximum Has Been Detected\n");
	
	fclose (fpx_a);
	return (1);
}

int match_YT(FILE *out, FILE *fpy, FILE *fpy_a, int flag)
{
	int		i, j;
	double	dyt;
	double	YT_;
	
	rotation ();
	
	ZERO_ARRAY (YT_array, YT_no_pixels);
	
	fprintf (out, "\n\n");
	fprintf (out, "Initial YT = %12.6lf(m)\n", YT);
	fprintf (out, "----------------------\n");
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			if (sp2[j].WTXY == 0)
				continue;
			
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();						
			compute_XYZA();
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			if (flag == 1)
			{
				check  = Compute_Normal_Distance();
				check1 = Compute_In_Out();
				check2 = check_coplanarity();
			}
			
			if (flag == 1)
				if (fabs(check1 - 360.0) < Angle_Threshold)
				{
					fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, check1);
					fprintf (out, "Normal Distance   = %12.4lf\n", check);
					fprintf (out, "Coplanarity Check = %12.4lf\n", check2);
				}
				
				if (flag == 1)
					if (fabs(check1 - 360.0) > Angle_Threshold)
						continue;
					
					dyt = solve_YT();		//correction increment (meter)
					
					if (dyt == 1000000.0)
						continue;
					
					YT_ = YT + dyt;;
					
					if ( (YT_ < YT_MIN) || (YT_ > YT_MAX))
						continue;
					
					double	dy = (YT_ - YT_O) / YT_pix_size + (double) YT_no_pixels / 2.0;
					int		iy = (int) (dy + 0.5);
					if ((iy > 0.0) && (iy < YT_no_pixels))
						YT_array[iy] += (1 * sp2[j].WTXY);
					
		}
	}
	
	int max = find_max (YT_array, YT_no_pixels);
	
	if (max != 0)
	{
		YT = ((double) max - YT_no_pixels / 2.0) * YT_pix_size + YT_O; //Newly Estimated YT
		
		fprintf (out, "Maximum Value of %12d in the Accumulator Array has been detected at %4d\n", YT_array[max], max);
		fprintf (out, "Newly Estimated YT = %12.6lf(m)\n", YT);
		
		fprintf (fpy,"%6d %14.4lf\n", ITY, YT);
		
		for (i = 0; i < YT_no_pixels; i++)
		{
			double yt = ((double) i - YT_no_pixels / 2.0) * YT_pix_size + YT_O;
			fprintf (fpy_a,"%12.6lf %6d\n", yt, YT_array[i]);
		}
		
	}
	else
		fprintf (out, "No Maximum Has Been Detected\n");
	
	fclose (fpy_a);
	
	return (1);
}

int match_ZT(FILE *out, FILE *fpz, FILE *fpz_a, int flag)
{
	int		i, j;
	double	dzt;
	double	ZT_;
	
	rotation ();
	
	ZERO_ARRAY (ZT_array, ZT_no_pixels);
	
	fprintf (out, "\n\n");
	fprintf (out, "Initial ZT = %12.6lf(m)\n", ZT);
	fprintf (out, "----------------------\n");
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			if (sp2[j].WTZ == 0)
				continue;
			
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();
			
			compute_XYZA();
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			if (flag == 1)
			{
				check  = Compute_Normal_Distance();
				check1 = Compute_In_Out();
				check2 = check_coplanarity();
			}
			
			
			if (flag == 1)
				if (fabs(check1 - 360.0) < Angle_Threshold)
				{
					fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, check1);
					fprintf (out, "Normal Distance   = %12.4lf\n", check);
					fprintf (out, "Coplanarity Check = %12.4lf\n", check2);
				}
				
				if (flag == 1)
					if (fabs(check1 - 360.0) > Angle_Threshold)
						continue;
					
					dzt = solve_ZT();		//correction increment (meter)
					
					if (dzt == 1000000.0)
						continue;
					
					ZT_ = ZT + dzt;;
					if ( (ZT_ < ZT_MIN) || (ZT_ > ZT_MAX))
						continue;
					
					double	dz = (ZT_ - ZT_O) / ZT_pix_size + (double) ZT_no_pixels / 2.0;
					int		iz = (int) (dz + 0.5);
					if ((iz > 0.0) && (iz < ZT_no_pixels))
						ZT_array[iz] += (1 * sp2[j].WTZ);
					
		}
	}
	
	int max = find_max (ZT_array, ZT_no_pixels);
	if (max != 0)
	{
		ZT = ((double) max - ZT_no_pixels / 2.0) * ZT_pix_size + ZT_O;	//Newly Estimated ZT
		
		fprintf (out, "Maximum Value of %12d in the Accumulator Array has been detected at %4d\n", ZT_array[max], max);
		fprintf (out, "Newly Estimated ZT = %12.6lf(m)\n", ZT);
		
		fprintf (fpz,"%6d %14.4lf\n", ITZ, ZT);
		
		
		for (i = 0; i < ZT_no_pixels; i++)
		{
			double zt = ((double) i - ZT_no_pixels / 2.0) * ZT_pix_size + ZT_O;
			fprintf (fpz_a,"%12.6lf %6d\n", zt, ZT_array[i]);
		}
		
	}
	else
		fprintf (out, "No Maximum Has Been Detected\n");
	
	fclose (fpz_a);
	
	return (1);
}

int match_S(FILE *out, FILE *fps, FILE *fps_a, int flag)
{
	
	int		i, j;
	double	ds;
	double	S_;
	
	rotation ();
	
	ZERO_ARRAY (S_array, S_no_pixels);
	
	fprintf (out, "\n\n");
	fprintf (out, "Initial S = %12.6lf(unitless)\n", S);
	fprintf (out, "----------------------\n");
	
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();
			
			compute_XYZA();
			compute_XYZa();		//no scale or shift is applied 
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			if (flag == 1)
			{
				check =  Compute_Normal_Distance();
				check2 = check_coplanarity(); 
				check1 = Compute_In_Out();
			}
			
			
			if (flag == 1)
				if (fabs(check1 - 360.0) < Angle_Threshold)
				{
					fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, check1);
					fprintf (out, "Normal Distance   = %12.4lf\n", check);
					fprintf (out, "Coplanarity Check = %12.4lf\n", check2);
				}
				
				
				if (flag == 1)
					if (fabs(check) > Distance_Threshold)
						continue;
					
					ds = solve_S();			//Correction Increment (unitless)
					
					if (ds == 1000000.0)
						continue;
					
					S_ = S + ds;;
					
					if ( (S_ < S_MIN) || (S_ > S_MAX))
						continue;
					
					double	ds = (S_ - S_O) / S_pix_size + (double) S_no_pixels / 2.0;
					int		is = (int) (ds + 0.5);
					if ((is > 0.0) && (is < S_no_pixels))
						S_array[is] += 1;
					
		}
	}
	
	int max = find_max (S_array, S_no_pixels);
	if (max != 0)
	{
		S = ((double) max - S_no_pixels / 2.0) * S_pix_size + S_O;		//Newly Estimated Scale Factor
		
		fprintf (out, "Maximum Value of %12d in the Accumulator Array has been detected at %4d\n", S_array[max], max);
		fprintf (out, "Newly Estimated S = %12.6lf(unitless)\n", S);
		
		fprintf (fps,"%6d %14.4lf\n", ITS, S);
		
		for (i = 0; i < S_no_pixels; i++)
		{
			double s = ((double) i - S_no_pixels / 2.0) * S_pix_size + S_O;
			fprintf (fps_a,"%12.6lf %6d\n", s, S_array[i]);
		}
	}
	else
		fprintf (out, "No Maximum Has Been Detected\n");
	
	fclose (fps_a);
	
	return (1);
}

int match_om(FILE *out, FILE *fpo, FILE *fpo_a, int flag)
{
	int		i, j;
	double	dom;
	double	om_;
	
	rotation ();
	
	ZERO_ARRAY (om_array, om_no_pixels);
	
	double pi = 2.0 * asin (1.0);	
	fprintf (out, "\n\n");
	fprintf (out, "Initial Omega = %12.6lf(degrees)\n", om * 180.0 / pi);
	fprintf (out, "----------------------\n");
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			
			rotation();
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();
			
			rotation();
			compute_XYZA();
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			if (flag == 1)
			{
				check  = Compute_Normal_Distance();
				check1 = Compute_In_Out();
				check2 = check_coplanarity();
			}
			
			if (flag == 1)
				if (fabs(check1 - 360.0) < Angle_Threshold)
				{
					fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, check1);
					fprintf (out, "Normal Distance   = %12.4lf\n", check);
					fprintf (out, "Coplanarity Check = %12.4lf\n", check2);
				}
				
				if (flag == 1)
					if (fabs(check1 - 360.0) > Angle_Threshold)
						continue;
					
					dom = solve_om();		//Correction Increment (radians)
					
					if (dom == 1000000.0)
						continue;
					
					om_ = om + dom;;
					if ( (om_ < om_MIN) || (om_ > om_MAX))
						continue;
					
					double	dom = (om_ - om_O) * 180.0 / pi * 3600.0 / om_pix_size + (double) om_no_pixels / 2.0;
					int		iom = (int) (dom + 0.5);
					if ((iom > 0.0) && (iom < om_no_pixels))
						om_array[iom] += 1;
					
		}
	}
	
	int max = find_max (om_array, om_no_pixels);
	
	if (max != 0)
	{
		om = ((double) max - om_no_pixels / 2.0) * om_pix_size * pi / 180.0 / 3600.0 + om_O;	//Newly Estimated Omega
		
		fprintf (out, "Maximum Value of %12d in the Accumulator Array has been detected at %4d\n", om_array[max], max);
		fprintf (out, "Newly Estimated Omega = %12.6lf(degrees)\n", om * 180.0 / pi);
		
		fprintf (fpo,"%6d %14.4lf\n", ITO, om * 180.0 / pi);
		
		for (i = 0; i < om_no_pixels; i++)
		{
			double om = (((double) i - om_no_pixels / 2.0) * om_pix_size * pi / 180.0 / 3600.0 + om_O) * 180.0 / pi;
			fprintf (fpo_a,"%12.6lf %6d\n", om, om_array[i]);
		}
	}
	else
		fprintf (out, "No Maximum Has Been Detected\n");
	
	fclose (fpo_a);
	
	return (1);
}

int match_phi(FILE *out, FILE *fpp, FILE *fpp_a, int flag)
{
	int		i, j;
	double	dphi;
	double	phi_;
	
	rotation ();
	
	ZERO_ARRAY (phi_array, phi_no_pixels);
	
	double pi = 2.0 * asin (1.0);	
	fprintf (out, "\n\n");
	fprintf (out, "Initial Phi = %12.6lf(degrees)\n", phi * 180.0 / pi);
	fprintf (out, "----------------------\n");
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			
			rotation();
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();
			
			rotation();
			compute_XYZA();
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			if (flag == 1)
			{
				check  = Compute_Normal_Distance();
				check1 = Compute_In_Out();
				check2 = check_coplanarity();
			}
			
			
			if (flag == 1)
				if (fabs(check1 - 360.0) < Angle_Threshold)
				{
					fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, check1);
					fprintf (out, "Normal Distance   = %12.4lf\n", check);
					fprintf (out, "Coplanarity Check = %12.4lf\n", check2);
				}
				
				if (flag == 1)
					if (fabs(check1 - 360.0) > Angle_Threshold)
						continue;
					
					dphi = solve_phi();		//Correction Increment (radians)
					
					if (dphi == 1000000.0)
						continue;
					
					phi_ = phi + dphi;;
					if ( (phi_ < phi_MIN) || (phi_ > phi_MAX))
						continue;
					
					double	dphi = (phi_ - phi_O) * 180.0 / pi * 3600.0 / phi_pix_size + (double) phi_no_pixels / 2.0;
					int		iphi = (int) (dphi + 0.5);
					if ((iphi > 0.0) && (iphi < phi_no_pixels))
						phi_array[iphi] += 1;
					
		}
	}
	
	int max = find_max (phi_array, phi_no_pixels);
	if (max != 0)
	{
		phi = ((double) max - phi_no_pixels / 2.0) * pi / 180.0 / 3600.0 * phi_pix_size + phi_O;		//Newly Estimated Phi
		
		fprintf (out, "Maximum Value of %12d in the Accumulator Array has been detected at %4d\n", phi_array[max], max);
		fprintf (out, "Newly Estimated Phi = %12.6lf(degrees)\n", phi * 180.0 / pi);
		
		fprintf (fpp,"%6d %14.4lf\n", ITP, phi * 180.0 / pi);
		
		for (i = 0; i < phi_no_pixels; i++)
		{
			double ph = (((double) i - phi_no_pixels / 2.0) * pi / 180.0 / 3600.0 * phi_pix_size + phi_O) * 180.0 / pi;
			fprintf (fpp_a,"%12.6lf %6d\n", ph, phi_array[i]);
		}
	}
	else
		fprintf (out, "No Maximum Has Been Detected\n");
	
	fclose (fpp_a);
	
	return (1);
}

int match_kap(FILE *out, FILE *fpk, FILE *fpk_a, int flag)
{
	int		i, j;
	double	dkap;
	double	kap_;
	
	rotation ();
	
	ZERO_ARRAY (kap_array, kap_no_pixels);
	
	double pi = 2.0 * asin (1.0);	
	fprintf (out, "\n\n");
	fprintf (out, "Initial Kapa = %12.6lf(degrees)\n", kap * 180.0 / pi);
	fprintf (out, "----------------------\n");
	
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		for (j = 0; j < no_of_sp2; j++)
		{
			
			rotation();
			compute_XYZA();
			
			if (XA < (sp2[j].min_x - delta_x))
				continue;
			
			if (XA > (sp2[j].max_x + delta_x))
				continue;
			
			if (YA < (sp2[j].min_y - delta_y))
				continue;
			
			if (YA > (sp2[j].max_y + delta_y))
				continue;
			
			V1[0] = sp2[j].X[0];
			V1[1] = sp2[j].Y[0];
			V1[2] = sp2[j].Z[0];
			
			V2[0] = sp2[j].X[1];
			V2[1] = sp2[j].Y[1];
			V2[2] = sp2[j].Z[1];
			
			V3[0] = sp2[j].X[2];
			V3[1] = sp2[j].Y[2];
			V3[2] = sp2[j].Z[2];
			
			compute_det();
			
			rotation();
			compute_XYZA();
			
			double check  = 0.0;
			double check1 = 0.0;
			double check2 = 0.0;
			
			
			if (flag == 1)
			{
				check  = Compute_Normal_Distance();
				check1 = Compute_In_Out();
				check2 = check_coplanarity();
			}
			
			if (flag == 1)
				if (fabs(check1 - 360.0) < Angle_Threshold)
				{
					fprintf (out, "point %5s is inside surface patch %5s - Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, check1);
					fprintf (out, "Normal Distance   = %12.4lf\n", check);
					fprintf (out, "Coplanarity Check = %12.4lf\n", check2);
				}
				
				if (flag == 1)
					if (fabs(check1 - 360.0) > Angle_Threshold)
						continue;
					
					dkap = solve_kap();		// Correction Increment (radians)
					
					if (dkap == 1000000.0)
						continue;
					
					kap_ = kap + dkap;;
					if ( (kap_ < kap_MIN) || (kap_ > kap_MAX))
						continue;
					
					double	dkap = (kap_ - kap_O) * 180.0 / pi * 3600.0 / kap_pix_size + (double) kap_no_pixels / 2.0;
					int		ikap = (int) (dkap + 0.5);
					if ((ikap > 0.0) && (ikap < kap_no_pixels))
						kap_array[ikap] += 1;
		}
	}
	
	int max = find_max (kap_array, kap_no_pixels);
	if (max != 0)
	{
		kap = ((double) max - kap_no_pixels / 2.0) * pi / 180.0 / 3600.0 * kap_pix_size + kap_O;	//Newly Estimated Kapa
		
		fprintf (out, "Maximum Value of %12d in the Accumulator Array has been detected at %4d\n", kap_array[max], max);
		fprintf (out, "Newly Estimated Kapa = %12.6lf(degrees)\n", kap * 180.0 / pi);
		
		fprintf (fpk,"%6d %14.4lf\n", ITK, kap * 180.0 / pi);
		
		for (i = 0; i < kap_no_pixels; i++)
		{
			double kp = (((double) i - kap_no_pixels / 2.0) * pi / 180.0 / 3600.0 * kap_pix_size + kap_O) * 180.0 / pi;
			fprintf (fpk_a,"%12.6lf %6d\n", kp, kap_array[i]);
		}
	}
	else
		fprintf (out, "No Maximum Has Been Detected\n");
	
	fclose (fpk_a);
	
	return (1);
}

int rotation(void)
{
	double  c1,s1,c2,s2,c3,s3;
	
   	c1 = cos(om);
   	s1 = sin(om);
   	c2 = cos(phi);
   	s2 = sin(phi);
   	c3 = cos(kap);
   	s3 = sin(kap);
	
   	R[0][0] =  c2*c3;
   	R[0][1] = -c2*s3;
   	R[0][2] =  s2;
   	R[1][0] =  c1*s3+s1*s2*c3;
   	R[1][1] =  c1*c3-s1*s2*s3;
   	R[1][2] = -s1*c2;
   	R[2][0] =  s1*s3-c1*s2*c3;
   	R[2][1] =  s1*c3+c1*s2*s3;
   	R[2][2] =  c1*c2;
	
   	return (1);
}

int     rotation_Matrix(double omega, double phi, double kappa, double R[])
{
	double c1, s1, c2, s2, c3, s3;
	
	c1 = cos(omega);
	s1 = sin(omega);
	c2 = cos(phi);
	s2 = sin(phi);
    c3 = cos(kappa);
	s3 = sin(kappa);
	
	R[0] =  c2*c3;
   	R[1] = -c2*s3;
   	R[2] =  s2;
   	R[3] =  c1*s3+s1*s2*c3;
   	R[4] =  c1*c3-s1*s2*s3;
   	R[5] = -s1*c2;
   	R[6] =  s1*s3-c1*s2*c3;
   	R[7] =  s1*c3+c1*s2*s3;
   	R[8] =  c1*c2;
	
	return (1);
}

int compute_det (void)
{  
	
	detx = V1[1] * (V2[2] - V3[2]) - V1[2] * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1]);
	dety = V1[0] * (V2[2] - V3[2]) - V1[2] * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0]);
	detz = V1[0] * (V2[1] - V3[1]) - V1[1] * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0]);
	
	det  = V1[0] * (V2[1] * V3[2] - V2[2] * V3[1]) - V1[1] * (V2[0] * V3[2] - V2[2] * V3[0]) + V1[2] * (V2[0] * V3[1] - V2[1] * V3[0]);
	
	return (1);
}

int compute_BZ (void)
{
	
	BZ1  =  (V1[1] * (V2[2] - V3[2]) - V1[2] * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1])) * S * R[0][2];
	BZ1 += -(V1[0] * (V2[2] - V3[2]) - V1[2] * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0])) * S * R[1][2];
	BZ1 +=  (V1[0] * (V2[1] - V3[1]) - V1[1] * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0])) * S * R[2][2];
	BZA  = -(XA * (V2[1] - V3[1]) - YA * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0]));
	BZB  =  (XA * (V1[1] - V3[1]) - YA * (V1[0] - V3[0]) + (V1[0] * V3[1] - V1[1] * V3[0]));
	BZC  = -(XA * (V1[1] - V2[1]) - YA * (V1[0] - V2[0]) + (V1[0] * V2[1] - V1[1] * V2[0]));
	
	return (1);
}

int compute_BY (void)
{
	
	BY1  =  (V1[1] * (V2[2] - V3[2]) - V1[2] * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1])) * S * R[0][1];
	BY1 += -(V1[0] * (V2[2] - V3[2]) - V1[2] * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0])) * S * R[1][1];
	BY1 +=  (V1[0] * (V2[1] - V3[1]) - V1[1] * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0])) * S * R[2][1];
	
	BYA  =  (XA * (V2[2] - V3[2]) - ZA * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0]));
	BYB  = -(XA * (V1[2] - V3[2]) - ZA * (V1[0] - V3[0]) + (V1[0] * V3[2] - V1[2] * V3[0]));
	BYC  =  (XA * (V1[2] - V2[2]) - ZA * (V1[0] - V2[0]) + (V1[0] * V2[2] - V1[2] * V2[0]));
	
	return (1);
}

int compute_BX (void)
{
	
	BX1  =  (V1[1] * (V2[2] - V3[2]) - V1[2] * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1])) * S * R[0][0];
	BX1 += -(V1[0] * (V2[2] - V3[2]) - V1[2] * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0])) * S * R[1][0];
	BX1 +=  (V1[0] * (V2[1] - V3[1]) - V1[1] * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0])) * S * R[2][0];
	
	BXA  = -(YA * (V2[2] - V3[2]) - ZA * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1]));
	BXB  =  (YA * (V1[2] - V3[2]) - ZA * (V1[1] - V3[1]) + (V1[1] * V3[2] - V1[2] * V3[1]));
	BXC  = -(YA * (V1[2] - V2[2]) - ZA * (V1[1] - V2[1]) + (V1[1] * V2[2] - V1[2] * V2[1]));
	
	return (1);
}

double solve_XT (void)
{
	
	double dxt, XT_T;
	XT_T = XT;
	
	for (int  i = 0; i < 5; i++)
	{	
		double y		= det - XA * detx + YA * dety - ZA * detz;
		double dfdxt	= detx;
		if (fabs (dfdxt) > 0.00001)
			dxt = y / dfdxt;
		else
		{
			dxt= 1000000.0;
			break;
		}
		
		XT += dxt;
		compute_XYZA();		
	}
	
	if (dxt != 1000000.0)
		dxt = XT - XT_T;
	
	XT = XT_T;
	
	return (dxt);
}

double solve_YT (void)
{
	
	double dyt, YT_T;
	YT_T = YT;
	
	for (int i = 0; i < 5; i++)
	{
		double y		= det - XA * detx + YA * dety - ZA * detz;
		double dfdyt	= -dety;
		if (fabs (dfdyt) > 0.00001)
			dyt = y / dfdyt;
		else
		{
			dyt = 1000000.0;
			break;
		}
		
		YT += dyt;
		compute_XYZA();
	}
	
	if (dyt != 1000000.0)
		dyt = YT - YT_T;
	
	YT = YT_T;
	
	return (dyt);
}

double solve_ZT (void)
{
	
	double dzt, ZT_T;
	
	ZT_T = ZT;
	
	for (int i = 0; i < 5; i++)
	{
		double y		= det - XA * detx + YA * dety - ZA * detz;
		double dfdzt	= detz;
		if (fabs (dfdzt) > 0.00001)
			dzt = y / dfdzt;
		else
		{
			dzt = 1000000.0;
			break;
		}
		
		ZT += dzt;
		compute_XYZA();
	}
	
	if (dzt != 1000000.0)
		dzt = ZT - ZT_T;
	
	ZT = ZT_T;
	
	return (dzt);
}

double solve_S(void)
{
	
	double ds, S_T;
	
	S_T = S;
	
	for (int i = 0; i < 5; i++)
	{
		double y		= det - XA * detx + YA * dety - ZA * detz;
		
		double dfds		= (detx * Xa  - dety * Ya + detz * Za);
		
		if (fabs (dfds) > 0.00001)
			ds = y / dfds;
		else
		{
			ds = 1000000.0;
			break;
		}
		
		S += ds;
		compute_XYZA();
	}
	
	if (ds != 1000000.0)
		ds = S - S_T;
	
	S = S_T;
	
	return (ds);
}

double solve_om (void)
{
	
	double dom, om_T;
	
	om_T = om;
	
	for (int i = 0; i < 5; i++)
	{
		double y		= det - XA * detx + YA * dety - ZA * detz;
		double dfdom	= (dety * (ZA - ZT) + detz * (YA - YT));
		if (fabs (dfdom) > 0.0000001)
			dom = y / dfdom;
		else
		{
			dom = 1000000.0;
			break;
		}
		
		om += dom;
		rotation();
		compute_XYZA();
	}
	
	if (dom != 1000000.0)
		dom = om - om_T;
	
	om = om_T;
	return (dom);
}

double solve_phi (void)
{
	
	double dphi, phi_T;
	
	phi_T = phi;
	
	for (int i = 0; i < 5; i++)
	{
		double y		= det - XA * detx + YA * dety - ZA * detz;
		
		double dfdphi	= (	  detx * (-(YA - YT) * sin(om) + (ZA - ZT) * cos(om))
			- dety * (XA - XT) * sin(om)
			- detz * (XA - XT) * cos(om));
		
		if (fabs (dfdphi) > 0.0000001)
			dphi = y / dfdphi;
		else
		{
			dphi = 1000000.0;
			break;
		}
		
		phi += dphi;
		
		rotation();
		compute_XYZA();
	}
	
	if (dphi != 1000000.0)
		dphi = phi - phi_T;
	
	phi = phi_T;
	
	return (dphi);
}

double	solve_kap	(void)

{
	
	double *x1, *X1;
	
	x1 = (double *) malloc (3 * sizeof(double));
	X1 = (double *) malloc (3 * sizeof(double));
	
	double dkap, kap_T;
	
	kap_T = kap;
	
	for (int i = 0; i < 5; i++)
	{
		double y		= det - XA * detx + YA * dety - ZA * detz;
		
		x1[0] =  -ya;
		x1[1] =   xa;
		x1[2] =  0.0;
		
		mult_AV (R, x1, X1, 3, 3);
		
		double dfdkap	= S * (detx * X1[0] - dety * X1[1] + detz * X1[2]);
		
		
		if (fabs (dfdkap) > 0.0000001)
			dkap = y / dfdkap;
		else
		{
			dkap = 1000000.0;
			break;
		}
		
		kap += dkap;
		
		rotation();
		compute_XYZA();
	}
	
	if (dkap != 1000000.0)
		dkap = kap - kap_T;
	
	kap = kap_T;
	
	free (x1);
	free (X1);
	return (dkap);
}

double **dmatrix ( int nrl , int nrh , int ncl , int nch)
{
	int i , nrow = nrh - nrl + 1 ;
	int ncol = nch - ncl + 1 ;
	double **m;
	
	m = (double **) malloc ( nrow * sizeof(double*));
	
	m[nrl] = (double *) malloc ( nrow * ncol * sizeof(double));
	
	for ( i = nrl+1 ; i <= nrh ; i++)
		m[i] = m[i-1] + ncol ;
	
	return m ;
}

void free_dmatrix (double **m , int nrl , int nrh , int ncl , int nch)
{
	
	free (m[nrl]+ncl);
	free (m+nrl);
	
}

int find_max (int *vec, int n)
{
	int maximum;
	int i, max_id;
	
	maximum = -1000000;
	
	for ( i = 0; i < n; i++)
	{
		if (vec[i] > maximum)
		{
			maximum = vec[i];
			max_id	= i;
		}
	}
	
	return max_id;
}

int find_min_max (int i)
{
	double min_x, min_y, max_x, max_y;
	int	j;
	
	min_x = min_y =  1.0e+12;
	max_x = max_y = -1.0e-12;
	
	for (j = 0; j < 3; j++)
	{
		if (sp2[i].X[j] < min_x)
			min_x = sp2[i].X[j];
		
		if (sp2[i].Y[j] < min_y)
			min_y = sp2[i].Y[j];
		
		if (sp2[i].X[j] > max_x)
			max_x = sp2[i].X[j];
		
		if (sp2[i].Y[j] > max_y)
			max_y = sp2[i].Y[j];
	}
	
	sp2[i].min_x = min_x;
	sp2[i].min_y = min_y;
	sp2[i].max_x = max_x;
	sp2[i].max_y = max_y;
	
	return (1);
}

int ZERO_ARRAY (int *vec, int n)
{
	int i;
	
	for (i = 0; i < n; i++)
		vec[i] = 0;
	
	return (1);
}

int	compute_XYZA(void)

{
	
	XA = XT + S * (R[0][0] * xa + R[0][1] * ya + R[0][2] * za);
	YA = YT + S * (R[1][0] * xa + R[1][1] * ya + R[1][2] * za);
	ZA = ZT + S * (R[2][0] * xa + R[2][1] * ya + R[2][2] * za);
	
	return (1);
}

int	compute_XYZa(void)

{
	
	Xa = (R[0][0] * xa + R[0][1] * ya + R[0][2] * za);
	Ya = (R[1][0] * xa + R[1][1] * ya + R[1][2] * za);
	Za = (R[2][0] * xa + R[2][1] * ya + R[2][2] * za);
	
	return (1);
}

double check_coplanarity(void)
{
	
	double DET;
	
	DET = det - XA * detx + YA * dety - ZA * detz;
	
	return (fabs(DET));
}

double Compute_In_Out (void)
{
	
	double V1x, V1y, V1z;
	double V2x, V2y, V2z;
	double V3x, V3y, V3z;
	
	double th12, th23, th31, th, D1, D2, D3;
	
	V1x = V1[0] - XA;
	V1y = V1[1] - YA;
	V1z = V1[2] - ZA;
	
	D1 = sqrt (V1x * V1x + V1y * V1y + V1z * V1z); 
	
	V2x = V2[0] - XA;
	V2y = V2[1] - YA;
	V2z = V2[2] - ZA;
	
	D2 = sqrt (V2x * V2x + V2y * V2y + V2z * V2z); 
	
	V3x = V3[0] - XA;
	V3y = V3[1] - YA;
	V3z = V3[2] - ZA;
	
	D3 = sqrt (V3x * V3x + V3y * V3y + V3z * V3z); 
	
	double pi = 2.0 * asin(1.0); 
	
	if ((D1 <0.01) || (D2 < 0.01) || (D3 < 0.01))
	{
		th = 360.0;
		return th;
	}
	
	
	double c12, c23, c31;
	
	c12 = (V1x * V2x + V1y * V2y + V1z * V2z) / D1 / D2;
	c23 = (V2x * V3x + V2y * V3y + V2z * V3z) / D2 / D3;
	c31 = (V3x * V1x + V3y * V1y + V3z * V1z) / D3 / D1;
	
	if (c12 > 1.0)
		c12 = 1.0;
	if (c12 < -1.0)
		c12 = -1.0;
	
	if (c23 > 1.0)
		c23 = 1.0;
	if (c23 < -1.0)
		c23 = -1.0;
	
	if (c31 > 1.0)
		c31 = 1.0;
	if (c31 < -1.0)
		c31 = -1.0;
	
	th12 = acos (c12) * 180.0 / pi;
	th23 = acos (c23) * 180.0 / pi;
	th31 = acos (c31) * 180.0 / pi;
	
	th = fabs(th12) + fabs(th23) + fabs(th31);
	
	return th;
}

int Compute_In_Out_NTH (double *Normal_distance, double *th)
{
	
	double V2x, V2y, V2z;
	double V3x, V3y, V3z;
	
	double Dx, Dy, Dz;
	
	V2x = V2[0] - V1[0];
	V2y = V2[1] - V1[1];
	V2z = V2[2] - V1[2];
	
	V3x = V3[0] - V1[0];
	V3y = V3[1] - V1[1];
	V3z = V3[2] - V1[2];
	
	Dx = sqrt (V2x * V2x + V2y * V2y + V2z * V2z); 
	
	//X axis
	double XX1 = V2x / Dx;
	double XX2 = V2y / Dx;
	double XX3 = V2z / Dx;
	
	//Z axis
	double ZZ1 = V2y * V3z - V2z * V3y;
	double ZZ2 = V2z * V3x - V2x * V3z;
	double ZZ3 = V2x * V3y - V2y * V3x;
	
	Dz = sqrt (ZZ1 * ZZ1 + ZZ2 * ZZ2 + ZZ3 * ZZ3); 
	
	ZZ1 = ZZ1 / Dz;
	ZZ2 = ZZ2 / Dz;
	ZZ3 = ZZ3 / Dz;
	
	//Y axis
	
	double YY1 = ZZ2 * XX3 - ZZ3 * XX2;
	double YY2 = ZZ3 * XX1 - ZZ1 * XX3;
	double YY3 = ZZ1 * XX2 - ZZ2 * XX1;
	
	Dy = sqrt (YY1 * YY1 + YY2 * YY2 + YY3 * YY3); 
	
	YY1 = YY1 / Dy;
	YY2 = YY2 / Dy;
	YY3 = YY3 / Dy;
	
	double X1, Y1, Z1, X2, Y2, Z2, X3, Y3, Z3, X, Y, Z;
	
	X1 = Y1 = Z1 = 0.0;
	
	X2 = XX1 * V2x + XX2 * V2y + XX3 * V2z;
	Y2 = YY1 * V2x + YY2 * V2y + YY3 * V2z;
	Z2 = ZZ1 * V2x + ZZ2 * V2y + ZZ3 * V2z;
	
	X3 = XX1 * V3x + XX2 * V3y + XX3 * V3z;
	Y3 = YY1 * V3x + YY2 * V3y + YY3 * V3z;
	Z3 = ZZ1 * V3x + ZZ2 * V3y + ZZ3 * V3z;
	
	X  = XX1 * (XA - V1[0]) + XX2 * (YA - V1[1]) + XX3 * (ZA - V1[2]);
	Y  = YY1 * (XA - V1[0]) + YY2 * (YA - V1[1]) + YY3 * (ZA - V1[2]);
	Z  = ZZ1 * (XA - V1[0]) + ZZ2 * (YA - V1[1]) + ZZ3 * (ZA - V1[2]);
	
	*Normal_distance = fabs(Z);
	
	if ((Y < 0.0) || (Y > Y3))
		*th = 0.0;
	else
	{
		double xi_13 = Y * X3 / Y3;
		double xi_23 = X3 - (X3 - X2) * (Y3 - Y) / (Y3 - Y2);
		
		if ((X > xi_13) && (X < xi_23))
			*th = 360.0;
		else
			*th = 0.0;
	}
	
	return (1);
}

double Compute_Normal_Distance (void)
{
	
	double V12x, V12y, V12z;
	double V13x, V13y, V13z;
	double Vx  , Vy  , Vz;
	
	double Nx, Ny, Nz;
	double nx, ny, nz;
	
	double ND;
	double distance;
	
	V12x = V2[0] - V1[0];
	V12y = V2[1] - V1[1];
	V12z = V2[2] - V1[2];
	
	V13x = V3[0] - V1[0];
	V13y = V3[1] - V1[1];
	V13z = V3[2] - V1[2];
	
	Vx	 = XA    - V1[0];
	Vy   = YA    - V1[1];
	Vz   = ZA    - V1[2];
	
	Nx = V12y * V13z - V12z * V13y;
	Ny = V12z * V13x - V12x * V13z;
	Nz = V12x * V13y - V12y * V13x;
	
	ND = sqrt (Nx * Nx + Ny * Ny + Nz * Nz); 
	
	nx = Nx / ND;
	ny = Ny / ND;
	nz = Nz / ND;
	
	distance = fabs (Vx * nx + Vy * ny + Vz * nz);
	
	return (distance);
}

//This function finds the directory and the prefix of the file name i_name upto the dot '.'

int prj_fnames (char *i_name)
{
	int         i, name_len, dot;
	char        *p1,*pi1,x_name[80];
	i = 0;
	dot = 0;
	p1 = i_name;
	pi1=p1;
	/* copy the input filename and detect a dot ----------------------   */
	
	name_len = strlen(i_name);
	for (i = 0; i < name_len; i++)      if (*p1++ == '.') dot = i;
	if (i==0) return 0;
	
	if (dot==0)
		strcpy(x_name, i_name);
	else{
		strncpy(x_name,i_name,dot);
		x_name[dot]='\0';
	}
	strcpy(i_name,x_name);
	return 1;
}

//This function finds the directory in the file name i_name including the 

int prj_dir (char *i_name)
{
	int  i, j, n;
	char temp[STR_LEN];
	
	n = strlen (i_name);
	
	for ( i = n - 1; i >=0 ; i--)
		if (i_name[i] == '\\')
		{
			j = i + 1;
			break;
		}
		
		for (i = 0; i < j ; i++)
			temp[i] = i_name[i];
		
		temp [j] = '\0';
		
		strcpy (i_name, temp);
		
		return 1;
}

double Compute_Parameters (FILE *out, int t)
{
	// t == 3	Solve for XT, YT, ZT
	// t == 4	Solve for XT, YT, ZT, OM
	// t == 5	Solve for XT, YT, ZT, OM, PH
	// t == 6	Solve for XT, YT, ZT, OM, PH, KP
	// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
	
	double	sigmo;
	double	*A, *x1, *X1;
	double	y;
	double  *xh, *C;
	double	**N, **NT, **v, *w;
	
	double	pi;
	
	sigmo = 0.0;
	
	double xt_a  = XT;
	double yt_a  = YT;
	double zt_a  = ZT;
	double s_a   = S;
	double om_a  = om;
	double phi_a = phi;
	double kap_a = kap;
	
	pi = 2.0 * asin(1.0);
	
	x1 = (double *) malloc (3 * sizeof(double));
	X1 = (double *) malloc (3 * sizeof(double));
	A  = (double *) malloc (7 * sizeof(double));
	
	N  = dmatrix (0, 6, 0, 6);
	NT = dmatrix (0, 6, 0, 6);
	v  = dmatrix (0, 6, 0, 6);
	
	xh  =   (double *) malloc (7 * sizeof(double));
	
	C	=	(double *) malloc (7 * sizeof(double));
	w	=	(double *) malloc (7 * sizeof(double));
	
	int		no_it, max_it;
	double	etpe, seg_new, seg_old, dif, threshold;
	
	seg_old = LARGE;
	max_it		= 30;
	
	int flag_sing = 0;
	
	threshold = 0.00000000001;
	double out_liar = 10000000.0;
	
	int	ii, jj, i, j, n, I, J;
	
	n = match_res_s.count;
	
	if (n < 8)
	{
		fprintf (out, "\n\nThere are not enough points to compute the transformation parameters\n");
		fprintf (out, "####################################################################\n\n");
		return (-1.0);
	}
	
	no_it = 0;
	do 
	{
		rotation ();
		
		etpe = 0.0;
		
		for (i = 0; i < 7; i++)
		{
			C[i] = 0.0;
			for (j = 0; j < 7; j++)
				N[i][j] = 0.0;
		}
		
		n = match_res_s.count;
		
		fprintf (out, "Iteration Number %4d\n", no_it);
		fprintf (out, "---------------------\n\n");
		
		for (i = 0; i < n; i++)
		{
			I = match_res_s.surf1_index[i];
			J = match_res_s.surf2_index[i];
			
			xa = p_sp1[I].x;
			ya = p_sp1[I].y;
			za = p_sp1[I].z;
			
			//xa -= DX;
			//ya -= DY;
			
			compute_XYZA();
			compute_XYZa();		//no scale or shift is applied 
			
			V1[0] = sp2[J].X[0];
			V1[1] = sp2[J].Y[0];
			V1[2] = sp2[J].Z[0];
			
			//V1[0] -= DX;
			//V1[1] -= DY;
			
			V2[0] = sp2[J].X[1];
			V2[1] = sp2[J].Y[1];
			V2[2] = sp2[J].Z[1];
			
			//V2[0] -= DX;
			//V2[1] -= DY;
			
			V3[0] = sp2[J].X[2];
			V3[1] = sp2[J].Y[2];
			V3[2] = sp2[J].Z[2];
			
			//V3[0] -= DX;
			//V3[1] -= DY;
			
			compute_det();
			
			x1[0] =  -ya;
			x1[1] =   xa;
			x1[2] =  0.0;
			
			mult_AV (R, x1, X1, 3, 3);
			
			y		= det - XA * detx + YA * dety - ZA * detz;
			
			//Partial Derivative w.r.t. XT
			A[0]	=  detx;
			
			//Partial Derivative w.r.t. YT
			A[1]	= -dety;
			
			//Partial Derivative w.r.t. ZT
			A[2]	=  detz;
			
			//Partial Derivative w.r.t. Scale Factor
			A[3]	= (detx * Xa  - dety * Ya + detz * Za);
			
			//Partial Derivative w.r.t. Omega
			A[4]	= (dety * (ZA - ZT) + detz * (YA - YT));
			
			//Partial Derivative w.r.t. Phi
			A[5]	= (	  detx * (-(YA - YT) * sin(om) + (ZA - ZT) * cos(om))
				- dety * (XA - XT) * sin(om)
				- detz * (XA - XT) * cos(om));
			
			//Partial Derivative w.r.t. Kapa
			A[6]	= S * (detx * X1[0] - dety * X1[1] + detz * X1[2]);
			
			//We need to compute the partial derivatives w.r.t. the observed quantities
			
			compute_BZ();
			compute_BX();
			compute_BY();
			
			double p = 1.0 / (BZ1 * BZ1 + BZA * BZA + BZB * BZB + BZC * BZC + BY1 * BY1 + BYA * BYA + BYB * BYB + BYC * BYC + BX1 * BX1 + BXA * BXA + BXB * BXB + BXC * BXC);
			//fprintf (out, "Weight %10s %10s %12.4lf\n", p_sp1[I].pt_id, sp2[J].patch_id, p);
			
			//p = 1.0; //AH January 2007
			/*
			if (fabs (y) < out_liar)
			fprintf (out, "CONSIDERED %10s %10s %12.4lf\n", p_sp1[I].pt_id, sp2[J].patch_id, y * sqrt(p));
			else
			{
			fprintf (out, "REJECTED   %10s %10s %12.4lf\n", p_sp1[I].pt_id, sp2[J].patch_id, y * sqrt(p));
			continue;
			}
			*/
			
			
			etpe += y * p * y;
			
			for (ii = 0; ii < 7; ii++)
				C[ii] += A[ii] * p * y;
			
			for (ii = 0; ii < 7; ii++)
				A[ii] *= sqrt(p);
			
			mult_VVT (A, A, NT, 7);
			
			for (ii = 0; ii < 7; ii++)
				for (jj = 0; jj < 7; jj++)
					N[ii][jj] += NT[ii][jj];
				
		}
		
		
		if (t == 6)
		{
			// t == 3	Solve for XT, YT, ZT
			// t == 4	Solve for XT, YT, ZT, OM
			// t == 5	Solve for XT, YT, ZT, OM, PH
			// t == 6	Solve for XT, YT, ZT, OM, PH, KP
			// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			C[3] = 0.0;
			N[3][3] = 1.0;
			for (ii = 0; ii < 7; ii++)
				if (ii != 3)
					N[ii][3] = 0.0;
				
				for (jj = 0; jj < 7; jj++)
					if (jj != 3)
						N[3][jj] = 0.0;
		}
		
		if (t == 5)
		{
			// t == 3	Solve for XT, YT, ZT
			// t == 4	Solve for XT, YT, ZT, OM
			// t == 5	Solve for XT, YT, ZT, OM, PH
			// t == 6	Solve for XT, YT, ZT, OM, PH, KP
			// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			C[3] = 0.0;
			N[3][3] = 1.0;
			for (ii = 0; ii < 7; ii++)
				if (ii != 3)
					N[ii][3] = 0.0;
				
				for (jj = 0; jj < 7; jj++)
					if (jj != 3)
						N[3][jj] = 0.0;
					
					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;
						
						for (jj = 0; jj < 7; jj++)
							if (jj != 6)
								N[6][jj] = 0.0;
		}
		
		if (t == 4)
		{
			// t == 3	Solve for XT, YT, ZT
			// t == 4	Solve for XT, YT, ZT, OM
			// t == 5	Solve for XT, YT, ZT, OM, PH
			// t == 6	Solve for XT, YT, ZT, OM, PH, KP
			// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			C[3] = 0.0;
			N[3][3] = 1.0;
			for (ii = 0; ii < 7; ii++)
				if (ii != 3)
					N[ii][3] = 0.0;
				
				for (jj = 0; jj < 7; jj++)
					if (jj != 3)
						N[3][jj] = 0.0;
					
					C[5] = 0.0;
					N[5][5] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 5)
							N[ii][5] = 0.0;
						
						for (jj = 0; jj < 7; jj++)
							if (jj != 5)
								N[5][jj] = 0.0;
							
							C[6] = 0.0;
							N[6][6] = 1.0;
							for (ii = 0; ii < 7; ii++)
								if (ii != 6)
									N[ii][6] = 0.0;
								
								for (jj = 0; jj < 7; jj++)
									if (jj != 6)
										N[6][jj] = 0.0;
		}
		
		if (t == 3)
		{
			// t == 3	Solve for XT, YT, ZT
			// t == 4	Solve for XT, YT, ZT, OM
			// t == 5	Solve for XT, YT, ZT, PH, KP
			// t == 6	Solve for XT, YT, ZT, OM, PH, KP
			// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			C[3] = 0.0;
			N[3][3] = 1.0;
			for (ii = 0; ii < 7; ii++)
				if (ii != 3)
					N[ii][3] = 0.0;
				
				for (jj = 0; jj < 7; jj++)
					if (jj != 3)
						N[3][jj] = 0.0;
					
					C[4] = 0.0;
					N[4][4] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 4)
							N[ii][4] = 0.0;
						
						for (jj = 0; jj < 7; jj++)
							if (jj != 4)
								N[4][jj] = 0.0;
							
							C[5] = 0.0;
							N[5][5] = 1.0;
							for (ii = 0; ii < 7; ii++)
								if (ii != 5)
									N[ii][5] = 0.0;
								
								for (jj = 0; jj < 7; jj++)
									if (jj != 5)
										N[5][jj] = 0.0;
									
									C[6] = 0.0;
									N[6][6] = 1.0;
									for (ii = 0; ii < 7; ii++)
										if (ii != 6)
											N[ii][6] = 0.0;
										
										for (jj = 0; jj < 7; jj++)
											if (jj != 6)
												N[6][jj] = 0.0;
		}
		
		dsvdcmp ( N , 7, 7, w, v);
		
		//AH Adjustment SVDcmp
		
		//*
		fprintf (out, "The Eigen Values\n");
		
		for ( ii = 0; ii < 7; ii++)
		{
			if (w[ii] < 0.00000001)
				flag_sing = 1;
			
			fprintf (out, "w[%3d] = %12.6lf\n", ii, w[ii]);
		}
		
		if (flag_sing == 1)
			break;
		
		//*/
		
		for ( jj = 0 ; jj < 7 ; jj++)
			for ( ii = 0 ; ii < 7 ; ii++)
				v[ii][jj] /= sqrt(w[jj]);
			
			
			mult_ABT (v , v , N , 7 , 7 , 7);
			mult_AV  (N , C , xh , 7 , 7);
			
			XT	+= (xh[0]);
			YT	+= (xh[1]);
			ZT  += (xh[2]);
			
			S	+= (xh[3]);
			
			om	+= (xh[4]);
			phi	+= (xh[5]);
			kap	+= (xh[6]);
			
			/*
			fprintf (out, "Iteration # %5d\n", no_it);
			fprintf (out, "Estimated Variance Component = %lf\n", sqrt(seg_new));
			
			  fprintf (out, "XT	  = %14.4lf   (%9.6lf) (m)\n", XT, sqrt(N[0][0] * seg_new));
			  fprintf (out, "YT     = %14.4lf   (%9.6lf) (m)\n", YT, sqrt(N[1][1] * seg_new));
			  fprintf (out, "ZT     = %14.4lf   (%9.6lf) (m)\n", ZT, sqrt(N[2][2] * seg_new));
			  
				fprintf (out, "S      = %14.4lf   (%9.6lf) (m)\n",  S, sqrt(N[3][3] * seg_new));
				
				  fprintf (out, "omega  = %14.4lf   (%9.6lf) (deg)\n", om  * 180.0 / pi, sqrt(N[4][4] * seg_new));
				  fprintf (out, "phi    = %14.4lf   (%9.6lf) (deg)\n", phi * 180.0 / pi, sqrt(N[5][5] * seg_new));
				  fprintf (out, "kapa   = %14.4lf   (%9.6lf) (deg)\n", kap * 180.0 / pi, sqrt(N[6][6] * seg_new));
				  
					fprintf (out, "\n\n");
			//*/
			
			no_it++;
			
			seg_new = etpe / ((double) n - 7.0);
			dif = seg_new - seg_old;
			seg_old = seg_new;			 
			
			
	} while ((no_it < max_it) && (fabs(dif) > threshold));
	
	if (flag_sing == 1)
	{
		XT  = xt_a;
		YT  = yt_a;
		ZT  = zt_a;
		S   = s_a;
		om  = om_a;
		phi = phi_a;
		kap = kap_a;
		
		fprintf (out, "Iteration # %5d\n", no_it);
		fprintf (out, "Estimated Variance Component = %lf\n", sqrt(seg_new));
		
		fprintf (out, "XT	  = %14.4lf (m)\n", XT);
		fprintf (out, "YT     = %14.4lf (m)\n", YT);
		fprintf (out, "ZT     = %14.4lf (m)\n", ZT);
		
		fprintf (out, "S      = %14.4lf    \n",  S);
		
		fprintf (out, "omega  = %14.4lf   (deg)\n", om  * 180.0 / pi);
		fprintf (out, "phi    = %14.4lf   (deg)\n", phi * 180.0 / pi);
		fprintf (out, "kapa   = %14.4lf   (deg)\n", kap * 180.0 / pi);
	}
	else
	{
		//AH Adjustment Intermediate Results
		
		fprintf (out, "Iteration # %5d\n", no_it);
		fprintf (out, "Estimated Variance Component = %lf\n", sqrt(seg_new));
		
		fprintf (out, "XT	  = %14.4lf   (%9.6lf) (m)\n", XT, sqrt(N[0][0] * seg_new));
		fprintf (out, "YT     = %14.4lf   (%9.6lf) (m)\n", YT, sqrt(N[1][1] * seg_new));
		fprintf (out, "ZT     = %14.4lf   (%9.6lf) (m)\n", ZT, sqrt(N[2][2] * seg_new));
		
		fprintf (out, "S      = %14.4lf   (%9.6lf) (m)\n",  S, sqrt(N[3][3] * seg_new));
		
		fprintf (out, "omega  = %14.4lf   (%9.6lf) (deg)\n", om  * 180.0 / pi, sqrt(N[4][4] * seg_new));
		fprintf (out, "phi    = %14.4lf   (%9.6lf) (deg)\n", phi * 180.0 / pi, sqrt(N[5][5] * seg_new));
		fprintf (out, "kapa   = %14.4lf   (%9.6lf) (deg)\n", kap * 180.0 / pi, sqrt(N[6][6] * seg_new));
		
		fprintf (out, "\n\n");
	}
	
	double NDIST;
	
	NDIST = 0.0;
	
	for (i = 0; i < n; i++)
	{
		I = match_res_s.surf1_index[i];
		J = match_res_s.surf2_index[i];
		
		xa = p_sp1[I].x;
		ya = p_sp1[I].y;
		za = p_sp1[I].z;
		
		//xa -= DX;
		//ya -= DY;
		
		compute_XYZA();
		
		V1[0] = sp2[J].X[0];
		V1[1] = sp2[J].Y[0];
		V1[2] = sp2[J].Z[0];
		
		//V1[0] -= DX;
		//V1[1] -= DY;
		
		V2[0] = sp2[J].X[1];
		V2[1] = sp2[J].Y[1];
		V2[2] = sp2[J].Z[1];
		
		//V2[0] -= DX;
		//V2[1] -= DY;
		
		V3[0] = sp2[J].X[2];
		V3[1] = sp2[J].Y[2];
		V3[2] = sp2[J].Z[2];
		
		double checkA = 0.0;
		double checkB = 0.0;
		
		Compute_In_Out_NTH (&checkA, &checkB);
		
		NDIST += (checkA * checkA);
	}
	
	NDIST = sqrt (NDIST/(double)n);
	
	fprintf (out, "Average Normal Distance = %lf\n", NDIST);
	
	free_dmatrix (N , 0, 6, 0, 6);
	free_dmatrix (NT, 0, 6, 0, 6);
	free_dmatrix (v , 0, 6, 0, 6);
	
	free (x1);
	free (X1);
	free (A);
	free (C);
	free (w);
	free (xh);
	
	if (match_res_s.count != 0)
	{
		free (match_res_s.surf1_index);
		free (match_res_s.surf2_index);
	}
	
	match_res_s.count = 0;
	
	sigmo = seg_new;
	
	return (sigmo);
	
}

/***************************************************************************
Singular Value decompisition
***************************************************************************/

void dsvdcmp  (double **a, int m, int n, double *w, double **v)
{
	int flag,i,its,j,jj,k,l,nm;
	
	double anorm,c,f,g,h,s,scale,x,y,z,*rv1;
	
	rv1 = (double *)malloc(n * sizeof(double));
	//if (!rv1) printf("allocation failure for rv1");
	
	g = scale = anorm = 0.0;
	
	for (i=0 ; i < n ; i++) {
		l=i+1;
		rv1[i] = scale * g;
		g = s = scale = 0.0;
		if (i < m) {
			for (k=i ; k<m ;k++) scale += fabs(a[k][i]);
			if (scale) {
				for (k=i ; k<m ;k++) {
					a[k][i] /= scale;
					s += a[k][i]*a[k][i];
				}
				f = a[i][i];
				
				
				g = -SIGN(sqrt(s),f);
				
				h = f*g-s;
				a[i][i]=f-g;
				for (j=l; j<n ;j++) {
					for (s=0.0,k=i ; k<m ; k++) s += a[k][i]*a[k][j];
					f=s/h;
					for (k=i;k<m;k++) a[k][j] += f*a[k][i];
				}
				for (k=i ; k<m ; k++) a[k][i] *= scale;
			}
		}
		
		
		w[i] = scale *g;
		g = s = scale =0.0;
		if (i < m && i != n-1) {
			for (k=l ; k<n ; k++) scale += fabs(a[i][k]);
			if (scale) {
				for (k=l ; k<n ; k++) {
					a[i][k] /= scale;
					s += a[i][k]*a[i][k];
				}
				f=a[i][l];
				g = -SIGN(sqrt(s),f);
				h=f*g-s;
				a[i][l]=f-g;
				for (k=l ; k<n ; k++) rv1[k]=a[i][k]/h;
				for (j=l ; j<m ; j++) {
					for (s=0.0,k=l ; k<n ; k++) s += a[j][k]*a[i][k];
					for (k=l ; k<n ; k++) a[j][k] += s*rv1[k];
				}
				for (k=l ; k<n ;k++) a[i][k] *= scale;
			}
		}
		
		
		anorm=DMAX(anorm,(fabs(w[i])+fabs(rv1[i])));
		
	}
	
	for (i=n-1 ; i>=0 ; i--) {
		
		if (i < n-1) {
			if (g) {
				for (j=l ; j<n ; j++)
					v[j][i]=(a[i][j]/a[i][l])/g;
				for (j=l ; j<n ; j++) {
					for (s=0.0,k=l ; k<n ; k++) s += a[i][k]*v[k][j];
					for (k=l ; k<n ; k++) v[k][j] += s*v[k][i];
				}
			}
			for (j=l ; j<n ;j++) v[i][j]=v[j][i]=0.0;
		}
		v[i][i]=1.0;
		g=rv1[i];
		l=i;
	}
	
	for (i=IMIN(m-1,n-1) ; i>=0 ;i--) {
		l=i+1;
		g=w[i];
		for (j=l ; j<n ; j++) a[i][j]=0.0;
		if (g) {
			g=1.0/g;
			for (j=l ; j<n ; j++) {
				for (s=0.0,k=l ; k<m ; k++) s += a[k][i]*a[k][j];
				f=(s/a[i][i])*g;
				for (k=i ; k<m ; k++) a[k][j] += f*a[k][i];
			}
			for (j=i ; j<m ; j++) a[j][i] *= g;
		} else for (j=i ; j<m ; j++) a[j][i]=0.0;
		++a[i][i];
	}
	
	for (k=n-1 ; k>=0 ; k--) {
		for (its=1 ; its<=60 ; its++) {
			flag=1;
			for (l=k ; l>=0 ;l--) {
				nm=l-1;
				if ((double)(fabs(rv1[l])+anorm) == anorm) {
					flag=0;
					break;
				}
				if ((double)(fabs(w[nm])+anorm) == anorm) break;
			}
			if (flag) {
				c=0.0;
				s=1.0;
				for (i=l ; i<=k ; i++) {
					f = s*rv1[i];
					rv1[i]=c*rv1[i];
					if ((double)(fabs(f)+anorm) == anorm) break;
					g=w[i];
					h=pythag(f,g);
					w[i]=h;
					h=1.0/h;
					c=g*h;
					s = -f*h;
					for (j=0 ; j<m ; j++) {
						y=a[j][nm];
						z=a[j][i];
						a[j][nm]=y*c+z*s;
						a[j][i]=z*c-y*s;
					}
				}
			}
			z=w[k];
			if (l == k) {
				if (z < 0.0) {
					w[k] = -z;
					for (j=0;j<n;j++) v[j][k] = -v[j][k];
				}
				break;
			}
			//if (its == 40) printf("no convergence in 40 svdcmp iterations @ k = %d\n", k);
			x=w[l];
			nm=k-1;
			y=w[nm];
			g=rv1[nm];
			h=rv1[k];
			f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
			g=pythag(f,1.0);
			f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
			c=s=1.0;
			for (j=l ; j<=nm ; j++) {
				i=j+1;
				g=rv1[i];
				y=w[i];
				h=s*g;
				g=c*g;
				z=pythag(f,h);
				rv1[j]=z;
				c=f/z;
				s=h/z;
				f=x*c+g*s;
				g = g*c-x*s;
				h=y*s;
				y *= c;
				for (jj=0 ; jj<n ; jj++) {
					x=v[jj][j];
					z=v[jj][i];
					v[jj][j]=x*c+z*s;
					v[jj][i]=z*c-x*s;
				}
				z=pythag(f,h);
				w[j]=z;
				if (z) {
					z=1.0/z;
					c=f*z;
					s=h*z;
				}
				f=c*g+s*y;
				x=c*y-s*g;
				for (jj=0 ; jj<m ; jj++) {
					y=a[jj][j];
					z=a[jj][i];
					a[jj][j]=y*c+z*s;
					a[jj][i]=z*c-y*s;
				}
			}
			rv1[l]=0.0;
			rv1[k]=f;
			w[k]=x;
		} /* end for its */
	} /* end for k */
	free(rv1);
}

double pythag( double a, double  b)
{
	double absa,absb;
	absa=fabs(a);
	absb=fabs(b);
	if (absa > absb) return absa*sqrt(1.0 + DSQR(absb/absa));
	else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0 + DSQR(absa/absb)));
}

void mult_VVT (double *A, double *B, double **C, int n)
{
	int i, j;
	
	for (i = 0; i < n; i++)
		for (j = 0; j < n; j++)
			C[i][j] = A[i] * B[j];
		
}

void mult_VTV (double *A, double *B, double *C, int n)
{
	int i;
	
	*C = 0.0; 
	for (i = 0; i < n; i++)
		*C = A[i] * B[i];
	
}

void mult_ABT ( double **a , double **b , double **abt , int n , int m , int k )
{
	int i ,j , l;
	
	for  (i=0 ; i< n ; ++i)
		for  (j=0 ; j< k ; ++j)
		{
			abt[i][j]=0.0;
			for (l=0 ; l< m ; ++l)
				abt[i][j] = abt[i][j] + a[i][l]*b[j][l];
		}
}

void  mult_ATB (double **a , double **b , double **atb , int n , int m , int k)
{
	
	int i ,j , l;
	
	for  (i=0 ; i<m ; ++i)
		for  (j=0 ; j<k ; ++j)
		{
			atb[i][j]=0.0;
			
			for (l=0 ; l<n ; ++l)
			{
				atb[i][j] = atb[i][j] + a[l][i]*b[l][j];
			}
		}
		
}

void mult_AV ( double **a , double *y , double *c , int n , int m)
{
	int i , j ;
	for ( i = 0 ; i < n ; i ++)
	{
		c[i] = 0.0 ;
		for ( j = 0 ; j < m ; j++)
			c[i] += a[i][j]*y[j];
	}
	
}

void  mult_ATV ( double **a, double *y, double *c, int n, int m)
{
	int i , j ;
	for ( i = 0 ; i < m ; i ++)
    {
		c[i] = 0.0 ;
		for ( j = 0 ; j < n ; j++)
			c[i] += a[j][i]*y[j];
    }
	
}

int		abs_ort_icpoint		(char *file_name, FILE *inp, FILE *inp1, FILE *inp2, FILE *out)
{
	
	//Performs Absolute Orientation using Iterative Closest Point
	double  pi;
	char	line[200];
	int		no_of_it;
	int		I, i, j;
	
	pi = 2.0 * asin(1.0);
	
	double Sig_Threshold;
	
	fgets  (line, 200, inp);	//Description line (Approximate Values)
	fgets  (line, 200, inp);	//Data Line (distance threshold is the minimum distance to be considered for ICP)
	sscanf (line, "%lf %lf",&Sig_Threshold , &Distance_Threshold);
	
	fgets  (line, 200, inp);	//Description line (Approximate Values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf %lf %lf %lf %lf %lf",&XT  , &YT  , &ZT  , &S  , &om  , &phi  , &kap  );
	fclose (inp);
	
	om	*= pi / 180.0;
	phi *= pi / 180.0;
	kap *= pi / 180.0;
	
	char	pt_id[20];
	double	x, y, z;
	
	no_of_p_sp1 = no_of_p_sp2 = 0;
	
	//AH Warning Hard Coded Fixed Values.
	//To Avoid dealing with large numbers.
	
	DX = 0.0;	
	DY = 0.0;
	DZ = 0.0;
	
	delta_x = delta_y = delta_z = 15.0;	//To limit the search space
	
	//Reading the data associated with the first surface points.
	
	int i_skip = 0;
	
	while (fgets(line, 200, inp1) != NULL)
	{
		i_skip++;
		
		if ((i_skip % 1) == 0)
		{
			no_of_p_sp1++;
			
			if (no_of_p_sp1 == 1)
				p_sp1 = (P_SPI *) malloc (no_of_p_sp1 * sizeof(P_SPI));
			else
				p_sp1 = (P_SPI *) realloc (p_sp1, no_of_p_sp1 * sizeof(P_SPI));
			
			sscanf (line, "%lf %lf %lf", &x, &y, &z);
			
			sprintf (pt_id, "%d", no_of_p_sp1);
			
			i = no_of_p_sp1 - 1;
			
			strcpy (p_sp1[i].pt_id, pt_id);
			
			p_sp1[i].x = x;
			p_sp1[i].y = y;
			p_sp1[i].z = z;
			
			DX += x;
			DY += y;
			DZ += z;
		}
	};
	
	
	DX /= no_of_p_sp1;
	DY /= no_of_p_sp1;
	DZ /= no_of_p_sp1;
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		p_sp1[i].x -= DX;
		p_sp1[i].y -= DY;
		p_sp1[i].z -= DZ;
		
	}
	
	fprintf (out, "DX = %12.3lf\n", DX);
	fprintf (out, "DY = %12.3lf\n", DY);
	fprintf (out, "DZ = %12.3lf\n", DZ);
	
	//Reading the data associated with the second surface points.
	
	i_skip = 0;
	while (fgets(line, 200, inp2) != NULL)
	{
		i_skip++;
		
		if ((i_skip % 1) == 0)
		{
			no_of_p_sp2++;
			
			if (no_of_p_sp2 == 1)
				p_sp2 = (P_SPI *) malloc (no_of_p_sp2 * sizeof(P_SPI));
			else
				p_sp2 = (P_SPI *) realloc (p_sp2, no_of_p_sp2 * sizeof(P_SPI));
			
			sscanf (line, "%lf %lf %lf", &x, &y, &z);
			
			sprintf (pt_id, "%d", no_of_p_sp2);
			
			i = no_of_p_sp2 - 1;
			
			strcpy (p_sp2[i].pt_id, pt_id);
			
			p_sp2[i].x = x - DX;
			p_sp2[i].y = y - DY;
			p_sp2[i].z = z - DZ;
		}
	};
	
	fprintf (out, "Number of Surface I  Points  = %12d\n", no_of_p_sp1);
	fprintf (out, "Number of Surface II Points  = %12d\n", no_of_p_sp2);
	fprintf (out, "__________________________________________\n");
	
	char str[200];
	
	sprintf (str, "Number of Surface I  Points  = %12d\nNumber of Surface II Points = %12d\n", no_of_p_sp1, no_of_p_sp2);
	AfxMessageBox( str, MB_OK, 0);
	
	//Files for writing the accumulator arrays and the progress of the estimated parameters with iterations
	
	FILE	*fpx  , *fpy  , *fpz  , *fps  , *fpo  , *fpp  , *fpk;
	
	//Parameter convergence with iterations
	char	fpx_name[200];
	char	fpy_name[200];
	char	fpz_name[200];
	
	char	fps_name[200];
	
	char	fpo_name[200];
	char	fpp_name[200];
	char	fpk_name[200];
	
	strcpy	(fpx_name, file_name);
	prj_dir	(fpx_name);
	strcpy  (dir_name, fpx_name);
	strcat	(fpx_name, "XT_point.out");
	
	strcpy	(fpy_name, file_name);
	prj_dir	(fpy_name);
	strcat	(fpy_name, "YT_point.out");
	
	strcpy	(fpz_name, file_name);
	prj_dir	(fpz_name);
	strcat	(fpz_name, "ZT_point.out");
	
	strcpy	(fps_name, file_name);
	prj_dir	(fps_name);
	strcat	(fps_name, "SF_point.out");
	
	strcpy	(fpo_name, file_name);
	prj_dir	(fpo_name);
	strcat	(fpo_name, "OM_point.out");
	
	strcpy	(fpp_name, file_name);
	prj_dir	(fpp_name);
	strcat	(fpp_name, "PH_point.out");
	
	strcpy	(fpk_name, file_name);
	prj_dir	(fpk_name);
	strcat	(fpk_name, "KP_point.out");
	
	fpx = fopen (fpx_name, "w");
	fpy = fopen (fpy_name, "w");
	fpz = fopen (fpz_name, "w");
	
	fps = fopen (fps_name, "w");
	
	fpo = fopen (fpo_name, "w");
	fpp = fopen (fpp_name, "w");
	fpk = fopen (fpk_name, "w");
	
	
	//The matching process
	int flag = 0;
	
	CTime t;
	
	CTime time1, time2;
	
	// Beginning Time
	
	time1		= CTime::GetCurrentTime();;
	int hr1		= time1.GetHour();
	int	min1	= time1.GetMinute();
	int sec1	= time1.GetSecond();
	
	ITX = ITY = ITZ = ITS = ITO = ITP = ITK = 0;
	
	double  seg_bef = 100000.0;
	
	int J_min;
	
	
	fprintf (fpx,"%6d %14.4lf\n", ITX, XT);
	fprintf (fpy,"%6d %14.4lf\n", ITY, YT);
	fprintf (fpz,"%6d %14.4lf\n", ITZ, ZT);
	
	fprintf (fps,"%6d %14.4lf\n", ITS, S);
	
	fprintf (fpo,"%6d %14.4lf\n", ITO, om  * 180.0 / pi);
	fprintf (fpp,"%6d %14.4lf\n", ITP, phi * 180.0 / pi);
	fprintf (fpk,"%6d %14.4lf\n", ITK, kap * 180.0 / pi);
	
	
	double min_x_sp, min_y_sp, max_x_sp, max_y_sp;
	
	min_x_sp = min_y_sp =  100000000.0;
	max_x_sp = max_y_sp = -100000000.0;
	
	for (i = 0; i < no_of_p_sp2; i++)
	{
		if (p_sp2[i].x < min_x_sp)
			min_x_sp = p_sp2[i].x;
		if (p_sp2[i].y < min_y_sp)
			min_y_sp = p_sp2[i].y;
		if (p_sp2[i].x > max_x_sp)
			max_x_sp = p_sp2[i].x;
		if (p_sp2[i].y > max_y_sp)
			max_y_sp = p_sp2[i].y;
	}
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		if (p_sp1[i].x < min_x_sp)
			min_x_sp = p_sp1[i].x;
		if (p_sp1[i].y < min_y_sp)
			min_y_sp = p_sp1[i].y;
		if (p_sp1[i].x > max_x_sp)
			max_x_sp = p_sp1[i].x;
		if (p_sp1[i].y > max_y_sp)
			max_y_sp = p_sp1[i].y;
	}
	
	typedef struct
	{
		int n_i, n_j;
		int	*i;
		int *j;
		double x1, y1, x2, y2;
	} PT_INDX;
	
	
	//Partitioning the space to 100x100 tiles
	PT_INDX pt_indx[100][100];
	
	for (i = 0; i < 100; i++)
		for (j = 0; j < 100; j++)
			pt_indx[i][j].n_i = pt_indx[i][j].n_j = 0;
		
		for (i = 0; i < no_of_p_sp1; i++)
		{
			int I1 = int ((p_sp1[i].x - min_x_sp) /  ((max_x_sp - min_x_sp) / 100.0));
			int J1 = int ((p_sp1[i].y - min_y_sp) /  ((max_y_sp - min_y_sp) / 100.0));
			
			if (I1 == 100)
				I1--;
			if (J1 == 100)
				J1--;
			
			pt_indx[I1][J1].n_i++;
			if (pt_indx[I1][J1].n_i == 1)
				pt_indx[I1][J1].i = (int *) malloc                     (pt_indx[I1][J1].n_i * sizeof(int));
			else
				pt_indx[I1][J1].i = (int *) realloc (pt_indx[I1][J1].i, pt_indx[I1][J1].n_i * sizeof(int));
			
			pt_indx[I1][J1].i[pt_indx[I1][J1].n_i - 1] = i;
		}
		
		for (i = 0; i < no_of_p_sp2; i++)
		{
			int I2 = int ((p_sp2[i].x - min_x_sp) /  ((max_x_sp - min_x_sp) / 100.0));
			int J2 = int ((p_sp2[i].y - min_y_sp) /  ((max_y_sp - min_y_sp) / 100.0));
			
			if (I2 == 100)
				I2--;
			if (J2 == 100)
				J2--;
			
			pt_indx[I2][J2].n_j++;
			if (pt_indx[I2][J2].n_j == 1)
				pt_indx[I2][J2].j = (int *) malloc                     (pt_indx[I2][J2].n_j * sizeof(int));
			else
				pt_indx[I2][J2].j = (int *) realloc (pt_indx[I2][J2].j, pt_indx[I2][J2].n_j * sizeof(int));
			
			pt_indx[I2][J2].j[pt_indx[I2][J2].n_j - 1] = i;
		}
		
		no_of_it = 200;
		
		for (I = 0; I <no_of_it; I++)
		{
			
			match_res_s.count = 0;
			
			rotation();
			
			for (int ii = 0; ii < 100; ii++)
				for (int jj = 0; jj < 100; jj++)
				{
					
					for (i = 0; i < pt_indx[ii][jj].n_i; i++)
					{
						int II;
						II = pt_indx[ii][jj].i[i];
						xa = p_sp1[II].x;
						ya = p_sp1[II].y;
						za = p_sp1[II].z;
						
						double min_distance = 1000000.0;
						
						compute_XYZA();		//output (XA, YA, ZA)
						
						J_min = -1;
						
						for (j = 0; j < pt_indx[ii][jj].n_j; j++)
						{
							int JJ;
							JJ = pt_indx[ii][jj].j[j];
							
							double dx = p_sp2[JJ].x - XA;
							double dy = p_sp2[JJ].y - YA;
							double dz = p_sp2[JJ].z - ZA;
							
							if ((fabs (dx) > delta_x) || (fabs (dy) > delta_y) || (fabs (dz) > delta_z))
								continue;
							
							double dist = sqrt(dx * dx + dy * dy + dz * dz);
							if (dist < min_distance)
							{
								min_distance = dist;
								J_min = JJ;
							}
						}
						
						if (min_distance < Distance_Threshold)
						{
							
							match_res_s.count++;
							if (match_res_s.count == 1)
							{
								match_res_s.surf1_index = (int *) malloc (match_res_s.count * sizeof(int));
								match_res_s.surf2_index = (int *) malloc (match_res_s.count * sizeof(int));
							}
							else
							{
								match_res_s.surf1_index = (int *) realloc (match_res_s.surf1_index, match_res_s.count * sizeof(int));
								match_res_s.surf2_index = (int *) realloc (match_res_s.surf2_index, match_res_s.count * sizeof(int));
							}
							
							int III =  match_res_s.count - 1;
							match_res_s.surf1_index[III] = II;
							match_res_s.surf2_index[III] = J_min;
							
						}
					}
				}
				
				
				if (match_res_s.count > 8)	
				{
					fprintf (out, "Iteration Number %4d\n", I);
					fprintf (out, "---------------------\n\n");
					
					fprintf (out, "Number of Matched Points = %10d\n", match_res_s.count);
					
					double NDIST;
					double check = Compute_Parameters_points (out, 7, NDIST);
					
					ITX++;
					ITY++;
					ITZ++;
					ITS++;
					ITO++;
					ITP++;
					ITK++;
					
					fprintf (fpx,"%6d %14.4lf\n", ITX, XT);
					fprintf (fpy,"%6d %14.4lf\n", ITY, YT);
					fprintf (fpz,"%6d %14.4lf\n", ITZ, ZT);
					
					fprintf (fps,"%6d %14.4lf\n", ITS, S);
					
					fprintf (fpo,"%6d %14.4lf\n", ITO, om  * 180.0 / pi);
					fprintf (fpp,"%6d %14.4lf\n", ITP, phi * 180.0 / pi);
					fprintf (fpk,"%6d %14.4lf\n", ITK, kap * 180.0 / pi);
					
					double dseg = check - seg_bef;
					seg_bef = check;
					if (fabs (dseg) < Sig_Threshold)
						break;
					
				}
				
	}
	
	time2 = CTime::GetCurrentTime();;
	int hr2  = time2.GetHour();
	int	min2 = time2.GetMinute();
	int sec2 = time2.GetSecond();
	
	fprintf (out, "Starting   Time \t\t\t%2d %2d %2d\n", hr1, min1, sec1);
	fprintf (out, "Ending     Time \t\t\t%2d %2d %2d\n", hr2, min2, sec2);
	
	int dhr, dmin, dsec;
	
	if (sec2 >= sec1)
		dsec = sec2 - sec1;
	else
	{
		dsec = sec2 + 60 - sec1;
		min2 = min2 - 1;
	}
	
	if (min2 >= min1)
		dmin = min2 - min1;
	else
	{
		dmin = min2 + 60 - min1;
		hr2 = hr2 - 1;
	}
	
	if (hr2 >= hr1)
		dhr = hr2 - hr1;
	else
	{
		dhr = hr2 + 24 - hr1;
	}
	
	for (i = 0; i < 100; i++)
		for (j = 0; j < 100; j++)
		{
			if (pt_indx[i][j].n_i > 0)
				free(pt_indx[i][j].i);
			if (pt_indx[i][j].n_j > 0)
				free(pt_indx[i][j].j);
		}
		
		fprintf (out, "Processing Time \t\t\t%2d %2d %2d\n", dhr, dmin, dsec);
		
		fclose (fpx);
		fclose (fpy);
		fclose (fpz);
		fclose (fps);
		fclose (fpo);
		fclose (fpp);
		fclose (fpk);
		
		fclose (out);
		
		free_dmatrix (R, 0, 2, 0, 2);
		
		free (p_sp2);
		free (p_sp1);
		
		sprintf (str, "DONE %20s!!\n", file_name);
		AfxMessageBox( str, MB_OK, 0);
		return (1);
}

double Compute_Parameters_points (FILE *out, int t, double &NDIST)
{
	// t == 3	Solve for XT, YT, ZT
	// t == 4	Solve for XT, YT, ZT, OM
	// t == 5	Solve for XT, YT, ZT, OM, PH
	// t == 6	Solve for XT, YT, ZT, OM, PH, KP
	// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
	
	double	sigmo;
	double	**A;
	double	*y;
	double  *xh, *C, *CT;
	double	**N, **NT, **v, *w;
	
	double	pi;
	
	sigmo = 0.0;
	
	double xt_a  = XT;
	double yt_a  = YT;
	double zt_a  = ZT;
	double s_a   = S;
	double om_a  = om;
	double phi_a = phi;
	double kap_a = kap;
	
	pi = 2.0 * asin(1.0);
	
	A  = dmatrix (0, 2, 0, 6);
	N  = dmatrix (0, 6, 0, 6);
	NT = dmatrix (0, 6, 0, 6);
	v  = dmatrix (0, 6, 0, 6);
	
	
	y	=	(double *) malloc (3 * sizeof(double));
	C	=	(double *) malloc (7 * sizeof(double));
	CT	=	(double *) malloc (7 * sizeof(double));
	xh  =   (double *) malloc (7 * sizeof(double));
	w	=	(double *) malloc (7 * sizeof(double));
	
	int		no_it, max_it;
	double	etpe, seg_new, seg_old, dif, threshold;
	
	seg_old = LARGE;
	max_it		= 30;
	
	int flag_sing = 0;
	
	threshold = 0.00000000001;
	
	int	ii, jj, i, j, n, I, J;
	
	n = match_res_s.count;
	
	if (n < 8)
	{
		fprintf (out, "\n\nThere are not enough points to compute the transformation parameters\n");
		fprintf (out, "####################################################################\n\n");
		return (-1.0);
	}
	
	no_it = 0;
	do 
	{
		rotation ();
		
		etpe = 0.0;
		
		for (i = 0; i < 7; i++)
		{
			C[i] = 0.0;
			for (j = 0; j < 7; j++)
				N[i][j] = 0.0;
		}
		
		for (i = 0; i < n; i++)
		{
			I = match_res_s.surf1_index[i];
			J = match_res_s.surf2_index[i];
			
			xa = p_sp1[I].x;
			ya = p_sp1[I].y;
			za = p_sp1[I].z;
			
			
			compute_XYZA();		//output (XA, YA, ZA)
			compute_XYZa();		//no scale or shift is applied (output Xa, Ya, Za) 			
			
			y[0] = p_sp2[J].x - XA;
			y[1] = p_sp2[J].y - YA;
			y[2] = p_sp2[J].z - ZA;
			
			//partial derivatives of the x-equation
			A[0][0] =  1.0;                                          //dx/dxt
			A[0][1] =  0.0;                                          //dx/dyt                      
			A[0][2] =  0.0;                                          //x/dzt    
			A[0][3] =  Xa;	                                         //dx/dsf
			A[0][4] =  0.0;                                          //dx/dom (m/rad)
			A[0][5] = -S * (sin(om) * Ya - cos(om) * Za);	         //dx/dph (m/rad)
			A[0][6] =  S * (R[0][1] * xa - R[0][0] * ya);		     //dx/dkp (m/rad)
			
			A[1][0] =  0.0;                                          //dy/dxt
			A[1][1] =  1.0;                                          //dy/dyt
			A[1][2] =  0.0;                                          //dy/dzt
			A[1][3] =  Ya;	                                         //dy/dsf
			A[1][4] = -S * Za;	                                     //dy/dom (m/rad)
			A[1][5] =  S * sin(om) * Xa;	                         //dy/dph (m/rad)
			A[1][6] =  S * (R[1][1] * xa - R[1][0] * ya);			 //dy/dkp (m/rad)
			
			A[2][0] =  0.0;                                          //dz/dxt
			A[2][1] =  0.0;                                          //dz/dyt
			A[2][2] =  1.0;                                          //dz/dzt
			A[2][3] =  Za;	                                         //dz/dsf
			A[2][4] =  S * Ya;	                                     //dz/dom (m/rad)
			A[2][5] = -S * cos(om) * Xa;	                         //dz/dph (m/rad)  
			A[2][6] =  S * (R[2][1] * xa - R[2][0] * ya);	         //dz/dkp (m/rad)
			
			mult_ATB (A, A, NT, 3, 7, 7);
			
			for (ii = 0; ii < 7; ii++)
				for (jj = 0; jj < 7; jj++)
					N[ii][jj] += NT[ii][jj];
				
				mult_ATV (A, y, CT, 3, 7);
				for (ii = 0; ii < 7; ii++)
					C[ii] += CT[ii];
				
				double sig_t;
				mult_VTV (y, y, &sig_t, 3);
				etpe += sig_t;
		}
		
		
		if (t == 6)
		{
			// t == 3	Solve for XT, YT, ZT
			// t == 4	Solve for XT, YT, ZT, OM
			// t == 5	Solve for XT, YT, ZT, OM, PH
			// t == 6	Solve for XT, YT, ZT, OM, PH, KP
			// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			C[3] = 0.0;
			N[3][3] = 1.0;
			for (ii = 0; ii < 7; ii++)
				if (ii != 3)
					N[ii][3] = 0.0;
				
				for (jj = 0; jj < 7; jj++)
					if (jj != 3)
						N[3][jj] = 0.0;
		}
		
		if (t == 5)
		{
			// t == 3	Solve for XT, YT, ZT
			// t == 4	Solve for XT, YT, ZT, OM
			// t == 5	Solve for XT, YT, ZT, OM, PH
			// t == 6	Solve for XT, YT, ZT, OM, PH, KP
			// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			C[3] = 0.0;
			N[3][3] = 1.0;
			for (ii = 0; ii < 7; ii++)
				if (ii != 3)
					N[ii][3] = 0.0;
				
				for (jj = 0; jj < 7; jj++)
					if (jj != 3)
						N[3][jj] = 0.0;
					
					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;
						
						for (jj = 0; jj < 7; jj++)
							if (jj != 6)
								N[6][jj] = 0.0;
		}
		
		if (t == 4)
		{
			// t == 3	Solve for XT, YT, ZT
			// t == 4	Solve for XT, YT, ZT, OM
			// t == 5	Solve for XT, YT, ZT, OM, PH
			// t == 6	Solve for XT, YT, ZT, OM, PH, KP
			// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			C[3] = 0.0;
			N[3][3] = 1.0;
			for (ii = 0; ii < 7; ii++)
				if (ii != 3)
					N[ii][3] = 0.0;
				
				for (jj = 0; jj < 7; jj++)
					if (jj != 3)
						N[3][jj] = 0.0;
					
					C[5] = 0.0;
					N[5][5] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 5)
							N[ii][5] = 0.0;
						
						for (jj = 0; jj < 7; jj++)
							if (jj != 5)
								N[5][jj] = 0.0;
							
							C[6] = 0.0;
							N[6][6] = 1.0;
							for (ii = 0; ii < 7; ii++)
								if (ii != 6)
									N[ii][6] = 0.0;
								
								for (jj = 0; jj < 7; jj++)
									if (jj != 6)
										N[6][jj] = 0.0;
		}
		
		if (t == 3)
		{
			// t == 3	Solve for XT, YT, ZT
			// t == 4	Solve for XT, YT, ZT, OM
			// t == 5	Solve for XT, YT, ZT, PH, KP
			// t == 6	Solve for XT, YT, ZT, OM, PH, KP
			// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			C[3] = 0.0;
			N[3][3] = 1.0;
			for (ii = 0; ii < 7; ii++)
				if (ii != 3)
					N[ii][3] = 0.0;
				
				for (jj = 0; jj < 7; jj++)
					if (jj != 3)
						N[3][jj] = 0.0;
					
					C[4] = 0.0;
					N[4][4] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 4)
							N[ii][4] = 0.0;
						
						for (jj = 0; jj < 7; jj++)
							if (jj != 4)
								N[4][jj] = 0.0;
							
							C[5] = 0.0;
							N[5][5] = 1.0;
							for (ii = 0; ii < 7; ii++)
								if (ii != 5)
									N[ii][5] = 0.0;
								
								for (jj = 0; jj < 7; jj++)
									if (jj != 5)
										N[5][jj] = 0.0;
									
									C[6] = 0.0;
									N[6][6] = 1.0;
									for (ii = 0; ii < 7; ii++)
										if (ii != 6)
											N[ii][6] = 0.0;
										
										for (jj = 0; jj < 7; jj++)
											if (jj != 6)
												N[6][jj] = 0.0;
		}
		
		dsvdcmp ( N , 7, 7, w, v);
		
		//AH Adjustment SVDcmp
		
		//*
		fprintf (out, "The Eigen Values\n");
		
		for ( ii = 0; ii < 7; ii++)
		{
			if (w[ii] < 0.00000001)
				flag_sing = 1;
			
			fprintf (out, "w[%3d] = %12.6lf\n", ii, w[ii]);
		}
		
		if (flag_sing == 1)
			break;
		
		//*/
		
		for ( jj = 0 ; jj < 7 ; jj++)
			for ( ii = 0 ; ii < 7 ; ii++)
				v[ii][jj] /= sqrt(w[jj]);
			
			
			mult_ABT (v , v , N , 7 , 7 , 7);
			mult_AV  (N , C , xh , 7 , 7);
			
			XT	+= (xh[0]);
			YT	+= (xh[1]);
			ZT  += (xh[2]);
			
			S	+= (xh[3]);
			
			om	+= (xh[4]);
			phi	+= (xh[5]);
			kap	+= (xh[6]);
			
			seg_new = etpe / ((double) n * 3.0 - 7.0);
			dif = seg_new - seg_old;
			seg_old = seg_new;			 
			
			//*
			fprintf (out, "Iteration # %5d\n", no_it);
			fprintf (out, "Estimated Variance Component = %lf\n", sqrt(seg_new));
			
			fprintf (out, "XT	  = %14.4lf   (%9.6lf) (m)\n", XT, sqrt(N[0][0] * seg_new));
			fprintf (out, "YT     = %14.4lf   (%9.6lf) (m)\n", YT, sqrt(N[1][1] * seg_new));
			fprintf (out, "ZT     = %14.4lf   (%9.6lf) (m)\n", ZT, sqrt(N[2][2] * seg_new));
			
			fprintf (out, "S      = %14.4lf   (%9.6lf) (m)\n",  S, sqrt(N[3][3] * seg_new));
			
			fprintf (out, "omega  = %14.4lf   (%9.6lf) (deg)\n", om  * 180.0 / pi, sqrt(N[4][4] * seg_new));
			fprintf (out, "phi    = %14.4lf   (%9.6lf) (deg)\n", phi * 180.0 / pi, sqrt(N[5][5] * seg_new));
			fprintf (out, "kapa   = %14.4lf   (%9.6lf) (deg)\n", kap * 180.0 / pi, sqrt(N[6][6] * seg_new));
			
			fprintf (out, "\n\n");
			//*/
			
			no_it++;
			
	} while ((no_it < max_it) && (fabs(dif) > threshold));
	
	if (flag_sing == 1)
	{
		XT  = xt_a;
		YT  = yt_a;
		ZT  = zt_a;
		S   = s_a;
		om  = om_a;
		phi = phi_a;
		kap = kap_a;
		
		fprintf (out, "Iteration # %5d\n", no_it);
		fprintf (out, "Estimated Variance Component = %lf\n", sqrt(seg_new));
		
		fprintf (out, "XT	  = %14.4lf (m)\n", XT);
		fprintf (out, "YT     = %14.4lf (m)\n", YT);
		fprintf (out, "ZT     = %14.4lf (m)\n", ZT);
		
		fprintf (out, "S      = %14.4lf    \n",  S);
		
		fprintf (out, "omega  = %14.4lf   (deg)\n", om  * 180.0 / pi);
		fprintf (out, "phi    = %14.4lf   (deg)\n", phi * 180.0 / pi);
		fprintf (out, "kapa   = %14.4lf   (deg)\n", kap * 180.0 / pi);
	}
	else
	{
		//AH Adjustment Intermediate Results
		
		fprintf (out, "Iteration # %5d\n", no_it);
		fprintf (out, "#################\n");
		
		fprintf (out, "Estimated Variance Component = %lf\n", sqrt(seg_new));
		
		fprintf (out, "XT	  = %14.4lf   (%9.6lf) (m)\n", XT, sqrt(N[0][0] * seg_new));
		fprintf (out, "YT     = %14.4lf   (%9.6lf) (m)\n", YT, sqrt(N[1][1] * seg_new));
		fprintf (out, "ZT     = %14.4lf   (%9.6lf) (m)\n", ZT, sqrt(N[2][2] * seg_new));
		
		fprintf (out, "S      = %14.4lf   (%9.6lf) (m)\n",  S, sqrt(N[3][3] * seg_new));
		
		fprintf (out, "omega  = %14.4lf   (%9.6lf) (deg)\n", om  * 180.0 / pi, sqrt(N[4][4] * seg_new));
		fprintf (out, "phi    = %14.4lf   (%9.6lf) (deg)\n", phi * 180.0 / pi, sqrt(N[5][5] * seg_new));
		fprintf (out, "kapa   = %14.4lf   (%9.6lf) (deg)\n", kap * 180.0 / pi, sqrt(N[6][6] * seg_new));
		
		fprintf (out, "\n\n");
	}
	
	NDIST = 0.0;
	
	rotation ();
	
	for (i = 0; i < n; i++)
	{
		I = match_res_s.surf1_index[i];
		J = match_res_s.surf2_index[i];
		
		xa = p_sp1[I].x;
		ya = p_sp1[I].y;
		za = p_sp1[I].z;
		
		compute_XYZA();
		
		double dx, dy, dz;
		
		dx = p_sp2[J].x - XA;
		dy = p_sp2[J].y - YA;
		dz = p_sp2[J].z - ZA;
		
		NDIST += sqrt(dx * dx + dy * dy + dz * dz);
	}
	
	NDIST = NDIST/(double)n;
	
	fprintf (out, "Average Distance = %lf\n", NDIST);
	
	free_dmatrix (A , 0, 2, 0, 6);
	free_dmatrix (N , 0, 6, 0, 6);
	free_dmatrix (NT, 0, 6, 0, 6);
	free_dmatrix (v , 0, 6, 0, 6);
	
	free (y);
	free (C);
	free (CT);
	free (w);
	free (xh);
	
	if (match_res_s.count != 0)
	{
		free (match_res_s.surf1_index);
		free (match_res_s.surf2_index);
	}
	
	match_res_s.count = 0;
	
	sigmo = seg_new;
	
	return (sigmo);
	
}

int		abs_ort_icpatch		(char *file_name, FILE *inp, FILE *inp1, FILE *inp2, FILE *out)
{
	
	//Performs Absolute Orientation using Iterative Closest Point
	double  pi;
	char	line[200];
	int		i, j;
	
	double  seg_bef = 100000.0;
	int		I_NO_IT = 0;
	
	pi = 2.0 * asin(1.0);
	
	double Sig_Threshold;
	
	fgets  (line, 200, inp);	//Description line (Approximate Values)
	fgets  (line, 200, inp);	//Data Line (distance threshold is the minimum distance to be considered for ICP)
	sscanf (line, "%lf %lf",&Sig_Threshold , &Distance_Threshold);
	
	fgets  (line, 200, inp);	//Description line (Approximate Values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf %lf %lf %lf %lf %lf",&XT  , &YT  , &ZT  , &S  , &om  , &phi  , &kap  );
	fclose (inp);
	
	om	*= pi / 180.0;
	phi *= pi / 180.0;
	kap *= pi / 180.0;
	
	char	pt_id[20], patch_id[20];
	double	x, y, z;
	double  X[3], Y[3], Z[3];
	
	no_of_p_sp1 = 0;
	no_of_sp2	= 0;
	
	delta_x = delta_y = 15.0;	//To limit the search space
	
	FILE	*pnt_dat, *tin_dat;
	char	pnt_dat_name[200], tin_dat_name[200];
	
	strcpy	(pnt_dat_name, file_name);
	prj_dir	(pnt_dat_name);
	strcat	(pnt_dat_name, "pnt.dat");
	pnt_dat = fopen(pnt_dat_name, "w");
	
	strcpy	(tin_dat_name, file_name);
	prj_dir	(tin_dat_name);
	strcat	(tin_dat_name, "tin.dat");
	tin_dat = fopen(tin_dat_name, "w");
	
	fprintf (pnt_dat, "Easting\tNorthing\tUp\n");
	fprintf (tin_dat, "Easting\tNorthing\tUp\n");
	
	//Reading the data associated with the first surface points.
	
	DX = DY = DZ = 0;
	
	while (fgets(line, 200, inp1) != NULL)
	{
		no_of_p_sp1++;
		
		if (no_of_p_sp1 == 1)
			p_sp1 = (P_SPI *) malloc (no_of_p_sp1 * sizeof(P_SPI));
		else
			p_sp1 = (P_SPI *) realloc (p_sp1, no_of_p_sp1 * sizeof(P_SPI));
		
		sscanf (line, "%s %lf %lf %lf", pt_id, &x, &y, &z);
		
		i = no_of_p_sp1 - 1;
		
		strcpy (p_sp1[i].pt_id, pt_id);
		
		p_sp1[i].x = x;
		p_sp1[i].y = y;
		p_sp1[i].z = z;
		
		DX += p_sp1[i].x;
		DY += p_sp1[i].y;
		DZ += p_sp1[i].z;
		
		fprintf (pnt_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", p_sp1[i].x, p_sp1[i].y, p_sp1[i].z);
	};
	
	//AH Warning Hard Coded Fixed Values.
	//To Avoid dealing with large numbers.
	
	DX /= no_of_p_sp1;
	DY /= no_of_p_sp1;
	DZ /= no_of_p_sp1;
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		p_sp1[i].x -= DX;
		p_sp1[i].y -= DY;
		p_sp1[i].z -= DZ;	
	}
	
	fclose (pnt_dat);
	
	fprintf (out, "DX = %12.3lf\n", DX);
	fprintf (out, "DY = %12.3lf\n", DY);
	fprintf (out, "DZ = %12.3lf\n", DZ);
	
	//Reading The Data associated with the second surface patches.
	
	int		WTXY, WTZ;
	while (fgets(line, 200, inp2) != NULL)
	{
		no_of_sp2++;
		
		if (no_of_sp2 == 1)
			sp2 = (SPII *) malloc  (no_of_sp2 * sizeof(SPII));
		else
			sp2 = (SPII *) realloc (sp2, no_of_sp2 * sizeof(SPII));
		
		sscanf (line, "%s %d %d", patch_id, &WTXY, &WTZ);
		
		fgets (line, 200, inp2);
		sscanf (line, "%lf %lf %lf ", &X[0], &Y[0], &Z[0]);
		
		fgets (line, 200, inp2);
		sscanf (line, "%lf %lf %lf ", &X[1], &Y[1], &Z[1]);
		
		fgets (line, 200, inp2);
		sscanf (line, "%lf %lf %lf ", &X[2], &Y[2], &Z[2]);
		
		int i = no_of_sp2 - 1;
		
		strcpy (sp2[i].patch_id, patch_id);
		
		sp2[i].WTXY = WTXY;
		sp2[i].WTZ  = WTZ;
		
		sp2[i].X[0] = X[0] - DX;
		sp2[i].Y[0] = Y[0] - DY;
		sp2[i].Z[0] = Z[0] - DZ;
		
		sp2[i].X[1] = X[1] - DX;
		sp2[i].Y[1] = Y[1] - DY;
		sp2[i].Z[1] = Z[1] - DZ;
		
		sp2[i].X[2] = X[2] - DX;
		sp2[i].Y[2] = Y[2] - DY;
		sp2[i].Z[2] = Z[2] - DZ;
		
		fprintf (tin_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", sp2[i].X[0], sp2[i].Y[0], sp2[i].Z[0]);
		fprintf (tin_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", sp2[i].X[1], sp2[i].Y[1], sp2[i].Z[1]);
		fprintf (tin_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", sp2[i].X[2], sp2[i].Y[2], sp2[i].Z[2]);
	};
	
	fclose (tin_dat);
	
	Angle_Threshold = 0.1;
	
	// Find the range for each patch
	
	for (i = 0; i < no_of_sp2; i++)
		find_min_max (i);
	
	fprintf (out, "Number of Surface I  Points  = %12d\n", no_of_p_sp1);
	fprintf (out, "Number of Surface II Patches = %12d\n", no_of_sp2);
	fprintf (out, "__________________________________________\n");
	
	char str[200];
	
	sprintf (str, "Number of Surface I  Points  = %12d\nNumber of Surface II Patches = %12d\n", no_of_p_sp1, no_of_sp2);
	AfxMessageBox( str, MB_OK, 0);
	
	//Files for writing the accumulator arrays and the progress of the estimated parameters with iterations
	
	FILE	*fpx  , *fpy  , *fpz  , *fps  , *fpo  , *fpp  , *fpk;
	
	char	fpx_name[200];
	char	fpy_name[200];
	char	fpz_name[200];
	
	char	fps_name[200];
	
	char	fpo_name[200];
	char	fpp_name[200];
	char	fpk_name[200];
	
	strcpy	(fpx_name, file_name);
	prj_dir	(fpx_name);
	strcpy  (dir_name, fpx_name);
	strcat	(fpx_name, "XT.out");
	
	strcpy	(fpy_name, file_name);
	prj_dir	(fpy_name);
	strcat	(fpy_name, "YT.out");
	
	strcpy	(fpz_name, file_name);
	prj_dir	(fpz_name);
	strcat	(fpz_name, "ZT.out");
	
	strcpy	(fps_name, file_name);
	prj_dir	(fps_name);
	strcat	(fps_name, "SF.out");
	
	strcpy	(fpo_name, file_name);
	prj_dir	(fpo_name);
	strcat	(fpo_name, "OM.out");
	
	strcpy	(fpp_name, file_name);
	prj_dir	(fpp_name);
	strcat	(fpp_name, "PH.out");
	
	strcpy	(fpk_name, file_name);
	prj_dir	(fpk_name);
	strcat	(fpk_name, "KP.out");
	
	fpx = fopen (fpx_name, "w");
	fpy = fopen (fpy_name, "w");
	fpz = fopen (fpz_name, "w");
	
	fps = fopen (fps_name, "w");
	
	fpo = fopen (fpo_name, "w");
	fpp = fopen (fpp_name, "w");
	fpk = fopen (fpk_name, "w");
	
	//The matching process
	int flag = 0;
	
	CTime t;
	
	double check;
	CTime time1, time2;
	
	// Beginning Time
	
	time1		= CTime::GetCurrentTime();;
	int hr1		= time1.GetHour();
	int	min1	= time1.GetMinute();
	int sec1	= time1.GetSecond();
	
	ITX = ITY = ITZ = ITS = ITO = ITP = ITK = 0;
	
	
	typedef struct
	{
		int  n_i;
		int *i;
	}
	TILE;
	
	//	TILE tile[450][450];
	
	int tile_size = 450;
	TILE **tile = new TILE*[tile_size];
	for(i=0;i<tile_size;i++)
	{
		tile[i] = new TILE[tile_size];
	}
	
	double min_x_sp, min_y_sp, max_x_sp, max_y_sp;
	
	min_x_sp = min_y_sp =  100000000.0;
	max_x_sp = max_y_sp = -100000000.0;
	
	for (i = 0; i < no_of_sp2; i++)
	{
		if (min_x_sp < sp2[i].min_x)
			min_x_sp = sp2[i].min_x;
		if (min_y_sp < sp2[i].min_y)
			min_y_sp = sp2[i].min_y;
		if (max_x_sp > sp2[i].max_x)
			max_x_sp = sp2[i].max_x;
		if (max_y_sp > sp2[i].max_y)
			max_y_sp = sp2[i].max_y;
	}
	
	min_x_sp -= 200.0;
	min_y_sp -= 200.0;
	
	max_x_sp += 200.0;
	max_y_sp += 200.0;
	
	for (i = 0; i < tile_size; i++)
		for (j = 0; j < tile_size; j++)
			tile[i][j].n_i = 0;
		
		for (i = 0; i < no_of_sp2; i++)
		{
			
			int I1 = int ((sp2[i].min_x - min_x_sp) /  ((max_x_sp - min_x_sp) / tile_size));
			int J1 = int ((sp2[i].min_y - min_y_sp) /  ((max_y_sp - min_y_sp) / tile_size));
			
			if (I1 == tile_size)
				I1--;
			if (J1 == tile_size)
				J1--;
			
			tile[I1][J1].n_i++;
			if (tile[I1][J1].n_i == 1)
				tile[I1][J1].i = (int *) malloc                  (tile[I1][J1].n_i * sizeof(int));
			else
				tile[I1][J1].i = (int *) realloc (tile[I1][J1].i, tile[I1][J1].n_i * sizeof(int));
			
			tile[I1][J1].i[tile[I1][J1].n_i - 1] = i;
		}
		
		for (int NNN = 0; NNN < 30; NNN++)
		{
			fprintf (out, "Iteration Number %4d\n", NNN);
			fprintf (out, "---------------------\n\n");
			
			ITX++;
			ITY++;
			ITZ++;
			ITS++;
			ITO++;
			ITP++;
			ITK++;
			
			double	CHECK1;
			
			rotation ();
			match_res_s.count = 0;
			
			/*******************/
			
			for (i = 0; i < no_of_p_sp1; i++)
			{
				xa = p_sp1[i].x;
				ya = p_sp1[i].y;
				za = p_sp1[i].z;
				
				compute_XYZA();
				
				int I1 = int ((XA - min_x_sp) /  ((max_x_sp - min_x_sp) / tile_size));
				int J1 = int ((YA - min_y_sp) /  ((max_y_sp - min_y_sp) / tile_size));
				
				if ((I1 < 0) || (I1 >= tile_size) || (J1 < 0) || (J1 >= tile_size))
					continue;
				
				double	min_dist_dif = 50.0;
				int		patch_id;
				
				
				for (int k = 0; k < tile[I1][J1].n_i; k++)
				{
					j = tile[I1][J1].i[k];
					
					if (XA < (sp2[j].min_x - delta_x))
						continue;
					
					if (XA > (sp2[j].max_x + delta_x))
						continue;
					
					if (YA < (sp2[j].min_y - delta_y))
						continue;
					
					if (YA > (sp2[j].max_y + delta_y))
						continue;
					
					V1[0] = sp2[j].X[0];
					V1[1] = sp2[j].Y[0];
					V1[2] = sp2[j].Z[0];
					
					V2[0] = sp2[j].X[1];
					V2[1] = sp2[j].Y[1];
					V2[2] = sp2[j].Z[1];
					
					V3[0] = sp2[j].X[2];
					V3[1] = sp2[j].Y[2];
					V3[2] = sp2[j].Z[2];
					
					double checkA = 0.0;
					double checkB = 0.0;
					
					Compute_In_Out_NTH (&checkA, &checkB);
					
					if ((fabs(checkA) < min_dist_dif) && (fabs(checkB - 360) < Angle_Threshold))
					{
						min_dist_dif = fabs(checkA);
						patch_id = j;
						CHECK1 = checkB;
					}
				}
				
				if ((min_dist_dif < Distance_Threshold) && (fabs(CHECK1 - 360) < Angle_Threshold))
				{
					//fprintf (out, "point %5s is inside surface patch %5s - Normal Distance = %12.4lf\n", p_sp1[i].pt_id, sp2[patch_id].patch_id, CHECK);
					
					match_res_s.count++;
					if (match_res_s.count == 1)
					{
						match_res_s.surf1_index = (int *) malloc (match_res_s.count * sizeof(int));
						match_res_s.surf2_index = (int *) malloc (match_res_s.count * sizeof(int));
					}
					else
					{
						match_res_s.surf1_index = (int *) realloc (match_res_s.surf1_index, match_res_s.count * sizeof(int));
						match_res_s.surf2_index = (int *) realloc (match_res_s.surf2_index, match_res_s.count * sizeof(int));
					}
					
					int I =  match_res_s.count - 1;
					match_res_s.surf1_index[I] = i;
					match_res_s.surf2_index[I] = patch_id;
				}
				
			}
			
			fprintf (out, "Number of Matched Point-Patch Pairs = %10d\n", match_res_s.count);
			
			fprintf (out, "\n\nLeast Squares Adjustment\n");
			fprintf (out, "-------------------------\n");
			
			fprintf (out, "\n\n");
			// t == 3	Solve for XT, YT, ZT
			// t == 4	Solve for XT, YT, ZT, OM
			// t == 5	Solve for XT, YT, ZT, OM, PH
			// t == 6	Solve for XT, YT, ZT, OM, PH, KP
			// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			
			check = Compute_Parameters(out, 7);
			
			fprintf (fpx,"%6d %14.4lf\n", ITX, XT);
			fprintf (fpy,"%6d %14.4lf\n", ITY, YT);
			fprintf (fpz,"%6d %14.4lf\n", ITZ, ZT);
			
			fprintf (fps,"%6d %14.4lf\n", ITS, S);
			
			fprintf (fpo,"%6d %14.4lf\n", ITO, om  * 180.0 / pi);
			fprintf (fpp,"%6d %14.4lf\n", ITP, phi * 180.0 / pi);
			fprintf (fpk,"%6d %14.4lf\n", ITK, kap * 180.0 / pi);
			
			double dseg = check - seg_bef;
			
			seg_bef = check;
			if (fabs (dseg) < Sig_Threshold)
				I_NO_IT++;
			else
				I_NO_IT = 0;
			
			if (I_NO_IT >= 9)
				break;
			
	}
	
	int n_m, n_nm;
	n_m = n_nm = 0;
	
	if (check != 0.0)
	{
		char fpm_name[200];
		FILE *fpm;
		
		strcpy	(fpm_name, file_name);
		prj_dir	(fpm_name);
		strcat	(fpm_name, "Matched.dat");
		
		fpm = fopen (fpm_name, "w");
		fprintf (fpm,"Easting\t\tNorthing\t\tUp\n");
		
		char fpmn_name[200];
		FILE *fpmn;
		
		strcpy	(fpmn_name, file_name);
		prj_dir	(fpmn_name);
		strcat	(fpmn_name, "NMatched.dat");
		
		fpmn = fopen (fpmn_name, "w");
		fprintf (fpmn,"Easting\t\tNorthing\t\tUp\n");
		
		char fp_nm_name[200];
		FILE *fp_nm;
		
		strcpy	(fp_nm_name, file_name);
		prj_dir	(fp_nm_name);
		strcat	(fp_nm_name, "Non_NMatched.dat");
		
		fp_nm = fopen (fp_nm_name, "w");
		fprintf (fp_nm,"Easting\t\tNorthing\t\tUp\n");
		
		fprintf (out, "Matched Points\n");
		fprintf (out, "***************\n\n");
		
		rotation ();
		
		for (i = 0; i < no_of_p_sp1; i++)
		{
			xa = p_sp1[i].x;
			ya = p_sp1[i].y;
			za = p_sp1[i].z;
			
			compute_XYZA();
			
			int flag_matched = 0;
			
			double checkA; 
			double checkB; 
			
			int I1 = int ((XA - min_x_sp) /  ((max_x_sp - min_x_sp) / tile_size));
			int J1 = int ((YA - min_y_sp) /  ((max_y_sp - min_y_sp) / tile_size));
			
			if ((I1 < 0) || (I1 >= tile_size) || (J1 < 0) || (J1 >= tile_size))
				continue;
			
			
			for (int k = 0; k < tile[I1][J1].n_i; k++)
			{
				j = tile[I1][J1].i[k];
				
				V1[0] = sp2[j].X[0];
				V1[1] = sp2[j].Y[0];
				V1[2] = sp2[j].Z[0];
				
				V2[0] = sp2[j].X[1];
				V2[1] = sp2[j].Y[1];
				V2[2] = sp2[j].Z[1];
				
				V3[0] = sp2[j].X[2];
				V3[1] = sp2[j].Y[2];
				V3[2] = sp2[j].Z[2];
				
				checkA = 0.0;
				checkB = 0.0;
				
				Compute_In_Out_NTH (&checkA, &checkB);
				
				if ((checkA < Distance_Threshold) && (fabs(checkB - 360.0) < Angle_Threshold))
				{
					fprintf (out , "point %5s and surface patch %5s are coplananr - NDistance = %12.4lf Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, checkA, checkB);
					fprintf (fpm , "%10s %12.4lf %12.4lf %12.4lf\n", p_sp1[i].pt_id, xa, ya, za);
					fprintf (fpmn, "%10s %12.4lf %12.4lf %12.4lf\n", p_sp1[i].pt_id, XA, YA, ZA);
					flag_matched = 1;
					n_m++;
				}
				
			}
			
			if (flag_matched == 0)
			{
				fprintf (fp_nm, "%10s %12.4lf %12.4lf %12.4lf\n", p_sp1[i].pt_id, XA, YA, ZA);
				n_nm++;
			}
		}
		
		fclose  (fpm );
		fclose  (fpmn);
		fclose  (fp_nm);
	}
	
	
	
	time2 = CTime::GetCurrentTime();;
	int hr2  = time2.GetHour();
	int	min2 = time2.GetMinute();
	int sec2 = time2.GetSecond();
	
	fprintf (out, "Starting   Time \t\t\t%2d %2d %2d\n", hr1, min1, sec1);
	fprintf (out, "Ending     Time \t\t\t%2d %2d %2d\n", hr2, min2, sec2);
	
	int dhr, dmin, dsec;
	
	if (sec2 >= sec1)
		dsec = sec2 - sec1;
	else
	{
		dsec = sec2 + 60 - sec1;
		min2 = min2 - 1;
	}
	
	if (min2 >= min1)
		dmin = min2 - min1;
	else
	{
		dmin = min2 + 60 - min1;
		hr2 = hr2 - 1;
	}
	
	if (hr2 >= hr1)
		dhr = hr2 - hr1;
	else
	{
		dhr = hr2 + 24 - hr1;
	}
	
	fprintf (out,"Total Number of     Matched Points = %8d\n", n_m);
	fprintf (out,"Total Number of Non-Matched Points = %8d\n", n_nm);
	
	
	fprintf (out, "Processing Time \t\t\t%2d %2d %2d\n", dhr, dmin, dsec);
	
	for (i = 0; i < tile_size; i++)
		for (j = 0; j < tile_size; j++)
		{
			if (tile[i][j].n_i > 0)
				free(tile[i][j].i);
		}
		
		fclose (fpx);
		fclose (fpy);
		fclose (fpz);
		fclose (fps);
		fclose (fpo);
		fclose (fpp);
		fclose (fpk);
		
		fclose (out);
		
		free_dmatrix (R, 0, 2, 0, 2);
		
		free (sp2);
		free (p_sp1);
		
		sprintf (str, "DONE %20s!!\n", file_name);
		AfxMessageBox( str, MB_OK, 0);
		
		
		for(i=0;i<tile_size;i++)
		{
			delete tile[i];
		}
		if (tile) delete[] tile;
		
		return (1);
}


int     abs_ort_icpatch_KdTree(char *file_name, FILE *inp, FILE *inp1, FILE *inp2, FILE *out, FILE *out_transformed)
{
	double  pi;
	char	line[200];
	//	int		no_of_it;
	int i, j;
	
	pi = 2.0 * asin(1.0);
	
	fgets  (line, 200, inp);	//Description line (Threshold Values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf",&Angle_Threshold , &Distance_Threshold  );
	
	fgets  (line, 200, inp);	//Description line (Approximate Values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf %lf %lf %lf %lf %lf",&XT  , &YT  , &ZT  , &S  , &om  , &phi  , &kap  );
	
	om	*= pi / 180.0;
	phi *= pi / 180.0;
	kap *= pi / 180.0;
	
	char	pt_id[20], patch_id[20];
	double	x, y, z;
	double  X[3], Y[3], Z[3];
	
	no_of_p_sp1 = 0;
	no_of_sp2	= 0;
	
	//AH Warning Hard Coded Fixed Values.
	//To Avoid dealing with large numbers.
	
	delta_x = delta_y = 15.0;//30.0;	//To limit the search space
	
	FILE	*pnt_dat, *tin_dat;
	char	pnt_dat_name[200], tin_dat_name[200];
	
	strcpy	(pnt_dat_name, file_name);
	prj_dir	(pnt_dat_name);
	strcat	(pnt_dat_name, "pnt.dat");
	pnt_dat = fopen(pnt_dat_name, "w");
	
	strcpy	(tin_dat_name, file_name);
	prj_dir	(tin_dat_name);
	strcat	(tin_dat_name, "tin.dat");
	tin_dat = fopen(tin_dat_name, "w");
	
	fprintf (pnt_dat, "Easting\tNorthing\tUp\n");
	fprintf (tin_dat, "Easting\tNorthing\tUp\n");
	
	//Reading the data associated with the first surface points.
    
    DX = DY = DZ = 0;
	
	while (fgets(line, 200, inp1) != NULL)
	{
		no_of_p_sp1++;
		
		if (no_of_p_sp1 == 1)
			p_sp1 = (P_SPI *) malloc (no_of_p_sp1 * sizeof(P_SPI));
		else
			p_sp1 = (P_SPI *) realloc (p_sp1, no_of_p_sp1 * sizeof(P_SPI));
		
		sscanf (line, "%s %lf %lf %lf", pt_id, &x, &y, &z);
		
		i = no_of_p_sp1 - 1;
		
		strcpy (p_sp1[i].pt_id, pt_id);
		
		p_sp1[i].x = x;
		p_sp1[i].y = y;
		p_sp1[i].z = z;
		
		DX += p_sp1[i].x;
		DY += p_sp1[i].y;
		DZ += p_sp1[i].z;
		
		fprintf (pnt_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", p_sp1[i].x, p_sp1[i].y, p_sp1[i].z);
	};
	
	
	DX /= no_of_p_sp1;
	DY /= no_of_p_sp1;
	DZ /= no_of_p_sp1;
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		p_sp1[i].x -= DX;
		p_sp1[i].y -= DY;
		p_sp1[i].z -= DZ;	
	}
	
	fclose (pnt_dat);
	
	
	fprintf (out, "DX = %12.3lf\n", DX);
	fprintf (out, "DY = %12.3lf\n", DY);
	fprintf (out, "DZ = %12.3lf\n", DZ);
	
	
	int		WTXY, WTZ;
	while (fgets(line, 200, inp2) != NULL)
	{
		no_of_sp2++;
		
		if (no_of_sp2 == 1)
			sp2 = (SPII *) malloc  (no_of_sp2 * sizeof(SPII));
		else
			sp2 = (SPII *) realloc (sp2, no_of_sp2 * sizeof(SPII));
		
		sscanf (line, "%s %d %d", patch_id, &WTXY, &WTZ);
		
		fgets (line, 200, inp2);
		sscanf (line, "%lf %lf %lf ", &X[0], &Y[0], &Z[0]);
		
		fgets (line, 200, inp2);
		sscanf (line, "%lf %lf %lf ", &X[1], &Y[1], &Z[1]);
		
		fgets (line, 200, inp2);
		sscanf (line, "%lf %lf %lf ", &X[2], &Y[2], &Z[2]);
		
		int i = no_of_sp2 - 1;
		
		strcpy (sp2[i].patch_id, patch_id);
		
		sp2[i].WTXY = WTXY;
		sp2[i].WTZ  = WTZ;
		
		sp2[i].X[0] = X[0] - DX;
		sp2[i].Y[0] = Y[0] - DY;
		sp2[i].Z[0] = Z[0] - DZ;
		
		sp2[i].X[1] = X[1] - DX;
		sp2[i].Y[1] = Y[1] - DY;
		sp2[i].Z[1] = Z[1] - DZ;
		
		sp2[i].X[2] = X[2] - DX;
		sp2[i].Y[2] = Y[2] - DY;
		sp2[i].Z[2] = Z[2] - DZ;
		
		//		fprintf (tin_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", sp2[i].X[0], sp2[i].Y[0], sp2[i].Z[0]);
		//		fprintf (tin_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", sp2[i].X[1], sp2[i].Y[1], sp2[i].Z[1]);
		//		fprintf (tin_dat,"%14.4lf\t%14.4lf\t%14.4lf\n", sp2[i].X[2], sp2[i].Y[2], sp2[i].Z[2]);
	};
	
	fclose (tin_dat);
	
	/************************************* by Ruifang ****************************************/
	/////////////////////////////////////////////////////////////////////////////////////////
    //build the point indexes of the second surface using the quick sort method;
	CArray<int, int> *pTriIndexes;
	pTriIndexes = new CArray<int, int>[3*no_of_sp2];
    VERTEX *pAllVertex = new VERTEX[3*no_of_sp2];
	for(i=0;i<no_of_sp2;i++)
	{
		pAllVertex[3*i+0].x = sp2[i].X[0];
		pAllVertex[3*i+0].y = sp2[i].Y[0];
		pAllVertex[3*i+0].z = sp2[i].Z[0];
		pAllVertex[3*i+0].Patch_ID = i;
		
		pAllVertex[3*i+1].x = sp2[i].X[1];
		pAllVertex[3*i+1].y = sp2[i].Y[1];
		pAllVertex[3*i+1].z = sp2[i].Z[1];
		pAllVertex[3*i+1].Patch_ID = i;
		
		pAllVertex[3*i+2].x = sp2[i].X[2];
		pAllVertex[3*i+2].y = sp2[i].Y[2];
		pAllVertex[3*i+2].z = sp2[i].Z[2];
		pAllVertex[3*i+2].Patch_ID = i;
	}
	
	// quick sort to X, Y, Z respectively;
	qsort(pAllVertex, 3*no_of_sp2, sizeof(VERTEX),compare_x);  //sort the X coordinates;
	CArray<VERTEX,VERTEX> *pXcorPt = new CArray<VERTEX,VERTEX>[3*no_of_sp2];
	CArray<VERTEX,VERTEX> *pXYcorPt = new CArray<VERTEX,VERTEX>[3*no_of_sp2];
	int XcorIndex=0; 
	int XYcorIndex=0; 
	
	pXcorPt[XcorIndex].Add(pAllVertex[0]);
	for(i=1;i<3*no_of_sp2;i++)
	{
		if(pAllVertex[i].x == pAllVertex[i-1].x)
		{
			pXcorPt[XcorIndex].Add(pAllVertex[i]);
		}
		else
		{
            XcorIndex++;
			pXcorPt[XcorIndex].Add(pAllVertex[i]);
		}
	}
	int nIndex = 0;
	int nTemp=0;
	VERTEX pQueY[1000];
	for(i=0;i<XcorIndex+1;i++)
	{
		nTemp = pXcorPt[i].GetSize();
		for(j=0;j<nTemp;j++)
		{
			pQueY[j] = pXcorPt[i].GetAt(j);
		}
		qsort(pQueY, nTemp, sizeof(VERTEX),compare_y);   // sort the y coordinates;
		for(j=0;j<nTemp;j++)
		{
			pAllVertex[nIndex+j] = pQueY[j];
		}
		nIndex+=nTemp;
	}
	for(i=0;i<3*no_of_sp2;i++)
	{
		pXcorPt[i].RemoveAll();
	}
	if(pXcorPt) delete []pXcorPt;
    pXcorPt = NULL;
	
	pXYcorPt[XYcorIndex].Add(pAllVertex[0]);
	for(i=1;i<3*no_of_sp2;i++)
	{
		if(pAllVertex[i].x == pAllVertex[i-1].x && pAllVertex[i].y == pAllVertex[i].y)
		{
			pXYcorPt[XYcorIndex].Add(pAllVertex[i]);
		}
		else
		{
			XYcorIndex++;
			pXYcorPt[XYcorIndex].Add(pAllVertex[i]);
		}
	}
	nIndex = 0;
	nTemp = 0;
	for(i=0;i<XYcorIndex+1;i++)
	{
		nTemp = pXYcorPt[i].GetSize();
		for(j=0;j<nTemp;j++)
		{
			pQueY[j] = pXYcorPt[i].GetAt(j);
		}
		qsort(pQueY, nTemp, sizeof(VERTEX),compare_z);       //sort the Z coordinates;
		for(j=0;j<nTemp;j++)
		{
			pAllVertex[nIndex+j] = pQueY[j];
		}
		nIndex+=nTemp;
	}
    for(i=0;i<3*no_of_sp2;i++)
	{
		pXYcorPt[i].RemoveAll();
	}
	if(pXYcorPt) delete [] pXYcorPt;
	pXYcorPt = NULL;
	//////////////////end building the Vertex Index;/////////////////////////////////////////////////
	
	
	///////////////////////////////////////////////////////////////////////////////
	//Build the k-d tree of the second surface;
	int kd_k =100;
	int dim = 3;
	double eps = 0;
	ANNpointArray		dataPts;				// data points
	ANNpoint			queryPt;				// query point
	ANNkd_tree*         kdTree;                 // search structure
	queryPt = annAllocPt(dim);					// allocate query point
	dataPts = annAllocPts(3*no_of_sp2, dim);    // allocate data points
	queryPt = annAllocPt(dim);					// allocate query point
	ANNidxArray			nnIdx;    		    	// near neighbor indices
	ANNdistArray		dists;					// near neighbor distances
	nnIdx = new ANNidx[kd_k];               // allocate near neigh indices
	dists = new ANNdist[kd_k];              // allocate near neighbor dists
	int nTriID;
    int mm,m;
	int nTriangles;                      
	BOOL *bComputed = new BOOL[no_of_sp2];   //whether the Triangle is computed;
	
	int nVertex= 0;
    pTriIndexes[nVertex].Add(pAllVertex[0].Patch_ID);
	dataPts[nVertex][0] = pAllVertex[0].x ;
	dataPts[nVertex][1] = pAllVertex[0].y;
	dataPts[nVertex][2] = pAllVertex[0].z;
    for(i=1;i<3*no_of_sp2;i++)
	{
		if((pAllVertex[i].x == pAllVertex[i-1].x)&& (pAllVertex[i].y == pAllVertex[i-1].y)
			&&(pAllVertex[i].z == pAllVertex[i-1].z))
		{
			pTriIndexes[nVertex].Add(pAllVertex[i].Patch_ID);
		}
		else
		{
			nVertex++;
			pTriIndexes[nVertex].Add(pAllVertex[i].Patch_ID);
			dataPts[nVertex][0] = pAllVertex[i].x;
			dataPts[nVertex][1] = pAllVertex[i].y;
			dataPts[nVertex][2] = pAllVertex[i].z;
		}
	}
	if(pAllVertex) delete []pAllVertex;
	
	
    kdTree = new ANNkd_tree(dataPts, nVertex,dim);
	
	/************************************end************************************************/
	
	
	// Find the range for each patch
	for (i = 0; i < no_of_sp2; i++)
		find_min_max (i);
	
	fprintf (out, "Number of Surface I  Points  = %12d\n", no_of_p_sp1);
	fprintf (out, "Number of Surface II Patches = %12d\n", no_of_sp2);
	fprintf (out, "__________________________________________\n");
	
	char str[200];
	
	sprintf (str, "Number of Surface I  Points  = %12d\nNumber of Surface II Patches = %12d\n", no_of_p_sp1, no_of_sp2);
	AfxMessageBox( str, MB_OK, 0);
	
	//Files for writing the accumulator arrays and the progress of the estimated parameters with iterations
	
	FILE	*fpx  , *fpy  , *fpz  , *fps  , *fpo  , *fpp  , *fpk;
	
	char	fpx_name[200];
	char	fpy_name[200];
	char	fpz_name[200];
	
	char	fps_name[200];
	
	char	fpo_name[200];
	char	fpp_name[200];
	char	fpk_name[200];
	
	strcpy	(fpx_name, file_name);
	prj_dir	(fpx_name);
	strcpy  (dir_name, fpx_name);
	strcat	(fpx_name, "XT.out");
	
	strcpy	(fpy_name, file_name);
	prj_dir	(fpy_name);
	strcat	(fpy_name, "YT.out");
	
	strcpy	(fpz_name, file_name);
	prj_dir	(fpz_name);
	strcat	(fpz_name, "ZT.out");
	
	strcpy	(fps_name, file_name);
	prj_dir	(fps_name);
	strcat	(fps_name, "SF.out");
	
	strcpy	(fpo_name, file_name);
	prj_dir	(fpo_name);
	strcat	(fpo_name, "OM.out");
	
	strcpy	(fpp_name, file_name);
	prj_dir	(fpp_name);
	strcat	(fpp_name, "PH.out");
	
	strcpy	(fpk_name, file_name);
	prj_dir	(fpk_name);
	strcat	(fpk_name, "KP.out");
	
	
	fpx = fopen (fpx_name, "w");
	fpy = fopen (fpy_name, "w");
	fpz = fopen (fpz_name, "w");
	
	fps = fopen (fps_name, "w");
	
	fpo = fopen (fpo_name, "w");
	fpp = fopen (fpp_name, "w");
	fpk = fopen (fpk_name, "w");
	
	//The matching process
	int flag = 0;
	
	CTime t;
	
	double check;
	CTime time1, time2;
	
	// Beginning Time
	
	time1		= CTime::GetCurrentTime();;
	int hr1		= time1.GetHour();
	int	min1	= time1.GetMinute();
	int sec1	= time1.GetSecond();
	
	ITX = ITY = ITZ = ITS = ITO = ITP = ITK = 0;
	
	double  seg_bef = 100000.0;
	int		I_NO_IT = 0;
	
	/********kd_k can be modified so as to enlarge the search area;********************/
	//	kd_k = 50;    // search the nearest kd_k points in the seconde surface, by Ruifang;
	
	for (int NNN = 0; NNN < 40; NNN++)
	{
		ITX++;
		ITY++;
		ITZ++;
		ITS++;
		ITO++;
		ITP++;
		ITK++;
		
		Match_Surfaces_c (out, kdTree, pTriIndexes, kd_k);
		// Least Squares Adjustment
		
		fprintf (out, "\n\nLeast Squares Adjustment\n");
		fprintf (out, "-------------------------\n");
		
		fprintf (out, "\n\n");
		// t == 5	Solve for XT, YT, ZT, PH, KP
		// t == 6	Solve for XT, YT, ZT, OM, PH, KP
		// t == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
		
		check = Compute_Parameters(out, 7);

		fprintf (fpx,"%6d %14.4lf\n", ITX, XT);
		fprintf (fpy,"%6d %14.4lf\n", ITY, YT);
		fprintf (fpz,"%6d %14.4lf\n", ITZ, ZT);
		
		fprintf (fps,"%6d %14.4lf\n", ITS, S);
		
		fprintf (fpo,"%6d %14.4lf\n", ITO, om  * 180.0 / pi);
		fprintf (fpp,"%6d %14.4lf\n", ITP, phi * 180.0 / pi);
		fprintf (fpk,"%6d %14.4lf\n", ITK, kap * 180.0 / pi);
		
		double dseg = check - seg_bef;
		seg_bef = check;
		if (fabs (dseg) <= 0.000001) break;
		else
			if (fabs (dseg) < 0.001 && fabs(dseg) > 0.000001)
				I_NO_IT++;
			else
				I_NO_IT = 0;
	}
	
	
	int n_m, n_nm;
	n_m = n_nm = 0;
	
	if (check != 0.0)
	{
		char fpm_name[200];
		FILE *fpm;
		
		strcpy	(fpm_name, file_name);
		prj_dir	(fpm_name);
		strcat	(fpm_name, "Matched.dat");
		
		fpm = fopen (fpm_name, "w");
		fprintf (fpm,"Easting\t\tNorthing\t\tUp\n");
		
		char fpmn_name[200];
		FILE *fpmn;
		
		strcpy	(fpmn_name, file_name);
		prj_dir	(fpmn_name);
		strcat	(fpmn_name, "NMatched.dat");
		
		fpmn = fopen (fpmn_name, "w");
		fprintf (fpmn,"Easting\t\tNorthing\t\tUp\n");
		
		char fp_nm_name[200];
		FILE *fp_nm;
		
		strcpy	(fp_nm_name, file_name);
		prj_dir	(fp_nm_name);
		strcat	(fp_nm_name, "Non_NMatched.dat");
		
		fp_nm = fopen (fp_nm_name, "w");
		fprintf (fp_nm,"Easting\t\tNorthing\t\tUp\n");
		
		fprintf (out, "Matched Points\n");
		fprintf (out, "***************\n\n");
		
		rotation ();
		
		for (i = 0; i < no_of_p_sp1; i++)
		{
			xa = p_sp1[i].x;
			ya = p_sp1[i].y;
			za = p_sp1[i].z;
			
			compute_XYZA();
			
			/************************* by Ruifang ****************************/
			queryPt[0] = XA;
			queryPt[1] = YA;
			queryPt[2] = ZA;
			kdTree->annkSearch(queryPt, kd_k, nnIdx, dists, eps); // search the nearest k points in the second surface, by Ruifang;
			for( mm=0; mm<no_of_sp2; mm++)
			{
				bComputed[mm] = 0;
			}
			
			
			int flag_matched = 0;
			
			double checkA; 
			double checkB; 
			
			double  min_ang_diff;
			
			min_ang_diff = 10000.0;
			
			for( m=0; m<kd_k; m++)
			{
				nTriangles = pTriIndexes[nnIdx[m]].GetSize();
				for(j=0;j<nTriangles;j++)
				{
					nTriID = pTriIndexes[nnIdx[m]].GetAt(j); //get the triangle ID of the query point;
					if(bComputed[nTriID] == 1) continue; // avoid computing the triangles repeatedly;
					
					if(queryPt[0] < (sp2[nTriID].min_x - delta_x)) continue;
					if(queryPt[0] > (sp2[nTriID].max_x + delta_x)) continue;
					if(queryPt[1] < (sp2[nTriID].min_y - delta_y)) continue;
					if(queryPt[1] > (sp2[nTriID].max_y + delta_y)) continue;
					
					bComputed[nTriID] = 1; 
					
					V1[0] = sp2[nTriID].X[0];
					V1[1] = sp2[nTriID].Y[0];
					V1[2] = sp2[nTriID].Z[0];
					
					V2[0] = sp2[nTriID].X[1];
					V2[1] = sp2[nTriID].Y[1];
					V2[2] = sp2[nTriID].Z[1];
					
					V3[0] = sp2[nTriID].X[2];
					V3[1] = sp2[nTriID].Y[2];
					V3[2] = sp2[nTriID].Z[2];
					
					checkA = 0.0;
					checkB = 0.0;
					
					Compute_In_Out_NTH (&checkA, &checkB);
					
					if ((checkA < Distance_Threshold) && (fabs(checkB - 360.0) < Angle_Threshold))
					{
						fprintf (out , "point %5s and surface patch %5s are coplananr - NDistance = %12.4lf Angle = %12.4lf\n", p_sp1[i].pt_id, sp2[j].patch_id, checkA, checkB);
						fprintf (fpm , "%10s %12.4lf %12.4lf %12.4lf\n", p_sp1[i].pt_id, xa, ya, za);
						fprintf (fpmn, "%10s %12.4lf %12.4lf %12.4lf\n", p_sp1[i].pt_id, XA, YA, ZA);
						flag_matched = 1;
						n_m++;
					}
				}
			}
			
			
			if (flag_matched == 0)
			{
				fprintf (fp_nm, "%10s %12.4lf %12.4lf %12.4lf\n", p_sp1[i].pt_id, XA, YA, ZA);
				n_nm++;
			}
		}
		
		fclose  (fpm );
		fclose  (fpmn);
		fclose	(fp_nm);
	}
	
	/*************************** by Ruifang ***********************************/
	if(kdTree) delete kdTree;
	annClose();
	for(i=0;i<3*no_of_sp2;i++)
	{
		pTriIndexes[i].RemoveAll();
	}
	if(pTriIndexes) delete []pTriIndexes;
	pTriIndexes = NULL;
	if(bComputed) delete []bComputed;
	bComputed = NULL;
	/**************************** end *****************************************/
	
	/***********************Transform the second data set into the first data;**********/
    P_SPI* pTransFormPts = new P_SPI[no_of_p_sp1];
    double Rot[9];
	rotation_Matrix(om, phi, kap, Rot);
	
	double Centerx, Centery, Centerz;
	Centerx = DX;
	Centery = DY;
	Centerz = DZ;
    for(i=0;i<no_of_p_sp1;i++)
	{
		pTransFormPts[i].x = S*(Rot[0]*p_sp1[i].x + Rot[1]*p_sp1[i].y + Rot[2]*p_sp1[i].z) + XT + Centerx;
		pTransFormPts[i].y = S*(Rot[3]*p_sp1[i].x + Rot[4]*p_sp1[i].y + Rot[5]*p_sp1[i].z) + YT + Centery;
		pTransFormPts[i].z = S*(Rot[6]*p_sp1[i].x + Rot[7]*p_sp1[i].y + Rot[8]*p_sp1[i].z) + ZT + Centerz;
	}
	
	//write to the out_transformed file;
	for(i=0; i<no_of_p_sp1; i++)
	{
		fprintf(out_transformed, "%15.5lf %15.5lf %15.5lf\n", pTransFormPts[i].x, pTransFormPts[i].y, pTransFormPts[i].z);
	}
	fclose(out_transformed);
	if(pTransFormPts) delete pTransFormPts;
	
	/**********************End Transform the second data set into the first data;*******/
	
	
	time2 = CTime::GetCurrentTime();;
	int hr2  = time2.GetHour();
	int	min2 = time2.GetMinute();
	int sec2 = time2.GetSecond();
	
	fprintf (out, "Starting   Time \t\t\t%2d %2d %2d\n", hr1, min1, sec1);
	fprintf (out, "Ending     Time \t\t\t%2d %2d %2d\n", hr2, min2, sec2);
	
	int dhr, dmin, dsec;
	
	if (sec2 >= sec1)
		dsec = sec2 - sec1;
	else
	{
		dsec = sec2 + 60 - sec1;
		min2 = min2 - 1;
	}
	
	if (min2 >= min1)
		dmin = min2 - min1;
	else
	{
		dmin = min2 + 60 - min1;
		hr2 = hr2 - 1;
	}
	
	if (hr2 >= hr1)
		dhr = hr2 - hr1;
	else
	{
		dhr = hr2 + 24 - hr1;
	}
	
	fprintf (out, "Processing Time \t\t\t%2d %2d %2d\n", dhr, dmin, dsec);
	
	fclose (inp );
	
	fclose (inp1);
	fclose (inp2);
	
	fclose (out );
	
	free ( XT_array);
	free ( YT_array);
	free ( ZT_array);
	free (  S_array);
	free ( om_array);
	free (phi_array);
	free (kap_array);
	
	//free_dmatrix (R, 0, 2, 0, 2);
	
	free (sp2);
	free (p_sp1);
	
	sprintf (str, "DONE %20s!!\n", file_name);
	AfxMessageBox( str, MB_OK, 0);
	
	return (1);
}

/**************************************by Ruifang********************************************/
int compare_x(const void*a, const void*b)   //quick sort of the X coordinate, by Ruifang
{
	VERTEX *arg1 = (VERTEX*) a;
	VERTEX *arg2 = (VERTEX*) b;
	if( arg1->x < arg2->x) return -1;
	else if(arg1->x == arg2->x) return 0;
	else return 1;
}

int compare_y(const void*a, const void*b) //quick sort of the Y coordinate, by Ruifang
{
	VERTEX *arg1 = (VERTEX*) a;
	VERTEX *arg2 = (VERTEX*) b;
	if( arg1->y < arg2->y) return -1;
	else if(arg1->y == arg2->y) return 0;
	else return 1;
}


int compare_z(const void*a, const void*b)  //quick sort of the Z coordinate, by Ruifang
{
	VERTEX *arg1 = (VERTEX*) a;
	VERTEX *arg2 = (VERTEX*) b;
	if( arg1->z < arg2->z) return -1;
	else if(arg1->z == arg2->z) return 0;
	else return 1;
}

int compare_VERTEX(const void*a, const void*c)  
{
	return 1;
}


double Match_Surfaces_c (FILE *out, ANNkd_tree* kdTree, CArray<int,int> *pTriIndexes, int k)
{
	//NOTE: we are not allowing multiple matches
	int		i, j;
	double	CHECK, CHECK1;
	
	rotation ();
	match_res_s.count = 0;
	
	/******************* by Ruifang ******************/
	ANNpoint			queryPt;				// query point
	ANNidxArray			nnIdx;    		    	// near neighbor indices
	ANNdistArray		dists;					// near neighbor distances
	int dim = 3;
	double eps = 0;
	queryPt = annAllocPt(dim);					// allocate query point
	nnIdx = new ANNidx[k];               // allocate near neigh indices
	dists = new ANNdist[k];              // allocate near neighbor dists
	int nTriID;
    int mm,m;
	int nTriangles;                      
	BOOL *bComputed = new BOOL[no_of_sp2];   //whether the Triangle is computed;
    
	for (i = 0; i < no_of_p_sp1; i++)
	{
		
		xa = p_sp1[i].x;
		ya = p_sp1[i].y;
		za = p_sp1[i].z;
		
		double	min_dist_dif = 50.0;
		int		patch_id;
		
		compute_XYZA();
		
		/************************* by Ruifang ****************************/
		queryPt[0] = XA;
		queryPt[1] = YA;
		queryPt[2] = ZA;
        kdTree->annkSearch(queryPt, k, nnIdx, dists, eps); // search the nearest k points in the second surface, by Ruifang;
		for( mm=0; mm<no_of_sp2; mm++)
		{
			bComputed[mm] = 0;
		}
		
		for( m=0; m<k; m++)
		{
			nTriangles = pTriIndexes[nnIdx[m]].GetSize();
			for(j=0;j<nTriangles;j++)
			{
				nTriID = pTriIndexes[nnIdx[m]].GetAt(j); //get the triangle ID of the query point;
				if(bComputed[nTriID] == 1) continue; // avoid computing the triangles repeatedly;
				
				if(queryPt[0] < (sp2[nTriID].min_x - delta_x)) continue;
				if(queryPt[0] > (sp2[nTriID].max_x + delta_x)) continue;
				if(queryPt[1] < (sp2[nTriID].min_y - delta_y)) continue;
				if(queryPt[1] > (sp2[nTriID].max_y + delta_y)) continue;
				
				bComputed[nTriID] = 1; 
				
				V1[0] = sp2[nTriID].X[0];
				V1[1] = sp2[nTriID].Y[0];
				V1[2] = sp2[nTriID].Z[0];
				
				V2[0] = sp2[nTriID].X[1];
				V2[1] = sp2[nTriID].Y[1];
				V2[2] = sp2[nTriID].Z[1];
				
				V3[0] = sp2[nTriID].X[2];
				V3[1] = sp2[nTriID].Y[2];
				V3[2] = sp2[nTriID].Z[2];
				
				/*		for (j = 0; j < no_of_sp2; j++)
				{
				
				  compute_XYZA();
				  
					if (XA < (sp2[j].min_x - delta_x))
					continue;
					
					  if (XA > (sp2[j].max_x + delta_x))
					  continue;
					  
						if (YA < (sp2[j].min_y - delta_y))
						continue;
						
						  if (YA > (sp2[j].max_y + delta_y))
						  continue;
						  
							V1[0] = sp2[j].X[0];
							V1[1] = sp2[j].Y[0];
							V1[2] = sp2[j].Z[0];
							
							  V2[0] = sp2[j].X[1];
							  V2[1] = sp2[j].Y[1];
							  V2[2] = sp2[j].Z[1];
							  
								V3[0] = sp2[j].X[2];
								V3[1] = sp2[j].Y[2];
				V3[2] = sp2[j].Z[2];*/
				
				
				compute_XYZA();
				
				double checkA = 0.0;
				double checkB = 0.0;
				
				Compute_In_Out_NTH (&checkA, &checkB);
				
				//		    	if (!strcmp(p_sp1[i].pt_id, "1_8") && !strcmp(sp2[j].patch_id, "1"))
				if (!strcmp(p_sp1[i].pt_id, "1_8") && !strcmp(sp2[nTriID].patch_id, "1"))
					double test = 1.0;
				
				if ((fabs(checkA) < min_dist_dif) && (fabs(checkB - 360) < Angle_Threshold))
				{
					min_dist_dif = fabs(checkA);
					//		            patch_id = j;
					patch_id = nTriID;
					CHECK1 = checkB;
					CHECK  = checkA;
				}
			}	
		}
		
		
		//
		if ((min_dist_dif < Distance_Threshold) && (fabs(CHECK1 - 360) < Angle_Threshold))
		{
			fprintf (out, "point %5s is inside surface patch %5s - Normal Distance = %12.4lf\n", p_sp1[i].pt_id, sp2[patch_id].patch_id, CHECK);
			
			match_res_s.count++;
			if (match_res_s.count == 1)
			{
				match_res_s.surf1_index = (int *) malloc (match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) malloc (match_res_s.count * sizeof(int));
			}
			else
			{
				match_res_s.surf1_index = (int *) realloc (match_res_s.surf1_index, match_res_s.count * sizeof(int));
				match_res_s.surf2_index = (int *) realloc (match_res_s.surf2_index, match_res_s.count * sizeof(int));
			}
			
			int I =  match_res_s.count - 1;
			match_res_s.surf1_index[I] = i;
			match_res_s.surf2_index[I] = patch_id;
		}
	}
	if(bComputed) delete []bComputed;
	bComputed = NULL;
    /******************************************************/
	return (1.0);
}

int     abs_ort_icpoint_KdTree(char *file_name, FILE *inp, FILE *inp1, FILE *inp2,  FILE *out, FILE *out_transformed, double &var, double &NDIST)
{
	//Performs Absolute Orientation using Iterative Closest Point
	double  pi;
	char	line[200];
	int		no_of_it;
	int		I, i;
	
	pi = 2.0 * asin(1.0);
	
	double Sig_Threshold;
	
	fgets  (line, 200, inp);	//Description line (Approximate Values)
	fgets  (line, 200, inp);	//Data Line (distance threshold is the minimum distance to be considered for ICP)
	sscanf (line, "%lf %lf",&Sig_Threshold , &Distance_Threshold);
	
	fgets  (line, 200, inp);	//Description line (Approximate Values)
	fgets  (line, 200, inp);	//Data line
	sscanf (line, "%lf %lf %lf %lf %lf %lf %lf",&XT  , &YT  , &ZT  , &S  , &om  , &phi  , &kap  );
	fclose (inp);
	
	om	*= pi / 180.0;
	phi *= pi / 180.0;
	kap *= pi / 180.0;
	
	char	pt_id[20];
	double	x, y, z;
	
	no_of_p_sp1 = no_of_p_sp2 = 0;
	
	//AH Warning Hard Coded Fixed Values.
	//To Avoid dealing with large numbers.
	
	DX = 0.0;	
	DY = 0.0;
	DZ = 0.0;
	
	delta_x = delta_y = delta_z = 15.0;	//To limit the search space
	
	//Reading the data associated with the first surface points.
	
	int i_skip = 0;
	
	while (fgets(line, 200, inp1) != NULL)
	{
		i_skip++;
		
		if ((i_skip % 1) == 0)
		{
			no_of_p_sp1++;
			
			if (no_of_p_sp1 == 1)
				p_sp1 = (P_SPI *) malloc (no_of_p_sp1 * sizeof(P_SPI));
			else
				p_sp1 = (P_SPI *) realloc (p_sp1, no_of_p_sp1 * sizeof(P_SPI));
			
			sscanf (line, "%lf %lf %lf", &x, &y, &z);
			
			sprintf (pt_id, "%d", no_of_p_sp1);
			
			i = no_of_p_sp1 - 1;
			
			strcpy (p_sp1[i].pt_id, pt_id);
			
			p_sp1[i].x = x;
			p_sp1[i].y = y;
			p_sp1[i].z = z;
			
			DX += x;
			DY += y;
			DZ += z;
		}
	};
	
	
	DX /= no_of_p_sp1;
	DY /= no_of_p_sp1;
	DZ /= no_of_p_sp1;
	
	for (i = 0; i < no_of_p_sp1; i++)
	{
		p_sp1[i].x -= DX;
		p_sp1[i].y -= DY;
		p_sp1[i].z -= DZ;
		
	}
	
	fprintf (out, "DX = %12.3lf\n", DX);
	fprintf (out, "DY = %12.3lf\n", DY);
	fprintf (out, "DZ = %12.3lf\n", DZ);
	
	//Reading the data associated with the second surface points.
	
	i_skip = 0;
	while (fgets(line, 200, inp2) != NULL)
	{
		i_skip++;
		
		if ((i_skip % 1) == 0)
		{
			no_of_p_sp2++;
			
			if (no_of_p_sp2 == 1)
				p_sp2 = (P_SPI *) malloc (no_of_p_sp2 * sizeof(P_SPI));
			else
				p_sp2 = (P_SPI *) realloc (p_sp2, no_of_p_sp2 * sizeof(P_SPI));
			
			sscanf (line, "%lf %lf %lf", &x, &y, &z);
			
			sprintf (pt_id, "%d", no_of_p_sp2);
			
			i = no_of_p_sp2 - 1;
			
			strcpy (p_sp2[i].pt_id, pt_id);
			
			p_sp2[i].x = x - DX;
			p_sp2[i].y = y - DY;
			p_sp2[i].z = z - DZ;
		}
	};
	
	fprintf (out, "Number of Surface I  Points  = %12d\n", no_of_p_sp1);
	fprintf (out, "Number of Surface II Points  = %12d\n", no_of_p_sp2);
	fprintf (out, "__________________________________________\n");
	
	char str[200];
	
	sprintf (str, "Number of Surface I  Points  = %12d\nNumber of Surface II Points = %12d\n", no_of_p_sp1, no_of_p_sp2);
	//AfxMessageBox( str, MB_OK, 0);
	
	//Files for writing the accumulator arrays and the progress of the estimated parameters with iterations
	
	FILE	*fpx  , *fpy  , *fpz  , *fps  , *fpo  , *fpp  , *fpk;
	
	//Parameter convergence with iterations
	char	fpx_name[200];
	char	fpy_name[200];
	char	fpz_name[200];
	
	char	fps_name[200];
	
	char	fpo_name[200];
	char	fpp_name[200];
	char	fpk_name[200];
	
	strcpy	(fpx_name, file_name);
	prj_dir	(fpx_name);
	strcpy  (dir_name, fpx_name);
	strcat	(fpx_name, "XT_point.out");
	
	strcpy	(fpy_name, file_name);
	prj_dir	(fpy_name);
	strcat	(fpy_name, "YT_point.out");
	
	strcpy	(fpz_name, file_name);
	prj_dir	(fpz_name);
	strcat	(fpz_name, "ZT_point.out");
	
	strcpy	(fps_name, file_name);
	prj_dir	(fps_name);
	strcat	(fps_name, "SF_point.out");
	
	strcpy	(fpo_name, file_name);
	prj_dir	(fpo_name);
	strcat	(fpo_name, "OM_point.out");
	
	strcpy	(fpp_name, file_name);
	prj_dir	(fpp_name);
	strcat	(fpp_name, "PH_point.out");
	
	strcpy	(fpk_name, file_name);
	prj_dir	(fpk_name);
	strcat	(fpk_name, "KP_point.out");
	
	fpx = fopen (fpx_name, "w");
	fpy = fopen (fpy_name, "w");
	fpz = fopen (fpz_name, "w");
	
	fps = fopen (fps_name, "w");
	
	fpo = fopen (fpo_name, "w");
	fpp = fopen (fpp_name, "w");
	fpk = fopen (fpk_name, "w");
	
	
	/************************************* by Ruifang ****************************************/
	/////////////////////////////////////////////////////////////////////////////////////////
	//Build the k-d tree of the second surface;
	int kd_k =1;
	int dim = 3;
	double eps = 0;
	ANNpointArray		dataPts;				// data points
	ANNpoint			queryPt;				// query point
	ANNkd_tree*         kdTree;                 // search structure
	queryPt = annAllocPt(dim);					// allocate query point
	dataPts = annAllocPts(no_of_p_sp2, dim);    // allocate data points
	ANNdistArray		dists;					// near neighbor distances
	ANNidxArray			nnIdx;    		    	// near neighbor indices
    dists = new ANNdist[kd_k];  
	nnIdx = new ANNidx[kd_k]; 
    for(i=0;i<no_of_p_sp2;i++)
	{
		dataPts[i][0] = p_sp2[i].x;
		dataPts[i][1] = p_sp2[i].y;
		dataPts[i][2] = p_sp2[i].z;
	}
	
    kdTree = new ANNkd_tree(dataPts, no_of_p_sp2 ,dim);
	/************************************end************************************************/
	
	
	//The matching process
	int flag = 0;
	
	CTime t;
	
	CTime time1, time2;
	
	// Beginning Time
	
	time1		= CTime::GetCurrentTime();;
	int hr1		= time1.GetHour();
	int	min1	= time1.GetMinute();
	int sec1	= time1.GetSecond();
	
	ITX = ITY = ITZ = ITS = ITO = ITP = ITK = 0;
	
	double  seg_bef = 100000.0;
	
	fprintf (fpx,"%6d %14.4lf\n", ITX, XT);
	fprintf (fpy,"%6d %14.4lf\n", ITY, YT);
	fprintf (fpz,"%6d %14.4lf\n", ITZ, ZT);
	
	fprintf (fps,"%6d %14.4lf\n", ITS, S);
	
	fprintf (fpo,"%6d %14.4lf\n", ITO, om  * 180.0 / pi);
	fprintf (fpp,"%6d %14.4lf\n", ITP, phi * 180.0 / pi);
	fprintf (fpk,"%6d %14.4lf\n", ITK, kap * 180.0 / pi);
	
	
	no_of_it = 200;
	
	for (I = 0; I <no_of_it; I++)
	{
		rotation();
		match_res_s.count = 0;
		for(int ii = 0; ii< no_of_p_sp1; ii++)
		{			
			xa = p_sp1[ii].x;
			ya = p_sp1[ii].y;
			za = p_sp1[ii].z;
			
			compute_XYZA();		//output (XA, YA, ZA)
			
			queryPt[0] = XA;
			queryPt[1] = YA;
			queryPt[2] = ZA;
			
			//search the closest point in the second surface;
			kdTree->annkSearch(queryPt, kd_k, nnIdx, dists, eps);
			
			if (sqrt(dists[0]) < Distance_Threshold)
			{
				
				match_res_s.count++;
				if (match_res_s.count == 1)
				{
					match_res_s.surf1_index = (int *) malloc (match_res_s.count * sizeof(int));
					match_res_s.surf2_index = (int *) malloc (match_res_s.count * sizeof(int));
				}
				else
				{
					match_res_s.surf1_index = (int *) realloc (match_res_s.surf1_index, match_res_s.count * sizeof(int));
					match_res_s.surf2_index = (int *) realloc (match_res_s.surf2_index, match_res_s.count * sizeof(int));
				}
				
				int III =  match_res_s.count - 1;
				match_res_s.surf1_index[III] = ii;
				match_res_s.surf2_index[III] = nnIdx[0];
			}
			
		}
		
		
		if (match_res_s.count > 8)	
		{
			fprintf (out, "Iteration Number %4d\n", I);
			fprintf (out, "---------------------\n\n");
			
			fprintf (out, "Number of Matched Points = %10d\n", match_res_s.count);
			
			double check = Compute_Parameters_points (out, 7, NDIST);

			var = sqrt(check);
			
			ITX++;
			ITY++;
			ITZ++;
			ITS++;
			ITO++;
			ITP++;
			ITK++;
			
			fprintf (fpx,"%6d %14.4lf\n", ITX, XT);
			fprintf (fpy,"%6d %14.4lf\n", ITY, YT);
			fprintf (fpz,"%6d %14.4lf\n", ITZ, ZT);
			
			fprintf (fps,"%6d %14.4lf\n", ITS, S);
			
			fprintf (fpo,"%6d %14.4lf\n", ITO, om  * 180.0 / pi);
			fprintf (fpp,"%6d %14.4lf\n", ITP, phi * 180.0 / pi);
			fprintf (fpk,"%6d %14.4lf\n", ITK, kap * 180.0 / pi);
			
			double dseg = check - seg_bef;
			seg_bef = check;
			if (fabs (dseg) < Sig_Threshold)
				break;
		}
	}
	
	time2 = CTime::GetCurrentTime();;
	int hr2  = time2.GetHour();
	int	min2 = time2.GetMinute();
	int sec2 = time2.GetSecond();
	
	fprintf (out, "Starting   Time \t\t\t%2d %2d %2d\n", hr1, min1, sec1);
	fprintf (out, "Ending     Time \t\t\t%2d %2d %2d\n", hr2, min2, sec2);
	
	int dhr, dmin, dsec;
	
	if (sec2 >= sec1)
		dsec = sec2 - sec1;
	else
	{
		dsec = sec2 + 60 - sec1;
		min2 = min2 - 1;
	}
	
	if (min2 >= min1)
		dmin = min2 - min1;
	else
	{
		dmin = min2 + 60 - min1;
		hr2 = hr2 - 1;
	}
	
	if (hr2 >= hr1)
		dhr = hr2 - hr1;
	else
	{
		dhr = hr2 + 24 - hr1;
	}
	
	
	fprintf (out, "Processing Time \t\t\t%2d %2d %2d\n", dhr, dmin, dsec);
	
	fclose (fpx);
	fclose (fpy);
	fclose (fpz);
	fclose (fps);
	fclose (fpo);
	fclose (fpp);
	fclose (fpk);
	
	fclose (out);
	
	//free_dmatrix (R, 0, 2, 0, 2);
	
	
	
	/*************************** by Ruifang ***********************************/
	if(kdTree) delete kdTree;
	annClose();
	/**************************** end *****************************************/
	
	/***********************Transform the Target data set into the fixed(reference) data;**********/
    P_SPI* pTransFormPts = new P_SPI[no_of_p_sp1];
    double Rot[9];
	rotation_Matrix(om, phi, kap, Rot);

	double Centerx, Centery, Centerz;
	Centerx = DX;
	Centery = DY;
	Centerz = DZ;
    for(i=0;i<no_of_p_sp1;i++)
	{
    	pTransFormPts[i].x = S*(Rot[0]*p_sp1[i].x + Rot[1]*p_sp1[i].y + Rot[2]*p_sp1[i].z) + XT + Centerx;
		pTransFormPts[i].y = S*(Rot[3]*p_sp1[i].x + Rot[4]*p_sp1[i].y + Rot[5]*p_sp1[i].z) + YT + Centery;
		pTransFormPts[i].z = S*(Rot[6]*p_sp1[i].x + Rot[7]*p_sp1[i].y + Rot[8]*p_sp1[i].z) + ZT + Centerz;
	}

	//write to the out_transformed file;
	for(i=0; i<no_of_p_sp1; i++)
	{
		fprintf(out_transformed, "%15.5lf %15.5lf %15.5lf\n", pTransFormPts[i].x, pTransFormPts[i].y, pTransFormPts[i].z);
	}
	fclose(out_transformed);

	if(pTransFormPts) delete pTransFormPts;
	
	/**********************End Transform the second data set into the first data;*******/
	
	free (p_sp2);
	free (p_sp1);
	
	
	sprintf (str, "DONE %20s!!\n", file_name);
	//AfxMessageBox( str, MB_OK, 0);
	return (1);
}