//==========================================================
//  Windows System Library
//==========================================================
//  Diagnose.h
//
//  Base classes for diagnostics and exceptions
//
//  Copyright 1992-96 Scott Robert Ladd. All rights reserved
//==========================================================

#if !defined(AFX_DIAGNOSE_H__0453F8E1_272D_11D3_9A0B_00A00C12369C__INCLUDED_)
#define AFX_DIAGNOSE_H__0453F8E1_272D_11D3_9A0B_00A00C12369C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

enum DiagLevel
{
	DIAG_MSG,
		DIAG_WARNING,
		DIAG_ERROR,
		DIAG_FATAL
};

class DiagOutput
{
public:
	virtual void DisplayMsg(const char* msg, DiagLevel level = DIAG_MSG) = 0;
};

class DiagnosticBase
{
public:
	virtual void Dump(DiagOutput& out) = 0;
	virtual void ShowInternals(DiagOutput& out) = 0;
	virtual bool CheckIntegrity() = 0;
};

class ExceptionBase
{
public:
	virtual ~ExceptionBase(){ };
	virtual void Explain(DiagOutput& out) = 0;
};


#endif // !defined(AFX_DIAGNOSE_H__0453F8E1_272D_11D3_9A0B_00A00C12369C__INCLUDED_)
