// LIDARBiasCalibration.cpp: implementation of the CLIDARBiasCalibration class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LIDARBiasCalibration.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define NUM_MINIMUM_POINT 3
#define NUM_PLANAR_PARAM 3

using namespace LIDARCALIBRATION_BBARAB;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLIDARBiasCalibration::CLIDARBiasCalibration()
{
	
}

CLIDARBiasCalibration::~CLIDARBiasCalibration()
{
	
}

bool CLIDARBiasCalibration::RunCalibration(CString cfgfilepath, CString vtxfilpath, CString planarpointspath, const char resultfile[], double lidarprecison, double Small_threshold_sigam, double Big_threshold_sigam, int max_iter)
{
	PatchList.RemoveAll();
	
	//	Config.ReadConfigFile(cfgfilepath); //*<Read configuration file*/
	//	Config.CheckZero(1.0e-20);
	//	ReadPlaneVertex_New(vtxfilpath, PatchList); //Read vertex file
	//	ReadPlanarPoints(planarpointspath); //*<Read point cloud file (TEXT)*/

	//*<copy original data*/
	//	OriginConfig = Config;

	bool retval;

	//Solve(lidarprecison, resultfile);

	//CString temp0 = resultfile; temp0 += ".old.result";
	//CString temp1 = resultfile; temp1 += ".new.result";
	CString temp2 = resultfile; temp2 += ".old.result";

	//	temp0.MakeLower();
	//	temp0.Replace(".result","_with_Volume.result");
	//	retval = SolveWithVolume_old(lidarprecison, temp0);
	//	

	double min[3], max[3];


	Config.ReadConfigFile(cfgfilepath); //*<Read configuration file*/
	Config.CheckZero(1.0e-20);
	ReadPlaneVertex(vtxfilpath, PatchList, min, max); //Read vertex file
	ReadPlanarPoints(planarpointspath); //*<Read point cloud file (TEXT)*/

	//*<copy original data*/
	OriginConfig = Config;
	
	//retval = SolveWithVolume_old(lidarprecison, temp2);
	//retval = SolveWithVolume(lidarprecison, resultfile, max_iter, threshold_sigam);
	retval = SolveWithVolume_RNM(lidarprecison, resultfile, max_iter, Small_threshold_sigam, Big_threshold_sigam);

	//CString temp1 = resultfile;
	//temp1.MakeLower();
	//temp1.Replace(".result","_with_ND.result");
	//SolveWithND(lidarprecison, temp1);

	///////////////////////////////////////////////////////////////////////////////////////////
	
	/*
	ReadConfigFile(cfgfilepath, Config); //Read configuration file
	ReadPlaneVertex_New(vtxfilpath, PatchList); //Read vertex file
	ReadPlanarPoints(planarpointspath); //Read point cloud file (TEXT)

	CString temp2 = resultfile;
	temp2.MakeLower();
	temp2.Replace(".result","_New.result");
	Solve_New(lidarprecison, temp2);
	*/

	return retval;
	
	//return Solve2();
}

bool CLIDARBiasCalibration::RunCalibration_Brazil(CString cfgfilepath, CString vtxfilpath, CString planarpointspath, const char resultfile[], double lidarprecison, double Small_threshold_sigam, double Big_threshold_sigam, int max_iter)
{
	PatchList.RemoveAll();
	
	//	Config.ReadConfigFile(cfgfilepath); //*<Read configuration file*/
	//	Config.CheckZero(1.0e-20);
	//	ReadPlaneVertex_New(vtxfilpath, PatchList); //Read vertex file
	//	ReadPlanarPoints(planarpointspath); //*<Read point cloud file (TEXT)*/

	//*<copy original data*/
	//	OriginConfig = Config;

	bool retval;

	//Solve(lidarprecison, resultfile);

	//	CString temp0 = resultfile; temp0 += ".old.result";
	//CString temp1 = resultfile; temp1 += ".new.result";
	CString temp2 = resultfile; temp2 += ".old.result";

	//	temp0.MakeLower();
	//	temp0.Replace(".result","_with_Volume.result");
	//	retval = SolveWithVolume_old(lidarprecison, temp0);
	//	

	double min[3], max[3];

	Config.ReadConfigFile(cfgfilepath); //*<Read configuration file*/
	Config.CheckZero(1.0e-20);
	ReadPlaneVertex(vtxfilpath, PatchList, min, max); //Read vertex file
	ReadPlanarPoints(planarpointspath); //*<Read point cloud file (TEXT)*/

	//*<copy original data*/
	OriginConfig = Config;
	
	//retval = SolveWithVolume_old(lidarprecison, temp2);
	//retval = SolveWithVolume(lidarprecison, resultfile, max_iter, threshold_sigam);
	retval = SolveWithVolume_RNM_Brazil(lidarprecison, resultfile, max_iter, Small_threshold_sigam, Big_threshold_sigam);

	//CString temp1 = resultfile;
	//temp1.MakeLower();
	//temp1.Replace(".result","_with_ND.result");
	//SolveWithND(lidarprecison, temp1);

	///////////////////////////////////////////////////////////////////////////////////////////
	
	/*
	ReadConfigFile(cfgfilepath, Config); //Read configuration file
	ReadPlaneVertex_New(vtxfilpath, PatchList); //Read vertex file
	ReadPlanarPoints(planarpointspath); //Read point cloud file (TEXT)

	CString temp2 = resultfile;
	temp2.MakeLower();
	temp2.Replace(".result","_New.result");
	Solve_New(lidarprecison, temp2);
	*/

	return retval;
	
	//return Solve2();
}

bool CLIDARBiasCalibration::ReadIndexFile(CString fileinfopath, CSMList<int>& index)
{
	CString TagList[16];
	
	TagList[0] = "NAVIGATION_X";
	TagList[1] = "NAVIGATION_Y";
	TagList[2] = "NAVIGATION_Z";
	TagList[3] = "TIME";
	TagList[4] = "NAVIGATION_PITCH";
	TagList[5] = "NAVIGATION_ROLL";
	TagList[6] = "NAVIGATION_YAW";
	TagList[7] = "SCAN_ANGLE_ALPHA";
	TagList[8] = "SCAN_ANGLE_BETA";
	TagList[9] = "RANGE";
	TagList[10] = "GROUND_X";
	TagList[11] = "GROUND_Y";
	TagList[12] = "GROUND_Z";
	TagList[13] = "INTENSITY";
	TagList[14] = "WEIGHT_MATRIX";
	TagList[15] = "N/A";

	fstream InfoFile;
	InfoFile.open(fileinfopath, ios::in);

	char temp_st[512];

	while(!InfoFile.eof())
	{
		InfoFile>>temp_st;
		CString TagName = temp_st;
		
		bool bCheck = false;
		
		for(int i=0; i<15; i++)
		{
			if(temp_st == TagList[i])
			{
				bCheck = true;
				index.AddTail(i);
				break;
			}
		}
		
		if(bCheck == false)
		{
			return false;
		}

		InfoFile>>ws;
	}
	
	InfoFile.close();

	return true;
}

bool CLIDARBiasCalibration::ReadRecord(fstream &infile, CSMList<int>& index, CLIDARCalibrationPoint &point)
{
	int num_field = index.GetNumItem();
	if(num_field < 1) return false;

	for(int i=0; i<num_field; i++)
	{
		int nfield = index.GetAt(i);

		switch(index.GetAt(i))
		{
		case 0:
			infile>>point.GPS_X;
			break;
		case 1:
			infile>>point.GPS_Y;
			break;
		case 2:
			infile>>point.GPS_Z;
			break;
		case 3:
			infile>>point.time;
			break;
		case 4:
			infile>>point.INS_O;
			point.INS_O = Deg2Rad(point.INS_O);
			break;
		case 5:
			infile>>point.INS_P;
			point.INS_P = Deg2Rad(point.INS_P);
			break;
		case 6:
			infile>>point.INS_K;
			point.INS_K = Deg2Rad(point.INS_K);
			break;
		case 7:
			infile>>point.alpha;
			point.alpha = Deg2Rad(point.alpha);
			break;
		case 8:
			infile>>point.beta;
			point.beta = Deg2Rad(point.beta);
			break;
		case 9:
			infile>>point.dist;
			break;
		case 10:
			infile>>point.X;
			break;
		case 11:
			infile>>point.Y;
			break;
		case 12:
			infile>>point.Z;
			break;
		case 13:
			infile>>point.intensity;
			break;
		case 14:
			{
				for(int i=0; i<3; i++)
				{
					for(int j=0; j<3; j++)
					{
						double weight;
						infile>>weight;
						point.P(i, j) = weight;
					}
				}
			}
			break;
		case 15://N/A
			infile>>ws;
			char line[MAX_LINE_LENGTH];
			infile.getline(line, MAX_LINE_LENGTH);
			break;
		default:
			AfxMessageBox("Input file parsing error");
			return false;
		}
	}

	infile>>ws;

	return true;
}

bool CLIDARBiasCalibration::RunCalibration_Titan(CString cfgfilepath, CString inputfilepath, CString indexfile, CString vertexfile, CString resultfile, double lidarprecison, double Small_threshold_sigam, double Big_threshold_sigam, int max_iter, bool bPoint)
{
	bool retval;

	Config.ReadConfigFile(cfgfilepath); //*<Read configuration file*/
	
	Config.CheckZero(1.0e-20);
	
	ControlPoints.RemoveAll();
	RawPointList.RemoveAll();
	CString ext = inputfilepath.Right(6);
	ext.MakeLower();
	
	if(bPoint == true)
	{
		ReadIndexFile(indexfile, filecontents);

		if(ext == ".patch")
			ReadPatch(inputfilepath, ControlPoints, RawPointList); //Read point file
		else
			ReadTerraPoints(inputfilepath, ControlPoints, RawPointList); //Read point file
	}
	else
	{
		double min[3], max[3];
		ReadPlaneVertex(vertexfile, PatchList, min, max); //Read vertex file
		ReadPlanarPoints(inputfilepath); //*<Read point cloud file (TEXT)*/

	}

	//*<copy original config data*/
	OriginConfig = Config;

	if(bPoint == true)
		retval = SolveWithPoint_RNM_Titan(lidarprecison, resultfile, max_iter, Small_threshold_sigam, Big_threshold_sigam);
	else
		retval = SolveWithVolume_RNM_Titan(lidarprecison, resultfile, max_iter, Small_threshold_sigam, Big_threshold_sigam);

	return retval;
}

bool CLIDARBiasCalibration::RunCalibration(CString cfgfilepath, const char resultfile[], double lidarprecison, double Small_threshold_sigam, double Big_threshold_sigam, int max_iter)
{
	bool retval;

	//Config.ReadConfigFile(cfgfilepath); //*<Read configuration file*/
	//Config.CheckZero(1.0e-20);
	//ReadPlaneVertex_New(vtxfilpath, PatchList); //Read vertex file
	//ReadPlanarPoints(planarpointspath); //*<Read point cloud file (TEXT)*/

	//*<copy original data*/
	OriginConfig = Config;
	
	retval = SolveWithVolume_RNM(lidarprecison, resultfile, max_iter, Small_threshold_sigam, Big_threshold_sigam);
	//retval = SolveWithVolume(lidarprecison, resultfile, max_iter, threshold_sigam);

	return retval;
}

bool CLIDARBiasCalibration::Solve(double LIDAR_PRECISION, const char resultfile[])
{
	//General least square
	//JX = K + BV
	//F = aX + bY + cZ + 1 = 0
	//F = F0 + F' = 0
	//F = aX0 + bY0 + cZ0 + 1 + aX' + bY' + cZ' = 0
	//aX' + bY' + cZ' = -aX0 - bY0 - cZ0 - 1 + V
	//-aX0 - bY0 - cZ0 - 1 - aEX - bEY - cEZ = aX' + bY' + cZ'

	CSMMatrix<double> JMAT, KMAT, XMAT, We;	
	CSMMatrix<double> jmat, kmat, bmat, qmat;
	CSMMatrix<double> CMAT, NMAT, NMATinv;
	fstream outfile;
	outfile.open(resultfile,ios::out);
	int i, j;
	bool bStop = true;
	int nIteration = 0;
	int num_unknown = 6;
	double old_sigma;
	do{
		//Increase iteration number
		nIteration ++;

		//BMAT.Resize(0,0,0.);
		JMAT.Resize(0,0,0.);
		KMAT.Resize(0,0,0.);
		kmat.Resize(1,1,0.);

		//QMAT.Resize(0,0,0.);
		We.Resize(0,0,0.);

		jmat.Resize(1,num_unknown,0.);
		bmat.Resize(1,3,0.);
		kmat.Resize(1,1,0.);
		qmat.Resize(3,3,0.);
		//we.Resize(1,1,1.);
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////
		
		//Number of patches
		int num_Patches = PatchList.GetNumItem();
		
		//index of no. of rows
		int row_index = 0;
		
		for(i=0; i<num_Patches; i++)
		{
			//To get patch
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			
			//To set bmat matrix
			bmat(0,0) = -patch.coeff[0]; 
			bmat(0,1) = -patch.coeff[1]; 
			bmat(0,2) = -patch.coeff[2];
			
			int num_points = patch.PointList.GetNumItem();
			
			CSMMatrix<double> IncreaseWe;
			IncreaseWe.Resize(num_points,num_points,0.0);
			We.Insert(We.GetRows(),We.GetRows(),IncreaseWe);

			qmat.Identity(); //이부분 고정된것을 입력데이터 값으로 변경할 것.

			for(j=0; j<num_points; j++)
			{
				CLIDARRawPoint point = patch.PointList.GetAt(j);
				
				Cal_PDs(point, Config, patch.coeff, jmat);
				
				CSMMatrix<double> X0 = LIDAREQ(point, Config);
				
				kmat(0,0) = -(patch.coeff[0]*X0(0,0) + patch.coeff[1]*X0(1,0) + patch.coeff[2]*X0(2,0) + 1.);

				CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
				we_temp = we_temp.Inverse();
				We.Insert(row_index,row_index,we_temp);
				//QMAT.Insert(row_index*3,row_index*3,qmat);
				//BMAT.Insert(row_index,row_index*3,bmat);
		
				JMAT.Insert(row_index,0,jmat);
				
				KMAT.Insert(row_index,0,kmat);
				
				row_index ++;
			}
		}
		
		//equivalent weight matrix
		//CSMMatrix<double> We;// = (BMAT%QMAT%BMAT.Transpose()).Inverse();  
		//We.Resize(row_index, row_index,0.);
		//We.Identity(1.0);

		//We.Resize(JMAT.GetRows(),JMAT.GetRows());
		//We.Identity(1.0);
		//We(JMAT.GetRows()-7,JMAT.GetRows()-7) = 1.0e100;
		//We(JMAT.GetRows()-6,JMAT.GetRows()-6) = 1.0e100;
		//We(JMAT.GetRows()-5,JMAT.GetRows()-5) = 1.0e100;
		//We(JMAT.GetRows()-4,JMAT.GetRows()-4) = 1.0e100;
		//We(JMAT.GetRows()-3,JMAT.GetRows()-3) = 1.0e100;
		//We(JMAT.GetRows()-2,JMAT.GetRows()-2) = 1.0e100;
		//We(JMAT.GetRows()-1,JMAT.GetRows()-1) = 1.0e100;

		//Weight for parameters
		CSMMatrix<double> WParam(6,6,0.);
		WParam(0,0) = 1./Config.sXb/Config.sXb;
		WParam(1,1) = 1./Config.sYb/Config.sYb;
		WParam(2,2) = 1./Config.sZb/Config.sZb;
		WParam(3,3) = 1./Config.sOb/Config.sOb;
		WParam(4,4) = 1./Config.sPb/Config.sPb;
		WParam(5,5) = 1./Config.sKb/Config.sKb;

		//Parameter observations
		We.Insert(row_index,row_index,WParam);

		jmat.Resize(1,6,0.);
		jmat(0,0) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Xb - Config.Xb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,1) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Yb - Config.Yb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,2) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Zb - Config.Zb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,3) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Ob - Config.Ob;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,4) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Pb - Config.Pb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,5) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Kb - Config.Kb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;


		//Normal matrix
		NMAT = JMAT.Transpose()%We%JMAT;
		
		CMAT = JMAT.Transpose()%We%KMAT;
		
		outfile.precision(10);
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<"[JMAT]"<<endl;
		/*for(i=0; i<(int)JMAT.GetRows(); i++)
		{
			for(j=0; j<(int)JMAT.GetCols(); j++)
				outfile<<setw(10)<<JMAT(i,j)<<"\t";
			outfile<<endl;
		}*/
		outfile.flush();

		outfile<<setw(10)<<"[KMAT]"<<endl;
		for(i=0; i<(int)KMAT.GetRows(); i++)
		{
			for(j=0; j<(int)KMAT.GetCols(); j++)
				outfile<<setw(10)<<KMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[NMAT]"<<endl;
		for(i=0; i<(int)NMAT.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT.GetCols(); j++)
				outfile<<setw(10)<<NMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[CMAT]"<<endl;
		for(i=0; i<(int)CMAT.GetRows(); i++)
		{
			for(j=0; j<(int)CMAT.GetCols(); j++)
				outfile<<setw(10)<<CMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		NMATinv = NMAT.Inverse();
		//XMAT
		XMAT = NMATinv%CMAT;

		outfile<<setw(10)<<"[NMATinv]"<<endl;
		for(i=0; i<(int)NMATinv.GetRows(); i++)
		{
			for(j=0; j<(int)NMATinv.GetCols(); j++)
				outfile<<setw(10)<<NMATinv(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile<<setw(10)<<XMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		CSMMatrix<double> V = JMAT%XMAT - KMAT;
		CSMMatrix<double> VTV = V.Transpose()%We%V;
		double variance = VTV(0,0)/(JMAT.GetRows() - num_unknown);
		double sigma = sqrt(variance);

		outfile<<setw(10)<<"[VMAT]"<<endl;
		/*for(i=0; i<(int)V.GetRows(); i++)
		{
			for(j=0; j<(int)V.GetCols(); j++)
				outfile<<setw(10)<<V(i,j)<<"\t";
			outfile<<endl;
		}*/
		outfile.flush();

		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);
		
		//ConfigList.GetDataHandle(i)->Xb += XMAT(i*7+0,0);
		//ConfigList.GetDataHandle(i)->Yb += XMAT(i*7+1,0);
		//ConfigList.GetDataHandle(i)->Zb += XMAT(i*7+2,0);
		//ConfigList.GetDataHandle(i)->Ob += XMAT(i*7+3,0);
		//ConfigList.GetDataHandle(i)->Pb += XMAT(i*7+4,0);
		//ConfigList.GetDataHandle(i)->Kb += XMAT(i*7+5,0);
		//ConfigList.GetDataHandle(i)->Rangeb += XMAT(i*7+6,0);
		
		outfile.precision(10);
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<nIteration<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
		
		outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		outfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
		outfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
		outfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<"\t";
		//outfile<<setw(10)<<"Rangeb: "<<ConfigList.GetAt(0).Rangeb<<endl;
		
		outfile<<endl;
		
		//outfile<<setw(10)<<"sigma_Xb(mm), sigma_Yb(mm), sigma_Zb(mm), sigma_Ob(sec), sigma_Pb(sec), sigma_Kb(sec)"<<endl;
		outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		outfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))*1000<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))*3600<<"\t";
		
		outfile<<endl;
		
		outfile.flush();

		old_sigma = sigma;

		if(nIteration > 1)
		{
			if(fabs(old_sigma-sigma)<fabs(LIDAR_PRECISION*0.0000001))
			{
				outfile<<endl<<endl<<"##################################################################"<<endl;
				outfile<<"Iteration Stop(fabs(old_sigma-sigma))): "<<fabs(old_sigma-sigma)<<endl;
				outfile<<"##################################################################"<<endl<<endl;
				bStop = false;
			}
		}

		old_sigma = sigma;

		if(nIteration >= 20)
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}

		if(bStop == false)
		{
			fstream newconfigfile;
			CString configpath = resultfile;
			configpath.MakeLower();
			configpath.Replace(".result",".config");
			newconfigfile.open(configpath,ios::out);
			newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Biases in spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Sigma of spatial offsets biases(3EA, M)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
			newconfigfile <<"#ID"<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile.precision(10);
			newconfigfile.setf(ios::scientific );
			newconfigfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))<<endl;
			newconfigfile<<setw(10)<<Config.Rangeb<<"\t"<<Config.sRangeb<<endl;
			newconfigfile<<setw(10)<<"1";
			newconfigfile.close();
		}
		
	}while(bStop == true);

	CBMPImage img;
	img.Create(NMAT.GetCols(),NMAT.GetRows(),8);
	CBMPPixelPtr ptr(img);
	for(i=0; i<(int)NMAT.GetRows(); i++)
	{
		for(j=0; j<(int)NMAT.GetCols(); j++) 
		{
			if(NMAT(i,j)==0.) ptr[i][j] = BYTE(255);
			else ptr[i][j] = BYTE(0);
		}
	}
	
	CString nimgpath = resultfile;
	nimgpath.MakeLower();
	nimgpath.Replace(".result","_N.bmp");
	img.SaveBMP(nimgpath);
	
	CBMPImage img2;
	CSMMatrix<double> CorreMat;
	CorreMat = Correlation(NMATinv);
	outfile<<setw(10)<<"[CorreMat]"<<endl;
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
			outfile<<setw(10)<<CorreMat(i,j)<<"\t";
		outfile<<endl;
	}
	outfile.flush();
	img2.Create(CorreMat.GetCols(),CorreMat.GetRows(),8);
	CBMPPixelPtr ptr2(img2);
	
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
		{
			if(fabs(CorreMat(i,j))>0.9) 
			{
				double temp = CorreMat(i,j)-0.9;
				ptr2[i][j] = BYTE(fabs(temp)/0.1*255);
			}
			else ptr2[i][j] = BYTE(0);
		}
	}
	
	CString correpath = resultfile;
	correpath.MakeLower();
	correpath.Replace(".result","_corre.bmp");
	img2.SaveBMP(correpath);

	outfile.close();

	return true;
}

bool CLIDARBiasCalibration::Solve_New(double LIDAR_PRECISION, const char resultfile[])
{
	//General least square
	//JX = K + BV
	//F = aX + bY + Z + c = 0
	//F = F0 + F' = 0
	//F = aX0 + bY0 + Z0 + c + aX' + bY' + Z' = 0
	//aX' + bY' + Z' = -aX0 - bY0 - Z0 - c + V
	//-aX0 - bY0 - Z0 - c - aEX - bEY - EZ = aX' + bY' + Z'
	CSMMatrix<double> JMAT, KMAT, XMAT, We;	
	CSMMatrix<double> jmat, kmat, bmat, qmat;
	CSMMatrix<double> CMAT, NMAT, NMATinv;
	fstream outfile;
	outfile.open(resultfile,ios::out);
	int i, j;
	bool bStop = true;
	int nIteration = 0;
	int num_unknown = 6;
	double old_sigma;
	do{
		//Increase iteration number
		nIteration ++;

		//BMAT.Resize(0,0,0.);
		JMAT.Resize(0,0,0.);
		KMAT.Resize(0,0,0.);
		kmat.Resize(1,1,0.);

		//QMAT.Resize(0,0,0.);
		We.Resize(0,0,0.);

		jmat.Resize(1,num_unknown,0.);
		bmat.Resize(1,3,0.);
		kmat.Resize(1,1,0.);
		qmat.Resize(3,3,0.);
		//we.Resize(1,1,1.);
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////
		
		//Number of patches
		int num_Patches = PatchList.GetNumItem();
		
		//index of no. of rows
		int row_index = 0;
		
		for(i=0; i<num_Patches; i++)
		{
			//To get patch
			CLIDARPlanarPatch patch = PatchList.GetAt(i);

			//To set bmat matrix
			//0=aX+bY+cZ+1
			//D = AX + BY + CZ
			//double dist = fabs(A*XG(0,0) + B*XG(1,0) + C*XG(2,0) - D)/sqrt(A*A+B*B+C*C);
			double A=(patch.coeff[0]), B=(patch.coeff[1]), C=1., D=-patch.coeff[2];
			double temp = sqrt(A*A+B*B+C*C);
			if(temp == 0) return false;
			bmat(0,0) = (A)/temp;
			bmat(0,1) = (B)/temp; 
			bmat(0,2) = (C)/temp;
			
			int num_points = patch.PointList.GetNumItem();
			
			CSMMatrix<double> IncreaseWe;
			IncreaseWe.Resize(num_points,num_points,0.0);
			We.Insert(We.GetRows(),We.GetRows(),IncreaseWe);

			qmat.Identity(); //이부분 고정된것을 입력데이터 값으로 변경할 것.

			for(j=0; j<num_points; j++)
			{
				CLIDARRawPoint point = patch.PointList.GetAt(j);
				
				Cal_PDs_New(point, Config, patch.coeff, jmat);
				
				CSMMatrix<double> X0 = LIDAREQ(point, Config);
				
				//-aX0 - bY0 - Z0 - c - aEX - bEY - EZ = aX' + bY' + Z'
				
				//To set bmat matrix
				//0=aX+bY+cZ+1
				//D = AX + BY + CZ
				//double dist = fabs(A*XG(0,0) + B*XG(1,0) + C*XG(2,0) - D)/sqrt(A*A+B*B+C*C);
				//AX'/temp + BY'/tmep + CZ'/temp = -fabs(A*X0 + B*Y0 + C*Z0 - D)/sqrt(A*A+B*B+C*C) -AEX - BEY - CEZ
				
				kmat(0,0) = -(A*X0(0,0) + B*X0(1,0) + C*X0(2,0) - D)/temp;

				CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
				we_temp = we_temp.Inverse();
				We.Insert(row_index,row_index,we_temp);
				//QMAT.Insert(row_index*3,row_index*3,qmat);
				//BMAT.Insert(row_index,row_index*3,bmat);
		
				JMAT.Insert(row_index,0,jmat);
				
				KMAT.Insert(row_index,0,kmat);
				
				row_index ++;
			}
		}
		
		//equivalent weight matrix
		//CSMMatrix<double> We;// = (BMAT%QMAT%BMAT.Transpose()).Inverse();  
		//We.Resize(row_index, row_index,0.);
		//We.Identity(1.0);

		//We.Resize(JMAT.GetRows(),JMAT.GetRows());
		//We.Identity(1.0);
		//We(JMAT.GetRows()-7,JMAT.GetRows()-7) = 1.0e100;
		//We(JMAT.GetRows()-6,JMAT.GetRows()-6) = 1.0e100;
		//We(JMAT.GetRows()-5,JMAT.GetRows()-5) = 1.0e100;
		//We(JMAT.GetRows()-4,JMAT.GetRows()-4) = 1.0e100;
		//We(JMAT.GetRows()-3,JMAT.GetRows()-3) = 1.0e100;
		//We(JMAT.GetRows()-2,JMAT.GetRows()-2) = 1.0e100;
		//We(JMAT.GetRows()-1,JMAT.GetRows()-1) = 1.0e100;

		//Weight for parameters
		CSMMatrix<double> WParam(6,6,0.);
		WParam(0,0) = 1./Config.sXb/Config.sXb;
		WParam(1,1) = 1./Config.sYb/Config.sYb;
		WParam(2,2) = 1./Config.sZb/Config.sZb;
		WParam(3,3) = 1./Config.sOb/Config.sOb;
		WParam(4,4) = 1./Config.sPb/Config.sPb;
		WParam(5,5) = 1./Config.sKb/Config.sKb;

		//Parameter observations
		We.Insert(row_index,row_index,WParam);

		jmat.Resize(1,6,0.);
		jmat(0,0) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Xb - Config.Xb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,1) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Yb - Config.Yb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,2) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Zb - Config.Zb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,3) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Ob - Config.Ob;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,4) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Pb - Config.Pb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,5) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Kb - Config.Kb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;


		//Normal matrix
		NMAT = JMAT.Transpose()%We%JMAT;
		
		CMAT = JMAT.Transpose()%We%KMAT;
		
		outfile.precision(10);
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<"[JMAT]"<<endl;
		/*for(i=0; i<(int)JMAT.GetRows(); i++)
		{
			for(j=0; j<(int)JMAT.GetCols(); j++)
				outfile<<setw(10)<<JMAT(i,j)<<"\t";
			outfile<<endl;
		}*/
		outfile.flush();

		outfile<<setw(10)<<"[KMAT]"<<endl;
		for(i=0; i<(int)KMAT.GetRows(); i++)
		{
			for(j=0; j<(int)KMAT.GetCols(); j++)
				outfile<<setw(10)<<KMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[NMAT]"<<endl;
		for(i=0; i<(int)NMAT.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT.GetCols(); j++)
				outfile<<setw(10)<<NMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[CMAT]"<<endl;
		for(i=0; i<(int)CMAT.GetRows(); i++)
		{
			for(j=0; j<(int)CMAT.GetCols(); j++)
				outfile<<setw(10)<<CMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		NMATinv = NMAT.Inverse();
		//XMAT
		XMAT = NMATinv%CMAT;

		outfile<<setw(10)<<"[NMATinv]"<<endl;
		for(i=0; i<(int)NMATinv.GetRows(); i++)
		{
			for(j=0; j<(int)NMATinv.GetCols(); j++)
				outfile<<setw(10)<<NMATinv(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile<<setw(10)<<XMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		CSMMatrix<double> V = JMAT%XMAT - KMAT;
		CSMMatrix<double> VTV = V.Transpose()%We%V;
		double variance = VTV(0,0)/(JMAT.GetRows() - num_unknown);
		double sigma = sqrt(variance);

		outfile<<setw(10)<<"[VMAT]"<<endl;
		/*for(i=0; i<(int)V.GetRows(); i++)
		{
			for(j=0; j<(int)V.GetCols(); j++)
				outfile<<setw(10)<<V(i,j)<<"\t";
			outfile<<endl;
		}*/
		outfile.flush();

		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);
		
		//ConfigList.GetDataHandle(i)->Xb += XMAT(i*7+0,0);
		//ConfigList.GetDataHandle(i)->Yb += XMAT(i*7+1,0);
		//ConfigList.GetDataHandle(i)->Zb += XMAT(i*7+2,0);
		//ConfigList.GetDataHandle(i)->Ob += XMAT(i*7+3,0);
		//ConfigList.GetDataHandle(i)->Pb += XMAT(i*7+4,0);
		//ConfigList.GetDataHandle(i)->Kb += XMAT(i*7+5,0);
		//ConfigList.GetDataHandle(i)->Rangeb += XMAT(i*7+6,0);
		
		outfile.precision(10);
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<nIteration<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
		
		outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		outfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
		outfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
		outfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<"\t";
		//outfile<<setw(10)<<"Rangeb: "<<ConfigList.GetAt(0).Rangeb<<endl;
		
		outfile<<endl;
		
		//outfile<<setw(10)<<"sigma_Xb(mm), sigma_Yb(mm), sigma_Zb(mm), sigma_Ob(sec), sigma_Pb(sec), sigma_Kb(sec)"<<endl;
		outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		outfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))*1000<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))*3600<<"\t";
		
		outfile<<endl;
		
		outfile.flush();

		if(nIteration > 1)
		{
			if(fabs(old_sigma-sigma)<fabs(LIDAR_PRECISION*0.0000001))
			{
				outfile<<endl<<endl<<"##################################################################"<<endl;
				outfile<<"Iteration Stop(fabs(old_sigma-sigma))): "<<fabs(old_sigma-sigma)<<endl;
				outfile<<"##################################################################"<<endl<<endl;
				bStop = false;
			}
		}

		old_sigma = sigma;

		if(nIteration >= 20)
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}

		if(bStop == false)
		{
			fstream newconfigfile;
			CString configpath = resultfile;
			configpath.MakeLower();
			configpath.Replace(".result",".config");
			newconfigfile.open(configpath,ios::out);
			newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Biases in spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Sigma of spatial offsets biases(3EA, M)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
			newconfigfile <<"#ID"<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile.precision(10);
			newconfigfile.setf(ios::scientific );
			newconfigfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))<<endl;
			newconfigfile<<setw(10)<<Config.Rangeb<<"\t"<<Config.sRangeb<<endl;
			newconfigfile<<setw(10)<<"1";
			newconfigfile.close();
		}
		
	}while(bStop == true);

	CBMPImage img;
	img.Create(NMAT.GetCols(),NMAT.GetRows(),8);
	CBMPPixelPtr ptr(img);
	for(i=0; i<(int)NMAT.GetRows(); i++)
	{
		for(j=0; j<(int)NMAT.GetCols(); j++) 
		{
			if(NMAT(i,j)==0.) ptr[i][j] = BYTE(255);
			else ptr[i][j] = BYTE(0);
		}
	}
	
	CString nimgpath = resultfile;
	nimgpath.MakeLower();
	nimgpath.Replace(".result","_N.bmp");
	img.SaveBMP(nimgpath);
	
	CBMPImage img2;
	CSMMatrix<double> CorreMat;
	CorreMat = Correlation(NMATinv);
	outfile<<setw(10)<<"[CorreMat]"<<endl;
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
			outfile<<setw(10)<<CorreMat(i,j)<<"\t";
		outfile<<endl;
	}
	outfile.flush();
	img2.Create(CorreMat.GetCols(),CorreMat.GetRows(),8);
	CBMPPixelPtr ptr2(img2);
	
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
		{
			if(fabs(CorreMat(i,j))>0.9) 
			{
				double temp = CorreMat(i,j)-0.9;
				ptr2[i][j] = BYTE(fabs(temp)/0.1*255);
			}
			else ptr2[i][j] = BYTE(0);
		}
	}
	
	CString correpath = resultfile;
	correpath.MakeLower();
	correpath.Replace(".result","_corre.bmp");
	img2.SaveBMP(correpath);

	outfile.close();

	return true;
}

bool CLIDARBiasCalibration::SolveWithVolume_RNM(double LIDAR_PRECISION, const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold)
{
	int i, j;

	//number of unknown parameters
	const int num_unknown = 10;//3 spatial offset, 3 rotational offset, 2 scan angle offset, range bias, range scale

	//Number of patches
	int num_Patches = PatchList.GetNumItem();

	//original patches
	OriginPatchList = PatchList;

	//Extract point ID
	CSMList<CString> VTXList;
	for(i=0; i<num_Patches; i++)
	{
		CLIDARPlanarPatch patch = PatchList.GetAt(i);
		
		int nSort;

		CString ID_A = patch.ID_A;
		CString ID_B = patch.ID_B;
		CString ID_C = patch.ID_C;
		nSort = FindCStringID(VTXList,ID_A);
		if(nSort == -1) VTXList.AddTail(ID_A);

		nSort = FindCStringID(VTXList,ID_B);
		if(nSort == -1) VTXList.AddTail(ID_B);

		nSort = FindCStringID(VTXList,ID_C);
		if(nSort == -1) VTXList.AddTail(ID_C);
	}
	
	CSMMatrix<double> NMAT_dot, CMAT_dot, NMAT_dot_inv;
	CSMMatrix<double> Normal_Matrix;
	CSMMatrix<double> N2dot_point;
	CSMMatrix<double> N2dot_line;
	CSMMatrix<double> N2dot_tri;
		
	//result files
	fstream outfile;//txt file
	outfile.precision(10);
	fstream csvfile;//csv file

	CString csvpath = resultfile;
	csvpath.MakeLower();
	csvpath.Replace(".result",".csv");
	csvfile.open(csvpath, ios::out);
	
	CString resultname = resultfile;
	outfile.open(resultname,ios::out);

	bool bStop = true;
	int nIteration = 0;
	//number of observations
	const int num_obs = 9;
	double old_sigma;
	double old_rmse_ND;

	//Weight for parameters
	CSMMatrix<double> QParam(num_unknown,num_unknown,0.);
	QParam(0,0) = Config.sXb*Config.sXb;
	QParam(1,1) = Config.sYb*Config.sYb;
	QParam(2,2) = Config.sZb*Config.sZb;

	QParam(3,3) = Config.sOb*Config.sOb;
	QParam(4,4) = Config.sPb*Config.sPb;
	QParam(5,5) = Config.sKb*Config.sKb;
	
	QParam(6,6) = Config.sAlpha_b*Config.sAlpha_b;
	QParam(7,7) = Config.sBeta_b*Config.sBeta_b;

	QParam(8,8) = Config.sRangeb*Config.sRangeb;
	QParam(9,9) = Config.sRangeS*Config.sRangeS;

	//fixed unknown parameters list	
	CSMMatrix<double> FixedParamIndex(1, num_unknown, 1.0);
	for(i=0; i<(int)num_unknown; i++)
	{
		if(QParam(i,i)<pow(Small_QThreshold,2))
		{
			FixedParamIndex(0,i) = 0.0;
		}
	}

	CSMMatrix<double> qmat;
	qmat.Resize(num_obs,num_obs,0.);

	qmat(0,0) = Config.sX*Config.sX;
	qmat(1,1) = Config.sY*Config.sY;
	qmat(2,2) = Config.sZ*Config.sZ;

	qmat(3,3) = Config.sO*Config.sO;
	qmat(4,4) = Config.sP*Config.sP;
	qmat(5,5) = Config.sK*Config.sK;

	qmat(6,6) = Config.sAlpha*Config.sAlpha;
	qmat(7,7) = Config.sBeta*Config.sBeta;
	qmat(8,8) = Config.sRange*Config.sRange;

	CSMMatrix<double> AREA, area, jmat_vertexA, jmat_vertexB, jmat_vertexC;

	area.Resize(1,1,0.);
	
	jmat_vertexA.Resize(1,3,0.);
	jmat_vertexB.Resize(1,3,0.);
	jmat_vertexC.Resize(1,3,0.);
	
	CSMMatrix<double> kmat, jmat_lidar, bmat;
	kmat.Resize(1,1,0.);
	jmat_lidar.Resize(1,num_unknown,0.);
	bmat.Resize(1,num_obs,0.);

	do
	{
		//Increase iteration number
		nIteration ++;

		CLeastSquareLIDAR LS;
		LS.SetDataConfig(1,num_unknown,0,0, num_Patches);
		
		Pvector.Resize(3,1);
		//boresight vector (spatial offset + spatial bias)
		Pvector(0,0) = Config.Xoffset + Config.Xb;
		Pvector(1,0) = Config.Yoffset + Config.Yb;
		Pvector(2,0) = Config.Zoffset + Config.Zb;
		
		//offset angle + bias angle
		dO = Config.Ooffset + Config.Ob;
		dP = Config.Poffset + Config.Pb;
		dK = Config.Koffset + Config.Kb;
		//INS Rotation matrix: R matrix
		Offset_R.ReMake(dO, dP, dK);
		
		//partial derivatives for angle biases (omega)
		dOffsetR_dO.Partial_dRdO(dO,dP,dK);
		//partial derivatives for angle biases (phi)
		dOffsetR_dP.Partial_dRdP(dO,dP,dK);
		//partial derivatives for angle biases (kappa)
		dOffsetR_dK.Partial_dRdK(dO,dP,dK);
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////

		outfile<<setw(10)<<"[K matrix (normal distances)]"<<endl;
		
		for(i=0; i<num_Patches; i++)
		{
			//To get patch
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			area(0,0) = patch.Area;
			AREA.Insert(AREA.GetRows(),0,area);
			
			int nSortA = FindCStringID(VTXList, patch.ID_A);
			int nSortB = FindCStringID(VTXList, patch.ID_B);
			int nSortC = FindCStringID(VTXList, patch.ID_C);
			
			int num_points = patch.PointList.GetNumItem();
			
			XA = patch.Vertex[0];	YA = patch.Vertex[1];	ZA = patch.Vertex[2];
			XB = patch.Vertex[3];	YB = patch.Vertex[4];	ZB = patch.Vertex[5];
			XC = patch.Vertex[6];	YC = patch.Vertex[7];	ZC = patch.Vertex[8];
			
			CSMMatrix<double> QVertexA(3,3,0.);
			QVertexA(0,0) = patch.SD[0]*patch.SD[0];
			QVertexA(1,1) = patch.SD[1]*patch.SD[1];
			QVertexA(2,2) = patch.SD[2]*patch.SD[2];
			
			CSMMatrix<double> QVertexB(3,3,0.);
			QVertexB(0,0) = patch.SD[3]*patch.SD[3];
			QVertexB(1,1) = patch.SD[4]*patch.SD[4];
			QVertexB(2,2) = patch.SD[5]*patch.SD[5];
			
			CSMMatrix<double> QVertexC(3,3,0.);
			QVertexC(0,0) = patch.SD[6]*patch.SD[6];
			QVertexC(1,1) = patch.SD[7]*patch.SD[7];
			QVertexC(2,2) = patch.SD[8]*patch.SD[8];

			CSMMatrix<double> FixedParamPoint(1,9,1.0);
			
			for(int index=0; index<(int)3;index++)
			{
				if(QVertexA(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index) = 0.0;
				}
				if(QVertexB(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index+3) = 0.0;
				}
				if(QVertexC(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index+6) = 0.0;
				}
			}
			
			//plane coefficient
			K1 = YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC - YC*ZB);
			K2 = -XA*(ZB-ZC) +ZA*(XB-XC) - (XB*ZC - XC*ZB);
			K3 = XA*(YB-YC) - YA*(XB-XC) + (XB*YC - XC*YB);
			K4 = -XA*(YB*ZC-YC*ZB) + YA*(XB*ZC-XC*ZB) -ZA* (XB*YC - XC*YB);

			//K1, K2, and K3
			K123.Resize(1,3);
			K123(0,0) = K1;
			K123(0,1) = K2;
			K123(0,2) = K3;

			for(j=0; j<num_points; j++)
			{
				CLIDARRawPoint point = patch.PointList.GetAt(j);
				
				Cal_PDsWithVolume(point, Config, patch, jmat_lidar,jmat_vertexA,jmat_vertexB,jmat_vertexC, bmat, kmat);

				outfile<<kmat(0,0)/patch.Area<<endl;
				
				CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
				we_temp = we_temp.Inverse();

				CSMMatrix<double> j_tri; j_tri.Resize(0,0);
				j_tri.Insert(0,0,jmat_vertexA); 
				j_tri.Insert(0,j_tri.GetCols(),jmat_vertexB);
				j_tri.Insert(0,j_tri.GetCols(),jmat_vertexC);
				//reduced normal matrix
				LS.Fill_Nmat_Data_LIDARPointwithDeterminent(FixedParamIndex, FixedParamPoint, jmat_lidar, j_tri, we_temp, kmat, i, area(0,0));
				//
			}//for(j=0; j<num_points; j++)

			//Parameter observations(vertex coord)
			CLIDARPlanarPatch oripatch = OriginPatchList.GetAt(i);
			CSMMatrix<double> jmat_vtx_param(9,9);
			jmat_vtx_param.Identity(1.0);
			CSMMatrix<double> k_vtx(9,1);
			CSMMatrix<double> WVTX(9,9);
			
			//vertex A
			k_vtx(0,0) = oripatch.Vertex[0] - patch.Vertex[0];
			k_vtx(1,0) = oripatch.Vertex[1] - patch.Vertex[1];
			k_vtx(2,0) = oripatch.Vertex[2] - patch.Vertex[2];

			WVTX.Insert(0,0,QVertexA.Inverse());

			//vertex B
			k_vtx(3,0) = oripatch.Vertex[3] - patch.Vertex[3];
			k_vtx(4,0) = oripatch.Vertex[4] - patch.Vertex[4];
			k_vtx(5,0) = oripatch.Vertex[5] - patch.Vertex[5];

			WVTX.Insert(3,3,QVertexB.Inverse());

			//vertex C
			k_vtx(6,0) = oripatch.Vertex[6] - patch.Vertex[6];
			k_vtx(7,0) = oripatch.Vertex[7] - patch.Vertex[7];
			k_vtx(8,0) = oripatch.Vertex[8] - patch.Vertex[8];

			WVTX.Insert(6,6,QVertexC.Inverse());

			LS.Fill_Nmat_TriangleVertex(jmat_vtx_param,WVTX,k_vtx,i);

			outfile<<k_vtx.matrixout()<<endl;

		}//for(i=0; i<num_Patches; i++)

	
		//Parameter observations(LIDAR param)
		CSMMatrix<double> WParam = QParam.Inverse();
		
		CSMMatrix<double> jmat_param(num_unknown,num_unknown,0.);
		jmat_param.Identity(1.0);
		CSMMatrix<double> jmat_lidar_k(num_unknown,1,0.);

		jmat_lidar_k(0,0) = OriginConfig.Xb - Config.Xb;
		jmat_lidar_k(1,0) = OriginConfig.Yb - Config.Yb;
		jmat_lidar_k(2,0) = OriginConfig.Zb - Config.Zb;
		
		jmat_lidar_k(3,0) = OriginConfig.Ob - Config.Ob;
		jmat_lidar_k(4,0) = OriginConfig.Pb - Config.Pb;
		jmat_lidar_k(5,0) = OriginConfig.Kb - Config.Kb;
		
		jmat_lidar_k(6,0) = OriginConfig.Alpha_b - Config.Alpha_b;
		jmat_lidar_k(7,0) = OriginConfig.Beta_b - Config.Beta_b;
		
		jmat_lidar_k(8,0) = OriginConfig.Rangeb - Config.Rangeb;
		jmat_lidar_k(9,0) = OriginConfig.RangeS - Config.RangeS;

		for(int param_index=0; param_index<num_unknown; param_index++)
		{
			if(sqrt(QParam(param_index,param_index)) <= fabs(Small_QThreshold))
			{
				jmat_lidar_k(param_index, 0) = 0.0; 
			}					
		}

		outfile<<jmat_lidar_k.matrixout()<<endl;

		LS.Fill_Nmat_LIDARParam(jmat_param,WParam,jmat_lidar_k);

		double var;
		CSMMatrix<double> XMAT = LS.RunLeastSquare_RN(var, NMAT_dot, CMAT_dot);
		NMAT_dot_inv = NMAT_dot.Inverse();

		//2007.05.09 Normal_Matrix = LS.GetNmatrix();
		Normal_Matrix = LS.GetNmatrix(N2dot_point, N2dot_line, N2dot_tri);

		outfile<<setw(10)<<"[NMAT_dot]"<<endl;
		for(i=0; i<(int)NMAT_dot.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT_dot.GetCols(); j++)
				outfile<<setw(10)<<NMAT_dot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[CMAT_dot]"<<endl;
		for(i=0; i<(int)CMAT_dot.GetRows(); i++)
		{
			for(j=0; j<(int)CMAT_dot.GetCols(); j++)
				outfile<<setw(10)<<CMAT_dot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		
		CSMMatrix<double> CorreMat;
		CorreMat = Correlation(NMAT_dot_inv);
		outfile<<setw(10)<<"[CorreMat]"<<endl;
		for(i=0; i<(int)CorreMat.GetRows(); i++)
		{
			for(j=0; j<(int)CorreMat.GetCols(); j++)
				outfile<<setw(10)<<CorreMat(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		
		CBMPImage img2;
		img2.Create(CorreMat.GetCols(),CorreMat.GetRows(),8);
		CBMPPixelPtr ptr2(img2);
		
		for(i=0; i<(int)CorreMat.GetRows(); i++)
		{
			for(j=0; j<(int)CorreMat.GetCols(); j++)
			{
				if(fabs(CorreMat(i,j))>0.9) 
				{
					double temp = CorreMat(i,j)-0.9;
					ptr2[i][j] = BYTE(fabs(temp)/0.1*255);
				}
				else ptr2[i][j] = BYTE(0);
			}
		}
		
		CString correpath = resultfile;
		correpath.MakeLower();
		correpath.Replace(".result","_corre.bmp");
		img2.SaveBMP(correpath);

		outfile.setf(ios::scientific);
		
		//*
		outfile<<setw(10)<<"[NMAT_dot_inv]"<<endl;
		for(i=0; i<(int)NMAT_dot_inv.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT_dot_inv.GetCols(); j++)
				outfile<<setw(10)<<NMAT_dot_inv(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile<<setw(10)<<XMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		//*/

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//		for(i=0; i<(int)num_unknown; i++)
		//		{
		//			if(QParam(i,i)<pow(QThreshold*0.01,2))
		//			{
		//				XMAT(i,0) = 0.0;
		//			}
		//		}

		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);

		Config.Alpha_b += XMAT(6,0);
		Config.Beta_b  += XMAT(7,0);

		Config.Rangeb += XMAT(8,0);
		Config.RangeS += XMAT(9,0);

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		for(i=0; i<(int)num_Patches; i++)
		{
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			
			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_A);
				
				//				if((patch.SD[j]*patch.SD[j])<QThreshold)
				//				{
				//					if(nSort != -1) XMAT(num_unknown+nSort*3+j,0) = 0.0;
				//				}
				if(nSort != -1) patch.Vertex[j] += XMAT(num_unknown+nSort*3+j,0);
			}

			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_B);
				//				if((patch.SD[j+3]*patch.SD[j+3])<QThreshold)
				//				{
				//					if(nSort != -1) XMAT(num_unknown+nSort*3+j,0) = 0.0;
				//				}
				if(nSort != -1) patch.Vertex[j+3] += XMAT(num_unknown+nSort*3+j,0);
			}

			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_C);
				//				if((patch.SD[j+6]*patch.SD[j+6])<QThreshold)
				//				{					
				//					if(nSort != -1) XMAT(num_unknown+nSort*3+j,0) = 0.0;
				//				}
				if(nSort != -1) patch.Vertex[j+6] += XMAT(num_unknown+nSort*3+j,0);
			}
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		CSMList<MatrixData> VolError, NDError; 
		CSMList<MatrixData> V;
		LS.GetLIDARErrorMatwithDeterminent(V, VolError, NDError);

		int n_mat = VolError.GetNumItem();
		
		DATA<MatrixData> *pos1 = NULL;
		DATA<MatrixData> *pos2 = NULL;
		
		double sum_EVol = 0;
		double sum_END = 0;
		int count=0;
		
		for(i=0; i<n_mat; i++)
		{
			pos1 = VolError.GetNextPos(pos1);
			pos2 = NDError.GetNextPos(pos2);
			
			int nrows = pos1->value.mat.GetRows();
			
			for(j=0; j<nrows; j++)
			{
				double temp = pos1->value.mat(j,0);
				temp = temp*temp;
				sum_EVol += temp;

				temp = pos2->value.mat(j,0);
				temp = temp*temp;
				sum_END += temp;

				count++;
			}
		}

		//2007.05.09
//		CSMMatrix<double> Sum_VolError2 = VolError.Transpose()%VolError;
//		double Mean_VolError2 = Sum_VolError2(0,0)/VolError.GetRows();
//		double RMSE_VolError = sqrt(Mean_VolError2);

//		CSMMatrix<double> Sum_NDError2 = NDError.Transpose()%NDError;
//		double Mean_NDError2 = Sum_NDError2(0,0)/NDError.GetRows();
//		double RMSE_NDError = sqrt(Mean_NDError2);

		double RMSE_VolError = sqrt(sum_EVol/count);

		double RMSE_NDError = sqrt(sum_END/count);

		double sigma = sqrt(var);
		
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<"\t"<<sigma<<endl;
		outfile<<setw(10)<<" RMSE(Volume errors): "<<"\t"<<RMSE_VolError<<endl;
		outfile<<setw(10)<<" RMSE(Normal distances errors): "<<"\t"<<RMSE_NDError<<endl;

		csvfile.precision(10);
		csvfile.setf(ios::scientific );
		csvfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
		csvfile<<setw(10)<<" RMSE(Volume errors): "<<","<<RMSE_VolError<<endl;
		csvfile<<setw(10)<<" RMSE(Normal distances errors): "<<","<<RMSE_NDError<<endl;
		
		outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		outfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
		outfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
		outfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		outfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<"\t";
		outfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<"\t";
		outfile<<setw(10)<<(Config.Rangeb)<<"\t";
		outfile<<setw(10)<<(Config.RangeS)<<"\t";
		outfile<<endl;
				

		csvfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		csvfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<",";
		csvfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<",";
		csvfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		csvfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<",";
		csvfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<",";
		csvfile<<setw(10)<<(Config.Rangeb)<<",";
		csvfile<<setw(10)<<(Config.RangeS)<<",";
		csvfile<<endl;
		
		outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<"\t";
		outfile<<endl<<endl;
		
		outfile.flush();

		csvfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<",";
		csvfile<<endl<<endl;

		csvfile<<setw(10)<<"[CorreMat]"<<endl;
		for(i=0; i<(int)CorreMat.GetRows(); i++)
		{
			for(j=0; j<(int)CorreMat.GetCols(); j++)
			{
				csvfile<<setw(10)<<CorreMat(i,j)<<",";
			}
			csvfile<<endl;
		}
		csvfile.flush();

		if(nIteration > 1)
		{
			if(fabs(old_rmse_ND-RMSE_NDError)<fabs(LIDAR_PRECISION*1.0e-4))
			{
				outfile<<endl<<endl<<"##################################################################"<<endl;
				outfile<<"Iteration Stop(fabs(old_rmse_ND-RMSE_NDError))): "<<fabs(old_rmse_ND-RMSE_NDError)<<endl;
				outfile<<"##################################################################"<<endl<<endl;
				bStop = false;
			}
		}

		old_sigma = sigma;
		old_rmse_ND = RMSE_NDError;

		if(nIteration >= max_iter)
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}

		if(bStop == false)
		{
			fstream newconfigfile;
			CString configpath = resultfile;
			configpath.MakeLower();
			configpath.Replace(".result",".config");
			newconfigfile.open(configpath,ios::out);
			newconfigfile <<"VER 1.2"<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Biases in spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Sigma of spatial offsets biases(3EA, M)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile.precision(10);
			newconfigfile.setf(ios::scientific );
			newconfigfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scan angle offsets(2EA, deg)"<<endl;
			newconfigfile <<"#Sigma of scan angle offset(2EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Alpha_b)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Beta_b)<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
			newconfigfile<<setw(10)<<(Config.Rangeb)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(8,8)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scale of laser ranging(1EA, M), Sigma of scale of laser ranging(1EA)"<<endl;
			newconfigfile<<setw(10)<<(Config.RangeS)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(9,9)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of GPS signal[3EA(X,Y,Z), M]"<<endl;
			newconfigfile<<setw(10)<<Config.sX<<"\t";
			newconfigfile<<setw(10)<<Config.sY<<"\t";
			newconfigfile<<setw(10)<<Config.sZ<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of INS signal[3EA(O,P,K), rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sO)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sP)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sK)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of swing angle[2EA, rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sAlpha)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sBeta)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"##sigma of laser ranging[1EA, M]"<<endl;
			newconfigfile<<setw(10)<<Config.sRange<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#ID"<<endl;
			newconfigfile <<Config.ID<<endl;
			newconfigfile.close();
		}
		
	}while(bStop == true);

	CBMPImage img;
	img.Create(Normal_Matrix.GetCols(), Normal_Matrix.GetRows(), 8);
	CBMPPixelPtr ptr(img);
	for(i=0; i<(int)Normal_Matrix.GetRows(); i++)
	{
		for(j=0; j<(int)Normal_Matrix.GetCols(); j++) 
		{
			if(Normal_Matrix(i,j)==0.) ptr[i][j] = BYTE(255);
			else ptr[i][j] = BYTE(0);
		}
	}
	
	CString nimgpath = resultfile;
	nimgpath.MakeLower();
	nimgpath.Replace(".result","_N.bmp");
	img.SaveBMP(nimgpath);
	
	outfile.close();
	csvfile.close();

	return true;
}

bool CLIDARBiasCalibration::SolveWithVolume_RNM_Brazil(double LIDAR_PRECISION, const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold)
{
	int i, j;

	//number of unknown parameters
	const int num_unknown = 10;//3 spatial offset, 3 rotational offset, 2 scan angle offset, range bias, range scale

	//Number of patches
	int num_Patches = PatchList.GetNumItem();

	//original patches
	OriginPatchList = PatchList;

	//Extract point ID
	CSMList<CString> VTXList;
	for(i=0; i<num_Patches; i++)
	{
		CLIDARPlanarPatch patch = PatchList.GetAt(i);
		
		int nSort;

		CString ID_A = patch.ID_A;
		CString ID_B = patch.ID_B;
		CString ID_C = patch.ID_C;
		nSort = FindCStringID(VTXList,ID_A);
		if(nSort == -1) VTXList.AddTail(ID_A);

		nSort = FindCStringID(VTXList,ID_B);
		if(nSort == -1) VTXList.AddTail(ID_B);

		nSort = FindCStringID(VTXList,ID_C);
		if(nSort == -1) VTXList.AddTail(ID_C);
	}
	
	//matrix for areas of triangles
	CSMMatrix<double> NMAT_dot, CMAT_dot, NMAT_dot_inv;
	CSMMatrix<double> Normal_Matrix;
	CSMMatrix<double> N2dot_point;
	CSMMatrix<double> N2dot_line;
	CSMMatrix<double> N2dot_tri;
	
	//result files
	fstream outfile;//txt file
	outfile.precision(10);
	fstream csvfile;//csv file

	CString csvpath = resultfile;
	csvpath.MakeLower();
	csvpath.Replace(".result",".csv");
	csvfile.open(csvpath, ios::out);
	
	CString resultname = resultfile;
	outfile.open(resultname,ios::out);

	bool bStop = true;
	int nIteration = 0;
	//number of observations
	const int num_obs = 9;
	double old_sigma;
	double old_rmse_ND;

	//Weight for parameters
	CSMMatrix<double> QParam(num_unknown,num_unknown,0.);
	QParam(0,0) = Config.sXb*Config.sXb;
	QParam(1,1) = Config.sYb*Config.sYb;
	QParam(2,2) = Config.sZb*Config.sZb;

	QParam(3,3) = Config.sOb*Config.sOb;
	QParam(4,4) = Config.sPb*Config.sPb;
	QParam(5,5) = Config.sKb*Config.sKb;
	
	QParam(6,6) = Config.sAlpha_b*Config.sAlpha_b;
	QParam(7,7) = Config.sBeta_b*Config.sBeta_b;

	QParam(8,8) = Config.sRangeb*Config.sRangeb;
	QParam(9,9) = Config.sRangeS*Config.sRangeS;
	
	//fixed unknown parameter list
	CSMMatrix<double> FixedParamIndex(1, num_unknown, 1.0);
	for(i=0; i<(int)num_unknown; i++)
	{
		if(QParam(i,i)<pow(Small_QThreshold,2))
		{
			FixedParamIndex(0,i) = 0.0;
		}
	}

	//weight for 9 observations
	CSMMatrix<double> qmat;
	qmat.Resize(num_obs,num_obs,0.);

	qmat(0,0) = Config.sX*Config.sX;
	qmat(1,1) = Config.sY*Config.sY;
	qmat(2,2) = Config.sZ*Config.sZ;

	qmat(3,3) = Config.sO*Config.sO;
	qmat(4,4) = Config.sP*Config.sP;
	qmat(5,5) = Config.sK*Config.sK;

	qmat(6,6) = Config.sAlpha*Config.sAlpha;
	qmat(7,7) = Config.sBeta*Config.sBeta;
	qmat(8,8) = Config.sRange*Config.sRange;
	
	CSMMatrix<double> AREA, area, jmat_vertexA, jmat_vertexB, jmat_vertexC;
	
	area.Resize(1,1,0.);
				
	jmat_vertexA.Resize(1,3,0.);
	jmat_vertexB.Resize(1,3,0.);
	jmat_vertexC.Resize(1,3,0.);
	
	CSMMatrix<double> kmat, jmat_lidar, bmat;
	kmat.Resize(1,1,0.);
	jmat_lidar.Resize(1,num_unknown,0.);
	bmat.Resize(1,num_obs,0.);
	
	do
	{
		//Increase iteration number
		nIteration ++;

		CLeastSquareLIDAR LS;
		LS.SetDataConfig(1,num_unknown,0,0, num_Patches);		
		
		//boresight vector (spatial offset + spatial bias)
		Pvector.Resize(3,1);
		Pvector(0,0) = Config.Xoffset + Config.Xb;
		Pvector(1,0) = Config.Yoffset + Config.Yb;
		Pvector(2,0) = Config.Zoffset + Config.Zb;
		
		//offset angle + bias angle
		dO = Config.Ooffset + Config.Ob;
		dP = Config.Poffset + Config.Pb;
		dK = Config.Koffset + Config.Kb;
		//INS Rotation matrix: R matrix
		Offset_R.ReMake(dO, dP, dK);
		
		//partial derivatives for angle biases (omega)
		dOffsetR_dO.Partial_dRdO(dO,dP,dK);
		//partial derivatives for angle biases (phi)
		dOffsetR_dP.Partial_dRdP(dO,dP,dK);
		//partial derivatives for angle biases (kappa)
		dOffsetR_dK.Partial_dRdK(dO,dP,dK);
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////
		
		outfile<<setw(10)<<"[K matrix (normal distances)]"<<endl;
		
		for(i=0; i<num_Patches; i++)
		{
			//To get patch
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			area(0,0) = patch.Area;
			AREA.Insert(AREA.GetRows(),0,area);
			
			int nSortA = FindCStringID(VTXList, patch.ID_A);
			int nSortB = FindCStringID(VTXList, patch.ID_B);
			int nSortC = FindCStringID(VTXList, patch.ID_C);
			
			int num_points = patch.PointList.GetNumItem();
			
			XA = patch.Vertex[0];	YA = patch.Vertex[1];	ZA = patch.Vertex[2];
			XB = patch.Vertex[3];	YB = patch.Vertex[4];	ZB = patch.Vertex[5];
			XC = patch.Vertex[6];	YC = patch.Vertex[7];	ZC = patch.Vertex[8];
			
			CSMMatrix<double> QVertexA(3,3,0.);
			QVertexA(0,0) = patch.SD[0]*patch.SD[0];
			QVertexA(1,1) = patch.SD[1]*patch.SD[1];
			QVertexA(2,2) = patch.SD[2]*patch.SD[2];
			
			CSMMatrix<double> QVertexB(3,3,0.);
			QVertexB(0,0) = patch.SD[3]*patch.SD[3];
			QVertexB(1,1) = patch.SD[4]*patch.SD[4];
			QVertexB(2,2) = patch.SD[5]*patch.SD[5];
			
			CSMMatrix<double> QVertexC(3,3,0.);
			QVertexC(0,0) = patch.SD[6]*patch.SD[6];
			QVertexC(1,1) = patch.SD[7]*patch.SD[7];
			QVertexC(2,2) = patch.SD[8]*patch.SD[8];

			CSMMatrix<double> FixedParamPoint(1,9,1.0);
			
			for(int index=0; index<(int)3;index++)
			{
				if(QVertexA(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index) = 0.0;
				}
				if(QVertexB(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index+3) = 0.0;
				}
				if(QVertexC(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index+6) = 0.0;
				}
			}
			
			//plane coefficient
			K1 = YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC - YC*ZB);
			K2 = -XA*(ZB-ZC) +ZA*(XB-XC) - (XB*ZC - XC*ZB);
			K3 = XA*(YB-YC) - YA*(XB-XC) + (XB*YC - XC*YB);
			K4 = -XA*(YB*ZC-YC*ZB) + YA*(XB*ZC-XC*ZB) -ZA* (XB*YC - XC*YB);

			//K1, K2, and K3
			K123.Resize(1,3);
			K123(0,0) = K1;
			K123(0,1) = K2;
			K123(0,2) = K3;

			for(j=0; j<num_points; j++)
			{
				CLIDARRawPoint point = patch.PointList.GetAt(j);
				
				Cal_PDsWithVolume_Brazil3(point, Config, patch, jmat_lidar,jmat_vertexA,jmat_vertexB,jmat_vertexC, bmat, kmat);

				outfile<<kmat(0,0)/patch.Area<<endl;

				CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
				we_temp = we_temp.Inverse();
  
				CSMMatrix<double> j_tri; j_tri.Resize(0,0);
				j_tri.Insert(0,0,jmat_vertexA); 
				j_tri.Insert(0,j_tri.GetCols(),jmat_vertexB);
				j_tri.Insert(0,j_tri.GetCols(),jmat_vertexC);
				//reduced normal matrix
				LS.Fill_Nmat_Data_LIDARPointwithDeterminent(FixedParamIndex, FixedParamPoint, jmat_lidar, j_tri, we_temp, kmat, i, area(0,0));
				//
			}//for(j=0; j<num_points; j++)

			//Parameter observations(vertex coord)
			CLIDARPlanarPatch oripatch = OriginPatchList.GetAt(i);
			CSMMatrix<double> jmat_vtx_param(9,9);
			jmat_vtx_param.Identity(1.0);
			CSMMatrix<double> k_vtx(9,1);
			CSMMatrix<double> WVTX(9,9);
			
			//vertex A
			k_vtx(0,0) = oripatch.Vertex[0] - patch.Vertex[0];
			k_vtx(1,0) = oripatch.Vertex[1] - patch.Vertex[1];
			k_vtx(2,0) = oripatch.Vertex[2] - patch.Vertex[2];

			WVTX.Insert(0,0,QVertexA.Inverse());

			//vertex B
			k_vtx(3,0) = oripatch.Vertex[3] - patch.Vertex[3];
			k_vtx(4,0) = oripatch.Vertex[4] - patch.Vertex[4];
			k_vtx(5,0) = oripatch.Vertex[5] - patch.Vertex[5];

			WVTX.Insert(3,3,QVertexB.Inverse());

			//vertex C
			k_vtx(6,0) = oripatch.Vertex[6] - patch.Vertex[6];
			k_vtx(7,0) = oripatch.Vertex[7] - patch.Vertex[7];
			k_vtx(8,0) = oripatch.Vertex[8] - patch.Vertex[8];

			WVTX.Insert(6,6,QVertexC.Inverse());

			LS.Fill_Nmat_TriangleVertex(jmat_vtx_param,WVTX,k_vtx,i);

			outfile<<k_vtx.matrixout()<<endl;

		}//for(i=0; i<num_Patches; i++)

	
		//Parameter observations(LIDAR param)
		CSMMatrix<double> WParam = QParam.Inverse();
		
		double* diff_param = new double[num_unknown];
		diff_param[0] = OriginConfig.Xb - Config.Xb;
		diff_param[1] = OriginConfig.Yb - Config.Yb;
		diff_param[2] = OriginConfig.Zb - Config.Zb;
		
		diff_param[3] = OriginConfig.Ob - Config.Ob;
		diff_param[4] = OriginConfig.Pb - Config.Pb;
		diff_param[5] = OriginConfig.Kb - Config.Kb;
		
		diff_param[6] = OriginConfig.Alpha_b - Config.Alpha_b;
		diff_param[7] = OriginConfig.Beta_b - Config.Beta_b;
		
		diff_param[8] = OriginConfig.Rangeb - Config.Rangeb;
		diff_param[9] = OriginConfig.RangeS - Config.RangeS;

		CSMMatrix<double> jmat_param(num_unknown,num_unknown,0.);
		jmat_param.Identity(1.0);
		CSMMatrix<double> jmat_lidar_k(num_unknown,1,0.);

		jmat_lidar_k(0,0) = OriginConfig.Xb - Config.Xb;
		jmat_lidar_k(1,0) = OriginConfig.Yb - Config.Yb;
		jmat_lidar_k(2,0) = OriginConfig.Zb - Config.Zb;
		
		jmat_lidar_k(3,0) = OriginConfig.Ob - Config.Ob;
		jmat_lidar_k(4,0) = OriginConfig.Pb - Config.Pb;
		jmat_lidar_k(5,0) = OriginConfig.Kb - Config.Kb;
		
		jmat_lidar_k(6,0) = OriginConfig.Alpha_b - Config.Alpha_b;
		jmat_lidar_k(7,0) = OriginConfig.Beta_b - Config.Beta_b;
		
		jmat_lidar_k(8,0) = OriginConfig.Rangeb - Config.Rangeb;
		jmat_lidar_k(9,0) = OriginConfig.RangeS - Config.RangeS;

		for(int param_index=0; param_index<num_unknown; param_index++)
		{
			if(sqrt(QParam(param_index,param_index)) <= fabs(Small_QThreshold))
			{
				jmat_lidar_k(param_index, 0) = 0.0; 
			}					
		}
		
		LS.Fill_Nmat_LIDARParam(jmat_param,WParam,jmat_lidar_k);

		outfile<<jmat_lidar_k.matrixout()<<endl;
		
		double var;
		CSMMatrix<double> XMAT = LS.RunLeastSquare_RN(var, NMAT_dot, CMAT_dot);
		NMAT_dot_inv = NMAT_dot.Inverse();

		// 2007.05.09 Normal_Matrix = LS.GetNmatrix();
		Normal_Matrix = LS.GetNmatrix(N2dot_point, N2dot_line, N2dot_tri);

		outfile<<setw(10)<<"[NMAT_dot]"<<endl;
		for(i=0; i<(int)NMAT_dot.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT_dot.GetCols(); j++)
				outfile<<setw(10)<<NMAT_dot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[CMAT_dot]"<<endl;
		for(i=0; i<(int)CMAT_dot.GetRows(); i++)
		{
			for(j=0; j<(int)CMAT_dot.GetCols(); j++)
				outfile<<setw(10)<<CMAT_dot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		
		CSMMatrix<double> CorreMat;
		CorreMat = Correlation(NMAT_dot_inv);
		outfile<<setw(10)<<"[CorreMat]"<<endl;
		for(i=0; i<(int)CorreMat.GetRows(); i++)
		{
			for(j=0; j<(int)CorreMat.GetCols(); j++)
				outfile<<setw(10)<<CorreMat(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		
		CBMPImage img2;
		img2.Create(CorreMat.GetCols(),CorreMat.GetRows(),8);
		CBMPPixelPtr ptr2(img2);
		
		for(i=0; i<(int)CorreMat.GetRows(); i++)
		{
			for(j=0; j<(int)CorreMat.GetCols(); j++)
			{
				if(fabs(CorreMat(i,j))>0.9) 
				{
					double temp = CorreMat(i,j)-0.9;
					ptr2[i][j] = BYTE(fabs(temp)/0.1*255);
				}
				else ptr2[i][j] = BYTE(0);
			}
		}
		
		CString correpath = resultfile;
		correpath.MakeLower();
		correpath.Replace(".result","_corre.bmp");
		img2.SaveBMP(correpath);

		outfile.precision(10);
		outfile.setf(ios::scientific);
		
		//*
		outfile<<setw(10)<<"[NMAT_dot_inv]"<<endl;
		for(i=0; i<(int)NMAT_dot_inv.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT_dot_inv.GetCols(); j++)
				outfile<<setw(10)<<NMAT_dot_inv(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile<<setw(10)<<XMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		//*/

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//		for(i=0; i<(int)num_unknown; i++)
		//		{
		//			if(QParam(i,i)<pow(QThreshold*0.01,2))
		//			{
		//				XMAT(i,0) = 0.0;
		//			}
		//		}

		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);

		Config.Alpha_b += XMAT(6,0);
		Config.Beta_b  += XMAT(7,0);

		Config.Rangeb += XMAT(8,0);
		Config.RangeS += XMAT(9,0);

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		for(i=0; i<(int)num_Patches; i++)
		{
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			
			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_A);
				
				//				if((patch.SD[j]*patch.SD[j])<QThreshold)
				//				{
				//					if(nSort != -1) XMAT(num_unknown+nSort*3+j,0) = 0.0;
				//				}
				if(nSort != -1) patch.Vertex[j] += XMAT(num_unknown+nSort*3+j,0);
			}

			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_B);
				//				if((patch.SD[j+3]*patch.SD[j+3])<QThreshold)
				//				{
				//					if(nSort != -1) XMAT(num_unknown+nSort*3+j,0) = 0.0;
				//				}
				if(nSort != -1) patch.Vertex[j+3] += XMAT(num_unknown+nSort*3+j,0);
			}

			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_C);
				//				if((patch.SD[j+6]*patch.SD[j+6])<QThreshold)
				//				{					
				//					if(nSort != -1) XMAT(num_unknown+nSort*3+j,0) = 0.0;
				//				}
				if(nSort != -1) patch.Vertex[j+6] += XMAT(num_unknown+nSort*3+j,0);
			}
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		CSMList<MatrixData> VolError, NDError; 
		CSMList<MatrixData> V;
		LS.GetLIDARErrorMatwithDeterminent(V, VolError, NDError);

		int n_mat = VolError.GetNumItem();
		
		DATA<MatrixData> *pos1 = NULL;
		DATA<MatrixData> *pos2 = NULL;
		
		double sum_EVol = 0;
		double sum_END = 0;
		int count=0;
		
		for(i=0; i<n_mat; i++)
		{
			pos1 = VolError.GetNextPos(pos1);
			pos2 = NDError.GetNextPos(pos2);
			
			int nrows = pos1->value.mat.GetRows();
			
			for(j=0; j<nrows; j++)
			{
				double temp = pos1->value.mat(j,0);
				temp = temp*temp;
				sum_EVol += temp;

				temp = pos2->value.mat(j,0);
				temp = temp*temp;
				sum_END += temp;

				count++;
			}
		}

		//2007.05.09
//		CSMMatrix<double> Sum_VolError2 = VolError.Transpose()%VolError;
//		double Mean_VolError2 = Sum_VolError2(0,0)/VolError.GetRows();
//		double RMSE_VolError = sqrt(Mean_VolError2);

//		CSMMatrix<double> Sum_NDError2 = NDError.Transpose()%NDError;
//		double Mean_NDError2 = Sum_NDError2(0,0)/NDError.GetRows();
//		double RMSE_NDError = sqrt(Mean_NDError2);

		double RMSE_VolError = sqrt(sum_EVol/count);

		double RMSE_NDError = sqrt(sum_END/count);

		double sigma = sqrt(var);
		
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<"\t"<<sigma<<endl;
		outfile<<setw(10)<<" RMSE(Volume errors): "<<"\t"<<RMSE_VolError<<endl;
		outfile<<setw(10)<<" RMSE(Normal distances errors): "<<"\t"<<RMSE_NDError<<endl;

		csvfile.precision(10);
		csvfile.setf(ios::scientific );
		csvfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
		csvfile<<setw(10)<<" RMSE(Volume errors): "<<","<<RMSE_VolError<<endl;
		csvfile<<setw(10)<<" RMSE(Normal distances errors): "<<","<<RMSE_NDError<<endl;
		
		outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		outfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
		outfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
		outfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		outfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<"\t";
		outfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<"\t";
		outfile<<setw(10)<<(Config.Rangeb)<<"\t";
		outfile<<setw(10)<<(Config.RangeS)<<"\t";
		outfile<<endl;
				

		csvfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		csvfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<",";
		csvfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<",";
		csvfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		csvfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<",";
		csvfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<",";
		csvfile<<setw(10)<<(Config.Rangeb)<<",";
		csvfile<<setw(10)<<(Config.RangeS)<<",";
		csvfile<<endl;
		
		outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<"\t";
		outfile<<endl<<endl;
		
		outfile.flush();

		csvfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<",";
		csvfile<<endl<<endl;

		csvfile<<setw(10)<<"[CorreMat]"<<endl;
		for(i=0; i<(int)CorreMat.GetRows(); i++)
		{
			for(j=0; j<(int)CorreMat.GetCols(); j++)
			{
				csvfile<<setw(10)<<CorreMat(i,j)<<",";
			}
			csvfile<<endl;
		}
		csvfile.flush();

		old_sigma = sigma;
		old_rmse_ND = RMSE_NDError;

		if(nIteration > 1)
		{
			if(fabs(old_rmse_ND-RMSE_NDError)<fabs(LIDAR_PRECISION*1.0e-4))
			{
				outfile<<endl<<endl<<"##################################################################"<<endl;
				outfile<<"Iteration Stop(fabs(old_rmse_ND-RMSE_NDError))): "<<fabs(old_rmse_ND-RMSE_NDError)<<endl;
				outfile<<"##################################################################"<<endl<<endl;
				bStop = false;
			}
		}

		old_sigma = sigma;
		old_rmse_ND = RMSE_NDError;

		if(nIteration >= max_iter)
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}

		if(bStop == false)
		{
			fstream newconfigfile;
			CString configpath = resultfile;
			configpath.MakeLower();
			configpath.Replace(".result",".config");
			newconfigfile.open(configpath,ios::out);
			newconfigfile <<"VER 1.2"<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Biases in spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Sigma of spatial offsets biases(3EA, M)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile.precision(10);
			newconfigfile.setf(ios::scientific );
			newconfigfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scan angle offsets(2EA, deg)"<<endl;
			newconfigfile <<"#Sigma of scan angle offset(2EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Alpha_b)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Beta_b)<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
			newconfigfile<<setw(10)<<(Config.Rangeb)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(8,8)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scale of laser ranging(1EA, M), Sigma of scale of laser ranging(1EA)"<<endl;
			newconfigfile<<setw(10)<<(Config.RangeS)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(9,9)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of GPS signal[3EA(X,Y,Z), M]"<<endl;
			newconfigfile<<setw(10)<<Config.sX<<"\t";
			newconfigfile<<setw(10)<<Config.sY<<"\t";
			newconfigfile<<setw(10)<<Config.sZ<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of INS signal[3EA(O,P,K), rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sO)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sP)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sK)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of swing angle[2EA, rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sAlpha)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sBeta)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"##sigma of laser ranging[1EA, M]"<<endl;
			newconfigfile<<setw(10)<<Config.sRange<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#ID"<<endl;
			newconfigfile <<Config.ID<<endl;
			newconfigfile.close();
		}
		
	}while(bStop == true);

	CBMPImage img;
	img.Create(Normal_Matrix.GetCols(), Normal_Matrix.GetRows(), 8);
	CBMPPixelPtr ptr(img);
	for(i=0; i<(int)Normal_Matrix.GetRows(); i++)
	{
		for(j=0; j<(int)Normal_Matrix.GetCols(); j++) 
		{
			if(Normal_Matrix(i,j)==0.) ptr[i][j] = BYTE(255);
			else ptr[i][j] = BYTE(0);
		}
	}
	
	CString nimgpath = resultfile;
	nimgpath.MakeLower();
	nimgpath.Replace(".result","_N.bmp");
	img.SaveBMP(nimgpath);
	
	outfile.close();
	csvfile.close();

	return true;
}

bool CLIDARBiasCalibration::SolveWithPoint_RNM_Titan(double LIDAR_PRECISION, const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold)
{
	int i, j;

	//number of unknown parameters
	const int num_unknown = 10;//3 spatial offset, 3 rotational offset, 2 scan angle offset, range bias, range scale

	//Number of patches
	int num_Points = ControlPoints.GetNumItem();

	//original patches
	OriginalControlPoints = ControlPoints;

	//matrix for areas of triangles
	CSMMatrix<double> NMAT_dot, CMAT_dot, NMAT_dot_inv;
	CSMMatrix<double> N2dot_point;
	CSMMatrix<double> N2dot_line;
	CSMMatrix<double> N2dot_tri;
	
	//result files
	fstream outfile;//txt file
	outfile.precision(10);
	fstream outfile_mat;//txt file
	outfile_mat.precision(10);
	fstream csvfile;//csv file

	CString csvpath = resultfile;
	csvpath.MakeLower();
	csvpath.Replace(".result",".csv");
	csvfile.open(csvpath, ios::out);
	
	CString resultname = resultfile;
	outfile.open(resultname,ios::out);
	outfile.setf(ios::fixed, ios::floatfield);

	CString resultname_mat = resultfile;
	resultname_mat.MakeLower(); resultname_mat.Replace(".result",".mat");
	outfile_mat.open(resultname_mat,ios::out);
	outfile_mat.setf(ios::fixed, ios::floatfield);

	bool bStop = true;
	int nIteration = 0;
	//number of observations
	const int num_obs = 9;
	double old_sigma;

	//Weight for parameters
	CSMMatrix<double> QParam(num_unknown,num_unknown,0.);
	QParam(0,0) = Config.sXb*Config.sXb;
	QParam(1,1) = Config.sYb*Config.sYb;
	QParam(2,2) = Config.sZb*Config.sZb;

	QParam(3,3) = Config.sOb*Config.sOb;
	QParam(4,4) = Config.sPb*Config.sPb;
	QParam(5,5) = Config.sKb*Config.sKb;
	
	QParam(6,6) = Config.sAlpha_b*Config.sAlpha_b;
	QParam(7,7) = Config.sBeta_b*Config.sBeta_b;

	QParam(8,8) = Config.sRangeb*Config.sRangeb;
	QParam(9,9) = Config.sRangeS*Config.sRangeS;
	
	//fixed unknown parameter list
	CSMMatrix<double> FixedParamIndex(1, num_unknown, 1.0);
	for(i=0; i<(int)num_unknown; i++)
	{
		if(QParam(i,i)<pow(Small_QThreshold,2))
		{
			FixedParamIndex(0,i) = 0.0;
		}
	}

	//weight for 9 observations
	CSMMatrix<double> qmat;
	qmat.Resize(num_obs,num_obs,0.);

	qmat(0,0) = Config.sX*Config.sX;
	qmat(1,1) = Config.sY*Config.sY;
	qmat(2,2) = Config.sZ*Config.sZ;

	qmat(3,3) = Config.sO*Config.sO;
	qmat(4,4) = Config.sP*Config.sP;
	qmat(5,5) = Config.sK*Config.sK;

	qmat(6,6) = Config.sAlpha*Config.sAlpha;
	qmat(7,7) = Config.sBeta*Config.sBeta;
	qmat(8,8) = Config.sRange*Config.sRange;

	CSMMatrix<double> kmat, jmat_lidar, jmat_Point, bmat;
	kmat.Resize(3,1,0.);
	jmat_lidar.Resize(3,num_unknown,0.);
	jmat_Point.Resize(3,3,0.);
	bmat.Resize(3,num_obs,0.);
	
	DATA<CLIDARRawPoint> *rawpoint_pos = NULL;
	DATA<SM3DPoint> *point_pos = NULL;
	DATA<SM3DPoint> *oripoint_pos = NULL;
	
	
	CLeastSquareLIDAR LS;
	
	do
	{	//Increase iteration number from 0
		nIteration ++;
		
		outfile<<"Iteration "<<nIteration<<endl;
		outfile<<"-------------------------------------"<<endl;

		outfile_mat<<"Iteration "<<nIteration<<endl;
		outfile_mat<<"-------------------------------------"<<endl;
		
		LS.SetDataConfig(1, num_unknown, num_Points, 0, 0);		
		
		//boresight vector (spatial offset + spatial bias)
		Pvector.Resize(3,1);
		Pvector(0,0) = Config.Xoffset + Config.Xb;
		Pvector(1,0) = Config.Yoffset + Config.Yb;
		Pvector(2,0) = Config.Zoffset + Config.Zb;
		
		//offset angle + bias angle
		dO = Config.Ooffset + Config.Ob;
		dP = Config.Poffset + Config.Pb;
		dK = Config.Koffset + Config.Kb;
		
		//INS Rotation matrix: R matrix
		_Rmat_ Rmat	(0,		dP,		0);//rotation matrix (roll)
		_Rmat_ Pmat	(dO,	0,		0);//rotation matrix (pitch)
		_Rmat_ Ymat	(0,		0,		dK);//rotation matrix (yaw)

		Offset_R.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;
		
		//partial derivatives for angle biases (omega)
		dOffsetR_dO.Partial_dRdO(dO,0,0);
		dOffsetR_dO.Rmatrix = Ymat.Rmatrix%dOffsetR_dO.Rmatrix%Rmat.Rmatrix;

		//partial derivatives for angle biases (phi)
		dOffsetR_dP.Partial_dRdP(0,dP,0);
		dOffsetR_dP.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%dOffsetR_dP.Rmatrix;

		//partial derivatives for angle biases (kappa)
		dOffsetR_dK.Partial_dRdK(0,0,dK);
		dOffsetR_dK.Rmatrix = dOffsetR_dK.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////
		
		rawpoint_pos = NULL;
		point_pos = NULL;
		oripoint_pos = NULL;
		for(i=0; i<num_Points; i++)
		{
			//To get control point
			//SM3DPoint point = ControlPoints.GetAt(i);

			//To get LIDAR raw point
			//CLIDARRawPoint rawpoint = RawPointList.GetAt(i);

			//To get original control point
			//SM3DPoint oripoint = OriginalControlPoints.GetAt(i);

			point_pos = ControlPoints.GetNextPos(point_pos);
			rawpoint_pos = RawPointList.GetNextPos(rawpoint_pos);
			oripoint_pos = OriginalControlPoints.GetNextPos(oripoint_pos);

			SM3DPoint point = point_pos->value;
			SM3DPoint oripoint = oripoint_pos->value;
			CLIDARRawPoint rawpoint = rawpoint_pos->value;
			
			CSMMatrix<double> QPoint(3,3,0.);
			QPoint(0,0) = point.cov[0];
			QPoint(0,1) = point.cov[1];
			QPoint(0,2) = point.cov[2];

			QPoint(1,0) = point.cov[3];
			QPoint(1,1) = point.cov[4];
			QPoint(1,2) = point.cov[5];

			QPoint(2,0) = point.cov[6];
			QPoint(2,1) = point.cov[7];
			QPoint(2,2) = point.cov[8];

			CSMMatrix<double> FixedParamPoint(1,3,1.0);
			
			for(int index=0; index<(int)3;index++)
			{
				if(QPoint(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index) = 0.0;
				}
			}
			
			Cal_PDsWithPoint_Titan(point, rawpoint, Config, jmat_lidar,jmat_Point, bmat, kmat);
			
 			outfile_mat<<kmat.matrixout()<<endl;
			
			CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
			we_temp = we_temp.Inverse();
			
			//reduced normal matrix
			LS.Fill_Nmat_Data_LIDARPointwithPoint(FixedParamIndex, FixedParamPoint, jmat_lidar, jmat_Point, we_temp, kmat, i);
			//
			
			//Parameter observations(vertex coord)
			CSMMatrix<double> jmat_vtx_param(3,3);
			jmat_vtx_param.Identity(1.0);
			CSMMatrix<double> k_vtx(3,1);
			CSMMatrix<double> WVTX(3,3);
			
			k_vtx(0,0) = oripoint.X - point.X;
			k_vtx(1,0) = oripoint.Y - point.Y;
			k_vtx(2,0) = oripoint.Z - point.Z;

			WVTX = QPoint.Inverse();

			LS.Fill_Nmat_Data_XYZ(jmat_vtx_param,WVTX,k_vtx,i);

 			outfile_mat<<k_vtx.matrixout()<<endl;

		}//for(i=0; i<num_Points; i++)

	
		//Parameter observations(LIDAR param)
		CSMMatrix<double> WParam = QParam.Inverse();
		
		CSMMatrix<double> jmat_param(num_unknown,num_unknown,0.);
		jmat_param.Identity(1.0);
		CSMMatrix<double> jmat_lidar_k(num_unknown,1,0.);

		jmat_lidar_k(0,0) = OriginConfig.Xb - Config.Xb;
		jmat_lidar_k(1,0) = OriginConfig.Yb - Config.Yb;
		jmat_lidar_k(2,0) = OriginConfig.Zb - Config.Zb;
		
		jmat_lidar_k(3,0) = OriginConfig.Ob - Config.Ob;
		jmat_lidar_k(4,0) = OriginConfig.Pb - Config.Pb;
		jmat_lidar_k(5,0) = OriginConfig.Kb - Config.Kb;
		
		jmat_lidar_k(6,0) = OriginConfig.Alpha_b - Config.Alpha_b;
		jmat_lidar_k(7,0) = OriginConfig.Beta_b - Config.Beta_b;
		
		jmat_lidar_k(8,0) = OriginConfig.Rangeb - Config.Rangeb;
		jmat_lidar_k(9,0) = OriginConfig.RangeS - Config.RangeS;

		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!              !!!!!!!!!!!!!!!!!!!!!!!!!!!
		////////////////////////////////////////////////////////////////////////////
		//
		//Check this part in different functions
		//
		//////////////
		for(int param_index=0; param_index<num_unknown; param_index++)
		{
			if(sqrt(QParam(param_index,param_index)) <= fabs(Small_QThreshold))
			{
				jmat_lidar_k(param_index, 0) = 0.0; 
			}					
		}

		
		LS.Fill_Nmat_LIDARParam(jmat_param,WParam,jmat_lidar_k);

// 		outfile<<jmat_lidar_k.matrixout()<<endl;
		
		double var;
		CSMMatrix<double> XMAT = LS.RunLeastSquare_RN(var, NMAT_dot, CMAT_dot);
		NMAT_dot_inv = NMAT_dot.Inverse();

		MatrixOut(outfile_mat, LS);
		
		outfile.precision(10);
		outfile.setf(ios::scientific);
		
		outfile<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile<<setw(10)<<XMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);

		Config.Alpha_b += XMAT(6,0);
		Config.Beta_b  += XMAT(7,0);

		Config.Rangeb += XMAT(8,0);
		Config.RangeS += XMAT(9,0);

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		point_pos = NULL;
		for(i=0; i<(int)num_Points; i++)
		{
			point_pos = ControlPoints.GetNextPos(point_pos);

			point_pos->value.X += XMAT(num_unknown+i*3+0,0);
			point_pos->value.Y += XMAT(num_unknown+i*3+1,0);
			point_pos->value.Z += XMAT(num_unknown+i*3+2,0);
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		double sigma = sqrt(var);
		
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<"\t"<<sigma<<endl;

		csvfile.precision(10);
		csvfile.setf(ios::scientific );
		csvfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
		
		outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		outfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
		outfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
		outfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		outfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<"\t";
		outfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<"\t";
		outfile<<setw(10)<<(Config.Rangeb)<<"\t";
		outfile<<setw(10)<<(Config.RangeS)<<"\t";
		outfile<<endl;
				

		csvfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		csvfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<",";
		csvfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<",";
		csvfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		csvfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<",";
		csvfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<",";
		csvfile<<setw(10)<<(Config.Rangeb)<<",";
		csvfile<<setw(10)<<(Config.RangeS)<<",";
		csvfile<<endl;
		
		outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<"\t";
		outfile<<endl<<endl;
		
		outfile.flush();

		csvfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<",";
		csvfile<<endl<<endl;

		if(nIteration > 1)
		{
			if(fabs(old_sigma-sigma)<fabs(1.0e-10))
			{
				outfile<<endl<<endl<<"##################################################################"<<endl;
				outfile<<"Iteration Stop(fabs(old_sigma-sigma)): "<<fabs(old_sigma - sigma)<<endl;
				outfile<<"##################################################################"<<endl<<endl;
				bStop = false;
			}
		}

		old_sigma = sigma;

		if(nIteration >= max_iter)
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}

		if(bStop == false)
		{
			fstream newconfigfile;
			CString configpath = resultfile;
			configpath.MakeLower();
			configpath.Replace(".result",".config");
			newconfigfile.open(configpath,ios::out);
			newconfigfile <<"VER 1.2"<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Biases in spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Sigma of spatial offsets biases(3EA, M)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile.precision(10);
			newconfigfile.setf(ios::scientific );
			newconfigfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scan angle offsets(2EA, deg)"<<endl;
			newconfigfile <<"#Sigma of scan angle offset(2EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Alpha_b)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Beta_b)<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
			newconfigfile<<setw(10)<<(Config.Rangeb)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(8,8)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scale of laser ranging(1EA, M), Sigma of scale of laser ranging(1EA)"<<endl;
			newconfigfile<<setw(10)<<(Config.RangeS)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(9,9)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of GPS signal[3EA(X,Y,Z), M]"<<endl;
			newconfigfile<<setw(10)<<Config.sX<<"\t";
			newconfigfile<<setw(10)<<Config.sY<<"\t";
			newconfigfile<<setw(10)<<Config.sZ<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of INS signal[3EA(O,P,K), rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sO)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sP)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sK)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of swing angle[2EA, rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sAlpha)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sBeta)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"##sigma of laser ranging[1EA, M]"<<endl;
			newconfigfile<<setw(10)<<Config.sRange<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#ID"<<endl;
			newconfigfile <<Config.ID<<endl;
			newconfigfile.close();
		}

		outfile<<"[Coordinates]"<<endl;

		outfile.setf(ios::fixed, ios::floatfield);
		outfile.precision(4);

		point_pos = NULL;
		oripoint_pos = NULL;
		for(i=0; i<(int)num_Points; i++)
		{
			point_pos = ControlPoints.GetNextPos(point_pos);
			SM3DPoint point = point_pos->value;
			//SM3DPoint point = ControlPoints.GetAt(i);
			
			outfile<<setw(15)<<point.X<<"\t";
			outfile<<setw(15)<<point.Y<<"\t";
			outfile<<setw(15)<<point.Z<<"\t";
			
			oripoint_pos = OriginalControlPoints.GetNextPos(oripoint_pos);
			SM3DPoint oldpoint = oripoint_pos->value;
			//SM3DPoint oldpoint = OriginalControlPoints.GetAt(i);
			
			outfile<<setw(15)<<oldpoint.X<<"\t";
			outfile<<setw(15)<<oldpoint.Y<<"\t";
			outfile<<setw(15)<<oldpoint.Z<<endl;
		}

		outfile<<endl<<endl;
		
	}while(bStop == true);

	CString nimgpath = resultfile;
	nimgpath.MakeLower();
	nimgpath.Replace(".result",".bmp");

	if(false == LS.MakeMatrixImg(0.9, nimgpath))
	{
		outfile<<endl<<endl<<"---------------------------------------"<<endl;
		outfile<<"Image Generation Fail: Size is too big"<<endl;
		outfile<<"---------------------------------------"<<endl;
	}

	CSMMatrix<double> CorreMat;
	CorreMat = Correlation(NMAT_dot_inv);
	outfile<<setw(10)<<"[CorreMat]"<<endl;
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
			outfile<<setw(10)<<CorreMat(i,j)<<"\t";
		outfile<<endl;
	}
	outfile.flush();
	
	outfile.setf(ios::scientific);
	
	//
	///////////////////////////////////////////////////////////////////////
	
	outfile.close();
	outfile_mat.close();
	csvfile.close();

	return true;
}

bool CLIDARBiasCalibration::SolveWithVolume_RNM_Titan(double LIDAR_PRECISION, const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold)
{
	int i, j;

	//number of unknown parameters
	const int num_unknown = 10;//3 spatial offset, 3 rotational offset, 2 scan angle offset, range bias, range scale

	//Number of patches
	int num_Patches = PatchList.GetNumItem();

	//original patches
	OriginPatchList = PatchList;

	DATA<CLIDARRawPoint> *rawpoint_pos = NULL;
	DATA<CLIDARPlanarPatch> *patch_pos = NULL;
	DATA<CLIDARPlanarPatch> *oripatch_pos = NULL;

	//Extract point ID
	CSMList<CString> VTXList;
	for(i=0; i<num_Patches; i++)
	{
		patch_pos = PatchList.GetNextPos(patch_pos);
		CLIDARPlanarPatch patch = patch_pos->value;
		//CLIDARPlanarPatch patch = PatchList.GetAt(i);
		
		int nSort;

		CString ID_A = patch.ID_A;
		CString ID_B = patch.ID_B;
		CString ID_C = patch.ID_C;
		nSort = FindCStringID(VTXList,ID_A);
		if(nSort == -1) VTXList.AddTail(ID_A);

		nSort = FindCStringID(VTXList,ID_B);
		if(nSort == -1) VTXList.AddTail(ID_B);

		nSort = FindCStringID(VTXList,ID_C);
		if(nSort == -1) VTXList.AddTail(ID_C);
	}
	
	CSMMatrix<double> NMAT_dot, CMAT_dot, NMAT_dot_inv;
	CSMMatrix<double> N2dot_point;
	CSMMatrix<double> N2dot_line;
	CSMMatrix<double> N2dot_tri;
		
	//result files
	fstream outfile;//txt file
	outfile.precision(10);
	fstream csvfile;//csv file

	CString csvpath = resultfile;
	csvpath.MakeLower();
	csvpath.Replace(".result",".csv");
	csvfile.open(csvpath, ios::out);
	
	CString resultname = resultfile;
	outfile.open(resultname,ios::out);

	bool bStop = true;
	int nIteration = 0;
	//number of observations
	const int num_obs = 9;
	double old_sigma;
	double old_rmse_ND;

	//Weight for parameters
	CSMMatrix<double> QParam(num_unknown,num_unknown,0.);
	QParam(0,0) = Config.sXb*Config.sXb;
	QParam(1,1) = Config.sYb*Config.sYb;
	QParam(2,2) = Config.sZb*Config.sZb;

	QParam(3,3) = Config.sOb*Config.sOb;
	QParam(4,4) = Config.sPb*Config.sPb;
	QParam(5,5) = Config.sKb*Config.sKb;
	
	QParam(6,6) = Config.sAlpha_b*Config.sAlpha_b;
	QParam(7,7) = Config.sBeta_b*Config.sBeta_b;

	QParam(8,8) = Config.sRangeb*Config.sRangeb;
	QParam(9,9) = Config.sRangeS*Config.sRangeS;

	//fixed unknown parameters list	
	CSMMatrix<double> FixedParamIndex(1, num_unknown, 1.0);
	for(i=0; i<(int)num_unknown; i++)
	{
		if(QParam(i,i)<pow(Small_QThreshold,2))
		{
			FixedParamIndex(0,i) = 0.0;
		}
	}

	CSMMatrix<double> qmat;
	qmat.Resize(num_obs,num_obs,0.);

	qmat(0,0) = Config.sX*Config.sX;
	qmat(1,1) = Config.sY*Config.sY;
	qmat(2,2) = Config.sZ*Config.sZ;

	qmat(3,3) = Config.sO*Config.sO;
	qmat(4,4) = Config.sP*Config.sP;
	qmat(5,5) = Config.sK*Config.sK;

	qmat(6,6) = Config.sAlpha*Config.sAlpha;
	qmat(7,7) = Config.sBeta*Config.sBeta;
	qmat(8,8) = Config.sRange*Config.sRange;

	CSMMatrix<double> AREA, area, jmat_vertexA, jmat_vertexB, jmat_vertexC;

	area.Resize(1,1,0.);
	
	jmat_vertexA.Resize(1,3,0.);
	jmat_vertexB.Resize(1,3,0.);
	jmat_vertexC.Resize(1,3,0.);
	
	CSMMatrix<double> kmat, jmat_lidar, bmat;
	kmat.Resize(1,1,0.);
	jmat_lidar.Resize(1,num_unknown,0.);
	bmat.Resize(1,num_obs,0.);

	CLeastSquareLIDAR LS;
	
	do
	{
		//Increase iteration number
		nIteration ++;

		outfile<<"Iteration "<<nIteration<<endl;
		outfile<<"-------------------------------------"<<endl;

		LS.SetDataConfig(1,num_unknown,0,0, num_Patches);
		
		Pvector.Resize(3,1);
		//boresight vector (spatial offset + spatial bias)
		Pvector(0,0) = Config.Xoffset + Config.Xb;
		Pvector(1,0) = Config.Yoffset + Config.Yb;
		Pvector(2,0) = Config.Zoffset + Config.Zb;
		
		//offset angle + bias angle
		dO = Config.Ooffset + Config.Ob;
		dP = Config.Poffset + Config.Pb;
		dK = Config.Koffset + Config.Kb;
		
		//INS Rotation matrix: R matrix
		_Rmat_ Rmat	(0,		dP,		0);//rotation matrix (roll)
		_Rmat_ Pmat	(dO,	0,		0);//rotation matrix (pitch)
		_Rmat_ Ymat	(0,		0,		dK);//rotation matrix (yaw)

		Offset_R.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;
		
		//partial derivatives for angle biases (omega)
		dOffsetR_dO.Partial_dRdO(dO,0,0);
		dOffsetR_dO.Rmatrix = Ymat.Rmatrix%dOffsetR_dO.Rmatrix%Rmat.Rmatrix;

		//partial derivatives for angle biases (phi)
		dOffsetR_dP.Partial_dRdP(0,dP,0);
		dOffsetR_dP.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%dOffsetR_dP.Rmatrix;

		//partial derivatives for angle biases (kappa)
		dOffsetR_dK.Partial_dRdK(0,0,dK);
		dOffsetR_dK.Rmatrix = dOffsetR_dK.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////

		outfile<<setw(10)<<"[K matrix (normal distances)]"<<endl;
		
		patch_pos = NULL;
		oripatch_pos = NULL;
		for(i=0; i<num_Patches; i++)
		{
			//To get patch
			patch_pos = PatchList.GetNextPos(patch_pos);
			CLIDARPlanarPatch patch = patch_pos->value;
			//CLIDARPlanarPatch patch = PatchList.GetAt(i);
			
			area(0,0) = patch.Area;
			AREA.Insert(AREA.GetRows(),0,area);
			
			int nSortA = FindCStringID(VTXList, patch.ID_A);
			int nSortB = FindCStringID(VTXList, patch.ID_B);
			int nSortC = FindCStringID(VTXList, patch.ID_C);
			
			int num_points = patch.PointList.GetNumItem();
			
			XA = patch.Vertex[0];	YA = patch.Vertex[1];	ZA = patch.Vertex[2];
			XB = patch.Vertex[3];	YB = patch.Vertex[4];	ZB = patch.Vertex[5];
			XC = patch.Vertex[6];	YC = patch.Vertex[7];	ZC = patch.Vertex[8];
			
			CSMMatrix<double> QVertexA(3,3,0.);
			QVertexA(0,0) = patch.SD[0]*patch.SD[0];
			QVertexA(1,1) = patch.SD[1]*patch.SD[1];
			QVertexA(2,2) = patch.SD[2]*patch.SD[2];
			
			CSMMatrix<double> QVertexB(3,3,0.);
			QVertexB(0,0) = patch.SD[3]*patch.SD[3];
			QVertexB(1,1) = patch.SD[4]*patch.SD[4];
			QVertexB(2,2) = patch.SD[5]*patch.SD[5];
			
			CSMMatrix<double> QVertexC(3,3,0.);
			QVertexC(0,0) = patch.SD[6]*patch.SD[6];
			QVertexC(1,1) = patch.SD[7]*patch.SD[7];
			QVertexC(2,2) = patch.SD[8]*patch.SD[8];

			CSMMatrix<double> FixedParamPoint(1,9,1.0);
			
			for(int index=0; index<(int)3;index++)
			{
				if(QVertexA(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index) = 0.0;
				}
				if(QVertexB(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index+3) = 0.0;
				}
				if(QVertexC(index,index)<pow(Small_QThreshold,2))
				{
					FixedParamPoint(0,index+6) = 0.0;
				}
			}
			
			//plane coefficient
			K1 = YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC - YC*ZB);
			K2 = -XA*(ZB-ZC) +ZA*(XB-XC) - (XB*ZC - XC*ZB);
			K3 = XA*(YB-YC) - YA*(XB-XC) + (XB*YC - XC*YB);
			K4 = -XA*(YB*ZC-YC*ZB) + YA*(XB*ZC-XC*ZB) -ZA* (XB*YC - XC*YB);

			//K1, K2, and K3
			K123.Resize(1,3);
			K123(0,0) = K1;
			K123(0,1) = K2;
			K123(0,2) = K3;

			rawpoint_pos = NULL;
			for(j=0; j<num_points; j++)
			{
				rawpoint_pos = patch.PointList.GetNextPos(rawpoint_pos);
				CLIDARRawPoint point = rawpoint_pos->value;
				//CLIDARRawPoint point = patch.PointList.GetAt(j);
				
				Cal_PDsWithVolume_Titan(point, Config, patch, jmat_lidar,jmat_vertexA,jmat_vertexB,jmat_vertexC, bmat, kmat);

				outfile<<kmat(0,0)/patch.Area<<endl;
				
				CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
				we_temp = we_temp.Inverse();

				CSMMatrix<double> j_tri; j_tri.Resize(0,0);
				j_tri.Insert(0,0,jmat_vertexA); 
				j_tri.Insert(0,j_tri.GetCols(),jmat_vertexB);
				j_tri.Insert(0,j_tri.GetCols(),jmat_vertexC);

				//reduced normal matrix
				LS.Fill_Nmat_Data_LIDARPointwithDeterminent(FixedParamIndex, FixedParamPoint, jmat_lidar, j_tri, we_temp, kmat, i, area(0,0));
				//
			}//for(j=0; j<num_points; j++)

			//Parameter observations(vertex coord)
			oripatch_pos = OriginPatchList.GetNextPos(oripatch_pos);
			CLIDARPlanarPatch oripatch = oripatch_pos->value;
			//CLIDARPlanarPatch oripatch = OriginPatchList.GetAt(i);
			CSMMatrix<double> jmat_vtx_param(9,9);
			jmat_vtx_param.Identity(1.0);
			CSMMatrix<double> k_vtx(9,1);
			CSMMatrix<double> WVTX(9,9);
			
			//vertex A
			k_vtx(0,0) = oripatch.Vertex[0] - patch.Vertex[0];
			k_vtx(1,0) = oripatch.Vertex[1] - patch.Vertex[1];
			k_vtx(2,0) = oripatch.Vertex[2] - patch.Vertex[2];

			WVTX.Insert(0,0,QVertexA.Inverse());

			//vertex B
			k_vtx(3,0) = oripatch.Vertex[3] - patch.Vertex[3];
			k_vtx(4,0) = oripatch.Vertex[4] - patch.Vertex[4];
			k_vtx(5,0) = oripatch.Vertex[5] - patch.Vertex[5];

			WVTX.Insert(3,3,QVertexB.Inverse());

			//vertex C
			k_vtx(6,0) = oripatch.Vertex[6] - patch.Vertex[6];
			k_vtx(7,0) = oripatch.Vertex[7] - patch.Vertex[7];
			k_vtx(8,0) = oripatch.Vertex[8] - patch.Vertex[8];

			WVTX.Insert(6,6,QVertexC.Inverse());

			LS.Fill_Nmat_TriangleVertex(jmat_vtx_param,WVTX,k_vtx,i);

			outfile<<k_vtx.matrixout()<<endl;

		}//for(i=0; i<num_Patches; i++)

	
		//Parameter observations(LIDAR param)
		CSMMatrix<double> WParam = QParam.Inverse();
		
		CSMMatrix<double> jmat_param(num_unknown,num_unknown,0.);
		jmat_param.Identity(1.0);
		CSMMatrix<double> jmat_lidar_k(num_unknown,1,0.);

		jmat_lidar_k(0,0) = OriginConfig.Xb - Config.Xb;
		jmat_lidar_k(1,0) = OriginConfig.Yb - Config.Yb;
		jmat_lidar_k(2,0) = OriginConfig.Zb - Config.Zb;
		
		jmat_lidar_k(3,0) = OriginConfig.Ob - Config.Ob;
		jmat_lidar_k(4,0) = OriginConfig.Pb - Config.Pb;
		jmat_lidar_k(5,0) = OriginConfig.Kb - Config.Kb;
		
		jmat_lidar_k(6,0) = OriginConfig.Alpha_b - Config.Alpha_b;
		jmat_lidar_k(7,0) = OriginConfig.Beta_b - Config.Beta_b;
		
		jmat_lidar_k(8,0) = OriginConfig.Rangeb - Config.Rangeb;
		jmat_lidar_k(9,0) = OriginConfig.RangeS - Config.RangeS;

		for(int param_index=0; param_index<num_unknown; param_index++)
		{
			if(sqrt(QParam(param_index,param_index)) <= fabs(Small_QThreshold))
			{
				jmat_lidar_k(param_index, 0) = 0.0; 
			}					
		}

		outfile<<jmat_lidar_k.matrixout()<<endl;

		LS.Fill_Nmat_LIDARParam(jmat_param,WParam,jmat_lidar_k);

		double var;
		CSMMatrix<double> XMAT = LS.RunLeastSquare_RN(var, NMAT_dot, CMAT_dot);
		NMAT_dot_inv = NMAT_dot.Inverse();

		//2007.05.09 Normal_Matrix = LS.GetNmatrix();
		CSMMatrix<double> Normal_Matrix = LS.GetNmatrix(N2dot_point, N2dot_line, N2dot_tri);

		outfile<<setw(10)<<"[NMAT_dot]"<<endl;
		for(i=0; i<(int)NMAT_dot.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT_dot.GetCols(); j++)
				outfile<<setw(10)<<NMAT_dot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[CMAT_dot]"<<endl;
		for(i=0; i<(int)CMAT_dot.GetRows(); i++)
		{
			for(j=0; j<(int)CMAT_dot.GetCols(); j++)
				outfile<<setw(10)<<CMAT_dot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		
		CSMMatrix<double> CorreMat;
		CorreMat = Correlation(NMAT_dot_inv);
		outfile<<setw(10)<<"[CorreMat]"<<endl;
		for(i=0; i<(int)CorreMat.GetRows(); i++)
		{
			for(j=0; j<(int)CorreMat.GetCols(); j++)
				outfile<<setw(10)<<CorreMat(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		
		CBMPImage img2;
		img2.Create(CorreMat.GetCols(),CorreMat.GetRows(),8);
		CBMPPixelPtr ptr2(img2);
		
		for(i=0; i<(int)CorreMat.GetRows(); i++)
		{
			for(j=0; j<(int)CorreMat.GetCols(); j++)
			{
				if(fabs(CorreMat(i,j))>0.9) 
				{
					double temp = CorreMat(i,j)-0.9;
					ptr2[i][j] = BYTE(fabs(temp)/0.1*255);
				}
				else ptr2[i][j] = BYTE(0);
			}
		}
		
		CString correpath = resultfile;
		correpath.MakeLower();
		correpath.Replace(".result","_corre.bmp");
		img2.SaveBMP(correpath);

		outfile.setf(ios::scientific);
		
		//*
		outfile<<setw(10)<<"[NMAT_dot_inv]"<<endl;
		for(i=0; i<(int)NMAT_dot_inv.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT_dot_inv.GetCols(); j++)
				outfile<<setw(10)<<NMAT_dot_inv(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile<<setw(10)<<XMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		//*/

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//		for(i=0; i<(int)num_unknown; i++)
		//		{
		//			if(QParam(i,i)<pow(QThreshold*0.01,2))
		//			{
		//				XMAT(i,0) = 0.0;
		//			}
		//		}

		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);

		Config.Alpha_b += XMAT(6,0);
		Config.Beta_b  += XMAT(7,0);

		Config.Rangeb += XMAT(8,0);
		Config.RangeS += XMAT(9,0);

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		patch_pos = NULL;
		for(i=0; i<(int)num_Patches; i++)
		{
			patch_pos = PatchList.GetNextPos(patch_pos);
			CLIDARPlanarPatch patch = patch_pos->value;
			//CLIDARPlanarPatch patch = PatchList.GetAt(i);
			
			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_A);
				
				if(nSort != -1) patch.Vertex[j] += XMAT(num_unknown+nSort*3+j,0);
			}

			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_B);
				
				if(nSort != -1) patch.Vertex[j+3] += XMAT(num_unknown+nSort*3+j,0);
			}

			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_C);
				
				if(nSort != -1) patch.Vertex[j+6] += XMAT(num_unknown+nSort*3+j,0);
			}
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		CSMList<MatrixData> VolError, NDError; 
		CSMList<MatrixData> V;
		LS.GetLIDARErrorMatwithDeterminent(V, VolError, NDError);

		int n_mat = VolError.GetNumItem();
		
		DATA<MatrixData> *pos1 = NULL;
		DATA<MatrixData> *pos2 = NULL;
		
		double sum_EVol = 0;
		double sum_END = 0;
		int count=0;
		
		for(i=0; i<n_mat; i++)
		{
			pos1 = VolError.GetNextPos(pos1);
			pos2 = NDError.GetNextPos(pos2);
			
			int nrows = pos1->value.mat.GetRows();
			
			for(j=0; j<nrows; j++)
			{
				double temp = pos1->value.mat(j,0);
				temp = temp*temp;
				sum_EVol += temp;

				temp = pos2->value.mat(j,0);
				temp = temp*temp;
				sum_END += temp;

				count++;
			}
		}

		//2007.05.09
//		CSMMatrix<double> Sum_VolError2 = VolError.Transpose()%VolError;
//		double Mean_VolError2 = Sum_VolError2(0,0)/VolError.GetRows();
//		double RMSE_VolError = sqrt(Mean_VolError2);

//		CSMMatrix<double> Sum_NDError2 = NDError.Transpose()%NDError;
//		double Mean_NDError2 = Sum_NDError2(0,0)/NDError.GetRows();
//		double RMSE_NDError = sqrt(Mean_NDError2);

		double RMSE_VolError = sqrt(sum_EVol/count);

		double RMSE_NDError = sqrt(sum_END/count);

		double sigma = sqrt(var);
		
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<"\t"<<sigma<<endl;
		outfile<<setw(10)<<" RMSE(Volume errors): "<<"\t"<<RMSE_VolError<<endl;
		outfile<<setw(10)<<" RMSE(Normal distances errors): "<<"\t"<<RMSE_NDError<<endl;

		csvfile.precision(10);
		csvfile.setf(ios::scientific );
		csvfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
		csvfile<<setw(10)<<" RMSE(Volume errors): "<<","<<RMSE_VolError<<endl;
		csvfile<<setw(10)<<" RMSE(Normal distances errors): "<<","<<RMSE_NDError<<endl;
		
		outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		outfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
		outfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
		outfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		outfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<"\t";
		outfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<"\t";
		outfile<<setw(10)<<(Config.Rangeb)<<"\t";
		outfile<<setw(10)<<(Config.RangeS)<<"\t";
		outfile<<endl;
				

		csvfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		csvfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<",";
		csvfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<",";
		csvfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		csvfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<",";
		csvfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<",";
		csvfile<<setw(10)<<(Config.Rangeb)<<",";
		csvfile<<setw(10)<<(Config.RangeS)<<",";
		csvfile<<endl;
		
		outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<"\t";
		outfile<<endl<<endl;
		
		outfile.flush();

		csvfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<",";
		csvfile<<endl<<endl;

		csvfile<<setw(10)<<"[CorreMat]"<<endl;
		for(i=0; i<(int)CorreMat.GetRows(); i++)
		{
			for(j=0; j<(int)CorreMat.GetCols(); j++)
			{
				csvfile<<setw(10)<<CorreMat(i,j)<<",";
			}
			csvfile<<endl;
		}
		csvfile.flush();

		if(nIteration > 1)
		{
			if(fabs(old_rmse_ND-RMSE_NDError)<fabs(LIDAR_PRECISION*1.0e-4))
			{
				outfile<<endl<<endl<<"##################################################################"<<endl;
				outfile<<"Iteration Stop(fabs(old_rmse_ND-RMSE_NDError)): "<<fabs(old_rmse_ND-RMSE_NDError)<<endl;
				outfile<<"##################################################################"<<endl<<endl;
				bStop = false;
			}
		}

		old_sigma = sigma;
		old_rmse_ND = RMSE_NDError;

		if(nIteration >= max_iter)
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}

		if(bStop == false)
		{
			fstream newconfigfile;
			CString configpath = resultfile;
			configpath.MakeLower();
			configpath.Replace(".result",".config");
			newconfigfile.open(configpath,ios::out);
			newconfigfile <<"VER 1.2"<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Biases in spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Sigma of spatial offsets biases(3EA, M)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile.precision(10);
			newconfigfile.setf(ios::scientific );
			newconfigfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scan angle offsets(2EA, deg)"<<endl;
			newconfigfile <<"#Sigma of scan angle offset(2EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Alpha_b)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Beta_b)<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
			newconfigfile<<setw(10)<<(Config.Rangeb)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(8,8)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scale of laser ranging(1EA, M), Sigma of scale of laser ranging(1EA)"<<endl;
			newconfigfile<<setw(10)<<(Config.RangeS)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(9,9)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of GPS signal[3EA(X,Y,Z), M]"<<endl;
			newconfigfile<<setw(10)<<Config.sX<<"\t";
			newconfigfile<<setw(10)<<Config.sY<<"\t";
			newconfigfile<<setw(10)<<Config.sZ<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of INS signal[3EA(O,P,K), rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sO)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sP)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sK)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of swing angle[2EA, rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sAlpha)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sBeta)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"##sigma of laser ranging[1EA, M]"<<endl;
			newconfigfile<<setw(10)<<Config.sRange<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#ID"<<endl;
			newconfigfile <<Config.ID<<endl;
			newconfigfile.close();
		}
		
	}while(bStop == true);

	CString nimgpath = resultfile;
	nimgpath.MakeLower();
	nimgpath.Replace(".result",".bmp");

	if(false == LS.MakeMatrixImg(0.9, nimgpath))
	{
		outfile<<endl<<endl<<"---------------------------------------"<<endl;
		outfile<<"Image Generation Fail: Size is too big"<<endl;
		outfile<<"---------------------------------------"<<endl;
	}
	
	outfile.close();
	csvfile.close();

	return true;
}

bool CLIDARBiasCalibration::SolveWithVolume(double LIDAR_PRECISION, const char resultfile[], int max_iter, double QThreshold)
{
	CSMMatrix<double> JMAT, KMAT, XMAT, We;	
	CSMMatrix<double> AREA, area;
	CSMMatrix<double> jmat_lidar,jmat_vertexA,jmat_vertexB,jmat_vertexC, kmat, bmat, qmat;
	CSMMatrix<double> CMAT, NMAT, NMATinv;
	int i, j;

	fstream outfile;
	fstream csvfile;

	CString csvpath = resultfile;
	csvpath.MakeLower();
	csvpath.Replace(".result",".csv");
	csvfile.open(csvpath, ios::out);
	
	CString resultname = resultfile;
	outfile.open(resultname,ios::out);

	bool bStop = true;
	int nIteration = 0;
	const int num_obs = 9;
	const int num_unknown = 10;
	double old_sigma;
	double old_rmse_Vol;

	//Weight for parameters
	CSMMatrix<double> QParam(num_unknown,num_unknown,0.);
	QParam(0,0) = Config.sXb*Config.sXb;
	QParam(1,1) = Config.sYb*Config.sYb;
	QParam(2,2) = Config.sZb*Config.sZb;

	QParam(3,3) = Config.sOb*Config.sOb;
	QParam(4,4) = Config.sPb*Config.sPb;
	QParam(5,5) = Config.sKb*Config.sKb;
	
	QParam(6,6) = Config.sAlpha_b*Config.sAlpha_b;
	QParam(7,7) = Config.sBeta_b*Config.sBeta_b;

	QParam(8,8) = Config.sRangeb*Config.sRangeb;
	QParam(9,9) = Config.sRangeS*Config.sRangeS;

	qmat.Resize(num_obs,num_obs,0.);

	qmat(0,0) = Config.sX*Config.sX;
	qmat(1,1) = Config.sY*Config.sY;
	qmat(2,2) = Config.sZ*Config.sZ;

	qmat(3,3) = Config.sO*Config.sO;
	qmat(4,4) = Config.sP*Config.sP;
	qmat(5,5) = Config.sK*Config.sK;

	qmat(6,6) = Config.sAlpha*Config.sAlpha;
	qmat(7,7) = Config.sBeta*Config.sBeta;
	qmat(8,8) = Config.sRange*Config.sRange;

	//Number of patches
	int num_Patches = PatchList.GetNumItem();

	//original patches
	OriginPatchList = PatchList;
	

	//Extract point ID
	CSMList<CString> VTXList;
	for(i=0; i<num_Patches; i++)
	{
		CLIDARPlanarPatch patch = PatchList.GetAt(i);
		
		int nSort;

		CString ID_A = patch.ID_A;
		CString ID_B = patch.ID_B;
		CString ID_C = patch.ID_C;
		nSort = FindCStringID(VTXList,ID_A);
		if(nSort == -1) VTXList.AddTail(ID_A);

		nSort = FindCStringID(VTXList,ID_B);
		if(nSort == -1) VTXList.AddTail(ID_B);

		nSort = FindCStringID(VTXList,ID_C);
		if(nSort == -1) VTXList.AddTail(ID_C);
	}


	do
	{
		//Increase iteration number
		nIteration ++;

		JMAT.Resize(0,0,0.);
		KMAT.Resize(0,0,0.);
		We.Resize(0,0,0.);
		AREA.Resize(0,0,0.);
		
		kmat.Resize(1,1,0.);
		jmat_vertexA.Resize(1,3,0.);
		jmat_vertexB.Resize(1,3,0.);
		jmat_vertexC.Resize(1,3,0.);
		jmat_lidar.Resize(1,num_unknown,0.);
		bmat.Resize(1,num_obs,0.);
		kmat.Resize(1,1,0.);
		area.Resize(1,1,0.);
		
		//boresight vector (spatial offset + spatial bias)
		Pvector.Resize(3,1);
		Pvector(0,0) = Config.Xoffset + Config.Xb;
		Pvector(1,0) = Config.Yoffset + Config.Yb;
		Pvector(2,0) = Config.Zoffset + Config.Zb;
		
		//offset angle + bias angle
		dO = Config.Ooffset + Config.Ob;
		dP = Config.Poffset + Config.Pb;
		dK = Config.Koffset + Config.Kb;
		//INS Rotation matrix: R matrix
		Offset_R.ReMake(dO, dP, dK);
		
		//partial derivatives for angle biases (omega)
		dOffsetR_dO.Partial_dRdO(dO,dP,dK);
		//partial derivatives for angle biases (phi)
		dOffsetR_dP.Partial_dRdP(dO,dP,dK);
		//partial derivatives for angle biases (kappa)
		dOffsetR_dK.Partial_dRdK(dO,dP,dK);
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////
		
		//index of no. of rows
		int row_index = 0;
		
		for(i=0; i<num_Patches; i++)
		{
			//To get patch
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			area(0,0) = patch.Area;
			
			int nSortA = FindCStringID(VTXList, patch.ID_A);
			int nSortB = FindCStringID(VTXList, patch.ID_B);
			int nSortC = FindCStringID(VTXList, patch.ID_C);
			
			int num_points = patch.PointList.GetNumItem();
			
			CSMMatrix<double> IncreaseMat;
			IncreaseMat.Resize(num_points,num_points,0.0);
			We.Insert(We.GetRows(),We.GetRows(),IncreaseMat);

			IncreaseMat.Resize(num_points,num_unknown,0.0);
			JMAT.Insert(JMAT.GetRows(),0,IncreaseMat);

			IncreaseMat.Resize(num_points,1,0.0);
			KMAT.Insert(KMAT.GetRows(),0,IncreaseMat);
			
			IncreaseMat.Resize(num_points,1,0.0);
			AREA.Insert(AREA.GetRows(),0,IncreaseMat);
			
			XA = patch.Vertex[0];	YA = patch.Vertex[1];	ZA = patch.Vertex[2];
			XB = patch.Vertex[3];	YB = patch.Vertex[4];	ZB = patch.Vertex[5];
			XC = patch.Vertex[6];	YC = patch.Vertex[7];	ZC = patch.Vertex[8];
			
			CSMMatrix<double> QVertexA(3,3,0.);
			QVertexA(0,0) = patch.SD[0]*patch.SD[0];
			QVertexA(1,1) = patch.SD[1]*patch.SD[1];
			QVertexA(2,2) = patch.SD[2]*patch.SD[2];
			
			CSMMatrix<double> QVertexB(3,3,0.);
			QVertexB(0,0) = patch.SD[3]*patch.SD[3];
			QVertexB(1,1) = patch.SD[4]*patch.SD[4];
			QVertexB(2,2) = patch.SD[5]*patch.SD[5];
			
			CSMMatrix<double> QVertexC(3,3,0.);
			QVertexC(0,0) = patch.SD[6]*patch.SD[6];
			QVertexC(1,1) = patch.SD[7]*patch.SD[7];
			QVertexC(2,2) = patch.SD[8]*patch.SD[8];
			
			//plane coefficient
			K1 = YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC - YC*ZB);
			K2 = -XA*(ZB-ZC) +ZA*(XB-XC) - (XB*ZC - XC*ZB);
			K3 = XA*(YB-YC) - YA*(XB-XC) + (XB*YC - XC*YB);
			K4 = -XA*(YB*ZC-YC*ZB) + YA*(XB*ZC-XC*ZB) -ZA* (XB*YC - XC*YB);

			//K1, K2, and K3
			K123.Resize(1,3);
			K123(0,0) = K1;
			K123(0,1) = K2;
			K123(0,2) = K3;

			for(j=0; j<num_points; j++)
			{
				CLIDARRawPoint point = patch.PointList.GetAt(j);
				
				Cal_PDsWithVolume(point, Config, patch, jmat_lidar,jmat_vertexA,jmat_vertexB,jmat_vertexC, bmat, kmat);
				
				CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
				we_temp = we_temp.Inverse();
				We.Insert(row_index,row_index,we_temp);

				JMAT.Insert(row_index,0,jmat_lidar);
				if(nSortA != -1) JMAT.Insert(row_index,num_unknown+3*nSortA,jmat_vertexA);
				if(nSortB != -1) JMAT.Insert(row_index,num_unknown+3*nSortB,jmat_vertexB);
				if(nSortC != -1) JMAT.Insert(row_index,num_unknown+3*nSortC,jmat_vertexC);

				KMAT.Insert(row_index,0,kmat);

				AREA.Insert(row_index, 0, area);
				
				row_index ++;
			}//for(j=0; j<num_points; j++)

			//Parameter observations(vertex coord)
			CLIDARPlanarPatch oripatch = OriginPatchList.GetAt(i);
			CSMMatrix<double> jmat_vtx_param(3,3);
			jmat_vtx_param.Identity(1.0);
			CSMMatrix<double> k_vtx(3,1);
			CSMMatrix<double> WVTX;

			//vertex A
			k_vtx(0,0) = oripatch.Vertex[0] - patch.Vertex[0];
			k_vtx(1,0) = oripatch.Vertex[1] - patch.Vertex[1];
			k_vtx(2,0) = oripatch.Vertex[2] - patch.Vertex[2];

			WVTX = QVertexA.Inverse();
			
			JMAT.Insert(row_index,num_unknown+nSortA*3,jmat_vtx_param);
			KMAT.Insert(row_index,0,k_vtx);
			We.Insert(row_index,row_index,WVTX);
			row_index = JMAT.GetRows();

			//vertex B
			k_vtx(0,0) = oripatch.Vertex[3] - patch.Vertex[3];
			k_vtx(1,0) = oripatch.Vertex[4] - patch.Vertex[4];
			k_vtx(2,0) = oripatch.Vertex[5] - patch.Vertex[5];

			WVTX = QVertexB.Inverse();

			JMAT.Insert(row_index,num_unknown+nSortB*3,jmat_vtx_param);
			KMAT.Insert(row_index,0,k_vtx);
			We.Insert(row_index,row_index,WVTX);
			row_index = JMAT.GetRows();

			//vertex C
			k_vtx(0,0) = oripatch.Vertex[6] - patch.Vertex[6];
			k_vtx(1,0) = oripatch.Vertex[7] - patch.Vertex[7];
			k_vtx(2,0) = oripatch.Vertex[8] - patch.Vertex[8];

			WVTX = QVertexC.Inverse();

			JMAT.Insert(row_index,num_unknown+nSortC*3,jmat_vtx_param);
			KMAT.Insert(row_index,0,k_vtx);
			We.Insert(row_index,row_index,WVTX);
			row_index = JMAT.GetRows();

		}//for(i=0; i<num_Patches; i++)

		//Parameter observations(LIDAR param)
		CSMMatrix<double> jmat_param(num_unknown,num_unknown,0.);
		jmat_param.Identity(1.0);
		CSMMatrix<double> jmat_lidar_k(num_unknown,1,0.);
		CSMMatrix<double> WParam = QParam.Inverse();
		
		jmat_lidar_k(0,0) = OriginConfig.Xb - Config.Xb;
		jmat_lidar_k(1,0) = OriginConfig.Yb - Config.Yb;
		jmat_lidar_k(2,0) = OriginConfig.Zb - Config.Zb;
		
		jmat_lidar_k(3,0) = OriginConfig.Ob - Config.Ob;
		jmat_lidar_k(4,0) = OriginConfig.Pb - Config.Pb;
		jmat_lidar_k(5,0) = OriginConfig.Kb - Config.Kb;
		
		jmat_lidar_k(6,0) = OriginConfig.Alpha_b - Config.Alpha_b;
		jmat_lidar_k(7,0) = OriginConfig.Beta_b - Config.Beta_b;
		
		jmat_lidar_k(8,0) = OriginConfig.Rangeb - Config.Rangeb;
		jmat_lidar_k(9,0) = OriginConfig.RangeS - Config.RangeS;
		
		JMAT.Insert(row_index,0,jmat_param);
		We.Insert(row_index,row_index,WParam);
		KMAT.Insert(row_index,0,jmat_lidar_k);
		
		row_index = JMAT.GetRows();

		//Normal matrix
		NMAT = JMAT.Transpose()%We%JMAT;
		
		CMAT = JMAT.Transpose()%We%KMAT;
		
		outfile.precision(10);
		outfile.setf(ios::scientific );

		CSMMatrix<double> NMAT_dot = NMAT.Subset(0,0,num_unknown,num_unknown);
		
		outfile<<setw(10)<<"[NMAT_dot]"<<endl;
		for(i=0; i<(int)NMAT_dot.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT_dot.GetCols(); j++)
				outfile<<setw(10)<<NMAT_dot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		CSMMatrix<double> CMAT_dot = CMAT.Subset(0,0,num_unknown,1);
		
		outfile<<setw(10)<<"[CMAT_dot]"<<endl;
		for(i=0; i<(int)CMAT_dot.GetRows(); i++)
		{
			for(j=0; j<(int)CMAT_dot.GetCols(); j++)
				outfile<<setw(10)<<CMAT_dot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		
		NMATinv = NMAT.Inverse();

		//XMAT
		XMAT = NMATinv%CMAT;

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		for(i=0; i<(int)num_unknown; i++)
		{
			if(QParam(i,i)<pow(QThreshold*0.01,2))
			{
				XMAT(i,0) = 0.0;
			}
		}

		outfile<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile<<setw(10)<<XMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);

		Config.Alpha_b += XMAT(6,0);
		Config.Beta_b += XMAT(7,0);

		Config.Rangeb += XMAT(8,0);
		Config.RangeS += XMAT(9,0);

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		for(i=0; i<(int)num_Patches; i++)
		{
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			
			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_A);
				
				if((patch.SD[j]*patch.SD[j])<QThreshold)
				{
					if(nSort != -1) XMAT(num_unknown+nSort*3+j,0) = 0.0;
				}
				if(nSort != -1) patch.Vertex[j] += XMAT(num_unknown+nSort*3+j,0);
			}

			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_B);
				if((patch.SD[j+3]*patch.SD[j+3])<QThreshold)
				{
					if(nSort != -1) XMAT(num_unknown+nSort*3+j,0) = 0.0;
				}
				if(nSort != -1) patch.Vertex[j+3] += XMAT(num_unknown+nSort*3+j,0);
			}

			for(int j=0; j<3; j++)
			{
				int nSort;
				nSort = FindCStringID(VTXList, patch.ID_C);
				if((patch.SD[j+6]*patch.SD[j+6])<QThreshold)
				{					
					if(nSort != -1) XMAT(num_unknown+nSort*3+j,0) = 0.0;
				}
				if(nSort != -1) patch.Vertex[j+6] += XMAT(num_unknown+nSort*3+j,0);
			}
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		CSMMatrix<double> V = JMAT%XMAT - KMAT;
		CSMMatrix<double> VolError(AREA.GetRows(),1,0.);
		CSMMatrix<double> NDError(AREA.GetRows(),1,0.);

		for(i=0; i<(int)VolError.GetRows(); i++)
		{
			VolError(i,0) = V(i,0)/6.;//volume error
			NDError(i,0) = VolError(i,0) * 3. / AREA(i,0);//calculate normal distance using area
		}

		CSMMatrix<double> Sum_VolError2 = VolError.Transpose()%VolError;
		double Mean_VolError2 = Sum_VolError2(0,0)/VolError.GetRows();
		double RMSE_VolError = sqrt(Mean_VolError2);

		CSMMatrix<double> Sum_NDError2 = NDError.Transpose()%NDError;
		double Mean_NDError2 = Sum_NDError2(0,0)/NDError.GetRows();
		double RMSE_NDError = sqrt(Mean_NDError2);

		CSMMatrix<double> VTV = V.Transpose()%We%V;
		double variance = VTV(0,0)/(JMAT.GetRows() - num_unknown);
		double sigma = sqrt(variance);
		
			
		outfile.precision(10);
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<"\t"<<sigma<<endl;
		outfile<<setw(10)<<" RMSE(Volume errors): "<<"\t"<<RMSE_VolError<<endl;
		outfile<<setw(10)<<" RMSE(Normal distances errors): "<<"\t"<<RMSE_NDError<<endl;

		csvfile.precision(10);
		csvfile.setf(ios::scientific );
		csvfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
		csvfile<<setw(10)<<" RMSE(Volume errors): "<<","<<RMSE_VolError<<endl;
		csvfile<<setw(10)<<" RMSE(Normal distances errors): "<<","<<RMSE_NDError<<endl;
		
		outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		outfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
		outfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
		outfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		outfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<"\t";
		outfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<"\t";
		outfile<<setw(10)<<(Config.Rangeb)<<"\t";
		outfile<<setw(10)<<(Config.RangeS)<<"\t";
		outfile<<endl;
				

		csvfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		csvfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<",";
		csvfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<",";
		csvfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
		csvfile<<setw(10)<<(Rad2Deg(Config.Alpha_b))<<",";
		csvfile<<setw(10)<<(Rad2Deg(Config.Beta_b))<<",";
		csvfile<<setw(10)<<(Config.Rangeb)<<",";
		csvfile<<setw(10)<<(Config.RangeS)<<",";
		csvfile<<endl;
		
		outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		outfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))*1000<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))*3600<<"\t";
		outfile<<endl;

		outfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(6,6)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(7,7)))*3600<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(8,8))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(9,9))*1000<<"\t";
		outfile<<endl<<endl;
		
		outfile.flush();

		csvfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		csvfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))*1000<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))*3600<<",";
		csvfile<<endl;

		csvfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(6,6)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(7,7)))*3600<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMATinv(8,8))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMATinv(9,9))*1000<<",";
		csvfile<<endl<<endl;

		csvfile.flush();

		if(nIteration > 1)
		{
			if(fabs(old_rmse_Vol-RMSE_VolError)<fabs(LIDAR_PRECISION))
			{
				outfile<<endl<<endl<<"##################################################################"<<endl;
				outfile<<"Iteration Stop(fabs(old_rmse_Vol-RMSE_VolError)): "<<fabs(old_rmse_Vol-RMSE_VolError)<<endl;
				outfile<<"##################################################################"<<endl<<endl;
				bStop = false;
			}
		}

		old_sigma = sigma;
		old_rmse_Vol = RMSE_VolError;

		if(nIteration >= max_iter)
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}

		if(bStop == false)
		{
			fstream newconfigfile;
			CString configpath = resultfile;
			configpath.MakeLower();
			configpath.Replace(".result",".config");
			newconfigfile.open(configpath,ios::out);
			newconfigfile <<"VER 1.2"<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Biases in spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Sigma of spatial offsets biases(3EA, M)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile.precision(10);
			newconfigfile.setf(ios::scientific );
			newconfigfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scan angle offsets(2EA, deg)"<<endl;
			newconfigfile <<"#Sigma of scan angle offset(2EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Alpha_b)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Beta_b)<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(6,6)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(7,7)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
			newconfigfile<<setw(10)<<(Config.Rangeb)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMATinv(8,8)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#Scale of laser ranging(1EA, M), Sigma of scale of laser ranging(1EA)"<<endl;
			newconfigfile<<setw(10)<<(Config.RangeS)<<"\t";
			newconfigfile<<setw(10)<<(sigma*sqrt(NMATinv(9,9)))<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of GPS signal[3EA(X,Y,Z), M]"<<endl;
			newconfigfile<<setw(10)<<Config.sX<<"\t";
			newconfigfile<<setw(10)<<Config.sY<<"\t";
			newconfigfile<<setw(10)<<Config.sZ<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of INS signal[3EA(O,P,K), rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sO)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sP)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sK)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#sigma of swing angle[2EA, rad]"<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.sAlpha)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.sBeta)<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"##sigma of laser ranging[1EA, M]"<<endl;
			newconfigfile<<setw(10)<<Config.sRange<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile <<"#ID"<<endl;
			newconfigfile <<Config.ID<<endl;
			newconfigfile.close();
		}
		
	}while(bStop == true);

	CBMPImage img;
	img.Create(NMAT.GetCols(),NMAT.GetRows(),8);
	CBMPPixelPtr ptr(img);
	for(i=0; i<(int)NMAT.GetRows(); i++)
	{
		for(j=0; j<(int)NMAT.GetCols(); j++) 
		{
			if(NMAT(i,j)==0.) ptr[i][j] = BYTE(255);
			else ptr[i][j] = BYTE(0);
		}
	}
	
	CString nimgpath = resultfile;
	nimgpath.MakeLower();
	nimgpath.Replace(".result","_N.bmp");
	img.SaveBMP(nimgpath);

	CBMPImage img2;
	CSMMatrix<double> CorreMat;
	CorreMat = Correlation(NMATinv);
	outfile<<setw(10)<<"[CorreMat]"<<endl;
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
			outfile<<setw(10)<<CorreMat(i,j)<<"\t";
		outfile<<endl;
	}
	outfile.flush();
	
	img2.Create(CorreMat.GetCols(),CorreMat.GetRows(),8);
	CBMPPixelPtr ptr2(img2);
	
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
		{
			if(fabs(CorreMat(i,j))>0.9) 
			{
				double temp = CorreMat(i,j)-0.9;
				ptr2[i][j] = BYTE(fabs(temp)/0.1*255);
			}
			else ptr2[i][j] = BYTE(0);
		}
	}
	
	CString correpath = resultfile;
	correpath.MakeLower();
	correpath.Replace(".result","_corre.bmp");
	img2.SaveBMP(correpath);

	outfile.close();
	csvfile.close();

	return true;
}

bool CLIDARBiasCalibration::SolveWithVolume_old(double LIDAR_PRECISION, const char resultfile[])
{
	CSMMatrix<double> JMAT, KMAT, XMAT, We;	
	CSMMatrix<double> AREA, area;
	CSMMatrix<double> jmat, kmat, bmat, qmat;
	CSMMatrix<double> CMAT, NMAT, NMATinv;
	int i, j;

	fstream outfile;
	fstream csvfile;

	CString csvpath = resultfile;
	csvpath.MakeLower();
	csvpath.Replace(".result",".csv");
	csvfile.open(csvpath, ios::out);
	
	CString resultname = resultfile;
	outfile.open(resultname,ios::out);

	double QThreshold = 1.0e-6;
	//Weight for parameters
	CSMMatrix<double> QParam(6,6,0.);
	QParam(0,0) = Config.sXb*Config.sXb;
	QParam(1,1) = Config.sYb*Config.sYb;
	QParam(2,2) = Config.sZb*Config.sZb;
	QParam(3,3) = Config.sOb*Config.sOb;
	QParam(4,4) = Config.sPb*Config.sPb;
	QParam(5,5) = Config.sKb*Config.sKb;

	
	bool bStop = true;
	int nIteration = 0;
	int num_unknown = 6;
	double old_sigma;
	double old_rmse_ND;

	do{
		//Increase iteration number
		nIteration ++;

		JMAT.Resize(0,0,0.);
		KMAT.Resize(0,0,0.);
		We.Resize(0,0,0.);
		AREA.Resize(0,0,0.);

		kmat.Resize(1,1,0.);
		jmat.Resize(1,num_unknown,0.);
		bmat.Resize(1,9,0.);
		kmat.Resize(1,1,0.);
		qmat.Resize(9,9,0.);
		area.Resize(1,1,0.);
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////
		
		//Number of patches
		int num_Patches = PatchList.GetNumItem();
		
		//index of no. of rows
		int row_index = 0;
		
		for(i=0; i<num_Patches; i++)
		{
			//To get patch
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			double Area = patch.Area;
			area(0,0) = Area;

			for(int row=0;row<(int)qmat.GetRows();row++) qmat(row,row) = patch.SD[row]*patch.SD[row];			
			
			int num_points = patch.PointList.GetNumItem();
			
			CSMMatrix<double> IncreaseMat;
			IncreaseMat.Resize(num_points,num_points,0.0);
			We.Insert(We.GetRows(),We.GetRows(),IncreaseMat);

			IncreaseMat.Resize(num_points,num_unknown,0.0);
			JMAT.Insert(JMAT.GetRows(),0,IncreaseMat);

			IncreaseMat.Resize(num_points,1,0.0);
			KMAT.Insert(KMAT.GetRows(),0,IncreaseMat);

			IncreaseMat.Resize(num_points,1,0.0);
			AREA.Insert(AREA.GetRows(),0,IncreaseMat);

			for(j=0; j<num_points; j++)
			{
				CLIDARRawPoint point = patch.PointList.GetAt(j);
				
				Cal_PDsWithVolume_old(point, Config, patch, jmat, bmat, kmat);
				
				CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
				we_temp = we_temp.Inverse();
				we_temp(0,0) = 1.0;

				We.Insert(row_index,row_index,we_temp);
						
				JMAT.Insert(row_index,0,jmat);
				
				KMAT.Insert(row_index,0,kmat);

				AREA.Insert(row_index, 0, area);
				
				row_index ++;
			}
		}

		CSMMatrix<double> Ndot = JMAT.Transpose()%We%JMAT;
		CSMMatrix<double> Cdot = JMAT.Transpose()%We%KMAT;

		outfile<<setw(10)<<"[N dot Check]"<<endl;
		for(i=0; i<(int)Ndot.GetRows(); i++)
		{
			for(j=0; j<(int)Ndot.GetCols(); j++)
				outfile<<setw(10)<<Ndot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[C dot Check]"<<endl;
		for(i=0; i<(int)Cdot.GetRows(); i++)
		{
			for(j=0; j<(int)Cdot.GetCols(); j++)
				outfile<<setw(10)<<Cdot(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
	
		//Weight for parameters
		CSMMatrix<double> WParam = QParam.Inverse();
		
		//Parameter observations
		We.Insert(row_index,row_index,WParam);
		
		jmat.Resize(1,6,0.);
		jmat(0,0) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Xb - Config.Xb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;
		
		jmat.Resize(1,6,0.);
		jmat(0,1) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Yb - Config.Yb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;
		
		jmat.Resize(1,6,0.);
		jmat(0,2) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Zb - Config.Zb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;
		
		jmat.Resize(1,6,0.);
		jmat(0,3) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Ob - Config.Ob;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;
		
		jmat.Resize(1,6,0.);
		jmat(0,4) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Pb - Config.Pb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;
		
		jmat.Resize(1,6,0.);
		jmat(0,5) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Kb - Config.Kb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;
		
		//Normal matrix
		NMAT = JMAT.Transpose()%We%JMAT;
		
		CMAT = JMAT.Transpose()%We%KMAT;
		
		outfile.precision(10);
		outfile.setf(ios::scientific );
		
		//J matrix
		outfile<<setw(10)<<"[JMAT]"<<endl;
		for(i=0; i<(int)JMAT.GetRows(); i++)
		{
			for(j=0; j<(int)JMAT.GetCols(); j++)
				outfile<<setw(10)<<JMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		
		//K matrix
		outfile<<setw(10)<<"[KMAT]"<<endl;
		for(i=0; i<(int)KMAT.GetRows(); i++)
		{
			for(j=0; j<(int)KMAT.GetCols(); j++)
				outfile<<setw(10)<<KMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		//N matrix
		outfile<<setw(10)<<"[NMAT]"<<endl;
		for(i=0; i<(int)NMAT.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT.GetCols(); j++)
				outfile<<setw(10)<<NMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		//C matrix
		outfile<<setw(10)<<"[CMAT]"<<endl;
		for(i=0; i<(int)CMAT.GetRows(); i++)
		{
			for(j=0; j<(int)CMAT.GetCols(); j++)
				outfile<<setw(10)<<CMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		NMATinv = NMAT.Inverse();

		//XMAT
		XMAT = NMATinv%CMAT;

		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			if(QParam(i,i)<QThreshold)
			{
				XMAT(i,0) = 0.0;
			}
		}

		outfile<<setw(10)<<"[NMATinv]"<<endl;
		for(i=0; i<(int)NMATinv.GetRows(); i++)
		{
			for(j=0; j<(int)NMATinv.GetCols(); j++)
				outfile<<setw(10)<<NMATinv(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();
		
		outfile<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile<<setw(10)<<XMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		CSMMatrix<double> V = JMAT%XMAT - KMAT;
		CSMMatrix<double> VolError(AREA.GetRows(),1,0.);
		CSMMatrix<double> NDError(AREA.GetRows(),1,0.);

		for(i=0; i<(int)VolError.GetRows(); i++)
		{
			VolError(i,0) = V(i,0)/6;//volume error
			NDError(i,0) = VolError(i,0) * 3. / AREA(i,0);//calculate normal distance using area
		}
		
		CSMMatrix<double> Sum_VolError2 = VolError.Transpose()%VolError;
		double Mean_VolError2 = Sum_VolError2(0,0)/VolError.GetRows();
		double RMSE_VolError = sqrt(Mean_VolError2);

		CSMMatrix<double> Sum_NDError2 = NDError.Transpose()%NDError;
		double Mean_NDError2 = Sum_NDError2(0,0)/NDError.GetRows();
		double RMSE_NDError = sqrt(Mean_NDError2);

		CSMMatrix<double> VTV = V.Transpose()%We%V;
		double variance = VTV(0,0)/(JMAT.GetRows() - num_unknown);
		double sigma = sqrt(variance);
		
		//		outfile<<setw(10)<<"[VMAT]"<<endl;
		//		for(i=0; i<(int)V.GetRows(); i++)
		//		{
		//			for(j=0; j<(int)V.GetCols(); j++)
		//				outfile<<setw(10)<<V(i,j)<<"\t";
		//			outfile<<endl;
		//		}
		//		outfile.flush();

		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);
		
		outfile.precision(10);
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<"\t"<<sigma<<endl;
		outfile<<setw(10)<<" RMSE(Volume errors): "<<"\t"<<RMSE_VolError<<endl;
		outfile<<setw(10)<<" RMSE(Normal distances errors): "<<"\t"<<RMSE_NDError<<endl;

		csvfile.precision(10);
		csvfile.setf(ios::scientific );
		csvfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
		csvfile<<setw(10)<<" RMSE(Volume errors): "<<","<<RMSE_VolError<<endl;
		csvfile<<setw(10)<<" RMSE(Normal distances errors): "<<","<<RMSE_NDError<<endl;
		
		outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		outfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
		outfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
		outfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<"\t";
		outfile<<endl;
		//outfile<<setw(10)<<"Rangeb: "<<ConfigList.GetAt(0).Rangeb<<endl;
		

		csvfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		csvfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<",";
		csvfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<",";
		csvfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<",";
		csvfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<",";
		csvfile<<endl;
		
		outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		outfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))*1000<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))*3600<<"\t";
		outfile<<endl<<endl;
		
		outfile.flush();

		csvfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		csvfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))*1000<<",";
		csvfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))*1000<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))*3600<<",";
		csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))*3600<<",";
		csvfile<<endl<<endl;

		csvfile.flush();

		if(nIteration > 1)
		{
			if(fabs(old_rmse_ND-RMSE_NDError)<fabs(LIDAR_PRECISION))
			{
				outfile<<endl<<endl<<"##################################################################"<<endl;
				outfile<<"Iteration Stop(fabs(old_rmse_ND-RMSE_NDError))): "<<fabs(old_rmse_ND-RMSE_NDError)<<endl;
				outfile<<"##################################################################"<<endl<<endl;
				bStop = false;
			}
		}

		old_sigma = sigma;
		old_rmse_ND = RMSE_NDError;

		if(nIteration >= 20)
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}

		if(bStop == false)
		{
			fstream newconfigfile;
			CString configpath = resultfile;
			configpath.MakeLower();
			configpath.Replace(".result",".config");
			newconfigfile.open(configpath,ios::out);
			newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Biases in spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Sigma of spatial offsets biases(3EA, M)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
			newconfigfile <<"#ID"<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile.precision(10);
			newconfigfile.setf(ios::scientific );
			newconfigfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))<<endl;
			newconfigfile<<setw(10)<<Config.Rangeb<<"\t"<<Config.sRangeb<<endl;
			newconfigfile<<setw(10)<<"1";
			newconfigfile.close();
		}
		
	}while(bStop == true);

	//	CBMPImage img;
	//	img.Create(NMAT.GetCols(),NMAT.GetRows(),8);
	//	CBMPPixelPtr ptr(img);
	//	for(i=0; i<(int)NMAT.GetRows(); i++)
	//	{
	//		for(j=0; j<(int)NMAT.GetCols(); j++) 
	//		{
	//			if(NMAT(i,j)==0.) ptr[i][j] = BYTE(255);
	//			else ptr[i][j] = BYTE(0);
	//		}
	//	}
	//	
	//	CString nimgpath = resultfile;
	//	nimgpath.MakeLower();
	//	nimgpath.Replace(".result","_N.bmp");
	//	img.SaveBMP(nimgpath);
	
	outfile<<setw(10)<<"[NMAT]"<<endl;
	for(i=0; i<(int)NMAT.GetRows(); i++)
	{
		for(j=0; j<(int)NMAT.GetCols(); j++)
			outfile<<setw(10)<<NMAT(i,j)<<"\t";
		outfile<<endl;
	}
	outfile.flush();

	outfile<<setw(10)<<"[NMATinv]"<<endl;
	for(i=0; i<(int)NMATinv.GetRows(); i++)
	{
		for(j=0; j<(int)NMATinv.GetCols(); j++)
			outfile<<setw(10)<<NMATinv(i,j)<<"\t";
		outfile<<endl;
	}
	outfile.flush();

	CBMPImage img2;
	CSMMatrix<double> CorreMat;
	CorreMat = Correlation(NMATinv);
	outfile<<setw(10)<<"[CorreMat]"<<endl;
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
			outfile<<setw(10)<<CorreMat(i,j)<<"\t";
		outfile<<endl;
	}
	outfile.flush();
	
	img2.Create(CorreMat.GetCols(),CorreMat.GetRows(),8);
	CBMPPixelPtr ptr2(img2);
	
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
		{
			if(fabs(CorreMat(i,j))>0.9) 
			{
				double temp = CorreMat(i,j)-0.9;
				ptr2[i][j] = BYTE(fabs(temp)/0.1*255);
			}
			else ptr2[i][j] = BYTE(0);
		}
	}
	
	CString correpath = resultfile;
	correpath.MakeLower();
	correpath.Replace(".result","_corre.bmp");
	img2.SaveBMP(correpath);

	outfile.close();
	csvfile.close();

	return true;
}

bool CLIDARBiasCalibration::SolveWithND(double LIDAR_PRECISION, const char resultfile[])
{
	CSMMatrix<double> JMAT, KMAT, XMAT, We;	
	CSMMatrix<double> jmat, kmat, bmat, qmat;
	CSMMatrix<double> CMAT, NMAT, NMATinv;
	fstream outfile;
	outfile.open(resultfile,ios::out);
	int i, j;
	bool bStop = true;
	int nIteration = 0;
	int num_unknown = 6;
	double old_sigma;
	
	do{
		//Increase iteration number
		nIteration ++;

		JMAT.Resize(0,0,0.);
		KMAT.Resize(0,0,0.);
		We.Resize(0,0,0.);

		kmat.Resize(1,1,0.);
		jmat.Resize(1,num_unknown,0.);
		bmat.Resize(1,9,0.);
		kmat.Resize(1,1,0.);
		qmat.Resize(9,9,0.);
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////
		
		//Number of patches
		int num_Patches = PatchList.GetNumItem();
		
		//index of no. of rows
		int row_index = 0;
		
		for(i=0; i<num_Patches; i++)
		{
			//To get patch
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			double AREA = patch.Area;

			for(int row=0;row<(int)qmat.GetRows();row++) qmat(row,row) = patch.SD[row]*patch.SD[row];	
			
			int num_points = patch.PointList.GetNumItem();
			
			CSMMatrix<double> IncreaseWe;
			IncreaseWe.Resize(num_points,num_points,0.0);
			We.Insert(We.GetRows(),We.GetRows(),IncreaseWe);

			for(j=0; j<num_points; j++)
			{
				CLIDARRawPoint point = patch.PointList.GetAt(j);
				
				Cal_PDsWithND(point, Config, patch, jmat, bmat, kmat, AREA);
				

				CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
				we_temp = we_temp.Inverse();
				We.Insert(row_index,row_index,we_temp);
						
				JMAT.Insert(row_index,0,jmat);
				
				KMAT.Insert(row_index,0,kmat);
				
				row_index ++;
			}
		}

		//Weight for parameters
		CSMMatrix<double> WParam(6,6,0.);
		WParam(0,0) = 1./Config.sXb/Config.sXb;
		WParam(1,1) = 1./Config.sYb/Config.sYb;
		WParam(2,2) = 1./Config.sZb/Config.sZb;
		WParam(3,3) = 1./Config.sOb/Config.sOb;
		WParam(4,4) = 1./Config.sPb/Config.sPb;
		WParam(5,5) = 1./Config.sKb/Config.sKb;

		//Parameter observations
		We.Insert(row_index,row_index,WParam);

		jmat.Resize(1,6,0.);
		jmat(0,0) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Xb - Config.Xb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,1) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Yb - Config.Yb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,2) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Zb - Config.Zb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,3) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Ob - Config.Ob;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,4) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Pb - Config.Pb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;

		jmat.Resize(1,6,0.);
		jmat(0,5) = 1.;
		JMAT.Insert(row_index,0,jmat);
		kmat(0,0) = OriginConfig.Kb - Config.Kb;
		KMAT.Insert(row_index,0,kmat);
		row_index ++;
		
		//Normal matrix
		NMAT = JMAT.Transpose()%We%JMAT;
		CMAT = JMAT.Transpose()%We%KMAT;

		outfile.precision(10);
		outfile.setf(ios::scientific );
		//outfile<<setw(10)<<"[JMAT]"<<endl;
		/*for(i=0; i<(int)JMAT.GetRows(); i++)
		{
			for(j=0; j<(int)JMAT.GetCols(); j++)
				outfile<<setw(10)<<JMAT(i,j)<<"\t";
			outfile<<endl;
		}*/
		outfile.flush();

		outfile<<setw(10)<<"[KMAT]"<<endl;
		for(i=0; i<(int)KMAT.GetRows(); i++)
		{
			for(j=0; j<(int)KMAT.GetCols(); j++)
				outfile<<setw(10)<<KMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[NMAT]"<<endl;
		for(i=0; i<(int)NMAT.GetRows(); i++)
		{
			for(j=0; j<(int)NMAT.GetCols(); j++)
				outfile<<setw(10)<<NMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[CMAT]"<<endl;
		for(i=0; i<(int)CMAT.GetRows(); i++)
		{
			for(j=0; j<(int)CMAT.GetCols(); j++)
				outfile<<setw(10)<<CMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		NMATinv = NMAT.Inverse();
		//XMAT
		XMAT = NMATinv%CMAT;

		outfile<<setw(10)<<"[NMATinv]"<<endl;
		for(i=0; i<(int)NMATinv.GetRows(); i++)
		{
			for(j=0; j<(int)NMATinv.GetCols(); j++)
				outfile<<setw(10)<<NMATinv(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		outfile<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile<<setw(10)<<XMAT(i,j)<<"\t";
			outfile<<endl;
		}
		outfile.flush();

		CSMMatrix<double> V = JMAT%XMAT - KMAT;

		CSMMatrix<double> VTV = V.Transpose()%We%V;
		double variance = VTV(0,0)/(JMAT.GetRows() - num_unknown);
		double sigma = sqrt(variance);

		outfile<<setw(10)<<"[VMAT]"<<endl;
		/*for(i=0; i<(int)V.GetRows(); i++)
		{
			for(j=0; j<(int)V.GetCols(); j++)
				outfile<<setw(10)<<V(i,j)<<"\t";
			outfile<<endl;
		}*/
		outfile.flush();

		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);
		
		outfile.precision(10);
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<nIteration<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
		
		outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
		outfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
		outfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
		outfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
		outfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<"\t";
		//outfile<<setw(10)<<"Rangeb: "<<ConfigList.GetAt(0).Rangeb<<endl;
		
		outfile<<endl;
		
		//outfile<<setw(10)<<"sigma_Xb(mm), sigma_Yb(mm), sigma_Zb(mm), sigma_Ob(sec), sigma_Pb(sec), sigma_Kb(sec)"<<endl;
		outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
		outfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))*1000<<"\t";
		outfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))*1000<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))*3600<<"\t";
		outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))*3600<<"\t";
		
		outfile<<endl;
		
		outfile.flush();

		if(nIteration > 1)
		{
			if(fabs(old_sigma-sigma)<fabs(LIDAR_PRECISION*0.0000001))
			{
				outfile<<endl<<endl<<"##################################################################"<<endl;
				outfile<<"Iteration Stop(fabs(old_sigma-sigma))): "<<fabs(old_sigma-sigma)<<endl;
				outfile<<"##################################################################"<<endl<<endl;
				bStop = false;
			}
		}

		old_sigma = sigma;

		if(nIteration >= 20)
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}

		if(bStop == false)
		{
			fstream newconfigfile;
			CString configpath = resultfile;
			configpath.MakeLower();
			configpath.Replace(".result",".config");
			newconfigfile.open(configpath,ios::out);
			newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Biases in spatial offsets(3EA, M)"<<endl;
			newconfigfile <<"#Sigma of spatial offsets biases(3EA, M)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
			newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
			newconfigfile <<"#"<<endl;
			newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
			newconfigfile <<"#ID"<<endl;
			newconfigfile <<"####################################################################################################################"<<endl;
			newconfigfile.precision(10);
			newconfigfile.setf(ios::scientific );
			newconfigfile<<setw(10)<<(Config.Xb+Config.Xoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Yb+Config.Yoffset)<<"\t";
			newconfigfile<<setw(10)<<(Config.Zb+Config.Zoffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(0,0))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(1,1))<<"\t";
			newconfigfile<<setw(10)<<sigma*sqrt(NMATinv(2,2))<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(Config.Ob+Config.Ooffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Pb+Config.Poffset)<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(Config.Kb+Config.Koffset)<<endl;
			newconfigfile<<setw(10)<<0.0<<"\t"<<0.0<<"\t"<<0.0<<endl;
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(3,3)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(4,4)))<<"\t";
			newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMATinv(5,5)))<<endl;
			newconfigfile<<setw(10)<<Config.Rangeb<<"\t"<<Config.sRangeb<<endl;
			newconfigfile<<setw(10)<<"1";
			newconfigfile.close();
		}
		
	}while(bStop == true);

	CBMPImage img;
	img.Create(NMAT.GetCols(),NMAT.GetRows(),8);
	CBMPPixelPtr ptr(img);
	for(i=0; i<(int)NMAT.GetRows(); i++)
	{
		for(j=0; j<(int)NMAT.GetCols(); j++) 
		{
			if(NMAT(i,j)==0.) ptr[i][j] = BYTE(255);
			else ptr[i][j] = BYTE(0);
		}
	}
	
	CString nimgpath = resultfile;
	nimgpath.MakeLower();
	nimgpath.Replace(".result","_N.bmp");
	img.SaveBMP(nimgpath);
	
	CBMPImage img2;
	CSMMatrix<double> CorreMat;
	CorreMat = Correlation(NMATinv);
	outfile<<setw(10)<<"[CorreMat]"<<endl;
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
			outfile<<setw(10)<<CorreMat(i,j)<<"\t";
		outfile<<endl;
	}
	outfile.flush();
	img2.Create(CorreMat.GetCols(),CorreMat.GetRows(),8);
	CBMPPixelPtr ptr2(img2);
	
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		if(fabs(CorreMat(i,j))>0.9) 
		{
			double temp = CorreMat(i,j)-0.9;
			ptr2[i][j] = BYTE(fabs(temp)/0.1*255);
		}
		else ptr2[i][j] = BYTE(0);
	}
	
	CString correpath = resultfile;
	correpath.MakeLower();
	correpath.Replace(".result","_corre.bmp");
	img2.SaveBMP(correpath);

	outfile.close();

	return true;
}

void CLIDARBiasCalibration::CalPlaneCoeff(double* points, int num_points, double* coeff)
{
	CSMMatrix<double> A, L, X;
	A.Resize(num_points,3);
	L.Resize(num_points,1);
	
	for(int i=0; i<num_points; i++)
	{
		A(i,0) = points[i*3+0];
		A(i,1) = points[i*3+1];
		A(i,2) = points[i*3+2];
		
		L(i,0) = -1;
	}
	
	X = (A.Transpose()%A).Inverse()%A.Transpose()%L;
	
	coeff[0] = X(0,0);
	coeff[1] = X(1,0);
	coeff[2] = X(2,0);
}

void CLIDARBiasCalibration::CalPlaneCoeff_New(double* points, int num_points, double* coeff)
{
	//aX + bY + Z + c = 0

	CSMMatrix<double> A, L, X, B, Q;
	A.Resize(num_points,3);
	L.Resize(num_points,1);
	B.Resize(num_points,num_points*3);
	Q.Resize(num_points*3,num_points*3);
	
	for(int i=0; i<num_points; i++)
	{
		A(i,0) = points[i*3+0];
		A(i,1) = points[i*3+1];
		//A(i,2) = points[i*3+2];
		A(i,2) = 1.0;

		L(i,0) = -points[i*3+2];
	}
	
	X = (A.Transpose()%A).Inverse()%A.Transpose()%L;
	
	coeff[0] = X(0,0);
	coeff[1] = X(1,0);
	coeff[2] = X(2,0);

	for(int i=0; i<num_points; i++)
	{
		B(i, i*3 + 0) = coeff[0];
		B(i, i*3 + 1) = coeff[1];
		B(i, i*3 + 2) = 1.0;

		//Q(i*3+0,i*3+0) = 1.;
		//Q(i*3+1,i*3+1) = 1.;
		//Q(i*3+2,i*3+2) = 1.;
	}

	Q.Identity();

	CSMMatrix<double> bqbt = B%Q%B.Transpose();
	CSMMatrix<double> W = bqbt.Inverse();
	X = (A.Transpose()%W%A).Inverse()%A.Transpose()%W%L;

	coeff[0] = X(0,0);
	coeff[1] = X(1,0);
	coeff[2] = X(2,0);
}

CSMMatrix<double> CLIDARBiasCalibration::LIDAREQ(CLIDARRawPoint point, CLIDARConfig config)
{
	CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging(3,1,0.);
	
	X0(0,0) = point.GPS_X; X0(1,0) = point.GPS_Y; X0(2,0) = point.GPS_Z;
	P(0,0) = config.Xoffset; P(1,0) = config.Yoffset; P(2,0) = config.Zoffset;
	Pb(0,0) = config.Xb; Pb(1,0) = config.Yb; Pb(2,0) = config.Zb;
	
	Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = -(config.RangeS*point.dist + config.Rangeb);
	
	CRotationcoeff2 INS((point.INS_O), (point.INS_P), (point.INS_K));
	CRotationcoeff2 Swing((point.alpha+config.Alpha_b), (point.beta+config.Beta_b), 0.);
	CRotationcoeff2 ROffset((config.Ooffset+config.Ob), (config.Poffset+config.Pb), (config.Koffset+config.Kb));
	
	CSMMatrix<double> XG = X0 + INS.Rmatrix%(P + Pb) 
		+ INS.Rmatrix%ROffset.Rmatrix%Swing.Rmatrix%Ranging;

	return XG;
}

CSMMatrix<double> CLIDARBiasCalibration::LIDAREQ_Brazil1(CLIDARRawPoint point, CLIDARConfig config)
{
	CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging(3,1,0.);
	
	X0(0,0) = point.GPS_X; X0(1,0) = point.GPS_Y; X0(2,0) = point.GPS_Z;
	P(0,0) = config.Xoffset; P(1,0) = config.Yoffset; P(2,0) = config.Zoffset;
	Pb(0,0) = config.Xb; Pb(1,0) = config.Yb; Pb(2,0) = config.Zb;
	
	Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = (config.RangeS*point.dist + config.Rangeb);
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//
	
	_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, point.INS_K);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(0,Deg2Rad(180),0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS; INS.Rmatrix = INS2G.Rmatrix	    %	(Rollmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Yawmat.Rmatrix);

	CRotationcoeff2 Swing((point.alpha+config.Alpha_b), (-point.beta-config.Beta_b), 0.);
	CRotationcoeff2 ROffset((config.Ooffset+config.Ob), (config.Poffset+config.Pb), (config.Koffset+config.Kb));
	
	CSMMatrix<double> XG = X0 + INS.Rmatrix%((P + Pb) + ROffset.Rmatrix%Swing.Rmatrix%Ranging);

	return XG;
}

CSMMatrix<double> CLIDARBiasCalibration::LIDAREQ_Point_Titan(CLIDARRawPoint point, CLIDARConfig config)
{
	CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging(3,1,0.);
	
	X0(0,0) = point.GPS_X; X0(1,0) = point.GPS_Y; X0(2,0) = point.GPS_Z;
	P(0,0) = config.Xoffset; P(1,0) = config.Yoffset; P(2,0) = config.Zoffset;
	Pb(0,0) = config.Xb; Pb(1,0) = config.Yb; Pb(2,0) = config.Zb;
	
	Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = (config.RangeS*point.dist + config.Rangeb);
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//
	
	_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, point.INS_K);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(0,Deg2Rad(180),0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS; INS.Rmatrix = INS2G.Rmatrix	    %	(Yawmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix);

	CRotationcoeff2 Swing((point.alpha+config.Alpha_b), (-point.beta-config.Beta_b), 0.);

	//
	//Rotational offset
	//
	CRotationcoeff2 ROffset;
	_Rmat_ Rmat	(0,		(config.Poffset+config.Pb),		0);//rotation matrix (roll)
	_Rmat_ Pmat	((config.Ooffset+config.Ob),	0,		0);//rotation matrix (pitch)
	_Rmat_ Ymat	(0,		0,		(config.Koffset+config.Kb));//rotation matrix (yaw)
	ROffset.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;
	
	CSMMatrix<double> XG = X0 + INS.Rmatrix%((P + Pb) + ROffset.Rmatrix%Swing.Rmatrix%Ranging);

	return XG;
}

CSMMatrix<double> CLIDARBiasCalibration::LIDAREQ_Brazil2(CLIDARRawPoint point, CLIDARConfig config)
{
	CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging(3,1,0.);
	
	X0(0,0) = point.GPS_X; X0(1,0) = point.GPS_Y; X0(2,0) = point.GPS_Z;
	P(0,0) = config.Xoffset; P(1,0) = config.Yoffset; P(2,0) = config.Zoffset;
	Pb(0,0) = config.Xb; Pb(1,0) = config.Yb; Pb(2,0) = config.Zb;
	
	Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = (config.RangeS*point.dist + config.Rangeb);
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//
	
	_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, point.INS_K);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(0,Deg2Rad(180),0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS; INS.Rmatrix = INS2G.Rmatrix	    %	(Yawmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix);

	CRotationcoeff2 Swing((point.alpha+config.Alpha_b), (-point.beta-config.Beta_b), 0.);
	CRotationcoeff2 ROffset((config.Ooffset+config.Ob), (config.Poffset+config.Pb), (config.Koffset+config.Kb));
	
	CSMMatrix<double> XG = X0 + INS.Rmatrix%((P + Pb) + ROffset.Rmatrix%Swing.Rmatrix%Ranging);

	return XG;
}

CSMMatrix<double> CLIDARBiasCalibration::LIDAREQ_Brazil3(CLIDARRawPoint point, CLIDARConfig config)
{
	CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging(3,1,0.);
	
	X0(0,0) = point.GPS_X; X0(1,0) = point.GPS_Y; X0(2,0) = point.GPS_Z;
	P(0,0) = config.Xoffset; P(1,0) = config.Yoffset; P(2,0) = config.Zoffset;
	Pb(0,0) = config.Xb; Pb(1,0) = config.Yb; Pb(2,0) = config.Zb;
	
	Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = (config.RangeS*point.dist + config.Rangeb);
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//
	
	_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, point.INS_K);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(Deg2Rad(180), 0, 0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS; INS.Rmatrix = INS2G.Rmatrix	    %	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix)
															%	(Yawmat.Rmatrix);

	CRotationcoeff2 Swing((point.alpha+config.Alpha_b), (point.beta + config.Beta_b), 0.);
	CRotationcoeff2 ROffset((config.Ooffset+config.Ob), (config.Poffset+config.Pb), (config.Koffset+config.Kb));
	
	CSMMatrix<double> XG = X0 + INS.Rmatrix%((P + Pb) + ROffset.Rmatrix%Swing.Rmatrix%Ranging);

	return XG;
}

void CLIDARBiasCalibration::Cal_PDs(CLIDARRawPoint point, CLIDARConfig config, double* coeff, CSMMatrix<double> &jmat)
{
	//aX + bY + cZ + 1 = 0

	CRotationcoeff2 INS_R; //INS Rotation matrix: R matrix
	INS_R.ReMake(point.INS_O, point.INS_P, point.INS_K);
	
	//offset angle + bias angle
	double dO = config.Ooffset + config.Ob;
	double dP = config.Poffset + config.Pb;
	double dK = config.Koffset + config.Kb;
	
	//partial derivatives for angle biases (omega)
	CSMMatrix<double> dOffsetR_dO(3,3,0.);
	dOffsetR_dO(0,0) = 0;	
	dOffsetR_dO(0,1) = 0;	
	dOffsetR_dO(0,2) = 0;
	
	dOffsetR_dO(1,0) = cos(dO)*sin(dP)*cos(dK) - sin(dO)*sin(dK);	
	dOffsetR_dO(1,1) = -cos(dO)*sin(dP)*sin(dK) - sin(dO)*cos(dK);	
	dOffsetR_dO(1,2) = -cos(dO)*cos(dP);
	
	dOffsetR_dO(2,0) = sin(dO)*sin(dP)*cos(dK) + cos(dO)*sin(dK);	
	dOffsetR_dO(2,1) = -sin(dO)*sin(dP)*sin(dK) + cos(dO)*cos(dK);	
	dOffsetR_dO(2,2) = -sin(dO)*cos(dP);
	
	//partial derivatives for angle biases (phi)
	CSMMatrix<double> dOffsetR_dP(3,3,0.);
	dOffsetR_dP(0,0) = -sin(dP)*cos(dK);	
	dOffsetR_dP(0,1) = sin(dP)*sin(dK);	
	dOffsetR_dP(0,2) = cos(dP);
	
	dOffsetR_dP(1,0) = sin(dO)*cos(dP)*cos(dK);	
	dOffsetR_dP(1,1) = -sin(dO)*cos(dP)*sin(dK);	
	dOffsetR_dP(1,2) = sin(dO)*sin(dP);
	
	dOffsetR_dP(2,0) = -cos(dO)*cos(dP)*cos(dK);	
	dOffsetR_dP(2,1) = cos(dO)*cos(dP)*sin(dK);	
	dOffsetR_dP(2,2) = -cos(dO)*sin(dP);
	
	//partial derivatives for angle biases (kappa)
	CSMMatrix<double> dOffsetR_dK(3,3,0.);
	dOffsetR_dK(0,0) = -cos(dP)*sin(dK);	
	dOffsetR_dK(0,1) = -cos(dP)*cos(dK);	
	dOffsetR_dK(0,2) = 0.;
	
	dOffsetR_dK(1,0) = -sin(dO)*sin(dP)*sin(dK) + cos(dO)*cos(dK);	
	dOffsetR_dK(1,1) = -sin(dO)*sin(dP)*cos(dK) - cos(dO)*sin(dK);	
	dOffsetR_dK(1,2) = 0.;
	
	dOffsetR_dK(2,0) = cos(dO)*sin(dP)*sin(dK) + sin(dO)*cos(dK);	
	dOffsetR_dK(2,1) = cos(dO)*sin(dP)*cos(dK) - sin(dO)*sin(dK);	
	dOffsetR_dK(2,2) = 0.;
	
	//ranging distance + distance bias
	double dist = point.dist + config.Rangeb;
	
	//elements of each row in INS rotation matrix
	CSMMatrix<double> INS_R1, INS_R2, INS_R3;
	INS_R1 = INS_R.Rmatrix.Subset(0,0,1,3);
	INS_R2 = INS_R.Rmatrix.Subset(1,0,1,3);
	INS_R3 = INS_R.Rmatrix.Subset(2,0,1,3);
	
	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(point.beta);
	SwingR(1,0) = -sin(point.alpha)*cos(point.beta);
	SwingR(2,0) = cos(point.alpha)*cos(point.beta);
	
	//INS rotation * boresight rotation
	CRotationcoeff2 RB(dO, dP, dK); 
	//INS rotation * boresight rotation * swing matrix * -(ranging distance + distance bias)
	CSMMatrix<double> R2 = INS_R.Rmatrix%RB.Rmatrix%SwingR*(-dist);
	
	//Partial derivatives
	double dXG_dXpbias = INS_R.Rmatrix(0,0);
	double dXG_dYpbias = INS_R.Rmatrix(0,1);
	double dXG_dZpbias = INS_R.Rmatrix(0,2);
	double dXG_dObias = -(INS_R1%dOffsetR_dO%SwingR*dist)(0,0);
	double dXG_dPbias = -(INS_R1%dOffsetR_dP%SwingR*dist)(0,0);
	double dXG_dKbias = -(INS_R1%dOffsetR_dK%SwingR*dist)(0,0);
	double dXG_drangebias = R2(0,0);
	
	double dYG_dXpbias = INS_R.Rmatrix(1,0);
	double dYG_dYpbias = INS_R.Rmatrix(1,1);
	double dYG_dZpbias = INS_R.Rmatrix(1,2);
	double dYG_dObias = -(INS_R2%dOffsetR_dO%SwingR*dist)(0,0);
	double dYG_dPbias = -(INS_R2%dOffsetR_dP%SwingR*dist)(0,0);
	double dYG_dKbias = -(INS_R2%dOffsetR_dK%SwingR*dist)(0,0);
	double dYG_drangebias = R2(1,0);
	
	double dZG_dXpbias = INS_R.Rmatrix(2,0);
	double dZG_dYpbias = INS_R.Rmatrix(2,1);
	double dZG_dZpbias = INS_R.Rmatrix(2,2);
	double dZG_dObias = -(INS_R3%dOffsetR_dO%SwingR*dist)(0,0);
	double dZG_dPbias = -(INS_R3%dOffsetR_dP%SwingR*dist)(0,0);
	double dZG_dKbias = -(INS_R3%dOffsetR_dK%SwingR*dist)(0,0);
	double dZG_drangebias = R2(2,0);
	
	//Make jmat matrix for a each LIDAR raw point
	jmat(0,0) = coeff[0]*dXG_dXpbias + coeff[1]*dYG_dXpbias + coeff[2]*dZG_dXpbias;
	jmat(0,1) = coeff[0]*dXG_dYpbias + coeff[1]*dYG_dYpbias + coeff[2]*dZG_dYpbias;
	jmat(0,2) = coeff[0]*dXG_dZpbias + coeff[1]*dYG_dZpbias + coeff[2]*dZG_dZpbias;
	
	jmat(0,3) = coeff[0]*dXG_dObias + coeff[1]*dYG_dObias + coeff[2]*dZG_dObias;
	jmat(0,4) = coeff[0]*dXG_dPbias + coeff[1]*dYG_dPbias + coeff[2]*dZG_dPbias;
	jmat(0,5) = coeff[0]*dXG_dKbias + coeff[1]*dYG_dKbias + coeff[2]*dZG_dKbias;
	
	//jmat(0,6) = coeff[0]*dXG_drangebias + coeff[1]*dYG_drangebias + coeff[2]*dZG_drangebias;
}

void CLIDARBiasCalibration::Cal_PDs_New(CLIDARRawPoint point, CLIDARConfig config, double* coeff, CSMMatrix<double> &jmat)
{
	//aX + bY + Z + c = 0

	//0=aX+bY+cZ+1
	//D = AX + BY + CZ
	//dist = fabs(AXc + BYc +CZc -D)/sqrt(A^2+B^2+C^2)
	//double A=a, B=b, C=1, D=-c;
	//double dist = fabs(A*XG(0,0) + B*XG(1,0) + C*XG(2,0) - D)/sqrt(A*A+B*B+C*C);


	CRotationcoeff2 INS_R; //INS Rotation matrix: R matrix
	INS_R.ReMake(point.INS_O, point.INS_P, point.INS_K);
	
	//offset angle + bias angle
	double dO = config.Ooffset + config.Ob;
	double dP = config.Poffset + config.Pb;
	double dK = config.Koffset + config.Kb;
	
	//partial derivatives for angle biases (omega)
	CSMMatrix<double> dOffsetR_dO(3,3,0.);
	dOffsetR_dO(0,0) = 0;	
	dOffsetR_dO(0,1) = 0;	
	dOffsetR_dO(0,2) = 0;
	
	dOffsetR_dO(1,0) = cos(dO)*sin(dP)*cos(dK) - sin(dO)*sin(dK);	
	dOffsetR_dO(1,1) = -cos(dO)*sin(dP)*sin(dK) - sin(dO)*cos(dK);	
	dOffsetR_dO(1,2) = -cos(dO)*cos(dP);
	
	dOffsetR_dO(2,0) = sin(dO)*sin(dP)*cos(dK) + cos(dO)*sin(dK);	
	dOffsetR_dO(2,1) = -sin(dO)*sin(dP)*sin(dK) + cos(dO)*cos(dK);	
	dOffsetR_dO(2,2) = -sin(dO)*cos(dP);
	
	//partial derivatives for angle biases (phi)
	CSMMatrix<double> dOffsetR_dP(3,3,0.);
	dOffsetR_dP(0,0) = -sin(dP)*cos(dK);	
	dOffsetR_dP(0,1) = sin(dP)*sin(dK);	
	dOffsetR_dP(0,2) = cos(dP);
	
	dOffsetR_dP(1,0) = sin(dO)*cos(dP)*cos(dK);	
	dOffsetR_dP(1,1) = -sin(dO)*cos(dP)*sin(dK);	
	dOffsetR_dP(1,2) = sin(dO)*sin(dP);
	
	dOffsetR_dP(2,0) = -cos(dO)*cos(dP)*cos(dK);	
	dOffsetR_dP(2,1) = cos(dO)*cos(dP)*sin(dK);	
	dOffsetR_dP(2,2) = -cos(dO)*sin(dP);
	
	//partial derivatives for angle biases (kappa)
	CSMMatrix<double> dOffsetR_dK(3,3,0.);
	dOffsetR_dK(0,0) = -cos(dP)*sin(dK);	
	dOffsetR_dK(0,1) = -cos(dP)*cos(dK);	
	dOffsetR_dK(0,2) = 0.;
	
	dOffsetR_dK(1,0) = -sin(dO)*sin(dP)*sin(dK) + cos(dO)*cos(dK);	
	dOffsetR_dK(1,1) = -sin(dO)*sin(dP)*cos(dK) - cos(dO)*sin(dK);	
	dOffsetR_dK(1,2) = 0.;
	
	dOffsetR_dK(2,0) = cos(dO)*sin(dP)*sin(dK) + sin(dO)*cos(dK);	
	dOffsetR_dK(2,1) = cos(dO)*sin(dP)*cos(dK) - sin(dO)*sin(dK);	
	dOffsetR_dK(2,2) = 0.;
	
	//ranging distance + distance bias
	double dist = point.dist + config.Rangeb;
	
	//elements of each row in INS rotation matrix
	CSMMatrix<double> INS_R1, INS_R2, INS_R3;
	INS_R1 = INS_R.Rmatrix.Subset(0,0,1,3);
	INS_R2 = INS_R.Rmatrix.Subset(1,0,1,3);
	INS_R3 = INS_R.Rmatrix.Subset(2,0,1,3);
	
	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(point.beta);
	SwingR(1,0) = -sin(point.alpha)*cos(point.beta);
	SwingR(2,0) = cos(point.alpha)*cos(point.beta);
	
	//INS rotation * boresight rotation
	CRotationcoeff2 RB(dO, dP, dK); 
	//INS rotation * boresight rotation * swing matrix * -(ranging distance + distance bias)
	CSMMatrix<double> R2 = INS_R.Rmatrix%RB.Rmatrix%SwingR*(-dist);
	
	//Partial derivatives
	double dXG_dXpbias = INS_R.Rmatrix(0,0);
	double dXG_dYpbias = INS_R.Rmatrix(0,1);
	double dXG_dZpbias = INS_R.Rmatrix(0,2);
	double dXG_dObias = -(INS_R1%dOffsetR_dO%SwingR*dist)(0,0);
	double dXG_dPbias = -(INS_R1%dOffsetR_dP%SwingR*dist)(0,0);
	double dXG_dKbias = -(INS_R1%dOffsetR_dK%SwingR*dist)(0,0);
	double dXG_drangebias = R2(0,0);
	
	double dYG_dXpbias = INS_R.Rmatrix(1,0);
	double dYG_dYpbias = INS_R.Rmatrix(1,1);
	double dYG_dZpbias = INS_R.Rmatrix(1,2);
	double dYG_dObias = -(INS_R2%dOffsetR_dO%SwingR*dist)(0,0);
	double dYG_dPbias = -(INS_R2%dOffsetR_dP%SwingR*dist)(0,0);
	double dYG_dKbias = -(INS_R2%dOffsetR_dK%SwingR*dist)(0,0);
	double dYG_drangebias = R2(1,0);
	
	double dZG_dXpbias = INS_R.Rmatrix(2,0);
	double dZG_dYpbias = INS_R.Rmatrix(2,1);
	double dZG_dZpbias = INS_R.Rmatrix(2,2);
	double dZG_dObias = -(INS_R3%dOffsetR_dO%SwingR*dist)(0,0);
	double dZG_dPbias = -(INS_R3%dOffsetR_dP%SwingR*dist)(0,0);
	double dZG_dKbias = -(INS_R3%dOffsetR_dK%SwingR*dist)(0,0);
	double dZG_drangebias = R2(2,0);
	
	

	//Make jmat matrix for a each LIDAR raw point
	//	jmat(0,0) = coeff[0]*dXG_dXpbias + coeff[1]*dYG_dXpbias + dZG_dXpbias;
	//	jmat(0,1) = coeff[0]*dXG_dYpbias + coeff[1]*dYG_dYpbias + dZG_dYpbias;
	//	jmat(0,2) = coeff[0]*dXG_dZpbias + coeff[1]*dYG_dZpbias + dZG_dZpbias;
	//	
	//	jmat(0,3) = coeff[0]*dXG_dObias + coeff[1]*dYG_dObias + dZG_dObias;
	//	jmat(0,4) = coeff[0]*dXG_dPbias + coeff[1]*dYG_dPbias + dZG_dPbias;
	//	jmat(0,5) = coeff[0]*dXG_dKbias + coeff[1]*dYG_dKbias + dZG_dKbias;


	//0=aX+bY+cZ+1
	//D = AX + BY + CZ
	//double dist = fabs(A*XG(0,0) + B*XG(1,0) + C*XG(2,0) - D)/sqrt(A*A+B*B+C*C);
	double A=coeff[0], B=coeff[1], C=1., D=-coeff[2];
	double temp = sqrt(A*A+B*B+C*C);

	if(temp==0) return;

	jmat(0,0) = (A*(dXG_dXpbias) + B*(dYG_dXpbias) + C*(dZG_dXpbias))/temp;
	jmat(0,1) = (A*(dXG_dYpbias) + B*(dYG_dYpbias) + C*(dZG_dYpbias))/temp;
	jmat(0,2) = (A*(dXG_dZpbias) + B*(dYG_dZpbias) + C*(dZG_dZpbias))/temp;
	
	jmat(0,3) = (A*(dXG_dObias)  + B*(dYG_dObias)  + C*(dZG_dObias))/temp;
	jmat(0,4) = (A*(dXG_dPbias)  + B*(dYG_dPbias)  + C*(dZG_dPbias))/temp;
	jmat(0,5) = (A*(dXG_dKbias)  + B*(dYG_dKbias)  + C*(dZG_dKbias))/temp;
	
	//jmat(0,6) = coeff[0]*dXG_drangebias + coeff[1]*dYG_drangebias + coeff[2]*dZG_drangebias;
}

void CLIDARBiasCalibration::Cal_PDsWithVolume(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat)
{
	double XP, YP, ZP;
	
	CSMMatrix<double> X0 = LIDAREQ(point, Config);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	//ranging distance + distance bias
	double dist = point.dist*config.RangeS + config.Rangeb;

	CSMMatrix<double> Rangevector(3,1);
	Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= -dist;

	//INS angle
	double INS_O=point.INS_O;
	double INS_P=point.INS_P;
	double INS_K=point.INS_K;

	CRotationcoeff2 INS_R; //INS Rotation matrix: R matrix
	INS_R.ReMake(INS_O, INS_P, INS_K);

	//partial derivatives for INS angle
	CRotationcoeff2 dR_dO;
	dR_dO.Partial_dRdO(INS_O, INS_P, INS_K);

	CRotationcoeff2 dR_dP;
	dR_dP.Partial_dRdP(INS_O, INS_P, INS_K);

	CRotationcoeff2 dR_dK;
	dR_dK.Partial_dRdK(INS_O, INS_P, INS_K);

	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(point.beta + config.Beta_b);
	SwingR(1,0) = -sin(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);
	SwingR(2,0) = cos(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);

	//partial derivatives for scan angle
	CRotationcoeff2 AlphaR(point.alpha,0,0);
	CRotationcoeff2 AlphaOffsetR(config.Alpha_b,0,0);
	CRotationcoeff2 BetaR(0,point.beta,0);
	CRotationcoeff2 BetaOffsetR(0,config.Beta_b,0);
	CRotationcoeff2 dAlphaR_dAlpha; dAlphaR_dAlpha.Partial_dRdO(point.alpha, 0, 0);
	CRotationcoeff2 dBetaR_dBeta; dBetaR_dBeta.Partial_dRdP(0, point.beta, 0);
	CRotationcoeff2 dAlphaBiasR_dAlphaOffset; dAlphaBiasR_dAlphaOffset.Partial_dRdO(config.Alpha_b,0, 0);
	CRotationcoeff2 dBetaBiasR_dBetaOffset; dBetaBiasR_dBetaOffset.Partial_dRdP(0,config.Beta_b,0);

	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
	CSMMatrix<double> R2 = BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R3 = R1%AlphaOffsetR.Rmatrix%BetaR.Rmatrix;
	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
	CSMMatrix<double> R5 = AlphaOffsetR.Rmatrix%BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R6 = R4%AlphaR.Rmatrix%AlphaOffsetR.Rmatrix;

	CSMMatrix<double> df1_dAlpha_offset = R1%dAlphaBiasR_dAlphaOffset.Rmatrix%R2%Rangevector;
	CSMMatrix<double> df2_dBeta_offset = R3%dBetaBiasR_dBetaOffset.Rmatrix%Rangevector;
	CSMMatrix<double> df3_dAlpha = R4%dAlphaR_dAlpha.Rmatrix%R5%Rangevector;
	CSMMatrix<double> df4_dBeta = R6%dBetaR_dBeta.Rmatrix%BetaOffsetR.Rmatrix%Rangevector;
	
	CSMMatrix<double> Lmat = R4%SwingR;

	CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%SwingR)*(-dist));
	CSMMatrix<double> dFdO = K123%dR_dO.Rmatrix%temp;
	CSMMatrix<double> dFdP = K123%dR_dP.Rmatrix%temp;
	CSMMatrix<double> dFdK = K123%dR_dK.Rmatrix%temp;

	/////////////////////////////
	//B Matrix
	/////////////////////////////
	//GNSS obs
	bmat(0,0) = K1;
	bmat(0,1) = K2;
	bmat(0,2) = K3;
	//INS obs
	bmat(0,3) = dFdO(0,0);
	bmat(0,4) = dFdP(0,0);
	bmat(0,5) = dFdK(0,0);
	//swing angle
	bmat(0,6) = (K123%df3_dAlpha)(0,0);//K1*df3_dAlpha(0,0) + K2*df3_dAlpha(1,0) + K3*df3_dAlpha(2,0);
	bmat(0,7) = (K123%df4_dBeta)(0,0);//K1*df4_dBeta(0,0) + K2*df4_dBeta(1,0) + K3*df4_dBeta(2,0);
	//ranging distance
	bmat(0,8) = -((K123%Lmat)*config.RangeS)(0,0);//-K1*config.RangeS*Mrange(0,0) - K2*config.RangeS*Mrange(1,0) - K3*config.RangeS*Mrange(2,0);

	/////////////////////////////
	//J Matrix
	/////////////////////////////
	//boresight factors (bias_X, bias_Y, bias_Z)
	//Partial derivatives
	CSMMatrix<double> KR = K123%INS_R.Rmatrix;
	jmat_lidar(0,0) = KR(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dXpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dXpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dXpbias;
	jmat_lidar(0,1) = KR(0,1);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dYpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dYpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dYpbias;
	jmat_lidar(0,2) = KR(0,2);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dZpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dZpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dZpbias;
	
	//rotational alignment (bias_O, bias_P, bias_K)
	jmat_lidar(0,3) = ((K123%INS_R.Rmatrix%dOffsetR_dO.Rmatrix%SwingR)*(-dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dObias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dObias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dObias;
	jmat_lidar(0,4) = ((K123%INS_R.Rmatrix%dOffsetR_dP.Rmatrix%SwingR)*(-dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dPbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dPbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dPbias;
	jmat_lidar(0,5) = ((K123%INS_R.Rmatrix%dOffsetR_dK.Rmatrix%SwingR)*(-dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dKbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dKbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dKbias;

	//scan angle offset
	jmat_lidar(0,6) = (K123%df1_dAlpha_offset)(0,0);
	jmat_lidar(0,7) = (K123%df2_dBeta_offset)(0,0);

	//laser ranging offset
	jmat_lidar(0,8) = ((K123%Lmat)*-1.0)(0,0);
	//laser ranging scale
	jmat_lidar(0,9) = ((K123%Lmat)*-point.dist)(0,0);

	//vertex points
	//vertex A
	jmat_vertexA(0,0) = (ZC - ZB)*YP + (YB - YC)*ZP + (YC*ZB - YB*ZC);
	jmat_vertexA(1,1) = (ZB - ZC)*XP + (XC - XB)*ZP + (XB*ZC - XC*ZB);
	jmat_vertexA(2,2) = (YC - YB)*XP + (XB - XC)*YP + (XC*YB - XB*YC);

	//vertex B
	jmat_vertexB(0,0) = (ZA - ZC)*YP + (YC - YA)*ZP + (YA*ZC - ZA*YC);
	jmat_vertexB(1,1) = (ZC - ZA)*XP + (XA - XC)*ZP + (ZA*XC - XA*ZC);
	jmat_vertexB(2,2) = (YA - YC)*XP + (XC - XA)*YP + (XA*YC - YA*XC);

	//vertex C
	jmat_vertexC(0,0) = (ZB - ZA)*YP  + (YA - YB)*ZP + (ZA*YB - YA*ZB);
	jmat_vertexC(1,1) = (ZA - ZB)*XP  + (XB - XA)*ZP + (XA*ZB - ZA*XB);
	jmat_vertexC(2,2) = (YB - YA)*XP + (XA - XB)*YP + (YA*XB - XA*YB);

	double K0 = XP*(YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC-YC*ZB))
		       -YP*(XA*(ZB-ZC) - ZA*(XB-XC) + (XB*ZC-XC*ZB))
			   +ZP*(XA*(YB-YC) - YA*(XB-XC) + (XB*YC-XC*YB))
			   -1.*(XA*(YB*ZC-YC*ZB) - YA*(XB*ZC-XC*ZB) + ZA*(XB*YC-XC*YB));
	
	kmat(0,0) = -K0;
}

void CLIDARBiasCalibration::Cal_PDsWithVolume_Brazil1(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat)
{
	double XP, YP, ZP;
	
	CSMMatrix<double> X0 = LIDAREQ_Brazil1(point, Config);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	//ranging distance + distance bias
	double dist = point.dist*config.RangeS + config.Rangeb;

	CSMMatrix<double> Rangevector(3,1);
	Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= dist;

	//INS angle
	double INS_Pitch=point.INS_O;
	double INS_Roll=point.INS_P;
	double INS_Yaw=point.INS_K;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//

	point.beta *= -1.0;
	
	_Rmat_ Rollmat	(0, INS_Roll,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(INS_Pitch,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, INS_Yaw);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(0, Deg2Rad(180), 0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS_R; INS_R.Rmatrix = INS2G.Rmatrix	%	(Rollmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Yawmat.Rmatrix);

	//partial derivatives for INS angle
	CRotationcoeff2 dR_dPitch;
	dR_dPitch.Partial_dRdO(INS_Pitch, 0, 0);
	dR_dPitch.Rmatrix = Rollmat.Rmatrix % dR_dPitch.Rmatrix % Yawmat.Rmatrix;

	CRotationcoeff2 dR_dRoll;
	dR_dRoll.Partial_dRdP(0, INS_Roll, 0);
	dR_dRoll.Rmatrix = dR_dRoll.Rmatrix % Pitchmat.Rmatrix % Yawmat.Rmatrix;

	CRotationcoeff2 dR_dYaw;
	dR_dYaw.Partial_dRdK(0, 0, INS_Yaw);
	dR_dYaw.Rmatrix = Rollmat.Rmatrix % Pitchmat.Rmatrix % dR_dYaw.Rmatrix;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(point.beta + config.Beta_b);
	SwingR(1,0) = -sin(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);
	SwingR(2,0) = cos(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);

	//partial derivatives for scan angle
	CRotationcoeff2 AlphaR(point.alpha,0,0);
	CRotationcoeff2 AlphaOffsetR(config.Alpha_b,0,0);
	CRotationcoeff2 BetaR(0,point.beta,0);
	CRotationcoeff2 BetaOffsetR(0,config.Beta_b,0);
	CRotationcoeff2 dAlphaR_dAlpha; dAlphaR_dAlpha.Partial_dRdO(point.alpha, 0, 0);
	CRotationcoeff2 dBetaR_dBeta; dBetaR_dBeta.Partial_dRdP(0, point.beta, 0);
	CRotationcoeff2 dAlphaBiasR_dAlphaOffset; dAlphaBiasR_dAlphaOffset.Partial_dRdO(config.Alpha_b,0, 0);
	CRotationcoeff2 dBetaBiasR_dBetaOffset; dBetaBiasR_dBetaOffset.Partial_dRdP(0,config.Beta_b,0);

	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
	CSMMatrix<double> R2 = BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R3 = R1%AlphaOffsetR.Rmatrix%BetaR.Rmatrix;
	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
	CSMMatrix<double> R5 = AlphaOffsetR.Rmatrix%BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R6 = R4%AlphaR.Rmatrix%AlphaOffsetR.Rmatrix;

	CSMMatrix<double> df1_dAlpha_offset = R1%dAlphaBiasR_dAlphaOffset.Rmatrix%R2%Rangevector;
	CSMMatrix<double> df2_dBeta_offset = R3%dBetaBiasR_dBetaOffset.Rmatrix%Rangevector;
	CSMMatrix<double> df3_dAlpha = R4%dAlphaR_dAlpha.Rmatrix%R5%Rangevector;
	CSMMatrix<double> df4_dBeta = R6%dBetaR_dBeta.Rmatrix%BetaOffsetR.Rmatrix%Rangevector;
	
	CSMMatrix<double> Lmat = R4%SwingR;

	CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%SwingR)*(dist));
	CSMMatrix<double> dFdPitch	= K123%dR_dPitch.Rmatrix	%temp;
	CSMMatrix<double> dFdRoll	= K123%dR_dRoll.Rmatrix		%temp;
	CSMMatrix<double> dFdYaw	= K123%dR_dYaw.Rmatrix		%temp;

	/////////////////////////////
	//B Matrix
	/////////////////////////////
	//GNSS obs
	bmat(0,0) = K1;
	bmat(0,1) = K2;
	bmat(0,2) = K3;
	//INS obs
	bmat(0,3) = dFdPitch(0,0);
	bmat(0,4) = dFdRoll(0,0);
	bmat(0,5) = dFdYaw(0,0);
	//swing angle
	bmat(0,6) = (K123%df3_dAlpha)(0,0);//K1*df3_dAlpha(0,0) + K2*df3_dAlpha(1,0) + K3*df3_dAlpha(2,0);
	bmat(0,7) = (K123%df4_dBeta)(0,0);//K1*df4_dBeta(0,0) + K2*df4_dBeta(1,0) + K3*df4_dBeta(2,0);
	//ranging distance
	bmat(0,8) = ((K123%Lmat)*config.RangeS)(0,0);//-K1*config.RangeS*Mrange(0,0) - K2*config.RangeS*Mrange(1,0) - K3*config.RangeS*Mrange(2,0);

	/////////////////////////////
	//J Matrix
	/////////////////////////////
	//bore-sight factors (bias_X, bias_Y, bias_Z)
	//Partial derivatives
	CSMMatrix<double> KR = K123%INS_R.Rmatrix;
	jmat_lidar(0,0) = KR(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dXpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dXpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dXpbias;
	jmat_lidar(0,1) = KR(0,1);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dYpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dYpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dYpbias;
	jmat_lidar(0,2) = KR(0,2);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dZpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dZpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dZpbias;
	
	//rotational alignment (bias_O, bias_P, bias_K)
	jmat_lidar(0,3) = ((K123%INS_R.Rmatrix%dOffsetR_dO.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dObias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dObias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dObias;
	jmat_lidar(0,4) = ((K123%INS_R.Rmatrix%dOffsetR_dP.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dPbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dPbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dPbias;
	jmat_lidar(0,5) = ((K123%INS_R.Rmatrix%dOffsetR_dK.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dKbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dKbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dKbias;

	//scan angle offset
	jmat_lidar(0,6) = (K123%df1_dAlpha_offset)(0,0);
	jmat_lidar(0,7) = (K123%df2_dBeta_offset)(0,0);

	//laser ranging offset
	jmat_lidar(0,8) = ((K123%Lmat)*1.0)(0,0);
	//laser ranging scale
	jmat_lidar(0,9) = ((K123%Lmat)*point.dist)(0,0);

	//vertex points
	//vertex A
	jmat_vertexA(0,0) = (ZC - ZB)*YP + (YB - YC)*ZP + (YC*ZB - YB*ZC);
	jmat_vertexA(1,1) = (ZB - ZC)*XP + (XC - XB)*ZP + (XB*ZC - XC*ZB);
	jmat_vertexA(2,2) = (YC - YB)*XP + (XB - XC)*YP + (XC*YB - XB*YC);

	//vertex B
	jmat_vertexB(0,0) = (ZA - ZC)*YP + (YC - YA)*ZP + (YA*ZC - ZA*YC);
	jmat_vertexB(1,1) = (ZC - ZA)*XP + (XA - XC)*ZP + (ZA*XC - XA*ZC);
	jmat_vertexB(2,2) = (YA - YC)*XP + (XC - XA)*YP + (XA*YC - YA*XC);

	//vertex C
	jmat_vertexC(0,0) = (ZB - ZA)*YP  + (YA - YB)*ZP + (ZA*YB - YA*ZB);
	jmat_vertexC(1,1) = (ZA - ZB)*XP  + (XB - XA)*ZP + (XA*ZB - ZA*XB);
	jmat_vertexC(2,2) = (YB - YA)*XP + (XA - XB)*YP + (YA*XB - XA*YB);

	double K0 = XP*(YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC-YC*ZB))
		       -YP*(XA*(ZB-ZC) - ZA*(XB-XC) + (XB*ZC-XC*ZB))
			   +ZP*(XA*(YB-YC) - YA*(XB-XC) + (XB*YC-XC*YB))
			   -1.*(XA*(YB*ZC-YC*ZB) - YA*(XB*ZC-XC*ZB) + ZA*(XB*YC-XC*YB));
	
	kmat(0,0) = -K0;
}

void CLIDARBiasCalibration::Cal_PDsWithVolume_Brazil2(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat)
{
	double XP, YP, ZP;
	
	CSMMatrix<double> X0 = LIDAREQ_Brazil2(point, Config);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	//ranging distance + distance bias
	double dist = point.dist*config.RangeS + config.Rangeb;

	CSMMatrix<double> Rangevector(3,1);
	Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= dist;

	//INS angle
	double INS_Pitch=point.INS_O;
	double INS_Roll=point.INS_P;
	double INS_Yaw=point.INS_K;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//

	point.beta *= -1.0;
	
	_Rmat_ Rollmat	(0, INS_Roll,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(INS_Pitch,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, INS_Yaw);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(0, Deg2Rad(180), 0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS_R; INS_R.Rmatrix = INS2G.Rmatrix	%	(Yawmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix);

	//partial derivatives for INS angle
	CRotationcoeff2 dR_dPitch;
	dR_dPitch.Partial_dRdO(INS_Pitch, 0, 0);
	dR_dPitch.Rmatrix = Yawmat.Rmatrix % dR_dPitch.Rmatrix % Rollmat.Rmatrix;

	CRotationcoeff2 dR_dRoll;
	dR_dRoll.Partial_dRdP(0, INS_Roll, 0);
	dR_dRoll.Rmatrix = Yawmat.Rmatrix % Pitchmat.Rmatrix % dR_dRoll.Rmatrix;

	CRotationcoeff2 dR_dYaw;
	dR_dYaw.Partial_dRdK(0, 0, INS_Yaw);
	dR_dYaw.Rmatrix = dR_dYaw.Rmatrix % Pitchmat.Rmatrix % Rollmat.Rmatrix;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(point.beta + config.Beta_b);
	SwingR(1,0) = -sin(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);
	SwingR(2,0) = cos(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);

	//partial derivatives for scan angle
	CRotationcoeff2 AlphaR(point.alpha,0,0);
	CRotationcoeff2 AlphaOffsetR(config.Alpha_b,0,0);
	CRotationcoeff2 BetaR(0,point.beta,0);
	CRotationcoeff2 BetaOffsetR(0,config.Beta_b,0);
	CRotationcoeff2 dAlphaR_dAlpha; dAlphaR_dAlpha.Partial_dRdO(point.alpha, 0, 0);
	CRotationcoeff2 dBetaR_dBeta; dBetaR_dBeta.Partial_dRdP(0, point.beta, 0);
	CRotationcoeff2 dAlphaBiasR_dAlphaOffset; dAlphaBiasR_dAlphaOffset.Partial_dRdO(config.Alpha_b,0, 0);
	CRotationcoeff2 dBetaBiasR_dBetaOffset; dBetaBiasR_dBetaOffset.Partial_dRdP(0,config.Beta_b,0);

	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
	CSMMatrix<double> R2 = BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R3 = R1%AlphaOffsetR.Rmatrix%BetaR.Rmatrix;
	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
	CSMMatrix<double> R5 = AlphaOffsetR.Rmatrix%BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R6 = R4%AlphaR.Rmatrix%AlphaOffsetR.Rmatrix;

	CSMMatrix<double> df1_dAlpha_offset = R1%dAlphaBiasR_dAlphaOffset.Rmatrix%R2%Rangevector;
	CSMMatrix<double> df2_dBeta_offset = R3%dBetaBiasR_dBetaOffset.Rmatrix%Rangevector;
	CSMMatrix<double> df3_dAlpha = R4%dAlphaR_dAlpha.Rmatrix%R5%Rangevector;
	CSMMatrix<double> df4_dBeta = R6%dBetaR_dBeta.Rmatrix%BetaOffsetR.Rmatrix%Rangevector;
	
	CSMMatrix<double> Lmat = R4%SwingR;

	CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%SwingR)*(dist));
	CSMMatrix<double> dFdPitch	= K123%dR_dPitch.Rmatrix	%temp;
	CSMMatrix<double> dFdRoll	= K123%dR_dRoll.Rmatrix		%temp;
	CSMMatrix<double> dFdYaw	= K123%dR_dYaw.Rmatrix		%temp;

	/////////////////////////////
	//B Matrix
	/////////////////////////////
	//GNSS obs
	bmat(0,0) = K1;
	bmat(0,1) = K2;
	bmat(0,2) = K3;
	//INS obs
	bmat(0,3) = dFdPitch(0,0);
	bmat(0,4) = dFdRoll(0,0);
	bmat(0,5) = dFdYaw(0,0);
	//swing angle
	bmat(0,6) = (K123%df3_dAlpha)(0,0);//K1*df3_dAlpha(0,0) + K2*df3_dAlpha(1,0) + K3*df3_dAlpha(2,0);
	bmat(0,7) = (K123%df4_dBeta)(0,0);//K1*df4_dBeta(0,0) + K2*df4_dBeta(1,0) + K3*df4_dBeta(2,0);
	//ranging distance
	bmat(0,8) = ((K123%Lmat)*config.RangeS)(0,0);//-K1*config.RangeS*Mrange(0,0) - K2*config.RangeS*Mrange(1,0) - K3*config.RangeS*Mrange(2,0);

	/////////////////////////////
	//J Matrix
	/////////////////////////////
	//bore-sight factors (bias_X, bias_Y, bias_Z)
	//Partial derivatives
	CSMMatrix<double> KR = K123%INS_R.Rmatrix;
	jmat_lidar(0,0) = KR(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dXpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dXpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dXpbias;
	jmat_lidar(0,1) = KR(0,1);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dYpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dYpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dYpbias;
	jmat_lidar(0,2) = KR(0,2);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dZpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dZpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dZpbias;
	
	//rotational alignment (bias_O, bias_P, bias_K)
	jmat_lidar(0,3) = ((K123%INS_R.Rmatrix%dOffsetR_dO.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dObias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dObias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dObias;
	jmat_lidar(0,4) = ((K123%INS_R.Rmatrix%dOffsetR_dP.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dPbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dPbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dPbias;
	jmat_lidar(0,5) = ((K123%INS_R.Rmatrix%dOffsetR_dK.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dKbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dKbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dKbias;

	//scan angle offset
	jmat_lidar(0,6) = (K123%df1_dAlpha_offset)(0,0);
	jmat_lidar(0,7) = (K123%df2_dBeta_offset)(0,0);

	//laser ranging offset
	jmat_lidar(0,8) = ((K123%Lmat)*1.0)(0,0);
	//laser ranging scale
	jmat_lidar(0,9) = ((K123%Lmat)*point.dist)(0,0);

	//vertex points
	//vertex A
	jmat_vertexA(0,0) = (ZC - ZB)*YP + (YB - YC)*ZP + (YC*ZB - YB*ZC);
	jmat_vertexA(1,1) = (ZB - ZC)*XP + (XC - XB)*ZP + (XB*ZC - XC*ZB);
	jmat_vertexA(2,2) = (YC - YB)*XP + (XB - XC)*YP + (XC*YB - XB*YC);

	//vertex B
	jmat_vertexB(0,0) = (ZA - ZC)*YP + (YC - YA)*ZP + (YA*ZC - ZA*YC);
	jmat_vertexB(1,1) = (ZC - ZA)*XP + (XA - XC)*ZP + (ZA*XC - XA*ZC);
	jmat_vertexB(2,2) = (YA - YC)*XP + (XC - XA)*YP + (XA*YC - YA*XC);

	//vertex C
	jmat_vertexC(0,0) = (ZB - ZA)*YP  + (YA - YB)*ZP + (ZA*YB - YA*ZB);
	jmat_vertexC(1,1) = (ZA - ZB)*XP  + (XB - XA)*ZP + (XA*ZB - ZA*XB);
	jmat_vertexC(2,2) = (YB - YA)*XP + (XA - XB)*YP + (YA*XB - XA*YB);

	double K0 = XP*(YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC-YC*ZB))
		       -YP*(XA*(ZB-ZC) - ZA*(XB-XC) + (XB*ZC-XC*ZB))
			   +ZP*(XA*(YB-YC) - YA*(XB-XC) + (XB*YC-XC*YB))
			   -1.*(XA*(YB*ZC-YC*ZB) - YA*(XB*ZC-XC*ZB) + ZA*(XB*YC-XC*YB));
	
	kmat(0,0) = -K0;
}

void CLIDARBiasCalibration::Cal_PDsWithVolume_Brazil3(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat)
{
	double XP, YP, ZP;
	
	CSMMatrix<double> X0 = LIDAREQ_Brazil3(point, Config);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	//ranging distance + distance bias
	double dist = point.dist*config.RangeS + config.Rangeb;

	CSMMatrix<double> Rangevector(3,1);
	Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= dist;

	//INS angle
	double INS_Pitch=point.INS_O;
	double INS_Roll=point.INS_P;
	double INS_Yaw=point.INS_K;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//
	
	_Rmat_ Rollmat	(0, INS_Roll,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(INS_Pitch,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, INS_Yaw);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(Deg2Rad(180), 0, 0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS_R; INS_R.Rmatrix = INS2G.Rmatrix	%	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix)
															%	(Yawmat.Rmatrix);

	//partial derivatives for INS angle
	CRotationcoeff2 dR_dPitch;
	dR_dPitch.Partial_dRdO(INS_Pitch, 0, 0);
	dR_dPitch.Rmatrix = dR_dPitch.Rmatrix % Rollmat.Rmatrix % Yawmat.Rmatrix;

	CRotationcoeff2 dR_dRoll;
	dR_dRoll.Partial_dRdP(0, INS_Roll, 0);
	dR_dRoll.Rmatrix = Pitchmat.Rmatrix % dR_dRoll.Rmatrix % Yawmat.Rmatrix;

	CRotationcoeff2 dR_dYaw;
	dR_dYaw.Partial_dRdK(0, 0, INS_Yaw);
	dR_dYaw.Rmatrix = Pitchmat.Rmatrix % Rollmat.Rmatrix % dR_dYaw.Rmatrix;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(point.beta + config.Beta_b);
	SwingR(1,0) = -sin(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);
	SwingR(2,0) = cos(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);

	//partial derivatives for scan angle
	CRotationcoeff2 AlphaR(point.alpha,0,0);
	CRotationcoeff2 AlphaOffsetR(config.Alpha_b,0,0);
	CRotationcoeff2 BetaR(0,point.beta,0);
	CRotationcoeff2 BetaOffsetR(0,config.Beta_b,0);
	CRotationcoeff2 dAlphaR_dAlpha; dAlphaR_dAlpha.Partial_dRdO(point.alpha, 0, 0);
	CRotationcoeff2 dBetaR_dBeta; dBetaR_dBeta.Partial_dRdP(0, point.beta, 0);
	CRotationcoeff2 dAlphaBiasR_dAlphaOffset; dAlphaBiasR_dAlphaOffset.Partial_dRdO(config.Alpha_b,0, 0);
	CRotationcoeff2 dBetaBiasR_dBetaOffset; dBetaBiasR_dBetaOffset.Partial_dRdP(0,config.Beta_b,0);

	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
	CSMMatrix<double> R2 = BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R3 = R1%AlphaOffsetR.Rmatrix%BetaR.Rmatrix;
	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
	CSMMatrix<double> R5 = AlphaOffsetR.Rmatrix%BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R6 = R4%AlphaR.Rmatrix%AlphaOffsetR.Rmatrix;

	CSMMatrix<double> df1_dAlpha_offset = R1%dAlphaBiasR_dAlphaOffset.Rmatrix%R2%Rangevector;
	CSMMatrix<double> df2_dBeta_offset = R3%dBetaBiasR_dBetaOffset.Rmatrix%Rangevector;
	CSMMatrix<double> df3_dAlpha = R4%dAlphaR_dAlpha.Rmatrix%R5%Rangevector;
	CSMMatrix<double> df4_dBeta = R6%dBetaR_dBeta.Rmatrix%BetaOffsetR.Rmatrix%Rangevector;
	
	CSMMatrix<double> Lmat = R4%SwingR;

	CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%SwingR)*(dist));
	CSMMatrix<double> dFdPitch	= K123%dR_dPitch.Rmatrix	%temp;
	CSMMatrix<double> dFdRoll	= K123%dR_dRoll.Rmatrix		%temp;
	CSMMatrix<double> dFdYaw	= K123%dR_dYaw.Rmatrix		%temp;

	/////////////////////////////
	//B Matrix
	/////////////////////////////
	//GNSS obs
	bmat(0,0) = K1;
	bmat(0,1) = K2;
	bmat(0,2) = K3;
	//INS obs
	bmat(0,3) = dFdPitch(0,0);
	bmat(0,4) = dFdRoll(0,0);
	bmat(0,5) = dFdYaw(0,0);
	//swing angle
	bmat(0,6) = (K123%df3_dAlpha)(0,0);//K1*df3_dAlpha(0,0) + K2*df3_dAlpha(1,0) + K3*df3_dAlpha(2,0);
	bmat(0,7) = (K123%df4_dBeta)(0,0);//K1*df4_dBeta(0,0) + K2*df4_dBeta(1,0) + K3*df4_dBeta(2,0);
	//ranging distance
	bmat(0,8) = ((K123%Lmat)*config.RangeS)(0,0);//-K1*config.RangeS*Mrange(0,0) - K2*config.RangeS*Mrange(1,0) - K3*config.RangeS*Mrange(2,0);

	/////////////////////////////
	//J Matrix
	/////////////////////////////
	//bore-sight factors (bias_X, bias_Y, bias_Z)
	//Partial derivatives
	CSMMatrix<double> KR = K123%INS_R.Rmatrix;
	jmat_lidar(0,0) = KR(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dXpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dXpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dXpbias;
	jmat_lidar(0,1) = KR(0,1);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dYpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dYpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dYpbias;
	jmat_lidar(0,2) = KR(0,2);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dZpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dZpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dZpbias;
	
	//rotational alignment (bias_O, bias_P, bias_K)
	jmat_lidar(0,3) = ((K123%INS_R.Rmatrix%dOffsetR_dO.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dObias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dObias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dObias;
	jmat_lidar(0,4) = ((K123%INS_R.Rmatrix%dOffsetR_dP.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dPbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dPbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dPbias;
	jmat_lidar(0,5) = ((K123%INS_R.Rmatrix%dOffsetR_dK.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dKbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dKbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dKbias;

	//scan angle offset
	jmat_lidar(0,6) = (K123%df1_dAlpha_offset)(0,0);
	jmat_lidar(0,7) = (K123%df2_dBeta_offset)(0,0);

	//laser ranging offset
	jmat_lidar(0,8) = ((K123%Lmat)*1.0)(0,0);
	//laser ranging scale
	jmat_lidar(0,9) = ((K123%Lmat)*point.dist)(0,0);

	//vertex points
	//vertex A
	jmat_vertexA(0,0) = (ZC - ZB)*YP + (YB - YC)*ZP + (YC*ZB - YB*ZC);
	jmat_vertexA(1,1) = (ZB - ZC)*XP + (XC - XB)*ZP + (XB*ZC - XC*ZB);
	jmat_vertexA(2,2) = (YC - YB)*XP + (XB - XC)*YP + (XC*YB - XB*YC);

	//vertex B
	jmat_vertexB(0,0) = (ZA - ZC)*YP + (YC - YA)*ZP + (YA*ZC - ZA*YC);
	jmat_vertexB(1,1) = (ZC - ZA)*XP + (XA - XC)*ZP + (ZA*XC - XA*ZC);
	jmat_vertexB(2,2) = (YA - YC)*XP + (XC - XA)*YP + (XA*YC - YA*XC);

	//vertex C
	jmat_vertexC(0,0) = (ZB - ZA)*YP  + (YA - YB)*ZP + (ZA*YB - YA*ZB);
	jmat_vertexC(1,1) = (ZA - ZB)*XP  + (XB - XA)*ZP + (XA*ZB - ZA*XB);
	jmat_vertexC(2,2) = (YB - YA)*XP + (XA - XB)*YP + (YA*XB - XA*YB);

	double K0 = XP*(YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC-YC*ZB))
		       -YP*(XA*(ZB-ZC) - ZA*(XB-XC) + (XB*ZC-XC*ZB))
			   +ZP*(XA*(YB-YC) - YA*(XB-XC) + (XB*YC-XC*YB))
			   -1.*(XA*(YB*ZC-YC*ZB) - YA*(XB*ZC-XC*ZB) + ZA*(XB*YC-XC*YB));
	
	kmat(0,0) = -K0;
}

void CLIDARBiasCalibration::Cal_PDsWithPoint_Titan(SM3DPoint P, CLIDARRawPoint point, CLIDARConfig config, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_Point, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat)
{
	double XP, YP, ZP;
	
	CSMMatrix<double> X0 = LIDAREQ_Point_Titan(point, Config);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	//ranging distance + distance bias
	double dist = point.dist*config.RangeS + config.Rangeb;

	CSMMatrix<double> Rangevector(3,1);
	Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= dist;

	//INS angle
	double INS_Pitch=point.INS_O;
	double INS_Roll=point.INS_P;
	double INS_Yaw=point.INS_K;

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//

	_Rmat_ Rollmat	(0, INS_Roll,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(INS_Pitch,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, INS_Yaw);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(0, Deg2Rad(180), 0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS_R; INS_R.Rmatrix = INS2G.Rmatrix	%	(Yawmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix);

	//partial derivatives for INS angle
	CRotationcoeff2 dR_dPitch;
	dR_dPitch.Partial_dRdO(INS_Pitch, 0, 0);
	dR_dPitch.Rmatrix = Yawmat.Rmatrix % dR_dPitch.Rmatrix % Rollmat.Rmatrix;

	CRotationcoeff2 dR_dRoll;
	dR_dRoll.Partial_dRdP(0, INS_Roll, 0);
	dR_dRoll.Rmatrix = Yawmat.Rmatrix % Pitchmat.Rmatrix % dR_dRoll.Rmatrix;

	CRotationcoeff2 dR_dYaw;
	dR_dYaw.Partial_dRdK(0, 0, INS_Yaw);
	dR_dYaw.Rmatrix = dR_dYaw.Rmatrix % Pitchmat.Rmatrix % Rollmat.Rmatrix;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	point.beta *= -1.0;
	SwingR(0,0) = sin(point.beta + config.Beta_b);
	SwingR(1,0) = -sin(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);
	SwingR(2,0) = cos(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);

	//partial derivatives for scan angle
	CRotationcoeff2 AlphaR(point.alpha,0,0);
	CRotationcoeff2 AlphaOffsetR(config.Alpha_b,0,0);
	CRotationcoeff2 BetaR(0,point.beta,0);
	CRotationcoeff2 BetaOffsetR(0,config.Beta_b,0);
	CRotationcoeff2 dAlphaR_dAlpha; dAlphaR_dAlpha.Partial_dRdO(point.alpha, 0, 0);
	CRotationcoeff2 dBetaR_dBeta; dBetaR_dBeta.Partial_dRdP(0, point.beta, 0);
	CRotationcoeff2 dAlphaBiasR_dAlphaOffset; dAlphaBiasR_dAlphaOffset.Partial_dRdO(config.Alpha_b,0, 0);
	CRotationcoeff2 dBetaBiasR_dBetaOffset; dBetaBiasR_dBetaOffset.Partial_dRdP(0,config.Beta_b,0);

	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
	CSMMatrix<double> R2 = BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R3 = R1%AlphaOffsetR.Rmatrix%BetaR.Rmatrix;
	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
	CSMMatrix<double> R5 = AlphaOffsetR.Rmatrix%BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R6 = R4%AlphaR.Rmatrix%AlphaOffsetR.Rmatrix;

	CSMMatrix<double> df1_dAlpha_offset = R1%dAlphaBiasR_dAlphaOffset.Rmatrix%R2%Rangevector;
	CSMMatrix<double> df2_dBeta_offset = R3%dBetaBiasR_dBetaOffset.Rmatrix%Rangevector;
	CSMMatrix<double> df3_dAlpha = R4%dAlphaR_dAlpha.Rmatrix%R5%Rangevector;
	CSMMatrix<double> df4_dBeta = R6%dBetaR_dBeta.Rmatrix%BetaOffsetR.Rmatrix%Rangevector;
	
	CSMMatrix<double> Lmat = R4%SwingR;

	CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%SwingR)*(dist));
	CSMMatrix<double> dFdPitch	= dR_dPitch.Rmatrix	%temp;
	CSMMatrix<double> dFdRoll	= dR_dRoll.Rmatrix%temp;
	CSMMatrix<double> dFdYaw	= dR_dYaw.Rmatrix%temp;

	/////////////////////////////
	//B Matrix
	/////////////////////////////
	//GNSS obs
	bmat(0,0) = 1.0;
	bmat(0,1) = 0.0;
	bmat(0,2) = 0.0;

	bmat(1,0) = 0.0;
	bmat(1,1) = 1.0;
	bmat(1,2) = 0.0;

	bmat(2,0) = 0.0;
	bmat(2,1) = 0.0;
	bmat(2,2) = 1.0;

	//INS obs
	bmat(0,3) = dFdPitch(0,0);
	bmat(1,3) = dFdPitch(1,0);
	bmat(2,3) = dFdPitch(2,0);

	bmat(0,4) = dFdRoll(0,0);
	bmat(1,4) = dFdRoll(1,0);
	bmat(2,4) = dFdRoll(2,0);

	bmat(0,5) = dFdYaw(0,0);
	bmat(1,5) = dFdYaw(1,0);
	bmat(2,5) = dFdYaw(2,0);

	//swing angle
	bmat(0,6) = df3_dAlpha(0,0);
	bmat(1,6) = df3_dAlpha(1,0);
	bmat(2,6) = df3_dAlpha(2,0);

	bmat(0,7) = df4_dBeta(0,0);
	bmat(1,7) = df4_dBeta(1,0);
	bmat(2,7) = df4_dBeta(2,0);
	//ranging distance
	bmat(0,8) = (Lmat*config.RangeS)(0,0);
	bmat(1,8) = (Lmat*config.RangeS)(1,0);
	bmat(2,8) = (Lmat*config.RangeS)(2,0);

	/////////////////////////////
	//J Matrix
	/////////////////////////////
	//bore-sight factors (bias_X, bias_Y, bias_Z)
	//Partial derivatives
	jmat_lidar.Insert(0,0,INS_R.Rmatrix);
	
	//rotational alignment (bias_O, bias_P, bias_K)
	temp = ((INS_R.Rmatrix%dOffsetR_dO.Rmatrix%SwingR)*dist);
	jmat_lidar.Insert(0,3,temp);

	temp = ((INS_R.Rmatrix%dOffsetR_dP.Rmatrix%SwingR)*dist);
	jmat_lidar.Insert(0,4,temp);

	temp = ((INS_R.Rmatrix%dOffsetR_dK.Rmatrix%SwingR)*dist);
	jmat_lidar.Insert(0,5,temp);

	//scan angle offset
	jmat_lidar.Insert(0,6,(df1_dAlpha_offset));

	jmat_lidar.Insert(0,7,(df2_dBeta_offset));

	//laser ranging offset
	jmat_lidar.Insert(0,8,Lmat);

	//laser ranging scale
	jmat_lidar.Insert(0,9, Lmat*point.dist);

	//points
	jmat_Point(0,0) = -1.0;
	jmat_Point(1,1) = -1.0;
	jmat_Point(2,2) = -1.0;

	kmat(0,0) = P.X - X0(0, 0);
	kmat(1,0) = P.Y - X0(1, 0);
	kmat(2,0) = P.Z - X0(2, 0);
}

void CLIDARBiasCalibration::Cal_PDsWithVolume_Titan(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat)
{
	double XP, YP, ZP;
	
	CSMMatrix<double> X0 = LIDAREQ_Point_Titan(point, Config);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	//ranging distance + distance bias
	double dist = point.dist*config.RangeS + config.Rangeb;

	CSMMatrix<double> Rangevector(3,1);
	Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= dist;

	//INS angle
	double INS_Pitch=point.INS_O;
	double INS_Roll=point.INS_P;
	double INS_Yaw=point.INS_K;
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//

	point.beta *= -1.0;
		
	_Rmat_ Rollmat	(0, INS_Roll,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(INS_Pitch,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, INS_Yaw);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(0, Deg2Rad(180), 0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS_R; INS_R.Rmatrix = INS2G.Rmatrix	%	(Yawmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix);

	//partial derivatives for INS angle
	CRotationcoeff2 dR_dPitch;
	dR_dPitch.Partial_dRdO(INS_Pitch, 0, 0);
	dR_dPitch.Rmatrix = Yawmat.Rmatrix % dR_dPitch.Rmatrix % Rollmat.Rmatrix;

	CRotationcoeff2 dR_dRoll;
	dR_dRoll.Partial_dRdP(0, INS_Roll, 0);
	dR_dRoll.Rmatrix = Yawmat.Rmatrix % Pitchmat.Rmatrix % dR_dRoll.Rmatrix;

	CRotationcoeff2 dR_dYaw;
	dR_dYaw.Partial_dRdK(0, 0, INS_Yaw);
	dR_dYaw.Rmatrix = dR_dYaw.Rmatrix % Pitchmat.Rmatrix % Rollmat.Rmatrix;

	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(point.beta + config.Beta_b);
	SwingR(1,0) = -sin(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);
	SwingR(2,0) = cos(point.alpha + config.Alpha_b)*cos(point.beta + config.Beta_b);

	//partial derivatives for scan angle
	CRotationcoeff2 AlphaR(point.alpha,0,0);
	CRotationcoeff2 AlphaOffsetR(config.Alpha_b,0,0);
	CRotationcoeff2 BetaR(0,point.beta,0);
	CRotationcoeff2 BetaOffsetR(0,config.Beta_b,0);
	CRotationcoeff2 dAlphaR_dAlpha; dAlphaR_dAlpha.Partial_dRdO(point.alpha, 0, 0);
	CRotationcoeff2 dBetaR_dBeta; dBetaR_dBeta.Partial_dRdP(0, point.beta, 0);
	CRotationcoeff2 dAlphaBiasR_dAlphaOffset; dAlphaBiasR_dAlphaOffset.Partial_dRdO(config.Alpha_b,0, 0);
	CRotationcoeff2 dBetaBiasR_dBetaOffset; dBetaBiasR_dBetaOffset.Partial_dRdP(0,config.Beta_b,0);

	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
	CSMMatrix<double> R2 = BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R3 = R1%AlphaOffsetR.Rmatrix%BetaR.Rmatrix;
	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
	CSMMatrix<double> R5 = AlphaOffsetR.Rmatrix%BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R6 = R4%AlphaR.Rmatrix%AlphaOffsetR.Rmatrix;

	CSMMatrix<double> df1_dAlpha_offset = R1%dAlphaBiasR_dAlphaOffset.Rmatrix%R2%Rangevector;
	CSMMatrix<double> df2_dBeta_offset = R3%dBetaBiasR_dBetaOffset.Rmatrix%Rangevector;
	CSMMatrix<double> df3_dAlpha = R4%dAlphaR_dAlpha.Rmatrix%R5%Rangevector;
	CSMMatrix<double> df4_dBeta = R6%dBetaR_dBeta.Rmatrix%BetaOffsetR.Rmatrix%Rangevector;
	
	CSMMatrix<double> Lmat = R4%SwingR;

	CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%SwingR)*(dist));
	CSMMatrix<double> dFdPitch	= K123%dR_dPitch.Rmatrix	%temp;
	CSMMatrix<double> dFdRoll	= K123%dR_dRoll.Rmatrix%temp;
	CSMMatrix<double> dFdYaw	= K123%dR_dYaw.Rmatrix%temp;

	/////////////////////////////
	//B Matrix
	/////////////////////////////
	//GNSS obs
	bmat(0,0) = K1;
	bmat(0,1) = K2;
	bmat(0,2) = K3;
	//INS obs
	bmat(0,3) = dFdPitch(0,0);
	bmat(0,4) = dFdRoll(0,0);
	bmat(0,5) = dFdYaw(0,0);
	//swing angle
	bmat(0,6) = (K123%df3_dAlpha)(0,0);//K1*df3_dAlpha(0,0) + K2*df3_dAlpha(1,0) + K3*df3_dAlpha(2,0);
	bmat(0,7) = (K123%df4_dBeta)(0,0);//K1*df4_dBeta(0,0) + K2*df4_dBeta(1,0) + K3*df4_dBeta(2,0);
	//ranging distance
	bmat(0,8) = ((K123%Lmat)*config.RangeS)(0,0);//K1*config.RangeS*Mrange(0,0) + K2*config.RangeS*Mrange(1,0) + K3*config.RangeS*Mrange(2,0);

	/////////////////////////////
	//J Matrix
	/////////////////////////////
	//boresight factors (bias_X, bias_Y, bias_Z)
	//Partial derivatives
	CSMMatrix<double> KR = K123%INS_R.Rmatrix;
	jmat_lidar(0,0) = KR(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dXpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dXpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dXpbias;
	jmat_lidar(0,1) = KR(0,1);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dYpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dYpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dYpbias;
	jmat_lidar(0,2) = KR(0,2);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dZpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dZpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dZpbias;
	
	//rotational alignment (bias_O, bias_P, bias_K)
	jmat_lidar(0,3) = ((K123%INS_R.Rmatrix%dOffsetR_dO.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dObias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dObias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dObias;
	jmat_lidar(0,4) = ((K123%INS_R.Rmatrix%dOffsetR_dP.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dPbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dPbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dPbias;
	jmat_lidar(0,5) = ((K123%INS_R.Rmatrix%dOffsetR_dK.Rmatrix%SwingR)*(dist))(0,0);//(YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dKbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dKbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dKbias;

	//scan angle offset
	jmat_lidar(0,6) = (K123%df1_dAlpha_offset)(0,0);
	jmat_lidar(0,7) = (K123%df2_dBeta_offset)(0,0);

	//laser ranging offset
	jmat_lidar(0,8) = (K123%Lmat)(0,0);
	//laser ranging scale
	jmat_lidar(0,9) = ((K123%Lmat)*point.dist)(0,0);

	//vertex points
	//vertex A
	jmat_vertexA(0,0) = (ZC - ZB)*YP + (YB - YC)*ZP + (YC*ZB - YB*ZC);
	jmat_vertexA(0,1) = (ZB - ZC)*XP + (XC - XB)*ZP + (XB*ZC - XC*ZB);
	jmat_vertexA(0,2) = (YC - YB)*XP + (XB - XC)*YP + (XC*YB - XB*YC);

	//vertex B
	jmat_vertexB(0,0) = (ZA - ZC)*YP + (YC - YA)*ZP + (YA*ZC - ZA*YC);
	jmat_vertexB(0,1) = (ZC - ZA)*XP + (XA - XC)*ZP + (ZA*XC - XA*ZC);
	jmat_vertexB(0,2) = (YA - YC)*XP + (XC - XA)*YP + (XA*YC - YA*XC);

	//vertex C
	jmat_vertexC(0,0) = (ZB - ZA)*YP  + (YA - YB)*ZP + (ZA*YB - YA*ZB);
	jmat_vertexC(0,1) = (ZA - ZB)*XP  + (XB - XA)*ZP + (XA*ZB - ZA*XB);
	jmat_vertexC(0,2) = (YB - YA)*XP + (XA - XB)*YP + (YA*XB - XA*YB);

	double K0 = XP*(YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC-YC*ZB))
		       -YP*(XA*(ZB-ZC) - ZA*(XB-XC) + (XB*ZC-XC*ZB))
			   +ZP*(XA*(YB-YC) - YA*(XB-XC) + (XB*YC-XC*YB))
			   -1.*(XA*(YB*ZC-YC*ZB) - YA*(XB*ZC-XC*ZB) + ZA*(XB*YC-XC*YB));
	
	kmat(0,0) = -K0;
}

void CLIDARBiasCalibration::Cal_PDsWithVolume_old(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat)
{
	double XP, YP, ZP;
	double XA, YA, ZA;
	double XB, YB, ZB;
	double XC, YC, ZC;

	CSMMatrix<double> X0 = LIDAREQ(point, Config);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	XA = patch.Vertex[0];
	YA = patch.Vertex[1];
	ZA = patch.Vertex[2];

	XB = patch.Vertex[3];
	YB = patch.Vertex[4];
	ZB = patch.Vertex[5];

	XC = patch.Vertex[6];
	YC = patch.Vertex[7];
	ZC = patch.Vertex[8];

	//vertex A
	bmat(0,0) = YP*(ZC-ZB) + ZP*(YB-YC) + (YC*ZB-YB*ZC);
	bmat(0,1) = XP*(ZB-ZC) + ZP*(XC-XB) + (XB*ZC-XC*ZB);
	bmat(0,2) = XP*(YC-YB) + YP*(XB-XC) + (XC*YB-XB*YC);
		
	//vertex B
	bmat(0,3) = YP*(ZA-ZC) + ZP*(YC-YA) + (YA*ZC-ZA*YC);
	bmat(0,4) = XP*(ZC-ZA) + ZP*(XA-XC) + (ZA*XC-XA*ZC);
	bmat(0,5) = XP*(YA-YC) + YP*(XC-XA) + (XA*YC-XC*YA);
	
	//vertex C
	bmat(0,6) = YP*(ZB-ZA) + ZP*(YA-YB) + (ZA*YB-YA*ZB);
	bmat(0,7) = XP*(ZA-ZB) + ZP*(XB-XA) + (XA*ZB-ZA*XB);
	bmat(0,8) = XP*(YB-YA) + YP*(XA-XB) + (YA*XB-XA*YB);

	CRotationcoeff2 INS_R; //INS Rotation matrix: R matrix
	INS_R.ReMake(point.INS_O, point.INS_P, point.INS_K);
	
	//offset angle + bias angle
	double dO = config.Ooffset + config.Ob;
	double dP = config.Poffset + config.Pb;
	double dK = config.Koffset + config.Kb;
	
	//partial derivatives for angle biases (omega)
	CSMMatrix<double> dOffsetR_dO(3,3,0.);
	dOffsetR_dO(0,0) = 0;	
	dOffsetR_dO(0,1) = 0;	
	dOffsetR_dO(0,2) = 0;
	
	dOffsetR_dO(1,0) = cos(dO)*sin(dP)*cos(dK) - sin(dO)*sin(dK);	
	dOffsetR_dO(1,1) = -cos(dO)*sin(dP)*sin(dK) - sin(dO)*cos(dK);	
	dOffsetR_dO(1,2) = -cos(dO)*cos(dP);
	
	dOffsetR_dO(2,0) = sin(dO)*sin(dP)*cos(dK) + cos(dO)*sin(dK);	
	dOffsetR_dO(2,1) = -sin(dO)*sin(dP)*sin(dK) + cos(dO)*cos(dK);	
	dOffsetR_dO(2,2) = -sin(dO)*cos(dP);
	
	//partial derivatives for angle biases (phi)
	CSMMatrix<double> dOffsetR_dP(3,3,0.);
	dOffsetR_dP(0,0) = -sin(dP)*cos(dK);	
	dOffsetR_dP(0,1) = sin(dP)*sin(dK);	
	dOffsetR_dP(0,2) = cos(dP);
	
	dOffsetR_dP(1,0) = sin(dO)*cos(dP)*cos(dK);	
	dOffsetR_dP(1,1) = -sin(dO)*cos(dP)*sin(dK);	
	dOffsetR_dP(1,2) = sin(dO)*sin(dP);
	
	dOffsetR_dP(2,0) = -cos(dO)*cos(dP)*cos(dK);	
	dOffsetR_dP(2,1) = cos(dO)*cos(dP)*sin(dK);	
	dOffsetR_dP(2,2) = -cos(dO)*sin(dP);
	
	//partial derivatives for angle biases (kappa)
	CSMMatrix<double> dOffsetR_dK(3,3,0.);
	dOffsetR_dK(0,0) = -cos(dP)*sin(dK);	
	dOffsetR_dK(0,1) = -cos(dP)*cos(dK);	
	dOffsetR_dK(0,2) = 0.;
	
	dOffsetR_dK(1,0) = -sin(dO)*sin(dP)*sin(dK) + cos(dO)*cos(dK);	
	dOffsetR_dK(1,1) = -sin(dO)*sin(dP)*cos(dK) - cos(dO)*sin(dK);	
	dOffsetR_dK(1,2) = 0.;
	
	dOffsetR_dK(2,0) = cos(dO)*sin(dP)*sin(dK) + sin(dO)*cos(dK);
	dOffsetR_dK(2,1) = cos(dO)*sin(dP)*cos(dK) - sin(dO)*sin(dK);	
	dOffsetR_dK(2,2) = 0.;
	
	//ranging distance + distance bias
	double dist = point.dist + config.Rangeb;
	
	//elements of each row in INS rotation matrix
	CSMMatrix<double> INS_R1, INS_R2, INS_R3;
	INS_R1 = INS_R.Rmatrix.Subset(0,0,1,3);
	INS_R2 = INS_R.Rmatrix.Subset(1,0,1,3);
	INS_R3 = INS_R.Rmatrix.Subset(2,0,1,3);
	
	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(point.beta);
	SwingR(1,0) = -sin(point.alpha)*cos(point.beta);
	SwingR(2,0) = cos(point.alpha)*cos(point.beta);
	
	//INS rotation * boresight rotation
	CRotationcoeff2 RB(dO, dP, dK); 
	//INS rotation * boresight rotation * swing matrix * -(ranging distance + distance bias)
	CSMMatrix<double> R2 = INS_R.Rmatrix%RB.Rmatrix%SwingR*(-dist);
	
	//Partial derivatives
	double dXG_dXpbias = INS_R.Rmatrix(0,0);
	double dXG_dYpbias = INS_R.Rmatrix(0,1);
	double dXG_dZpbias = INS_R.Rmatrix(0,2);
	double dXG_dObias = -(INS_R1%dOffsetR_dO%SwingR*dist)(0,0);
	double dXG_dPbias = -(INS_R1%dOffsetR_dP%SwingR*dist)(0,0);
	double dXG_dKbias = -(INS_R1%dOffsetR_dK%SwingR*dist)(0,0);
	double dXG_drangebias = R2(0,0);
	
	double dYG_dXpbias = INS_R.Rmatrix(1,0);
	double dYG_dYpbias = INS_R.Rmatrix(1,1);
	double dYG_dZpbias = INS_R.Rmatrix(1,2);
	double dYG_dObias = -(INS_R2%dOffsetR_dO%SwingR*dist)(0,0);
	double dYG_dPbias = -(INS_R2%dOffsetR_dP%SwingR*dist)(0,0);
	double dYG_dKbias = -(INS_R2%dOffsetR_dK%SwingR*dist)(0,0);
	double dYG_drangebias = R2(1,0);
	
	double dZG_dXpbias = INS_R.Rmatrix(2,0);
	double dZG_dYpbias = INS_R.Rmatrix(2,1);
	double dZG_dZpbias = INS_R.Rmatrix(2,2);
	double dZG_dObias = -(INS_R3%dOffsetR_dO%SwingR*dist)(0,0);
	double dZG_dPbias = -(INS_R3%dOffsetR_dP%SwingR*dist)(0,0);
	double dZG_dKbias = -(INS_R3%dOffsetR_dK%SwingR*dist)(0,0);
	double dZG_drangebias = R2(2,0);
	
	//Make jmat matrix for a each LIDAR raw point
	//jmat(0,0) = coeff[0]*dXG_dXpbias + coeff[1]*dYG_dXpbias + coeff[2]*dZG_dXpbias;
	//jmat(0,1) = coeff[0]*dXG_dYpbias + coeff[1]*dYG_dYpbias + coeff[2]*dZG_dYpbias;
	//jmat(0,2) = coeff[0]*dXG_dZpbias + coeff[1]*dYG_dZpbias + coeff[2]*dZG_dZpbias;
	
	//jmat(0,3) = coeff[0]*dXG_dObias + coeff[1]*dYG_dObias + coeff[2]*dZG_dObias;
	//jmat(0,4) = coeff[0]*dXG_dPbias + coeff[1]*dYG_dPbias + coeff[2]*dZG_dPbias;
	//jmat(0,5) = coeff[0]*dXG_dKbias + coeff[1]*dYG_dKbias + coeff[2]*dZG_dKbias;

	jmat(0,0) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dXpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dXpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dXpbias;
	jmat(0,1) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dYpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dYpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dYpbias;
	jmat(0,2) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dZpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dZpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dZpbias;
	
	jmat(0,3) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dObias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dObias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dObias;
	jmat(0,4) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dPbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dPbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dPbias;
	jmat(0,5) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dKbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dKbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dKbias;

	double Vol= XP*(YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC-YC*ZB))
		       -YP*(XA*(ZB-ZC) - ZA*(XB-XC) + (XB*ZC-XC*ZB))
			   +ZP*(XA*(YB-YC) - YA*(XB-XC) + (XB*YC-XC*YB))
			   -1.*(XA*(YB*ZC-YC*ZB) - YA*(XB*ZC-XC*ZB) + ZA*(XB*YC-XC*YB));
	
	//jmat(0,6) = coeff[0]*dXG_drangebias + coeff[1]*dYG_drangebias + coeff[2]*dZG_drangebias;

	kmat(0,0) = -Vol;
}

void CLIDARBiasCalibration::Cal_PDsWithND(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat, double AREA)
{
	double XP, YP, ZP;
	double XA, YA, ZA;
	double XB, YB, ZB;
	double XC, YC, ZC;

	CSMMatrix<double> X0 = LIDAREQ(point, Config);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	XA = patch.Vertex[0];
	YA = patch.Vertex[1];
	ZA = patch.Vertex[2];

	XB = patch.Vertex[3];
	YB = patch.Vertex[4];
	ZB = patch.Vertex[5];

	XC = patch.Vertex[6];
	YC = patch.Vertex[7];
	ZC = patch.Vertex[8];

	//vertex A
	bmat(0,0) = YP*(ZC-ZB) + ZP*(YB-YC) + (YC*ZB-YB*ZC);
	bmat(0,1) = XP*(ZB-ZC) + ZP*(XC-XB) + (XB*ZC-XC*ZB);
	bmat(0,2) = XP*(YC-YB) + YP*(XB-XC) + (XC*YB-XB*YC);
		
	//vertex B
	bmat(0,3) = YP*(ZA-ZC) + ZP*(YC-YA) + (YA*ZC-ZA*YC);
	bmat(0,4) = XP*(ZC-ZA) + ZP*(XA-XC) + (ZA*XC-XA*ZC);
	bmat(0,5) = XP*(YA-YC) + YP*(XC-XA) + (XA*YC-XC*YA);
	
	//vertex C
	bmat(0,6) = YP*(ZB-ZA) + ZP*(YA-YB) + (ZA*YB-YA*ZB);
	bmat(0,7) = XP*(ZA-ZB) + ZP*(XB-XA) + (XA*ZB-ZA*XB);
	bmat(0,8) = XP*(YB-YA) + YP*(XA-XB) + (YA*XB-XA*YB);

	//vertex A
	bmat(0,0) = bmat(0,0)/2./AREA;
	bmat(0,1) = bmat(0,1)/2./AREA;
	bmat(0,2) = bmat(0,2)/2./AREA;
		
	//vertex B
	bmat(0,3) = bmat(0,3)/2./AREA;
	bmat(0,4) = bmat(0,4)/2./AREA;
	bmat(0,5) = bmat(0,5)/2./AREA;
	
	//vertex C
	bmat(0,6) = bmat(0,6)/2./AREA;
	bmat(0,7) = bmat(0,7)/2./AREA;
	bmat(0,8) = bmat(0,8)/2./AREA;
		
	CRotationcoeff2 INS_R; //INS Rotation matrix: R matrix
	INS_R.ReMake(point.INS_O, point.INS_P, point.INS_K);
	
	//offset angle + bias angle
	double dO = config.Ooffset + config.Ob;
	double dP = config.Poffset + config.Pb;
	double dK = config.Koffset + config.Kb;
	
	//partial derivatives for angle biases (omega)
	CSMMatrix<double> dOffsetR_dO(3,3,0.);
	dOffsetR_dO(0,0) = 0;	
	dOffsetR_dO(0,1) = 0;	
	dOffsetR_dO(0,2) = 0;
	
	dOffsetR_dO(1,0) = cos(dO)*sin(dP)*cos(dK) - sin(dO)*sin(dK);	
	dOffsetR_dO(1,1) = -cos(dO)*sin(dP)*sin(dK) - sin(dO)*cos(dK);	
	dOffsetR_dO(1,2) = -cos(dO)*cos(dP);
	
	dOffsetR_dO(2,0) = sin(dO)*sin(dP)*cos(dK) + cos(dO)*sin(dK);	
	dOffsetR_dO(2,1) = -sin(dO)*sin(dP)*sin(dK) + cos(dO)*cos(dK);	
	dOffsetR_dO(2,2) = -sin(dO)*cos(dP);
	
	//partial derivatives for angle biases (phi)
	CSMMatrix<double> dOffsetR_dP(3,3,0.);
	dOffsetR_dP(0,0) = -sin(dP)*cos(dK);	
	dOffsetR_dP(0,1) = sin(dP)*sin(dK);	
	dOffsetR_dP(0,2) = cos(dP);
	
	dOffsetR_dP(1,0) = sin(dO)*cos(dP)*cos(dK);	
	dOffsetR_dP(1,1) = -sin(dO)*cos(dP)*sin(dK);	
	dOffsetR_dP(1,2) = sin(dO)*sin(dP);
	
	dOffsetR_dP(2,0) = -cos(dO)*cos(dP)*cos(dK);	
	dOffsetR_dP(2,1) = cos(dO)*cos(dP)*sin(dK);	
	dOffsetR_dP(2,2) = -cos(dO)*sin(dP);
	
	//partial derivatives for angle biases (kappa)
	CSMMatrix<double> dOffsetR_dK(3,3,0.);
	dOffsetR_dK(0,0) = -cos(dP)*sin(dK);	
	dOffsetR_dK(0,1) = -cos(dP)*cos(dK);	
	dOffsetR_dK(0,2) = 0.;
	
	dOffsetR_dK(1,0) = -sin(dO)*sin(dP)*sin(dK) + cos(dO)*cos(dK);	
	dOffsetR_dK(1,1) = -sin(dO)*sin(dP)*cos(dK) - cos(dO)*sin(dK);	
	dOffsetR_dK(1,2) = 0.;
	
	dOffsetR_dK(2,0) = cos(dO)*sin(dP)*sin(dK) + sin(dO)*cos(dK);	
	dOffsetR_dK(2,1) = cos(dO)*sin(dP)*cos(dK) - sin(dO)*sin(dK);	
	dOffsetR_dK(2,2) = 0.;
	
	//ranging distance + distance bias
	double dist = point.dist + config.Rangeb;
	
	//elements of each row in INS rotation matrix
	CSMMatrix<double> INS_R1, INS_R2, INS_R3;
	INS_R1 = INS_R.Rmatrix.Subset(0,0,1,3);
	INS_R2 = INS_R.Rmatrix.Subset(1,0,1,3);
	INS_R3 = INS_R.Rmatrix.Subset(2,0,1,3);
	
	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(point.beta);
	SwingR(1,0) = -sin(point.alpha)*cos(point.beta);
	SwingR(2,0) = cos(point.alpha)*cos(point.beta);
	
	//INS rotation * boresight rotation
	CRotationcoeff2 RB(dO, dP, dK); 
	//INS rotation * boresight rotation * swing matrix * -(ranging distance + distance bias)
	CSMMatrix<double> R2 = INS_R.Rmatrix%RB.Rmatrix%SwingR*(-dist);
	
	//Partial derivatives
	double dXG_dXpbias = INS_R.Rmatrix(0,0);
	double dXG_dYpbias = INS_R.Rmatrix(0,1);
	double dXG_dZpbias = INS_R.Rmatrix(0,2);
	double dXG_dObias = -(INS_R1%dOffsetR_dO%SwingR*dist)(0,0);
	double dXG_dPbias = -(INS_R1%dOffsetR_dP%SwingR*dist)(0,0);
	double dXG_dKbias = -(INS_R1%dOffsetR_dK%SwingR*dist)(0,0);
	double dXG_drangebias = R2(0,0);
	
	double dYG_dXpbias = INS_R.Rmatrix(1,0);
	double dYG_dYpbias = INS_R.Rmatrix(1,1);
	double dYG_dZpbias = INS_R.Rmatrix(1,2);
	double dYG_dObias = -(INS_R2%dOffsetR_dO%SwingR*dist)(0,0);
	double dYG_dPbias = -(INS_R2%dOffsetR_dP%SwingR*dist)(0,0);
	double dYG_dKbias = -(INS_R2%dOffsetR_dK%SwingR*dist)(0,0);
	double dYG_drangebias = R2(1,0);
	
	double dZG_dXpbias = INS_R.Rmatrix(2,0);
	double dZG_dYpbias = INS_R.Rmatrix(2,1);
	double dZG_dZpbias = INS_R.Rmatrix(2,2);
	double dZG_dObias = -(INS_R3%dOffsetR_dO%SwingR*dist)(0,0);
	double dZG_dPbias = -(INS_R3%dOffsetR_dP%SwingR*dist)(0,0);
	double dZG_dKbias = -(INS_R3%dOffsetR_dK%SwingR*dist)(0,0);
	double dZG_drangebias = R2(2,0);
	
	//Make jmat matrix for a each LIDAR raw point
	//jmat(0,0) = coeff[0]*dXG_dXpbias + coeff[1]*dYG_dXpbias + coeff[2]*dZG_dXpbias;
	//jmat(0,1) = coeff[0]*dXG_dYpbias + coeff[1]*dYG_dYpbias + coeff[2]*dZG_dYpbias;
	//jmat(0,2) = coeff[0]*dXG_dZpbias + coeff[1]*dYG_dZpbias + coeff[2]*dZG_dZpbias;
	
	//jmat(0,3) = coeff[0]*dXG_dObias + coeff[1]*dYG_dObias + coeff[2]*dZG_dObias;
	//jmat(0,4) = coeff[0]*dXG_dPbias + coeff[1]*dYG_dPbias + coeff[2]*dZG_dPbias;
	//jmat(0,5) = coeff[0]*dXG_dKbias + coeff[1]*dYG_dKbias + coeff[2]*dZG_dKbias;

	jmat(0,0) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dXpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dXpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dXpbias;
	jmat(0,1) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dYpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dYpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dYpbias;
	jmat(0,2) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dZpbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dZpbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dZpbias;
	
	jmat(0,3) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dObias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dObias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dObias;
	jmat(0,4) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dPbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dPbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dPbias;
	jmat(0,5) = (YA*(ZB-ZC)-ZA*(YB-YC)+(YB*ZC-YC*ZB))*dXG_dKbias + (-(XA*(ZB-ZC)-ZA*(XB-XC)+(XB*ZC-XC*ZB)))*dYG_dKbias + (XA*(YB-YC)-YA*(XB-XC)+(XB*YC-XC*YB))*dZG_dKbias;

	jmat(0,0) = jmat(0,0)/2./AREA;
	jmat(0,1) = jmat(0,1)/2./AREA;
	jmat(0,2) = jmat(0,2)/2./AREA;
	
	jmat(0,3) = jmat(0,3)/2./AREA;
	jmat(0,4) = jmat(0,4)/2./AREA;
	jmat(0,5) = jmat(0,5)/2./AREA;
	
	

	double Vol= XP*(YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC-YC*ZB))
		       -YP*(XA*(ZB-ZC) - ZA*(XB-XC) + (XB*ZC-XC*ZB))
			   +ZP*(XA*(YB-YC) - YA*(XB-XC) + (XB*YC-XC*YB))
			   -1.*(XA*(YB*ZC-YC*ZB) - YA*(XB*ZC-XC*ZB) + ZA*(XB*YC-XC*YB));

	double ND = Vol/2./AREA;
	
	//jmat(0,6) = coeff[0]*dXG_drangebias + coeff[1]*dYG_drangebias + coeff[2]*dZG_drangebias;

	kmat(0,0) = -ND;

	////
	//Vol = 6 * Volume
	//Normal Dist = Volume / Area * 3
	//Normal Dist = Vol / Area / 6 * 3 = Vol/2/Area
	//////
}

void CLIDARBiasCalibration::BatchExtractPlanarPatch(CString Cfgpath, CString Prjpath, double lidarprecision)
{
	fstream vtxlistfile;
	vtxlistfile.open(Prjpath, ios::in);

	char line[512];

	while(!vtxlistfile.eof())
	{
		double angle, height;
		vtxlistfile>>angle>>height;
		angle = Deg2Rad(angle);
		vtxlistfile>>ws;
		vtxlistfile.getline(line, 512);
		vtxlistfile>>ws;

		CString vtxfile = line;
		CString rawpath = vtxfile;
		//rawpath.Replace(".vertex",".txt.raw");
		rawpath.Replace(".vertex",".txt_Error.raw");
		CString patchpath = rawpath;
		patchpath.Replace(".raw",".patch");
		ExtractPlanarPatch(Cfgpath, rawpath, vtxfile,patchpath, lidarprecision);
	}

	vtxlistfile.close();
}

void CLIDARBiasCalibration::ExtractPlanarPatch(CString Cfgpath, CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision)
{
	//double lidarprecision;

	Config.ReadConfigFile(Cfgpath); //Read configuration file
	
	double min[3], max[3];
	ReadPlaneVertex(Vertexpath, PatchList, min, max);

	fstream LidarFile;
	fstream outfile;
	fstream outfile2;
	fstream outfile3;
	
	double OX, OY, OZ;//Spatial offsets(3EA, M)
	OX = Config.Xoffset;
	OY = Config.Yoffset;
	OZ = Config.Zoffset;

	double OXb, OYb, OZb;//Biases in spatial offsets(3EA, M)
	OXb = Config.Xb;
	OYb = Config.Yb;
	OZb = Config.Zb;

	double sOXb, sOYb, sOZb;//Sigma of spatial offsets biases(3EA, M)
	sOXb = Config.sXb;
	sOYb = Config.sYb;
	sOZb = Config.sZb;

	double OO, OP, OK;//Rotational offsets(3EA, deg)
	OO = Config.Ooffset;
	OP = Config.Poffset;
	OK = Config.Koffset;

	double OOb, OPb, OKb;//Biases in rotational offsets(3EA, deg)
	OOb = Config.Ob;
	OPb = Config.Pb;
	OKb = Config.Kb;

	double sOOb, sOPb, sOKb;//Sigma of rotational offsets biases(3EA, deg)
	sOOb = Config.sOb;
	sOPb = Config.sPb;
	sOKb = Config.sKb;

	double Rangeb, sRangeb;//Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)
	Rangeb = Config.Rangeb;
	sRangeb = Config.sRangeb;

	double GPS_X, GPS_Y, GPS_Z; //meter
	double INS_O, INS_P, INS_K; //deg
	double alpha, beta;//deg
	double dist, time;//meter, sec
	///////////////////////////////////////////////////////////////////////////////
	LidarFile.open(Lidarpath, ios::in);
	outfile.open(Planarpath, ios::out);
	outfile2.open(Planarpath+"error.txt", ios::out);
	outfile3.open(Planarpath+"points.txt", ios::out);

	outfile<<"#Patch ID GPS_X(M)	GPS_Y(M)	GPS_Z(M)	INS_O(deg)	INS_P(deg)	INS_K(deg)	alpha(deg)	beta(deg)	dist(M)	time(sec)"<<endl;
	outfile<<"###############################################################################################################"<<endl;
	outfile2<<"#Patch ID a	b	c	are1 are2 are3 are_sum/AREA XG	YG	ZG	Fitting error	time(sec)"<<endl;
	outfile2<<"###############################################################################################################"<<endl;
	
	RemoveCommentLine(LidarFile, '#');
	LidarFile>>ws;
	
	LONG count = 0;
	while(!LidarFile.eof())
	{	
		count++;

		LidarFile>>GPS_X>>GPS_Y>>GPS_Z;
		LidarFile>>INS_O>>INS_P>>INS_K;
		LidarFile>>alpha>>beta;
		LidarFile>>dist>>time;
		
		CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging(3,1,0.);
		
		X0(0,0) = GPS_X; X0(1,0) = GPS_Y; X0(2,0) = GPS_Z;
		P(0,0) = OX; P(1,0) = OY; P(2,0) = OZ;
		Pb(0,0) = OXb; Pb(1,0) = OYb; Pb(2,0) = OZb;
		
		Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = -(dist + Rangeb);
		
		CRotationcoeff2 INS(Deg2Rad(INS_O), Deg2Rad(INS_P), Deg2Rad(INS_K));
		CRotationcoeff2 Swing(Deg2Rad(alpha), Deg2Rad(beta), 0.);
		CRotationcoeff2 ROffset((OO+OOb), (OP+OPb), (OK+OKb));
		
		CSMMatrix<double> XG = X0 + INS.Rmatrix%(P + Pb) 
			+ INS.Rmatrix%ROffset.Rmatrix%Swing.Rmatrix%Ranging;
		
		//내외부 판단.
		bool binner = false;
		for(unsigned int i=0; i<PatchList.GetNumItem(); i++)
		{
			double x1, y1, z1;
			double x2, y2, z2;
			double x3, y3, z3;
			double a, b, c;

			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			x1 = patch.Vertex[0];
			y1 = patch.Vertex[1];
			z1 = patch.Vertex[2];

			x2 = patch.Vertex[3];
			y2 = patch.Vertex[4];
			z2 = patch.Vertex[5];

			x3 = patch.Vertex[6];
			y3 = patch.Vertex[7];
			z3 = patch.Vertex[8];

			a = patch.coeff[0];
			b = patch.coeff[1];
			c = patch.coeff[2];

			double sum=0;
			for(unsigned int j=0; j<3; j++)
			{
				double sumi=0;
				sumi += patch.SD[j*3+0]*patch.SD[j*3+0];
				sumi += patch.SD[j*3+1]*patch.SD[j*3+1];
				sumi += patch.SD[j*3+2]*patch.SD[j*3+2];

				sum += sqrt(sumi);
			}
			double precision = sum/3.0;

			double ARE,are1, are2, are3;
			ARE =  patch.Area;//CalTriangleArea(x1,y1,z1,x2,y2,z2,x3,y3,z3);
			are1 = CalTriangleArea(XG(0,0),XG(1,0),XG(2,0),x2,y2,z2,x3,y3,z3);
			are2 = CalTriangleArea(XG(0,0),XG(1,0),XG(2,0),x1,y1,z1,x3,y3,z3);
			are3 = CalTriangleArea(XG(0,0),XG(1,0),XG(2,0),x2,y2,z2,x1,y1,z1);

			//double fittingerror = vtx.a*XG(0,0) + vtx.b*XG(1,0) + vtx.c*XG(2,0) + 1;
			double temp = a*XG(0,0) + b*XG(1,0) + c*XG(2,0);
			double a_ = a/(1+(temp));
			double b_ = b/(1+(temp));
			double c_ = c/(1+(temp));

			double fittingerror = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);

			//if(((are1/ARE + are2/ARE + are3/ARE)<1))
			//if(((are1/ARE + are2/ARE + are3/ARE)<=(1.0+1.0e-6))&&fittingerror<precision)
			if(((are1/ARE + are2/ARE + are3/ARE)<=(1.0+lidarprecision/ARE))&&fittingerror<lidarprecision)
			{
				outfile.precision(10);
				outfile.setf(ios::scientific );
				outfile<<setw(10)<<patch.stID<<"\t";
				outfile<<setw(10)<<GPS_X<<"\t"<<setw(10)<<GPS_Y<<"\t"<<setw(10)<<GPS_Z<<"\t";
				outfile<<setw(10)<<INS_O<<"\t"<<setw(10)<<INS_P<<"\t"<<setw(10)<<INS_K<<"\t";
				outfile<<setw(10)<<alpha<<"\t"<<setw(10)<<beta<<"\t";
				outfile<<setw(10)<<dist<<"\t"<<setw(10)<<time<<endl;
				outfile.flush();
				
				outfile3.precision(10);
				outfile3.setf(ios::scientific );
				outfile3<<setw(10)<<XG(0,0)<<"\t"<<setw(10)<<XG(1,0)<<"\t"<<setw(10)<<XG(2,0)<<endl;
				outfile3.flush();

				outfile2.precision(10);
				outfile2.setf(ios::scientific );
				outfile2<<"[IN]"<<"\t";
				outfile2<<setw(10)<<patch.stID<<"\t";
				outfile2<<setw(10)<<a<<"\t";
				outfile2<<setw(10)<<b<<"\t";
				outfile2<<setw(10)<<c<<"\t";
				outfile2<<setw(10)<<XG(0,0)<<"\t";
				outfile2<<setw(10)<<XG(1,0)<<"\t";
				outfile2<<setw(10)<<XG(2,0)<<"\t";
				outfile2<<setw(10)<<fittingerror<<"\t"<<setw(10)<<time<<endl;
				outfile2.flush();

				binner = true;
				
				break;
			}
			else
			{
				outfile2.precision(10);
				outfile2.setf(ios::scientific );
				outfile2<<"[OUT]"<<"\t";
				outfile2<<setw(10)<<patch.stID<<"\t";
				outfile2<<setw(10)<<a<<"\t";
				outfile2<<setw(10)<<b<<"\t";
				outfile2<<setw(10)<<c<<"\t";
				outfile2<<setw(10)<<are1<<"\t";
				outfile2<<setw(10)<<are2<<"\t";
				outfile2<<setw(10)<<are3<<"\t";
				outfile2<<setw(10)<<(are1+are2+are3)/ARE<<"\t";
				outfile2<<setw(10)<<XG(0,0)<<"\t";
				outfile2<<setw(10)<<XG(1,0)<<"\t";
				outfile2<<setw(10)<<XG(2,0)<<"\t";
				outfile2<<setw(10)<<fittingerror<<"\t"<<setw(10)<<time<<endl;
				outfile2.flush();
			}
		}

		if(binner == false)
		{
			outfile2<<"[-----OUT-----]"<<endl;
			outfile2.flush();
		}

		LidarFile>>ws;
	}
	
	LidarFile.close();
	outfile.close();
	outfile2.close();
	outfile3.close();
	///////////////////////////////////////////////////////////////////////////////
}

void CLIDARBiasCalibration::ExtractPlanarPatch_Brazil(CString Cfgpath, CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision)
{
	//double lidarprecision;

	Config.ReadConfigFile(Cfgpath); //Read configuration file

	double min[3], max[3];
	ReadPlaneVertex(Vertexpath, PatchList, min, max);

	fstream LidarFile;
	fstream outfile;
	//fstream outfile2;
	fstream outfile3;
	
	double OX, OY, OZ;//Spatial offsets(3EA, M)
	OX = Config.Xoffset;
	OY = Config.Yoffset;
	OZ = Config.Zoffset;

	double OXb, OYb, OZb;//Biases in spatial offsets(3EA, M)
	OXb = Config.Xb;
	OYb = Config.Yb;
	OZb = Config.Zb;

	double sOXb, sOYb, sOZb;//Sigma of spatial offsets biases(3EA, M)
	sOXb = Config.sXb;
	sOYb = Config.sYb;
	sOZb = Config.sZb;

	double OO, OP, OK;//Rotational offsets(3EA, deg)
	OO = Config.Ooffset;
	OP = Config.Poffset;
	OK = Config.Koffset;

	double OOb, OPb, OKb;//Biases in rotational offsets(3EA, deg)
	OOb = Config.Ob;
	OPb = Config.Pb;
	OKb = Config.Kb;

	double sOOb, sOPb, sOKb;//Sigma of rotational offsets biases(3EA, deg)
	sOOb = Config.sOb;
	sOPb = Config.sPb;
	sOKb = Config.sKb;

	double Rangeb, sRangeb;//Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)
	Rangeb = Config.Rangeb;
	sRangeb = Config.sRangeb;

	double GPS_X, GPS_Y, GPS_Z; //meter
	double INS_O, INS_P, INS_K; //deg
	double alpha, beta;//deg
	double dist, time;//meter, sec
	///////////////////////////////////////////////////////////////////////////////
	LidarFile.open(Lidarpath, ios::in);
	outfile.open(Planarpath, ios::out);
	//outfile2.open(Planarpath+"error.txt", ios::out);
	outfile3.open(Planarpath+"points.txt", ios::out);

	outfile<<"#Patch ID GPS_X(M)	GPS_Y(M)	GPS_Z(M)	INS_O(deg)	INS_P(deg)	INS_K(deg)	alpha(deg)	beta(deg)	dist(M)	time(sec)"<<endl;
	outfile<<"###############################################################################################################"<<endl;
	//outfile2<<"#Patch ID a	b	c	are1 are2 are3 are_sum/AREA XG	YG	ZG	Fitting error	time(sec)"<<endl;
	//outfile2<<"###############################################################################################################"<<endl;
	
	RemoveCommentLine(LidarFile, '#');
	LidarFile>>ws;
	
	LONG count = 0;
	while(!LidarFile.eof())
	{	
		count++;

		LidarFile>>GPS_X>>GPS_Y>>GPS_Z;
		LidarFile>>INS_O>>INS_P>>INS_K;
		LidarFile>>alpha>>beta;
		LidarFile>>dist>>time;
		
		CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging(3,1,0.);
		
		X0(0,0) = GPS_X; X0(1,0) = GPS_Y; X0(2,0) = GPS_Z;
		P(0,0) = OX; P(1,0) = OY; P(2,0) = OZ;
		Pb(0,0) = OXb; Pb(1,0) = OYb; Pb(2,0) = OZb;
		
		//Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = -(dist + Rangeb);
		Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = (dist + Rangeb);
		
		CRotationcoeff2 INS2G(Deg2Rad(180),0,0);
		CRotationcoeff2 INS(Deg2Rad(INS_O), Deg2Rad(INS_P), Deg2Rad(INS_K));
		INS.Rmatrix = INS2G.Rmatrix%INS.Rmatrix;

		CRotationcoeff2 Swing(Deg2Rad(alpha), Deg2Rad(beta), 0.);
		CRotationcoeff2 ROffset((OO+OOb), (OP+OPb), (OK+OKb));
		
		CSMMatrix<double> XG = X0 + INS.Rmatrix%(P + Pb) 
			+ INS.Rmatrix%ROffset.Rmatrix%Swing.Rmatrix%Ranging;
		
		//내외부 판단.
		bool binner = false;
		for(unsigned int i=0; i<PatchList.GetNumItem(); i++)
		{
			double x1, y1, z1;
			double x2, y2, z2;
			double x3, y3, z3;
			double a, b, c;

			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			x1 = patch.Vertex[0];
			y1 = patch.Vertex[1];
			z1 = patch.Vertex[2];

			x2 = patch.Vertex[3];
			y2 = patch.Vertex[4];
			z2 = patch.Vertex[5];

			x3 = patch.Vertex[6];
			y3 = patch.Vertex[7];
			z3 = patch.Vertex[8];

			a = patch.coeff[0];
			b = patch.coeff[1];
			c = patch.coeff[2];

			double sum=0;
			for(unsigned int j=0; j<3; j++)
			{
				double sumi=0;
				sumi += patch.SD[j*3+0]*patch.SD[j*3+0];
				sumi += patch.SD[j*3+1]*patch.SD[j*3+1];
				sumi += patch.SD[j*3+2]*patch.SD[j*3+2];

				sum += sqrt(sumi);
			}
			double precision = sum/3.0;

			double ARE,are1, are2, are3;

			ARE = CalTriangleArea(x1,y1,0,x2,y2,0,x3,y3,0);
			are1 = CalTriangleArea(XG(0,0),XG(1,0),0,x2,y2,0,x3,y3,0);
			are2 = CalTriangleArea(XG(0,0),XG(1,0),0,x1,y1,0,x3,y3,0);
			are3 = CalTriangleArea(XG(0,0),XG(1,0),0,x2,y2,0,x1,y1,0);

			double temp = a*XG(0,0) + b*XG(1,0) + c*XG(2,0);
			double a_ = a/(1+(temp));
			double b_ = b/(1+(temp));
			double c_ = c/(1+(temp));

			double fittingerror = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);

			if((fittingerror<lidarprecision) && (are1/ARE + are2/ARE + are3/ARE)<=(1.0+lidarprecision/ARE))
			{
				outfile.precision(10);
				outfile.setf(ios::scientific );
				outfile<<setw(10)<<patch.stID<<"\t";
				outfile<<setw(10)<<GPS_X<<"\t"<<setw(10)<<GPS_Y<<"\t"<<setw(10)<<GPS_Z<<"\t";
				outfile<<setw(10)<<INS_O<<"\t"<<setw(10)<<INS_P<<"\t"<<setw(10)<<INS_K<<"\t";
				outfile<<setw(10)<<alpha<<"\t"<<setw(10)<<beta<<"\t";
				outfile<<setw(10)<<dist<<"\t"<<setw(10)<<time<<endl;
				outfile.flush();
				
				outfile3.precision(10);
				outfile3.setf(ios::scientific );
				outfile3<<setw(10)<<XG(0,0)<<"\t"<<setw(10)<<XG(1,0)<<"\t"<<setw(10)<<XG(2,0)<<endl;
				outfile3.flush();

				binner = true;
				
				break;
			}
			else
			{

			}
		}


		LidarFile>>ws;
	}
	
	LidarFile.close();
	outfile.close();
	//outfile2.close();
	outfile3.close();
	///////////////////////////////////////////////////////////////////////////////
}

void CLIDARBiasCalibration::ExtractSelectedRegion_Brazil(CString Cfgpath, CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision)
{
	//double lidarprecision;

	Config.ReadConfigFile(Cfgpath); //Read configuration file

	double min[3], max[3];
	ReadPlaneVertex(Vertexpath, PatchList, min, max);

	fstream LidarFile;
	fstream *rawfile;
	fstream *pointfile;
	int num_patch = PatchList.GetNumItem();

	rawfile = new fstream[num_patch];
	pointfile = new fstream[num_patch];

	for(int i=0; i<num_patch; i++)
	{
		CString count; count.Format("%d",i+1);
		CString outfilename = Planarpath+count+".txt"; 
		rawfile[i].open(outfilename, ios::out);

		rawfile[i]<<"#Patch ID GPS_X(M)	GPS_Y(M)	GPS_Z(M)	INS_O(deg)	INS_P(deg)	INS_K(deg)	alpha(deg)	beta(deg)	dist(M)	time(sec)"<<endl;
		rawfile[i]<<"###############################################################################################################"<<endl;

		CString pointfilename = Planarpath+count+"_points.txt"; 
		pointfile[i].open(pointfilename, ios::out);
	}
	
	
	double OX, OY, OZ;//Spatial offsets(3EA, M)
	OX = Config.Xoffset;
	OY = Config.Yoffset;
	OZ = Config.Zoffset;

	double OXb, OYb, OZb;//Biases in spatial offsets(3EA, M)
	OXb = Config.Xb;
	OYb = Config.Yb;
	OZb = Config.Zb;

	double sOXb, sOYb, sOZb;//Sigma of spatial offsets biases(3EA, M)
	sOXb = Config.sXb;
	sOYb = Config.sYb;
	sOZb = Config.sZb;

	double OO, OP, OK;//Rotational offsets(3EA, deg)
	OO = Config.Ooffset;
	OP = Config.Poffset;
	OK = Config.Koffset;

	double OOb, OPb, OKb;//Biases in rotational offsets(3EA, deg)
	OOb = Config.Ob;
	OPb = Config.Pb;
	OKb = Config.Kb;

	double sOOb, sOPb, sOKb;//Sigma of rotational offsets biases(3EA, deg)
	sOOb = Config.sOb;
	sOPb = Config.sPb;
	sOKb = Config.sKb;

	double Rangeb, sRangeb;//Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)
	Rangeb = Config.Rangeb;
	sRangeb = Config.sRangeb;

	double GPS_X, GPS_Y, GPS_Z; //meter
	double INS_O, INS_P, INS_K; //deg
	double alpha, beta;//deg
	double dist, time;//meter, sec
	///////////////////////////////////////////////////////////////////////////////
	LidarFile.open(Lidarpath, ios::in);

	RemoveCommentLine(LidarFile, '#');
	LidarFile>>ws;
	
	LONG count = 0;
	while(!LidarFile.eof())
	{	
		count++;

		LidarFile>>GPS_X>>GPS_Y>>GPS_Z;
		LidarFile>>INS_O>>INS_P>>INS_K;
		LidarFile>>alpha>>beta;
		LidarFile>>dist>>time;
		
		CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging(3,1,0.);
		
		X0(0,0) = GPS_X; X0(1,0) = GPS_Y; X0(2,0) = GPS_Z;
		P(0,0) = OX; P(1,0) = OY; P(2,0) = OZ;
		Pb(0,0) = OXb; Pb(1,0) = OYb; Pb(2,0) = OZb;
		
		Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = -(dist + Rangeb);
		
		//CRotationcoeff2 INS2G(Deg2Rad(180),0,0);
		//CRotationcoeff2 INS(Deg2Rad(INS_O), Deg2Rad(INS_P), Deg2Rad(INS_K));
		//INS.Rmatrix = INS2G.Rmatrix%INS.Rmatrix;

		CRotationcoeff2 INS(Deg2Rad(INS_O), Deg2Rad(INS_P), Deg2Rad(INS_K));
		
		CRotationcoeff2 Swing(Deg2Rad(alpha), Deg2Rad(beta), 0.);
		CRotationcoeff2 ROffset((OO+OOb), (OP+OPb), (OK+OKb));
		
		CSMMatrix<double> XG = X0 + INS.Rmatrix%(P + Pb) 
			+ INS.Rmatrix%ROffset.Rmatrix%Swing.Rmatrix%Ranging;
		
		//내외부 판단.
		bool binner = false;
		for(unsigned int i=0; i<PatchList.GetNumItem(); i++)
		{
			double x1, y1, z1;
			double x2, y2, z2;
			double x3, y3, z3;
			double a, b, c;

			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			x1 = patch.Vertex[0];
			y1 = patch.Vertex[1];
			z1 = patch.Vertex[2];

			x2 = patch.Vertex[3];
			y2 = patch.Vertex[4];
			z2 = patch.Vertex[5];

			x3 = patch.Vertex[6];
			y3 = patch.Vertex[7];
			z3 = patch.Vertex[8];

			a = patch.coeff[0];
			b = patch.coeff[1];
			c = patch.coeff[2];

			double sum=0;
			for(unsigned int j=0; j<3; j++)
			{
				double sumi=0;
				sumi += patch.SD[j*3+0]*patch.SD[j*3+0];
				sumi += patch.SD[j*3+1]*patch.SD[j*3+1];
				sumi += patch.SD[j*3+2]*patch.SD[j*3+2];

				sum += sqrt(sumi);
			}
			double precision = sum/3.0;

			double ARE,are1, are2, are3;

			ARE = CalTriangleArea(x1,y1,0,x2,y2,0,x3,y3,0);
			are1 = CalTriangleArea(XG(0,0),XG(1,0),0,x2,y2,0,x3,y3,0);
			are2 = CalTriangleArea(XG(0,0),XG(1,0),0,x1,y1,0,x3,y3,0);
			are3 = CalTriangleArea(XG(0,0),XG(1,0),0,x2,y2,0,x1,y1,0);

			double temp = a*XG(0,0) + b*XG(1,0) + c*XG(2,0);
			double a_ = a/(1+(temp));
			double b_ = b/(1+(temp));
			double c_ = c/(1+(temp));

			double fittingerror = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);

			if((are1/ARE + are2/ARE + are3/ARE)<=(1.0+lidarprecision/ARE))
			{
				rawfile[i].precision(10);
				rawfile[i].setf(ios::scientific );
				rawfile[i]<<setw(10)<<patch.stID<<"\t";
				rawfile[i]<<setw(10)<<GPS_X<<"\t"<<setw(10)<<GPS_Y<<"\t"<<setw(10)<<GPS_Z<<"\t";
				rawfile[i]<<setw(10)<<INS_O<<"\t"<<setw(10)<<INS_P<<"\t"<<setw(10)<<INS_K<<"\t";
				rawfile[i]<<setw(10)<<alpha<<"\t"<<setw(10)<<beta<<"\t";
				rawfile[i]<<setw(10)<<dist<<"\t"<<setw(10)<<time<<endl;
				rawfile[i].flush();
				
				pointfile[i].precision(10);
				pointfile[i].setf(ios::scientific );
				pointfile[i]<<setw(10)<<XG(0,0)<<"\t"<<setw(10)<<XG(1,0)<<"\t"<<setw(10)<<XG(2,0)<<endl;
				pointfile[i].flush();

				binner = true;
				
				break;
			}
			else
			{
			}
		}

		LidarFile>>ws;
	}
	
	LidarFile.close();

	for(int i=0; i<num_patch; i++)
	{
		rawfile[i].close();
		pointfile[i].close();
	}
	///////////////////////////////////////////////////////////////////////////////
}

void CLIDARBiasCalibration::ExtractSelectedRegion_TXT(CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision, bool bIntensity, double areathreshold, CString rawpath)
{
	ExtractSelectedRegion_TXT(Lidarpath, Vertexpath, Planarpath, lidarprecision, areathreshold, bIntensity, rawpath);
}

void CLIDARBiasCalibration::ExtractSelectedRegion_TXT(CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision, double areathreshold, bool bIntensity, CString rawpath)
{
	double min[3], max[3];
	ReadPlaneVertex(Vertexpath, PatchList, min, max);

	fstream LidarFile;
	fstream RawFile;
	fstream *patchpointfile;
	fstream *patchpointfile_raw;

	bool bRaw = false;
	if(rawpath != "") bRaw = true;
	
	///////////////////////////////////////////////////////////////////////////////
	LidarFile.open(Lidarpath, ios::in);

	CString rawfiledescription = "";
	
	if(bRaw == true)
	{
		RawFile.open(rawpath, ios::in);
		
		//version check
		int pos;
		double ver_num = 1.0;
		bool bVersion = FindString(RawFile, "VER", pos);
		if(bVersion == true)
		{
			//To move to begin of file
			RawFile.seekg(0,ios::beg);
			char temp[256];//"VER"
			RawFile>>temp>>ver_num;//version
		}
		else
		{
			//To move to begin of file
			RawFile.seekg(0,ios::beg);//old version without version number(before ver 1.1)
		}

		rawfiledescription.Format("VER %lf",ver_num);
				
		///////////////////////////////////////////////////////////////////////////////
		//Removing titles in a raw file
		//
		RemoveCommentLine(RawFile, '#');
		RawFile>>ws;
	}
	
	RemoveCommentLine(LidarFile, '#');
	LidarFile>>ws;
	
	double X, Y, Z;
	int I;
	
	char line[MAX_LINE_LENGTH];
	
	LONG count = 0;
	
	int num_patch = PatchList.GetNumItem();
	
	patchpointfile = new fstream[num_patch];
	patchpointfile_raw = new fstream[num_patch];
	
	for(int i=0; i<num_patch; i++)
	{
		CString count; count.Format("%d",i+1);
		CString outfilename = Planarpath+count+".txt"; 
		CString outfilename_raw = Planarpath+count+".raw"; 

		patchpointfile[i].open(outfilename, ios::out);
		
		if(bRaw == true)
		{
			
			patchpointfile_raw[i].open(outfilename_raw, ios::out);

			patchpointfile_raw[i]<<rawfiledescription<<endl;
		}
	}
	
	while(!LidarFile.eof())
	{	
		count++;		
		
		if(bRaw == true)
		{
			RawFile.getline(line,MAX_LINE_LENGTH);
			RawFile>>ws;
		}
		
		LidarFile>>X>>Y>>Z;
		if(bIntensity == true) LidarFile>>I;
		
		for(int i=0; i<num_patch; i++)
		{
			double x1, y1, z1;
			double x2, y2, z2;
			double x3, y3, z3;
			
			CLIDARPlanarPatch patch = PatchList.GetAt(i);
			
			if((X>=patch.minX)&&(X<=patch.maxX)&&(Y>=patch.minY)&&(Y<=patch.maxY))
			{
				x1 = patch.Vertex[0];
				y1 = patch.Vertex[1];
				z1 = patch.Vertex[2];
				
				x2 = patch.Vertex[3];
				y2 = patch.Vertex[4];
				z2 = patch.Vertex[5];
				
				x3 = patch.Vertex[6];
				y3 = patch.Vertex[7];
				z3 = patch.Vertex[8];
				
				double ARE,are1, are2, are3;
				
				ARE = CalTriangleArea(x1,y1,0,x2,y2,0,x3,y3,0);
				are1 = CalTriangleArea(X,Y,0,x2,y2,0,x3,y3,0);
				are2 = CalTriangleArea(X,Y,0,x1,y1,0,x3,y3,0);
				are3 = CalTriangleArea(X,Y,0,x2,y2,0,x1,y1,0);
				
				if((are1/ARE + are2/ARE + are3/ARE) <= areathreshold)
				{
					ARE = CalTriangleArea(x1,y1,0,x2,y2,0,x3,y3,0);
					are1 = CalTriangleArea(X,Y,0,x2,y2,0,x3,y3,0);
					are2 = CalTriangleArea(X,Y,0,x1,y1,0,x3,y3,0);
					are3 = CalTriangleArea(X,Y,0,x2,y2,0,x1,y1,0);
					
					patchpointfile[i].precision(10);
					patchpointfile[i].setf(ios::fixed, ios::floatfield);
					patchpointfile[i]<<setw(10)<<X<<"\t"<<setw(10)<<Y<<"\t"<<setw(10)<<Z<<endl;
					patchpointfile[i].flush();
					
					if(bRaw == true)
					{
						
						patchpointfile_raw[i]<<line<<endl;
						patchpointfile_raw[i].flush();
					}
					
					break;
				}
			}
		}
		
		LidarFile>>ws;
	}
	
	LidarFile.close();
	
	for(int i=0; i<num_patch; i++)
	{
		patchpointfile[i].close();
	}

	if(bRaw == true)
	{
		RawFile.close();

		for(int i=0; i<num_patch; i++)
		{
			patchpointfile[i].close();
		}
	}

	///////////////////////////////////////////////////////////////////////////////
}

void CLIDARBiasCalibration::ReadPlaneVertex(CString vtxfile, CSMList<CLIDARPlanarPatch> &PatchList, double min[], double max[])
{	
	//Initialize min and max values
	min[0] = min[1] = min[2] = 1.0e99;
	max[0] = max[1] = max[2] = -1.0e99;

	double x, y, z;
	double sx, sy, sz;
	//int id;

	///////////////////////////////////////////////////////////////////////////////
	fstream VertexFile;
	VertexFile.open(vtxfile, ios::in);

	//version check
	int pos;
	double ver_num = 1.0;
	bool bVersion = FindString(VertexFile, "VER", pos);
	if(bVersion == true)
	{
		//To move to begin of file
		VertexFile.seekg(0,ios::beg);
		char temp[256];//"VER"
		VertexFile>>temp>>ver_num;//version
	}
	else
	{
		//To move to begin of file
		VertexFile.seekg(0,ios::beg);//old version without version number(before ver 1.1)
	}
	
	RemoveCommentLine(VertexFile, '#');
	VertexFile>>ws;
	while(!VertexFile.eof())
	{
		CLIDARPlanarPatch patch;
		
		char temp[256];
		VertexFile>>temp;
		patch.stID = temp;//Patch ID

		////////////////////////////////////////////////////////////////////////////
		//1st VERTEX
		//
		if(ver_num >= 1.2)//vertex A ID
		{
			VertexFile>>patch.ID_A;
		}

		VertexFile>>x>>y>>z;//coordinate
		patch.Vertex[0] = x; patch.Vertex[1] = y; patch.Vertex[2] = z;
		
		if(ver_num >= 1.1)
		{
			VertexFile>>sx>>sy>>sz;//sigma of each coordiante
			patch.SD[0] = sx; patch.SD[1] = sy; patch.SD[2] = sz;
		}
		else
		{
			patch.SD[0] = 1.; patch.SD[1] = 1.; patch.SD[2] = 1.;
		}
		
		patch.maxX = patch.minX = x; patch.maxY = patch.minY = y; patch.minZ = patch.maxZ = z;

		////////////////////////////////////////////////////////////////////////////
		//2nd VERTEX
		//
		if(ver_num >= 1.2)//vertex B ID
		{
			VertexFile>>patch.ID_B;
		}
		
		VertexFile>>x>>y>>z;//coordinate
		patch.Vertex[3] = x; patch.Vertex[4] = y; patch.Vertex[5] = z;
		if(ver_num >= 1.1)
		{
			VertexFile>>sx>>sy>>sz;//sigma of each coordiante
			patch.SD[3] = sx; patch.SD[4] = sy; patch.SD[5] = sz;
		}
		else
		{
			patch.SD[0] = 1.; patch.SD[1] = 1.; patch.SD[2] = 1.;
		}

		if(patch.maxX < x) patch.maxX = x;
		if(patch.maxY < y) patch.maxY = y;
		if(patch.maxZ < z) patch.maxZ = z;
		
		if(patch.minX > x) patch.minX = x;
		if(patch.minY > y) patch.minY = y;
		if(patch.minZ > z) patch.minZ = z;
		
		////////////////////////////////////////////////////////////////////////////
		//3rd VERTEX
		//
		if(ver_num >= 1.2)//vertex C ID
		{
			VertexFile>>patch.ID_C;
		}

		VertexFile>>x>>y>>z;//coordinate
		patch.Vertex[6] = x; patch.Vertex[7] = y; patch.Vertex[8] = z;
		if(ver_num >= 1.1)
		{
			VertexFile>>sx>>sy>>sz;//sigma of each coordiante
			patch.SD[6] = sx; patch.SD[7] = sy; patch.SD[8] = sz;
		}
		else
		{
			patch.SD[0] = 1.; patch.SD[1] = 1.; patch.SD[2] = 1.;
		}
		
		if(patch.maxX < x) patch.maxX = x;
		if(patch.maxY < y) patch.maxY = y;
		if(patch.maxZ < z) patch.maxZ = z;
		
		if(patch.minX > x) patch.minX = x;
		if(patch.minY > y) patch.minY = y;
		if(patch.minZ > z) patch.minZ = z;

		if(min[0] > patch.minX) min[0] = patch.minX;
		if(min[1] > patch.minY) min[1] = patch.minY;
		if(min[2] > patch.minZ) min[2] = patch.minZ;

		if(max[0] < patch.maxX) max[0] = patch.maxX;
		if(max[1] < patch.maxY) max[1] = patch.maxY;
		if(max[2] < patch.maxZ) max[2] = patch.maxZ;
				
		PatchList.AddTail(patch);
		
		RemoveCommentLine(VertexFile, '#');
		VertexFile>>ws;
	}
	
	VertexFile.close();

	const int num_vertex=3;
	
	//To calculate plane equation parameters
	for(unsigned int i=0; i<PatchList.GetNumItem(); i++)
	{
		//To get patch
		CLIDARPlanarPatch *patch = PatchList.GetHandleAt(i);
		
		//Plane equation parameters
		double coeff[NUM_PLANAR_PARAM];
		
		//Vertex points for planar patches
		double points[num_vertex*3]; //triangle <--temporary
		
		//To copy Vertex points
		for(int k=0; k<(num_vertex*3); k++)
		{
			points[k] = patch->Vertex[k];
		}
		
		//Plane equation parameters
		//3개 이상의 포인트를 사용하여 면을 규정하는 겨우, 확장 가능함.
		//To calculate plane equation parameters
		CalPlaneCoeff(points, num_vertex, coeff);
		//CalPlaneCoeff_New(points, num_vertex, coeff);

		patch->coeff[0] = coeff[0];
		patch->coeff[1] = coeff[1];
		patch->coeff[2] = coeff[2];

		//Area of a patche
		double x1 = patch->Vertex[0];
		double y1 = patch->Vertex[1];
		double z1 = patch->Vertex[2];

		double x2 = patch->Vertex[3];
		double y2 = patch->Vertex[4];
		double z2 = patch->Vertex[5];

		double x3 = patch->Vertex[6];
		double y3 = patch->Vertex[7];
		double z3 = patch->Vertex[8];

		patch->Area =  CalTriangleArea(x1,y1,z1,x2,y2,z2,x3,y3,z3);
	}
}

void CLIDARBiasCalibration::ReadTerraPoints(CString filepath, CSMList<SM3DPoint> &PointList, CSMList<CLIDARRawPoint> &LidarRawList)
{	
	fstream LidarFile;
	LidarFile.open(filepath, ios::in);
	
	int intensity;
	double time, scan, Roll, Pitch, Yaw;
	CLIDARRawPoint rawpoint;
	SM3DPoint point;
	int nID=0;
	while(!LidarFile.eof())
	{
		nID++;
//old format
		point.nID = nID;
		LidarFile>>point.X>>point.Y>>point.Z;
		LidarFile>>intensity;
		LidarFile>>time;
		LidarFile>>Pitch>>Roll>>Yaw; 
		rawpoint.INS_O=Deg2Rad(Pitch);rawpoint.INS_P=Deg2Rad(Roll);rawpoint.INS_K=Deg2Rad(Yaw); //roataion about X, rotation about Y, rotation about Z
		LidarFile>>rawpoint.GPS_X>>rawpoint.GPS_Y>>rawpoint.GPS_Z;
		LidarFile>>scan; 
		rawpoint.beta = Deg2Rad(scan); //rotation about Y(in laser unit)
		rawpoint.alpha = 0.0;
		LidarFile>>rawpoint.dist;

//new format
//		point.nID = nID;
//		LidarFile>>point.X>>point.Y>>point.Z;
//		LidarFile>>time;
//		LidarFile>>Pitch>>Roll>>Yaw;
//		rawpoint.INS_O=Deg2Rad(Pitch);rawpoint.INS_P=Deg2Rad(Roll);rawpoint.INS_K=Deg2Rad(Yaw); //roataion about X, rotation about Y, rotation about Z
//		LidarFile>>intensity;
//		LidarFile>>rawpoint.GPS_X>>rawpoint.GPS_Y>>rawpoint.GPS_Z;
//		LidarFile>>scan; 
//		rawpoint.beta = Deg2Rad(scan); //rotation about Y(in laser unit)
//		rawpoint.alpha = 0.0;
//		LidarFile>>rawpoint.dist;

		for(int i=0; i<9; i++)
			LidarFile>>point.cov[i];

		PointList.AddTail(point);
		LidarRawList.AddTail(rawpoint);

		LidarFile>>ws;
	}
	
	LidarFile.close();
}

void CLIDARBiasCalibration::ReadPatch(CString filepath, CSMList<SM3DPoint> &PointList, CSMList<CLIDARRawPoint> &LidarRawList)
{	
	fstream LidarFile;
	LidarFile.open(filepath, ios::in);
	
	CLIDARRawPoint rawpoint;
	SM3DPoint point;
	int nID=0;
	while(!LidarFile.eof())
	{
		nID++;
		
		CLIDARCalibrationPoint P;
		if(false == ReadRecord(LidarFile, filecontents, P)) return;

		point.nID = nID;
		point.X = P.X;
		point.Y = P.Y;
		point.Z = P.Z;

		//memcpy(point.cov, P.P,sizeof(double)*9);
		
		int index = 0;
		for(int i=0; i<3; i++)
		{
			for(int j=0; j<3; j++)
			{
				point.cov[index] = P.P(i, j);
				index++;
			}
		}
		
		rawpoint.INS_O = P.INS_O;
		rawpoint.INS_P = P.INS_P;
		rawpoint.INS_K = P.INS_K;

		rawpoint.GPS_X = P.GPS_X;
		rawpoint.GPS_Y = P.GPS_Y;
		rawpoint.GPS_Z = P.GPS_Z;

		rawpoint.beta = P.beta;
		rawpoint.alpha = 0;
		rawpoint.dist = P.dist;

		rawpoint.time = P.time;

		rawpoint.intensity = P.intensity;

// old pts format
//		point.nID = nID;
//		LidarFile>>point.X>>point.Y>>point.Z;
//		LidarFile>>intensity;
//		LidarFile>>time;
//		LidarFile>>Pitch>>Roll>>Yaw; 
//		rawpoint.INS_O=Deg2Rad(Pitch);rawpoint.INS_P=Deg2Rad(Roll);rawpoint.INS_K=Deg2Rad(Yaw); //roataion about X, rotation about Y, rotation about Z
//		LidarFile>>rawpoint.GPS_X>>rawpoint.GPS_Y>>rawpoint.GPS_Z;
//		LidarFile>>alpha>>beta; 
//		rawpoint.alpha = Deg2Rad(alpha); //rotation about Y(in laser unit)
//		rawpoint.beta = Deg2Rad(beta); //rotation about Y(in laser unit)
//		LidarFile>>rawpoint.dist;

// new pts format
//		point.nID = nID;
//		LidarFile>>point.X>>point.Y>>point.Z;
//		LidarFile>>time;
//		LidarFile>>Pitch>>Roll>>Yaw;
//		rawpoint.INS_O=Deg2Rad(Pitch);rawpoint.INS_P=Deg2Rad(Roll);rawpoint.INS_K=Deg2Rad(Yaw); //roataion about X, rotation about Y, rotation about Z
//		LidarFile>>intensity;
//		LidarFile>>rawpoint.GPS_X>>rawpoint.GPS_Y>>rawpoint.GPS_Z;
//		LidarFile>>beta; 
//		rawpoint.beta = Deg2Rad(beta); //rotation about Y(in laser unit)
//		rawpoint.alpha = 0.0;
//		LidarFile>>rawpoint.dist;

//		for(int i=0; i<9; i++)
//			LidarFile>>point.cov[i];

		PointList.AddTail(point);
		LidarRawList.AddTail(rawpoint);

		LidarFile>>ws;
	}


	LidarFile.close();
}

void CLIDARBiasCalibration::ReadPlanarPoints(CString pointspath)
{
	double GPS_X, GPS_Y, GPS_Z; //meter
	double INS_O, INS_P, INS_K; //deg
	double alpha, beta;//deg
	double dist, time;//meter, sec
	int intensity;
	//int patchid;//id of a patch
	CString patchid;//id of a patch
	
	///////////////////////////////////////////////////////////////////////////////
	fstream LidarFile;
	LidarFile.open(pointspath, ios::in);

	//version check
	int pos;
	double ver_num = 1.0;
	bool bVersion = FindString(LidarFile, "VER", pos);
	if(bVersion == true)
	{
		//To move to begin of file
		LidarFile.seekg(0,ios::beg);
		char temp[256];//"VER"
		LidarFile>>temp>>ver_num;//version
	}
	else
	{
		//To move to begin of file
		LidarFile.seekg(0,ios::beg);//old version without version number(before ver 1.1)
	}
	
	RemoveCommentLine(LidarFile, '#');
	LidarFile>>ws;

	int count = 0;
	
	while(!LidarFile.eof())
	{
		count ++;
		//LidarFile>>patchid;
		char temp[256];
		LidarFile>>temp;
		patchid = temp;
		
		LidarFile>>GPS_X>>GPS_Y>>GPS_Z;
		LidarFile>>INS_O>>INS_P>>INS_K;
		LidarFile>>alpha>>beta;
		LidarFile>>dist>>time;
		if(ver_num >= 1.1) LidarFile>>intensity;
		
		CLIDARRawPoint point;
		
		point.alpha = Deg2Rad(alpha);
		point.beta = Deg2Rad(beta);
		
		point.dist = dist;
		
		point.GPS_X = GPS_X;
		point.GPS_Y = GPS_Y;
		point.GPS_Z = GPS_Z;
		
		
		point.INS_O = Deg2Rad(INS_O);
		point.INS_P = Deg2Rad(INS_P);
		point.INS_K = Deg2Rad(INS_K);
		
		for(unsigned int i=0; i<PatchList.GetNumItem(); i++)
		{
			CLIDARPlanarPatch *patch = PatchList.GetHandleAt(i);
			//if(patch->nID == patchid)
			if(patch->stID == patchid)
			{
				patch->PointList.AddTail(point);
				break;
			}
		}

		RemoveCommentLine(LidarFile, '#');
		LidarFile>>ws;
	}
	
	LidarFile.close();

	for(unsigned int i=0; i<PatchList.GetNumItem(); i++)
	{
		CLIDARPlanarPatch *patch = PatchList.GetHandleAt(i);
		int num = patch->PointList.GetNumItem();
		if(num < NUM_MINIMUM_POINT)
		{
			patch->PointList.DeleteData(i);
		}
	}
	
	
}

void CLIDARBiasCalibration::ExtractIntensityFromBrazilStrip(CString striptxtpath, CString trajectorypath, CString configpath, CString intensitypath)
{
	char line[MAX_LINE_LENGTH];
	double GPSTime;
	int pulsecount;
	double Range1;
	double Range2;
	double Range3;
	double Range4;
	int intensity1;
	int intensity2;
	int intensity3;
	int intensity4;
	double scanrad;
	double pitchrad;
	double rollrad;
	double headingrad;
	double latitude;
	double longitude;
	double height;

	double LIDARtime0=-1.0, LIDARtime1=-1.0;

	fstream stripfile;
	fstream trajectory;

	fstream xyzfile_first;
	fstream xyzfile_last;

	fstream info_first;
	fstream info_last;
	
	stripfile.open(striptxtpath, ios::in);
	trajectory.open(trajectorypath, ios::in);
	
	CString xyzfilepath_first;
	xyzfilepath_first = intensitypath+"_first.txt";

	CString xyzfilepath_last;
	xyzfilepath_last = intensitypath+"_last.txt";

	xyzfile_first.open(xyzfilepath_first, ios::out);
	
	xyzfile_last.open(xyzfilepath_last, ios::out);

	CString infofilepath_first = intensitypath+"_info_first.txt";

	CString infofilepath_last = intensitypath+"_info_last.txt";

	info_first.open(infofilepath_first, ios::out);
	
	info_last.open(infofilepath_last, ios::out);
	
	stripfile>>ws;
	stripfile.getline(line,MAX_LINE_LENGTH);

	for(int i=0; i<23; i++)
	{
		if(!trajectory.eof())
		{
			trajectory.getline(line,MAX_LINE_LENGTH);
		}
	}

	double Time1=0.0, Time2=0.0;
	double Pos_X1=0.0, Pos_Y1=0.0, Pos_Z1=0.0;
	double Pos_X2=0.0, Pos_Y2=0.0, Pos_Z2=0.0;
	double Roll1=0.0, Pitch1=0.0, Yaw1=0.0;
	double Roll2=0.0, Pitch2=0.0, Yaw2=0.0;

	CLIDARConfig Cfg;
	Cfg.ReadConfigFile(configpath);

	double sTime=-1.0, eTime=-1.0;
	//double MinX_first=1.0e20, MaxX_first=-1.0e20, MinY_first=1.0e20, MaxY_first=-1.0e20, MinZ_first=1.0e20, MaxZ_first=-1.0e20;
	//double MinX_last=1.0e20, MaxX_last=-1.0e20, MinY_last=1.0e20, MaxY_last=-1.0e20, MinZ_last=1.0e20, MaxZ_last=-1.0e20;
	double MinX_first=1.0e20, MaxX_first=-1.0e20;
	double MinX_Y_first;
	double MinX_Z_first;
	double MaxX_Y_first;
	double MaxX_Z_first;

	double MinX_last=1.0e20, MaxX_last=-1.0e20;
	double MinX_Y_last;
	double MinX_Z_last;
	double MaxX_Y_last;
	double MaxX_Z_last;

	double MinY_first=1.0e20, MaxY_first=-1.0e20;
	double MinY_X_first;
	double MinY_Z_first;
	double MaxY_X_first;
	double MaxY_Z_first;

	double MinY_last=1.0e20, MaxY_last=-1.0e20;
	double MinY_X_last;
	double MinY_Z_last;
	double MaxY_X_last;
	double MaxY_Z_last;

	double MinZ_first=1.0e20, MaxZ_first=-1.0e20;
	double MinZ_X_first;
	double MinZ_Y_first;
	double MaxZ_X_first;
	double MaxZ_Y_first;

	double MinZ_last=1.0e20, MaxZ_last=-1.0e20;
	double MinZ_X_last;
	double MinZ_Y_last;
	double MaxZ_X_last;
	double MaxZ_Y_last;
	
	double MinSwing=1.0e20, MaxSwing=-1.0e20;

	bool newstrip = true;

	long p_num = 0;
		
	do
	{
		///////////////////////////////////////
		//Read Strip File
		///////////////////////////////////////
		//GPS time
		FindNumber(stripfile);
		stripfile>>GPSTime;

		LIDARtime0 = GPSTime;
		
		//pulse count
		FindNumber(stripfile);
		stripfile>>pulsecount;
		
		//1st range
		FindNumber(stripfile);
		stripfile>>Range1;
		
		//2nd range
		FindNumber(stripfile);
		stripfile>>Range2;
		
		//3rd range
		FindNumber(stripfile);
		stripfile>>Range3;
		
		//4th range
		FindNumber(stripfile);
		stripfile>>Range4;
		
		//1st intensity
		FindNumber(stripfile);
		stripfile>>intensity1;
		
		//2nd intensity
		FindNumber(stripfile);
		stripfile>>intensity2;
		
		//3rd intensity
		FindNumber(stripfile);
		stripfile>>intensity3;
		
		//4th intensity
		FindNumber(stripfile);
		stripfile>>intensity4;
		
		//scan angle (rad)
		FindNumber(stripfile);
		stripfile>>scanrad;
		
		//pitch angle (rad)
		FindNumber(stripfile);
		stripfile>>pitchrad;
		
		//roll angle (rad)
		FindNumber(stripfile);
		stripfile>>rollrad;
		
		//heading angle (rad)
		FindNumber(stripfile);
		stripfile>>headingrad;
		
		//latitude (rad)
		FindNumber(stripfile);
		stripfile>>latitude;
		
		//longitude (rad)
		FindNumber(stripfile);
		stripfile>>longitude;
		
		//height (meter)
		FindNumber(stripfile);
		stripfile>>height;
		
		//eat white space
		stripfile>>ws;
		
		double dumy;
		///////////////////////////////////////
		//Read Trajectory File
		///////////////////////////////////////
		if(GPSTime > Time2)
		{
			
			while(Time2<GPSTime)
			{
				//Time (sec)				
				Time1 = Time2;
				trajectory>>Time2;
				//dist (M)				
				trajectory>>dumy;
				//Pos(X,Y,Z)
				Pos_X1 = Pos_X2;
				Pos_Y1 = Pos_Y2;
				Pos_Z1 = Pos_Z2;
				trajectory>>Pos_X2;
				trajectory>>Pos_Y2;
				trajectory>>Pos_Z2;
				//Pos(Lat,Lon,H)
				trajectory>>dumy;
				trajectory>>dumy;
				trajectory>>dumy;
				//Attitute
				Pitch1 = Pitch2;
				Roll1 = Roll2;
				Yaw1 = Yaw2;
				trajectory>>Pitch2;
				trajectory>>Roll2;
				trajectory>>Yaw2;
				
				//getline
				trajectory.getline(line,MAX_LINE_LENGTH);
				
			}
		}
				
		if(Time1 != 0.0)
		{
			double T_diff1 = (GPSTime - Time1);
			double T_diff2 = (Time2 - Time1);
			double ratio = T_diff1/T_diff2;
			
			double Pos_X, Pos_Y, Pos_Z;
			Pos_X = Pos_X1 + (Pos_X2-Pos_X1)*ratio;
			Pos_Y = Pos_Y1 + (Pos_Y2-Pos_Y1)*ratio;
			Pos_Z = Pos_Z1 + (Pos_Z2-Pos_Z1)*ratio;
			double Pitch, Roll, Yaw;
			Pitch = Pitch1 + (Pitch2-Pitch1)*ratio;
			Roll = Roll1 + (Roll2-Roll1)*ratio;
			Yaw = Yaw1 + (Yaw2-Yaw1)*ratio;
			
			p_num ++;
			
			///////////////////////////////////////
			//Write Raw File
			///////////////////////////////////////
			CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging1(3,1,0.), Ranging4(3,1,0.);
			X0(0,0) = Pos_X; X0(1,0) = Pos_Y; X0(2,0) = Pos_Z;
			P(0,0) = Cfg.Xoffset; P(1,0) = Cfg.Yoffset; P(2,0) = Cfg.Zoffset;
			Pb(0,0) = Cfg.Xb; Pb(1,0) = Cfg.Yb; Pb(2,0) = Cfg.Zb;
			
			Ranging1(0,0) = 0; Ranging1(1,0) = 0; Ranging1(2,0) = (Range1 + Cfg.Rangeb);
			Ranging4(0,0) = 0; Ranging4(1,0) = 0; Ranging4(2,0) = (Range4 + Cfg.Rangeb);
			
			//CRotationcoeff2 INS(Deg2Rad(Pitch), Deg2Rad(Roll), Deg2Rad(Yaw));
			CRotationcoeff2 INS(pitchrad, rollrad, headingrad);
			CRotationcoeff2 INS2G(Deg2Rad(180),0,0);
			
			INS.Rmatrix = INS2G.Rmatrix%INS.Rmatrix;
			
			CRotationcoeff2 Swing(0.0, scanrad, 0.);
			CRotationcoeff2 ROffset((Cfg.Ooffset+Cfg.Ob), (Cfg.Koffset+Cfg.Kb), (Cfg.Koffset+Cfg.Kb));
			CSMMatrix<double> XG1 = X0 + INS.Rmatrix%(P + Pb) 
				+ INS.Rmatrix%ROffset.Rmatrix%Swing.Rmatrix%Ranging1;
			CSMMatrix<double> XG4 = X0 + INS.Rmatrix%(P + Pb) 
				+ INS.Rmatrix%ROffset.Rmatrix%Swing.Rmatrix%Ranging4;
			
			xyzfile_first.precision(10);
			xyzfile_first.setf(ios::fixed, ios::floatfield);
			xyzfile_first<<setw(10)<<XG1(0,0)<<"\t"<<setw(10)<<XG1(1,0)<<"\t"<<setw(10)<<XG1(2,0)<<"\t"<<setw(10)<<intensity1<<endl;
			xyzfile_first.flush();
			
			if(MinX_first>XG1(0,0)) {MinX_first   = XG1(0,0);	MinX_Y_first = XG1(1,0);	MinX_Z_first = XG1(2,0);}
			if(MinY_first>XG1(1,0)) {MinY_X_first = XG1(0,0);	MinY_first	 = XG1(1,0);	MinY_Z_first = XG1(2,0);}
			if(MinZ_first>XG1(2,0)) {MinZ_X_first = XG1(0,0);	MinZ_Y_first = XG1(1,0);	MinZ_first   = XG1(2,0);}
			
			if(MaxX_first<XG1(0,0)) {MaxX_first   = XG1(0,0);	MaxX_Y_first = XG1(1,0);	MaxX_Z_first = XG1(2,0);}
			if(MaxY_first<XG1(1,0)) {MaxY_X_first = XG1(0,0);	MaxY_first	 = XG1(1,0);	MaxY_Z_first = XG1(2,0);}
			if(MaxZ_first<XG1(2,0)) {MaxZ_X_first = XG1(0,0);	MaxZ_Y_first = XG1(1,0);	MaxZ_first   = XG1(2,0);}
			
			
			xyzfile_last.precision(10);
			xyzfile_last.setf(ios::fixed, ios::floatfield);
			xyzfile_last<<setw(10)<<XG4(0,0)<<"\t"<<setw(10)<<XG4(1,0)<<"\t"<<setw(10)<<XG4(2,0)<<"\t"<<setw(10)<<intensity4<<endl;
			xyzfile_last.flush();
			
			if(MinX_last>XG1(0,0)) {MinX_last   = XG1(0,0);	MinX_Y_last = XG1(1,0);	MinX_Z_last = XG1(2,0);}
			if(MinY_last>XG1(1,0)) {MinY_X_last = XG1(0,0);	MinY_last	= XG1(1,0);	MinY_Z_last = XG1(2,0);}
			if(MinZ_last>XG1(2,0)) {MinZ_X_last = XG1(0,0);	MinZ_Y_last = XG1(1,0);	MinZ_last   = XG1(2,0);}
			
			if(MaxX_last<XG1(0,0)) {MaxX_last   = XG1(0,0);	MaxX_Y_last = XG1(1,0);	MaxX_Z_last = XG1(2,0);}
			if(MaxY_last<XG1(1,0)) {MaxY_X_last = XG1(0,0);	MaxY_last	= XG1(1,0);	MaxY_Z_last = XG1(2,0);}
			if(MaxZ_last<XG1(2,0)) {MaxZ_X_last = XG1(0,0);	MaxZ_Y_last = XG1(1,0);	MaxZ_last   = XG1(2,0);}
			
			
			if(newstrip == true)
			{						
				sTime = GPSTime;
				
				info_first.precision(10);
				info_first.setf(ios::fixed, ios::floatfield);
				info_first<<"sTime: "<<setw(10)<<sTime<<endl;
				info_first.flush();
				
				info_last.precision(10);
				info_last.setf(ios::fixed, ios::floatfield);
				info_last<<"sTime: "<<setw(10)<<sTime<<endl;
				info_last.flush();

				newstrip = false;
			}
		}
			
	}while((!stripfile.eof())&&(!trajectory.eof()));
	
	eTime = GPSTime;
	
	info_first<<"eTime: "<<setw(10)<<eTime<<endl;
	info_first.flush();
	
	info_first<<"Min(X): "<<MinX_first<<"\t"<<"Min(X)Y: "<<MinX_Y_first<<"\t"<<"Min(X)Z: "<<MinX_Z_first<<endl;
	info_first<<"Min(Y)X: "<<MinY_X_first<<"\t"<<"Min(Y): "<<MinY_first<<"\t"<<"Min(Y)Z: "<<MinY_Z_first<<endl;
	info_first<<"Min(Z)X: "<<MinZ_X_first<<"\t"<<"Min(Z)Y: "<<MinZ_Y_first<<"\t"<<"Min(Z): "<<MinZ_first<<endl;
	
	info_first<<"Max(X): "<<MaxX_first<<"\t"<<"Max(X)Y: "<<MaxX_Y_first<<"\t"<<"Max(X)Z: "<<MaxX_Z_first<<endl;
	info_first<<"Max(Y)X: "<<MaxY_X_first<<"\t"<<"Max(Y): "<<MaxY_first<<"\t"<<"Max(Y)Z: "<<MaxY_Z_first<<endl;
	info_first<<"Max(Z)X: "<<MaxZ_X_first<<"\t"<<"Max(Z)Y: "<<MaxZ_Y_first<<"\t"<<"Max(Z): "<<MaxZ_first<<endl;
	
	info_first<<"#Point: "<<p_num<<endl;
	
	info_last<<"eTime: "<<setw(10)<<eTime<<endl;
	info_last.flush();
	
	info_last<<"Min(X): "<<MinX_last<<"\t"<<"Min(X)Y: "<<MinX_Y_last<<"\t"<<"Min(X)Z: "<<MinX_Z_last<<endl;
	info_last<<"Min(Y)X: "<<MinY_X_last<<"\t"<<"Min(Y): "<<MinY_last<<"\t"<<"Min(Y)Z: "<<MinY_Z_last<<endl;
	info_last<<"Min(Z)X: "<<MinZ_X_last<<"\t"<<"Min(Z)Y: "<<MinZ_Y_last<<"\t"<<"Min(Z): "<<MinZ_last<<endl;
	
	info_last<<"Max(X): "<<MaxX_last<<"\t"<<"Max(X)Y: "<<MaxX_Y_last<<"\t"<<"Max(X)Z: "<<MaxX_Z_last<<endl;
	info_last<<"Max(Y)X: "<<MaxY_X_last<<"\t"<<"Max(Y): "<<MaxY_last<<"\t"<<"Max(Y)Z: "<<MaxY_Z_last<<endl;
	info_last<<"Max(Z)X: "<<MaxZ_X_last<<"\t"<<"Max(Z)Y: "<<MaxZ_Y_last<<"\t"<<"Max(Z): "<<MaxZ_last<<endl;
	
	
	info_last<<"#Point: "<<p_num<<endl;

	double area = (MaxX_first - MinX_first)*(MaxY_first - MinY_first);
	info_first<<"Area: "<<area<<endl;
	
	info_first<<"Density: "<<p_num/area<<endl;

	area = (MaxX_last - MinX_last)*(MaxY_last - MinY_last);
	info_last<<"Area: "<<area<<endl;
	
	info_last<<"Density: "<<p_num/area<<endl;

	info_first.flush();
	info_last.flush();


	stripfile.close();
	trajectory.close();

	xyzfile_first.close();
	
	xyzfile_last.close();
}

void CLIDARBiasCalibration::ReadBrazilStripTxt(CString striptxtpath, CString rawpath)
{
	char line[MAX_LINE_LENGTH];
	double GPSTime;
	int pulsecount;
	double Range1;
	double Range2;
	double Range3;
	double Range4;
	int intensity1;
	int intensity2;
	int intensity3;
	int intensity4;
	double scanrad;
	double pitchrad;
	double rollrad;
	double headingrad;
	double latitude;
	double longitude;
	double height;
	
	CString newrawpath_first = rawpath+"_first.raw";
	CString newrawpath_last = rawpath+"_last.raw";
	
	fstream stripfile;
	
	fstream rawfile_first;
	fstream rawfile_last;
	
	stripfile.open(striptxtpath, ios::in);
	
	rawfile_first.open(newrawpath_first, ios::out);
	rawfile_first.precision(10);
	rawfile_first.setf(ios::fixed, ios::floatfield);
	
	rawfile_last.open(newrawpath_last, ios::out);
	rawfile_last.precision(10);
	rawfile_last.setf(ios::fixed, ios::floatfield);
	
	rawfile_first<<"VER 1.1"<<endl;
	rawfile_first<<"#Pos_X(M)	Pos_Y(M)	Pos_Z(M)	Pitch(deg)	Roll(deg)	Heading(deg)	alpha(deg)	betta(deg)	range(M)	time(sec)	intensity"<<endl;
	rawfile_first<<"######################################################################################################################################"<<endl;
	
	rawfile_last<<"VER 1.1"<<endl;
	rawfile_last<<"#Pos_X(M)	Pos_Y(M)	Pos_Z(M)	Pitch(deg)	Roll(deg)	Heading(deg)	alpha(deg)	betta(deg)	range(M)	time(sec)	intensity"<<endl;
	rawfile_last<<"######################################################################################################################################"<<endl;
	
	stripfile>>ws;
	stripfile.getline(line,MAX_LINE_LENGTH);
	
	double Time1=0.0, Time2=0.0;
	double Pos_X1=0.0, Pos_Y1=0.0, Pos_Z1=0.0;
	double Pos_X2=0.0, Pos_Y2=0.0, Pos_Z2=0.0;
	double Roll1=0.0, Pitch1=0.0, Yaw1=0.0;
	double Roll2=0.0, Pitch2=0.0, Yaw2=0.0;
	
	do
	{
		///////////////////////////////////////
		//Read Strip File
		///////////////////////////////////////
		//GPS time
		FindNumber(stripfile);
		stripfile>>GPSTime;
		
		//pulse count
		FindNumber(stripfile);
		stripfile>>pulsecount;
		
		//1st range
		FindNumber(stripfile);
		stripfile>>Range1;
		
		//2nd range
		FindNumber(stripfile);
		stripfile>>Range2;
		
		//3rd range
		FindNumber(stripfile);
		stripfile>>Range3;
		
		//4th range
		FindNumber(stripfile);
		stripfile>>Range4;
		
		//1st intensity
		FindNumber(stripfile);
		stripfile>>intensity1;
		
		//2nd intensity
		FindNumber(stripfile);
		stripfile>>intensity2;
		
		//3rd intensity
		FindNumber(stripfile);
		stripfile>>intensity3;
		
		//4th intensity
		FindNumber(stripfile);
		stripfile>>intensity4;
		
		//scan angle (rad)
		FindNumber(stripfile);
		stripfile>>scanrad;
		
		//pitch angle (rad)
		FindNumber(stripfile);
		stripfile>>pitchrad;
		
		//roll angle (rad)
		FindNumber(stripfile);
		stripfile>>rollrad;
		
		//heading angle (rad)
		FindNumber(stripfile);
		stripfile>>headingrad;
		
		//latitude (rad)
		FindNumber(stripfile);
		stripfile>>latitude;
		
		//longitude (rad)
		FindNumber(stripfile);
		stripfile>>longitude;
		
		//height (meter)
		FindNumber(stripfile);
		stripfile>>height;
		
		//eat white space
		stripfile>>ws;
		
		///////////////////////////////////////
		//Write Raw File
		///////////////////////////////////////
		
		
		rawfile_first<<setw(10)<<Rad2Deg(longitude)<<"\t"<<setw(10)<<Rad2Deg(latitude)<<"\t"<<setw(10)<<height<<"\t";
		rawfile_first<<setw(10)<<Rad2Deg(pitchrad)<<"\t"<<setw(10)<<Rad2Deg(rollrad)<<"\t"<<setw(10)<<Rad2Deg(headingrad)<<"\t";
		rawfile_first<<setw(10)<<Rad2Deg(scanrad)<<"\t"<<setw(10)<<0.0<<"\t";
		rawfile_first<<setw(10)<<Range1<<"\t"<<setw(10)<<GPSTime<<"\t"<<setw(10)<<intensity1<<endl;
		rawfile_first.flush();
		
		
		
		rawfile_last<<setw(10)<<Rad2Deg(longitude)<<"\t"<<setw(10)<<Rad2Deg(latitude)<<"\t"<<setw(10)<<height<<"\t";
		rawfile_last<<setw(10)<<Rad2Deg(pitchrad)<<"\t"<<setw(10)<<Rad2Deg(rollrad)<<"\t"<<setw(10)<<Rad2Deg(headingrad)<<"\t";
		rawfile_last<<setw(10)<<Rad2Deg(scanrad)<<"\t"<<setw(10)<<0.0<<"\t";
		rawfile_last<<setw(10)<<Range4<<"\t"<<setw(10)<<GPSTime<<"\t"<<setw(10)<<intensity4<<endl;
		rawfile_last.flush();
		
	}while(!stripfile.eof());
	
	stripfile.close();
	
	rawfile_first.close();
	
	rawfile_last.close();
}

void CLIDARBiasCalibration::ReadBrazilStripTxt_old(CString striptxtpath, CString trajectorypath, CString configpath, CString rawpath, double Tthreshold)
{
	char line[MAX_LINE_LENGTH];
	double GPSTime;
	int pulsecount;
	double Range1;
	double Range2;
	double Range3;
	double Range4;
	int intensity1;
	int intensity2;
	int intensity3;
	int intensity4;
	double scanrad;
	double pitchrad;
	double rollrad;
	double headingrad;
	double latitude;
	double longitude;
	double height;

	CString stnumber;
	int count=1;
	stnumber.Format("%d",count);
	CString newrawpath_first = rawpath+stnumber+"_first.raw";
	CString newrawpath_last = rawpath+stnumber+"_last.raw";

	double LIDARtime0=-1.0, LIDARtime1=-1.0;

	fstream stripfile;
	fstream trajectory;

	fstream rawfile_first;
	fstream rawfileinfo_first;
	fstream xyzfile_first;
	fstream intensityfile_first;

	fstream rawfile_last;
	fstream rawfileinfo_last;
	fstream xyzfile_last;
	fstream intensityfile_last;

	CString xyzfilepath_first=rawpath;
	xyzfilepath_first = rawpath + stnumber + "_xyz_first.txt";

	CString xyzfilepath_last=rawpath;
	xyzfilepath_last = rawpath + stnumber + "_xyz_last.txt";

	CString infopath_first=rawpath;
	infopath_first = rawpath + stnumber + "_info_first.txt";

	CString infopath_last=rawpath;
	infopath_last = rawpath + stnumber + "_info_last.txt";

	CString intensitypath_first=rawpath;
	intensitypath_first = rawpath + stnumber + "_intensity_first.txt";

	CString intensitypath_last=rawpath;
	intensitypath_last = rawpath + stnumber + "_intensity_last.txt";
		
	stripfile.open(striptxtpath, ios::in);
	trajectory.open(trajectorypath, ios::in);

	rawfile_first.open(newrawpath_first, ios::out);
	rawfileinfo_first.open(infopath_first, ios::out);
	xyzfile_first.open(xyzfilepath_first, ios::out);
	intensityfile_first.open(intensitypath_first,ios::out);

	rawfile_last.open(newrawpath_last, ios::out);
	rawfileinfo_last.open(infopath_last, ios::out);
	xyzfile_last.open(xyzfilepath_last, ios::out);
	intensityfile_last.open(intensitypath_last,ios::out);
	
	rawfile_first<<"VER 1.1"<<endl;
	rawfile_first<<"#Pos_X(M)	Pos_Y(M)	Pos_Z(M)	Pitch(deg)	Roll(deg)	Heading(deg)	alpha(deg)	betta(deg)	range(M)	time(sec)	intensity"<<endl;
	rawfile_first<<"######################################################################################################################################"<<endl;

	rawfile_last<<"VER 1.1"<<endl;
	rawfile_last<<"#Pos_X(M)	Pos_Y(M)	Pos_Z(M)	Pitch(deg)	Roll(deg)	Heading(deg)	alpha(deg)	betta(deg)	range(M)	time(sec)	intensity"<<endl;
	rawfile_last<<"######################################################################################################################################"<<endl;
	
	stripfile>>ws;
	stripfile.getline(line,MAX_LINE_LENGTH);

	for(int i=0; i<23; i++)
	{
		if(!trajectory.eof())
		{
			trajectory.getline(line,MAX_LINE_LENGTH);
		}
	}

	double Time1=0.0, Time2=0.0;
	double Pos_X1=0.0, Pos_Y1=0.0, Pos_Z1=0.0;
	double Pos_X2=0.0, Pos_Y2=0.0, Pos_Z2=0.0;
	double Roll1=0.0, Pitch1=0.0, Yaw1=0.0;
	double Roll2=0.0, Pitch2=0.0, Yaw2=0.0;

	CLIDARConfig Cfg;
	Cfg.ReadConfigFile(configpath);

	double sTime, eTime;
	//double MinX_first=1.0e20, MaxX_first=-1.0e20, MinY_first=1.0e20, MaxY_first=-1.0e20, MinZ_first=1.0e20, MaxZ_first=-1.0e20;
	//double MinX_last=1.0e20, MaxX_last=-1.0e20, MinY_last=1.0e20, MaxY_last=-1.0e20, MinZ_last=1.0e20, MaxZ_last=-1.0e20;
	double MinX_first=1.0e20, MaxX_first=-1.0e20;
	double MinX_Y_first;
	double MinX_Z_first;
	double MaxX_Y_first;
	double MaxX_Z_first;

	double MinX_last=1.0e20, MaxX_last=-1.0e20;
	double MinX_Y_last;
	double MinX_Z_last;
	double MaxX_Y_last;
	double MaxX_Z_last;

	double MinY_first=1.0e20, MaxY_first=-1.0e20;
	double MinY_X_first;
	double MinY_Z_first;
	double MaxY_X_first;
	double MaxY_Z_first;

	double MinY_last=1.0e20, MaxY_last=-1.0e20;
	double MinY_X_last;
	double MinY_Z_last;
	double MaxY_X_last;
	double MaxY_Z_last;

	double MinZ_first=1.0e20, MaxZ_first=-1.0e20;
	double MinZ_X_first;
	double MinZ_Y_first;
	double MaxZ_X_first;
	double MaxZ_Y_first;

	double MinZ_last=1.0e20, MaxZ_last=-1.0e20;
	double MinZ_X_last;
	double MinZ_Y_last;
	double MaxZ_X_last;
	double MaxZ_Y_last;
	
	double MinSwing=1.0e20, MaxSwing=-1.0e20;

	bool newstrip = true;

	long p_num = 0;
	
	do
	{
		{
			///////////////////////////////////////
			//Read Strip File
			///////////////////////////////////////
			//GPS time
			FindNumber(stripfile);
			stripfile>>GPSTime;

			if(LIDARtime0 > 0)
			{
				if(fabs(LIDARtime0 - GPSTime) > Tthreshold)
				{
					eTime = LIDARtime0;

					rawfileinfo_first.precision(10);
					rawfileinfo_first.setf(ios::fixed, ios::floatfield);

					rawfileinfo_last.precision(10);
					rawfileinfo_last.setf(ios::fixed, ios::floatfield);
					
					rawfileinfo_first<<"eTime: "<<setw(10)<<eTime<<endl;
					rawfileinfo_last<<"eTime: "<<setw(10)<<eTime<<endl;
					rawfileinfo_first.flush();
					rawfileinfo_last.flush();

					rawfileinfo_first<<"Min(X): "<<MinX_first<<"\t"<<"Min(X)Y: "<<MinX_Y_first<<"\t"<<"Min(X)Z: "<<MinX_Z_first<<endl;
					rawfileinfo_first<<"Min(Y)X: "<<MinY_X_first<<"\t"<<"Min(Y): "<<MinY_first<<"\t"<<"Min(Y)Z: "<<MinY_Z_first<<endl;
					rawfileinfo_first<<"Min(Z)X: "<<MinZ_X_first<<"\t"<<"Min(Z)Y: "<<MinZ_Y_first<<"\t"<<"Min(Z): "<<MinZ_first<<endl;

					rawfileinfo_first<<"Max(X): "<<MaxX_first<<"\t"<<"Max(X)Y: "<<MaxX_Y_first<<"\t"<<"Max(X)Z: "<<MaxX_Z_first<<endl;
					rawfileinfo_first<<"Max(Y)X: "<<MaxY_X_first<<"\t"<<"Max(Y): "<<MaxY_first<<"\t"<<"Max(Y)Z: "<<MaxY_Z_first<<endl;
					rawfileinfo_first<<"Max(Z)X: "<<MaxZ_X_first<<"\t"<<"Max(Z)Y: "<<MaxZ_Y_first<<"\t"<<"Max(Z): "<<MaxZ_first<<endl;

					rawfileinfo_last<<"Min(X): "<<MinX_last<<"\t"<<"Min(X)Y: "<<MinX_Y_last<<"\t"<<"Min(X)Z: "<<MinX_Z_last<<endl;
					rawfileinfo_last<<"Min(Y)X: "<<MinY_X_last<<"\t"<<"Min(Y): "<<MinY_last<<"\t"<<"Min(Y)Z: "<<MinY_Z_last<<endl;
					rawfileinfo_last<<"Min(Z)X: "<<MinZ_X_last<<"\t"<<"Min(Z)Y: "<<MinZ_Y_last<<"\t"<<"Min(Z): "<<MinZ_last<<endl;

					rawfileinfo_last<<"Max(X): "<<MaxX_last<<"\t"<<"Max(X)Y: "<<MaxX_Y_last<<"\t"<<"Max(X)Z: "<<MaxX_Z_last<<endl;
					rawfileinfo_last<<"Max(Y)X: "<<MaxY_X_last<<"\t"<<"Max(Y): "<<MaxY_last<<"\t"<<"Max(Y)Z: "<<MaxY_Z_last<<endl;
					rawfileinfo_last<<"Max(Z)X: "<<MaxZ_X_last<<"\t"<<"Max(Z)Y: "<<MaxZ_Y_last<<"\t"<<"Max(Z): "<<MaxZ_last<<endl;
					
					rawfileinfo_first<<"#Point: "<<p_num<<endl;
					rawfileinfo_last<<"#Point: "<<p_num<<endl;
					
					//double area1 = CalTriangleArea(MinX_first,MinX_Y_first,MinX_Z_first,MaxY_X_first,MaxY_first,MaxY_Z_first,MinY_X_first,MinY_first,MinY_Z_first);
					//double area2 = CalTriangleArea(MaxX_first,MaxX_Y_first,MaxX_Z_first,MaxY_X_first,MaxY_first,MaxY_Z_first,MinY_X_first,MinY_first,MinY_Z_first);
					
					//rawfileinfo_first<<"Area1: "<<area1<<endl;
					//rawfileinfo_first<<"Area2: "<<area2<<endl;
					
					double area = (MaxX_first - MinX_first)*(MaxY_first - MinY_first);
					rawfileinfo_first<<"Area: "<<area<<endl;
					
					//rawfileinfo_first<<"Density: "<<p_num/(area1+area2)<<endl;
					rawfileinfo_first<<"Density: "<<p_num/area<<endl;
					
					//area1 = CalTriangleArea(MinX_last,MinX_Y_last,MinX_Z_last,MaxY_X_last,MaxY_last,MaxY_Z_last,MinY_X_last,MinY_last,MinY_Z_last);
					//area2 = CalTriangleArea(MaxX_last,MaxX_Y_last,MaxX_Z_last,MaxY_X_last,MaxY_last,MaxY_Z_last,MinY_X_last,MinY_last,MinY_Z_last);
					
					//rawfileinfo_last<<"Area1: "<<area1<<endl;
					//rawfileinfo_last<<"Area2: "<<area2<<endl;
					
					area = (MaxX_last - MinX_last)*(MaxY_last - MinY_last);
					rawfileinfo_first<<"Area: "<<area<<endl;
					
					//rawfileinfo_last<<"Density: "<<p_num/(area1+area2)<<endl;
					rawfileinfo_last<<"Density: "<<p_num/area<<endl;
					
					rawfileinfo_first.flush();
					rawfileinfo_last.flush();
					
					newstrip = true;

					p_num = 0;

					
					count++;
					stnumber.Format("%d",count);
					
					rawfile_first.close();
					newrawpath_first = rawpath+stnumber+"_first.raw";
					rawfile_first.open(newrawpath_first, ios::out);

					rawfile_last.close();
					newrawpath_last = rawpath+stnumber+"_last.raw";
					rawfile_last.open(newrawpath_last, ios::out);
					
					rawfile_first<<"VER 1.1"<<endl;
					rawfile_first<<"#Pos_X(M)	Pos_Y(M)	Pos_Z(M)	Pitch(deg)	Roll(deg)	Heading(deg)	alpha(deg)	betta(deg)	range(M)	time(sec)	intensity"<<endl;
					rawfile_first<<"######################################################################################################################################"<<endl;

					rawfile_last<<"VER 1.1"<<endl;
					rawfile_last<<"#Pos_X(M)	Pos_Y(M)	Pos_Z(M)	Pitch(deg)	Roll(deg)	Heading(deg)	alpha(deg)	betta(deg)	range(M)	time(sec)	intensity"<<endl;
					rawfile_last<<"######################################################################################################################################"<<endl;

					rawfileinfo_first.close();
					infopath_first = rawpath + stnumber + "_info_first.txt";
					rawfileinfo_first.open(infopath_first, ios::out);

					rawfileinfo_last.close();
					infopath_last = rawpath + stnumber + "_info_last.txt";
					rawfileinfo_last.open(infopath_last, ios::out);

					xyzfile_first.close();
					xyzfilepath_first = rawpath + stnumber + "_xyz_first.txt";
					xyzfile_first.open(xyzfilepath_first, ios::out);

					xyzfile_last.close();
					xyzfilepath_last = rawpath + stnumber + "_xyz_last.txt";
					xyzfile_last.open(xyzfilepath_last, ios::out);

					intensityfile_first.close();
					intensitypath_first = rawpath + stnumber + "_intensity_first.txt";
					intensityfile_first.open(intensitypath_first, ios::out);

					intensityfile_last.close();
					intensitypath_last = rawpath + stnumber + "_intensity_last.txt";
					intensityfile_last.open(intensitypath_last, ios::out);

				}

				LIDARtime0 = GPSTime;
			}
			else
			{
				LIDARtime0 = GPSTime;
			}
			
			//pulse count
			FindNumber(stripfile);
			stripfile>>pulsecount;
			
			//1st range
			FindNumber(stripfile);
			stripfile>>Range1;
			
			//2nd range
			FindNumber(stripfile);
			stripfile>>Range2;
			
			//3rd range
			FindNumber(stripfile);
			stripfile>>Range3;
			
			//4th range
			FindNumber(stripfile);
			stripfile>>Range4;
			
			//1st intensity
			FindNumber(stripfile);
			stripfile>>intensity1;
			
			//2nd intensity
			FindNumber(stripfile);
			stripfile>>intensity2;
			
			//3rd intensity
			FindNumber(stripfile);
			stripfile>>intensity3;
			
			//4th intensity
			FindNumber(stripfile);
			stripfile>>intensity4;
			
			//scan angle (rad)
			FindNumber(stripfile);
			stripfile>>scanrad;
			
			//pitch angle (rad)
			FindNumber(stripfile);
			stripfile>>pitchrad;
			
			//roll angle (rad)
			FindNumber(stripfile);
			stripfile>>rollrad;
			
			//heading angle (rad)
			FindNumber(stripfile);
			stripfile>>headingrad;
			
			//latitude (rad)
			FindNumber(stripfile);
			stripfile>>latitude;
			
			//longitude (rad)
			FindNumber(stripfile);
			stripfile>>longitude;
			
			//height (meter)
			FindNumber(stripfile);
			stripfile>>height;
			
			//eat white space
			stripfile>>ws;
		}
		
		{
			double dumy;
			///////////////////////////////////////
			//Read Trajectory File
			///////////////////////////////////////
			if(GPSTime > Time2)
			{
				
				while(Time2<GPSTime)
				{
					//Time (sec)				
					Time1 = Time2;
					trajectory>>Time2;
					//dist (M)				
					trajectory>>dumy;
					//Pos(X,Y,Z)
					Pos_X1 = Pos_X2;
					Pos_Y1 = Pos_Y2;
					Pos_Z1 = Pos_Z2;
					trajectory>>Pos_X2;
					trajectory>>Pos_Y2;
					trajectory>>Pos_Z2;
					//Pos(Lat,Lon,H)
					trajectory>>dumy;
					trajectory>>dumy;
					trajectory>>dumy;
					//Attitute
					Pitch1 = Pitch2;
					Roll1 = Roll2;
					Yaw1 = Yaw2;
					trajectory>>Pitch2;
					trajectory>>Roll2;
					trajectory>>Yaw2;
					
					//getline
					trajectory.getline(line,MAX_LINE_LENGTH);
					
				}
			}
			
			if(Time1 != 0.0)
			{
				double T_diff1 = (GPSTime - Time1);
				double T_diff2 = (Time2 - Time1);
				double ratio = T_diff1/T_diff2;
				
				double Pos_X, Pos_Y, Pos_Z;
				Pos_X = Pos_X1 + (Pos_X2-Pos_X1)*ratio;
				Pos_Y = Pos_Y1 + (Pos_Y2-Pos_Y1)*ratio;
				Pos_Z = Pos_Z1 + (Pos_Z2-Pos_Z1)*ratio;
				double Pitch, Roll, Yaw;
				Pitch = Pitch1 + (Pitch2-Pitch1)*ratio;
				Roll = Roll1 + (Roll2-Roll1)*ratio;
				Yaw = Yaw1 + (Yaw2-Yaw1)*ratio;
				
				{
					p_num ++;

					///////////////////////////////////////
					//Write Raw File
					///////////////////////////////////////
					//double Range = (Range1+Range4)/2.0;
					rawfile_first.precision(10);
					rawfile_first.setf(ios::fixed, ios::floatfield);
					rawfile_first<<setw(10)<<Pos_X<<"\t"<<setw(10)<<Pos_Y<<"\t"<<setw(10)<<Pos_Z<<"\t";
					//rawfile_first<<setw(10)<<Pitch<<"\t"<<setw(10)<<Roll<<"\t"<<setw(10)<<Yaw<<"\t";
					rawfile_first<<setw(10)<<Rad2Deg(pitchrad)<<"\t"<<setw(10)<<Rad2Deg(rollrad)<<"\t"<<setw(10)<<Rad2Deg(headingrad)<<"\t";
					rawfile_first<<setw(10)<<"0.0"<<"\t"<<setw(10)<<Rad2Deg(scanrad)<<"\t";
					rawfile_first<<setw(10)<<Range1<<"\t"<<setw(10)<<GPSTime<<"\t"<<setw(10)<<intensity1<<endl;
					rawfile_first.flush();

					rawfile_last.precision(10);
					rawfile_last.setf(ios::fixed, ios::floatfield);
					
					rawfile_last<<setw(10)<<Pos_X<<"\t"<<setw(10)<<Pos_Y<<"\t"<<setw(10)<<Pos_Z<<"\t";
					//rawfile_last<<setw(10)<<Pitch<<"\t"<<setw(10)<<Roll<<"\t"<<setw(10)<<Yaw<<"\t";
					rawfile_last<<setw(10)<<Rad2Deg(pitchrad)<<"\t"<<setw(10)<<Rad2Deg(rollrad)<<"\t"<<setw(10)<<Rad2Deg(headingrad)<<"\t";
					
					rawfile_last<<setw(10)<<"0.0"<<"\t"<<setw(10)<<Rad2Deg(scanrad)<<"\t";
					rawfile_last<<setw(10)<<Range4<<"\t"<<setw(10)<<GPSTime<<"\t"<<setw(10)<<intensity4<<endl;
					rawfile_last.flush();
					
					CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Pb(3,1,0.), Ranging1(3,1,0.), Ranging4(3,1,0.);
					X0(0,0) = Pos_X; X0(1,0) = Pos_Y; X0(2,0) = Pos_Z;
					P(0,0) = Cfg.Xoffset; P(1,0) = Cfg.Yoffset; P(2,0) = Cfg.Zoffset;
					Pb(0,0) = Cfg.Xb; Pb(1,0) = Cfg.Yb; Pb(2,0) = Cfg.Zb;
					
					Ranging1(0,0) = 0; Ranging1(1,0) = 0; Ranging1(2,0) = (Range1 + Cfg.Rangeb);
					Ranging4(0,0) = 0; Ranging4(1,0) = 0; Ranging4(2,0) = (Range4 + Cfg.Rangeb);
					
					//CRotationcoeff2 INS(Deg2Rad(Pitch), Deg2Rad(Roll), Deg2Rad(Yaw));
					CRotationcoeff2 INS(pitchrad, rollrad, headingrad);
					CRotationcoeff2 INS2G(Deg2Rad(180),0,0);
					
					INS.Rmatrix = INS2G.Rmatrix%INS.Rmatrix;
					
					CRotationcoeff2 Swing(0.0, scanrad, 0.);
					CRotationcoeff2 ROffset((Cfg.Ooffset+Cfg.Ob), (Cfg.Koffset+Cfg.Kb), (Cfg.Koffset+Cfg.Kb));
					CSMMatrix<double> XG1 = X0 + INS.Rmatrix%(P + Pb) 
						+ INS.Rmatrix%ROffset.Rmatrix%Swing.Rmatrix%Ranging1;
					CSMMatrix<double> XG4 = X0 + INS.Rmatrix%(P + Pb) 
						+ INS.Rmatrix%ROffset.Rmatrix%Swing.Rmatrix%Ranging4;
					
					xyzfile_first.precision(10);
					xyzfile_first.setf(ios::fixed, ios::floatfield);
					xyzfile_first<<setw(10)<<XG1(0,0)<<"\t"<<setw(10)<<XG1(1,0)<<"\t"<<setw(10)<<XG1(2,0)<<endl;
					xyzfile_first.flush();

					intensityfile_first.precision(10);
					intensityfile_first.setf(ios::fixed, ios::floatfield);
					intensityfile_first<<setw(10)<<XG1(0,0)<<"\t"<<setw(10)<<XG1(1,0)<<"\t"<<setw(10)<<intensity1<<endl;
					intensityfile_first.flush();

					if(MinX_first>XG1(0,0)) {MinX_first   = XG1(0,0);	MinX_Y_first = XG1(1,0);	MinX_Z_first = XG1(2,0);}
					if(MinY_first>XG1(1,0)) {MinY_X_first = XG1(0,0);	MinY_first	 = XG1(1,0);	MinY_Z_first = XG1(2,0);}
					if(MinZ_first>XG1(2,0)) {MinZ_X_first = XG1(0,0);	MinZ_Y_first = XG1(1,0);	MinZ_first   = XG1(2,0);}

					if(MaxX_first<XG1(0,0)) {MaxX_first   = XG1(0,0);	MaxX_Y_first = XG1(1,0);	MaxX_Z_first = XG1(2,0);}
					if(MaxY_first<XG1(1,0)) {MaxY_X_first = XG1(0,0);	MaxY_first	 = XG1(1,0);	MaxY_Z_first = XG1(2,0);}
					if(MaxZ_first<XG1(2,0)) {MaxZ_X_first = XG1(0,0);	MaxZ_Y_first = XG1(1,0);	MaxZ_first   = XG1(2,0);}


					xyzfile_last.precision(10);
					xyzfile_last.setf(ios::fixed, ios::floatfield);
					xyzfile_last<<setw(10)<<XG4(0,0)<<"\t"<<setw(10)<<XG4(1,0)<<"\t"<<setw(10)<<XG4(2,0)<<endl;
					xyzfile_last.flush();

					intensityfile_last.precision(10);
					intensityfile_last.setf(ios::fixed, ios::floatfield);
					intensityfile_last<<setw(10)<<XG1(0,0)<<"\t"<<setw(10)<<XG1(1,0)<<"\t"<<setw(10)<<intensity4<<endl;
					intensityfile_last.flush();

					if(MinX_last>XG1(0,0)) {MinX_last   = XG1(0,0);	MinX_Y_last = XG1(1,0);	MinX_Z_last = XG1(2,0);}
					if(MinY_last>XG1(1,0)) {MinY_X_last = XG1(0,0);	MinY_last	= XG1(1,0);	MinY_Z_last = XG1(2,0);}
					if(MinZ_last>XG1(2,0)) {MinZ_X_last = XG1(0,0);	MinZ_Y_last = XG1(1,0);	MinZ_last   = XG1(2,0);}

					if(MaxX_last<XG1(0,0)) {MaxX_last   = XG1(0,0);	MaxX_Y_last = XG1(1,0);	MaxX_Z_last = XG1(2,0);}
					if(MaxY_last<XG1(1,0)) {MaxY_X_last = XG1(0,0);	MaxY_last	= XG1(1,0);	MaxY_Z_last = XG1(2,0);}
					if(MaxZ_last<XG1(2,0)) {MaxZ_X_last = XG1(0,0);	MaxZ_Y_last = XG1(1,0);	MaxZ_last   = XG1(2,0);}

					
					if(newstrip == true)
					{						
						sTime = GPSTime;
						
						rawfileinfo_first.precision(10);
						rawfileinfo_first.setf(ios::fixed, ios::floatfield);
						
						rawfileinfo_last.precision(10);
						rawfileinfo_last.setf(ios::fixed, ios::floatfield);
						
						rawfileinfo_first<<"sTime: "<<setw(10)<<sTime<<endl;
						rawfileinfo_last<<"sTime: "<<setw(10)<<sTime<<endl;
						rawfileinfo_first.flush();
						rawfileinfo_last.flush();
						newstrip = false;
					}
				}
			}
		}
		
		stripfile>>ws;
		trajectory>>ws;
		
	}while((!stripfile.eof())&&(!trajectory.eof()));
	
	eTime = GPSTime;
	
	rawfileinfo_first<<"eTime: "<<setw(10)<<eTime<<endl;
	rawfileinfo_last<<"eTime: "<<setw(10)<<eTime<<endl;
	rawfileinfo_first.flush();
	rawfileinfo_last.flush();
	
	rawfileinfo_first<<"Min(X): "<<MinX_first<<"\t"<<"Min(X)Y: "<<MinX_Y_first<<"\t"<<"Min(X)Z: "<<MinX_Z_first<<endl;
	rawfileinfo_first<<"Min(Y)X: "<<MinY_X_first<<"\t"<<"Min(Y): "<<MinY_first<<"\t"<<"Min(Y)Z: "<<MinY_Z_first<<endl;
	rawfileinfo_first<<"Min(Z)X: "<<MinZ_X_first<<"\t"<<"Min(Z)Y: "<<MinZ_Y_first<<"\t"<<"Min(Z): "<<MinZ_first<<endl;
	
	rawfileinfo_first<<"Max(X): "<<MaxX_first<<"\t"<<"Max(X)Y: "<<MaxX_Y_first<<"\t"<<"Max(X)Z: "<<MaxX_Z_first<<endl;
	rawfileinfo_first<<"Max(Y)X: "<<MaxY_X_first<<"\t"<<"Max(Y): "<<MaxY_first<<"\t"<<"Max(Y)Z: "<<MaxY_Z_first<<endl;
	rawfileinfo_first<<"Max(Z)X: "<<MaxZ_X_first<<"\t"<<"Max(Z)Y: "<<MaxZ_Y_first<<"\t"<<"Max(Z): "<<MaxZ_first<<endl;
	
	rawfileinfo_last<<"Min(X): "<<MinX_last<<"\t"<<"Min(X)Y: "<<MinX_Y_last<<"\t"<<"Min(X)Z: "<<MinX_Z_last<<endl;
	rawfileinfo_last<<"Min(Y)X: "<<MinY_X_last<<"\t"<<"Min(Y): "<<MinY_last<<"\t"<<"Min(Y)Z: "<<MinY_Z_last<<endl;
	rawfileinfo_last<<"Min(Z)X: "<<MinZ_X_last<<"\t"<<"Min(Z)Y: "<<MinZ_Y_last<<"\t"<<"Min(Z): "<<MinZ_last<<endl;
	
	rawfileinfo_last<<"Max(X): "<<MaxX_last<<"\t"<<"Max(X)Y: "<<MaxX_Y_last<<"\t"<<"Max(X)Z: "<<MaxX_Z_last<<endl;
	rawfileinfo_last<<"Max(Y)X: "<<MaxY_X_last<<"\t"<<"Max(Y): "<<MaxY_last<<"\t"<<"Max(Y)Z: "<<MaxY_Z_last<<endl;
	rawfileinfo_last<<"Max(Z)X: "<<MaxZ_X_last<<"\t"<<"Max(Z)Y: "<<MaxZ_Y_last<<"\t"<<"Max(Z): "<<MaxZ_last<<endl;
	
	rawfileinfo_first<<"#Point: "<<p_num<<endl;
	rawfileinfo_last<<"#Point: "<<p_num<<endl;

	//double area1 = CalTriangleArea(MinX_first,MinX_Y_first,MinX_Z_first,MaxY_X_first,MaxY_first,MaxY_Z_first,MinY_X_first,MinY_first,MinY_Z_first);
	//double area2 = CalTriangleArea(MaxX_first,MaxX_Y_first,MaxX_Z_first,MaxY_X_first,MaxY_first,MaxY_Z_first,MinY_X_first,MinY_first,MinY_Z_first);

	//rawfileinfo_first<<"Area1: "<<area1<<endl;
	//rawfileinfo_first<<"Area2: "<<area2<<endl;

	double area = (MaxX_first - MinX_first)*(MaxY_first - MinY_first);
	rawfileinfo_first<<"Area: "<<area<<endl;
	
	//rawfileinfo_first<<"Density: "<<p_num/(area1+area2)<<endl;
	rawfileinfo_first<<"Density: "<<p_num/area<<endl;

	//area1 = CalTriangleArea(MinX_last,MinX_Y_last,MinX_Z_last,MaxY_X_last,MaxY_last,MaxY_Z_last,MinY_X_last,MinY_last,MinY_Z_last);
	//area2 = CalTriangleArea(MaxX_last,MaxX_Y_last,MaxX_Z_last,MaxY_X_last,MaxY_last,MaxY_Z_last,MinY_X_last,MinY_last,MinY_Z_last);

	//rawfileinfo_last<<"Area1: "<<area1<<endl;
	//rawfileinfo_last<<"Area2: "<<area2<<endl;

	area = (MaxX_last - MinX_last)*(MaxY_last - MinY_last);
	rawfileinfo_first<<"Area: "<<area<<endl;
	
	//rawfileinfo_last<<"Density: "<<p_num/(area1+area2)<<endl;
	rawfileinfo_last<<"Density: "<<p_num/area<<endl;

	rawfileinfo_first.flush();
	rawfileinfo_last.flush();


	stripfile.close();
	trajectory.close();

	rawfile_first.close();
	rawfileinfo_first.close();
	xyzfile_first.close();
	intensityfile_first.close();

	rawfile_last.close();
	rawfileinfo_last.close();
	xyzfile_last.close();
	intensityfile_last.close();
}

void CLIDARBiasCalibration::BrazilStrip2Raw(CString striptxtpath, CString trajectorypath, CString rawpath)
{
	char line[MAX_LINE_LENGTH];
	double GPSTime;
	int pulsecount;
	double Range1;
	double Range2;
	double Range3;
	double Range4;
	int intensity1;
	int intensity2;
	int intensity3;
	int intensity4;
	double scanrad;
	double pitchrad;
	double rollrad;
	double headingrad;
	double latitude;
	double longitude;
	double height;

	CString stnumber;
	int count=1;
	stnumber.Format("%d",count);
	CString newrawpath_first = rawpath+stnumber+"_first.raw";
	CString newrawpath_last = rawpath+stnumber+"_last.raw";

	double LIDARtime0 = -1.0, LIDARtime1 = -1.0;

	fstream stripfile;
	fstream trajectory;

	fstream rawfile_first;
	fstream rawfile_last;
	
	stripfile.open(striptxtpath, ios::in);
	trajectory.open(trajectorypath, ios::in);

	rawfile_first.open(newrawpath_first, ios::out);
	
	rawfile_last.open(newrawpath_last, ios::out);

	rawfile_first<<"VER 1.1"<<endl;
	rawfile_last<<"VER 1.1"<<endl;
	
	rawfile_first<<"#Pos_X(M)	Pos_Y(M)	Pos_Z(M)	Pitch(deg)	Roll(deg)	Heading(deg)	alpha(deg)	betta(deg)	range(M)	time(sec)	intensity"<<endl;
	rawfile_first<<"######################################################################################################################################"<<endl;

	rawfile_last<<"#Pos_X(M)	Pos_Y(M)	Pos_Z(M)	Pitch(deg)	Roll(deg)	Heading(deg)	alpha(deg)	betta(deg)	range(M)	time(sec)	intensity"<<endl;
	rawfile_last<<"######################################################################################################################################"<<endl;
	
	stripfile>>ws;
	stripfile.getline(line,MAX_LINE_LENGTH);

	for(int i=0; i<23; i++)
	{
		if(!trajectory.eof())
		{
			trajectory.getline(line,MAX_LINE_LENGTH);
		}
	}

	double Time1=0.0, Time2=0.0;
	double Pos_X1=0.0, Pos_Y1=0.0, Pos_Z1=0.0;
	double Pos_X2=0.0, Pos_Y2=0.0, Pos_Z2=0.0;
	double Roll1=0.0, Pitch1=0.0, Yaw1=0.0;
	double Roll2=0.0, Pitch2=0.0, Yaw2=0.0;

	long p_num = 0;
	
	do
	{
		///////////////////////////////////////
		//Read Strip File
		///////////////////////////////////////
		//GPS time
		FindNumber(stripfile);
		stripfile>>GPSTime;
		
		LIDARtime0 = GPSTime;
		
		//pulse count
		FindNumber(stripfile);
		stripfile>>pulsecount;
		
		//1st range
		FindNumber(stripfile);
		stripfile>>Range1;
		
		//2nd range
		FindNumber(stripfile);
		stripfile>>Range2;
		
		//3rd range
		FindNumber(stripfile);
		stripfile>>Range3;
		
		//4th range
		FindNumber(stripfile);
		stripfile>>Range4;
		
		//1st intensity
		FindNumber(stripfile);
		stripfile>>intensity1;
		
		//2nd intensity
		FindNumber(stripfile);
		stripfile>>intensity2;
		
		//3rd intensity
		FindNumber(stripfile);
		stripfile>>intensity3;
		
		//4th intensity
		FindNumber(stripfile);
		stripfile>>intensity4;
		
		//scan angle (rad)
		FindNumber(stripfile);
		stripfile>>scanrad;
		
		//pitch angle (rad)
		FindNumber(stripfile);
		stripfile>>pitchrad;
		
		//roll angle (rad)
		FindNumber(stripfile);
		stripfile>>rollrad;
		
		//heading angle (rad)
		FindNumber(stripfile);
		stripfile>>headingrad;
		
		//latitude (rad)
		FindNumber(stripfile);
		stripfile>>latitude;
		
		//longitude (rad)
		FindNumber(stripfile);
		stripfile>>longitude;
		
		//height (meter)
		FindNumber(stripfile);
		stripfile>>height;
		
		//eat white space
		stripfile>>ws;
		
		double dumy;
		///////////////////////////////////////
		//Read Trajectory File
		///////////////////////////////////////
		if(GPSTime > Time2)
		{
			
			while(Time2<GPSTime)
			{
				//Time (sec)				
				Time1 = Time2;
				trajectory>>Time2;
				//dist (M)				
				trajectory>>dumy;
				//Pos(X,Y,Z)
				Pos_X1 = Pos_X2;
				Pos_Y1 = Pos_Y2;
				Pos_Z1 = Pos_Z2;
				trajectory>>Pos_X2;
				trajectory>>Pos_Y2;
				trajectory>>Pos_Z2;
				//Pos(Lat,Lon,H)
				trajectory>>dumy;
				trajectory>>dumy;
				trajectory>>dumy;
				//Attitute
				Pitch1 = Pitch2;
				Roll1 = Roll2;
				Yaw1 = Yaw2;
				trajectory>>Pitch2;
				trajectory>>Roll2;
				trajectory>>Yaw2;
				
				//getline
				trajectory.getline(line,MAX_LINE_LENGTH);
				trajectory>>ws;
			}
		}
		
		if(Time1 != 0.0)
		{
			double T_diff1 = (GPSTime - Time1);
			double T_diff2 = (Time2 - Time1);
			double ratio = T_diff1/T_diff2;
			
			double Pos_X, Pos_Y, Pos_Z;
			Pos_X = Pos_X1 + (Pos_X2-Pos_X1)*ratio;
			Pos_Y = Pos_Y1 + (Pos_Y2-Pos_Y1)*ratio;
			Pos_Z = Pos_Z1 + (Pos_Z2-Pos_Z1)*ratio;
			double Pitch, Roll, Yaw;
			Pitch = Pitch1 + (Pitch2-Pitch1)*ratio;
			Roll = Roll1 + (Roll2-Roll1)*ratio;
			Yaw = Yaw1 + (Yaw2-Yaw1)*ratio;
			
			p_num ++;
			
			///////////////////////////////////////
			//Write Raw File
			///////////////////////////////////////
			//double Range = (Range1+Range4)/2.0;
			rawfile_first.precision(10);
			rawfile_first.setf(ios::fixed, ios::floatfield);
			rawfile_first<<setw(10)<<Pos_X<<"\t"<<setw(10)<<Pos_Y<<"\t"<<setw(10)<<Pos_Z<<"\t";
			//rawfile_first<<setw(10)<<Pitch<<"\t"<<setw(10)<<Roll<<"\t"<<setw(10)<<Yaw<<"\t";
			rawfile_first<<setw(10)<<Rad2Deg(pitchrad)<<"\t"<<setw(10)<<Rad2Deg(rollrad)<<"\t"<<setw(10)<<Rad2Deg(headingrad)<<"\t";
			rawfile_first<<setw(10)<<"0.0"<<"\t"<<setw(10)<<Rad2Deg(scanrad)<<"\t";
			rawfile_first<<setw(10)<<Range1<<"\t"<<setw(10)<<GPSTime<<"\t"<<setw(10)<<intensity1<<endl;
			rawfile_first.flush();
			
			rawfile_last.precision(10);
			rawfile_last.setf(ios::fixed, ios::floatfield);
			
			rawfile_last<<setw(10)<<Pos_X<<"\t"<<setw(10)<<Pos_Y<<"\t"<<setw(10)<<Pos_Z<<"\t";
			//rawfile_last<<setw(10)<<Pitch<<"\t"<<setw(10)<<Roll<<"\t"<<setw(10)<<Yaw<<"\t";
			rawfile_last<<setw(10)<<Rad2Deg(pitchrad)<<"\t"<<setw(10)<<Rad2Deg(rollrad)<<"\t"<<setw(10)<<Rad2Deg(headingrad)<<"\t";
			
			rawfile_last<<setw(10)<<"0.0"<<"\t"<<setw(10)<<Rad2Deg(scanrad)<<"\t";
			rawfile_last<<setw(10)<<Range4<<"\t"<<setw(10)<<GPSTime<<"\t"<<setw(10)<<intensity4<<endl;
			rawfile_last.flush();
		}	

		stripfile.getline(line,MAX_LINE_LENGTH);
		stripfile>>ws;
		
	}while((!stripfile.eof())&&(!trajectory.eof()));
	
	stripfile.close();
	trajectory.close();

	rawfile_first.close();
	
	rawfile_last.close();
	
}

void CLIDARBiasCalibration::SeparateBrazilStripTxt(CString striptxtpath, CString newpath, double Tthreshold)
{
	char comment[MAX_LINE_LENGTH];
	double GPSTime;
	int pulsecount;
	double Range1;
	double Range2;
	double Range3;
	double Range4;
	int intensity1;
	int intensity2;
	int intensity3;
	int intensity4;
	double scanrad;
	double pitchrad;
	double rollrad;
	double headingrad;
	double latitude;
	double longitude;
	double height;

	CString stnumber;
	int count=1;
	stnumber.Format("%d",count);
	CString newstrippath = newpath+stnumber+".txt";
	
	double LIDARtime0=-1.0, LIDARtime1=-1.0;
	
	fstream stripfile;
	fstream newstripfile;
	
	stripfile.open(striptxtpath, ios::in);
	
	newstripfile.open(newstrippath, ios::out);
	
	stripfile.getline(comment,MAX_LINE_LENGTH);
	newstripfile<<comment<<endl;
	
	newstripfile.precision(10);
	newstripfile.setf(ios::fixed, ios::floatfield);
	
	while(!stripfile.eof())
	{
		//GPS time
		FindNumber(stripfile);
		stripfile>>GPSTime;
		
		if(LIDARtime0 > 0)
		{
			if(fabs(LIDARtime0 - GPSTime) > Tthreshold)
			{
				count++;
				stnumber.Format("%d",count);
				
				newstripfile.close();
				
				newstrippath = newpath+stnumber+".txt";
				newstripfile.open(newstrippath, ios::out);
				newstripfile<<comment<<endl;
			}
			
			LIDARtime0 = GPSTime;
		}
		else
		{
			LIDARtime0 = GPSTime;
		}

		newstripfile<<setw(10)<<GPSTime<<","<<"\t";
		
		//pulse count
		FindNumber(stripfile);
		stripfile>>pulsecount;

		newstripfile<<setw(10)<<pulsecount<<","<<"\t";
		
		//1st range
		FindNumber(stripfile);
		stripfile>>Range1;

		newstripfile<<setw(10)<<Range1<<","<<"\t";
		
		//2nd range
		FindNumber(stripfile);
		stripfile>>Range2;

		newstripfile<<setw(10)<<Range2<<","<<"\t";
		
		//3rd range
		FindNumber(stripfile);
		stripfile>>Range3;

		newstripfile<<setw(10)<<Range3<<","<<"\t";
		
		//4th range
		FindNumber(stripfile);
		stripfile>>Range4;

		newstripfile<<setw(10)<<Range4<<","<<"\t";
		
		//1st intensity
		FindNumber(stripfile);
		stripfile>>intensity1;

		newstripfile<<setw(10)<<intensity1<<","<<"\t";
		
		//2nd intensity
		FindNumber(stripfile);
		stripfile>>intensity2;

		newstripfile<<setw(10)<<intensity2<<","<<"\t";
		
		//3rd intensity
		FindNumber(stripfile);
		stripfile>>intensity3;

		newstripfile<<setw(10)<<intensity3<<","<<"\t";
		
		//4th intensity
		FindNumber(stripfile);
		stripfile>>intensity4;

		newstripfile<<setw(10)<<intensity4<<","<<"\t";
		
		//scan angle (rad)
		FindNumber(stripfile);
		stripfile>>scanrad;

		newstripfile<<setw(10)<<scanrad<<","<<"\t";
		
		//pitch angle (rad)
		FindNumber(stripfile);
		stripfile>>pitchrad;

		newstripfile<<setw(10)<<pitchrad<<","<<"\t";
		
		//roll angle (rad)
		FindNumber(stripfile);
		stripfile>>rollrad;

		newstripfile<<setw(10)<<rollrad<<","<<"\t";
		
		//heading angle (rad)
		FindNumber(stripfile);
		stripfile>>headingrad;

		newstripfile<<setw(10)<<headingrad<<","<<"\t";
		
		//latitude (rad)
		FindNumber(stripfile);
		stripfile>>latitude;

		newstripfile<<setw(10)<<latitude<<","<<"\t";
		
		//longitude (rad)
		FindNumber(stripfile);
		stripfile>>longitude;

		newstripfile<<setw(10)<<longitude<<","<<"\t";
		
		//height (meter)
		FindNumber(stripfile);
		stripfile>>height;

		newstripfile<<setw(10)<<height<<","<<endl;

		newstripfile.flush();
		
		//eat white space
		stripfile>>ws;

	}
		

	stripfile.close();
	newstripfile.close();
}

void CLIDARBiasCalibration::RemoveOutlier(CString stripxyzpath, CString newpath, double threshold)
{
	fstream xyzfile;
	fstream newfile;
	
	xyzfile.open(stripxyzpath, ios::in);
	newfile.open(newpath, ios::out);

	double X0, Y0, Z0;
	double X, Y, Z;
	
	xyzfile>>ws;

	if(!xyzfile.eof())
	{
		//X
		xyzfile>>X0;
		
		//Y
		xyzfile>>Y0;
		
		//Z
		xyzfile>>Z0;	
	}

	do
	{
		///////////////////////////////////////
		//Read Strip File
		///////////////////////////////////////
		//X
		xyzfile>>X;
		
		//Y
		xyzfile>>Y;
		
		//Z
		xyzfile>>Z;

		xyzfile>>ws;

		double dist_ver = fabs(Z-Z0);
		double dist_hor = sqrt(pow((X-X0),2) + pow((Y-Y0),2));

		if(dist_ver<threshold)
		{
			newfile.precision(10);
			newfile.setf(ios::fixed, ios::floatfield);
			newfile<<setw(10)<<X<<"\t"<<setw(10)<<Y<<"\t"<<setw(10)<<Z<<endl;
			newfile.flush();

			X0=X; Y0=Y; Z0=Z;
		}
		
	}while(!xyzfile.eof());

	xyzfile.close();
	newfile.close();
}


/**Get correlation matrix from variance-covariance matrix*/
CSMMatrix<double> CLIDARBiasCalibration::Correlation(CSMMatrix<double> VarCov)
{
	unsigned int i, j;
	//Correlation of Unknown Matrix
	CSMMatrix<double> Correlation;
	Correlation.Resize(VarCov.GetRows(),VarCov.GetCols(),0.);
	
	for(i=0; i<Correlation.GetRows(); i++)
	{
		double sigma_i, sigma_j;
		sigma_i = sqrt(VarCov(i,i));
		
		for(j=0; j<Correlation.GetCols();j++)
		{
			sigma_j = sqrt(VarCov(j,j));
			Correlation(i,j) = VarCov(i,j)/sigma_i/sigma_j;
		}
	}
	return Correlation;
}

double CLIDARBiasCalibration::CalTriangleArea(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3)
{
	double dist1, dist2, dist3;
	dist1 = (double)sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
	dist2 = (double)sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3)+(z2-z3)*(z2-z3));
	dist3 = (double)sqrt((x1-x3)*(x1-x3)+(y1-y3)*(y1-y3)+(z1-z3)*(z1-z3));
	
	double area;
	double s;
	double K;
	s=(dist1+dist2+dist3)/2;
	K=(s*(s-dist1)*(s-dist2)*(s-dist3));
	if(K < 1.0e-9) K = 0.0;
	area=sqrt(K);
	return (double)area;
}

void CLIDARBiasCalibration::BiasCalibrationResultTestwithPlaneFitting(CString AdjustedCfgpath, CString TrueCfgpath, CString PatchPath, CString VTXpath, CString outpath, bool bBrazil)
{
	////////////////////////////////
	//
	//To read configuration files
	//
	CLIDARConfig AdjustedCfg;
	CLIDARConfig TrueCfg;
	AdjustedCfg.ReadConfigFile(AdjustedCfgpath);
	TrueCfg.ReadConfigFile(TrueCfgpath);
	double min[3], max[3];
	ReadPlaneVertex(VTXpath, PatchList, min, max);
	ReadPlanarPoints(PatchPath);

	///////////////////////////////////////
	//
	//To read LIDAR point cloud (raw data)
	//

	fstream outfile;
	double sum_ND_new=0;
	double sum_ND_true=0;
	double sum_ND_new2=0;
	double sum_ND_true2=0;
	double sum_EX=0, sum_EY=0, sum_EZ=0;
	double sum_EX2=0, sum_EY2=0, sum_EZ2=0;
	int count = 0;
	///////////////////////////////////////////////////////////////////////////////
	outfile.open(outpath, ios::out);
	
	outfile<<"#dX(M) dY(M) dZ(M) ND_new(M) ND_true(M)"<<endl;
	outfile<<"###############################################################################################################"<<endl;
	
	int numpatch = PatchList.GetNumItem();
	for(int i=0; i<numpatch; i++)
	{
		CLIDARPlanarPatch patch = PatchList.GetAt(i);
		int numpoint = patch.PointList.GetNumItem();
		
		XA = patch.Vertex[0];	YA = patch.Vertex[1];	ZA = patch.Vertex[2];
		XB = patch.Vertex[3];	YB = patch.Vertex[4];	ZB = patch.Vertex[5];
		XC = patch.Vertex[6];	YC = patch.Vertex[7];	ZC = patch.Vertex[8];
		
		for(int j=0; j<numpoint; j++)
		{
			CLIDARRawPoint  point = patch.PointList.GetAt(j);
						
			CSMMatrix<double> Xnew = LIDAREQ(point, AdjustedCfg);
			double XP = Xnew(0,0);
			double YP = Xnew(1,0);
			double ZP = Xnew(2,0);
			
			double NDnew = XP*(YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC-YC*ZB))
				-YP*(XA*(ZB-ZC) - ZA*(XB-XC) + (XB*ZC-XC*ZB))
				+ZP*(XA*(YB-YC) - YA*(XB-XC) + (XB*YC-XC*YB))
				-1.*(XA*(YB*ZC-YC*ZB) - YA*(XB*ZC-XC*ZB) + ZA*(XB*YC-XC*YB));
			NDnew = NDnew/2/patch.Area;
			
			CSMMatrix<double> Xtrue = LIDAREQ(point, TrueCfg);
			
			XP = Xtrue(0,0);
			YP = Xtrue(1,0);
			ZP = Xtrue(2,0);
			
			double NDtrue = XP*(YA*(ZB-ZC) - ZA*(YB-YC) + (YB*ZC-YC*ZB))
				-YP*(XA*(ZB-ZC) - ZA*(XB-XC) + (XB*ZC-XC*ZB))
				+ZP*(XA*(YB-YC) - YA*(XB-XC) + (XB*YC-XC*YB))
				-1.*(XA*(YB*ZC-YC*ZB) - YA*(XB*ZC-XC*ZB) + ZA*(XB*YC-XC*YB));
			NDtrue = NDnew/2/patch.Area;
			
			CSMMatrix<double> dX = Xnew - Xtrue;
			
			sum_ND_new += NDnew;
			sum_ND_true += NDtrue;

			sum_ND_new2 += NDnew*NDnew;
			sum_ND_true2 += NDtrue*NDtrue;
			
			sum_EZ += dX(0,0);
			sum_EY += dX(1,0);
			sum_EZ += dX(2,0);
			
			sum_EZ2 += dX(0,0)*dX(0,0);
			sum_EY2 += dX(1,0)*dX(1,0);
			sum_EZ2 += dX(2,0)*dX(2,0);
			
			//To print out
			outfile.precision(10);
			outfile.setf(ios::scientific );
			outfile<<setw(10)<<dX(0,0)<<"\t"<<setw(10)<<dX(1,0)<<"\t"<<setw(10)<<dX(2,0)<<"\t";
			outfile<<setw(10)<<NDnew<<"\t"<<setw(10)<<NDtrue<<endl;
			outfile.flush();
			
			count ++;
		}
		
	}

	outfile.precision(10);
	outfile.setf(ios::scientific );

	outfile<<"[RMSE(dX, dY, dZ)]"<<endl;
	outfile<<setw(10)<<sqrt(sum_EX2/count)<<"\t";
	outfile<<setw(10)<<sqrt(sum_EY2/count)<<"\t";
	outfile<<setw(10)<<sqrt(sum_EZ2/count)<<endl<<endl;

	outfile<<"[RMSE(ND_new, ND_true)]"<<endl;
	outfile<<setw(10)<<sqrt(sum_ND_new2/count)<<"\t";
	outfile<<setw(10)<<sqrt(sum_ND_true2/count)<<endl;
	
	outfile.close();
	///////////////////////////////////////////////////////////////////////////////
}

CString CLIDARBiasCalibration::BiasCalibrationResultTest(CString AdjustedCfgpath, CString TrueCfgpath, CString Lidarpath, CString outpath)
{
	////////////////////////////////
	//
	//To read configuration files
	//
	CLIDARConfig AdjustedCfg;
	CLIDARConfig TrueCfg;

	AdjustedCfg.ReadConfigFile(AdjustedCfgpath);
	TrueCfg.ReadConfigFile(TrueCfgpath);

	///////////////////////////////////////
	//
	//To read LIDAR point cloud (raw data)
	//

	double GPS_X, GPS_Y, GPS_Z; //meter
	double INS_O, INS_P, INS_K; //deg
	double alpha, beta;//deg
	double dist, time;//meter, sec
	fstream LidarFile;
	fstream outfile;
	fstream outfile_adjustedxyz;
	fstream outfile_refxyx;
	
	double sum_EX=0., sum_EY=0., sum_EZ=0.;
	double abssum_EX=0., abssum_EY=0., abssum_EZ=0.;
	double sum_EX2=0., sum_EY2=0., sum_EZ2=0.;
	int count = 0;
	
	///////////////////////////////////////////////////////////////////////////////
	LidarFile.open(Lidarpath, ios::in);
	outfile.open(outpath, ios::out);

	CString st_outfile_adjustedxyz = outpath;
	CString st_outfile_refxyz = outpath;
	
	st_outfile_adjustedxyz.MakeLower(); st_outfile_adjustedxyz.Replace(".qt","_adjusted.txt");
	st_outfile_refxyz.MakeLower(); st_outfile_refxyz.Replace(".qt","_ref.txt");
	
	outfile_adjustedxyz.open(st_outfile_adjustedxyz, ios::out);
	outfile_refxyx.open(st_outfile_refxyz, ios::out);
	
	outfile<<"#dX(M) dY(M) dZ(M) distance(M) time(sec)"<<endl;
	outfile<<"###############################################################################################################"<<endl;
	
	RemoveCommentLine(LidarFile, '#');
	LidarFile>>ws;
	
	while(!LidarFile.eof())
	{		
		LidarFile>>GPS_X>>GPS_Y>>GPS_Z;
		LidarFile>>INS_O>>INS_P>>INS_K;
		LidarFile>>alpha>>beta;
		LidarFile>>dist>>time;
		
		CSMMatrix<double> X0(3,1,0.), TrueP(3,1,0.), TruePb(3,1,0.), TrueRanging(3,1,0.);
		
		X0(0,0) = GPS_X; X0(1,0) = GPS_Y; X0(2,0) = GPS_Z;
		TrueP(0,0) = TrueCfg.Xoffset; TrueP(1,0) = TrueCfg.Yoffset; TrueP(2,0) = TrueCfg.Zoffset;
		TruePb(0,0) = TrueCfg.Xb; TruePb(1,0) = TrueCfg.Yb; TruePb(2,0) = TrueCfg.Zb;
		
		TrueRanging(0,0) = 0; TrueRanging(1,0) = 0; TrueRanging(2,0) = -(dist + TrueCfg.Rangeb);

		//photogrammetric rotation matrix
		CRotationcoeff2 Attitude(Deg2Rad(INS_O), Deg2Rad(INS_P), Deg2Rad(INS_K));

		CRotationcoeff2 TrueSwing(Deg2Rad(alpha), Deg2Rad(beta), 0.);
		CRotationcoeff2 TrueROffset((TrueCfg.Ooffset+TrueCfg.Ob), (TrueCfg.Koffset+TrueCfg.Kb), (TrueCfg.Koffset+TrueCfg.Kb));
		
		CSMMatrix<double> TrueXG = X0 + Attitude.Rmatrix%(TrueP + TruePb) 
			+ Attitude.Rmatrix%TrueROffset.Rmatrix%TrueSwing.Rmatrix%TrueRanging;

		CSMMatrix<double> AdjustedP(3,1,0.), AdjustedPb(3,1,0.), AdjustedRanging(3,1,0.);
				
		AdjustedP(0,0) = AdjustedCfg.Xoffset; AdjustedP(1,0) = AdjustedCfg.Yoffset; AdjustedP(2,0) = AdjustedCfg.Zoffset;
		AdjustedPb(0,0) = AdjustedCfg.Xb; AdjustedPb(1,0) = AdjustedCfg.Yb; AdjustedPb(2,0) = AdjustedCfg.Zb;
		
		AdjustedRanging(0,0) = 0; AdjustedRanging(1,0) = 0; AdjustedRanging(2,0) = -(dist*AdjustedCfg.RangeS + AdjustedCfg.Rangeb);
		
		CRotationcoeff2 AdjustedSwing(Deg2Rad(alpha) + AdjustedCfg.Alpha_b, Deg2Rad(beta) + AdjustedCfg.Beta_b, 0.);
		CRotationcoeff2 AdjustedROffset((AdjustedCfg.Ooffset+AdjustedCfg.Ob), (AdjustedCfg.Poffset+AdjustedCfg.Pb), (AdjustedCfg.Koffset+AdjustedCfg.Kb));
		
		CSMMatrix<double> AdjustedXG = X0 + Attitude.Rmatrix%(AdjustedP + AdjustedPb) 
			+ Attitude.Rmatrix%AdjustedROffset.Rmatrix%AdjustedSwing.Rmatrix%AdjustedRanging;

		//To evaluate the accuracy
		CSMMatrix<double> dX = AdjustedXG - TrueXG;

		//To print out
		outfile.precision(10);
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<dX(0,0)<<"\t"<<setw(10)<<dX(1,0)<<"\t"<<setw(10)<<dX(2,0)<<"\t";
		outfile<<setw(10)<<dist<<"\t"<<setw(10)<<time<<endl;
		outfile.flush();

		outfile_adjustedxyz.precision(10);
		outfile_adjustedxyz<<setw(10)<<AdjustedXG(0,0)<<"\t"<<setw(10)<<AdjustedXG(1,0)<<"\t"<<setw(10)<<AdjustedXG(2,0)<<endl;
		outfile_adjustedxyz.flush();

		outfile_refxyx.precision(10);
		outfile_refxyx<<setw(10)<<TrueXG(0,0)<<"\t"<<setw(10)<<TrueXG(1,0)<<"\t"<<setw(10)<<TrueXG(2,0)<<endl;
		outfile_refxyx.flush();
		
		sum_EX += dX(0,0);
		sum_EY += dX(1,0);
		sum_EZ += dX(2,0);
		
		abssum_EX += fabs(dX(0,0));
		abssum_EY += fabs(dX(1,0));
		abssum_EZ += fabs(dX(2,0));

		sum_EX2 += pow(dX(0,0),2);
		sum_EY2 += pow(dX(1,0),2);
		sum_EZ2 += pow(dX(2,0),2);

		count ++;
	}

	outfile.precision(10);
	outfile.setf(ios::scientific );
	outfile<<endl<<endl;
	outfile<<"[AVE_ERROR]"<<endl;
	outfile<<setw(10)<<sum_EX/count<<"\t"<<setw(10)<<sum_EY/count<<"\t"<<setw(10)<<sum_EZ/count<<"\t";
	outfile<<endl;
	outfile<<"[AVE_ERROR(absolute value)]"<<endl;
	outfile<<setw(10)<<abssum_EX/count<<"\t"<<setw(10)<<abssum_EY/count<<"\t"<<setw(10)<<abssum_EZ/count<<"\t";
	outfile<<endl;
	outfile<<"[RMSE]"<<endl;
	outfile<<setw(10)<<sqrt(sum_EX2/count)<<"\t";
	outfile<<setw(10)<<sqrt(sum_EY2/count)<<"\t";
	outfile<<setw(10)<<sqrt(sum_EZ2/count)<<"\t";
	outfile<<endl;
	outfile.flush();
	
	outfile_adjustedxyz.close();
	outfile_refxyx.close();
	LidarFile.close();
	outfile.close();
	///////////////////////////////////////////////////////////////////////////////

	CString retval;
	retval.Format("%le\t%le\t%le",sqrt(sum_EX2/count),sqrt(sum_EY2/count),sqrt(sum_EZ2/count));
	return retval;
}

CString CLIDARBiasCalibration::BiasCalibrationResultTest_Brazil(CString AdjustedCfgpath, CString TrueCfgpath, CString Lidarpath, CString outpath)
{
	////////////////////////////////
	//
	//To read configuration files
	//
	CLIDARConfig AdjustedCfg;
	CLIDARConfig TrueCfg;
	AdjustedCfg.ReadConfigFile(AdjustedCfgpath);
	TrueCfg.ReadConfigFile(TrueCfgpath);

	///////////////////////////////////////
	//
	//To read LIDAR point cloud (raw data)
	//

	double GPS_X, GPS_Y, GPS_Z; //meter
	double INS_O, INS_P, INS_K; //deg
	double alpha, beta;//deg
	double dist, time;//meter, sec
	fstream LidarFile;
	fstream outfile;
	fstream outfile_adjustedxyz;
	fstream outfile_adjustedxyz_2;
	//fstream outfile_adjustedxyz_ayman;
	fstream outfile_refxyx;
	double sum_EX=0., sum_EY=0., sum_EZ=0.;
	double abssum_EX=0., abssum_EY=0., abssum_EZ=0.;
	double sum_EX2=0., sum_EY2=0., sum_EZ2=0.;
	int count = 0;
	///////////////////////////////////////////////////////////////////////////////
	LidarFile.open(Lidarpath, ios::in);
	outfile.open(outpath, ios::out);

	CString st_outfile_adjustedxyz = outpath;
	CString st_outfile_adjustedxyz_2 = outpath;
	//CString st_outfile_adjustedxyz_ayman = outpath;
	CString st_outfile_refxyz = outpath;
	st_outfile_adjustedxyz.MakeLower(); st_outfile_adjustedxyz.Replace(".qt","_adjusted.txt");
	st_outfile_adjustedxyz_2.MakeLower(); st_outfile_adjustedxyz_2.Replace(".qt","_adjusted_2.txt");
	//st_outfile_adjustedxyz_ayman.MakeLower(); st_outfile_adjustedxyz_ayman.Replace(".qt","_adjusted_ayman.txt");
	st_outfile_refxyz.MakeLower(); st_outfile_refxyz.Replace(".qt","_ref.txt");
	outfile_adjustedxyz.open(st_outfile_adjustedxyz, ios::out);
	outfile_adjustedxyz_2.open(st_outfile_adjustedxyz_2, ios::out);
	//outfile_adjustedxyz_ayman.open(st_outfile_adjustedxyz_ayman, ios::out);
	outfile_refxyx.open(st_outfile_refxyz, ios::out);
	
	outfile<<"#dX(M) dY(M) dZ(M) distance(M) time(sec)"<<endl;
	outfile<<"###############################################################################################################"<<endl;
	
	RemoveCommentLine(LidarFile, '#');
	LidarFile>>ws;
	
	while(!LidarFile.eof())
	{		
		LidarFile>>GPS_X>>GPS_Y>>GPS_Z;
		LidarFile>>INS_O>>INS_P>>INS_K;
		LidarFile>>alpha>>beta;
		LidarFile>>dist>>time;
		
		CSMMatrix<double> X0(3,1,0.), TrueP(3,1,0.), TruePb(3,1,0.), TrueRanging(3,1,0.);
		
		X0(0,0) = GPS_X; X0(1,0) = GPS_Y; X0(2,0) = GPS_Z;
		TrueP(0,0) = TrueCfg.Xoffset; TrueP(1,0) = TrueCfg.Yoffset; TrueP(2,0) = TrueCfg.Zoffset;
		TruePb(0,0) = TrueCfg.Xb; TruePb(1,0) = TrueCfg.Yb; TruePb(2,0) = TrueCfg.Zb;
		
		TrueRanging(0,0) = 0; TrueRanging(1,0) = 0; TrueRanging(2,0) = (dist + TrueCfg.Rangeb);

		//photogrammetric rotation matrix
		CRotationcoeff2 INS2G(Deg2Rad(180),0,0);
		CRotationcoeff2 Attitude(Deg2Rad(INS_O), Deg2Rad(INS_P), Deg2Rad(INS_K));
		Attitude.Rmatrix = INS2G.Rmatrix%Attitude.Rmatrix;

		double O = Deg2Rad(INS_O);//pitch in brazil data
		double P = Deg2Rad(INS_P);//roll in brazil data
		double K = Deg2Rad(INS_K);//yaw in brazil data

		CRotationcoeff2 RollR, PitchR, YawR;

		RollR.ReMake(0, P, 0);
		PitchR.ReMake(O, 0, 0);
		YawR.ReMake(0, 0, K);

		_Mmat_ RollM, PitchM, YawM;
		RollM.ReMake(0, P, 0);
		PitchM.ReMake(O, 0, 0);
		YawM.ReMake(0, 0, -K);

		RollR.ReMake(0, P, 0);
		PitchR.ReMake(0, 0, 0);
		YawR.ReMake(0, 0, -K);

		//2)MK.MO.MP
		CSMMatrix<double> Attitude_2;
		//Attitude_2 = RollR.Rmatrix%PitchR.Rmatrix%YawR.Rmatrix;
		//Attitude_2 = YawR.Rmatrix%PitchR.Rmatrix%RollR.Rmatrix;
		Attitude_2 = RollR.Rmatrix%YawR.Rmatrix;

		CRotationcoeff2 Phi180(Deg2Rad(0), Deg2Rad(180), Deg2Rad(0));
		//Attitude_2 = Phi180.Rmatrix%Attitude_2;

		//CRotationcoeff2 Attitude_2_2(Deg2Rad(180+INS_O), Deg2Rad(INS_P), Deg2Rad(INS_K));
		//Attitude_2_2.Rmatrix = INS2G.Rmatrix%Attitude_2_2.Rmatrix;
		//Attitude_2 = Attitude_2_2.Rmatrix;

		CSMMatrix<double> AdjustedRanging_2;

		//5)Ayman
//		CSMMatrix<double> Attitude_ayman(3,3);
//		double ROLL = Deg2Rad(INS_O);//pitch in input file
//		double PITCH = Deg2Rad(INS_P);//roll in input file
//		double YAW = Deg2Rad(INS_K);//yaw in input file
//
//		Attitude_ayman(0,0) = sin(YAW)*cos(PITCH);
//		Attitude_ayman(0,1) = -cos(YAW)*cos(ROLL) - sin(YAW)*sin(PITCH)*sin(ROLL);
//		Attitude_ayman(0,2) = cos(YAW)*sin(ROLL) - sin(YAW)*sin(PITCH)*cos(ROLL);
//
//		Attitude_ayman(1,0) = cos(YAW)*cos(PITCH);
//		Attitude_ayman(1,1) = sin(YAW)*cos(ROLL) - cos(YAW)*sin(PITCH)*sin(ROLL);
//		Attitude_ayman(1,2) = -sin(YAW)*sin(ROLL) - cos(YAW)*sin(PITCH)*cos(ROLL);
//
//		Attitude_ayman(2,0) = sin(PITCH);
//		Attitude_ayman(2,1) = cos(PITCH)*sin(ROLL);
//		Attitude_ayman(2,2) = cos(PITCH)*cos(ROLL);
				
		CRotationcoeff2 TrueSwing(Deg2Rad(alpha), Deg2Rad(beta), 0.);
		CRotationcoeff2 TrueROffset((TrueCfg.Ooffset+TrueCfg.Ob), (TrueCfg.Koffset+TrueCfg.Kb), (TrueCfg.Koffset+TrueCfg.Kb));
		
		CSMMatrix<double> TrueXG = X0 + Attitude.Rmatrix%(TrueP + TruePb) 
			+ Attitude.Rmatrix%TrueROffset.Rmatrix%TrueSwing.Rmatrix%TrueRanging;

		CSMMatrix<double> AdjustedP(3,1,0.), AdjustedPb(3,1,0.), AdjustedRanging(3,1,0.);
				
		AdjustedP(0,0) = AdjustedCfg.Xoffset; AdjustedP(1,0) = AdjustedCfg.Yoffset; AdjustedP(2,0) = AdjustedCfg.Zoffset;
		AdjustedPb(0,0) = AdjustedCfg.Xb; AdjustedPb(1,0) = AdjustedCfg.Yb; AdjustedPb(2,0) = AdjustedCfg.Zb;
		
		AdjustedRanging(0,0) = 0; AdjustedRanging(1,0) = 0; AdjustedRanging(2,0) = (dist*AdjustedCfg.RangeS + AdjustedCfg.Rangeb);
		
		CRotationcoeff2 AdjustedSwing(Deg2Rad(alpha) + AdjustedCfg.Alpha_b, Deg2Rad(beta) + AdjustedCfg.Beta_b, 0.);
		CRotationcoeff2 AdjustedROffset((AdjustedCfg.Ooffset+AdjustedCfg.Ob), (AdjustedCfg.Poffset+AdjustedCfg.Pb), (AdjustedCfg.Koffset+AdjustedCfg.Kb));
		
		CSMMatrix<double> AdjustedXG = X0 + Attitude.Rmatrix%(AdjustedP + AdjustedPb) 
			+ Attitude.Rmatrix%AdjustedROffset.Rmatrix%AdjustedSwing.Rmatrix%AdjustedRanging;

		AdjustedRanging_2 = AdjustedRanging;
		AdjustedRanging_2(2,0) = -1.0*AdjustedRanging_2(2,0);
	
		CRotationcoeff2 AdjustedSwing2(Deg2Rad(-alpha) - AdjustedCfg.Alpha_b, Deg2Rad(-beta) - AdjustedCfg.Beta_b, 0.);

		CSMMatrix<double> AdjustedXG_2 = X0 + Attitude_2%(AdjustedP + AdjustedPb) 
			+ Attitude_2%AdjustedROffset.Rmatrix%AdjustedSwing2.Rmatrix%AdjustedRanging_2;

//		CSMMatrix<double> AdjustedXG_ayman = X0 + Attitude_ayman%(AdjustedP + AdjustedPb) 
//			+ Attitude_ayman%AdjustedROffset.Rmatrix%AdjustedSwing2.Rmatrix%(-AdjustedRanging);

		//To evaluate the accuracy
		CSMMatrix<double> dX = AdjustedXG - TrueXG;

		//To print out
		outfile.precision(10);
		outfile.setf(ios::scientific );
		outfile<<setw(10)<<dX(0,0)<<"\t"<<setw(10)<<dX(1,0)<<"\t"<<setw(10)<<dX(2,0)<<"\t";
		outfile<<setw(10)<<dist<<"\t"<<setw(10)<<time<<endl;
		outfile.flush();

		outfile_adjustedxyz.precision(10);
		outfile_adjustedxyz<<setw(10)<<AdjustedXG(0,0)<<"\t"<<setw(10)<<AdjustedXG(1,0)<<"\t"<<setw(10)<<AdjustedXG(2,0)<<endl;
		outfile_adjustedxyz.flush();

		outfile_adjustedxyz_2.precision(10);
		outfile_adjustedxyz_2<<setw(10)<<AdjustedXG_2(0,0)<<"\t"<<setw(10)<<AdjustedXG_2(1,0)<<"\t"<<setw(10)<<AdjustedXG_2(2,0)<<endl;
		outfile_adjustedxyz_2.flush();

//		outfile_adjustedxyz_ayman.precision(10);
//		outfile_adjustedxyz_ayman<<setw(10)<<AdjustedXG_ayman(0,0)<<"\t"<<setw(10)<<AdjustedXG_ayman(1,0)<<"\t"<<setw(10)<<AdjustedXG_ayman(2,0)<<endl;
//		outfile_adjustedxyz_ayman.flush();

		outfile_refxyx.precision(10);
		outfile_refxyx<<setw(10)<<TrueXG(0,0)<<"\t"<<setw(10)<<TrueXG(1,0)<<"\t"<<setw(10)<<TrueXG(2,0)<<endl;
		outfile_refxyx.flush();
		
		sum_EX += dX(0,0);
		sum_EY += dX(1,0);
		sum_EZ += dX(2,0);
		
		abssum_EX += fabs(dX(0,0));
		abssum_EY += fabs(dX(1,0));
		abssum_EZ += fabs(dX(2,0));

		sum_EX2 += pow(dX(0,0),2);
		sum_EY2 += pow(dX(1,0),2);
		sum_EZ2 += pow(dX(2,0),2);

		LidarFile>>ws;

		count ++;
	}

	outfile.precision(10);
	outfile.setf(ios::scientific );
	outfile<<endl<<endl;
	outfile<<"[AVE_ERROR]"<<endl;
	outfile<<setw(10)<<sum_EX/count<<"\t"<<setw(10)<<sum_EY/count<<"\t"<<setw(10)<<sum_EZ/count<<"\t";
	outfile<<endl;
	outfile<<"[AVE_ERROR(absolute value)]"<<endl;
	outfile<<setw(10)<<abssum_EX/count<<"\t"<<setw(10)<<abssum_EY/count<<"\t"<<setw(10)<<abssum_EZ/count<<"\t";
	outfile<<endl;
	outfile<<"[RMSE]"<<endl;
	outfile<<setw(10)<<sqrt(sum_EX2/count)<<"\t";
	outfile<<setw(10)<<sqrt(sum_EY2/count)<<"\t";
	outfile<<setw(10)<<sqrt(sum_EZ2/count)<<"\t";
	outfile<<endl;
	outfile.flush();
	
	outfile_adjustedxyz.close();
	outfile_adjustedxyz_2.close();
	//outfile_adjustedxyz_ayman.close();
	outfile_refxyx.close();
	LidarFile.close();
	outfile.close();
	///////////////////////////////////////////////////////////////////////////////

	CString retval;
	retval.Format("%le\t%le\t%le",sqrt(sum_EX2/count),sqrt(sum_EY2/count),sqrt(sum_EZ2/count));
	return retval;
}

int CLIDARBiasCalibration::FindCStringID(CSMList<CString> &list, CString ID)
{
	int nSort = -1;

	int num = list.GetNumItem();
	for(int i=0; i<num; i++)
	{
		CString nID = list.GetAt(i);
		if(nID == ID)
		{
			nSort = i;
			break;
		}
	}

	return nSort;
}

void CLIDARBiasCalibration::BrazilTrajectoryDraw(CString trajectorypath, CString imgpath, double res)
{
	double Pos_X, Pos_Y, Pos_Z;
	double Roll, Yaw, Pitch;
	double Time;
	double dumy;

	fstream trajectory;
	trajectory.open(trajectorypath, ios::in);

	//Read descriptions
	char line[MAX_LINE_LENGTH];
	for(int i=0; i<23; i++)
	{
		if(!trajectory.eof())
		{
			trajectory.getline(line,MAX_LINE_LENGTH);
		}
	}

	double max_X=-1.0e20, max_Y=-1.0e20, max_Z=-1.0e20;
	double min_X=1.0e20, min_Y=1.0e20, min_Z=1.0e20;

	CSMList<double> List_X, List_Y, List_Z;
		
	while(!trajectory.eof())
	{
		//Time (sec)
		trajectory>>Time;
		//dist (M)
		trajectory>>dumy;
		//Pos(X,Y,Z)
		trajectory>>Pos_X;
		trajectory>>Pos_Y;
		trajectory>>Pos_Z;
		//Pos(Lat,Lon,H)
		trajectory>>dumy;
		trajectory>>dumy;
		trajectory>>dumy;
		//Attitute
		trajectory>>Pitch;
		trajectory>>Roll;
		trajectory>>Yaw;
		
		//getline
		trajectory.getline(line,MAX_LINE_LENGTH);

		List_X.AddTail(Pos_X);
		List_Y.AddTail(Pos_Y);
		List_Z.AddTail(Pos_Z);

		if(max_X<Pos_X) max_X = Pos_X;
		if(max_Y<Pos_Y) max_Y = Pos_Y;
		if(max_Z<Pos_Z) max_Z = Pos_Z;

		if(min_X>Pos_X) min_X = Pos_X;
		if(min_Y>Pos_Y) min_Y = Pos_Y;
		if(min_Z>Pos_Z) min_Z = Pos_Z;
	}

	int width = (int)((max_X - min_X)/res + 10.5);
	int height = (int)((max_Y - min_Y)/res + 10.5);
	CBMPImage img; img.Create(width, height, 8);
	CBMPPixelPtr imgpixel(img);

	DATA<double>* dataX=NULL;
	DATA<double>* dataY=NULL;
	DATA<double>* dataZ=NULL;

	int index_row, index_col;

	for(int i=0; i<height; i++)
	{
		for(int j=0; j<width; j++)
		{
			imgpixel[i][j] = (BYTE)125;
		}
	}

	
	int num = List_X.GetNumItem();
	for(int i=0; i<num; i++)
	{
		dataX = List_X.GetNextPos(dataX);
		dataY = List_Y.GetNextPos(dataY);
		dataZ = List_Z.GetNextPos(dataZ);

		Pos_X = dataX->value;
		Pos_Y = dataY->value;
		Pos_Z = dataZ->value;

		index_col = (int)((Pos_X - min_X)/res + 0.5) + 5;
		index_row = (int)((max_Y - Pos_Y)/res + 0.5) + 5;

		if(i==0)
		{
			for(int r=index_row-2; r<=index_row+2; r++)
			{
				for(int c=index_col-2; c<=index_col+2; c++)
				{
					imgpixel[r][c] = (BYTE)255;
				}
			}
		}
		else if(i == (num-1))
		{
			for(int r=index_row-2; r<=index_row+2; r++)
			{
				for(int c=index_col-2; c<=index_col+2; c++)
				{
					imgpixel[r][c] = (BYTE)0;
				}
			}
		}
		else
			imgpixel[index_row][index_col] = (BYTE)0;
	}

	img.SaveBMP(imgpath);

	trajectory.close();
}

void CLIDARBiasCalibration::RawDataTrajectoryDraw(CString rawpath, CString imgpath, double res)
{
	fstream LidarFile;
	LidarFile.open(rawpath, ios::in);

	RemoveCommentLine(LidarFile, '#');
	LidarFile>>ws;

	double max_X=-1.0e20, max_Y=-1.0e20, max_Z=-1.0e20;
	double min_X=1.0e20, min_Y=1.0e20, min_Z=1.0e20;

	CSMList<double> List_X, List_Y, List_Z;

	double GPS_X, GPS_Y, GPS_Z; //meter
	double INS_O, INS_P, INS_K; //deg
	double alpha, beta;//deg
	double dist, time;//meter, sec
		
	while(!LidarFile.eof())
	{
		LidarFile>>GPS_X>>GPS_Y>>GPS_Z;
		LidarFile>>INS_O>>INS_P>>INS_K;
		LidarFile>>alpha>>beta;
		LidarFile>>dist>>time;

		LidarFile>>ws;

		List_X.AddTail(GPS_X);
		List_Y.AddTail(GPS_Y);
		List_Z.AddTail(GPS_Z);

		if(max_X<GPS_X) max_X = GPS_X;
		if(max_Y<GPS_Y) max_Y = GPS_Y;
		if(max_Z<GPS_Z) max_Z = GPS_Z;

		if(min_X>GPS_X) min_X = GPS_X;
		if(min_Y>GPS_Y) min_Y = GPS_Y;
		if(min_Z>GPS_Z) min_Z = GPS_Z;
	}

	int width = (int)((max_X - min_X)/res + 10.5);
	int height = (int)((max_Y - min_Y)/res + 10.5);
	CBMPImage img; img.Create(width, height, 8);
	CBMPPixelPtr imgpixel(img);

	DATA<double>* dataX=NULL;
	DATA<double>* dataY=NULL;
	DATA<double>* dataZ=NULL;

	for(int i=0; i<height; i++)
	{
		for(int j=0; j<width; j++)
		{
			imgpixel[i][j] = (BYTE)125;
		}
	}

	int index_row, index_col;
	int num = List_X.GetNumItem();

	for(int i=0; i<num; i++)
	{
		dataX = List_X.GetNextPos(dataX);
		dataY = List_Y.GetNextPos(dataY);
		dataZ = List_Z.GetNextPos(dataZ);

		GPS_X = dataX->value;
		GPS_Y = dataY->value;
		GPS_Z = dataZ->value;

		index_col = (int)((GPS_X - min_X)/res + 0.5) + 5;
		index_row = (int)((max_Y - GPS_Y)/res + 0.5) + 5;

		if(i==0)
		{
			for(int r=index_row-2; r<=index_row+2; r++)
			{
				for(int c=index_col-2; c<=index_col+2; c++)
				{
					imgpixel[r][c] = (BYTE)255;
				}
			}
		}
		else if(i == (num-1))
		{
			for(int r=index_row-2; r<=index_row+2; r++)
			{
				for(int c=index_col-2; c<=index_col+2; c++)
				{
					imgpixel[r][c] = (BYTE)0;
				}
			}
		}
		else
			imgpixel[index_row][index_col] = (BYTE)0;
	}

	img.SaveBMP(imgpath);

	LidarFile.close();
}

void CLIDARBiasCalibration::MatrixOut(fstream &file, CLeastSquareLIDAR &LS)
{
	int i, j;
	
	CSMMatrix<double> CMAT = LS.GetCmatrix();
	file<<setw(10)<<"[CMAT]"<<endl;
	for(i=0; i<(int)CMAT.GetRows(); i++)
	{
		for(j=0; j<(int)CMAT.GetCols(); j++)
			file<<setw(10)<<CMAT(i,j)<<"\t";
		file<<endl;
	}
	file<<endl;
	
	CSMMatrix<double> CorreMat;
	CSMMatrix<double> NMAT_dot = LS.GetNdot();
	CSMMatrix<double> NMAT_dot_inv = NMAT_dot.Inverse();

//	file<<setw(10)<<"[NMAT_dot Mat]"<<endl;
//	for(i=0; i<(int)NMAT_dot.GetRows(); i++)
//	{
//		for(j=0; j<(int)NMAT_dot.GetCols(); j++)
//			file<<setw(10)<<NMAT_dot(i,j)<<"\t";
//		file<<endl;
//	}
//	file.flush();
//	file<<endl;
	
	CorreMat = Correlation(NMAT_dot_inv);
	file<<setw(10)<<"[CorreMat]"<<endl;
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
			file<<setw(10)<<CorreMat(i,j)<<"\t";
		file<<endl;
	}
	file.flush();
	file<<endl;

	CSMList<MatrixData> V;
	LS.GetLIDARErrorMatwithPoint(V);
	int n_mat = V.GetNumItem();
	DATA<MatrixData> *pos = NULL;
	file<<setw(10)<<"[V]"<<endl;
	for(i=0; i<n_mat; i++)
	{
		pos = V.GetNextPos(pos);
		int nrows = pos->value.mat.GetRows();
		for(j=0; j<nrows; j++)
		{
			file<<setw(10)<<pos->value.mat(j,0)<<endl;
		}
	}
	file<<endl;
	file.flush();

//2007.05.09
//	for(i=0; i<(int)V.GetRows(); i++)
//	{
//		for(j=0; j<(int)V.GetCols(); j++)
//			file<<setw(10)<<V(i,j)<<"\t";
//		file<<endl;
//	}
//	file.flush();
//	file<<endl;
}

CSMMatrix<double> CLIDARBiasCalibration::Cal_ErrorPropagation(
															  /*GPS[m]*/double sigma_GPSX, double sigma_GPSY, double sigma_GPSZ,
															  /*INS angle[deg]*/double INS_O, double sigma_INS_O, double INS_P, double sigma_INS_P, double INS_K, double sigma_INS_K, 
															  /*Offset vector[m]*/double Px, double sigma_Px, double Py, double sigma_Py, double Pz, double sigma_Pz,
															  /*Offset angle[deg]*/double dO, double sigma_dO, double dP, double sigma_dP, double dK, double sigma_dK,
															  /*Range[m]*/double dist, double sigma_dist,
															  /*Scan angles[deg]*/double alpha, double sigma_alpha, double beta, double sigma_beta
															  )
	{
		//LIDAR Equation
		//G = GPS + R_ins*P + R_ins*R_bore*R_scan*Range
		
		//Degree to Radian
		INS_O = Deg2Rad(INS_O);
		INS_P = Deg2Rad(INS_P);
		INS_K = Deg2Rad(INS_K);
		
		sigma_INS_O = Deg2Rad(sigma_INS_O);
		sigma_INS_P = Deg2Rad(sigma_INS_P);
		sigma_INS_K = Deg2Rad(sigma_INS_K);
		
		alpha = Deg2Rad(alpha);
		beta = Deg2Rad(beta);
		
		sigma_alpha = Deg2Rad(sigma_alpha);
		sigma_beta = Deg2Rad(sigma_beta);
		
		dO = Deg2Rad(dO);
		dP = Deg2Rad(dP);
		dK = Deg2Rad(dK);
		
		sigma_dO = Deg2Rad(sigma_dO);
		sigma_dP = Deg2Rad(sigma_dP);
		sigma_dK = Deg2Rad(sigma_dK);
		
		//B and Q matrices
		CSMMatrix<double> bmat(3, 15);//GPS(3) INS(3) Scan Angle(2) Range(1) Shift(3) Rotation(3)
		CSMMatrix<double> qmat(15, 15, 0.0);//GPS(3) INS(3) Scan Angle(2) Range(1) Shift(3) Rotation(3)
		qmat(0, 0) = sigma_GPSX;
		qmat(1, 1) = sigma_GPSY;
		qmat(2, 2) = sigma_GPSZ;
		
		qmat(3, 3) = sigma_INS_O;
		qmat(4, 4) = sigma_INS_P;
		qmat(5, 5) = sigma_INS_K;
		
		qmat(6, 6) = sigma_alpha;
		qmat(7, 7) = sigma_beta;
		
		qmat(8, 8) = sigma_dist;
		
		qmat(9, 9) = sigma_Px;
		qmat(10, 10) = sigma_Py;
		qmat(11, 11) = sigma_Pz;
		
		qmat(12, 12) = sigma_dO;
		qmat(13, 13) = sigma_dP;
		qmat(14, 14) = sigma_dK;

		qmat = qmat*qmat;
		
		CRotationcoeff2 INS_R; //INS Rotation matrix: R matrix
		INS_R.ReMake(INS_O, INS_P, INS_K);
		
		//partial derivatives for INS angle
		CRotationcoeff2 dR_dO;
		dR_dO.Partial_dRdO(INS_O, INS_P, INS_K);
		
		CRotationcoeff2 dR_dP;
		dR_dP.Partial_dRdP(INS_O, INS_P, INS_K);
		
		CRotationcoeff2 dR_dK;
		dR_dK.Partial_dRdK(INS_O, INS_P, INS_K);
		
		CSMMatrix<double> Pvector(3,1);
		//boresight vector (spatial offset + spatial bias)
		Pvector(0,0) = Px;
		Pvector(1,0) = Py;
		Pvector(2,0) = Pz;
		
		//INS Rotation matrix: R matrix
		_Rmat_ Offset_R(dO, dP, dK);
		
		//partial derivatives for angle biases (omega)
		_Rmat_ dOffsetR_dO;
		dOffsetR_dO.Partial_dRdO(dO,dP,dK);
		//partial derivatives for angle biases (phi)
		_Rmat_ dOffsetR_dP;
		dOffsetR_dP.Partial_dRdP(dO,dP,dK);
		//partial derivatives for angle biases (kappa)
		_Rmat_ dOffsetR_dK;
		dOffsetR_dK.Partial_dRdK(dO,dP,dK);
		
		//ranging distance + distance bias
		CSMMatrix<double> Rangevector(3,1);
		Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= -dist;
		
		//Scan angle matrix
		_Rmat_ Scan_R(alpha, beta, 0);
		
		CRotationcoeff2 AlphaR(alpha,0,0);
		CRotationcoeff2 BetaR(0,beta,0);
		
		//matrix(3X1) derived from swing angle
		CSMMatrix<double> ScannerVector;
		ScannerVector = Scan_R.Rmatrix%Rangevector;
		
		//
		//partial derivatives for scan angle
		//
		CRotationcoeff2 dAlphaR_dAlpha; 
		dAlphaR_dAlpha.Partial_dRdO(alpha, beta, 0);
		CRotationcoeff2 dBetaR_dBeta; 
		dBetaR_dBeta.Partial_dRdP(alpha, beta, 0);
		
		//	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
		//	CSMMatrix<double> R2 = BetaR.Rmatrix;
		//	CSMMatrix<double> R3 = R1%BetaR.Rmatrix;
		//	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
		//	CSMMatrix<double> R5 = BetaR.Rmatrix;
		//	CSMMatrix<double> R6 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
		
		CSMMatrix<double> F_dAlpha = INS_R.Rmatrix%Offset_R.Rmatrix%dAlphaR_dAlpha.Rmatrix%Rangevector;
		CSMMatrix<double> F_dBeta = INS_R.Rmatrix%Offset_R.Rmatrix%dBetaR_dBeta.Rmatrix%Rangevector;
		
		CSMMatrix<double> Lmat =  INS_R.Rmatrix%Offset_R.Rmatrix%ScannerVector;
		
		CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%ScannerVector));//(P + Offset_R*Scan_R*Range)
		
		CSMMatrix<double> dFdO = dR_dO.Rmatrix%temp;
		CSMMatrix<double> dFdP = dR_dP.Rmatrix%temp;
		CSMMatrix<double> dFdK = dR_dK.Rmatrix%temp;
		
		CSMMatrix<double> dFdOffsetO = INS_R.Rmatrix%dOffsetR_dO.Rmatrix%ScannerVector;
		CSMMatrix<double> dFdOffsetP = INS_R.Rmatrix%dOffsetR_dP.Rmatrix%ScannerVector;
		CSMMatrix<double> dFdOffsetK = INS_R.Rmatrix%dOffsetR_dK.Rmatrix%ScannerVector;
		
		/////////////////////////////
		//B Matrix
		/////////////////////////////
		//GNSS obs
		bmat(0,0) = 1.0;
		bmat(0,1) = 0.0;
		bmat(0,2) = 0.0;
		
		bmat(1,0) = 0.0;
		bmat(1,1) = 1.0;
		bmat(1,2) = 0.0;
		
		bmat(2,0) = 0.0;
		bmat(2,1) = 0.0;
		bmat(2,2) = 1.0;
		
		//INS obs
		bmat(0,3) = dFdO(0,0);
		bmat(1,3) = dFdO(1,0);
		bmat(2,3) = dFdO(2,0);
		
		bmat(0,4) = dFdP(0,0);
		bmat(1,4) = dFdP(1,0);
		bmat(2,4) = dFdP(2,0);
		
		bmat(0,5) = dFdK(0,0);
		bmat(1,5) = dFdK(1,0);
		bmat(2,5) = dFdK(2,0);
		
		//swing angle
		bmat(0,6) = F_dAlpha(0,0);
		bmat(1,6) = F_dAlpha(1,0);
		bmat(2,6) = F_dAlpha(2,0);
		
		bmat(0,7) = F_dBeta(0,0);
		bmat(1,7) = F_dBeta(1,0);
		bmat(2,7) = F_dBeta(2,0);
		
		//ranging distance
		bmat(0,8) = Lmat(0, 0);
		bmat(1,8) = Lmat(1, 0);
		bmat(2,8) = Lmat(2, 0);
		
		//Offset vector obs
		bmat(0,9) = INS_R.Rmatrix(0, 0);
		bmat(0,10) = INS_R.Rmatrix(0, 1);
		bmat(0,11) = INS_R.Rmatrix(0, 2);
		
		bmat(1,9) = INS_R.Rmatrix(1, 0);
		bmat(1,10) = INS_R.Rmatrix(1, 1);
		bmat(1,11) = INS_R.Rmatrix(1, 2);
		
		bmat(2,9) = INS_R.Rmatrix(2, 0);
		bmat(2,10) = INS_R.Rmatrix(2, 1);
		bmat(2,11) = INS_R.Rmatrix(2, 2);
		
		//allignment obs
		bmat(0,12) = dFdOffsetO(0, 0);
		bmat(0,13) = dFdOffsetP(0, 0);
		bmat(0,14) = dFdOffsetK(0, 0);
		
		bmat(1,12) = dFdOffsetO(1, 0);
		bmat(1,13) = dFdOffsetP(1, 0);
		bmat(1,14) = dFdOffsetK(1, 0);
		
		bmat(2,12) = dFdOffsetO(2, 0);
		bmat(2,13) = dFdOffsetP(2, 0);
		bmat(2,14) = dFdOffsetK(2, 0);
		
		CSMMatrix<double> ret = bmat%qmat%(bmat.Transpose());
		
		return ret;
}