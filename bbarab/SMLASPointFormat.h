#pragma once

#include <afx.h>

class PointFormat0//size: 20 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		UserData;
	unsigned short		PointSourceID;
public:
	PointFormat0() {}

	PointFormat0(PointFormat0 &copy) { DataCopy(copy); }

	PointFormat0& operator = (PointFormat0 &copy) { DataCopy(copy); return (*this); }

	void DataCopy(PointFormat0 &copy)
	{
		memcpy(this, &copy, sizeof(PointFormat0));
	}
};

class PointFormat1//size: 28 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		UserData;
	unsigned short		PointSourceID;
	//char				GPSTime[8];//should be changed as double when it is used
	double				GPSTime;
public:
	PointFormat1() {}

	PointFormat1(PointFormat1 &copy) { DataCopy(copy); }

	PointFormat1& operator = (PointFormat1 &copy) { DataCopy(copy); return (*this); }

	void DataCopy(PointFormat1 &copy)
	{
		memcpy(this, &copy, sizeof(PointFormat1));
	}
};

class PointFormat2//size: 26 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		UserData;
	unsigned short		PointSourceID;
	unsigned short		Red;
	unsigned short		Green;
	unsigned short		Blue;
public:
	PointFormat2() {}

	PointFormat2(PointFormat2 &copy) { DataCopy(copy); }

	PointFormat2& operator = (PointFormat2 &copy) { DataCopy(copy); return (*this); }

	void DataCopy(PointFormat2 &copy)
	{
		memcpy(this, &copy, sizeof(PointFormat2));
	}
};

class PointFormat3//size: 34 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	char				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		UserData;
	unsigned short		PointSourceID;
	//char				GPSTime[8];//should be changed as double when it is used
	double				GPSTime;
	unsigned short		Red;
	unsigned short		Green;
	unsigned short		Blue;
public:
	PointFormat3() {}

	PointFormat3(PointFormat3 &copy) { DataCopy(copy); }

	PointFormat3& operator = (PointFormat3 &copy) { DataCopy(copy); return (*this); }

	void DataCopy(PointFormat3 &copy)
	{
		memcpy(this, &copy, sizeof(PointFormat3));
	}
};

class PointFormat7//size: 36 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 4;
	unsigned char		NumberOfReturns : 4;
	unsigned char		ClassificationFlags : 4;
	unsigned char		ScannerChannel : 2;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	unsigned char		UserData;
	short				ScanAngleRank;//-90(left) to 90(right)	
	unsigned short		PointSourceID;
	double				GPSTime;
	unsigned short		Red;
	unsigned short		Green;
	unsigned short		Blue;
public:
	PointFormat7() {}

	PointFormat7(PointFormat7 &copy) { DataCopy(copy); }

	PointFormat7& operator = (PointFormat7 &copy) { DataCopy(copy); return (*this); }

	void DataCopy(PointFormat7 &copy)
	{
		memcpy(this, &copy, sizeof(PointFormat7));
	}
};

class PointFormat8//size: 38 bytes
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 4;
	unsigned char		NumberOfReturns : 4;
	unsigned char		ClassificationFlags : 4;
	unsigned char		ScannerChannel : 2;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	unsigned char		UserData;
	short				ScanAngleRank;//-90(left) to 90(right)	
	unsigned short		PointSourceID;
	double				GPSTime;
	unsigned short		Red;
	unsigned short		Green;
	unsigned short		Blue;
	unsigned short		NIR;
public:
	PointFormat8() {}

	PointFormat8(PointFormat8 &copy) { DataCopy(copy); }

	PointFormat8& operator = (PointFormat8 &copy) { DataCopy(copy); return (*this); }

	void DataCopy(PointFormat8 &copy)
	{
		memcpy(this, &copy, sizeof(PointFormat8));
	}
};