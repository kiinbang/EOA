[M matrix]
phi=asin(m(3,1)); % note: two choices here
omega=atan2(-m(3,2)/cos(phi),m(3,3)/cos(phi));
kappa=atan2(-m(2,1)/cos(phi),m(1,1)/cos(phi));
