/*
 * Copyright (c) 2002-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(_SPACEMATICS_ORTHOIMAGE_FRAME_)
#define _SPACEMATICS_ORTHOIMAGE_FRAME_      
                                      
#if _MSC_VER > 1000                   
#pragma once                          
#endif // _MSC_VER > 1000             
                                      
#include <TemplateDEM.h>
#include <SMCoordinateClass.h>
#include "./Image_Util/ImagePixel.h"
#include <SMOrthoimageFrameClass.h>

//////////////////////////////////////////////////////////////////////////////////////////////
//
//2006: Add CFrameOrthoImage Class <--Ortho gen with rigorous param for frame imagery
//          1)New ortho-photo class for frame imagery
//          2)Derived form CSatOrthoImage class
//			3)Completely new source code without modifying old codes
//
//
//
//
//
//
// SMOrthoimageFrame.h : header file
// Created by BANG, KI IN

/**
* @class CFrameOrthoImage
* @brief Revision: 2006-03-29\n
* This class is derived form CSatOrthoImage class.\n
* @author Bang, Ki In
* @version 1.0
* @Since 2006~
* @bug N/A.
* @warning N/A.
*
*/
class CFrameOrthoImage : public CFrameOrthoClass
{
protected: //member variable
	CImage m_Source; /**<Source image */
	char source_path[256];
	CImage m_Ortho; /**<Ortho image */
	char ortho_path[256];
	CTemplateDEM<double> m_RefDEM;/**<DEM */
	char dem_path[256];
	char demhdr_path[256];

	int method; //0:Rectification 1:DiffZ 2:Angle 3:Sorted DSM

public: //member function

	/**MakeOrtho
	* Description	    : start function for the ortho-image generation 
	*@param char* inputpath: To read input data
	*@return bool 
	*/
	bool MakeOrtho_LTM(char* inputpath);


private:

	/**ReadInputFile
	* Description	    : To input data file
	*@param const char* fname: file path
	*@return bool
	*/
	bool ReadInputFile(const char* fname);

	/**DataOpen
	* Description	    : To open data file and create empty ortho-photo
	*@return bool
	*/
	bool DataOpen();

	/**ResampleOrtho_Slope_DiffZ
	* Description	    : Gray ortho image resampling by slope method
	*@return bool 
	*/
	bool ResampleOrtho_Slope_DiffZ();

	/**ColorResampleOrtho_Slope_DiffZ
	* Description	    : Color ortho image resampling by slope method
	*@return bool 
	*/
	bool ColorResampleOrtho_Slope_DiffZ();

	/**CheckOcclusion_DiffZ
	* Description	    : to estimate occlusion area 
	*@param double Xl : perspective center coordinates(Xo)
	*@param double Yl : perspective center coordinates(Yo)
	*@param double Zl : perspective center coordinates(Zo)
	*@param int indexX : DEM index (for X direction)
	*@param int indexY : DEM index (for Y direction)
	*@param double MAXDIFFHEIGHT : serching limit
	*@return bool 
	*/
	bool CheckOcclusion_DiffZ(double Xl, double Yl, double Zl, int indexX, int indexY, double MAXDIFFHEIGHT);
	bool CheckOcclusion_Angle(double Xl, double Yl, double Zl, int indexX, int indexY, double MAXDIFFHEIGHT);

};


/////////////////////////////////////////////////////////////////////////////

#endif // !defined(_SPACEMATICS_ORTHOIMAGE_FRAME_)