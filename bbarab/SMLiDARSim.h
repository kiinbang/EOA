/*
 * Copyright (c) 2006-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// SMLiDARSim.h: interface for the CISSMLiDAR class.
//
//////////////////////////////////////////////////////////////////////
//
//Written by Bang, Ki In
//
//Since 2006/02/14
//
//This file is for the CISSMLiDAR class which support LiDAR data manipulation.
//
//
//
//
#if !defined(__INTERSOLUTION__SPACEMATICS__BBARAB__LIDAR__SOLUTION___)
#define __INTERSOLUTION__SPACEMATICS__BBARAB__LIDAR__SOLUTION___

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TemplateDEM.h"
#include "SpaceMaticsStr.h"
#include "LiDarBiasCalibration.h"
#include "SMCoordinateClass.h"

#include "KDTree.h"
#include "SMList.h"

namespace ISSM_BBARAB
{
	using namespace SMATICS_BBARAB;
	
#define _MAX_LOOP_ 500
#define _MAX_STRING_LENGTH_ 512

	/**
	* @class TrjData
	* @brief class for trajectory data\r.
	* @author Bang, Ki In
	* @version 1.0
	* @Since 2010-02-21
	* @bug N/A.
	* @warning N/A.
	*/
	class TrjData
	{
	public:
		double time, X, Y, Z, O, P, K;
	};

	/**
	* @class CLiDARSimConfig
	* @brief class for LiDAR configuration information\r.
	* @author Bang, Ki In
	* @version 1.0
	* @Since 2006-02-15
	* @bug N/A.
	* @warning N/A.
	*
	* \nThis is a class of LIDAR bias calibration program.
	* <ul> 
	* <li> boresight biases
	*	<ol>
	*	<li> Spatial(3) and rotational(3) biases in boresight
	*	<li> ranging bias (1)
	*	</ol>
	* </ul>\n
	*/
	class CLiDARSimConfig
	{
		friend class CBBARABLiDARSim;
	public:
		
		CLiDARSimConfig();
		
		virtual ~CLiDARSimConfig();

		double GetRandNum()
		{
			double half_RAND_MAX = (double)RAND_MAX/2.;
			double rand_num = (double)rand();//0 ~ RAND_MAX
			rand_num = rand_num/half_RAND_MAX - 1.0;//-1~1
			rand_num = (rand_num - rand_ave)/rand_sd;

			return rand_num;
		}

		
		void SetData(double dOmega, double dPhi, double dKappa,
			double OffsetX, double OffsetY, double OffsetZ,
			double startX, double startY, double startZ,
			double *a, double *b, double *c,
			double startO, double startP, double startK,
			double *d, double *e, double *f,
			double TA, double TB,
			double bf_time, double interval_time,
			double stime, double etime,
			int debug_tag);


		
		void GetOrientation(double t);

		void FindGround(double &X, double &Y, double Z);
		
		void GetOrientationWithError();
		
		void SetTime(double t, double &GPSX, double &GPSY, double &GPSZ, double &INSO, double &INSP, double &INSK, double &alpha, double &beta);
		void SetTime_II(double t, double &GPSX, double &GPSY, double &GPSZ, double &INSO, double &INSP, double &INSK, double &alpha, double &beta);

		
	public://property
		
		double GetEndTime() {return EndTime;}
		double GetStartTime() {return StartTime;}
		double GetMax_Zval() {return Max_Zval;}
		double GetMin_Zval() {return Min_Zval;}
		void SetMax_Zval(double Z) {Max_Zval=Z;}
		void SetMin_Zval(double Z) {Min_Zval=Z;}
		int GetDEBUG_TAG() {return m_DEBUG_TAG;}
		double GetTimeGap() {return TimeGap;}
		
		//Get MG matrix: MG = ML*M_INS
		inline CSMMatrix<double> GetMG() {return MG;}
		inline CSMMatrix<double> GetMG_Error() {return MG_Error;}
		//Get T matrix: MG*GPS + ML*Offset
		inline CSMMatrix<double> GetT() {return T;}
		inline CSMMatrix<double> GetT_Error() {return T_Error;}
		//Get L_Unit matrix: L_Unit = GPS + R_INS*Offset
		inline CSMMatrix<double> GetL_Unit() {return L_Unit;}
		//Get alpha(X axis) and beta(Y axis)
		inline void GetLBAngle(double &a, double &b) {a=alpha; b=beta;}
		//Get alpha(X axis) and beta(Y axis) with error
		inline void GetLBErrorAngle(double &a, double &b) {a=alpha_error; b=beta_error;}
		//Get GPS position
		void GetGPSPosition(double &X, double &Y, double &Z) {X=GPS(0,0); Y=GPS(1,0); Z=GPS(2,0);}
		//Get GPS position with error
		void GetGPSErrorPosition(double &X, double &Y, double &Z) {X=GPS_Error(0,0); Y=GPS_Error(1,0); Z=GPS_Error(2,0);}
		//Get INS angle
		inline void GetINSAngle(double &O, double &P, double &K) {O=INSO; P=INSP; K=INSK;}
		//Get INS angle with random error
		inline void GetINSErrorAngle(double &O, double &P, double &K) {O=INSO_Error; P=INSP_Error; K=INSK_Error;}
		//Get Hanlde of sigma of offset angle of Laser Unit
		inline double* GetHandleSigmaOffsetAngle() {return sigma_R_LU;}
		//Get Hanlde of sigma of offset angle of Laser Unit
		inline double* GetHandleSigmaOffsetVector() {return sigma_OffsetVector;}
		//Get Hanlde of sigma of GPS signal
		inline double* GetHandleSigmaGPS() {return sigma_GPS;}
		//Get Hanlde of sigma of INS signal
		double* GetHandleSigmaINS() {return sigma_INS;}
		//Get Handle of sigma of alpha
		inline double* GetHandleSigmaAlpha() {return &sigma_alpha;}
		//Get Handle of bias of alpha
		inline double* GetHandleBiasAlpha() {return &bias_alpha;}
		//Get Handle of scale of alpha
		inline double* GetHandleScaleAlpha() {return &scale_alpha;}
		//GetHandle of sigma of beta
		inline double* GetHandleSigmaBeta() {return &sigma_beta;}
		//Get Handle of bias of beta
		inline double* GetHandleBiasBeta() {return &bias_beta;}
		//Get Handle of scale of beta
		inline double* GetHandleScaleBeta() {return &scale_beta;}
		//GetHandle of bias of beam distance
		inline double* GetHandleBiasBeam() {return &bias_beam;}
		//GetHandle of bias of offset angle of laser unit
		inline double* GetHandleBiasOffsetAngle() {return bias_R_LU;}
		//GetHandle of bias of offset vector of laser unit
		inline double* GetHandleBiasOffsetVector() {return bias_OffsetVector;}
		//GetHandle of sigma of ranging distance
		inline double* GetHandleSigmaRanging() {return &sigma_ranging;}
		//Get sigma of ranigng
		inline double GetSigmaRanging() {return sigma_ranging;}
		//Get BFTime(1 cycle time)
		inline double GetBFTime() {return BFTime;}
		//GetHandle of scale of ranging distance
		inline double* GetHandleScaleRanging() {return &scale_beam;}		
		
	protected:
		//
		//Offset attributes
		//
		double dO, dP, dK; /**<(unit: rad)dO, dP, dK: Offset angle between INS and laser unit. Laser_unit = M(dO,dP,dK)*INS*/
		double dX, dY, dZ; /**<(unit: m)dX, dY, dZ: Offset from GPS phase center to laser unit origin*/
		
		//
		//Trajectory model
		//X = sX + A1*t + A2*t^2 + A3*t^3
		//Y = sY + B1*t + B2*t^2 + B3*t^3
		//Z = sZ + C1*t + C2*t^2 + C3*t^3
		//
		double A1, B1, C1;
		double A2, B2, C2;
		double A3, B3, C3;
		//Start position
		double sX, sY, sZ;
		
		//
		//Attitude model
		//O = sO + D1*t + D2*t^2 + D3*t^3
		//P = sP + E1*t + E2*t^2 + E3*t^3
		//K = sK + F1*t + F2*t^2 + F3*t^3
		//
		double D1, E1, F1;
		double D2, E2, F2;
		double D3, E3, F3;
		//Start attitude
		double sO, sP, sK;
		
		//
		//Max and Min height
		//
		double Max_Zval;
		double Min_Zval;
		
		//
		//Simulation time
		//
		double StartTime;
		double EndTime;
		
		//
		//Laser beam configuration
		//
		double Total_a, Total_b; //*<Field of angle(unit: rad)*/
		double BFTime;//back-and-forth-time(unit: sec)*/
		double TimeGap;//*<Time interval between each signal(unit: sec)*/
		
		//0:no debug 1: simple debug 2: detail debug
		int m_DEBUG_TAG;

		//
		//Given trajectory data
		//
		int trj_option; //0: parameter input 1: file input
		
		KDTree KDTrjTime;
				
		CSMList<TrjData> TrjRecord; 
		
	private:
		CSMMatrix<double> GPS; /**<X0, Y0, Z0: Ground coordinates of GPS phase center at time t*/
		CSMMatrix<double> GPS_Error; /**<X0, Y0, Z0: Ground coordinates of GPS phase center at time t with random error*/
		double sigma_GPS[3];/**<sigma of GPS signal*/
		CSMMatrix<double> L_Unit; /**<Ground coordinates of laser unit optical center at time t*/
		double INSO, INSP, INSK; //Omega, phi, kappa of INS at time t
		double INSO_Error, INSP_Error, INSK_Error; //Omega, phi, kappa of INS at time t with random error
		CSMMatrix<double> R_INS; /**<O, P, K: INS rotation angle. INS = M(O,P,K)*Ground at time t*/
		CSMMatrix<double> R_INS_Error; /**<O, P, K: INS rotation angle with random error. INS = M(O+eo,P+ep,K+ek)*Ground at time t*/
		double sigma_INS[3];
		CSMMatrix<double> OffsetVector; /**<dX, dY, dZ: Offset from GPS phase center to laser unit origin*/
		CSMMatrix<double> OffsetVector_Error;
		double sigma_OffsetVector[3];/**<sigma of Laser Unit offset vector w.r.t INS*/
		double bias_OffsetVector[3];/**<bias of Laser Unit offset vector w.r.t INS*/
		CSMMatrix<double> R_LU; /**<dO, dP, dK: Offset angle between INS and laser unit. Laser_unit = M(dO,dP,dK)*INS*/
		CSMMatrix<double> R_LU_Error;
		double sigma_R_LU[3];/**<sigma of Laser Unit relative angle w.r.t INS*/
		double bias_R_LU[3];/**<bias of Laser Unit relative angle w.r.t INS*/
		double a2;// = tan(Total_a)*tan(Total_a);//a^2 = tan(Total_a)^2(Ÿ�� ù��° �ݰ�)
		double b2;// = tan(Total_b)*tan(Total_b);//b^2 = tan(Total_b)^2(Ÿ�� �ι�° �ݰ�)
		double alpha; //swing angle for X axis at time t
		double alpha_error; //swing angle for X axis at time t with random error
		double bias_alpha; //bias of alpha [deg]
		double sigma_alpha;/**<sigma of Laser beam angle(alpha:X axis)*/
		double scale_alpha;
		double beta; //swing angle for Y axis at time t
		double beta_error; //swing angle for Y axis at time t with random error
		double sigma_beta;/**<sigma of Laser beam angle(beta: Y axis)*/
		double bias_beta; /**<bias of beta [deg]*/
		double scale_beta;
		CSMMatrix<double> R_LB; /**<alpha, beta: Laser beam swing angle: Laser_beam = M(alpha,beta,0)*Laser_unit at time t*/
		CSMMatrix<double> R_LB_Error;
		CSMMatrix<double> MG;/**<MG = ML*M_INS at time t*/
		CSMMatrix<double> MG_Error;
		CSMMatrix<double> ML;/**<ML = M_LB*M_LU at time t*/
		CSMMatrix<double> ML_Error;/**<ML = M_LB*M_LU at time t*/
		CSMMatrix<double> T;/**<T = MG*GPS + ML*Offset at time t*/
		CSMMatrix<double> T_Error;
		double sigma_ranging;/**<sigma of laser beam ranging */
		double RE_range;//random error of range which is calculated in GetOrientationWithError()
		double bias_beam;/**<bias of beam distance*/
		double scale_beam;/**<scale of beam distance */
		double _PI_;/**<Pi(3.141592654....)*/

	public:
		double rand_ave;
		double rand_sd;
	};

using namespace LIDARCALIBRATION_BBARAB;

	/**
	* @class CBBARABLiDARSim
	* @brief class for LIDAR simulation\nThis is a class for LiDAR simulation using LiDAR equation.
	* @author Bang, Ki In
	* @version 1.0
	* @Since 2006-02-15
	* @bug N/A.
	* @warning N/A.
	*
	* \nThis is a class of LIDAR bias calibration program.
	* <ul> 
	* <li> boresight biases
	*	<ol>
	*	<li> Spatial(3) and rotational(3) biases in boresight
	*	<li> ranging bias (1)
	*	</ol>
	* </ul>\n
	*/
	class CBBARABLiDARSim
	{
	public:
		CBBARABLiDARSim();
		
		~CBBARABLiDARSim();


		//Input data (real terrain: DEM)
		bool InputData(const char* simfile, const char* dempath, const char* hdrpath, double lidar_precision);
		
		//Input data (flat terrain: Z=given value)
		bool InputData(const char* simfile, double lidar_precision=0.0);
		
		//Input data (mathematical surface)
		bool InputData(const char* simfile, const char* vertexfile);
		
		//To make LiDAR simulation data (mathematical surface model)
		bool MakeLiDARSimData_MathematicalSurface(const char* fname, double init_distance, bool bGround);
		
		//To make LiDAR simulation data (Real DEM(DSM))
		bool CBBARABLiDARSim::MakeLiDARSimData(const char* fname);
		
		//To make LiDAR simulation data (flat terrain: Z=0)
		bool MakeLiDARSimData_FlatTerrain(const char* fname, double given_Z = 0.0);

		//Batch processor to make simulation data
		bool MakeBatchLiDARSimData_MathematicalSurface(double resolution, const char* prjfile, const char* simfile, double init_distance, bool bGround);

		//Read a given trajectory data
		bool ReadTrajectoryData(CString trjpath);
		
	public:

		/**DEMSimulation
		* Description				: a static function to DEM simulation
		*@param CString vertexfile	: vertex file path
		*@param CString outfile		: output file path
		*@return static void		: return void
		*/
		void DEMSimulation(CString vertexfile, CString outfile);
		
	private:

		/**SearchingInterMediatePoint
		* Description				: to search an intermediate point along the building walls
		*@param double DEM_Z		: height value in DEM
		*@param double old_Z		: previous estimated value for surface Z coordinate
		*@param double &X			: ground X coordinate as an result
		*@param double &Y			: ground Y coordinate as an result
		*@param double &Z			: ground Z coordinate as an result
		*@param DWORD &indexX		: index for DEM (X coordinate)
		*@param DWORD &indexY		: index for DEM (Y coordinate)
		*@param fstream &debugfile	: file stream for the debug file
		*@param int DEBUG_TAG		: tag for debugging
		*@return bool		: true and false
		*/
		bool SearchingInterMediatePoint(double DEM_Z, double old_Z, double &X,  double &Y, double &Z, DWORD &indexX, DWORD &indexY, fstream &debugfile, int DEBUG_TAG);
		

		/**FindGroundCoord
		* Description				: to find an ground coordinate in a DEM(DSM) space
		*@param CSMMatrix<double> MG	: MG = ML*M_INS at time t
		*@param CSMMatrix<double> T	: T = MG*GPS + ML*Offset at time t
		*@param double dist			: ranging distance
		*@param double &Xa			: ground X coordinate as an result
		*@param double &Ya			: ground Y coordinate as an result
		*@param double &Za			: ground Z coordinate as an result
		*@return void				: return void
		*/
		void FindGroundCoord(CSMMatrix<double> MG, CSMMatrix<double> T, double dist, double &Xa, double &Ya, double &Za);
		
		/**FindGroundCoord
		* Description				: to find an ground coordinate (X, Y) using given Z value
		*@param CSMMatrix<double> MG	: MG = ML*M_INS at time t
		*@param CSMMatrix<double> T	: T = MG*GPS + ML*Offset at time t
		*@param double &Xa			: ground X coordinate as an result
		*@param double &Ya			: ground Y coordinate as an result
		*@param double Za			: given ground Z coordinate
		*@return void				: return void
		*/
		void FindGroundCoord(CSMMatrix<double> MG, CSMMatrix<double> T, double &Xa, double &Ya, double Za);

		/**FindGroundCoord
		* Description				: to find an ground coordinate (Z) using given X and Y value
		*@param CSMMatrix<double> MG	: MG = ML*M_INS at time t
		*@param CSMMatrix<double> T	: T = MG*GPS + ML*Offset at time t
		*@param double Xa			: ground X coordinate as an result
		*@param double Ya			: ground Y coordinate as an result
		*@return double				: calculated Z
		*/
		double FindGroundCoord(CSMMatrix<double> MG, CSMMatrix<double> T, double Xa, double Ya);


		/**FindIntersectPoint
		* Description					: To find a intersection point between a plane and a line
		*@param CLIDARPlanarPatch patch	: planar patch data
		*@param CVector P1				: a point 1 on the line
		*@param CVector P2				: a point 2 on the line
		*@param CVector P				: intersection point
		*@return bool					: return true or false
		*/
	
	private:
		CTemplateDEM<double> m_RefDEM; /**< Reference DEM */
		int num_point; /**< number of LiDAR points */
		CLiDARSimConfig config; /**< configuration data */
		double LIDAR_PRECISION; /**< precision of LIDAR simulation */
		//
		//Reference Terrain
		//
		char* refDEMPath; /**< reference DEM(DSM) path */
		char* refDEMHDRPath; /**< reference DEM(DSM) header path */
		bool bErrorModeling; /**< tag for error modeling */
		int m_DEMInterpolationMethod; /**< tag for interpolation method */
		//
		//
		//
		CSMList<CLIDARPlanarPatch> PatchList; /**<planar patches data*/
		double vertex_min[3];
		double vertex_max[3];
	};

};//namespace ISSM_BBARAB

#endif // !defined(__INTERSOLUTION__SPACEMATICS__BBARAB__LIDAR__SOLUTION___)