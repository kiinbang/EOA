// SMOrthoimageFrame.cpp : implementation file
//

#include "stdafx.h"
#include <SMOrthoimageFrame.h>
#include <Collinearity.h>
#include <ProgressBar.h>
#include <UtilityGrocery.h>

#define PATCH_ORTHO_RESOLUTION 10

#define BOUNDARY_MARGIN 300
#define FIRST_RECORD -999
#define _MAXDIFFROW_ 0.5
#define _MAX_STRING_SIZE_ 512
#define BACKGROUND_BW 255
#define _NULLDEM_BW 255
#define _OCCLUSION_BW 0

using namespace SMATICS_BBARAB;

/////////////////////////////////////////////////////////////////////////////
//
//
//
//
// CFrameOrthoImage
//
//
//
//

bool CFrameOrthoImage::ReadInputFile(const char* fname)
{
	double _PI_ = GetPI();
	fstream infile;
	infile.open(fname, ios::in);
	
	RemoveCommentLine(infile, '#');
	//# Version
	double version;
	infile>>version; 
	if(version != 1.0)
	{
		MessageBox(NULL,"Input file version is different.","Worning",MB_OK);
		return false;
	}

	RemoveCommentLine(infile, '#');
	//# xp yp focal_length
	infile>>xp>>yp>>f;

	RemoveCommentLine(infile, '#');
	//# Lens distortion (0: unavailable 1: table 2: parameters)
	infile>>LD_tag;
	RemoveCommentLine(infile, '#');
	//# K1, K2, D1, D2, A1, A2
	//or
	//# Table
	switch(LD_tag)
	{
	case 0:
		{
			k1 = k2 = d1 = d2 = a1 = a2 = 0.0;
		}
		break;
	case 1:
		{
			MessageBox(NULL,"Not implemented","Worning",MB_OK);
			k1 = k2 = d1 = d2 = a1 = a2 = 0.0;
		}
		break;
	case 2:
		{
			infile>>k1>>k2>>d1>>d2>>a1>>a2;
		}
		break;
	default:
		{
			k1 = k2 = d1 = d2 = a1 = a2 = 0.0;
		}
		break;
	}

	RemoveCommentLine(infile, '#');
	//#Photo2Image transformation
	//# Affine(a1, a2, a3, b1, b2, b3)
	//# x'= a0 + a1*x + a2*y
	//# y'= a3 + a4*x + a5*y
	infile>>AffineParam[0]>>AffineParam[1]>>AffineParam[2]>>AffineParam[3]>>AffineParam[4]>>AffineParam[5];

	RemoveCommentLine(infile, '#');
	//#Exterior orientation parameters(Xo,Yo,Zo,Oo,Po,Ko)
	infile>>Xo>>Yo>>Zo>>Oo>>Po>>Ko;
	Oo = Deg2Rad(Oo); Po = Deg2Rad(Po); Ko = Deg2Rad(Ko);

	RemoveCommentLine(infile, '#');
	//#Ortho-photo method setting
	//# 0: Simple differential rectification, 1: Slope method
	infile>>MethodTag;

	RemoveCommentLine(infile, '#');
	//#Resampling method
	//#0: nearest neighborhood 1: bilinear interpolation
	infile>>ResamplingTag;
	
	RemoveCommentLine(infile, '#');
	//# Source image path
	infile.getline(source_path, _MAX_STRING_SIZE_);
	RemoveCommentLine(infile, '#');
	//# DEM path
	infile.getline(dem_path, _MAX_STRING_SIZE_);
	//# NULL DEM
	RemoveCommentLine(infile, '#');
	infile>>m_NullDEM;
	RemoveCommentLine(infile, '#');
	//# DEM header path
	infile.getline(demhdr_path, _MAX_STRING_SIZE_);
	RemoveCommentLine(infile, '#');
	//# Ortho image path
	infile.getline(ortho_path, _MAX_STRING_SIZE_);
	
	infile.close();	

	return true;
}

bool CFrameOrthoImage::DataOpen()
{
	//To open source image
	if(FALSE == m_Source.Load(source_path)) return false;
	//To open DEM
	if(false == m_RefDEM.ReadDEM(dem_path, demhdr_path)) return false;

	//To create a empty ortho-photo
	int depth = m_Source.GetBitCount();
	if(FALSE == m_Ortho.Create(m_RefDEM.width, m_RefDEM.height,depth)) return false;
		
	return true;
}

//bool CFrameOrthoImage::MakeOrtho(CImage &oriImage, CString parampath, CString DEMPath, CString HDRPath, CString OrthoPath, bool bZBuf, bool bBilinear, double MaxDiffZ, double SInterval, bool angle_availability)
bool CFrameOrthoImage::MakeOrtho_LTM(char* inputpath)
{
	//To read input file
	if(false == ReadInputFile(inputpath)) return false;

	//To load image&DEM and create empty ortho-photo
	if(false == DataOpen()) return false;

	//min and max height of DEM
	MinZ = 99999.;
	MaxZ = -99999.;
	
	for(int i=0; i<(int)m_RefDEM.height; i++)
	{
		for(int j=0; j<(int)m_RefDEM.width; j++)
		{
			double z;
			if(false == m_RefDEM.GetZ(j,i,z)) continue;
			
			if(z == m_NullDEM) continue;

			if(z<MinZ) MinZ = z;
			if(z>MaxZ) MaxZ = z;
		}
	}

	//Start time
	DWORD dwStart = GetTickCount();

	//Pixel handlers
	if(m_Source.GetBitCount() == 8)
	{
		if(false == ResampleOrtho_Slope_DiffZ()) 
		{
			AfxMessageBox("Not successful.");
			return false;
		}
	}
	else if(m_Source.GetBitCount() == 24)
	{
		if(false == ColorResampleOrtho_Slope_DiffZ())
		{
			AfxMessageBox("Not successful.");
			return false;
		}
	}
	else
	{
		AfxMessageBox("This program supports only Gray and 24bit RGB color images.");
	}

	DWORD ElapsedTime = GetTickCount() - dwStart;
	CString msg;
	msg.Format("Elapsed Time: %d",ElapsedTime/1000);
	AfxMessageBox(msg);
	return true;
}

//bool CFrameOrthoImage::MakeOrtho(CImage &oriImage, CString parampath, CString DEMPath, CString HDRPath, CString OrthoPath, bool bZBuf, bool bBilinear, double MaxDiffZ, double SInterval, bool angle_availability)
bool CFrameOrthoImage::ResampleOrtho_Slope_DiffZ()
{
	CPixelPtr OriginPixel(m_Source);
	CPixelPtr OrthoPixel(m_Ortho);
	
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, m_RefDEM.height);
	
	DWORD W, H;
	W = (DWORD)m_Source.GetWidth();
	H = (DWORD)m_Source.GetHeight();

	CPixel p;

	//To initialize ortho-photo using BACKGROUND
	for(DWORD j=0 ; j<(DWORD)m_Ortho.GetHeight() ; j++) 
	{
		for(DWORD i=0 ; i<(DWORD)m_Ortho.GetWidth(); i++)
		{
			p = BACKGROUND_BW;
			p >> OrthoPixel[LONG(j)][LONG(i)];
		}
	}

	double DiffZ = MaxZ - MinZ;

	for(DWORD i=0;i<m_RefDEM.height;i++)
	{
		for(DWORD j=0;j<m_RefDEM.width;j++)
		{
			double X, Y, Z;
			Point2D<double> a;
			
			m_RefDEM.GetXYZ(j,i,X,Y,Z);

			/**************************************************************/
			GroundCoord2SceneCoord(X, Y, Z, a);
			a.x = double(int(a.x+0.5)); a.y = double(int(a.y+0.5));
			/**************************************************************/

			//Nearest Neighborhood
			if(ResamplingTag == 0)		
			{
				if(((a.x >= 0) && (a.y >=0)) && ((a.x <= W-1) && (a.y <= H-1)))
				{
					if(Z == m_NullDEM)
					{
						p = _NULLDEM_BW;
						p >> OrthoPixel[LONG(i)][LONG(j)];
					}
					//Method 0: simple digital differential rectification
					else if(MethodTag == (unsigned int)0)
					{
						p << OriginPixel[LONG(a.y)][LONG(a.x)];
						p >> OrthoPixel[LONG(i)][LONG(j)];
					}
					//Method 1: Slope method (using difference of height)
					else if(MethodTag == (unsigned int)1)
					{
						//To remove occlusion
						if(false == CheckOcclusion_DiffZ(Xo,Yo,Zo,j,i,DiffZ))
						{
							p = _OCCLUSION_BW;
							p >> OrthoPixel[LONG(i)][LONG(j)];
						}
						else
						{
							p << OriginPixel[LONG(a.y)][LONG(a.x)];
							p >> OrthoPixel[LONG(i)][LONG(j)];
						}
						
					}
					//Method 2: Slope method (Angle based method)
					else if(MethodTag == (unsigned int)2)
					{
						//To remove occlusion
						if(false == CheckOcclusion_Angle(Xo,Yo,Zo,j,i,DiffZ))
						{
							p = _OCCLUSION_BW;
							p >> OrthoPixel[LONG(i)][LONG(j)];
						}
						else
						{
							p << OriginPixel[LONG(a.y)][LONG(a.x)];
							p >> OrthoPixel[LONG(i)][LONG(j)];
						}
						
					}
					//Method 0: simple digital differential rectification
					else
					{
						p << OriginPixel[LONG(a.y)][LONG(a.x)];
						p >> OrthoPixel[LONG(i)][LONG(j)];
					}
					
				}
			}
			//Bilinear interpolation
			else if(ResamplingTag == 1)		
			{
				if(((a.x >= 1) && (a.y >= 1)) && ((a.x <= W-2) && (a.y <= H-2)))
				{
					if(Z == m_NullDEM)
					{
						p = _NULLDEM_BW;
						p >> OrthoPixel[LONG(i)][LONG(j)];
					}
					//Method 0: simple digital differential rectification
					else if(MethodTag == (unsigned int)0)
					{
						CPixel value1, value2, value3, value4;
						value1 << OriginPixel[LONG(a.y)][LONG(a.x)];
						value2 << OriginPixel[LONG(a.y)][LONG(a.x+1)];
						value3 << OriginPixel[LONG(a.y+1)][LONG(a.x)];
						value4 << OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
						
						double A, B, C, D;
						A = (double)((int)a.x + 1) - a.x;
						B = 1 - A;
						C = (double)((int)a.y + 1) - a.y;
						D = 1 - C;
						
						CPixel value; value = value1*A*C + value2*B*C + value3*A*D + value4*B*D;
						value >> OrthoPixel[LONG(i)][LONG(j)];
					}
					//Method 1: Slope method (using difference of height)
					else if(MethodTag == (unsigned int)1)
					{
						//To remove occlusion
						if(false == CheckOcclusion_DiffZ(Xo,Yo,Zo,j,i,DiffZ))
						{
							p = _OCCLUSION_BW;
							p >> OrthoPixel[LONG(i)][LONG(j)];
						}
						else
						{
							CPixel value1, value2, value3, value4;
							value1 << OriginPixel[LONG(a.y)][LONG(a.x)];
							value2 << OriginPixel[LONG(a.y)][LONG(a.x+1)];
							value3 << OriginPixel[LONG(a.y+1)][LONG(a.x)];
							value4 << OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							CPixel value; value = value1*A*C + value2*B*C + value3*A*D + value4*B*D;
							value >> OrthoPixel[LONG(i)][LONG(j)];
						}
						
					}
					//Method 2: Slope method (Angle based method)
					else if(MethodTag == (unsigned int)2)
					{
						//To remove occlusion
						if(false == CheckOcclusion_Angle(Xo,Yo,Zo,j,i,DiffZ))
						{
							p = _OCCLUSION_BW;
							p >> OrthoPixel[LONG(i)][LONG(j)];
						}
						else
						{
							CPixel value1, value2, value3, value4;
							value1 << OriginPixel[LONG(a.y)][LONG(a.x)];
							value2 << OriginPixel[LONG(a.y)][LONG(a.x+1)];
							value3 << OriginPixel[LONG(a.y+1)][LONG(a.x)];
							value4 << OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							CPixel value; value = value1*A*C + value2*B*C + value3*A*D + value4*B*D;
							value >> OrthoPixel[LONG(i)][LONG(j)];
						}
						
					}
					//Method 0: simple digital differential rectification
					else
					{
						CPixel value1, value2, value3, value4;
						value1 << OriginPixel[LONG(a.y)][LONG(a.x)];
						value2 << OriginPixel[LONG(a.y)][LONG(a.x+1)];
						value3 << OriginPixel[LONG(a.y+1)][LONG(a.x)];
						value4 << OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
						
						double A, B, C, D;
						A = (double)((int)a.x + 1) - a.x;
						B = 1 - A;
						C = (double)((int)a.y + 1) - a.y;
						D = 1 - C;
						
						CPixel value; value = value1*A*C + value2*B*C + value3*A*D + value4*B*D;
						value >> OrthoPixel[LONG(i)][LONG(j)];
					}
					
				}
			}
		}

		bar.StepIt();
	}

	if(FALSE == m_Ortho.Save(ortho_path)) return false;

	AfxMessageBox("Resampling Finished!");
	
	return true;
}

bool CFrameOrthoImage::ColorResampleOrtho_Slope_DiffZ()
{
	CColorPixelPtr OriginPixel(m_Source);
	CColorPixelPtr OrthoPixel(m_Ortho);
	
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, m_RefDEM.height);
	
	DWORD W, H;
	W = (DWORD)m_Source.GetWidth();
	H = (DWORD)m_Source.GetHeight();

	CColorPixel p;
	CColorPixel p_BG(BACKGROUND_BW,BACKGROUND_BW,BACKGROUND_BW);
	CColorPixel p_NULL(_NULLDEM_BW,_NULLDEM_BW,_NULLDEM_BW);
	CColorPixel p_OCCL(_OCCLUSION_BW,_OCCLUSION_BW,_OCCLUSION_BW);

	//To initialize ortho-photo using BACKGROUND
	for(DWORD j=0 ; j<(DWORD)m_Ortho.GetHeight() ; j++) 
	{
		for(DWORD i=0 ; i<(DWORD)m_Ortho.GetWidth(); i++)
		{
			p = p_BG;
			p >> OrthoPixel[LONG(j)][LONG(i)];
		}
	}

	double DiffZ = MaxZ - MinZ;

	for(DWORD i=0;i<m_RefDEM.height;i++)
	{
		for(DWORD j=0;j<m_RefDEM.width;j++)
		{
			double X, Y, Z;
			Point2D<double> a;
			
			m_RefDEM.GetXYZ(j,i,X,Y,Z);

			/**************************************************************/
			GroundCoord2SceneCoord(X, Y, Z, a);
			a.x = double(int(a.x+0.5)); a.y = double(int(a.y+0.5));
			/**************************************************************/

			//Nearest Neighborhood
			if(ResamplingTag == 0)		
			{
				if(((a.x >= 0) && (a.y >=0)) && ((a.x <= W-1) && (a.y <= H-1)))
				{
					if(Z == m_NullDEM)
					{
						p = p_NULL;
						p >> OrthoPixel[LONG(i)][LONG(j)];
					}
					//Method 1: Slope method (using difference of height)
					else if(MethodTag == (unsigned int)1)
					{
						//To remove occlusion
						if(false == CheckOcclusion_DiffZ(Xo,Yo,Zo,j,i,DiffZ))
						{
							p = p_OCCL;
							p >> OrthoPixel[LONG(i)][LONG(j)];
						}
						else
						{
							p << OriginPixel[LONG(a.y)][LONG(a.x)];
							p >> OrthoPixel[LONG(i)][LONG(j)];
						}
						
					}
					//Method 1: simple digital differential rectification
					else
					{
						p << OriginPixel[LONG(a.y)][LONG(a.x)];
						p >> OrthoPixel[LONG(i)][LONG(j)];
					}
					
				}
			}
			//Bilinear interpolation
			else if(ResamplingTag == 1)		
			{
				if(((a.x >= 1) && (a.y >= 1)) && ((a.x <= W-2) && (a.y <= H-2)))
				{
					if(Z == m_NullDEM)
					{
						p = p_NULL;
						p >> OrthoPixel[LONG(i)][LONG(j)];
					}
					//Method 1: Slope method (using difference of height)
					else if(MethodTag == (unsigned int)1)
					{
						//To remove occlusion
						if(false == CheckOcclusion_DiffZ(Xo,Yo,Zo,j,i,DiffZ))
						{
							p = p_OCCL;
							p >> OrthoPixel[LONG(i)][LONG(j)];
						}
						else
						{
							CColorPixel value1, value2, value3, value4;
							value1 << OriginPixel[LONG(a.y)][LONG(a.x)];
							value2 << OriginPixel[LONG(a.y)][LONG(a.x+1)];
							value3 << OriginPixel[LONG(a.y+1)][LONG(a.x)];
							value4 << OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							CColorPixel value; value = value1*A*C + value2*B*C + value3*A*D + value4*B*D;
							value >> OrthoPixel[LONG(i)][LONG(j)];
						}
						
					}
					//Method 1: simple digital differential rectification
					else
					{
						CColorPixel value1, value2, value3, value4;
						value1 << OriginPixel[LONG(a.y)][LONG(a.x)];
						value2 << OriginPixel[LONG(a.y)][LONG(a.x+1)];
						value3 << OriginPixel[LONG(a.y+1)][LONG(a.x)];
						value4 << OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
						
						double A, B, C, D;
						A = (double)((int)a.x + 1) - a.x;
						B = 1 - A;
						C = (double)((int)a.y + 1) - a.y;
						D = 1 - C;
						
						CColorPixel value; value = value1*A*C + value2*B*C + value3*A*D + value4*B*D;
						value >> OrthoPixel[LONG(i)][LONG(j)];
					}
					
				}
			}
		}

		bar.StepIt();
	}

	if(FALSE == m_Ortho.Save(ortho_path)) return false;

	AfxMessageBox("Resampling Finished!");
	
	return true;
}

bool CFrameOrthoImage::CheckOcclusion_DiffZ(double Xl, double Yl, double Zl, int indexX, int indexY, double MAXDIFFHEIGHT)
{
	//Ground point (DEM point)
	double Xa, Ya, Za; 
	m_RefDEM.GetXYZ(indexX, indexY,Xa,Ya,Za);
	
	int nX, nY;
	nX = indexX + int((Xl - Xa)/m_RefDEM.offset + 0.5);
	nY = indexY + int((Yl - Ya)/m_RefDEM.offset + 0.5);

	double a, b;
	//Y = a*X + b
	double diffX = ((double)indexX - (double)nX);
	if(fabs(diffX) < 1.0e-99) {a = 1.0e99;}//a�� ���밪�� ���Ѵ뿡 ������ ��ȣ�� �ǹ̰� ����.
	else
	{
		a = ((double)indexY - (double)nY)/diffX;
		//DEM Grid�� ���ȣ�� ������ Y��ǥ�� ������ ���� �ݴ��̴�.
		a = -a;
	}
	
	b = (double)indexY - a*(double)indexX;
	
	//Dist_X is longer than dist_Y;
	if(abs(indexY - nY)<abs(indexX - nX))
	{
		double limit_Z = (Zl - Za)/(double)(abs(indexX - nX));
		
		int ix, iy;
		int interval;
		int offset;
		
		if(nX>indexX) offset = +1;
		else offset = -1;
		
		interval = 0;
		
		do
		{
			//To increase the interval
			interval += offset;
			ix = indexX + interval;
			
			//Y = a*X + b
			iy = int(a*ix + b + 0.5);
			
			//check the boundary
			double Z;
			if(false == m_RefDEM.GetZ(ix, iy, Z)) break;
			if(Z == m_NullDEM) continue;

			double dZ = Z - Za;
			double maxdZ = fabs(limit_Z*interval);

			if(maxdZ>MAXDIFFHEIGHT) break;
			if(Z>MaxZ) break;
			if((dZ)>maxdZ) return false;
			if((Za+maxdZ)>MaxZ) break;
		} while(1);
	}
	else//Dist_Y is longer than Dist_X;
	{
		double limit_Z = fabs((Zl - Za)/(indexY - nY));
		
		int ix, iy;
		int interval;
		int offset;
		
		//DEM Grid�� ���ȣ�� ������ Y��ǥ�� ������ ���� �ݴ��̴�.
		if(nY>indexY) offset = -1;
		else offset = +1;
		
		interval = 0;
		
		do
		{
			//To increase the interval
			interval += offset;
			iy = indexY + interval;
			
			//Y = a*X + b
			ix = int((iy-b)/a + 0.5);
			
			double Z;
			if(false == m_RefDEM.GetZ(ix, iy, Z)) break;
			if(Z == m_NullDEM) continue;

			double dZ = Z - Za;
			double maxdZ = fabs(limit_Z*interval);

			if(maxdZ>MAXDIFFHEIGHT) break;
			if(Z>MaxZ) break;
			if((dZ)>maxdZ) return false;
			if((Za+maxdZ)>MaxZ) break;
		} while(1);
	}
	
	return true;
}

bool CFrameOrthoImage::CheckOcclusion_Angle(double Xl, double Yl, double Zl, int indexX, int indexY, double MAXDIFFHEIGHT)
{
	//Ground point (DEM point)
	double Xa, Ya, Za; 
	m_RefDEM.GetXYZ(indexX, indexY,Xa,Ya,Za);
	
	int nX, nY;
	nX = indexX + int((Xl - Xa)/m_RefDEM.offset + 0.5);
	nY = indexY + int((Yl - Ya)/m_RefDEM.offset + 0.5);
	
	double a, b;
	//Y = a*X + b
	double diffX = ((double)indexX - (double)nX);
	if(fabs(diffX) < 1.0e-99) {a = 1.0e99;}//a�� ���밪�� ���Ѵ뿡 ������ ��ȣ�� �ǹ̰� ����.
	else
	{
		a = ((double)indexY - (double)nY)/diffX;
		//DEM Grid�� ���ȣ�� ������ Y��ǥ�� ������ ���� �ݴ��̴�.
		a = -a;
	}

	b = (double)indexY - a*(double)indexX;
	
	//Dist_X is longer than dist_Y;
	if(abs(indexY - nY)<abs(indexX - nX))
	{
		double limit_Z = (Zl - Za)/(double)(abs(indexX - nX));
		double limit_Angle = atan((Zl-Za)/sqrt((Xl-Xa)*(Xl-Xa)+(Yl-Ya)*(Yl-Ya)));
		
		int ix, iy;
		int interval;
		int offset;
		
		if(nX>indexX) offset = +1;
		else offset = -1;
		
		interval = 0;
		
		do
		{
			interval += offset;
			ix = indexX + interval;
			
			//Y = a*X + b
			iy = int(a*ix + b + 0.5);
			
			double X,Y,Z;
			if(false == m_RefDEM.GetXYZ(ix, iy, X, Y, Z))break;

			double angle = atan((Zl-Z)/sqrt((Xl-X)*(Xl-X)+(Yl-Y)*(Yl-Y)));
			
			double maxdZ = fabs(limit_Z*interval);
			
			if(angle<limit_Angle) return false;

			if(Z>MaxZ) break;
			if((Za+maxdZ)>MaxZ) break;
		} while(1);
	}
	else//Dist_Y is longer than Dist_X;
	{
		double limit_Z = fabs((Zl - Za)/(indexY - nY));
		double limit_Angle = atan((Zl-Za)/sqrt((Xl-Xa)*(Xl-Xa)+(Yl-Ya)*(Yl-Ya)));
		
		int ix, iy;
		int interval;
		int offset;
		
		//DEM Grid�� ���ȣ�� ������ Y��ǥ�� ������ ���� �ݴ��̴�.
		if(nY>indexY) offset = -1;
		else offset = +1;
		
		interval = 0;
		
		do
		{
			interval += offset;
			iy = indexY + interval;
			
			//Y = a*X + b
			ix = int((iy-b)/a + 0.5);
			
			double X, Y,Z;
			if(false == m_RefDEM.GetXYZ(ix, iy, X, Y, Z))break;

			double angle = atan((Zl-Z)/sqrt((Xl-X)*(Xl-X)+(Yl-Y)*(Yl-Y)));

			double maxdZ = fabs(limit_Z*interval);

			if(angle<limit_Angle) return false;

			if(Z>MaxZ) break;
			if((Za+maxdZ)>MaxZ) break;
		} while(1);
	}
	
	return true;
}