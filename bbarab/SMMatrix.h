/*
 * Copyright (c) 2001-2001, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include "SpaceMaticsStr.h"
#include "SMMatrixClass.h"

namespace SMATICS_MATRIX
{
	/**get elements of matrix as a string format*/
	template<typename T>
	CSMStr matrixout(const CSMMatrix<T> &mat, int outoption = 0) //메트릭스 출력
	{
		CSMStr out, matrix;
		out = "";
		for (int i = 0;i < (int)(mat.GetRows());i++)
		{
			for (int j = 0;j < (int)(mat.GetCols());j++)
			{
				switch (outoption)
				{
				case 0:
					matrix.Format("[%d,%d]%20le\t", i, j, mat(i, j));
					break;
				case 1:
					matrix.Format("[%d,%d]%20lf\t", i, j, mat(i, j));
					break;
				default:
					matrix.Format("[%d,%d]%20le\t", i, j, mat(i, j));
					break;
				}
				out += matrix;
			}
			out += "\r\n";
		}

		return out;
	};

	template<typename T>
	/**check the empty element of a matrix*/
	CSMStr CheckEmptyElem(const CSMMatrix<T> &mat) //메트릭스 출력
	{
		CSMStr out;
		out = "";
		for (unsigned int r = 0; r < mat.GetRows(); r++)
		{
			for (unsigned int c = 0; c < mat.GetCols(); c++)
			{
				if (mat(r, c) == 0) out += "O";
				else out += "A";
			}
			out += "\r\n";
		}

		return out;
	};


}//namespace SMATICS_BBARAB