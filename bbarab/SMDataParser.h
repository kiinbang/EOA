// SMDataParser.h: interface for the CSMDataParser class.
//
//////////////////////////////////////////////////////////////////////

/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/

#if !defined(__BBARAB_Matlab_DATA_PARSER_CLASS__)
#define __BBARAB_Matlab_DATA_PARSER_CLASS__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMList.h"
#include "SMCoordinateClass.h"
#include "UtilityGrocery.h"

#define NUM_NORMALIZE_COEFF 10

class _ICP_;
class _ICL_;
class _GCL_;
class _GCP_;
class _SCENE_;
class _ParallelPPP_;

/**
*@class CSMDataParser
*@brief This class give us the function that parse the text format data for Matlab, and other type data.
*/
class CSMDataParser  
{
public:
	CSMDataParser();
	CSMDataParser(CSMDataParser &copy);
	virtual ~CSMDataParser();

	void operator=(CSMDataParser &copy);

	/*********************************************************************************/
	/*********************************Matlab DAT PARSER FUNC****************************/
	/*********************************************************************************/

	/** ParserforMatlabICPFile<br>
	* Parse the image control points file(Matlab format).
    * @param fname ICP file path name.
    * @param bDataString bool variable for checking the saving data.
	*IF bDataString is true, return the contents of file in CString format.
	*/
	void ParserforMatlabICPFile(const CString fname, bool bDataString=true);

	/** ParserforMatlabICLFile<br>
	* Parse the line data in the image (Matlab format).
    * @param fname ICL file path name.
    * @param bDataString bool variable for checking the saving data.
	*IF bDataString is true, return the contents of file in CString format.
	*/
	void ParserforMatlabICLFile(const CString fname, bool bDataString=true);

	/** ParserforMatlabGCPFile<br>
	* Parse the GCP data in the Ground (Matlab format).
    * @param fname GCP file path name.
    * @param bDataString bool variable for checking the saving data.
	*IF bDataString is true, return the contents of file in CString format.
	*/
	void ParserforMatlabGCPFile(const CString fname, bool bDataString=true);

	/** ParserforMatlabGCLFile<br>
	* Parse the Ground Control Line data (Matlab format).
    * @param fname GCL file path name.
    * @param bDataString bool variable for checking the saving data.
	*IF bDataString is true, return the contents of file in CString format.
	*/
	void ParserforMatlabGCLFile(const CString fname, bool bDataString=true);

	/*********************************************************************************/
	/****************************END OF Matlab DAT PARSER FUNC**************************/
	/*********************************************************************************/

	/** DataNoramlization<br>
	* static member function<br>
	* GCP and MatlabGCL Data normalization function(MatlabData) (-1.0 ~ 1.0).
    */
	static void DataNormalization_GCP_MatlabGCL(CSMList<_GCP_> GCP_List, CSMList<_GCL_> GCL_List, double coeff[6]);
	/** DataNoramlization<br>
	* static member function<br>
	* ICP and ICL Data normalization function(MatlabData) (-1.0 ~ 1.0).
    */
	static void DataNormalization_ICP_ICL(CSMList<_ICP_> ICP_List, CSMList<_ICL_> ICL_List, double coeff[4]);

	/** Init.
	* Initialization of module.
	*/
	void Init()
	{
		ICP_List.RemoveAll(); 
		GCP_List.RemoveAll();
		ICL_List.RemoveAll(); 
		GCL_List.RemoveAll();
		SCENE_List.RemoveAll();
	}

public:
	//property
	CString m_FileContents;/**<Saving the file contents.*/
	CSMList<_ICP_> ICP_List; /**<ICP Data List manager. */
	CSMList<_ICL_> ICL_List; /**<ICL Data List manager. */
	CSMList<_GCP_> GCP_List; /**<GCP Data List manager. */	
	CSMList<_GCL_> GCL_List; /**<GCL Data List manager. */	
	CSMList<_SCENE_> SCENE_List; /**<scene data list. */	

	double coeff[NUM_NORMALIZE_COEFF]; /**<SHIFT_X[0], SCALE_X[1], SHIFT_Y[2], SCALE_Y[3], SHIFT_Z[4], SCALE_Z[5], SHIFT_col[6], SCALE_col[7], SHIFT_row[8], SCALE_row[9] */
};

/** 
*@class _ICL_
*@brief data structure for ICL(intermediate point in Control line). 
*/
class _ICL_
{
public:
	double row; /**<row*/
	double col; /**<col*/
	double s[4]; /**<sigma: s(1,1)s(1,2)s(2,1)s(2,2)*/
	CString flag; /**<Point flag: A&B(ending point), C(intermediate point)*/
	CString SID; /**<Scene ID*/
	CString LID; /**< Line ID*/

	_ICL_() {s[0]=s[1]=s[2]=s[3]=1.;}
};

/** 
*@class _ICP_
*@brief data structure for ICP(Image Control Point). 
*/
class _ICP_
{
public:
	double row; /**<row*/
	double col; /**<col*/
	double s[4]; /**<sigma: s(1,1)s(1,2)s(2,1)s(2,2)*/
	CString SID; /**<Scene ID*/
	CString PID;/**< Point ID*/
	_ICP_() {}
	_ICP_(_ICL_ IL){row=IL.row;col=IL.col; for(int i=0;i<4;i++)s[i]=IL.s[i]; PID=(IL.LID+IL.flag);}
};

/** 
*@class _GCL_ 
*@brief data structure for GCL(Ground Control Line).
*/
class _GCL_
{
public:	
	double X, Y, Z; /**<Ground point coord*/
	double s[9]; /**<sigma: s(1,1)s(1,2)s(1,3)s(2,1)s(2,2)s(2,3)s(3,1)s(3,2)s(3,3)*/
	CString LID; /**< Line ID*/
	CString flag; /**<flag*/
};

/** 
*@class _GCP_ 
*@brief data structure for GCP(Ground Control Point).
*/
class _GCP_
{
public:
	double X; /**<X*/
	double Y; /**<Y*/
	double Z; /**<Z*/
	double s[9]; /**<sigma: s(1,1)s(1,2)s(1,3)s(2,1)s(2,2)s(2,3)s(3,1)s(3,2)s(3,3)*/
	CString PID; /**< Point ID*/
	_GCP_() {}
	_GCP_(Point3D<double> G) {X=G.x;Y=G.y;Z=G.z;}
	void _COPY_(_GCL_ GL){X=GL.X; Y=GL.Y; Z=GL.Z; for(int i=0;i<9;i++)s[i]=GL.s[i]; PID=(GL.LID+GL.flag);}
};

/** 
*@class _SCENE_. 
*@brief data structure for Scene.
*/
class _SCENE_
{
public:	
	//property
	CSMList<_ICP_> ICP_List; /**<ICP Data List manager. */
	CSMList<_ICL_> ICL_List; /**<ICL Data List manager. */
	
	CString SID; /**<scene id*/
};

#endif // !defined(__BBARAB_Matlab_DATA_PARSER_CLASS__)
