/*
 * Copyright (c) 2001-2015, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

//max KD tree points number
#define KD_MAX_POINTS 100000000

#define SMALL_NUM 0.001

#include <vector>

inline double distance2(double* x1, double* x2, int dim)
{
	double S = 0;
	for(int k=0; k < dim; k++)
		S+= (x1[k]-x2[k])*(x1[k]-x2[k]);
	return S;
}

inline bool IsEqual(double* x1, double* x2, int dim)
{
	for(int k=0; k < dim; k++)
	{
		if(x1[k] != x2[k])
			return false;
	}
	
	return true ;
}

//KDTreeNode template class implementation

class KDNode
{
	friend class KDTree;

	//member functions
public:
	double *Contents;

protected:
	int axis ;
	int ID ;

	int UID;
	double *x;
	
	bool checked ;
	bool orientation ;
	int sd;
	int num_contents;

	KDNode* Parent ;
	KDNode* Left ;
	KDNode* Right ;

public:

	double* Get_x_Handle()
	{
		return x;
	}

	double Get_x_(int i)
	{
		if(i >= sd)
			return -99999.99999;
		else
			return x[i];
	}

	bool Set_x_(int i, double val)
	{
		if(i >= sd)
			return false;
		else
		{
			x[i] = val;
			return true;
		}
	}

	int GetAxis() {return axis;}

	void SetID(int val, int uid) 
	{ 
		UID = uid;
		ID = val; 
	} 
	
	int GetID() 
	{
		return ID;
	} 

	int GetUID() 
	{
		return UID;
	} 

	KDNode& operator = (KDNode &copy)
	{
		copy_node(copy);
		return *this;
	}

	void copy_node(KDNode &copy)
	{
		UID = copy.UID;
		axis = copy.axis;
		ID = copy.ID;
		checked = copy.checked;
		orientation = copy.orientation;
		sd = copy.sd;
		Parent = copy.Parent;
		Left = copy.Left;
		Right = copy.Right;
		num_contents = copy.num_contents;

		if( NULL != x)
		{
			delete[] x;
			x = NULL;
		}

		x = new double[sd];
		
		for( int i=0; i<(int)sd; i++ )
		{
			x[i] = copy.x[i];
		}

		if( NULL != Contents)
		{
			delete[] Contents;
			Contents = NULL;
		}
		
		if(num_contents > 0)
		{	
			Contents = new double[num_contents];
			
			for(int i=0; i<(int)num_contents; i++ )
			{
				Contents[i] = copy.Contents[i];
			}
		}
	}

	KDNode()
	{
		x = NULL;
		Contents = NULL;

		Parent = Left = Right = NULL;
	}

	KDNode(KDNode &copy)
	{
		x = NULL;
		Contents = NULL;

		Parent = Left = Right = NULL;

		copy_node(copy);
	}

	KDNode(double* x0, int axis0, unsigned int SD, unsigned int SD2 = 0, double* val_cont = NULL)
	{
		if(SD < 1)
		{
			x = NULL;
			//::MessageBox(NULL, "SD < 1", "Worning", MB_OK);
			return;
		}
		else
		{
			sd = SD;
			num_contents = SD2;

			x = new double[SD];
			axis = axis0;
			for(int k=0; k<(int)SD; k++)
				x[k] = x0[k];
			
			Left = Right = Parent = NULL ;

			checked = false ;
			ID = 0;

			if(SD2 > 0)
			{
				Contents = new double[num_contents];
				for(int k=0; k<(int)num_contents; k++)
					Contents[k] = val_cont[k];
			}
			else
				Contents = NULL;
		}
	}

	virtual ~KDNode()
	{
		if(x != NULL) delete[] x;
		if(Contents != NULL) delete[] Contents;
	}
	
	inline KDNode*	Insert(double* p, unsigned int SD, bool bChangeEqualData, unsigned int SD2 = 0, double* val_cont = NULL)
	{
		KDNode* parent = FindParent(p);

		if(bChangeEqualData == false)
		{
			if(IsEqual(p, parent->x, SD))
				return NULL;
		}
		else
		{
			while(IsEqual(p, parent->x, SD) == true)
			{
				for(unsigned int i=0; i<SD; i++)
				{
					p[i] += SMALL_NUM;
				}
			}
		}

		KDNode* newNode = new KDNode(p, parent->axis +1 < (int)SD? parent->axis+1:0, SD, SD2, val_cont);
		newNode->Parent = parent ;
		
		if(p[parent->axis] > parent->x[parent->axis])
		{
			parent->Right = newNode ;
			newNode->orientation = 1 ;
		}
		else
		{
			parent->Left = newNode ;
			newNode->orientation = 0 ;
		}
		
		return newNode ;
	}

	inline KDNode*	FindParent(double* x0)
	{
		KDNode* parent ;
		parent = NULL;
		KDNode* next = this ;
		int split ;
		while(next)
		{
			split = next->axis  ;
			parent = next ;
			if(x0[split] > next->x[split])
				next = next->Right ;
			else
				next = next->Left ;
		}
		return parent ;
	}
};


class KDTree
{
public:
	
	KDTree()
	{
		x_min = NULL;
		x_max = NULL; 
		max_boundary = NULL; 
		min_boundary = NULL;

		//[Begin] Bang 2009.06.28
		//List = new KDNode*[KD_MAX_POINTS];
		List = NULL;
		//CheckedNodes = new KDNode*[KD_MAX_POINTS];
		CheckedNodes = NULL;
		//[End] Bang 2009.06.28

		bMemoryFull = false;
		
		pRoot = NULL;

		num_points_initial = 0;
		nList  = 0;
	}

	KDTree(int num_points, unsigned int sd, unsigned int sd2=0)
	{
		x_min = NULL;
		x_max = NULL; 
		max_boundary = NULL; 
		min_boundary = NULL;

		//[Begin] Bang 2009.06.28
		//List = new KDNode*[KD_MAX_POINTS];
		List = NULL;
		//CheckedNodes = new KDNode*[KD_MAX_POINTS];
		CheckedNodes = NULL;
		//[End] Bang 2009.06.28

		bMemoryFull = false;
		
		pRoot = NULL;

		num_points_initial = 0;
		nList  = 0;

		InitialSetup(num_points, sd, sd2);
	}

	inline void InitialSetup(int num_points, unsigned int sd, unsigned sd2, bool ChangeEqualData=false)
	{
		//[Begin] Bang 2010.02.10

		RemoveAll();

		List = new KDNode*[num_points];

		CheckedNodes = new KDNode*[num_points];

		num_points_initial = num_points;

		for(int i=0; i<num_points; i++)
		{
			List[i] = NULL;
			CheckedNodes[i] = NULL;
		}

		//[End] Bang 2010.02.10

		if(sd < 1)
		{
			sd = 1;
			//::MessageBox(NULL, "SD = 1", "Worning", MB_OK);
		}
		
		if(sd < 1) sd = 1;
		
		SD = sd;
		SD2 = sd2;
		pRoot = NULL ;
		KD_id = 0;
		nList = 0;

		bMemoryFull = false;
		
		x_min = new double[SD];
		x_max = new double[SD]; 
		
		max_boundary = new bool[SD];
		min_boundary = new bool[SD];	

		m_bChangeEqualData = ChangeEqualData;
	}
	
	virtual ~KDTree()
	{
		RemoveAll();
	}

	inline void RemoveAll()
	{
		//if(Root != NULL)
		//{
		//	delete Root;
		//	Root = NULL;
		//}

		if(List != NULL)
		{
			for(int i=0; i<nList; i++)
			{
				if(List[i] != NULL)
					delete List[i];
			}

			delete[] List;
			List = NULL;
		}

		if(CheckedNodes != NULL)
		{
			delete[] CheckedNodes;
			CheckedNodes = NULL;
		}
		
		nList  = 0;
		
		if(x_min != NULL) {delete[] x_min; x_min = NULL;}
		if(x_max != NULL) {delete[] x_max; x_max = NULL;}
		
		if(max_boundary != NULL) {delete[] max_boundary; max_boundary = NULL;}
		if(min_boundary != NULL) {delete[] min_boundary; min_boundary = NULL;}
		
		bMemoryFull = false;
	}
	
	inline bool add(double* x)
	{
		if(bMemoryFull == false)
		{
			if(nList >= num_points_initial)
			{
				//::MessageBox(NULL, "Check the maximum point numbers!", "Warnning", MB_OK);
				bMemoryFull = true;
				
				return false; //can't add more points
			}
		}
		else
		{
			return false;
		}
		
		if(!pRoot)
		{
			List[0] = new KDNode(x, 0, SD);
			pRoot =  List[0];
			pRoot->SetID(KD_id, KD_id);
			KD_id++;
			nList++;

			//Root =  new KDNode(x, 0, SD);
			//Root->SetID(KD_id, KD_id);
			//KD_id++;
			//List[nList++] = Root;
		}
		else
		{
			KDNode* pNode;
			if((pNode = pRoot->Insert( x, SD, m_bChangeEqualData)))
			{
				pNode->SetID(KD_id, KD_id);
				KD_id++;
				List[nList++] = pNode;
			}
			else
			{
				return false;
			}
		}
		
		return true ;
	}

	inline bool add(double* x, int uid)
	{
		if(bMemoryFull == false)
		{
			if(nList >= num_points_initial)
			{
				//::MessageBox(NULL, "Check the maximum point numbers!", "Warnning", MB_OK);
				bMemoryFull = true;
				
				return false; //can't add more points
			}
		}
		else
		{
			return false;
		}
		
		if(!pRoot)
		{
			List[0] = new KDNode(x, 0, SD);
			pRoot =  List[0];
			pRoot->SetID(KD_id, uid);
			KD_id++;
			nList++;

			//Root =  new KDNode(x, 0, SD);
			//Root->SetID(KD_id, uid);
			//KD_id++;
			//List[nList++] = Root;
		}
		else
		{
			KDNode* pNode ;
			if((pNode = pRoot->Insert( x, SD, m_bChangeEqualData)))
			{
				pNode->SetID(KD_id, uid);
				KD_id++;
				List[nList++] = pNode;
			}
			else
			{
				return false;
			}
		}
		
		return true ;
	}

	inline bool add(double* x, double* contents, int uid)
	{
		if(bMemoryFull == false)
		{
			if(nList >= num_points_initial)
			{
				//::MessageBox(NULL, "Check the maximum point numbers!", "Warnning", MB_OK);
				bMemoryFull = true;
				
				return false; //can't add more points
			}
		}
		else
		{
			return false;
		}
		
		if(!pRoot)
		{
			List[0] = new KDNode(x, 0, SD, SD2, contents);
			pRoot =  List[0];
			pRoot->SetID(KD_id, uid);
			KD_id++;
			nList++;

			//Root =  new KDNode(x, 0, SD, SD2, contents);
			//Root->SetID(KD_id, uid);
			//KD_id++;
			//List[nList++] = Root;
		}
		else
		{
			KDNode* pNode ;
			if((pNode = pRoot->Insert( x, SD, m_bChangeEqualData, SD2, contents)))
			{
				pNode->SetID(KD_id, uid);
				KD_id++;
				List[nList++] = pNode;
			}
			else
			{
				return false;
			}
		}
		
		return true ;
	}
	
	inline KDNode* find_nearest(double* x)
	{
		if(!pRoot)
			return NULL ;
		
		checked_nodes = 0;
		
		KDNode* parent = pRoot->FindParent(x);
		nearest_neighbour = parent ;
		d_min = distance2(x, parent->x, SD); ;
		
		if(IsEqual(x, parent->x, SD))
			return nearest_neighbour ;
		
		search_parent(parent, x);
		uncheck();
		
		return nearest_neighbour;
	}

	inline int search(double *low, const double *high, KDNode *pParent, int level, std::vector<KDNode> &SelectedList)
	{
		if(pParent == NULL) return 0;

		int comparison = 1;

		bool bSuitable =  true;

		for(int i=0; i<(int)SD; i++)
		{
			if( pParent->x[i] < low[i] || high[i] < pParent->x[i] )
			{
				bSuitable = false;
				break;
			}
		}
		
		if(bSuitable)
		{
			KDNode node = *pParent;
			SelectedList.AddTail(node);
		}
		
		if( low[level] <= pParent->x[level] )
		{
			if ( NULL != pParent->Left )
				comparison += search(low, high, pParent->Left, (level+1)%SD, SelectedList);
		}

		if( pParent->x[level] <= high[level] )
		{
			if ( NULL != pParent->Right )
				comparison += search(low, high, pParent->Right, (level+1)%SD, SelectedList);
		}
		
		return comparison;
	}

	inline int search(double *low, const double *high, KDNode *pParent, int level, std::vector<int> &SelectedList)
	{
		if(pParent == NULL) return 0;

		KDNode *Last_pParent = pParent;
		
		bool bLoof1, bLoof2;
		
		int comparison = 0;
		
		do 
		{
			comparison ++;
			
			bool bSuitable =  true;
			
			for(int i=0; i<(int)SD; i++)
			{
				if( pParent->x[i] < low[i] || high[i] < pParent->x[i] )
				{
					bSuitable = false;
					break;
				}
			}
			
			if(bSuitable)
			{
				KDNode node = *pParent;
				SelectedList.push_back(node.GetID());
			}
			
			bLoof1 = false;
			bLoof2 = false;

			//Left
			if( low[level] <= pParent->x[level])
			{
				if ( NULL != pParent->Left )
				{
					pParent = pParent->Left;
					level = (level+1)%SD;
					bLoof1 = true;
					continue;
				}
			}
			
			Last_pParent = GetLastAvailableParent(pParent);
			
			//Right
			if( pParent->x[level] <= high[level] )
			{
				if ( NULL != pParent->Right )
				{
					pParent = pParent->Right;
					level = (level+1)%SD;
					bLoof2 = true;
					continue;
				}
			}

			if(bLoof1 == false && bLoof2 == false)
			{
				pParent = Last_pParent;

				if(pParent == NULL)
					break;

				level = pParent->GetAxis();
			}
			
		} while (1);
		
		return comparison;
	}

	inline KDNode* GetLastAvailableParent(KDNode* node)
	{
		KDNode *pParent;

		do 
		{
			if(node->Parent->Left == node && node->Parent->Right != NULL)
			{
				return node->Parent->Right;
			}
			else
			{
				node = node->Parent;
				if(node->Parent == NULL)
					return NULL;
			}

		} while (1);

		return pParent;
	}

	//Function to search all nodes
	inline int search_bad(double *low, const double *high, CSMList<KDNode> &SelectedList)
	{
		int count=0;

		for( int i=0; i<nList; i++)
		{
			KDNode *node = GetAt(i);
			
			bool bSuitable =  true;

			//20090701
			for(int s=0; s<(int)SD; s++)
			{
				if( node->x[s] < low[s] || high[s] < node->x[s] )
				{
					bSuitable = false;
					break;
				}
			}

			if(bSuitable == true)
			{
				SelectedList.AddTail(*node);
				count++;
			}
		}

		return count;
	}
	
	//sequential nearest neighbor search
	inline KDNode* find_nearest_brute(double* x)
	{
		if(!pRoot)
			return NULL;
		
		KDNode* nearest = pRoot ;
		double d ;
		d_min = distance2(pRoot->x, x, SD);
		for(int n=1; n<nList; n++)
		{
			d =  distance2(List[n]->x, x, SD);
			if(d < d_min)
			{
				nearest = List[n];
				d_min = d;
			}
		}
		
		return nearest ;
	}

	inline KDNode* GetAt(int n)
	{		
		if( n < 0 || n >= nList)
			return NULL;

		KDNode *node;

		node = List[n];
		return node;
	}
	
	inline	void check_subtree(KDNode* node, double* x)
	{
		if(!node || node->checked)
			return ;
		
		CheckedNodes[checked_nodes++] = node ;
		node->checked = true ;
		set_bounding_cube(node, x);
		
		int dim = node->GetAxis();
		double d= node->x[dim] - x[dim];
		
		if (d*d > d_min) {
			if (node->x[dim] > x[dim])
				check_subtree(node->Left, x);
			else 
				check_subtree(node->Right, x);
		}
		// If the distance from the key to the current value is 
		// less than the nearest distance, we still need to look
		// in both directions.
		else {
			check_subtree(node->Left, x);
			check_subtree(node->Right, x);
		}
	}
	
	inline  void set_bounding_cube(KDNode* node, double* x)
	{
		if(!node)
			return ;
		
		double d = (double)0;
		double dx;

		for(int k=0; k<(int)SD; k++)
		{
			dx = node->x[k]-x[k];
			if(dx > 0)
			{
				dx *= dx ;
				if(!max_boundary[k])
				{
					if(dx > x_max[k])
						x_max[k] = dx;
					if(x_max[k]>d_min)
					{
						max_boundary[k] =true;
						n_boundary++ ;
					}
				}
			}
			else 
			{
				dx *= dx ;
				if(!min_boundary[k])
				{
					if(dx > x_min[k])
						x_min[k] = dx;
					if(x_min[k]>d_min)
					{
						min_boundary[k] =true;
						n_boundary++ ;
					}
				}
			}
			d += dx;
			if(d>d_min)
				return ;
		}
		
		if(d<d_min)
		{
			d_min = d;
			nearest_neighbour = node ;
		}
	}
	
	inline KDNode* search_parent(KDNode* parent, double* x)
	{
		for(int k=0; k<(int)SD; k++)
		{
			x_min[k] = x_max[k] = 0;
			max_boundary[k] = min_boundary[k] = 0;
		}
		n_boundary = 0;
		
		KDNode* search_root = parent ;
		while(parent && n_boundary != 2*(int)SD)
		{	
			check_subtree(parent, x);
			search_root = parent ;
			parent = parent->Parent ;
		}
		
		return search_root ;
	}
	
	inline void uncheck()
	{
		for(int n=0; n<checked_nodes; n++)
			CheckedNodes[n]->checked = false;
	}
	
public:	

	unsigned int SD; //space dimension
	unsigned int SD2;

	KDNode*  pRoot ;
	double d_min ;
	KDNode* nearest_neighbour ;
	
	int KD_id  ;
	
	KDNode** List;
	int nList ;
	
	KDNode** CheckedNodes;
	int checked_nodes ;
	
	double *x_min, *x_max; 
	bool *max_boundary, *min_boundary;
	int n_boundary ;

	bool bMemoryFull;

	bool m_bChangeEqualData;

	int num_points_initial;
};