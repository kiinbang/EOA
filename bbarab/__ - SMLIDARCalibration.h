/*
 * Copyright (c) 2006-2007, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// SMLIDARCalibration.h: interface for the CSMLIDARCalibration class.
//
//////////////////////////////////////////////////////////////////////
//
//Written by Bang, Ki In
//
//Since 2006/06/28
//
//This file is newly generated from LIDARBiasCalibration.h
//
#if !defined(__SPATIAL_SPACE_MATICS_LIDAR_CALIBRATION__)
#define __SPATIAL_SPACE_MATICS_LIDAR_CALIBRATION__

#include "SMDataStruct.h"
#include "UtilityGrocery.h"
#include "SMLIDARConfig.h"
#include "Collinearity.h"
#include "LeastSquare.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMMatrix.h"
#include "KDTree.h"

namespace SMATICS_BBARAB
{

	class CTriangle
	{
	public:
		double vertex1[3], vertex2[3], vertex3[3];
		long ID;
	};

/**
* @class CSMLIDARCalibration
* @brief class for LIDAR system calibration\n
* @author Bang, Ki In
* @version 1.0
* @Since 2006-06-28
* @bug N/A.
* @warning N/A.
*
* \nThis is a class of LIDAR bias calibration program.
* <ul> 
* <li> boresight biases
*	<ol>
*	<li> Spatial(3) and rotational(3) biases in boresight
*	<li> ranging bias (1)
*	</ol>
* </ul>\n
*/
class CSMLIDARCalibration
{
public:

	CSMLIDARCalibration();
	virtual ~CSMLIDARCalibration();

private:
	/**< List of config info */
	CSMLIDARConfig Config;/**< config info */
	CSMLIDARConfig OriginConfig;/**< original config info */
	CSMList<CLIDARCalibrationPoint> PointList;/**<LIDAR calibration data */
	CSMList<CLIDARCalibrationPoint> Control_PointList;
	CSMList<CLIDARCalibrationPoint> All_ICP_PointList;
	CSMList<CLIDARCalibrationPoint> Selected_ICP_PointList;
	CSMList<CLIDARCalibrationPoint> All_ICPatch_PointList;
	CSMList<CLIDARCalibrationPoint> Selected_ICPatch_PointList;
	CSMList<CLIDARCalibrationPoint> OriginalPointList;
	bool bICPoint; /**<ICPoint method availability*/
	bool bICPatch; /**<ICPatch method availability*/
	bool bControlPoint; /**<Control Points Availability*/
	CString stPrj_Path;
	double prj_ver_num;

public:
	/**CalibrationWithPlaneConstraint
	* Description	    : To run a LIDAR calibration process using plane constraints
	*@param CString cfgfilepath:	: LIDAR configuration data
	*@param CString patchpath	: LIDAR raw measurement data
	*@param CString fileinfopath	: *.patch contents information file path
	*@param CString resultfile	: result file path
	*@param double precision	: precision of LIDAR data
	*@param double Small_threshold_sigam	: lower sigma threshold
	*@param double Large_threshold_sigam	: upper sigma threshold
	*@param int max_iter	: maximum number of iterations
	*@param bool bMeridianConversion	: meridian conversion option
	*@param ReferenceEllipsoid ellipsoid	: reference ellipsoid for meridian conversion
	*@return bool
	*/
	bool CalibrationWithPlaneConstraints(CString cfgfilepath, CString patchpath, CString fileinfopath, CString resultfile, 
		                                                     double Small_threshold_sigam, double Large_threshold_sigam, int max_iter, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid);

	/**CalibrationWithPlaneConstraint
	* Description	    : To run a LIDAR calibration process using plane constraints
	*@param CString prjpath:			: project file path
	*@param CString WorkFolder	: work folder path
	*@return CString : message
	*/
	CString CalibrationWithPlaneConstraints(CString prjpath, CString WorkFolder="");

	/**ReadInputData
	* Description	    : To read given input data
	*@param CString patchpath:	: LIDAR patch data
	*@param CString fileinfopath:	: input file contents information
	*@param CSMList<CLIDARCalibrationPoint&> PointLIST	: LIDAR raw measurement list
	*@param bool bMeridianConversion	: meridian conversion option
	*@param ReferenceEllipsoid ellipsoid	: reference ellipsoid for meridian conversion
	*@return bool
	*/
	bool ReadInputData(CString patchpath, CString fileinfopath, CSMList<CLIDARCalibrationPoint> &PointList, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid);

	/**GetGroundFootPrint
	* Description	    : To calculate ground foot-prints of given raw measurements
	*@param CString infilepath	: LIDAR raw measurement data
	*@param CString fileinfopath:	: input file contents information
	*@param CString cfgfilepath:	: LIDAR configuration data
	*@param CString outpath	: outfile path
	*@param	int EQtype	: LIDAR EQ type
	*@param bool bMeridianConversion	: meridian conversion option
	*@param ReferenceEllipsoid ellipsoid	: reference ellipsoid for meridian conversion
	*@return bool
	*/
	bool GetGroundFootPrint(CString infilepath, CString fileinfopath, CString cfgfilepath, CString outpath, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid, CString outpts="");
	void GetGroundFootPrint(CLIDARCalibrationPoint &point, int EQtype);

private:
	/**ReadProjectFile
	* Description	    : To read project file
	*@param CString prjpath:	: project file path
	*@param , LIDAR_CALIBRATION_PRJ_STRUCT &prj : project data
	*@return bool
	*/
	bool ReadProjectFile(CString prjpath, LIDAR_CALIBRATION_PRJ_STRUCT &prj);

	/**ReadProjectFile_100
	* Description	    : To read project file(version 1.0)
	*@param fstream &PrjFile:	: project file stream
	*@param char* delimiter:	: delimiter
	*@param int num_delimiter	:  number of delimiter
	*@param , LIDAR_CALIBRATION_PRJ_STRUCT &prj : project data
	*@return bool
	*/
	bool ReadProjectFile_100(fstream &PrjFile, char* delimiter, int num_delimiter, LIDAR_CALIBRATION_PRJ_STRUCT &prj);

	/**ReadProjectFile_110
	* Description	    : To read project file(version 1.1)
	*@param fstream &PrjFile:	: project file stream
	*@param char* delimiter:	: delimiter
	*@param int num_delimiter	:  number of delimiter
	*@param , LIDAR_CALIBRATION_PRJ_STRUCT &prj : project data
	*@return bool
	*/
	bool ReadProjectFile_110(fstream &PrjFile, char* delimiter, int num_delimiter, LIDAR_CALIBRATION_PRJ_STRUCT &prj);

	/**ReadIndexFile
	* Description	    : To read information of input file contents
	*@param CString fileinfopath:	: input file contents information
	*@param CSMList<int> &index	: index of contents
	*@return bool
	*/
	bool ReadIndexFile(CString fileinfopath, CSMList<int>& index);

	/**ReadRecord
	* Description	    : To read each record (point) in the input file
	*@param fstream& infile	: fstream of the input file
	*@param CLIDARCalibrationPoint& point	: LIDAR calibration point after meridian conversion
	*@param CLIDARCalibrationPoint& point_old	: LIDAR calibration point (original values)
	*@param bool bMeridianConversion	: meridian conversion option
	*@param ReferenceEllipsoid ellipsoid	: reference ellipsoid for meridian conversion
	*@return bool
	*/
	bool ReadRecord(fstream &infile, CSMList<int>& index, CLIDARCalibrationPoint &point, CLIDARCalibrationPoint& point_old, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid);

	/**SolveWithPlaneConstraints
	* Description	    : To solve LIDAR calibration with plane constraints
	*@param double LIDAR_PRECISION	: LIDAR precision
	*@param const char resultfile[]	: result file name
	*@param int max_iter	: maximum iteration number
	*@param double Small_QThreshold	: lower sigma
	*@param double Big_QThreshold	: upper sigma
	*@return bool
	*/
	bool SolveWithPlaneConstraints(const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold, int EQtype, double &sigma, double limit_diff_sd=1.0e-10);

	/**Cal_PDsWithPlaneConstraints_UofC
	* Description	    : To calculate partial derivatives of LIDAR equation
	*@param double LIDAR_PRECISION	: LIDAR precision
	*@param CLIDARCalibrationPoint point	: LIDAR point
	*@param CSMLIDARConfig config	: configuration of LIDAR system
	*@param CSMMatrix<double> &jmat_lidar	: J matrix of raw measurements
	*@param CSMMatrix<double> &jmat_Point	: J matrix of ground points
	*@param CSMMatrix<double> &bmat	: B matrix
	*@param CSMMatrix<double> &kmat	: K matrix
	*@return void
	*/
	void Cal_PDsofLIDAREQ_UofC(CLIDARCalibrationPoint point, CSMLIDARConfig config, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_Point, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat, _Rmat_& Offset_R, CSMMatrix<double> &Pvector, _Rmat_& dOffsetR_dO, _Rmat_& dOffsetR_dP, _Rmat_& dOffsetR_dK);

	/**Cal_PDsWithPlaneConstraints_UofC
	* Description	    : To calculate partial derivatives of LIDAR equation
	*@param double LIDAR_PRECISION	: LIDAR precision
	*@param CLIDARCalibrationPoint point	: LIDAR point
	*@param CSMLIDARConfig config	: configuration of LIDAR system
	*@param CSMMatrix<double> &jmat_lidar	: J matrix of raw measurements
	*@param CSMMatrix<double> &jmat_Point	: J matrix of ground points
	*@param CSMMatrix<double> &bmat	: B matrix
	*@param CSMMatrix<double> &kmat	: K matrix
	*@param CSMMatrix<double> dumy	: additional rotation matrix
	*@return void
	*/
	void Cal_PDsofLIDAREQ_Basic(CLIDARCalibrationPoint Point, CSMLIDARConfig config, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_Point, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat, _Rmat_& Offset_R, CSMMatrix<double> &Pvector, _Rmat_& dOffsetR_dO, _Rmat_& dOffsetR_dP, _Rmat_& dOffsetR_dK, CSMMatrix<double> dumy);

	/**Cal_PDsofLIDAREQ_Terrapoint
	* Description	    : To calculate partial derivatives of LIDAR equation
	*@param double LIDAR_PRECISION	: LIDAR precision
	*@param CLIDARCalibrationPoint point	: LIDAR point
	*@param CSMLIDARConfig config	: configuration of LIDAR system
	*@param CSMMatrix<double> &jmat_lidar	: J matrix of raw measurements
	*@param CSMMatrix<double> &jmat_Point	: J matrix of ground points
	*@param CSMMatrix<double> &bmat	: B matrix
	*@param CSMMatrix<double> &kmat	: K matrix
	*@return void
	*/
	void Cal_PDsofLIDAREQ_Terrapoint(CLIDARCalibrationPoint point, CSMLIDARConfig config, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_Point, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat, _Rmat_& Offset_R, CSMMatrix<double> &Pvector, _Rmat_& dOffsetR_dO, _Rmat_& dOffsetR_dP, _Rmat_& dOffsetR_dK);

	/**LIDAREQ_Point_Terrapoint
	* Description	    : To calculate LIDAR EQ
	*@param CLIDARCalibrationPoint point	: LIDAR point
	*@param CSMLIDARConfig config	: configuration of LIDAR system
	*@return CSMMatrix<double>
	*/
	CSMMatrix<double> LIDAREQ_Point_Terrapoint(CLIDARCalibrationPoint point, CSMLIDARConfig config, bool bplus90);
	CSMMatrix<double> LIDAREQ_Point_Terrapoint_UofC(CLIDARCalibrationPoint point, CSMLIDARConfig config);
	//Copy from terrapoint source code
	CSMMatrix<double> LIDAREQ_Point_Terrapoint_source(CLIDARCalibrationPoint point, CSMLIDARConfig config);
	CSMMatrix<double> LIDAREQ_Point_Terrapoint_source_plus90(CLIDARCalibrationPoint point, CSMLIDARConfig config);
	CSMMatrix<double> LIDAREQ_Point_Terrapoint_etc(CLIDARCalibrationPoint point, CSMLIDARConfig config, CSMMatrix<double> dumy);
	CSMMatrix<double> LIDAREQ_Point_BasicEQ(CLIDARCalibrationPoint point, CSMLIDARConfig config, CSMMatrix<double> dumy);

	/**MatrixOut
	* Description	    : To print out calculate LIDAR EQ
	*@param CLIDARCalibrationPoint point	: LIDAR point
	*@param CSMLIDARConfig config	: configuration of LIDAR system
	*@return CSMMatrix<double>
	*/
	void MatrixOut(fstream &file, CLeastSquareLIDAR &LS);
	bool IterationReport(int nIteration, int max_iter, fstream &outfile, fstream &csvfile, CString resultfile, double sigma, double &old_sigma, CSMLIDARConfig OutConfig, CSMMatrix<double> &NMAT_dot_inv, CSMList<CLIDARCalibrationPoint> &PointList, CSMList<CLIDARCalibrationPoint> &OriginalPointList, double limit_change_sd = 1.0e-10);

	/**Correlation
	* Description	    : To calculate correlation using given matrix
	*@param CSMMatrix<double> VarCov	: variance-covariance matrix
	*@return CSMMatrix<double>
	*/
	CSMMatrix<double> Correlation(CSMMatrix<double> VarCov);

	void ApplyMeridianConversion(CLIDARCalibrationPoint& point, ReferenceEllipsoid ellipsoid);

	bool RunICPoint(int EQType, double &NDIST, KDTree &Ref_KD_Tree, double threshold, CSMList<CLIDARCalibrationPoint> &alllist, CSMList<CLIDARCalibrationPoint> &newlist);

	bool RunICPatch(int EQType, double &NDIST, CSMList<CTriangle> &TIN, double threshold, CSMList<CLIDARCalibrationPoint> &alllist, CSMList<CLIDARCalibrationPoint> &newlist);

	bool ReadICPoint(LIDAR_CALIBRATION_PRJ_STRUCT &prj, KDTree &Ref_KD_Tree, double &Dist_Threshold);

	bool ReadICPatch(LIDAR_CALIBRATION_PRJ_STRUCT &prj, CSMList<CTriangle> &TIN, double &norm_dist);
};

} //namespace SMATICS_BBARAB

#endif // !defined(__SPATIAL_SPACE_MATICS_LIDAR_CALIBRATION__)