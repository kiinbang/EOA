/*
 * Copyright (c) 2006-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// SMGrid.h: interface for the CSMGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__SM_GRID_DATA_CLASS__)
#define __SM_GRID_DATA_CLASS__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AFX.h"

template<typename T>
class CSMGridCell
{
public:
	CSMGridCell()
	{
		gridcell = NULL;
		nBands = 0;
	}

	CSMGridCell(int num)
	{
		nBands = num;
		gridcell = new T[nBands];
	}

	CSMGridCell(CSMGridCell<T>& copy)
	{
		nBands=copy.nBands;
		gridcell = new T[nBands];
		for(int i=0; i<nBands; i++)
			gridcell[i] = copy.gridcell[i];
	}

	~CSMGridCell()
	{
		Free();
	}

	void Free()
	{
		if(gridcell != NULL)
		{
			delete[] gridcell;
			gridcell = NULL;
			nBands = 0;
		}
	}
	
	void operator = (CSMGridCell<T>& copy)
	{
		Free();
		nBands=copy.nBands;
		gridcell = new T[nBands];

		for(int i=0; i<nBands; i++)
			gridcell[i] = copy.gridcell[i];
	}

	void operator = (T val)
	{
		for(int i=0; i<nBands; i++)
			gridcell[i] = val;
	}

	void operator >> (CSMGridCell<T>& copy)
	{
		copy.Free();
		copy.nBands = nBands;
		copy.gridcell = new T[nBands];

		for(int i=0; i<nBands; i++)
		{
			copy.gridcell[i] = gridcell[i];
		}
	}

	void operator >> (T* copy)
	{
		for(int i=0; i<nBands; i++)
		{
			copy[i] = gridcell[i];
		}
	}

	void operator << (CSMGridCell<T>& copy)
	{
		Free();
		nBands=copy.nBands;
		gridcell = new T[nBands];

		for(int i=0; i<nBands; i++)
		{
			gridcell[i] = copy.gridcell[i];
		}
	}

	void operator << (T* copy)
	{
		for(int i=0; i<nBands; i++)
		{
			gridcell[i] = copy[i];
		}
	}

	CSMGridCell<T> operator * (double val)
	{
		CSMGridCell<T> temp(this->nBands);
		temp << this->gridcell;

		for(int i=0; i<nBands; i++)
		{
			temp.gridcell[i] = (T)((double)this->gridcell[i]*val);
		}

		return temp;
	}

	CSMGridCell<T> operator + (CSMGridCell<T> &copy)
	{
		CSMGridCell<T> temp(this->nBands);
		temp << this->gridcell;

		for(int i=0; i<nBands; i++)
		{
			temp.gridcell[i] = (T)(temp.gridcell[i] + copy.gridcell[i]);
		}

		return temp;
	}

public:
	T *gridcell;
	unsigned char nBands;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
class CSMGrid
{
public:
	CSMGrid(){grid = NULL; nBands = 0; Width = 0; Height = 0; Inverse = false;}

	CSMGrid(CSMGrid<T> &copy)
	{
		Copy(copy);
	}

	CSMGrid(int num, int w, int h)
	{
		nBands = num;
		Width = w;
		Height = h;

		grid = NULL;

		if(num == 0) return;
		if(w == 0) return;
		if(h == 0) return;

		grid = new T[Height*Width*nBands];

		 Inverse = false;
	}

	void operator = (CSMGrid<T> &copy)
	{
		Free();

		Copy(copy);
	}

	void Copy(CSMGrid<T> &copy)
	{
		nBands = copy.GetnBands();
		Width = copy.GetWidth();
		Height = copy.GetHeight();
		
		if(copy.grid == NULL)
			return;
		
		grid = new T[Height*Width*nBands];		
		
		for(int i=0; i<Height*Width*nBands; i++)
		{
			grid[i] = copy.grid[i];
		}

		 Inverse = copy.Inverse;
	}

	void PutBuffer(int num, int w, int h, T* val)
	{
		Free();

		nBands = num; Width = w; Height = h;
		
		if(num == 0) return;
		if(w == 0) return;
		if(h == 0) return;

		grid = new T[Height*Width*nBands];
		
		//for(int i=0; i<Height*Width*nBands; i++)
		//{
		//	grid[i] = *val; val++;
		//}
		
		for(int i=0; i<Height; i++)
		{
			for(int j=0; j<Width; j++)
			{
				for(int k=0; k<nBands; k++)
				{
					if(Inverse == false)
					{grid[(i*Width + j)*nBands + k] = *val; val++;}
					else
					{grid[((Height-i-1)*Width + j)*nBands + k] = *val; val++;}
				}
			}
		}

	}

	virtual ~CSMGrid()
	{	
		Free();
	}
	
	void Free()
	{
		if(grid != NULL)
		{
			delete[] grid;
			grid = NULL; nBands = 0; Width = 0; Height = 0;
		}
	}

	void Resize(int num, int w, int h)
	{
		Free();

		nBands = num; Width = w; Height = h;
		
		if(num == 0) return;
		if(w == 0) return;
		if(h == 0) return;

		grid = new T[Height*Width*nBands];
	}
	

	T* operator () (unsigned int col, unsigned int row)
	{
		int index = (row*Width + col)*nBands;
		return &grid[index];
	}

	T& operator () (int band, unsigned int col, unsigned int row)
	{
		int index = (row*Width + col)*nBands + band;

		return grid[index];
	}

	bool operator () (int band, unsigned int col, unsigned int row, T &value)
	{
		int index = (row*Width + col)*nBands + band;
		if(index >= Width*Height*nBands) return false;

		value =  grid[index];
		return true;
	}

public:
	inline int& nBandsHandle() {return nBands;}
	inline int& WidthHandle() {return Width;}
	inline int& HeightHandle() {return Height;}
	inline int GetnBands() {return nBands;}
	inline int GetWidth() {return Width;}
	inline int GetHeight() {return Height;}
	inline int GetBitCount() {return (int)sizeof(T)*nBands*8;}
protected:
	T *grid;
	unsigned char nBands;
	
public:
	bool Inverse;
	int Width;
	int Height;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
class CSMGridDEM : public CSMGrid<T>
{
public:
	CSMGridDEM() : CSMGrid<T>(){}
	virtual ~CSMGridDEM() {};
	
	CSMGridDEM(int w, int h) : CSMGrid<T>(1,w,h) {}
	
	CSMGridDEM(int w, int h, T val) : CSMGrid<T>(1,w,h) 
	{for(int i=0; i<nBands*Width*Height; i++) grid[i] = val;}
	
	CSMGridDEM(int w, int h, T* val) : CSMGrid<T>(1,w,h) 
	{
		CSMGrid<T>::PutBuffer(nBands, Width, Height, val);
		bInverse = false;
	}
	CSMGridDEM(CSMGridDEM &copy) : CSMGrid<double>(copy)
	{
		Copy(copy);
	}

	void Copy(CSMGridDEM &copy)
	{
		leftX = copy.leftX;
		upperY = copy.upperY;

		maxZ = copy.maxZ;
		minZ = copy.minZ;
		
		resolution_X = copy.resolution_X;
		resolution_Y = copy.resolution_Y;
		
		val_fail = copy.val_fail;
		val_back = copy.val_back;
	}

	void operator = (CSMGridDEM &copy)
	{
		CSMGrid<double>::operator = (copy);

		Copy(copy);
	}

	T* operator[] (int index)
	{
		return &grid[index*Width];
	}

	bool ReadDEM(CString FileName)
	{
		CFile file;
		CFileException fe;
		
		// 읽기 모드로 파일 열기
		if(!file.Open(FileName, CFile::modeRead|CFile::shareDenyWrite, &fe))
		{
			AfxMessageBox("파일읽기 에러");
			return 0;
		}
		
		// 메모리 할당
		Resize(1,Width,Height);

		maxZ=-1.0e10, minZ=1.0e10;

		T temp;		
		for(int j=0; j<Height; j++)
		{
			for(int i=0; i<Width; i++)
			{
				if (file.Read((void*)&temp, sizeof(T)) == sizeof(T)) 
				{
					if(Inverse == true)
						SetZ(i,Height-j-1,temp);
					else
						SetZ(i,j,temp);

					if((temp != val_back)&&(temp != val_fail))
					{						
						if((double)temp > maxZ) maxZ = (double)temp;
						if((double)temp < minZ) minZ = (double)temp;
					}
				}
				else
				{
					AfxMessageBox("파읽기에러");
					return false;
				}				
			}
		}

		file.Close();

		return true;
	}

	bool ReadTextXYZDEM(CString FileName)
	{
		// 메모리 할당
		Resize(1,Width,Height);

		// Text DEM 파일 읽기
		FILE *file;
		file = fopen(FileName, "r");
		double X, Y; double Z;
		
		for(long j=0; j<(long)Height; j++)
		{
			for(long i=0; i<(long)Width; i++)
			{
				if(EOF == fscanf(file,"%lf",&X)) break;
				if(EOF == fscanf(file,"%lf",&Y)) break;
				if(EOF == fscanf(file,"%lf",&Z)) break;
				
				SetZ(i,j,T(Z));
				
				if(Inverse == true)
					SetZ(i,Height-j-1,T(Z));
				else
					SetZ(i,j,T(Z));
			}
		}
		
		fclose(file);
		
		return true;
	}
	
	void SaveTXTDEM2BINDEM(CString path)
	{
		CFile newfile;
		CFileException fe;
		// 쓰기 모드로 파일 열기
		newfile.Open(path, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe);

		for(long j=0; j<(long)Height; j++)
		{
			for(long i=0; i<(long)Width; i++)
			{
				T newdata = GetZ(i, j);
				
				if(newdata > 1.0e5) newdata = -9999.0;//<--false values
				if(newdata < 0.0) newdata = -9999.0;//<--false values
				
				newfile.Write(&newdata, sizeof(T));
			}
		}
		
		newfile.Close();
	}

public:
	double leftX;
	double upperY;
	double resolution_X;
	double resolution_Y;
	double val_fail;
	double val_back;
	double maxZ, minZ;

public:
	inline double GetX(int indexX) {double value;value = leftX + resolution_X*indexX;return value;}
	
	inline double GetY(int indexY) {double value;value = upperY - resolution_Y*indexY;return value;}
	
	inline T& GetZ(int indexX, int indexY) 
	{
		if(false == CheckIndex(indexX, indexY)) return this->val_fail;

		return operator()(0,indexX,indexY);
	}
	
	inline bool GetZ(int indexX, int indexY, T& value)
	{
		if(indexY<0||indexY>=Height) return false;
		if(indexX<0||indexX>=Width) return false;

		value = operator()(0,indexX,indexY);

		if(value == val_fail) return false;
		if(value == val_back) return false;

		return true;
	}

	inline bool GetZ(double indexX, double indexY, T& value)
	{
		if(indexY<0||indexY>=Height) return false;
		if(indexX<0||indexX>=Width) return false;

		double value1, value2, value3, value4;

		int row = (int)indexY;
		int col = (int)indexX;

		if(false == this->GetZ(col, row, value1)) return false;
		if(false == this->GetZ(col, row+1, value2)) return false;
		if(false == this->GetZ(col+1, row, value3)) return false;
		if(false == this->GetZ(col+1, row+1, value4)) return false;
		
		double A, B, C, D;
		A = (double)((int)indexX + 1) - indexX;
		B = 1 - A;
		C = (double)((int)indexY + 1) - indexY;
		D = 1 - C;

		value = (value1*A*C + value2*B*C + value3*A*D + value4*B*D);

		return true;
	}

	inline bool GetZ_Bilinear(double indexX, double indexY, T& value)
	{
		return GetZ(indexX, indexY, value);
	}

	inline bool SetZ(int indexX, int indexY, T value) 
	{
		if(false == CheckIndex(indexX, indexY)) return false;

		grid[indexY*Width + indexX] = value;

		return true;
	}
	
	inline bool GetBuf(int indexX, int indexY, T& value)
	{
		if(false == CheckIndex(indexX, indexY)) return false;

		value = GetZ(indexX,indexY);

		if(value == val_fail) return false;
		if(value == val_back) return false;

		return true;
	}

	inline bool CheckIndex(int indexX, int indexY)
	{
		if(indexX >= this->Width) return false;
		if(indexX < 0) return false;
		if(indexY >= this->Height) return false;
		if(indexY < 0) return false;

		return true;
	}

	inline bool GetXYZ(int indexX, int indexY, double& X, double& Y, T& Z)
	{
		X = leftX + resolution_X*indexX;
		Y = upperY - resolution_Y*indexY;
		
		if(indexY<0||indexY>=Height) return false;
		if(indexX<0||indexX>=Width) return false;
		
		Z = operator()(0,indexX,indexY);

		if(Z == val_fail) return false;
		if(Z == val_back) return false;

		return true;
	}

	inline bool GetXYZ(double indexX, double indexY, double& X, double& Y, T& Z)
	{
		if(indexY<0||indexY>=Height) return false;
		if(indexX<0||indexX>=Width) return false;
		
		if(false == this->GetZ(indexX, indexY, Z)) return false;

		X = leftX + resolution_X*indexX;
		Y = upperY - resolution_Y*indexY;

		return true;
	}

	inline bool GetIndex(double X, double Y, int &indexX, int &indexY)
	{
		indexX = (int)((X - leftX)/resolution_X + 0.5);
		indexY = (int)((upperY - Y)/resolution_Y + 0.5);
		
		if(indexY<0||indexY>=Height) return false;
		if(indexX<0||indexX>=Width) return false;

		return true;
	}

	inline bool GetIndex(double X, double Y, double &indexX, double &indexY)
	{
		indexX = (X - leftX)/resolution_X;
		indexY = (upperY - Y)/resolution_Y;
		
		if(indexY<0.0||indexY>=(double)Height) return false;
		if(indexX<0.0||indexX>=(double)Width) return false;

		return true;
	}
	
	inline bool SetBuf(int indexX, int indexY, T value)
	{
		if(false == CheckIndex(indexX, indexY)) return false;

		SetZ(indexX,indexY,value);

		return true;
	}

	inline void CopyBuf(T* value)
	{
		CSMGrid<T>::SetBuf(nBands,Width,Height,(T*)value);
	}
	
	inline void SetConfig(int W, int H, double LX, double UY, double ResX, double ResY, double failval, double backval, bool bInverse = false)
	{
		Width = W;
		Height = H;		
	
		leftX = LX; upperY = UY; resolution_X = ResX; resolution_Y = ResY; val_fail = failval; val_back = backval;
		Inverse = bInverse;
	}

	inline void SetConfig(char* hdrpath, bool bInverse)
	{
		fstream hdrfile; hdrfile.open(hdrpath,ios::in);
		double res;
		hdrfile>>Width>>Height>>res>>leftX>>upperY>>val_back>>val_fail;
		
		hdrfile.close();
		
		resolution_X = resolution_Y = res;

		Inverse = bInverse;
	}
};

#endif // !defined(__SM_GRID_DATA_CLASS__)