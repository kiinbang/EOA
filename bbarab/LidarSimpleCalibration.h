/*
* Copyright (c) 2005-2006, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

// LidarSimpleCalibration.h: interface for the CLidarSimpleCalibration class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(___SSMATICS___BBARAB___LIDAR___CALIBRATION___SIMPLIFIED___METHOD___)
#define ___SSMATICS___BBARAB___LIDAR___CALIBRATION___SIMPLIFIED___METHOD___

#include "UtilityGrocery.h"
#include "SMRotationMatrix.h"

using namespace SMATICS_BBARAB;

class CLidarSimpleCalibration  
{
public:
	CLidarSimpleCalibration()
	{
		dX = dY = dO = dP = dK = dRange = dS = 0.0;
	}
	
	~CLidarSimpleCalibration()
	{
	}

	void SetParameters(double dX, double dY, double dO, double dP, double dK, double dRange, double dS)
	{
		this->dX = dX;
		this->dY = dY;
		this->dO = dO;
		this->dP = dP;
		this->dK = dK;
		this->dRange = dRange;
		this->dS = dS;
	}

	static double CalLateralCoord(const double Pa[], const double Pb[], const double P0[], double *P)
	{
		double FlightDirectionAzi = CalAzimuth(Pa[0]-Pa[0], Pa[1]-Pa[1], Pb[0]-Pa[0], Pb[1]-Pa[1]);
		
		_Mmat_ Azimuth_M(0, 0, -FlightDirectionAzi);

		CSMMatrix<double> P_(3,1,0.0);
		P_(0,0) = P0[0]-Pa[0];
		P_(1,0) = P0[1]-Pa[1];
		P_(2,0) = P0[2]-Pa[2];

		CSMMatrix<double> Rotated_P = Azimuth_M.Mmatrix%P_;

		//find intersection point
		CSMMatrix<double> Intersection(3,1);
		Intersection(0,0) = 0.0;
		Intersection(1,0) = Rotated_P(1,0);
		Intersection(2,0) = Rotated_P(2,0);

		CSMMatrix<double> Unrotated_P = Azimuth_M.Mmatrix.Transpose()%Intersection;
		P[0] = Unrotated_P(0,0) + Pa[0];
		P[1] = Unrotated_P(1,0) + Pa[1];
		P[2] = Unrotated_P(2,0);
		
		return Rotated_P(0,0);
	}

	void GetCalibratedPos(const double X, const double Y, const double Z, const double FH0, const double x, double &newX, double &newY, double &newZ, const bool bForward)
	{
		double FH = FH0 - Z;
		double Beta;
		Beta = -atan(x/FH);
		
		if(bForward == true)//Forward strip
		{
			newX = X - (dX - FH*dP - sin(Beta)*dRange - FH*Beta*dS);
			newY = Y - (dY + FH*dO + x*dK);
			newZ = Z - (-x*dP - cos(Beta)*dRange - x*Beta*dS);
		}
		else//Backward strip
		{
			newX = X + (dX - FH*dP - sin(Beta)*dRange - FH*Beta*dS);
			newY = Y + (dY + FH*dO + x*dK);
			newZ = Z - (-x*dP - cos(Beta)*dRange - x*Beta*dS);
		}
	}

public:
	/*Lidar calibration parameters*/
	double dX, dY, dO, dP, dK, dRange, dS;

};

#endif // !defined(___SSMATICS___BBARAB___LIDAR___CALIBRATION___SIMPLIFIED___METHOD___)
