/*
 * Copyright (c) 2002-2002, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#if !defined(_SPACEMATICS_ORTHOIMAGE_DLT)
#define _SPACEMATICS_ORTHOIMAGE_DLT

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <SMaticsDLT.h>
#include "./Image_Util/imagepixel.h"
#include <Transformation.h>
#include <SMMatrix.h>
#include <SMLidardem.h>

/////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//
//
//
//
//
/**
* @struct DLTparameters
* @brief data struct for 11 parameters of DLT and additional parameters.\n
* @author Bang, Ki In
* @version 1.0
* @Since 2002~
* @bug N/A.
* @warning N/A.
*
* \nThis struct has 11 DLT parameters for the frame DLT and linear scanner DLT:
* <ul> 
* <li> 11 parameters: A1~A3, B1~B3, C1~C3, D1, D2\n
*		col = (A1*X+B1*Y+C1*Z+D1)/(A3*X+B3*Y+C3*Z+1)\n
*		row = (A2*X+B2*Y+C2*Z+D1)/(A3*X+B3*Y+C3*Z+1) OR row = (A2*X+B2*Y+C2*Z+D1)\n
* <li> additional parameters
*	<ol>
*	<li>alpah1, alpha2
*	<li>beta1, beta2
*	</ol>
* </ul>\n
*/
typedef struct DLTparameters
{
	double A1, B1, C1, D1;
	double A2, B2, C2, D2;
	double A3, B3, C3;
	double A4, B4, C4;
	double A5, B5, C5;
	double alpha, beta;
	double alpha2, beta2;
} DLTparameters;

/////////////////////////////////////////////////////////////////////////////
//
/**
* @struct CSatDLTOrthoImage
* @brief This is the class for ortho-rectification using DLT sensor model.\n
* @author Bang, Ki In
* @version 1.0
* @Since 2002~
* @bug N/A.
* @warning N/A.
*
* \nThis class has three method for the ortho-rectification:
* <ul> 
* <li> point-by-point method\n
* <li> patch-by-patch method
*	<ol>
*	<li>rectangle area resampling by affine transformation
*	<li>triangle area resampling by simplicial coordinate system
*	</ol>
* </ul>\n
*/
class CSatDLTOrthoImage
{
public:
	//SatDLT Sensor Model
	CSatDLT SatDLT;
	//LIDAR DEM
	CSMDEM LIDAR;
	//Original Image
	CImage OriginImage;
	
public:
	/**constructor*/
	CSatDLTOrthoImage();
	/**destructor*/
	~CSatDLTOrthoImage();
	

	/**MakeOrtho
	* Description	    : to make ortho-photo using digital differential rectification by point-by-point method.
	*@param DWORD Start_C: index of start column
	*@param DWORD Start_R: index of start row
	*@param DWORD End_C : index of end column
	*@param DWORD End_R : index of end row
	*@param char* OrthoName : ortho image(output) file path
	*@return bool 
	*/
	bool MakeOrtho(DWORD Start_C, DWORD Start_R, DWORD End_C, DWORD End_R, char* OrthoName);
	
	/**MakeOrtho_Affine
	* Description	    : to make ortho image by patch-by-patch method using affine transformation
	*@param DWORD Start_C: index of start column
	*@param DWORD Start_R: index of start row
	*@param DWORD End_C : index of end column
	*@param DWORD End_R : index of end row
	*@param char* OrthoName : ortho image(output) file path
	*@return bool 
	*/
	bool MakeOrtho_Affine(DWORD Start_C, DWORD Start_R, DWORD End_C, DWORD End_R, char* OrthoName);

	/**MakeOrtho_Affine
	* Description	    : to make ortho image by patch-by-patch method using simplicial coordinates system
	*@param DWORD Start_C: index of start column
	*@param DWORD Start_R: index of start row
	*@param DWORD End_C : index of end column
	*@param DWORD End_R : index of end row
	*@param char* OrthoName : ortho image(output) file path
	*@return bool 
	*/
	bool MakeOrtho_Convex(DWORD Start_C, DWORD Start_R, DWORD End_C, DWORD End_R, char* OrthoName);
	
	/**SetOriginImage
	* Description	    : to input source image
	*@param char* FileName : source image path
	*@return bool 
	*/
	bool SetOriginImage(char* FileName);
	
	/**TriArea
	* Description	    : to calculate triangle area
	*@param Point2D<double> p : a point of vertexes of triangle
	*@param Point2D<double> p1 : a point of vertexes of triangle
	*@param Point2D<double> p2 : a point of vertexes of triangle
	*@return double 
	*/
	double TriArea(Point2D<double> p, Point2D<double> p1, Point2D<double> p2);

};

#endif // !defined(_SPACEMATICS_ORTHOIMAGE_DLT)