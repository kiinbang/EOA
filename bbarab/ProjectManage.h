// ProjectManage.h: interface for the CProjectManage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROJECTMANAGE_H__81F9092D_344B_4380_9AC5_D4694090C97D__INCLUDED_)
#define AFX_PROJECTMANAGE_H__81F9092D_344B_4380_9AC5_D4694090C97D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GCPManage.h"

enum SENSORTYPE {IKONOS_PAN, SPOT_PAN, KOMPSAT_EOC, GENERAL_PUSH, GENERAL_WISK};
enum COORDSYS {TM, UTM, WGS84};
enum SENSORMODEL {COLLINEARITY, DLT, RFM, POLYNOMIAL, ORBIT};
enum IMAGEFORMAT {TIF, RAW};

class CProjectManage  
{
public:
	
public:
	CProjectManage();
	virtual ~CProjectManage();

	//project open
	bool ProjectOpen(char* filename);
	//temp project file open
	bool TempProjectOpen(char* filename,char* oldfilename);

	//project save
	bool SaveProject(char* filename);

	//projec init
	void Init(void);

//member variable
public:
	//Project Name
	CString m_stProjectName;
	//Project Path
	CString m_stProjectPath;
	//Sensor Type
	SENSORTYPE m_SensorType;
	CString m_stSensorTypeName;
	//Sensor Model
	SENSORMODEL m_SensorModel;
	CString m_stSensorModelName;
	//Coordinates System
	COORDSYS m_CoordSystem;
	CString m_stCoordSystemName;
	//Image Path
	CString m_LeftPath;
	CString m_RightPath;
	//Image Format
	IMAGEFORMAT m_LFormat;
	IMAGEFORMAT m_RFormat;
	//GCP
	bool m_bGCP;
	//Tie Point
	bool m_bTie;
	//DEM
	bool m_bDEM;
	CString m_stDEMPath;
	//Ortho
	bool m_bOrtho;
	CString m_stOrthoPath;
	//Epipolar
	bool m_bEpipolar;
	CString m_stEpiLeftPath;
	CString m_stEpiRightPath;
	//Mapping
	bool m_bMapping;
	CString m_stMapPath;

	//Project Description
	//CString m_Description;

	//GCP Data
	CGCPManage m_LGCP;
	CGCPManage m_RGCP;
	CICPManage m_LICP;
	CICPManage m_RICP;

	//Tie Point Data
	CICPManage m_LTie;
	CICPManage m_RTie;

	//Sensor Model

};

#endif // !defined(AFX_PROJECTMANAGE_H__81F9092D_344B_4380_9AC5_D4694090C97D__INCLUDED_)
