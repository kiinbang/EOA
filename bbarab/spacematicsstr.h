/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/

#ifndef __SPACEMATICSSTR_H__2002_04_29
#define __SPACEMATICSSTR_H__2002_04_29

#include <assert.h>
#include <math.h>
#include <string.h>

/**@class CSMStr.
*@brief string class<br>
*instead of CString for making the program light<br>
*somethimes for ATL without MFC.
*/

#pragma warning(disable: 4996)

class CSMStr
{
public:
	/**constructor*/
	CSMStr(const CSMStr& str)
	{ 
		if(str == 0)
		{
			m_nLength = 0;
			m_pString = 0;
		}
		else
		{
			m_nLength = str.m_nLength; 
			m_pString = new char[m_nLength + 1];
			assert(m_pString != 0);
			strcpy(m_pString, str.m_pString); 
		}
	}
	/**constructor*/
	CSMStr(const char* str)
	{
		if(str == 0)
		{
			m_nLength = 0;
			m_pString = 0;
		}
		else
		{
			m_nLength = strlen(str);
			m_pString = new char[m_nLength + 1];
			assert(m_pString != 0);
			strcpy(m_pString, str);
		}
	}
	/**constructor*/
	CSMStr()								{ m_nLength = 0; m_pString = 0; }
	/**destructor*/
	virtual ~CSMStr()						{ if (m_pString) delete m_pString; }

	// operator overloading helper
	/**operator overloading*/
	template <class T> friend CSMStr _cdecl operator +(T var, const CSMStr& str);
	
	/**operator overloading*/
	CSMStr& operator =(const CSMStr& str)
	{
		assert(str != 0);
		if (m_pString) delete m_pString;
		m_nLength = strlen(str.m_pString);
		m_pString = new char[m_nLength + 1]; 
		assert(m_pString != 0);
		strcpy(m_pString, str.m_pString);
		
		return *this;
	}

	/**operator overloading*/
	CSMStr& operator =(const char* str)
	{
		if (m_pString) delete m_pString;
		m_nLength = strlen(str);
		m_pString = new char[m_nLength + 1]; 
		assert(m_pString != 0);
		strcpy(m_pString, str);
		
		return *this;
	}
	template <class T>
	CSMStr& operator +=(const char* str)	{ return *this += (CSMStr)str; }
	CSMStr& operator +=(const CSMStr& str)
	{
		m_nLength += str.m_nLength;
		char* pNew = new char[m_nLength + 1];
		assert(pNew != 0);
		strcpy(pNew, m_pString);
		strcat(pNew, str.m_pString);
		delete m_pString;
		m_pString = pNew;
		
		return *this;
	}

	// add more logic comparison operators as following, for example, although not efficient
	virtual bool operator !=(char* str)	{ return strcmp(str, m_pString) != 0; }
	virtual bool operator !=(CSMStr str) {return strcmp(str.m_pString, m_pString) != 0;}

	// c type string conversion
	operator char* ()					{ return m_pString; }
	operator const char* ()	const		{ return m_pString; }
	char* GetChar()						{ return m_pString; }
	int GetLength()						{ return m_nLength; }

	// format string
	int Format(const char* format, ...)
	{
		assert(format != 0);
		
		int len;
		char* MaxBuf;
		for(int i = 5; ; i ++)
		{
			len = (int)pow(2.0, i);
			MaxBuf = new char[len];
			if (!MaxBuf) return 0;
			// some UNIX's do not support vsnprintf and snprintf
			len = _vsnprintf(MaxBuf, len, format, (char*)(&format + 1));
			if (len > 0) break;
			delete []MaxBuf;
			if (len == 0) return 0;
		}
		
		if (!m_pString)
		{
			m_nLength = len;
			m_pString = new char[m_nLength + 1];
		}
		else if (m_nLength < len)
		{
			delete m_pString;
			m_nLength = len;
			m_pString = new char[m_nLength + 1];
		}
		if (m_pString) 
			strcpy(m_pString, MaxBuf);
		else
			len = 0;
		delete []MaxBuf;
		
		return len;
	}
	
protected:
	
	// data block
	int   m_nLength;
	char* m_pString;
};


#endif