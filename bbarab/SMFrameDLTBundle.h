// SMFrameDLTBundle.h interface for the CParallel class.
//
//////////////////////////////////////////////////////////////////////

/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/

#if !defined(__BBARAB__FRAME__DLT__BUNDLE__UOFC__)
#define __BBARAB__FRAME__DLT__BUNDLE__UOFC__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMModifiedParallel.h"

/**
*@class CSMFrameDLTBA
*@brief This is the class for DLT(frame image)<br>
*and derived from SMModifiedParallel class<br>
*[DLT]<br>
*row = (A1X + A2Y + A3Z + A4)/(A9X + A10Y + A11Z + 1)<br>
*col = (A5X + A6Y + A7Z + A8)/(A9X + A10Y + A11Z + 1).
*/
class CSMFrameDLTBA:public SMModifiedParallel
{
	//friend class CSMSDLTBA;
public:
	CSMFrameDLTBA();
	virtual ~CSMFrameDLTBA();
	/**
	*Bundle Adjustment(Frame DLT)
	*/
	virtual CString DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize);

protected:
	/**
	*Calculate the initial value<br>
	*This func is called by DoModeling().
	*/
	virtual CString DoInitModeling(unsigned int num_param);
	
	/**
	*Get row value using approximate parameters.
	*/
	virtual double Func_row(_GCP_ G, unsigned int index);
	virtual double Func_row(_GCL_ G, unsigned int index);
	/**
	*Get col value using approximate parameters.
	*/
	virtual double Func_col(_GCP_ G, unsigned int index);
	virtual double Func_col(_GCL_ G, unsigned int index);

	/**Cal_PDs<br>
	*Partial derivatives
	*@param G ground point coord
	*@param I image point coord
	*@param ret1 partial derivative(row)
	*@param ret2 partial derivative(col)
	*@param num_param number of parameters of sensor model
	*@param index scene #
	*/
	virtual void Cal_PDs_linear(_GCP_ G, _ICP_ I, double* ret1, double* ret2, unsigned int scene_index);
	virtual void Cal_PDs(_GCP_ G, double* ret1, double* ret2, unsigned int index);
	/**
	*Partial derivatives for a line
	*@param GL1 ground control line
	*@param GL2 ground control line
	*@param IL image control line
	*@param ret partial derivatives
	*@param index scene #
	*@param num_param number of parameters of sensor model
	*/
	virtual void Cal_PDs_Line_linear(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int image_index, unsigned int num_param);
	virtual void Cal_PDs_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int image_index, unsigned int num_param);
	
	/**
	*Denormalization for the 3D ground coords.
	*/
	virtual SMParam ParamDenormalization(SMParam param, unsigned int num_param);

	/**
	*Get initial approximate value by only using points
	*/
	virtual void PointOBSInit(CSMList<_GCP_> &gcp, _SCENE_ &scene, unsigned int num_param, Matrix<double> &A, Matrix<double> &L, Matrix<double> &W, unsigned int scene_index);
	
	/**
	*Find line flag "B"
	*/
	bool FindFlagB(unsigned int GCL_Num, CSMList<_GCL_> gcl, CString LID, unsigned int nStart, _GCL_ &GLB);
	/**
	*Find line flag "C"
	*/
	bool FindFlagC(CSMList<_ICL_> &icl, CString LID, _ICL_ &ILA, _ICL_ &ILB);

};

/**
*@class CSMSDLTBA
*@brief This is the class for Wang's SDLT(self-calibration DLt)<br>
*and derived from SMModifiedParallel class<br>
*[SDLT]<br>
*row = (A1X + A2Y + A3Z + A4)/(A9X + A10Y + A11Z + 1)<br>
*col + A12*row*col = (A5X + A6Y + A7Z + A8)/(A9X + A10Y + A11Z + 1).
*/
class CSMSDLTBA:public CSMFrameDLTBA
{
public:
	//      (A1X + A2Y + A3Z + A4)
	//row = ----------------------
	//      (A9X + A10Y + A11Z + 1)
	//
	//      (A5X + A6Y + A7Z + A8)
	//col = ---------------------- + A12*row*col
	//      (A9X + A10Y + A11Z + 1)
	CSMSDLTBA();
	virtual ~CSMSDLTBA();
public:
	/**
	*Bundle Adjustment(Wang's SDLT)
	*/
	virtual CString DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize);
	/**
	*Open ICL file<br>
	*SDLT module does not support linear constraints.
	*/
	CString InputMatlabICLFile(CString fname, bool bDataString)
	{
		CString message="SDLT module does not support linear constraints!";
		AfxMessageBox(message);
		return message;
	}
	/**
	*Open GCL file<br>
	*SDLT module does not support linear constraints.
	*/
	virtual CString InputMatlabGCLFile(CString fname, bool bDataString=true)
	{
		CString message="SDLT module does not support linear constraints!";
		AfxMessageBox(message);
		return message;
	}
protected:
	/**
	*0 = (param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3])<br>
	*   /(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.)<br>
	*   - I.row.
	*/
	virtual double Func_F(_GCP_ G, _ICP_ I, unsigned int index);
	virtual double Func_F(_GCL_ G, _ICL_ I, unsigned int index);
	
	/**
	* 0 = (param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7])<br>
	*    /(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.)<br>
	*    + param.A[11]*I.row*I.col - I.col.
	*/
	virtual double Func_G(_GCP_ G, _ICP_ I, unsigned int index);
	virtual double Func_G(_GCL_ G, _ICL_ I, unsigned int index);

	/**
	*Get col value using approximate parameters.
	*/
	virtual double Func_col(_GCP_ G, double row, unsigned int index);
	virtual double Func_col(_GCL_ G, double row, unsigned int index);

	/**
	*Get row value using approximate parameters.
	*/
	virtual double Func_row(_GCP_ G, unsigned int index);
	virtual double Func_row(_GCL_ G, unsigned int index);

	/**Cal_PDs<br>
	*Partial derivatives
	*@param G ground point coord
	*@param I image point coord
	*@param ret1 partial derivative(row)
	*@param ret2 partial derivative(col)
	*@param num_param number of parameters of sensor model
	*@param index scene #
	*/
	virtual void Cal_PDs(_GCP_ G, _ICP_ I, double* ret1, double* ret2, unsigned int index, unsigned int num_param);
	/**
	*Fill elements of Normal matrix for each image point observation(i).
	*/
	virtual void FillMatrix_Point_i(CLeastSquare &LS, _GCP_ GP, _ICP_ IP, unsigned image_index, Matrix<double> &a1, Matrix<double> &a2, Matrix<double> &l, Matrix<double> &w, unsigned int num_param);
	/**
	*Denormalization for the 3D ground coords.
	*/
	virtual SMParam ParamDenormalization(SMParam param, unsigned int num_param);
};

#endif // !defined(__BBARAB__FRAME__DLT__BUNDLE__UOFC__)
