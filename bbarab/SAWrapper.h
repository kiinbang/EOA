// SAWrapper.h: interface for the CSAOneDimWrapper class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SAWRAPPER_H__4AA32868_2E3B_49DC_884A_01FCB6B14C00__INCLUDED_)
#define AFX_SAWRAPPER_H__4AA32868_2E3B_49DC_884A_01FCB6B14C00__INCLUDED_


#include <comdef.h>

/**************************/
/*SafeArray Wrapping Class*/
/**************************/

/***************************************/
/*Made by B.bara.B and Spacematics Lab.*/
/***************************************/

//Class Name: CSAOneDimWrap

//Description:
//Safearray Wrapping Class

//revision
//date: 2002.10.17


template <VARTYPE vt>
class CSAOneDimWrap
{


/////////////////////////////
//Constructor and Destructor
/////////////////////////////
public:
	//default constructor
	CSAOneDimWrap()
	{
		SA = NULL;
		bArray_avail = false;
		bDelete_avail = false;
	}
	
	//constructor(safearray create)
	CSAOneDimWrap(long LBound, long Elements)
	{
		CreateSA(LBound, Elements);
	}

	//constructor(safearray pointer substitution)
	CSAOneDimWrap(SAFEARRAY *sa)
	{
		SA = sa;

		long mDim = SafeArrayGetDim(SA);
		if(mDim != 1)
			return;

		long UB;
				
		//get safearray bound in each dimension
		HRESULT hr;
		
		hr = SafeArrayGetLBound(SA,1,&mLBound);

		hr = SafeArrayGetUBound(SA,1,&UB);

		mElements = UB - mLBound + 1;
		
		bArray_avail = true;
		bDelete_avail = false;
	}

	//destructor
	virtual ~CSAOneDimWrap()
	{
		::SafeArrayUnaccessData(SA);
		SafeArrayDestroy();
	}


//////////////////
//Member variable
//////////////////
private:

	//safearray pointer
	SAFEARRAY *SA;

	//safearray spec
	long mLBound, mElements;

	//bool type variable of array generation
	bool bArray_avail;

	//bool type variable of array self generation
	bool bDelete_avail;


//////////////////
//Member function
//////////////////
public:

	//return Safearray pointer
	SAFEARRAY* GetSA(void)
	{
		if(bArray_avail == true)
			return SA;
		return NULL;
	}



	//create safearray
	bool CreateSA(long LBound, long Elements)
	{
		if(bArray_avail == true)
			//SA_Setting is complete already.
			return false;

		mLBound = LBound;
		mElements = Elements;
	
		//generate SA bound
		SAFEARRAYBOUND rgsbound;
		
		//SA bound(member variable)
		rgsbound.cElements = mElements;
		rgsbound.lLbound = mLBound;
		
		//create SA
		SA = SafeArrayCreate(vt, 1, &rgsbound);

		//bool type variable set
		bArray_avail = true; //is SA available?
		bDelete_avail = true; //is SA self_generate_mem?

		return true;
	}



	//Destroy safearray
	bool SafeArrayDestroy(void)
	{
		if(bDelete_avail & true)
		{
			if(bArray_avail & true)
			{
				if(SA != NULL)
				{
					::SafeArrayDestroy(SA);
					return true;
				}
			}
		}
		
		return false;
	}


	//Get element
	//bool Get(long elm_index, _variant_t *value)
	bool Get(long elm_index, void* value)
	{
		if(bArray_avail == false)
			return false;

		long index;
		index = elm_index;

		if((index>(mElements+mLBound-1))||(index<mLBound))
			return false;

		//value->vt = vt;

		HRESULT hr = SafeArrayGetElement(SA,&index,(void*)value);
		if(FAILED(hr))
			return false;
		
		return true;
	}


	//Set element
	bool Set(long elm_index, void *value)
	{
		if(bArray_avail == false)
			return false;

		long index;
		index = elm_index;

		if((index>(mElements+mLBound-1))||(index<mLBound))
			return false;

		HRESULT hr = SafeArrayPutElement(SA,&index,(void*)value);
		if(FAILED(hr))
			return false;

		return true;
	}


	//Get buffer of Array
	bool GetBuf(void HUGEP* FAR* buf)
	{
		HRESULT hr;
		hr = SafeArrayAccessData(SA,buf);
		if (FAILED(hr))
			return false;

		return true;
	}


	//Get size of element
	UINT SafeArrayGetElemsize(void)
	{
		return ::SafeArrayGetElemsize(SA);
	}


	//Get num of elements
	long GetElements(void)
	{
		return mElements;
	}


	//Get Low Bound
	long GetLBound(void)
	{
		return mLBound;
	}

};

#endif // !defined(AFX_SAWRAPPER_H__4AA32868_2E3B_49DC_884A_01FCB6B14C00__INCLUDED_)
