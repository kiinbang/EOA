// smorthoimage.cpp : implementation file
//

#include "stdafx.h"
#include <SMOrthoimagePush.h>
#include <ProgressBar.h>

#define PATCH_ORTHO_RESOLUTION 10

#define _MAXDIFFROW_ 0.5
#define BACKGROUND 200
#define WALLINDEX -888

#define _MAX_STRING_LENGTH_ 512
using namespace SMATICS_BBARAB;

/////////////////////////////////////////////////////////////////////////////
//
//
//
//
// CSatOrthoImage
//
//
//
//
CSatOrthoImage::CSatOrthoImage()
{
	idx_x = NULL;
	idx_y = NULL;
	m_pData = NULL;

	m_DEM = new CSMGridDEM<double>;
}

CSatOrthoImage::~CSatOrthoImage()
{
	delete m_DEM;
}

bool CSatOrthoImage::ReadEOParam(CString parampath)
{
	m_OIEOP.RemoveAll();
	m_SEOP.O.RemoveAll();
	m_SEOP.P.RemoveAll();
	m_SEOP.K.RemoveAll();
	m_SEOP.X.RemoveAll();
	m_SEOP.Y.RemoveAll();
	m_SEOP.Z.RemoveAll();

	//O(deg), P(deg), K(deg), X(m), Y(m), Z(m)
	double EO1[6], EO2[6];

	fstream infile;
	infile.open(parampath, ios::in);

	//version check
	int pos;
	bool bVersion = FindString(infile, "VER", pos);
	if(bVersion == true)
	{
		//To move to begin of file
		infile.seekg(0,ios::beg);	
		char temp[_MAX_STRING_LENGTH_];//"VER"
		infile>>temp>>number_ver;//version	
	}
	else
	{
		//To move to begin of file
		infile.seekg(0,ios::beg);//old version without version number(before ver 1.1)
	}


	//                  row = x = flight direction
	//                   ^
	//               |
	//|------------- |---------------|--------->col = y
	//|              |               |
	//|              |               |
	//|------------- |---------------|
	//|              |               |
	//|              |               |
	//|------------- |---------------|
	//
	//Flight direction = row = x
	//

	if((number_ver >= 1.0)&&(number_ver < 1.1))//12 parameters
	{
		RemoveCommentLine(infile, "!#", 2);
		infile>>IOParam[0]; // width of image (unit: pixel)
		RemoveCommentLine(infile, "!#", 2);
		infile>>IOParam[1];// height of image (unit: pixel)
		RemoveCommentLine(infile, "!#", 2);
		infile>>IOParam[2];// focal length of image (unit: pixel)
		
		RemoveCommentLine(infile, "!#", 2);//first scan line
		for(int i=0; i<6; i++)
		{
			infile>>EO1[i];
		}
		
		RemoveCommentLine(infile, "!#", 2);//last scan line
		for(int i=0; i<6; i++)
		{
			infile>>EO2[i];
		}
		
		double k[6];
		
		//to calculate angle velocity(rad/line): K4, K5, K6
		for(int i=0; i<3; i++)
		{
			double p = PHI;
			k[i+3] = (Deg2Rad(EO2[i])-Deg2Rad(EO1[i]))/IOParam[1];
		}
		//to calculate velocity(m/line): K1, K2, K3
		for(int i=0; i<3; i++)
		{
			k[i] = (EO2[i+3]-EO1[i+3])/IOParam[1];
		}
		
		//X0, K1, Y0, K2, Z0, K3
		for(int i=0; i<3; i++)
		{
			EOParam[i*2]   = EO1[i+3];
			EOParam[i*2+1] = k[i];
		}
		//Omega0, K4, Phi0, K5, Kappa0, K6
		for(int i=0; i<3; i++)
		{
			double p = PHI;
			//to convert degree to radian
			EOParam[6+i*2  ] = Deg2Rad(EO1[i]);
			EOParam[6+i*2+1] = k[i+3];
		}
		
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if((number_ver >= 1.1)&&(number_ver < 1.2))//IOP & EOP parameters
	{
		RemoveCommentLine(infile, "!#", 2);
		infile>>m_nEOPOption;
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>IOParam[0]; // width of image (unit: pixel)
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>IOParam[1];// height of image (unit: pixel)
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>IOParam[2];// focal length of image (unit: pixel)
		
		if(m_nEOPOption == 0)//orientation images
		{
			if(false == ReadOrientationImageParam(infile)) return false;
		}
		else if(m_nEOPOption == 1)//time function
		{
			if(false == ReadTimeFunctionParam(infile)) return false;
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if(number_ver >= 1.2)
	{
		RemoveCommentLine(infile, "!#", 2);
		infile>>m_nEOPOption;
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>IOParam[0]; // width of image
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>IOParam[1];// height of image
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>m_CCDSize;// CCD size
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>IOParam[2];// focal length of image
		IOParam[2] = IOParam[2]/m_CCDSize;
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>m_xp;// xp
		m_xp = m_xp/m_CCDSize;
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>m_yp;//yp
		m_yp = m_yp/m_CCDSize;
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>m_d;//d(scan line offset)
		m_d = m_d/m_CCDSize;
		
		if(m_nEOPOption == 0)//orientation images
		{
			if(false == ReadOrientationImageParam(infile)) return false;
		}
		else if(m_nEOPOption == 1)//time function
		{
			if(false == ReadTimeFunctionParam(infile)) return false;
		}
		else
			return false;
	}
	else
		return false;

	infile.close();

	num_OI = m_OIEOP.GetNumItem();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return true;
}

bool CSatOrthoImage::ReadOrientationImageParam(fstream &infile)
{
	do 
	{
		OIEOP oieop;
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) infile>>oieop.n_line;
		else return false;
		
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) infile>>oieop.O;
		else return false;	
		oieop.O = Deg2Rad(oieop.O);
		
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) infile>>oieop.P;
		else return false;	
		oieop.P = Deg2Rad(oieop.P);
		
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) infile>>oieop.K;
		else return false;	
		oieop.K = Deg2Rad(oieop.K);
		
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) infile>>oieop.X;
		else return false;	
		
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) infile>>oieop.Y;
		else return false;	
		
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) infile>>oieop.Z;
		else return false;	
		
		m_OIEOP.AddTail(oieop);
		
		RemoveCommentLine(infile, "!#", 2);
		infile>>ws;

	} while(!infile.eof());

	return true;
}

bool CSatOrthoImage::ReadTimeFunctionParam(fstream &infile)
{
	RemoveCommentLine(infile, "!#", 2);
	infile>>m_SEOP.n_Order;
	
	double val;
	for(int i=0; i<=m_SEOP.n_Order; i++)
	{
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) {infile>>val; val=Deg2Rad(val); m_SEOP.O.AddTail(val);}
		else return false;	
	}
	for(int i=0; i<=m_SEOP.n_Order; i++)
	{
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) {infile>>val; val=Deg2Rad(val); m_SEOP.P.AddTail(val);}
		else return false;
	}
	for(int i=0; i<=m_SEOP.n_Order; i++)
	{
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) {infile>>val; val=Deg2Rad(val); m_SEOP.K.AddTail(val);}
		else return false;
	}
	for(int i=0; i<=m_SEOP.n_Order; i++)
	{
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) {infile>>val; m_SEOP.X.AddTail(val);}
		else return false;
	}
	for(int i=0; i<=m_SEOP.n_Order; i++)
	{
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) {infile>>val; m_SEOP.Y.AddTail(val);}
		else return false;
	}
	for(int i=0; i<=m_SEOP.n_Order; i++)
	{
		RemoveCommentLine(infile, "!#", 2);
		if(!infile.eof()) {infile>>val; m_SEOP.Z.AddTail(val);}
		else return false;
	}
	
	RemoveCommentLine(infile, "!#", 2);
	infile>>ws;
	
	return true;
}

bool CSatOrthoImage::ReadGCPFile(CString path, CSMList<double> *LR, CSMList<double> *LC)
{
	fstream infile;
	fstream outfile;
	infile.open(path, ios::in);
	outfile.open("temp.txt", ios::out);

	double X, Y, Z;
	double Xs, Ys, Zs;
	double row, col;
	double init_r = 0;
	
	char stID[256];

	while(!infile.eof())
	{
		RemoveCommentLine(infile,'!');
		infile>>stID;
		RemoveCommentLine(infile, '!');
		infile>>X;
		RemoveCommentLine(infile, '!');
		infile>>Y;
		RemoveCommentLine(infile, '!');
		infile>>Z;

		GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

		LR->AddTail(col);
		LC->AddTail(row);

		outfile<<stID<<"\t"<<row<<"\t"<<col<<"\t"<<endl;

		init_r = row;
	}
	
	infile.close();
	outfile.close();

	return true;
}

bool CSatOrthoImage::Readlidardem(CString DEMPath, CString DEMHDRPath) 
{
	FILE *hdr;
	hdr = fopen(DEMHDRPath,"r");
	double offset, sX, sY;
	int w, h;
	double backval, falseval;
	
	if(EOF == fscanf(hdr,"%d",&w)) return false;
	if(EOF == fscanf(hdr,"%d",&h)) return false;
	if(EOF == fscanf(hdr,"%lf",&offset)) return false;
	if(EOF == fscanf(hdr,"%lf",&sX)) return false;
	if(EOF == fscanf(hdr,"%lf",&sY)) return false;
	if(EOF == fscanf(hdr,"%lf",&backval)) return false;
	if(EOF == fscanf(hdr,"%lf",&falseval)) return false;
	
	fclose(hdr);
	
	DEM_Path = DEMPath;
	m_DEM->SetConfig(w,h,sX,sY,offset,offset,falseval,backval);

	DEM_Path.MakeUpper();
	if(DEM_Path.Find(".DEM") > -1) return m_DEM->ReadDEM(DEM_Path);
	else if(DEM_Path.Find(".TXT") > -1) return m_DEM->ReadTextXYZDEM(DEM_Path);
	else return false;
}

bool CSatOrthoImage::ReadSorteddem(CString DEMPath, CString DEMHDRPath) 
{
	if(idx_x != NULL) {delete[] idx_x; idx_x = NULL;}
	if(idx_y != NULL) {delete[] idx_y; idx_y = NULL;}
	if(m_pData != NULL) {delete[] m_pData; m_pData = NULL;}

	FILE *hdr;
	hdr = fopen(DEMHDRPath,"r");
	double offset, sX, sY;
	int w, h;
	double backval, falseval;
	
	if(EOF == fscanf(hdr,"%d",&w)) return false;
	if(EOF == fscanf(hdr,"%d",&h)) return false;
	if(EOF == fscanf(hdr,"%lf",&offset)) return false;
	if(EOF == fscanf(hdr,"%lf",&sX)) return false;
	if(EOF == fscanf(hdr,"%lf",&sY)) return false;
	if(EOF == fscanf(hdr,"%lf",&backval)) return false;
	if(EOF == fscanf(hdr,"%lf",&falseval)) return false;
	
	fclose(hdr);

	DEM_Path = DEMPath;
	m_DEM->Resize(1,w,h);
	m_DEM->SetConfig(w,h,sX,sY,offset,offset,falseval,backval);

	//double* temp = new double[100];
	//m_DEM->CopyBuf(temp);

	m_pData = new float[m_DEM->GetHeight()*m_DEM->GetWidth()];
	idx_x = new int[m_DEM->GetHeight()*m_DEM->GetWidth()];
	idx_y = new int[m_DEM->GetHeight()*m_DEM->GetWidth()];
	
	fstream demfile;
	demfile.open(DEMPath,ios::in);

	int count = 0;
	while ( !demfile.eof() )
	{
		int x, y; short z;
		demfile>>x>>y>>z;
		
		m_pData[count] = z;
		idx_x[count] = x;
		idx_y[count] = y;

		count ++;
	}

	demfile.close();

	return true;
}

bool CSatOrthoImage::ResampleColorOrtho_Slope_DiffZ(CString OrthoPath, bool bBilinear, bool bOcclusion)
{
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),24);
	CColorPixelPtr ColorOrthoPixel(OrthoImage);
	CColorPixelPtr ColorOriginPixel(*OriginImage);

	CColorPixel black; black = 0;
	CColorPixel back;  back = 200;
	
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());
	
	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();
	
	double init_r = IOParam[1]/2;
	for(DWORD i=0;i<(DWORD)m_DEM->GetHeight();i++)
	{
		for(DWORD j=0;j<(DWORD)m_DEM->GetWidth();j++)
		{
			double X, Y, Z;
			double Xs, Ys, Zs;
			Point2D<double> a;
			
			X = m_DEM->GetX(j);
			Y = m_DEM->GetY(i);
			Z = m_DEM->GetZ(j,i);
			if((Z == m_DEM->val_fail)||(Z < 0.0))
			{
				black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				continue;
			}
			/**************************************************************/
			double row, col;
			
			if(false == GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r))
			{
				black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				continue;
			}

			a.x = col; a.y = row;
			a.x = double(int(col+0.5)); a.y = double(int(row+0.5));
			//Updating initial row number
			init_r = a.y;
			/**************************************************************/
			if(bBilinear != true)			
			{
				//Nearest Neighborhood
				if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
				{
					//
					//to remove occlusion
					//
					if(bOcclusion == true)
					{
						//Check Occlusion
						double Xs, Ys, Zs;
						
						if((number_ver >= 1.0)&&(number_ver < 1.1))
						{
							Xs = EOParam[0] + EOParam[1]*a.y;
							Ys = EOParam[2] + EOParam[3]*a.y;
							Zs = EOParam[4] + EOParam[5]*a.y;
						}
						else if(number_ver >= 1.1)
						{
							double tempO, tempP, tempK;
							if(false == FindEOP(a.y, tempO, tempP, tempK, Xs, Ys, Zs)) return false;
						}
						
						if(false == CheckOcclusion_DiffZ(Xs, Ys, Zs, j, i))
						{
							black >> ColorOrthoPixel[LONG(i)][LONG(j)];//"black"
						}
						else
						{
							CColorPixel p;
							p << ColorOriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
							p >> ColorOrthoPixel[LONG(i)][LONG(j)];
						}
					}
					//
					//not to remove occlusion
					//
					else
					{
						CColorPixel p;
						p << ColorOriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
						p >> ColorOrthoPixel[LONG(i)][LONG(j)];
					}
				}
				else//out of range of original image
				{
					black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				}
			}
			else
			{
				
				//Bilinear Interpolation
				if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
				{
					//
					//to remove occlusion
					//
					if(bOcclusion == true)
					{
						//Check Occlusion
						double Xs, Ys, Zs;
						
						if((number_ver >= 1.0)&&(number_ver < 1.1))
						{
							Xs = EOParam[0] + EOParam[1]*a.y;
							Ys = EOParam[2] + EOParam[3]*a.y;
							Zs = EOParam[4] + EOParam[5]*a.y;
						}
						else if(number_ver >= 1.1)
						{
							double tempO, tempP, tempK;
							if(false == FindEOP(a.y, tempO, tempP, tempK, Xs, Ys, Zs)) return false;
						}
						
						if(false == CheckOcclusion_DiffZ(Xs, Ys, Zs, j, i))
						{
							black >> ColorOrthoPixel[LONG(i)][LONG(j)];//"black"
						}
						else
						{
							CColorPixel value1, value2, value3, value4;
							value1 << ColorOriginPixel[LONG(a.y)][LONG(a.x)];
							value2 << ColorOriginPixel[LONG(a.y)][LONG(a.x+1)];
							value3 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x)];
							value4 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x+1)];
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> ColorOrthoPixel[LONG(i)][LONG(j)];
						}
					}
					//
					//not to remove occlusion
					//
					else
					{
						CColorPixel value1, value2, value3, value4;
						value1 << ColorOriginPixel[LONG(a.y)][LONG(a.x)];
						value2 << ColorOriginPixel[LONG(a.y)][LONG(a.x+1)];
						value3 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x)];
						value4 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x+1)];
						
						double A, B, C, D;
						A = (double)((int)a.x + 1) - a.x;
						B = 1 - A;
						C = (double)((int)a.y + 1) - a.y;
						D = 1 - C;
						
						(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> ColorOrthoPixel[LONG(i)][LONG(j)];
					}
				}
				else
				{
					black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				}
			}
		}
		bar.StepIt();
	}
	
	if(FALSE == OrthoImage.Save(OrthoPath)) return false;
	
	return true;
}

bool CSatOrthoImage::ResampleOrtho_Slope_DiffZ(CString OrthoPath, bool bBilinear, bool bOcclusion)
{
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),8);
	CPixelPtr OrthoPixel(OrthoImage);
	CPixelPtr OriginPixel(*OriginImage);
	
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());
	
	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();
	
	double init_r = IOParam[1]/2;
	for(DWORD i=0;i<(DWORD)m_DEM->GetHeight();i++)
	{
		for(DWORD j=0;j<(DWORD)m_DEM->GetWidth();j++)
		{
			double X, Y, Z;
			double Xs, Ys, Zs;
			Point2D<double> a;
			
			X = m_DEM->GetX(j);
			Y = m_DEM->GetY(i);
			Z = m_DEM->GetZ(j,i);
			if((Z == m_DEM->val_fail  || Z == m_DEM->val_back)||(Z < 0.0))
			{
				OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				continue;
			}
			/**************************************************************/
			double row, col;
			
			if(false == GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r))
			{
				OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				continue;
			}
			
			a.x = col; a.y = row;
			a.x = double(int(col+0.5)); a.y = double(int(row+0.5));
			//Updating initial row number
			init_r = a.y;
			/**************************************************************/
			if(bBilinear != true)			
			{
				//Nearest Neighborhood
				if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
				{
					//
					//to remove occlusion
					//
					if(bOcclusion == true)
					{
						//Check Occlusion
						double Xs, Ys, Zs;
						
						if((number_ver >= 1.0)&&(number_ver < 1.1))
						{
							Xs = EOParam[0] + EOParam[1]*a.y;
							Ys = EOParam[2] + EOParam[3]*a.y;
							Zs = EOParam[4] + EOParam[5]*a.y;
						}
						else if(number_ver >= 1.1)
						{
							double tempO, tempP, tempK;
							if(false == FindEOP(a.y, tempO, tempP, tempK, Xs, Ys, Zs)) return false;
						}
						
						if(false == CheckOcclusion_DiffZ(Xs, Ys, Zs, j, i))
						{
							OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//"black"
						}
						else
						{
							OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
						}
					}
					//
					//not to remove occlusion
					//
					else
					{
						OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
					}
				}
				else//out of range of original image
				{
					OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				}
			}
			else
			{
				
				//Bilinear Interpolation
				if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
				{
					//
					//to remove occlusion
					//
					if(bOcclusion == true)
					{
						//Check Occlusion
						double Xs, Ys, Zs;
						
						if((number_ver >= 1.0)&&(number_ver < 1.1))
						{
							Xs = EOParam[0] + EOParam[1]*a.y;
							Ys = EOParam[2] + EOParam[3]*a.y;
							Zs = EOParam[4] + EOParam[5]*a.y;
						}
						else if(number_ver >= 1.1)
						{
							double tempO, tempP, tempK;
							if(false == FindEOP(a.y, tempO, tempP, tempK, Xs, Ys, Zs)) return false;
						}
						
						if(false == CheckOcclusion_DiffZ(Xs, Ys, Zs, j, i))
						{
							OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//"black"
						}
						else
						{
							double value1, value2, value3, value4;
							value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
							value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
							value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
							value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
						}
					}
					//
					//not to remove occlusion
					//
					else
					{
						double value1, value2, value3, value4;
						value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
						value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
						value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
						value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
						
						double A, B, C, D;
						A = (double)((int)a.x + 1) - a.x;
						B = 1 - A;
						C = (double)((int)a.y + 1) - a.y;
						D = 1 - C;
						
						OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
					}
				}
				else
				{
					OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				}
			}
		}
		bar.StepIt();
	}
	
	if(FALSE == OrthoImage.Save(OrthoPath)) return false;
	
	return true;
}

bool CSatOrthoImage::ResampleOrtho_Slope_Angle(CString OrthoPath, bool bBilinear, bool bOcclusion)
{
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),8);
	CPixelPtr OrthoPixel(OrthoImage);
	CPixelPtr OriginPixel(*OriginImage);
	
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());
	
	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();
	
	double init_r = IOParam[1]/2;
	for(DWORD i=0;i<(DWORD)m_DEM->GetHeight();i++)
	{
		for(DWORD j=0;j<(DWORD)m_DEM->GetWidth();j++)
		{
			double X, Y, Z;
			double Xs, Ys, Zs;
			Point2D<double> a;
			
			X = m_DEM->GetX(j);
			Y = m_DEM->GetY(i);
			Z = m_DEM->GetZ(j,i);
			if((Z == m_DEM->val_fail)||(Z < 0.0))
			{
				OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				continue;
			}
			/**************************************************************/
			double row, col;
			
			GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

			a.x = col; a.y = row;
			a.x = double(int(col+0.5)); a.y = double(int(row+0.5));
			//Updating initial row number
			init_r = a.y;
			/**************************************************************/
			if(bBilinear != true)			
			{
				//Nearest Neighborhood
				if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
				{
					//
					//to remove occlusion
					//
					if(bOcclusion == true)
					{
						//Check Occlusion
						double Xs, Ys, Zs;
						
						if((number_ver >= 1.0)&&(number_ver < 1.1))
						{
							Xs = EOParam[0] + EOParam[1]*a.y;
							Ys = EOParam[2] + EOParam[3]*a.y;
							Zs = EOParam[4] + EOParam[5]*a.y;
						}
						else if(number_ver >= 1.1)
						{
							double tempO, tempP, tempK;
							if(false == FindEOP(a.y, tempO, tempP, tempK, Xs, Ys, Zs)) return false;
						}
						
						if(false == CheckOcclusion_Angle(Xs, Ys, Zs, j, i))
						{
							OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//"black"
						}
						else
						{
							OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
						}
					}
					//
					//not to remove occlusion
					//					
					else
					{
						OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
					}
				}
				else
				{
					OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				}
			}
			else
			{
				
				//Bilinear Interpolation
				if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
				{
					//
					//to remove occlusion
					//
					if(bOcclusion == true)
					{
						//Check Occlusion
						double Xs, Ys, Zs;
						
						if((number_ver >= 1.0)&&(number_ver < 1.1))
						{
							Xs = EOParam[0] + EOParam[1]*a.y;
							Ys = EOParam[2] + EOParam[3]*a.y;
							Zs = EOParam[4] + EOParam[5]*a.y;
						}
						else if(number_ver >= 1.1)
						{
							double tempO, tempP, tempK;
							if(false == FindEOP(a.y, tempO, tempP, tempK, Xs, Ys, Zs)) return false;
						}
						
						if(false == CheckOcclusion_Angle(Xs, Ys, Zs, j, i))
						{
							OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//"black"
						}
						else
						{
							double value1, value2, value3, value4;
							value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
							value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
							value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
							value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
						}
					}
					//
					//not to remove occlusion
					//
					else
					{
						double value1, value2, value3, value4;
						value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
						value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
						value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
						value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
						
						double A, B, C, D;
						A = (double)((int)a.x + 1) - a.x;
						B = 1 - A;
						C = (double)((int)a.y + 1) - a.y;
						D = 1 - C;
						
						OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
					}
				}
				else
				{
					OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				}
			}
		}
		bar.StepIt();
	}
	
	if(FALSE == OrthoImage.Save(OrthoPath)) return false;
	
	return true;
}

bool CSatOrthoImage::ResampleColorOrtho_Slope_Angle(CString OrthoPath, bool bBilinear, bool bOcclusion)
{
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),24);
	CColorPixelPtr ColorOrthoPixel(OrthoImage);
	CColorPixelPtr ColorOriginPixel(*OriginImage);
	CColorPixel black; black = 0;
	CColorPixel back;  back = 200;
	
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());
	
	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();
	
	double init_r = IOParam[1]/2;
	for(DWORD i=0;i<(DWORD)m_DEM->GetHeight();i++)
	{
		for(DWORD j=0;j<(DWORD)m_DEM->GetWidth();j++)
		{
			double X, Y, Z;
			double Xs, Ys, Zs;
			Point2D<double> a;
			
			X = m_DEM->GetX(j);
			Y = m_DEM->GetY(i);
			Z = m_DEM->GetZ(j,i);
			if((Z == m_DEM->val_fail)||(Z < 0.0))
			{
				black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				continue;
			}
			/**************************************************************/
			double row, col;
			
			GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

			a.x = col; a.y = row;
			a.x = double(int(col+0.5)); a.y = double(int(row+0.5));
			//Updating initial row number
			init_r = a.y;
			/**************************************************************/
			if(bBilinear != true)			
			{
				//Nearest Neighborhood
				if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
				{
					//
					//to remove occlusion
					//
					if(bOcclusion == true)
					{
						//Check Occlusion
						double Xs, Ys, Zs;
						
						if((number_ver >= 1.0)&&(number_ver < 1.1))
						{
							Xs = EOParam[0] + EOParam[1]*a.y;
							Ys = EOParam[2] + EOParam[3]*a.y;
							Zs = EOParam[4] + EOParam[5]*a.y;
						}
						else if(number_ver >= 1.1)
						{
							double tempO, tempP, tempK;
							if(false == FindEOP(a.y, tempO, tempP, tempK, Xs, Ys, Zs)) return false;
						}
						
						if(false == CheckOcclusion_Angle(Xs, Ys, Zs, j, i))
						{
							black >> ColorOrthoPixel[LONG(i)][LONG(j)];//"black"
						}
						else
						{
							CColorPixel p;
							p << ColorOriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
							p >> ColorOrthoPixel[LONG(i)][LONG(j)];
						}
					}
					//
					//not to remove occlusion
					//					
					else
					{
						CColorPixel p;
						p << ColorOriginPixel[LONG(a.y+0.5)][LONG(a.x+0.5)];
						p >> ColorOrthoPixel[LONG(i)][LONG(j)];
					}
				}
				else
				{
					black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				}
			}
			else
			{
				
				//Bilinear Interpolation
				if(((a.x > 0) && (a.y >0)) && ((a.x < W-1) && (a.y < H-1)))
				{
					//
					//to remove occlusion
					//
					if(bOcclusion == true)
					{
						//Check Occlusion
						double Xs, Ys, Zs;
						
						if((number_ver >= 1.0)&&(number_ver < 1.1))
						{
							Xs = EOParam[0] + EOParam[1]*a.y;
							Ys = EOParam[2] + EOParam[3]*a.y;
							Zs = EOParam[4] + EOParam[5]*a.y;
						}
						else if(number_ver >= 1.1)
						{
							double tempO, tempP, tempK;
							if(false == FindEOP(a.y, tempO, tempP, tempK, Xs, Ys, Zs)) return false;
						}
						
						if(false == CheckOcclusion_Angle(Xs, Ys, Zs, j, i))
						{
							black >> ColorOrthoPixel[LONG(i)][LONG(j)];//"black"
						}
						else
						{
							CColorPixel value1, value2, value3, value4;
							value1 << ColorOriginPixel[LONG(a.y)][LONG(a.x)];
							value2 << ColorOriginPixel[LONG(a.y)][LONG(a.x+1)];
							value3 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x)];
							value4 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x+1)];
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							 (value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> ColorOrthoPixel[LONG(i)][LONG(j)];
						}
					}
					//
					//not to remove occlusion
					//
					else
					{
						CColorPixel value1, value2, value3, value4;
						value1 << ColorOriginPixel[LONG(a.y)][LONG(a.x)];
						value2 << ColorOriginPixel[LONG(a.y)][LONG(a.x+1)];
						value3 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x)];
						value4 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x+1)];
						
						double A, B, C, D;
						A = (double)((int)a.x + 1) - a.x;
						B = 1 - A;
						C = (double)((int)a.y + 1) - a.y;
						D = 1 - C;
						
						(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> ColorOrthoPixel[LONG(i)][LONG(j)];
					}
				}
				else
				{
					black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				}
			}
		}
		bar.StepIt();
	}
	
	if(FALSE == OrthoImage.Save(OrthoPath)) return false;
	
	return true;
}

bool CSatOrthoImage::ResampleOrtho_ZBuffer_Height(CString OrthoPath, bool bBilinear)
{
	//Empty ortho-image
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),8);
	CPixelPtr OrthoPixel(OrthoImage);
	CPixelPtr OriginPixel(*OriginImage);
	
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());

	//
	//buffer size: h, w
	//target boundary: sr, sc, er, ec
	//start row, start col, end row, end col
	//
	int h, w, sr, sc, er, ec;
	if( false == GetTargetBoundary(h, w, sr, sc, er, ec) )
		return false;
	
	//
	//Create buffer for z-buffer
	//
	double tempdist, SCALE;
	
	double O0, P0, K0, X0, Y0, Z0;
	FindEOP(0, O0, P0, K0, X0, Y0, Z0);
	double dX = X0;
	double dY = Y0;
	double dZ = Z0;
	
	tempdist = sqrt(dX*dX+dY*dY+dZ*dZ);
	
	SCALE = tempdist * 2.;
	
	//index: image pixel index(int) + distance index(<1.0)
	//therefor, these two indecis can resotred in the same memory
	double init_indexval;
	MakeIndex(m_DEM->GetWidth(),SCALE,init_indexval,FIRST_RECORD,FIRST_RECORD,0.);
	CSMGridDEM<double> indexmap(w,h, (double)init_indexval);
	
	//
	//Initial approximate scanner line number
	//
	double init_r = IOParam[1]/2;
	//
	//Image point coordinates
	//
	Point2D<double> a;
	
	//Width and height of original image
	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();
	
	for(DWORD i=0;i<(DWORD)m_DEM->GetHeight();i++)
	{
		for(DWORD j=0;j<(DWORD)m_DEM->GetWidth();j++)
		{
			//DEM point
			double X, Y, Z;
			//perspective center of each line
			double Xs, Ys, Zs;

			X = m_DEM->GetX(j);
			Y = m_DEM->GetY(i);
			Z = m_DEM->GetZ(j,i);
			if((Z == m_DEM->val_fail)||(Z < 0.0))
			{
				OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				continue;
			}
			/********************************************/
			double row, col;
			
			GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

			a.x = col; a.y = row;
			int ir, ic;
			ir = int(a.y + 0.5); ic = int(a.x + 0.5);
			//Updating initial row number
			init_r = ir;
			/********************************************/
			
			//
			//Nearest neighborhood method
			//
			if(bBilinear != true)			
			{
				//Nearest Neighborhood
				if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))//original image boundary check
				{
					int tr, tc;
					tr = ir-sr;
					tc = ic-sc;
					
					double indexval;
					int r_index, c_index;
					double oldZ;
					
					if(false == indexmap.GetBuf(tc,tr,indexval))
					{
						continue;
					}
					
					AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,r_index,c_index,oldZ);

					if(r_index == FIRST_RECORD)//the first recording
					{
						//to record row index, col index and distance
						double indexval;
						MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,Z);
						if(false == indexmap.SetBuf(tc,tr,indexval))
						{
							continue;
						}
						
						//to assign pixel value to ortho image
						OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(ir)][LONG(ic)];
					}
					else//if this recording is not the first time, compare two distances
					{
						double newZ = Z;//new Z
						
						if(oldZ>newZ)//new one is longer than old one(occlusion)
						{
							OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//corresponding ortho image pixel is "black"
						}
						else//old one is longer than new one
						{
							//ortho image pixel which corresponds to old distance is "black".(occlusion)
							OrthoPixel[(LONG)r_index][(LONG)c_index] = (BYTE)0;//occlusion(black)
							
							//to assign pixel value to ortho image pixel which corresponds to new distance
							OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(ir)][LONG(ic)];
							
							//to record row index, col index and distance
							double indexval;
							MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newZ);
							if(false == indexmap.SetBuf(tc,tr,indexval))
							{
								continue;
							}
						}
					}					
				}
				else//out of original image region
				{
					OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				}
			}
			//
			//Bininear resampling method
			//
			else
			{
				//Bilinear
				if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))
				{
					int tr, tc;
					tr = ir-sr;
					tc = ic-sc;
					
					double indexval;
					int r_index, c_index;
					double oldZ;
					
					if(false == indexmap.GetBuf(tc,tr,indexval))
					{
						continue;
					}
					AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,r_index,c_index,oldZ);

					if(r_index == FIRST_RECORD)
					{
						double indexval;
						MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,Z);
						if(false == indexmap.SetBuf(tc,tr,indexval))
						{
							continue;
						}
						
						//
						//Bilinear interpolation
						//
						double value1, value2, value3, value4;
						value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
						value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
						value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
						value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
						
						double A, B, C, D;
						A = (double)((int)a.x + 1) - a.x;
						B = 1 - A;
						C = (double)((int)a.y + 1) - a.y;
						D = 1 - C;
						
						OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
						
					}
					else
					{
						double newZ = Z;
						
						if(oldZ>newZ)
						{
							OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
						}
						else
						{
							OrthoPixel[(LONG)r_index][(LONG)c_index] = (BYTE)0;//occlusion(black)
							
							double value1, value2, value3, value4;
							value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
							value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
							value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
							value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
							
							double indexval;
							MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newZ);
							if(false == indexmap.SetBuf(tc,tr,indexval))
							{
								continue;
							}
						}
					}					
				}
				else
				{
					OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				}
			}
		}
		bar.StepIt();
	}
	
	if(FALSE == OrthoImage.Save(OrthoPath)) return false;
	
	return true;
}

bool CSatOrthoImage::ResampleColorOrtho_ZBuffer_Height(CString OrthoPath, bool bBilinear)
{
	//Empty ortho-image
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),24);
	CColorPixelPtr ColorOrthoPixel(OrthoImage);
	CColorPixelPtr ColorOriginPixel(*OriginImage);

	CColorPixel black; black = 0;
		
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());

	//
	//buffer size: h, w
	//target boundary: sr, sc, er, ec
	//start row, start col, end row, end col
	//
	int h, w, sr, sc, er, ec;
	if( false == GetTargetBoundary(h, w, sr, sc, er, ec) ) 
		return false;
	
	//
	//Create buffer for z-buffer
	//
	double tempdist, SCALE;
	
	double O0, P0, K0, X0, Y0, Z0;
	FindEOP(0, O0, P0, K0, X0, Y0, Z0);
	double dX = X0;
	double dY = Y0;
	double dZ = Z0;
	
	tempdist = sqrt(dX*dX+dY*dY+dZ*dZ);
	
	SCALE = tempdist * 2.;
	
	//index: image pixel index(int) + distance index(<1.0)
	//therefor, these two indecis can resotred in the same memory
	double init_indexval;
	MakeIndex(m_DEM->GetWidth(),SCALE,init_indexval,FIRST_RECORD,FIRST_RECORD,0.);
	CSMGridDEM<double> indexmap(w,h, (double)init_indexval);
	
	//
	//Initial approximate scanner line number
	//
	double init_r = IOParam[1]/2;
	//
	//Image point coordinates
	//
	Point2D<double> a;
	
	//Width and height of original image
	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();
	
	for(DWORD i=0;i<(DWORD)m_DEM->GetHeight();i++)
	{
		for(DWORD j=0;j<(DWORD)m_DEM->GetWidth();j++)
		{
			//DEM point
			double X, Y, Z;
			//perspective center of each line
			double Xs, Ys, Zs;

			X = m_DEM->GetX(j);
			Y = m_DEM->GetY(i);
			Z = m_DEM->GetZ(j,i);
			if((Z == m_DEM->val_fail)||(Z < 0.0))
			{
				black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				continue;
			}
			/********************************************/
			double row, col;
			GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

			a.x = col; a.y = row;
			int ir, ic;
			ir = int(a.y + 0.5); ic = int(a.x + 0.5);
			//Updating initial row number
			init_r = ir;
			/********************************************/
			
			//
			//Nearest neighborhood method
			//
			if(bBilinear != true)			
			{
				//Nearest Neighborhood
				if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))//original image boundary check
				{
					int tr, tc;
					tr = ir-sr;
					tc = ic-sc;
					
					double indexval;
					int r_index, c_index;
					double oldZ;
					
					if(false == indexmap.GetBuf(tc,tr,indexval))
					{
						continue;
					}
					
					AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,r_index,c_index,oldZ);

					if(r_index == FIRST_RECORD)//the first recording
					{
						//to record row index, col index and distance
						double indexval;
						MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,Z);
						if(false == indexmap.SetBuf(tc,tr,indexval))
						{
							continue;
						}
						
						//to assign pixel value to ortho image
						CColorPixel p;
						p << ColorOriginPixel[LONG(ir)][LONG(ic)];
						p >> ColorOrthoPixel[LONG(i)][LONG(j)];
					}
					else//if this recording is not the first time, compare two distances
					{
						double newZ = Z;//new Z
						
						if(oldZ>newZ)//new one is longer than old one(occlusion)
						{
							black >> ColorOrthoPixel[LONG(i)][LONG(j)];//corresponding ortho image pixel is "black"
						}
						else//old one is longer than new one
						{
							//ortho image pixel which corresponds to old distance is "black".(occlusion)
							black >> ColorOrthoPixel[(LONG)r_index][(LONG)c_index];//occlusion(black)
							
							//to assign pixel value to ortho image pixel which corresponds to new distance
							CColorPixel p;
							p << ColorOriginPixel[LONG(ir)][LONG(ic)];
							p >> ColorOrthoPixel[LONG(i)][LONG(j)];
							
							//to record row index, col index and distance
							double indexval;
							MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newZ);
							if(false == indexmap.SetBuf(tc,tr,indexval))
							{
								continue;
							}

						}
					}					
				}
				else//out of original image region
				{
					black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				}
			}
			//
			//Bininear resampling method
			//
			else
			{
				//Bilinear
				if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))
				{
					int tr, tc;
					tr = ir-sr;
					tc = ic-sc;
					
					double indexval;
					int r_index, c_index;
					double oldZ;
					if(false == indexmap.GetBuf(tc,tr,indexval))
					{
						continue;
					}
					AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,r_index,c_index,oldZ);

					if(r_index == FIRST_RECORD)
					{
						double indexval;
						MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,Z);
						if(false == indexmap.SetBuf(tc,tr,indexval))
						{
							continue;
						}

						//
						//Bilinear interpolation
						//
						CColorPixel value1, value2, value3, value4;
						value1 << ColorOriginPixel[LONG(a.y)][LONG(a.x)];
						value2 << ColorOriginPixel[LONG(a.y)][LONG(a.x+1)];
						value3 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x)];
						value4 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x+1)];
						
						double A, B, C, D;
						A = (double)((int)a.x + 1) - a.x;
						B = 1 - A;
						C = (double)((int)a.y + 1) - a.y;
						D = 1 - C;
						
						(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> ColorOrthoPixel[LONG(i)][LONG(j)];
						
					}
					else
					{
						double newZ = Z;
						
						if(oldZ>newZ)
						{
							black >> ColorOrthoPixel[LONG(i)][LONG(j)];
						}
						else
						{
							black >> ColorOrthoPixel[(LONG)r_index][(LONG)c_index];//occlusion(black)
							
							CColorPixel value1, value2, value3, value4;
							value1 << ColorOriginPixel[LONG(a.y)][LONG(a.x)];
							value2 << ColorOriginPixel[LONG(a.y)][LONG(a.x+1)];
							value3 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x)];
							value4 << ColorOriginPixel[LONG(a.y+1)][LONG(a.x+1)];
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> ColorOrthoPixel[LONG(i)][LONG(j)];
							
							double indexval;
							MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newZ);
							if(false == indexmap.SetBuf(tc,tr,indexval))
							{
								continue;
							}
						}
					}					
				}
				else
				{
					black >> ColorOrthoPixel[LONG(i)][LONG(j)];
				}
			}
		}
		bar.StepIt();
	}
	
	if(FALSE == OrthoImage.Save(OrthoPath)) return false;
	
	return true;
}

bool CSatOrthoImage::ResampleOrtho_EnhancedZBuffer(CString OrthoPath, bool bBilinear, bool bEnhanced)
{
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),8);
	CPixelPtr OrthoPixel(OrthoImage);
	CPixelPtr OriginPixel(*OriginImage);
	
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());
	
	int h, w, sr, sc, er, ec;
	if( false == GetTargetBoundary(h, w, sr, sc, er, ec) ) 
		return false;
	//
	//Create buffer for z-buffer
	//
	double tempdist, SCALE;
	
	double O0, P0, K0, X0, Y0, Z0;
	FindEOP(0, O0, P0, K0, X0, Y0, Z0);
	double dX = X0;
	double dY = Y0;
	double dZ = Z0;
	
	tempdist = sqrt(dX*dX+dY*dY+dZ*dZ);
	
	SCALE = tempdist * 2.;
	
	//index: image pixel index(int) + distance index(<1.0)
	//therefor, these two indecis can resotred in the same memory
	
	double init_indexval;
	MakeIndex(m_DEM->GetWidth(),SCALE,init_indexval,FIRST_RECORD,FIRST_RECORD,0.);
	CSMGridDEM<double> indexmap(w,h, (double)init_indexval);
	
	//
	//Initial approximate scanner line number
	//
	double init_r = IOParam[1]/2;
	//
	//Image point coordinates
	//
	Point2D<double> a;
	
	//Width and height of original image
	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();

	double SearchInterval;
	SearchInterval = m_DEM->resolution_X;
	
	for(DWORD i=0;i<(DWORD)m_DEM->GetHeight();i++)
	{
		for(DWORD j=0;j<(DWORD)m_DEM->GetWidth();j++)
		{
			//DEM point
			double X, Y, Z;
			//perspective center of each line
			double Xs, Ys, Zs;

			X = m_DEM->GetX(j);
			Y = m_DEM->GetY(i);
			Z = m_DEM->GetZ(j,i);
			if((Z == m_DEM->val_fail)||(Z < 0.0))
			{
				OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
				continue;
			}
			/////////////////////////////////////////////////////////////////////
			//Searching region (pseudo DEM)
			//
			double min_neighbor = Z;
			int nei_index=0;
			double MaxDiffZ;
			
			double SearchInterval;

			if(bEnhanced == true)
			{
				SearchInterval = m_DEM->resolution_X;

				double row, col;
				
				GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

				if(false == CalSInterval(SearchInterval, row, col))
				{
					continue;
				}
				else
				{
					SearchInterval *= 0.5;
				}

				for(int nei_x=-1; nei_x<1; nei_x++)
				{
					for(int nei_y=-1; nei_y<1; nei_y++)
					{
						int index_x = nei_x+j;
						int index_y = nei_y+i;
						if(index_x<0) continue;
						if(index_y<0) continue;
						if(index_x>=m_DEM->GetWidth()) continue;
						if(index_y>=m_DEM->GetHeight()) continue;
						
						double temp = m_DEM->GetZ(index_x,index_y);
												
						if(temp == m_DEM->val_fail) continue;
						
						if(temp<min_neighbor) min_neighbor = temp;
					}
				}
				MaxDiffZ = Z - min_neighbor;
				if(MaxDiffZ<SearchInterval) MaxDiffZ = SearchInterval;
			}
			else
			{
				MaxDiffZ = SearchInterval;
			}
			///////////////////////////////////////////////////////////////////////

			for(int k=0; k>int(-MaxDiffZ/SearchInterval-0.5); k--)
			{
				Z = Z + k*SearchInterval; //SearchInterval 만큼씩 조정해가며 검색.
				if(Z<MinZ) break;
				
				/********************************************/
				double row, col;
				
				GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

				a.x = col; a.y = row;
				int ir, ic;
				ir = int(a.y + 0.5); ic = int(a.x + 0.5);//최근접 화소 선택
				//Updating initial row number
				init_r = ir;
				/********************************************/

				if(bBilinear != true)//Nearest
				{
					//Nearest Neighborhood
					if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))
					{
						int tr, tc;
						tr = ir-sr;
						tc = ic-sc;
						
						double indexval;
						int row_index, col_index;
						double dist_index;
						
						if(false == indexmap.GetBuf(tc,tr,indexval))
						{
							continue;
						}
						
						AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,row_index,col_index,dist_index);
						
						if(row_index == FIRST_RECORD)
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double dist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								double indexval;
								MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,dist);
								if(false == indexmap.SetBuf(tc,tr,indexval))
								{
									continue;
								}
								
								OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(ir)][LONG(ic)];
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double dist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								double indexval;
								MakeIndex(m_DEM->GetWidth(),SCALE,indexval,-888,-888,dist);
								if(false == indexmap.SetBuf(tc,tr,indexval))
								{
									continue;
								}
							}							
						}//FIRST_RECORD(initial points)
						else if(row_index == -888)
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								double indexval;
								int row_index, col_index;
								double dist_index;
								if(false == indexmap.GetBuf(tc,tr,indexval))
								{
									continue;
								}
								AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,row_index,col_index,dist_index);
								
								if(dist_index<newdist)
								{
									OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//건물 중간에 가리워짐.
								}
								else
								{
									OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(ir)][LONG(ic)];
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								if(dist_index<newdist)
								{
								}
								else
								{
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
									//건물 중간으로 가리는 지역 업데이트.(거리 업데이트)
								}
							}	
						}//-888(building mid points)
						else
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								if(dist_index<newdist)
								{
									OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//건물 상단으로 다른 건물 상단을 가림.
								}
								else
								{
									OrthoPixel[(LONG)row_index][(LONG)col_index] = (BYTE)0;
									OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(ir)][LONG(ic)];
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								if(dist_index<newdist)
								{
								}
								else
								{
									OrthoPixel[(LONG)row_index][(LONG)col_index] = (BYTE)0;//다른 검물 중간부분으로 가리워짐.occlusion(black)
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,-888,-888,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
						}					
					}
					else
					{
						OrthoPixel[LONG(i)][LONG(j)] = (BYTE)BACKGROUND;
					}
				}//Nearest
				else//Bilinear
				{				
					//Bilinear
					if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))
					{
						int tr, tc;
						tr = ir-sr;
						tc = ic-sc;
						
						double indexval;
						int row_index, col_index;
						double dist_index;
						
						if(false == indexmap.GetBuf(tc,tr,indexval))
						{
							continue;
						}
						AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,row_index,col_index,dist_index);

						if(row_index == FIRST_RECORD)
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double dist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								double indexval;
								MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,dist);
								if(false == indexmap.SetBuf(tc,tr,indexval))
								{
									continue;
								}

								double value1, value2, value3, value4;
								value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
								value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
								value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
								value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
								
								double A, B, C, D;
								A = (double)((int)a.x + 1) - a.x;
								B = 1 - A;
								C = (double)((int)a.y + 1) - a.y;
								D = 1 - C;
								
								OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double dist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								double indexval;
								MakeIndex(m_DEM->GetWidth(),SCALE,indexval,-888,-888,dist);
								if(false == indexmap.SetBuf(tc,tr,indexval))
								{
									continue;
								}

							}							
						}//FIRST_RECORD(initial points)
						else if(row_index == -888)
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								double indexval;
								int row_index, col_index;
								double dist_index;
								if(false == indexmap.GetBuf(tc,tr,indexval))
								{
									continue;
								}
								AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,row_index,col_index,dist_index);

								if(dist_index<newdist)
								{
									OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//건물 중간에 가리워짐.
								}
								else
								{
									double value1, value2, value3, value4;
									value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
									value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
									value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
									value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
									
									double A, B, C, D;
									A = (double)((int)a.x + 1) - a.x;
									B = 1 - A;
									C = (double)((int)a.y + 1) - a.y;
									D = 1 - C;
									
									OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								if(dist_index<newdist)
								{
								}
								else
								{
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}	
						}//-888(building mid points)
						else
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								if(dist_index<newdist)
								{
									OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//다른 DEM에 의해 가리워짐(pseudo DEM 아님.). 일반적인 폐색.
								}
								else
								{
									OrthoPixel[(LONG)row_index][(LONG)col_index] = (BYTE)0;

									double value1, value2, value3, value4;
									value1 = (double)OriginPixel[LONG(a.y)][LONG(a.x)];
									value2 = (double)OriginPixel[LONG(a.y)][LONG(a.x+1)];
									value3 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x)];
									value4 = (double)OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
									
									double A, B, C, D;
									A = (double)((int)a.x + 1) - a.x;
									B = 1 - A;
									C = (double)((int)a.y + 1) - a.y;
									D = 1 - C;
									
									OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								if(dist_index<newdist)
								{
								}
								else
								{
									OrthoPixel[(LONG)row_index][(LONG)col_index] = (BYTE)0;//다른 검물 중간부분으로 가리워짐.occlusion(black)
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,-888,-888,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
						}					
					}
					else
					{
						OrthoPixel[LONG(i)][LONG(j)] = (BYTE)BACKGROUND;
					}
				}//Bilinear
			}//for(k)			
			
			
		}//for(j)
		bar.StepIt();
	}//for(i)

	if(FALSE == OrthoImage.Save(OrthoPath)) return false;
	
	return true;
}

bool CSatOrthoImage::ResampleColorOrtho_EnhancedZBuffer(CString OrthoPath, bool bBilinear, bool bEnhanced)
{
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),24);
	CColorPixelPtr OrthoPixel(OrthoImage);
	CColorPixelPtr OriginPixel(*OriginImage);

	CColorPixel black; black = 0;
	CColorPixel red; red=0; red.SetR(0);
	CColorPixel blue; blue=0; blue.SetB(0);
	CColorPixel green; green=0; green.SetG(0);
	CColorPixel yellow; yellow=0; yellow.SetG(0); yellow.SetR(0);
	
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());
	
	int h, w, sr, sc, er, ec;
	if( false == GetTargetBoundary(h, w, sr, sc, er, ec) ) 
		return false;
	
	//
	//Create buffer for z-buffer
	//
	
	double tempdist, SCALE;
	
	double O0, P0, K0, X0, Y0, Z0;
	FindEOP(0, O0, P0, K0, X0, Y0, Z0);
	double dX = X0;
	double dY = Y0;
	double dZ = Z0;
	
	tempdist = sqrt(dX*dX+dY*dY+dZ*dZ);
	
	SCALE = tempdist * 2.0;
	
	//index: image pixel index(int) + distance index(<1.0)
	//therefor, these two indecis can resotred in the same memory
	
	double init_indexval;
	MakeIndex(m_DEM->GetWidth(),SCALE,init_indexval,FIRST_RECORD,FIRST_RECORD,0.);
	CSMGridDEM<double> indexmap(w,h, (double)init_indexval);
	
	//
	//Initial approximate scanner line number
	//
	double init_r = IOParam[1]/2;
	//
	//Image point coordinates
	//
	Point2D<double> a;
	
	//Width and height of original image
	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();

	double SearchInterval;
	SearchInterval = m_DEM->resolution_X;
	//perspective center of each line
	double Xs, Ys, Zs;
	
	for(DWORD i=0;i<(DWORD)m_DEM->GetHeight();i++)
	{
		for(DWORD j=0;j<(DWORD)m_DEM->GetWidth();j++)
		{
			//DEM point
			double X, Y, Z;
			
			X = m_DEM->GetX(j);
			Y = m_DEM->GetY(i);
			Z = m_DEM->GetZ(j,i);

			if((Z == m_DEM->val_fail)||(Z < 0.0))
			{
				blue >> OrthoPixel[LONG(i)][LONG(j)];
				continue;
			}
			else
			{
				//red >> OrthoPixel[LONG(i)][LONG(j)];
				//continue;
			}
			/////////////////////////////////////////////////////////////////////
			//Searching region (pseudo DEM)
			//
			double min_neighbor = Z;
			int nei_index=0;
			double MaxDiffZ;
			
			if(bEnhanced == true)
			{
				SearchInterval = m_DEM->resolution_X;
	
				double row, col;
				
				GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

				if(false == CalSInterval(SearchInterval, row, col))
				{
					continue;
				}
				else
				{
					SearchInterval *= 0.5;
				}

				for(int nei_x=-1; nei_x<1; nei_x++)
				{
					for(int nei_y=-1; nei_y<1; nei_y++)
					{
						int index_x = nei_x+j;
						int index_y = nei_y+i;
						if(index_x<0) continue;
						if(index_y<0) continue;
						if(index_x>=m_DEM->GetWidth()) continue;
						if(index_y>=m_DEM->GetHeight()) continue;
						
						double temp = m_DEM->GetZ(index_x,index_y);
						if(temp == m_DEM->val_fail) continue;
						if(temp<min_neighbor) min_neighbor = temp;
					}
				}
				MaxDiffZ = Z - min_neighbor;
				if(MaxDiffZ<SearchInterval) MaxDiffZ = SearchInterval;
			}
			else
			{
				MaxDiffZ = SearchInterval;
			}
			///////////////////////////////////////////////////////////////////////

			for(int k=0; k>int(-MaxDiffZ/SearchInterval-0.5); k--)
			{
				Z = Z + k*SearchInterval; //SearchInterval 만큼씩 조정해가며 검색.
				if(Z<MinZ) break;
				
				/********************************************/
				double row, col;
				
				GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

				a.x = col; a.y = row;
				int ir, ic;
				ir = int(a.y + 0.5); ic = int(a.x + 0.5);//최근접 화소 선택
				//Updating initial row number
				init_r = ir;
				/********************************************/

				if(bBilinear != true)//Nearest
				{
					//Nearest Neighborhood
					if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))
					{
						int tr, tc;
						tr = ir-sr;
						tc = ic-sc;
						
						double indexval;
						int row_index, col_index;
						double dist_index;
						if(false == indexmap.GetBuf(tc,tr,indexval))
						{
							continue;
						}
						AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,row_index,col_index,dist_index);

						if(col_index == 253)
							int iii=0;
						
						if(row_index == FIRST_RECORD)
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double dist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								double indexval;
								MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,dist);
								if(false == indexmap.SetBuf(tc,tr,indexval))
								{
									continue;
								}
								
								OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(ir)][LONG(ic)];
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double dist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								double indexval;
								MakeIndex(m_DEM->GetWidth(),SCALE,indexval,-888,-888,dist);
								if(false == indexmap.SetBuf(tc,tr,indexval))
								{
									continue;
								}
							}							
						}//FIRST_RECORD(initial points)
						else if(row_index == -888)
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								double indexval;
								int row_index, col_index;
								double dist_index;
								
								if(false == indexmap.GetBuf(tc,tr,indexval))
								{
									continue;
								}

								AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,row_index,col_index,dist_index);
								
								if(dist_index<newdist)
								{
									black >> OrthoPixel[LONG(i)][LONG(j)];
									red >> OrthoPixel[LONG(i)][LONG(j)];
								}
								else
								{
									OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(ir)][LONG(ic)];
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								if(dist_index<newdist)
								{
								}
								else
								{
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
									//건물 중간으로 가리는 지역 업데이트.(거리 업데이트)
								}
							}	
						}//-888(building mid points)
						else
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								if(dist_index<newdist)
								{
									black >> OrthoPixel[LONG(i)][LONG(j)];
									red >> OrthoPixel[LONG(i)][LONG(j)];
								}
								else
								{
									black >> OrthoPixel[(LONG)row_index][(LONG)col_index];

									OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG(ir)][LONG(ic)];
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								if(dist_index<newdist)
								{
								}
								else
								{
									black >> OrthoPixel[(LONG)row_index][(LONG)col_index];
									green >> OrthoPixel[(LONG)row_index][(LONG)col_index];
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,-888,-888,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
						}					
					}
					else
					{
						//OrthoPixel[LONG(i)][LONG(j)] = (BYTE)BACKGROUND;
						black >> OrthoPixel[LONG(i)][LONG(j)];
					}
				}//Nearest
				else//Bilinear
				{				
					//Bilinear
					if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))
					{
						int tr, tc;
						tr = ir-sr;
						tc = ic-sc;
						
						double indexval;
						int row_index, col_index;
						double dist_index;
						
						if(false == indexmap.GetBuf(tc,tr,indexval))
						{
							continue;
						}

						AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,row_index,col_index,dist_index);

						if(row_index == FIRST_RECORD)
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double dist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								double indexval;
								MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,dist);
								if(false == indexmap.SetBuf(tc,tr,indexval))
								{
									continue;
								}

								CColorPixel value1, value2, value3, value4;
								value1 << OriginPixel[LONG(a.y)][LONG(a.x)];
								value2 << OriginPixel[LONG(a.y)][LONG(a.x+1)];
								value3 << OriginPixel[LONG(a.y+1)][LONG(a.x)];
								value4 << OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
								
								double A, B, C, D;
								A = (double)((int)a.x + 1) - a.x;
								B = 1 - A;
								C = (double)((int)a.y + 1) - a.y;
								D = 1 - C;
								
								(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> OrthoPixel[LONG(i)][LONG(j)];
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double dist = sqrt(dX*dX+dY*dY+dZ*dZ);
								
								double indexval;
								MakeIndex(m_DEM->GetWidth(),SCALE,indexval,-888,-888,dist);
								if(false == indexmap.SetBuf(tc,tr,indexval))
								{
									continue;
								}

							}							
						}//FIRST_RECORD(initial points)
						else if(row_index == -888)
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								double indexval;
								int row_index, col_index;
								double dist_index;
								if(false == indexmap.GetBuf(tc,tr,indexval))
								{
									continue;
								}
								AnalysisIndex(m_DEM->GetWidth(),SCALE,indexval,row_index,col_index,dist_index);

								if(dist_index<newdist)
								{
									black >> OrthoPixel[LONG(i)][LONG(j)];
								}
								else
								{
									CColorPixel value1, value2, value3, value4;
								value1 << OriginPixel[LONG(a.y)][LONG(a.x)];
								value2 << OriginPixel[LONG(a.y)][LONG(a.x+1)];
								value3 << OriginPixel[LONG(a.y+1)][LONG(a.x)];
								value4 << OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
								
								double A, B, C, D;
								A = (double)((int)a.x + 1) - a.x;
								B = 1 - A;
								C = (double)((int)a.y + 1) - a.y;
								D = 1 - C;
								
								(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> OrthoPixel[LONG(i)][LONG(j)];
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								if(dist_index<newdist)
								{
								}
								else
								{
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}	
						}//-888(building mid points)
						else
						{
							if(k==0)
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								if(dist_index<newdist)
								{
									black >> OrthoPixel[LONG(i)][LONG(j)];
								}
								else
								{
									black >> OrthoPixel[(LONG)row_index][(LONG)col_index];

									CColorPixel value1, value2, value3, value4;
									value1 << OriginPixel[LONG(a.y)][LONG(a.x)];
									value2 << OriginPixel[LONG(a.y)][LONG(a.x+1)];
									value3 << OriginPixel[LONG(a.y+1)][LONG(a.x)];
									value4 << OriginPixel[LONG(a.y+1)][LONG(a.x+1)];
									
									double A, B, C, D;
									A = (double)((int)a.x + 1) - a.x;
									B = 1 - A;
									C = (double)((int)a.y + 1) - a.y;
									D = 1 - C;
									
									(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> OrthoPixel[LONG(i)][LONG(j)];
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,i,j,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
							else
							{
								double dX, dY, dZ;
								dX = Xs - X; dY = Ys - Y; dZ = Zs - Z;
								double newdist = sqrt(dX*dX+dY*dY+dZ*dZ);

								if(dist_index<newdist)
								{
								}
								else
								{
									black >> OrthoPixel[(LONG)row_index][(LONG)col_index];
									
									double indexval;
									MakeIndex(m_DEM->GetWidth(),SCALE,indexval,-888,-888,newdist);
									if(false == indexmap.SetBuf(tc,tr,indexval))
									{
										continue;
									}
								}
							}
						}					
					}
					else
					{
						//OrthoPixel[LONG(i)][LONG(j)] = (BYTE)BACKGROUND;
						black >> OrthoPixel[LONG(i)][LONG(j)];
					}
				}//Bilinear
			}//for(k)			
			
			
		}//for(j)
		bar.StepIt();
	}//for(i)

	if(FALSE == OrthoImage.Save(OrthoPath)) return false;
	
	return true;
}

bool CSatOrthoImage::MakeOrtho(CImage &oriImage, CString parampath, CString DEMPath, CString HDRPath, CString OrthoPath, bool bZBuf, bool bBilinear, bool bRemoveOcclu, bool angle_availability, bool bHeight, bool bForward, double val_fail)
{
	if(bForward == true)
	{
		AfxMessageBox("Forward is not implemented yet");
		return false;
	}

	OriginImage = &oriImage;

	int bitcount = OriginImage->GetBitCount();
	if((bitcount != 8)&&(bitcount != 24))
	{
		AfxMessageBox("This program supports only 8bit gray scale images and 24bit color images.");
		return false;
	}
	
	if(false==ReadEOParam(parampath)) return false;
	Readlidardem(DEMPath, HDRPath);

	//failed DEM value setting
	m_DEM->val_fail = val_fail;
	
	//min and max height of DEM
	MinZ = 99999.;
	MaxZ = -99999.;
	for(int i=0; i<(int)m_DEM->GetHeight(); i++)
	{
		for(int j=0; j<(int)m_DEM->GetWidth(); j++)
		{
			double z;
			if(false == m_DEM->GetZ(j,i,z)) continue;
			if(z == val_fail) continue;
			if(z == m_DEM->val_back) continue;
			if(z<MinZ) MinZ = z;
			if(z>MaxZ) MaxZ = z;
		}
	}

	//Start time
	DWORD dwStart = GetTickCount();
	if(bZBuf == true)
	{
		if(bitcount == 8)
			ResampleOrtho_EnhancedZBuffer(OrthoPath, bBilinear, bRemoveOcclu);
		else if(bitcount == 24)
			ResampleColorOrtho_EnhancedZBuffer(OrthoPath, bBilinear, bRemoveOcclu);
	}
	else if(bHeight == true)
	{
		////////////////////////////////////////////////////////////////////
		//
		//DEM height를 이용한 zbuffer 방법: 잘 작동함.
		//
		if(bitcount == 8)
			ResampleOrtho_ZBuffer_Height(OrthoPath, bBilinear);
		else if(bitcount == 24)
			ResampleColorOrtho_ZBuffer_Height(OrthoPath, bBilinear);
		//
		//
	}
	else if(angle_availability == true)
	{
		if(bitcount == 8)
			ResampleOrtho_Slope_Angle(OrthoPath, bBilinear, bRemoveOcclu);
		else if(bitcount == 24)
			ResampleColorOrtho_Slope_Angle(OrthoPath, bBilinear, bRemoveOcclu);
	}
	else
	{
		if(bitcount == 8)
			ResampleOrtho_Slope_DiffZ(OrthoPath, bBilinear, bRemoveOcclu);
		else if(bitcount == 24)
			ResampleColorOrtho_Slope_DiffZ(OrthoPath, bBilinear, bRemoveOcclu);
	}
	
	DWORD ElapsedTime = GetTickCount() - dwStart;
	CString msg;
	msg.Format("Elapsed Time: %lf",(double)ElapsedTime/(double)1000);
	AfxMessageBox(msg);
	return true;
}

bool CSatOrthoImage::MakeOrtho_SortedDEM(CImage &oriImage, CString parampath, CString DEMPath, CString HDRPath, CString OrthoPath, bool bBilinear, double val_fail)
{	
	OriginImage = &oriImage;

	int bitcount = OriginImage->GetBitCount();
	if((bitcount != 8)&&(bitcount != 24))
	{
		AfxMessageBox("This program supports only 8bit gray scale images and 24bit color images.");
		return false;
	}

	if(false == ReadEOParam(parampath)) return false;
	ReadSorteddem(DEMPath, HDRPath);
	
	//min and max height of DEM
	MinZ = 99999.;
	MaxZ = -99999.;
	for(int i=0; i<(int)m_DEM->GetHeight()*(int)m_DEM->GetWidth(); i++)
	{
		if(m_pData[i]<MinZ) MinZ = m_pData[i];
		if(m_pData[i]>MaxZ) MaxZ = m_pData[i];
	}

	//failed DEM value setting
	m_DEM->val_fail = val_fail;
	
	//Start time
	DWORD dwStart = GetTickCount();
	
	//if(MaxDiffZ>0)
	//{
	//	if(bitcount == 8)
			//ResampleOrtho_SortedDEM_Enhanced(OrthoPath, bBilinear, MaxDiffZ, SInterval);
		//else if(bitcount == 24)
			//ResampleColorOrtho_SortedDEM_Enhanced(OrthoPath, bBilinear, MaxDiffZ, SInterval);
	//}
	//else
	{
		if(bitcount == 8)
			ResampleOrtho_SortedDEM(OrthoPath, bBilinear);
		else if(bitcount == 24)
			ResampleColorOrtho_SortedDEM(OrthoPath, bBilinear);
	}
	
	DWORD ElapsedTime = GetTickCount() - dwStart;
	CString msg;
	msg.Format("Elapsed Time: %lf",(double)ElapsedTime/(double)1000);
	AfxMessageBox(msg);

	return true;
}

void CSatOrthoImage::TXT2BIN(CString txtpath, CString hdrpath, CString binpath)
{
	Readlidardem(txtpath, hdrpath);
	m_DEM->SaveTXTDEM2BINDEM(binpath);
}

bool CSatOrthoImage::ResampleOrtho_SortedDEM_Enhanced(CString OrthoPath, bool bBilinear, double MaxDiffZ, double SearchInterval)
{
	AfxMessageBox("Not implemented");
	return true;
}

bool CSatOrthoImage::ResampleColorOrtho_SortedDEM_Enhanced(CString OrthoPath, bool bBilinear, double MaxDiffZ, double SearchInterval)
{
	AfxMessageBox("Not implemented");
	return true;
}

bool CSatOrthoImage::ResampleOrtho_SortedDEM(CString OrthoPath, bool bBilinear)
{
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),8);
	CPixelPtr OrthoPixel(OrthoImage);
	CPixelPtr OriginPixel(*OriginImage);

	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());

	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();

	CImage CopyOriginImage;
	CopyOriginImage.Create(W, H, OriginImage->GetBitCount());
	CPixelPtr CopyPixel(CopyOriginImage);
	//Image copy
	if(bBilinear == true)
	{
		for(int w=0; w<(int)W; w++)
		{
			for(int h=0; h<(int)H; h++)
			{
				//Prepare a copy-image for calculating interpolated pixel values
				CopyPixel[h][w] = OriginPixel[h][w];
			}
		}
	}
	
	/////////////////////////
	//
	//Resampling
	//
	/////////////////////////
	int num_list = m_DEM->GetWidth()*m_DEM->GetHeight();
	if(num_list<1) return false;

	Point2D<double> a;
	
	//initial value for scan-line
	double init_r = IOParam[1]/2;
	int barpos=0;
	for(int idx=(num_list-1); idx>=0; idx--)
	{
		//DEM point
		double X, Y, Z;
		//perspective center of each line
		double Xs, Ys, Zs;
		
		DWORD index_x, index_y;
		index_x = (DWORD)idx_x[idx];
		index_y = (DWORD)idx_y[idx];

		if(idx_x[idx] < 0 || idx_y[idx]<0)
		{
			continue;
		}

		Z = (double)m_pData[idx];
		X = m_DEM->leftX + m_DEM->resolution_X*index_x;
		Y = m_DEM->upperY - m_DEM->resolution_Y*index_y;

		if((Z == m_DEM->val_fail)||(Z == m_DEM->val_back)||(Z <= 0.0))
		{
			OrthoPixel[LONG(index_y)][LONG(index_x)] = (BYTE)0;
			continue;
		}
		
		double row, col;
		
		GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);
		
		a.x = col; a.y = row;
		int ir, ic;
		ir = int(a.y + 0.5); ic = int(a.x + 0.5);
		//Updating initial row number
		init_r = ir;

		//////////////////////////////////////////////////////////////////////////////////////
		if(bBilinear != true)			
		{
			//Nearest Neighborhood
			if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))//original image boundary check
			{
				OrthoPixel[LONG(index_y)][LONG(index_x)] = OriginPixel[LONG(ir)][LONG(ic)];
				OriginPixel[LONG(ir)][LONG(ic)] = (BYTE)0;
			}
			else//out of range of image
			{
				OrthoPixel[LONG(index_y)][LONG(index_x)] = (BYTE)200;
			}
		}
		else//Bilinear interpolation
		{
			if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))
			{
				if(OriginPixel[LONG(ir)][LONG(ic)] == (BYTE)0)
				{
					OrthoPixel[LONG(index_y)][LONG(index_x)] = (BYTE)0;
				}
				else
				{
					double value1, value2, value3, value4;
					value1 = (double)CopyPixel[LONG(a.y)][LONG(a.x)];
					value2 = (double)CopyPixel[LONG(a.y)][LONG(a.x+1)];
					value3 = (double)CopyPixel[LONG(a.y+1)][LONG(a.x)];
					value4 = (double)CopyPixel[LONG(a.y+1)][LONG(a.x+1)];
					
					double A, B, C, D;
					A = (double)((int)a.x + 1) - a.x;
					B = 1 - A;
					C = (double)((int)a.y + 1) - a.y;
					D = 1 - C;
					
					OrthoPixel[LONG(index_y)][LONG(index_x)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
					OriginPixel[LONG(ir)][LONG(ic)] = (BYTE)0;
				}				
			}
			else//out of range of image
			{
				OrthoPixel[LONG(index_y)][LONG(index_x)] = (BYTE)0;
			}
		}
	}

	//To save ortho-photo
	if(FALSE == OrthoImage.Save(OrthoPath)) return false;

	if(idx_x != NULL){/*delete[] idx_x; idx_x = NULL;*/}
	if(idx_y != NULL){/*delete[] idx_y; idx_y = NULL;*/}
	if(m_pData != NULL){/*delete[] m_pData; m_pData = NULL;*/}
	
	return true;
}

bool CSatOrthoImage::ResampleColorOrtho_SortedDEM(CString OrthoPath, bool bBilinear)
{
	CImage OrthoImage;
	OrthoImage.Create(m_DEM->GetWidth(),m_DEM->GetHeight(),24);
	CColorPixelPtr ColorOrthoPixel(OrthoImage);
	CColorPixelPtr ColorOriginPixel(*OriginImage);

	CColorPixel black; black = 0;
	CColorPixel back;  back = 0;

	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());

	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();

	CImage CopyOriginImage;
	CopyOriginImage.Create(W, H, OriginImage->GetBitCount());
	CColorPixelPtr ColorCopyPixel(CopyOriginImage);
	//Image copy
	if(bBilinear == true)
	{
		for(int w=0; w<(int)W; w++)
		{
			for(int h=0; h<(int)H; h++)
			{
				CColorPixel p;
				p << ColorOriginPixel[h][w];
				p >> ColorCopyPixel[h][w];
			}
		}
	}
	
	/////////////////////////
	//
	//Resampling
	//
	/////////////////////////
	int num_list = m_DEM->GetWidth()*m_DEM->GetHeight();
	if(num_list<1) return false;

	Point2D<double> a;
	
	//initial value for scan-line
	double init_r = IOParam[1]/2;
	int barpos=0;
	for(int idx=num_list-1; idx>=0; idx--)
	{
		//DEM point
		double X, Y, Z;
		//perspective center of each line
		double Xs, Ys, Zs;
		
		DWORD index_x, index_y;
		index_x = (DWORD)idx_x[idx];
		index_y = (DWORD)idx_y[idx];

		if(idx_x[idx] < 0 || idx_y[idx]<0)
		{
			continue;
		}

		Z = (double)m_pData[idx];
		X = m_DEM->leftX + m_DEM->resolution_X*index_x;
		Y = m_DEM->upperY - m_DEM->resolution_Y*index_y;

		if((Z == m_DEM->val_fail)||(Z == m_DEM->val_back)||(Z <= 0.0))
		{
			black >> ColorOrthoPixel[LONG(index_y)][LONG(index_x)];
			continue;
		}

		double row, col;
		
		GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);
		
		a.x = col; a.y = row;
		int ir, ic;
		ir = int(a.y + 0.5); ic = int(a.x + 0.5);
		//Updating initial row number
		init_r = ir;

		//////////////////////////////////////////////////////////////////////////////////////
		if(bBilinear != true)			
		{
			//Nearest Neighborhood
			if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))//original image boundary check
			{
				CColorPixel p;
				p << ColorOriginPixel[LONG(ir)][LONG(ic)];
				p >> ColorOrthoPixel[LONG(index_y)][LONG(index_x)];
				black >> ColorOriginPixel[LONG(ir)][LONG(ic)];
			}
			else//out of range of image
			{
				back >> ColorOrthoPixel[LONG(index_y)][LONG(index_x)];
			}
		}
		else//Bilinear interpolation
		{
			if(((ic > 0) && (ir >0)) && ((ic < int(W-1)) && (ir < int(H-1))))
			{
				CColorPixel p; p << ColorOriginPixel[LONG(ir)][LONG(ic)];
				if(black == p)
				{
					black >> ColorOrthoPixel[LONG(index_y)][LONG(index_x)];
				}
				else
				{
					CColorPixel value1, value2, value3, value4;
					value1 << ColorCopyPixel[LONG(a.y)][LONG(a.x)];
					value2 << ColorCopyPixel[LONG(a.y)][LONG(a.x+1)];
					value3 << ColorCopyPixel[LONG(a.y+1)][LONG(a.x)];
					value4 << ColorCopyPixel[LONG(a.y+1)][LONG(a.x+1)];
					
					double A, B, C, D;
					A = (double)((int)a.x + 1) - a.x;
					B = 1 - A;
					C = (double)((int)a.y + 1) - a.y;
					D = 1 - C;
					
					(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> ColorOrthoPixel[LONG(index_y)][LONG(index_x)];
					black >> ColorOriginPixel[LONG(ir)][LONG(ic)];
				}				
			}
			else//out of range of image
			{
				black >> ColorOrthoPixel[LONG(index_y)][LONG(index_x)];
			}
		}

		barpos ++;
		if(barpos%m_DEM->GetWidth() == 0) bar.StepIt();
	}

	//To save ortho-photo
	if(FALSE == OrthoImage.Save(OrthoPath)) return false;

	if(idx_x != NULL){/*delete[] idx_x; idx_x = NULL;*/}
	if(idx_y != NULL){/*delete[] idx_y; idx_y = NULL;*/}
	if(m_pData != NULL){/*delete[] m_pData; m_pData = NULL;*/}
	
	return true;
}

bool CSatOrthoImage::OrthoMerge(CImage &image1, CImage &image2, CString path, bool bFillHoles)
{
	if(image1.GetWidth() != image2.GetWidth()) return false;
	if(image1.GetHeight() != image2.GetHeight()) return false;
	if(image1.GetBitCount() != image2.GetBitCount()) return false;
	
	CPixelPtr Pixel1_BW(image1);
	CPixelPtr Pixel2_BW(image2);

	CColorPixelPtr Pixel1_C(image1);
	CColorPixelPtr Pixel2_C(image2);

	int BitDepth = image1.GetBitCount();
	if( BitDepth != image2.GetBitCount() )
		return false;
	
	CImage OrthoImage;
	OrthoImage.Create(image1.GetWidth(),image1.GetHeight(),BitDepth);
	
	CPixelPtr OrthoPixel_BW(OrthoImage);
	CColorPixelPtr OrthoPixel_C(OrthoImage);

	CColorPixel p_C, p_C1, p_C2;
	CColorPixel Black_C(0,0,0), White_C(255,255,255);

	if( 8 == BitDepth )
	{
		for(long i=0; i<(long)image1.GetHeight(); i++)
		{
			for(long j=0; j<(long)image1.GetWidth(); j++)
			{
				//white/black image pixels
				
				if( (BYTE)(Pixel1_BW[LONG(i)][LONG(j)]) == (BYTE)0 || (BYTE)(Pixel1_BW[LONG(i)][LONG(j)]) == (BYTE)255 ) 
					OrthoPixel_BW[LONG(i)][LONG(j)] =  Pixel2_BW[LONG(i)][LONG(j)];
				else if( (BYTE)(Pixel2_BW[LONG(i)][LONG(j)]) == (BYTE)0 || (BYTE)(Pixel2_BW[LONG(i)][LONG(j)]) == (BYTE)255 ) 
					OrthoPixel_BW[LONG(i)][LONG(j)] =  Pixel1_BW[LONG(i)][LONG(j)];
				else
					OrthoPixel_BW[LONG(i)][LONG(j)] =  ((BYTE)(Pixel1_BW[LONG(i)][LONG(j)])+(BYTE)(Pixel2_BW[LONG(i)][LONG(j)]))/2;
			}
		}
	}
	else if( 24 == BitDepth )
	{
		for(long i=0; i<(long)image1.GetHeight(); i++)
		{
			for(long j=0; j<(long)image1.GetWidth(); j++)
			{
				//color image pixels
				
				p_C1 = Pixel1_C[LONG(i)][LONG(j)];
				p_C2 = Pixel2_C[LONG(i)][LONG(j)];

				if( p_C1 == Black_C || p_C1 == White_C ) {
					p_C = p_C2;
					p_C >> OrthoPixel_C[LONG(i)][LONG(j)];
				}
				else if( p_C2 == Black_C || p_C2 == White_C ) {
					p_C = p_C1;
					p_C >> OrthoPixel_C[LONG(i)][LONG(j)];
				}
				else {
					((p_C1+p_C2)/2) >> OrthoPixel_C[LONG(i)][LONG(j)];
				}
			}
		}
	}
	else
		return false;
	
	if(bFillHoles == true)
	{
		CImage NewImage;
		NewImage.Create(image1.GetWidth(),image1.GetHeight(),image1.GetBitCount());
		FillHoles(OrthoImage, NewImage, 5);
		if(FALSE == NewImage.Save(path)) return false;
		else return true;
	}
	else
	{
		if(FALSE == OrthoImage.Save(path)) return false;
		else return true;
	}
}

void CSatOrthoImage::FillHoles(CImage &Image, CImage &NewImage, long bound)
{
	if(NewImage.GetBitCount() == 24)
	{
		CColorPixelPtr Pixel(Image);
		CColorPixelPtr NewPixel(NewImage);
		CColorPixel black; black.SetR(0); black.SetG(0); black.SetB(0);
		CColorPixel sum;
		
		for(long i=bound; i<Image.GetHeight()-bound; i++)
		{
			for(long j=bound; j<Image.GetWidth()-bound; j++)
			{
				if(black == Pixel[LONG(i)][LONG(j)])
				{
					sum.SetR(0); sum.SetG(0); sum.SetB(0);
					long num = 0;
					for(long row=-bound; row<bound; row++)
					{
						for(long col=-bound; col<bound; col++)
						{
							if(!(black == Pixel[i+row][j+col]))
								sum += Pixel[i+row][j+col];
							num++;
						}
					}
					
					sum = sum/num;
					sum >> NewPixel[i][j];
				}
				else
					NewPixel[i][j] = Pixel[i][j];
			}
		}
	}
	else if(NewImage.GetBitCount() == 8)
	{
		
		CPixelPtr Pixel(Image);
		CPixelPtr NewPixel(NewImage);
		CPixel black; black=0;
		CPixel sum;
		
		for(long i=bound; i<Image.GetHeight()-bound; i++)
		{
			for(long j=bound; j<Image.GetWidth()-bound; j++)
			{
				if(black.I == Pixel[LONG(i)][LONG(j)])
				{
					sum.I=0;
					long num = 0;
					for(long row=-bound; row<bound; row++)
					{
						for(long col=-bound; col<bound; col++)
						{
							if(!(black.I == Pixel[i+row][j+col]))
								sum += Pixel[i+row][j+col];
							num++;
						}
					}
					
					sum.I = sum.I/num;
					sum >> NewPixel[i][j];
				}
				else
					NewPixel[i][j] = Pixel[i][j];
			}
		}
	}
} 

void CSatOrthoImage::SortDEM_Insert_SortMethod()
{
	fstream temp;
	temp.open("temp.tmp",ios::out);
	//Sorted DEM List
	//CSMList<short> List_Z;
	//Sorted sorted DEM index
	List_X_index.RemoveAll();
	List_Y_index.RemoveAll();
	
	//sorting
	for(int i=0; i<(int)m_DEM->GetHeight(); i++)
	{
		for(int j=0; j<(int)m_DEM->GetWidth(); j++)
		{
			//given height
			double z;
			if(false == m_DEM->GetZ(j,i,z)) continue;

			//number of already sorted DEM
			int num = List_X_index.GetNumItem();
			//sorting index
			//int insert_pos = -1;
			
			int lo=0; int hi=num-1;
			int mid;
			if(num == 0) 
			{
				List_X_index.AddHead(j); 
				List_Y_index.AddHead(i);
			}
			else if(num == 1)
			{
				double sortedDEM;
				int ix = List_X_index.GetAt(0);
				int iy = List_Y_index.GetAt(0);
				//To compare height and record index
				if(false == m_DEM->GetZ(ix,iy,sortedDEM)) continue;
				if(sortedDEM>z) {List_X_index.AddTail(j); List_Y_index.AddTail(i);}
				else {List_X_index.AddHead(j); List_Y_index.AddHead(i);}
			}
			else
			{
				do 
				{
					double sortedDEM;
					int ix, iy;

					if((hi-lo) <= 1)
					{
						int ix = List_X_index.GetAt(lo);
						int iy = List_Y_index.GetAt(lo);
						//To compare height and record index
						if(false == m_DEM->GetZ(ix,iy,sortedDEM)) continue;
						
						if(sortedDEM<z)
						{
							List_X_index.InsertData(lo, j); 
							List_Y_index.InsertData(lo, i);
						}
						else
						{
							ix = List_X_index.GetAt(hi);
							iy = List_Y_index.GetAt(hi);
							if(false == m_DEM->GetZ(ix,iy,sortedDEM)) continue;
							if(sortedDEM<z)
							{
								List_X_index.InsertData(hi, j);
								List_Y_index.InsertData(hi, i);
							}
							else
							{
								List_X_index.InsertData(hi+1, j); 
								List_Y_index.InsertData(hi+1, i);
							}
						}

						break;
					}
					
					int half_range = (int)((double)(hi-lo)/(double)2.);
					
					mid = lo + half_range;

					ix = List_X_index.GetAt(mid);
					iy = List_Y_index.GetAt(mid);
					//To compare height and record index
					if(false == m_DEM->GetZ(ix,iy,sortedDEM)) return;
					if(sortedDEM>z) {lo=mid;} else {hi=mid;}
					
					
					
				}while(1);
			}

			temp<<"\t"<<i<<"\t"<<j<<endl;			

			/*
			for(int k=0; k<num; k++)
			{
				double sortedDEM;
				int ix = List_X_index.GetAt(k);
				int iy = List_Y_index.GetAt(k);
				
				//To compare height and record index
				if(false == m_DEM->GetZ(ix,iy,sortedDEM)) continue;
				if(sortedDEM<z) {insert_pos = k; break;}
			}
			
			if(insert_pos == -1)//given z is smaller than others which are in the list.
			{
				List_X_index.AddTail(j);
				List_Y_index.AddTail(i);
			}
			else//insert given z into the list
			{
				List_X_index.InsertData(insert_pos, j);
				List_Y_index.InsertData(insert_pos, i);
			}
			*/

			//temp<<insert_pos<<"\t"<<z<<"\t"<<endl;
		}
		temp<<i<<endl;
	}
	temp.close();
}

void CSatOrthoImage::QuickSort(char* DEMPath, char* DEMHDRPath, char* SortedDEMPath)
{
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());

	Readlidardem(DEMPath, DEMHDRPath);
	fstream sorteddemfile;
	sorteddemfile.open(SortedDEMPath,ios::out);

	//sorting boundary, pivot index, and traveling pointers for partition step
	if(idx_x != NULL) {delete[] idx_x; idx_x = NULL;}
	if(idx_y != NULL) {delete[] idx_y; idx_y = NULL;}
	if(m_pData != NULL) {delete[] m_pData; m_pData = NULL;}

	LONG array_size = m_DEM->GetWidth()*m_DEM->GetHeight();

	m_pData	= new float	[array_size];
	idx_x	= new int	[array_size];
	idx_y	= new int	[array_size];
	//Initialize array
	for(LONG index_array=0; index_array<array_size; index_array++)
	{
		m_pData	[index_array] = (float)m_DEM->val_fail;
		idx_x	[index_array] = (int)-1;
		idx_y	[index_array] = (int)-1;
	}

	for(int i=0; i<(int)m_DEM->GetHeight(); i++)
	{
		for(int j=0; j<(int)m_DEM->GetWidth(); j++)
		{
			double value;

			if(false == m_DEM->GetZ(j,i,value))
			{
				continue;
			}
			
			m_pData	[i*m_DEM->GetWidth() + j] = (short)value;
			idx_x	[i*m_DEM->GetWidth() + j] = j;
			idx_y	[i*m_DEM->GetWidth() + j] = i;
		}
	}

	int lo, hi, mid, loguy, higuy;
 
	//stack for saving sub-array to be processed
	int lostk[40], histk[40], stkptr=0;
	int m_nCount = m_DEM->GetWidth()*m_DEM->GetHeight();
	lo=0; hi=m_nCount-1; //initialize limits
 
	float temp1; int temp2; int temp3;
	
	while(1) 
	{
		mid=lo+( ((hi-lo)+1) /2); //find middle element
		//Swap
		temp1 = m_pData[mid];
		m_pData[mid] = m_pData[lo];
		m_pData[lo] = temp1;

		temp2 = idx_x[mid];
		idx_x[mid] = idx_x[lo];
		idx_x[lo] = temp2;

		temp3 = idx_y[mid];
		idx_y[mid] = idx_y[lo];
		idx_y[lo] = temp3;
 
/* Would it be better to use insertion sort, when hi-lo is very small?
			for(loguy=lo+1; loguy<=hi; loguy++) {
				for(higuy=loguy-1, r=m_pData[loguy]; higuy>=lo && m_pData[higuy]>r;
					higuy--) m_pData[higuy+1]=m_pData[higuy];
				m_pData[higuy+1]=r;
			}
*/
		loguy=lo; higuy=hi+1;
		while(1) 
		{
			do loguy++; while (loguy<=hi && m_pData[loguy]<=m_pData[lo]);
			do higuy--; while (higuy>lo && m_pData[higuy]>=m_pData[lo]);
 
			if(higuy<loguy) break;
			
			//Swap
			temp1 = m_pData[loguy];
			m_pData[loguy] = m_pData[higuy];
			m_pData[higuy] = temp1;

			temp2 = idx_x[loguy];
			idx_x[loguy] = idx_x[higuy];
			idx_x[higuy] = temp2;

			temp3 = idx_y[loguy];
			idx_y[loguy] = idx_y[higuy];
			idx_y[higuy] = temp3;
		}

		//put pivot element in place
		temp1 = m_pData[lo];
		m_pData[lo] = m_pData[higuy];
		m_pData[higuy] = temp1;
		
		temp2 = idx_x[lo];
		idx_x[lo] = idx_x[higuy];
		idx_x[higuy] = temp2;

		temp3 = idx_y[lo];
		idx_y[lo] = idx_y[higuy];
		idx_y[higuy] = temp3;
 
		if( higuy-1-lo >= hi-loguy ) {
			if(lo+1<higuy) { //save big recursion for later
				lostk[stkptr]=lo; histk[stkptr]=higuy-1; ++stkptr;
			}
			if(loguy<hi) { lo=loguy; continue; } //do small recursion
		}
		else {
			if(loguy<hi) { //save big recursion for later
				lostk[stkptr]=loguy; histk[stkptr]=hi; ++stkptr;
			}
			if(lo+1<higuy) { hi=higuy-1; continue; } //do small recursion
		}
 
		--stkptr; //pop subarray from stack
		if(stkptr>=0) { lo=lostk[stkptr]; hi=histk[stkptr]; }
		else break; //all subarrays done--sorting finished.

	}

	//DEM record to txt file
	for(int i=0; i<(int)m_DEM->GetHeight(); i++)
	{
		for(int j=0; j<(int)m_DEM->GetWidth(); j++)
		{
			sorteddemfile<<idx_x[i*m_DEM->GetWidth() + j]<<"\t"<<idx_y[i*m_DEM->GetWidth() + j]<<"\t"<<m_pData[i*m_DEM->GetWidth() + j]<<endl;
		}
	}

	if(idx_x != NULL) {delete[] idx_x; idx_x = NULL;}
	if(idx_y != NULL) {delete[] idx_y; idx_y = NULL;}
	if(m_pData != NULL) {delete[] m_pData; m_pData = NULL;}

	sorteddemfile.close();
}

bool CSatOrthoImage::GetTargetBoundary(int &h, int &w, int &sr, int &sc, int &er, int &ec)
{
	/****************************************************************************
	*image boundary for making z-buffer and index buffer
	*to reduce the needed memory, full image size is not effective.
	*For that reason, to calculate boundary is required.
	*BOUNDARY_MARGIN is required because a image(scene) has relief displacement
	*****************************************************************************/
	double X, Y, Z;
	double Xs, Ys, Zs;
	Point2D<double> a;
	double init_r;

	init_r = IOParam[1]/2.;

	double row, col;
	double max_r, max_c, min_r,min_c;

	/********************************************
	[DEM data struct]
	top left(min_X, max_Y)           top right(max_X, max_Y)
	__________________________________
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	|                                |
	----------------------------------
	down left(min_X, min_Y)         down right(max_X, min_Y)
	*********************************************/
	/********************************************/
	//
	//LIDAR: top Left
	//
	X = m_DEM->GetX(0);
	Y = m_DEM->GetY(0);
	Z = m_DEM->GetZ(0,0);
	
	if((Z == m_DEM->val_fail)||(Z == m_DEM->val_back)||(Z<=0))
		Z = 0.0;
	
	GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);

	a.x = col; a.y = row;
	
	max_r = a.y; max_c = a.x;
	min_r = a.y; min_c = a.x;
	
	//
	//LIDAR: down Left
	//
	X = m_DEM->GetX(0);
	Y = m_DEM->GetY(m_DEM->GetHeight()-1);
	Z = m_DEM->GetZ(0,m_DEM->GetHeight()-1);

	if((Z == m_DEM->val_fail)||(Z == m_DEM->val_back)||(Z<=0))
		Z = 0.0;
	
	GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);
	
	a.x = col; a.y = row;
	
	if(max_r<a.y) max_r = a.y;
	if(max_c<a.x) max_c = a.x;
	if(min_r>a.y) min_r = a.y;
	if(min_c>a.x) min_c = a.x;
	
	//
	//LIDAR: top right
	//
	X = m_DEM->GetX(m_DEM->GetWidth()-1);
	Y = m_DEM->GetY(0);
	Z = m_DEM->GetZ(m_DEM->GetWidth()-1,0);

	if((Z == m_DEM->val_fail)||(Z == m_DEM->val_back)||(Z<=0))
		Z = 0.0;
	
	GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);
	
	a.x = col; a.y = row;
	
	if(max_r<a.y) max_r = a.y;
	if(max_c<a.x) max_c = a.x;
	if(min_r>a.y) min_r = a.y;
	if(min_c>a.x) min_c = a.x;
	
	//
	//LIDAR: down right
	//
	X = m_DEM->GetX(m_DEM->GetWidth()-1);
	Y = m_DEM->GetY(m_DEM->GetHeight()-1);
	Z = m_DEM->GetZ(m_DEM->GetWidth()-1,m_DEM->GetHeight()-1);

	if((Z == m_DEM->val_fail)||(Z == m_DEM->val_back)||(Z<=0))
		Z = 0.0;
	
	GroundCoord2SceneCoord(X, Y, Z, Xs, Ys, Zs, row, col, init_r);
	
	a.x = col; a.y = row;
	
	if(max_r<a.y) max_r = a.y;
	if(max_c<a.x) max_c = a.x;
	if(min_r>a.y) min_r = a.y;
	if(min_c>a.x) min_c = a.x;
	
	//
	//original image: widht and height
	//
	DWORD W, H;
	W = (DWORD)OriginImage->GetWidth();
	H = (DWORD)OriginImage->GetHeight();

	h = unsigned int(H);
	w = unsigned int(W);
	
	//
	//start row and column, end row and column
	//
	//sr, sc, ec, er;
	
	//To add the margin to image boundary
	er = int(max_r + BOUNDARY_MARGIN);
	ec = int(max_c + BOUNDARY_MARGIN);
	sr = int(min_r - BOUNDARY_MARGIN);
	sc = int(min_c - BOUNDARY_MARGIN);

	//To check if the boundary is within original image size
	if(er>(double)H) er = int(H);
	if(ec>(double)W) ec = int(W);
	if(sr<0) sr = 0;
	if(sc<0) sc = 0;
	
//	sr = 0;
//	sc = 0;
//	er = int(H);
//	ec = int(W);

	h = er - sr;
	w = ec - sc;

	return false;
}
