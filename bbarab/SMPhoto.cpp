// SMPhoto.cpp: implementation of the CSSMPhoto class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SSMaticsSolution.h"
#include "SMPhoto.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSSMPhoto::CSSMPhoto()
{
	dispersion.Resize(6, 6);
	dispersion.Identity(1.0);
}

CSSMPhoto::~CSSMPhoto()
{
	image_points.RemoveAll();
}
