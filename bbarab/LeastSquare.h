/*
 * Copyright (c) 2000-2005, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// leastsquare.h: interface for the CLeastSquare class.
//
//////////////////////////////////////////////////////////////////////

/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/

//////////////////////////////////////////////////////////////////////
//This is the class for the adjustment solution
//
//Directly compose normal matrix
//N_dot, N_2dot, N_bar
//Reduced normal matrix are used for the better performance
//////////////////////////////////////////////////////////////////////

#if !defined(__BBARAB_LEAST_SQUARE_ADJUSTMENT_CLASS__)
#define __BBARAB_LEAST_SQUARE_ADJUSTMENT_CLASS__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMMatrix.h"
#include "BasicImage.h"
//#include "../Image_Util/ImagePixel.h"

//#include <vld.h>//memory leak detector

/////////////////////////////////////////////////////////////////////////////////////
//This is the class for Least Square
//Made by Bang, Ki In
//2005-07-12: Start day
//[Mathematical Model]
//A%X = L+V
/////////////////////////////////////////////////////////////////////////////////////

using namespace SMATICS_BBARAB;

/**@class RETMAT
*@brief class for return data of LS
*/
class RETMAT
{
public:
	CSMMatrix<double> A; /**<Design matrix */
	CSMMatrix<double> L; /**<Observation matrix */
	CSMMatrix<double> W; /**<Weight matrix */
	CSMMatrix<double> AT; /**<A transpose matrix */
	CSMMatrix<double> N; /**<Normal matrix */
	CSMMatrix<double> Ninv; /**<Normal inverse matrix */
	CSMMatrix<double> VTV; /**<Sum of residual square*/
	CSMMatrix<double> X; /**<Unknown matrix */
	CSMMatrix<double> V; /**<Residual matrix */
	CSMMatrix<double> XVarCov; /**<Variance-covariance matrix of unknown parameters */
	CSMMatrix<double> LVarCov; /**<Variance-covariance matrix of observation */
	CSMMatrix<double> LVar; /**<Variance matrix of observation */
	CSMMatrix<double> XCorrelation; /**<Correlation matrix of unknown parameters */
	CSMMatrix<double> VCorrelation; /**<Correlation matrix of observations */
	double SD; /**<Strandar deviation */
	double Variance; /**<Variance */
	double max_X_element;

	CBMPImage Nimg;//image of N matrix
	CBMPImage CorrImg;//image of parameters correlation
};

/**@class MatrixData
*@brief for using the CSMList class, Matrix are wrapped with un-template class.
*/
class MatrixData
{
public:
	CSMMatrix<double> mat;
};

/**@class CLeastSquare
*@brief class for least square and bundle adjustment including reduced normal matrix.
*/
class CLeastSquare  
{
	friend class CLeastSquareLIDAR;
public:
	/**constructor*/
	CLeastSquare(){Init();}
	/**destructor*/
	virtual ~CLeastSquare()
	{
	}

	/**initialized the object*/
	void Init()
	{
		Pos_Var = 0.0;
		num_eq = 0;
		num_unknown = 0;

		num_param = 0;
		num_image = 0;
		num_line = 0;
		num_triangle = 0;
		num_point = 0;
	}
	
	/**simple LS without weight matrix*/
	RETMAT RunLeastSquare(CSMMatrix<double> A, CSMMatrix<double> L)
	{
		unsigned int _size_ = A.GetRows();
		CSMMatrix<double> W(_size_, _size_);
		W.Identity();
		return RunLeastSquare(A, L, W);
	}

	/**simple and fast(poor result, only X matrix) LS*/
	CSMMatrix<double> RunLeastSquare_Fast(CSMMatrix<double> A, CSMMatrix<double> L)
	{
		unsigned int _size_ = A.GetRows();
		CSMMatrix<double> W(_size_, _size_);
		W.Identity();
		return RunLeastSquare_Fast(A, L, W);
	}

	/**LS using weight matrix, not using reduced normal matrix for simple modeling*/
	RETMAT RunLeastSquare(CSMMatrix<double> A, CSMMatrix<double> L, CSMMatrix<double> W)
	{
		RETMAT RETVAL;
		RETVAL.A = A;
		RETVAL.L = L;
		RETVAL.W = W;
		RETVAL.AT = RETVAL.A.Transpose();
		RETVAL.N = RETVAL.AT%RETVAL.W%RETVAL.A;
		RETVAL.Ninv = RETVAL.N.Inverse();
		RETVAL.X = RETVAL.Ninv%RETVAL.AT%RETVAL.W%RETVAL.L;
		RETVAL.V = RETVAL.A%RETVAL.X - RETVAL.L;
		RETVAL.VTV = RETVAL.V.Transpose()%RETVAL.W%RETVAL.V;
		RETVAL.Variance = RETVAL.VTV(0,0)/(RETVAL.A.GetRows()-RETVAL.A.GetCols());
		RETVAL.SD = sqrt(RETVAL.Variance);
		RETVAL.XVarCov = RETVAL.Ninv*RETVAL.Variance;
		RETVAL.LVarCov = (RETVAL.A%RETVAL.Ninv%RETVAL.AT)*RETVAL.Variance;
		
		return RETVAL;
	}

	/**LS using weight matrix, not using reduced normal matrix for simple modeling<br>
	*poor result, only X matrix, fast a little.
	*/
	CSMMatrix<double> RunLeastSquare_Fast(CSMMatrix<double> A, CSMMatrix<double> L, CSMMatrix<double> W)
	{
		RETMAT RETVAL;
		RETVAL.A = A;
		RETVAL.L = L;
		RETVAL.W = W;

		RETVAL.AT = RETVAL.A.Transpose();
		RETVAL.N = RETVAL.AT%RETVAL.W%RETVAL.A;
		RETVAL.Ninv = RETVAL.N.Inverse();
		RETVAL.X = RETVAL.Ninv%RETVAL.AT%RETVAL.W%RETVAL.L;
				
		return RETVAL.X;
	}

	CSMMatrix<double> RunLeastSquare_Fast(CSMMatrix<double> A, CSMMatrix<double> L, CSMMatrix<double> W, CSMMatrix<double> &N)
	{
		RETMAT RETVAL;
		RETVAL.A = A;
		RETVAL.L = L;
		RETVAL.W = W;

		RETVAL.AT = RETVAL.A.Transpose();
		RETVAL.N = RETVAL.AT%RETVAL.W%RETVAL.A;
		RETVAL.Ninv = RETVAL.N.Inverse();
		RETVAL.X = RETVAL.Ninv%RETVAL.AT%RETVAL.W%RETVAL.L;

		N = RETVAL.N;
				
		return RETVAL.X;
	}
	
	/**Get correlation matrix from variance-covariance matrix*/
	CSMMatrix<double> Correlation(CSMMatrix<double> VarCov)
	{
		unsigned int i, j;
		//Correlation of Unknown Matrix
		CSMMatrix<double> Correlation;
		Correlation.Resize(VarCov.GetRows(),VarCov.GetCols(),0.);
		
		for(i=0; i<Correlation.GetRows(); i++)
		{
			double sigma_i, sigma_j;
			sigma_i = sqrt(VarCov(i,i));
			
			for(j=i; j<Correlation.GetCols();j++)
			{
				sigma_j = sqrt(VarCov(j,j));
				Correlation(i,j) = VarCov(i,j)/sigma_i/sigma_j;
			}
		}
		return Correlation;
	}

	/**Extract variance(diagonal element) matrix from variance-covariance matrix*/
	CSMMatrix<double> ExtractVar(CSMMatrix<double> VarCov)
	{
		unsigned int i;
		//Correlation of Unknown Matrix
		CSMMatrix<double> Variance(VarCov.GetRows(),1,0.);

		for(i=0; i<Variance.GetRows(); i++)
		{
			Variance(i,0) = VarCov(i,i);
		}
		return Variance;
	}

	/**GetErrorList*/
	double* GetErrorList()
	{
		double *retval;
				
		int num_mat = Error.GetNumItem();
		
		DATA<MatrixData> *pos = NULL;
		int count =0;
		for(int i=0; i<num_mat; i++)
		{
			pos = Error.GetNextPos(pos);
			count += pos->value.mat.GetRows();
		}

		retval = new double[count];
		
		int index=0;
		for(int i=0; i<num_mat; i++)
		{
			pos = Error.GetNextPos(pos);
			int nrows = pos->value.mat.GetRows();
			for(int j=0; j<nrows; j++)
			{
				retval[index] = pos->value.mat(j,0);
				index++;
			}
		}

		return retval;
	}

	////////////////////////////
	///Reduced Normal Matrix////
	////////////////////////////
	/**LS for bundle using reduced normal matrix<br>
	*EO parameters, point data and straight line data.
	*/
	CSMMatrix<double> RunLeastSquare_RN(double &var, CSMMatrix<double> &N_dot, CSMMatrix<double> &C_dot)
	{
		CSMMatrix<double> X = ReducedNormalMatrix(var,  N_dot, C_dot);
				
		return X;
	}

	CSMMatrix<double> GetNdot() {CSMMatrix<double> N_dot = Make_N_dot(); return N_dot;}

	CSMMatrix<double> GetCdot() {CSMMatrix<double> C_dot = Make_C_dot(); return C_dot;}

	void Make_N_bar_N_2dot_inv(CSMMatrix<double> &Inv, CSMMatrix<double> &N_bar_N_2dot_inv, CSMMatrix<double> &N_bar, int index)
	{
		int cols = Inv.GetCols();
		int rows = N_bar.GetRows();

		CSMMatrix<double> Sub_N_bar = N_bar.Subset(0, index, rows, cols);
		CSMMatrix<double> Sub_N_bar_Inv = Sub_N_bar%Inv;
		N_bar_N_2dot_inv.InsertPlus(0, index, Sub_N_bar_Inv);
	}

	CSMMatrix<double> ReducedNormalMatrix(double &var, CSMMatrix<double> &N_dot, CSMMatrix<double> &C_dot)
	{	
		//N_dot
		N_dot = Make_N_dot();
		
		//N_2dot_inverse
		CSMList<MatrixData> N_2dot_inv;
		
		//N_bar_transe
		CSMMatrix<double> N_bar_trans; N_bar_trans = N_bar.Transpose();
		
		//C_dot(K_dot)
		C_dot = Make_C_dot();
		
		//C_2dot(K_2dot)
		CSMMatrix<double> C_2dot; C_2dot = Make_C_2dot();

		CSMMatrix<double>N_bar_N_2dot_inv(N_bar.GetRows(), N_bar.GetCols(), 0.0);

		//Make N_2dot_inverse matrix (Points)
		for(unsigned int point_index=0; point_index<num_point; point_index++)
		{
			CSMMatrix<double> inv;
			inv = N_2dot_point_list.Subset(0,point_index*3,3,3);
			inv = inv.Inverse();
			
			MatrixData mat;
			mat.mat = inv;
			N_2dot_inv.AddTail(mat);

			Make_N_bar_N_2dot_inv(inv, N_bar_N_2dot_inv, N_bar, point_index*3);//2007.05.09
		}

		//Make N_2dot_inverse matrix (Lines)
		for(unsigned int line_index=0; line_index<num_line; line_index++)
		{
			CSMMatrix<double> inv;
			
			inv = N_2dot_line_list.Subset(0, line_index*6, 6, 6);
			inv = inv.Inverse();
			
			MatrixData mat;
			mat.mat = inv;
			N_2dot_inv.AddTail(mat);

			Make_N_bar_N_2dot_inv(inv, N_bar_N_2dot_inv, N_bar, num_point*3+line_index*6);//2007.05.09
		}

		//Make N_2dot_inverse matrix (Trianlge)
		for(unsigned int tri_index=0; tri_index<num_triangle; tri_index++)
		{
			CSMMatrix<double> inv;
			
			inv = N_2dot_triangle_list.Subset(0, tri_index*9, 9, 9);
			inv = inv.Inverse();
			
			MatrixData mat;
			mat.mat = inv;
			N_2dot_inv.AddTail(mat);

			Make_N_bar_N_2dot_inv(inv, N_bar_N_2dot_inv, N_bar, num_point*3+num_line*6+9*tri_index);//2007.05.09
		}
		
		CSMMatrix<double> S;
		CSMMatrix<double> N_bar_N_2dot_inv_N_bar_trans = N_bar_N_2dot_inv%N_bar_trans;
		
		S = (N_dot - N_bar_N_2dot_inv_N_bar_trans);
		
		CSMMatrix<double> E;
		
		E = (C_dot - N_bar_N_2dot_inv%C_2dot);

		///////////////////////////////////////////////
		CSMMatrix<double> X_dot = S.Inverse()%E;
		///////////////////////////////////////////////

		//i: image, j: point, k: line
		CSMMatrix<double> X_2dot;
		int size = num_unknown - num_image*num_param;
		X_2dot.Resize(size,0);

		int X_2dot_index = 0;
		
		DATA<MatrixData> *pos = NULL;
		//Point
		for(unsigned int point_index=0; point_index<num_point; point_index++)
		{
			//X_2dot of point j
			CSMMatrix<double> X_2dot_j, NX_ij(3,1);
			for(unsigned int image_index=0; image_index<num_image; image_index++)
			{
				//N_bar_trans of scene i, point j
				//X_dot of scene i
				CSMMatrix<double> N_b_t_ij, X_dot_i;
				N_b_t_ij = N_bar_trans.Subset(point_index*3,image_index*num_param,3,num_param);
				X_dot_i = X_dot.Subset(image_index*num_param,0,num_param,1);
				//Sum of (N_bar_trans*X_dot) of scene i, point j
				NX_ij += N_b_t_ij%X_dot_i;
			}

			//K_2dot of point j
			CSMMatrix<double> C_2dot_point_j;
			C_2dot_point_j = C_2dot_point.Subset(point_index*3, 0, 3, 1);
			
			CSMMatrix<double> N_2dot_inv_j;
			
			//N_2dot_inverse of point j
			pos = N_2dot_inv.GetNextPos(pos);
			N_2dot_inv_j = pos->value.mat;

			/////////////////////////////////////////////////////
			//X_2dot of point j
			X_2dot_j = N_2dot_inv_j%(C_2dot_point_j - NX_ij);
			/////////////////////////////////////////////////////
			X_2dot.Insert(X_2dot_index,0,X_2dot_j);
			X_2dot_index += X_2dot_j.GetRows();
		}

		//Line
		for(unsigned int line_index=0; line_index<num_line; line_index++)
		{
			//X_2dot(6,1) of line k
			CSMMatrix<double> X_2dot_k, NX_ik(6,1);
			for(unsigned int image_index=0; image_index<num_image; image_index++)
			{
				//N_bar_trans of scene i, line k
				//X_dot of scene i
				CSMMatrix<double> N_b_t_ik, X_dot_i;
				N_b_t_ik = N_bar_trans.Subset(num_point*3+line_index*6,image_index*num_param,6,num_param);
				X_dot_i = X_dot.Subset(image_index*num_param,0,num_param,1);
				NX_ik += N_b_t_ik%X_dot_i;
			}
			
			//K_2dot of line k
			CSMMatrix<double> C_2dot_line_k;
			C_2dot_line_k = C_2dot_line.Subset(line_index*6, 0, 6, 1);
			
			//N_2dot_inverse of line k
			CSMMatrix<double> N_2dot_inv_k;
			pos = N_2dot_inv.GetNextPos(pos);
			N_2dot_inv_k = pos->value.mat;

			/////////////////////////////////////////////////////
			//X_2dot of line k
			X_2dot_k = N_2dot_inv_k%(C_2dot_line_k - NX_ik);
			/////////////////////////////////////////////////////
			X_2dot.Insert(X_2dot_index,0,X_2dot_k);
			X_2dot_index += X_2dot_k.GetRows();
		}

		//Triangle
		for(unsigned int tri_index=0; tri_index<num_triangle; tri_index++)
		{
			//X_2dot(9,1) of triangle k
			CSMMatrix<double> X_2dot_k, NX_ik(9,1);
			for(unsigned int image_index=0; image_index<num_image; image_index++)
			{
				//N_bar_trans of scene i, line k
				//X_dot of scene i
				CSMMatrix<double> N_b_t_ik, X_dot_i;
				N_b_t_ik = N_bar_trans.Subset(num_point*3+num_line*6+tri_index*9,image_index*num_param,9,num_param);
				X_dot_i = X_dot.Subset(image_index*num_param,0,num_param,1);
				NX_ik += N_b_t_ik%X_dot_i;
			}
			
			//K_2dot of line k
			CSMMatrix<double> C_2dot_tri_k;
			C_2dot_tri_k = C_2dot_triangle.Subset(tri_index*9, 0, 9, 1);
			
			//N_2dot_inverse of line k
			CSMMatrix<double> N_2dot_inv_k;
			pos = N_2dot_inv.GetNextPos(pos);
			N_2dot_inv_k = pos->value.mat;
			
			/////////////////////////////////////////////////////
			//X_2dot of triangle k
			X_2dot_k = N_2dot_inv_k%(C_2dot_tri_k - NX_ik);
			/////////////////////////////////////////////////////
			
			X_2dot.Insert(X_2dot_index,0,X_2dot_k);
			X_2dot_index += X_2dot_k.GetRows();
		}

		//X_2dot add to X_dot
		X_dot.AddRow(X_2dot);

		//standard deviation of unit weight
		var = Pos_Var/(num_eq-num_unknown);

		return X_dot;
	}

	CSMMatrix<double> GetCmatrix()
	{	
		//C_dot
		CSMMatrix<double> C_dot = Make_C_dot();
		
		//C_2dot
		CSMMatrix<double> C_2dot = Make_C_2dot();
		
		//Cmat
		CSMMatrix<double> Cmat; Cmat.Resize(0,0);
		Cmat.Insert(0,0,C_dot);
		Cmat.Insert(C_dot.GetRows(),0,C_2dot);

		return Cmat;
	}

	CSMMatrix<double> GetNmatrix(CSMMatrix<double> &N2dot_point, CSMMatrix<double> &N2dot_line, CSMMatrix<double> &N2dot_triangle)
	{	
		N2dot_point = Get_N_2dot_point();

		N2dot_line = Get_N_2dot_line();

		N2dot_triangle = Get_N_2dot_triangle();

		CSMMatrix<double> N_dot = Make_N_dot();
		return N_dot;
	}

	CSMMatrix<double> Get_N_2dot_point()
	{
		CSMMatrix<double> retmat;
		retmat.Resize(num_point*3, num_point*3, 0.0);

		int i;

		for(i=0; i<(int)num_point; i++)
		{
			CSMMatrix<double> mat = N_2dot_point_list.Subset(0, i*3, 3, 3);
			retmat.Insert(i*3, i*3, mat);
		}

		return retmat;
	}

	CSMMatrix<double> Get_N_2dot_line()
	{
		CSMMatrix<double> retmat;
		retmat.Resize(num_line*6, num_line*6, 0.0);

		int i;

		for(i=0; i<(int)num_line; i++)
		{
			CSMMatrix<double> mat = N_2dot_line_list.Subset(0, i*6, 6, 6);
			retmat.Insert(i*6, i*6, mat);
		}

		return retmat;
	}

	CSMMatrix<double> Get_N_2dot_triangle()
	{
		CSMMatrix<double> retmat;
		retmat.Resize(num_triangle*9, num_triangle*9, 0.0);

		int i;

		for(i=0; i<(int)num_triangle; i++)
		{
			CSMMatrix<double> mat = N_2dot_triangle_list.Subset(0, i*9, 9, 9);
			retmat.Insert(i*9, i*9, mat);
		}

		return retmat;
	}

	/**Parameters' constraints*/
	double Fill_Nmat_EOP(CSMMatrix<double> a, CSMMatrix<double> w, CSMMatrix<double> l, int index)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a_t = a.Transpose();
		
		temp = a_t%w%a;
		N_dot_list.InsertPlus(0, index*num_param,temp);

		temp = a_t%w%l;
		C_dot.InsertPlus(index*num_param, 0, temp);

		//Posteriori variance
		double etpe = (l.Transpose()%w%l)(0,0);
		Pos_Var += etpe;
		
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		//Number of equations
		num_eq += l.GetRows();

		return etpe;
	}

	/**fill the normal matrix for the point data*/
	void Fill_Nmat_Data_Point(CSMMatrix<double> a1, CSMMatrix<double> a2, CSMMatrix<double> w, CSMMatrix<double> l, int point_index, int image_index)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a1_t; a1_t = a1.Transpose();
		CSMMatrix<double> a2_t; a2_t = a2.Transpose();
		
		temp = a1_t%w%a1;
		N_dot_list.InsertPlus(0,image_index*num_param,temp);

		temp = a1_t%w%a2;
		N_bar.InsertPlus(image_index*num_param,point_index*3,temp);

		temp = a2_t%w%a2;
		N_2dot_point_list.InsertPlus(0, point_index*3, temp);

		temp = a1_t%w%l;
		C_dot.InsertPlus(image_index*num_param, 0, temp);

		temp = a2_t%w%l;
		C_2dot_point.InsertPlus(point_index*3, 0, temp);

		//Posteriori variance
		Pos_Var +=(l.Transpose()%w%l)(0,0);
		
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		//Number of equations
		num_eq += 2;
	}

	/**fill the normal matrix for the ending point data of the line*/
	void Fill_Nmat_Data_PointofLine(CSMMatrix<double> a1, CSMMatrix<double> a2, CSMMatrix<double> w, CSMMatrix<double> l, int line_index, int image_index)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a1_t; a1_t = a1.Transpose();
		CSMMatrix<double> a2_t; a2_t = a2.Transpose();
		
		temp = a1_t%w%a1;
		N_dot_list.InsertPlus(0,image_index*num_param,temp);

		temp = a1.Transpose()%w%a2;
		N_bar.InsertPlus(image_index*num_param,num_point*3+line_index*6,temp);

		temp = a2_t%w%a2;
		N_2dot_line_list.InsertPlus(0, line_index*6, temp);

		temp = a1_t%w%l;
		C_dot.InsertPlus(image_index*num_param, 0, temp);

		temp = a2_t%w%l;
		C_2dot_line.InsertPlus(line_index*6, 0, temp);

		//Posteriori variance
		Pos_Var +=(l.Transpose()%w%l)(0,0);
		
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		//Number of equations
		num_eq += 2;
	}

	/**fill the normal matrix for the line data*/
	void Fill_Nmat_Data_Line(CSMMatrix<double> a1, CSMMatrix<double> a2, CSMMatrix<double> w, CSMMatrix<double> l, int line_index, int image_index)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a1_t; a1_t = a1.Transpose();
		CSMMatrix<double> a2_t; a2_t = a2.Transpose();
		
		temp = a1_t%w%a1;
		N_dot_list.InsertPlus(0,image_index*num_param,temp);

		temp = a1_t%w%a2;
		N_bar.InsertPlus(image_index*num_param,num_point*3+line_index*6,temp);

		temp = a2_t%w%a2;
		N_2dot_line_list.InsertPlus(0, line_index*6, temp);

		temp = a1_t%w%l;
		C_dot.InsertPlus(image_index*num_param, 0, temp);

		temp = a2_t%w%l;
		C_2dot_line.InsertPlus(line_index*6, 0, temp);

		//Posteriori variance
		Pos_Var +=(l.Transpose()%w%l)(0,0);
		//Error list
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		//Number of equations
		num_eq += 1;
	}

	/**3D ground point constraints*/
	double Fill_Nmat_Data_XYZ(CSMMatrix<double> a2, CSMMatrix<double> w, CSMMatrix<double> l, int point_index)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a2_t; a2_t = a2.Transpose();
		
		temp = a2_t%w%a2;
		N_2dot_point_list.InsertPlus(0, point_index*3, temp);

		temp = a2_t%w%l;
		C_2dot_point.InsertPlus(point_index*3, 0, temp);

		//Posteriori variance
		double etpe = (l.Transpose()%w%l)(0,0);
		Pos_Var += etpe;
		//Error list
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		//Number of equations
		num_eq += 3;

		return etpe;
	}

	/**3D ending point of the line constraints*/
	//
	//2007.05.10
	//I have to check whether this function is
	//required for the line constraint.
	//Maybe, this function can be replaced with "Fill_Nmat_Data_XYZ()"
	//
	//
	void Fill_Nmat_Data_XYZofLine(CSMMatrix<double> a2, CSMMatrix<double> w, CSMMatrix<double> l, int line_index)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a2_t; a2_t = a2.Transpose();
		temp = a2_t%w%a2;
		
		N_2dot_line_list.InsertPlus(0, line_index*6, temp);

		temp = a2_t%w%l;
		C_2dot_line.InsertPlus(line_index*6, 0, temp);

		//Posteriori variance
		Pos_Var +=(l.Transpose()%w%l)(0,0);
		//Error list
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		//Number of equations
		num_eq += 6;
	}

	/**Set configuration data for RunLeastSquare_RN*/
	CSMMatrix<double>* SetDataConfig(unsigned int n_image, unsigned int n_param, unsigned int n_point, unsigned int n_line, unsigned int n_triangle=0)
	{
		num_image = n_image;
		num_param = n_param;
		num_point = n_point;
		num_line = n_line;
		num_triangle = n_triangle;

		num_unknown = num_image*num_param 
			        + num_point*3
					+ num_line*6
					+ num_triangle*9;

		MatrixData temp;

		N_dot_list.Resize(num_param, num_image*num_param, 0.0);//Notice: u by (n*u) matrix
		
		N_2dot_point_list.Resize(3, num_point*3, 0.0);//Notice: 3 by (n*3) matrix

		N_2dot_line_list.Resize(6, num_line*6, 0.0);//Notice: 6 by (n*6) matrix

		N_2dot_triangle_list.Resize(9, num_triangle*9, 0.0);//Notice: 9 by (n*9) matrix

		N_bar.Resize(num_image*num_param,num_point*3+num_line*6+num_triangle*9,0.);

		C_dot.Resize(num_image*num_param, 1, 0.0);

		C_2dot_point.Resize(num_point*3, 1, 0.0);

		C_2dot_line.Resize(num_line*6, 1, 0.0);

		C_2dot_triangle.Resize(num_triangle*9, 1, 0.0);

		//Error.RemoveAll();
		Error.RemoveAll();

		num_eq = 0;

		Pos_Var = 0;

		return &N_dot_list;
	}

protected:
	CSMList<MatrixData> Error;
	CSMMatrix<double> N_dot_list;/**<N_dot_list(EOP(num_param)) matrix: num_param by (num_param*num_image) matrix*/
	CSMMatrix<double> N_2dot_point_list;/**<N_2dot_point(point(3)) matrix: 3 by (num_point*3)*/
	CSMMatrix<double> N_2dot_line_list;/**<N_2dot_line(line(6)) matrix: 6 by (num_line*6)*/
	CSMMatrix<double> N_2dot_triangle_list;/**<N_2dot_triangle(triangle(9)) matrix: 9 by (num_triangle*9)*/
	CSMMatrix<double> N_bar;/**<N_bar matrix*/
	
	CSMMatrix<double> C_dot;/**<C_dot(EOP) matrix*/
	CSMMatrix<double> C_2dot_point;/**<C_2dot_point matrix*/
	CSMMatrix<double> C_2dot_line;/**<C_2dot_line matrix*/
	CSMMatrix<double> C_2dot_triangle;/**<C_2dot_triangle matrix*/

	unsigned int num_image;/**<number of scene data*/
	unsigned int num_param;/**<number of EO parameters*/
	unsigned int num_point;/**<number of points*/
	unsigned int num_line;/**<number of lines*/
	unsigned int num_triangle;/**<number of triangle patches*/

	double Pos_Var;/**<posteriori variance*/
	unsigned int num_unknown;/**<number of all unknown parameters*/
	unsigned int num_eq;/**<number of equations and constraints*/

protected:
	/**Make N_dot matrix from N_dot elements list*/
	CSMMatrix<double> Make_N_dot()
	{
		CSMMatrix<double> retmat(num_image*num_param, num_image*num_param, 0.0);
		for(unsigned int i=0; i<num_image; i++)
		{
			CSMMatrix<double> mat = N_dot_list.Subset(0, i*num_param, num_param, num_param);
			retmat.Insert(i*num_param, i*num_param, mat);
		}
		return retmat;
	}

	/**Make C_dot matrix from C_dot elements list*/
	CSMMatrix<double> Make_C_dot()
	{
		return C_dot;
	}

	/**Make C_2dot matrix from C_2dot_point and C_2dot_line elements list & triangle patches for LIDAR*/
	CSMMatrix<double> Make_C_2dot()
	{
		int count = 3*num_point + 6*num_line + 9*num_triangle;
		
		CSMMatrix<double> retmat;
		retmat.Resize(count,1);

		retmat.Insert(0, 0, C_2dot_point);
		retmat.Insert(3*num_point, 0, C_2dot_line);
		retmat.Insert(3*num_point + 6*num_line, 0, C_2dot_triangle);

		return retmat;
	}

	/**write given string on given file*/
	void FileOut(CString fname, CString st)
	{
		FILE* outfile;
		outfile = NULL;
		outfile = fopen(fname,"a");
		if(outfile == NULL) 
			outfile = fopen(fname,"w");
		fprintf(outfile,st);
		fclose(outfile);
	}

public:
	/**make N matrix image*/
	bool MakeMatrixImg(double threshold, CString imgname)
	{
		int i, j, k;
		int offset;

		int size = num_image*num_param 
			        + num_point*3
					+ num_line*6
					+ num_triangle*9;

		if(size > 10000) return false;

		//
		//Normal Matrix Image
		//
		CBMPImage img;
		img.Create(size,size,8);

		CBMPPixelPtr ptr(img);

		CSMMatrix<double> N_dot = Make_N_dot();

		//N_dot
		for(i=0; i<int(num_image*num_param); i++)
		{
			for(j=0; j<int(num_image*num_param); j++)
			{
				if(N_dot(i,j)==0.) ptr[i][j] = BYTE(0);
				else ptr[i][j] = BYTE(255);
			}
		}
		
		//N_bar
		offset = num_image*num_param;
		for(i=0; i<int(num_image*num_param); i++)
		{
			for(j=0; j<int(num_point*3 + num_line*6 + num_triangle*9); j++)
			{
				if(N_bar(i,j)==0.)
				{
					ptr[i][j + offset] = BYTE(0);
					ptr[j + offset][i] = BYTE(0);
				}
				else
				{
					ptr[i][j + offset] = BYTE(255);
					ptr[j + offset][i] = BYTE(255);
				}
			}
		}

		//N_2dot_point
		for(i=0; i<int(num_point); i++)
		{
			for(j=0; j<3; j++)
			{
				for(k=0; k<3; k++)
				{
					if(N_2dot_point_list(j,i*3 + k)==0.) ptr[offset + i*3 + j][offset + i*3 + k] = BYTE(0);
					else ptr[offset + i*3 + j][offset + i*3 + k] = BYTE(255);
				}
			}
		}

		//N_2dot_line
		offset += num_point*3;
		for(i=0; i<int(num_line); i++)
		{
			for(j=0; j<6; j++)
			{
				for(k=0; k<6; k++)
				{
					if(N_2dot_line_list(j,i*6 + k)==0.) ptr[offset + i*6 + j][offset + i*6 + k] = BYTE(0);
					else ptr[offset + i*6 + j][offset + i*6 + k] = BYTE(255);
				}
			}
		}

		//N_2dot_triangle
		offset += num_line*6;
		for(i=0; i<int(num_triangle); i++)
		{
			for(j=0; j<9; j++)
			{
				for(k=0; k<9; k++)
				{
					if(N_2dot_triangle_list(j,i*9 + k)==0.) ptr[offset + i*9 + j][offset + i*9 + k] = BYTE(0);
					else ptr[offset + i*9 + j][offset + i*9 + k] = BYTE(255);
				}
			}
		}

		CString imgname_N = imgname;
		imgname_N.MakeLower(); imgname_N.Replace(".bmp", "_N_.bmp");
		img.SaveBMP((LPCTSTR)imgname_N);

		//
		//Correlation Matrix Image
		//
		
		CSMMatrix<double> Cor_mat = Correlation(N_dot.Inverse());
		
		CBMPImage img_cor;
		int img_cor_size = Cor_mat.GetCols();
		img_cor.Create(img_cor_size, img_cor_size,8);

		CBMPPixelPtr ptr_cor(img_cor);

		//Cor_mat (num_image*num_param)
		for(i=0; i<int(img_cor_size); i++)
		{
			for(j=0; j<int(img_cor_size); j++)
			{
				double cor_val = fabs(Cor_mat(i,j));
				if(cor_val <= threshold)
				{
					ptr_cor[i][j] = BYTE(0);
				}
				else
				{
					ptr_cor[i][j] = BYTE((1.0 - cor_val + threshold)/(1.0 - threshold)*255.0 + 0.5);
				}
			}
		}

		CString imgname_Corr = imgname;
		imgname_Corr.MakeLower(); imgname_Corr.Replace(".bmp", "_Corr_.bmp");
		img_cor.SaveBMP((LPCTSTR)imgname_Corr);
		
		return true;
	}

	
};

class CLeastSquareLIDAR : public CLeastSquare  
{
public:
	CSMList<MatrixData> 	LIDARErrorVol;
	CSMList<MatrixData> 	LIDARErrorND;
	
	CSMMatrix<double>* SetDataConfig(unsigned int n_image, unsigned int n_param, unsigned int n_point, unsigned int n_line, unsigned int n_tri)
	{
		CSMMatrix<double>* ret = CLeastSquare::SetDataConfig(n_image, n_param, n_point, n_line, n_tri);

		LIDARErrorVol.RemoveAll();
		LIDARErrorND.RemoveAll();

		return ret;
	}

	void GetLIDARErrorMatwithDeterminent(CSMList<MatrixData> &e_LIDAR, CSMList<MatrixData> &eVol_LIDAR, CSMList<MatrixData> &eND_LIDAR)
	{
		e_LIDAR = Error;
		eVol_LIDAR = LIDARErrorVol;
		eND_LIDAR = LIDARErrorND;
	}

	void GetLIDARErrorMatwithPoint(CSMList<MatrixData> &e_LIDAR)
	{
		e_LIDAR = Error;
	}

	CSMMatrix<double> GetNdotMatrix()
	{
		CSMMatrix<double> N_dot = Make_N_dot();
		return N_dot;
	}

	CSMMatrix<double> GetCdotMatrix()
	{
		CSMMatrix<double> C_dot = Make_C_dot();
		return C_dot;
	}
	
	/**fill the normal matrix for the ending point data of the line*/
	void Fill_Nmat_Data_LIDARPointwithDeterminent(CSMMatrix<double> fixedparam, CSMMatrix<double> fixedpoint, CSMMatrix<double> a1, CSMMatrix<double> a2, CSMMatrix<double> w, CSMMatrix<double> l, int idxTri, double area)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a1_t = a1.Transpose();
		CSMMatrix<double> a2_t = a2.Transpose();

		//int i;
		CSMMatrix<double> param_index = fixedparam.Transpose()%fixedparam;
		
		CSMMatrix<double> point_index = fixedpoint.Transpose()%fixedpoint;
		
		CSMMatrix<double> param_point_index = fixedparam.Transpose()%fixedpoint;

		temp = a1_t%w%a1;
		temp = temp*param_index;
		N_dot_list.InsertPlus(0,0,temp);
		
		temp = a1_t%w%a2;
		temp = temp*param_point_index;
		N_bar.InsertPlus(0*num_param,num_point*3+num_line*6+idxTri*9,temp);

		temp = a2_t%w%a2;
		temp = temp*point_index;
		
		N_2dot_triangle_list.InsertPlus(0, idxTri*9, temp);

		temp = a1_t%w%l;
		temp = temp*fixedparam.Transpose();
		C_dot.InsertPlus(0, 0, temp);
		
		temp = a2_t%w%l;
		temp = temp*fixedpoint.Transpose();
		C_2dot_triangle.InsertPlus(idxTri*9, 0, temp);
		
		//Posteriori variance
		Pos_Var +=(l.Transpose()%w%l)(0,0);
		
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		CSMMatrix<double> errormat;

		CSMMatrix<double> errormatVol;
		errormat = l/6.;
		mat.mat = errormat;
		LIDARErrorVol.AddTail(mat);

		CSMMatrix<double> errormatND;
		errormat = l/2./area;
		mat.mat = errormat;
		LIDARErrorND.AddTail(mat);
		
		//Number of equations
		num_eq += l.GetRows();
	}

	/**fill the normal matrix for the ending point data of the line*/
	double Fill_Nmat_Data_LIDARPointwithPoint(CSMMatrix<double> fixedparam, CSMMatrix<double> fixedpoint, CSMMatrix<double> a1, CSMMatrix<double> a2, CSMMatrix<double> w, CSMMatrix<double> l, int idxpoint)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a1_t = a1.Transpose();
		CSMMatrix<double> a2_t = a2.Transpose();

		//int i;
		CSMMatrix<double> param_index = fixedparam.Transpose()%fixedparam;
				
		CSMMatrix<double> point_index = fixedpoint.Transpose()%fixedpoint;
		
		CSMMatrix<double> param_point_index = fixedparam.Transpose()%fixedpoint;

		temp = a1_t%w%a1;
		temp = temp*param_index;
		
		N_dot_list.InsertPlus(0,0,temp);
		
		temp = a1_t%w%a2;
		temp = temp*param_point_index;
		N_bar.InsertPlus(0*num_param,idxpoint*3+num_line*6+num_triangle*9,temp);

		temp = a2_t%w%a2;
		temp = temp*point_index;
		
		N_2dot_point_list.InsertPlus(0, idxpoint*3, temp);

		temp = a1_t%w%l;
		temp = temp*fixedparam.Transpose();
		C_dot.InsertPlus(0, 0, temp);
		
		temp = a2_t%w%l;
		temp = temp*fixedpoint.Transpose();
		C_2dot_point.InsertPlus(idxpoint*3, 0, temp);
		
		//Posteriori variance
		double etpe = (l.Transpose()%w%l)(0,0);
		Pos_Var += etpe;
		
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		//Number of equations
		num_eq += l.GetRows();

		return etpe;
	}

	/**Parameters' constraints*/
	double Fill_Nmat_LIDARParam(CSMMatrix<double> a, CSMMatrix<double> w, CSMMatrix<double> l)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a_t = a.Transpose();
		
		temp = a_t%w%a;
		N_dot_list.InsertPlus(0,0,temp);

		temp = a_t%w%l;
		C_dot.InsertPlus(0, 0, temp);

		//Posteriori variance
		double etpe = (l.Transpose()%w%l)(0,0);
		Pos_Var += etpe;
		
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		//Number of equations
		num_eq += l.GetRows();

		return etpe;
	}

	/**Parameters' constraints*/
	void Fill_Nmat_TriangleVertex(CSMMatrix<double> a2, CSMMatrix<double> w, CSMMatrix<double> l, unsigned int idxTri)
	{
		CSMMatrix<double> temp;
		CSMMatrix<double> a2_t; a2_t = a2.Transpose();
		temp = a2_t%w%a2;
		N_2dot_triangle_list.InsertPlus(0, idxTri*9, temp);

		temp = a2_t%w%l;
		C_2dot_triangle.InsertPlus(idxTri*9, 0, temp);

		//Posteriori variance
		Pos_Var +=(l.Transpose()%w%l)(0,0);
		
		MatrixData mat;
		mat.mat = l;
		Error.AddTail(mat);

		//Number of equations
		num_eq += l.GetRows();
	}

};

class C3DRigidBodyTransformation : public CLeastSquare
{
public:
	
protected:
	
	CSMList<MatrixData> Error;
	CSMMatrix<double> N_dot_list;/**<N_dot_list(EOP(num_param)) matrix: num_param by (num_param*num_image) matrix*/
	CSMMatrix<double> N_2dot_point_list;/**<N_2dot_point(point(3)) matrix: 3 by (num_point*3)*/
	CSMMatrix<double> N_2dot_line_list;/**<N_2dot_line(line(6)) matrix: 6 by (num_line*6)*/
	CSMMatrix<double> N_2dot_triangle_list;/**<N_2dot_triangle(triangle(9)) matrix: 9 by (num_triangle*9)*/
	CSMMatrix<double> N_bar;/**<N_bar matrix*/
	
	CSMMatrix<double> C_dot;/**<C_dot(EOP) matrix*/
	CSMMatrix<double> C_2dot_point;/**<C_2dot_point matrix*/
	CSMMatrix<double> C_2dot_line;/**<C_2dot_line matrix*/
	CSMMatrix<double> C_2dot_triangle;/**<C_2dot_triangle matrix*/
	
	unsigned int num_image;/**<number of scene data*/
	unsigned int num_param;/**<number of EO parameters*/
	unsigned int num_point;/**<number of points*/
	unsigned int num_line;/**<number of lines*/
	unsigned int num_triangle;/**<number of triangle patches*/
	
	double Pos_Var;/**<posteriori variance*/
	unsigned int num_unknown;/**<number of all unknown parameters*/
	unsigned int num_eq;/**<number of equations and constraints*/
};

#endif // !defined(__BBARAB_LEAST_SQUARE_ADJUSTMENT_CLASS__)