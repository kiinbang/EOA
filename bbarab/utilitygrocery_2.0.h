/*
* Copyright (c) 2005-2006, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include <fstream>
#include <math.h>
#include <string>
#include <vector>

using namespace std;

namespace util
{
	
#define MAX_LINE_LENGTH 512
#define PI 3.14159265358979
	
	/**GetPI
	* Description	    : to get pi(3.14159265....)
	*@param void
	*@return inline double : PI
	*/
	double GetPI(void);
	
	/**Rad2Deg
	* Description	    : to convert the degree to the radian
	*@param double rad
	*@return inline double 
	*/
	double Rad2Deg(double rad);
	
	/**Deg2Rad
	* Description	    : to convert the radian to the degree
	*@param double deg
	*@return inline double 
	*/
	inline double Deg2Rad(double deg);
	
	/**RemoveCommentLine
	* Description	    : inline function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@return void 
	*/
	void RemoveCommentLine(fstream &file, char delimiter);
	
	/**RemoveCommentLine
	* Description	    : inline function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@param in num: number of delimiters
	*@return void 
	*/
	void RemoveCommentLine(fstream &file, char* delimiter, int num);
	
	/**RemoveCommentLine
	* Description	    : inline function for removing comments in the input file
	*@param FILE* file : FILE struct pointer
	*@param char delimiter: delimiter
	*@return void 
	*/
	void RemoveCommentLine(FILE* file, char delimiter);
	
	/**FindNumber
	* Description	    : inline function for removing delimiter in the input file
	*@param fstream file : file stream
	*@return void 
	*/
	void FindNumber(fstream &file);
	
	/**FindString
	* Description	    : inline function for finding given string in the input file
	*@param fstream file : file stream
	*@param char* target : target string
	*@return void 
	*/
	bool FindString(fstream &fin, const char target[], int &result);
	
	/**FindString
	* Description	    : checking version of the given text file
	*@param fstream file : file stream
	*@return float: version number 
	*/
	float VersionCheck(fstream &fin);
	
	/**CalAzimuth
	* Description	    : calculate azimuth with 2 points
	*@param double Xa	: X coordinate
	*@param double Ya	: Y coordinate
	*@param double Xb	: X coordinate
	*@param double Yb	: Y coordinate
	*@return double: azimuth
	*/
	double CalAzimuth(double Xa, double Ya, double Xb, double Yb);
	
	/**MeridianConversion
	* Description	    : calculate azimuth with 2 points
	*@param double Lon	: longitude
	*@param double Lat	: latitude
	*@param double CenterLon	: longitude of center meridian
	*@return double: meridian conversion
	*/
	double MeridianConversion(double Lon, double Lat, double CenterLon);
	
	/**CentralMeridian
	* Description	    : calculate azimuth with 2 points
	*@param int zone	: UTM zone
	*@return double: central meridian (rad)
	*/
	double CentralMeridian(int zone);
	
	/**Line2DIntersect
	* Description	    :  find intersect point between two lines
	*@param double x1, y1 : given point of line
	*@param double x2, y2 : given point of line
	*@param double x3, y3 : given point of line
	*@param double x4, y4 : given point of line
	*@param double &x, &y : intersection point
	*@return int : -3 (parallel) -2(same) -1 (Not line) 0 (out of ranges) 1(intersection within ranges) 2 (end point)
	*/
	int Line2DIntersect(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double &x, double &y);
	
	/**Inside2DPolygon
	* Description	    :  topology between line and point
	*@param double *x, *y	: vertices
	*@param double n		  : number of vertices
	*@param double Tx, Ty	: target point
	*@return int	: -1 (not polygon) 0 (outside) 1 (inside)
	*/
	int Inside2DPolygon(double *x, double* y, int n, double Tx, double Ty);
	
	/**IntersectPlaneLine
	* Description	    :  intersection between a line and a plane
	*@param double *N: normal vector of a plane
	*@param double *V: a point on a plane
	*@param double *P1, P2: two point on a line
	*@param double *P: intersection point
	*@return bool: [false] //out of two point [true]
	*/
	bool IntersectPlaneLine(double *N, double *V, double *P1, double *P2, double *P);

	/**IntersectPlaneLine
	* Description	    :  intersection between a line and a plane
	*@param double *A: parameters (A0X + A1Y + A2Z = 1)
	*@param double *P1, P2: two point on a line
	*@param double *P: intersection point
	*@return bool: [false] //out of two point [true]
	*/
	bool IntersectPlaneLine(double *A, double *P1, double *P2, double *P);
	
	/**IntersectPointBetweenOnePointandOneLine
	* Description	    :  intersection between a line and a point
	*@param double *P0: given one point
	*@param double *PA, *PB: two end points of a line
	*@param double *P: intersection of P0 and a line
	*@return bool: false and true
	*/
	bool IntersectPointBetweenOnePointandOneLine(double *P0, double *Pa, double *Pb, double *P);
	
	/**Area_of_Polygon2D
	* Description	    :  area of a polygon
	*@param double *x: list of x coordinates
	*@param double *y: list of y coordinates
	*@param int n: number of vertices
	*@return double: area
	*/
	double Area_of_Polygon2D(double* x, double* y, int n);
	
	//Extract angle from normal vector
	/**Area_of_Polygon2D
	* Description	    :  area of a polygon
	*@param double *x: list of x coordinates
	*@param double *y: list of y coordinates
	*@param int n: number of vertices
	*@return double: area
	*/
	bool ExtractEulerAngles(double dX, double dY, double dZ, double &O, double &P);
	
	//Randomly select string lines from a txt file
	bool RandomTXTLIneExtraction(int numlines, char* ptspath, char* outpath);
	
	/// File extention check
	bool ExtCheck(char *path, char* ext, bool bMatchCase);
	
	/// Txt file open for reading
	bool ASCIIReadOpen(fstream &infile, char* path);
	
	//T/ xt file open for writing
	bool ASCIISaveOpen(fstream &outfile, char* path);
	
	/// Binary file open for reading
	bool BINReadOpen(fstream &infile, char* path);
	
	/// Binary file open for writing
	bool BINSaveOpen(fstream &outfile, char* path);
	
	/// Precision of txt file setting
	void ASCIIFilePrecision(fstream &file, unsigned int precision);

	/// Get file size
	long long fileLength(const string& filename);

	/// Print a debug message
	void debugMsg(std::string msg);

	/// Conduct plane fitting
	bool estimatePlane(const std::vector<double>& x, const std::vector<double>& y, const std::vector<double>& z);

	/// Get system environment variable
	void getSysVariable(size_t& len, char* val, const size_t valLength, const char* name);

}