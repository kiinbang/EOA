/*
 * Copyright (c) 2006-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// LIDARRawPointList.h: interface for the CLIDARRawPointList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__INTERSOLUTION__SPACEMATICS__BBARAB__LIDAR__RAW__DATA__INFOMATION___)
#define __INTERSOLUTION__SPACEMATICS__BBARAB__LIDAR__RAW__DATA__INFOMATION___

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMList.h"
#include "UtilityGrocery.h"
#include "SMDataStruct.h"
#include <math.h>

namespace SMATICS_BBARAB
{
	class CSMLIDARConfig
	{
	public:
		
		CSMLIDARConfig()
		{
			Init();
		}
		virtual ~CSMLIDARConfig(){}
		
	public:
		
		int ID;
		//boresight 
		//[meter]
		double Xb;/**<Xoffset: spatial offset in boresight */
		double Yb;/**<Yoffset: spatial offset in boresight */
		double Zb;/**<Zoffset: spatial offset in boresight */
		
		//[meter]
		double sXb, sYb, sZb;/**<sigma of spatial bias in boresight Ground coordinate */
		
		//[rad]
		double Ob;/**<Ooffset: rotational offset in boresight */
		double Pb;/**<Poffset: rotational offset in boresight */
		double Kb;/**<Koffset: rotational offset in boresight */		
		
		//[rad]
		double sOb, sPb, sKb;/**<sigma of rotational bias in boresight Ground coordinate */
		
		//[meter]
		double Rangeb;/**<Db: bias in ranging distance */
		
		//[meter]
		double sRange; /**<sigma of ranging distance */
		
		//[meter]
		double sRangeb; /**<sigma of bias in ranging distance */
		
		double RangeS; /**<Scale of laser ranging distance */
		
		double sRangeS; /**<sigma of ranging distance scale */
		
		//[meter]
		double sX, sY, sZ; /**<sigma of GNSS signal*/
		
		//[rad]
		double sO, sP, sK; /**<sigma of INS signal */
		
		//[rad]
		double sAlpha, sBeta; /**<sigma of swing angle */
		
		//[rad]
		double Alpha_b, Beta_b;/**<bias of swing angle */
		
		//[rad]
		double sAlpha_b, sBeta_b;/**<sigma of swing angle bias*/
		
	public:
		
		void Init(void)
		{
			ID=0;
				
			Xb=0;	Yb=0;	Zb=0;
			
			sXb=0;	sYb=0;	sZb=0;
			
			Ob=0;	Pb=0;	Kb=0;
			
			sOb=0;	sPb=0;	sKb=0;
			
			sRange = 0;
			
			Rangeb=0;	sRangeb=0;	
			
			RangeS=1.0;	sRangeS=0;
			
			sX=0; sY=0; sZ=0;
			
			sO=0;	sP=0;	sK=0;
			
			sAlpha=0;	sBeta=0;
			
			Alpha_b=0; Beta_b=0;
			
			sAlpha_b=0; sBeta_b=0;
			
		}
		
		//	/**ReadConfigFile
		//	* Description	    : To read configuration data
		//	*@param CString cfgfile: configuration file path
		//	*@return void
		//	*/
		bool ReadConfigFile(CString configfile, int EQtype)
		{
			fstream CfgFile;
			//double OX, OY, OZ;//Spatial offsets(3EA, M)
			double OXb, OYb, OZb;//Biases in spatial offsets(3EA, M)
			double sOXb, sOYb, sOZb;//Sigma of spatial offsets biases(3EA, M)
			double OOb, OPb, OKb;//Biases in rotational offsets(3EA, deg)
			double sOOb, sOPb, sOKb;//Sigma of rotational offsets biases(3EA, deg)
			double Rangeb, sRangeb;//Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)
			double sX, sY, sZ;//sigma of GPS signal
			double sO, sP, sK;//sigma of INS signal
			double sAlpha, sBeta;//sigma of swing angle
			//int cfgid;
			
			///////////////////////////////////////////////////////////////////////////////////////////
			
			CfgFile.open(configfile, ios::in|ios::nocreate);
			
			if(!CfgFile)
				return false;
			
			//version check
			int pos;
			double ver_num = 1.0;
			bool bVersion = FindString(CfgFile, "VER", pos);
			if(bVersion == true)
			{
				//To move to begin of file
				CfgFile.seekg(0,ios::beg);
				char temp[256];//"VER"
				CfgFile>>temp>>ver_num;//version
			}
			else
			{
				//To move to begin of file
				CfgFile.seekg(0,ios::beg);//old version without version number(before ver 1.1)
			}
			
			if(ver_num >= 1.3)
			{
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>OXb>>OYb>>OZb;//spatial offet bias
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>sOXb>>sOYb>>sOZb;//sigma
				
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>OOb>>OPb>>OKb;//rotational offset bias
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>sOOb>>sOPb>>sOKb;//sigma
			}
			else
			{
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				double OXb2, OYb2, OZb2;
				CfgFile>>OXb2>>OYb2>>OZb2;//spatial offset
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>OXb>>OYb>>OZb;//spatial offet bias
				OXb += OXb2;
				OYb += OYb2;
				OZb += OZb2;
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>sOXb>>sOYb>>sOZb;//sigma
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				double OOb2, OPb2, OKb2;
				CfgFile>>OOb2>>OPb2>>OKb2;//rotational offset
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>OOb>>OPb>>OKb;//rotational offset bias
				OOb += OOb2;
				OPb += OPb2;
				OKb += OKb2;
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>sOOb>>sOPb>>sOKb;//sigma
			}
			
			if(ver_num >= 1.2)
			{
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>Alpha_b>>Beta_b;
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>sAlpha_b>>sBeta_b;
			}
			
			RemoveCommentLine(CfgFile, '#');
			CfgFile.eatwhite();
			CfgFile>>Rangeb>>sRangeb;
			
			if(ver_num >= 1.2)
			{
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>RangeS>>sRangeS;
			}
			
			//###############################################################
			
			if(ver_num >= 1.1)
			{
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>sX>>sY>>sZ;
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>sO>>sP>>sK;
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>sAlpha>>sBeta;
			}
			
			if(ver_num >= 1.2)
			{
				RemoveCommentLine(CfgFile, '#');
				CfgFile.eatwhite();
				CfgFile>>sRange;
			}
			
			RemoveCommentLine(CfgFile, '#');
			CfgFile.eatwhite();
			//CfgFile>>cfgid;
			
			CfgFile.close();
			///////////////////////////////////////////////////////////////////////////////////////////////
			
			//ID = cfgid;		
			
			Ob = Deg2Rad(OOb);
			Pb = Deg2Rad(OPb);
			Kb = Deg2Rad(OKb);
			
			
			sOb = Deg2Rad(sOOb);
			sPb = Deg2Rad(sOPb);
			sKb = Deg2Rad(sOKb);
			
			Xb = OXb;
			Yb = OYb;
			Zb = OZb;
						
			sXb = sOXb;
			sYb = sOYb;
			sZb = sOZb;
			
			if(ver_num >= 1.2)
			{
				Alpha_b = Deg2Rad(Alpha_b);
				sAlpha_b = Deg2Rad(sAlpha_b);
				
				Beta_b = Deg2Rad(Beta_b);
				sBeta_b = Deg2Rad(sBeta_b);
			}
			
			this->Rangeb = Rangeb;
			this->sRangeb = sRangeb;
			
			if(ver_num >= 1.1)
			{
				this->sX = sX;
				this->sY = sY;
				this->sZ = sZ;
				
				this->sO = Deg2Rad(sO);
				this->sP = Deg2Rad(sP);
				this->sK = Deg2Rad(sK);
				
				this->sAlpha = Deg2Rad(sAlpha);
				this->sBeta = Deg2Rad(sBeta);
			}
			
			switch(EQtype)
			{
			case 0://Terrapoint
				this->Beta_b = - Beta_b;
				this->Kb = - Kb;
				break;
			case 1://Terrapoint(plus90)
				break;
			case 2://Simulator data
				this->Beta_b = - Beta_b;
				this->Rangeb = - Rangeb;
				break;
			case 3://etc
				//this->Beta_b = - Beta_b;
				//this->Rangeb = - Rangeb;
				break;
			default:
				AfxMessageBox("option fail: ReadConfigFile()");
				return false;
				break;
			}

			return true;
		}
		
		void CheckZero(double threshold = 1.0e-30)
		{
			if(fabs(sXb)<=threshold)	sXb = threshold;
			if(fabs(sYb)<=threshold)	sYb = threshold;
			if(fabs(sZb)<=threshold)	sZb = threshold;
			
			if(fabs(sOb)<=threshold)	sOb = threshold;
			if(fabs(sPb)<=threshold)	sPb = threshold;
			if(fabs(sKb)<=threshold)	sKb = threshold;
			
			
			if(fabs(sAlpha_b)<=threshold)	sAlpha_b = threshold;
			if(fabs(sBeta_b)<=threshold)	sBeta_b = threshold;
			
			if(fabs(sRangeb)<=threshold)	sRangeb = threshold;
			if(fabs(sRangeS)<=threshold)	sRangeS = threshold;
			
			///////////////////////////////////////////////////////////////////////////
			
			if(fabs(sO)<=threshold)	sO = threshold;
			if(fabs(sP)<=threshold)	sP = threshold;
			if(fabs(sK)<=threshold)	sK = threshold;
			
			if(fabs(sX)<=threshold)	sX = threshold;
			if(fabs(sY)<=threshold)	sY = threshold;
			if(fabs(sZ)<=threshold)	sZ = threshold;
			
			if(fabs(sAlpha)<=threshold)	sAlpha = threshold;
			if(fabs(sBeta)<=threshold)	sBeta = threshold;
			
			if(fabs(sRange)<=threshold)	sRange = threshold;
		}
	};
	
}//namespace SMATICS_BBARAB

#endif // !defined(__INTERSOLUTION__SPACEMATICS__BBARAB__LIDAR__RAW__DATA__INFOMATION___)
