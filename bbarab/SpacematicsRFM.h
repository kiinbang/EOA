// SpacematicsRFM.h: interface for the CRFM class.

/**********************/
/*RFM Manipulate Class*/
/**********************/

/********************/
/*made by ki-In Bang*/
/********************/

/***************************/
/*revision date: 2002-06-16*/
/***************************/
//CRFM 작성
/***************************/
/*revision date: 2002-06-25*/
/***************************/
//CRFMIntersection 추가
//현재 복사생성자 및 대입연산자 불완전

#if !defined(AFX_RFM_H__C9B196E1_01A6_40D2_B8F5_8AB1563F81B7__INCLUDED_)
#define AFX_RFM_H__C9B196E1_01A6_40D2_B8F5_8AB1563F81B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Matrix.h"
#include "SMCoordinateClass.h"
#include "GCPManage.h"
#include "Transformation.h"

#define NUM_NORMALIZE_PARAM 5
#define NUM_RPC 20

#define NUMDC 30
#define NUMRPCPARAM 59

class CRFM;
class CRFMIntersection;

/****************************/
/*Class for RFM Sensor Model*/
/****************************/
class CRFM //friend CIntersection
{
public:

	/*CRFMIntersection friend class 선언*/
	friend class CRFMIntersection;
	
	CRFM();
	CRFM(CRFM &copy);
	virtual ~CRFM();

public:
	/*RPC Parameters*/
	//x = A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
	//	  A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
	//	  A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;
	/*Offset*/
	//Line Offset, Column Offset, Latitude Offset, Longitude Offset, Height Offset
	/*Scale*/
	//Line Scale, Column Scale, Latitude Scale, Longitude Scale, Height Scale
	
	/*Set RPC Parameters and Offset/Scale*/
	void SetParameters(/*array size NUM_RPC*/double *Col_NUM,
                       /*array size NUM_RPC*/double *Col_DEN,
					   /*array size NUM_RPC*/double *Row_NUM,
					   /*array size NUM_RPC*/double *Row_DEN,
		               /*offset value array size 5*/double *Offset, 
				       /*scale value array size 5*/double *Scale);
	//Initialize RFM
	void Init(void);
	//Open IKONOS RPC
	bool OpenIKONOSRPC(LPCTSTR rpcpath);
	//Write IKONOS RPC
	bool WriteIKONOSRPC(LPCTSTR rpcpath);
	//Get IKONOS RPC as String
	CString GetIKONOSRPCasString();

	/**********************/
	/*Coordinate Transform*/
	/**********************/

	/*Get Image Coordinates from Lat, Lon, H*/
	void GetGroundCoord2ImageCoord(double Lat, double Lon, double H, double *col, double *row);

	/*Get Image Coordinates from Lat, Lon, H*/
	void GetImprovedGroundCoord2ImageCoord(double Lat, double Lon, double H, double *col, double *row);

	/*Get Normalized Image Coordinates from Lat, Lon, H*/
	void GetGroundCoord2NormalImageCoord(double Lat, double Lon, double H, double *Ncol, double *Nrow);

	/*Get Normalized Image Coordinates from Normalized Lat, Lon, H*/
	void GetNormalGroundCoord2NormalImageCoord(double NLat, double NLon, double NH, double *Ncol, double *Nrow);

	/*Get Normalized Image Coordinates from Image Coord*/
	void GetImageCoord2NormalImageCoord(double col, double row, double *Ncol, double *Nrow);

	/*Get Image Coordinates from Normalized Image Coord*/
	void GetNormalImageCoord2ImageCoord(double Ncol, double Nrow, double *col, double *row);

	/*Get Normalized Ground Coordinates from Ground Coord*/
	void GetGroundCoord2NormalGroundCoord(double Lat, double Lon, double H, double *NLat, double *NLon, double *NH);

	/*Get Ground Coordinates from Normalized Ground Coord*/
	void GetNormalGroundCoord2GroundCoord(double NLat, double NLon, double NH, double *Lat, double *Lon, double *H);

	/************/
	/*Data Cubic*/
	/************/

	/*Set Data Cubic*/
	bool SetDataCubic(void);

	/*Get Data Cubic*/
	bool GetDataCubic(double *u, double *v, double *w, double *x, double *y);


	/*********************/
	/*RPC Update Solution*/
	/*********************/

	/*RPC Update by Data Cubic(Pseudo GCP)*/
	CSMStr DoRPCUpdatebyDataCubic(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);

	/*RPC Update by Sequential LS*/
	CSMStr DoRPCUpdatebySequentialLS_ParamObs(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);
	
	/*RPC Update by Sequential LS*/
	CSMStr DoRPCUpdatebySequentialLS_DataCubic(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);

	/*RPC Update by Parameters Observation*/
	CSMStr DoRPCUpdatebyParamObs(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);

	/*RPC Update by removing bias_rigid body transform*/
	CSMStr DoRPCUpdatebyRemoveBias_Rigid(void);
	/*RPC Update by removing bias_image coord shift*/
	CSMStr DoRPCUpdatebyRemoveBias_Shift(void);

	/*RPC Update by Data Cubic2005(Pseudo GCP)*/
	CSMStr DoRPCUpdatebyDataCubic2005(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);
	CSMStr DataNormalization();
	CSMStr DoRPCUpdatebyDataCubic2005_Init(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);
	CSMStr DoRPCUpdatebyDataCubic2005_1(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);
	CSMStr DoRPCUpdatebyDataCubic2005_2(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);
	CSMStr DoRPCUpdatebyDataCubic2005_3(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);

	/*RFM Improvement by 2D Transformation_additional transform (affine)*/
	CSMStr DoRFMUpdatebyTransform(unsigned int option = 1, double MaxCorrection=1.0e-6, unsigned short MaxIteration=20);

	/*********************/
	/*RPC Update Solution*/
	/*Adapt Linear Form */
	/*********************/

	/*RPC Update by Data Cubic(Pseudo GCP) with linear form function*/
	CSMStr DoRPCUpdatebyDataCubic_Linear(double MaxCorrection=1.0e-6, unsigned short MaxIteration=20, double sd = 0.1, double h = 0.001);

	
	/**************/
	/*GCP Handling*/
	/**************/

	/*Insert GCP*/
	bool InsertGCP(double Lat, double Lon, double H, double Col, double Row, long id);

	/*Get GCP*/
	bool GetGCP(unsigned int index, double *Lat, double *Lon, double *H, double *Col, double *Row);

	/*Get Addition Transform Availability*/
	bool Getadditional_transform(void) {return additional_transform;}


public:

	/* = operator */
	void operator = (CRFM &copy);

	/*Get Denominator of column*/
	inline double* GetColDEN(void)
	{
		return dColDEN;
	}

	/*Get Numerator of column*/
	inline double* GetColNUM(void)
	{
		return dColNUM;
	}

	/*Get Denominator of row*/
	inline double* GetRowDEN(void)
	{
		return dRowDEN;
	}

	/*Get Numerator of row*/
	inline double* GetRowNUM(void)
	{
		return dRowNUM;
	}

	/*Get Coordinates Offset*/
	inline double* GetCoordOffset(void)
	{
		return dCoordOffset;
	}

	/*Get Coordinates Scale*/
	inline double* GetCoordScale(void)
	{
		return dCoordScale;
	}


private:

	/***************************/
	/*Denominator and Numerator*/
	/***************************/

	/*Get Col Denominator*/
	double GetColDenominator(double Lat_, double Lon_, double H_);
	
	/*Get Col Numerator*/
	double GetColNumerator(double Lat_, double Lon_, double H_);
	
	/*Get Row Denominator*/
	double GetRowDenominator(double Lat_, double Lon_, double H_);
	
	/*Get Row Numerator*/
	double GetRowNumerator(double Lat_, double Lon_, double H_);

	/**************************/
	/*Differential Coefficient*/
	/**************************/

	/*Differential Coefficient Calculation*/
	void CalDifferentialCoeff(double u, double v, double w, double *Coeff);

	/*Differential Coefficient Calculation (for linear for function)*/

	/*********************/
	/*Make Matrix for LS**/
	/*RPC Update Solution*/
	/*********************/
	
	/*RPC Update Function (Data Cubic)*/
	void MakeAandL(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
		                 double *u, double *v, double *w,
						 double *x, double *y);

	/*RPC Update Function (Only GCP)*/
	void MakeAandL_OnlyGCP1st(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
		                 double *u, double *v, double *w,
						 double *x, double *y);
	void MakeAandL_OnlyGCP2nd(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
		                 double *u, double *v, double *w,
						 double *x, double *y);
	void MakeAandL_OnlyGCP3rd(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
		                 double *u, double *v, double *w,
						 double *x, double *y);

	/*RPC Update Function (Sequential LS for First GCP, Data Cubic)*/
	void MakeAandL(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
		                 double u, double v, double w,
						 double x, double y);

	/*RPC Update Function (Parameters Observation)*/
	void MakeAandL_Param(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
		                 double *u, double *v, double *w,
						 double *x, double *y);

	/*RPC Update Function (Sequential LS for First GCP, Parameters Observation)*/
	void MakeAandL_Param(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
		                 double u, double v, double w,
						 double x, double y);

	/*RPC Update Function (Sequential LS for i번째 GCP)*/
	void MakeAandL_Sequential_LSi(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
		                 double u, double v, double w,
						 double x, double y);

	/*********************/
	/*Make Matrix for LS**/
	/*RPC Update Solution*/
	/*Linear Form        */
	/*********************/
	void MakeAandL_Linear(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
		                 double *u, double *v, double *w,
						 double *x, double *y);

private:
	/* Rational Function Coefficient*/
	/*RPC Parameters*/
	//F[G] = A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
	//	     A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
	//	     A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;
	
	//row = F1/G1
	//col = F2/G2
	
	/*Numerator of column term*/
	double dColNUM[NUM_RPC];
	double old_dColNUM[NUM_RPC];
	
	/*Denominator of column term*/
	double dColDEN[NUM_RPC];
	double old_dColDEN[NUM_RPC];
	
	/*Numerator of row term*/
	double dRowNUM[NUM_RPC];
	double old_dRowNUM[NUM_RPC];
	
	/*Denominator of row term*/
	double dRowDEN[NUM_RPC];
	double old_dRowDEN[NUM_RPC];
	
	//Col_Offset, Row_Offset, Lon_Offset, Lat_Offset, H_Offset
	double dCoordOffset[NUM_NORMALIZE_PARAM];
	
	//Col_Scale, Row_Scale, Lon_Scale, Lat_Scale, H_Scale
	double dCoordScale[NUM_NORMALIZE_PARAM];

	/*Normalized Data Cubic*/
	double DC_u[NUMDC];
	double DC_v[NUMDC];
	double DC_w[NUMDC];
	double DC_x[NUMDC];
	double DC_y[NUMDC];

	/*RFM improvement transformation parameter*/
	Transformation *trans;
	Conformal conf; //case 0
	Affine aff; //case 1(default)
	Bilinear bil; //case 2
	Projective_nonlinear pro; //case 3
	KnownPoint KP;
	unsigned int trans_option;
	bool additional_transform;

	/*GCP*/
	typedef struct RFMGCP
	{
		CICPManage mI;
		CGCPManage mG;
	}GCP;

	GCP mGCP;

public:
	/*Statistical value*/
	
	//Standard deviation
	double m_SD;
	//variance covariance of observation
	Matrix<double> VarCov_L;
	//variance covariance of unknown
	Matrix<double> VarCov_X;

};

/****************************/
/*Class for RFM Intersection*/
/****************************/
class CRFMIntersection
{
public:
	CRFMIntersection();
	virtual ~CRFMIntersection();
public:
	/*set number of RFM set(number of imagery)*/

	/*set RFM (RFM of image)*/
	void SetRFM(CRFM Lrfm, CRFM Rrfm);

	/*get 3D Coordinates from RFM Sets*/
	//return: false->MaxIteration over and MaxCorrection
	bool DoIntersection(double LC, double LR, double RC, double RR,
		                double *Lat, double *Lon, double *H,
						double MaxCorrection=1.0e-6, unsigned short MaxIteration=10);
	
private:
	/*Intial Approximation Value*/
	void InitApproximation(Point2D<double> La, Point2D<double> Ra);

	/*J and K matrix manipulation*/
	void MakeMatrixJK(void);

	/*Differential Coefficient Calculate*/
	void CalDifferentialCoeff(void);

	
private:
	/*RFM Instance*/
	CRFM LRFM;
	CRFM RRFM;

	/*Number of RFM Set*/
	unsigned short num_RFMs;

	/*Matrix for LS*/
	Matrix<double> J, K, X, V;
	Matrix<double> JT, N, Ninv, VTV;
	double pos_var;

	/*Difficiential Coefficient*/
	double LdC_dLon, LdC_dLat, LdC_dH;
	double LdR_dLon, LdR_dLat, LdR_dH;

	double RdC_dLon, RdC_dLat, RdC_dH;
	double RdR_dLon, RdR_dLat, RdR_dH;

	/*Stereo image coordinates*/
	Point2D<double> L, R;
	double GLat, GLon, GH;
	double u, v, w;
};


/*****************/
/*Global Function*/
/*****************/

/*Get Polynomial value*/
double GetPolyValue(double Lat, double Lon, double H, double *A);

/*Get Differential value*/
double GetDifferentialValue_Lat(double Lat, double Lon, double H, double *A);
double GetDifferentialValue_Lon(double Lat, double Lon, double H, double *A);
double GetDifferentialValue_H(double Lat, double Lon, double H, double *A);


#endif // !defined(AFX_RFM_H__C9B196E1_01A6_40D2_B8F5_8AB1563F81B7__INCLUDED_)
