#include "stdafx.h"
#include "SMPointCloud.h"
#include "./Image_Util/BasicImage.h"
#include "../bbarab/UtilityGrocery.h"
#include <fstream>
#include "../bbarab/Collinearity.h"

#define _2D_CIRCLE_ (double)-999

using namespace SMATICS_BBARAB;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMPointCloud::CSMPointCloud()
{
	ShiftX = 0.0;
	ShiftY = 0.0;
	ShiftZ = 0.0;
	bNormalCoord = false;
}

CSMPointCloud::~CSMPointCloud()
{
	m_Points.RemoveAll();
}

void CSMPointCloud::ReadTXTPoints(CString fname, bool bIntensity, bool bNormal)
{
	bNormalCoord = bNormal;
	
	MaxX = MaxY = MaxZ = -1.0e10;
	MinX = MinY = MinZ = +1.0e10;

	MinI = 99999; MaxI = -99999;

	fstream infile;
	infile.open(fname, ios::in);

	_Point_ p;
	
	infile>>ws;

	double X, Y, Z;
	int I;

	int count = 0;
	CString ID;

	char line[512];

	while(!infile.eof())
	{
		if(bIntensity == false)
		{
			infile>>X>>Y>>Z;
			infile.getline(line, 512);

			p.X=X; p.Y=Y; p.Z=Z;
			p.sort = count;
			
			//p.X = p.X + 100;
			//p.Y = p.Y + 100;
			//p.Z = p.Z + 100;
		}
		else
		{
			infile>>X>>Y>>Z>>I;
			infile.getline(line, 512);

			p.X=X; p.Y=Y; p.Z=Z; p.intensity = I;
			p.sort = count;
		}		
		
		infile>>ws;

		//2007.05.09 count ++; ID.Format("%d",count); p.ID = ID;
		count ++; p.nID = count;

		//if(0!=(count%5)) continue;
		
		m_Points.AddTail(p);

		if(MinX>p.X) MinX = p.X; if(MaxX<p.X) MaxX = p.X;
		if(MinY>p.Y) MinY = p.Y; if(MaxY<p.Y) MaxY = p.Y;
		if(MinZ>p.Z) MinZ = p.Z; if(MaxZ<p.Z) MaxZ = p.Z;
		if(MinI>p.intensity) MinI = p.intensity; 
		if(MaxI<p.intensity) MaxI = p.intensity;
	}

	infile.close();

	//
	//Coordinate regularization
	//
	if(bNormalCoord == true)
	{
		ShiftX = MinX;
		ShiftY = MinY;
		
		bNormalCoord = bNormal;
		DATA<_Point_> *pos = NULL;
		for(int i=0; i<(int)m_Points.GetNumItem(); i++)
		{
			pos = m_Points.GetNextPos(pos);
			pos->value.X = pos->value.X - ShiftX;
			pos->value.Y = pos->value.Y - ShiftY;
		}
	}
}

void CSMPointCloud::ReadPTSPoints(CString fname, bool bIntensity, bool bNormal)
{
	bNormalCoord = bNormal;
	
	MaxX = MaxY = MaxZ = -1.0e10;
	MinX = MinY = MinZ = +1.0e10;

	MinI = 99999; MaxI = -99999;

	fstream infile;
	infile.open(fname, ios::in);

	_Point_ p;
	
	infile>>ws;

	double X, Y, Z;
	int I;

	int count = 1;
	CString ID;

	char line[512];

	while(!infile.eof())
	{
		if(bIntensity == false)
		{
			infile>>X>>Y>>Z;
			p.X=X; p.Y=Y; p.Z=Z;
			p.sort = count;
			
			//p.X = p.X + 100;
			//p.Y = p.Y + 100;
			//p.Z = p.Z + 100;
		}
		else
		{
			infile>>X>>Y>>Z>>I;
			p.X=X; p.Y=Y; p.Z=Z; p.intensity = I;
			p.sort = count;
		}		
		
		infile.getline(line, 512);
		infile>>ws;

		//2007.05.09 count ++; ID.Format("%d",count); p.ID = ID;
		count ++; p.nID = count;

		m_Points.AddTail(p);

		if(MinX>p.X) MinX = p.X; if(MaxX<p.X) MaxX = p.X;
		if(MinY>p.Y) MinY = p.Y; if(MaxY<p.Y) MaxY = p.Y;
		if(MinZ>p.Z) MinZ = p.Z; if(MaxZ<p.Z) MaxZ = p.Z;
		if(MinI>p.intensity) MinI = p.intensity; 
		if(MaxI<p.intensity) MaxI = p.intensity;
	}

	infile.close();

	//
	//Coordinate regularization
	//
	if(bNormalCoord == true)
	{
		ShiftX = MinX;
		ShiftY = MinY;
		
		bNormalCoord = bNormal;
		DATA<_Point_> *pos = NULL;
		for(int i=0; i<(int)m_Points.GetNumItem(); i++)
		{
			pos = m_Points.GetNextPos(pos);
			pos->value.X = pos->value.X - ShiftX;
			pos->value.Y = pos->value.Y - ShiftY;
		}
	}
}

void CSMPointCloud::PTS2XYZPoints(CString ptsfilename, CString xyzfilename)
{
	double Given_X, Given_Y, Given_Z;//given X, Y, and Z coordinates
	double GPS_X, GPS_Y, GPS_Z; //GPS positions //unit: meter
	double Roll, Pitch, Yaw; //INS attitudes //unit: deg
	double beta;//laser beam angle //unit: deg
	double dist, time;//laser ranging and scan time //gunit: meter, sec
	int intensity;
	
	fstream PTSFile;//PTS file (Terrapoint point cloud raw file)
	fstream outfile_xyz;//xyz coordinates file
	fstream outfile_aux;//auxillary data
	fstream outfile_raw;//raw file

	int count = 0;
	
	double maxX=-1.0e10, minX=1.0e10; 
	double maxY=-1.0e10, minY=1.0e10;
	double maxZ=-1.0e10, minZ=1.0e10;
	
	///////////////////////////////////////////////////////////////////////////////
	//File open
	//
	PTSFile.open(ptsfilename, ios::in);
	
	outfile_xyz.open(xyzfilename, ios::out);
	
	CString fname_raw = xyzfilename;
	fname_raw.MakeLower(); fname_raw.Replace(".xyz",".raw");
	outfile_raw.open(fname_raw, ios::out);

	CString fname_aux = xyzfilename;
	fname_aux.MakeLower(); fname_aux.Replace(".xyz",".aux");
	outfile_aux.open(fname_aux, ios::out);
	
	///////////////////////
	//Save as raw file
	//
	///////////////////////////////////////////////////////////////////////////////
	outfile_raw<<"VER 1.1"<<endl;
	outfile_raw<<"#Pos_X(M)	Pos_Y(M)	Pos_Z(M)	Pitch(deg)	Roll(deg)	Heading(deg)	alpha(deg)	betta(deg)	range(M)	time(sec)	intensity"<<endl;
	outfile_raw<<"######################################################################################################################################"<<endl;
	///////////////////////////////////////////////////////////////////////////////
	
	outfile_raw.setf(ios::fixed, ios::floatfield);
	outfile_xyz.setf(ios::fixed, ios::floatfield);

	while(!PTSFile.eof())
	{
		count ++;
		
		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>intensity;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		
		PTSFile>>ws;

		outfile_raw<<GPS_X<<"\t";
		outfile_raw<<GPS_Y<<"\t";
		outfile_raw<<GPS_Z<<"\t";

		outfile_raw<<Pitch<<"\t";
		outfile_raw<<Roll<<"\t";
		outfile_raw<<Yaw<<"\t";

		outfile_raw<<0.0<<"\t";
		outfile_raw<<beta<<"\t";

		outfile_raw<<dist<<"\t";

		outfile_raw<<time<<"\t";

		outfile_raw<<intensity<<"\t";

		outfile_raw<<endl;
		outfile_raw.flush();

		outfile_xyz<<Given_X<<"\t";
		outfile_xyz<<Given_Y<<"\t";
		outfile_xyz<<Given_Z<<"\t";
		
		outfile_xyz<<intensity<<"\t";
		
		outfile_xyz<<time<<"\t";

		outfile_xyz<<endl;
		outfile_xyz.flush();

		if(Given_X>maxX) maxX = Given_X;
		if(Given_X<minX) minX = Given_X;

		if(Given_Y>maxY) maxY = Given_Y;
		if(Given_Y<minY) minY = Given_Y;

		if(Given_Z>maxZ) maxZ = Given_Z;
		if(Given_Z<minZ) minZ = Given_Z;
	}

	///////////////////////////////////////////////////////////////////////////////

	outfile_aux.setf(ios::fixed, ios::floatfield);

	outfile_aux<<"#Number of point, minX, maxX, minY, maxY, minZ, maxZ"<<endl;

	outfile_aux<<count<<"\t";

	outfile_aux<<minX<<"\t";
	outfile_aux<<maxX<<"\t";
	
	outfile_aux<<minY<<"\t";
	outfile_aux<<maxY<<"\t";
	
	outfile_aux<<minZ<<"\t";
	outfile_aux<<maxZ<<"\t";

	outfile_aux<<endl;

	/////////////////////////////////////////////////////////////////////////////////

	outfile_xyz.close();

	outfile_aux.close();

	outfile_raw.close();

	PTSFile.close();
}
void CSMPointCloud::ExtractPTS(CString ptsfilename, CString outptsfilename, double* x, double* y, int num_vertices)
{
	double Given_X, Given_Y, Given_Z;//given X, Y, and Z coordinates
	double GPS_X, GPS_Y, GPS_Z; //GPS positions //unit: meter
	double Roll, Pitch, Yaw; //INS attitudes //unit: deg
	double beta;//laser beam angle //unit: deg
	double dist, time;//laser ranging and scan time //gunit: meter, sec
	int intensity;
	
	fstream PTSFile;//PTS file (Terrapoint point cloud raw file)
	fstream outPTSFile;//output PTS file
	fstream outfile_aux;

	int count = 0;
	
	double maxX=-1.0e10, minX=1.0e10; 
	double maxY=-1.0e10, minY=1.0e10;
	double maxZ=-1.0e10, minZ=1.0e10;
	
	///////////////////////////////////////////////////////////////////////////////
	//File open
	//
	PTSFile.open(ptsfilename, ios::in);
	
	outPTSFile.open(outptsfilename, ios::out);
	outPTSFile.setf(ios::fixed, ios::floatfield);

	CString outauxfname = outptsfilename;
	outauxfname.MakeLower(); outauxfname.Replace(".pts",".aux");
	outfile_aux.open(outauxfname, ios::out);

	while(!PTSFile.eof())
	{
		//Old format
		/*
		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>intensity;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		*/

		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>intensity;
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		
		PTSFile>>ws;

		//if( 1 == InsidePolygon2D(x, y, num_vertices, Given_X, Given_Y))
		if( 1 == Inside2DPolygon(x, y, num_vertices, Given_X, Given_Y))
			count ++;
		else
			continue;

		outPTSFile<<Given_X<<"\t"<<Given_Y<<"\t"<<Given_Z<<"\t";
		outPTSFile<<time<<"\t";
		outPTSFile<<Pitch<<"\t"<<Roll<<"\t"<<Yaw<<"\t"; //roataion about X, rotation about Y, rotation about Z
		outPTSFile<<intensity<<"\t";
		outPTSFile<<GPS_X<<"\t"<<GPS_Y<<"\t"<<GPS_Z<<"\t";
		outPTSFile<<beta<<"\t"; //rotation about Y(in laser unit)
		outPTSFile<<dist<<endl;
		outPTSFile.flush();

		if(Given_X>maxX) maxX = Given_X;
		if(Given_X<minX) minX = Given_X;

		if(Given_Y>maxY) maxY = Given_Y;
		if(Given_Y<minY) minY = Given_Y;

		if(Given_Z>maxZ) maxZ = Given_Z;
		if(Given_Z<minZ) minZ = Given_Z;
	}

	///////////////////////////////////////////////////////////////////////////////

	outfile_aux.setf(ios::fixed, ios::floatfield);

	outfile_aux<<"#Selected polygon"<<endl;
	for(int i=0; i<num_vertices; i++)
	{
		outfile_aux<<i<<"\t";
		outfile_aux<<x[i]<<"\t";
		outfile_aux<<y[i]<<endl;
	}
	
	outfile_aux<<"#Point density"<<endl;
	outfile_aux<<count/Area_of_Polygon2D(x, y, num_vertices)<<" points/m^2"<<endl;

	outfile_aux<<"#Number of point, minX, maxX, minY, maxY, minZ, maxZ"<<endl;

	outfile_aux<<count<<"\t";

	outfile_aux<<minX<<"\t";
	outfile_aux<<maxX<<"\t";
	
	outfile_aux<<minY<<"\t";
	outfile_aux<<maxY<<"\t";
	
	outfile_aux<<minZ<<"\t";
	outfile_aux<<maxZ<<"\t";

	outfile_aux<<endl;

	/////////////////////////////////////////////////////////////////////////////////

	outfile_aux.close();

	PTSFile.close();

	outPTSFile.close();
}

void CSMPointCloud::ExtractPTS(CString ptsfilename, CString outptsfilename, double LT_X, double LT_Y, double RB_X, double RB_Y)
{
	double Given_X, Given_Y, Given_Z;//given X, Y, and Z coordinates
	double GPS_X, GPS_Y, GPS_Z; //GPS positions //unit: meter
	double Roll, Pitch, Yaw; //INS attitudes //unit: deg
	double beta;//laser beam angle //unit: deg
	double dist, time;//laser ranging and scan time //gunit: meter, sec
	int intensity;
	
	fstream PTSFile;//PTS file (Terrapoint point cloud raw file)
	fstream outPTSFile;//output PTS file
	fstream outfile_aux;

	int count = 0;
	
	double maxX=-1.0e10, minX=1.0e10; 
	double maxY=-1.0e10, minY=1.0e10;
	double maxZ=-1.0e10, minZ=1.0e10;
	
	///////////////////////////////////////////////////////////////////////////////
	//File open
	//
	PTSFile.open(ptsfilename, ios::in);
	
	outPTSFile.open(outptsfilename, ios::out);
	outPTSFile.setf(ios::fixed, ios::floatfield);

	CString outauxfname = outptsfilename;
	outauxfname.MakeLower(); outauxfname.Replace(".pts",".aux");
	outfile_aux.open(outauxfname, ios::out);

	while(!PTSFile.eof())
	{
		//Old format
		/*
		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>intensity;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		*/

		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>intensity;
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		
		PTSFile>>ws;

		if((Given_X < LT_X) || (Given_X > RB_X) || (Given_Y > LT_Y) || (Given_Y < RB_Y))
			continue;

		count ++;

		outPTSFile<<Given_X<<"\t"<<Given_Y<<"\t"<<Given_Z<<"\t";
		outPTSFile<<time<<"\t";
		outPTSFile<<Pitch<<"\t"<<Roll<<"\t"<<Yaw<<"\t"; //roataion about X, rotation about Y, rotation about Z
		outPTSFile<<intensity<<"\t";
		outPTSFile<<GPS_X<<"\t"<<GPS_Y<<"\t"<<GPS_Z<<"\t";
		outPTSFile<<beta<<"\t"; //rotation about Y(in laser unit)
		outPTSFile<<dist<<endl;
		outPTSFile.flush();

		if(Given_X>maxX) maxX = Given_X;
		if(Given_X<minX) minX = Given_X;

		if(Given_Y>maxY) maxY = Given_Y;
		if(Given_Y<minY) minY = Given_Y;

		if(Given_Z>maxZ) maxZ = Given_Z;
		if(Given_Z<minZ) minZ = Given_Z;
	}

	///////////////////////////////////////////////////////////////////////////////

	outfile_aux.setf(ios::fixed, ios::floatfield);

	outfile_aux<<"#Selected Region"<<endl;
	outfile_aux<<"#Left top X and Y"<<endl;
	outfile_aux<<LT_X<<"\t";
	outfile_aux<<LT_Y<<"\t";
	outfile_aux<<"#Right bottom X and Y"<<endl;
	outfile_aux<<RB_X<<"\t";
	outfile_aux<<RB_Y<<"\t";
	outfile_aux<<"#Point density"<<endl;
	outfile_aux<<count/(RB_X-LT_X)/(LT_Y-RB_Y)<<" points/m^2"<<endl;

	outfile_aux<<"#Number of point, minX, maxX, minY, maxY, minZ, maxZ"<<endl;

	outfile_aux<<count<<"\t";

	outfile_aux<<minX<<"\t";
	outfile_aux<<maxX<<"\t";
	
	outfile_aux<<minY<<"\t";
	outfile_aux<<maxY<<"\t";
	
	outfile_aux<<minZ<<"\t";
	outfile_aux<<maxZ<<"\t";

	outfile_aux<<endl;

	/////////////////////////////////////////////////////////////////////////////////

	outfile_aux.close();

	PTSFile.close();

	outPTSFile.close();
}

void CSMPointCloud::ExtractPTS(CString ptsfilename, CString outptsfilename, int num_sphere, double* radius, double* CX, double* CY, double* CZ)
{
	if(num_sphere < 1) return;

	double Given_X, Given_Y, Given_Z;//given X, Y, and Z coordinates
	double GPS_X, GPS_Y, GPS_Z; //GPS positions //unit: meter
	double Roll, Pitch, Yaw; //INS attitudes //unit: deg
	double beta;//laser beam angle //unit: deg
	double dist, time;//laser ranging and scan time //gunit: meter, sec
	int intensity;
	
	fstream PTSFile;//PTS file (Terrapoint point cloud raw file)
	fstream* outPTSFile = new fstream[num_sphere];//xyzit file (X, Y, Z, intensity, and time)
	fstream* outfile_aux = new fstream[num_sphere];//raw file

	double* maxX = new double[num_sphere];
	double* minX = new double[num_sphere];
	double* maxY = new double[num_sphere];
	double* minY = new double[num_sphere];
	double* maxZ = new double[num_sphere];
	double* minZ = new double[num_sphere];

	int* count = new int[num_sphere];
	
	///////////////////////////////////////////////////////////////////////////////
	//File open
	//
	PTSFile.open(ptsfilename, ios::in);
	
	if(num_sphere == 1)
	{
		int i = 0;
		//PTS file
		outPTSFile[i].open(outptsfilename, ios::out);
		outPTSFile[i].setf(ios::fixed, ios::floatfield);
		
		//AUX file
		CString noutauxfname = outptsfilename;
		noutauxfname.MakeLower(); noutauxfname.Replace(".pts",".aux");
		outfile_aux[i].open(noutauxfname, ios::out);
		
		maxX[i] = maxY[i] = maxZ[i] = -1.0e10;
		minX[i] = minY[i] = minZ[i] = 1.0e10;
		count[i] = 0;
	}
	else if(num_sphere > 1)
	{
		for(int i=0; i < num_sphere; i++)
		{
			//PTS file
			CString noutptsfilename = outptsfilename;
			noutptsfilename.MakeLower();
			CString temp;
			temp.Format("_%d_.pts", i);
			noutptsfilename.Replace(".pts", temp);
			outPTSFile[i].open(noutptsfilename, ios::out);
			
			outPTSFile[i].setf(ios::fixed, ios::floatfield);
			
			//AUX file
			CString noutauxfname = noutptsfilename;
			noutauxfname.MakeLower(); noutauxfname.Replace(".pts",".aux");
			outfile_aux[i].open(noutauxfname, ios::out);
			
			maxX[i] = maxY[i] = maxZ[i] = -1.0e10;
			minX[i] = minY[i] = minZ[i] = 1.0e10;
			count[i] = 0;
		}
	}
	else
		return;
	
	bool bIn = false;

	while(!PTSFile.eof())
	{
		bIn = false;

		//Old format
		/*
		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>intensity;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		*/

		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>intensity;
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		
		PTSFile>>ws;
		
		int nSphere=0;
		for(nSphere=0; nSphere < num_sphere; nSphere++)
		{
			double dX, dY, dZ;
			dX = Given_X - CX[nSphere];
			dY = Given_Y - CY[nSphere];
			dZ = Given_Z - CZ[nSphere];
			
			double dist;

			if(CZ[nSphere] < _2D_CIRCLE_)
			{
				dist = sqrt(dX*dX + dY*dY);
			}
			else//3D Sphere
			{
				//dist = sqrt(dX*dX + dY*dY + dZ*dZ);
				dist = sqrt(dX*dX + dY*dY);
			}
						
			if(dist < radius[nSphere])
			{
				bIn = true;
				break;
			}
		}

		if(bIn == false)
			continue;

		count[nSphere] ++;

		outPTSFile[nSphere]<<Given_X<<"\t"<<Given_Y<<"\t"<<Given_Z<<"\t";
		outPTSFile[nSphere]<<time<<"\t";
		outPTSFile[nSphere]<<Pitch<<"\t"<<Roll<<"\t"<<Yaw<<"\t"; //roataion about X, rotation about Y, rotation about Z
		outPTSFile[nSphere]<<intensity<<"\t";
		outPTSFile[nSphere]<<GPS_X<<"\t"<<GPS_Y<<"\t"<<GPS_Z<<"\t";
		outPTSFile[nSphere]<<beta<<"\t"; //rotation about Y(in laser unit)
		outPTSFile[nSphere]<<dist<<endl;
		outPTSFile[nSphere].flush();

		if(Given_X>maxX[nSphere]) maxX[nSphere] = Given_X;
		if(Given_X<minX[nSphere]) minX[nSphere] = Given_X;

		if(Given_Y>maxY[nSphere]) maxY[nSphere] = Given_Y;
		if(Given_Y<minY[nSphere]) minY[nSphere] = Given_Y;

		if(Given_Z>maxZ[nSphere]) maxZ[nSphere] = Given_Z;
		if(Given_Z<minZ[nSphere]) minZ[nSphere] = Given_Z;
	}

	for(int i=0; i < num_sphere; i++)
	{
		outPTSFile[i].close();

		outfile_aux[i].setf(ios::fixed, ios::floatfield);
		
		outfile_aux[i]<<"#Selected Region"<<endl;
		outfile_aux[i]<<"#Center of Sphere (CX, CY, CZ)"<<endl;
		outfile_aux[i]<<CX[i]<<"\t";
		outfile_aux[i]<<CY[i]<<"\t";
		outfile_aux[i]<<CZ[i]<<endl;
		outfile_aux[i]<<"#Radius of Sphere"<<endl;
		outfile_aux[i]<<radius[i]<<endl;
		outfile_aux[i]<<"#Point density"<<endl;
		outfile_aux[i]<<count[i]/(PHI*radius[i]*radius[i])<<" points/m^2"<<endl;
		
		outfile_aux[i]<<"#Number of point, minX, maxX, minY, maxY, minZ, maxZ"<<endl;
		
		outfile_aux[i]<<count[i]<<"\t";
		
		outfile_aux[i]<<minX[i]<<"\t";
		outfile_aux[i]<<maxX[i]<<"\t";
		
		outfile_aux[i]<<minY[i]<<"\t";
		outfile_aux[i]<<maxY[i]<<"\t";
		
		outfile_aux[i]<<minZ[i]<<"\t";
		outfile_aux[i]<<maxZ[i]<<endl;

		outfile_aux[i].close();
	}
	
	PTSFile.close();
}

bool CSMPointCloud::ReadPTS(CString ptsfilename)
{
	MaxX = MaxY = MaxZ = -1.0e10;
	MinX = MinY = MinZ = +1.0e10;
	
	MinI = 99999; MaxI = -99999;

	fstream PTSFile;//PTS file (Terrapoint point cloud raw file)
	
	///////////////////////////////////////////////////////////////////////////////
	//File open
	//
	PTSFile.open(ptsfilename, ios::in);
	
	m_Pts.RemoveAll();
	
	while(!PTSFile.eof())
	{
		//Old format
		/*
		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>intensity;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		*/

		RemoveCommentLine(PTSFile, '!');
		
		_PTS_Point_ P;
		
		PTSFile>>P.X>>P.Y>>P.Z;
		PTSFile>>P.time;
		PTSFile>>P.Pitch>>P.Roll>>P.Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>P.intensity;
		PTSFile>>P.GPS_X>>P.GPS_Y>>P.GPS_Z;
		PTSFile>>P.beta; //rotation about Y(in laser unit)
		PTSFile>>P.dist;
		
		PTSFile>>ws;

		m_Pts.AddTail(P);

		if(MinX>P.X) MinX = P.X; if(MaxX<P.X) MaxX = P.X;
		if(MinY>P.Y) MinY = P.Y; if(MaxY<P.Y) MaxY = P.Y;
		if(MinZ>P.Z) MinZ = P.Z; if(MaxZ<P.Z) MaxZ = P.Z;
		if(MinI>P.intensity) MinI = P.intensity; 
		if(MaxI<P.intensity) MaxI = P.intensity;
	}

	PTSFile.close();

	return true;
}

bool CSMPointCloud::MakeIntensityImageFromPTS(CString ptspath, CString imgpath, double minX, double maxX, double minY, double maxY, double minintensity, double maxintensity, double res, bool bStretch)
{
	fstream PTSFile;//PTS file 

	//
	//Create range image
	//
	///////////////////////////////////////////////////////////////////////////////
	
	//
	//INtensity image file
	//
	CBMPImage intensityimg;
	intensityimg.Create(int((maxX-minX)/res+0.5 + 1),int((maxY-minY)/res+0.5 + 1),8);
	CBMPPixelPtr intensitypixel(intensityimg);

	CString imginfopath = imgpath;
	imginfopath.MakeLower(); imginfopath.Replace(".bmp", ".bmpinfo");
	fstream imginfo;
	imginfo.open(imginfopath, ios::out);

	//File open
	//
	PTSFile.open(ptspath, ios::in);

	///////////////////////////////////////////////////////////////////////////////

	double Given_X, Given_Y, Given_Z;
	double GPS_X, GPS_Y, GPS_Z; //GPS positions //unit: meter
	double Roll, Pitch, Yaw; //INS attitudes //unit: deg
	double beta;//laser beam angle //unit: deg
	double dist, time;//laser ranging and scan time //gunit: meter, sec
	double intensity;

	double diffintensity = maxintensity - minintensity;//difference between maxmum range and minimum range
	
	double dintensity;
	if(diffintensity < 1)
		dintensity = 1.0;
	else
		dintensity = 255/diffintensity;

	int row, col;

	double minvalue=1000;
	double maxvalue=-1000;

	double max, min;
	//
	//Calculation of object coordinates
	//
	while(!PTSFile.eof())
	{
		//Old format
		/*
		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>intensity;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		*/

		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>intensity;
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		
		PTSFile>>ws;

		row = int((maxY-Given_Y)/res + 0.5);
		col = int((Given_X-minX)/res + 0.5);

		BYTE value;
		if(bStretch == true)
		{
			value = BYTE((intensity-minintensity)*dintensity + 0.5);
			if((double)value>maxvalue) {maxvalue = (double)value; max = intensity;}
			if((double)value<minvalue) {minvalue = (double)value; min = intensity;}
		}
		else
		{
			value = (BYTE)(intensity + 0.5);
		}

		intensitypixel[row][col] = value;
	}
	
	///////////////////////////////////////////////////////////////////////////////
	if(FALSE == intensityimg.SaveBMP(imgpath)) AfxMessageBox("Image Save Error");

	imginfo.setf(ios::fixed, ios::floatfield);
	imginfo.precision(3);

	imginfo<<"[MinX] "<<minX<<endl;
	imginfo<<"[MaxX] "<<maxX<<endl;
	imginfo<<"[MinY] "<<minY<<endl;
	imginfo<<"[MaxY] "<<maxY<<endl;
	imginfo<<"[Min value] "<<(int)minvalue<<"\t"<<min<<endl;
	imginfo<<"[Max value] "<<(int)maxvalue<<"\t"<<max<<endl;
	imginfo<<"[Resolution] "<<res<<endl;

	imginfo.close();

	PTSFile.close();

	return true;
}

bool CSMPointCloud::MakeRangeImageFromPTS(CString ptspath, CString imgpath, double minX, double maxX, double minY, double maxY, double minrange, double maxrange, double res)
{
	fstream PTSFile;//PTS file 

	//
	//Create range image
	//
	///////////////////////////////////////////////////////////////////////////////
	
	//
	//Ranging image file
	//
	CBMPImage rangeimg;
	rangeimg.Create(int((maxX-minX)/res+0.5 + 1),int((maxY-minY)/res+0.5 + 1),8);
	CBMPPixelPtr rangepixel(rangeimg);

	CString imginfopath = imgpath;
	imginfopath.MakeLower(); imginfopath.Replace(".bmp", ".bmpinfo");
	fstream imginfo;
	imginfo.open(imginfopath, ios::out);

	//File open
	//
	PTSFile.open(ptspath, ios::in);

	///////////////////////////////////////////////////////////////////////////////

	double Given_X, Given_Y, Given_Z;
	double GPS_X, GPS_Y, GPS_Z; //GPS positions //unit: meter
	double Roll, Pitch, Yaw; //INS attitudes //unit: deg
	double beta;//laser beam angle //unit: deg
	double dist, time;//laser ranging and scan time //gunit: meter, sec
	int intensity;

	double diffrange = maxrange - minrange;//difference between maxmum range and minimum range
	double drange;
	if(diffrange < 1)
		drange = 1.0;
	else
		drange = 255/diffrange;
	
	int row, col;

	double minvalue=1000;
	double maxvalue=-1000;

	double max, min;
	//
	//Calculation of object coordinates
	//
	while(!PTSFile.eof())
	{
		//Old format
		/*
		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>intensity;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		*/

		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>intensity;
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		
		PTSFile>>ws;

		row = int((maxY-Given_Y)/res + 0.5);
		col = int((Given_X-minX)/res + 0.5);

		BYTE value = BYTE((dist-minrange)*drange + 0.5);
		if((double)value>maxvalue) {maxvalue = (double)value; max = dist;}
		if((double)value<minvalue) {minvalue = (double)value; min = dist;}

		rangepixel[row][col] = value;
	}
	
	///////////////////////////////////////////////////////////////////////////////
	if(FALSE == rangeimg.SaveBMP(imgpath)) AfxMessageBox("Image Save Error");
	
	imginfo.setf(ios::fixed, ios::floatfield);
	imginfo.precision(3);
	
	imginfo<<"[MinX] "<<minX<<endl;
	imginfo<<"[MaxX] "<<maxX<<endl;
	imginfo<<"[MinY] "<<minY<<endl;
	imginfo<<"[MaxY] "<<maxY<<endl;
	imginfo<<"[Min value] "<<(int)minvalue<<"\t"<<min<<endl;
	imginfo<<"[Max value] "<<(int)maxvalue<<"\t"<<max<<endl;
	imginfo<<"[Resolution] "<<res<<endl;

	imginfo.close();

	PTSFile.close();

	return true;
}

bool CSMPointCloud::PTSStatistics(CString ptspath, CString outpath)
{
	//
	//File open
	//
	fstream PTSFile;//PTS file 
	PTSFile.open(ptspath, ios::in);

	//
	//Output file
	//
	fstream outfile;
	outfile.open(outpath, ios::out);
	outfile.setf(ios::fixed, ios::floatfield);

	///////////////////////////////////////////////////////////////////////////////

	double Given_X, Given_Y, Given_Z;
	double GPS_X, GPS_Y, GPS_Z; //GPS positions //unit: meter
	double Roll, Pitch, Yaw; //INS attitudes //unit: deg
	double beta;//laser beam angle //unit: deg
	double dist, time;//laser ranging and scan time //gunit: meter, sec
	int intensity;

	double Min_Given_X=1.0e10, Min_Given_Y=1.0e10, Min_Given_Z=1.0e10;
	double Min_GPS_X=1.0e10, Min_GPS_Y=1.0e10, Min_GPS_Z=1.0e10; 
	double Min_Roll=1.0e10, Min_Pitch=1.0e10, Min_Yaw=1.0e10;
	double Min_beta=1.0e10;
	double Min_dist=1.0e10, Min_time=1.0e10;
	int Min_intensity=9999;

	double Max_Given_X=-1.0e10, Max_Given_Y=-1.0e10, Max_Given_Z=-1.0e10;
	double Max_GPS_X=-1.0e10, Max_GPS_Y=-1.0e10, Max_GPS_Z=-1.0e10; 
	double Max_Roll=-1.0e10, Max_Pitch=-1.0e10, Max_Yaw=-1.0e10;
	double Max_beta=-1.0e10;
	double Max_dist=-1.0e10, Max_time=-1.0e10;
	int Max_intensity=-9999;

	while(!PTSFile.eof())
	{
		//Old format
		/*
		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>intensity;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //roataion about X, rotation about Y, rotation about Z
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		*/

		PTSFile>>Given_X>>Given_Y>>Given_Z;
		PTSFile>>time;
		PTSFile>>Pitch>>Roll>>Yaw; //rotaion about X, rotation about Y, rotation about Z
		PTSFile>>intensity;
		PTSFile>>GPS_X>>GPS_Y>>GPS_Z;
		PTSFile>>beta; //rotation about Y(in laser unit)
		PTSFile>>dist;
		
		PTSFile>>ws;

		if(Given_X<Min_Given_X) Min_Given_X = Given_X;
		if(Given_X>Max_Given_X) Max_Given_X = Given_X;

		if(Given_Y<Min_Given_Y) Min_Given_Y = Given_Y;
		if(Given_Y>Max_Given_Y) Max_Given_Y = Given_Y;

		if(Given_Z<Min_Given_Z) Min_Given_Z = Given_Z;
		if(Given_Z>Max_Given_Z) Max_Given_Z = Given_Z;

		if(time<Min_time) Min_time = time;
		if(time>Max_time) Max_time = time;
		
		if(Pitch<Min_Pitch) Min_Pitch = Pitch;
		if(Pitch>Max_Pitch) Max_Pitch = Pitch;

		if(Roll<Min_Roll) Min_Roll = Roll;
		if(Roll>Max_Roll) Max_Roll = Roll;

		if(Yaw<Min_Yaw) Min_Yaw = Yaw;
		if(Yaw>Max_Yaw) Max_Yaw = Yaw;

		if(intensity<Min_intensity) Min_intensity = intensity;
		if(intensity>Max_intensity) Max_intensity = intensity;

		if(GPS_X<Min_GPS_X) Min_GPS_X = GPS_X;
		if(GPS_X>Max_GPS_X) Max_GPS_X = GPS_X;

		if(GPS_Y<Min_GPS_Y) Min_GPS_Y = GPS_Y;
		if(GPS_Y>Max_GPS_Y) Max_GPS_Y = GPS_Y;

		if(GPS_Z<Min_GPS_Z) Min_GPS_Z = GPS_Z;
		if(GPS_Z>Max_GPS_Z) Max_GPS_Z = GPS_Z;

		if(beta<Min_beta) Min_beta = beta;
		if(beta>Max_beta) Max_beta = beta;

		if(dist<Min_dist) Min_dist = dist;
		if(dist>Max_dist) Max_dist = dist;
	}

	outfile<<"! Given_XYZ, Time, Pitch, Roll, Yaw, Intensity, GPS_XYZ, Scan_angle, Range"<<endl;
	outfile<<"! [Min values]"<<endl;
	outfile<<Min_Given_X<<"\t"<<Min_Given_Y<<"\t"<<Min_Given_Z;
	outfile<<"\t"<<Min_time;
	outfile<<"\t"<<Min_Pitch<<"\t"<<Min_Roll<<"\t"<<Min_Yaw;
	outfile<<"\t"<<Min_intensity;
	outfile<<"\t"<<Min_GPS_X<<"\t"<<Min_GPS_Y<<"\t"<<Min_GPS_Z;
	outfile<<"\t"<<Min_beta;
	outfile<<"\t"<<Min_dist<<endl;

	outfile<<"! [Max values]"<<endl;
	outfile<<Max_Given_X<<"\t"<<Max_Given_Y<<"\t"<<Max_Given_Z;
	outfile<<"\t"<<Max_time;
	outfile<<"\t"<<Max_Pitch<<"\t"<<Max_Roll<<"\t"<<Max_Yaw;
	outfile<<"\t"<<Max_intensity;
	outfile<<"\t"<<Max_GPS_X<<"\t"<<Max_GPS_Y<<"\t"<<Max_GPS_Z;
	outfile<<"\t"<<Max_beta;
	outfile<<"\t"<<Max_dist<<endl;
	
	PTSFile.close();
	outfile.close();

	return true;
}

bool CSMPointCloud::SeperateTXT(int numlines, CString ptspath, CString outpath)
{
	//
	//File open
	//
	fstream PTSFile;//PTS file 
	PTSFile.open(ptspath, ios::in);
	
	//
	//Output file
	//
	int index=0;
	CString noutpath = outpath;
	CString temp;
	temp.Format("_%d_.", index);
	noutpath.Replace(".", temp);
	
	fstream outfile;
	outfile.open(noutpath, ios::out);
	
	int count = 0;
	char line[512];
	while(!PTSFile.eof())
	{
		if(count >= numlines)
		{
			outfile.close();
			
			index++;
			noutpath = outpath;
			temp.Format("_%d_.", index);
			noutpath.Replace(".", temp);
			outfile.open(noutpath, ios::out);
			
			count = 0;
		}
		
		PTSFile.getline(line, 512);
		PTSFile>>ws;
		outfile<<line<<endl;
		count++;
	}
	
	outfile.close();
	
	PTSFile.close();
	
	return true;
}

bool CSMPointCloud::MergeTXT(CString ptspath1, CString ptspath2, CString outpath)
{
	//
	//File open
	//
	fstream PTSFile;//PTS file 
	PTSFile.open(ptspath1, ios::in);
	
	//
	//Output file
	//
	fstream outfile;
	outfile.open(outpath, ios::out);
	
	DWORD count1 = 0;
	char line[512];
	while(!PTSFile.eof())
	{	
		PTSFile.getline(line, 512);
		PTSFile>>ws;
		outfile<<line<<endl;
		count1++;
	}
	
	PTSFile.close();

	PTSFile.open(ptspath2, ios::in);

	DWORD count2 = 0;
	while(!PTSFile.eof())
	{	
		PTSFile.getline(line, 512);
		PTSFile>>ws;
		outfile<<line<<endl;
		count2++;
	}

	PTSFile.close();
	
	outfile.close();

	CString line_count;
	line_count.Format("%d lines from %s and\r\n %d lines from %s", count1, ptspath1, count2, ptspath2);
	AfxMessageBox(line_count);	
	
	return true;
}

bool CSMPointCloud::PTS2Points(CString ptspath, CString outpath)
{
	AfxMessageBox("Not implemented: CSMPointCoud::PTS2Points");
	return true;
}

bool CSMPointCloud::PlaneFitting(double threshold, CSMList<_PTS_Point_> &pointlist, double &a, double &b, double &c, double &omega, double &phi, double &pos_var, double &dist2origin, double *errordist, int maxiteration)
{
	//aX + bY + cZ = 1
	
	//Initialize orientation
	omega = 0;
	phi = 0;

	//Check number of points
	int num_points = pointlist.GetNumItem();
	if(num_points < 3) return false;

	int i;
	double minX, maxX;
	double minY, maxY;
	double minZ, maxZ;

	//
	////Min Max of coordinates
	//
	minX = maxX = pointlist.GetAt(0).X;
	minY = maxY = pointlist.GetAt(0).Y;
	minZ = maxZ = pointlist.GetAt(0).Z;

	for(i=1; i<num_points; i++)
	{
		_PTS_Point_ point = pointlist.GetAt(i);

		if(point.X<minX) minX = point.X;
		if(point.X>maxX) maxX = point.X;

		if(point.Y<minY) minY = point.Y;
		if(point.Y>maxY) maxY = point.Y;

		if(point.Z<minZ) minZ = point.Z;
		if(point.Z>maxZ) maxZ = point.Z;
	}

	///////////////////////////////////////////////////////////////////////

	//
	/////Shift and Scale
	//

	double shiftX = minX+100;
	double shiftY = minY+100;
	double shiftZ = minZ+100;
	double scale = (maxX - minX);
	if(scale < (maxY - minY)) scale = maxY - minY;
	if(scale < (maxZ - minZ)) scale = maxZ - minZ;
	scale = 1.0/scale;

	////////////////////////////////////////////////////////////////////////

	CSMMatrix<double> Amat(1, 3), Lmat(1, 1), Xmat;
	CSMMatrix<double> AT;
	CSMMatrix<double> We(num_points, num_points, 0.0);
	We.Identity(1.0);
	CSMMatrix<double> N(3,3,0), C(3,1,0); 
	CSMMatrix<double> etpe(1,1);

	//////////////////////////////////////////////////////////////////////////
	
	int num_availpoints;
	
	double pos_var0 = 1.0e99;
	
	double max_dist = 0.0, max_dist0 = 0.0;
	
	DATA<_PTS_Point_>* pos = NULL;
	
	//
	//calculate initial values
	//
	N.Resize(3,3,0);
	C.Resize(3,1,0);

	_PTS_Point_ pp = pointlist.GetAt(0);
		
	for(i=0; i<num_points; i++)
	{
		//a2(x-dx)Scale + b2(y-dy)Scale + c2(z-dz)Scale = 1
		
		pos = pointlist.GetNextPos(pos);			
		
		//
		////Normalize coordinates
		//
		CSMMatrix<double> P(3,1);
		
		P(0,0) = scale*(pos->value.X - shiftX);
		P(1,0) = scale*(pos->value.Y - shiftY);
		P(2,0) = scale*(pos->value.Z - shiftZ);
		
		/////////////////////////////////////////////////////////
		
		Amat(0,0) = P(0,0);
		Amat(0,1) = P(1,0);
		Amat(0,2) = P(2,0);
		
		Lmat(0,0) = 1.0;
		
		AT = Amat.Transpose();
		CSMMatrix<double> w(1,1);
		w(0,0) = We(i, i);
		N.InsertPlus(0, 0, AT%w%Amat);
		C.InsertPlus(0, 0, AT%w%Lmat);
	}
	
	Xmat = N.Inverse()%C;

	a = Xmat(0,0);
	b = Xmat(1,0);
	c = Xmat(2,0);

	int num_iter = 1;
	
	//
	//Orientation matrix
	//
	_Mmat_ RotUVW(0, 0, 0);
	
	while(num_iter < maxiteration)
	{
		num_availpoints = num_points;
		
		N.Resize(3,3,0);
		C.Resize(3,1,0);
		etpe.Resize(1,1,0);

		pos = NULL;
		
		for(i=0; i<num_points; i++)
		{
			//a2(x-dx)Scale + b2(y-dy)Scale + c2(z-dz)Scale = 1
			
			pos = pointlist.GetNextPos(pos);			
			
			//
			////Normalize coordinates
			//
			CSMMatrix<double> P(3,1);
			
			P(0,0) = scale*(pos->value.X - shiftX);
			P(1,0) = scale*(pos->value.Y - shiftY);
			P(2,0) = scale*(pos->value.Z - shiftZ);
			
			//
			//Rotate coordinates
			//
			CSMMatrix<double> UVW = RotUVW.Mmatrix%P;
			
			/////////////////////////////////////////////////////////
			
			Amat(0,0) = UVW(0,0);
			Amat(0,1) = UVW(1,0);
			Amat(0,2) = UVW(2,0);
			
			Lmat(0,0) = 1.0 - a*UVW(0,0) - b*UVW(1,0) - c*UVW(2,0);
			
			AT = Amat.Transpose();
			CSMMatrix<double> w(1,1);
			w(0,0) = We(i, i);
			N.InsertPlus(0, 0, AT%w%Amat);
			C.InsertPlus(0, 0, AT%w%Lmat);
			etpe.InsertPlus(0,0,Lmat.Transpose()%w%Lmat);
		}
		
		Xmat = N.Inverse()%C;
		
		max_dist = 0.0;

		///////////////////////////////////////////////////////////////////////////////////////////////

		if(num_points > 3)
		{
			pos_var = etpe(0,0)/(num_points - 3);
		}
		else
		{
			pos_var = 0.0;
		}
		
		//a2(x-dx)Scale + b2(y-dy)Scale + c2(z-dz)Scale = 1
		a += Xmat(0,0);
		b += Xmat(1,0);
		c += Xmat(2,0);

		//origin to plane
		double sqrtabc = sqrt(a*a + b*b + c*c);
		double Dist = 1.0/sqrtabc;

		double l = a*Dist;
		double m = b*Dist;
		double n = c*Dist;

		/////////////////////////////////////////////////////////////////////////////////

		//
		//Check normal distances
		//

		pos = NULL;

		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			
			//error distance (point to plane)
			CSMMatrix<double> P(3,1);

			P(0,0) = scale*(pos->value.X - shiftX);
			P(1,0) = scale*(pos->value.Y - shiftY);
			P(2,0) = scale*(pos->value.Z - shiftZ);

			CSMMatrix<double> UVW = RotUVW.Mmatrix%P;

			errordist[i] = fabs(l*UVW(0,0) + m*UVW(1,0) + n*UVW(2,0) - Dist);

			if(max_dist<errordist[i]) max_dist = errordist[i];

			//
			//Update weight matrix using normal distance errors
			//
			
			if(errordist[i] > threshold*scale)
			{
				We(i, i) = 0.0;
				num_availpoints --;
			}
			else
			{
				We(i, i) = 1/errordist[i];
			}
		}

		//
		//Check iteration stop
		//

		if(num_availpoints < 3) break;
		if(fabs(pos_var - pos_var0) < 1.0e-10) break;
		pos_var0 = pos_var;
		if(fabs(max_dist - max_dist0) < threshold*scale*0.000001) break;
		max_dist0 = max_dist;
		
		//////////////////////////////////////////////////////////////////////////

		//
		//Update orientation matrix
		//

		double Dist2 = Dist*Dist;		
		double dU = a*Dist2;
		double dV = b*Dist2;
		double dW = c*Dist2;

		//orientation of an extracted plane
		omega = Rad2Deg(-atan2(dV, dW));
		double dVW = sqrt(dW*dW + dV*dV);
		phi = Rad2Deg(atan2(dU, dVW));

		_Mmat_ Rot_new(Deg2Rad(omega), Deg2Rad(phi), 0);
		RotUVW.Mmatrix = Rot_new.Mmatrix%RotUVW.Mmatrix;

		CSMMatrix<double> abc(3,1);
		abc(0,0) = a;
		abc(1,0) = b;
		abc(2,0) = c;
		abc = Rot_new.Mmatrix%abc;
		a = abc(0,0);
		b = abc(1,0);
		c = abc(2,0);

		/////////////////////////////////////////////////////////////////////////
				
		num_iter ++;
	}
	
	//aX + bY + cZ  = 1

	CSMMatrix<double> UVW(3,1);
	
	UVW(0,0) = a;
	UVW(1,0) = b;
	UVW(2,0) = c;
	
	CSMMatrix<double> XYZ = RotUVW.Mmatrix.Transpose()%UVW;

	a = XYZ(0,0);
	b = XYZ(1,0);
	c = XYZ(2,0);

	double conversion = a*scale*shiftX + b*scale*shiftY + c*scale*shiftZ + 1;
	a = a*scale/conversion;
	b = b*scale/conversion;
	c = c*scale/conversion;
	
	double sqrtabc = sqrt(a*a + b*b + c*c);
	dist2origin = 1.0/sqrtabc;
	
	double l = a*dist2origin;
	double m = b*dist2origin;
	double n = c*dist2origin;
	
	double dist2origin2 = dist2origin*dist2origin;		
	double dX = a*dist2origin2;
	double dY = b*dist2origin2;
	double dZ = c*dist2origin2;

	//orientation of an extracted plane
	omega = Rad2Deg(-atan2(dY, dZ));
	double dYZ = sqrt(dZ*dZ + dY*dY);
	phi = Rad2Deg(atan2(dX, dYZ));

	pos = NULL;

	for(i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		
		//error distance (point to plane)
		CSMMatrix<double> P(3,1);
		P(0,0) = pos->value.X;
		P(1,0) = pos->value.Y;
		P(2,0) = pos->value.Z;
		
		errordist[i] = fabs(l*P(0,0) + m*P(1,0) + n*P(2,0) - dist2origin);
		
		if(max_dist<errordist[i]) max_dist = errordist[i];
	}
	
	return true;
}

bool CSMPointCloud::PlaneFitting(CSMList<_PTS_Point_> &pointlist, CSMList<_PTS_Point_> &resultpointlist, double a, double b, double c, double omega, double phi)
{
	int num_points = pointlist.GetNumItem();
	_Mmat_ M(omega, phi, 0.0);

	double sqrtabc = sqrt(a*a + b*b + c*c);
	double dist2origin = 1.0/sqrtabc;

	CSMMatrix<double> LMN(3,1), UVW_LMN;

	double dist2origin2 = dist2origin*dist2origin;		
	double dX = a*dist2origin2;
	double dY = b*dist2origin2;
	double dZ = c*dist2origin2;
	
	LMN(0,0) = dX;
	LMN(1,0) = dY;
	LMN(2,0) = dZ;

	UVW_LMN = M.Mmatrix%LMN;

	DATA<_PTS_Point_> *pos = NULL;

	for(int i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		_PTS_Point_ point = pos->value;
		CSMMatrix<double> P(3,1), Q;
		P(0,0) = point.X;
		P(1,0) = point.Y;
		P(2,0) = point.Z;
				
		Q = M.Mmatrix%P;
		Q(2,0) = UVW_LMN(2,0);

		Q = M.Mmatrix.Transpose()%Q;

		_PTS_Point_ q;
		q = point;
		q.X = Q(0,0);
		q.Y = Q(1,0);
		q.Z = Q(2,0);

		resultpointlist.AddTail(q);
	}
	return true;
}
