/*
 * Copyright (c) 1999-2001, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//////////////////////////
//Collinearity.h
//made by BbaraB
//revision date 2000-5-16
//revision date 2000-5-24
//revision date 2000-5-31
//revision date 2000-6-14
//revision date 2000-6-17
//revision date 2000-7-17
//revision date 2000-8-03
//revision date 2000-8-27
//resision date 2001-2-16
//////////////////////////
//revision data 2001-08-29
//////////////////////////
//ParallaxEquation 함수 추가
//ParallaxEQ와 동일한 코드임.
//option 인자의 순서를 제일 뒷 부분으로 옮기고
//기본값으로 3을 지정해 좋았음.
//하위버전과의 호환을 위해 ParallaxEQ를 삭제하지 않고
//남겨 두었음.
///////////////////////////////////////////////////////

#ifndef __COLLINEARITY_EQUATION_H__
#define __COLLINEARITY_EQUATION_H__

#include "SMMatrixClass.h"
#include "SMRotationMatrix.h"
#include "SMCoordinateClass.h"

#include <math.h>

namespace SMATICS_BBARAB
{

class CSMCollinearity
{
public:
	double Omega, Phi, Kappa;
	double Xa, Ya, Za;
	double Xl, Yl, Zl;
	double f;
	double xp, yp;
	double x, y;
	_Mmat_ M;
	
	double dF_dOmega;
	double dF_dPhi;
	double dF_dKappa;
	double dF_dXa;		
	double dF_dYa;		
	double dF_dZa;		
	double dF_dXl;		
	double dF_dYl;		
	double dF_dZl;		
	double dF_f;			
	double dF_xp;		
	double dF_yp;		
	double dF_x;		
	double dF_y;			
		
	double dG_dOmega;
	double dG_dPhi;	
	double dG_dKappa;
	double dG_dXa;		
	double dG_dYa;		
	double dG_dZa;		
	double dG_dXl;		
	double dG_dYl;		
	double dG_dZl;		
	double dG_f;			
	double dG_xp;		
	double dG_yp;		
	double dG_x;			
	double dG_y;			

public:
	CSMCollinearity() {}

	CSMCollinearity(Point3D<double> PC, Point3D<double> Attitude, Point3D<double> A, double c) {SetParam(PC, Attitude, A, c);}
	
	void SetParam(Point3D<double> PC, Point3D<double> Attitude, Point3D<double> A, double c)
	{
		Omega = Attitude.x;
		Phi = Attitude.y;
		Kappa = Attitude.z;
		Xl = PC.x;
		Yl = PC.y;
		Zl = PC.z;
		Xa = A.x;
		Ya = A.y;
		Za = A.z;
		f = c;
	}

	static Point2D<double> GetPhotoCoord(Point3D<double> PC, Point3D<double> Attitude, Point3D<double> A, double c, Point2D<double> PPA)
	{
		double D, Nx, Ny;
		double dX, dY, dZ;
		dX  = A.x - PC.x;
		dY = A.y - PC.y;
		dZ = A.z - PC.z;

		_Mmat_ M(Attitude.x, Attitude.y, Attitude.z);
		CSMMatrix<double> m = M.Mmatrix;
		
		Nx = m(0,0)*dX + m(0,1)*dY + m(0,2)*dZ;
		Ny = m(1,0)*dX + m(1,1)*dY + m(1,2)*dZ;
		D = m(2,0)*dX + m(2,1)*dY + m(2,2)*dZ;

		Point2D<double> P;
		P.x = -c*Nx/D + PPA.x;
		P.y = -c*Ny/D + PPA.y;

		return P;
	}

	void CalPartial()
	{
		M.ReMake(Omega, Phi, Kappa);
		CSMMatrix<double> m = M.Mmatrix;
		
		double D, Nx, Ny;
		double dX, dY, dZ;
		dX  = Xa - Xl;
		dY = Ya - Yl;
		dZ = Za - Zl;
		
		Nx = m(0,0)*dX + m(0,1)*dY + m(0,2)*dZ;
		Ny = m(1,0)*dX + m(1,1)*dY + m(1,2)*dZ;
		D = m(2,0)*dX + m(2,1)*dY + m(2,2)*dZ;
		
		dF_dOmega	= f/(D*D)*(Nx*(-m(2,2)*dY + m(2,1)*dZ) - D*(-m(0,2)*dY + m(0,1)*dZ));
		dF_dPhi			= f/(D*D)*(Nx*(cos(Phi)*dX + sin(Omega)*sin(Phi)*dY - cos(Omega)*sin(Phi)*dZ) - D*(-sin(Phi)*cos(Kappa)*dX + sin(Omega)*cos(Phi)*cos(Kappa)*dY - cos(Omega)*cos(Phi)*cos(Kappa)*dZ));
		dF_dKappa	= -f/(D)*(m(1,0)*dX + m(1,1)*dY + m(1,2)*dZ);
		dF_dXa			= f/(D*D)*(Nx*m(2,0) - D*m(0,0));
		dF_dYa			= f/(D*D)*(Nx*m(2,1) - D*m(0,1));
		dF_dZa			= f/(D*D)*(Nx*m(2,2) - D*m(0,2));
		dF_dXl			= -dF_dXa;
		dF_dYl			= -dF_dYa;
		dF_dZl			= -dF_dZa;
		dF_f				= -Nx/D;
		
		dG_dOmega	= f/(D*D)*(Ny*(-m(2,2)*dY+m(2,1)*dZ) - D*(-m(1,2)*dY+m(1,1)*dZ));
		dG_dPhi			= f/(D*D)*(Ny*(cos(Phi)*dX + sin(Omega)*sin(Phi)*dY - cos(Omega)*sin(Phi)*dZ) - D*(sin(Phi)*sin(Kappa)*dX - sin(Omega)*cos(Phi)*sin(Kappa)*dY + cos(Omega)*cos(Phi)*sin(Kappa)*dZ));
		dG_dKappa	= f/(D)*(m(0,0)*dX + m(0,1)*dY + m(0,2)*dZ);
		dG_dXa			= f/(D*D)*(Ny*m(2,0) - D*m(1,0));
		dG_dYa			= f/(D*D)*(Ny*m(2,1) - D*m(1,1));
		dG_dZa			= f/(D*D)*(Ny*m(2,2) - D*m(1,2));
		dG_dXl			= -dG_dXa;
		dG_dYl			= -dG_dYa;
		dG_dZl			= -dG_dZa;
		dG_f				= -Ny/D;
	}
};

inline Point2D<double> CollinearityEQ(double omega,double phi,double kappa,double Xl,double Yl, double Zl, double Xa,double Ya, double Za, double f)
{
	
	double q,r,s;
	Point2D<double> a;
	CRotationcoeff m(omega,phi,kappa);
	
	q = m.Mmatrix(2,0)*(Xa - Xl) + m.Mmatrix(2,1)*(Ya - Yl) + m.Mmatrix(2,2)*(Za - Zl);
	r = m.Mmatrix(0,0)*(Xa - Xl) + m.Mmatrix(0,1)*(Ya - Yl) + m.Mmatrix(0,2)*(Za - Zl);
	s = m.Mmatrix(1,0)*(Xa - Xl) + m.Mmatrix(1,1)*(Ya - Yl) + m.Mmatrix(1,2)*(Za - Zl);
	
	a.x = -f*(r/q);
	a.y = -f*(s/q);
	
	return a;
};

inline void CollinearityEQ_LineCCD(double omega,double phi,double kappa,double Xl,double Yl, double Zl, double Xa,double Ya, double Za, double &q, double &r, double &s)
{
	CRotationcoeff m(omega,phi,kappa);
	
	q = m.Mmatrix(2,0)*(Xa - Xl) + m.Mmatrix(2,1)*(Ya - Yl) + m.Mmatrix(2,2)*(Za - Zl);
	r = m.Mmatrix(0,0)*(Xa - Xl) + m.Mmatrix(0,1)*(Ya - Yl) + m.Mmatrix(0,2)*(Za - Zl);
	s = m.Mmatrix(1,0)*(Xa - Xl) + m.Mmatrix(1,1)*(Ya - Yl) + m.Mmatrix(1,2)*(Za - Zl);
	
};

inline Point3D<double> ParallaxEQ(int option, double H, double bm, double f, Point2D<double> aL, Point2D<double> aR)
{
	double temp = 0.0;
	double pa;//parallax
	Point3D<double> A;
	pa = aL.x - aR.x; 
	switch (option)
	{
	case 1:
		//왼쪽사진을 이용
		A.x = bm*aL.x/pa;
		A.y = bm*aL.y/pa;
		break;
	case 2:
		//오른쪽사진을 이용
		A.x = bm + bm*aR.x/pa; 
		A.y = bm*aR.y/pa;
		break;
	case 3:
		//양쪽 사진의 평균을 이용
		temp = bm*aL.x/pa;
		temp += bm + bm*aR.x/pa; 
		A.x = temp/2;
		temp = bm*aL.y/pa;
		temp += bm*aR.y/pa;
		A.y = temp/2;
		break;
	default:
		//양쪽 사진의 평균을 이용
		temp = bm*aL.x/pa;
		temp += bm + bm*aR.x/pa; 
		A.x = temp/2;
		temp = bm*aL.y/pa;
		temp += bm*aR.y/pa;
		A.y = temp/2;
		break;
	}
	
	A.z = H - bm*f/pa;
	
	return A;
};

inline NTPoint3D ParallaxEquation(double H, double bm, double f, double aLx, double aLy, double aRx, double aRy, int option = 3)
{
	double temp = 0.0;
	double pa;//parallax
	NTPoint3D A;
	pa = aLx - aRx; 
	switch (option)
	{
	case 1:
		//왼쪽사진을 이용
		A.x = bm*aLx/pa;
		A.y = bm*aLy/pa;
		break;
	case 2:
		//오른쪽사진을 이용
		A.x = bm + bm*aRx/pa; 
		A.y = bm*aRy/pa;
		break;
	case 3:
		//양쪽 사진의 평균을 이용
		temp = bm*aLx/pa;
		temp += bm + bm*aRx/pa; 
		A.x = temp/2;
		temp = bm*aLy/pa;
		temp += bm*aRy/pa;
		A.y = temp/2;
		break;
	default:
		//양쪽 사진의 평균을 이용
		temp = bm*aLx/pa;
		temp += bm + bm*aRx/pa; 
		A.x = temp/2;
		temp = bm*aLy/pa;
		temp += bm*aRy/pa;
		A.y = temp/2;
		break;
	}
	
	A.z = H - bm*f/pa;
	
	return A;
};

inline Point3D<double> ParallaxEquation(double H, double bm, double f, Point2D<double> aL, Point2D<double> aR, int option = 3)
{
	double temp = 0.0;
	double pa;//parallax
	Point3D<double> A;
	pa = aL.x - aR.x; 
	switch (option)
	{
	case 1:
		//왼쪽사진을 이용
		A.x = bm*aL.x/pa;
		A.y = bm*aL.y/pa;
		break;
	case 2:
		//오른쪽사진을 이용
		A.x = bm + bm*aR.x/pa; 
		A.y = bm*aR.y/pa;
		break;
	case 3:
		//양쪽 사진의 평균을 이용
		temp = bm*aL.x/pa;
		temp += bm + bm*aR.x/pa; 
		A.x = temp/2;
		temp = bm*aL.y/pa;
		temp += bm*aR.y/pa;
		A.y = temp/2;
		break;
	default:
		//양쪽 사진의 평균을 이용
		temp = bm*aL.x/pa;
		temp += bm + bm*aR.x/pa; 
		A.x = temp/2;
		temp = bm*aL.y/pa;
		temp += bm*aR.y/pa;
		A.y = temp/2;
		break;
	}
	
	A.z = H - bm*f/pa;
	
	return A;
};

/**Intersection
* G = T1+lamda*R1*(La-Lppa)
* G = T2+mu*R2*(Ra-Rppa)
* (T2-T1) = lamda*R1*(La-Lppa) - mu*R2*(Ra-Rppa)
*/
inline NTPoint3D Intersection(double Lo,double Lp,double Lk,/*Left photo's attitude*/
							   double Lx,double Ly,double Lz,/*Left photo's position*/
							   double Lxp, double Lyp, double Lc,/*Left camera's xp, yp and focal lenght*/
							   double lx, double ly,/*Tie point's coordinates of left photo*/
							   double Ro,double Rp,double Rk,/*Right photo's attitude*/
							   double Rx,double Ry,double Rz,/*Right photo's position*/
							   double Rxp, double Ryp, double Rc,/*Right camera's xp, yp and focal lenght*/
							   double rx, double ry,/*Tie point's coordinates of right photo*/
							   double &sigma/*return value: sigma*/)
{
	CRotationcoeff2 LM(Lo,Lp,Lk);/*Left photo's rotation matrix*/
	CRotationcoeff2 RM(Ro,Rp,Rk);/*Right photo's rotation matrix*/
	
	CSMMatrix<double> La(3,1,0.0);/*Matrix of left photo cooordinates*/
	La(0,0) = lx-Lxp;
	La(1,0) = ly-Lyp;
	La(2,0) = -Lc;
	CSMMatrix<double> Ra(3,1,0.0);;/*Matrix of left photo cooordinates*/
	Ra(0,0) = rx-Rxp;
	Ra(1,0) = ry-Ryp;
	Ra(2,0) = -Rc;
	CSMMatrix<double>LRO(3,1,0.0);;/*Right position - left position*/
	LRO(0,0) = Rx-Lx;
	LRO(1,0) = Ry-Ly;
	LRO(2,0) = Rz-Lz;

	CSMMatrix<double> Lmat, Rmat;
	Lmat = LM.Rmatrix%La;
	Rmat = RM.Rmatrix%Ra;
	
	CSMMatrix<double> A(3,2), X;

	A(0,0) = Lmat(0,0);
	A(0,1) = -Rmat(0,0);

	A(1,0) = Lmat(1,0);
	A(1,1) = -Rmat(1,0);

	A(2,0) = Lmat(2,0);
	A(2,1) = -Rmat(2,0);

	CSMMatrix<double> AT;
	AT = A.Transpose();

	CSMMatrix<double> N_inv;
	N_inv = (AT%A).Inverse();
	X = N_inv%AT%LRO;
	CSMMatrix<double> V;
	V = A%X - LRO;
	CSMMatrix<double> VTV;
	VTV = V.Transpose()%V;
	sigma = sqrt(VTV(0,0));

	CSMMatrix<double> LG(3,3), RG(3,3);
	/*To calculate ground coordinates using left photo data*/
	LG(0,0) = Lx + X(0,0)*Lmat(0,0);
	LG(1,0) = Ly + X(0,0)*Lmat(1,0);
	LG(2,0) = Lz + X(0,0)*Lmat(2,0);

	/*To calculate ground coordinates using right photo data*/
	RG(0,0) = Rx + X(1,0)*Rmat(0,0);
	RG(1,0) = Ry + X(1,0)*Rmat(1,0);
	RG(2,0) = Rz + X(1,0)*Rmat(2,0);

	NTPoint3D G;
	/*To calculate average values*/
	G.x = (LG(0,0)+RG(0,0))/2;
	G.y = (LG(1,0)+RG(1,0))/2;
	G.z = (LG(2,0)+RG(2,0))/2;

	return G;
};

/**Intersection</br>
* Init value of X and Y using Z</br>
* X-XL = (Z-ZL)(m11(x-xo)+ m12(y-yo)+ m11(-f))/(m31(x-xo)+ m32(y-xo)+ m31(-f))</br>
* Y-YL = (Z-ZL)(m21(x-xo)+ m22(y-yo)+ m21(-f))/(m31(x-xo)+ m32(y-xo)+ m31(-f)).
*/
inline NTPoint3D GetInitXYwithZ(double Lo,double Lp,double Lk,
							 double Lx,double Ly,double Lz,
							 double Lxp, double Lyp, double Lc,
							 double lx, double ly,
							 double Z=0.0)
{
	CRotationcoeff LM(Lo,Lp,Lk);
	
	CSMMatrix<double> Limg(3,1,0.0);
	Limg(0,0) = lx - Lxp;
	Limg(1,0) = ly - Lyp;
	Limg(2,0) = -Lc;

	NTPoint3D ans;
	ans.x = (Z-Lz)*(LM.Mmatrix(0,0)*Limg(0,0) + LM.Mmatrix(1,0)*Limg(1,0) + LM.Mmatrix(2,0)*Limg(2,0))/
		(LM.Mmatrix(0,2)*Limg(0,0) + LM.Mmatrix(1,2)*Limg(1,0) + LM.Mmatrix(2,2)*Limg(2,0)) + Lx;
	ans.y = (Z-Lz)*(LM.Mmatrix(0,1)*Limg(0,0) + LM.Mmatrix(1,1)*Limg(1,0) + LM.Mmatrix(2,1)*Limg(2,0))/
		(LM.Mmatrix(0,2)*Limg(0,0) + LM.Mmatrix(1,2)*Limg(1,0) + LM.Mmatrix(2,2)*Limg(2,0)) + Ly;
	ans.z = Z;
	
	return ans;
};

}//namespace
#endif