/*
 * Copyright (c) 1999-2007, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// SMPhoto.h: interface for the CSSMPhoto class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SMPHOTO_H__25E5A57C_0302_4BEA_925E_90BD348CAF7F__INCLUDED_)
#define AFX_SMPHOTO_H__25E5A57C_0302_4BEA_925E_90BD348CAF7F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SSMPoint.h"
#include "SSMCamera.h"
#include "SMList.h"
#include "SMMatrix.h"
#define NUM_PHOTO_PARAM 6

using namespace SMATICS_BBARAB;

class CSSMPhoto  
{
public:
	CSSMPhoto();
	virtual ~CSSMPhoto();
public:
	char m_ID[512];
	char m_CameraID[512];
	double m_X0, m_Y0, m_Z0;
	double m_Omega0, m_Phi0, m_Kappa0;
	CSMMatrix<double> m_Dispersion;
	CSSMPoint GetImagePoint(CSSMCamera camera, int index);
	void AddImagePoint(CSSMPoint IP);
	bool SetImagePoint(int index, CSSMPoint IP);
	int GetNumberofPoints();
	void RemoveAllImagePoints();
private:
	CSMList<CSSMPoint> m_ImagePoints;
};

#endif // !defined(AFX_SMPHOTO_H__25E5A57C_0302_4BEA_925E_90BD348CAF7F__INCLUDED_)
