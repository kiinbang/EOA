#include "stdafx.h"
#include "SMPseudoGridData.h"
#include "ProgressBar.h"
#include <iomanip>
#include "SMMatrix.h"

#include "./Image_Util/BasicImage.h"
#include "UtilityGrocery.h"

using namespace SMATICS_BBARAB;

#define empty -999.0
#define none_plane -888.0

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMPseudoGridData::CSMPseudoGridData()
{
	bNormalize = false;
	ShiftX = ShiftY = ShiftZ = 0.0;
	m_Grid = NULL;
	m_Grid_PTS = NULL;
	nPlaneIndex = 0;
}

CSMPseudoGridData::~CSMPseudoGridData()
{
	DeleteMemory();
}

void CSMPseudoGridData::DeleteMemory()
{
	if(m_Grid == NULL) return;

	int i, j;
	for(i=0; i<height; i++)
		for(j=0; j<width; j++)
			delete[] m_Grid[i][j];

	for(i=0; i<height; i++)
		delete[] m_Grid[i];

	delete[] m_Grid;

	if(m_Grid_PTS == NULL) return;

	for(i=0; i<height; i++)
		for(j=0; j<width; j++)
			delete[] m_Grid_PTS[i][j];

	for(i=0; i<height; i++)
		delete[] m_Grid_PTS[i];

	delete[] m_Grid_PTS;
}

void CSMPseudoGridData::ReadTXTPoints(CString fname, double offset_X, double offset_Y, double offset_Z, bool bNormal)
{
	bNormalize = bNormal;

	CString ext = fname;
	ext = fname.Right(4);
	ext.MakeLower();
	if(ext == ".pts")
	{
		m_PointsCloud.ReadPTSPoints(fname, false, bNormalize);
	}
	else
	{
		m_PointsCloud.ReadTXTPoints(fname, false, bNormalize);
	}
	
	if(bNormalize == true) 
	{
		ShiftX = m_PointsCloud.ShiftX;
		ShiftY = m_PointsCloud.ShiftY;
		ShiftZ = m_PointsCloud.ShiftZ;
	}

	//영역 계산
	oX = offset_X; 
	oY = offset_Y;
	oZ = offset_Z;
	
	sX = m_PointsCloud.MinX - m_PointsCloud.ShiftX; 
	sY = m_PointsCloud.MaxY - m_PointsCloud.ShiftY;
	sZ = m_PointsCloud.MinZ - m_PointsCloud.ShiftZ;
	
	//그리드 크기 계산
	width = (int)((m_PointsCloud.MaxX - m_PointsCloud.MinX)/oX + 1.5);
	height = (int)((m_PointsCloud.MaxY - m_PointsCloud.MinY)/oY + 1.5);
	depth = (int)((m_PointsCloud.MaxZ - m_PointsCloud.MinZ)/oZ + 1.5);

	//메모리 할당.
	m_Grid = new _GridElement_**[height];
	
	for(int i=0; i<height; i++)
		m_Grid[i] = new _GridElement_*[width];

	for(int i=0; i<height; i++)
		for(int j=0; j<width; j++)
			m_Grid[i][j] = new _GridElement_[depth];


	int num = m_PointsCloud.m_Points.GetNumItem();

	for(int i=0; i<num; i++)
	{
		_Point_ p = m_PointsCloud.m_Points.GetAt(i);
		int ix, iy, iz;
		
		bool bSuccess = PutPoint(p, ix, iy, iz);
	}

	m_PointsCloud.m_Points.RemoveAll();
}

void CSMPseudoGridData::ReadTXTPoints_Tree(CString fname, double &cx, double &cy, double &cz)
{
	bNormalize = false;
	
	CSMList<double> Xlist, Ylist, Zlist;
	double MinX = 1.0e99;
	double MaxX = -1.0e99;
	double MinY = 1.0e99;
	double MaxY = -1.0e99;
	double MinZ = 1.0e99;
	double MaxZ = -1.0e99;
	
	fstream infile;
	infile.open(fname, ios::in);
	
	infile>>ws;
	
	double X, Y, Z;
	char line[512];

	cx = cy = cz = 0.0;
	
	while(!infile.eof())
	{
		infile>>X>>Y>>Z;
		infile.getline(line, 512);
		infile>>ws;

		Xlist.AddTail(X);
		Ylist.AddTail(Y);
		Zlist.AddTail(Z);

		cx += X;
		cy += Y;
		cz += Z;
		
		if(MinX>X) MinX = X; if(MaxX<X) MaxX = X;
		if(MinY>Y) MinY = Y; if(MaxY<Y) MaxY = Y;
		if(MinZ>Z) MinZ = Z; if(MaxZ<Z) MaxZ = Z;
	}
	
	infile.close();

	cx /= Xlist.GetNumItem();
	cy /= Ylist.GetNumItem();
	cz /= Zlist.GetNumItem();
	
	int num_p = Xlist.GetNumItem();

	tree.InitialSetup(num_p, 3, 4);
	double x[3], param[4]={empty,empty,empty,1.0e99};

	for(INT i=0; i<num_p; i++)
	{
		x[0] = Xlist.GetAt(i) - cx;
		x[1] = Ylist.GetAt(i) - cy;
		x[2] = Zlist.GetAt(i) - cz;

		tree.add(x, param, i);
	}
}

void CSMPseudoGridData::ReadCLSPoints_Tree(CString fname, double &cx, double &cy, double &cz)
{
	bNormalize = false;
	
	CSMList<double> Xlist, Ylist, Zlist;
	CSMList<double> Rlist, Glist, Blist;
	double MinX = 1.0e99;
	double MaxX = -1.0e99;
	double MinY = 1.0e99;
	double MaxY = -1.0e99;
	double MinZ = 1.0e99;
	double MaxZ = -1.0e99;
	
	fstream infile;
	infile.open(fname, ios::in);
	
	infile>>ws;
	
	double X, Y, Z;
	double R, G, B;
	char line[512];
	
	cx = cy = cz = 0.0;
	
	while(!infile.eof())
	{
		infile>>X>>Y>>Z;
		infile>>R>>G>>B;
		infile.getline(line, 512);
		infile>>ws;
		
		Xlist.AddTail(X);
		Ylist.AddTail(Y);
		Zlist.AddTail(Z);

		Rlist.AddTail(R);
		Glist.AddTail(G);
		Blist.AddTail(B);
		
		cx += X;
		cy += Y;
		cz += Z;
		
		if(MinX>X) MinX = X; if(MaxX<X) MaxX = X;
		if(MinY>Y) MinY = Y; if(MaxY<Y) MaxY = Y;
		if(MinZ>Z) MinZ = Z; if(MaxZ<Z) MaxZ = Z;
	}
	
	infile.close();
	
	cx /= Xlist.GetNumItem();
	cy /= Ylist.GetNumItem();
	cz /= Zlist.GetNumItem();
	
	int num_p = Xlist.GetNumItem();
	
	tree.InitialSetup(num_p, 3, 4);
	double x[3], param[4]={0,0,0,0};
	
	for(INT i=0; i<num_p; i++)
	{
		x[0] = Xlist.GetAt(i) - cx;
		x[1] = Ylist.GetAt(i) - cy;
		x[2] = Zlist.GetAt(i) - cz;

		param[0] = Rlist.GetAt(i);
		param[1] = Glist.GetAt(i);
		param[2] = Blist.GetAt(i);
		
		tree.add(x, param, i);
	}
}

void CSMPseudoGridData::MakePseudoGridFromPTS(double offset_X, double offset_Y, double offset_Z, bool bNormal)
{
	bNormalize = bNormal;

	if(bNormalize == true) 
	{
		ShiftX = m_PointsCloud.ShiftX;
		ShiftY = m_PointsCloud.ShiftY;
		ShiftZ = m_PointsCloud.ShiftZ;
	}

	//영역 계산
	oX = offset_X; 
	oY = offset_Y;
	oZ = offset_Z;
	
	sX = m_PointsCloud.MinX - m_PointsCloud.ShiftX; 
	sY = m_PointsCloud.MaxY - m_PointsCloud.ShiftY;
	sZ = m_PointsCloud.MinZ - m_PointsCloud.ShiftZ;
	
	//그리드 크기 계산
	width = (int)((m_PointsCloud.MaxX - m_PointsCloud.MinX)/oX + 1.5);
	height = (int)((m_PointsCloud.MaxY - m_PointsCloud.MinY)/oY + 1.5);
	depth = (int)((m_PointsCloud.MaxZ - m_PointsCloud.MinZ)/oZ + 1.5);

	//메모리 할당.
	m_Grid_PTS = new _GridElement_PTS_**[height];
	
	for(int i=0; i<height; i++)
		m_Grid_PTS[i] = new _GridElement_PTS_*[width];

	for(int i=0; i<height; i++)
		for(int j=0; j<width; j++)
			m_Grid_PTS[i][j] = new _GridElement_PTS_[depth];

	int num = m_PointsCloud.m_Pts.GetNumItem();

	for(int i=0; i<num; i++)
	{
		_PTS_Point_ p = m_PointsCloud.m_Pts.GetAt(i);
		int ix, iy, iz;
		
		bool bSuccess = PutPoint(p, ix, iy, iz);
	}
}

CSMAuxForXYZ CSMPseudoGridData::ReadAux(CString xyzfname)
{
	CSMAuxForXYZ aux;

	fstream auxfile;
	CString auxfname = xyzfname;
	auxfname.MakeLower(); auxfname.Replace(".xyz",".aux");
	auxfile.open(auxfname, ios::in);

	RemoveCommentLine(auxfile,"#!", 2);

	auxfile>>aux.count;

	auxfile>>aux.minX;
	auxfile>>aux.maxX;

	auxfile>>aux.minY;
	auxfile>>aux.maxY;

	auxfile>>aux.minZ;
	auxfile>>aux.maxZ;

	auxfile.close();

	return aux;
}

void CSMPseudoGridData::XYZ2PseudoGrid(CString xyzfname, CString gridfname, double resX, double resY, double resZ, int w_c, int w_r, int w_d, int minNum)
{
	int i, j, k;

	CSMAuxForXYZ aux = ReadAux(xyzfname);
	
	int width = int((aux.maxX - aux.minX)/resX + 1.5);
	int height = int((aux.maxY - aux.minY)/resY + 1.5);
	int depth = int((aux.maxZ - aux.minZ)/resZ + 1.5);

	double initS_X = aux.minX;
	double initS_Y = aux.maxY;
	double initS_Z = aux.minZ;

	int numCells = width*height*depth;
	
	//BYTE *grid; grid = new BYTE[numCells];

	FILEMEM grid(1);
	grid.Open("test.tmp");
	long mem_pos = 0L;
	BYTE val = 0;
	for(i=0; i<numCells; i++)
	{
		grid.Set(mem_pos,&val);
		mem_pos++;
	}

	fstream xyzfile;
	xyzfile.open(xyzfname, ios::in);

	double X, Y, Z;
	int intensity;
	double time;

	streamoff pos;
	streamoff length;
	xyzfile.seekg(0,ios::end);

	length = xyzfile.tellg();
	
	xyzfile.seekg(0,ios::beg);

	pos = xyzfile.tellg();

	while(pos<length)
	{
		xyzfile>>X>>Y>>Z>>intensity>>time;
		xyzfile>>ws;

		pos = xyzfile.tellg();

		int index_c, index_r, index_d;

		index_c = int((X - initS_X)/resX+0.5);
		index_r = int((initS_Y - Y)/resY+0.5);
		index_d = int((Z - initS_Z)/resZ+0.5);

		if((index_c<0)||(index_c>=width)) continue;
		if((index_r<0)||(index_r>=height)) continue;
		if((index_d<0)||(index_d>=depth)) continue;

		int index = width*height*index_d + width*index_r + index_c;

		//grid[index] = 1;
		
		BYTE val = 1;
		grid.Set((long)index, &val);

		pos = xyzfile.tellg();
	}

	xyzfile.close();

	CheckOutlierinGrid(&grid, width, height, depth, w_c, w_r, w_d, minNum);

	int minW, maxW;
	int minH, maxH;
	int minD, maxD;

	CheckEmptyLayer(&grid, width, height, depth, minW, maxW, minH, maxH, minD, maxD);

	double S_X, S_Y, S_Z;
	S_X = initS_X + minW*resX;
	S_Y = initS_Y + minH*resY;
	S_Z = initS_Z + minD*resZ;

	int newwidth = maxW - minW + 1;
	int newheight = maxH - minH + 1;
	int newdepth = maxD - minD +1;

	//
	//Write index file
	//
	CString idxfname = gridfname;
	idxfname.MakeLower(); idxfname.Replace(".grd",".idx");

	fstream idxfile; idxfile.open(idxfname, ios::out);
	idxfile.setf(ios::fixed, ios::floatfield);

	idxfile<<"#width	height	dpeth	res_X	res_Y	res_Z	left_X	top_Y	low_Z"<<endl;
	idxfile<<"##############################################################################"<<endl;

	idxfile<<newwidth<<"\t";
	idxfile<<newheight<<"\t";
	idxfile<<newdepth<<"\t";

	idxfile<<resX<<"\t";
	idxfile<<resY<<"\t";
	idxfile<<resZ<<"\t";

	idxfile<<S_X<<"\t";
	idxfile<<S_Y<<"\t";
	idxfile<<S_Z<<"\t";

	idxfile<<endl;
	idxfile.flush();

	xyzfile.open(xyzfname, ios::in);

	pos = 0L;

	while(pos<length)
	{
		xyzfile>>X>>Y>>Z>>intensity>>time;
		xyzfile>>ws;

		int index_c, index_r, index_d;

		index_c = int((X - initS_X)/resX+0.5);
		index_r = int((initS_Y - Y)/resY+0.5);
		index_d = int((Z - initS_Z)/resZ+0.5);

		if((index_c<minW)||(index_c>maxW)) continue;
		if((index_r<minH)||(index_r>maxH)) continue;
		if((index_d<minD)||(index_d>maxD)) continue;

		int index = width*height*index_d + width*index_r + index_c;

		//BYTE val = grid[index];
		BYTE val;
		grid.Get(index, &val);

		if(val == (BYTE)1)
		{
			
			idxfile<<index_c<<"\t";
			idxfile<<index_r<<"\t";
			idxfile<<index_d<<"\t";
			
			idxfile<<time<<"\t";
			
			idxfile<<endl;
		}

		pos = xyzfile.tellg();
	}
	
	idxfile.close();

	xyzfile.close();

	//
	//Write grid file
	//

	fstream gridfile;
	gridfile.open(gridfname, ios::out|ios::binary);

	for(k=0; k<newdepth; k++)
	{
		for(j=0; j<newheight; j++)
		{
			for(i=0; i<newwidth; i++)
			{
				int index = width*height*(k+minD) + width*(j+minH) + (i+minW);
				
				//BYTE val = grid[index];

				BYTE val;
				grid.Get(index, &val);
				
				gridfile.write((char*)&val,1);
			}
		}
	}
	gridfile.close();
}

void CSMPseudoGridData::CheckOutlierinGrid(FILEMEM *grid, int width, int height, int depth, int w_c, int w_r, int w_d, int minNum)
{
	int i, j, k;

	for(k=0; k<depth; k++)
	{
		for(j=0; j<height; j++)
		{
			for(i=0; i<width; i++)
			{
				int index = width*height*k + width*j + i;
				
				BYTE val;
				grid->Get(index, &val);

				if(val != 0)
				{
					int count = CountNoneZeroCells(grid, i, j, k, width, height, depth, w_c, w_r, w_d);
					if(count < minNum)
					{
						//grid[index] = 0;
						BYTE temp = 0;
						grid->Set(index, &temp);
					}
				}
			}
		}
	}
}

int CSMPseudoGridData::CountNoneZeroCells(FILEMEM *grid, int cur_c, int cur_r, int cur_d, int width, int height, int depth, int w_c, int w_r, int w_d)
{
	int i, j, k;

	int count = 0;

	for(k=-int(w_d/2); k<=int(w_d/2); k++)
	{
		for(j=-int(w_r/2); j<=int(w_r/2); j++)
		{
			for(i=-int(w_c/2); i<=int(w_c/2); i++)
			{
				int d = cur_d + k;
				int r = cur_r + j;
				int c = cur_c + i;

				if((d<0)||(d>=depth)) continue;
				if((r<0)||(r>=height)) continue;
				if((c<0)||(c>=width)) continue;

				int index = width*height*d + width*r + c;
				
				BYTE val;
				grid->Get(index, &val);

				//if(grid[index] != 0) count ++;

				if(val != 0)
					count ++;
			}
		}
	}

	return count;
}

void CSMPseudoGridData::CheckEmptyLayer(FILEMEM *grid, int width, int height, int depth, int &minW, int &maxW, int &minH, int &maxH, int &minD, int &maxD)
{
	int i;

	for(i=0; i<depth; i++)
	{
		int count = CountNoneZeroCells(grid, int(width/2 + 0.5), int(height/2 + 0.5), i, width, height, depth, width+2, height+2, 1);
		if(count>0) break;
	}

	minD = i;

	for(i=depth-1; i<=0; i++)
	{
		int count = CountNoneZeroCells(grid, int(width/2 + 0.5), int(height/2 + 0.5), i, width, height, depth, width+2, height+2, 1);
		if(count>0) break;
	}

	maxD = i;

	for(i=0; i<width; i++)
	{
		int count = CountNoneZeroCells(grid, i, int(height/2 + 0.5), int(depth/2 + 0.5), width, height, depth, 1, height+2, depth+2);
		if(count>0) break;
	}

	minW = i;

	for(i=width-1; i<=0; i++)
	{
		int count = CountNoneZeroCells(grid, i, int(height/2 + 0.5), int(depth/2 + 0.5), width, height, depth, 1, height+2, depth+2);
		if(count>0) break;
	}

	maxW = i;

	for(i=0; i<height; i++)
	{
		int count = CountNoneZeroCells(grid, int(width/2 + 0.5), i, int(depth/2 + 0.5), width, height, depth, width+2, 1, depth+2);
		if(count>0) break;
	}

	minH = i;

	for(i=height-1; i<=0; i++)
	{
		int count = CountNoneZeroCells(grid, int(width/2 + 0.5), i, int(depth/2 + 0.5), width, height, depth, width+2, 1, depth+2);
		if(count>0) break;
	}

	maxH = i;
}

void CSMPseudoGridData::VerticalPointsFreezing()
{
	int i, j, k;

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			bool bWall = false;
			
			for(k=depth-1; k>=0; k--)
			{
				if(bWall == true)
				{
					m_Grid[i][j][k].type = _OUT_;
				}
				else
				{
					if(m_Grid[i][j][k].point.GetNumItem()<1) continue;
					else
						bWall = true;
				}
			}
		}
	}
}

void CSMPseudoGridData::HorizontalPointsFreezing()
{
	int i, j, k;

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			int count = 0;
			
			int oldk;
			
			for(k=depth-1; k>=0; k--)
			{
				
				if(m_Grid[i][j][k].point.GetNumItem()<1) continue;
				else
					count ++;
				
				if(count == 1)
				{
					oldk = k;
				}
				else if(count >= 2)
				{
					break;
				}
			}

			if((count == 1)&&(m_Grid[i][j][oldk].type == _IN_))
			{
				m_Grid[i][j][oldk].type = _OUT_;
			}
		}
	}
	
}

void CSMPseudoGridData::HorizontalPointsFreezing_PTS(CString outfilename)
{
	fstream outpts;
	outpts.open(outfilename, ios::out);
	outpts.setf(ios::fixed, ios::floatfield);

	int i, j, k;

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			int count = 0;
			
			int oldk;
			
			for(k=depth-1; k>=0; k--)
			{
				
				if(m_Grid_PTS[i][j][k].point.GetNumItem()<1) continue;
				else
					count ++;
				
				if(count == 1)
				{
					oldk = k;
				}
				else if(count >= 2)
				{
					break;
				}
			}

			if(count == 1)
			{
				m_Grid_PTS[i][j][oldk].type = _OUT_;
			}
		}
	}

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			for(k=0; k<depth; k++)
			{
				int numP = m_Grid_PTS[i][j][k].point.GetNumItem();
				if((numP<1)||m_Grid_PTS[i][j][k].type == _OUT_)
					continue;
				else
				{
					DATA<_PTS_Point_>* pos = NULL;
					for(int c=0; c<numP; c++)
					{
						pos = m_Grid_PTS[i][j][k].point.GetNextPos(pos);
						outpts<<pos->value.X<<"\t"<<pos->value.Y<<"\t"<<pos->value.Z<<'\t';
						outpts<<pos->value.time<<'\t';
						outpts<<pos->value.Pitch<<"\t"<<pos->value.Roll<<"\t"<<pos->value.Yaw<<'\t';
						outpts<<pos->value.intensity<<"\t";
						outpts<<pos->value.GPS_X<<"\t"<<pos->value.GPS_Y<<"\t"<<pos->value.GPS_Z<<'\t';
						outpts<<pos->value.beta<<"\t"<<pos->value.dist<<endl;
						outpts.flush();
					}
				}
			}
		}
	}

	outpts.close();
}

bool CSMPseudoGridData::CheckList(CSMList<int>& list, int ID)
{
	int num = list.GetNumItem();

	for(int i=0; i<num; i++)
	{
		if(ID == list.GetAt(i))
		{
			return true;
		}
	}
	
	return false;
}

void CSMPseudoGridData::MarkGround(int ID)
{
	for(int k=0; k<depth; k++)//Z direction
	{
		for(int i=0; i<height; i++)//Y direction
		{
			for(int j=0; j<width; j++)//X direction
			{
				if(m_Grid[i][j][k].PlaneID == ID)
					m_Grid[i][j][k].type = _GROUND_;
			}
		}
	}
}

void CSMPseudoGridData::PointsMelting()
{
	int i, j, k;

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			for(k=depth-1; k>=0; k--)
			{
				if(m_Grid[i][j][k].type == _OUT_)
				{
					m_Grid[i][j][k].type = _IN_;
				}
			}
		}
	}
	
}

void CSMPseudoGridData::SaveAsGrid(CString infname, CString outfname, bool bAutoRes, double offset_X, double offset_Y, bool bcolor, bool bInterpolation, bool bIntensity, double Intensity_res)
{
	double MinX_first=1.0e20, MaxX_first=-1.0e20;
	double MinX_Y_first;
	double MinX_Z_first;
	double MaxX_Y_first;
	double MaxX_Z_first;

	double MinY_first=1.0e20, MaxY_first=-1.0e20;
	double MinY_X_first;
	double MinY_Z_first;
	double MaxY_X_first;
	double MaxY_Z_first;

	double MinZ_first=1.0e20, MaxZ_first=-1.0e20;
	double MinZ_X_first;
	double MinZ_Y_first;
	double MaxZ_X_first;
	double MaxZ_Y_first;

	double min_I=999., max_I=-999.;

	double average = 0.0;

	CString imgname = outfname;
	imgname.MakeLower();
	imgname.Replace(".dem",".bmp");

	CString hdrname = outfname;
	hdrname.MakeLower();
	hdrname.Replace(".dem",".hdr");

	CString infoname = outfname;
	infoname.MakeLower();
	infoname.Replace(".dem","_info.txt");

	//intensity image path
	CString intensityname = outfname;
	intensityname.MakeLower();
	intensityname.Replace(".dem","_intensity.bmp");

	fstream infile;
	infile.open(infname, ios::in);
	char line[512];
	int num_points = 0;
	double X, Y, Z;
	double I;
	MinX_first = MinY_first = MinZ_first = 1.0e99;
	MaxX_first = MaxY_first = MaxZ_first = -1.0e99;

	while(!infile.eof())
	{
		infile>>X>>Y>>Z;
		if(bIntensity == true)
			infile>>I;

		infile.getline(line, 512);
		
		if(X < MinX_first) {MinX_first   = X;	MinX_Y_first = Y;	MinX_Z_first = Z;}
		if(Y < MinY_first) {MinY_X_first = X;	MinY_first	 = Y;	MinY_Z_first = Z;}
		if(Z < MinZ_first) {MinZ_X_first = X;	MinZ_Y_first = Y;	MinZ_first   = Z;}

		if(X > MaxX_first) {MaxX_first   = X;	MaxX_Y_first = Y;	MaxX_Z_first = Z;}
		if(Y > MaxY_first) {MaxY_X_first = X;	MaxY_first	 = Y;	MaxY_Z_first = Z;}
		if(Z > MaxZ_first) {MaxZ_X_first = X;	MaxZ_Y_first = Y;	MaxZ_first   = Z;}

		if(I <= 255 && I >= 0)
		{			
			if(min_I > I) min_I = I;
			if(max_I < I) max_I = I;
		}
		
		average += Z;

		num_points ++;
	}

	average = average/num_points;

	double area0 = (MaxX_first - MinX_first)*(MaxY_first - MinY_first);

	double density0 = area0/num_points;

	if(bAutoRes == true) {
		double root_density0 = sqrt(density0);
		offset_X = offset_Y = (double)(int)( 1.0/root_density0 * 100 ); //cm unit
	}

	double Range_I = (max_I - min_I);

	infile.close();

	oX = offset_X; oY = offset_Y; oZ = MaxZ_first - MinZ_first;

	sX = MinX_first;//left 
	sY = MaxY_first;//top 
	sZ = MinZ_first;//lowest point
	
	width = (int)((MaxX_first - MinX_first)/oX + 1.5);
	height = (int)((MaxY_first - MinY_first)/oY + 1.5);
	depth = 1;
	
	int i, j, k;
	//DEM
	double* newdata = new double[width*height];

	//Intensity image
	CBMPImage intensityimg;
	int I_width = (int)((MaxX_first - MinX_first)/Intensity_res + 1.5);
	int I_height = (int)((MaxY_first - MinY_first)/Intensity_res + 1.5); 
	CBMPPixelPtr intensity_pixel;
	CPixel value; value = (BYTE)0;

	if(bIntensity == true)
	{
		intensityimg.Create(I_width,I_height,8);
		
		intensity_pixel.SetImage(&intensityimg);
		
		//Initialize a intensity image
		for(i=0; i<intensityimg.GetHeight(); i++)
		{
			for(j=0; j<intensityimg.GetWidth(); j++)
			{
				intensity_pixel[i][j] = value;
			}
		}
	}

	double init_val = -999.0;//init value for DEM cells

	//Initialize a grid
	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)

		{
			newdata[i*width + j] = init_val;
		}
	}

	CBMPImage gridimg;//DEM  image
	int bitcount;
	if(bcolor == true) bitcount = 24;
	else bitcount = 8;
	gridimg.Create(width,height,bitcount);

	CBMPColorPixelPtr pixel;
	CBMPPixelPtr greypixel;

	if(bitcount == 8)
		greypixel.SetImage(&gridimg);
	else if(bitcount == 24)
		pixel.SetImage(&gridimg);
	else
		return;

	CColorPixel color;
	color.SetR(0); color.SetG(0); color.SetB(0);
	
	//Initialize a dem-image
	for(i=0; i<gridimg.GetHeight(); i++)
	{
		for(j=0; j<gridimg.GetWidth(); j++)
		{
			if(bitcount == 24)
				color >> pixel[i][i];//black
			else if(bitcount == 8)
				greypixel[i][i] = (BYTE)0;//black
			else
				return;
		}
	}

	infile.open(infname, ios::in);

	double Range_Z = (MaxZ_first - MinZ_first);
	
	for(k=0; k<num_points; k++)
	{
		_Point_ point;
		infile>>point.X>>point.Y>>point.Z;
		
		double temp_val;
		if(bIntensity == true)
			infile>>temp_val;

		point.intensity = int(temp_val + 0.5);

		infile.getline(line, 512);
		
		int ix, iy, iz;

		if(true == Point2GridPos(point, iy, ix, iz))
		{
			int grid_index = iy*width + ix;
			
			if(newdata[grid_index] != init_val)
			{
				if(newdata[grid_index] < point.Z) newdata[grid_index] = point.Z;//select a lowest point.
			}
			else
			{
				newdata[grid_index] = point.Z;
			}

			if(bIntensity == true)
			{
				//Intensity image
				int row = (int)((sY - point.Y)/Intensity_res + 0.5); 
				int col = (int)((point.X - sX)/Intensity_res + 0.5);
				
				if(point.intensity > max_I)
				{
					point.intensity = int(max_I + 0.5);
				}

				if(point.intensity < min_I)
				{
					point.intensity = int(min_I + 0.5);
				}

				BYTE DN_intensity = (int)(((double)point.intensity - (double)min_I)/(double)Range_I * (double)255 + 0.5);
				//CBasicImage::GetRainbowColor_new(DN_intensity, r, g, b);
				intensity_pixel[row][col] = DN_intensity;
			}
		}
	}

	infile.close();

	//interpolation (DEM)
	if(bInterpolation == true)
	{
		double* copydata = new double[width*height];
		for(i=0; i<height; i++)
		{
			for(j=0; j<width; j++)
			{
				copydata[i*width + j] = newdata[i*width + j];
			}
		}

		for(i=1; i<height-1; i++)
		{
			for(j=1; j<width-1; j++)
			{
				if(copydata[i*width + j] == init_val)
				{
					double sum=0;
					int count=0;
					for(int m=-1; m<=1; m++)
					{
						for(int n=-1; n<=1; n++)
						{
							if(copydata[(i+m)*width + (j+n)] != init_val)
							{
								sum += newdata[(i+m)*width + (j+n)];
								count++;
							}
						}
					}
					
					newdata[i*width + j] = sum/count;
				}
				else
				{
					newdata[i*width + j] = copydata[i*width + j];
				}
			}
		}

		if((width*height)>0)
			delete[] copydata;
	}

	//interpolation (Intensity image)
	if(bInterpolation == true && bIntensity == true)
	{
		CBMPImage copyimg = intensityimg;
		CBMPPixelPtr copy_pixel;
		copy_pixel.SetImage(&copyimg);

		for(i=1; i<I_height-1; i++)
		{
			for(j=1; j<I_width-1; j++)
			{
				if(BYTE(0) == copy_pixel[i][j])
				{
					int sum=0;
					int count=0;
					for(int m=-1; m<=1; m++)
					{
						for(int n=-1; n<=1; n++)
						{
							if(BYTE(0) != copy_pixel[i+m][j+n])
							{
								sum += (int)copy_pixel[i+m][j+n];
								count++;
							}
						}
					}
					
					intensity_pixel[i][j] = (BYTE)((float)sum/(float)count + 0.5);
				}
			}
		}

		copyimg.Free();
	}

	if(bIntensity == true)
	{
		intensityimg.SaveBMP(intensityname);
	}

	//DEM image
	BYTE r, g, b;

	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			//DEM Image
			BYTE intensity;
			double heightval;
			
			heightval = newdata[i*width + j];

			if(heightval != init_val) intensity = (BYTE)(((heightval - MinZ_first)/Range_Z)*255.0);
			else intensity = (BYTE)(0);
			
			if(bitcount == 8)
			{
				greypixel[i][j] = intensity;
			}
			else
			{
				CBasicImage::GetRainbowColor_new(intensity, r, g, b);
				color.SetR(r); color.SetG(g); color.SetB(b);
				color >> pixel[i][j];
			}
		}
	}
	
	CFile newfile;
	CFileException fe;
	newfile.Open(outfname, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe);
	newfile.Write(newdata, DWORD(width)*DWORD(height)*sizeof(double));

	//Cal area
	//double area0 = (MaxX_first - MinX_first)*(MaxY_first - MinY_first);
	int num_NoneZero=0;
	color.SetR(0); color.SetG(0); color.SetB(0);
	for(i=0; i<gridimg.GetHeight(); i++)
	{
		for(j=0; j<gridimg.GetWidth(); j++)
		{
			if(bitcount == 8)
			{
				if(!((BYTE)0 == greypixel[i][j]))
				{
					num_NoneZero ++;
				}
			}
			else if(bitcount == 24)
			{
				if(!(color == pixel[i][j]))
				{
					num_NoneZero ++;
				}
			}
			else
				return;
		}
	}

	double area1 = area0 * num_NoneZero/(gridimg.GetHeight()*gridimg.GetWidth());
	double density = num_points/area1;

	gridimg.SaveBMP(imgname);

	fstream infofile;
	infofile.open(infoname,ios::out);
	infofile.precision(10);
	infofile<<"AREA0: "<<area0<<" (M^2)"<<endl;
	infofile<<"AREA1: "<<area1<<" (M^2)"<<endl;
	infofile<<"Num_Points: "<<num_points<<endl;
	infofile<<"DENSITY: "<<density<<" (points/M^2)"<<endl;
	
	infofile<<"Min(X): "<<MinX_first<<"\t"<<"Min(X)Y: "<<MinX_Y_first<<"\t"<<"Min(X)Z: "<<MinX_Z_first<<endl;
	infofile<<"Min(Y)X: "<<MinY_X_first<<"\t"<<"Min(Y): "<<MinY_first<<"\t"<<"Min(Y)Z: "<<MinY_Z_first<<endl;
	infofile<<"Min(Z)X: "<<MinZ_X_first<<"\t"<<"Min(Z)Y: "<<MinZ_Y_first<<"\t"<<"Min(Z): "<<MinZ_first<<endl;
	
	infofile<<"Max(X): "<<MaxX_first<<"\t"<<"Max(X)Y: "<<MaxX_Y_first<<"\t"<<"Max(X)Z: "<<MaxX_Z_first<<endl;
	infofile<<"Max(Y)X: "<<MaxY_X_first<<"\t"<<"Max(Y): "<<MaxY_first<<"\t"<<"Max(Y)Z: "<<MaxY_Z_first<<endl;
	infofile<<"Max(Z)X: "<<MaxZ_X_first<<"\t"<<"Max(Z)Y: "<<MaxZ_Y_first<<"\t"<<"Max(Z): "<<MaxZ_first<<endl;

	infofile<<"Average(Z): "<<average<<endl;
	
	infofile.close();
	
	fstream hdrfile;
	hdrfile.open(hdrname,ios::out);
	hdrfile.precision(10);
	hdrfile<<width<<"\t"<<height<<"\t"<<setw(10)<<offset_X<<"\t"<<setw(10)<<sX<<"\t"<<setw(10)<<sY<<"\t"<<setw(10)<<init_val<<"\t"<<setw(10)<<init_val<<endl;

	hdrfile.close();
	delete[] newdata;
	//delete[] num_cell;
	gridimg.Free();
	newfile.Close();
	
}

void CSMPseudoGridData::SaveAsIntensityGrid(CString infname, CString outfname, double offset_X, double offset_Y, bool bcolor, bool bStretch)
{
	//Min & Max values
	double MinX_first=1.0e20, MaxX_first=-1.0e20;
	double MinX_Y_first;
	double MinX_Z_first;
	double MaxX_Y_first;
	double MaxX_Z_first;

	double MinY_first=1.0e20, MaxY_first=-1.0e20;
	double MinY_X_first;
	double MinY_Z_first;
	double MaxY_X_first;
	double MaxY_Z_first;

	double MinZ_first=1.0e20, MaxZ_first=-1.0e20;
	double MinZ_X_first;
	double MinZ_Y_first;
	double MaxZ_X_first;
	double MaxZ_Y_first;

	double average = 0.0;

	int average_intensity = 0;

	//dem image path
	CString imgname = outfname;
	imgname.MakeLower();
	imgname.Replace(".dem",".bmp");

	//intensity image path
	CString intensityname = outfname;
	intensityname.MakeLower();
	intensityname.Replace(".dem","_intensity.bmp");
	
	//dem header path
	CString hdrname = outfname;
	hdrname.MakeLower();
	hdrname.Replace(".dem",".hdr");

	//dem info path
	CString infoname = outfname;
	infoname.MakeLower();
	infoname.Replace(".dem","_info.txt");

	//read point cloud data (txt file)
	m_PointsCloud.ReadTXTPoints(infname,true, false);

	//start X & Y and offset values
	oX = offset_X; oY = offset_Y; oZ = 1.0e10;
	sX = m_PointsCloud.MinX; sY = m_PointsCloud.MaxY; sZ = m_PointsCloud.MinZ;
	
	//width, height and depth
	width = (int)((m_PointsCloud.MaxX - m_PointsCloud.MinX)/oX + 1.5);
	height = (int)((m_PointsCloud.MaxY - m_PointsCloud.MinY)/oY + 1.5);
	depth = (int)((m_PointsCloud.MaxZ - m_PointsCloud.MinZ)/oZ + 1.5);
	
	int i, j, k;
	
	double* newdata = new double[width*height];
	BYTE* intensity_newdata = new BYTE[width*height];

	int* num_cell = new int[width*height];

	for(i=0; i<height; i++)
		for(j=0; j<width; j++)
			num_cell[i*width + j] = 0;

	CBMPImage gridimg;
	CBMPImage intensityimg;
	
	int bitcount;
	if(bcolor == true) bitcount = 24;
	else bitcount = 8;
	
	gridimg.Create(width,height,bitcount);
	intensityimg.Create(width,height,bitcount);

	CBMPColorPixelPtr pixel;
	CBMPPixelPtr greypixel;

	CBMPColorPixelPtr intensity_pixel;
	CBMPPixelPtr intensity_greypixel;

	if(bitcount == 8)
		greypixel.SetImage(&gridimg);
	else if(bitcount == 24)
		pixel.SetImage(&gridimg);
	else
		return;

	if(bitcount == 8)
		intensity_greypixel.SetImage(&intensityimg);
	else if(bitcount == 24)
		intensity_pixel.SetImage(&intensityimg);
	else
		return;
	
	CColorPixel value; value.SetR(0); value.SetG(0); value.SetB(0);
	
	for(i=0; i<gridimg.GetHeight(); i++)
	{
		for(j=0; j<gridimg.GetWidth(); j++)
		{
			if(bitcount == 24)
			{
				value >> pixel[i][j];
				value >> intensity_pixel[i][j];
			}
			else if(bitcount == 8)
			{
				greypixel[i][j] = (BYTE)0;
				intensity_greypixel[i][j] = (BYTE)0;
			}
			else
				return;
		}
	}

	int num = m_PointsCloud.m_Points.GetNumItem();

	double Range_Z = (m_PointsCloud.MaxZ - m_PointsCloud.MinZ);

	int Range_I = (m_PointsCloud.MaxI - m_PointsCloud.MinI);

	BYTE r, g, b;
	
	for(k=0; k<num; k++)
	{
		//_Point_ point = m_PointsCloud.m_Points.GetAt(k);
		_Point_ point;
		DATA<_Point_>* pos;

		if(k==0)
		{
			pos = m_PointsCloud.m_Points.GetFirstPos();
			point = pos->value;
		}
		else
		{
			pos = m_PointsCloud.m_Points.GetNextPos(pos);
			point = pos->value;
		}
		
		int ix, iy, iz;
		if(true == Point2GridPos(point, iy, ix, iz))
		{
			int grid_index = iy*width + ix;
			if(num_cell[grid_index] != 0)
			{
				int next_count = num_cell[grid_index] + 1;
				
				double Z = newdata[grid_index];
				newdata[grid_index] = (Z*num_cell[grid_index] + point.Z)/next_count;

				BYTE I = intensity_newdata[grid_index];
				intensity_newdata[grid_index] = (BYTE)(I*num_cell[grid_index] + point.intensity)/next_count;
				
				num_cell[grid_index] = next_count;
			}
			else
			{
				newdata[grid_index] = point.Z;
				intensity_newdata[grid_index] = point.intensity;

				num_cell[grid_index] = 1;
			}
			
			if(MinX_first>point.X) {MinX_first   = point.X;	MinX_Y_first = point.Y;	MinX_Z_first = point.Z;}
			if(MinY_first>point.Y) {MinY_X_first = point.X;	MinY_first	 = point.Y;	MinY_Z_first = point.Z;}
			if(MinZ_first>point.Z) {MinZ_X_first = point.X; MinZ_Y_first = point.Y;	MinZ_first   = point.Z;}
			
			if(MaxX_first<point.X) {MaxX_first   = point.X;	MaxX_Y_first = point.Y;	MaxX_Z_first = point.Z;}
			if(MaxY_first<point.Y) {MaxY_X_first = point.X; MaxY_first	 = point.Y;	MaxY_Z_first = point.Z;}
			if(MaxZ_first<point.Z) {MaxZ_X_first = point.X; MaxZ_Y_first = point.Y;	MaxZ_first   = point.Z;}
			
			if(k==0)
			{
				average = point.Z;
				average_intensity = point.intensity;
			}
			else
			{
				average = (average*k + point.Z)/k+1;
				average_intensity = (int)((double)average_intensity*k + (double)point.intensity)/k+1;
			}

			average_intensity += point.intensity;

			BYTE DN; 
			DN = (BYTE)(((newdata[grid_index] - m_PointsCloud.MinZ)/Range_Z)*255.0);
			
			BYTE DN_intensity;
			if(bStretch == true) DN_intensity = (BYTE)((((double)intensity_newdata[grid_index] - (double)m_PointsCloud.MinI)/(double)Range_I)*255.0);
			else DN_intensity = (BYTE)intensity_newdata[grid_index];

			if(bitcount == 8)
			{
				greypixel[iy][ix] = DN;
				intensity_greypixel[iy][ix] = DN_intensity;
			}
			else
			{
				CBasicImage::GetRainbowColor_new(DN, r, g, b);
				value.SetR(r); value.SetG(g); value.SetB(b);
				value >> pixel[iy][ix];

				CBasicImage::GetRainbowColor_new(DN_intensity, r, g, b);
				value.SetR(r); value.SetG(g); value.SetB(b);
				value >> intensity_pixel[iy][ix];
			}
		}
	}
	
	CFile newfile;
	CFileException fe;
	newfile.Open(outfname, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe);
	newfile.Write(newdata, DWORD(width)*DWORD(height)*sizeof(double));

	//Cal area
	double area0 = (m_PointsCloud.MaxX-m_PointsCloud.MinX)*(m_PointsCloud.MaxY - m_PointsCloud.MinY);
	int num_NoneZero=0;
	value.SetR(0); value.SetG(0); value.SetB(0);
	for(i=0; i<gridimg.GetHeight(); i++)
	{
		for(j=0; j<gridimg.GetWidth(); j++)
		{
			if(bitcount == 8)
			{
				if(!((BYTE)0 == greypixel[i][j]))
				{
					num_NoneZero ++;
				}
			}
			else if(bitcount == 24)
			{
				if(!(value == pixel[i][j]))
				{
					num_NoneZero ++;
				}
			}
			else
				return;
		}
	}

	double area1 = area0 * num_NoneZero/(gridimg.GetHeight()*gridimg.GetWidth());
	double density = num/area1;

	gridimg.SaveBMP(imgname);
	intensityimg.SaveBMP(intensityname);

	fstream infofile;
	infofile.open(infoname,ios::out);
	infofile.precision(10);
	infofile<<"AREA0: "<<area0<<" (M^2)"<<endl;
	infofile<<"AREA1: "<<area1<<" (M^2)"<<endl;
	infofile<<"Num_Points: "<<num<<endl;
	infofile<<"DENSITY: "<<density<<" (points/M^2)"<<endl;
	
	infofile<<"Min(X): "<<MinX_first<<"\t"<<"Min(X)Y: "<<MinX_Y_first<<"\t"<<"Min(X)Z: "<<MinX_Z_first<<endl;
	infofile<<"Min(Y)X: "<<MinY_X_first<<"\t"<<"Min(Y): "<<MinY_first<<"\t"<<"Min(Y)Z: "<<MinY_Z_first<<endl;
	infofile<<"Min(Z)X: "<<MinZ_X_first<<"\t"<<"Min(Z)Y: "<<MinZ_Y_first<<"\t"<<"Min(Z): "<<MinZ_first<<endl;
	
	infofile<<"Max(X): "<<MaxX_first<<"\t"<<"Max(X)Y: "<<MaxX_Y_first<<"\t"<<"Max(X)Z: "<<MaxX_Z_first<<endl;
	infofile<<"Max(Y)X: "<<MaxY_X_first<<"\t"<<"Max(Y): "<<MaxY_first<<"\t"<<"Max(Y)Z: "<<MaxY_Z_first<<endl;
	infofile<<"Max(Z)X: "<<MaxZ_X_first<<"\t"<<"Max(Z)Y: "<<MaxZ_Y_first<<"\t"<<"Max(Z): "<<MaxZ_first<<endl;

	infofile<<"Average(Z): "<<average/num<<endl;

	infofile<<"Min/Max(I): "<<m_PointsCloud.MinI<<", "<<m_PointsCloud.MaxI<<endl;
	infofile<<"Average(I): "<<average_intensity/num<<endl;
	
	infofile.close();
	
	fstream hdrfile;
	hdrfile.open(hdrname,ios::out);
	hdrfile.precision(10);
	hdrfile<<width<<"\t"<<height<<"\t"<<setw(10)<<offset_X<<"\t"<<setw(10)<<sX<<"\t"<<setw(10)<<sY<<endl;

	hdrfile.close();
	delete[] newdata;
	delete[] intensity_newdata;
	delete[] num_cell;

	gridimg.Free();
	intensityimg.Free();
	newfile.Close();
}

bool CSMPseudoGridData::Point2GridPos(double X, double Y, double Z, int &i, int &j, int &k)
{
	i = (int)((sY - Y)/oY + 0.5); j = (int)((X - sX)/oX + 0.5); k = (int)((Z - sZ)/oZ + 0.5);

	if(i<0) return false;
	if(j<0) return false;
	if(k<0) return false;

	if(i>=height) return false;
	if(j>=width) return false;
	if(k>=depth) return false;

	return true;
}

bool CSMPseudoGridData::Point2GridPos(_Point_ p, int &i, int &j, int &k)
{
	i = (int)((sY - p.Y)/oY); j = (int)((p.X - sX)/oX); k = (int)((p.Z - sZ)/oZ);

	if(i<0) return false;
	if(j<0) return false;
	if(k<0) return false;

	if(i>=height) return false;
	if(j>=width) return false;
	if(k>=depth) return false;

	return true;
}

bool CSMPseudoGridData::PutPoint(_Point_ p, int &i, int &j, int &k)
{
	if(false == Point2GridPos(p, i, j, k)) return false;

	m_Grid[i][j][k].point.AddTail(p);

	return true;
}

bool CSMPseudoGridData::PutPoint(_PTS_Point_ p, int &i, int &j, int &k)
{
	if(false == Point2GridPos(p, i, j, k)) return false;

	m_Grid_PTS[i][j][k].point.AddTail(p);

	return true;
}

void CSMPseudoGridData::RemoveDuplication(CString fname, CString outname, double offset_X, double offset_Y, double offset_Z)
{
	double MaxX, MinX;
	double MaxY, MinY;
	double MaxZ, MinZ;
	
	MaxX = MaxY = MaxZ = -1.0e10;
	MinX = MinY = MinZ = +1.0e10;

	fstream infile;
	infile.open(fname, ios::in);

	int num = 0;

	_Point_ p;
	
	infile>>ws;
	while(!infile.eof())
	{	
		num ++;
		infile>>p.X>>p.Y>>p.Z;
		infile>>ws;
		
		if(MinX>p.X) MinX = p.X; if(MaxX<p.X) MaxX = p.X;
		if(MinY>p.Y) MinY = p.Y; if(MaxY<p.Y) MaxY = p.Y;
		if(MinZ>p.Z) MinZ = p.Z; if(MaxZ<p.Z) MaxZ = p.Z;
	}

	infile.close();

	//영역 계산
	oX = offset_X; oY = offset_Y; oZ = offset_Z;
	sX = MinX; sY = MaxY; sZ = MinZ;
	
	//그리드 크기 계산
	width = (int)((MaxX - MinX)/oX + 1.5);
	height = (int)((MaxY - MinY)/oY + 1.5);
	depth = (int)((MaxZ - MinZ)/oZ + 1.5);

	//메모리 할당.
	int i, j, k;
	bool ***bCheck;
	bCheck = new bool**[height];
	for(i=0; i<height; i++)
		bCheck[i] = new bool*[width];
	for(i=0; i<height; i++)
		for(j=0; j<width; j++)
			bCheck[i][j] = new bool[depth];

	for(i=0; i<height; i++)
		for(j=0; j<width; j++)
			for(k=0; k<depth; k++)
				bCheck[i][j][k] = false;

	infile.open(fname, ios::in);
	fstream outfile;
	outfile.open(outname, ios::out);

	CProgressBar bar("Removing duplication...", 100, num);

	infile>>ws;
	while(!infile.eof())
	{		
		infile>>p.X>>p.Y>>p.Z;
		infile>>ws;

		int ix, iy, iz;
		if(false == Point2GridPos(p, iy, ix, iz)) continue;
		if(bCheck[iy][ix][iz] == false)
		{
			outfile.precision(6);
			outfile.setf(ios::fixed, ios::floatfield);
			outfile<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<endl;
			outfile.flush();

			bCheck[ix][iy][iz] = true;
		}
		
		bar.StepIt();
	}

	infile.close();
	outfile.close();
}

void CSMPseudoGridData::SaveData(CString fname)
{
	fstream outfile;
	outfile.open(fname, ios::out);
	CProgressBar bar("Removing duplication...", 100, height*width);
	int i, j, k, count;
	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			for(k=0; k<depth; k++)
			{
				int num = m_Grid[i][j][k].point.GetNumItem();
				for(count=0; count<num; count++)
				{
					_Point_ p = m_Grid[i][j][k].point.GetAt(count);
					
					outfile.precision(6);
					outfile.setf(ios::fixed, ios::floatfield);
					outfile<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<endl;
					outfile.flush();
				}
			}
			bar.StepIt();
		}
	}
	outfile.close(); 
}

bool CSMPseudoGridData::PlaneSearching_3DGrid_OneOrigin(int method, unsigned int W_x, unsigned int W_y, unsigned int W_z, unsigned int max_iter_num, CString outpath, double threshold, double Thresholdratio, unsigned int min_num_points, int ObjectType)
{
	CString stOutFile = outpath;
	
	int i, j, k;

	//window size error check
	if(W_x%2 != 1)
	{
		W_x = W_x -1;
		half_size_x = W_x/2;
	}
	else
	{
		half_size_x = W_x/2;
	}

	if(W_y%2 != 1)
	{
		W_y = W_y -1;
		half_size_y = W_y/2;
	}
	else
	{
		half_size_y = W_y/2;
	}

	if(W_z%2 != 1)
	{
		W_z = W_z -1;
		half_size_z = W_z/2;
	}
	else
	{
		half_size_z = W_z/2;
	}

	//accumulator 메모리 할당
	if(height<1) return false;
	if(width<1) return false;
	if(depth<1)return false;

	_Accumulator_<float> accu;
	accu.SetMemory(height, width, depth, 4);
			
	//초기화.
	for(i=0; i<height; i++)
		for(j=0; j<width; j++)
			for(k=0; k<depth; k++)
			{
				accu(i, j, k, 0) = (float)empty;
				accu(i, j, k, 1) = (float)empty;
				accu(i, j, k, 2) = (float)empty;
				accu(i, j, k, 3) = (float)+3.402823466e+30;//3.402823466 E + 38: float maximum value
			}
		
	int max_buf_size = height*width*depth*3;
	
	min_a = min_b = min_c = (float)+3.402823466e+30;//3.402823466 E + 38: float maximum value
	max_a = max_b = max_c = (float)-3.402823466e+30;

	CProgressBar bar("Plane fitting...", 100, height*width);

	int num_non_planar=0;
	int count_total=0;
	
	for(i=0; i<height; i++)//Y direction
	{
		for(j=0; j<width; j++)//X direction
		{
			for(k=0; k<depth; k++)//Z direction
			{
				double a, b, c;
				bool bRet;
				double pos_var;

				if(m_Grid[i][j][k].point.GetNumItem()<1) continue;

				count_total += m_Grid[i][j][k].point.GetNumItem();
				
				if(method == 0)
				{
					//bRet = GLSforCoeff((int)j, (int)i, (int)k, (int)half_size, a, b, c, max_iter_num, pos_var, limit_var);
					return false;
				}
				else if(method == 1)
				{
					bRet = RobustLSforCoeff((int)j, (int)i, (int)k, a, b, c, max_iter_num, pos_var, threshold);
				}
				else
				{
					bRet = LSforCoeff((int)j, (int)i, (int)k, a, b, c, max_iter_num, pos_var);
				}
				
				/////////////////////////////////////////////
				//
				//
				if(bRet == true)
				{
					//ax + by + cz + 1 = 0;
					UpdateDistance(accu, i, j, k, a, b, c);
				}
				
			}//end of k	
			
			bar.StepIt();
			
		}//end of j
	}//end of i

	fstream param_file;
	param_file.open(outpath,ios::out);
	
	param_file.precision(6);
	param_file.setf(ios::fixed, ios::floatfield);

	count_total = 0;

	for(i=0; i<height; i++)//Y direction
	{
		for(j=0; j<width; j++)//X direction
		{
			for(k=0; k<depth; k++)//Z direction
			{
				float a, b, c, errordist;
				a = accu(i, j, k, 0);
				b = accu(i, j, k, 1);
				c = accu(i, j, k, 2);
				errordist = accu(i, j, k, 3);

				if((a==(float)empty)&&(b==(float)empty)&&(c==(float)empty))
				{
					continue;
				}
				else
				{
					int num = m_Grid[i][j][k].point.GetNumItem();

					count_total += m_Grid[i][j][k].point.GetNumItem();
					
					for(int count=0; count<num; count++)//each point in each grid
					{
						_Point_ p = m_Grid[i][j][k].point.GetAt(count);
						
						if(bNormalize == true)
						{
							p.X += ShiftX; p.Y += ShiftY; p.Z += ShiftZ;
						}

						param_file<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
						param_file<<setw(10)<<a<<"\t"<<setw(10)<<b<<"\t"<<setw(10)<<c<<"\t"<<setw(10)<<errordist<<endl;
						param_file.flush();
					}//end of count
					
					if((float)a>max_a) max_a = (float)a; if((float)a<min_a) min_a = (float)a;
					if((float)b>max_b) max_b = (float)b; if((float)b<min_b) min_b = (float)b;
					if((float)c>max_c) max_c = (float)c; if((float)c<min_c) min_c = (float)c;
					
				}
				
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//Use of azimuth and aspect
	//Feb-11, 2011

	param_file<<endl<<endl<<"slope and aspect"<<endl<<endl;

	min_a = min_b = min_c = (float)+3.402823466e+30;//3.402823466 E + 38: float maximum value
	max_a = max_b = max_c = (float)-3.402823466e+30;

	for(i=0; i<height; i++)//Y direction
	{
		for(j=0; j<width; j++)//X direction
		{
			for(k=0; k<depth; k++)//Z direction
			{
				float accu_dX, accu_dY, accu_dZ, accu_dist;
				accu_dX = accu(i, j, k, 0);
				accu_dY = accu(i, j, k, 1);
				accu_dZ = accu(i, j, k, 2);
				accu_dist = accu(i, j, k, 3);

				if((accu_dX==(float)empty)&&(accu_dY==(float)empty)&&(accu_dZ==(float)empty))
				{
					continue;
				}
				else if((accu_dX==(float)none_plane)&&(accu_dY==(float)none_plane)&&(accu_dZ==(float)none_plane))
				{
					continue;
				}
				else
				{
					double azimuth = Rad2Deg(atan2(accu_dX, accu_dY));
					double Hrz_Dist = sqrt(accu_dX*accu_dX + accu_dY*accu_dY);
					double slope = Rad2Deg(atan2(accu_dZ, Hrz_Dist));
					double Dist = sqrt(Hrz_Dist*Hrz_Dist + accu_dZ*accu_dZ);
					accu(i, j, k, 0) = (float)azimuth;
					accu(i, j, k, 1) = (float)slope;
					accu(i, j, k, 2) = (float)Dist;

					if((float)accu(i, j, k, 0)>max_a) max_a = (float)accu(i, j, k, 0); if((float)accu(i, j, k, 0)<min_a) min_a = (float)accu(i, j, k, 0);
					if((float)accu(i, j, k, 1)>max_b) max_b = (float)accu(i, j, k, 1); if((float)accu(i, j, k, 1)<min_b) min_b = (float)accu(i, j, k, 1);
					if((float)accu(i, j, k, 2)>max_c) max_c = (float)accu(i, j, k, 2); if((float)accu(i, j, k, 2)<min_c) min_c = (float)accu(i, j, k, 2);

					param_file<<setw(10)<<i<<"\t"<<setw(10)<<j<<"\t"<<setw(10)<<k<<"\t";
					param_file<<setw(10)<<azimuth<<"\t"<<setw(10)<<slope<<"\t"<<setw(10)<<Dist<<endl;
					param_file.flush();
				}
			}
		}
	}

	//
	//
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	range_a = max_a - min_a;
	range_b = max_b - min_b;
	range_c = max_c - min_c;

	CString int_path = outpath;
	int_path.Replace(".txt","_distmap_int.cls");

	fstream int_file; int_file.open(int_path,ios::out);
	
	int_file.precision(6);
	int_file.setf(ios::fixed, ios::floatfield);

	for(i=0; i<height; i++)//Y direction
	{
		for(j=0; j<width; j++)//X direction
		{
			for(k=0; k<depth; k++)//Z direction
			{
				float accu_azimuth, accu_slope, accu_dist;
				accu_azimuth = accu(i, j, k, 0);
				accu_slope = accu(i, j, k, 1);
				accu_dist = accu(i, j, k, 2);

				if((accu_azimuth==(float)empty)&&(accu_slope==(float)empty)&&(accu_dist==(float)empty))
				{
					continue;
				}
				else if((accu_azimuth==(float)none_plane)&&(accu_slope==(float)none_plane)&&(accu_dist==(float)none_plane))
				{
					continue;
				}
				else
				{
					//quantization (256 step)
					//int aI = (int)((accu_azimuth - min_a)/range_a*255 + 0.5);
					//int bI = (int)((accu_slope - min_b)/range_b*255 + 0.5);
					//int cI = (int)((accu_dist - min_c)/range_c*255 + 0.5);

					int aI, bI, cI;
					aI = (int)((accu_azimuth + 180)/360*255 + 0.5);
					bI = (int)((accu_slope + 90)/180*255 + 0.5);
					cI = (int)((accu_dist - min_c)/range_c*255 + 0.5);

					accu(i, j, k, 0) = (float)aI;
					accu(i, j, k, 1) = (float)bI;
					accu(i, j, k, 2) = (float)cI;

					int num = m_Grid[i][j][k].point.GetNumItem();
					
					for(int count=0; count<num; count++)//each point in each grid
					{
						_Point_ p = m_Grid[i][j][k].point.GetAt(count);
						
						if(bNormalize == true)
						{
							p.X += ShiftX; p.Y += ShiftY; p.Z += ShiftZ;
						}
						
						int_file<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
						int_file<<setw(10)<<aI<<"\t"<<setw(10)<<bI<<"\t"<<setw(10)<<cI<<endl;
						int_file.flush();
					}//end of count
				}
			}
		}
	}

	///////////////////////////
	//
	//
	DistanceMapClassification((float)Thresholdratio, accu, outpath, (int)min_num_points, "Clustering...", ObjectType);

	int_file.close();
	param_file.close();

	//////////////////////////////////////////////////////////////////

	return true;
}

bool CSMPseudoGridData::PlaneSearching_3DGrid_OneOrigin_old(int method, unsigned int W_x, unsigned int W_y, unsigned int W_z, unsigned int max_iter_num, CString outpath, double threshold, double Thresholdratio, unsigned int min_num_points, int ObjectType)
{
	CString stOutFile = outpath;
	
	int i, j, k;

	//window size error check
	if(W_x%2 != 1)
	{
		W_x = W_x -1;
		half_size_x = W_x/2;
	}
	else
	{
		half_size_x = W_x/2;
	}

	if(W_y%2 != 1)
	{
		W_y = W_y -1;
		half_size_y = W_y/2;
	}
	else
	{
		half_size_y = W_y/2;
	}

	if(W_z%2 != 1)
	{
		W_z = W_z -1;
		half_size_z = W_z/2;
	}
	else
	{
		half_size_z = W_z/2;
	}

	//accumulator 메모리 할당
	if(height<1) return false;
	if(width<1) return false;
	if(depth<1)return false;

	_Accumulator_<float> accu;
	accu.SetMemory(height, width, depth, 4);
			
	//초기화.
	for(i=0; i<height; i++)
		for(j=0; j<width; j++)
			for(k=0; k<depth; k++)
			{
				accu(i, j, k, 0) = (float)empty;
				accu(i, j, k, 1) = (float)empty;
				accu(i, j, k, 2) = (float)empty;
				accu(i, j, k, 3) = (float)+3.402823466e+30;//3.402823466 E + 38: float maximum value
			}
		
	int max_buf_size = height*width*depth*3;
	
	min_a = min_b = min_c = (float)+3.402823466e+30;//3.402823466 E + 38: float maximum value
	max_a = max_b = max_c = (float)-3.402823466e+30;

	CProgressBar bar("Plane fitting...", 100, height*width);

	int num_non_planar=0;
	int count_total=0;
	
	for(i=0; i<height; i++)//Y direction
	{
		for(j=0; j<width; j++)//X direction
		{
			for(k=0; k<depth; k++)//Z direction
			{
				double a, b, c;
				bool bRet;
				double pos_var;

				if(m_Grid[i][j][k].point.GetNumItem()<1) continue;

				count_total += m_Grid[i][j][k].point.GetNumItem();
				
				if(method == 0)
				{
					//bRet = GLSforCoeff((int)j, (int)i, (int)k, (int)half_size, a, b, c, max_iter_num, pos_var, limit_var);
					return false;
				}
				else if(method == 1)
				{
					bRet = RobustLSforCoeff((int)j, (int)i, (int)k, a, b, c, max_iter_num, pos_var, threshold);
				}
				else
				{
					bRet = LSforCoeff((int)j, (int)i, (int)k, a, b, c, max_iter_num, pos_var);
				}
				
				/////////////////////////////////////////////
				//
				//
				if(bRet == true)
				{
					UpdateDistance(accu, i, j, k, a, b, c);
				}
				
			}//end of k	
			
			bar.StepIt();
			
		}//end of j
	}//end of i

	fstream param_file;
	param_file.open(outpath,ios::out);
	
	param_file.precision(6);
	param_file.setf(ios::fixed, ios::floatfield);

	count_total = 0;

	for(i=0; i<height; i++)//Y direction
	{
		for(j=0; j<width; j++)//X direction
		{
			for(k=0; k<depth; k++)//Z direction
			{
				float a, b, c, errordist;
				a = accu(i, j, k, 0);
				b = accu(i, j, k, 1);
				c = accu(i, j, k, 2);
				errordist = accu(i, j, k, 3);

				if((a==(float)empty)&&(b==(float)empty)&&(c==(float)empty))
				{
					continue;
				}
				else
				{
					int num = m_Grid[i][j][k].point.GetNumItem();

					count_total += m_Grid[i][j][k].point.GetNumItem();
					
					for(int count=0; count<num; count++)//each point in each grid
					{
						_Point_ p = m_Grid[i][j][k].point.GetAt(count);
						
						if(bNormalize == true)
						{
							p.X += ShiftX; p.Y += ShiftY; p.Z += ShiftZ;
						}

						param_file<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
						param_file<<setw(10)<<a<<"\t"<<setw(10)<<b<<"\t"<<setw(10)<<c<<"\t"<<setw(10)<<errordist<<endl;
						param_file.flush();
					}//end of count
					
					if((float)a>max_a) max_a = (float)a; if((float)a<min_a) min_a = (float)a;
					if((float)b>max_b) max_b = (float)b; if((float)b<min_b) min_b = (float)b;
					if((float)c>max_c) max_c = (float)c; if((float)c<min_c) min_c = (float)c;
					
				}
				
			}
		}
	}

	range_a = max_a - min_a;
	range_b = max_b - min_b;
	range_c = max_c - min_c;

	CString int_path = outpath;
	int_path.Replace(".txt","_distmap_int.cls");

	fstream int_file; int_file.open(int_path,ios::out);
	
	int_file.precision(6);
	int_file.setf(ios::fixed, ios::floatfield);

	for(i=0; i<height; i++)//Y direction
	{
		for(j=0; j<width; j++)//X direction
		{
			for(k=0; k<depth; k++)//Z direction
			{
				float accu_dX, accu_dY, accu_dZ;
				accu_dX = accu(i, j, k, 0);
				accu_dY = accu(i, j, k, 1);
				accu_dZ = accu(i, j, k, 2);

				if((accu_dX==(float)empty)&&(accu_dY==(float)empty)&&(accu_dZ==(float)empty))
				{
					continue;
				}
				else if((accu_dX==(float)none_plane)&&(accu_dY==(float)none_plane)&&(accu_dZ==(float)none_plane))
				{
					continue;
				}
				else
				{
					//quantization (256 step)
					int aI = (int)((accu_dX - min_a)/range_a*255 + 0.5);
					int bI = (int)((accu_dY - min_b)/range_b*255 + 0.5);
					int cI = (int)((accu_dZ - min_c)/range_c*255 + 0.5);

					accu(i, j, k, 0) = (float)aI;
					accu(i, j, k, 1) = (float)bI;
					accu(i, j, k, 2) = (float)cI;

					int num = m_Grid[i][j][k].point.GetNumItem();
					
					for(int count=0; count<num; count++)//each point in each grid
					{
						_Point_ p = m_Grid[i][j][k].point.GetAt(count);
						
						if(bNormalize == true)
						{
							p.X += ShiftX; p.Y += ShiftY; p.Z += ShiftZ;
						}
						
						int_file<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
						int_file<<setw(10)<<aI<<"\t"<<setw(10)<<bI<<"\t"<<setw(10)<<cI<<endl;
						int_file.flush();
					}//end of count
				}
			}
		}
	}

	///////////////////////////
	//
	//
	DistanceMapClassification((float)Thresholdratio, accu, outpath, (int)min_num_points, "Clustering...", ObjectType);

	int_file.close();
	param_file.close();

	//////////////////////////////////////////////////////////////////

	return true;
}

bool CSMPseudoGridData::BuildingExtractionwith3DPseudoGrid(CString pointsfilepath, CString rawpath, double oX, double oY, double oZ, int method, unsigned int W_x, unsigned int W_y, unsigned int W_z, unsigned int max_iter_num, CString outpath, double threshold, double Thresholdratio, unsigned int min_num_points)
{
	if(rawpath != "") {RawPath = rawpath;}

	nPlaneIndex = 0;

	int nRead=0; 
	int offsetRead = 1000;

	//CString ext = pointsfilepath.Right(4);
	//ext.MakeLower();
	//if(ext == ".pts")
	//{
	//	ReadPTSPoints(pointsfilepath,oX, oY, oZ, true, nRead, offsetRead);
	//}
	//else
	{
		ReadTXTPoints(pointsfilepath,oX, oY, oZ, true);
	}

	//freeze vertical points
	VerticalPointsFreezing();

	//ground filtering
	//GroundPointsRemove(outpath, 10.0, 5.0);

	CString outRoof = outpath;
	outRoof.MakeLower(); outRoof.Replace(".txt", "_Roof_.txt");

	//plane fitting using only horizontal points
	if(false == PlaneSearching_3DGrid_OneOrigin(method, W_x, W_y, 3, max_iter_num, outRoof, threshold, Thresholdratio, min_num_points, _SELECTION_ROOF_)) return false;

	//unfreeze vertical points
	PointsMelting();

	//freeze horizontal points (surface points)
	HorizontalPointsFreezing();

	//remove ground

	CString outWall = outpath;
	outWall.MakeLower(); outWall.Replace(".txt", "_Wall_.txt");

	//plane fitting using only vertical points
	if(false == PlaneSearching_3DGrid_OneOrigin(method, W_x, W_y, (height<int(20/oX)?height:int(30/oX)), max_iter_num, outWall, threshold, Thresholdratio*2, (int)(min_num_points), _SELECTION_WALL_)) return false;

	CString mergefilename = outpath;
	mergefilename.MakeLower(); mergefilename.Replace(".txt", "_merge_result.cls");

	FinalPlaneFitting(threshold*5, 30.0, max_iter_num, (int)(min_num_points*0.7), mergefilename);

	return true;
}

void CSMPseudoGridData::FinalPlaneFitting(double res_threshold, double heightthreshold, int max_iteration, int min_num_points, CString outpath)
{
	int i, j, k, count, n, m;

	fstream resultfile; resultfile.open(outpath,ios::out);

	resultfile.precision(6);
	resultfile.setf(ios::fixed, ios::floatfield);
	
	CString infopath = outpath; infopath.MakeLower(); infopath.Replace(".cls", "_info.txt");
	fstream resultinfo; resultinfo.open(infopath,ios::out);

	CSMList<_Point_> pointlist;
	DATA<_Point_>* pos;

	double* Zdist = new double[nPlaneIndex];
	for(i=0; i<nPlaneIndex; i++) Zdist[i] = -999.0;

	double minZdist = 1.0e20;
	double maxZdist = 0.0;

	double *abc = new double[nPlaneIndex*3];

	/////////////////////////////////////////////////////////////////
	//
	//
	fstream oldraw, newraw;
	
	oldraw.open(RawPath,ios::in);
	RemoveCommentLine(oldraw, '#');
	oldraw>>ws;
	
	CSMList<CString> rawcontents;

	while(!oldraw.eof())
	{
		char line[512];
		oldraw.getline(line,512);
		rawcontents.AddTail(line);
	}
	
	oldraw.close();

	CString newpath = RawPath+"new.txt";
	newraw.open(newpath, ios::out);

	//
	//
	///////////////////////////////////////////////////////////////////////

	for(count = 0; count<=nPlaneIndex; count++)
	{
		int ObjectType;
		pointlist.RemoveAll();

		for(i=0; i<height; i++)//Y direction
		{
			for(j=0; j<width; j++)//X direction
			{
				for(k=0; k<depth; k++)//Z direction
				{
					if(m_Grid[i][j][k].PlaneID == count)
					{
						ObjectType = m_Grid[i][j][k].type;

						if((ObjectType != _SELECTION_ROOF_)&&(ObjectType != _SELECTION_WALL_))
							continue;							

						int numP = m_Grid[i][j][k].point.GetNumItem();
						
						pos = NULL;
						for(n=0; n<numP; n++)
						{
							pos = m_Grid[i][j][k].point.GetNextPos(pos);
							pointlist.AddTail(pos->value);
						}
					}
				}
			}
		}

		CSMPlaneFitting PF;
		double a, b, c, pos_var, dist2origin;
		int num_points = pointlist.GetNumItem();
		double* errordist = new double[num_points];

		if(num_points < min_num_points) continue;

		int new_num_points = num_points;
		
		if(true == PF.PlaneFitting(res_threshold, pointlist, a, b, c, pos_var, dist2origin, errordist, max_iteration))
		{
			abc[count*3 + 0] = a;
			abc[count*3 + 1] = b;
			abc[count*3 + 2] = c;

			double ori2plane;
			double abc = sqrt(a*a + b*b + c*c);
			ori2plane = 1.0/abc;

			if(ObjectType == _SELECTION_ROOF_)
				Zdist[count] = -c*ori2plane*ori2plane;//roof height

			if(minZdist > Zdist[count]) minZdist = Zdist[count];

			if(maxZdist < Zdist[count]) maxZdist = Zdist[count];

			pos = NULL;
			
			int nR = rand()%255;
			int nG = rand()%255;
			int nB = rand()%255;

			resultinfo<<"[#ID. "<<count<<" ]"<<endl;
			resultinfo<<"[ Points IDs List ]"<<endl<<endl;
			resultinfo.flush();
			
			for(m=0; m<num_points; m++)
			{
				pos = pointlist.GetNextPos(pos);
				
				if(fabs(errordist[m]) < res_threshold)
				{
					resultfile<<setw(10)<<pos->value.X<<"\t"<<setw(10)<<pos->value.Y<<"\t"<<setw(10)<<pos->value.Z<<"\t";
					resultfile<<setw(10)<<nR<<"\t"<<setw(10)<<nG<<"\t"<<setw(10)<<nB<<endl;
					resultfile.flush();
					
					//2007.05.09 resultinfo<<pos->value.ID<<endl;
					resultinfo<<pos->value.nID<<endl;
					resultinfo.flush();

					CString out = rawcontents.GetAt(pos->value.sort);
					newraw<<out<<endl;
					newraw.flush();
				}
				else
				{
					new_num_points --;
				}
			}
			
			resultinfo<<endl<<"Number of points: "<<new_num_points<<endl;
			resultinfo.precision(6);
			resultinfo.setf(ios::fixed, ios::floatfield);
			resultinfo<<"aX + bY + cZ + 1 = 0: "<<setw(10)<<a<<"\t"<<setw(10)<<b<<"\t"<<setw(10)<<c<<endl;
			resultinfo.flush();
		}

		delete[] errordist;
	}

	delete[] Zdist;

	resultfile.close();
	resultinfo.close();
	newraw.close();
}

void CSMPseudoGridData::FinalPlaneFitting_PTS(CString ptspath, double res_threshold, double heightthreshold, int max_iteration, int min_num_points, CString outpath)
{
	int i, j, k, count, n, m;

	fstream resultfile; resultfile.open(outpath,ios::out);

	resultfile.precision(6);
	resultfile.setf(ios::fixed, ios::floatfield);
	
	CString infopath = outpath; infopath.MakeLower(); infopath.Replace(".cls", "_info.txt");
	fstream resultinfo; resultinfo.open(infopath,ios::out);

	CSMList<_Point_> pointlist;
	DATA<_Point_>* pos;

	double* Zdist = new double[nPlaneIndex];
	for(i=0; i<nPlaneIndex; i++) Zdist[i] = -999.0;

	double minZdist = 1.0e20;
	double maxZdist = 0.0;

	double *abc = new double[nPlaneIndex*3];

	/////////////////////////////////////////////////////////////////
	//
	//
	fstream oldpts, newpts;
	
	oldpts.open(ptspath,ios::in);
	
	CSMList<CString> oldptscontents;

	while(!oldpts.eof())
	{
		char line[512];
		oldpts.getline(line,512);
		oldptscontents.AddTail(line);
	}
	
	oldpts.close();

	CString newpath = ptspath+"_new.pts";
	newpts.open(newpath, ios::out);

	//
	//
	///////////////////////////////////////////////////////////////////////

	for(count = 0; count<=nPlaneIndex; count++)
	{
		int ObjectType;
		pointlist.RemoveAll();

		for(i=0; i<height; i++)//Y direction
		{
			for(j=0; j<width; j++)//X direction
			{
				for(k=0; k<depth; k++)//Z direction
				{
					if(m_Grid[i][j][k].PlaneID == count)
					{
						ObjectType = m_Grid[i][j][k].type;

						if((ObjectType != _SELECTION_ROOF_)&&(ObjectType != _SELECTION_WALL_))
							continue;							

						int numP = m_Grid[i][j][k].point.GetNumItem();
						
						pos = NULL;
						for(n=0; n<numP; n++)
						{
							pos = m_Grid[i][j][k].point.GetNextPos(pos);
							pointlist.AddTail(pos->value);
						}
					}
				}
			}
		}

		CSMPlaneFitting PF;
		double a, b, c, pos_var, dist2origin;
		int num_points = pointlist.GetNumItem();
		double* errordist = new double[num_points];

		if(num_points < min_num_points) continue;

		int new_num_points = num_points;
		
		if(true == PF.PlaneFitting_old(false, res_threshold, pointlist, a, b, c, pos_var, dist2origin, errordist, max_iteration))
		{
			abc[count*3 + 0] = a;
			abc[count*3 + 1] = b;
			abc[count*3 + 2] = c;

			double ori2plane;
			double abc = sqrt(a*a + b*b + c*c);
			ori2plane = 1.0/abc;

			if(ObjectType == _SELECTION_ROOF_)
				Zdist[count] = -c*ori2plane*ori2plane;//roof height

			if(minZdist > Zdist[count]) minZdist = Zdist[count];

			if(maxZdist < Zdist[count]) maxZdist = Zdist[count];

			pos = NULL;
			
			int nR = rand()%255;
			int nG = rand()%255;
			int nB = rand()%255;

			resultinfo<<"[#ID. "<<count<<" ]"<<endl;
			resultinfo<<"[ Points IDs List ]"<<endl<<endl;
			resultinfo.flush();
			
			for(m=0; m<num_points; m++)
			{
				pos = pointlist.GetNextPos(pos);
				
				if(fabs(errordist[m]) < res_threshold)
				{
					resultfile<<setw(10)<<pos->value.X<<"\t"<<setw(10)<<pos->value.Y<<"\t"<<setw(10)<<pos->value.Z<<"\t";
					resultfile<<setw(10)<<nR<<"\t"<<setw(10)<<nG<<"\t"<<setw(10)<<nB<<endl;
					resultfile.flush();
					
					//2007.05.09 resultinfo<<pos->value.ID<<endl;
					resultinfo<<pos->value.nID<<endl;
					resultinfo.flush();

					CString out = oldptscontents.GetAt(pos->value.sort);
					newpts<<out<<endl;
					newpts.flush();
				}
				else
				{
					new_num_points --;
				}
			}
			
			resultinfo<<endl<<"Number of points: "<<new_num_points<<endl;
			resultinfo.precision(6);
			resultinfo.setf(ios::fixed, ios::floatfield);
			resultinfo<<"aX + bY + cZ + 1 = 0: "<<setw(10)<<a<<"\t"<<setw(10)<<b<<"\t"<<setw(10)<<c<<endl;
			resultinfo.flush();
		}

		delete[] errordist;
	}

	delete[] Zdist;

	resultfile.close();
	resultinfo.close();
	newpts.close();
}

bool CSMPseudoGridData::GLSforCoeff(int index_x, int index_y, int index_z,
							  double &a, double &b, double &c, 
							  unsigned int max_iter_num,
							  double &pos_var,
							  double limit_var)
{
	CSMMatrix<double> Bmat, Jmat, Kmat, Lmat;
	CSMMatrix<double> bmat, jmat, kmat, lmat;
	CSMMatrix<double> We;
	CSMMatrix<double> JT;
	CSMMatrix<double> X;
	CSMMatrix<double> Ve, V;

	jmat.Resize(1, 3);
	lmat.Resize(1, 1);

	unsigned int r_index=0;
	
	unsigned int iteration = 0;
	
	//Make J, L
	Jmat.Resize(0,0);
	Lmat.Resize(0,0);
	for(int i=-half_size_y; i<=half_size_y; i++)//Y direction
	{
		for(int j=-half_size_x; j<=half_size_x; j++)//X direction
		{
			for(int k=-half_size_z; k<=half_size_z; k++)//Z direction
			{
				if(((i+index_y)<0)||((i+index_y)>=height)) continue;
				if(((j+index_x)<0)||((j+index_x)>=width)) continue;
				if(((k+index_z)<0)||((k+index_z)>=depth)) continue;

				int num = m_Grid[i+index_y][j+index_x][k+index_z].point.GetNumItem();
				if(num>=1) num = 1;
				for(int count=0; count<num; count++)//each point in each grid
				{
					_Point_ p = m_Grid[i+index_y][j+index_x][k+index_z].point.GetAt(count);
					double X, Y, Z;
					X = p.X-oX; Y = p.Y-oY; Z = p.Z-oZ;

					Y = sY-(i+index_y)*oY-oY/2.;
					X = sX+(j+index_x)*oX+oX/2.;
					Z = sZ+(k+index_z)*oZ+oZ/2.;
										
					jmat(0,0) = X;
					jmat(0,1) = Y;
					jmat(0,2) = Z;

					lmat(0,0) = -1;
					
					Jmat.Insert(r_index,0,jmat);
					Lmat.Insert(r_index,0,lmat);
					
					r_index++;
				}//end of count
			}//end of k
			
		}//end of j
	}//end of i
	
	if(Jmat.GetRows()<4) return false;

	X = (Jmat.Transpose()%Jmat).Inverse()%Jmat.Transpose()%Lmat;
	V = Jmat%X - Lmat;
	pos_var = (V.Transpose()%V)(0,0)/(Jmat.GetRows()-Jmat.GetCols());

	if(pos_var>limit_var) return false;

	a=X(0,0); b=X(1,0); c=X(2,0);

	bmat.Resize(1, 3);
	kmat.Resize(1, 1);
	
	CSMMatrix<double> Q(Jmat.GetRows()*3,Jmat.GetRows()*3);
	Q.Identity();
	
	double old_variance = 0;
	bool bStop = true;
	do
	{
		iteration ++;
		//Make B, K
		Bmat.Resize(0,0);
		Kmat.Resize(0,0);
		r_index=0;

		for(int i=-half_size_y; i<=half_size_y; i++)//Y direction
		{
			for(int j=-half_size_x; j<=half_size_x; j++)//X direction
			{
				for(int k=-half_size_z; k<=half_size_z; k++)//Z direction
				{
					if(((i+index_y)<0)||((i+index_y)>=height)) continue;
					if(((j+index_x)<0)||((j+index_x)>=width)) continue;
					if(((k+index_z)<0)||((k+index_z)>=depth)) continue;
					
					int num = m_Grid[i+index_y][j+index_x][k+index_z].point.GetNumItem();
					if(num>=1) num = 1;
					for(int count=0; count<num; count++)//each point in each grid
					{
						_Point_ p = m_Grid[i+index_y][j+index_x][k+index_z].point.GetAt(count);
						double X, Y, Z;
						X = p.X-oX; Y = p.Y-oY; Z = p.Z-oZ;
						
						Y = sY-(i+index_y)*oY-oY/2.;
						X = sX+(j+index_x)*oX+oX/2.;
						Z = sZ+(k+index_z)*oZ+oZ/2.;
						
						bmat(0,0) = a;
						bmat(0,1) = b;
						bmat(0,2) = c;
						
						kmat(0,0) = -a*X -b*Y -c*Z -1;
						
						Bmat.Insert(r_index,r_index*3,bmat);
						Kmat.Insert(r_index,0,kmat);
						
						r_index++;

					}//end of count
				}//end of k
				
			}//end of j
		}//end of i
				
		We = Bmat%Q%Bmat.Transpose();
		We = We.Inverse();
		
		JT = Jmat.Transpose();
		X = (JT%We%Jmat).Inverse()%JT%We%Kmat;
		
		a = a + X(0,0);
		b = b + X(1,0);
		c = c + X(2,0);
		
		Ve = Jmat%X - Kmat;
		V = Bmat.Transpose()%We%Ve;
		//Modifiy Q matrix
		for(int r=0; r<(int)Jmat.GetRows()*3; r++)
			Q(r,r) = fabs(V(r,0));

		//pos_var = (V.Transpose()%V)(0,0)/(Jmat.GetRows()-Jmat.GetCols());
		pos_var = (Ve.Transpose()%We%Ve)(0,0)/(Jmat.GetRows()-Jmat.GetCols());//(same value)

		if(iteration == 1)
		{
			old_variance = fabs(pos_var);
		}
		else
		{
			if((fabs(pos_var)-fabs(old_variance))<limit_var) bStop = false;
			else old_variance = pos_var;
		}
		
		//if(fabs(pos_var) < limit_var) bStop = false;
		if(iteration > max_iter_num) bStop = false;
		
	} while(bStop);//end of do-while

	if(pos_var>limit_var) return false;
/*
	double dist1;
	double a_, b_, c_;
	
	double x, y, z;
	m_DEM.GetX(index_x,x); m_DEM.GetY(index_y,y); m_DEM.GetZ(index_x,index_y,z);
	
	a_ = a/(1+(a*x + b*y + c*z));
	b_ = b/(1+(a*x + b*y + c*z));
	c_ = c/(1+(a*x + b*y + c*z));
	
	dist1 = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
	
	double e = a*x + b*y + c*z+1;

	if(dist1>sqrt(max_dist)) return false;
	else return true;
*/
	return true;
}

bool CSMPseudoGridData::LSforCoeff(int index_x, int index_y, int index_z,
							  double &a, double &b, double &c, 
							  unsigned int max_iter_num,
							  double &pos_var)
{
	CSMMatrix<double> Bmat, Jmat, Kmat, Lmat;
	CSMMatrix<double> bmat, jmat, kmat, lmat;
	CSMMatrix<double> We;
	CSMMatrix<double> JT;
	CSMMatrix<double> X;
	CSMMatrix<double> Ve, V;

	jmat.Resize(1, 3);
	lmat.Resize(1, 1);
	
	unsigned int r_index=0;
	
	unsigned int iteration = 0;
	
	//Make J, L
	Jmat.Resize(0,0);
	Lmat.Resize(0,0);
	
	for(int i=-half_size_y; i<=half_size_y; i++)//Y direction
	{
		for(int j=-half_size_x; j<=half_size_x; j++)//X direction
		{
			for(int k=-half_size_z; k<=half_size_z; k++)//Z direction
			{
				if(((i+index_y)<0)||((i+index_y)>=height)) continue;
				if(((j+index_x)<0)||((j+index_x)>=width)) continue;
				if(((k+index_z)<0)||((k+index_z)>=depth)) continue;
				
				if(m_Grid[i+index_y][j+index_x][k+index_z].type != _IN_) continue;

				int num = m_Grid[i+index_y][j+index_x][k+index_z].point.GetNumItem();
				if(num>=1) num = 1;
				for(int count=0; count<num; count++)//each point in each grid
				{
					_Point_ p = m_Grid[i+index_y][j+index_x][k+index_z].point.GetAt(count);
					double X, Y, Z;
					X = p.X-oX; Y = p.Y-oY; Z = p.Z-oZ;
					
					Y = sY-(i+index_y)*oY-oY/2.;
					X = sX+(j+index_x)*oX+oX/2.;
					Z = sZ+(k+index_z)*oZ+oZ/2.;
					
					jmat(0,0) = X;
					jmat(0,1) = Y;
					jmat(0,2) = Z;
					
					lmat(0,0) = -1;
					
					Jmat.Insert(r_index,0,jmat);
					Lmat.Insert(r_index,0,lmat);
					
					r_index++;
				}//end of count
			}//end of k
			
		}//end of j
	}//end of i
	
	if(Jmat.GetRows()<4) return false;
	
	X = (Jmat.Transpose()%Jmat).Inverse()%Jmat.Transpose()%Lmat;
	V = Jmat%X - Lmat;
	pos_var = (V.Transpose()%V)(0,0)/(Jmat.GetRows()-Jmat.GetCols());
	
	a=X(0,0); b=X(1,0); c=X(2,0);

	return true;
}

bool CSMPseudoGridData::RobustLSforCoeff(int index_x, int index_y, int index_z,
							  double &a, double &b, double &c, 
							  unsigned int max_iter_num,
							  double &pos_var,
							  double res_threshold)
{	
	CSMList<_Point_> pointlist;
	double *errordist;
	double dist2origin;
	int num_points = 0;

	int i, j, k, count;

	for(i=-half_size_y; i<=half_size_y; i++)//Y direction
	{
		for(j=-half_size_x; j<=half_size_x; j++)//X direction
		{
			for(k=-half_size_z; k<=half_size_z; k++)//Z direction
			{
				if(((i+index_y)<0)||((i+index_y)>=height)) continue;
				if(((j+index_x)<0)||((j+index_x)>=width)) continue;
				if(((k+index_z)<0)||((k+index_z)>=depth)) continue;

				if(m_Grid[i+index_y][j+index_x][k+index_z].type != _IN_) continue;
				
				int num = m_Grid[i+index_y][j+index_x][k+index_z].point.GetNumItem();
				
				for(count=0; count<num; count++)//each point in each grid
				{
					_Point_ p = m_Grid[i+index_y][j+index_x][k+index_z].point.GetAt(count);
					pointlist.AddTail(p);
				}//end of count
			}//end of k
			
		}//end of j
	}//end of i


	num_points = pointlist.GetNumItem();
	errordist = new double[num_points];

	CSMPlaneFitting PF;

	if(true == PF.PlaneFitting(res_threshold, pointlist, a, b, c, pos_var, dist2origin, errordist, max_iter_num))
	{
		delete[] errordist;

		return true;
	}
	else
	{
		delete[] errordist;

		return false;
	}
}

bool CSMPseudoGridData::RobustLSforCoeff_Tree_old(double X, double Y, double Z, double BSX, double BSY, double BSZ, unsigned int max_iter_num, double res_threshold, double &a0, double &b0, double &c0)
{	
	CSMList<KDNode> pointlist;
	double low[3], high[3];
	
	low[0] = X - BSX;
	low[1] = Y - BSY;
	low[2] = Z - BSZ;

	high[0] = X + BSX;
	high[1] = Y + BSY;
	high[2] = Z + BSZ;

	tree.search(low, high, tree.Root, 0, pointlist);

	///////////////////////////
	//aX + bY + cZ + 1 = 0
	///////////////////////////
	int num_points = pointlist.GetNumItem();
	
	if(num_points < 4) return false;
	
	int i;
	
	CSMMatrix<double> Xmat;
	CSMMatrix<double> Nmat, Cmat;
	CSMMatrix<double> We(num_points, num_points, 0.0);
	We.Identity(1.0);
	
	int num_availpoints = num_points;
	
	double pos_var0 = 1.0e99;
	double pos_var = 1.0e99;
	double SD_NDError;
	double Sum_NDError2;
	bool bContinue = true;
	bool retval = true;
	
	double max_dist = 0.0, max_dist0 = 0.0;
	
	int num_iter = 0;
	
	_Mmat_ Rot(0, 0, 0);
	
	//
	//centroid
	//
	double sum_x=0, sum_y=0, sum_z=0;
	double cx, cy, cz;
	double *errordist = new double[num_points];
	double threshold = res_threshold;
	
	DATA<KDNode>* pos = NULL;
	
	for(i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		
		sum_x += pos->value.Get_x_(0);
		sum_y += pos->value.Get_x_(1);
		sum_z += pos->value.Get_x_(2);
	}
	
	cx = sum_x/num_points;
	cy = sum_y/num_points;
	cz = sum_z/num_points;
	
	num_availpoints = num_points;
	
	bool bIterNum = true;
	bool bNumAvailablePoints = true;
	
	cx = sum_x/num_availpoints;
	cy = sum_y/num_availpoints;
	cz = sum_z/num_availpoints;
	
	pos_var0 = pos_var;			
	
	max_dist = 0.0;
	
	Nmat.Resize(3,3,0);
	
	Cmat.Resize(3,1,0);
	
	CSMMatrix<double> A(num_points,3);
	CSMMatrix<double> L(num_points,1);
	CSMMatrix<double> Ni, Ci;
	
	CSMMatrix<double> P(3,1);
	
	CSMMatrix<double> W(1,1,0);
	
	CSMMatrix<double> etpe(1,1,0);
	
	pos = NULL;
	
	for(i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		
		P(0,0) = pos->value.Get_x_(0) - cx;
		P(1,0) = pos->value.Get_x_(1) - cy;
		P(2,0) = pos->value.Get_x_(2) - cz;
		
		//Rotate points
		P = Rot.Mmatrix%P;
		
		A(i, 0) = P(0,0) + 1.0;
		A(i, 1) = P(1,0) + 1.0;
		A(i, 2) = P(2,0) + 1.0;
		
		L(i, 0) = 1.0;
	}
	
	Nmat = A.Transpose()%A;
	Cmat = A.Transpose()%L;
	Xmat = Nmat.Inverse()%Cmat;

	CSMMatrix<double> V = A%Xmat - L;
	pos_var = sqrt((V.Transpose()%V)(0,0)/(A.GetRows()-3));
	
	double temp = 1-(Xmat(0,0)+Xmat(1,0)+Xmat(2,0));
	CSMMatrix<double> Xmat_ = Xmat/temp;
	
	double norm = sqrt(Xmat_(0,0)*Xmat_(0,0) + Xmat_(1,0)*Xmat_(1,0) + Xmat_(2,0)*Xmat_(2,0));
	a0 = Xmat_(0,0)/norm;
	b0 = Xmat_(1,0)/norm;
	c0 = Xmat_(2,0)/norm;
	
	double sd1 = sqrt(pos_var);
				
	pos = NULL;
	
	Sum_NDError2 = 0;
	
	for(i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		
		P(0,0) = pos->value.Get_x_(0) - cx;
		P(1,0) = pos->value.Get_x_(1) - cy;
		P(2,0) = pos->value.Get_x_(2) - cz;
		
		//Rotate points
		P = Rot.Mmatrix%P;
		
		//error distance (point to plane)
		errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0)-1;
		errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
		
		Sum_NDError2 += errordist[i]*errordist[i];
		
		if(max_dist<fabs(errordist[i])) max_dist = fabs(errordist[i]);
	}
	
	SD_NDError = sqrt(Sum_NDError2);
	
	//Update parameters
	pos = NULL;
	double* params;
	if(retval == true || SD_NDError <threshold)
	{
		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			params = pos->value.Contents;
			
			if(SD_NDError < params[3])
			{
				int id = pos->value.GetID();
				tree.List[id]->Contents[0] = a0;
				tree.List[id]->Contents[1] = b0;
				tree.List[id]->Contents[2] = c0;
				tree.List[id]->Contents[3] = SD_NDError;
			}
			
			errordist[i];
		}
	}

	delete[] errordist;
	
	return retval;
}

bool CSMPseudoGridData::RobustLSforCoeff_Tree_nonlinear(double X, double Y, double Z, double BSX, double BSY, double BSZ, unsigned int max_iter_num, double res_threshold)
{	
	/*
	CSMList<KDNode> pointlist;
	double low[3], high[3];
	
	low[0] = X - BSX;
	low[1] = Y - BSY;
	low[2] = Z - BSZ;

	high[0] = X + BSX;
	high[1] = Y + BSY;
	high[2] = Z + BSZ;

	tree.search(low, high, tree.Root, 0, pointlist);

	///////////////////////////
	//aX + bY + cZ + 1 = 0
	///////////////////////////
	int num_points = pointlist.GetNumItem();
	
	if(num_points < 4) return false;
	
	int i;
	
	CSMMatrix<double> Xmat;
	CSMMatrix<double> Nmat, Cmat;
	CSMMatrix<double> We(num_points, num_points, 0.0);
	We.Identity(1.0);
	
	int num_availpoints = num_points;
	
	double pos_var0 = 1.0e99;
	double pos_var = 1.0e99;
	double SD_NDError;
	double Sum_NDError2;
	bool bContinue = true;
	bool retval = true;
	
	double max_dist = 0.0, max_dist0 = 0.0;
	
	int num_iter = 0;
	
	_Mmat_ Rot(0, 0, 0);
	
	double a0=0, b0=0, c0=1;
	double a, b, c;
	
	//
	//centroid
	//
	double sum_x=0, sum_y=0, sum_z=0;
	double cx, cy, cz;
	double *errordist = new double[num_points];
	double threshold = res_threshold;
	
	DATA<KDNode>* pos = NULL;
	
	for(i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		
		sum_x += pos->value.Get_x_(0);
		sum_y += pos->value.Get_x_(1);
		sum_z += pos->value.Get_x_(2);
	}
	
	num_availpoints = num_points;
	
	bool bIterNum = true;
	bool bNumAvailablePoints = true;
	
	do
	{
		cx = sum_x/num_availpoints;
		cy = sum_y/num_availpoints;
		cz = sum_z/num_availpoints;

		pos_var0 = pos_var;			
		
		max_dist = 0.0;
		
		Nmat.Resize(3,3,0);
		
		Cmat.Resize(3,1,0);
		
		CSMMatrix<double> amat(1,3);
		CSMMatrix<double> lmat(1,1);
		CSMMatrix<double> Ni, Ci;
		
		CSMMatrix<double> P(3,1);
		
		CSMMatrix<double> W(1,1,0);
		
		CSMMatrix<double> etpe(1,1,0);
		
		pos = NULL;
		
		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			
			P(0,0) = pos->value.Get_x_(0) - cx;
			P(1,0) = pos->value.Get_x_(1) - cy;
			P(2,0) = pos->value.Get_x_(2) - cz;
			
			//Rotate points
			P = Rot.Mmatrix%P;
			
			amat(0,0) = P(0,0);
			amat(0,1) = P(1,0);
			amat(0,2) = P(2,0);
			
			//ax + by + cz = 0
			
			lmat(0,0) = -a0*P(0,0) -b0*P(1,0) -c0*P(2,0);
			
			CSMMatrix<double> we = We.Subset(i, i, 1, 1);
			W(0,0) += we(0,0);
			
			Ni = amat.Transpose()%we%amat;
			Ci = amat.Transpose()%we%lmat;
			etpe(0,0) += (lmat.Transpose()%we%lmat)(0,0);
			
			Nmat = Nmat + Ni;
			Cmat = Cmat + Ci;
		}
		
		amat(0,0) = 2*a0;
		amat(0,1) = 2*b0;
		amat(0,2) = 2*c0;
		
		lmat(0,0) = 1 - (a0*a0 + b0*b0 + c0*c0);
		
		Ni = amat.Transpose()%W%amat;
		Ci = amat.Transpose()%W%lmat;
		etpe(0,0) += (lmat.Transpose()%W%lmat)(0,0);
		
		Nmat = Nmat + Ni;
		Cmat = Cmat + Ci;
		
		Xmat = Nmat.Inverse()%Cmat;
		a0 = a0 + Xmat(0,0);
		b0 = b0 + Xmat(1,0);
		c0 = c0 + Xmat(2,0);
		
		pos_var = etpe(0,0)/(num_availpoints - 3);
		
		double sd1 = sqrt(pos_var);
		
		if(fabs(pos_var-pos_var0) < 1.0e-6)
		{
			bContinue = false;
			retval = true;
		}
		
		//ax+by+cz+d=0 where d is zero
		//Dist point2plane: fabs(ax1 + by1 + cz1 + d=0)/sqrt(a*a + b*b + c*c) 
		//                = fabs(ax1 + by1 + cz1 + d=0) because sqrt(a*a + b*b + c*c) = 1.0
		
		pos = NULL;
		
		num_availpoints = num_points;//reset num_available points
		
		//
		//Weight modification using studentized residual for robust LS
		//

		Sum_NDError2 = 0;

		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			
			P(0,0) = pos->value.Get_x_(0) - cx;
			P(1,0) = pos->value.Get_x_(1) - cy;
			P(2,0) = pos->value.Get_x_(2) - cz;
			
			//Rotate points
			P = Rot.Mmatrix%P;
			
			//error distance (point to plane)
			errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0);
			errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);

			Sum_NDError2 += errordist[i]*errordist[i];
			
			if(max_dist<fabs(errordist[i])) max_dist = fabs(errordist[i]);
		}

		SD_NDError = sqrt(Sum_NDError2);

		sum_x = sum_y = sum_z = 0;
		pos = NULL;

		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);

			CSMMatrix<double> P0(3,1);
			
			P0(0,0) = pos->value.Get_x_(0) - cx;
			P0(1,0) = pos->value.Get_x_(1) - cy;
			P0(2,0) = pos->value.Get_x_(2) - cz;
			
			//Rotate points
			P = Rot.Mmatrix%P0;
			
			//error distance (point to plane)
			errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0);
			errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
			
			if(fabs(errordist[i]) > SD_NDError)
			{
				We(i, i) = 0.0;
				num_availpoints --;
			}
			else
			{
				//We(i, i) = sd1/fabs(errordist[i]);//using studentized residuals
				We(i, i) = 1.0;//using studentized residuals

				sum_x += pos->value.Get_x_(0);
				sum_y += pos->value.Get_x_(1);
				sum_z += pos->value.Get_x_(2);
			}
		}
		
		if(num_availpoints < 4) 
		{
			bContinue = false;
			bNumAvailablePoints = false;
			retval = false;
		}
		
		if(fabs(max_dist - max_dist0) < 0.000001) 
		{
			bContinue = false;
			retval = true;
		}
		
		max_dist0 = max_dist;
		
		//normal vector and distance
		//ax + by + cz = 0
		
		CSMMatrix<double> norm(3,1);
		norm(0,0) = a0;
		norm(1,0) = b0;
		norm(2,0) = c0;
		
		//inverse-rotation for normal vector
		norm = Rot.Mmatrix.Transpose()%norm;
		
		//a, b, c in mapping frame
		a = norm(0,0);
		b = norm(1,0);
		c = norm(2,0);
		
		//rotation angle to the plane
		double dYZ = sqrt(b*b + c*c);
		double OofX = -atan2(b, c);
		double PofY = atan2(a, dYZ);
		
		Rot.ReMake(OofX, PofY, 0);
		
		if(num_iter >= max_iter_num)
		{
			bContinue = false;
			bIterNum = false;
			retval = false;
		}
		else
		{
			num_iter ++;
		}			
		
		}while(bContinue);
		
		//ax + by + cz + (-a*cx - b*cy - c*cz) = 0
		double dist2origin = fabs(-a*cx - b*cy - c*cz);
		
		//a'x + b'y + c'z + 1 = 0
		double temp = -a*cx - b*cy - c*cz;
		
		a = a/temp;
		b = b/temp;
		c = c/temp;

		//Update parameters
		pos = NULL;
		double* params;
		if(retval == true && SD_NDError <threshold)
		{
			for(i=0; i<num_points; i++)
			{
				pos = pointlist.GetNextPos(pos);
				params = pos->value.Contents;

				//if(errordist[i] < params[3])
				if(SD_NDError < params[3])
				{
					int id = pos->value.GetID();
					tree.List[id]->Contents[0] = a;
					tree.List[id]->Contents[1] = b;
					tree.List[id]->Contents[2] = c;
					//tree.List[id]->Contents[3] = errordist[i];
					tree.List[id]->Contents[3] = SD_NDError;
				}
				
				errordist[i];
			}
		}
		
		delete[] errordist;

		return retval;
*/

CSMList<KDNode> pointlist;
double low[3], high[3];

low[0] = X - BSX;
low[1] = Y - BSY;
low[2] = Z - BSZ;

high[0] = X + BSX;
high[1] = Y + BSY;
high[2] = Z + BSZ;

tree.search(low, high, tree.Root, 0, pointlist);

///////////////////////////
//aX + bY + c = z
///////////////////////////
int num_points = pointlist.GetNumItem();

if(num_points < 4) return false;

int i;

CSMMatrix<double> Xmat;
CSMMatrix<double> Nmat, Cmat;
CSMMatrix<double> We(num_points, num_points, 0.0);
We.Identity(1.0);

int num_availpoints = num_points;

double pos_var0 = 1.0e99;
double pos_var = 1.0e99;
double SD_NDError;
double Sum_NDError2;
bool bContinue = true;
bool retval = true;

double max_dist = 0.0, max_dist0 = 0.0;

unsigned int num_iter = 0;

_Mmat_ Rot(0, 0, 0);

double a0=0, b0=0, c0=1;
double a, b, c;

//
//centroid
//
double *errordist = new double[num_points];
double threshold = res_threshold;

DATA<KDNode>* pos = NULL;

num_availpoints = num_points;

bool bIterNum = true;
bool bNumAvailablePoints = true;

do
{
	pos_var0 = pos_var;			
	
	max_dist = 0.0;
	
	Nmat.Resize(3,3,0);
	
	Cmat.Resize(3,1,0);
	
	CSMMatrix<double> amat(1,3);
	CSMMatrix<double> lmat(1,1);
	CSMMatrix<double> Ni, Ci;
	
	CSMMatrix<double> P(3,1);
	
	CSMMatrix<double> W(1,1,0);
	
	pos = NULL;
	
	for(i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		
		P(0,0) = pos->value.Get_x_(0);
		P(1,0) = pos->value.Get_x_(1);
		P(2,0) = pos->value.Get_x_(2);
		
		//Rotate points
		P = Rot.Mmatrix%P;
		
		amat(0,0) = P(0,0);
		amat(0,1) = P(1,0);
		amat(0,2) = 1;
		
		//ax + by + c = z
		
		lmat(0,0) = P(2,0);
		
		CSMMatrix<double> we = We.Subset(i, i, 1, 1);
		W(0,0) += we(0,0);
		
		Ni = amat.Transpose()%we%amat;
		Ci = amat.Transpose()%we%lmat;
		
		Nmat = Nmat + Ni;
		Cmat = Cmat + Ci;
	}
	
	Nmat = Nmat + Ni;
	Cmat = Cmat + Ci;
	
	Xmat = Nmat.Inverse()%Cmat;
	a0 = Xmat(0,0);
	b0 = Xmat(1,0);
	c0 = Xmat(2,0);
	
	pos = NULL;
	
	num_availpoints = num_points;//reset num_available points
	
	//
	//Weight modification using studentized residual for robust LS
	//
	
	//ax+by-z=-c

	if(c0 == 0.0)
	{
		c0 = -1.0;		
	}
	else
	{
		a0 = a0/-c0;
		b0 = b0/-c0;
		c0 = -1.0/-c0;
	}

	Sum_NDError2 = 0;
	
	for(i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		
		P(0,0) = pos->value.Get_x_(0);
		P(1,0) = pos->value.Get_x_(1);
		P(2,0) = pos->value.Get_x_(2);
		
		//Rotate points
		P = Rot.Mmatrix%P;
		
		//error distance (point to plane)

		if(c0 == 0.0)
		{
			errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0);
			errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
		}
		else
		{
			errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0) -1.0;
			errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
		}
		
		Sum_NDError2 += errordist[i]*errordist[i];
		
		if(max_dist<fabs(errordist[i])) max_dist = fabs(errordist[i]);
	}
	
	SD_NDError = sqrt(Sum_NDError2);
	
	pos = NULL;
	
	for(i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		
		CSMMatrix<double> P0(3,1);
		
		P0(0,0) = pos->value.Get_x_(0);
		P0(1,0) = pos->value.Get_x_(1);
		P0(2,0) = pos->value.Get_x_(2);
		
		//Rotate points
		P = Rot.Mmatrix%P0;
		
		//error distance (point to plane)
		if(c0 == 0.0)
		{
			errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0);
			errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
		}
		else
		{
			errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0) -1.0;
			errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
		}
		
		if(fabs(errordist[i]) > SD_NDError)
		{
			We(i, i) = 0.0;
			num_availpoints --;
		}
		else
		{
			//We(i, i) = sd1/fabs(errordist[i]);//using studentized residuals
			We(i, i) = 1.0;//using studentized residuals
		}
	}
	
	if(num_availpoints < 4) 
	{
		bContinue = false;
		bNumAvailablePoints = false;
		retval = false;
	}
	
	if(fabs(max_dist - max_dist0) < 0.000001) 
	{
		bContinue = false;
		retval = true;
	}
	
	max_dist0 = max_dist;
	
	//normal vector and distance
	//ax + by + cz = 1
	
	CSMMatrix<double> norm(3,1);
	norm(0,0) = a0;
	norm(1,0) = b0;
	norm(2,0) = c0;
	
	//inverse-rotation for normal vector
	norm = Rot.Mmatrix.Transpose()%norm;
	
	//a, b, c in mapping frame
	a = norm(0,0);
	b = norm(1,0);
	c = norm(2,0);
	
	//rotation angle to the plane
	double dYZ = sqrt(b*b + c*c);
	double OofX = -atan2(b, c);
	double PofY = atan2(a, dYZ);
	
	Rot.ReMake(OofX, PofY, 0);
	
	if(num_iter >= max_iter_num)
	{
		bContinue = false;
		bIterNum = false;
		retval = false;
	}
	else
	{
		num_iter ++;
	}			
	
}while(bContinue);
		
		//Update parameters
		pos = NULL;
		double* params;
		if(retval == true && SD_NDError <threshold)
		{
			for(i=0; i<num_points; i++)
			{
				pos = pointlist.GetNextPos(pos);
				params = pos->value.Contents;
				
				//if(errordist[i] < params[3])
				if(SD_NDError < params[3])
				{
					int id = pos->value.GetID();
					tree.List[id]->Contents[0] = a;
					tree.List[id]->Contents[1] = b;
					tree.List[id]->Contents[2] = c;
					//tree.List[id]->Contents[3] = errordist[i];
					tree.List[id]->Contents[3] = SD_NDError;
				}
				
				errordist[i];
			}
		}
		
		delete[] errordist;
		
		return retval;
}

bool CSMPseudoGridData::RobustLSforCoeff_Tree(double X, double Y, double Z, double BSX, double BSY, double BSZ, unsigned int max_iter_num, double res_threshold, CSMList<KDNode> &pointlist)
{	
	double low[3], high[3];
	
	low[0] = X - BSX;
	low[1] = Y - BSY;
	low[2] = Z - BSZ;
	
	high[0] = X + BSX;
	high[1] = Y + BSY;
	high[2] = Z + BSZ;
	
	tree.search(low, high, tree.Root, 0, pointlist);
	
	///////////////////////////
	//aX + bY + c = z
	///////////////////////////
	int num_points = pointlist.GetNumItem();
	
	if(num_points < 4) return false;
	
	int i;
	
	CSMMatrix<double> Xmat;
	CSMMatrix<double> Nmat, Cmat;
	CSMMatrix<double> We(num_points, num_points, 0.0);
	We.Identity(1.0);
	
	int num_availpoints = num_points;
	
	double pos_var0 = 1.0e99;
	double pos_var = 1.0e99;
	double SD_NDError;
	double Sum_NDError2;
	bool bContinue = true;
	bool retval = true;
	
	double max_dist = 0.0, max_dist0 = 0.0;
	
	unsigned int num_iter = 0;
	
	_Mmat_ Rot(0, 0, 0);
	
	double a0=0, b0=0, c0=1;
	double a, b, c;
	
	//
	//centroid
	//
	double *errordist = new double[num_points];
	double threshold = res_threshold;
	
	DATA<KDNode>* pos = NULL;
	
	num_availpoints = num_points;
	
	bool bIterNum = true;
	bool bNumAvailablePoints = true;
	
	do
	{
		pos_var0 = pos_var;			
		
		max_dist = 0.0;
		
		Nmat.Resize(3,3,0);
		
		Cmat.Resize(3,1,0);
		
		CSMMatrix<double> amat(1,3);
		CSMMatrix<double> lmat(1,1);
		CSMMatrix<double> Ni, Ci;
		
		CSMMatrix<double> P(3,1);
		
		CSMMatrix<double> W(1,1,0);
		
		pos = NULL;
		
		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			
			P(0,0) = pos->value.Get_x_(0);
			P(1,0) = pos->value.Get_x_(1);
			P(2,0) = pos->value.Get_x_(2);
			
			//Rotate points
			P = Rot.Mmatrix%P;
			
			amat(0,0) = P(0,0);
			amat(0,1) = P(1,0);
			amat(0,2) = 1;
			
			//ax + by + c = z
			
			lmat(0,0) = P(2,0);
			
			CSMMatrix<double> we = We.Subset(i, i, 1, 1);
			W(0,0) += we(0,0);
			
			Ni = amat.Transpose()%we%amat;
			Ci = amat.Transpose()%we%lmat;
			
			Nmat = Nmat + Ni;
			Cmat = Cmat + Ci;
		}
		
		Nmat = Nmat + Ni;
		Cmat = Cmat + Ci;
		
		Xmat = Nmat.Inverse()%Cmat;
		a0 = Xmat(0,0);
		b0 = Xmat(1,0);
		c0 = Xmat(2,0);
		
		pos = NULL;
		
		num_availpoints = num_points;//reset num_available points
		
		//
		//Weight modification using studentized residual for robust LS
		//
		
		//ax+by-z=-c
		
		if(c0 == 0.0)
		{
			c0 = -1.0;		
		}
		else
		{
			a0 = a0/-c0;
			b0 = b0/-c0;
			c0 = -1.0/-c0;
		}
		
		Sum_NDError2 = 0;
		
		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			
			P(0,0) = pos->value.Get_x_(0);
			P(1,0) = pos->value.Get_x_(1);
			P(2,0) = pos->value.Get_x_(2);
			
			//Rotate points
			P = Rot.Mmatrix%P;
			
			//error distance (point to plane)
			
			if(c0 == 0.0)
			{
				errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0);
				errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
			}
			else
			{
				errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0) -1.0;
				errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
			}
			
			Sum_NDError2 += errordist[i]*errordist[i];
			
			if(max_dist<fabs(errordist[i])) max_dist = fabs(errordist[i]);
		}
		
		SD_NDError = sqrt(Sum_NDError2);
		
		pos = NULL;
		
		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			
			CSMMatrix<double> P0(3,1);
			
			P0(0,0) = pos->value.Get_x_(0);
			P0(1,0) = pos->value.Get_x_(1);
			P0(2,0) = pos->value.Get_x_(2);
			
			//Rotate points
			P = Rot.Mmatrix%P0;
			
			//error distance (point to plane)
			if(c0 == 0.0)
			{
				errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0);
				errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
			}
			else
			{
				errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0) -1.0;
				errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
			}
			
			if(fabs(errordist[i]) > SD_NDError)
			{
				We(i, i) = 0.0;
				num_availpoints --;
			}
			else
			{
				//We(i, i) = sd1/fabs(errordist[i]);//using studentized residuals
				We(i, i) = 1.0;//using studentized residuals
			}
		}
		
		if(num_availpoints < 4) 
		{
			bContinue = false;
			bNumAvailablePoints = false;
			retval = false;
		}
		
		if(fabs(max_dist - max_dist0) < 0.000001) 
		{
			bContinue = false;
			retval = true;
		}
		
		max_dist0 = max_dist;
		
		//normal vector and distance
		//ax + by + cz = 1
		
		CSMMatrix<double> norm(3,1);
		norm(0,0) = a0;
		norm(1,0) = b0;
		norm(2,0) = c0;
		
		//inverse-rotation for normal vector
		norm = Rot.Mmatrix.Transpose()%norm;
		
		//a, b, c in mapping frame
		a = norm(0,0);
		b = norm(1,0);
		c = norm(2,0);
		
		//rotation angle to the plane
		double dYZ = sqrt(b*b + c*c);
		double OofX = -atan2(b, c);
		double PofY = atan2(a, dYZ);
		
		Rot.ReMake(OofX, PofY, 0);
		
		if(num_iter >= max_iter_num)
		{
			bContinue = false;
			bIterNum = false;
			retval = false;
		}
		else
		{
			num_iter ++;
		}			
		
	}while(bContinue);

	//Update parameters
	pos = NULL;
	double* params;
	if(retval == true && SD_NDError <threshold)
	{
		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			params = pos->value.Contents;
			
			//if(errordist[i] < params[3])
			if(SD_NDError < params[3])
			{
				int id = pos->value.GetID();
				tree.List[id]->Contents[0] = a;
				tree.List[id]->Contents[1] = b;
				tree.List[id]->Contents[2] = c;
				//tree.List[id]->Contents[3] = errordist[i];
				tree.List[id]->Contents[3] = SD_NDError;
			}
			
			errordist[i];
		}
	}
	
	delete[] errordist;
	
	return retval;
}

bool CSMPseudoGridData::RobustLSforCoeff_Tree(CSMList<KDNode> pointlist, unsigned int max_iter_num, double res_threshold)
{	
	///////////////////////////
	//aX + bY + cZ + 1 = 0
	///////////////////////////
	int num_points = pointlist.GetNumItem();
	
	if(num_points < 4) return false;
	
	int i;
	
	CSMMatrix<double> Xmat;
	CSMMatrix<double> Nmat, Cmat;
	CSMMatrix<double> We(num_points, num_points, 0.0);
	We.Identity(1.0);
	
	int num_availpoints = num_points;
	
	double pos_var0 = 1.0e99;
	double pos_var = 1.0e99;
	double SD_NDError;
	double Sum_NDError2;
	bool bContinue = true;
	bool retval = true;
	
	double max_dist = 0.0, max_dist0 = 0.0;
	
	unsigned int num_iter = 0;
	
	_Mmat_ Rot(0, 0, 0);
	
	double a0=0, b0=0, c0=1;
	double a, b, c;
	
	//
	//centroid
	//
	double sum_x=0, sum_y=0, sum_z=0;
	double cx, cy, cz;
	double *errordist = new double[num_points];
	double threshold = res_threshold;
	
	DATA<KDNode>* pos = NULL;
	
	for(i=0; i<num_points; i++)
	{
		pos = pointlist.GetNextPos(pos);
		
		sum_x += pos->value.Get_x_(0);
		sum_y += pos->value.Get_x_(1);
		sum_z += pos->value.Get_x_(2);
	}
	
	cx = sum_x/num_points;
	cy = sum_y/num_points;
	cz = sum_z/num_points;
	
	num_availpoints = num_points;
	
	bool bIterNum = true;
	bool bNumAvailablePoints = true;
	
	do
	{
		cx = sum_x/num_availpoints;
		cy = sum_y/num_availpoints;
		cz = sum_z/num_availpoints;

		pos_var0 = pos_var;			
		
		max_dist = 0.0;
		
		Nmat.Resize(3,3,0);
		
		Cmat.Resize(3,1,0);
		
		CSMMatrix<double> amat(1,3);
		CSMMatrix<double> lmat(1,1);
		CSMMatrix<double> Ni, Ci;
		
		CSMMatrix<double> P(3,1);
		
		CSMMatrix<double> W(1,1,0);
		
		CSMMatrix<double> etpe(1,1,0);
		
		pos = NULL;
		
		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			
			P(0,0) = pos->value.Get_x_(0) - cx;
			P(1,0) = pos->value.Get_x_(1) - cy;
			P(2,0) = pos->value.Get_x_(2) - cz;
			
			//Rotate points
			P = Rot.Mmatrix%P;
			
			amat(0,0) = P(0,0);
			amat(0,1) = P(1,0);
			amat(0,2) = P(2,0);
			
			//ax + by + cz = 0
			
			lmat(0,0) = -a0*P(0,0) -b0*P(1,0) -c0*P(2,0);
			
			CSMMatrix<double> we = We.Subset(i, i, 1, 1);
			W(0,0) += we(0,0);
			
			Ni = amat.Transpose()%we%amat;
			Ci = amat.Transpose()%we%lmat;
			etpe(0,0) += (lmat.Transpose()%we%lmat)(0,0);
			
			Nmat = Nmat + Ni;
			Cmat = Cmat + Ci;
		}
		
		//a*a + b*b + c*c = 1.0
		amat(0,0) = 2*a0;
		amat(0,1) = 2*b0;
		amat(0,2) = 2*c0;
		
		lmat(0,0) = 1 - (a0*a0 + b0*b0 + c0*c0);
		
		Ni = amat.Transpose()%W%amat;
		Ci = amat.Transpose()%W%lmat;
		etpe(0,0) += (lmat.Transpose()%W%lmat)(0,0);
		
		Nmat = Nmat + Ni;
		Cmat = Cmat + Ci;
		
		Xmat = Nmat.Inverse()%Cmat;
		a0 = a0 + Xmat(0,0);
		b0 = b0 + Xmat(1,0);
		c0 = c0 + Xmat(2,0);
		
		pos_var = etpe(0,0)/(num_availpoints - 3);
		
		double sd1 = sqrt(pos_var);
		
		if(fabs(pos_var-pos_var0) < 1.0e-6)
		{
			bContinue = false;
			retval = true;
		}
		
		//ax+by+cz+d=0 where d is zero
		//Dist point2plane: fabs(ax1 + by1 + cz1 + d=0)/sqrt(a*a + b*b + c*c) 
		//                = fabs(ax1 + by1 + cz1 + d=0) because sqrt(a*a + b*b + c*c) = 1.0
		
		pos = NULL;
		
		num_availpoints = num_points;//reset num_available points
		
		//
		//Weight modification using studentized residual for robust LS
		//

		Sum_NDError2 = 0;

		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			
			P(0,0) = pos->value.Get_x_(0) - cx;
			P(1,0) = pos->value.Get_x_(1) - cy;
			P(2,0) = pos->value.Get_x_(2) - cz;
			
			//Rotate points
			P = Rot.Mmatrix%P;
			
			//error distance (point to plane)
			errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0);
			errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);

			Sum_NDError2 += errordist[i]*errordist[i];
			
			if(max_dist<fabs(errordist[i])) max_dist = fabs(errordist[i]);
		}

		SD_NDError = sqrt(Sum_NDError2);

		sum_x = sum_y = sum_z = 0;
		pos = NULL;

		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);

			CSMMatrix<double> P0(3,1);
			
			P0(0,0) = pos->value.Get_x_(0) - cx;
			P0(1,0) = pos->value.Get_x_(1) - cy;
			P0(2,0) = pos->value.Get_x_(2) - cz;
			
			//Rotate points
			P = Rot.Mmatrix%P0;
			
			//error distance (point to plane)
			errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0);
			errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);
			
			if(fabs(errordist[i]) > SD_NDError)
			{
				We(i, i) = 0.0;
				num_availpoints --;
			}
			else
			{
				//We(i, i) = sd1/fabs(errordist[i]);//using studentized residuals
				We(i, i) = 1.0;//using studentized residuals

				sum_x += pos->value.Get_x_(0);
				sum_y += pos->value.Get_x_(1);
				sum_z += pos->value.Get_x_(2);
			}
		}
		
		if(num_availpoints < 4) 
		{
			bContinue = false;
			bNumAvailablePoints = false;
			retval = false;
		}
		
		if(fabs(max_dist - max_dist0) < 0.000001) 
		{
			bContinue = false;
			retval = true;
		}
		
		max_dist0 = max_dist;
		
		//normal vector and distance
		//ax + by + cz = 0
		
		CSMMatrix<double> norm(3,1);
		norm(0,0) = a0;
		norm(1,0) = b0;
		norm(2,0) = c0;
		
		//inverse-rotation for normal vector
		norm = Rot.Mmatrix.Transpose()%norm;
		
		//a, b, c in mapping frame
		a = norm(0,0);
		b = norm(1,0);
		c = norm(2,0);
		
		//rotation angle to the plane
		double dYZ = sqrt(b*b + c*c);
		double OofX = -atan2(b, c);
		double PofY = atan2(a, dYZ);
		
		Rot.ReMake(OofX, PofY, 0);
		
		if(num_iter >= max_iter_num)
		{
			bContinue = false;
			bIterNum = false;
			retval = false;
		}
		else
		{
			num_iter ++;
		}			
		
		}while(bContinue);
		
		//ax + by + cz + (-a*cx - b*cy - c*cz) = 0
		double dist2origin = fabs(-a*cx - b*cy - c*cz);
		
		//a'x + b'y + c'z + 1 = 0
		double temp = -a*cx - b*cy - c*cz;
		
		a = a/temp;
		b = b/temp;
		c = c/temp;

		//Update parameters
		pos = NULL;
		double* params;
		if(retval == true && SD_NDError <threshold)
		{
			for(i=0; i<num_points; i++)
			{
				pos = pointlist.GetNextPos(pos);
				params = pos->value.Contents;

				//if(errordist[i] < params[3])
				if(SD_NDError < params[3])
				{
					int id = pos->value.GetID();
					tree.List[id]->Contents[0] = a;
					tree.List[id]->Contents[1] = b;
					tree.List[id]->Contents[2] = c;
					//tree.List[id]->Contents[3] = errordist[i];
					tree.List[id]->Contents[3] = SD_NDError;
				}
				
				errordist[i];
			}
		}
		
		delete[] errordist;

		return retval;
}

void CSMPseudoGridData::DistanceMapClassification_neighbor_old(CSMList<int> &num_element, int &index, int index_y, int index_x, int index_z, float Thresholdratio, _Accumulator_<int> &indexmap, _Accumulator_<float> &accu)
{

	float None_Plane = (float)none_plane;
	float Empty = (float)empty;
	float Ta, Tb, Tc, Tdist;
	Ta = accu(index_y, index_x, index_z, 0);
	Tb = accu(index_y, index_x, index_z, 1);
	Tc = accu(index_y, index_x, index_z, 2);
	Tdist = accu(index_y, index_x, index_z, 3);

	if((Ta==Empty)&&(Tb==Empty)&&(Tc==Empty))
	{
		indexmap(index_y, index_x, index_z) = -1;
		return;
	}
	
	if((Ta==None_Plane)&&(Tb==None_Plane)&&(Tc==None_Plane)) 
	{
		indexmap(index_y, index_x, index_z) = 0;
		return;
	}

	for(int i=-half_size_y; i<=half_size_y; i++)//Y direction
	{
		for(int j=-half_size_x; j<=half_size_x; j++)//X direction
		{
			for(int k=-half_size_z; k<=half_size_z; k++)//Z direction
			{
				if(((i+index_y)<0)||((i+index_y)>=height)) continue;
				if(((j+index_x)<0)||((j+index_x)>=width)) continue;
				if(((k+index_z)<0)||((k+index_z)>=depth)) continue;

				if(((i+index_y)==index_y)&&((j+index_x)==index_x)&&((k+index_z)==index_z)) continue;//target cell
				
				if(indexmap(i+index_y, j+index_x, k+index_z)>0) continue;//already determined cell

				if(m_Grid[i+index_y][j+index_x][k+index_z].type != _IN_) continue;//_IN_ == 0 (if a grid cell is not _IN_, the cell is already classified or out of concern.)

				float a , b, c, dist;
				a = accu(i+index_y, j+index_x, k+index_z, 0);
				b = accu(i+index_y, j+index_x, k+index_z, 1);
				c = accu(i+index_y, j+index_x, k+index_z, 2);
				dist = accu(i+index_y, j+index_x, k+index_z, 3);

				if((a==Empty)&&(b==Empty)&&(c==Empty)) continue;
				if((a==None_Plane)&&(b==None_Plane)&&(c==None_Plane)) continue;
				
				//if((fabs((Ta-a)/range_a)<fabs(Tdist*Thresholdratio))&&(fabs((Tb-b)/range_b)<fabs(Tdist*Thresholdratio))&&(fabs((Tc-c)/range_c)<fabs(Tdist*Thresholdratio))) 
				if((fabs((Ta-a)/range_a)<fabs(Thresholdratio))&&(fabs((Tb-b)/range_b)<fabs(Thresholdratio))&&(fabs((Tc-c)/range_c)<fabs(Thresholdratio))) 
				//if((fabs(Ta-a)<fabs(Thresholdratio))&&(fabs(Tb-b)<fabs(Thresholdratio))&&(fabs(Tc-c)<fabs(Thresholdratio))) 
				{
					if(indexmap(index_y, index_x, index_z) == 0)
					{
						indexmap(index_y, index_x, index_z) = index;
						
						num_element.AddTail(1);

						index++;
					}

					indexmap(i+index_y, j+index_x, k+index_z) = indexmap(index_y, index_x, index_z);
					
					int num = num_element.GetAt(indexmap(index_y, index_x, index_z));
					
					num_element.SetAt(indexmap(index_y, index_x, index_z),num+1);

					DistanceMapClassification_neighbor_old(num_element, index, i+index_y, j+index_x, k+index_z, Thresholdratio, indexmap, accu);
				}
			}//end of k
			
		}//end of j
	}//end of i
}

void CSMPseudoGridData::DistanceMapClassification_old(float Thresholdratio, _Accumulator_<float> &accu, CString outpath, int min_num_points, CString strProgressBar, int ObjectType)
{
	int i, j, k;
	
	//accumulator 메모리 할당
	_Accumulator_<int> indexmap;
	indexmap.SetMemory(height, width, depth, 1);

	//초기화.
	for(i=0; i<height; i++)
		for(j=0; j<width; j++)
			for(k=0; k<depth; k++)
			{
				indexmap(i, j, k, 0) = 0;
			}

	CSMList<int> num_element;
	num_element.AddTail(0);

	CProgressBar bar(strProgressBar, 100, height*width);

	int index = 1;//index: start from 1
	
	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			for(k=0; k<depth; k++)
			{
				DistanceMapClassification_neighbor_old(num_element, index, i, j, k, Thresholdratio, indexmap, accu);
			}
			bar.StepIt();
		}
		
	}
	
	CString result_path = outpath;
	result_path.Replace(".txt","_result.cls");

	CString result_path_only_plane = outpath;
	result_path_only_plane.Replace(".txt","_result_only_plane.cls");

	CString result_path_only_false = outpath;
	result_path_only_false.Replace(".txt","_result_only_false.cls");
	
	fstream result_file; result_file.open(result_path,ios::out);
	fstream result_file_plane; result_file_plane.open(result_path_only_plane,ios::out);
	fstream result_file_false; result_file_false.open(result_path_only_false,ios::out);
	
	for(int count=0; count<index; count++)
	{
		int nR = rand()%255;
		int nG = rand()%255;
		int nB = rand()%255;

		nPlaneIndex ++;
		
		int num_points = num_element.GetAt(count);
		
		//if(count == 0) {nR = 0; nG = 0; nB = 0;}

		if(num_points<min_num_points) {nR = 0; nG = 0; nB = 0;}

		for(i=0; i<height; i++)//Y direction
		{
			for(j=0; j<width; j++)//X direction
			{
				for(k=0; k<depth; k++)//Z direction
				{
					if(count == indexmap(i, j, k))
					{	
						for(int num=0; num<(int)(m_Grid[i][j][k].point.GetNumItem()); num++)
						{
							if(m_Grid[i][j][k].type != _IN_) continue;

							m_Grid[i][j][k].PlaneID = nPlaneIndex;

							_Point_ p = m_Grid[i][j][k].point.GetAt(num);
							
							if(bNormalize == true)
							{
								p.X += ShiftX; p.Y += ShiftY; p.Z += ShiftZ;
							}
							
							result_file.precision(6);
							result_file.setf(ios::fixed, ios::floatfield);
							result_file<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
							result_file<<setw(10)<<nR<<"\t"<<setw(10)<<nG<<"\t"<<setw(10)<<nB<<endl;
							result_file.flush();

							if((nR!=0)&&(nG!=0)&&(nB!=0))
							{
								result_file_plane.precision(6);
								result_file_plane.setf(ios::fixed, ios::floatfield);
								result_file_plane<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
								result_file_plane<<setw(10)<<nR<<"\t"<<setw(10)<<nG<<"\t"<<setw(10)<<nB<<endl;
								result_file_plane.flush();

								m_Grid[i][j][k].type = ObjectType;
							}
							else
							{
								float a, b, c;
								a = accu(i, j, k, 0);
								b = accu(i, j, k, 1);
								c = accu(i, j, k, 2);

								result_file_false.precision(6);
								result_file_false.setf(ios::fixed, ios::floatfield);
								result_file_false<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
								result_file_false<<setw(10)<<a<<"\t"<<setw(10)<<b<<"\t"<<setw(10)<<c<<endl;
								result_file_false.flush();
							}
						}
					}
				}
			}
		}
	}
	
	result_file.close();
	result_file_plane.close();
	result_file_false.close();
}

void CSMPseudoGridData::DistanceMapClassification_neighbor(CSMList<int> &class_list, int &index, int index_y, int index_x, int index_z, float Thresholdratio, _Accumulator_<int> &indexmap, _Accumulator_<float> &accu, float Ta, float Tb, float Tc, float Tdist)
{
	for(int i=-half_size_y; i<=half_size_y; i++)//Y direction
	{
		for(int j=-half_size_x; j<=half_size_x; j++)//X direction
		{
			for(int k=-half_size_z; k<=half_size_z; k++)//Z direction
			{
				//check boundaries
				if(((i+index_y)<0)||((i+index_y)>=height)) continue;
				if(((j+index_x)<0)||((j+index_x)>=width)) continue;
				if(((k+index_z)<0)||((k+index_z)>=depth)) continue;
				
				if(((i+index_y)==index_y)&&((j+index_x)==index_x)&&((k+index_z)==index_z)) continue;//skip the target cell (center cell)
				
				if(m_Grid[i+index_y][j+index_x][k+index_z].type != _IN_) continue;//_IN_ == 0 (if a grid cell is not _IN_, the cell is already classified or out of concern.)

				float a , b, c, dist;
				a = accu(i+index_y, j+index_x, k+index_z, 0);
				b = accu(i+index_y, j+index_x, k+index_z, 1);
				c = accu(i+index_y, j+index_x, k+index_z, 2);
				dist = accu(i+index_y, j+index_x, k+index_z, 3);
				
				//skip un-available cells
				if((a==(float)empty)&&(b==(float)empty)&&(c==(float)empty)) continue;
				if((a==(float)none_plane)&&(b==(float)none_plane)&&(c==(float)none_plane)) continue;

				//
				//Check parameters for classification
				//
				//if((fabs((Ta-a)/range_a)<fabs(Tdist*Thresholdratio))&&(fabs((Tb-b)/range_b)<fabs(Tdist*Thresholdratio))&&(fabs((Tc-c)/range_c)<fabs(Tdist*Thresholdratio))) 
				//if((fabs((Ta-a)/range_a)<fabs(Thresholdratio))&&(fabs((Tb-b)/range_b)<fabs(Thresholdratio))&&(fabs((Tc-c)/range_c)<fabs(Thresholdratio))) 
				//if((fabs(Ta-a)<fabs(Thresholdratio))&&(fabs(Tb-b)<fabs(Thresholdratio))&&(fabs(Tc-c)<fabs(Thresholdratio))) 
				if((fabs((Ta-a)/255)<fabs(Thresholdratio))&&(fabs((Tb-b)/255)<fabs(Thresholdratio))&&(fabs((Tc-c)/255)<fabs(Thresholdratio))) 
				{
					int neighbor_idx = indexmap(i+index_y, j+index_x, k+index_z);

					int num_classes = (int)class_list.GetNumItem();

					if(neighbor_idx > 0) //already classified cell
					{
						if(num_classes == 0)//Even the target cell was not classified
						{
							indexmap(index_y, index_x, index_z) = neighbor_idx;

							class_list.AddTail(neighbor_idx);
						}
						else
						{
							bool bNewIDX = true;
							for(int iClass=0; iClass<num_classes; iClass++)
							{
								if(neighbor_idx == class_list.GetAt(iClass))
								{
									bNewIDX = false;
									break;
								}
							}
							
							//Add a new idx code
							if(bNewIDX == true)
								class_list.AddTail(neighbor_idx);
						}
					}
					else //Not classified cell
					{
						if(num_classes == 0)//Even the target cell was not classified
						{
							//set a new class
							indexmap(i+index_y, j+index_x, k+index_z) = indexmap(index_y, index_x, index_z) = index;

							class_list.AddTail(index);

							//increase the index
							index ++;
						}
						else
						{
							indexmap(i+index_y, j+index_x, k+index_z) = indexmap(index_y, index_x, index_z);
						}
					}
				}
			}//end of k
			
		}//end of j
	}//end of i
}

void CSMPseudoGridData::DistanceMapClassification(float Thresholdratio, _Accumulator_<float> &accu, CString outpath, int min_num_points, CString strProgressBar, int ObjectType)
{
	int i, j, k;
	
	//accumulator 메모리 할당
	_Accumulator_<int> indexmap;
	indexmap.SetMemory(height, width, depth, 1);

	//초기화.
	for(i=0; i<height; i++)
		for(j=0; j<width; j++)
			for(k=0; k<depth; k++)
			{
				indexmap(i, j, k, 0) = 0;//Initialize indexmap for zero
			}

	CProgressBar bar(strProgressBar, 100, height*width);

	int index = 1;//index: start from 1
	
	for(i=0; i<height; i++)
	{
		for(j=0; j<width; j++)
		{
			for(k=0; k<depth; k++)
			{
				float Ta, Tb, Tc, Tdist;
				
				Ta = accu(i, j, k, 0);
				Tb = accu(i, j, k, 1);
				Tc = accu(i, j, k, 2);
				Tdist = accu(i, j, k, 3);
				
				if((Ta==(float)empty)&&(Tb==(float)empty)&&(Tc==(float)empty))
				{
					indexmap(i, j, k) = -1;//empty cell
					continue;
				}
				else if((Ta==(float)none_plane)&&(Tb==(float)none_plane)&&(Tc==(float)none_plane)) 
				{
					indexmap(i, j, k) = 0;//non-plane
					continue;
				}

				//Already classified
				if(indexmap(i, j, k) > 0)
					continue;

				CSMList<int> class_list;
				DistanceMapClassification_neighbor(class_list, index, i, j, k, Thresholdratio, indexmap, accu, Ta, Tb, Tc, Tdist);

				if(class_list.GetNumItem() > 1)
				{
					ReAssignClassCode(class_list, indexmap);
				}
			}
			bar.StepIt();
		}
		
	}
	
	CString result_path = outpath;
	result_path.Replace(".txt","_result.cls");

	CString result_path_only_plane = outpath;
	result_path_only_plane.Replace(".txt","_result_only_plane.cls");

	CString result_path_only_false = outpath;
	result_path_only_false.Replace(".txt","_result_only_false.cls");
	
	fstream result_file; result_file.open(result_path,ios::out);
	fstream result_file_plane; result_file_plane.open(result_path_only_plane,ios::out);
	fstream result_file_false; result_file_false.open(result_path_only_false,ios::out);

	CSMList<int> class_list;
	CSMList<int> class_pt_num_list;
	CSMList<int> nR_list;
	CSMList<int> nG_list;
	CSMList<int> nB_list;
	srand((unsigned int)time(NULL));

	int nR, nG, nB;

	for(i=0; i<height; i++)//Y direction
	{
		for(int j=0; j<width; j++)//X direction
		{
			for(int k=0; k<depth; k++)//Z direction
			{
				int index = indexmap(i, j, k);

				if(index <= 0) continue;

				if(class_list.GetNumItem() > 0)
				{
					bool bEqual = false;
					for(int n = 0; n<(int)class_list.GetNumItem(); n++)
					{
						if(class_list.GetAt(n) == index)
						{
							bEqual = true;
							nPlaneIndex = n+1;							
							break;
						}
					}
					
					if(bEqual == false)
					{
						nR = rand()%255;
						nG = rand()%255;
						nB = rand()%255;
						
						nR_list.AddTail(nR);
						nG_list.AddTail(nG);
						nB_list.AddTail(nB);
						
						class_list.AddTail(index);
						
						nPlaneIndex = class_list.GetNumItem();
						
						class_pt_num_list.AddTail(0);
					}
				}
				else
				{
					nPlaneIndex = 1;

					nR = rand()%255;
					nG = rand()%255;
					nB = rand()%255;
					
					nR_list.AddTail(nR);
					nG_list.AddTail(nG);
					nB_list.AddTail(nB);

					class_list.AddTail(index);

					class_pt_num_list.AddTail(0);
				}

				indexmap(i, j, k) = nPlaneIndex;//Re-arrange indexmap

				//count number of points in a class
				class_pt_num_list.SetAt(nPlaneIndex-1, class_pt_num_list.GetAt(nPlaneIndex-1) + m_Grid[i][j][k].point.GetNumItem());
			}
		}
	}

	for(i=0; i<height; i++)//Y direction
	{
		for(int j=0; j<width; j++)//X direction
		{
			for(int k=0; k<depth; k++)//Z direction
			{
				int index = indexmap(i, j, k);

				bool bPlane = true;
				bool bSmallPlane = false;

				if(index <= 0) 
				{
					bPlane = false;
				}
				else
				{					
					if(class_pt_num_list.GetAt(index-1) < min_num_points)
					{
						bSmallPlane = true;
					}
				}

				for(int num=0; num<(int)(m_Grid[i][j][k].point.GetNumItem()); num++)
				{
					if(m_Grid[i][j][k].type != _IN_) continue;
					
					m_Grid[i][j][k].PlaneID = nPlaneIndex;
					
					_Point_ p = m_Grid[i][j][k].point.GetAt(num);
					
					if(bNormalize == true)
					{
						p.X += ShiftX; p.Y += ShiftY; p.Z += ShiftZ;
					}
					
					result_file.precision(6);
					result_file.setf(ios::fixed, ios::floatfield);
					result_file<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
					result_file<<setw(10)<<nR<<"\t"<<setw(10)<<nG<<"\t"<<setw(10)<<nB<<endl;
					result_file.flush();
					
					if(bPlane == true && bSmallPlane == false)
					{
						result_file_plane.precision(6);
						result_file_plane.setf(ios::fixed, ios::floatfield);
						result_file_plane<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
						result_file_plane<<setw(10)<<nR_list.GetAt(index-1)<<"\t"<<setw(10)<<nG_list.GetAt(index-1)<<"\t"<<setw(10)<<nB_list.GetAt(index-1)<<endl;
						result_file_plane.flush();
						
						m_Grid[i][j][k].type = ObjectType;
					}
					else if(bPlane == true && bSmallPlane == true)
					{
						result_file_plane.precision(6);
						result_file_plane.setf(ios::fixed, ios::floatfield);
						result_file_plane<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
						result_file_plane<<setw(10)<<0<<"\t"<<setw(10)<<0<<"\t"<<setw(10)<<0<<endl;
						result_file_plane.flush();
					}
					else
					{
						//float a, b, c;
						//a = accu(i, j, k, 0);
						//b = accu(i, j, k, 1);
						//c = accu(i, j, k, 2);
						
						result_file_false.precision(6);
						result_file_false.setf(ios::fixed, ios::floatfield);
						result_file_false<<setw(10)<<p.X<<"\t"<<setw(10)<<p.Y<<"\t"<<setw(10)<<p.Z<<"\t";
						result_file_false<<setw(10)<<0<<"\t"<<setw(10)<<0<<"\t"<<setw(10)<<0<<endl;
						result_file_false.flush();
						
					}
				}
			}
		}
	}

	result_file.close();
	result_file_plane.close();
	result_file_false.close();
}

void CSMPseudoGridData::ReAssignClassCode(CSMList<int> &class_list, _Accumulator_<int> &indexmap)
{
	int num_classes = class_list.GetNumItem();
	int index = class_list.GetAt(0);
	
	for(int i=0; i<height; i++)
	{
		for(int j=0; j<width; j++)
		{
			for(int k=0; k<depth; k++)
			{
				int idx = indexmap(i, j, k);
				bool bEqual = false;
				
				for(int iclass=1; iclass<num_classes; iclass++)
				{
					if(idx == class_list.GetAt(iclass))
					{
						bEqual = true;
						break;
					}
				}

				if(bEqual == true)
					indexmap(i, j, k) = index;
			}
		}
	}
}

void CSMPseudoGridData::UpdateDistance(_Accumulator_<float> &accu, int index_y, int index_x, int index_z, double a, double b, double c)
{
	//vector and distance
	//(l, m, n)에 수직이고, 원점에서 거리가 P인 평면.
	//lx + my + nz = p
	
	//ax + by + cz + 1 = 0
	double dist;
	double abc = sqrt(a*a + b*b + c*c);
	dist = 1.0/abc;
	
	//평면의 방향 코사인
	double l = -a*dist;
	double m = -b*dist;
	double n = -c*dist;
	double A, B, C;		
	
	A = l*dist; B = m*dist; C = n*dist;
				
	for(int i=-half_size_y; i<=half_size_y; i++)//Y direction
	{
		for(int j=-half_size_x; j<=half_size_x; j++)//X direction
		{
			for(int k=-half_size_z; k<=half_size_z; k++)//Z direction
			{
				if(((i+index_y)<0)||((i+index_y)>=height)) continue;
				if(((j+index_x)<0)||((j+index_x)>=width)) continue;
				if(((k+index_z)<0)||((k+index_z)>=depth)) continue;
				
				if(m_Grid[i+index_y][j+index_x][k+index_z].type != _IN_) continue;
				
				int num = m_Grid[i+index_y][j+index_x][k+index_z].point.GetNumItem();
				if(num>=1)
				{
					double dist1;
					double X, Y, Z;
					Y = sY-(i+index_y)*oY-oY/2.;
					X = sX+(j+index_x)*oX+oX/2.;
					Z = sZ+(k+index_z)*oZ+oZ/2.;
					
					double sqrtabc = sqrt(a*a + b*b + c*c);
					dist1 = fabs(a*X + b*Y + c*Z + 1)/sqrtabc;
					
					if(dist1<accu(i+index_y, j+index_x, k+index_z, 3))
					{
						accu(i+index_y, j+index_x, k+index_z, 0) = (float)A;
						accu(i+index_y, j+index_x, k+index_z, 1) = (float)B;
						accu(i+index_y, j+index_x, k+index_z, 2) = (float)C;
						accu(i+index_y, j+index_x, k+index_z, 3) = (float)dist1;
					}
				}
				
			}//end of k
			
		}//end of j
	}//end of i
}

void CSMPseudoGridData::GroundPointsRemove(CString outpath, double offsetZ, double heightthreshold)
{
	CProgressBar bar("Ground Removing...", 100, width*(int)(offsetZ/oZ+0.5));

	CSMList<int> GroundList;
	CSMList<int> RoofList;
		
	for(int k=0; k<(int)(offsetZ/oZ+0.5); k++)//Z direction
	{
		for(int i=0; i<height; i++)//Y direction
		{
			for(int j=0; j<width; j++)//X direction
			{
				if((m_Grid[i][j][k].type != _GROUND_)&&(m_Grid[i][j][k].type == _IN_))
				{
					m_Grid[i][j][k].type = _GROUND_;
					int ret = GroundSearch(GroundList, (int)5, (int)5, (int)(heightthreshold/oZ + 0.5), i, j, k);
				}
			}
			
			bar.StepIt();
		}
		
	}

	CString nonegroundpath = outpath; nonegroundpath.MakeLower(); nonegroundpath.Replace(".txt", "_none_ground.cls");
	fstream resultwithoutground; resultwithoutground.open(nonegroundpath,ios::out);
	
	resultwithoutground.precision(6);
	resultwithoutground.setf(ios::fixed, ios::floatfield);
	
	for(int i=0; i<height; i++)//Y direction
	{
		for(int j=0; j<width; j++)//X direction
		{
			for(int k=0; k<depth; k++)//Z direction
			{
				
				if(m_Grid[i][j][k].type != _GROUND_)
				{
					int numP = m_Grid[i][j][k].point.GetNumItem();
					for(int n=0; n<numP; n++)
					{
						_Point_ P = m_Grid[i][j][k].point.GetAt(n);
						resultwithoutground<<setw(10)<<P.X<<"\t"<<setw(10)<<P.Y<<"\t"<<setw(10)<<P.Z<<"\t";
						resultwithoutground<<setw(10)<<0<<"\t"<<setw(10)<<0<<"\t"<<setw(10)<<255<<endl;
						resultwithoutground.flush();
					}
				}
				else
				{
					int numP = m_Grid[i][j][k].point.GetNumItem();
					for(int n=0; n<numP; n++)
					{
						_Point_ P = m_Grid[i][j][k].point.GetAt(n);
						resultwithoutground<<setw(10)<<P.X<<"\t"<<setw(10)<<P.Y<<"\t"<<setw(10)<<P.Z<<"\t";
						resultwithoutground<<setw(10)<<255<<"\t"<<setw(10)<<0<<"\t"<<setw(10)<<0<<endl;
						resultwithoutground.flush();
					}
				}
			}
		}
	}	
	
	resultwithoutground.close();
}

int CSMPseudoGridData::GroundSearch(CSMList<int> &GList, int half_size_y, int half_size_x, int half_size_z, int index_x, int index_y, int index_z)
{
	for(int i=-half_size_y; i<=half_size_y; i++)//Y direction
	{
		for(int j=-half_size_x; j<=half_size_x; j++)//X direction
		{
			for(int k=-half_size_z; k<=half_size_z; k++)//Z direction
			{
				if(((i+index_y)<0)||((i+index_y)>=height)) continue;
				if(((j+index_x)<0)||((j+index_x)>=width)) continue;
				if(((k+index_z)<0)||((k+index_z)>=depth)) continue;
				if(((i+index_y)==index_y)&&((j+index_x)==index_x)&&((k+index_z)==index_z)) continue;
				
				if(m_Grid[i+index_y][j+index_x][k+index_z].type == _IN_)
				{
					if(m_Grid[i+index_y][j+index_x][k+index_z].point.GetNumItem() > 1)
					{
						m_Grid[i+index_y][j+index_x][k+index_z].type = _GROUND_;

						GList.AddTail(m_Grid[i+index_y][j+index_x][k+index_z].PlaneID);
						
						GroundSearch(GList, half_size_y, half_size_x, half_size_z, i, j, k);
					}
					
				}

			}//end of k
			
		}//end of j
	}//end of i

	return -1;
}

void CSMPseudoGridData::ReadPTS(CString ptsfile)
{
	DeleteMemory();
	m_PointsCloud.ReadPTS(ptsfile);
}

bool CSMPseudoGridData::PlaneFittingForRoofs_old(CString pointsfilepath, double BS_x, double BS_y, double BS_z, unsigned int max_iter_num, CString outpath, double threshold)
{
	CString outpath_result = outpath;
	outpath_result += ".config";
	fstream resultfile;
	resultfile.open(outpath_result,ios::out);

	double center_x, center_y, center_z;

	ReadTXTPoints_Tree(pointsfilepath, center_x, center_y, center_z);

	resultfile.precision(3);
	resultfile.setf(ios::fixed, ios::floatfield);

	resultfile<<"---Input data--- "<<endl;
	resultfile<<pointsfilepath<<endl<<endl;

	resultfile<<"---Output data--- "<<endl;
	resultfile<<outpath<<endl<<endl;
	resultfile<<"---Buffer size---"<<endl;
	resultfile<<BS_x<<"m\t"<<BS_y<<"m\t"<<BS_z<<"m"<<endl;
	resultfile<<"---Max iteration (plane fitting procedure)---"<<endl;
	resultfile<<max_iter_num<<endl;
	resultfile<<"---Centroid---"<<endl;
	resultfile<<center_x<<"\t"<<center_y<<"\t"<<center_z<<endl;
	
	min_a = min_b = min_c = (float)+3.402823466e+30;//3.402823466 E + 38: float maximum value
	max_a = max_b = max_c = (float)-3.402823466e+30;
	
	int num_p = tree.nList;

	CProgressBar bar("Plane fitting...", 100, num_p);

	bar.SetRange(1, num_p);

	for(int i=0; i<num_p; i++)
	{
		double X, Y, Z;
		X = tree.List[i]->Get_x_(0);
		Y = tree.List[i]->Get_x_(1);
		Z = tree.List[i]->Get_x_(2);

		double a, b, c;
		RobustLSforCoeff_Tree_old(X, Y, Z, BS_x, BS_y, BS_z, max_iter_num, threshold, a, b, c);

		//tree.List[i]->Contents[0]=a;
		//tree.List[i]->Contents[1]=b;
		//tree.List[i]->Contents[2]=c;

		bar.StepIt();
	}

	double min_a=1.0e99, max_a=-1.0e99;
	double min_b=1.0e99, max_b=-1.0e99;
	double min_c=1.0e99, max_c=-1.0e99;
	double min_dist=1.0e99, max_dist=-1.0e99;

	for(int i=0; i<num_p; i++)
	{
		double a, b, c, errordist;
		a = tree.List[i]->Contents[0];
		b = tree.List[i]->Contents[1];
		c = tree.List[i]->Contents[2];
		errordist = tree.List[i]->Contents[3];
		
		if((a==(float)empty)||(b==(float)empty)||(c==(float)empty))
			continue;

		if(a<min_a) min_a = a; if(a>max_a) max_a = a;
		if(b<min_b) min_b = b; if(b>max_b) max_b = b;
		if(c<min_c) min_c = c; if(c>max_c) max_c = c;
	}

	double range_a, range_b, range_c, range_dist;

	range_a = max_a - min_a;
	range_b = max_b - min_b;
	range_c = max_c - min_c;
	range_dist = max_dist - min_dist;

	fstream param_file;
	param_file.open(outpath,ios::out);
	
	fstream cls_file;
	CString outpath_cls = outpath;
	outpath_cls += ".cls";
	cls_file.open(outpath_cls,ios::out);
	
	param_file.precision(6);
	param_file.setf(ios::fixed, ios::floatfield);
	
	cls_file.precision(6);
	cls_file.setf(ios::fixed, ios::floatfield);

	CProgressBar bar2("Preparing output...", 100, num_p);
	
	bar2.SetRange(1, num_p);

	for(int i=0; i<num_p; i++)
	{
		double X, Y, Z;
		X = tree.List[i]->Get_x_(0);
		Y = tree.List[i]->Get_x_(1);
		Z = tree.List[i]->Get_x_(2);

		double a, b, c, errordist;
		a = tree.List[i]->Contents[0];
		b = tree.List[i]->Contents[1];
		c = tree.List[i]->Contents[2];
		errordist = tree.List[i]->Contents[3];

		if((a==(float)empty)||(b==(float)empty)||(c==(float)empty))
			continue;

		double azimuth = Rad2Deg(atan2(a, b));
		double Hrz_Dist = sqrt(a*a + b*b);
		double slope = Rad2Deg(atan2(c, Hrz_Dist));
		double Dist = 1.0/sqrt(Hrz_Dist*Hrz_Dist + c*c);
		
		tree.List[i]->Contents[0] = (float)azimuth;
		tree.List[i]->Contents[1] = (float)slope;
		tree.List[i]->Contents[3] = (float)Dist;

		int aI, bI, cI;
				
		aI = (int)((a - min_a)/range_a*255 +0.5);
		bI = (int)((b - min_b)/range_b*255 +0.5);
		cI = (int)((c - min_c)/range_c*255 +0.5);

		param_file<<setw(10)<<X+center_x<<"\t"<<setw(10)<<Y+center_y<<"\t"<<setw(10)<<Z+center_z<<"\t";
		param_file<<setw(10)<<azimuth<<"\t"<<setw(10)<<slope<<"\t"<<setw(10)<<Dist<<"\t"<<setw(10)<<errordist<<endl;
		param_file.flush();
		
		cls_file<<setw(10)<<X+center_x<<"\t"<<setw(10)<<Y+center_y<<"\t"<<setw(10)<<Z+center_z<<"\t";
		cls_file<<setw(10)<<aI<<"\t"<<setw(10)<<bI<<"\t"<<setw(10)<<cI<<endl;
		cls_file.flush();

		bar2.StepIt();
	}

	resultfile.close();
	param_file.close();
	cls_file.close();

	return true;
}

bool CSMPseudoGridData::PlaneFittingForRoofs(CString pointsfilepath, double BS_x, double BS_y, double BS_z, unsigned int max_iter_num, CString outpath, double threshold)
{
	CString outpath_result = outpath;
	outpath_result += ".config";
	fstream resultfile;
	resultfile.open(outpath_result,ios::out);

	fstream logfile;
	CString logfile_path = outpath;
	logfile_path += ".log";
	logfile.open(logfile_path,ios::out);

	double center_x, center_y, center_z;

	ReadTXTPoints_Tree(pointsfilepath, center_x, center_y, center_z);

	resultfile.precision(3);
	resultfile.setf(ios::fixed, ios::floatfield);

	resultfile<<"---Input data--- "<<endl;
	resultfile<<pointsfilepath<<endl<<endl;

	resultfile<<"---Output data--- "<<endl;
	resultfile<<outpath<<endl<<endl;
	resultfile<<"---Buffer size---"<<endl;
	resultfile<<BS_x<<"m\t"<<BS_y<<"m\t"<<BS_z<<"m"<<endl;
	resultfile<<"---Max iteration (plane fitting procedure)---"<<endl;
	resultfile<<max_iter_num<<endl;
	resultfile<<"---Centroid---"<<endl;
	resultfile<<center_x<<"\t"<<center_y<<"\t"<<center_z<<endl;
	resultfile<<"---Plane fitting threshold [m]---"<<endl;
	resultfile<<threshold<<endl;
	
	min_a = min_b = min_c = (float)+3.402823466e+30;//3.402823466 E + 38: float maximum value
	max_a = max_b = max_c = (float)-3.402823466e+30;
	
	int num_p = tree.nList;

	CProgressBar bar("Plane fitting...", 100, num_p);

	bar.SetRange(1, num_p);

	for(int i=0; i<num_p; i++)
	{
		double X, Y, Z;
		X = tree.List[i]->Get_x_(0);
		Y = tree.List[i]->Get_x_(1);
		Z = tree.List[i]->Get_x_(2);

		CSMList<KDNode> pointlist;

		if( true == RobustLSforCoeff_Tree(X, Y, Z, BS_x, BS_y, BS_z, max_iter_num, threshold, pointlist) )
			logfile<<i<<"\t"<<"true"<<endl;
		else
		{
			logfile<<i<<"\t"<<"false"<<endl;

			int num_pts = pointlist.GetNumItem();
			for(int i=0; i<num_pts; i++)
			{
				KDNode node = pointlist.GetAt(i);
				logfile<<node.Get_x_(0)<<"\t"<<node.Get_x_(1)<<"\t"<<node.Get_x_(2)<<endl;
			}
		}

		logfile.flush();

		bar.StepIt();
	}

	double Min_A=1.0e99, Max_A=-1.0e99;
	double Min_B=1.0e99, Max_B=-1.0e99;
	double Min_C=1.0e99, Max_C=-1.0e99;

	for(int i=0; i<num_p; i++)
	{
		double a, b, c, errordist;
		a = tree.List[i]->Contents[0];
		b = tree.List[i]->Contents[1];
		c = tree.List[i]->Contents[2];
		errordist = tree.List[i]->Contents[3];
		
		if((a==(float)empty)||(b==(float)empty)||(c==(float)empty))
			continue;
		
		double Dist = 1.0/sqrt(a*a + b*b + c*c);	
		double A, B, C;
		
		A = -a*Dist;
		B = -b*Dist;
		C = -c*Dist;

		if(A<Min_A) Min_A = A; if(A>Max_A) Max_A = A;
		if(B<Min_B) Min_B = B; if(B>Max_B) Max_B = B;
		if(C<Min_C) Min_C = C; if(C>Max_C) Max_C = C;
	}

	double Range_A = Max_A - Min_A;
	double Range_B = Max_B - Min_B;
	double Range_C = Max_C - Min_C;

	fstream param_file;
	param_file.open(outpath,ios::out);
	
	fstream cls_file_plane;
	fstream cls_file_nonplane;
	CString outpath_cls_plane = outpath;
	outpath_cls_plane += ".plane.cls";
	cls_file_plane.open(outpath_cls_plane,ios::out);

	CString outpath_cls_nonplane = outpath;
	outpath_cls_nonplane += ".non-plane.cls";
	cls_file_nonplane.open(outpath_cls_nonplane,ios::out);
	
	param_file.precision(6);
	param_file.setf(ios::fixed, ios::floatfield);
	
	cls_file_plane.precision(6);
	cls_file_plane.setf(ios::fixed, ios::floatfield);

	cls_file_nonplane.precision(6);
	cls_file_nonplane.setf(ios::fixed, ios::floatfield);

	CProgressBar bar2("Preparing output...", 100, num_p);
	
	bar2.SetRange(1, num_p);

	for(int i=0; i<num_p; i++)
	{
		double X, Y, Z;
		X = tree.List[i]->Get_x_(0);
		Y = tree.List[i]->Get_x_(1);
		Z = tree.List[i]->Get_x_(2);

		double a, b, c, errordist;
		a = tree.List[i]->Contents[0];
		b = tree.List[i]->Contents[1];
		c = tree.List[i]->Contents[2];
		errordist = tree.List[i]->Contents[3];

		if((a==(float)empty)||(b==(float)empty)||(c==(float)empty))
		{
			param_file<<setw(10)<<X+center_x<<"\t"<<setw(10)<<Y+center_y<<"\t"<<setw(10)<<Z+center_z<<"\t";
			param_file<<setw(10)<<empty<<"\t"<<setw(10)<<empty<<"\t"<<setw(10)<<empty<<"\t"<<setw(10)<<empty<<endl;
			param_file.flush();
			
			cls_file_nonplane<<setw(10)<<X+center_x<<"\t"<<setw(10)<<Y+center_y<<"\t"<<setw(10)<<Z+center_z<<"\t";
			cls_file_nonplane<<setw(10)<<0<<"\t"<<setw(10)<<0<<"\t"<<setw(10)<<0<<endl;
			cls_file_nonplane.flush();

			continue;
		}

		//double azimuth = Rad2Deg(atan2(a, b));
		double Hrz_Dist = sqrt(a*a + b*b);
		//double slope = Rad2Deg(atan2(c, Hrz_Dist));
		double Dist = 1.0/sqrt(Hrz_Dist*Hrz_Dist + c*c);
		
		int aI, bI, cI;
		//azimuth: -180 ~ 180
		//slope: -90~90
		
		//aI = (int)(((int)(azimuth + 180 + 0.5)%180)*255/180 + 0.5);
		//bI = (int)(fabs(slope)*255/90 + 0.5);
		//cI = (int)(Dist/0.1+0.5)%255;//100cm quantization

		double A, B, C;
		//double Dist2 = Dist*Dist;
		
		A = -a*Dist;
		B = -b*Dist;
		C = -c*Dist;
		
		aI = (int)(fabs(A - Min_A)/Range_A*255 + 0.5);
		bI = (int)(fabs(B - Min_B)/Range_B*255 + 0.5);
		cI = (int)(fabs(C - Min_C)/Range_C*255 + 0.5);

		//aI = (int)(A/0.001)%255;
		//bI = (int)(B/0.001)%255;
		//cI = (int)(C/0.001)%255;

		param_file<<setw(10)<<X+center_x<<"\t"<<setw(10)<<Y+center_y<<"\t"<<setw(10)<<Z+center_z<<"\t";
		param_file<<setw(10)<<A<<"\t"<<setw(10)<<B<<"\t"<<setw(10)<<C<<"\t"<<setw(10)<<Dist<<"\t"<<setw(10)<<errordist<<endl;
		param_file.flush();
		
		cls_file_plane<<setw(10)<<X+center_x<<"\t"<<setw(10)<<Y+center_y<<"\t"<<setw(10)<<Z+center_z<<"\t";
		cls_file_plane<<setw(10)<<aI<<"\t"<<setw(10)<<bI<<"\t"<<setw(10)<<cI<<endl;
		cls_file_plane.flush();

		bar2.StepIt();
	}

	//resultfile<<"Quantization (azimuth): 180/255 deg"<<endl;
	//resultfile<<"Quantization (slope): 90/255 deg"<<endl;
	//resultfile<<"Quantization (distance): 0.1 m"<<endl;

	resultfile.close();
	param_file.close();
	cls_file_plane.close();
	cls_file_nonplane.close();
	logfile.close();

	return true;
}

void CSMPseudoGridData::NormalVectorClassification(CString norvectorfile, CString outpath, float Thresholdratio, float buffer_size_x, float buffer_size_y, float buffer_size_z, int min_num_points)
{
	CString class_file_path = outpath;
	fstream class_file;
	class_file.open(class_file_path,ios::out);

	class_file.precision(3);
	class_file.setf(ios::fixed, ios::floatfield);

	CString outpath_result = outpath;
	outpath_result += ".config";
	fstream resultfile;
	resultfile.open(outpath_result,ios::out);
	
	resultfile.precision(3);
	resultfile.setf(ios::fixed, ios::floatfield);
	
	resultfile<<"---Input data--- "<<endl;
	resultfile<<norvectorfile<<endl<<endl;	
	resultfile<<"---Output data--- "<<endl;
	resultfile<<outpath<<endl<<endl;
	resultfile<<"---Buffer size---"<<endl;
	resultfile<<buffer_size_x<<"m\t"<<buffer_size_y<<"m\t"<<buffer_size_z<<"m"<<endl;
	resultfile<<"---Min number of points---"<<endl;
	resultfile<<min_num_points<<endl;

	CSMList<IdxGroup> groups;

	double center_x, center_y, center_z;

	ReadCLSPoints_Tree(norvectorfile, center_x, center_y, center_z);

	int num_pts = tree.nList;

	CProgressBar bar("Clustering", 100, num_pts);

	double buffer[3] = {buffer_size_x, buffer_size_y, buffer_size_z};

	for(int i=0; i<num_pts; i++)
	{
		double* contents;
		contents = tree.List[i]->Contents;

		//non-plane points
		if((contents[0]==0)&&(contents[1]==0)&&(contents[2]==0))
		{
			tree.List[i]->Contents[3] = -1;
			continue;
		}
		else if(contents[3] != 0)//Already classified or false point
			continue;

		int grp_index = groups.GetNumItem() + 1;
		IdxGroup group;
		group.idx_list.AddTail(i);

		tree.List[i]->Contents[3] = grp_index;		

		bool bContinue = true;

		do 
		{
			int num_points = group.idx_list.GetNumItem();
			
			CSMList<int> class_list;

			for(int j=0; j<num_points; j++)
			{
				NormalVectorClassification_neighbor(grp_index, class_list, group.idx_list.GetAt(j), buffer, Thresholdratio);
			}

			if(class_list.GetNumItem() == 0)
				bContinue = false;
			else
				group.idx_list += class_list;

		} while (bContinue);

		groups.AddTail(group);

		bar.StepIt();
	}

	CString result_path_plane = outpath;
	result_path_plane.Replace(".txt",".planes.cls");
	
	CString result_path_false = outpath;
	result_path_false.Replace(".txt",".false.cls");

	fstream result_file_plane; result_file_plane.open(result_path_plane,ios::out);
	fstream result_file_false; result_file_false.open(result_path_false,ios::out);

	result_file_false.precision(6);
	result_file_false.setf(ios::fixed, ios::floatfield);

	result_file_plane.precision(6);
	result_file_plane.setf(ios::fixed, ios::floatfield);

	srand((unsigned int)time(NULL));

	int num_grps = groups.GetNumItem();

	for(int i=0; i<num_grps; i++)
	{
		int num_points = groups.GetAt(i).idx_list.GetNumItem();

		class_file<<"class #"<<i<<endl;

		if(num_points < min_num_points)
		{
			for(int j=0; j<num_points; j++)
			{
				int idx = groups.GetAt(i).idx_list.GetAt(j);
				
				double X = tree.List[idx]->Get_x_(0) + center_x;
				double Y = tree.List[idx]->Get_x_(1) + center_y;
				double Z = tree.List[idx]->Get_x_(2) + center_z;

				int R = (int)tree.List[idx]->Contents[0];
				int G = (int)tree.List[idx]->Contents[1];
				int B = (int)tree.List[idx]->Contents[2];
				
				result_file_false<<setw(10)<<X<<"\t"<<setw(10)<<Y<<"\t"<<setw(10)<<Z<<"\t";
				result_file_false<<setw(10)<<0<<"\t"<<setw(10)<<0<<"\t"<<setw(10)<<0<<endl;
				result_file_false.flush();

				class_file<<setw(10)<<X<<"\t"<<setw(10)<<Y<<"\t"<<setw(10)<<Z<<"\t";
				class_file<<setw(10)<<R<<"\t"<<setw(10)<<G<<"\t"<<setw(10)<<B<<endl;
				class_file.flush();
			}
		}
		else
		{
			int R, G, B;

			R = rand()%255;
			G = rand()%255;
			B = rand()%255;

			for(int j=0; j<num_points; j++)
			{
				int idx = groups.GetAt(i).idx_list.GetAt(j);
				
				double X = tree.List[idx]->Get_x_(0) + center_x;
				double Y = tree.List[idx]->Get_x_(1) + center_y;
				double Z = tree.List[idx]->Get_x_(2) + center_z;

				result_file_plane<<setw(10)<<X<<"\t"<<setw(10)<<Y<<"\t"<<setw(10)<<Z<<"\t";
				result_file_plane<<setw(10)<<R<<"\t"<<setw(10)<<G<<"\t"<<setw(10)<<B<<endl;
				result_file_plane.flush();

				class_file<<setw(10)<<X<<"\t"<<setw(10)<<Y<<"\t"<<setw(10)<<Z<<"\t";
				class_file<<setw(10)<<R<<"\t"<<setw(10)<<G<<"\t"<<setw(10)<<B<<endl;
				class_file.flush();
			}
		}
	}
	
	result_file_plane.close();
	result_file_false.close();
	resultfile.close();
}

void CSMPseudoGridData::NormalVectorClassification_neighbor(int grp_index, CSMList<int> &class_list, int Pindex, double buffer[3], float Thresholdratio)
{
	double low[3], high[3];
	
	KDNode* P = tree.List[Pindex];
	
	double X, Y, Z;
	X = P->Get_x_(0);
	Y = P->Get_x_(1);
	Z = P->Get_x_(2);

	double* Pcontents = P->Contents;

	low[0] = X - buffer[0];
	low[1] = Y - buffer[1];
	low[2] = Z - buffer[2];

	high[0] = X + buffer[0];
	high[1] = Y + buffer[1];
	high[2] = Z + buffer[2];

	CSMList<KDNode> points;
	tree.search(low, high, tree.Root, 0, points);
	int npoints = points.GetNumItem();

	for(int i=0; i<npoints; i++)
	{
		KDNode Q = points[i];
		int Qindex = Q.GetID();
		double* Qcontents = Q.Contents;

		if(Qcontents[0]==0 && Qcontents[1]==0 && Qcontents[2]==0)//Non-planar points
		{
			Qcontents[3] = -1;
			continue;
		}
		else if(Qcontents[3] != 0)
		{
			continue;
		}
		else if((fabs((Pcontents[0]-Qcontents[0])/255)<fabs(Thresholdratio))&&
			    (fabs((Pcontents[1]-Qcontents[1])/255)<fabs(Thresholdratio))&&
				(fabs((Pcontents[2]-Qcontents[2])/255)<fabs(Thresholdratio)))
		{
			tree.List[Qindex]->Contents[3] = grp_index;
			class_list.AddTail(Qindex);
		}
	}//end of i
}
