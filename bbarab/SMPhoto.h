// SMPhoto.h: interface for the CSSMPhoto class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SMPHOTO_H__25E5A57C_0302_4BEA_925E_90BD348CAF7F__INCLUDED_)
#define AFX_SMPHOTO_H__25E5A57C_0302_4BEA_925E_90BD348CAF7F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SSMPoint.h"
#include "SMList.h"
#include "SMMatrix.h"
using namespace SMATICS_BBARAB;

class CSSMPhoto  
{
public:
	CSSMPhoto();
	virtual ~CSSMPhoto();
public:
	CSMStr ID;
	CSMStr Camera_ID;
	double X0, Y0, Z0;
	double Omega0, Phi0, Kappa0;
	CSMMatrix<double> dispersion;
	CSMList<CSSMPoint> image_points;

};

#endif // !defined(AFX_SMPHOTO_H__25E5A57C_0302_4BEA_925E_90BD348CAF7F__INCLUDED_)
