// AffineEpipolar.h: interface for the AffineEpipolar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AFFINEEPIPOLAR_H__0F064BD1_69FF_4C74_8524_B482D33100E3__INCLUDED_)
#define AFX_AFFINEEPIPOLAR_H__0F064BD1_69FF_4C74_8524_B482D33100E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
//														
//	AffineEpipolar Class
//	(AffineEpipolar.h, AffineEpipolar.cpp)							
//													
//	inha univ.										
//	spacematics lab.									
//	Lee Young Jin									
//	 2002. 6.17 -  7.5						
//													
//////////////////////////////////////////////////////////////////////

#include "Matrix.h"
#include "이미지관련/Image.h"
 
struct Point2D  
{
	double x;
	double y;
};

struct GCP
{
	char ID[5];
	double x;
	double y;
	double z;
};

class AffineEpipolar  
{
public:
	AffineEpipolar(CString infilename);
	virtual ~AffineEpipolar();

private:
	//Data Reading
	CString LeftImageName, RightImageName;
	CString LeftEpipolarName, RightEpipolarName;

	double c_um;	//Principal Distance(um)
	double c;		//Principal Distance(pixel);

	double CCD_Size;
	double Left_Omega, Right_Omega;		//radian

	int Number_of_Observations;

	Point2D *LeftCoord;
	Point2D *RightCoord;	
	Point2D *LeftCoord_Temp;
	Point2D *RightCoord_Temp;

	GCP *ControlPoint;		

	double Left_A[8];	//왼쪽 영상의 Affine Parameters
	double Right_A[8];	//오른쪽 영상의 Affine Parameters

	double B[8];

	double C[4];
	double C_[4];

	double K[6];		//오른쪽 영상에서 왼쪽 영상으로 가는 Affine Paramenters
	double K_[6];		//왼쪽 영상에서 오른쪽 영상으로 가는 Affine Paramenters	

	CImage Left_Original_Image;
	CImage Right_Original_Image;

	CImage Left_Epipolar_Image;
	CImage Right_Epipolar_Image;

	double Left_half_Row,  Left_half_Col;	//좌표변환을 위한 변수
	double Right_half_Row, Right_half_Col;

	Point2D Left_N_Translation;		//Epipolar image의 translation
	Point2D Right_N_Translation;	

	int Left_Epipolar_Width,  Left_Epipolar_Height;
	int Right_Epipolar_Width, Right_Epipolar_Height;
	int Epipolar_Width,	Epipolar_Height;

	double Left_angle, Right_angle;	 

	double V_diff;
	 
private:
	BOOL ReadData(CString infilename);
	BOOL ReadImage();

	BOOL Coord2Temp();

	BOOL All_Image2Photo();		//입력받은 좌표를 사진좌표로 변환
	BOOL MakeAffineImage_ScanLine();

	BOOL GetEpipolarDirection(Point2D *Left, Point2D *Right);

	BOOL MakeAffineImage_EpipolarLine();
	BOOL GetK(Point2D *Left,Point2D* Right);	
	
	BOOL ProjectLeftImageToVirtualPlane();

	double Make_Parallel_Projection(double yp,double omega);	//Scanline 방향 y좌표 변환함수
	BOOL Coordinate_Print(CString outfilename,Point2D *Left, Point2D *Right);

	BOOL RotationAlongEpipolarLine();

	BOOL Y_Parallax_Print(CString outfilename,Point2D *Left, Point2D *Right);

	BOOL GetEpipolarImageSize();
	BOOL ResamplingEpipolarImage(LPCTSTR LeftEpipolarName,LPCTSTR RightEpipolarName);	
	BOOL AlignEpipolarImage();

	//왼쪽 에피폴라 영상 제작 
	BOOL GetLeftEpipolarImageSize();
	BOOL ReSampling_LeftImage(LPCTSTR LeftEpipolarName);

	//오른쪽 에피폴라 영상 제작
	BOOL GetRightEpipolarImageSize();
	BOOL ReSampling_RightImage(LPCTSTR RightEpipolarName);

	//좌표변환에 사용되는 함수들
	Point2D Affine_Left2Right(Point2D Left);
	Point2D Affine_Right2Left(Point2D Right);
	
	Point2D Left_Image2Photo(double x,double y);
	Point2D	Left_Image2Photo(Point2D Image);
	Point2D	Left_Photo2Image(Point2D Photo);
	Point2D Left_Direct_Rotation(Point2D photo);
	Point2D Left_Indirect_Rotation(Point2D rotated_photo);
	Point2D Left_Epipolar_Image2Photo(double x,double y);
	Point2D Left_Epipolar_Photo2Image(Point2D photo);

	Point2D	Right_Image2Photo(double x,double y);	
	Point2D	Right_Image2Photo(Point2D Image);	
	Point2D	Right_Photo2Image(Point2D Photo);	
	Point2D Right_Direct_Rotation(Point2D photo);
	Point2D Right_Indirect_Rotation(Point2D rotated_photo);
	Point2D Right_Epipolar_Image2Photo(double x,double y);
	Point2D Right_Epipolar_Photo2Image(Point2D photo);			


	//에피폴라 라인을 그리는 함수
//	BOOL DrawLeftEpipolarLine();

	//여색입체시 제작에 필요한 값을 리턴하는 함수
public:
	int GetHorizontalDifference();
};

#endif // !defined(AFX_AFFINEEPIPOLAR_H__0F064BD1_69FF_4C74_8524_B482D33100E3__INCLUDED_)
