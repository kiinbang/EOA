/*
 * Copyright (c) 1999-2007, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// SSMCamera.h: interface for the CSSMCamera class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SSMCAMERA_H__74157B80_8E3F_4A79_BF1A_1C1EB18BBF34__INCLUDED_)
#define AFX_SSMCAMERA_H__74157B80_8E3F_4A79_BF1A_1C1EB18BBF34__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMMatrix.h"

#define NUM_CAMERA_PARAM 3

using namespace SMATICS_BBARAB;

class CSSMCamera  
{
public:
	CSSMCamera();
	virtual ~CSSMCamera();
public:
	double m_c;//principal distance
	double m_xp;
	double m_yp;
	char m_ID[512];
	CSMMatrix<double> m_Dispersion;
	double k0, k1, k2, k3;
	double p1, p2, p3;

	void SetSMACParameters(double k[4], double p[3]);

	void GetSMACDistortion(double x, double y, double &delta_x, double &delta_y);
};

#endif // !defined(AFX_SSMCAMERA_H__74157B80_8E3F_4A79_BF1A_1C1EB18BBF34__INCLUDED_)
