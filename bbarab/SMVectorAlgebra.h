#pragma once

#include "SMQuaternion.h"

using namespace SSMATICS_EOA;

/**
*@class CSMVector Class
*@brief 3D vector class.
*/
template<class T>
class CSMVector
{
public:
	CSMVector() { mMat.Resize(mDim, 1, 0.0); }

	CSMVector(const T x, const T y, const T z)
	{
		mVec[0] = x; mVec[1] = y; mVec[2] = z;
	}

	/**copy constructor*/
	CSMVector(const CSMVector<T>& copy)
	{
		operator = (copy);
	}
	
	/**Assignmnet operator*/
	void operator = (const CSMVector<T>& copy)
	{
		for (unsigned int i = 0; i < mDim; ++i)
			mVec[i] = copy.mVec[i];
	}

	/**Convert to quaternion*/
	CSMQuaternion toQuaternion()
	{
		CSMQuaternion retval(this->mVec[0], this->mVec[1], this->mVec[2]);
		return retval;
	}

	CSMVector operator + (const CSMVector<T>& copy) const
	{
		CSMVector retval(*this);
		for (unsigned int i = 0; i < mDim; ++i)
		{
			retval.mVec[i] += copy.mVec[i];
		}
		return retval;
	}

	void operator += (const CSMVector<T>& copy) const
	{
		for (unsigned int i = 0; i < mDim; ++i)
		{
			mVec[i] += copy.mVec[i];
		}
	}

	CSMVector operator - (const CSMVector<T>& copy) const
	{
		CSMVector retval(*this);
		for (unsigned int i = 0; i < mDim; ++i)
		{
			retval.mVec[i] -= copy.mVec[i];
		}
		return retval;
	}

	void operator -= (const CSMVector<T>& copy) const
	{
		for (unsigned int i = 0; i < mDim; ++i)
		{
			mVec[i] -= copy.mVec[i];
		}
	}
	
	/**operator overloading for dot product*/
	T operator * (const CSMVector<T>& copy) const
	{
		return dot(copy);
	}
	
	/**Dot product*/
	T dot(const CSMVector<T>& copy) const
	{
		T retval = T(0);
		for (unsigned int i = 0; i < mDim; ++i)
		{
			retval += mVec[i] * copy.mVec[i];
		}

		return retval;
	}

	CSMVector operator * (const T val) const
	{
		CSMVector retval(*this);
		for (unsigned int i = 0; i < mDim; ++i)
		{
			retval.mVec[i] *= val;
		}
		return retval;
	}

	void operator *= (const T val)
	{
		for (unsigned int i = 0; i < 3; ++i)
		{
			mVec[i] *= val;
		}
	}

	CSMVector operator / (const T val)
	{
		CSMVector retval(*this);
		for (unsigned int i = 0; i < 3; ++i)
		{
			retval.mVec[i] /= val;
		}
		return retval;
	}

	void operator /= (const T val)
	{
		for (unsigned int i = 0; i < mDim; ++i)
		{
			mVec[i] /= val;
		}
	}	

	CSMVector<T> operator % (const CSMVector<T>& copy)
	{
		return cross(copy);
	}

	CSMVector<T> cross(const CSMVector<T>& copy)
	{
		CSMVector<T> retval;
		retval.Resize(mDim, 1);
		retval(0, 0) = mVec[1] * copy.mVec[2] - mVec[2] * copy.mVec[1];
		retval(1, 0) = mVec[2] * copy.mVec[0] - mVec[0] * copy.mVec[2];
		retval(2, 0) = mVec[0] * copy.mVec[1] - mVec[1] * copy.mVec[0];
		return retval;
	}

	T& operator [] (const unsigned int i)
	{
		if (i >= mDim) return T(0);
		return mVec[i];
	}

	static CSMVector Reflection(const CSMVector<T>& i0, const CSMVector<T>& normalVec0)
	{
		//r = i - 2(i.n)n;

		CSMVector<T> i(i0);
		CSMVector<T> n(normalVec0);
				
		T iLength = i.length();
		if (iLength != T(1))
			i /= iLength;

		T nLength = n.length();
		if (nLength != T(1))
			n /= nLength;

		return ( i - n*( T(2)*(i*n) ) );
	}

	T length() const
	{
		T sum2 = 0.;
		for (unsigned int i = 0; i < 3; ++i)
			sum2 += mVec[i] * mVec[i];
		return sqrt(sum2);
	}

private:
	T mVec[3];
	const unsigned int mDim = 3;
};