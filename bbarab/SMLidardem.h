/*
 * Copyright (c) 2001-2002, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(_SPACEMATICS_BINARY_DEM)
#define _SPACEMATICS_BINARY_DEM

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSMDEM
class CSMDEM
{
//member function
public:
	CSMDEM();
	CSMDEM(CSMDEM &copy);
	virtual ~CSMDEM();
	CSMDEM& operator=(CSMDEM &copy);
	CSMDEM& Copy(CSMDEM &copy);
	
	/**ReadLIDARDEM
	* Description	    : 
	* Return type		: bool 
	*/
	virtual bool ReadLIDARDEM();
	
	/**FindIndex
	* Description	    : 
	* Return type		: DWORD 
	*@param DWORD x
	*@param DWORD y
	*/
	virtual DWORD FindIndex(DWORD x, DWORD y);
	
	/**GetDEMValue
	* Description	    : 
	* Return type		: short 
	*@param DWORD x
	*@param DWORD y
	*/
	virtual short GetDEMValue(DWORD x, DWORD y);

	/**GetDEMValue
	* Description	    : Get X, Y and Z using index x and index y
	* Return type		: bool 
	*@param DWORD x
	*@param DWORD y
	*@param double &X
	*@param double &Y
	*@param double &Z
	*/
	bool GetXYZ(DWORD x, DWORD y, double &X, double &Y, double &Z);
	
	/**GetZ
	* Description	    : 
	* Return type		: double 
	*@param DWORD x
	*@param DWORD y
	*/
	virtual double GetZ(DWORD x, DWORD y);
	
	/**GetZ
	* Description	    : 
	* Return type		: bool 
	*@param DWORD x
	*@param DWORD y
	*@param double &value
	*/
	virtual bool GetZ(DWORD x, DWORD y, double &value);
	
	/**GetX
	* Description	    : 
	* Return type		: double 
	*@param DWORD x
	*/
	virtual double GetX(DWORD x);
	
	/**GetX
	* Description	    : 
	* Return type		: bool 
	*@param DWORD x
	*@param double &value
	*/
	virtual bool GetX(DWORD x, double &value);
	
	/**GetY
	* Description	    : 
	* Return type		: double 
	*@param DWORD y
	*/
	virtual double GetY(DWORD y);
	
	/**GetY
	* Description	    : 
	* Return type		: bool 
	*@param DWORD y
	*@param double &value
	*/
	virtual bool GetY(DWORD y, double &value);
	
	/**DEMResize
	* Description	    : 
	* Return type		: void 
	*@param void
	*/
	virtual void DEMResize(void);
	
	/**SaveTXTDEM2BINDEM
	* Description	    : 
	* Return type		: void 
	*@param CString path
	*/
	virtual void SaveTXTDEM2BINDEM(CString path);
	
	/**CreatEmptyLONGBuf
	* Description	    : 
	* Return type		: bool 
	*@param DWORD w
	*@param DWORD h
	*@param short value
	*/
	virtual bool CreatEmptyLONGBuf(DWORD w, DWORD h, short value);
	
	/**SetBuf
	* Description	    : 
	* Return type		: void 
	*@param DWORD x
	*@param DWORD y
	*@param short value
	*/
	virtual void SetBuf(DWORD x, DWORD y, short value);
	
	/**GetBuf
	* Description	    : 
	* Return type		: void 
	*@param DWORD x
	*@param DWORD y
	*@param short &value
	*/
	virtual void GetBuf(DWORD x, DWORD y, short &value);
	
	/**CreatEmptyLONGBuf
	* Description	    : 
	* Return type		: bool 
	*@param DWORD w
	*@param DWORD h
	*@param double value
	*/
	virtual bool CreatEmptyLONGBuf(DWORD w, DWORD h, double value);
	
	/**SetBuf
	* Description	    : 
	* Return type		: void 
	*@param DWORD x
	*@param DWORD y
	*@param double value
	*/
	virtual void SetBuf(DWORD x, DWORD y, double value);
	
	/**GetBuf
	* Description	    : 
	* Return type		: void 
	*@param DWORD x
	*@param DWORD y
	*@param double &value
	*/
	virtual void GetBuf(DWORD x, DWORD y, double &value);

	
	/**ReadBINDEM
	* Description	    : 
	* Return type		: bool 
	*@param CString DEMPath
	*@param CString DEMHDRPath
	*/
	virtual bool ReadBINDEM(CString DEMPath, CString DEMHDRPath);
	
	/**ReadTextXYZDEM_surf
	* Description	    : 
	* Return type		: bool 
	*/
	virtual bool ReadTextXYZDEM_surf();

	DWORD& Width() {return width;};
	DWORD& Height() {return height;}

private:
	DWORD width, height;//DEM width and height
//member variable
public:
	CString FileName;//파일 이름
	ULONGLONG dwBitsSize;//DEM size
	HGLOBAL m_Mem;//메모리핸들
	double start_coordX;//시작좌표(X)
	double start_coordY;//시작좌표(Y)
	double offset;//DEM 간격
	

};

#endif // !defined(_SPACEMATICS_BINARY_DEM)
