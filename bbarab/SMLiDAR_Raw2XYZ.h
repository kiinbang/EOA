/*
* Copyright (c) 2006-2007, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

// SMLiDAR_Raw2XYZ.h: LiDAR point cloud re-conconstruction using system raw measurements
//
//////////////////////////////////////////////////////////////////////
//
//Written by Bang, Ki In
//
//Since 2008/07/18
//
//
#if !defined(__SPATIAL_SPACE_MATICS_LIDAR_RAW2XYZ__)
#define __SPATIAL_SPACE_MATICS_LIDAR_RAW2XYZ__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMLIDARConfig.h"
#include "Coordtr.h"
#include "UtilityGrocery.h"
#include <iomanip.h>
#include "Collinearity.h"

namespace SMATICS_BBARAB
{
	
	class CSMLiDARPointCalculation
	{
	public:
		
		static bool ReadIndexFile(CString fileinfopath, CSMList<int>& index)
		{
			int num_tag = 20;
			CString* TagList = new CString[num_tag];
			
			int count = -1;
			TagList[++count] = "NAVIGATION_X";//0
			TagList[++count] = "NAVIGATION_Y";//1
			TagList[++count] = "NAVIGATION_Z";//2
			TagList[++count] = "TIME";//3
			TagList[++count] = "NAVIGATION_PITCH";//4
			TagList[++count] = "NAVIGATION_ROLL";//5
			TagList[++count] = "NAVIGATION_YAW";//6
			TagList[++count] = "SCAN_ANGLE_ALPHA";//7
			TagList[++count] = "SCAN_ANGLE_BETA";//8
			TagList[++count] = "RANGE";//9
			TagList[++count] = "GROUND_X";//10
			TagList[++count] = "GROUND_Y";//11
			TagList[++count] = "GROUND_Z";//12
			TagList[++count] = "INTENSITY";//13
			TagList[++count] = "WEIGHT_MATRIX";//14
			TagList[++count] = "COV_MATRIX";//15
			TagList[++count] = "OMEGA_PLANE";//16
			TagList[++count] = "PHI_PLANE";//17
			TagList[++count] = "KAPPA_PLANE";//18
			TagList[++count] = "N/A";//19
			
			fstream InfoFile;
			InfoFile.open(fileinfopath, ios::in|ios::nocreate);
			if(!InfoFile) return false;
			
			char temp_st[512];
			
			while(!InfoFile.eof())
			{
				InfoFile>>temp_st;
				CString TagName = temp_st;
				
				bool bCheck = false;
				
				for(int i=0; i<num_tag; i++)
				{
					if(temp_st == TagList[i])
					{
						bCheck = true;
						index.AddTail(i);
						break;
					}
				}
				
				if(bCheck == false)
				{
					return false;
				}
				
				InfoFile.eatwhite();
				
			}
			
			InfoFile.close();
			
			return true;
		}
		
		static void ApplyMeridianConversion(CLIDARCalibrationPoint& point, ReferenceEllipsoid ellipsoid)
		{		
			double lambda, phi;
			//UTM2Geographic
			inv_utmproj(point.GPS_Y, point.GPS_X, &lambda, &phi, ellipsoid.SemiMajor, ellipsoid.SemiMinor, ellipsoid.zone, ellipsoid.hemi);
			
			if(lambda > PI) lambda -= 2*PI;
			else if(lambda < -PI) lambda += 2*PI;
			
			double centralmeridian = CentralMeridian(ellipsoid.zone);
			
			if(centralmeridian > PI) centralmeridian -= 2*PI;
			else if(centralmeridian < -PI) centralmeridian += 2*PI;
			
			double MC = MeridianConversion(lambda, phi, centralmeridian);
			
			if(MC > PI) MC -= 2*PI;
			else if(MC < -PI) MC += 2*PI;
			
			//To update heading angle
			point.INS_K = point.INS_K + MC;
		}
		
		static bool ReadRecord(fstream &infile, CSMList<int>& index, CLIDARCalibrationPoint &point, CLIDARCalibrationPoint &point_old, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid)
		{
			RemoveCommentLine(infile, "#!",2);
			
			int num_field = index.GetNumItem();
			if(num_field < 1) return false;
			
			//To read a record
			for(int i=0; i<num_field; i++)
			{
				switch(index.GetAt(i))
				{
				case 0:
					infile>>point.GPS_X;
					break;
				case 1:
					infile>>point.GPS_Y;
					break;
				case 2:
					infile>>point.GPS_Z;
					break;
				case 3:
					infile>>point.time;
					break;
				case 4:
					infile>>point.INS_O;//pitch
					point.INS_O = Deg2Rad(point.INS_O);
					break;
				case 5:
					infile>>point.INS_P;//roll
					point.INS_P = Deg2Rad(point.INS_P);
					break;
				case 6:
					infile>>point.INS_K;//yaw
					point.INS_K = Deg2Rad(point.INS_K);
					break;
				case 7:
					infile>>point.alpha;
					point.alpha = Deg2Rad(point.alpha);
					break;
				case 8:
					infile>>point.beta;
					point.beta = Deg2Rad(point.beta);
					break;
				case 9:
					infile>>point.dist;
					break;
				case 10:
					infile>>point.X;
					break;
				case 11:
					infile>>point.Y;
					break;
				case 12:
					infile>>point.Z;
					break;
				case 13:
					infile>>point.intensity;
					break;
				case 14://Weight matrix
					{
						for(int i=0; i<3; i++)
							for(int j=0; j<3; j++)
								infile>>point.P(i, j);
					}
					break;
				case 15://Co-varance matrix
					{
						for(int i=0; i<3; i++)
							for(int j=0; j<3; j++)
								infile>>point.P(i, j);
							//cov2weg
							point.P = point.P.Inverse();
					}
					break;
				case 16://OMEGA_PLANE
					infile>>point.PlaneAttitude[0];
					break;
				case 17://PHI_PLANE
					infile>>point.PlaneAttitude[1];
					break;
				case 18://KAPPA_PLANE
					infile>>point.PlaneAttitude[2];
					break;
				case 19://N/A
					infile.eatwhite();
					char line[MAX_LINE_LENGTH];
					infile.getline(line, MAX_LINE_LENGTH);
					break;
				default:
					AfxMessageBox("Input file parsing error");
					return false;
				}
			}
			
			infile.eatwhite();
			
			point_old = point;
			
			if(bMeridianConversion == true)
			{
				ApplyMeridianConversion(point, ellipsoid);
			}
			
			//Heading angle and scan angle correction based on the options
			switch(EQtype)
			{
			case 0://Terrapoint
				point.INS_K = - point.INS_K;
				point.beta = Deg2Rad(90) - point.beta;
				break;
			case 1://Terrapoint(scan_angle plus 90deg)
				break;
			case 2://Simulator data (Meridian-conversion is not required.)
				break;
			case 3://etc
				break;
			default:
				AfxMessageBox("option fail");
				break;
			}
			
			return true;
			
		}
		
		static CSMMatrix<double> LIDAREQ_Point_BasicEQ(CLIDARCalibrationPoint point, CSMLIDARConfig config, CSMMatrix<double> dumy)
		{
			CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Ranging(3,1,0.);
			
			X0(0,0) = point.GPS_X; 
			X0(1,0) = point.GPS_Y; 
			X0(2,0) = point.GPS_Z;
			
			P(0,0) = config.Xb; 
			P(1,0) = config.Yb; 
			P(2,0) = config.Zb;
			
			CRotationcoeff2 Swing((point.alpha+config.Alpha_b), (point.beta + config.Beta_b), 0.);
			
			Ranging(0,0) = 0; 
			Ranging(1,0) = 0; 
			Ranging(2,0) = -(config.RangeS*point.dist + config.Rangeb);
			
			CSMMatrix<double> LU = Swing.Rmatrix%Ranging;
			
			////////////////////////////////////////////////////////////////////////////////////////
			//
			//INS configuration
			//
			//Flight direction : Y
			//Wing direction: X
			
			_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
			_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
			_Rmat_ Yawmat	(0, 0, point.INS_K);//rotation matrix (yaw)
			
			//
			//Attitude matrix
			//
			//Rotation order (about Z <- about Y <- about X)
			CSMMatrix<double> INS; INS = dumy%(Pitchmat.Rmatrix)%(Rollmat.Rmatrix)%(Yawmat.Rmatrix);
			//
			//Rotational offset (boresighting)
			//
			CRotationcoeff2 ROffset;
			_Rmat_ Rmat	(0,		config.Pb,		0);//rotation matrix (roll)
			_Rmat_ Pmat	(config.Ob,	0,		0);//rotation matrix (pitch)
			_Rmat_ Ymat	(0,		0,		config.Kb);//rotation matrix (yaw)
			
			ROffset.Rmatrix = Pmat.Rmatrix%Rmat.Rmatrix%Ymat.Rmatrix;
			
			CSMMatrix<double> XG = X0 + INS%(P + ROffset.Rmatrix%Swing.Rmatrix%Ranging);
			
			return XG;
		}

		static CSMMatrix<double> LIDAREQ_Terrapoint_2(CLIDARCalibrationPoint point, CSMLIDARConfig config)
		{
			CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Ranging(3,1,0.);
			
			X0(0,0) = point.GPS_X; 
			X0(1,0) = point.GPS_Y; 
			X0(2,0) = point.GPS_Z;
			
			P(0,0) = config.Xb; 
			P(1,0) = config.Yb; 
			P(2,0) = config.Zb;
			
			CRotationcoeff2 Swing(0, -(point.beta + config.Beta_b), 0.);
			
			Ranging(0,0) = 0; 
			Ranging(1,0) = 0; 
			Ranging(2,0) = -(config.RangeS*point.dist + config.Rangeb);
			
			CSMMatrix<double> LU = Swing.Rmatrix%Ranging;
			
			////////////////////////////////////////////////////////////////////////////////////////
			//
			//INS configuration
			//
			//Flight direction : Y
			//Wing direction: X
			
			_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
			_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
			_Rmat_ Yawmat	(0, 0, -point.INS_K);//rotation matrix (yaw)
			
			//
			//Attitude matrix
			//
			CSMMatrix<double> INS; 
			//Rotation order (about Z -> about Y -> about X)
			INS = (Yawmat.Rmatrix)%(Rollmat.Rmatrix)%(Pitchmat.Rmatrix);
			
			//
			//Rotational offset (boresighting)
			//
			CRotationcoeff2 ROffset;
			_Rmat_ Rmat	(0,		config.Pb,		0);//rotation matrix (roll)
			_Rmat_ Pmat	(config.Ob,	0,		0);//rotation matrix (pitch)
			_Rmat_ Ymat	(0,		0,		-config.Kb);//rotation matrix (yaw)
			//Rotation order (about Z -> about Y -> about X)
			ROffset.Rmatrix = Ymat.Rmatrix%Rmat.Rmatrix%Pmat.Rmatrix;
			
			CSMMatrix<double> XG = X0 + INS%(P + ROffset.Rmatrix%Swing.Rmatrix%Ranging);
			
			return XG;
		}

		static CSMMatrix<double> LIDAREQ_Terrapoint(CLIDARCalibrationPoint point, CSMLIDARConfig config)
		{
			CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Ranging(3,1,0.);
			
			X0(0,0) = point.GPS_X; 
			X0(1,0) = point.GPS_Y; 
			X0(2,0) = point.GPS_Z;
			
			P(0,0) = config.Xb; 
			P(1,0) = config.Yb; 
			P(2,0) = config.Zb;
			
			CRotationcoeff2 Swing(0, -(point.beta + config.Beta_b), 0.);
			
			Ranging(0,0) = 0; 
			Ranging(1,0) = 0; 
			Ranging(2,0) = -(config.RangeS*point.dist + config.Rangeb);
			
			CSMMatrix<double> LU = Swing.Rmatrix%Ranging;
			
			////////////////////////////////////////////////////////////////////////////////////////
			//
			//INS configuration
			//
			//Flight direction : Y
			//Wing direction: X
			
			_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
			_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
			_Rmat_ Yawmat	(0, 0, -point.INS_K);//rotation matrix (yaw)
			
			//
			//Attitude matrix
			//
			CSMMatrix<double> INS; 
			//Rotation order (about Z -> about Y -> about X)
			INS = (Yawmat.Rmatrix)%(Rollmat.Rmatrix)%(Pitchmat.Rmatrix);
			
			//
			//Rotational offset (boresighting)
			//
			CRotationcoeff2 ROffset;
			_Rmat_ Rmat	(0,		config.Pb,		0);//rotation matrix (roll)
			_Rmat_ Pmat	(config.Ob,	0,		0);//rotation matrix (pitch)
			_Rmat_ Ymat	(0,		0,		-config.Kb);//rotation matrix (yaw)
			//Rotation order (about Z -> about Y -> about X)
			ROffset.Rmatrix = Ymat.Rmatrix%Rmat.Rmatrix%Pmat.Rmatrix;
			
			CSMMatrix<double> XG = X0 + INS%(P + ROffset.Rmatrix%Swing.Rmatrix%Ranging);
			
			return XG;
		}

		static CSMMatrix<double> LIDAREQ_Simulator(CLIDARCalibrationPoint point, CSMLIDARConfig config)
		{
			CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Ranging(3,1,0.);
			
			X0(0,0) = point.GPS_X; 
			X0(1,0) = point.GPS_Y; 
			X0(2,0) = point.GPS_Z;
			
			P(0,0) = config.Xb; 
			P(1,0) = config.Yb; 
			P(2,0) = config.Zb;
			
			CRotationcoeff2 Swing((point.alpha+config.Alpha_b), (point.beta + config.Beta_b), 0.);
			
			Ranging(0,0) = 0; 
			Ranging(1,0) = 0; 
			Ranging(2,0) = -(config.RangeS*point.dist + config.Rangeb);
			
			CSMMatrix<double> LU = Swing.Rmatrix%Ranging;
			
			////////////////////////////////////////////////////////////////////////////////////////
			//
			//INS configuration
			//
			//Flight direction : Y
			//Wing direction: X
			
			_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
			_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
			_Rmat_ Yawmat	(0, 0, point.INS_K);//rotation matrix (yaw)
			
			//
			//Attitude matrix
			//
			//Rotation order (about X -> about Y -> about Z)
			CSMMatrix<double> INS; INS = (Pitchmat.Rmatrix)%(Rollmat.Rmatrix)%(Yawmat.Rmatrix);
			//
			//Rotational offset (boresighting)
			//
			CRotationcoeff2 ROffset;
			_Rmat_ Rmat	(0,		config.Pb,		0);//rotation matrix (roll)
			_Rmat_ Pmat	(config.Ob,	0,		0);//rotation matrix (pitch)
			_Rmat_ Ymat	(0,		0,		config.Kb);//rotation matrix (yaw)
			
			ROffset.Rmatrix = Pmat.Rmatrix%Rmat.Rmatrix%Ymat.Rmatrix;
			
			CSMMatrix<double> XG = X0 + INS%(P + ROffset.Rmatrix%Swing.Rmatrix%Ranging);
			
			return XG;
		}

		static CSMMatrix<double> LIDAREQ_etc(CLIDARCalibrationPoint point, CSMLIDARConfig config)
		{
			/*
			point.GPS_X = 453371.3796052;
			point.GPS_Y = 4947146.1524930;
			point.GPS_Z = 1477.6306289;
			point.INS_O = Deg2Rad(-2.3947140);
			point.INS_P = Deg2Rad(0.9161657);
			point.INS_K = Deg2Rad(168.8690615);
			point.beta = Deg2Rad(-21.4189204);
			point.dist = 1534.8427802;
			//453869.013	4947217.092	27.519
			//497.6333948	70.939507	-1450.111629

			//454571.585;	4943306.409	3.856;							
			//365.7955155	250.0411741	-1494.95385

			point.GPS_X = 454205.7895;	point.GPS_Y = 4943056.368;	point.GPS_Z = 1498.80985;
			point.INS_O = Deg2Rad(1.4344115);
			point.INS_P = Deg2Rad(0.7817758);
			point.INS_K = Deg2Rad(-32.048389);
			point.beta = Deg2Rad(18.0103274);
			point.dist = 1559.2972182;
			*/



			CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Ranging(3,1,0.);
			
			X0(0,0) = point.GPS_X;//E
			X0(1,0) = point.GPS_Y;//N
			X0(2,0) = point.GPS_Z;//H
			
			P(0,0) = config.Xb;
			P(1,0) = config.Yb;
			P(2,0) = config.Zb;
			
			//Flight direction: X
			CRotationcoeff2 Swing(0, -(point.beta + config.Beta_b), 0);
			
			Ranging(0,0) = 0; 
			Ranging(1,0) = 0; 
			Ranging(2,0) = -(config.RangeS*point.dist + config.Rangeb);
			
			CSMMatrix<double> LU = Swing.Rmatrix%Ranging;
			
			////////////////////////////////////////////////////////////////////////////////////////
			//
			//INS configuration
			//
			//Flight direction : X
			//Wing direction: Y
			//Rotation order: R = Roll(X)*Pitch(Y)*Yaw(Z)
			
			_Rmat_ Rollmat	(-point.INS_P, 0, 0);
			_Rmat_ Pitchmat	(0, point.INS_O, 0);
			_Rmat_ Yawmat	(0, 0, -point.INS_K);
			
			//
			//Attitude matrix
			//
			//Rotation order (about X -> about Y -> about Z)
			CSMMatrix<double> INS; INS = (Rollmat.Rmatrix)%(Pitchmat.Rmatrix)%(Yawmat.Rmatrix);
			CSMMatrix<double> INS_LU = INS%Swing.Rmatrix%Ranging;
			
			//
			//Rotational offset (boresighting)
			//Flight direction : X
			//Wing direction: Y
			//Rotation order: R = Roll(X)*Pitch(Y)*Yaw(Z)
			CRotationcoeff2 ROffset;
			_Rmat_ Rmat	(config.Ob,	0, 0);
			_Rmat_ Pmat	(0, config.Pb, 0);		
			_Rmat_ Ymat	(0, 0, config.Kb);
			
			ROffset.Rmatrix = Rmat.Rmatrix%Pmat.Rmatrix%Ymat.Rmatrix;
			
			CSMMatrix<double> XG(3,1), XG_;
			XG_ = X0 + INS%(P + ROffset.Rmatrix%Swing.Rmatrix%Ranging);
 			XG(0,0) = XG_(0,0);
 			XG(1,0) = XG_(1,0);
 			XG(2,0) = XG_(2,0);
			//453869.013	4947217.092	27.519


			return XG;
		}
		
		static bool GetGroundFootPrint(CString infilepath, CString fileinfopath, CString cfgfilepath, CString outpath, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid)
		{
			CSMLIDARConfig Config;/**< config info */
			
			if(false == Config.ReadConfigFile(cfgfilepath, EQtype)) return false; //*<Read configuration file*/
			
			Config.CheckZero(1.0e-30); //*<Make Zero given threshold (default: 1.0e-30)
			
			fstream outfile;
			outfile.open(outpath, ios::out);
			outfile.setf(ios::fixed, ios::floatfield);
			
			fstream LidarFile;
			LidarFile.open(infilepath, ios::in|ios::nocreate);
			if(!LidarFile) return false;
			
			CSMList<int> index;	
			if(false == ReadIndexFile(fileinfopath, index)) 
			{
				AfxMessageBox("Error: ReadIndexFile");
				return false;
			}
			
			CSMMatrix<double> dumy(3,3); dumy.Identity();
			CSMMatrix<double> P;
			CLIDARCalibrationPoint point, point_old;
			
			int count =0;
						
			while(!LidarFile.eof())
			{
				if(false == ReadRecord(LidarFile, index, point, point_old, EQtype, bMeridianConversion, ellipsoid)) return false;
				CSMMatrix<double> Range(3,1,0), Leverarm(3,1,0), GPS(3,1,0);
				
				Range(2,0) = -point.dist;
				
				GPS(0,0) = point.GPS_X;
				GPS(1,0) = point.GPS_Y;
				GPS(2,0) = point.GPS_Z;

				Leverarm(0,0) = Config.Xb;
				Leverarm(1,0) = Config.Yb;
				Leverarm(2,0) = Config.Zb;

				_Rmat_ Swing, Bore, INS;

				INS.ReMake(point.INS_O, point.INS_P, point.INS_K);
				Bore.ReMake(Config.Ob, Config.Pb, Config.Kb);
				Swing.ReMake(point.alpha, point.beta, 0);

				CSMMatrix<double> PX = GPS + INS.Rmatrix%(Leverarm + Bore.Rmatrix%Swing.Rmatrix%Range);
				//PX = Swing.Rmatrix%Range;
				//PX = Bore.Rmatrix%Swing.Rmatrix%Range;
				//PX = INS.Rmatrix%Bore.Rmatrix%Swing.Rmatrix%Range;

				switch(EQtype)
				{
				case 0://Terrapoint
					P = LIDAREQ_Point_BasicEQ(point, Config, dumy);
					break;
				case 1://Terrapoint(scan_angle + 90deg)
					P = LIDAREQ_Terrapoint_2(point, Config);
					break;
				case 2://Simulator data
					{
						P = LIDAREQ_Simulator(point, Config);
					}
					break;
				case 3://etc
					{
						//_Rmat_ Rdumy(0, Deg2Rad(180), 0);
						//dumy = Rdumy.Rmatrix;
						//P = LIDAREQ_Point_BasicEQ(point, Config, dumy);

						P = LIDAREQ_etc(point, Config);
					}
					break;
				default:
					AfxMessageBox("option fail");
					break;
				}
				
				outfile.precision(3);
				outfile<<setw(12)<<P(0,0)<<"\t"<<setw(12)<<P(1,0)<<"\t"<<setw(12)<<P(2,0)<<endl;
				outfile.flush();
				
				count++;
			}
			
			LidarFile.close();
						
			outfile.close();
			
			return true;
			
		}
	};
	
}//namespace

#endif // !defined(__SPATIAL_SPACE_MATICS_LIDAR_RAW2XYZ__)