/*
 * Copyright (c) 1999-2007, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// SSMCamera.cpp: implementation of the CSSMCamera class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SSMCamera.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSSMCamera::CSSMCamera()
{
	m_Dispersion.Resize(NUM_CAMERA_PARAM, NUM_CAMERA_PARAM);
	m_Dispersion.Identity(1.0);

	k0 = k1 = k2 = k3 = 0.0;
	p1 = p2 = p3 = 0.0;
}

CSSMCamera::~CSSMCamera()
{
}

void CSSMCamera::GetSMACDistortion(double x, double y, double &delta_x, double &delta_y)
{
	double dx = x - m_xp;
	double dy = y - m_yp;
	
	double r2 = dx*dx + dy*dy;
	double r4 = r2 * r2;
	double r6 = r2 * r4;
	
	delta_x = dx*(k0 +	k1*r2 + k2*r4 + k3*r6) + // radial distortion
		(1.0 + p3* r2) * (p1*(r2 + 2*dx*dx) + p2*2*dx*dy);	//decentring
	delta_y = dy*(k0 +	k1*r2 + k2*r4 + k3*r6) + //radial
		(1 + p3* r2) * (p2*(r2 + 2*dy*dy) + p1*2*dx*dy);	//decentring
}

void CSSMCamera::SetSMACParameters(double k[4], double p[3])
{
	k0 = k[0]; k1 = k[1]; k2 = k[2]; k3 = k[3];
	p1 = p[0]; p2 = p[1]; p3 = p[2];
}