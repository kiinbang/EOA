//////////////////////
//CoordinateClass.h
//for Transformation
//made by Bang Ki In
//revision date 2000/08/03
//revision date 2001/03/09
//////////////////////
#ifndef _COORDINATE_CLASS_
#define _COORDINATE_CLASS_

#include<math.h>

///////////////////////////
// class for 2D coordinate
///////////////////////////
template<class T>
class COORD2D  // Declare 2D coordinate struct type
{
public:
	T x;			// x
	T y;			// y
	COORD2D() { x = T(0); y = T(0); }
	COORD2D(const COORD2D<T>& coord)	{ x = coord.x; y = coord.y; }
	COORD2D(const T& _x, const T& _y) { x = _x; y = _y; }
	COORD2D(const int n) { x = T(n); y = T(n); }
	virtual ~COORD2D() { }
	//operator
	COORD2D<T>& operator=(const COORD2D<T>& coord) { x = coord.x; y = coord.y; return *this; }
	COORD2D<T>& operator=(CPoint pt) { x = T(pt.x); y = T(pt.y); return *this; }
	COORD2D<T> operator+(const COORD2D<T>& coord) {
		COORD2D<T> r;
		r.x = x + coord.x;
		r.y = y + coord.y;
		return r;
	}
	COORD2D<T> operator-(const COORD2D<T>& coord) {
		COORD2D<T> r;
		r.x = x - coord.x;
		r.y = y - coord.y;
		return r;
	}
	COORD2D<T> operator*(const double s) {
		COORD2D<T> r;
		r.x = x*s;
		r.y = y*s;
		return r;
	}
	COORD2D<T> operator/(const double s) {
		COORD2D<T> r;
		if(s != 0.0)
		{
			r.x = x/s;
			r.y = y/s;
		}
		else
		{
			::MessageBox(_T("Divided zero"),MB_OK);
		}
		return r;
	}
	
	void operator()(const T& _x, const T& _y)	{ x = _x; y = _y; }
	
};

///////////////////////////
// class for 3D coordinate
///////////////////////////
template<class T>
class COORD3D  // Declare 3D coordinate struct type
{
public:
	T x;			// x
	T y;			// y
	T z;			// z
	COORD3D() { x = T(0); y = T(0); z = T(0); }
	COORD3D(const COORD3D<T>& coord)	{ x = coord.x; y = coord.y; z = coord.z; }
	COORD3D(const T& _x, const T& _y, const T& _z) { x = _x; y = _y; z = _z; }
	COORD3D(const int n) { x = T(n); y = T(n); z = T(n);}
	virtual ~COORD3D() { }
	//operator
	COORD3D<T>& operator=(const COORD3D<T>& coord) { x = coord.x; y = coord.y; z = coord.z; return *this; }
	COORD3D<T> operator+(const COORD3D<T>& coord) {
		COORD3D<T> r;
		r.x = x + coord.x;
		r.y = y + coord.y;
		r.z = z + coord.z;
		return r;
	}
	COORD3D<T> operator-(const COORD3D<T>& coord) {
		COORD3D<T> r;
		r.x = x - coord.x;
		r.y = y - coord.y;
		r.y = y - coord.z;
		return r;
	}
	
	void operator()(const T& _x, const T& _y, const T& _z)	{ x = _x; y = _y; z = _z; }
	
};

////////////////////////////////////////////////////////////////////
#endif