// smlidardem.cpp : implementation file
//

#include "stdafx.h"
#include <smlidardem.h>

/////////////////////////////////////////////////////////////////////////////
// CSMDEM
CSMDEM::CSMDEM()
{
}

CSMDEM::CSMDEM(CSMDEM &copy)
{
	Copy(copy);
}

CSMDEM& CSMDEM::operator =(CSMDEM &copy)
{
	return Copy(copy);
}

CSMDEM& CSMDEM::Copy(CSMDEM &copy)
{
	dwBitsSize = copy.dwBitsSize;
	FileName = copy.FileName;
	height = copy.height;
	width = copy.width;
	m_Mem = copy.m_Mem;
	offset = copy.offset;
	start_coordX = copy.start_coordX;
	start_coordY = copy.start_coordY;

	return *this;
}

CSMDEM::~CSMDEM()
{
}

bool CSMDEM::ReadBINDEM(CString DEMPath, CString DEMHDRPath)
{
	//헤더파일 읽기
	FILE *hdr;
	hdr = fopen(DEMHDRPath,"r");
	double interval, sX, sY;
	int w, h;
	
	//width, height, offset, start_X(left), start_Y(top)
	if(EOF == fscanf(hdr,"%d",&w)) return false;
	if(EOF == fscanf(hdr,"%d",&h)) return false;
	if(EOF == fscanf(hdr,"%lf",&interval)) return false;
	if(EOF == fscanf(hdr,"%lf",&sX)) return false;
	if(EOF == fscanf(hdr,"%lf",&sY)) return false;
	
	fclose(hdr);
	
	FileName = DEMPath;
	height = h;
	width = w;
	offset = interval;
	start_coordX = sX;
	start_coordY = sY;
	
	DEMPath.MakeUpper();
	if(DEMPath.Find(".DEM") > -1) return ReadLIDARDEM();
	else if(DEMPath.Find(".TXT") > -1) return ReadTextXYZDEM_surf();
	else return false;
}

bool CSMDEM::ReadLIDARDEM()
{
	CFile file;
	CFileException fe;
	short * pData;

	// 읽기 모드로 파일 열기
	if(!file.Open(FileName, CFile::modeRead|CFile::shareDenyWrite, &fe))
	{
		AfxMessageBox("파일읽기 에러");
		return 0;
	}
	
	// 파일의 길이를 구함
	dwBitsSize = file.GetLength();

	// 메모리 할당
	if((m_Mem = (HGLOBAL)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, (SIZE_T)dwBitsSize)) == NULL)
	{
		AfxMessageBox("메모리 할당 에러");
		return 0;
	}

	// 메모리 고정
	pData = (short*) ::GlobalLock((HGLOBAL) m_Mem);

	// 파일 읽기
	if (file.Read(pData, (UINT)dwBitsSize) != dwBitsSize) 
	{
		::GlobalUnlock((HGLOBAL) m_Mem);
		::GlobalFree((HGLOBAL) m_Mem);
		AfxMessageBox("파읽기에러");
		return 0;
	}

	// 메모리 풀어줌
	::GlobalUnlock((HGLOBAL) m_Mem);
	file.Close();

	return 1;
}

bool CSMDEM::CreatEmptyLONGBuf(DWORD w, DWORD h, short value)
{
	width = w; height = h;
	//메모리 크기 설정
	dwBitsSize = sizeof(short)*width*height;

	// 메모리 할당
	if((m_Mem = (HGLOBAL)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, (SIZE_T)dwBitsSize)) == NULL)
	{
		AfxMessageBox("메모리 할당 에러");
		return 0;
	}

	// 메모리 고정
	short *data;
	data = (short*) ::GlobalLock((HGLOBAL) m_Mem);
	
	// Initialize
	for(long j=0; j<(long)height; j++)
	{
		for(long i=0; i<(long)width; i++)
		{
			data[(height-1-j)*width + i] = value;
		}
	}
	
	// 메모리 풀어줌
	::GlobalUnlock((HGLOBAL) m_Mem);

	return 1;
}

bool CSMDEM::CreatEmptyLONGBuf(DWORD w, DWORD h, double value)
{
	width = w; height = h;
	//메모리 크기 설정
	dwBitsSize = sizeof(double)*width*height;

	// 메모리 할당
	if((m_Mem = (HGLOBAL)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, (SIZE_T)dwBitsSize)) == NULL)
	{
		AfxMessageBox("메모리 할당 에러");
		return 0;
	}

	// 메모리 고정
	double *data;
	data = (double*) ::GlobalLock((HGLOBAL) m_Mem);
	
	// Initialize
	for(long j=0; j<(long)height; j++)
	{
		for(long i=0; i<(long)width; i++)
		{
			data[(height-1-j)*width + i] = value;
		}
	}
	
	// 메모리 풀어줌
	::GlobalUnlock((HGLOBAL) m_Mem);

	return 1;
}

bool CSMDEM::ReadTextXYZDEM_surf()
{
	//메모리 크기 설정
	dwBitsSize = sizeof(short)*width*height;

	// 메모리 할당
	if((m_Mem = (HGLOBAL)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, (SIZE_T)dwBitsSize)) == NULL)
	{
		AfxMessageBox("메모리 할당 에러");
		return 0;
	}

	// 메모리 고정
	short *data;
	data = (short*) ::GlobalLock((HGLOBAL) m_Mem);
	
	// Text DEM 파일 읽기
	FILE *file;
	file = fopen(FileName, "r");
	int X, Y; double Z;

	for(long j=0; j<(long)height; j++)
	{
		for(long i=0; i<(long)width; i++)
		{
			if(EOF == fscanf(file,"%d",&X)) break;
			if(EOF == fscanf(file,"%d",&Y)) break;
			if(EOF == fscanf(file,"%lf",&Z)) break;
			
			data[(height-1-j)*width + i] = (short)(Z+0.5);
		}
	}
	
	void DEMInverse();

	fclose(file);
	
	// 메모리 풀어줌
	::GlobalUnlock((HGLOBAL) m_Mem);

	return 1;
}

DWORD CSMDEM::FindIndex(DWORD x, DWORD y)
{
	if(x<0||x>=width) 
	{
		//::MessageBox(NULL,"index_X is out of area.","WARNING",MB_OK);
		return -1;
	}
	if(y<0||y>=height) 
	{
		//::MessageBox(NULL,"index_Y is out of area.","WARNING",MB_OK);
		return -1;
	}
	DWORD index;
	index = y*width + x;
	return index;
}

short CSMDEM::GetDEMValue(DWORD x, DWORD y)
{
	short value;
	short *data;
	data = (short *)::GlobalLock(m_Mem);
	value =  data[FindIndex(x,y)];
	::GlobalUnlock(m_Mem);

	return value;
}

double CSMDEM::GetX(DWORD x)
{
	double value;
	value = start_coordX + offset*x;
	
	return value;
}

bool CSMDEM::GetX(DWORD x, double &value)
{
	if(x<0||x>=width) return false;
	value = start_coordX + offset*x;
	return true;
}

double CSMDEM::GetY(DWORD y)
{
	double value;
	value = start_coordY - offset*y;
	return value;
}

bool CSMDEM::GetY(DWORD y, double &value)
{
	if(y<0||y>=height) return false;
	value = start_coordY - offset*y;
	return true;
}

double CSMDEM::GetZ(DWORD x, DWORD y)
{
	short value;
	short *data;
	data = (short *)::GlobalLock(m_Mem);
	value =  data[FindIndex(x,y)];
	::GlobalUnlock(m_Mem);

	return (double)value;
}

bool CSMDEM::GetZ(DWORD x, DWORD y, double &value)
{
	if(x<0||x>=width) return false;
	if(y<0||y>=height) return false;

	short *data;
	data = (short *)::GlobalLock(m_Mem);
	value =  (double)data[FindIndex(x,y)];
	::GlobalUnlock(m_Mem);

	return true;
}

bool CSMDEM::GetXYZ(DWORD x, DWORD y, double &X, double &Y, double &Z)
{
	if(false == GetX(x, X)) return false;
	if(false == GetY(y, Y)) return false;
	Z = GetZ(x,y);

	return true;
}

void CSMDEM::SetBuf(DWORD x, DWORD y, short value)
{
	short *data;
	data = (short *)::GlobalLock(m_Mem);
	data[FindIndex(x,y)] = value;
	::GlobalUnlock(m_Mem);
}

void CSMDEM::GetBuf(DWORD x, DWORD y, short &value)
{
	short *data;
	DWORD index = FindIndex(x,y);
	data = (short *)::GlobalLock(m_Mem);
	value =  data[index];
	::GlobalUnlock(m_Mem);
}

void CSMDEM::SetBuf(DWORD x, DWORD y, double value)
{
	double *data;
	data = (double *)::GlobalLock(m_Mem);
	data[FindIndex(x,y)] = value;
	::GlobalUnlock(m_Mem);
}

void CSMDEM::GetBuf(DWORD x, DWORD y, double &value)
{
	double *data;
	data = (double *)::GlobalLock(m_Mem);
	value =  data[FindIndex(x,y)];
	::GlobalUnlock(m_Mem);
}

void CSMDEM::DEMResize()
{
	HGLOBAL hg;
	DWORD size = 50;
	hg = (HGLOBAL)::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, DWORD(width/size)*DWORD(height/size)*2);
		
	short *olddata;
	short *newdata;

	olddata = (short*)::GlobalLock(m_Mem);

	newdata = (short*)::GlobalLock(hg);
	
	for(DWORD i=0; i<DWORD(height/size); i++)
	{
		for(DWORD j=0; j<DWORD(width/size); j++)
		{
			newdata[i*DWORD(width/size) + j] = olddata[FindIndex(j*size,i*size)];
		}
	}

	CFile newfile;
	CFileException fe;
	// 쓰기 모드로 파일 열기
	newfile.Open("lidar1000.bil", CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe);

	
	newfile.Write(newdata, DWORD(width/size)*DWORD(height/size)*2);

	// 메모리 풀어줌
	::GlobalUnlock((HGLOBAL) m_Mem);
	::GlobalUnlock((HGLOBAL) hg);

	newfile.Close();
	
}

void CSMDEM::SaveTXTDEM2BINDEM(CString path)
{
	short* newdata = (short*)::GlobalLock(m_Mem);
	CFile newfile;
	CFileException fe;
	// 쓰기 모드로 파일 열기
	newfile.Open(path, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &fe);

	newfile.Write(newdata, DWORD(width)*DWORD(height)*sizeof(short));

	// 메모리 풀어줌
	::GlobalUnlock((HGLOBAL) m_Mem);

	newfile.Close();
}