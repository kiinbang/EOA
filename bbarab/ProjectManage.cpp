// ProjectManage.cpp: implementation of the CProjectManage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ProjectManage.h"
#include <fstream.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define LINELENGTH 200

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProjectManage::CProjectManage()
{

}

CProjectManage::~CProjectManage()
{

}

void CProjectManage::Init(void)
{
	//Project Name
	m_stProjectName = "";
	//Project Path
	m_stProjectPath = "";
	//Sensor Type
	m_stSensorTypeName = "";
	//Sensor Model
	m_stSensorModelName = "";
	//Coordinates System
	m_stCoordSystemName = "";
	//Image Path
	m_LeftPath = "";
	m_RightPath = "";
	//GCP
	m_bGCP = false;
	//Tie Point
	m_bTie = false;
	//DEM
	m_bDEM = false;
	m_stDEMPath = "";
	//Ortho
	m_bOrtho = false;
	m_stOrthoPath = "";
	//Epipolar
	m_bEpipolar = false;
	m_stEpiLeftPath = "";
	m_stEpiRightPath = "";
	//Mapping
	m_bMapping = false;
	m_stMapPath = "";
	//GCP Data
	m_LGCP.EmptyGCP();
	m_RGCP.EmptyGCP();
	m_LICP.EmptyICP();
	m_RICP.EmptyICP();
	//Tie Point Data
	m_LTie.EmptyICP();
	m_RTie.EmptyICP();
}

bool CProjectManage::TempProjectOpen(char* filename, char* oldfilename)
{
	bool b;
	CString temp = m_stProjectPath;
	b = ProjectOpen(filename);
	m_stProjectPath = temp;
	return b;
}

bool CProjectManage::ProjectOpen(char* filename)
{
	Init();

	m_stProjectPath = filename;
	//FILE *prjfile;
	ifstream infile;
	char temp[LINELENGTH];
		
	int i, j;
	infile.open((LPCTSTR)filename,ios::in,filebuf::openprot);
	if(infile.fail())
	{
		return false;
	}
	else
	{
		char delimiter[] = ", \r\t\n";
		char *token; 
		//[OPTPRJ10]
		infile.getline(temp,sizeof(temp));
		//Project name  
		infile.getline(temp,sizeof(temp));
		m_stProjectName = temp;
		//Sensor type
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		i = atoi(token);
		m_SensorType = static_cast<SENSORTYPE>(i);
		token = strtok(NULL,delimiter);
		m_stSensorTypeName = (CString)token;
		//Sensor model
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		i = atoi(token);
		m_SensorModel = static_cast<SENSORMODEL>(i);
		token = strtok(NULL,delimiter);
		m_stSensorModelName = (CString)token;
		//Coordinate system
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		i = atoi(token);
		m_CoordSystem = static_cast<COORDSYS>(i);
		token = strtok(NULL,delimiter);
		m_stCoordSystemName = (CString)token;
		//Image Path 1
		infile.getline(temp,sizeof(temp));
		m_LeftPath = (CString)temp;
		//Image Format 1
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		i = atoi(token);
		m_LFormat = static_cast<IMAGEFORMAT>(i);
		//Image Path 2
		infile.getline(temp,sizeof(temp));
		m_RightPath = temp;
		//Image Format 2
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		i = atoi(token);
		m_RFormat = static_cast<IMAGEFORMAT>(i);
		//GCP
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		token = strtok(NULL,delimiter);
		if(atoi(token) == 1)
			m_bGCP = true;
		else
			m_bGCP = false;
		//Tie Point
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		token = strtok(NULL,delimiter);
		if(atoi(token) == 1)
			m_bTie = true;
		else
			m_bTie = false;
		//DEM
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		token = strtok(NULL,delimiter);
		if(atoi(token) == 1)
			m_bDEM = true;
		else
			m_bDEM = false;
		
		infile.getline(temp,sizeof(temp));
		m_stDEMPath = (CString)temp;
		
		//Ortho
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		token = strtok(NULL,delimiter);
		if(atoi(token) == 1)
			m_bOrtho = true;
		else
			m_bOrtho = false;
		
		infile.getline(temp,sizeof(temp));
		m_stOrthoPath = (CString)temp;
		
		//Epipolar
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		token = strtok(NULL,delimiter);
		if(atoi(token) == 1)
			m_bEpipolar = true;
		else
			m_bEpipolar = false;
		
		infile.getline(temp,sizeof(temp));
		m_stEpiLeftPath = (CString)temp;
		infile.getline(temp,sizeof(temp));
		m_stEpiRightPath = (CString)temp;
		
		//Mapping
		infile.getline(temp,sizeof(temp));
		token = strtok(temp,delimiter);
		token = strtok(NULL,delimiter);
		if(atoi(token) == 1)
			m_bMapping = true;
		else
			m_bMapping = false;
		
		infile.getline(temp,sizeof(temp));
		m_stMapPath = (CString)temp;
					
		//GCP Data
		if(m_bGCP == true)
		{
			//Left GCP Data
			infile.getline(temp,sizeof(temp));
			token = strtok(temp, delimiter);
			i = atoi(token);
			for(j=0; j<i; j++)
			{
				int id;
				double col, row;
				double X, Y, Z;
				
				infile.getline(temp,sizeof(temp));
				token = strtok(temp,delimiter);
				id = atoi(token);
				
				token = strtok(NULL,delimiter);
				col = atof(token);
				
				token = strtok(NULL,delimiter);
				row = atof(token);
					
				token = strtok(NULL,delimiter);
				X = atof(token);
					
				token = strtok(NULL,delimiter);
				Y = atof(token);
					
				token = strtok(NULL,delimiter);
				Z = atof(token);
					
				m_LICP.InsertICP(col,row,id);
				m_LGCP.InsertGCP(X,Y,Z,id);
			}
				
			//Right GCP Data
			infile.getline(temp,sizeof(temp));
			token = strtok(temp, delimiter);
			i = atoi(token);
			for(j=0; j<i; j++)
			{
				int id;
				double col, row;
				double X, Y, Z;
					
				infile.getline(temp,sizeof(temp));
				token = strtok(temp,delimiter);
				id = atoi(token);
					
				token = strtok(NULL,delimiter);
				col = atof(token);
					
				token = strtok(NULL,delimiter);
				row = atof(token);
					
				token = strtok(NULL,delimiter);
				X = atof(token);
					
				token = strtok(NULL,delimiter);
				Y = atof(token);
					
				token = strtok(NULL,delimiter);
				Z = atof(token);
					
				m_RICP.InsertICP(col,row,id);
				m_RGCP.InsertGCP(X,Y,Z,id);
			}
		}
			
		//Tie Point Data
		if(m_bTie == true)
		{
			infile.getline(temp,sizeof(temp));
			token = strtok(temp, delimiter);
			i = atoi(token);
			for(j=0; j<i; j++)
			{
				int id;
				double Lcol, Lrow;
				double Rcol, Rrow;
				
				infile.getline(temp,sizeof(temp));
				token = strtok(temp,delimiter);
				id = atoi(token);
				
				token = strtok(NULL,delimiter);
				Lcol = atof(token);
				
				token = strtok(NULL,delimiter);
				Lrow = atof(token);
				
				token = strtok(NULL,delimiter);
				Rcol = atof(token);
				
				token = strtok(NULL,delimiter);
				Rrow = atof(token);
				
				m_LTie.InsertICP(Lcol,Lrow,id);
				m_RTie.InsertICP(Rcol,Rrow,id);
			}
		}
	}
		
	//fclose(prjfile);
	infile.close();

	return true;
}

bool CProjectManage::SaveProject(char* filename)
{
	// TODO: Add your control notification handler code here
	CString st="";
	CString temp;
	//Title and Project Name
	st = _T("[OPTPRJ10]\r\n");
	st += m_stProjectName;
	st += _T("\r\n");
	//Sensor Type
	temp.Format("%d",(int)m_SensorType);
	st += temp;
	st += _T(" ");
	st += m_stSensorTypeName;
	st += _T("\r\n");
	//Sensor Model Type
	temp.Format("%d",(int)m_SensorModel);
	st += temp;
	st += _T(" ");
	st += m_stSensorModelName;
	st += _T("\r\n");
	//Reference System
	temp.Format("%d",(int)m_CoordSystem);
	st += temp;
	st += _T(" ");
	st += m_CoordSystem;
	st += _T("\r\n");
	//Image 1(Left) Path
	st += m_LeftPath;
	st += _T("\r\n");
	//Image Format 1
	temp.Format("%d",(int)m_LFormat);
	st += temp;
	st += _T(" ");
	if(0 == m_LFormat)
	{
		st += _T("TIF\r\n");
	}
	else if(1 == m_LFormat)
	{
		st += _T("RAW\r\n");
	}
	else
	{
		st += _T("\r\n");
	}
	//Image 2(Right) Path
	st += m_RightPath;
	st += _T("\r\n");
	//Image Format 2
	temp.Format("%d",(int)m_RFormat);
	st += temp;
	st += _T(" ");
	if(0 == m_RFormat)
	{
		st += _T("TIF\r\n");
	}
	else if(1 == m_RFormat)
	{
		st += _T("RAW\r\n");
	}
	else
	{
		st += _T("\r\n");
	}
	//GCP
	temp.Format("GCP %d\r\n",(int)m_bGCP);
	st += temp;
	//Tie
	temp.Format("Tie %d\r\n",(int)m_bTie);
	st += temp;
	//DEM
	temp.Format("DEM %d\r\n%s\r\n",(int)m_bDEM, m_stDEMPath);
	st += temp;
	//Ortho
	temp.Format("Ortho %d\r\n%s\r\n",(int)m_bOrtho, m_stOrthoPath);
	st += temp;
	//Epipolar
	temp.Format("Epipolar %d\r\n%s\r\n%s\r\n",(int)m_bEpipolar, m_stEpiLeftPath, m_stEpiRightPath);
	st += temp;
	//Mapping
	temp.Format("Mapping %d\r\n%s\r\n",(int)m_bMapping, m_stMapPath);
	st += temp;
		
	//GCP Data
	if(m_bGCP == true)
	{
		
		temp.Format("%d\r\n",m_LICP.GetICPNum());
		st += temp;
		for(int i=0; i<m_LICP.GetICPNum(); i++)
		{
			double col, row;
			double x, y, z;
			int id;
			id = m_LICP.GetICPID(i+1);
			m_LICP.GetICPCoord(id,&col,&row);
			m_LGCP.GetGCPCoord(id,&x,&y,&z);
			temp.Format("%d\t%lf\t%lf\t%lf\t%lf\t%lf\r\n",id,col,row,x,y,z);
			st += temp;
		}
			
		temp.Format("%d\r\n",m_RICP.GetICPNum());
		st += temp;
		for(i=0; i<m_RICP.GetICPNum(); i++)
		{
			double col, row;
			double x, y, z;
			int id;
			id = m_RICP.GetICPID(i+1);
			m_RICP.GetICPCoord(id,&col,&row);
			m_RGCP.GetGCPCoord(id,&x,&y,&z);
			temp.Format("%d\t%lf\t%lf\t%lf\t%lf\t%lf\r\n",id,col,row,x,y,z);
			st += temp;
		}
	}

	//Tie Point Data
	if(m_bTie == true)
	{
		temp.Format("%d\r\n",m_LTie.GetICPNum());
		st += temp;
		for(int i=0; i<m_LTie.GetICPNum(); i++)
		{
			double Lcol, Lrow;
			double Rcol, Rrow;
			int id;
			id = m_LTie.GetICPID(i+1);
			m_LTie.GetICPCoord(id,&Lcol,&Lrow);
			m_RTie.GetICPCoord(id,&Rcol,&Rrow);
			temp.Format("%d\t%lf\t%lf\t%lf\t%lf\r\n",id,Lcol,Lrow,Rcol,Rrow);
			st += temp;
		}
	}
		
	CFile prjfile;
	prjfile.Open((LPSTR)filename,CFile::modeCreate | CFile::modeReadWrite, NULL);
	prjfile.Write(st.GetBuffer(0),st.GetLength());
	prjfile.Close();

	return true;
}