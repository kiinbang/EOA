/////////////////////////////////////////////////////////////////////
//Coord2DManager.h
//made by BangBaraBang
/////////////////////////////////////////////////////////////////////
//revision: 2002-04-20
///////////////////////////////////////////////////////////////////////
//CCoord2DManager: 2D coordinates management class
//CCoordinate2D: 2D coordinates class
/////////////////////////////////////////////////////////////////////////

#if !defined(_SpaceMatics_bbarab_Coord2DManager_)
#define SpaceMatics_bbarab_Coord2DManager

/************************/
/*2D Coordinates class*/
/************************/
//template<class T>
class CCoordinate2D
{
public:
	CCoordinate2D()
	{
		next = NULL;
		forward = NULL;
		Init();
	}

	CCoordinate2D(CCoordinate2D& copy)
	{
		next = copy.next;
		forward = copy.forward;
		x = copy.x;
		y = copy.y;
		available = copy.available;
	}

	~CCoordinate2D() {}

	//substitution operator
	void operator=(CCoordinate2D& copy)
	{
		next = copy.next;
		forward = copy.forward;
		x = copy.x;
		y = copy.y;
		available = copy.available;
	}

//Member variable
public:
	//column
	double x;
	//row
	double y;

	long ID;
	
	bool available;
	CCoordinate2D *next;
	CCoordinate2D *forward;
public:

	void Init(void)
	{
		available = true;
		x = y = (double)0;
		ID = 0;
	}

};

/******************************/
/*2D Coordinates manager class*/
/******************************/
//template<class T>
class CCoord2DManager
{

//Member variable
private:
	long num_point;
	CCoordinate2D *firstpoint;
	CCoordinate2D *nextpoint;

public:
	bool normalization;
	double Scale_I_X, Scale_I_Y;
	double Offset_I_X, Offset_I_Y;

public:
	//constructor
	CCoord2DManager()
	{
		num_point = 0;
		nextpoint = NULL;
		firstpoint = NULL;
		normalization = false;
	}

	//copy constructor
	CCoord2DManager(CCoord2DManager& copy)
	{
		num_point = 0;
		nextpoint = NULL;
		firstpoint = NULL;
		normalization = false;

		CCoordinate2D* pointbag;
		pointbag = copy.firstpoint;
	
		for(long i=0;i<copy.GetNumOfPoint();i++)
		{
			//Insert 함수를 이용한 ICP복사
			InsertPoint(pointbag->x, pointbag->y, pointbag->ID);
			//enable 복사
			SetAvailable(pointbag->ID,pointbag->available);
			pointbag = pointbag->next;
		}
	}

	//destructor
	virtual ~CCoord2DManager() { Empty(); };

//Member function
public:

	//insert point data
	bool InsertPoint(double X, double Y, long ID)
	{
		if(num_point == 0)
		{
			nextpoint = new CCoordinate2D;
			nextpoint->forward = NULL;
			firstpoint = nextpoint;
		}
		
		num_point++;
		nextpoint->x = X;
		nextpoint->y = Y;
		nextpoint->ID = ID;
		nextpoint->next = new CCoordinate2D;
		nextpoint->next->forward = new CCoordinate2D;
		nextpoint->next->forward = nextpoint;
		nextpoint = nextpoint->next;
		nextpoint->next = NULL;
		
		return true;
	}
	
	//delete point data
	bool DeletePoint(long ID)
	{
		CCoordinate2D* targetpoint = GetPointHandle(ID);
		if(targetpoint != NULL)
		{
			if(targetpoint->forward == NULL)
			{
				targetpoint->next->forward = NULL;
				firstpoint = targetpoint->next;
				delete targetpoint;
			}
			else
			{
				targetpoint->forward->next = targetpoint->next;
				targetpoint->next->forward = targetpoint->forward;
				delete targetpoint;
			}

			num_point--;
		
			return true;
		}
		return false;
	}

	//Get Point Coordinates by sort number
	bool GetCoordbySort(long sort, double *X, double *Y)
	{
		CCoordinate2D *targetpoint = NULL;
		
		targetpoint = GetPointHandle(GetID(sort));
		if(targetpoint != NULL)
		{
			*X = targetpoint->x;
			*Y = targetpoint->y;
		}
		else
		{
			return false;
		}
		
		return true;
	}

	//Get Point Coordinates by ID
	bool GetCoordbyID(long ID, double *X, double *Y)
	{
		CCoordinate2D *targetpoint = NULL;
		
		targetpoint = GetPointHandle(ID);
		if(targetpoint != NULL)
		{
			*X = targetpoint->x;
			*Y = targetpoint->y;
		}
		else
		{
			return false;
		}
		
		return true;
	}
	
	//Set Point Coordinates by ID
	bool SetCoord(long ID, double X, double Y)
	{
		CCoordinate2D *targeticp = NULL;
		
		targeticp = GetPointHandle(ID);
		if(targeticp != NULL)
		{
			targeticp->x = X;
			targeticp->y = Y;
		}
		else
		{
			return false;
		}
		
		return true;
	}

	//Get ID by sort
	long GetID(long sort)
	{
		CCoordinate2D *targetpoint;
		targetpoint = firstpoint;
		
		for(long i=0; i<sort; i++)
		{
			targetpoint = targetpoint->next;
		}
		
		return targetpoint->ID;
	}
	
	//Get ID by coord
	bool GetID(double X, double Y, long *ID)
	{
		CCoordinate2D *targetpoint;
		targetpoint = firstpoint;
		
		for(long i=0; i<num_point; i++)
		{
			if((X==targetpoint->x)&&(Y==targetpoint->y))
			{
				*ID = targetpoint->ID;
				return true;
			}

			targetpoint = targetpoint->next;
		}
		
		return false;
	}

	//set availability of point by ID
	bool SetAvailable(long ID, bool boolvalue)
	{
		CCoordinate2D *targetpoint = NULL;
		
		targetpoint = GetPointHandle(ID);
		if(targetpoint != NULL)
		{
			targetpoint->available = boolvalue;
		}
		else
		{
			return false;
		}
		
		return true;
	}

	//get availability of point by ID
	bool GetAvailable(long ID)
	{
		CCoordinate2D *targetpoint = NULL;
		
		targetpoint = GetPointHandle(ID);
		if(targetpoint != NULL)
		{		
			return targetpoint->available;
		}
		else
		{
			return false;
		}
	}

	//empty buffer and data initialize
	bool Empty(void)
	{
		CCoordinate2D *targetpoint;
		
		//last point pointer
		if(num_point == 0)
		{
			return TRUE;
		}
		
		if(num_point == 1)
		{
			targetpoint = nextpoint->forward;
			delete targetpoint->next;
			delete targetpoint;
			targetpoint = NULL;
			num_point --;
			return true;
		}
		
		
		targetpoint = nextpoint;

		int count = num_point;
		for(long i=0; i<count; i++)
		{
			if(targetpoint != NULL)
			{
				delete targetpoint->next;
				targetpoint->next = NULL;
			}

			targetpoint = targetpoint->forward;
			num_point --;
		}
		
		delete targetpoint->next;
		delete targetpoint;
		
		return true;
	}

	//get number of point
	long GetNumOfPoint(void) {return num_point;}

	//get number of availabe point
	long GetAvailableNumOfPoint(void)
	{
		long num_disabledICP= 0;

		for(long i=0; i<num_point; i++)
		{
			bool value;
			value = GetAvailable(GetID(i));
			if(false == value)
				num_disabledICP++;
		}

		return (num_point - num_disabledICP);
	}

	//get normalized coordinates
	void GetNormalCoord(double x, double y, double &nx, double &ny)
	{
		nx = (x + Offset_I_X)*Scale_I_X;
		ny = (y + Offset_I_Y)*Scale_I_Y;
	}

	//ger un-normalized coordinates
	void GetUnNormalCoord(double nx, double ny, double &x, double &y)
	{
		x = (nx/Scale_I_X) - Offset_I_X;
		y = (ny/Scale_I_Y) - Offset_I_Y;
	}

	//coord normalization
	void CoordNormalize()
	{
		int i;
		double maxX, maxY;
		double minX, minY;
		
		//coordinates backup and get offset, scale (Image Coordinagtes)
		double X, Y;
		GetCoordbySort(0,&X,&Y);
		
		maxX = minX = X;
		maxY = minY = Y;
		
		for(i=0; i<num_point; i++)
		{
			GetCoordbySort(i,&X,&Y);
			
			if(maxX < X)
				maxX = X;
			if(minX > X)
				minX = X;
			
			if(maxY < Y)
				maxY = Y;
			if(minY > Y)
				minY = Y;
		}
		
		//Scale_I_X = 2/(maxX - minX);
		//Scale_I_Y = 2/(maxY - minY);
		//////////////////////////////////
		if((maxX - minX)<(maxY - minY))
		{
			Scale_I_X = Scale_I_Y = 2/(maxX - minX);
		}
		else
		{
			Scale_I_X = Scale_I_Y = 2/(maxY - minY);
		}
		//////////////////////////////////
		
		Offset_I_X = -(maxX + minX)/2;
		Offset_I_Y = -(maxY + minY)/2;
		
		for(i=0; i<num_point; i++)
		{
			CCoordinate2D *point = GetPointHandle(GetID(i));
			
			point->x = (point->x + Offset_I_X)*Scale_I_X;
			point->y = (point->y + Offset_I_Y)*Scale_I_Y;
		}
		
		normalization = true;
	}

	//set coordinate normalizaton parameters
	bool SetCoordNormalize(double scale, double OX, double OY)
	{
		Scale_I_X = Scale_I_Y = scale;
		
		Offset_I_X = OX;
		Offset_I_Y = OY;
		
		for(long i=0; i<num_point; i++)
		{
			CCoordinate2D *point = GetPointHandle(GetID(i));
			
			point->x = (point->x + Offset_I_X)*Scale_I_X;
			point->y = (point->y + Offset_I_Y)*Scale_I_Y;
		}
		
		normalization = true;
		
		return true;
	}


	//substitution operator
	void operator=(CCoord2DManager& copy)
	{
		Empty();
		num_point = 0;
		nextpoint = NULL;
		firstpoint = NULL;
		normalization = false;

		CCoordinate2D* pointbag;
		pointbag = copy.firstpoint;
	
		for(long i=0;i<copy.num_point;i++)
		{
			//Insert 함수를 이용한 ICP복사
			InsertPoint(pointbag->x, pointbag->y, pointbag->ID);
			//enable 복사
			SetAvailable(pointbag->ID,pointbag->available);
			pointbag = pointbag->next;
		}
	}

	//Member function
	CCoordinate2D* GetPointHandle(long ID)
	{
		CCoordinate2D* pointbag;
		pointbag = firstpoint;
		
		for(long i=0; i<num_point; i++)
		{
			if(ID == pointbag->ID)
			{
				return pointbag;
			}
			pointbag = pointbag->next;
		}
		
		return NULL;
	}

};

#endif // !defined(AFX_GCPMANAGE_H__19F4C1A5_855E_4046_9B84_50ABEED90325__INCLUDED_)
