// GCPWrap.cpp: implementation of the CGCPWrap class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GCPWrap.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
//#define new DEBUG_NEW <--MFC
#endif

#define CHECK(hr) if(FAILED(hr)) return hr

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGCPWrap::CGCPWrap()
{
	//AfxOleInit();
	HRESULT hr;
	hr = m_GCPs.CreateInstance(CLSID_GCPs,NULL);
}

CGCPWrap::~CGCPWrap()
{

}

VARIANT CGCPWrap::GetGCPInterface(void)
{
	VARIANT gcp;
	gcp.vt = VT_DISPATCH;
	HRESULT hr;
	hr = m_GCPs->QueryInterface(IID_IGCPs,(void**)&gcp.pdispVal);

	return gcp;
}

bool CGCPWrap::SetGCPInterface(VARIANT gcp)
{

	gcp.vt = VT_DISPATCH;
	m_GCPs = gcp.pdispVal;

	return true;
}

bool CGCPWrap::InsertGCP(double X, double Y, double Z, double Col, double Row, long ID)
{
	IGCPDataPtr GCP;
	VARIANT V_GCP;
	
	HRESULT hr;
	hr = GCP.CreateInstance(CLSID_GCPData,NULL);
	if(FAILED(hr))
		return false;

	GCP->X = X;
	GCP->Y = Y;
	GCP->Z = Z;
	GCP->Col = Col;
	GCP->Row = Row;
	GCP->ID = ID;

	V_GCP.vt = VT_DISPATCH;
	hr = GCP->QueryInterface(IID_IDispatch,(void**)&V_GCP.pdispVal);
	if(FAILED(hr))
		return false;

	m_GCPs->InsertGCP(V_GCP);

	return true;
}

bool CGCPWrap::GetGCPbyID(long ID, double *X, double *Y, double *Z, double *Col, double *Row)
{
	VARIANT V_GCP;

	V_GCP = m_GCPs->GetGCPbyID(ID);
	
	V_GCP.vt = VT_DISPATCH;

	if(NULL == V_GCP.pdispVal)
	{
		return false;
	}

	IGCPDataPtr GCP = V_GCP.pdispVal;
	
	*X = GCP->X;
	*Y = GCP->Y;
	*Z = GCP->Z;

	*Col = GCP->Col;
	*Row = GCP->Row;

	return true;
}

bool CGCPWrap::GetGCPbySort(long sort, double *X, double *Y, double *Z, double *Col, double *Row)
{
	VARIANT V_GCP;

	V_GCP = m_GCPs->GetGCPbySort(sort);
	
	V_GCP.vt = VT_DISPATCH;

	if(NULL == V_GCP.pdispVal)
	{
		return false;
	}

	IGCPDataPtr GCP = V_GCP.pdispVal;
	
	*X = GCP->X;
	*Y = GCP->Y;
	*Z = GCP->Z;

	*Col = GCP->Col;
	*Row = GCP->Row;

	return true;
}

bool CGCPWrap::GetGCPbySort(long sort, long *ID, double *X, double *Y, double *Z, double *Col, double *Row)
{
	VARIANT V_GCP;

	V_GCP = m_GCPs->GetGCPbySort(sort);
	
	V_GCP.vt = VT_DISPATCH;

	if(NULL == V_GCP.pdispVal)
	{
		return false;
	}

	IGCPDataPtr GCP = V_GCP.pdispVal;
	
	*X = GCP->X;
	*Y = GCP->Y;
	*Z = GCP->Z;

	*Col = GCP->Col;
	*Row = GCP->Row;

	*ID = GCP->ID;

	return true;
}

long CGCPWrap::GetGCPNum()
{
	long num;

	num = m_GCPs->GetGCPNum();
	
	return num;
}

bool CGCPWrap::DeleteGCPbyID(long ID)
{
	if(S_OK != m_GCPs->DeleteGCPbyID(ID))
		return false;

	return true;
}

bool CGCPWrap::DeleteGCPbySort(long sort)
{
	if(S_OK != m_GCPs->DeleteGCPbySort(sort))
		return false;

	return true;
}

bool CGCPWrap::EmptyGCP(void)
{
	if(S_OK != m_GCPs->Init())
		return false;

	return true;
}



