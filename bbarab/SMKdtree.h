/*
 * Copyright (c) 2000-2003, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#if !defined(__SPACEMATICS_SMKDTREE_H__)
#define __SPACEMATICS_SMKDTREE_H__

/**
*@class DATA
*@brief data template class.
*/
template <typename T>
class TREEDATA
{
public:
	TREEDATA()
	{
		forward = NULL;
		next = NULL;
		nID = -1;
	}

	virtual ~TREEDATA() {}

	TREEDATA<T>* Parent ;
	TREEDATA<T>* Left ;
	TREEDATA<T>* Right ;
	int nID;
	T value;/**<this place data*/
	bool checked;
	bool orientation;
	int sd;
	int axis ;
};


/**@class CSMKDTree
*@brief template class for list data[class DATA] management.
*/
template<typename T>
class CSMKDTree
{
public:
	/**constuctor*/
	CSMKDTree()
	{
		first = NULL;
		next = NULL;
		m_nNumData = 0;
	}

	/**copy constructor*/
	CSMKDTree(CSMKDTree& copy)
	{
		m_nNumData = 0;
		next = NULL;
		first = NULL;
			
		TREEDATA<T> *pos = NULL;
		for(unsigned long i=0; i<copy.m_nNumData; i++)
		{
			//AddTail() 함수를 이용한 복사
			//AddTail(copy.GetAt(i));
			pos = copy.GetNextPos(pos);
			T value = pos->value;
			AddTail(value);
		}
	}

	/**destructor*/
	virtual ~CSMKDTree()
	{
		RemoveAll();
	}

private:
	//member variable
	TREEDATA<T> *first;/**<pointer of first data*/
	TREEDATA<T> *next; /**<pointer of next data*/
	unsigned long m_nNumData; /**<number of data(elements)*/

//member function
public:
	/**Get number of item*/
	unsigned long GetNumItem()
	{
		return m_nNumData;
	}

	/**Set void data*/
	void SetVoidData(unsigned int num)
	{
		T* data;
		data = new T;
		for(unsigned int i=0; i<num; i++) AddTail(*data);
	}
	/**fill the data with default value*/
	void SetVoidData(unsigned int num, T data)
	{
		for(unsigned int i=0; i<num; i++) AddTail(data);
	}

	/**Add data to tail*/
	bool AddTail(T data)
	{
		if(m_nNumData == 0)
		{
			next = new TREEDATA<T>;
			next->forward = NULL;
			first = next;
		}
		
		m_nNumData++;
		next->value = data;
		next->next = new DATA<T>;
		//next->next->forward = new DATA<T>;
		next->next->forward = next;
		next = next->next;
		next->next = NULL;
		return true;
	}
	
	/**Add data to head*/
	bool AddHead(T data)
	{
		if(m_nNumData == 0)
		{
			next = new TREEDATA<T>;
			next->forward = NULL;
			first = next;
			first->value = data;
			first->next = new TREEDATA<T>;
			//next->next->forward = new DATA<T>;
			first->next->forward = first;
			next = first->next;
			next->next = NULL;
			m_nNumData++;
		}
		else
		{
			DATA<T> *newfirst = new TREEDATA<T>;
			newfirst->value = data;
			newfirst->forward = NULL;
			first->forward = newfirst;
			newfirst->next = first;
			first = newfirst;
			m_nNumData++;
		}
		
		return true;
	}

	/**Delete item by index which starts from #0*/
	bool DeleteData(unsigned long sort)
	{
		if(sort >= m_nNumData)
			return false;

		TREEDATA<T>* target;
		target = GetDataHandle(sort);
		
		if(target != NULL)
		{
			if(target->forward == NULL)//첫번째 데이터 인경우
			{
				target->next->forward = NULL;
				first = target->next;
				delete target;
			}
			else
			{
				target->forward->next = target->next;
				target->next->forward = target->forward;
				delete target;
			}
			m_nNumData--;
			return true;
		}

		return false;
	}

	/**Insert item by index which starts from #0*/
	bool InsertData(unsigned long sort, T data)
	{
		if(sort > m_nNumData)
			return false;

		if(sort==0) {AddHead(data); return true;}
		if(sort == m_nNumData){AddTail(data); return true;}
		
		//forward link
		TREEDATA<T>* target1;
		target1 = GetDataHandle(sort-1);

		//backward link
		TREEDATA<T>* target2;
		target2 = GetDataHandle(sort);
		
		//new link
		TREEDATA<T> *newlink = new TREEDATA<T>;
		newlink->value = data;

		//insert
		target1->next = newlink;
		newlink->forward = target1;

		target2->forward = newlink;
		newlink->next = target2;

		m_nNumData++;

		return true;
	}

	/**Empty list*/
	bool RemoveAll()
	{
		unsigned long i, num;
		num = m_nNumData;

		if(num != 0)
		{
			TREEDATA<T>* target0 = first;
			TREEDATA<T>* target1 = first;
			
			for(i=0; i<num-1; i++)
			{
				target1 = GetNextPos(target0);
				
				delete target0;
				
				target0 = target1;
				
				m_nNumData --;
			}
			
			delete target1;
			m_nNumData --;
		}

		return true;
	}

	/**Remove first data*/
	bool RemoveHead(void)
	{
		TREEDATA<T>* target;
		target = first;
		target->next->forward = NULL;
		first = target->next;
		delete target;
		m_nNumData--;
		return true;
	}
	
	/**Remove last data*/
	bool RemoveTail(void)
	{
		TREEDATA<T>* target;
		target = GetDataHandle(m_nNumData-1);
		if(target->forward == NULL)//첫번째 데이터 인경우
		{
			target->next->forward = NULL;
			first = target->next;
			delete target;
		}
		else
		{
			target->forward->next = target->next;
			target->next->forward = target->forward;
			delete target;
		}
		
		m_nNumData--;
		return true;
	}

	/**Get specific data by index(sort)*/
	void SetIDAt(unsigned long sort, int ID)
	{
		TREEDATA<T>* target;
		target = first;
		for(unsigned long i=0; i<sort; i++)
		{
			target = target->next;
		}

		target->ID = ID;
	}

	/**Get specific data by index(sort)*/
	int GetIDAt(unsigned long sort)
	{
		TREEDATA<T>* target;
		target = first;
		for(unsigned long i=0; i<sort; i++)
		{
			target = target->next;
		}

		return target->ID;
	}

	T GetAt(unsigned long sort)
	{
		TREEDATA<T>* target;
		
		target = first;
		
		for(unsigned long i=0; i<sort; i++)
		{
			target = target->next;
		}
		
		return target->value;
	}

	/**Get the next post*/
	TREEDATA<T>* GetNextPos(TREEDATA<T>* pos)
	{
		if(pos == NULL)
		{
			return first;
		}
		else
		{			
			TREEDATA<T>* target;
			target = pos->next;
			return target;
		}
	}

	/**Get the first position*/
	TREEDATA<T>* GetFirstPos()
	{
		return first;
	}

	/**Get specific handle by index(sort)*/
	T* GetHandleAt(unsigned long sort)
	{
		TREEDATA<T>* target;
		target = first;
		for(unsigned long i=0; i<sort; i++)
		{
			target = target->next;
		}
		
		return &(target->value);
	}

	/**Set specific data by index(sort)*/
	bool SetAt(unsigned long sort, T newvalue)
	{
		TREEDATA<T> *target = GetDataHandle(sort);
		target->value = newvalue;
		return true;
	}

	/**Get first data*/
	T GetHead()
	{
		TREEDATA<T>* target;
		target = first;
		return target->value;
	}

	/**Get last data*/
	T GetTail()
	{
		TREEDATA<T>* target;
		target = first;
		for(unsigned long i=0; i<(m_nNumData-1); i++)
		{
			target = target->next;
		}

		return target->value;
	}

	/**[] operator overloading*/
	T& operator [] (unsigned long sort)
	{
		TREEDATA<T> *target = GetDataHandle(sort);
		return target->value;
	}

	/**= operator overloading*/
	void operator=(CSMKDTree& copy)
	{
		RemoveAll();
		m_nNumData = 0;
		next = NULL;
		first = NULL;
		
		TREEDATA<T> *pos = NULL;
		for(unsigned long i=0; i<copy.m_nNumData; i++)
		{
			//AddTail() 함수를 이용한 복사
			//AddTail(copy.GetAt(i));
			pos = copy.GetNextPos(pos);
			T value = pos->value;
			AddTail(value);
		}
	}

private:
	/**Get DATA handle*/
	TREEDATA<T>* GetDataHandle(unsigned long sort)
	{
		TREEDATA<T> *target;
		target = first;
		
		for(unsigned long i=0; i<sort; i++)
		{
			target = target->next;
		}
		return target;
	}
};

#endif // !defined(__SPACEMATICS_SMKDTREE_H__)
