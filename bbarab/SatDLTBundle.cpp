// SatDLTBundle.cpp: implementation of the CSatDLTBundle class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SatDLTBundle.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSatDLTBundle::CSatDLTBundle()
{

}

CSatDLTBundle::~CSatDLTBundle()
{

}

bool CSatDLTBundle::DLTRO(CICPManage Licp, CICPManage Ricp)
{
	//Input ICP(image control points) data
	LICP = Licp;
	RICP = Ricp;

	//Verification icp
	bool retval = true;
	if(false == CheckTiePoint())
		return false;

	//Init approximation of DLT RO parameters
	if(false == InitDLTROParam())
		return false;
	return retval;
}

bool CSatDLTBundle::CheckTiePoint(CICPManage Licp, CICPManage Ricp)
{
	return true;
}

bool CSatDLTBundle::InitDLTROParam()
{
	return true;
}
