/*
* Copyright(c) 2008-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <iostream>

#include <SSM/SSMPhotoSimulation.h>
#include <SSM/include/SSMSMACModel.h>
#include <utilitygrocery_2.0.h>

CSMPhotoSimulation::CSMPhotoSimulation()
{
	ver = V10;
}

CSMPhotoSimulation::~CSMPhotoSimulation()
{
}

bool CSMPhotoSimulation::RunSimulation(const std::string& simpath, const std::string& outfilepath)
{
	srand(static_cast<unsigned int>(time(NULL)));

	ReadSimFile(simpath);

	/// Width and height of a target area
	double ground_W = (RB_X - LT_X), ground_H = (LT_Y - RB_Y);

	if (ground_W < std::numeric_limits<double>::epsilon())
		throw std::runtime_error("ground_W < std::numeric_limits<double>::epsilon()");

	if (ground_H < std::numeric_limits<double>::epsilon())
		throw std::runtime_error("ground_H < std::numeric_limits<double>::epsilon()");

	util::debugMsg(std::string("ground_W:\t" + std::to_string(ground_W)));
	util::debugMsg(std::string("ground_H:\t" + std::to_string(ground_H)));

	if (this->ver == V11 && regularDistribution)
	{
		/// Calculate number of points using width, height, and point density
		unsigned int num_points = int((ground_W*ground_H) / point_density);

		/// Calculate number of columns
		unsigned int cols = static_cast<unsigned int>(sqrt(ground_W / ground_H*num_points) + 0.5);

		/// Calculate number of rows
		unsigned int rows = static_cast<unsigned int>(cols * ground_H / ground_W + 0.5);

		std::string msg = std::string("[Ground point simulation for tie points]");
		util::debugMsg(msg);
		
		GroundPointSimulationRegular(rows, cols, this->Ground_tie_points, 0);

		msg = std::string("Ground_tie_points.size():\t" + std::to_string(Ground_tie_points.size()));
		util::debugMsg(msg);
		msg = std::string("cols:\t" + std::to_string(cols));
		util::debugMsg(msg);
		msg = std::string("rows:\t" + std::to_string(rows));
		util::debugMsg(msg);
	}
	else
	{
		unsigned int num_tie_points = static_cast<unsigned int>( (ground_W*ground_H) / point_density );
		GroundPointSimulation(num_tie_points, Ground_tie_points, 0);
	}

	AddRandomErrors_To_TIE();

	if (this->ver == V11 && regularDistribution)
	{
		/// Calculate number of columns
		unsigned int cols = static_cast<unsigned int>(sqrt(ground_W / ground_H*this->NumControl) + 0.5);

		/// Calculate number of rows
		unsigned int rows = static_cast<unsigned int>(cols * ground_H / ground_W + 0.5);

		util::debugMsg(std::string("[Ground point simulation for constrol points]"));

		GroundPointSimulationRegular(rows, cols, this->Ground_control_points, CONTROL_ID);

		util::debugMsg(std::string("Ground_control_points.size():\t" + std::to_string(Ground_control_points.size())));
		util::debugMsg(std::string("cols:\t" + std::to_string(cols)));
		util::debugMsg(std::string("rows:\t" + std::to_string(rows)));
	}
	else
	{
		GroundPointSimulation(this->NumControl, this->Ground_control_points, CONTROL_ID);
	}

	AddRandomError_To_Control();

	GPSINS_Simulation();

	AddRandomErrors_To_Trajectory();

	for (unsigned int i = 0; i<num_cam; i++)
	{
		EOP_Simulation(i, block, trajectory);
		EOP_Simulation(i, block_noise, trajectory_noise);

		ImgMeasurement_Simulation(i);
	}

	CheckMultipleCapturedTiePoints();

	CheckMultipleCapturedControlPoints();

	SaveSimulationData(outfilepath);

	return true;
}

bool CSMPhotoSimulation::ReadSimFile(const std::string& simpath)
{
	fstream infile;
	infile.open(simpath, ios::in);

	if (!infile)
	{
		throw std::runtime_error("File does not exist.");
	}

	util::debugMsg(std::string("Simulation configulation: \t" + simpath));

	if (util::VersionCheck(infile) == 1.1f)
		ver = V11;

	if (ver == V11)
	{
		int opt;
		/// option: random distribution
		util::RemoveCommentLine(infile, '#');
		infile >> opt;

		if (opt > 0)
			regularDistribution = true;
		else
			regularDistribution = false;

		if(regularDistribution)
			util::debugMsg(std::string("Option: Ground points are regularly distributed."));
		else
			util::debugMsg(std::string("Option: Ground points are randomly distributed"));
	}

	util::RemoveCommentLine(infile, '#');
	infile >> num_cam;

	if (num_cam > 0)
	{
		cam.resize(2);

		camera_id.resize(num_cam);
		LA_BS.resize(num_cam);
		block.resize(num_cam);
		block_noise.resize(num_cam);
	}
	else
	{
		return false;
	}

	for (unsigned int i = 0; i<num_cam; i++)
	{
		double camera_c;
		data::Point2D xpyp;
		double pixel_size;
		unsigned int width, height;

		char temp[512];

		util::RemoveCommentLine(infile, '#');
		infile.getline(temp, 512);
		camera_id[i] = temp;

		util::RemoveCommentLine(infile, '#');
		infile >> camera_c;

		util::RemoveCommentLine(infile, '#');
		infile >> xpyp(0);

		util::RemoveCommentLine(infile, '#');
		infile >> xpyp(1);

		util::RemoveCommentLine(infile, '#');
		infile >> pixel_size;

		double k[4], p[3];
		if (ver == V11)
		{
			util::RemoveCommentLine(infile, '#');

			for (unsigned int j = 0; j < 4; ++j)
				infile >> k[j];///radial lens distortion parameters of SMAC

			for (unsigned int j = 0; j < 3; ++j)
				infile >> p[j];///decentring lens distortion parameters of SMAC
		}

		util::RemoveCommentLine(infile, '#');
		infile >> width;///pixels

		util::RemoveCommentLine(infile, '#');
		infile >> height;///pixels

		util::RemoveCommentLine(infile, '#');
		infile >> LA_BS[i].pos[0];

		util::RemoveCommentLine(infile, '#');
		infile >> LA_BS[i].pos[1];

		util::RemoveCommentLine(infile, '#');
		infile >> LA_BS[i].pos[2];

		util::RemoveCommentLine(infile, '#');
		infile >> LA_BS[i].ori[0];
		LA_BS[i].ori[0] = util::Deg2Rad(LA_BS[i].ori[0]);

		util::RemoveCommentLine(infile, '#');
		infile >> LA_BS[i].ori[1];
		LA_BS[i].ori[1] = util::Deg2Rad(LA_BS[i].ori[1]);

		util::RemoveCommentLine(infile, '#');
		infile >> LA_BS[i].ori[2];
		LA_BS[i].ori[2] = util::Deg2Rad(LA_BS[i].ori[2]);

		std::shared_ptr<sensor::DistortionModel> distortion(new sensor::SMACModel(k, p));
		cam[i].reset(new sensor::FrameCamera(i, camera_c, xpyp, width, height, pixel_size, distortion));
	}

	util::RemoveCommentLine(infile, '#');
	infile >> LT_X; LT_X -= REGIONMARGIN;

	util::RemoveCommentLine(infile, '#');
	infile >> LT_Y; LT_Y += REGIONMARGIN;

	util::RemoveCommentLine(infile, '#');
	infile >> RB_X; RB_X += REGIONMARGIN;

	util::RemoveCommentLine(infile, '#');
	infile >> RB_Y; RB_Y -= REGIONMARGIN;

	util::RemoveCommentLine(infile, '#');
	infile >> min_ground_H;

	util::RemoveCommentLine(infile, '#');
	infile >> max_ground_H;

	/// Note that its unit is meter^2/point; it is a inverse of points/meter^2 (number of points per square meter)
	util::RemoveCommentLine(infile, '#');
	infile >> point_density;

	util::RemoveCommentLine(infile, '#');
	infile >> num_flight_lines;

	time_offset.resize(num_flight_lines);
	num_images.resize(num_flight_lines);
	time_gap.resize(num_flight_lines);
	trajectory_parameter.resize(num_flight_lines);

	for (unsigned int i = 0; i<num_flight_lines; i++)
	{
		util::RemoveCommentLine(infile, '#');
		infile >> time_offset[i];

		util::RemoveCommentLine(infile, '#');
		infile >> num_images[i];

		util::RemoveCommentLine(infile, '#');
		infile >> time_gap[i];

		for (unsigned int j = 0; j<4; j++)
		{
			util::RemoveCommentLine(infile, '#');
			infile >> trajectory_parameter[i].a[j];
		}

		for (unsigned int j = 0; j<4; j++)
		{
			util::RemoveCommentLine(infile, '#');
			infile >> trajectory_parameter[i].b[j];
		}

		for (unsigned int j = 0; j<4; j++)
		{
			util::RemoveCommentLine(infile, '#');
			infile >> trajectory_parameter[i].c[j];
		}

		for (unsigned int j = 0; j<4; j++)
		{
			util::RemoveCommentLine(infile, '#');
			infile >> trajectory_parameter[i].d[j];
			trajectory_parameter[i].d[j] = util::Deg2Rad(trajectory_parameter[i].d[j]);
		}

		for (unsigned int j = 0; j<4; j++)
		{
			util::RemoveCommentLine(infile, '#');
			infile >> trajectory_parameter[i].e[j];
			trajectory_parameter[i].e[j] = util::Deg2Rad(trajectory_parameter[i].e[j]);
		}

		for (unsigned int j = 0; j<4; j++)
		{
			util::RemoveCommentLine(infile, '#');
			infile >> trajectory_parameter[i].f[j];
			trajectory_parameter[i].f[j] = util::Deg2Rad(trajectory_parameter[i].f[j]);
		}
	}

	util::RemoveCommentLine(infile, '#');
	infile >> sX;

	util::RemoveCommentLine(infile, '#');
	infile >> sY;

	util::RemoveCommentLine(infile, '#');
	infile >> sZ;

	util::RemoveCommentLine(infile, '#');
	infile >> sO;
	sO = util::Deg2Rad(sO);

	util::RemoveCommentLine(infile, '#');
	infile >> sP;
	sP = util::Deg2Rad(sP);

	util::RemoveCommentLine(infile, '#');
	infile >> sK;
	sK = util::Deg2Rad(sK);

	util::RemoveCommentLine(infile, '#');
	infile >> scol;///pixel

	util::RemoveCommentLine(infile, '#');
	infile >> srow;///pixel

	util::RemoveCommentLine(infile, '#');
	infile >> NumControl;///Num control points

	util::RemoveCommentLine(infile, '#');
	infile >> Noise_GCP_X;///meter

	util::RemoveCommentLine(infile, '#');
	infile >> Noise_GCP_Y;///meter

	util::RemoveCommentLine(infile, '#');
	infile >> Noise_GCP_Z;///meter

	util::RemoveCommentLine(infile, '#');
	infile >> Error_TIE_X;///meter

	util::RemoveCommentLine(infile, '#');
	infile >> Error_TIE_Y;///meter

	util::RemoveCommentLine(infile, '#');
	infile >> Error_TIE_Z;///meter

	return true;

}

void CSMPhotoSimulation::GroundPointSimulation(const unsigned int numPoints, std::vector<GroundPoint>& groundPoints, const unsigned int idGain)
{
	util::debugMsg(std::string("Ground point simulation"));

	srand((unsigned)time(NULL));

	double ground_W = (RB_X - LT_X), ground_H = (LT_Y - RB_Y), ground_D = (max_ground_H - min_ground_H);

	groundPoints.resize(numPoints);

	for (unsigned int i = 0; i<numPoints; i++)
	{
		groundPoints[i].bConjugate = false;
		groundPoints[i](0) = (GetRandNum() + 1.0)*ground_W / 2.0 + LT_X;
		groundPoints[i](1) = (GetRandNum() + 1.0)*ground_H / 2.0 + RB_Y;
		groundPoints[i](2) = (GetRandNum() + 1.0)*ground_D / 2.0 + min_ground_H;
		groundPoints[i].ID = idGain + i;
	}
}

void CSMPhotoSimulation::GroundPointSimulationRegular(const unsigned int rows, const unsigned int cols, std::vector<GroundPoint>& groundPoints, const unsigned int idGain)
{
	util::debugMsg(std::string("Ground point simulation (regular distribution)"));

	unsigned int numPoints = cols * rows;
	groundPoints.reserve(numPoints);

	/// Calculate a height gap
	unsigned int hSteps;
	if (numPoints > 100)
		hSteps = 30;
	else
		hSteps = 10;

	double ground_D = (max_ground_H - min_ground_H);

	double hgap = ground_D / static_cast<double>(hSteps - 1);

	/// Caculate a grid size using the point density
	double gap = sqrt(point_density);
	
	util::debugMsg(std::string("height interval:\t") + std::to_string(hgap));
	util::debugMsg(std::string("height steps:\t") + std::to_string(hSteps));
	util::debugMsg(std::string("Min Z coordinate:\t") + std::to_string(min_ground_H));
	util::debugMsg(std::string("planimetric interval:\t") + std::to_string(gap));
	util::debugMsg(std::string("Left X coordinate:\t") + std::to_string(LT_X));
	util::debugMsg(std::string("Bottom Y coordinate:\t") + std::to_string(RB_Y));

	for (unsigned int r = 0; r < rows; ++r)
	{
		for (unsigned int c = 0; c < cols; ++c)
		{
			GroundPoint gpt;

			gpt.bConjugate = false;
			gpt(0) = c*gap + LT_X;
			gpt(1) = r*gap + RB_Y;
			unsigned int count = r*cols + c;
			gpt(2) =  static_cast<double>(count%hSteps)*hgap + min_ground_H;
			gpt.ID = count;

			groundPoints.push_back(gpt);
		}
	}
}

void CSMPhotoSimulation::GPSINS_Simulation()
{
	srand((unsigned)time(NULL));

	TrjectoryParameters trj_param;
	double t_offset;
	double t_gap;
	double epoch_time;
	int num_img;

	trajectory.resize(num_flight_lines);

	for (unsigned int i = 0; i<num_flight_lines; i++)
	{
		trj_param = trajectory_parameter[i];
		t_offset = time_offset[i];
		num_img = num_images[i];
		t_gap = time_gap[i];

		epoch_time = 0.0;

		for (int j = 0; j<num_img; j++)
		{
			GPSINS epoch;

			///update epoch time
			epoch_time += t_gap;

			///update recored time (t_offset + epoch_time)
			epoch.t = t_offset + epoch_time;

			///trajectory is simulated using epoch_time (not epoch.t)
			epoch.pos[0] = trj_param.a[0]
				+ trj_param.a[1] * epoch_time
				+ trj_param.a[2] * pow(epoch_time, 2)
				+ trj_param.a[3] * pow(epoch_time, 3);

			epoch.pos[1] = trj_param.b[0]
				+ trj_param.b[1] * epoch_time
				+ trj_param.b[2] * pow(epoch_time, 2)
				+ trj_param.b[3] * pow(epoch_time, 3);

			epoch.pos[2] = trj_param.c[0]
				+ trj_param.c[1] * epoch_time
				+ trj_param.c[2] * pow(epoch_time, 2)
				+ trj_param.c[3] * pow(epoch_time, 3);

			epoch.ori[0] = trj_param.d[0]
				+ trj_param.d[1] * epoch_time
				+ trj_param.d[2] * pow(epoch_time, 2)
				+ trj_param.d[3] * pow(epoch_time, 3);

			epoch.ori[1] = trj_param.e[0]
				+ trj_param.e[1] * epoch_time
				+ trj_param.e[2] * pow(epoch_time, 2)
				+ trj_param.e[3] * pow(epoch_time, 3);

			epoch.ori[2] = trj_param.f[0]
				+ trj_param.f[1] * epoch_time
				+ trj_param.f[2] * pow(epoch_time, 2)
				+ trj_param.f[3] * pow(epoch_time, 3);

			trajectory[i].push_back(epoch);
		}
	}
}

void CSMPhotoSimulation::EOP_Simulation(unsigned int camera_index, std::vector<BlockData>& photoBlock, const std::vector<Trajectory>& trj)
{
	math::Matrix<double> Lever_Arm(3, 1);
	Lever_Arm(0, 0) = LA_BS[camera_index].pos[0];
	Lever_Arm(1, 0) = LA_BS[camera_index].pos[1];
	Lever_Arm(2, 0) = LA_BS[camera_index].pos[2];

	math::Matrix<double> Boresight = math::rotation::getRMat(LA_BS[camera_index].ori[0], LA_BS[camera_index].ori[1], LA_BS[camera_index].ori[2]);

	///[Definition]
	///G_vec = GPS_vec + Rins*LA + S*Rins*Boresight*IMG_vec
	///

	photoBlock[camera_index].resize(num_flight_lines);

	int num_img;
	Trajectory gpsins;
	GPSINS epoch;
	data::Point3D pos;
	StripData strip;
	GPSINS eop_j;
	Photo dumy_photo_j;
	math::Matrix<double> EOP_pos, EOP_ori;

	math::Matrix<double> Rins;

	int image_count = 0;

	for (unsigned int i = 0; i<num_flight_lines; i++)
	{
		gpsins = trj[i];
		num_img = num_images[i];

		strip.eop.resize(num_img);
		strip.photo.resize(num_img);

		for (int j = 0; j<num_img; j++)
		{
			epoch = gpsins[j];

			eop_j.t = epoch.t;

			pos(0) = epoch.pos[0];
			pos(1) = epoch.pos[1];
			pos(2) = epoch.pos[2];

			Rins = math::rotation::getRMat(epoch.ori[0], epoch.ori[1], epoch.ori[2]);

			EOP_pos = pos + Rins % Lever_Arm;///perspective center

			eop_j.pos[0] = EOP_pos(0);
			eop_j.pos[1] = EOP_pos(1);
			eop_j.pos[2] = EOP_pos(2);

			EOP_ori = Rins % Boresight;

			GetRotationfromMatrix(EOP_ori, eop_j.ori[0], eop_j.ori[1], eop_j.ori[2]);

			strip.eop[j] = eop_j;

			dumy_photo_j.ID = image_count + camera_index * 100000;
			strip.photo[j] = dumy_photo_j;
			image_count++;
		}

		photoBlock[camera_index][i] = strip;
	}
}

void CSMPhotoSimulation::ImgMeasurement_Simulation(int camera_index)
{
	int num_img;
	GPSINS eop_j;

	for (unsigned int i = 0; i<num_flight_lines; i++)
	{
		num_img = num_images[i];

		StripData& strip_i = block[camera_index][i];

		for (int j = 0; j<num_img; j++)
		{
			eop_j = strip_i.eop[j];
			Photo& photo_j = strip_i.photo[j];

			ImgMeasurement_Simulation(eop_j, photo_j, camera_index);

			/// copy block image measurements to block_noise
			/// Image measurement includes true and noise measurements
			/// Todo 001: seperate later; let block include only true simulation data
			/// and block_noise includes only noise-simulation data
			block_noise[camera_index][i] = block[camera_index][i];
		}
	}
}


void CSMPhotoSimulation::ImgMeasurement_Simulation(GPSINS& eop_j, Photo& photo_j, const int camera_index)
{
	srand((unsigned)time(NULL));

	///[Definition]
	///GP = EOP_pos + S*EOP_ori*IMG_vec
	///
	///S*IMG_vec = (EOP_ori_Trans)(GP - EOP_pos)

	photo_j.img_point.clear();
	photo_j.img_point_noise.clear();

	data::Point3D pos;
	pos(0) = eop_j.pos[0];
	pos(1) = eop_j.pos[1];
	pos(2) = eop_j.pos[2];

	math::Matrix<double> R = math::rotation::getRMat(eop_j.ori[0], eop_j.ori[1], eop_j.ori[2]);
	auto mmat = math::rotation::getMMat(eop_j.ori[0], eop_j.ori[1], eop_j.ori[2]);

	auto w = cam[camera_index]->getSensorSize()[0];
	auto h = cam[camera_index]->getSensorSize()[1];
	auto pitch = cam[camera_index]->getPixPitch();
	auto xRange = w * pitch * 0.5;
	auto yRange = h * pitch * 0.5;

	for (GroundPoint& point : Ground_control_points)
	{
		data::Point3D objPt = point;
		
		//data::Point2D photoPt = ssm::Collinearity::getMeasuredPhotoCoordinates(mmat, pos, cam[camera_index], objPt);
		auto refinedPhotoCoord = ssm::Collinearity::getUndistortedPhotoCoordinates(mmat, pos, cam[camera_index], objPt);
		if (xRange < fabs(refinedPhotoCoord(0)) || yRange < fabs(refinedPhotoCoord(1)))
			continue;
		auto photoPt = cam[camera_index]->getDistortedPhotoCoordFromRefinedPhotoCoord(refinedPhotoCoord);
						
		if (true == CheckPhotoCoordAvailability(photoPt, camera_index))
		{
			Point2DID imgP;
			imgP(0) = photoPt(0);
			imgP(1) = photoPt(1);
			imgP.ID = point.ID;
			photo_j.img_point.push_back(imgP);

			///Add noises
			Point2DID imgP_noise = imgP;
			imgP_noise(0) += GetRandNum() * scol*cam[camera_index]->getPixPitch();
			imgP_noise(1) += GetRandNum() * srow*cam[camera_index]->getPixPitch();

			photo_j.img_point_noise.push_back(imgP_noise);
		}
	}
	
	for (const GroundPoint& point : Ground_tie_points)
	{
		data::Point3D objPt = point;
		//data::Point2D photoPt = ssm::Collinearity::getMeasuredPhotoCoordinates(mmat, pos, cam[camera_index], objPt);
		auto refinedPhotoCoord = ssm::Collinearity::getUndistortedPhotoCoordinates(mmat, pos, cam[camera_index], objPt);
		if (xRange < fabs(refinedPhotoCoord(0)) || yRange < fabs(refinedPhotoCoord(1)))
			continue;
		auto photoPt = cam[camera_index]->getDistortedPhotoCoordFromRefinedPhotoCoord(refinedPhotoCoord);

		if (true == CheckPhotoCoordAvailability(photoPt, camera_index))
		{
			Point2DID imgP;
			imgP(0) = photoPt(0);
			imgP(1) = photoPt(1);
			imgP.ID = point.ID;

			photo_j.img_point.push_back(imgP);

			///Add noises
			Point2DID imgP_noise = imgP;
			imgP_noise(0) += GetRandNum() * scol*cam[camera_index]->getPixPitch();
			imgP_noise(1) += GetRandNum() * srow*cam[camera_index]->getPixPitch();

			photo_j.img_point_noise.push_back(imgP_noise);
		}
	}
}

double CSMPhotoSimulation::GetRandNum()
{
	double half_RAND_MAX = (double)RAND_MAX / 2.;
	double rand_num = (double)rand();///0 ~ RAND_MAX
	rand_num = rand_num / half_RAND_MAX - 1.0;///-1~1

	return rand_num;
}

void CSMPhotoSimulation::GetRotationfromMatrix(math::Matrix<double> R, double &Omg, double &Phi, double &Kap)
{
	/// from R-matrix

	Phi = asin(R(0, 2));
	Kap = atan2(-R(0, 1) / cos(Phi), R(0, 0) / cos(Phi));
	Omg = atan2(-R(1, 2) / cos(Phi), R(2, 2) / cos(Phi));
}

bool CSMPhotoSimulation::CheckPhotoCoordAvailability(data::Point2D P, int camera_index)
{
	if (fabs(P(0)) > cam[camera_index]->getSensorSize()(0) * cam[camera_index]->getPixPitch() / 2.0)
		return false;
	if (fabs(P(1)) > cam[camera_index]->getSensorSize()(1) * cam[camera_index]->getPixPitch() / 2.0)
		return false;

	return true;
}

bool CSMPhotoSimulation::FindImgPoint(Photo photo, int ID)
{
	for (const Point2DID& pt : photo.img_point)
		if (ID == pt.ID)
			return true;

	return false;
}

bool CSMPhotoSimulation::FindGroundPoint(int ID, GroundPoint &G, std::vector<GroundPoint> &Points)
{
	for (const GroundPoint& gpt : Points)
	{
		if (ID == gpt.ID)
		{
			G = gpt;
			return true;
		}
	}

	return false;
}

void CSMPhotoSimulation::CheckMultipleCapturedTiePoints()
{
	unsigned int i = 0;
	for (GroundPoint& point_i : Ground_tie_points)
	{
		unsigned int obs_count = 0;

		for (unsigned int camera_index = 0; camera_index<num_cam; camera_index++)
		{
			obs_count = 0;

			for (unsigned int j = 0; j<num_flight_lines; j++)
			{
				const StripData& strip_j = block[camera_index][j];

				for (unsigned int k = 0; k<num_images[j]; k++)
				{
					if (FindImgPoint(strip_j.photo[k], point_i.ID))
						obs_count++;

					if (obs_count >= 2)
						break;
				}

				if (obs_count >= 2)
					break;
			}

			if (obs_count >= 2)
				break;
		}

		if (obs_count >= 2)
		{
			point_i.bConjugate = true;
			Ground_tie_points_error[i].bConjugate = true;
		}

		++i;
	}
}

void CSMPhotoSimulation::CheckMultipleCapturedControlPoints()
{
	unsigned int num_points = static_cast<unsigned int>(Ground_control_points_error.size());
	unsigned int obs_count = 0;

	for (unsigned int i = 0; i<num_points; i++)
	{
		GroundPoint& point_i = Ground_control_points_error[i];

		for (unsigned int camera_index = 0; camera_index<num_cam; camera_index++)
		{
			obs_count = 0;

			unsigned int num_img;
			Photo photo_k;

			for (unsigned int j = 0; j<num_flight_lines; j++)
			{
				num_img = num_images[j];

				const StripData& strip_j = block[camera_index][j];

				for (unsigned int k = 0; k<num_img; k++)
				{
					photo_k = strip_j.photo[k];

					if (FindImgPoint(photo_k, point_i.ID))
					{
						obs_count++;
					}

					if (obs_count >= 2)
						break;
				}

				if (obs_count >= 2)
					break;
			}

			if (obs_count >= 2)
				break;
		}

		if (obs_count >= 2)
		{
			point_i.bConjugate = true;
			Ground_control_points[i].bConjugate = true;
		}
	}
}

void CSMPhotoSimulation::AddRandomErrors_To_Trajectory()
{
	srand((unsigned)time(NULL));

	///
	///GPSINS with noise
	///

	///copy from error free data
	trajectory_noise = trajectory;

	unsigned int num_tri = static_cast<unsigned int>(trajectory_noise.size());

	for (unsigned int i = 0; i<num_tri; i++)
	{
		for (GPSINS& epoch_j : trajectory_noise[i])
		{
			///Add noises
			epoch_j.pos[0] += GetRandNum() * sX;
			epoch_j.pos[1] += GetRandNum() * sY;
			epoch_j.pos[2] += GetRandNum() * sZ;

			epoch_j.ori[0] += GetRandNum() * sO;
			epoch_j.ori[1] += GetRandNum() * sP;
			epoch_j.ori[2] += GetRandNum() * sK;
		}
	}
}

void CSMPhotoSimulation::AddRandomErrors_To_TIE()
{
	srand((unsigned)time(NULL));

	///
	///tie points with noises
	///

	///copy from error free data
	Ground_tie_points_error = Ground_tie_points;

	for (GroundPoint& GP_i : Ground_tie_points_error)
	{
		GP_i(0) += GetRandNum() * Error_TIE_X;
		GP_i(1) += GetRandNum() * Error_TIE_Y;
		GP_i(2) += GetRandNum() * Error_TIE_Z;
	}
}

void CSMPhotoSimulation::AddRandomError_To_Control()
{
	srand((unsigned)time(NULL));

	///
	///control points with noises
	///

	///copy from error free data
	Ground_control_points_error = Ground_control_points;

	for (GroundPoint& GP_i : Ground_control_points_error)
	{
		GP_i(0) += GetRandNum() * Noise_GCP_X;
		GP_i(1) += GetRandNum() * Noise_GCP_Y;
		GP_i(2) += GetRandNum() * Noise_GCP_Z;
	}
}

void CSMPhotoSimulation::SaveSimulationData(const std::string& outfilepath)
{
	///gpsins file (noise free)
	std::string gpsins_path = outfilepath + ".gps";
	fstream gps_file; gps_file.setf(ios::fixed, ios::floatfield);
	gps_file.open(gpsins_path, ios::out);
	PrintOut_gpsins(gps_file, false);
	gps_file.close();

	///gpsins file (noise)
	std::string gpsins_noise_path = outfilepath + "_noise.gps";
	fstream gps_noise_file; gps_noise_file.setf(ios::fixed, ios::floatfield);
	gps_noise_file.open(gpsins_noise_path, ios::out);
	PrintOut_gpsins(gps_noise_file, true);
	gps_noise_file.close();

	///icf file (noise free)
	std::string icf_path = outfilepath + ".icf";
	fstream icf_file; icf_file.setf(ios::fixed, ios::floatfield);
	icf_file.open(icf_path, ios::out);
	PrintOut_icf(icf_file, false);
	icf_file.close();

	///icf file (noise)
	std::string icf_noise_path = outfilepath + "_noise.icf";
	fstream icf_noise_file; icf_noise_file.setf(ios::fixed, ios::floatfield);
	icf_noise_file.open(icf_noise_path, ios::out);
	PrintOut_icf(icf_noise_file, true);
	icf_noise_file.close();

	///gcp file (noise free)
	std::string gcp_path = outfilepath + ".gcp";
	fstream gcp_file; gcp_file.setf(ios::fixed, ios::floatfield);
	gcp_file.open(gcp_path, ios::out);
	PrintOut_gcp(gcp_file, false);
	gcp_file.close();

	///gcp file (noise)
	std::string gcp_noise_path = outfilepath + "_ERROR.gcp";
	fstream gcp_noise_file; gcp_noise_file.setf(ios::fixed, ios::floatfield);
	gcp_noise_file.open(gcp_noise_path, ios::out);
	PrintOut_gcp(gcp_noise_file, true);
	gcp_noise_file.close();

	///ori file
	std::string ORI_path = outfilepath + ".ori";
	fstream ORI_file; ORI_file.setf(ios::fixed, ios::floatfield);
	ORI_file.open(ORI_path, ios::out);
	PrintOut_ori(ORI_file, false);
	ORI_file.close();

	///ori file (noise)
	std::string ORI_noise_path = outfilepath + "_noise.ori";
	fstream ORI_noise_file; ORI_noise_file.setf(ios::fixed, ios::floatfield);
	ORI_noise_file.open(ORI_noise_path, ios::out);
	PrintOut_ori(ORI_noise_file, true);
	ORI_noise_file.close();

	///cam file
	std::string CAM_path = outfilepath + ".cam";
	fstream CAM_file; CAM_file.setf(ios::fixed, ios::floatfield);
	CAM_file.open(CAM_path, ios::out);
	PrintOut_cam(CAM_file);
	CAM_file.close();

	///Save true orientation
	std::string EOP_path = outfilepath + "_TRUE_EOP.txt";
	fstream EOP_file; EOP_file.setf(ios::fixed, ios::floatfield);
	EOP_file.open(EOP_path, ios::out);

	std::string ImgBND_path = outfilepath + "_IMG_BND.txt";
	fstream ImgBND_file; ImgBND_file.setf(ios::fixed, ios::floatfield);
	ImgBND_file.open(ImgBND_path, ios::out);

	PrintOut_EOP(EOP_file, ImgBND_file);

	EOP_file.close();
	ImgBND_file.close();
}

void CSMPhotoSimulation::PrintOut_gpsins(fstream &gpsins_file, bool bNoise)
{
	for (unsigned int i = 0; i<num_flight_lines; i++)
	{
		Trajectory trj_i;

		if (bNoise == false)
			trj_i = trajectory[i];
		else
			trj_i = trajectory_noise[i];

		for (GPSINS& epoch_j : trj_i)
		{
			gpsins_file.precision(7);

			gpsins_file << epoch_j.t << endl;///sec

			gpsins_file.precision(6);

			gpsins_file << util::Rad2Deg(epoch_j.ori[0]) << "\t";///deg
			gpsins_file << util::Rad2Deg(epoch_j.ori[1]) << "\t";///deg
			gpsins_file << util::Rad2Deg(epoch_j.ori[2]) << endl;///deg

			if (bNoise == true)
			{
				gpsins_file << util::Rad2Deg(sO) * 3600 * util::Rad2Deg(sO) * 3600 << "\t";///sec^2
				gpsins_file << "0.0" << "\t";
				gpsins_file << "0.0" << endl;

				gpsins_file << "0.0" << "\t";
				gpsins_file << util::Rad2Deg(sP) * 3600 * util::Rad2Deg(sP) * 3600 << "\t";///sec^2
				gpsins_file << "0.0" << endl;

				gpsins_file << "0.0" << "\t";
				gpsins_file << "0.0" << "\t";
				gpsins_file << util::Rad2Deg(sK) * 3600 * util::Rad2Deg(sK) * 3600 << endl;///sec^2
			}
			else
			{
				gpsins_file << "1.0e-9 0 0" << endl;
				gpsins_file << "0 1.0e-9 0" << endl;
				gpsins_file << "0 0 1.0e-9" << endl;
			}

			gpsins_file.precision(3);

			gpsins_file << epoch_j.pos[0] << "\t";///m
			gpsins_file << epoch_j.pos[1] << "\t";///m
			gpsins_file << epoch_j.pos[2] << endl;///m

			if (bNoise == true)
			{
				gpsins_file << sX*sX << "\t";///m^2
				gpsins_file << "0.0" << "\t";
				gpsins_file << "0.0" << endl;

				gpsins_file << "0.0" << "\t";
				gpsins_file << sY*sY << "\t";///m^2
				gpsins_file << "0.0" << endl;

				gpsins_file << "0.0" << "\t";
				gpsins_file << "0.0" << "\t";
				gpsins_file << sZ*sZ << endl;///m^2

			}
			else
			{
				gpsins_file << "1.0e-9 0 0" << endl;
				gpsins_file << "0 1.0e-9 0" << endl;
				gpsins_file << "0 0 1.0e-9" << endl;
			}

			gpsins_file << endl;
		}
	}
}

void CSMPhotoSimulation::PrintOut_icf(fstream &icf_file, bool bNoise)
{
	icf_file.precision(5);

	std::reference_wrapper<std::vector<GroundPoint>> groundTie = std::ref(Ground_tie_points);
	auto groundControl = std::ref(Ground_control_points);
	if (bNoise)
	{
		groundTie = std::ref(Ground_tie_points_error);
		groundControl = std::ref(Ground_control_points_error);
	}

	for (unsigned int camera_index = 0; camera_index<num_cam; camera_index++)
	{
		for (unsigned int i = 0; i<num_flight_lines; i++)
		{
			StripData& strip_i = block[camera_index][i];

			unsigned int num_img = static_cast<unsigned int>(strip_i.photo.size());

			for (unsigned int j = 0; j<num_img; j++)
			{
				Photo& photo_j = strip_i.photo[j];

				unsigned int num_points;

				if (bNoise == false)
					num_points = static_cast<unsigned int>(photo_j.img_point.size());
				else
					num_points = static_cast<unsigned int>(photo_j.img_point_noise.size());

				if (num_points < MIN_IMG_OBS) continue;

				for (unsigned int k = 0; k<num_points; k++)
				{
					Point2DID point;

					if (bNoise == false)
						point = photo_j.img_point[k];
					else
						point = photo_j.img_point_noise[k];

					GroundPoint GP;

					if (FindGroundPoint(point.ID, GP, groundTie))
					{
						if (GP.bConjugate == true)
						{
							icf_file << photo_j.ID << "\t";
							icf_file << point.ID << "\t";
							icf_file << point(0) << "\t";
							icf_file << point(1) << "\t";
							icf_file << "1	0	0	1" << endl;
						}
					}
					else if (FindGroundPoint(point.ID, GP, groundControl))
					{
						if (GP.bConjugate == true)
						{
							icf_file << photo_j.ID << "\t";
							icf_file << point.ID << "\t";
							icf_file << point(0) << "\t";
							icf_file << point(1) << "\t";
							icf_file << "1	0	0	1" << endl;
						}
					}
				}

			}

		}
	}
}

void CSMPhotoSimulation::PrintOut_gcp(fstream &gcp_file, bool bNoise)
{
	gcp_file.precision(5);

	unsigned int num_points = static_cast<unsigned int>(Ground_control_points_error.size());

	for (unsigned int i = 0; i<num_points; i++)
	{
		GroundPoint point;

		if (bNoise == false)
			point = Ground_control_points[i];
		else
			point = Ground_control_points_error[i];

		if (point.bConjugate == false) continue;

		gcp_file << point.ID << "\t";
		gcp_file << point(0) << "\t";
		gcp_file << point(1) << "\t";
		gcp_file << point(2) << "\t";

		if (bNoise == true)
		{
			gcp_file << Noise_GCP_X*Noise_GCP_X << "\t"; gcp_file << "0" << "\t";  gcp_file << "0" << "\t";
			gcp_file << "0" << "\t";  gcp_file << Noise_GCP_Y*Noise_GCP_Y << "\t"; gcp_file << "0" << "\t";
			gcp_file << "0" << "\t";  gcp_file << "0" << "\t"; gcp_file << Noise_GCP_Z*Noise_GCP_Z << endl;
		}
		else
		{
			gcp_file << "1.0e-9 0 0 0 1.0e-9 0 0 0 1.0e-9" << endl;
		}
	}

	num_points = static_cast<unsigned int>(Ground_tie_points_error.size());

	for (unsigned int i = 0; i<num_points; i++)
	{
		GroundPoint point;

		if (bNoise == false)
			point = Ground_tie_points[i];
		else
			point = Ground_tie_points_error[i];

		if (point.bConjugate == false) continue;

		gcp_file << point.ID << "\t";
		gcp_file << point(0) << "\t";
		gcp_file << point(1) << "\t";
		gcp_file << point(2) << "\t";

		if (bNoise == true)
		{
			gcp_file << Error_TIE_X*Error_TIE_X << "\t"; gcp_file << "0" << "\t";  gcp_file << "0" << "\t";
			gcp_file << "0" << "\t";  gcp_file << Error_TIE_Y*Error_TIE_Y << "\t"; gcp_file << "0" << "\t";
			gcp_file << "0" << "\t";  gcp_file << "0" << "\t"; gcp_file << Error_TIE_Z*Error_TIE_Z << endl;
		}
		else
		{
			gcp_file << "1.0e-9 0 0 0 1.0e-9 0 0 0 1.0e-9" << endl;
		}
	}
}

void CSMPhotoSimulation::PrintOut_ori(fstream &ORI_file, bool bNoise)
{
	StripData strip_i;

	for (unsigned int camera_index = 0; camera_index<num_cam; camera_index++)
	{
		for (unsigned int i = 0; i<num_flight_lines; i++)
		{
			if(bNoise)
				strip_i = block_noise[camera_index][i];
			else
				strip_i = block[camera_index][i];

			unsigned int num_img = static_cast<unsigned int>(strip_i.photo.size());

			GPSINS eop_j;
			Photo photo_j;

			for (unsigned int j = 0; j<num_img; j++)
			{
				eop_j = strip_i.eop[j];
				photo_j = strip_i.photo[j];

				if (photo_j.img_point.size() < MIN_IMG_OBS)
					continue;

				ORI_file << "!	Photo_id		Camera_id		sigma_XY		scan_time		Degree_OPK		Degree_XYZ" << endl;
				ORI_file << photo_j.ID << "\t" << camera_id[camera_index] << "\t" << cam[camera_index]->getPixPitch() * 0.5 << " 0 0 0" << endl << endl;
				ORI_file << "! Alpha Coeff and it's Dispersion" << endl;
				ORI_file << "0" << endl << endl;

				ORI_file << "! Imc Coef and it's Dispersion" << endl;
				ORI_file << "0" << endl << endl;

				ORI_file << "! no  Ori Images" << endl;
				ORI_file << "1" << endl << endl;
				ORI_file << "!Ori_no	Ori_time	   omega	   phi		kappa		X		Y	  Z" << endl;

				ORI_file.precision(6);
				ORI_file << "1 \t" << eop_j.t << "\t";

				ORI_file << util::Rad2Deg(eop_j.ori[0]) << "\t";
				ORI_file << util::Rad2Deg(eop_j.ori[1]) << "\t";
				ORI_file << util::Rad2Deg(eop_j.ori[2]) << "\t";

				ORI_file.precision(4);
				ORI_file << eop_j.pos[0] << "\t";
				ORI_file << eop_j.pos[1] << "\t";
				ORI_file << eop_j.pos[2] << endl << endl;
			}
		}
	}
}

void CSMPhotoSimulation::PrintOut_cam(fstream &CAM_file)
{
	StripData strip_i;

	for (unsigned int camera_index = 0; camera_index<num_cam; camera_index++)
	{
		CAM_file << "! cameraID	type		xp	yp	c pixel-pitch" << endl;
		CAM_file << camera_id[camera_index] << "\t";
		CAM_file << "FRAME" << "\t";
		CAM_file << cam[camera_index]->getPP()(0) << "\t";
		CAM_file << cam[camera_index]->getPP()(1) << "\t";
		CAM_file << cam[camera_index]->getFL() << "\t";
		CAM_file << cam[camera_index]->getPixPitch() << endl << endl;

		CAM_file << "! dispersion of xp,yp,c" << endl;
		CAM_file << "1.0e-09	0.0		0.0" << endl;
		CAM_file << "0.0		1.0e-09	0.0" << endl;
		CAM_file << "0.0		0.0		1.0e-09" << endl << endl;

		CAM_file << "! no fiducials" << endl;
		CAM_file << "0" << endl << endl;

		CAM_file << "! User defined Ro" << endl;
		CAM_file << "0" << endl << endl;

		auto distModel = cam[camera_index]->getDistortionModel();
		if (constant::distmdlnames::_smac_ != distModel->getModelName())
			return;

		auto distParams = cam[camera_index]->getDistParameters();

		CAM_file << "! Distortion Model used: 1=UofC, 2=SMAC" << endl;
		CAM_file << "2" << endl << endl;
		
		CAM_file << "! no distortion and array elements" << endl;
		CAM_file << size_t(distParams.size()) << endl << endl;

		for (const auto& p : distParams)
			CAM_file << p->get() << "\t";
		CAM_file << std::endl;

		for (size_t r = 0; r < distParams.size(); ++r)
		{
			for (size_t c = 0; c < distParams.size(); ++c)
			{
				if(r == c)
					CAM_file << "1.0e-9\t";
				else
					CAM_file << "0.0\t";				
			}

			CAM_file << std::endl;
		}

		CAM_file << "!Lever-arm and dispersion" << endl << endl;

		CAM_file << LA_BS[camera_index].pos[0] << "\t";
		CAM_file << LA_BS[camera_index].pos[1] << "\t";
		CAM_file << LA_BS[camera_index].pos[2] << endl << endl;

		CAM_file << "1.0e-09	0.0		0.0" << endl;
		CAM_file << "0.0		1.0e-09	0.0" << endl;
		CAM_file << "0.0		0.0		1.0e-09" << endl << endl;

		CAM_file << "!Boresight and dispersion" << endl << endl;

		CAM_file << util::Rad2Deg(LA_BS[camera_index].ori[0]) << "\t";
		CAM_file << util::Rad2Deg(LA_BS[camera_index].ori[1]) << "\t";
		CAM_file << util::Rad2Deg(LA_BS[camera_index].ori[2]) << endl << endl;

		CAM_file << "1.0e-09	0.0		0.0" << endl;
		CAM_file << "0.0		1.0e-09	0.0" << endl;
		CAM_file << "0.0		0.0		1.0e-09" << endl << endl;
	}
}

void CSMPhotoSimulation::PrintOut_EOP(fstream &EOP_file, fstream &ImgBND_file)
{
	StripData strip_i;

	for (unsigned int camera_index = 0; camera_index<num_cam; camera_index++)
	{
		for (unsigned int i = 0; i<num_flight_lines; i++)
		{
			strip_i = block[camera_index][i];

			unsigned int num_img = static_cast<unsigned int>(strip_i.photo.size());

			GPSINS eop_j;
			Photo photo_j;

			for (unsigned int j = 0; j<num_img; j++)
			{
				eop_j = strip_i.eop[j];
				photo_j = strip_i.photo[j];

				EOP_file << photo_j.ID << "\t";

				EOP_file.precision(6);
				EOP_file << util::Rad2Deg(eop_j.ori[0]) << "\t";
				EOP_file << util::Rad2Deg(eop_j.ori[1]) << "\t";
				EOP_file << util::Rad2Deg(eop_j.ori[2]) << "\t";

				EOP_file.precision(4);
				EOP_file << eop_j.pos[0] << "\t";
				EOP_file << eop_j.pos[1] << "\t";
				EOP_file << eop_j.pos[2] << endl;

				math::Matrix<double> PC(3, 1), coner[4];
				PC(0, 0) = eop_j.pos[0];
				PC(1, 0) = eop_j.pos[1];
				PC(2, 0) = eop_j.pos[2];

				coner[0].resize(3, 1);
				coner[0](0, 0) = -cam[camera_index]->getPixPitch() * cam[camera_index]->getSensorSize()(0) / 2.0;
				coner[0](1, 0) = cam[camera_index]->getPixPitch() * cam[camera_index]->getSensorSize()(1) / 2.0;
				coner[0](2, 0) = -cam[camera_index]->getFL();

				coner[1].resize(3, 1);
				coner[1](0, 0) = -cam[camera_index]->getPixPitch() * cam[camera_index]->getSensorSize()(0) / 2.0;
				coner[1](1, 0) = -cam[camera_index]->getPixPitch() * cam[camera_index]->getSensorSize()(1) / 2.0;
				coner[1](2, 0) = -cam[camera_index]->getFL();

				coner[2].resize(3, 1);
				coner[2](0, 0) = cam[camera_index]->getPixPitch() * cam[camera_index]->getSensorSize()(0) / 2.0;
				coner[2](1, 0) = -cam[camera_index]->getPixPitch() * cam[camera_index]->getSensorSize()(1) / 2.0;
				coner[2](2, 0) = -cam[camera_index]->getFL();

				coner[3].resize(3, 1);
				coner[3](0, 0) = cam[camera_index]->getPixPitch() * cam[camera_index]->getSensorSize()(0) / 2.0;
				coner[3](1, 0) = cam[camera_index]->getPixPitch() * cam[camera_index]->getSensorSize()(1) / 2.0;
				coner[3](2, 0) = -cam[camera_index]->getFL();

				math::Matrix<double> R = math::rotation::getRMat(eop_j.ori[0], eop_j.ori[1], eop_j.ori[2]);

				double scale = eop_j.pos[2] / cam[camera_index]->getFL();///*1000.0;

				coner[0] *= scale;
				coner[1] *= scale;
				coner[2] *= scale;
				coner[3] *= scale;

				coner[0] = R % coner[0];
				coner[1] = R % coner[1];
				coner[2] = R % coner[2];
				coner[3] = R % coner[3];

				coner[0] += PC;
				coner[1] += PC;
				coner[2] += PC;
				coner[3] += PC;

				ImgBND_file << photo_j.ID << "_1\t" << endl;
				ImgBND_file << coner[0](0, 0) << "\t" << coner[0](1, 0) << "\t" << coner[0](2, 0) << endl;
				ImgBND_file << coner[1](0, 0) << "\t" << coner[1](1, 0) << "\t" << coner[1](2, 0) << endl;

				ImgBND_file << photo_j.ID << "_2\t" << endl;
				ImgBND_file << coner[1](0, 0) << "\t" << coner[1](1, 0) << "\t" << coner[1](2, 0) << endl;
				ImgBND_file << coner[2](0, 0) << "\t" << coner[2](1, 0) << "\t" << coner[2](2, 0) << endl;

				ImgBND_file << photo_j.ID << "_3\t" << endl;
				ImgBND_file << coner[2](0, 0) << "\t" << coner[2](1, 0) << "\t" << coner[2](2, 0) << endl;
				ImgBND_file << coner[3](0, 0) << "\t" << coner[3](1, 0) << "\t" << coner[3](2, 0) << endl;

				ImgBND_file << photo_j.ID << "_4\t" << endl;
				ImgBND_file << coner[3](0, 0) << "\t" << coner[3](1, 0) << "\t" << coner[3](2, 0) << endl;
				ImgBND_file << coner[0](0, 0) << "\t" << coner[0](1, 0) << "\t" << coner[0](2, 0) << endl;
			}

		}
	}
}