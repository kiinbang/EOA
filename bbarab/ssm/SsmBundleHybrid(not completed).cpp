﻿/*
* Copyright(c) 2005-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <ssm/include/SsmBundleHybrid.h>

#include <ssm/include/coplanarity.h>
#include <ssm/include/NumericPDE.h>

namespace ssm
{
	BundleHybrid::BundleHybrid() {}

	bool BundleHybrid::runBundle(const std::shared_ptr<util::SsmList<std::shared_ptr<data::PhotoData>>> inputPhotos,
		const std::shared_ptr<util::SsmList<std::shared_ptr<data::ObjectPoint>>> inputObjPoints,
		const unsigned int maxIteration)
	{
		photos = inputPhotos;
		objPoints = inputObjPoints;

		/// 1.
		/// Prepare image and object points
		if (!checkObservations())
		{
			throw std::runtime_error("Check completeness of data: number of available observations and unknowns");
			return false;
		}

		/// 2. 
		/// Initialize parameters

		/// 3.
		/// Iterative estimation

		bool repeat = true;
		unsigned int iterationNum = 0;

		do
		{
			++iterationNum;

			std::cout << "Iteration #: " << iterationNum << " of " << maxIteration;

			/// AX + BV = K
			/// F = determinant_func ≈ F0 + (∂f/∂x1)dx1 + (∂f/∂x2)r
			/// (∂f/∂x1)dx1 + (∂f/∂x2)r = -F0
			/// A: (∂f/∂x1)
			/// X: dx1
			/// B: (∂f/∂x2)
			/// V: r
			/// K: -F0
			/// Parameter observations
			/// X0: approximation
			/// dX: correction
			/// X00: observation
			/// V: residual
			/// X0 + dX = X00 + V
			/// -dX + V = (X0 - X00)

			/// Model function:
			/// Collinearity equations
			for (unsigned int phIdx = 0; phIdx < photos->size(); ++phIdx)
			{
				/// Camera
				std::shared_ptr<sensor::Camera> cam = photos->operator[](phIdx)->getCam();
				/// Camera parameters
				std::shared_ptr<param::ParameterCollection> camParam = cam->getParameters();
				/// Distortion parameters
				std::shared_ptr<param::ParameterCollection> distParam = cam->getDistortionModel()->getParameters();
				/// EO parameters
				std::shared_ptr<param::ParameterCollection> eoParam = photos->operator[](phIdx)->getEop()->getParameters();

				auto imgPts = photos->operator[](phIdx)->getPts();

				for (unsigned int ptIdx = 0; ptIdx < imgPts->pts.size(); ++ptIdx)
				{
					std::shared_ptr<data::ImagePoint> imgPt = imgPts->pts[ptIdx);
				}
			}

			/// Model function:
			/// Coplanarity condition equations

			/// check number of models
			if (this->tieObsParams.size() != modelTiePts.size())
			{
				/// collect tie points again for double check
				checkStereoModels();

				if (this->tieObsParams.size() != modelTiePts.size())
				{
					throw std::runtime_error("number of models in modelTiePts and number of model in tieObsParams are not same.");
					return false;
				}
			}

			for (unsigned int mdlIdx = 0; mdlIdx < this->tieObsParams.size(); ++mdlIdx)
			{
				auto& tiePts = modelTiePts[mdlIdx];
				unsigned int numTies = tiePts.size();

				if (numTies < 1)
					continue;

				unsigned int ph0Idx = find(tiePts[0].val0->imgUuid, photos);
				std::shared_ptr<data::PhotoData> pho0;
				if (ph0Idx < photos->size()) 
					pho0 = photos->operator[](ph0Idx);

				unsigned int ph1Idx = find(tiePts[0].val1->imgUuid, photos);
				std::shared_ptr<data::PhotoData> pho1;
				if (ph1Idx < photos->size())
					pho1 = photos->operator[](ph1Idx);

				bool twoCameras;

				if (pho0->getCam()->getId() != pho1->getCam()->getId())
					twoCameras = true;
				else
					twoCameras = false;

				auto& tieObs = this->tieObsParams[mdlIdx];
				
				/// Coplanarity conditions for models
				
				util::SsmList<std::shared_ptr<param::ParameterCollection>> paramCltPtr;

				/// First camera and distortion: f, xp, yp, w, h, pixel pitch and distortion parameters
				paramCltPtr.addHead(pho0->getCam()->getParameters());
				paramCltPtr.addHead(pho0->getCam()->getDistortionModel()->getParameters());
				
				/// Different two cameras
				if (twoCameras)
				{
					/// Second camera and distortion: f, xp, yp, w, h, pixel pitch and distortion parameters
					paramCltPtr.addHead(pho1->getCam()->getParameters());
					paramCltPtr.addHead(pho1->getCam()->getDistortionModel()->getParameters());
				}

				/// First photo eop
				paramCltPtr.addHead(pho0->getEop()->getParameters());
				/// Second photo eop
				paramCltPtr.addHead(pho1->getEop()->getParameters());

				/// IOP and EOP for coplanarity condition
				unsigned int numCollections = paramCltPtr.size();
				util::SsmList<unsigned int > numParameters(numCollections);
				util::SsmList<unsigned int> numSets(numCollections);

				for (unsigned int i = 0; i < numCollections; ++i)
				{
					numParameters[i] = paramCltPtr[i]->getAllNumParameters();
					numSets[i] = 1;
				}

				math::Matrixd a, b, l;

				if(twoCameras)
					math::getNumericPDE(ssm::getDeterminantStereoCam, paramCltPtr, tieObs, a, b, l);
				else
					math::getNumericPDE(ssm::getDeterminantSingleCam, paramCltPtr, tieObs, a, b, l);
			}

			if (iterationNum > maxIteration)
				repeat = false;

		} while (repeat);

		return true;
	}

	bool BundleHybrid::checkObservations()
	{
		/// 1.
		/// Set initially availability of all images(photos) true
		/// and set initially availability of all image points true
		for (unsigned int phIdx = 0; phIdx < photos->size(); ++phIdx)
		{
			photos->setAvailability(phIdx, true);

			auto imgPts = photos->operator[](phIdx)->getPts();

			for (unsigned int ptIdx = 0; ptIdx < imgPts->size(); ++ptIdx)
			{
				imgPts->setAvailability(ptIdx, true);
			}
		}

		/// Set initially availability of all object points true
		for (unsigned int objPtIdx = 0; objPtIdx < objPoints->size(); ++objPtIdx)
		{
			objPoints->setAvailability(objPtIdx, true);
		}

		/// 2. Duplicated ids
		/// Find duplicated ids for all image points (observations)
		checkDuplicatedImgPointIds();		
		//// Find duplicated ids for photos
		checkDuplicatedPhotoIds();		
		//// Find duplicated ids for object points
		checkDuplicatedObjPointIds();		
		
		/// 3.
		/// Check the number of observations for each photos
		checkLackOfObservations();

		bool repeat;
		do
		{
			repeat = false;

			/// 4. 
			/// Find non-available image points (non-multiple observed points)
			/// and add new object points to the list if not exist
			if (checkNonMultipleObservationsAndDefineObjPts())
			{
				/// 5.
				/// Check the number of observations for each photos
				repeat = checkLackOfObservations();
			}

		} while (repeat);

		/// 6.
		/// Check number of datum constraints
		if(!checkDatumConstraints())
			return false;

		/// 7.
		/// Check approximate reduncancy
		if(!checkPseudoRedundancy())
			return false;
				
		return true;
	}

	void BundleHybrid::checkDuplicatedImgPointIds()
	{
		//// Find duplicated ids for image points
		for (unsigned int phIdx = 0; phIdx < photos->size(); ++phIdx)
		{
			auto imgPts = photos->operator[](phIdx)->getPts();

			for (unsigned int ptIdx = 0; ptIdx < imgPts->size(); ++ptIdx)
			{
				for (unsigned int ptIdx2 = ptIdx+1; ptIdx2 < imgPts->size(); ++ptIdx2)
				{
					/// Duplicated point id in a same image(photo)
					if (imgPts->operator[](ptIdx)->ptUuid == imgPts->operator[](ptIdx2)->ptUuid)
					{
						imgPts->setAvailability(ptIdx, false);
						imgPts->setAvailability(ptIdx2, false);

						throw std::runtime_error(
							std::string("Duplicated point uuid in an image: \n") +
							std::string("In Photo ") +
							std::string(photos->operator[](phIdx)->getId().printIdToHex().getChar()) + 
							std::string("\n") +
							std::string("Duplicated id: ") + 
							std::string(imgPts->operator[](ptIdx)->ptUuid.printIdToHex().getChar())
							);
					}
				}
			}

			photos->operator[](phIdx)->getPts()->deleteUnavailableData();
		}
	}

	void BundleHybrid::checkDuplicatedObjPointIds()
	{
		//// Find duplicated ids for object points
		for (unsigned int objPtIdx = 0; objPtIdx < objPoints->size(); ++objPtIdx)
		{
			for (unsigned int objPtIdx2 = objPtIdx+1; objPtIdx2 < objPoints->size(); ++objPtIdx2)
			{
				if (objPoints->operator[](objPtIdx)->ptUuid == objPoints->operator[](objPtIdx2)->ptUuid)
				{
					objPoints->setAvailability(objPtIdx, false);
					objPoints->setAvailability(objPtIdx2, false);

					throw std::runtime_error(
						std::string("Duplicated photo uuid: \n") + 
						std::string("In ") +
						std::to_string(objPtIdx) + std::string("the object point id: ") +
						std::string(objPoints->operator[](objPtIdx)->ptUuid.printIdToHex().getChar()) + std::string("\n") +
						std::string("In ") +
						std::to_string(objPtIdx2) + std::string("the object point id: ") +
						std::string(objPoints->operator[](objPtIdx2)->ptUuid.printIdToHex().getChar())
					);
				}
			}
		}

		objPoints->deleteUnavailableData();		
	}

	void BundleHybrid::checkDuplicatedPhotoIds()
	{
		//// Find duplicated ids for photos
		for (unsigned int phIdx = 0; phIdx < photos->size(); ++phIdx)
		{
			for (unsigned int phIdx2 = phIdx+1; phIdx2 < photos->size(); ++phIdx2)
			{
				if (photos->operator[](phIdx)->getId() == photos->operator[](phIdx2)->getId())
				{
					photos->setAvailability(phIdx, false);
					photos->setAvailability(phIdx2, false);

					throw std::runtime_error(
						std::string("Duplicated photo uuid: \n") +
						std::string("In ") +
						std::to_string(phIdx) + std::string("the photo id: ") +
						std::string(photos->operator[](phIdx)->getId().printIdToHex().getChar()) + std::string("\n") +
						std::string("In ") +
						std::to_string(phIdx2) + std::string("the photo id: ") +
						std::string(photos->operator[](phIdx2)->getId().printIdToHex().getChar())
					);
				}
			}
		}

		photos->deleteUnavailableData();
	}

	bool BundleHybrid::checkLackOfObservations()
	{
		//// Find duplicated ids for photos
		for (unsigned int phIdx = 0; phIdx < photos->size(); ++phIdx)
		{
			if (photos->operator[](phIdx)->getPts()->size() < constant::minNumTies)
				photos->setAvailability(phIdx, false);
		}

		if (photos->deleteUnavailableData() > 0)
			return true;
		else
			return false;
	}

	/// check non-multiple observations and define object points
	bool BundleHybrid::checkNonMultipleObservationsAndDefineObjPts()
	{
		unsigned int n = 0;

		/// Find non-available image points (non-multiple observed points)
		/// and add new object points to the list if not exist
		for (unsigned int phIdx = 0; phIdx < photos->size(); ++phIdx)
		{
			auto imgPts = photos->operator[](phIdx)->getPts();

			for (unsigned int ptIdx = 0; ptIdx < imgPts->size(); ++ptIdx)
			{
				unsigned int findNum = 1;
				util::SsmList<std::shared_ptr<data::ImagePoint>> foundPts;
				foundPts.addTail(imgPts->operator[](ptIdx));

				for (unsigned int phIdx = 0; phIdx < photos->size(); ++phIdx)
				{
					/// Find a point having a same id
					unsigned int idx = find(imgPts->operator[](ptIdx)->ptUuid, photos->operator[](phIdx)->getPts());
					std::shared_ptr<data::ImagePoint> imgPt = nullptr;
					if (idx < photos->operator[](phIdx)->getPts()->size())
						imgPt = photos->operator[](phIdx)->getPts()->operator[](idx);

					if (imgPt != nullptr)
					{
						foundPts.addTail(imgPt);
						++findNum;
						break;
					}
				}

				/// Check number of observed image points
				if (findNum == 1)/// Single observed point
				{
					std::shared_ptr<data::ObjectPoint> foundObjPt = nullptr;
					unsigned int idx = find(imgPts->operator[](ptIdx)->ptUuid, objPoints);
					if (idx < objPoints->size())
						foundObjPt = objPoints->operator[](idx);
					

					if (nullptr == foundObjPt)/// No corresponding object point
					{
						imgPts->setAvailability(ptIdx, false);
					}
					else
					{
						if (foundObjPt->sd(0) >= constant::MAX_SD
							&& foundObjPt->sd(1) >= constant::MAX_SD
							&& foundObjPt->sd(2) >= constant::MAX_SD) //// Fully unknown point
						{
							/// There is no coordinate can be used as control data
							/// Therefore, this point can be determined in a bundle procedure
							imgPts->setAvailability(ptIdx, false);
							this->setAvailability(foundObjPt->ptUuid, objPoints, false);
						}
						else //// Single observation but control point
						{
							imgPts->setAvailability(ptIdx, true);
							this->setAvailability(foundObjPt->ptUuid, objPoints, true);
						}
					}
				}
				else if (findNum >= 2) /// Multiple observation
				{
					for (unsigned int i = 0; i < foundPts.size(); ++i)
						this->setAvailability(foundPts[i]->ptUuid, objPoints, true);

					std::shared_ptr<data::ObjectPoint> foundObjPt = nullptr;
					unsigned int idx = find(imgPts->operator[](ptIdx)->ptUuid, objPoints);
					if (idx < objPoints->size())
						foundObjPt = objPoints->operator[](idx);\

					if (nullptr == foundObjPt) /// There is no existing object point corresponding to the image points
					{
						/// Create and add a new object points and assign the same uuid of the image points to the created object point
						std::shared_ptr<data::ObjectPoint> objPt = std::make_shared<data::ObjectPoint>();
						objPt->ptUuid = imgPts->operator[](ptIdx)->ptUuid;
						objPoints->addTail(objPt);
					}
					else
					{
						this->setAvailability(foundObjPt->ptUuid, objPoints, true);
					}
				}
			}

			n += imgPts->deleteUnavailableData();
		}

		return n > 0 ? true : false;
	}

	/// Check number of datum constraints
	bool BundleHybrid::checkDatumConstraints()
	{
		/// Set initially availability of all object points false
		/// and check datum deficiency
		unsigned int numControlData = 0;
		for (unsigned int objPtIdx = 0; objPtIdx < objPoints->size(); ++objPtIdx)
		{
			/// Set initially availability of all object points false
			objPoints->setAvailability(objPtIdx, false);

			/// Check control data
			if (objPoints->operator[](objPtIdx)->sd(0) <= constant::MAX_SD)
				++numControlData;
			if (objPoints->operator[](objPtIdx)->sd(1) <= constant::MAX_SD)
				++numControlData;
			if (objPoints->operator[](objPtIdx)->sd(2) <= constant::MAX_SD)
				++numControlData;
		}

		if (numControlData < constant::numDatumParams)
		{
			std::cout << "ERROR: lack of datum: number of known parameters in object points is less than " << constant::numDatumParams << "." << std::endl;
			return false;
		}
		else
			return true;
	}

	/// Check approximate redundancy
	bool BundleHybrid::checkPseudoRedundancy()
	{
		unsigned int numImgPts = 0;
		unsigned int numUnknownsEop = 0;
		unsigned int numEopObservations = 0;
		cameras = std::make_shared<util::SsmList<std::shared_ptr<sensor::Camera>>>();
		
		for (unsigned int phIdx = 0; phIdx < photos->size(); ++phIdx)
		{
			if (find(photos->operator[](phIdx)->getCam()->getId(), cameras) > cameras->size())
				cameras->addTail(photos->operator[](phIdx)->getCam());

			numImgPts += photos->operator[](phIdx)->getPts()->size();

			auto eop = photos->operator[](phIdx)->getEop();
			eop->getParameters();

			for (unsigned int s = 0; s < eop->getParameters()->size(); ++s)
			{
				for (unsigned int p = 0; p < eop->getParameters()->operator[](s).size(); ++p)
				{
					if (eop->getParameters()->operator[](s)[p].getStDev() > constant::MAX_SD)
					{
						++numUnknownsEop;
					}
					else
					{
						++numEopObservations;
					}
				}
			}
		}

		/// Number of unknowns in object points
		unsigned int numUnknownPtsObs = 0;
		/// Number of object point observations
		unsigned int numPtObservations = 0;
		for(unsigned int objPtIdx = 0; objPtIdx < objPoints->size(); ++objPtIdx)
		{
			auto& objPt = objPoints->operator[](objPtIdx);

			if (objPt->sd(0) >= constant::MAX_SD)
				++numUnknownPtsObs;

			if (objPt->sd(1) >= constant::MAX_SD)
				++numUnknownPtsObs;

			if (objPt->sd(2) >= constant::MAX_SD)
				++numUnknownPtsObs;
		}

		numPtObservations = objPoints->size() * 3 - numUnknownPtsObs;

		/// Number of unknowns in object points
		unsigned int numUnknownCamParams = 0;
		/// Number of camera parameter observations
		unsigned int numCamParamObservations = 0;
		for (unsigned int camIdx = 0; camIdx < cameras->size(); ++camIdx)
		{
			auto& cam = cameras->operator[](camIdx);
			auto collection = cam->getParameters();

			for (unsigned int s = 0; s < collection->size(); ++s)
			{
				for (unsigned int p = 0; p < collection->operator[](s).size(); ++p)
				{
					if (collection->operator[](s)[p].getStDev() > constant::MAX_SD)
					{
						++numUnknownCamParams;
					}
					else
					{
						++numCamParamObservations;
					}
				}
			}
		}

		/// Find coplanarity conditions
		void checkStereoModels();

		unsigned int numTiePairs = 0;
		for (unsigned int i = 0; i < modelTiePts.size(); ++i)
			numTiePairs = modelTiePts[i].size();

		std::cout << "Total number of cameras: " << cameras->size() << std::endl;
		std::cout << "Total number of unknows in cameras: " << numUnknownCamParams << std::endl;
		std::cout << "Total number of images: " << photos->size() << std::endl;
		std::cout << "Total number of object points: " << objPoints->size() << std::endl;
		std::cout << "Total number of unknowns in object points: " << numUnknownPtsObs << std::endl;
		std::cout << "Total number of image points: " << numImgPts << std::endl;
		std::cout << "Total number of unknowns in Eops: " << numUnknownsEop << std::endl;

		unsigned int numUnknowns = numUnknownsEop + numUnknownPtsObs + numUnknownCamParams;
		unsigned int numEquations = numImgPts * 2 + numEopObservations + numPtObservations + numCamParamObservations + numTiePairs;
		unsigned int redundancy = numEquations - numUnknowns;

		std::cout << "Approximately check the reduncancy with given data:" << std::endl;
		std::cout << "-. Number of unknowns: " << numUnknowns << std::endl;
		std::cout << "-. Number of point coordinate observations: " << numImgPts * 2 << std::endl;
		std::cout << "-. Number of coplanarity conditions: " << numTiePairs << std::endl;
		std::cout << "-. Number of eop observations: " << numEopObservations << std::endl;
		std::cout << "-. Number of object point observations: " << numPtObservations << std::endl;
		std::cout << "-. Number of camera parameter observations: " << numCamParamObservations << std::endl;
		std::cout << "-. Approximate reduncancy: " << redundancy << std::endl;

		if (redundancy < 1)
		{
			std::cout << "Lack of reduncancy!" << std::endl;
			return false;
		}
		else
		{
			return true;
		}
	}

	/// Find a camera using uuid
	unsigned int BundleHybrid::find(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<sensor::Camera>>> cameras)
	{
		for (unsigned int camIdx = 0; camIdx < cameras->size(); ++camIdx)
		{
			if (id == cameras->operator[](camIdx)->getId())
				return camIdx;
		}

		return cameras->size() + 1;
	}

	/// Find an image point
	unsigned int BundleHybrid::find(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<data::ImagePoint>>> imgPoints)
	{
		for (unsigned int ptIdx = 0; ptIdx < imgPoints->size(); ++ptIdx)
		{
			if (id == imgPoints->operator[](ptIdx)->ptUuid)
				return ptIdx;
		}

		return imgPoints->size() + 1;
	}

	/// Find an object point
	unsigned int BundleHybrid::find(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<data::ObjectPoint>>> objPoints)
	{
		for (unsigned int objIdx = 0; objIdx < objPoints->size(); ++objIdx)
		{
			if (id == objPoints->operator[](objIdx)->ptUuid)
				return objIdx;
		}

		return objPoints->size() + 1;
	}

	/// Find an object point
	unsigned int BundleHybrid::find(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<data::PhotoData>>> photos)
	{
		for (unsigned int phIdx = 0; phIdx < photos->size(); ++phIdx)
		{
			if (id == photos->operator[](phIdx)->getId())
				return phIdx;
		}

		return photos->size() + 1;
	}

	void BundleHybrid::setAvailability(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<data::ObjectPoint>>> objPts, const bool availability) const
	{
		for (unsigned int i = 0; i < objPts->size(); ++i)
		{
			if (objPts->operator[](i)->ptUuid == id)
			{
				objPts->setAvailability(i, availability);
			}
		}
	}

	/// Check coplanarity condition equations
	void BundleHybrid::checkStereoModels()
	{
		///
		/// Todo: remove the variable modelTiePts later if it is not essential.
		///
		modelTiePts.removeAll();
		util::SsmList<data::Pair<std::shared_ptr<data::ImagePoint>, std::shared_ptr<data::ImagePoint>>> tie;

		///
		/// It is tie points for all model data. It is parameter collections for being used for numericPDE
		///
		tieObsParams.removeAll();
		util::SsmList<std::shared_ptr<param::ParameterCollection>> tieObservations;
		
		for (unsigned int phIdx0 = 0; phIdx0 < photos->size(); ++phIdx0)
		{
			auto& photo0 = photos->operator[](phIdx0);
			auto pts0 = photo0->getPts();

			for (unsigned int phIdx1 = phIdx0; phIdx1 < photos->size(); ++phIdx1)
			{
				tie.removeAll();
				tieObservations.removeAll();

				auto& photo1 = photos->operator[](phIdx1);
				auto pts1 = photo1->getPts();

				for (unsigned int pts0Idx = 0; pts0Idx < pts0->size(); ++pts0Idx)
				{
					unsigned int pts1Idx = find(pts0->operator[](pts0Idx)->ptUuid, pts1);

					if(pts1Idx < pts1->size())
					{
						data::Pair<std::shared_ptr<data::ImagePoint>, std::shared_ptr<data::ImagePoint>> pair;
						pair.val0 = pts0->operator[](pts0Idx);
						pair.val1 = pts1->operator[](pts1Idx);
						tie.addTail(pair);

						/// Create parameter collection
						std::shared_ptr<param::ParameterCollection> params = std::make_shared<param::ParameterCollection>();
						params->resize(2);///Two tie points
						params->operator[](0).resize(2);//2D image coordinates
						params->operator[](1).resize(2);//2D image coordinates
						/// Set coordinates
						params->operator[](0)[0].set(pair.val0->val(0));
						params->operator[](0)[1].set(pair.val0->val(1));
						params->operator[](1)[0].set(pair.val1->val(0));
						params->operator[](1)[1].set(pair.val1->val(1));
						/// Set standard deviations
						params->operator[](0)[0].setStDev(pair.val0->sd(0));
						params->operator[](0)[1].setStDev(pair.val0->sd(1));
						params->operator[](1)[0].setStDev(pair.val1->sd(0));
						params->operator[](1)[1].setStDev(pair.val1->sd(1));

						tieObservations.addTail(params);
					}
				}

				if (tie.size() > 0)
				{
					modelTiePts.addTail(tie);
					tieObsParams.addTail(tieObservations);
				}
			}
		}
	}
}