/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <vector>

//#include <ssm/include/SsmList.h>
#include <ssm/include/SSMMatrixd.h>
#include <ssm/include/SSMParameterCollection.h>

namespace math
{
	enum class NPDETYPE { FirstOrder = 0/*first order*/, FourthOrder = 1/*fourth order cenrtral differences*/ };

	const double coordDelta = 0.001;

	/**
	 * @brief definition of a function type
	*/
	typedef Matrixd(*funcParameter)(const std::vector<std::shared_ptr<param::Parameter>>&, const std::vector<std::shared_ptr<param::Parameter>>&);

	/**getNumericPDEWithParameters
	*@brief get a, b and l matrices with partial derivatives
	*@param eq: given equation
	*@param givenParams: given parameters
	*@param givenObs: given observations
	*@param a design matrix
	*@param b coefficient matrix of residuals
	*@param l -F0
	*/
	void getNumericPDEWithParameters(funcParameter eq,
		const std::vector<std::shared_ptr<param::Parameter>>& givenParams,
		const std::vector<std::shared_ptr<param::Parameter>>& givenObs,
		Matrixd& a,
		Matrixd& b,
		Matrixd& l,
		const NPDETYPE npdeType);

	/**
	 * @brief definition of a function type
	*/
	typedef Matrixd(*funcCollection)(const std::vector<std::shared_ptr<param::ParameterCollection>>&, const std::vector<std::shared_ptr<param::ParameterCollection>>&);

	/**getNumericPDE
	*@brief get a, b and l matrices with partial derivatives
	*@param eq: given equation
	*@param givenParams: given parameters
	*@param givenObs: given observations
	*@param a design matrix
	*@param b coefficient matrix of residuals
	*@param l -F0
	*/
	void getNumericPDE(funcCollection eq,
		const std::vector<std::shared_ptr<param::ParameterCollection>>& givenParams,
		const std::vector<std::shared_ptr<param::ParameterCollection>>& givenObs,
		Matrixd& a,
		Matrixd& b,
		Matrixd& l,
		const NPDETYPE npdeType = NPDETYPE::FirstOrder);
}
