/*
* Copyright(c) 1999-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/SSMDataStructures.h>
#include <ssm/include/SSMParameterSet.h>

namespace param
{
	//class __declspec (dllexport) ParameterCollection
	class ParameterCollection
	{
	public:
		ParameterCollection();

		ParameterCollection(const ParameterCollection &copy);

		void operator = (const ParameterCollection &copy);

		ParameterCollection operator - (const ParameterCollection &copy) const;

		ParameterSet& operator [] (const unsigned int idx);

		unsigned int size() const;
		void resize(const unsigned int n);

		ParameterSet get(const unsigned int idx) const;

		std::vector<ParameterSet> get() const;
		void set(const std::vector<ParameterSet> &newVals);

		std::string getName() const;
		void setName(const std::string &newName);

		std::string getDescription() const;
		void setDescription(const std::string &newDescription);

		unsigned int getAllNumParameters() const;

		std::vector<double> getAllParameters() const;

		void add(const ParameterCollection& copy);

		void add(const ParameterSet& copySet);

	private:
		/// Copy member variables
		void getCopy(const ParameterCollection& copy);

	private:
		std::vector<ParameterSet> collection;
		std::string name;
		std::string description;
	};
}