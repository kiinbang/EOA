/*
* Copyright(c) 1999-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>

#include <ssm/include/SSMMatrixd.h>
#include <ssm/include/SSMParam.h>

namespace param
{
	/// A set of parameters
	//class __declspec (dllexport) ParameterSet
	class ParameterSet
	{
	public:
		ParameterSet();
		
		ParameterSet(const ParameterSet &copy);

		void operator = (const ParameterSet &copy);

		ParameterSet operator - (const ParameterSet &copy) const;

		Parameter& operator [] (const unsigned int idx);

		const Parameter& operator [] (const unsigned int idx) const;

		unsigned int size() const;
		void resize(const unsigned int n);

		std::vector<Parameter> get() const;
		void set(const std::vector<Parameter> &newVals);

		math::Matrixd getVariance() const;
		void setVariance(const math::Matrixd& newVals);

		std::string getName() const;
		void setName(const std::string &newName);

		std::string getDescription() const;
		void setDescription(const std::string &newDescription);

	private:
		/// Copy member variables
		void getCopy(const ParameterSet& copy);

	private:
		std::vector<Parameter> vals;
		math::Matrixd varcov;
		std::string name;
		std::string description;
	};
}