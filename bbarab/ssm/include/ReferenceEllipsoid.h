#pragma once

#include <iostream>
#include <vector>

#include <ssm/include/SSMMatDatad.h>

namespace math
{
	namespace geo
	{
		const double _PI_ = 3.14159265358979;
		const double r2d = 180.0 / _PI_;
		const double d2r = _PI_ / 180.0;

		namespace UTM
		{
			const double falseNorthing = 10000000;
			const double falseEasting = 500000;
			const double scale = 0.9996;
		}

		namespace WGS84
		{
			const std::string name = "WGS84";
			const double major = 6378137.0;// semi-major [m]
			const double minor = 6356752.3142;// semi-minor [m]
		}

		namespace BESSEL1841
		{
			const std::string name = "BESSEL1841";
			const double major = 6377397.155;// semi-major [m]
			const double minor = 6356078.963;// semi-minor [m]
		}

		namespace GRS80
		{
			const std::string name = "GRS80";
			const double major = 6378137.0;// semi-major [m]
			const double minor = 6356752.314140347;// semi-minor [m]
		}

		using LL = Arrayd; /// latitude[0] and longitude[1]
		using LLH = Arrayd; /// latitude[0], longitude[1] and height[2]
		using EN = Arrayd; /// east[0] and north[1]
		using ENZn = Arrayd; /// east[0], north[1] and Zone#[2]
		using ECEF = Arrayd; /// x[0], y[1] and z[2]
		using ENUZnHemi = Arrayd; /// east[0], north[1], up[2], Zone#[3] and Hemisphere[4]

		typedef struct ReferenceEllipsoidParams
		{
			ReferenceEllipsoidParams()
			{
				this->name = WGS84::name;
				this->semiMajor = WGS84::major;
				this->semiMinor = WGS84::minor;
			}

			ReferenceEllipsoidParams(const std::string& refname, const double major, const double minor)
			{
				this->name = refname;
				this->semiMajor = major;
				this->semiMinor = minor;
			}

			ReferenceEllipsoidParams(const ReferenceEllipsoidParams& val)
			{
				copy(val);
			}

			ReferenceEllipsoidParams operator = (const ReferenceEllipsoidParams& val)
			{
				copy(val);
				return *this;
			}

			void copy(const ReferenceEllipsoidParams& val)
			{
				this->name = val.name;
				this->semiMajor = val.semiMajor;
				this->semiMinor = val.semiMinor;
			}

			std::string name;
			double semiMajor;
			double semiMinor;
		} REP;
		
		const REP wgs84(WGS84::name, WGS84::major, WGS84::minor);
		const REP grs80(GRS80::name, GRS80::major, GRS80::minor);
		const REP bessel(BESSEL1841::name, BESSEL1841::major, BESSEL1841::minor);
		
		enum class HEMISPHERE { NORTHERN = 1, SOUTHERN = -1 };

		/// <summary>
		/// Reference ellipsoid class for coordinate transformation
		/// It can transform Lat-Lon, East-North, ECEF even between different reference ellipsoids.
		/// </summary>
		class ReferenceEllipsoid
		{
			struct UsefulCoeff
			{
				double ns; /// third eccentricity
				double ns2, ns3, ns4, ns5;
			};

		public:
			/// <summary>
			/// Constructor
			/// </summary>
			ReferenceEllipsoid();

			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="eliInfo"> ellipsoid parameters</param>
			ReferenceEllipsoid(const REP& eliInfo);

			/// <summary>
			/// Copy constructor
			/// </summary>
			/// <param name="copy"></param>
			ReferenceEllipsoid(const ReferenceEllipsoid& copy)
			{
				eliParam = copy.eliParam;
				initUsefulParams();
			}

			/// <summary>
			/// Assignment opertor
			/// </summary>
			/// <param name="copy"></param>
			ReferenceEllipsoid operator = (const ReferenceEllipsoid& copy)
			{
				eliParam = copy.eliParam;
				initUsefulParams();
				return *this;
			}

			/// <summary>
			/// compare operator: if all ellipsodial parameters are same, then return true.
			/// </summary>
			/// <param name="compare"></param>
			/// <returns></returns>
			bool operator == (const ReferenceEllipsoid& compare) const;

		private:
			void initUsefulParams();

		public:
			ReferenceEllipsoidParams eliParam;
			double flattening; /// flattening
			double eccentricity; /// first eccentricity
			double eccentricity2; /// second eccentricity

			UsefulCoeff n; /// Useful parameters used for coordinate transformation
		};
	}
}