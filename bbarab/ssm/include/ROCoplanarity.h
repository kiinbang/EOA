/*
* Copyright(c) 2000-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

////////////////////////////////////////////////
//ROCoplanarity.h(공면조건식을 이용한 상호표정)
//Coplanarity condition Relative Orientation
//made by BbaraB
//revision date 2000-6-8
//revision date 2000-6-13
//revision data 2002-04-29
//revision data 2020-1-16
////////////////////////////////////////////////

#pragma once

#include "RONonlinear.h"

//class for RO_Coplanarity_Condition

class CModelCoplanarity:public CModel
{
public:
	CModelCoplanarity(char *infile);
	bool RO_dependent_PseudoObs();
	bool RO_independent_PseudoObs();
	bool RO_dependent_General();
	bool RO_independent_General();
private:
	void InitApp_de();
	void InitApp_inde();
	void makeA_Ldependent();
	void makeA_Lindependent();
	void makeA_L_Bdependent();
	void makeA_L_Bindependent();
	math::Matrix<double> makedRdO(math::Matrix<double> R);
	math::Matrix<double> makedRdP(double o,double p,double k);
	math::Matrix<double> makedRdK(double o,double p,double k);
	std::string PrintResult_de_PseudoObs();
	std::string PrintResult_inde_PseudoObs();
	std::string PrintResult_de_General();
	std::string PrintResult_inde_General();

};