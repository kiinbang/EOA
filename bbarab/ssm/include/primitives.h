#pragma once

#include <SSM/include/SSMDerivedMatrix.h>

namespace data
{
	/**3d vector*/
	typedef math::VectorMat<float, 3> vector3df;
	/**2d vector*/
	typedef math::VectorMat<float, 2> vector2df;
	/**3d vector*/
	typedef math::VectorMat<double, 3> Point3D;
	/**2d vector*/
	typedef math::VectorMat<double, 2> Point2D;
	/**2d vector*/
	typedef math::VectorMat<unsigned int, 2> Point2di;
	/**3by3 rotation matrix*/
	typedef math::FixedSizeMat<double, 3, 3> RotationMatrix;

	/**3 Euler angles*/
	typedef Point3D EulerAngles;
}