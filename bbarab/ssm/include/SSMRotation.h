/*
* Copyright(c) 1999-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/SSMConstants.h>
#include <SSM/include/SSMDataStructures.h>

namespace math
{
	enum class AxisName { _X_, _Y_, _Z_ };

	namespace rotation
	{
		/**2D Rotation matrix*/
		math::Matrix<double> getRMat(const double angle);

		/**Rotation matrix for a specific axis*/
		data::RotationMatrix getRMat(const AxisName axisName, const double angle);

		/**Rotation matrix for three Euler angles*/
		data::RotationMatrix getRMat(const double omega, const double phi, const double kappa);

		/**Partial derivative of omega for R-matrix*/
		data::RotationMatrix dRdO(const double omega, const double phi, const double kappa);

		/**Partial derivative of phi for R-matrix*/
		data::RotationMatrix dRdP(const double omega, const double phi, const double kappa);

		/**Partial derivative of kappa for R-matrix*/
		data::RotationMatrix dRdK(const double omega, const double phi, const double kappa);

		/**Rotation matrix for three Euler angles*/
		data::RotationMatrix getMMat(const double omega, const double phi, const double kappa);

		/**Partial derivative of omega for M-matrix*/
		data::RotationMatrix dMdO(const double omega, const double phi, const double kappa);

		/**Partial derivative of phi for M-matrix*/
		data::RotationMatrix dMdP(const double omega, const double phi, const double kappa);

		/**Partial derivative of kappa for M-matrix*/
		data::RotationMatrix dMdK(const double omega, const double phi, const double kappa);

		/**Extract Euler angles from R-rotation matrix*/
		void ExtractEulerFromR(const data::RotationMatrix& Rmatrix, double& omega, double& phi, double& kappa);

		/**Extract Euler angles from M-rotation matrix*/
		void ExtractEulerFromM(const data::RotationMatrix& Mmatrix, double& omega, double& phi, double& kappa);

		/**Arbitrary axis rotation*/
		math::Matrix<double> rotateArbitraryAxis(const double x, const double y, const double z,
			const double rx, const double ry, const double rz,
			const double theta);
	};
}