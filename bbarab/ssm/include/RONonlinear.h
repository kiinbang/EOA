/*
* Copyright(c) 2000-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

//////////////////////////
//RONonlinear.h(Non Linear Collinearity)
//made by BbaraB
//revision date 2000-5-17
//revision date 2000-5-22
//revision date 2000-5-24
//revision date 2000-5-25
//revision date 2000-5-25
//revision date 2000-5-31
//revision date 2000-6-10
//revision data 2002-04-29
//revision data 2020-01-16
//////////////////////////

#pragma once

#include <string>
#include <vector>

//#include "Collinearity.h"
//#include "Transformation.h"

#include <ssm/include/SSMDataStructures.h>
#include <ssm/include/SSMRotation.h>

//class for camera
class CCamera
{
public:
	std::string ID;				//Camera ID
	int num_fiducial;			//number of fiducial mark
	double f;					//focal length
	data::Point2D *fiducial;	//fiducial mark
	data::Point2D PPA;			//principal point of auto-collimation
	data::Point2D PPBS;			//principal point of best symmetry
};

//class for photo
class CPhoto
{
public:
	std::string ID;			//Photo ID
	CCamera camera;			//Camera ID
	int num_ROP;			//Number Of RO Point
	std::vector<data::Point2D> ROPoint; //Photo Point For RO
	CPhoto();
	~CPhoto();
	CCamera GetCamera() {return camera;}

};

//class for model
class CModel
{
public:
	//class for RO_EQ Coefficient
	class coefficient
	{
	public:
		double q, r, s;
		double b11, b12, b13, b14, b15, b16;
		double b21, b22, b23, b24, b25, b26;
		double J, K;
	};

	//class for RO Compute Matrix
	class RO_Matrix
	{
	public:
		math::Matrix<double> A, L, X, V, N, Ninv, VTV, Var_Co;
		math::Matrix<double> B, We, Ve;
		math::Matrix<double> Var;
		double pos_var, SD;
		double maxX;//largest element of a X matrix
		int DF;
	};

	//class for Relative Orientation Parameters
	class CROParameter
	{
	public:
		std::string ID;						//Parameter ID
		std::string LPhotoID, RPhotoID;		//L & R Photo ID
		double Lomega, Lphi, Lkappa;		//Left Photo Rotation Angle
		double Xl, Yl, Zl;					//Left Photo PC Coordinate
		double Romega, Rphi, Rkappa;		//Right Photo Rotation Angle
		double Xr, Yr, Zr;					//Right Photo PC Coordinate
	};

	//Control Data For RO
	int num_ROPoint;			//Number Of RO Point
	CROParameter Param_de;		//RO Parameter(dependent)
	CROParameter Param_inde;	//RO Parameter(independent)
	coefficient b;				//RO_EQ Coefficient
	RO_Matrix ROmat;			//matrix for RO compute
	math::Matrix<double> Ga;			//model space coordinate for RO point
	int maxIteration;			//maximum iteration limit
	double maxCorrection;		//maximum correction term limit
	double maxSD;				//standard deviation limits
	double diffVar;				//variance difference between old_variance and new_variance
	double maxdiffVar;			//maximum variance difference between old_variance and new_variance limit
	enum optionApproximation {PARALLAXEQ, LPHOTOCOORDINATE, RPHOTOCOORDINATE, TWOPHOTOMEAN}; //Option For Ga(model space 3D-coord) Approximation
	enum optionApproximation GaApproximation_Option;
	enum optionApproximation ParallaxEQ_Option;
	std::string result;			//string for result out
	///////////////////////////////////////////////////////////////////
	
	std::string ID;				//model ID
	CPhoto LPhoto, RPhoto;		//Photo(pair)
	CModel();
	CModel(char* infile);
	CModel(std::string modelID, CPhoto L, CPhoto R, int maxi = 10, double correction = 0.000001, double sd = 0.000001, double diffvar = 0.0);
	~CModel();

	//Member Function for Relative Orientation
	bool RO_dependent(void);		//Dependent Method
	bool RO_independent(void);		//Independent Method
	bool DependentTOIndependent(void);//From Dependent To Independent
	bool IndependentTODependent(void);//From Independent To Dependent
	void FilePrintROResult(char* outfile);	//RO Result File Print

private:
	void InitApp_de(void);			//Initial Approximation Function(dependent)
	void InitApp_inde(void);		//Initial Approximation Function(independent)
	void makeA_Ldependent(void);	//Make A & L Matrix For Dependent
	void makeA_Lindependent(void);	//Make A & L Matrix For Independent
	std::string PrintResult_de();	//Store Compute_Result to string(Dependent)
	std::string PrintResult_inde();	//Store Compute_Result to string(Independent)	
};