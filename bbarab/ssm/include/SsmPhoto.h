#pragma once

#include <ssm/include/SSMCamera.h>
#include <ssm/include/SSMConstants.h>
#include <ssm/include/SSMDataStructures.h>

using namespace constant;

namespace data
{
	const unsigned int NUM_EOP = 6;
	
	const unsigned int oIdx = 0;
	const unsigned int pIdx = 1;
	const unsigned int kIdx = 2;
	const unsigned int xIdx = 3;
	const unsigned int yIdx = 4;
	const unsigned int zIdx = 5;

	class EOP
	{
	public:
		EOP()
		{
			params.resize(NUM_EOP);
			for (auto& p : params)
				p = std::make_shared<param::Parameter>();

			this->params[oIdx]->setName(constant::photo::_omega_);
			this->params[oIdx]->setDescription("Photogrammetric rotation omega about X axis");
			this->params[oIdx]->setDelta(1.0e-6);
			this->params[oIdx]->setStDev(0.0);
			this->params[oIdx]->set(0.0);

			this->params[pIdx]->setName(constant::photo::_phi_);
			this->params[pIdx]->setDescription("Photogrammetric rotation phi about Y axis");
			this->params[pIdx]->setDelta(1.0e-6);
			this->params[pIdx]->setStDev(0.0);
			this->params[pIdx]->set(0.0);

			this->params[kIdx]->setName(constant::photo::_kappa_);
			this->params[kIdx]->setDescription("Photogrammetric rotation kappa about Z axis");
			this->params[kIdx]->setDelta(1.0e-6);
			this->params[kIdx]->setStDev(0.0);
			this->params[kIdx]->set(0.0);

			this->params[xIdx]->setName(constant::photo::_X0_);
			this->params[xIdx]->setDescription("EOP position X0");
			this->params[xIdx]->setDelta(1.0e-6);
			this->params[xIdx]->setStDev(0.0);
			this->params[xIdx]->set(0.0);

			this->params[yIdx]->setName(constant::photo::_Y0_);
			this->params[yIdx]->setDescription("EOP position Y0");
			this->params[yIdx]->setDelta(1.0e-6);
			this->params[yIdx]->setStDev(0.0);
			this->params[yIdx]->set(0.0);

			this->params[zIdx]->setName(constant::photo::_Z0_);
			this->params[zIdx]->setDescription("EOP position Z0");
			this->params[zIdx]->setDelta(1.0e-6);
			this->params[zIdx]->setStDev(0.0);
			this->params[zIdx]->set(0.0);
		}

		void setPC(const Point3D& perspectiveCenter)
		{
			this->params[xIdx]->set(perspectiveCenter(0));
			this->params[yIdx]->set(perspectiveCenter(1));
			this->params[zIdx]->set(perspectiveCenter(2));
		}

		void setPC(const Point3D& perspectiveCenter, const Point3D& sd)
		{
			this->params[xIdx]->set(perspectiveCenter(0));
			this->params[xIdx]->setStDev(sd(0));
			this->params[yIdx]->set(perspectiveCenter(1));
			this->params[yIdx]->setStDev(sd(1));
			this->params[zIdx]->set(perspectiveCenter(2));
			this->params[zIdx]->setStDev(sd(2));
		}

		Point3D getPC() const
		{
			Point3D pc;
			pc(0) = this->params[xIdx]->get();
			pc(1) = this->params[yIdx]->get();
			pc(2) = this->params[zIdx]->get();

			return pc;
		}

		void setEulerAngles(const double omega, const double phi, const double kappa)
		{
			this->params[oIdx]->set(omega);
			this->params[pIdx]->set(phi);
			this->params[kIdx]->set(kappa);
		}

		void setEulerAngles(const EulerAngles& ori)
		{
			this->params[oIdx]->set(ori(0));
			this->params[pIdx]->set(ori(1));
			this->params[kIdx]->set(ori(2));
		}

		void setEulerAngles(const EulerAngles& ori, const Point3D& sd)
		{
			this->params[oIdx]->set(ori(0));
			this->params[oIdx]->setStDev(sd(0));
			this->params[pIdx]->set(ori(1));
			this->params[pIdx]->setStDev(sd(1));
			this->params[kIdx]->set(ori(2));
			this->params[kIdx]->setStDev(sd(2));
		}

		void getEulerAngles(double& omega, double& phi, double& kappa) const
		{
			omega	= this->params[oIdx]->get();
			phi		= this->params[pIdx]->get();
			kappa	= this->params[kIdx]->get();
		}

		EulerAngles getOri() const
		{
			EulerAngles ori;
			ori(0) = this->params[oIdx]->get();
			ori(1) = this->params[pIdx]->get();
			ori(2) = this->params[kIdx]->get();

			return ori;
		}

		std::vector<std::shared_ptr<param::Parameter>>& getParameters()
		{
			return this->params;
		}

		void setParameters(const std::vector<std::shared_ptr<param::Parameter>> newParams)
		{
			this->params = newParams;
		}

	private:
		/// Parameters
		std::vector<std::shared_ptr<param::Parameter>> params;
	};

	struct PhotoData
	{
	public:
		PhotoData(const std::shared_ptr<sensor::Camera> camera,
			const std::shared_ptr<EOP> eo,
			const std::shared_ptr<ImagePointData> points,
			const unsigned int photoId)
			: cam(camera),
			eop(eo),
			imgPts(points),
			id(photoId)
		{
			imgPath = L"N/A";
			expTimeTag = L"N/A";
			//xyzUnit = L"N/A";
			//opkUnit = L"N/A";
		}

		std::shared_ptr<sensor::Camera> getCam() const { return cam; }
		std::shared_ptr<EOP> getEop() const { return eop; }
		const std::vector<ImagePointParam>& getPts() const { return imgPts->pts; }
		std::vector<ImagePointParam>& accessImgPts() const { return imgPts->pts; }
		const unsigned int& getId() const { return id; }
		const unsigned int& getCameraId() const { return cameraId; }
		std::wstring getImgPath() const { return imgPath; }
		std::wstring getTimeTag() const { return expTimeTag; }
		//std::wstring getXyzUnit() const { return xyzUnit; }
		//data::Point3D getXyz() const { return xyz; }
		//std::vector<double> getXyzVariance() const { return xyzVariance; }
		//std::wstring getOpkUnit() const { return opkUnit; }
		//data::EulerAngles getOpk() const { return opk; }
		//std::vector<double> getOpkVariance() const { return opkVariance; }

		void setCam(std::shared_ptr<sensor::Camera> camera) { cam = camera; }
		void setEop(std::shared_ptr<EOP> eo) { eop = eo; }
		void setPts(const ImagePointParam& pt, const unsigned int idx) { imgPts->pts[idx] = pt; }
		void setId(const unsigned int photoId) { id = photoId; }
		void setCameraId(const unsigned int camId) { this->cameraId = camId; }
		void setImgPath(const std::wstring& path) { imgPath = path; }
		void setTimeTag(const std::wstring& time) { expTimeTag = time; }
		//void setXyzUnit(const std::wstring& unit) { xyzUnit = unit; }
		//void setXyz(const std::vector<double>& pos) { xyz(0) = pos[0]; xyz(1) = pos[1]; xyz(2) = pos[2]; };
		//void setXyzVariance(const std::vector<double>& posVar) { xyzVariance = posVar; };
		//void setOpkUnit(const std::wstring& unit) { opkUnit = unit; }
		//void setOpk(const std::vector<double>& opk) { this->opk(0) = opk[0]; this->opk(1) = opk[1]; this->opk(2) = opk[2]; };
		//void setOpkVariance(const std::vector<double>& opkVar) { opkVariance = opkVar; };

	private:
		/// Camera
		std::shared_ptr<sensor::Camera> cam;
		/// Exterior orientation parameters
		std::shared_ptr<EOP> eop;
		/// 2D points observed in a photo
		std::shared_ptr<ImagePointData> imgPts;
		/// photo id
		unsigned int id;
		/// camera id
		unsigned int cameraId;
		/// image path
		std::wstring imgPath;
		/// exposure time tag
		std::wstring expTimeTag;
		/// coordinate unit
		//std::wstring xyzUnit;
		/// position info
		//data::Point3D xyz;
		/// variance of position
		//std::vector<double> xyzVariance;
		/// orientation unit
		//std::wstring opkUnit;
		/// orientation info
		//data::EulerAngles opk;
		/// variance of orientation
		//std::vector<double> opkVariance;
	};
}