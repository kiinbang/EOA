/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <memory>

#include <ssm/include/MeshData.h>
#include <nanoflann/nanoflann.hpp>

#pragma warning(disable: 4996)

namespace mesh
{
    /// Mesh data type
    using MeshDataType = MeshData<double>;

    /// KD Tree type to use for the mesh node data
    using KDTree = nanoflann::KDTreeSingleIndexAdaptor<nanoflann::L2_Simple_Adaptor<MeshDataType::CoordValueType, MeshDataType>, MeshDataType, 3>;

    /// data dimension
    const std::size_t dimension = 3;

    /// Pseudo point type
    enum class PSEUDOTYPE {NONE = 0, CENTROID = 1, MIDPOINT = 2};

    class SSMFlann
    {
    public:
        SSMFlann(const double maxLength = 0.0, const PSEUDOTYPE pType = PSEUDOTYPE::MIDPOINT);

        /**setNumberOfPoints
        *@brief resize vertex data vector
        *@param size : vertex data size (number of vertices)
        */
        void setNumberOfPoints(const unsigned int size);

        /**setVertex
        *@brief add a vertex without checking duplicated points
        *@param index : vertex index
        *@param x
        *@param y
        *@param z
        *@return bool
        */
        bool setVertex(const unsigned int index, const double x, const double y, const double z);

        /**setNumberOfFaces
        *@brief resize face data vector
        *@param size : number of faces
        */
        void setNumberOfFaces(const unsigned int size);

        /**setPseudoType
	    *@brief set the type of generating pseudo points
	    *@param pType: type
	    */
		void setPseudoType(const PSEUDOTYPE pType);

        /**setFace
        *@brief add a face
		*@param faceIndex : face index
        *@param vtxIndex : indices of vertices consisting a face
        *@param fnx : face normal vector x coordinate
        *@param fny : face normal vector y coordinate
        *@param fnz : face normal vector z coordinate
        *@return bool
        */
        bool setFace(const unsigned int faceIndex, const std::vector<unsigned int>& vtxIndex);

        /**setFaceNormal
        *@brief add a face normal vector
        *@param faceIndex : face index
        *@param fnx : face normal vector x coordinate
        *@param fny : face normal vector y coordinate
        *@param fnz : face normal vector z coordinate
        *@return bool
        */
        bool setFaceNormal(const unsigned int faceIndex, const double fnx, const double fny, const double fnz);

        /**getFaceNormal
        *@brief get a face normal vector
        *@param faceIndex : face index
        *@return Vertex3D
        */
        Vertex3D getFaceNormal(const unsigned int faceIndex) const;

        /**clearVertexData
        *@brief clear vertex data
        */
        void clearVertexData();

        /**buildTree
        *@brief buildTree
        *@param maxLeaf max number of leaves of kdtree
        */
        void buildTree(const unsigned int maxLeaf = 20);

        /**buildTree
        *@brief import index info from a file
        *@param maxLeaf max number of leaves of kdtree
        *@return bool
        */
        bool buildTree(const std::string& idxFilePath, const unsigned int maxLeaf = 20);

        /**search
        *@brief search closest vertices
        */
        bool search(const double x0, const double y0, const double z0,
            std::vector<double>& distances,
            std::vector<std::size_t>& indices,
            const unsigned int numClosest,
            const double distThreshold = 0.0) const;

        /**raytrace
        *@brief find the closest point along the given ray
        *@param sx: x coordinate of a start point of a ray
        *@param sy: y coordinate of a start point of a ray
        *@param sz: z coordinate of a start point of a ray
		*@param ex: x coordinate of a end point of a ray
		*@param ey: y coordinate of a end point of a ray
		*@param ez: z coordinate of a end point of a ray
        *@param radius: search radius
        *@param foundPts: point indices and distances, result values
        *@return bool
        */
        bool raytrace(const double sx, const double sy, const double sz,
            const double ex, const double ey, const double ez,
            const double radius,
            std::vector<std::pair<size_t, double>>& foundPts);

        /**getVertex
        *@param vtxIndex : vertex index
        */
        Vertex3D getVertex(const std::size_t vtxIndex) const;

        /**getDistThreshold
        */
        double getDistThreshold() const { return this->maxDist; }

        /**getVertices
        */
        const std::vector<Vertex3D>& getVertices() const { return this->meshData.vertices; }

        /**getPseudoPts
        */
        const std::vector<Vertex3D>& getPseudoPts() const { return this->pseudoPts; }

        /**getFaces
        */
        const std::vector<std::vector<unsigned int>>& getFaces() const { return this->meshData.faces; }

        /**setDistThreshold
        */
        void setDistThreshold(const double newDistTh) { this->maxDist = newDistTh; }

        /**exportIndex
		*@param idxFilePath : index file path
        *@return bool
        */
        bool exportIndex(const std::string& idxFilePath);

    private:
        /**defineFace
        *@brief define face with vertex indices
        *@param faceIndex face index
        *@param vertexIndices vertex indices in a face
        *@return bool
        */
        bool defineFace(const unsigned int faceIndex, const std::vector<unsigned int>& vertexIndices);

        /**computeCentroid
        *@param vertices : vertices consisting a face
        *@return Vertex3D : centroid 3D coordinates
        */
        Vertex3D computeCentroid(const std::vector<Vertex3D>& vertices);

        /**checkDist2Center
        *@param vertices : vertices consisting a face
        *@param centroid : centroid of a polygon
        *@return bool : [false] there is a too long side (>maxSide) in the vertices defining a face
        */
        bool checkDist2Center(const std::vector<Vertex3D>& vertices, const Vertex3D& centroid);

        /**defineSubTriangles
        *@brief define sub-polygons (triangles)
        *@param vertices : vertices consisting a face
        *@param centroid : centroid of a polygon
        */
        std::vector< std::vector<Vertex3D>> defineSubTriangles(const std::vector<Vertex3D>& vertices, const Vertex3D& centroid);

        /**insertPseudoPoint: generate pseudo points in a face with centroid points
        *@param vertexIndices : vertex indices consisting a face
        *@param faceIndex : face index
        */
        void insertPseudoPoint(const std::vector<unsigned int>& vertexIndices, const unsigned int faceIndex);

        /**insertPseudoPointMid: generate pseudo points in a face with centroid points
        *@param vertexIndices : vertex indices consisting a face
        *@param faceIndex : face index
        */
        void insertPseudoPointMid(const std::vector<unsigned int>& vertexIndices, const unsigned int faceIndex);

        /**addMidPts: add mid points to a given line
        *@param p0 : the start point of a line
        *@param p1 : the end point of a line
        *@param maxLength : maximum length threshold
        *@param pseudoPts : bucket for storing pseudo point (midPts)
        *@return bool: if true, the line is large, else, short
        */
        bool addMidPts(const Vertex3D& p0, const Vertex3D& p1, const double maxLength, std::vector<Vertex3D>& pseudoPts);

       /**divideTriangle: divide a triangle into three sub-triangles
       *@param triVtx : three vertices
       *@param maxLength : maximum length threshold
       *@param pseudoPts : bucket for storing pseudo point (midPts)
       *@param centroid : center of a triangle
       *@return bool: if true, triangle is large, else, small
       */
        bool divideTriangle(const std::vector<Vertex3D>& triVtx, const double maxLength, std::vector<Vertex3D>& pseudoPts, Vertex3D& centroid);

       /**divideFace: divide a face (polygon)
       *@param faceVtx : face vertices
       *@param maxLength : maximum length threshold
       *@param pseudoPts : bucket for storing pseudo point (midPts)
       */
        void divideFace(const std::vector<Vertex3D>& faceVtx, const double maxLength, std::vector<Vertex3D>& pseudoPts);

    private:
        MeshDataType meshData;
        std::vector<Vertex3D> pseudoPts;
        std::shared_ptr<KDTree> tree;
        double maxDist;
        PSEUDOTYPE pseudoType;
    };
}