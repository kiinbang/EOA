#pragma once

#include <fstream>
#include <iostream>
#include <string>

#include <boost/algorithm/string/replace.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace bpt = boost::property_tree;

#include <ssm/include/SSMConstants.h>
#include <ssm/include/SsmFrameCamera.h>
#include <ssm/include/SSMSMACModel.h>
#include <ssm/include/utilitygrocery.h>

namespace data
{
	/// Sensor type names
	const wchar_t* _frame = L"frame";
	/// Unit: pixel
	const wchar_t* _pixel = L"pixel";
	/// Unit: mm
	const wchar_t* _milimeter = L"mm";
	/// Unit: micro meter
	const wchar_t* _micrometer = L"micro_meter";
	/// Unit: meter
	const wchar_t* _meter = L"m";
	/// Unit: degrees
	const wchar_t* _deg = L"degrees";
	/// Unit: radian
	const wchar_t* _rad = L"radian";
	/// Photo coordinates
	const wchar_t* _phoCoordinates = L"photo_coordinates";
	/// Image coordinates
	const wchar_t* _imgCoordinates = L"image_coordinates";

	namespace prj
	{
		/// Fore reading project file
		const wchar_t* _name = L"project.name";
		const wchar_t* _type = L"project.type";
		const wchar_t* _min_sigma = L"project.config.min_sigma";
		const wchar_t* _max_sigma = L"project.config.max_sigma";
		const wchar_t* _min_correlation = L"project.config.min_correlation";
		const wchar_t* _max_iteration = L"project.iteration.max_iteration";
		const wchar_t* _sigma_threshold = L"project.iteration.sigma_threshold";
		const wchar_t* _pho_path = L"project.datapath.pho_path";
		const wchar_t* _obj_path = L"project.datapath.obj_path";
		const wchar_t* _cam_path = L"project.datapath.cam_path";
	}

	namespace pho
	{
		/// For reading pho file
		const wchar_t* _imageObservation = L"image_observation";
		const wchar_t* _image = L"image";
		const wchar_t* _imageId = L"image_id";
		const wchar_t* _cameraId = L"camera_id";
		const wchar_t* _imaPath = L"path";
		const wchar_t* _eop = L"eop";
		const wchar_t* _xyz = L"xyz";
		const wchar_t* _opk = L"opk";
		const wchar_t* _unit = L"unit";
		const wchar_t* _type = L"type";
		const wchar_t* _points = L"points";
		const wchar_t* _point = L"point";
		const wchar_t* _pointId = L"point_id";
		const wchar_t* _coordinates = L"coordinates";
		const wchar_t* _variance = L"variance";
		const wchar_t* _time = L"time";
		const wchar_t* _value = L"value";
	}

	namespace obj
	{
		/// For reading obj file
		const wchar_t* _objectpoints = L"object_points";
		const wchar_t* _point = L"point";
		const wchar_t* _pointId = L"point_id";
		const wchar_t* _xyz = L"xyz";
		const wchar_t* _variance = L"variance";
		const wchar_t* _unit = L"unit";
	}

	namespace cam
	{
		/// For reading cam file
		const wchar_t* _cameras = L"cameras";
		const wchar_t* _camera = L"camera";
		const wchar_t* _camera_id = L"camera_id";
		const wchar_t* _focal_length = L"focal_length";
		const wchar_t* _principal_point = L"principal_point";
		const wchar_t* _lens_distortion = L"lens_distortion";
		const wchar_t* _smac = L"smac";
		const wchar_t* _description = L"description";
		const wchar_t* _parameter = L"parameter";
		const wchar_t* _value = L"value";
		const wchar_t* _variance = L"variance";
		const wchar_t* _boresight = L"boresight";
		const wchar_t* _offset = L"offset";
		const wchar_t* _alignment = L"alignment";
		const wchar_t* _distmodelname = L"model_name";
		const wchar_t* _unit = L"unit";
		const wchar_t* _type = L"type";
		const wchar_t* _pixel_pitch = L"pixel_pitch";
		const wchar_t* _sensor_size = L"sensor_size";
	}

	template<typename T>
	T unitConvert(const T& value, const std::wstring& unitName, const double mmPixelPitch = 0.0)
	{
		if (data::_pixel == unitName)
		{
			if (mmPixelPitch == 0.0)
				throw std::runtime_error("Wrong unit name in unitConverter: it cannot be a pixel value");
			else
				/// change unit to "mm"
				return (value * mmPixelPitch);
		}
		else if (data::_micrometer == unitName)
		{
			/// change unit to "mm"
			return (value * 0.001);
		}
		else if (data::_milimeter == unitName)
		{
			return value;
		}
		else if (data::_deg == unitName)
		{
			/// change unit to "radian"
			return value / 180.0 * util::PI;
		}
		else if (data::_rad == unitName)
		{
			return value;
		}
		else
		{
			throw std::runtime_error("Wrong(not supported) unit name in unitConverter.");
		}
	}

	template<typename T>
	T xyzOpkUnitConvert(const T& value, const std::wstring& unitName)
	{
		if (data::_meter == unitName)
		{
			return value;
		}
		else if (data::_rad == unitName)
		{
			return value;
		}
		else if (data::_deg == unitName)
		{
			/// change unit to "radian"
			return value / 180.0 * util::PI;
		}
		else
		{
			throw std::runtime_error("Wrong(not supported) unit name in unitConverter.");
		}
	}

	template<typename T> void readArray(std::vector<T>& values, const int size, const std::wstring& data)
	{
		std::wstringstream iss(data);
		values.resize(size);
		for (auto& val : values)
		{
			T number;
			if (iss >> number)
				val = number;
		}
	}

	struct Project
	{
		struct ProjectData
		{
			struct Iteration
			{
				unsigned int maxIteration = 20;
				double sigma_threshold = 1.0e-6;
			};

			struct Config
			{
				double minSigma = 1.0e-12;
				double maxSigma = 1.0e+12;
				double minCorrelation = 0.9;
			};

			struct DataPath
			{
				std::wstring phoPath = L"";
				std::wstring objPath = L"";
				std::wstring camPath = L"";
				std::wstring retPath = L"";
			};

			std::wstring name = L"No Name";
			std::wstring type = L"bundle_block";
			Config config;
			Iteration iteration;
			DataPath dataPath;
		};

		bool readProject(const std::wstring& xmlPath)
		{
			try
			{
				std::wcout << L"Project config file:\t" << xmlPath << std::endl;

				std::wifstream infile(xmlPath);
				/// set locale
				//path.imbue(std::locale("kor"));
				setlocale(LC_ALL, "");
				infile.imbue(std::locale(setlocale(LC_ALL, NULL)));
				/// Read a project config file (xml)
				bpt::wptree reader;
				bpt::read_xml(infile, reader);

				project.name = reader.get<std::wstring>(prj::_name);
				project.type = reader.get<std::wstring>(prj::_type);

				project.config.minSigma = reader.get<double>(prj::_min_sigma);
				project.config.maxSigma = reader.get<double>(prj::_max_sigma);
				project.config.minCorrelation = reader.get<double>(prj::_min_correlation);

				project.iteration.maxIteration = reader.get<int>(prj::_max_iteration);
				project.iteration.sigma_threshold = reader.get<double>(prj::_sigma_threshold);

				project.dataPath.phoPath = reader.get<std::wstring>(prj::_pho_path);
				project.dataPath.objPath = reader.get<std::wstring>(prj::_obj_path);
				project.dataPath.camPath = reader.get<std::wstring>(prj::_cam_path);

				/// A result file path
				project.dataPath.retPath = project.dataPath.phoPath;
				boost::replace_all(project.dataPath.retPath, ".pho", ".ret");
			}
			catch (...)
			{
				std::wcout << L"Error in reading a project config file(xml) in ProjectReader::ProjectReader(const std::string& xmlPath)" << std::endl;
				return false;
			}

			return true;
		}

		static ProjectData getPrjData(const std::wstring& prjName,
			const std::wstring& prjType,
			const double minS,
			const double maxS,
			const double minCorr,
			const unsigned int maxIterNum,
			const double sigmaTh)
		{
			ProjectData prjData;
			prjData.config.maxSigma = maxS;
			prjData.config.minSigma = minS;
			prjData.config.minCorrelation = minCorr;
			prjData.iteration.maxIteration = maxIterNum;
			prjData.iteration.sigma_threshold = sigmaTh;
			prjData.name = prjName;
			prjData.type = prjType;
			return prjData;
		}

		ProjectData project;
	};

	struct Pho
	{
		struct Image
		{
			struct ImgPt
			{
				unsigned int pointId;
				std::vector<double> coordinates;
				std::vector<double> dispersion;
			};

			std::wstring path;
			unsigned int imageId;
			unsigned int cameraId;
			std::vector<double> xyz;
			std::vector<double> xyzVariance;
			std::wstring xyzUnit;
			std::vector<double> opk;
			std::vector<double> opkVariance;
			std::wstring opkUnit;
			std::wstring timeTag; /// time tag
			std::wstring pointsUnit;
			std::wstring coordinateType;
			std::vector<ImgPt> imgPts;
		};

		bool readPho(const std::wstring& xmlPath)
		{
			try
			{
				std::wifstream path(xmlPath);
				if (!path)
					return false;
				bpt::wptree tree;
				read_xml(path, tree);

				images.clear();

				BOOST_FOREACH(const bpt::wptree::value_type & v, tree.get_child(pho::_imageObservation))
				{
					/// image data
					Image img;

					if (v.first == pho::_image)
					{
						BOOST_FOREACH(const bpt::wptree::value_type & imageChild, v.second.get_child(L""))
						{
							if (imageChild.first == L"<xmlattr>")
							{
								BOOST_FOREACH(bpt::wptree::value_type imageAttr, imageChild.second.get_child(L""))
								{
									if (imageAttr.first == pho::_imaPath)
										img.path = imageAttr.second.data();
								}
							}
							else if (imageChild.first == pho::_imageId)
							{
								img.imageId = imageChild.second.get_value<int>();
							}
							else if (imageChild.first == pho::_cameraId)
							{
								img.cameraId = imageChild.second.get_value<int>();
							}
							else if (imageChild.first == pho::_time)
							{
								img.timeTag = imageChild.second.get_value<std::wstring>();
							}
							else if (imageChild.first == pho::_eop)
							{
								BOOST_FOREACH(bpt::wptree::value_type eop, imageChild.second.get_child(L""))
								{
									if (eop.first == pho::_xyz)
									{
										BOOST_FOREACH(const bpt::wptree::value_type & xyz, eop.second.get_child(L""))
										{
											if (xyz.first == L"<xmlattr>")
											{
												BOOST_FOREACH(bpt::wptree::value_type xyzAttr, xyz.second.get_child(L""))
												{
													if (xyzAttr.first == pho::_unit)
														img.xyzUnit = xyzAttr.second.data();
												}
											}
											else if (xyz.first == pho::_value)
											{
												std::wstring data = xyz.second.data();
												readArray(img.xyz, 3, data);
											}
											else if (xyz.first == pho::_variance)
											{
												std::wstring data = xyz.second.data();
												readArray(img.xyzVariance, 9, data);
											}
										}
									}
									else if (eop.first == pho::_opk)
									{
										BOOST_FOREACH(const bpt::wptree::value_type & opk, eop.second.get_child(L""))
										{
											if (opk.first == L"<xmlattr>")
											{
												BOOST_FOREACH(bpt::wptree::value_type opkAttr, opk.second.get_child(L""))
												{
													if (opkAttr.first == pho::_unit)
														img.opkUnit = opkAttr.second.data();
												}
											}
											else if (opk.first == pho::_value)
											{
												std::wstring data = opk.second.data();
												readArray(img.opk, 3, data);
											}
											else if (opk.first == pho::_variance)
											{
												std::wstring data = opk.second.data();
												readArray(img.opkVariance, 9, data);
											}
										}
									}
									else
										continue;
								}
							}
							else if (imageChild.first == pho::_points)
							{
								BOOST_FOREACH(const bpt::wptree::value_type & points, imageChild.second.get_child(L""))
								{
									if (points.first == L"<xmlattr>")
									{
										BOOST_FOREACH(bpt::wptree::value_type pointsAttr, points.second.get_child(L""))
										{
											if (pointsAttr.first == pho::_unit)
												img.pointsUnit = pointsAttr.second.data();
											else if (pointsAttr.first == pho::_type)
												img.coordinateType = pointsAttr.second.data();
										}
									}
									else if (points.first == pho::_point)
									{
										Image::ImgPt pt;

										BOOST_FOREACH(const bpt::wptree::value_type & pointData, points.second.get_child(L""))
										{
											if (pointData.first == pho::_pointId)
											{
												pt.pointId = pointData.second.get_value<int>();
											}
											else if (pointData.first == pho::_coordinates)
											{
												std::wstring data = pointData.second.data();
												readArray(pt.coordinates, 2, data);
											}
											else if (pointData.first == pho::_variance)
											{
												std::wstring data = pointData.second.data();
												readArray(pt.dispersion, 4, data);
											}
											else
												continue;
										}

										/// add image point data
										img.imgPts.push_back(pt);
									}
								}
							}
						}
						/// add image data
						images.push_back(img);
					}
				}
			}
			catch (...)
			{
				std::wcout << L"Error in reading a img file(xml) in PhoReader::PhoReader(const std::string& xmlPath)" << std::endl;
			}

#ifdef _DEBUG
			echoPrint();
#endif
			return true;
		}

		void echoPrint()
		{
			/// Echo print
			std::wcout << L"-----Photo data echo-print-----" << std::endl;
			for (const auto& img : images)
			{
				std::wcout << std::endl;
				std::wcout << L"Image path:\t" << img.path << std::endl;
				std::wcout << L"\tImage id:\t" << img.imageId << std::endl;
				std::wcout << L"\tCamera id:\t" << img.cameraId << std::endl;
				std::wcout << L"\tExposure time:\t" << img.timeTag << std::endl;
				std::wcout << L"\tEop:\t" << std::endl;
				std::wcout << L"\t\txyz[" << img.xyzUnit << L"]:\t" << img.xyz[0] << "\t" << img.xyz[1] << "\t" << img.xyz[2] << std::endl;
				std::wcout << L"\t\tVariance";
				for (unsigned int i = 0; i < 9; ++i)
				{
					if (i % 3 == 0)
					{
						std::wcout << std::endl << L"\t\t\t";
					}

					std::wcout << img.xyzVariance[i] << L"\t";
				}
				std::wcout << std::endl;
				std::wcout << L"\t\topk[" << img.opkUnit << L"]:\t" << img.opk[0] << "\t" << img.opk[1] << "\t" << img.opk[2] << std::endl;
				std::wcout << L"\t\tVariance";
				for (unsigned int i = 0; i < 9; ++i)
				{
					if (i % 3 == 0)
					{
						std::wcout << std::endl << L"\t\t\t";
					}

					std::wcout << img.opkVariance[i] << L"\t";
				}
				std::wcout << std::endl;
				std::wcout << L"\tpoints[" << img.pointsUnit << L"]\t" << img.coordinateType << L":\t" << std::endl;
				for (unsigned int i = 0; i < img.imgPts.size(); ++i)
				{
					std::wcout << L"\t\t" << img.imgPts[i].pointId << L"\t";
					std::wcout << img.imgPts[i].coordinates[0] << L"\t" << img.imgPts[i].coordinates[1] << L"\t";
					std::wcout << img.imgPts[i].dispersion[0] << L"\t" << img.imgPts[i].dispersion[1] << L"\t";
					std::wcout << img.imgPts[i].dispersion[2] << L"\t" << img.imgPts[i].dispersion[3] << std::endl;
				}
			}
		}

		static data::Pho::Image getImage(const int id,
			const int camId,
			const std::vector<double>& opk,
			const std::wstring& opkUnit,
			const std::vector<double>& opkVariance,
			const std::vector<double>& xyz,
			const std::wstring& xyzUnit,
			const std::vector<double>& xyzVariance,
			const std::vector<data::Pho::Image::ImgPt>& imgPts,
			const std::wstring& imgPtCoordType,
			const std::wstring& ptCoordUnit,
			const std::wstring& imgPath,
			const std::wstring& timeTag)
		{
			data::Pho::Image img;
			img.imageId = id;
			img.cameraId = camId;
			img.opk = opk;
			img.opkUnit = opkUnit;
			img.opkVariance = opkVariance;
			img.xyz = xyz;
			img.xyzUnit = xyzUnit;
			img.xyzVariance = xyzVariance;
			img.imgPts = imgPts;
			img.coordinateType = imgPtCoordType;
			img.pointsUnit = ptCoordUnit;
			img.path = imgPath;
			img.timeTag = timeTag;
			return img;
		}

	public:
		std::vector<Image> images;
	};

	struct Obj
	{
		struct ObjectPoint
		{
			unsigned int pointId;
			std::vector<double> xyz;
			std::vector<double> variance;
		};

		using ObjectPoints = std::vector<ObjectPoint>;

		bool readObj(const std::wstring& xmlPath)
		{
			try
			{
				std::wifstream path(xmlPath);
				if (!path)
					return false;
				bpt::wptree tree;
				read_xml(path, tree);

				objPts.clear();

				BOOST_FOREACH(const bpt::wptree::value_type & v, tree.get_child(obj::_objectpoints))
				{
					if (v.first == L"<xmlattr>")
					{
						BOOST_FOREACH(bpt::wptree::value_type vAttr, v.second.get_child(L""))
						{
							if (vAttr.first == obj::_unit)
								unit = vAttr.second.data();
						}
					}
					else if (v.first == obj::_point)
					{
						/// object point data
						ObjectPoint objPoint;

						BOOST_FOREACH(const bpt::wptree::value_type & pointData, v.second.get_child(L""))
						{
							if (pointData.first == obj::_pointId)
							{
								objPoint.pointId = pointData.second.get_value<int>();
							}
							else if (pointData.first == obj::_xyz)
							{
								std::wstring data = pointData.second.data();
								readArray(objPoint.xyz, 3, data);
							}
							else if (pointData.first == obj::_variance)
							{
								std::wstring data = pointData.second.data();
								readArray(objPoint.variance, 9, data);
							}
							else
								continue;
						}

						/// add image data
						objPts.push_back(objPoint);
					}
				}
			}
			catch (...)
			{
				std::wcout << L"Error in reading a obj file(xml) in Obj::readObj(const std::string& xmlPath)" << std::endl;
			}

#ifdef _DEBUG
			echoPrint();
#endif
			return true;
		}

		void echoPrint()
		{
			/// Echo print
			std::wcout << L"-----Obj data echo-print-----" << std::endl;

			std::wcout << std::endl << "unit: " << this->unit << std::endl;

			for (const auto& objP : this->objPts)
			{
				std::wcout << std::endl;
				std::wcout << L"\tpoint id:\t" << objP.pointId << std::endl;
				std::wcout << L"\t\t" << objP.xyz[0] << "\t" << objP.xyz[1] << "\t" << objP.xyz[2] << std::endl;
				std::wcout << L"\t\tVariance";
				for (unsigned int i = 0; i < 9; ++i)
				{
					if (i % 3 == 0)
					{
						std::wcout << std::endl << L"\t\t";
					}

					std::wcout << objP.variance[i] << L"\t";
				}
				std::wcout << std::endl;
			}
		}

	public:
		ObjectPoints objPts;
		std::wstring unit;
	};

	struct Camera
	{
		struct CameraData
		{
			std::wstring type;
			int id;
			std::wstring fUnit;
			double f;
			double fVariance;
			std::wstring ppUnit;
			std::wstring ppCoordinateType;
			std::vector<double> pp;
			std::vector<double> ppVariance;
			std::wstring nameDist;
			std::wstring descriptDist;
			unsigned int numDistParams;
			std::vector<double> distParams;
			std::vector<double> distParamsVariance;
			std::wstring offsetUnit;
			std::vector<double> offset;
			std::vector<double> offsetVariance;
			std::wstring boresightUnit;
			std::vector<double> boresight;
			std::vector<double> boresightVariance;
			std::wstring pixelPitchUnit;
			double pixelPitch;
			unsigned int width;
			unsigned int height;
		};

		bool readCamera(const std::wstring& xmlPath)
		{
			try
			{
				std::wifstream path(xmlPath);
				if (!path)
					return false;
				bpt::wptree tree;
				read_xml(path, tree);

				cameras.clear();

				BOOST_FOREACH(const bpt::wptree::value_type & v, tree.get_child(cam::_cameras))
				{
					if (v.first == cam::_camera)
					{
						/// Camera data
						CameraData cam;

						cam.type = v.second.get_child(L"<xmlattr>.type").data();

						BOOST_FOREACH(const bpt::wptree::value_type & camera, v.second.get_child(L""))
						{
							if (camera.first == cam::_camera_id)
							{
								cam.id = camera.second.get_value<int>();
							}
							else if (camera.first == cam::_focal_length)
							{
								BOOST_FOREACH(const bpt::wptree::value_type & fl, camera.second.get_child(L""))
								{
									if (fl.first == L"<xmlattr>")
									{
										BOOST_FOREACH(bpt::wptree::value_type flAttr, fl.second.get_child(L""))
										{
											if (flAttr.first == cam::_unit)
												cam.fUnit = flAttr.second.data();
										}
									}
									else if (fl.first == cam::_value)
									{
										cam.f = fl.second.get_value<double>();
									}
									else if (fl.first == cam::_variance)
									{
										cam.fVariance = fl.second.get_value<double>();
									}
								}
							}
							else if (camera.first == cam::_principal_point)
							{
								BOOST_FOREACH(const bpt::wptree::value_type & pp, camera.second.get_child(L""))
								{
									if (pp.first == L"<xmlattr>")
									{
										BOOST_FOREACH(bpt::wptree::value_type ppAttr, pp.second.get_child(L""))
										{
											if (ppAttr.first == cam::_unit)
												cam.ppUnit = ppAttr.second.data();
											else if (ppAttr.first == cam::_type)
												cam.ppCoordinateType = ppAttr.second.data();
										}
									}
									else if (pp.first == cam::_value)
									{
										std::wstring data = pp.second.data();
										readArray(cam.pp, 2, data);
									}
									else if (pp.first == cam::_variance)
									{
										std::wstring data = pp.second.data();
										readArray(cam.ppVariance, 2 * 2, data);
									}
								}
							}
							else if (camera.first == cam::_pixel_pitch)
							{
								cam.pixelPitch = camera.second.get_value<double>();

								BOOST_FOREACH(const bpt::wptree::value_type & pitch, camera.second.get_child(L""))
								{
									if (pitch.first == L"<xmlattr>")
									{
										BOOST_FOREACH(bpt::wptree::value_type pitchAttr, pitch.second.get_child(L""))
										{
											if (pitchAttr.first == cam::_unit)
												cam.pixelPitchUnit = pitchAttr.second.data();
										}
									}
								}
							}
							else if (camera.first == cam::_sensor_size)
							{
								std::wstring data = camera.second.data();
								std::vector<unsigned int> size;
								readArray(size, 2, data);
								cam.width = size[0];
								cam.height = size[1];
							}
							else if (camera.first == cam::_lens_distortion)
							{
								BOOST_FOREACH(const bpt::wptree::value_type & distortion, camera.second.get_child(L""))
								{
									if (distortion.first == L"<xmlattr>")
									{
										BOOST_FOREACH(const bpt::wptree::value_type & attributes, distortion.second.get_child(L""))
										{
											if (attributes.first == cam::_distmodelname)
											{
												cam.nameDist = attributes.second.data();
												if (cam.nameDist == cam::_smac)
												{
													cam.numDistParams = constant::smac::NUM_SMAC_PARMS;
												}
												else
												{
													cam.nameDist = L"unknown";
													cam.numDistParams = 0;
												}
											}
										}
									}
									else if (distortion.first == cam::_description)
									{
										cam.descriptDist = distortion.second.data();
									}
									else if (distortion.first == cam::_parameter)
									{
										BOOST_FOREACH(const bpt::wptree::value_type & child, distortion.second.get_child(L""))
										{
											if (child.first == cam::_value)
											{
												std::wstring data = child.second.data();
												if (cam.nameDist == cam::_smac)
												{
													readArray(cam.distParams, cam.numDistParams, data);
												}
											}
											else if (child.first == cam::_variance)
											{
												std::wstring data = child.second.data();
												if (cam.nameDist == cam::_smac)
												{
													readArray(cam.distParamsVariance, cam.numDistParams * cam.numDistParams, data);
												}
											}
										}
									}
								}
							}
							else if (camera.first == cam::_alignment)
							{
								BOOST_FOREACH(const bpt::wptree::value_type & alignment, camera.second.get_child(L""))
								{
									if (alignment.first == cam::_offset)
									{
										BOOST_FOREACH(const bpt::wptree::value_type & offset, alignment.second.get_child(L""))
										{
											if (offset.first == L"<xmlattr>")
											{
												BOOST_FOREACH(bpt::wptree::value_type offsetAttr, offset.second.get_child(L""))
												{
													if (offsetAttr.first == cam::_unit)
														cam.offsetUnit = offsetAttr.second.data();
												}
											}
											else if (offset.first == cam::_value)
											{
												std::wstring data = offset.second.data();
												readArray(cam.offset, 3, data);
											}
											else if (offset.first == cam::_variance)
											{
												std::wstring data = offset.second.data();
												readArray(cam.offsetVariance, 3 * 3, data);
											}
										}
									}
									else if (alignment.first == cam::_boresight)
									{
										cam.boresightUnit = alignment.second.get_child(L"<xmlattr>.unit").data();
										std::wstring data = alignment.second.data();
										readArray(cam.boresight, 3, data);

										BOOST_FOREACH(const bpt::wptree::value_type & bore, alignment.second.get_child(L""))
										{
											if (bore.first == L"<xmlattr>")
											{
												BOOST_FOREACH(bpt::wptree::value_type boreAttr, bore.second.get_child(L""))
												{
													if (boreAttr.first == cam::_unit)
														cam.boresightUnit = boreAttr.second.data();
												}
											}
											else if (bore.first == cam::_value)
											{
												std::wstring data = bore.second.data();
												readArray(cam.boresight, 3, data);
											}
											else if (bore.first == cam::_variance)
											{
												std::wstring data = bore.second.data();
												readArray(cam.boresightVariance, 3 * 3, data);
											}
										}
									}
								}
							}
						}

						/// add image data
						cameras.push_back(cam);
					}
				}
			}
			catch (...)
			{
				std::wcout << L"Error in reading a cam file(xml) in CameraReader::CameraReader(const std::string& xmlPath)" << std::endl;
			}

#ifdef _DEBUG
			echoPrint();
#endif
			return true;
		}

		void echoPrint() const
		{
			/// Echo print
			std::wcout << L"-----Camera information echo-print-----" << std::endl;
			for (const auto& cam : cameras)
			{
				std::wcout << std::endl;
				std::wcout << L"Camera id:\t" << cam.id << std::endl;
				std::wcout << L"\t" << L"Camera type:\t" << cam.type << std::endl;
				std::wcout << L"\t" << L"Focal length[" << cam.fUnit << L"]:\t" << cam.f << L"\tVariance:\t" << cam.fVariance << std::endl;
				std::wcout << L"\t" << L"Principal point[" << cam.ppUnit << L"]\t" << cam.ppCoordinateType << ":\t" << cam.pp[0] << L"\t" << cam.pp[1] << std::endl;
				std::wcout << L"\t\t" << "Variance:" << std::endl;
				for (const auto& var : cam.ppVariance)
					std::wcout << L"\t\t" << var << L"\t";
				std::wcout << std::endl;
				std::wcout << L"\t" << L"Pixel pitch[" << cam.pixelPitchUnit << L"]:\t" << cam.pixelPitch << std::endl;
				std::wcout << L"\t" << L"Sensor width and height: " << cam.width << L"\t" << cam.height << std::endl;
				std::wcout << L"\t" << L"Lens distortion[" << cam.nameDist << L"]" << std::endl;
				std::wcout << L"\t" << cam.descriptDist << std::endl;
				std::wcout << L"\t\tDistortion parameters";
				std::wcout << std::endl << L"\t\t"; 
				for (const auto& var : cam.distParams)
					std::wcout << var << L"\t";
				std::wcout << std::endl;
				std::wcout << L"\t\tVariance";
				for (unsigned int i = 0; i < cam.numDistParams * cam.numDistParams; ++i)
				{
					if (i % cam.numDistParams == 0)
					{
						std::wcout << std::endl << L"\t\t";
					}
					std::wcout << cam.distParamsVariance[i] << L"\t";
				}
				std::wcout << std::endl;
				std::wcout << L"\t" << L"Offset[" << cam.offsetUnit << L"]:\t" << cam.offset[0] << L"\t" << cam.offset[1] << L"\t" << cam.offset[2] << std::endl;
				std::wcout << L"\t\tVariance";
				for (unsigned int i = 0; i < 9; ++i)
				{
					if (i % 3 == 0)
					{
						std::wcout << std::endl << L"\t\t";
					}
					std::wcout << cam.offsetVariance[i] << L"\t";
				}
				std::wcout << std::endl;
				std::wcout << L"\t" << L"Boresight[" << cam.boresightUnit << L"]:\t" << cam.boresight[0] << L"\t" << cam.boresight[1] << L"\t" << cam.boresight[2] << std::endl;
				std::wcout << L"\t\tVariance";
				for (unsigned int i = 0; i < 9; ++i)
				{
					if (i % 3 == 0)
					{
						std::wcout << std::endl << L"\t\t";
					}
					std::wcout << cam.boresightVariance[i] << L"\t";
				}
				std::wcout << std::endl;
			}
		}

		std::vector<std::shared_ptr<sensor::FrameCamera>> getFrameCameras(const double minSigma)
		{
			std::vector<std::shared_ptr<sensor::FrameCamera>> camPtrs;

			for (const auto& cam : cameras)
			{
				if (data::_frame == cam.type)
				{
					std::shared_ptr<sensor::FrameCamera> fcam = std::make_shared<sensor::FrameCamera>();

					/// Id
					setId(cam, *fcam);
					/// Focal length
					setf(cam, *fcam);
					/// xp yp
					setxpyp(cam, *fcam);
					/// width and height
					setWidthHeight(cam, *fcam, minSigma);
					/// Pixel pitch			
					setPixPitch(cam, *fcam, minSigma);
					/// Alignment: boresight and lever-arm
					setAlignment(cam, *fcam);
					/// Distortion
					setDistortion(cam, *fcam, minSigma);

					//camPtrs.push_back(static_pointer_cast<sensor::Camera>(fcam));
					camPtrs.push_back(fcam);
				}
				else
				{
					std::wcout << L"Wrong sensor type name: " << cam.type << std::endl;
				}
			}

			return camPtrs;
		}

		void setId(const CameraData& cam, sensor::FrameCamera& frameCam)
		{
			/// Id
			frameCam.setId(cam.id);
		}

		void setf(const CameraData& cam, sensor::FrameCamera& frameCam)
		{
			double f = unitConvert(cam.f, cam.fUnit, unitConvert(cam.pixelPitch, cam.pixelPitchUnit));
			double fStdev = unitConvert(sqrt(cam.fVariance), cam.fUnit, unitConvert(cam.pixelPitch, cam.pixelPitchUnit));
			double fVar = fStdev * fStdev;

			param::Parameter fParam;
			fParam.set(f);
			fParam.setDelta(1.0e-6);
			fParam.setDescription("Lens focal length");
			fParam.setInitVal(f);
			fParam.setName(constant::camera::_f_);
			fParam.setStDev(sqrt(fVar));

			frameCam.setFL(fParam);
		}

		void setxpyp(const CameraData& cam, sensor::FrameCamera& frameCam)
		{
			double pitch = unitConvert(cam.pixelPitch, cam.pixelPitchUnit);
			double xp = cam.pp[0];
			double yp = cam.pp[1];
			
			math::Matrixd varMat(2, 2);
			double* var = varMat.getDataHandle();
			for (unsigned int i = 0; i < 4; ++i)
			{
				var[i] = cam.ppVariance[i];
			}

			if (data::_phoCoordinates == cam.ppCoordinateType)
			{
				/// Do notihing
			}
			else if (data::_imgCoordinates == cam.ppCoordinateType)
			{
				/// first value is row and second value is col
				/// row is y and col is x;
				/// 
				/// image coordinate type: (row,   col)
				/// photo coordinate type: (x,	   y  )
				/// x correspondes to col and y correspondes to row		
				/// therefore, swap two values
				auto row = xp;
				auto col = yp;
				xp = col - cam.width * 0.5;
				yp = cam.height * 0.5 - row;

				double rowVar = varMat(0, 0);
				double colVar = varMat(1, 1);
				varMat(0, 0) = colVar;
				varMat(1, 1) = rowVar;
			}
			else
				throw std::runtime_error("Wrong coordinate type of principal point");

			xp = unitConvert(xp, cam.ppUnit, pitch);
			yp = unitConvert(yp, cam.ppUnit, pitch);

			for (unsigned int i = 0; i < 4; ++i)
			{
				double stdev = unitConvert(sqrt(var[i]), cam.ppUnit, pitch);
				var[i] = stdev * stdev;
			}			

			std::vector<param::Parameter> ppParamSet(2);
			ppParamSet[0].set(xp);
			ppParamSet[0].setDelta(1.0e-6);
			ppParamSet[0].setDescription("x of a principal point");
			ppParamSet[0].setInitVal(xp);
			ppParamSet[0].setName(constant::camera::_xp_);
			ppParamSet[0].setStDev(sqrt(varMat(0, 0)));

			ppParamSet[1].set(yp);
			ppParamSet[1].setDelta(1.0e-6);
			ppParamSet[1].setDescription("y of a principal point");
			ppParamSet[1].setInitVal(yp);
			ppParamSet[1].setName(constant::camera::_yp_);
			ppParamSet[1].setStDev(sqrt(varMat(1, 1)));

			frameCam.setPP(ppParamSet);
		}

		void setPixPitch(const CameraData& cam, sensor::FrameCamera& frameCam, const double minSigma)
		{
			double pitch = unitConvert(cam.pixelPitch, cam.pixelPitchUnit);

			param::Parameter pitchParam;
			pitchParam.set(pitch);
			pitchParam.setDelta(1.0e-6);
			pitchParam.setDescription("Pixel pitch of a detector");
			pitchParam.setInitVal(pitch);
			pitchParam.setName(constant::camera::_pixel_pitch_);
			pitchParam.setStDev(minSigma);

			frameCam.setPixPitch(pitchParam);
		}

		void setWidthHeight(const CameraData& cam, sensor::FrameCamera& frameCam, const double minSigma)
		{
			param::Parameter wParam, hParam;

			wParam.set(cam.width);
			wParam.setDelta(1.0e-6);
			wParam.setDescription("Sensor width");
			wParam.setInitVal(cam.width);
			wParam.setName(constant::camera::_width_);
			wParam.setStDev(minSigma);

			hParam.set(cam.height);
			hParam.setDelta(1.0e-6);
			hParam.setDescription("Sensor height");
			hParam.setInitVal(cam.height);
			hParam.setName(constant::camera::_height_);
			hParam.setStDev(minSigma);

			frameCam.setSensorSize(wParam, hParam);
		}

		static void setAlignment(const CameraData& cam, sensor::FrameCamera& frameCam)
		{
			std::vector<param::ParameterPtr> boresight(3);
			for (auto& p : boresight)
				p = std::make_shared<param::Parameter>();

			auto boresight_0 = xyzOpkUnitConvert(cam.boresight[0], cam.boresightUnit);
			auto boresightSigma_0 = xyzOpkUnitConvert(sqrt(cam.boresightVariance[0]), cam.boresightUnit);
			auto boresight_1 = xyzOpkUnitConvert(cam.boresight[1], cam.boresightUnit);
			auto boresightSigma_4 = xyzOpkUnitConvert(sqrt(cam.boresightVariance[4]), cam.boresightUnit);
			auto boresight_2 = xyzOpkUnitConvert(cam.boresight[2], cam.boresightUnit);
			auto boresightSigma_8 = xyzOpkUnitConvert(sqrt(cam.boresightVariance[8]), cam.boresightUnit);

			boresight[0]->set(boresight_0);
			boresight[0]->setDelta(0.000001);
			boresight[0]->setDescription("boresight_omega");
			boresight[0]->setInitVal(boresight_0);
			boresight[0]->setName(constant::camera::_omega_bs_);
			boresight[0]->setStDev(boresightSigma_0);

			boresight[1]->set(boresight_1);
			boresight[1]->setDelta(0.000001);
			boresight[1]->setDescription("boresight_phi");
			boresight[1]->setInitVal(boresight_1);
			boresight[1]->setName(constant::camera::_phi_bs_);
			boresight[1]->setStDev(boresightSigma_4);

			boresight[2]->set(boresight_2);
			boresight[2]->setDelta(0.000001);
			boresight[2]->setDescription("boresight_kappa");
			boresight[2]->setInitVal(boresight_2);
			boresight[2]->setName(constant::camera::_kappa_bs_);
			boresight[2]->setStDev(boresightSigma_8);

			frameCam.setBoresight(boresight);

			std::vector<param::ParameterPtr> leverarm(3);
			for (auto& p : leverarm)
				p = std::make_shared<param::Parameter>();

			auto offset_0 = xyzOpkUnitConvert(cam.offset[0], cam.offsetUnit);
			auto offsetSigma_0 = xyzOpkUnitConvert(sqrt(cam.offsetVariance[0]), cam.offsetUnit);
			auto offset_1 = xyzOpkUnitConvert(cam.offset[1], cam.offsetUnit);
			auto offsetSigma_4 = xyzOpkUnitConvert(sqrt(cam.offsetVariance[4]), cam.offsetUnit);
			auto offset_2 = xyzOpkUnitConvert(cam.offset[2], cam.offsetUnit);
			auto offsetSigma_8 = xyzOpkUnitConvert(sqrt(cam.offsetVariance[8]), cam.offsetUnit);

			leverarm[0]->set(offset_0);
			leverarm[0]->setDelta(0.000001);
			leverarm[0]->setDescription("lever-arm: dX");
			leverarm[0]->setInitVal(offset_0);
			leverarm[0]->setName(constant::camera::_dX_);
			leverarm[0]->setStDev(offsetSigma_0);

			leverarm[1]->set(offset_1);
			leverarm[1]->setDelta(0.000001);
			leverarm[1]->setDescription("lever-arm: dY");
			leverarm[1]->setInitVal(offset_1);
			leverarm[1]->setName(constant::camera::_dY_);
			leverarm[1]->setStDev(offsetSigma_4);

			leverarm[2]->set(offset_2);
			leverarm[2]->setDelta(0.000001);
			leverarm[2]->setDescription("lever-arm: dZ");
			leverarm[2]->setInitVal(offset_2);
			leverarm[2]->setName(constant::camera::_dZ_);
			leverarm[2]->setStDev(offsetSigma_8);

			frameCam.setLeverArm(leverarm);
		}

		void setDistortion(const CameraData& cam0, sensor::FrameCamera& frameCam, const double minSigma)
		{
			CameraData cam = cam0;

			/// Distortion
			std::shared_ptr<sensor::DistortionModel> distortion;
			/// Distortion parameters
			std::vector<std::shared_ptr<param::Parameter>> distParameters;

			/// SMAC parameter names
			std::vector<std::string> smacName(sensor::numSmacParams);
			smacName[0] = constant::smac::_k0_;
			smacName[1] = constant::smac::_k1_;
			smacName[2] = constant::smac::_k2_;
			smacName[3] = constant::smac::_k3_;
			smacName[4] = constant::smac::_p1_;
			smacName[5] = constant::smac::_p2_;
			smacName[6] = constant::smac::_p3_;

			if (cam.nameDist == data::cam::_smac && cam.numDistParams == sensor::numSmacParams)
			{
				distortion = std::make_shared<sensor::SMACModel>();
				distParameters.resize(cam.numDistParams);

				for (unsigned int i = 0; i < distParameters.size(); ++i)
				{
					distParameters[i] = std::make_shared<param::Parameter>();
					distParameters[i]->set(cam.distParams[i]);
					distParameters[i]->setDelta(0.000001);
					distParameters[i]->setDescription(util::uni2multi(cam.descriptDist));
					distParameters[i]->setInitVal(cam.distParams[i]);
					distParameters[i]->setName(smacName[i]);
					distParameters[i]->setStDev(sqrt(cam.distParamsVariance[static_cast<unsigned int>(i * cam.numDistParams + i)]));

					if (i == 6)
						distParameters[i]->setStDev(minSigma);

				}
			}
			else
			{
				/// set dummy smac parameters
				distortion = std::make_shared<sensor::SMACModel>();
				distParameters.resize(sensor::numSmacParams);

				for (unsigned int i = 0; i < distParameters.size(); ++i)
				{
					distParameters[i] = std::make_shared<param::Parameter>();
					distParameters[i]->set(0.0);
					distParameters[i]->setDelta(0.000001);
					distParameters[i]->setDescription("zero-parameter");
					distParameters[i]->setInitVal(0.0);
					distParameters[i]->setName(smacName[i]);
					distParameters[i]->setStDev(minSigma);
				}

#ifdef _DEBUG
				std::cerr << "Wrong distortion type : dist model is not a correct SMAC. Force set zero values to smac parameters" << std::endl;
#endif
			}

			distortion->setParameters(distParameters);
			frameCam.setDistortionModel(distortion);
		}

		static data::Camera::CameraData getCameraData(const int id,
			const std::wstring& camType,
			const double fl,
			const std::wstring& fUnit,
			const double fVariance,
			const unsigned int sensorW,
			const unsigned int sensorH,
			const double pixPitch,
			const std::wstring& pixPitchUnit,
			const std::vector<double>& pp,
			const std::wstring& ppCoordType,
			const std::wstring& ppUnit,
			const std::vector<double>& ppVariance,
			const std::vector<double>& boresight,
			const std::wstring& boresightUnit,
			const std::vector<double>& boresightVariance,
			const std::vector<double>& offset,
			const std::wstring& offsetUnit,
			const std::vector<double>& offsetVariance,
			const std::wstring& distModelName,
			const std::wstring& descDistModel,
			const unsigned int numDistParam,
			const std::vector<double>& distParams,
			const std::vector<double>& distParamVariance)
		{
			data::Camera::CameraData camData;
			camData.id = id;
			camData.type = camType;
			camData.f = fl;
			camData.fUnit = fUnit;
			camData.fVariance = fVariance;
			camData.width = sensorW;
			camData.height = sensorH;
			camData.pixelPitch = pixPitch;
			camData.pixelPitchUnit = pixPitchUnit;
			camData.pp = pp;
			camData.ppCoordinateType = ppCoordType;
			camData.ppUnit = ppUnit;
			camData.ppVariance = ppVariance;
			camData.boresight = boresight;
			camData.boresightUnit = boresightUnit;
			camData.boresightVariance = boresightVariance;
			camData.offset = offset;
			camData.offsetUnit = offsetUnit;
			camData.offsetVariance = offsetVariance;
			camData.nameDist = distModelName;
			camData.descriptDist = descDistModel;
			camData.numDistParams = numDistParam;
			camData.distParams = distParams;
			camData.distParamsVariance = distParamVariance;
			return camData;
		}

	public:
		std::vector<CameraData> cameras;
	};
}