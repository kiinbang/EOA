/*
* Copyright(c) 2001-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <SSM/include/SSMMatrix.h>

namespace math
{
	/**
	*@class FixedSizeMat Class
	*@brief Class for supporting a fixed-size matrix
	*/
	template<typename T, size_t r, size_t c>
	class FixedSizeMat : public Matrix<T>
	{
	public:
		FixedSizeMat(const T val = T(0))
			: Matrix<T>(r, c, val)
		{
		}

		FixedSizeMat(const FixedSizeMat<T, r, c>& mat)
			: Matrix<T>(r, c, 0.0)
		{
			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] = mat.getAccessData()[i];
		}

		template<typename S>
		FixedSizeMat(const Matrix<S>& mat)
			: Matrix<T>(r, c, 0.0)
		{
			if (r != mat.getRows() || c != mat.getCols())
				throw std::runtime_error("Wrong size error from FixedSizeMat constructor");

			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] = static_cast<T>(mat.getAccessData()[i]);
		}

		void operator = (const FixedSizeMat<T, r, c>& mat)
		{
			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] = mat.getAccessData()[i];
		}

		template<typename S>
		void operator = (const Matrix<S>& mat)
		{
			if (r != mat.getRows() || c != mat.getCols())
				throw std::runtime_error("Wrong size error from FixedSizeMat constructor");

			for (unsigned int i = 0; i < this->getSize(); ++i)
				this->getDataHandle()[i] = static_cast<T>(mat.getAccessData()[i]);
		}
	};

	/**
	*@class VectorMat Class
	*@brief Class for supporting a fixed-size 1d matrix (vector)
	*/
	template<typename T, size_t d>
	class VectorMat : public Matrix<T>
	{
	public:
		VectorMat(const T val = T(0))
			: Matrix<T>(d, 1, val)
		{
		}

		VectorMat(const VectorMat<T, d>& mat)
			: Matrix<T>(d, 1, T(0))
		{
			for (unsigned int i = 0; i < d; ++i)
				this->operator()(i) = mat(i);
		}

		template<typename S>
		VectorMat(const Matrix<S>& mat)
			: Matrix<T>(d, 1, T(0))
		{
			if (d != mat.getRows() || mat.getCols() != 1)
				throw std::runtime_error("Wrong size error from VectorMat constructor");

			for (unsigned int i = 0; i < d; ++i)
				this->operator()(i) = static_cast<T>(mat(i));
		}

		void operator = (const VectorMat<T, d>& mat)
		{
			for (unsigned int i = 0; i < d; ++i)
				this->operator()(i) = mat(i);
		}

		template<typename S>
		void operator = (const Matrix<S>& mat)
		{
			if (d != mat.getRows() || mat.getCols() != 1)
				throw std::runtime_error("Wrong size error from VectorMat constructor");

			for (unsigned int i = 0; i < d; ++i)
				this->operator()(i) = static_cast<T>(mat(i));
		}

		// vector operations
	public:
		VectorMat<T, d> cross(const VectorMat<T, d>& mat) const
		{
			VectorMat<T, d> result;

			for (unsigned int i = 0; i < d; i++)
			{
				unsigned int idx1, idx2;
				idx1 = static_cast<unsigned int>(fmod(i + 1, d));
				idx2 = static_cast<unsigned int>(fmod(i + 2, d));

				result(i, 0) = this->getAccessData[idx1] * mat.getAccessData[idx2] - mat.getAccessData[idx1] * this->getAccessData[idx2];
			}

			return result;
		}

		T dot(const VectorMat<T, d>& mat) const
		{
			T result = 0.0;

			for (unsigned int i = 0; i < d; i++)
			{
				result += this->getAccessData[i] * mat.getAccessData[i];
			}

			return result;
		}
	};
}