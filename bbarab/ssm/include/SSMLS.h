/*
* Copyright(c) 2005-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <fstream>
#include <stdio.h>
#include <vector>

//#include <ssm/include/ssmList.h>
#include <ssm/include/SSMRETMAT.h>

namespace ssm
{
	/// Note that  this is for the case including iops, eops, points, lines represented two points and planar patches represented three points,
	/// and it corresponds to the collinearity equations.
	/// For the use of other features, unknown parameters and models (such as a coplanarity condition), it should be modified and add additional functions.
	class LeastSquare
	{
	public:

		struct AMat
		{
			math::Matrixd a;
			unsigned int index=0; /// dataset index
			unsigned int sect=0; /// section index
		};

		class NdotMat
		{
			friend class CdotMat;
		public:
			NdotMat()
			{
				const unsigned int nParams = 0;
				const unsigned int nSets = 0;

				config(nParams, nSets);
			}

			NdotMat(const unsigned int nParams, const unsigned int nSets)
			{
				config(nParams, nSets);
			}

			void config(const unsigned int nParams, const unsigned int nSets)
			{
				numSets = nSets;
				numParams = nParams;

				dot.resize(numSets);
				for(unsigned int i=0; i<dot.size(); ++i)
				{
					dot[i].resize(nParams, nParams, 0.0);
				}
			}

			std::vector<math::Matrixd>& get() { return dot; }

			const std::vector<math::Matrixd>& get() const { return dot; }

			unsigned int getNumParams() { return numParams; }

			unsigned int getNumSets() { return numSets; }

		protected:
			std::vector<math::Matrixd> dot;
			unsigned int numParams;
			unsigned int numSets;
		};

		class CdotMat : public NdotMat
		{
		public:
			CdotMat() : NdotMat() {}

			CdotMat(const unsigned int nParams, const unsigned int nSets) : NdotMat(nParams, nSets) {}

			void config(const unsigned int nParams, const unsigned int nSets)
			{
				numSets = nSets;
				numParams = nParams;

				dot.resize(numSets);
				for(unsigned int i=0; i<dot.size(); ++i)
				{
					dot[i].resize(nParams, 1, 0.0);
				}
			}
		};

		class NbarMat
		{
		public:
			NbarMat()
			{
				const unsigned int nSets1 = 0;
				const unsigned int nSets2 = 0;

				config(nSets1, nSets1);
			}

			NbarMat(const unsigned int nSets1, const unsigned int nSets2)
			{
				config(nSets1, nSets2);
			}

			void config(const unsigned int nSets1, const unsigned int nSets2)
			{
				numSets1 = nSets1;
				numSets2 = nSets2;

				nbar.resize(numSets1);
				for(unsigned int i=0; i<nbar.size(); ++i)
				{
					auto& nbari = nbar[i];

					nbari.resize(numSets2);
					for (unsigned int j = 0; j < nbar[i].size(); ++j)
					{
						nbar[i][j].del();
					}
					/// Make the matrix empty because only some of sub-parts of N-bar matrix are not empty and it need to save memory.
				}
			}

			std::vector<std::vector<math::Matrixd>>& get() { return nbar; }

			const std::vector<std::vector<math::Matrixd>>& readOnly() const { return nbar; }

			unsigned int getNumSets1() { return numSets1; }
			unsigned int getNumSets2() { return numSets2; }

		private:
			std::vector<std::vector<math::Matrixd>> nbar;
			unsigned int numSets1;
			unsigned int numSets2;
		};

		/** Constructor */
		LeastSquare();
		
		/** Constructor */
		LeastSquare(const std::vector<unsigned int> numParams,/**<number of parameters for each section*/
			const std::vector<unsigned int> numDatasets /**<number of input datasets for each section*/);

		/**destructor*/
		virtual ~LeastSquare();

		/** configuration */
		void config(const std::vector<unsigned int> numParams,/**<number of parameters for each section*/
			const std::vector<unsigned int> numDatasets /**<number of input datasets for each section*/);

		/**initialized the object*/
		void init();

		/**simple LS without weight matrix*/
		RetMat runLeastSquare(const math::Matrixd& A, const math::Matrixd& L);

		/**simple and fast(simple result, only X matrix) LS*/
		math::Matrixd runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L);

		/**LS using weight matrix, not using reduced normal matrix for simple modeling*/
		RetMat runLeastSquare(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W);

		/**LS using weight matrix, not using reduced normal matrix for simple modeling<br>
		*simple result, only X matrix, fast a little.
		*/
		math::Matrixd runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W);

		math::Matrixd runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W, math::Matrixd& N);

		/**Get correlation matrix from variance-covariance matrix*/
		static math::Matrixd getCorrelation(const math::Matrixd& VarCov);

		/**Extract variance(diagonal element) matrix from variance-covariance matrix*/
		static math::Matrixd extractVar(const math::Matrixd& VarCov);

		/**GetErrorList*/
		std::vector<double> getErrorList() const;

		////////////////////////////
		///Reduced Normal Matrix////
		////////////////////////////
		
		/**Solve*/
		math::Matrixd runLeastSquare(double& var, math::Matrixd& X, const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect, math::Matrixd& retN);

		/**Solve using the reduced normal matrix approach*/
		void runLeastSquare_RN(double& var, math::Matrixd& N_dot, math::Matrixd& C_dot, math::Matrixd& C_2dot, math::Matrixd& N_bar, math::Matrixd& X, const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect);

		/**Levenberg-Marquardt*/
		void runLeastSquare_RN_LM(double& var, math::Matrixd& N_dot, math::Matrixd& C_dot, math::Matrixd& C_2dot, math::Matrixd& N_bar, math::Matrixd& X, const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect, const double& mult = 1.0);

		/**Solve using partitioned inverse matrix*/
		math::Matrixd runLeastSquare_Partitioned(double& var, math::Matrixd& X, const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect, math::Matrixd& retN);

		/**Parameters' constraints*/
		double fill_Nmat_Param(const math::Matrixd& aParam, const math::Matrixd& w, const math::Matrixd& l, const unsigned int index, const unsigned int section);

		/**fill the normal matrix*/
		double fillNmat(const std::vector<AMat>& a, const math::Matrixd& w, const math::Matrixd& l, const int adjustedNumEq = 0);

		/**get a normal matrix*/
		math::Matrixd getNmat(const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect);

		/**get a normal matrix*/
		std::vector<math::Matrixd> getPartitionedNmat(const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect);

	protected:
		/**Make N_dot matrix from N_dot elements list*/
		math::Matrixd make_N_dot_New(const unsigned int sizeOfUpper, const unsigned int boundarySect) const;

		/**Make C_dot matrix from C_dot elements list*/
		math::Matrixd make_C_dot_New(const unsigned int sizeOfUpper, const unsigned int sect) const;

		/**Make C_2dot matrix from C_2dot_point and C_2dot_line elements list & triangle patches for LIDAR*/
		math::Matrixd make_C_2dot_New(const unsigned int sizeOfLower, const unsigned int sect) const;

		/**write given string on given file*/
		void fileOut(const std::string& fname, const std::string& st) const;

		/**get Cdot matrix for each section*/
		math::Matrixd getCdot(const unsigned int idx) const;

		/**get Ndot matrix for each section*/
		math::Matrixd getNdot(const unsigned int idx) const;

		/**get Nbar matrix for each off-diagonal section*/
		math::Matrixd getNbar(const unsigned int idx1, const unsigned int idx2) const;

		/**fix parameters*/
		void fixParametersRN(math::Matrixd& Ndot, math::Matrixd& Nbar, math::Matrixd& Cdot, math::Matrixd& C2dot, const unsigned int boundarySect, const std::vector<unsigned int>& fixedParamIdx);

		/**fix parameters*/
		void fixParametersPartitioned(math::Matrixd& Ndot, math::Matrixd& N2dot, math::Matrixd& Nbar, math::Matrixd& C, const std::vector<unsigned int>& fixedParamIdx);

		/**fix parameters*/
		void fixParametersPartitioned(math::Matrixd& Ndot, math::Matrixd& N2dot, math::Matrixd& Nbar, const std::vector<unsigned int>& fixedParamIdx);

		/**fix parameters*/
		void fixParameters(math::Matrixd& N, math::Matrixd& C, const std::vector<unsigned int>& fixedParamIdx);

		/**fix parameters*/
		void fixParameters(math::Matrixd& N, const std::vector<unsigned int>& fixedParamIdx);

		/**Make N_2dot matrix from N_dot elements list*/
		math::Matrixd make_N_2dot_New(const unsigned int sizeOfLower, const unsigned int sect) const;

		/**Make N_bar matrix */
		math::Matrixd make_N_bar(const unsigned int boundarySect) const;

	protected:
		unsigned int numSections;
		std::vector<math::Matrixd> Error;
		double sumEtpe;/**<posteriori variance*/
		unsigned int numUnknown;/**<number of all unknown parameters*/
		unsigned int numEq;/**<number of equations and constraints*/
		std::vector<unsigned int> numParameters;/**<number of parameters for each section*/
		std::vector<unsigned int> numSets;/**<number of input datasets for each section*/
		std::vector<NdotMat> NdotList; /// size is numSections.
		std::vector<std::vector<NbarMat>> NbarList; /// size is numSections X numSections, where diagonal parts and some of off-diagonal parts are empty
		std::vector<CdotMat> CdotList;
	};
}