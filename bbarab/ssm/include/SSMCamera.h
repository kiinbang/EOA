/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <SSM/include/SSMDataStructures.h>
#include <SSM/include/SSMDistortionModel.h>
#include <ssm/include/SSMParam.h>
#include <ssm/include/ssmUuid.h>

namespace sensor
{
	/// Camera
	/// Description: Definition of a Camera interface
	///
	class Camera
	{
	public:
		/**getRefinedPhotoCoord
		*@brief Get the refined(undistorted) photo coordinates from the distorted photo coordinates.
		*@param const data::Point2D& distortedPhotoCoord : distorted photo coordinates
		*@return data::Point2D : refined photo coordinates
		*/
		virtual data::Point2D getRefinedPhotoCoord(const data::Point2D& distortedPhotoCoord) const = 0;

		/**getRefinedPhotoCoord
		*@brief Get the refined(undistorted) photo coordinates from the distorted photo coordinates.
		*@param const std::vector<std::shared_ptr<param::Parameter>>& distortedPhotoCoord : distorted photo coordinates
		*@return data::Point2D : refined photo coordinates
		*/
		virtual data::Point2D getRefinedPhotoCoord(const std::vector<std::shared_ptr<param::Parameter>>& distortedPhotoCoord) const = 0;

		/**getPhoto2ImgCoord
		*@brief Get image coordinates from photo coordinates
		*@param const data::Point2D& photoCoord : photo coordinates
		*@return data::Point2D : image coordinates
		*/ 
		virtual data::Point2D getPhoto2ImgCoord(const data::Point2D& photoCoord) const = 0;

		/**getPhoto2ImgCoord
		*@brief Get image coordinates from photo coordinates
		*@param const std::vector<param::Parameter>& photoCoord
		*@return data::Point2D : image coordinates
		*/
		virtual data::Point2D getPhoto2ImgCoord(const std::vector<std::shared_ptr<param::Parameter>>& photoCoord) const = 0;

		/**getImg2PhotoCoord
		*@brief Get photo coordinates from image coordinates
		*@param const data::Point2D& imgCoord : image coordinates
		*@return data::Point2D : photo coordinates
		*/
		virtual data::Point2D getImg2PhotoCoord(const data::Point2D& imgCoord) const = 0;

		/**getRefinedPhotoCoordFromImgCoord
		*@brief Transfer image coordinates to refined(undistorted) photo coordinates.
		*@param const data::Point2D& distortedImgCoord : distorted image coordinates
		*@return data::Point2D : refined photo coordinates
		*/
		virtual data::Point2D getRefinedPhotoCoordFromImgCoord(const data::Point2D& distortedImgCoord) const = 0;

		/**getDistortedPhotoCoordFromRefinedPhotoCoord
		*@brief Get the distorted photo coordinates from the refined(undistorted) photo coordinates.
		*@param const data::Point2D& refinedPhotoCoord : refined image coordinates
		*@return data::Point2D : distorted photo coordinates
		*/
		virtual data::Point2D getDistortedPhotoCoordFromRefinedPhotoCoord(const data::Point2D& refinedPhotoCoord) const = 0;

		/**getDistortedImgCoordFromRefinedPhotoCoord
		*@brief Get the distorted image coordinates from the refined(undistorted) photo coordinates.
		*@param const std::vector<data::Point2D>& refinedPhotoCoord : refined photo coordinates
		*@return std::vector<data::Point2D> : distorted image coordinates
		*/
		virtual std::vector<data::Point2D> getDistortedImgCoordFromRefinedPhotoCoord(const std::vector<data::Point2D>& refinedPhotoCoord) const = 0;

		/**getDistortedImgCoordFromRefinedPhotoCoord
		*@brief Get the distorted image coordinates from the refined(undistorted) photo coordinates.
		*@param const data::Point2D& refinedPhotoCoord : refined photo coordinates
		*@return data::Point2D : distorted image coordinates
		*/
		virtual data::Point2D getDistortedImgCoordFromRefinedPhotoCoord(const data::Point2D& refinedPhotoCoord) const = 0;

		/**getFL
		*@brief Get a focal length
		*@return double : focal length
		*/
		virtual double getFL() const = 0;
		
		/**setFL
		*@brief Set a focal length
		*@param double : focal length
		*@return void
		*/
		virtual void setFL(const double focalLength) = 0;

		/**setFL
		*@brief Set a focal length
		*@param parameter : focal length
		*@return void
		*/
		virtual void setFL(const param::Parameter& focalLength) = 0;

		/**getPP
		*@brief Get a principal point(pp)
		*@return data::Point2D : principal point(pp)
		*/
		virtual data::Point2D getPP() const = 0;
		
		/**setPP
		*@brief Set a principal point(pp)
		*@param double : principal point(pp)
		*@return void
		*/
		virtual void setPP(const data::Point2D& principalPoint) = 0;

		/**setPP
		*@brief Set a principal point(pp)
		*@param parameter : principal point
		*@return void
		*/
		virtual void setPP(const std::vector<param::Parameter>& principalPoint) = 0;

		/**getSensorSize
		*@brief Get a sensor size (width and height)
		*@return math::VectorMat<unsigned int, 2> : sensor width and height
		*/
		virtual math::VectorMat<unsigned int, 2> getSensorSize()  const = 0;

		/**setSensorSize
		*@brief Set a sensor size (width and height)
		*@param const unsigned int width : width
		*@param const unsigned int height : height
		*@return void
		*/
		virtual void setSensorSize(const unsigned int width, const unsigned int height) = 0;

		/**setSensorSize
		*@brief Set a sensor size (width and height)
		*@param const Parameter : width
		*@param const Parameter : height
		*@return void
		*/
		virtual void setSensorSize(const param::Parameter width, const param::Parameter height) = 0;

		/**getPixPitch
		*@brief Get a pixel pitch (pixel interval size)
		*@return double : pixel pitch
		*/
		virtual double getPixPitch() const = 0;

		/**setPixPitch
		*@brief Set a pixel pitch (pixel interval size)
		*@param const double pixelPitch: pixel pitch
		*@return void
		*/
		virtual void setPixPitch(const double pixelPitch) = 0;

		/**setPixPitch
		*@brief Set a pixel pitch (pixel interval size)
		*@param const parameter pixelPitch: pixel pitch
		*@return void
		*/
		virtual void setPixPitch(const param::Parameter& pixelPitch) = 0;

		/**getLeverArm
		*@brief Get lever-arm (dx, dy, dz)
		*@return Point3D: lever-arm
		*/
		virtual data::Point3D getLeverArm() const = 0;

		/**setLeverArm
		*@brief Set lever-arm
		*@param Point3D: lever-arm (dx, dy, dz)
		*@return void
		*/
		virtual void setLeverArm(const data::Point3D& leverArm) = 0;

		/**setLeverArm
		*@brief Set lever-arm
		*@param const std::vector<param::ParameterPtr>& leverArm: dx, dy and dz
		*@return void
		*/
		virtual void setLeverArm(const std::vector<param::ParameterPtr>& leverArm) = 0;

		/**getBoresight
		*@brief Get boresight
		*@return EulerAngles (omega, phi, kappa)
		*/
		virtual data::EulerAngles getBoresight() const = 0;

		/**setBoresight
		*@brief Set boresight
		*@param EulerAngles (omega, phi, kappa)
		*@return void
		*/
		virtual void setBoresight(const data::EulerAngles& boresight) = 0;

		/**setBoresight
		*@brief Set boresight
		*@param const std::vector<param::ParameterPtr>& boresight: omega, phi and kappa
		*@return void
		*/
		virtual void setBoresight(const std::vector<param::ParameterPtr>& boresight) = 0;

		/**getDistortionModel
		*@brief Get a distortion model
		*@return sensor::DistortionModel : distortion model
		*/
		virtual std::shared_ptr<sensor::DistortionModel> getDistortionModel() const = 0;

		/**setDistortionModel
		*@brief Set a distortion model
		*@param const sensor::DistortionModel : distortion model
		*@return void
		*/
		virtual void setDistortionModel(const std::shared_ptr<sensor::DistortionModel> newDistModel) = 0;

		/**getParameters
		*@brief get all parameters (camera, alignment and distortion)
		*@return std::vector<std::shared_ptr<param::Parameter>>
		*/
		virtual std::vector<std::shared_ptr<param::Parameter>> getParameters() = 0;

		/**setParameterCollectionPtr
		*@brief set all parameters (camera, alignment and distortion)
		*@param std::vector<std::shared_ptr<param::Parameter>> : a pointer of new parameters
		*/
		virtual void setParameters(const std::vector<std::shared_ptr<param::Parameter>>& collection) = 0;

		/**getCamParameters
		*@brief get camera parameters
		*@return std::vector<std::shared_ptr<param::Parameter>>
		*/
		virtual std::vector<std::shared_ptr<param::Parameter>>& getCamParameters() = 0;

		/**getDistParameters
		*@brief get distortion parameters
		*@return std::vector<std::shared_ptr<param::Parameter>>
		*/
		virtual std::vector<std::shared_ptr<param::Parameter>>& getDistParameters() = 0;

		/**getId
		*@brief get a camera id
		*@return unsigned int id
		*/
		virtual unsigned int getId() const = 0;

		/**setId
		*@brief set a camera id
		*@param unsigned int id
		*/
		virtual void setId(unsigned int id) = 0;
	};

}
