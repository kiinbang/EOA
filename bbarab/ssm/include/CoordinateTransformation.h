/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/ReferenceEllipsoid.h>

namespace math
{
	template<typename T>
	inline bool almostSame(T a, T b)
	{
		if (fabs(a - b) < std::numeric_limits<T>::epsilon())
			return true;
		else
			return false;

	}

	namespace geo
	{
		const std::string _ECEF = "ECEF"; /// X Y Z in ECEF
		const std::string _ENU = "ENU"; /// East North Up in UTM
		const std::string _LLH_DEG = "LLH_DEG"; /// Lat Lon Height in degrees
		const std::string _LLH_RAD = "LLH_RAD"; /// Lat Lon Height in radians

		enum class COORDSYSTYPE {ECEF = 0/*geocentric x-y-z*/, ENU = 1/*E-N-Up*/, LLH = 2 /*lat-lon-h*/};

		/**
		* @brief  Get a central meridian
		*/
		double getCentralMeridian(const unsigned int noZone);

		/**
		* @brief  Get a UTM zone number
		*/
		unsigned int getZone(const double longitude);

		/// <summary>
		/// geodetic (lat-lon-h) to geocentric (x-y-z) coordinates
		/// </summary>
		/// <param name="lat">latitude</param>
		/// <param name="lon">longitude</param>
		/// <param name="h">ellipsoid height</param>
		/// <returns></returns>
		ECEF LLH2ECEF(const LLH& llh, const ReferenceEllipsoid& ellips);

		/// <summary>
		/// geocentric (x-y-z) coordinates to geodetic (lat-lon-h) 
		/// </summary>
		/// <param name="x">ECEF x</param>
		/// <param name="y">ECEF y</param>
		/// <param name="z">ECEF z</param>
		/// <returns></returns>
		LLH ECEF2LLH(const ECEF& ecef, const ReferenceEllipsoid& ellips);

		/// <summary>
		/// geodetic (lat-lon-H) to UTM Cartesian coordinates (E-N-Up & Zone#)
		/// </summary>
		/// <param name="ll0">lat and long</param>
		/// <returns>UTM East, North and zone number</returns>
		ENUZnHemi LLH2ENUZnHm(const LLH& llh0, const ReferenceEllipsoid& ellips);

		/// <summary>
		/// UTM (E-N-U) to lat-lon-h
		/// </summary>
		/// <param name="enuz0">E-N-U-Zone#</param>
		/// <param name="hemi">northern or sourthern hemisphere</param>
		/// <returns>lat-lon-h</returns>
		LLH ENUZnHm2LLH(const ENUZnHemi& enuznhm, const ReferenceEllipsoid& ellips);

		/**
		* @brief  ENU+Zone#+Hemisphere to ECEF
		*/
		ECEF ENUZnHm2ECEF(const ENUZnHemi& enuznhm, const ReferenceEllipsoid& ellips);

		/**
		* @brief  ECEF to ENU+Zone#+Hemisphere
		*/
		ENUZnHemi ECEF2ENUZnHm(const ECEF& ecef, const ReferenceEllipsoid& ellips);

		/**
		* @brief  general definition of a coordinate transformation function type
		*/
		typedef math::Arrayd(*transformCoord)(const math::Arrayd&, const ReferenceEllipsoid&);

		using PipeNode = std::pair<transformCoord, ReferenceEllipsoid>;

		/**
		* @brief  set a pipeline for sequential multiple coordinate transformation functions
		*/
		bool setPipeline(const std::string& inCoordSys, const ReferenceEllipsoid& inEllips, const std::string& outCoordSys, const ReferenceEllipsoid& outEllips, std::vector<PipeNode>& pipeline);

		/**
		* @brief  sequential coordinate transformation
		*/
		math::Arrayd sequentialTransform(const math::Arrayd& inCoord, const std::vector<PipeNode>& sequentialFuncs);
	}
}