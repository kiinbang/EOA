/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/Vertex.h>

namespace mesh
{
    ///  MeshData: structure for mesh data
    template<class T> struct MeshData
    {        
    public:
        typedef T CoordValueType;

        /// Must return the number of data points
        inline std::size_t kdtree_get_point_count() const { return vertices.size(); }

        /// Returns the distance between the vector "p1[0:size-1]" and the data point with index "idx_p2" stored in the class:
        inline T kdtree_distance(const T* p1, const size_t idx_p2, size_t size) const
        {
            T s = 0;
            for (size_t i = 0; i < size; ++i)
            {
                const T d = p1[i] - vertices[idx_p2][i];
                s += d * d;
            }
            return s;
        }

        /// Returns the dim'th component of the idx'th point in the class:
        inline T kdtree_get_pt(const size_t idx, int dim) const
        {
            return vertices[idx][dim];
        }

        /// Optional bounding-box computation: return false to default to a standard bbox computation loop.
        /// Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
        /// Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
        template <class BBOX> bool kdtree_get_bbox(BBOX& /*bb*/) const 
        {
            return false;
        }

    public:
        std::vector<Vertex3D> vertices;
        std::vector<std::vector<unsigned int>> faces;
        std::vector<Vertex3D> fNormals;
    };
}