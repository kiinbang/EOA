#pragma once

#include <limits>
#include <string>

#include <bitmap-master/bitmap_image.hpp>
#include <freeglut/include/GL/freeglut.h>
/// For the use of static lib of freeglut, define "FREEGLUT_STATIC"

#include "datastructs.hpp"

const bool saveEachPartInDebug = false;
const bool savePartsForEachImgInDebug = false;
const bool printObjCoordinatesInDebug = false;
const double FALSECOORD = -999.999;

namespace globj
{
	static double zNear = 0.1;
	static double zFar = 1000.0;

	/// window width and height
	static int win_width, win_height;
	/// shade flag
	static int shadeFlag = 1;
	/// vertex ids of a face
	static std::vector<tobj::FaceVtxIdx> fVtxIds;
	/// face normal vectors
	static std::vector<tobj::Vertex> faceNormals;
	///	vertex normal vectors
	static std::vector<tobj::Vertex> vtxNormalV;
	/// vertices
	static std::vector<tobj::Vertex> vertices;
	/// vertex colors
	static std::vector<globj::VC> vtxColors;
	static bool initColor = false;
	/// number of vertices of each file
	static std::vector<size_t> nVertices;
	///	normalized vertices
	static std::vector<tobj::Vertex> normalizedVtxs;

	/// Camera looks at
	static globj::Pos3D eyeXyz(0, 0, 1);
	static globj::Pos3D centerXyz(0, 0, 0);
	static globj::Pos3D upXyz(0, 1, 0);
	/// zoom factor
	static double zoomFactor = 1.0;
	/// field of view in degrees
	static double fov = 40.0;
	/// screen capture path
	static std::string outImgPath;
	/// shift, center of vertices
	static tobj::Vertex mdlCenter;
	/// translation
	static globj::Pos3D objTrans;
	/// quaternion
	static SSMATICS_EOA::CSMQuaternion objRot;
	/// preparation for computing object pt coordinates
	static bool prepareGetObjPt = false;

	inline void setNearFar(const double nearVal, const double farVal)
	{
		zNear = nearVal;
		zFar = farVal;
	}

	/// get denormalized vertex coordinates
	inline void getDenormalized(const double nx, const double ny, const double nz, double& x0, double& y0, double& z0)
	{
		x0 = nx + mdlCenter[0];
		y0 = ny + mdlCenter[1];
		z0 = nz + mdlCenter[2];
	}

	///	Convert quaternion to OpenGL matrix
	inline void convert(const SSMATICS_EOA::CSMQuaternion& q, double(&m)[16])
	{
		double l = q * q;
		double s = 2.0 / l;
		double xs = q.x * s;
		double ys = q.y * s;
		double zs = q.z * s;

		double wx = q.w * xs;
		double wy = q.w * ys;
		double wz = q.w * zs;

		double xx = q.x * xs;
		double xy = q.x * ys;
		double xz = q.x * zs;

		double yy = q.y * ys;
		double yz = q.y * zs;
		double zz = q.z * zs;

		m[0 * 4 + 0] = 1.0 - (yy + zz);
		m[1 * 4 + 0] = xy - wz;

		m[2 * 4 + 0] = xz + wy;
		m[0 * 4 + 1] = xy + wz;
		m[1 * 4 + 1] = 1.0 - (xx + zz);
		m[2 * 4 + 1] = yz - wx;

		m[0 * 4 + 2] = xz - wy;
		m[1 * 4 + 2] = yz + wx;
		m[2 * 4 + 2] = 1.0 - (xx + yy);


		m[0 * 4 + 3] = 0.0;

		m[1 * 4 + 3] = 0.0;
		m[2 * 4 + 3] = 0.0;
		m[3 * 4 + 0] = m[3 * 4 + 1] = m[3 * 4 + 2] = 0.0;
		m[3 * 4 + 3] = 1.0;
	};

	/// define view transform matrix
	/// transform from the eye coordinate system to the world system
	inline void setupEye(const double eyeX, const double eyeY, const double eyeZ,
		const double cX, const double cY, const double cZ,
		const double upX, const double upY, const double upZ)
	{
		eyeXyz[0] = eyeX;
		eyeXyz[1] = eyeY;
		eyeXyz[2] = eyeZ;

		centerXyz[0] = cX;
		centerXyz[1] = cY;
		centerXyz[2] = cZ;

		upXyz[0] = upX;
		upXyz[1] = upY;
		upXyz[2] = upZ;

		glLoadIdentity();
		gluLookAt(eyeXyz[0], eyeXyz[1], eyeXyz[2],
			centerXyz[0], centerXyz[1], centerXyz[2],
			upXyz[0], upXyz[1], upXyz[2]);
	}

	inline void setupEye(const globj::Pos3D& newEyeXyz, const globj::Pos3D& newCenterXyz, const globj::Pos3D& newUpXyz)
	{
		setupEye(newEyeXyz[0], newEyeXyz[1], newEyeXyz[2],
			newCenterXyz[0], newCenterXyz[1], newCenterXyz[2],
			newUpXyz[0], newUpXyz[1], newUpXyz[2]);
	}

	/// setup the object, transform from the world to the object coordinate system
	inline void setupObject()
	{
		double rot[16];
		glTranslated(objTrans[0], objTrans[1], objTrans[2]);
		convert(objRot, rot);
		glMultMatrixd((GLdouble*)rot);
	}

	inline void draw_object()
	{
		if (!initColor)
		{
			vtxColors.clear();
			vtxColors.resize(normalizedVtxs.size());
			for (auto& c : vtxColors)
			{
				c.r = c.g = c.b = 0.0f;
			}
		}

		for (size_t f = 0; f < faceNormals.size(); f++)
		{
			glBegin(GL_TRIANGLES);

			if (shadeFlag == 0)
			{
				glNormal3f(float(faceNormals[f][0]), float(faceNormals[f][1]), float(faceNormals[f][2]));
			}

			const tobj::FaceVtxIdx& vtxIds = fVtxIds[f];

			for (int v = 0; v < 3; v++)
			{
				const auto& vtxN = vtxNormalV[vtxIds[v]];

				if (shadeFlag == 1)
				{
					glNormal3f(GLfloat(vtxN[0]), GLfloat(vtxN[1]), GLfloat(vtxN[2]));
				}

				if (!initColor)
				{
					//glColor3f(235 / 255.0, 180 / 255.0, 173 / 255.0);
					float max = float(fabs(vtxN[0]) > fabs(vtxN[1]) ? fabs(vtxN[0]) : fabs(vtxN[1]));
					max = max > fabs(vtxN[2]) ? max : float(fabs(vtxN[2]));
					
					//max = float(fabs(vtxN[0])) + float(fabs(vtxN[1])) + float(fabs(vtxN[2]));

					vtxColors[vtxIds[v]].r = GLfloat(fabs(vtxN[0])) / max;
					vtxColors[vtxIds[v]].g = GLfloat(fabs(vtxN[1])) / max;
					vtxColors[vtxIds[v]].b = GLfloat(fabs(vtxN[2])) / max;
				}

				glColor3f(vtxColors[vtxIds[v]].r, vtxColors[vtxIds[v]].g, vtxColors[vtxIds[v]].b);

				const auto& p = normalizedVtxs[vtxIds[v]];
				glVertex3d(p[0], p[1], p[2]);
			}

			glEnd();
		}

		if (!initColor)
			initColor = true;
	}

	/// display call back function
	inline void display()
	{
		/* clear frame buffer */
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/* transform from the eye coordinate system to the world system */
		setupEye(eyeXyz, centerXyz, upXyz);
		glPushMatrix();

		/* transform from the world to the object coordinate system */
		setupObject();
		/* draw the mesh */
		draw_object();
		//for(const auto& obj : objInstances)
		//	draw_object(obj);

		glPopMatrix();
		glFlush();
		glutSwapBuffers();
	}

	/// Called when a "resize" event is received by the window.
	inline void reshape(int w, int h)
	{
		win_width = w;
		win_height = h;
		double ar = static_cast<double>(w) / static_cast<double>(h);
		/// set viewport
		glViewport(0, 0, w, h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		// magic imaging commands
		gluPerspective(fov, /* field of view in degrees */
			ar, /* aspect ratio */
			zNear, /* Z near */
			zFar /* Z far */);

		glMatrixMode(GL_MODELVIEW);

		glutPostRedisplay();
	}

	/// get normalized vertex coordinates
	inline void getNormalized(const double x0, const double y0, const double z0, double& nx, double& ny, double& nz)
	{
		nx = x0 - mdlCenter[0];
		ny = y0 - mdlCenter[1];
		nz = z0 - mdlCenter[2];
	}

	inline tobj::Vertex getNormalizedVtx(const tobj::Vertex& vtx)
	{
		tobj::Vertex nVtx;
		getNormalized(vtx[0], vtx[1], vtx[2], nVtx[0], nVtx[1], nVtx[2]);

		return nVtx;
	}

	/// get denormalized vertex coordinates
	inline tobj::Vertex getDenormalizedVtx(const tobj::Vertex& nVtx)
	{
		tobj::Vertex vtx;
		getDenormalized(nVtx[0], nVtx[1], nVtx[2], vtx[0], vtx[1], vtx[2]);

		return vtx;
	}

	inline void getObjCoord(const int col, const int row, double& X, double& Y, double& Z, double& depthDLB, bool& retval)
	{
		if (!prepareGetObjPt)
		{
			retval = false;
			return;
		}

		/// The codes below are from lidarviewer
		/// Found issue: it works correctly in lidarviewer, but it produces wrong obj-coords after rotating or shifting object space.
		/// Without rotate or shift, it looks fine.
		GLint viewport[4];
		GLdouble modelview[16];
		GLdouble projection[16];
		GLdouble winX, winY, winZ;
		GLdouble posX, posY, posZ;

		//Once we have the viewport information, we can grab the Modelview information. The Modelview Matrix determines how the vertices of OpenGL primitives are transformed to eye coordinates.
		glGetDoublev(GL_MODELVIEW_MATRIX, modelview);// Retrieve The Modelview Matrix
		//After that, we need to get the Projection Matrix. The Projection Matrix transforms vertices in eye coordinates to clip coordinates. 	
		glGetDoublev(GL_PROJECTION_MATRIX, projection);// Retrieve The Projection Matrix
		//We need to grab the current viewport. The information we need is the starting X and Y position of our GL viewport along with the viewport width and height.
		//Once we get this information using glGetIntegerv(GL_VIEWPORT, viewport), viewport will hold the following information: 
		glGetIntegerv(GL_VIEWPORT, viewport);// Retrieves The Viewport Values (X, Y, Width, Height)

		winX = (GLdouble)col;
		winY = (GLdouble)viewport[3] - GLdouble(1) - static_cast<GLdouble>(row);

		if (winX < 0 || 
			winX >= viewport[2] || 
			winY < 0 || 
			winY >= viewport[3])
		{
			retval = false;
			return;
		}

		/// Depth buffer at mouse position
		GLfloat depth;
		glReadPixels(int(winX), int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);

		depthDLB = double(depth);

		if (depthDLB <= 0.0 || depthDLB >= 1.0)
		{
			retval = false;
			return;
		}

		winZ = depthDLB; // GLdouble(depth);

		/// gluUnProject converts Windows screen coordinates to OpenGL coordinates.
		gluUnProject(winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

		getDenormalized(posX, posY, posZ, X, Y, Z);

		retval = true;

#ifdef _DEBUG
		if (printObjCoordinatesInDebug)
		{
			std::clog.setf(std::ios::fixed, std::ios::floatfield);
			std::clog.precision(6);
			std::clog << "glx, gly(Height - row), depth: \t" << winX << "\t" << winY << "\t" << depth << "\t";
			std::clog << "X, Y, Z: \t" << X << "\t" << Y << "\t" << Z << std::endl;
		}
#endif
	}

	/// save an render image
	inline void saveDepthImg(const std::string path)
	{
		//write 
		bitmap_image image2(win_width, win_height);
		double X, Y, Z;
		double depth;
		bool retval;

		for (int r = 0; r < win_height; ++r)
		{
			for (int c = 0; c < win_width; ++c)
			{
				getObjCoord(c, r, X, Y, Z, depth, retval);

				if (retval)
				{
					unsigned char pixval = unsigned char(depth * 255.f);
					image2.set_pixel(c, r, pixval, pixval, pixval);
				}
				else
				{
					image2.set_pixel(c, r, 255, 255, 255);
					continue;
				}
			}
		}

		if (image2.save_image(path))
			std::clog << "Succeed in saving: " << path << std::endl;
	}

	/// save gl screen
	inline void fillScreenImg(bitmap_image& image)
	{
		const auto H = image.height();
		const auto W = image.width();

		// Image Writing	
		unsigned int nCh = 3;
		uint8_t* imageData = new uint8_t[W * H * nCh];

		//glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glReadBuffer(GL_FRONT);
		//glReadBuffer(GL_BACK);
		glReadPixels(GLint(0), GLint(0), GLsizei(W), GLsizei(H), GL_RGB, GL_UNSIGNED_BYTE, imageData);

		int idx = 0;
		for (unsigned int r = 0; r < H; ++r)
		{
			for (unsigned int c = 0; c < W; ++c)
			{
				auto rIdx = idx++;
				auto gIdx = idx++;
				auto bIdx = idx++;

				//auto A = *(imageData[c++]);
				unsigned int red = static_cast<unsigned int>(imageData[rIdx]);
				unsigned int green = static_cast<unsigned int>(imageData[gIdx]);
				unsigned int blue = static_cast<unsigned int>(imageData[bIdx]);

				image.set_pixel(c, (H - 1) - r, red, green, blue);
			}
		}

		delete[] imageData;
	}

	/// save gl screen
	inline void saveScreen(const std::string& path)
	{
		//write 
		bitmap_image image(win_width, win_height);

		fillScreenImg(image);

		if (image.save_image(path))
			std::clog << "Succeed in saving: " << path << std::endl;
	}

	/// save gl screen
	inline void saveScreen(const std::string& path, const std::vector<int>& imgCol, const std::vector<int>& imgRow)
	{
		//write 
		bitmap_image image(win_width, win_height);

		fillScreenImg(image);

		const int H = image.height();
		const int W = image.width();

		for (unsigned int i = 0; i < imgCol.size(); ++i)
		{
			for (unsigned int j = 0; j < 5; ++j)
			{
				std::cout << std::to_string(i) << std::string(", ");
				std::cout << std::to_string(j) << std::endl;

				int c = imgCol[i] + j;
				int r = imgRow[i];
				if (c >= 0 && c < W && r >= 0 && r < H)
					image.set_pixel(c, r, 255, 0, 0);

				c = imgCol[i] - j;
				r = imgRow[i];
				if (c >= 0 && c < W && r >= 0 && r < H)
					image.set_pixel(c, r, 255, 0, 0);

				c = imgCol[i];
				r = imgRow[i] + j;
				if (c >= 0 && c < W && r >= 0 && r < H)
					image.set_pixel(c, r, 255, 0, 0);

				c = imgCol[i];
				r = imgRow[i] - j;
				if (c >= 0 && c < W && r >= 0 && r < H)
					image.set_pixel(c, r, 255, 0, 0);
			}
		}

		if (image.save_image(path))
			std::clog << "Succeed in saving: " << path << std::endl;
	}

	/// mouse click call back function
	inline void  mouseClick(int button, int state, const int x, const int y)
	{
		if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		{
#ifdef _DEBUG
			double X, Y, Z;
			double depth;
			bool retval;
			getObjCoord(x, y, X, Y, Z, depth, retval);
			std::clog.setf(std::ios::fixed, std::ios::floatfield);
			std::clog.precision(3);
			std::clog << "Mouse position: " << x << "\t" << y << "\t" << X << "\t" << Y << "\t" << Z << "\t" << depth << "\n";
#endif
#ifdef _DEBUG
			if (false)
			{
				saveDepthImg("mouseClick.bmp");
			}
#endif
		}
	}

	//Define the callback function.The second parameter gives the direction of the scroll.Values of + 1 is forward, -1 is backward.
	inline void mouseWheel(int button, int dir, int x, int y)
	{
		double scale = 50. / win_height;

#ifdef _DEBUG
		if (printObjCoordinatesInDebug)
		{
			std::clog << "eyeXyz[0] - centerXyz[0]: " << eyeXyz[0] - centerXyz[0] << std::endl;
		}
#endif

		auto dx = (eyeXyz[0] - centerXyz[0]);
		auto dy = (eyeXyz[1] - centerXyz[1]);
		auto dz = (eyeXyz[2] - centerXyz[2]);
		auto l = sqrt(dx * dx + dy * dy + dz * dz);

		if (dir > 0)
		{
			eyeXyz[0] += 1.0 / 5.0 * dx / l;
			eyeXyz[1] += 1.0 / 5.0 * dy / l;
			eyeXyz[2] += 1.0 / 5.0 * dz / l;
		}
		else
		{
			eyeXyz[0] -= 1.0 / 5.0 * dx / l;
			eyeXyz[1] -= 1.0 / 5.0 * dy / l;
			eyeXyz[2] -= 1.0 / 5.0 * dz / l;
		}


		glLoadIdentity();

		gluLookAt(eyeXyz[0], eyeXyz[1], eyeXyz[2],
			centerXyz[0], centerXyz[1], centerXyz[2],
			upXyz[0], upXyz[1], upXyz[2]);

		glutPostRedisplay();

#ifdef _DEBUG
		if (printObjCoordinatesInDebug)
		{
			std::clog << "eyeXyz[0]: " << eyeXyz[0] << std::endl;
			std::clog << "centerXyz[0]: " << centerXyz[0] << std::endl;
			std::clog << "eyeXyz[0] - centerXyz[0]: " << eyeXyz[0] - centerXyz[0] << std::endl;
			std::clog << "1.0 / 5.0 * dx / l: " << 1.0 / 5.0 * dx / l << std::endl;
		}
#endif
		return;
	}

	// keyboard call back function
	inline void keyBoard(unsigned char key, int x, int y)
	{
		switch (key)
		{
		case 'f':
			//Flat Shading
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			shadeFlag = 0;
			break;
		case 's':
			//Smooth Shading
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			shadeFlag = 1;
			break;
		case 'w':
			//Wireframe mode
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			break;
		case 'r':
			objRot = SSMATICS_EOA::CSMQuaternion(1, 0, 0, 0);
			objTrans = globj::CPoint(0, 0, 0);
			break;
		case 27:
			exit(0);
			break;
		case 'o':
			saveScreen(outImgPath);
			break;
		}

		glutPostRedisplay();
	}

	class ViewSim
	{
	public:

		ViewSim()
		{
			/// translation
			objTrans[0] = 0.0;
			objTrans[1] = 0.0;
			objTrans[2] = 0.0;
			/// quaternion (rotation)
			objRot.w = 1.0;
			objRot.x = 0.0;
			objRot.y = 0.0;
			objRot.z = 0.0;

			/// screen capture path
			outImgPath = "viewsimCapture.bmp";

			shadeFlag = 1;
		}

		void init(const int w, const int h, const std::string& title, bool outofscreen = false)
		{
			width = w;
			height = h;

			if (glutGet(GLUT_INIT_STATE) != 1)
			{
				/// initialize glut
				int argc = 0;
				char** argv = nullptr;
				glutInit(&argc, argv);
			}

			glutInitWindowSize(width, height);

			/// vpX, vpY: lower left corner
			/// vpW, vpH: viewport width and height
			int vpX = 0;
			int vpY = 0;
			int vpW = width + vpX;
			int vpH = height + vpY;
			glViewport(vpX, vpY, vpW, vpH);

			if (outofscreen)
			{
				RECT desktop;
				// Get a handle to the desktop window
				const HWND hDesktop = GetDesktopWindow();
				// Get the size of screen to the variable desktop
				GetWindowRect(hDesktop, &desktop);
				// The top left corner will have coordinates (0,0)
				// and the bottom right corner will have coordinates
				// (horizontal, vertical)
				auto horizontal = desktop.right;
				auto vertical = desktop.bottom;

				glutInitWindowPosition(horizontal, vertical);
			}

			glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
			glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH);

			/// Create window with given title
			if (glutGetWindow() < 1)
				glutCreateWindow(title.c_str());

			this->setupGLstate();

#ifdef _DEBUG
			if (printObjCoordinatesInDebug)
			{
				std::clog << "width: " << width << "\theight: " << height << std::endl;
				std::clog << "vpX: " << vpX << "\tvpY: " << vpY << std::endl;
				std::clog << "vpW: " << vpW << "\tvpH: " << vpH << std::endl;
			}
#endif				
		}

		bool load(const std::vector<std::string>& objPaths, const std::vector <std::string>& metaPaths)
		{
			/// clear all drawing data
			fVtxIds.clear();
			faceNormals.clear();
			vtxNormalV.clear();
			vertices.clear();
			nVertices.clear();
			normalizedVtxs.clear();

			initColor = false;

			size_t nFiles = objPaths.size();
			nVertices.resize(nFiles);

			for (size_t i = 0; i < nFiles; ++i)
			{
				nVertices[i] = 0;

				std::string name(objPaths[i]);

				size_t pos = name.find_last_of(".");
				std::string type = name.substr(pos + 1);

				if (type != "obj")
				{
					std::clog << "Not obj file." << std::endl;
					continue;
				}

				std::shared_ptr<tobj::ObjLoad> obj = tobj::createObjLoad(tobj::_TObjLoad_);

				obj = tobj::createObjLoad(tobj::_TObjLoad_);
				tobj::Meta meta = tobj::readModelConfigWithBOM(metaPaths[i].c_str());
				obj->load(name.c_str(), meta);

				/// previous size of vertices
				auto oldSize = vertices.size();
				/// extend vertex vector
				vertices.reserve(oldSize + obj->numVertices());
				/// new added vertices
				std::vector<tobj::Vertex> newVertices(obj->numVertices());

				for (size_t v = 0; v < obj->numVertices(); ++v)
				{
					newVertices[v] = obj->getVertex(v);
				}

				/// save the number of vertices for each file
				nVertices[i] = obj->numVertices();
				/// insert new vertices
				vertices.insert(vertices.end(), newVertices.begin(), newVertices.end());

				/// compute face normal vectors
				compute_normal(obj, i);
			}

			/// compute normalization parameters: center and scale(length)
			computeNormalization();

			/// normalized vertices
			normalizedVtxs.resize(vertices.size());
			for (size_t v = 0; v < vertices.size(); ++v)
			{
				normalizedVtxs[v] = getNormalizedVtx(vertices[v]);
			}

			return true;
		}

		void showObj()
		{
#ifdef _DEBUG
			if (printObjCoordinatesInDebug)
			{
				std::clog << "eyeXyz: " << eyeXyz[0] << "\t" << eyeXyz[1] << "\t" << eyeXyz[2] << std::endl;
				std::clog << "centerXyz: " << centerXyz[0] << "\t" << centerXyz[1] << "\t" << centerXyz[2] << std::endl;
				std::clog << "upXyz: " << upXyz[0] << "\t" << upXyz[1] << "\t" << upXyz[2] << std::endl;
			}
#endif
			///Set-up callback functions
			glutDisplayFunc(display);
			glutReshapeFunc(reshape);
			glutMouseFunc(mouseClick);
			glutMouseWheelFunc(mouseWheel);
			glutKeyboardFunc(keyBoard);

			glutMainLoop();
		}

		void saveRender(const std::string& outPath)
		{
#ifdef _DEBUG
			if (printObjCoordinatesInDebug)
			{
				std::clog << "eyeXyz: " << eyeXyz[0] << "\t" << eyeXyz[1] << "\t" << eyeXyz[2] << std::endl;
				std::clog << "centerXyz: " << centerXyz[0] << "\t" << centerXyz[1] << "\t" << centerXyz[2] << std::endl;
				std::clog << "upXyz: " << upXyz[0] << "\t" << upXyz[1] << "\t" << upXyz[2] << std::endl;
			}
#endif
			glViewport(0, 0, width, height);

			reshape(width, height);
			display();

			saveScreen(outPath);
		}

		void prepareComputingObjPts()
		{
			glViewport(0, 0, width, height);

			reshape(width, height);
			display();

			prepareGetObjPt = true;

#ifdef _DEBUG
			if (false)
			{
				saveDepthImg("prepareComputingObjPts.bmp");
			}
#endif
		}

		bool computeObjCoord(const int imgCol, const int imgRow, double& Xa, double& Ya, double& Za)
		{
			try
			{
				double depth;
				bool retval;
				getObjCoord(imgCol, imgRow, Xa, Ya, Za, depth, retval);

				if (retval)
					return true;
				else
				{
					Xa = Ya = Za = FALSECOORD;
					return false;
				}
			}
			catch (...)
			{
				throw std::runtime_error("Error getObjCoords");
			}
		}

		std::vector<bool> getObjCoords(const std::vector<int>& imgCol,
			const std::vector<int>& imgRow,
			std::vector<double>& Xa,
			std::vector<double>& Ya,
			std::vector<double>& Za)
		{
#ifdef _DEBUG
			if (printObjCoordinatesInDebug)
			{
				std::clog << "eyeXyz: " << eyeXyz[0] << "\t" << eyeXyz[1] << "\t" << eyeXyz[2] << std::endl;
				std::clog << "centerXyz: " << centerXyz[0] << "\t" << centerXyz[1] << "\t" << centerXyz[2] << std::endl;
				std::clog << "upXyz: " << upXyz[0] << "\t" << upXyz[1] << "\t" << upXyz[2] << std::endl;
			}
#endif
			std::vector<bool> ret(imgCol.size());

			try
			{
				Xa.clear(); Xa.resize(imgCol.size());
				Ya.clear(); Ya.resize(imgCol.size());
				Za.clear(); Za.resize(imgCol.size());
				ret.clear(); ret.resize(imgCol.size());

				for (size_t i = 0; i < imgCol.size(); ++i)
				{
					ret[i] = computeObjCoord(imgCol[i], imgRow[i], Xa[i], Ya[i], Za[i]);
				}
			}
			catch (...)
			{
				throw std::runtime_error("Error getObjCoords");
			}

#ifdef _DEBUG
			if (saveEachPartInDebug)
			{
				saveScreen("getObjCoords.bmp", imgCol, imgRow);

				for (const auto& b : ret)
				{
					if (b)
					{
						saveScreen("getObjCoordsTrue.bmp", imgCol, imgRow);
						break;
					}
				}
			}
#endif

			return ret;
		}

		void setFov(const float newFov)
		{
			fov = newFov;
#ifdef _DEBUG
			std::clog << "new fov: " << fov << std::endl;
#endif
		}

		size_t getNumVertices() const
		{
			return vertices.size();
		}

		const tobj::Vertex& getVtx(const size_t idx) const
		{
			return vertices[idx];
		}

		size_t getNumFaces() const
		{
			return fVtxIds.size();
		}

		const tobj::FaceVtxIdx& getFVtxId(const size_t idx) const
		{
			return fVtxIds[idx];
		}

		const tobj::Vertex& getFNormal(const size_t idx) const
		{
			return faceNormals[idx];
		}

		double minX() { return this->min[0]; }
		double minY() { return this->min[1]; }
		double minZ() { return this->min[2]; }

		double maxX() { return this->max[0]; }
		double maxY() { return this->max[1]; }
		double maxZ() { return this->max[2]; }

		double centerX() { return mdlCenter[0]; }
		double centerY() { return mdlCenter[1]; }
		double centerZ() { return mdlCenter[2]; }

		double sizeX() { return this->size[0]; }
		double sizeY() { return this->size[1]; }
		double sizeZ() { return this->size[2]; }

	private:

		/// compute normalization parameters
		void computeNormalization()
		{
			double c[3];
			c[0] = c[1] = c[2] = 0.0;

			for (int j = 0; j < 3; ++j)
			{
				max[j] = (std::numeric_limits<double>::lowest)();
				min[j] = (std::numeric_limits<double>::max)();
			}

			for (size_t v = 0; v < vertices.size(); ++v)
			{
				for (int j = 0; j < 3; ++j)
				{
					c[j] += vertices[v][j];

					max[j] = (max[j] < vertices[v][j]) ? static_cast<double>(vertices[v][j]) : max[j];
					min[j] = (min[j] > vertices[v][j]) ? static_cast<double>(vertices[v][j]) : min[j];
				}
			}

			size[0] = max[0] - min[0];
			size[1] = max[1] - min[1];
			size[2] = max[2] - min[2];

			mdlCenter[0] = min[0] + size[0] * 0.5;
			mdlCenter[1] = min[1] + size[1] * 0.5;
			mdlCenter[2] = min[2] + size[2] * 0.5;

			//for (int j = 0; j < 3; ++j)
			//	mdlCenter[j] = static_cast<float>(c[j] / vertices.size());
		}

		/// compute face normal vectors
		void compute_normal(const std::shared_ptr<tobj::ObjLoad>& obj, const size_t fileIdx)
		{
			size_t idxOffset = 0;
			for (int i = 0; i < fileIdx; ++i)
			{
				idxOffset += nVertices[i];
			}

			/// new face normal vectors
			std::vector<tobj::Vertex> newFaceNormals;
			newFaceNormals.reserve(obj->numFaces());
			/// new vertex normal vectors
			std::vector<tobj::Vertex> newVtxNormalV(obj->numVertices());
			/// initialize new vertex normals
			for (auto& n : newVtxNormalV)
			{
				n[0] = n[1] = n[2] = 0.0;
			}

			tobj::Vertex fVtx[3];
			std::vector<tobj::FaceVtxIdx> newFVtxIds;
			newFVtxIds.reserve(obj->numFaces());

			for (size_t f = 0; f < obj->numFaces(); ++f)
			{
				auto vtxIds = obj->getVtxIdx(f);

				for (int v = 0; v < 3; ++v)
					fVtx[v] = obj->getVertex(vtxIds[v]);

				tobj::Vertex p01, p02;
				p01 = fVtx[1] - fVtx[0];
				p02 = fVtx[2] - fVtx[0];

				/// cross product
				auto crossP = p01 ^ p02;

				/// area
				auto faceAreas = sqrt(crossP[0] * crossP[0] + crossP[1] * crossP[1] + crossP[2] * crossP[2]);

				if (faceAreas < std::numeric_limits<double>::epsilon())
				{
					continue;
				}

				/// face normal
				newFaceNormals.push_back(crossP * (1.0 / faceAreas));

				/// vertex normal vectors
				tobj::FaceVtxIdx fvtxid;
				for (int v = 0; v < 3; ++v)
				{
					newVtxNormalV[vtxIds[v]] = newVtxNormalV[vtxIds[v]] + crossP;
					/// save vertex indices of a face after adding an offset (previous number of vertices)
					fvtxid[v] = int(vtxIds[v] + idxOffset);
				}

				newFVtxIds.push_back(fvtxid);
			}

			/// previous number of faces
			auto nPreFace = faceNormals.size();
			/// extend vectors
			faceNormals.reserve(nPreFace + newFaceNormals.size());
			/// insert new face normals
			faceNormals.insert(faceNormals.end(), newFaceNormals.begin(), newFaceNormals.end());

			/// normalize new vertex normals
			for (auto& n : newVtxNormalV)
			{
				auto size = sqrt(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
				if (size < std::numeric_limits<double>::epsilon())
					continue;
				n = n * (1.0 / size);
			}

			/// previous number of vertex normals
			auto nPreVNormals = vtxNormalV.size();
			/// extend
			vtxNormalV.reserve(nPreVNormals + newVtxNormalV.size());
			/// insert new vertex normals
			vtxNormalV.insert(vtxNormalV.end(), newVtxNormalV.begin(), newVtxNormalV.end());

			/// previous number of face vertex Ids
			auto nPreFVtxIdx = fVtxIds.size();
			/// extend
			fVtxIds.reserve(fVtxIds.size() + newFVtxIds.size());
			/// insert new face vtx indices
			fVtxIds.insert(fVtxIds.end(), newFVtxIds.begin(), newFVtxIds.end());
		}

		/// setup GL states
		void setupGLstate()
		{
			GLfloat lightOneColor[] = { 1, 1, 1, 1 };
			GLfloat globalAmb[] = { .1f, .1f, .1f, 1.f };
			GLfloat light1Position[] = { 0,  0, 1, 0 };

			glEnable(GL_CULL_FACE);
			glFrontFace(GL_CCW);
			//glFrontFace(GL_CW);
			glEnable(GL_DEPTH_TEST);
			//glClearColor(0, 0, 0, 1); /// black bg
			glClearColor(1.0, 1.0, 1.0, 1.0); /// white bg
			glShadeModel(GL_SMOOTH);

			glEnable(GL_LIGHT1);
			glEnable(GL_LIGHT2);
			//glEnable(GL_LIGHT3);
			//glEnable(GL_LIGHT4);
			glEnable(GL_LIGHTING);
			glEnable(GL_NORMALIZE);
			glEnable(GL_COLOR_MATERIAL);

			glLightfv(GL_LIGHT1, GL_DIFFUSE, lightOneColor);
			glLightfv(GL_LIGHT2, GL_DIFFUSE, lightOneColor);
			//glLightfv(GL_LIGHT3, GL_DIFFUSE, lightOneColor);
			//glLightfv(GL_LIGHT4, GL_DIFFUSE, lightOneColor);
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmb);
			glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

			//glColorMaterial(GL_BACK, GL_AMBIENT_AND_DIFFUSE);

			glLightfv(GL_LIGHT1, GL_POSITION, light1Position);
		}

	private:
		/// min, max, and size of vertex coordinates
		double max[3];
		double min[3];
		double size[3];
		int width, height;
	};
}