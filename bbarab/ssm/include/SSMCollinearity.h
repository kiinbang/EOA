/*
* Copyright(c) 1999-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/SSMDataStructures.h>
#include <SSM/include/SSMFrameCamera.h>
#include <SSM/include/SSMRotation.h>
#include <ssm/include/SSMSMACModel.h>

#define getMeasuredPhotoCoordinates getDistortedPhotoCoordinates
#define getMeasuredImageCoordinates getDistortedImageCoordinates
#define getRefinedPhotoCoordinates getUndistortedPhotoCoordinates

namespace ssm
{
	class Collinearity
	{
	public:
		/**Collinearity equations for getting refined photo coordinates using 6 eop, camera, and object point*/
		static data::Point2D getUndistortedPhotoCoordinates(const double omega, const double phi, const double kappa,
			const data::Point3D& pos,
			const std::shared_ptr<sensor::Camera> cam,
			const data::Point3D& objPt);

		/**Collinearity equations for getting refined photo coordinates using M-rotation matrix, perspective center, camera, and object point*/
		static data::Point2D getUndistortedPhotoCoordinates(const data::RotationMatrix& matM,
			const data::Point3D& pos,
			const std::shared_ptr<sensor::Camera> cam,
			const data::Point3D& objPt);

		/**Collinearity equations for getting measured(distorted) photo coordinates using 6 eop, camera, and object point*/
		static data::Point2D getDistortedPhotoCoordinates(const double omega,
			const double phi, const double kappa,
			const data::Point3D& pos,
			const std::shared_ptr<sensor::Camera> cam,
			const data::Point3D& objPt);

		/**Collinearity equations for getting measured(distorted) photo coordinates using M-rotation matrix, perspective center, camera, and object point*/
		static data::Point2D getDistortedPhotoCoordinates(const data::RotationMatrix& matM,
			const data::Point3D& pos,
			const std::shared_ptr<sensor::Camera> cam,
			const data::Point3D& objPt);

		/**Collinearity equations for getting measured(distorted) image coordinates using 6 eop, camera, and object point*/
		static data::Point2D getDistortedImageCoordinates(const double omega, const double phi, const double kappa,
			const data::Point3D& pos,
			const std::shared_ptr<sensor::Camera> cam,
			const data::Point3D& objPt, bool& availability,
			const unsigned int widthMargin = 0,
			unsigned int heightMargin = 0,
			const double targetGSD = 0.0);

		/**Collinearity equations for getting measured(distorted) image coordinates using M-rotation matrix, perspective center, camera, and object point*/
		static data::Point2D getDistortedImageCoordinates(const data::RotationMatrix& matM,
			const data::Point3D& pos,
			const std::shared_ptr<sensor::Camera> cam,
			const data::Point3D& objPt,
			bool& availability,
			const unsigned int widthMargin = 0,
			const unsigned int heightMargin = 0,
			const double targetGSD = 0.0);

		/**Collinearity equations for getting measured(distorted) image coordinates using M-rotation matrix, perspective center, camera, and object point*/
		static void getDistortedImageCoordinates(const std::vector<std::shared_ptr<param::Parameter>>& params, std::vector<double>& retVals);

		/**Check scale and direction for availability*/
		static bool checkAvailability(const double omega, const double phi, const double kappa, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point2D& photoCoord, const data::Point3D& obj, const double targetGSD);

		/**Check scale and direction for availability*/
		static bool checkAvailability(const data::RotationMatrix& rMat, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point2D& photoCoord, const data::Point3D& obj, const double targetGSD);
	};

	/** Collinearity equations without alignment
	*@brief get these:
	* x - xp + dx + f(r/q) = -Vx
	* y - yp + dy + f(s/q) = -Vy
	*@param[in] givenParams: given parameters
	*@param[in] givenObs: given observations (2D image points)
	*@return matrix of a residuals
	*/
	math::Matrixd collinearityEqWithoutAlignment(const std::vector<std::shared_ptr<param::Parameter>>& givenParams, const std::vector<std::shared_ptr<param::Parameter>>& givenObs);

	/** Collinearity equations with alignment
	*@brief get these:
	* x - xp + dx + f(r/q) = -Vx
	* y - yp + dy + f(s/q) = -Vy
	*@param[in] givenParams: given parameters
	*@param[in] givenObs: given observations (2D image points)
	*@return matrix of a residuals
	*/
	math::Matrixd collinearityEqWithAlignment(const std::vector<std::shared_ptr<param::Parameter>>& givenParams, const std::vector<std::shared_ptr<param::Parameter>>& givenObs);

	/** Collinearity equations to get refined photo coordinate from object point coordinates with alignment
	*@brief get these:
	* x = -f(r/q)
	* y = -f(s/q)
	*@param[in] f: focal length
	*@param[in] rotBsM: boresight matrix (M rotation matrix)
	*@param[in] leverarm: lever-arm
	*@param[in] pc: perspective center
	*@param[in] rotBodyM: bodyframe rotation matrix (M rotation matrix)
	*@param[in] objP: 3D object point
	*@return matrix (2D refiend photo coordinates)
	*/
	data::Point2D collinearityEqWithAlignment(const double f,
		const data::RotationMatrix& rotBsM,
		const data::Point3D& leverarm,
		const data::Point3D pc,
		const data::RotationMatrix& rotBodyM,
		const data::Point3D& objP);

	/** Collinearity equations to get refined photo coordinate from object point coordinates with alignment
	*@brief get these:
	* x = -f(r/q)
	* y = -f(s/q)
	*@param[in] f: focal length
	*@param[in] rotBsM: boresight matrix (M rotation matrix)
	*@param[in] leverarm: lever-arm
	*@param[in] pc: perspective center
	*@param[in] rotBodyM: bodyframe rotation matrix (M rotation matrix)
	*@param[in] objP: 3D object points
	*@return vector<Point2D> (2D refiend photo coordinates)
	*/
	std::vector<data::Point2D> collinearityEqWithAlignment(const double f,
		const data::RotationMatrix& rotBsM,
		const data::Point3D& leverarm,
		const data::Point3D pc,
		const data::RotationMatrix& rotBodyM,
		const std::vector<data::Point3D>& objP);

	/** get distorted photo coordinates from refined photo coordinates using camera info
	*@param[in] cam: camera interface
	*@param[in] phoPts: refined photo points coordinates
	*@return vector<Point2D> (2D refiend photo points coordinates)
	*/
	std::vector<data::Point2D> photo2DistortedPhoto(const std::shared_ptr<sensor::Camera>& cam, const std::vector<data::Point2D>& phoPts);

	/** get distorted image coordinates(col, row) from refined photo coordinates using camera info
	*@param[in] cam: camera interface
	*@param[in] phoPts: refined photo points coordinates
	*@return vector<Point2D> (2D refiend photo points coordinates)
	*/
	std::vector<data::Point2D> photo2DistortedImg(const std::shared_ptr<sensor::Camera>& cam, const std::vector<data::Point2D>& phoPts);

	/** Partial derivatives of collinearity equations for 6 eop (x, y, z, o, p, and k)
	*@brief get these:
	* x - xp + dx + f(r/q) = -Vx
	* y - yp + dy + f(s/q) = -Vy
	*@param[in] givenParams: given parameters
	*@param[in] givenObs: given observations (2D image points)
	*@return matrix of partial derivatives
	*/
	math::Matrixd collinearityPDForEop(const std::vector<std::shared_ptr<param::Parameter>>& givenParams, const std::vector<std::shared_ptr<param::Parameter>>& givenObs);
}