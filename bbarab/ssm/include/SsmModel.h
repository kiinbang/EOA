#pragma once

#include <ssm/include/SsmPhoto.h>

namespace data
{

	class ModelData
	{
	public:
		ModelData(const std::shared_ptr<PhotoData> pho0,
		const std::shared_ptr<PhotoData> pho1,
		const std::vector<TiePoint>& ties)
			: photo0(pho0),
			photo1(pho1),
			tiePts(ties)
		{
		}

		void findTiePts()
		{
			tiePts.clear();

			std::vector<ImagePoint>& ptData0 = photo0->getPts();
			std::vector<ImagePoint>& ptData1 = photo1->getPts();

			for(unsigned int i=0; i<ptData0.size(); ++i)
			{
				for(unsigned int j=0; j<ptData1.size(); ++j)
				{
					if (ptData0[i].ptUuid == ptData1[j].ptUuid)
					{
						TiePoint tie(ptData0[i], ptData1[j]);
						tiePts.push_back(tie);
					}
				}
			}
		}

		std::shared_ptr<PhotoData> getPhoto0() { return photo0; }
		std::shared_ptr<PhotoData> getPhoto1() { return photo1; }
		std::vector<TiePoint>& getTiePts() { return tiePts; }

	private:
		std::shared_ptr<PhotoData> photo0;
		std::shared_ptr<PhotoData> photo1;
		std::vector<TiePoint> tiePts;
	};
}