#pragma once

#include <fstream>
#include <iostream>
#include <string>

#include <boost/algorithm/string/replace.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace bpt = boost::property_tree;

#include <ssm/include/utilitygrocery.h>

namespace data
{
	namespace model
	{
		const wchar_t* _tx = L"Softgraphy.GIX.SceneNode.Transform.tx";
		const wchar_t* _ty = L"Softgraphy.GIX.SceneNode.Transform.ty";
		const wchar_t* _tz = L"Softgraphy.GIX.SceneNode.Transform.tz";
		const wchar_t* _rx = L"Softgraphy.GIX.SceneNode.Transform.rx";
		const wchar_t* _ry = L"Softgraphy.GIX.SceneNode.Transform.ry";
		const wchar_t* _rz = L"Softgraphy.GIX.SceneNode.Transform.rz";
		const wchar_t* _sx = L"Softgraphy.GIX.SceneNode.Transform.sx";
		const wchar_t* _sy = L"Softgraphy.GIX.SceneNode.Transform.sy";
		const wchar_t* _sz = L"Softgraphy.GIX.SceneNode.Transform.sz";

		struct cfg
		{
			bool readModelConfig(const std::wstring& xmlPath)
			{
#ifdef _DEBUG
				//std::wcout << L"Model config file:\t" << xmlPath << std::endl;
#endif

				try
				{
					std::wifstream infile(xmlPath);

					if (!infile)
					{
						std::wcout << L"Error in reading a modelmeta.gxxml" << std::endl;
						return false;
					}

					/// set locale
					//xmlPath.imbue(std::locale("kor"));
					setlocale(LC_ALL, "");
					infile.imbue(std::locale(setlocale(LC_ALL, NULL)));
					
					/// Read a project config file (xml)
					bpt::wptree reader;
					bpt::read_xml(infile, reader);

					BOOST_FOREACH(const bpt::wptree::value_type & v, reader.get_child(L"Softgraphy.GIX.SceneNode.Transform"))
					{
						BOOST_FOREACH(const bpt::wptree::value_type & var, v.second.get_child(L""))
						{
							if (var.first == L"tx") tx = var.second.get_value<double>();
							else if (var.first == L"ty") ty = var.second.get_value<double>();
							else if (var.first == L"tz") tz = var.second.get_value<double>();

							else if (var.first == L"rx") rx = var.second.get_value<double>();
							else if (var.first == L"ry") ry = var.second.get_value<double>();
							else if (var.first == L"rz") rz = var.second.get_value<double>();
							
							else if (var.first == L"sx") sx = var.second.get_value<double>();
							else if (var.first == L"sy") sy = var.second.get_value<double>();
							else if (var.first == L"sz") sz = var.second.get_value<double>();
						}
					}
				}
				catch (...)
				{
					std::wcout << L"Error in reading a modelmeta.gxxml" << std::endl;
					return false;
				}

				return true;
			}

			///skip BOM
			bool readModelConfigWithBOM(const std::wstring& xmlPath)
			{
#ifdef _DEBUG
				//std::wcout << L"Model config file:\t" << xmlPath << std::endl;
#endif

				try
				{
					boost::property_tree::ptree pt;
					std::wifstream file(xmlPath, std::ios::in);
					if (file.is_open())
					{
						///skip BOM
						wchar_t buffer[8];
						buffer[0] = 255;
						while (file.good() && buffer[0] > 127)
							file.read((wchar_t*)buffer, 1);

						std::fpos_t pos = file.tellg();
						if (pos > 0)
							file.seekg(pos - 1);

						///parse rest stream
						/// set locale
						//xmlPath.imbue(std::locale("kor"));
						setlocale(LC_ALL, "");
						file.imbue(std::locale(setlocale(LC_ALL, NULL)));

						/// Read a project config file (xml)
						bpt::wptree reader;
						bpt::read_xml(file, reader);

						BOOST_FOREACH(const bpt::wptree::value_type & v, reader.get_child(L"Softgraphy.GIX.SceneNode.Transform"))
						{
							BOOST_FOREACH(const bpt::wptree::value_type & var, v.second.get_child(L""))
							{
								if (var.first == L"tx") tx = var.second.get_value<double>();
								else if (var.first == L"ty") ty = var.second.get_value<double>();
								else if (var.first == L"tz") tz = var.second.get_value<double>();

								else if (var.first == L"rx") rx = var.second.get_value<double>();
								else if (var.first == L"ry") ry = var.second.get_value<double>();
								else if (var.first == L"rz") rz = var.second.get_value<double>();

								else if (var.first == L"sx") sx = var.second.get_value<double>();
								else if (var.first == L"sy") sy = var.second.get_value<double>();
								else if (var.first == L"sz") sz = var.second.get_value<double>();
							}
						}

						file.close();
					}
					else
					{
						std::wcout << L"Error in opening a meta.gxxml" << std::endl;
						return false;
					}
				}
				catch (...)
				{
					std::wcout << L"Error in reading a meta.gxxml" << std::endl;
					return false;
				}

				return true;
			}

			double tx, ty, tz;
			double rx, ry, rz;
			double sx, sy, sz;
		};
	}
}