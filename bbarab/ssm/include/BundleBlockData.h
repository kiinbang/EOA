#pragma once

#include <string>

#include <glog/logging.h>

#include "BundleBlockDataReader.hpp"
#include <ssm/include/SSMConstants.h>
#include <ssm/include/SSMDataStructures.h>
#include <ssm/include/SsmPhoto.h>
#include <ssm/include/utilitygrocery.h>

namespace data
{
	struct XmlData
	{
		Project prjxml;
		Pho phoxml;
		Obj objxml;
		Camera camxml;
	};

	class BundleBlockPrj
	{
	public:
		bool readData(const std::wstring& prjxmlPath)
		{
			/// Data stored in xml files
			XmlData xmlData;

			LOG(INFO) << "Read " << util::uni2multi(prjxmlPath).c_str();
			if (!xmlData.prjxml.readProject(prjxmlPath))
			{
				LOG(INFO) << "Error in reading " << prjxmlPath.c_str();
				return false;
			}

			LOG(INFO) << "Read " << util::uni2multi(xmlData.prjxml.project.dataPath.phoPath).c_str();
			if (!xmlData.phoxml.readPho(xmlData.prjxml.project.dataPath.phoPath))
			{
				LOG(INFO) << "Error in reading a photo file: " << xmlData.prjxml.project.dataPath.phoPath.c_str();
				return false;
			}
			LOG(INFO) << "Read " << util::uni2multi(xmlData.prjxml.project.dataPath.objPath).c_str();
			if (!xmlData.objxml.readObj(xmlData.prjxml.project.dataPath.objPath))
			{
				LOG(INFO) << "Error in reading an object point file: " << xmlData.prjxml.project.dataPath.objPath.c_str();
				return false;
			}
			LOG(INFO) << "Read " << util::uni2multi(xmlData.prjxml.project.dataPath.camPath).c_str();
			if (!xmlData.camxml.readCamera(xmlData.prjxml.project.dataPath.camPath))
			{
				LOG(INFO) << "Error in reading a camera file: " << xmlData.prjxml.project.dataPath.camPath.c_str();
				return false;
			}

			/// Photo folder path
			photoDataFilePath = xmlData.prjxml.project.dataPath.phoPath;

			/// Check input data
			LOG(INFO) << "Data integrity check";
			if (!checkData(xmlData))
				return false;
			/// Prepare bundle data
			LOG(INFO) << "Prepare bundle data";
			setBundleData(xmlData);

			LOG(INFO) << "Done: BundleBlockData::readData";

			return true;
		}

		void setBundleData(const data::Project::ProjectData& prjData,
			const std::vector<data::Camera::CameraData>& cameras,
			const std::vector<data::Pho::Image>& images,
			const data::Obj& objPtData)
		{
			XmlData xmlData;
			xmlData.prjxml.project = prjData;
			xmlData.camxml.cameras = cameras;
			xmlData.phoxml.images = images;
			xmlData.objxml = objPtData;

			setBundleData(xmlData);
		}

		std::wstring getPhotoDataPath() { return photoDataFilePath; }
		double getMaxSigma() const { return maxSigma; }
		double getMinSigma() const { return minSigma; }
		double getMinCorrelation() const { return minCorrelation; }
		double getSigmaThreshold() const { return sigmaThreshold; }
		unsigned int getMaxIteration() const { return maxIteration; }
		std::string getProjectName() const { return projectName; }
		std::string getProjectType() const { return projectType; }
		std::vector<std::shared_ptr<sensor::Camera>>& getCamera() { return cameras; }
		std::vector<std::shared_ptr<data::PhotoData>>& getPhotos() { return photos; }
		std::vector< std::shared_ptr<data::ObjectPointParam>>& getObjectPoints() { return objectPoints; }

	private:
		void setBundleData(XmlData& xmlData)
		{
			/// Project configuration
			this->maxSigma = xmlData.prjxml.project.config.maxSigma;
			this->minSigma = xmlData.prjxml.project.config.minSigma;
			this->minCorrelation = xmlData.prjxml.project.config.minCorrelation;
			this->maxIteration = xmlData.prjxml.project.iteration.maxIteration;
			this->sigmaThreshold = xmlData.prjxml.project.iteration.sigma_threshold;
			this->projectName = util::uni2multi(xmlData.prjxml.project.name);
			this->projectType = util::uni2multi(xmlData.prjxml.project.type);

			/// Cameras
			this->cameras.clear();
			for (const auto& cam : xmlData.camxml.cameras)
			{
				if (data::_frame == cam.type)
				{
					std::shared_ptr<sensor::FrameCamera> fcam = std::make_shared<sensor::FrameCamera>();

					/// Id
					xmlData.camxml.setId(cam, *fcam);
					/// Focal length
					xmlData.camxml.setf(cam, *fcam);
					/// xp yp
					xmlData.camxml.setxpyp(cam, *fcam);
					/// width and height
					xmlData.camxml.setWidthHeight(cam, *fcam, xmlData.prjxml.project.config.minSigma);
					/// Pixel pitch			
					xmlData.camxml.setPixPitch(cam, *fcam, xmlData.prjxml.project.config.minSigma);
					/// Alignment: boresight and lever-arm
					xmlData.camxml.setAlignment(cam, *fcam);
					/// Distortion
					xmlData.camxml.setDistortion(cam, *fcam, xmlData.prjxml.project.config.minSigma);

					this->cameras.push_back(fcam);
				}
				else
				{
					std::wcout << L"Wrong sensor type name: " << cam.type << std::endl;
				}
			}

			/// Photos
			this->photos.clear();
			for (const auto& pho : xmlData.phoxml.images)
			{
				data::Camera::CameraData cam;
				for (unsigned int i = 0; i < xmlData.camxml.cameras.size(); ++i)
				{
					if (xmlData.camxml.cameras[i].id == pho.cameraId)
						cam = xmlData.camxml.cameras[i];
				}

				/// Set eop parameters
				std::shared_ptr<data::EOP> eopPtr = std::make_shared<data::EOP>();
				setEop(pho, eopPtr);

				/// Set image points
				std::shared_ptr<data::ImagePointData> imgPoints;
				setImgPoints(pho, imgPoints, cam);

				std::shared_ptr<data::PhotoData> photoPtr = std::make_shared<data::PhotoData>(
					this->getCameraPtr(pho.cameraId),
					eopPtr,
					imgPoints,
					pho.imageId);

				this->photos.push_back(photoPtr);
			}

			/// Object points
			this->objectPoints.clear();
			for (const auto& objPt : xmlData.objxml.objPts)
			{
				std::shared_ptr<data::ObjectPointParam> pt(new data::ObjectPointParam);
				pt->id = objPt.pointId;
				pt->operator()(0) = objPt.xyz[0];
				pt->operator()(1) = objPt.xyz[1];
				pt->operator()(2) = objPt.xyz[2];
				pt->coord[0]->setInitVal(objPt.xyz[0]);
				pt->coord[1]->setInitVal(objPt.xyz[1]);
				pt->coord[2]->setInitVal(objPt.xyz[2]);
				pt->coord[0]->setStDev(sqrt(objPt.variance[0]));
				pt->coord[1]->setStDev(sqrt(objPt.variance[4]));
				pt->coord[2]->setStDev(sqrt(objPt.variance[8]));
				this->objectPoints.push_back(pt);
			}
		}

		bool checkData(XmlData& xmlData)
		{
			if (xmlData.phoxml.images.size() < 2)
			{
				std::cout << std::endl << std::endl << "Number of images is less than 2" << std::endl << std::endl;
				//return false;
			}

			/// 1. Erase duplicated id (in cameras)
			std::cout << std::endl << std::endl << "Erase cameras having same Id: " << std::endl << std::endl;
			eraseDuplicatedIds(xmlData.camxml.cameras);

			/// 2. Check camera ids of image data
			std::cout << std::endl << std::endl << "Erase images having wrong camera Id: " << std::endl << std::endl;
			checkCameraIds(xmlData);

			/// 3. Erase duplicated id (in image points)
			std::cout << std::endl << std::endl << "Erase image points having same Id: " << std::endl << std::endl;
			for (unsigned int i = 0; i < xmlData.phoxml.images.size(); ++i)
			{
				std::cout << "\t" << "Image Id::" << xmlData.phoxml.images[i].imageId << std::endl << std::endl;
				eraseDuplicatedIds(xmlData.phoxml.images[i].imgPts);

				if (xmlData.phoxml.images[i].imgPts.size() < 3)
				{
					std::cout << std::endl << std::endl << "Number of image points is less than 3 in " << xmlData.phoxml.images[i].imageId << " image." << std::endl << std::endl;
					return false;
				}
			}

			/// 4. Erase duplicated id (in object points)
			std::cout << std::endl << std::endl << "Erase object points having same Id: " << std::endl << std::endl;
			eraseDuplicatedIds(xmlData.objxml.objPts);

			if (xmlData.objxml.objPts.size() < 3)
			{
				std::cout << std::endl << std::endl << "Number of available object points is less than 3." << std::endl << std::endl;
				return false;
			}

			/// 5. Check lack of observations (tie points observed in a single image)
			std::cout << std::endl << std::endl << "Erase unavailable image and object points points: " << std::endl << std::endl;
			checkMultipleCapturedTiePoints(xmlData);

			return true;
		}

		void checkCameraIds(XmlData& xmlData)
		{
			for (unsigned int i = 0; i < xmlData.phoxml.images.size(); ++i)
			{
				bool found = false;
				for (const auto& cam : xmlData.camxml.cameras)
				{
					if (cam.id == xmlData.phoxml.images[i].cameraId)
					{
						found = true;
						break;
					}
				}

				if (!found)
				{
					std::cout << "\t\tDELETED Image: " << xmlData.phoxml.images[i].imageId << "Not found camera id " << xmlData.phoxml.images[i].cameraId << std::endl;
					xmlData.phoxml.images.erase(xmlData.phoxml.images.begin() + i, xmlData.phoxml.images.begin() + i + 1);
				}
			}
		}

		void eraseDuplicatedIds(std::vector<Pho::Image::ImgPt>& imagePoints)
		{
			/// Find duplicated points
			std::vector<unsigned int> collectedIds;

			for (unsigned int i = 0; i < imagePoints.size(); ++i)
			{
				for (unsigned int j = i + 1; j < imagePoints.size(); ++j)
				{
					if (imagePoints[i].pointId == imagePoints[j].pointId)
					{
						collectedIds.push_back(imagePoints[j].pointId);
					}
				}
			}

			std::cout << "\t\t" << collectedIds.size() << " duplicated IMAGE POINT ids were found." << std::endl << std::endl;

			/// Delete duplicated points
			for (unsigned int i = 0; i < collectedIds.size(); ++i)
			{
				for (unsigned int j = 0; j < imagePoints.size(); ++j)
				{
					if (imagePoints[j].pointId == collectedIds[i])
					{
						std::cout << "\t\tDELETED Id: " << imagePoints[j].pointId << std::endl;
						imagePoints.erase(imagePoints.begin() + j, imagePoints.begin() + j + 1);
					}
				}
			}
		}

		void eraseDuplicatedIds(Obj::ObjectPoints& objectPoints)
		{
			/// Find duplicated points
			std::vector<unsigned int> collectedIds;

			for (unsigned int i = 0; i < objectPoints.size(); ++i)
			{
				for (unsigned int j = i + 1; j < objectPoints.size(); ++j)
				{
					if (objectPoints[i].pointId == objectPoints[j].pointId)
					{
						collectedIds.push_back(objectPoints[j].pointId);
					}
				}
			}

			std::cout << "\t\t" << collectedIds.size() << " duplicated OBJECT POINT ids were found." << std::endl;

			/// Delete duplicated points
			for (unsigned int i = 0; i < collectedIds.size(); ++i)
			{
				for (unsigned int j = 0; j < objectPoints.size(); ++j)
				{
					if (objectPoints[j].pointId == collectedIds[i])
					{
						std::cout << "\t\tDELETED Id: " << objectPoints[j].pointId << std::endl;
						objectPoints.erase(objectPoints.begin() + j, objectPoints.begin() + j + 1);
					}
				}
			}
		}

		void eraseDuplicatedIds(std::vector<Camera::CameraData>& cameras)
		{
			/// Find duplicated points
			std::vector<unsigned int> collectedIds;

			for (unsigned int i = 0; i < cameras.size(); ++i)
			{
				for (unsigned int j = i + 1; j < cameras.size(); ++j)
				{
					if (cameras[i].id == cameras[j].id)
					{
						collectedIds.push_back(cameras[j].id);
					}
				}
			}

			std::cout << "\t\t" << collectedIds.size() << " duplicated CAMERA ids were found." << std::endl;

			/// Delete duplicated points
			for (unsigned int i = 0; i < collectedIds.size(); ++i)
			{
				for (unsigned int j = 0; j < cameras.size(); ++j)
				{
					if (cameras[j].id == collectedIds[i])
					{
						std::cout << "\t\tDELETED ID: " << collectedIds[i] << std::endl;
						cameras.erase(cameras.begin() + j, cameras.begin() + j + 1);
					}
				}
			}
		}

		void checkMultipleCapturedTiePoints(XmlData& xmlData)
		{
			/// Find duplicated points
			std::vector<unsigned int> collectedIds;

			unsigned int i = 0;
			unsigned int availableCount = 0;

			for (const auto& objPt : xmlData.objxml.objPts)
			{
				unsigned int obs_count = 0;

				for (unsigned int j = 0; j < xmlData.phoxml.images.size(); ++j)
				{
					if (findImgPoint(xmlData.phoxml.images[j].imgPts, objPt.pointId))
						obs_count++;

					if (obs_count >= 2)
						break;
				}

				if (obs_count >= 2)
				{
					++availableCount;
					std::cout << "\t\t" << objPt.pointId << " is available: observed in multiple images." << std::endl;
				}
				else if (1 <= obs_count
					/// not full unknowns (stocastic or fixed)
					&& sqrt(objPt.variance[0]) <= xmlData.prjxml.project.config.minSigma
					&& sqrt(objPt.variance[1]) <= xmlData.prjxml.project.config.minSigma
					&& sqrt(objPt.variance[2]) <= xmlData.prjxml.project.config.minSigma)
				{
					++availableCount;
					std::cout << "\t\t" << objPt.pointId << " is available: fixed control point." << std::endl;
				}
				else
				{
					collectedIds.push_back(objPt.pointId);
				}
			}

			std::cout << availableCount << " object point Ids are available." << std::endl;
			std::cout << collectedIds.size() << " unavailable object point ids were found." << std::endl;

			/// Erase unavailable points
			for (const auto id : collectedIds)
			{
				std::cout << "\t" << "ID:\t" << id << std::endl;

				/// For object points
				for (unsigned int i = 0; i < xmlData.objxml.objPts.size(); ++i)
				{
					if (xmlData.objxml.objPts[i].pointId == id)
					{
						std::cout << "\t\tDELETED image point Id: " << xmlData.objxml.objPts[i].pointId << std::endl;
						xmlData.objxml.objPts.erase(xmlData.objxml.objPts.begin() + i, xmlData.objxml.objPts.begin() + i + 1);
					}
				}

				/// For image points
				for (auto& img : xmlData.phoxml.images)
				{
					for (unsigned int i = 0; i < img.imgPts.size(); ++i)
					{
						if (img.imgPts[i].pointId == id)
						{
							std::cout << "\t\tDELETED object poin Id: " << img.imgPts[i].pointId << std::endl;
							img.imgPts.erase(img.imgPts.begin() + i, img.imgPts.begin() + i + 1);
						}
					}
				}
			}
		}

		bool findImgPoint(const std::vector<Pho::Image::ImgPt>& imagePoints, const unsigned int ID)
		{
			for (const auto& pt : imagePoints)
			{
				if (ID == pt.pointId)
					return true;
			}

			return false;
		}

		void setEop(const data::Pho::Image& pho, std::shared_ptr<data::EOP> eopPtr)
		{
			std::vector<param::ParameterPtr> eopParams(data::NUM_EOP);
			for (auto& p : eopParams)
				p = std::make_shared<param::Parameter>();

			auto opk_0 = xyzOpkUnitConvert(pho.opk[0], pho.opkUnit);
			auto opk_1 = xyzOpkUnitConvert(pho.opk[1], pho.opkUnit);
			auto opk_2 = xyzOpkUnitConvert(pho.opk[2], pho.opkUnit);
			auto opkSigma_0 = xyzOpkUnitConvert(sqrt(pho.opkVariance[0]), pho.opkUnit);
			auto opkSigma_4 = xyzOpkUnitConvert(sqrt(pho.opkVariance[4]), pho.opkUnit);
			auto opkSigma_8 = xyzOpkUnitConvert(sqrt(pho.opkVariance[8]), pho.opkUnit);

			eopParams[data::oIdx]->set(opk_0);
			eopParams[data::oIdx]->setDelta(0.000001);
			eopParams[data::oIdx]->setDescription("Eop omega");
			eopParams[data::oIdx]->setStDev(opkSigma_0);
			eopParams[data::oIdx]->setInitVal(opk_0);
			eopParams[data::oIdx]->setName(constant::photo::_omega_);

			eopParams[data::pIdx]->set(opk_1);
			eopParams[data::pIdx]->setDelta(0.000001);
			eopParams[data::pIdx]->setDescription("Eop phi");
			eopParams[data::pIdx]->setStDev(opkSigma_4);
			eopParams[data::pIdx]->setInitVal(opk_1);
			eopParams[data::pIdx]->setName(constant::photo::_phi_);

			eopParams[data::kIdx]->set(opk_2);
			eopParams[data::kIdx]->setDelta(0.000001);
			eopParams[data::kIdx]->setDescription("Eop kappa");
			eopParams[data::kIdx]->setStDev(opkSigma_8);
			eopParams[data::kIdx]->setInitVal(opk_2);
			eopParams[data::kIdx]->setName(constant::photo::_kappa_);

			auto xyz_0 = xyzOpkUnitConvert(pho.xyz[0], pho.xyzUnit);
			auto xyz_1 = xyzOpkUnitConvert(pho.xyz[1], pho.xyzUnit);
			auto xyz_2 = xyzOpkUnitConvert(pho.xyz[2], pho.xyzUnit);
			auto xyzSigma_0 = xyzOpkUnitConvert(sqrt(pho.xyzVariance[0]), pho.xyzUnit);
			auto xyzSigma_4 = xyzOpkUnitConvert(sqrt(pho.xyzVariance[4]), pho.xyzUnit);
			auto xyzSigma_8 = xyzOpkUnitConvert(sqrt(pho.xyzVariance[8]), pho.xyzUnit);

			eopParams[data::xIdx]->set(xyz_0);
			eopParams[data::xIdx]->setDelta(0.000001);
			eopParams[data::xIdx]->setDescription("Eop X0");
			eopParams[data::xIdx]->setStDev(xyzSigma_0);
			eopParams[data::xIdx]->setInitVal(xyz_0);
			eopParams[data::xIdx]->setName(constant::photo::_X0_);

			eopParams[data::yIdx]->set(xyz_1);
			eopParams[data::yIdx]->setDelta(0.000001);
			eopParams[data::yIdx]->setDescription("Eop Y0");
			eopParams[data::yIdx]->setStDev(xyzSigma_4);
			eopParams[data::yIdx]->setInitVal(xyz_1);
			eopParams[data::yIdx]->setName(constant::photo::_Y0_);

			eopParams[data::zIdx]->set(xyz_2);
			eopParams[data::zIdx]->setDelta(0.000001);
			eopParams[data::zIdx]->setDescription("Eop Z0");
			eopParams[data::zIdx]->setStDev(xyzSigma_8);
			eopParams[data::zIdx]->setInitVal(xyz_2);
			eopParams[data::zIdx]->setName(constant::photo::_Z0_);

			eopPtr->setParameters(eopParams);
		}

		void setImgPoints(const data::Pho::Image& pho, std::shared_ptr<data::ImagePointData>& imgPoints, const data::Camera::CameraData& cam)
		{
			const double pixPitch = cam.pixelPitch;

			imgPoints = std::make_shared<data::ImagePointData>();

			imgPoints->imgId = pho.imageId;

			for (auto& xmlpt : pho.imgPts)
			{
				//*
				auto coordinates_0 = unitConvert(xmlpt.coordinates[0], pho.pointsUnit, pixPitch);
				auto coordinates_1 = unitConvert(xmlpt.coordinates[1], pho.pointsUnit, pixPitch);
				auto sd_0 = unitConvert(sqrt(xmlpt.dispersion[0]), pho.pointsUnit, pixPitch);
				auto sd_3 = unitConvert(sqrt(xmlpt.dispersion[3]), pho.pointsUnit, pixPitch);

				if (data::_imgCoordinates == pho.coordinateType)
				{
					/// swap coordinates (row-col to x-y)
					auto temp = sd_0;
					sd_0 = sd_3;
					sd_3 = sd_0;

					auto row = coordinates_0;
					auto col = coordinates_1;

					coordinates_0 = col - cam.width * pixPitch * 0.5;
					coordinates_1 = cam.height * pixPitch * 0.5 - row;
				}
				else if (data::_phoCoordinates == pho.coordinateType) {}
				else { throw std::runtime_error("Wrong coordinate type of principal point"); }
				//*/

				/*
				auto coordinates_0 = xmlpt.coordinates[0];
				auto coordinates_1 = xmlpt.coordinates[1];
				auto sd_0 = sqrt(xmlpt.dispersion[0]);
				auto sd_3 = sqrt(xmlpt.dispersion[3]);

				if (data::_imgCoordinates == pho.coordinateType)
				{
					auto temp = coordinates_0;
					coordinates_0 = coordinates_1;
					coordinates_1 = temp;
					temp = sd_0;
					sd_0 = sd_3;
					sd_3 = sd_0;
				}
				*/

				data::ImagePointParam imgPt;
				imgPt.setObjPtIndex(xmlpt.pointId);
				imgPt(0) = coordinates_0;
				imgPt(1) = coordinates_1;
				imgPt.coord[0]->setInitVal(coordinates_0);
				imgPt.coord[1]->setInitVal(coordinates_1);
				imgPt.coord[0]->setStDev(sd_0);
				imgPt.coord[1]->setStDev(sd_3);
				imgPoints->pts.push_back(imgPt);
			}
		}

		std::shared_ptr<sensor::Camera> getCameraPtr(const unsigned int camId)
		{
			for (auto& cam : cameras)
			{
				if (camId == cam->getId())
					return cam;
			}
			return nullptr;
		}

	private:
		/// Project
		double maxSigma;
		double minSigma;
		double minCorrelation;
		unsigned int maxIteration;
		double sigmaThreshold;
		std::string projectName;
		std::string projectType;
		/// Camera
		std::vector<std::shared_ptr<sensor::Camera>> cameras;
		/// Photos
		std::vector<std::shared_ptr<data::PhotoData>> photos;
		/// Object points
		std::vector< std::shared_ptr<data::ObjectPointParam>> objectPoints;
		/// Photo data file path
		std::wstring photoDataFilePath;
	};
}