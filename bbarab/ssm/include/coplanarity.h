#pragma once

#include <memory>

#include <ssm/include/SsmFrameCamera.h>
#include <ssm/include/SsmPhoto.h>
#include <ssm/include/SSMRotation.h>
#include <ssm/include/SSMSMACModel.h>

namespace ssm
{
	/** getDeterminant
	*@brief get determinant
	*@param[in] givenObs given observations (2D image points)
	*@return matrix of a determinant
	*/
	math::Matrixd getDeterminantSingleCam(const std::vector<std::shared_ptr<param::ParameterCollection>>& givenParams, const std::vector<std::shared_ptr<param::ParameterCollection>>& givenObs)
	{
		if (givenObs.size() < 1)
			throw std::runtime_error("Wrong number of observation collections");

		/// Camera and distortion model
		sensor::FrameCamera cam;
		cam.setParameters(givenParams[0]);
		cam.setDistortionModel(std::static_pointer_cast<sensor::DistortionModel>(std::make_shared<sensor::SMACModel>(givenParams[1])));

		/// Eop
		data::EOP eop0, eop1;
		eop0.setParameterCltPtr(givenParams[4]);
		eop1.setParameterCltPtr(givenParams[5]);

		auto baseVec = eop1.getPC() - eop0.getPC();

		/// R-rotation matrix
		auto ori0 = eop0.getOri();
		auto ori1 = eop1.getOri();
		const auto rotR0 = math::Rotation::getRMat(ori0(0), ori0(1), ori0(2));
		const auto rotR1 = math::Rotation::getRMat(ori1(0), ori1(1), ori1(2));

		math::Matrixd det(givenObs.size(), 1);

		for (unsigned int i = 0; i < givenObs.size(); ++i)
		{
			/// Given observed image coordinates
			data::Point2D imgPt0, imgPt1;

#ifdef _DEBUG
			if (givenObs[i]->size() != 2)
				throw std::runtime_error("Wrong number of observation parameter sets");

			if (givenObs[i]->operator[](0).size() != 2)
				throw std::runtime_error("It is not 2D-coordinate point data.");

			if (givenObs[i]->operator[](1).size() != 2)
				throw std::runtime_error("It is not 2D-coordinate point data.");
#endif
			imgPt0(0) = givenObs[i]->operator[](0)[0].get();
			imgPt0(1) = givenObs[i]->operator[](0)[1].get();

			imgPt1(0) = givenObs[i]->operator[](1)[0].get();
			imgPt1(1) = givenObs[i]->operator[](1)[1].get();

			/// Refined photo coordinates
			auto refinedPt0 = cam.getRefinedPhotoCoordFromImgCoord(imgPt0);
			auto refinedPt1 = cam.getRefinedPhotoCoordFromImgCoord(imgPt1);

			data::Point3D vec0;
			vec0(0) = refinedPt0(0);
			vec0(1) = refinedPt0(1);
			vec0(2) = cam.getFL();

			data::Point3D vec1;
			vec1(0) = refinedPt1(0);
			vec1(1) = refinedPt1(1);
			vec1(2) = cam.getFL();

			auto rVec0 = rotR0 % vec0;
			auto rVec1 = rotR1 % vec1;

			det(i, 0) =
				- baseVec(0) * (rVec0(1) * rVec1(2) - rVec0(2) * rVec1(1))
				- baseVec(1) * (rVec0(2) * rVec1(0) - rVec0(0) * rVec1(2))
				- baseVec(2) * (rVec0(0) * rVec1(1) - rVec0(1) * rVec1(0));
		}

		return det;
	}

	/** getDeterminant
	*@brief get determinant
	*@param[in] givenObs given observations (2D image points)
	*@return matrix of a determinant
	*/
	math::Matrixd getDeterminantStereoCam(const std::vector<std::shared_ptr<param::ParameterCollection>>& givenParams, const std::vector<std::shared_ptr<param::ParameterCollection>>& givenObs)
	{
		if (givenObs.size() < 1)
			throw std::runtime_error("Wrong number of observation collections");

		/// Camera and distortion model
		sensor::FrameCamera cam0, cam1;
		cam0.setParameters(givenParams[0]);
		cam0.setDistortionModel(std::static_pointer_cast<sensor::DistortionModel>(std::make_shared<sensor::SMACModel>(givenParams[1])));
		cam1.setParameters(givenParams[2]);
		cam1.setDistortionModel(std::static_pointer_cast<sensor::DistortionModel>(std::make_shared<sensor::SMACModel>(givenParams[3])));

		/// Eop
		data::EOP eop0, eop1;
		eop0.setParameterCltPtr(givenParams[4]);
		eop1.setParameterCltPtr(givenParams[5]);

		auto baseVec = eop1.getPC() - eop0.getPC();

		/// R-rotation matrix
		auto ori0 = eop0.getOri();
		auto ori1 = eop1.getOri();
		const auto rotR0 = math::Rotation::getRMat(ori0(0), ori0(1), ori0(2));
		const auto rotR1 = math::Rotation::getRMat(ori1(0), ori1(1), ori1(2));

		math::Matrixd det(givenObs.size(), 1);

		for (unsigned int i = 0; i < givenObs.size(); ++i)
		{
			/// Given observed image coordinates
			data::Point2D imgPt0, imgPt1;

#ifdef _DEBUG
			if (givenObs[i]->size() != 2)
				throw std::runtime_error("Wrong number of observation parameter sets");

			if (givenObs[i]->operator[](0).size() != 2)
				throw std::runtime_error("It is not 2D-coordinate point data.");

			if (givenObs[i]->operator[](1).size() != 2)
				throw std::runtime_error("It is not 2D-coordinate point data.");
#endif
			imgPt0(0) = givenObs[i]->operator[](0)[0].get();
			imgPt0(1) = givenObs[i]->operator[](0)[1].get();

			imgPt1(0) = givenObs[i]->operator[](1)[0].get();
			imgPt1(1) = givenObs[i]->operator[](1)[1].get();

			/// Refined photo coordinates
			auto refinedPt0 = cam0.getRefinedPhotoCoordFromImgCoord(imgPt0);
			auto refinedPt1 = cam1.getRefinedPhotoCoordFromImgCoord(imgPt1);

			data::Point3D vec0;
			vec0(0) = refinedPt0(0);
			vec0(1) = refinedPt0(1);
			vec0(2) = cam0.getFL();

			data::Point3D vec1;
			vec1(0) = refinedPt1(0);
			vec1(1) = refinedPt1(1);
			vec1(2) = cam1.getFL();

			auto rVec0 = rotR0 % vec0;
			auto rVec1 = rotR1 % vec1;

			det(i, 0) =
				-baseVec(0) * (rVec0(1) * rVec1(2) - rVec0(2) * rVec1(1))
				- baseVec(1) * (rVec0(2) * rVec1(0) - rVec0(0) * rVec1(2))
				- baseVec(2) * (rVec0(0) * rVec1(1) - rVec0(1) * rVec1(0));
		}

		return det;
	}
}