/*
* Copyright(c) 2001-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <SSM/include/SSMMatrixd.h>

namespace math
{
	/**
	*@class FixedSizeMat Class
	*@brief Class for supporting a fixed-size matrix
	*/
	template<size_t r, size_t c>
	class FixedSizeMatd : public Matrixd
	{
	public:
		FixedSizeMatd(const double val = double(0))
			: Matrixd(r, c, val)
		{
		}

		FixedSizeMatd(const FixedSizeMatd<r, c>& mat)
			: Matrixd(r, c, 0.0)
		{
			for (unsigned int i = 0; i < getSize(); ++i)
				this->data[i] = mat.data[i];
		}

		FixedSizeMatd(const Matrixd& mat)
			: Matrix(r, c, 0.0)
		{
			if (r != mat.getRows() || c != mat.getCols())
				throw std::runtime_error("Wrong size error from FixedSizeMat constructor");

			for (unsigned int i = 0; i < getSize(); ++i)
				this->data[i] = mat.data[i];
		}

		void operator = (const FixedSizeMatd<r, c>& mat)
		{
			for (unsigned int i = 0; i < getSize(); ++i)
				this->data[i] = mat.data[i];
		}

		void operator = (const Matrixd& mat)
		{
			if (r != mat.getRows() || c != mat.getCols())
				throw std::runtime_error("Wrong size error from FixedSizeMat constructor");

			for (unsigned int i = 0; i < size; ++i)
				this->data[i] = mat.data[i];
		}
	};
}