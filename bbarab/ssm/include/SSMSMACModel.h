/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <SSM/include/SSMDistortionModel.h>
#include <SSM/include/SSMParam.h>

namespace sensor
{
	const unsigned int numSmacParams = 7;

	/// SMAC lens distortion model
	class SMACModel : public DistortionModel
	{
	public:
		SMACModel();

		SMACModel(const double(&k0Tok3)[4], const double(&p1Top3)[3]);

		SMACModel(const std::vector<std::shared_ptr<param::Parameter>>& parameters);

		SMACModel(const SMACModel& copy);

		void operator = (const SMACModel& copy);

		std::string getModelName() override;

		std::vector<std::shared_ptr<param::Parameter>>& getParameters() override;

		void setParameters(const std::vector<std::shared_ptr<param::Parameter>>& collection) override;

		data::Point2D getDistortion(const data::Point2D& measuredPhotoCoord) override;

		data::Point2D getDistortion(const data::Point2D& measuredPhotoCoord, const data::Point2D& pp) override;

	private:
		//**Copy parameter values (not references)*/
		void copyParameterValues(const std::vector<std::shared_ptr<param::Parameter>>& collection);

	private:
		std::vector<std::shared_ptr<param::Parameter>> smacPtr;
	};
}