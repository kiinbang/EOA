/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>

namespace param
{
	//class __declspec (dllexport) Parameter
	class Parameter
	{
	public:
		/// Constructor
		Parameter();

		/// Copy constructor
		Parameter(const Parameter& copy);

		/// Assignment operator
		void operator = (const Parameter& copy);

		Parameter operator - (const Parameter& copy) const;

		double& get();
		double get() const;
		void set(const double newVal);

		double getInitVal() const;
		void setInitVal(const double newVal0);

		double getDelta() const;
		void setDelta(const double newDelta);

		double getStDev() const;
		void setStDev(const double newStDev);

		std::string getName() const;
		void setName(const std::string& newName);

		std::string getDescription() const;
		void setDescription(const std::string& newDescription);

	private:
		/// Copy member variables
		void getCopy(const Parameter& copy);

	private:
		/// Current value of the parameter
		double val;
		/// Initial value
		double val0;
		/// A predefined small difference value for a numeric partial differential equation
		double delta;
		/// Current standard deviation
		double stDev;
		/// Parameter's name
		std::string name;
		/// Description of the parameter
		std::string description;
	};

	using ParameterPtr = std::shared_ptr<Parameter>;

	/// Search parameter with a parameter's name
	Parameter getParameter(const std::vector<Parameter>& parameters, const std::string& name);

	/// Search parameter with a parameter's name
	std::shared_ptr<Parameter> getParameter(std::vector<std::shared_ptr<Parameter>>& parameters, const std::string& name);

	/// Search parameter with a parameter's name
	bool findParameter(const std::vector<Parameter>& parameters, const std::string& name, param::Parameter& found);

	/// Search parameter with a parameter's name
	std::shared_ptr<param::Parameter> findParameter(const std::vector< std::shared_ptr<Parameter>>& parameters, const std::string& name, bool& found);

	/// Search parameter with a parameter's name
	bool findParameter(const std::vector< std::shared_ptr<Parameter>>& parameters, const std::string& name, std::shared_ptr<param::Parameter>& found);

	/// Compare parameters
	bool compareParameters(std::vector<std::shared_ptr<param::Parameter>>& a, std::vector<std::shared_ptr<param::Parameter>>& b);

	/// Get a parameter value using a parameter name
	double get(const std::vector<std::shared_ptr<Parameter>>& parameters, const std::string& name);

	/// set a new parameter value using a parameter name
	void set(const std::vector<std::shared_ptr<Parameter>>& parameters, const std::string& name, const double value);

	/// Find an index of parameter using a parameter name
	int findIndex(const std::vector<std::shared_ptr<Parameter>>& parameters, const std::string& name);
}