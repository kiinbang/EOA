/*
* Copyright(c) 2005-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#define GOOGLE_GLOG_DLL_DECL
#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <glog/logging.h>

#define scui(num) static_cast<unsigned int>(num)

#include <stdio.h>

#include <ssm/include/BundleBlockData.h>
#include <ssm/include/NumericPDE.h>
#include <ssm/include/SSMCollinearity.h>
#include <ssm/include/SSMConstants.h>
#include <ssm/include/SSMDataStructures.h>
#include <ssm/include/SSMLS.h>
#include <ssm/include/ssmphoto.h>
#include <ssm/include/utilitygrocery.h>

/**
Adjustment solution
Directly compose normal matrix
N_dot, N_2dot, N_bar
Reduced normal matrices are used for better performance.

[Mathematical Model]
A%X = L+V
*/

#define BALOG_INFO LOG(INFO) << "SSMBundle:\t"

const double rad2deg = 180.0 / 3.14159265358979;
const double deg2rad = 3.14159265358979 / 180.0;

namespace ssm
{
	const unsigned int FEWPOINTS = 30;
	const unsigned int MANYPOINTS = 1000;
	enum class SOLVEMETHOD { NONE = 0, SIMPLE = 1, PARTITIONED = 2, REDUCED = 3, LMM = 4 };

	void screenout(const std::string& msg, std::stringstream& logstream)
	{
		BALOG_INFO << msg;
		std::cout << msg << std::endl;
		logstream << msg << std::endl;
	}

	void logmsg(const std::string& msg, std::stringstream& logstream)
	{
		BALOG_INFO << msg;
		logstream << msg << std::endl;

		std::cout << msg << std::endl;
	}

	class Bundle
	{
	public:
		Bundle(const std::vector<std::shared_ptr<data::PhotoData>>& inputPhotos,
			const std::vector<std::shared_ptr<data::ObjectPointParam>>& inputObjPoints,
			const double minS,
			const double maxS,
			const unsigned int maxIter,
			const double sigmaThreshold,
			const double corrTh)
			: photos(inputPhotos),
			objPoints0(inputObjPoints),
			minSigma(minS),
			maxSigma(maxS),
			maxIteration(maxIter),
			sigmaTh(sigmaThreshold),
			correlationTh(corrTh)
		{
		}

		std::string run(const std::string& operationName,
			std::stringstream& camParameters,
			std::stringstream& eopParameters,
			std::stringstream& objParameters,
			const bool LM,
			const SOLVEMETHOD optimizerType)
		{
			/// Prepare required configurations
			prepareRunning(operationName);

			/// Set number of parameters
			numParams.resize(NUMSECTION);
			numParams[CAM] = scui(photos[0]->getCam()->getParameters().size());
			numParams[PHO] = numEOPs;
			numParams[OBJ] = objDimension;

			numDatasets.resize(NUMSECTION);
			numDatasets[CAM] = numCameras;
			numDatasets[PHO] = scui(photos.size());
			numDatasets[OBJ] = scui(collectedObj.size());

			screenout(std::string("Number of cameras: ") + std::to_string(numCameras), this->resultLog);
			screenout(std::string("Number of photos: ") + std::to_string(photos.size()), this->resultLog);
			screenout(std::string("Number of object points: ") + std::to_string(collectedObj.size()), this->resultLog);
			screenout(std::string("Number of all cameras parameters: ") + std::to_string(numParams[CAM]), this->resultLog);
			screenout(std::string("\tNumber of distortion parameters: ") + std::to_string(numDistParams), this->resultLog);

			/// Run least squares
			unsigned int cIteration = 0;
			double currentSd;
			double preSd;
			bool runAgain = true;

			this->slvMethod = optimizerType;
			if (this->slvMethod == SOLVEMETHOD::NONE)
			{
				const unsigned int numObjPts = numParams[OBJ] * numDatasets[OBJ];
				if (numObjPts < FEWPOINTS)
				{
					screenout("Solve method: Simple", this->resultLog);
					this->slvMethod = SOLVEMETHOD::SIMPLE;
				}
				else if (numObjPts > MANYPOINTS)
				{
					screenout("Solve method: Reduced normal matrix", this->resultLog);
					this->slvMethod = SOLVEMETHOD::REDUCED;
					if (LM)
						this->slvMethod = SOLVEMETHOD::LMM;
				}
				else
				{
					screenout("Solve method: partitioned matrix", this->resultLog);
					this->slvMethod = SOLVEMETHOD::PARTITIONED;
				}
			}

			math::Matrixd matNDot, matN2Dot, matCDot, matC2Dot, matNbar, matX, Ninv, N;
			double posVar;
			ssm::LeastSquare LS;
			LS.init();
			std::vector<unsigned int> fixedParameters;
			const unsigned int boundarySect = PHO;
			/// ----- For LM method -----
			const double up = 10.0; /// For LM method
			const double down = 0.1; /// For LM method
			double dampingStep = 0.0001; /// For LM method
			double mult = 1.0 + dampingStep; /// For LM method
			double preVar; /// For LM method
			double successfulIt = true;

			/// Estimated parameter log
			camParameters.str("");
			camParameters.setf(std::ios::scientific, std::ios::fixed);
			camParameters.precision(12);
			eopParameters.str("");
			eopParameters.setf(std::ios::scientific, std::ios::fixed);
			eopParameters.precision(12);
			objParameters.str("");
			objParameters.setf(std::ios::scientific, std::ios::fixed);
			objParameters.precision(12);

			if(true)
			{
				/// Analyze image observation residuals
				double mean, std;
				auto resImg = getResidualsOfCollinearity(mean, std);
				std::stringstream msg;
				msg.precision(12);
				msg.setf(std::ios::floatfield, std::ios::fixed);
				msg << "Mean of img-pt observation residuals[mm] for initial parameters: " << mean;
				screenout(msg.str(), this->resultLog);
				msg.str("");
				msg << "Std of img-pt observation residuals[mm] for initial parameters: " << std;
				screenout(msg.str(), this->resultLog);
			}

			screenout("[Initial parameters]", this->resultLog);
			this->printOutParameters();

			/// Posteriori variance log
			std::stringstream varLog;

			do {
				++cIteration;
				std::stringstream coutmsg;

				coutmsg << std::endl;
				coutmsg << "------------------------------------" << std::endl;
				coutmsg << "Iteration:\t" << cIteration << std::endl;
				coutmsg << "------------------------------------" << std::endl;
				screenout(coutmsg.str(), this->resultLog);

				/// Configuration of LS
				LS.config(numParams, numDatasets);

				/// 1. Collinearity equations (observation equations)
				logmsg("Fill a normal matrix for collinearity equations", this->resultLog);
				unsigned int numEqs = 2;
				doForCollinearity(LS, numEqs, 1.0);

				/// 2. Parameter observations (constraints): camera parameter, eop, and object point contributions
				logmsg("Fill a normal matrix for parameter contributions", this->resultLog);
				fixedParameters = doForParameters(LS, 1.0);

				logmsg("Solve least squares", this->resultLog);
				switch (this->slvMethod)
				{
				case SOLVEMETHOD::SIMPLE:
					Ninv = LS.runLeastSquare(posVar, matX, fixedParameters, boundarySect, N);
					break;
				case SOLVEMETHOD::PARTITIONED:
					Ninv = LS.runLeastSquare_Partitioned(posVar, matX, fixedParameters, boundarySect, N);
					break;
				case SOLVEMETHOD::REDUCED:
					LS.runLeastSquare_RN(posVar, matNDot, matCDot, matC2Dot, matNbar, matX, fixedParameters, boundarySect);
					break;
				case SOLVEMETHOD::LMM:
					screenout(std::string("Damping-step: ") + std::to_string(dampingStep), this->resultLog);
					screenout(std::string("Multiplier: ") + std::to_string(mult), this->resultLog);
					LS.runLeastSquare_RN_LM(posVar, matNDot, matCDot, matC2Dot, matNbar, matX, fixedParameters, boundarySect, mult);
					if (cIteration > 1)
					{
						if ((posVar - preVar) < 0.0)
						{
							dampingStep *= down;
							mult = 1.0 + dampingStep;
							successfulIt = true;
						}
						else
						{
							const double oldDampingStep = dampingStep;
							dampingStep *= up;
							mult = (1.0 + dampingStep) / (1.0 + oldDampingStep);
							successfulIt = false;
						}
					}
					/// Update previous variance
					preVar = posVar;
					break;
				default:
					this->slvMethod = SOLVEMETHOD::REDUCED;
					LS.runLeastSquare_RN(posVar, matNDot, matCDot, matC2Dot, matNbar, matX, fixedParameters, boundarySect);
					break;
				}

				/// record posteriori variance
				varLog << std::to_string(posVar) << std::endl;

				/// Update unknowns				
				if (successfulIt)
				{
					screenout("Update parameters", this->resultLog);
					updateUnknowns(matX, camParameters, eopParameters, objParameters);
					this->printOutParameters();
				}

				/// Check iteration criteria
				currentSd = sqrt(posVar);
				screenout(std::string("Posteriori standard deviation:\t") + std::to_string(currentSd), this->resultLog);

				if (cIteration > 1 && fabs(currentSd - preSd) < this->sigmaTh && (this->slvMethod != SOLVEMETHOD::LMM || (this->slvMethod == SOLVEMETHOD::LMM && successfulIt)))
				{
					screenout(std::string("(sd - pre_sd) < ") + std::to_string(this->sigmaTh), this->resultLog);
					runAgain = false;
				}
				else
				{
					preSd = currentSd;
				}

				if (cIteration >= this->maxIteration)
				{
					runAgain = false;
				}

				screenout(std::string("Iteration# ") + std::to_string(cIteration) + std::string(" Done"), this->resultLog);

				/// Analyze image observation residuals
				double mean, std;
				auto resImg = getResidualsOfCollinearity(mean, std);
				std::stringstream msg;
				msg.precision(12);
				msg.setf(std::ios::floatfield, std::ios::fixed);
				msg << "Mean of img-pt observation residuals[mm]: " << mean;
				screenout(msg.str(), this->resultLog);
				msg.str("");
				msg << "Std of img-pt observation residuals[mm]: " << std;
				screenout(msg.str(), this->resultLog);

			} while (runAgain);

			/// Last N matrix and its inverse
			if (this->slvMethod == SOLVEMETHOD::REDUCED)
			{
				this->lastNMat = LS.getNmat(fixedParameters, boundarySect);
			}
			else
			{
				this->lastNMat = N;
				this->lastNinvMat = Ninv;
			}

			this->lastPosVar = posVar;

			screenout(std::string("\nLeast squares with reduced normal matrix is done."), this->resultLog);

			/// Return a result-log
			return this->resultLog.str();
		}

		std::string getCorrelation()
		{
			if (this->slvMethod == SOLVEMETHOD::REDUCED)
			{
				std::vector<math::Matrixd> partitionedN(4);
				unsigned int nRows = this->lastNMat.getRows();
				unsigned int nRowsHalf = (nRows / 2);
				partitionedN[0] = lastNMat.getSubset(0, 0, nRowsHalf, nRowsHalf);
				partitionedN[1] = lastNMat.getSubset(0, nRowsHalf, nRowsHalf, nRows - nRowsHalf);
				partitionedN[2] = lastNMat.getSubset(nRowsHalf, 0, nRows - nRowsHalf, nRowsHalf);
				partitionedN[3] = lastNMat.getSubset(nRowsHalf, nRowsHalf, nRows - nRowsHalf, nRows - nRowsHalf);
				this->lastNinvMat = math::inversePartitionedMatrixd(partitionedN[0], partitionedN[1], partitionedN[2], partitionedN[3]);
			}
			
			math::Matrixd correlation = ssm::LeastSquare::getCorrelation(this->lastNinvMat * this->lastPosVar);

			{
				auto VarCov = this->lastNinvMat * this->lastPosVar;
				unsigned int i, j;
				//Correlation of Unknown Matrix
				math::Matrixd Correlation;
				Correlation.resize(VarCov.getRows(), VarCov.getCols(), 0.);

				for (i = 0; i < Correlation.getRows(); i++)
				{
					double sigma_i, sigma_j;
					sigma_i = sqrt(VarCov(i, i));

					for (j = i+1; j < Correlation.getCols(); j++)
					{
						sigma_j = sqrt(VarCov(j, j));
						Correlation(i, j) = VarCov(i, j) / sigma_i / sigma_j;

						if (fabs(Correlation(i, j)) > 1.0)
						{
							double temp_ij = Correlation(i, j);
							std::cout << "(" << i << "\t" << j << ")" << std::endl;
							std::cout << temp_ij << std::endl;
							std::cout << VarCov(i, j) << std::endl;
							std::cout << VarCov(i, i) << std::endl;
							std::cout << VarCov(j, j) << std::endl;
						}
					}
				}
			}

			return analysisCorrelation(correlation);
		}

		/// Get camera parameters
		std::vector<std::vector<param::Parameter>> getCameraParameters()
		{
			std::vector<std::vector<param::Parameter>> camParams;

			for (unsigned int camIdx = 0; camIdx < this->camIds.size(); ++camIdx)
			{
				std::vector<param::Parameter> camParam_i;

				for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
				{
					auto cam = photos[phoIdx]->getCam();

					if (camIds[camIdx] == cam->getId())
					{
						auto parameters = cam->getParameters();
						camParam_i.resize(parameters.size());

						for (unsigned int p = 0; p < parameters.size(); ++p)
						{
							/// copy a parameter
							camParam_i[p] = (*parameters[p]);
						}

						/// Next camera
						break;
					}
				}

				/// Put a parameter-set into a container
				camParams.push_back(camParam_i);
			}

			return camParams;
		}

		/// Get EO parameters
		std::vector<std::vector<param::Parameter>> getEopParameters()
		{
			std::vector<std::vector<param::Parameter>> eopParams;

			for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
			{
				auto& photo = photos[phoIdx];
				auto eop = photo->getEop()->getParameters();
				std::vector<param::Parameter> eopParam_i(eop.size());

				for (unsigned int p = 0; p < eop.size(); ++p)
				{
					/// copy a parameter
					eopParam_i[p] = (*eop[p]);
				}

				eopParams.push_back(eopParam_i);
			}

			return eopParams;
		}

		/// Get object points
		std::vector<std::vector<param::Parameter>> getObjPoints()
		{
			std::vector<std::vector<param::Parameter>> objPoints;

			for (unsigned int objIdx = 0; objIdx < collectedObj.size(); ++objIdx)
			{
				auto& ptCoord = collectedObj[objIdx]->coord;
				std::vector<param::Parameter> objPt(ptCoord.size());

				for (unsigned int p = 0; p < ptCoord.size(); ++p)
				{
					/// copy a parameter
					objPt[p] = (*ptCoord[p]);
				}

				objPoints.push_back(objPt);
			}

			return objPoints;
		}

		/// Residuals of parameters (residual = estimated value - initial value)
		std::vector<std::vector<std::vector<double>>> getResidualsOfParameters()
		{
			std::vector<std::vector<double>> residualsCam(this->camIds.size());

			for (unsigned int camIdx = 0; camIdx < this->camIds.size(); ++camIdx)
			{
				for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
				{
					auto cam = photos[phoIdx]->getCam();

					if (camIds[camIdx] == cam->getId())
					{
						auto camParams = cam->getParameters();
						residualsCam[camIdx].resize(cam->getParameters().size());

						for (unsigned int p = 0; p < cam->getParameters().size(); ++p)
						{
							if (camParams[p]->getStDev() >= this->maxSigma)
								residualsCam[camIdx][p] = 0.0;
							else
								residualsCam[camIdx][p] = camParams[p]->get() - camParams[p]->getInitVal();
						}

						/// Next camera
						break;
					}
				}
			}

			std::vector<std::vector<double>> residualsEop(photos.size());

			for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
			{
				auto& photo = photos[phoIdx];

				auto photoParams = photo->getEop();
				residualsEop[phoIdx].resize(photoParams->getParameters().size());

				for (unsigned int p = 0; p < photoParams->getParameters().size(); ++p)
				{
					if (photoParams->getParameters()[p]->getStDev() >= this->maxSigma)
						residualsEop[phoIdx][p] = 0.0;
					else
						residualsEop[phoIdx][p] = photoParams->getParameters()[p]->get() - photoParams->getParameters()[p]->getInitVal();
				}
			}

			std::vector<std::vector<double>> residualsObjPt(collectedObj.size());

			for (unsigned int objIdx = 0; objIdx < collectedObj.size(); ++objIdx)
			{
				auto& objPt = collectedObj[objIdx];
				residualsObjPt[objIdx].resize(objPt->coord.size());

				for (unsigned int p = 0; p < objPt->coord.size(); ++p)
				{
					if (objPt->coord[p]->getStDev() >= this->maxSigma)
						residualsObjPt[objIdx][p] = 0.0;
					else
						residualsObjPt[objIdx][p] = objPt->coord[p]->get() - objPt->coord[p]->getInitVal();
				}
			}

			std::vector < std::vector<std::vector<double>>> residuals(3);
			residuals[0] = residualsCam;
			residuals[1] = residualsEop;
			residuals[2] = residualsObjPt;

			return residuals;
		}

		/// Residuals of image observations for the final estimated parameters and collinearity equations
		std::vector<std::vector<std::vector<double>>> getResidualsOfCollinearity()
		{
			double meanError, stdError;
			return getResidualsOfCollinearity(meanError, stdError);
		}

		std::vector<std::vector<std::vector<double>>> getResidualsOfCollinearity(double& mean, double& std)
		{
			double sum = 0.0;
			double sum2 = 0.0;
			unsigned int count = 0;

			std::vector<std::vector<std::vector<double>>> residuals(photos.size());

			for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
			{
				auto& pho = photos[phoIdx];
				unsigned int camIdx = camIdxOfPhotos[phoIdx];

				std::vector<std::shared_ptr<param::Parameter>>& camParams = pho->getCam()->getCamParameters();
				std::vector<std::shared_ptr<param::Parameter>>& distParams = pho->getCam()->getDistortionModel()->getParameters();
				std::vector<std::shared_ptr<param::Parameter>>& eopParams = pho->getEop()->getParameters();

				residuals[phoIdx].resize(pho->getPts().size());

				for (unsigned int imgPtIdx = 0; imgPtIdx < pho->getPts().size(); ++imgPtIdx)
				{
					auto& imgPt = pho->accessImgPts()[imgPtIdx];

					std::vector<std::shared_ptr<param::Parameter>>& objPtParams = collectedObj[imgPt.getObjPtId()]->coord;
					std::vector<std::shared_ptr<param::Parameter>>& givenObs = imgPt.coord;
					/// Prepare camera, lens distortion, eop and object point parameters
					std::vector<std::shared_ptr<param::Parameter>> givenParams;
					givenParams.insert(givenParams.end(), camParams.begin(), camParams.end());
					givenParams.insert(givenParams.end(), distParams.begin(), distParams.end());
					givenParams.insert(givenParams.end(), eopParams.begin(), eopParams.end());
					givenParams.insert(givenParams.end(), objPtParams.begin(), objPtParams.end());

					math::Matrixd amat, bmat, lmat;

					auto l = -ssm::collinearityEqWithAlignment(givenParams, givenObs);

					residuals[phoIdx][imgPtIdx].resize(l.getRows());
					for (unsigned int r = 0; r < l.getRows(); ++r)
					{
						residuals[phoIdx][imgPtIdx][r] = l(r, 0);
						sum += l(r, 0);
						sum2 += l(r, 0) * l(r, 0);
						++count;
					}
				}
			}

			if (count > 0)
			{
				mean = sum / count;
				std = sqrt(sum2 / count);
			}

			return residuals;
		}

		/// get log message
		std::string getLogMsg() const { return this->resultLog.str(); }

	private:

		void printOutParameters()
		{
			for (unsigned int camIdx = 0; camIdx < this->camIds.size(); ++camIdx)
			{
				for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
				{
					auto cam = photos[phoIdx]->getCam();

					if (camIds[camIdx] == cam->getId())
					{
						screenout(std::to_string(cam->getId()) + std::string(" Camera: "), this->resultLog);

						std::string names;
						std::string values;

						auto camParams = cam->getParameters();
						for (unsigned int p = 0; p < camParams.size(); ++p)
						{
							auto nameP = camParams[p]->getName();
							names += nameP;

							if (camParams[p]->getStDev() <= this->minSigma)
							{
								names += std::string("(Fixed)");
							}

							auto valueP = camParams[p]->get();
							if (nameP == constant::camera::_omega_bs_ ||
								nameP == constant::camera::_phi_bs_ ||
								nameP == constant::camera::_kappa_bs_)
							{
								valueP = util::Rad2Deg(valueP);
								names += std::string("[deg]");
							}
							names += std::string("\t");
							values += std::to_string(valueP) + std::string("\t");
						}
						screenout(names, this->resultLog);
						screenout(values, this->resultLog);

						/// Next camera
						break;
					}
				}
			}

			for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
			{
				auto& photo = photos[phoIdx];

				screenout(std::to_string(photo->getId()) + std::string("th Photo Eops: "), this->resultLog);

				std::string names;
				std::string values;

				auto photoParams = photo->getEop()->getParameters();
				for (unsigned int p = 0; p < photoParams.size(); ++p)
				{
					auto nameP = photoParams[p]->getName();
					names += nameP;

					if (photoParams[p]->getStDev() <= this->minSigma)
					{
						names += std::string("(Fixed)");
					}

					auto valueP = photoParams[p]->get();
					if (nameP == constant::photo::_omega_ ||
						nameP == constant::photo::_phi_ ||
						nameP == constant::photo::_kappa_)
					{
						valueP = util::Rad2Deg(valueP);
						names += std::string("[deg]");
					}

					names += std::string("\t");
					values += std::to_string(valueP) + std::string("\t");
				}
				screenout(names, this->resultLog);
				screenout(values, this->resultLog);
			}

			for (unsigned int objIdx = 0; objIdx < collectedObj.size(); ++objIdx)
			{
				screenout(std::to_string(collectedObj[objIdx]->id) + std::string(" Object point: "), this->resultLog);

				std::string names;
				std::string values;

				auto& ptCoord = collectedObj[objIdx]->coord;
				for (unsigned int p = 0; p < ptCoord.size(); ++p)
				{
					names += ptCoord[p]->getName();

					if (ptCoord[p]->getStDev() <= this->minSigma)
					{
						names += std::string("(Fixed)");
					}

					names += std::string("\t");
					values += std::to_string(ptCoord[p]->get()) + std::string("\t");
				}

				/// Check whether it is a full control point
				if (ptCoord[0]->getStDev() <= this->minSigma &&
					ptCoord[1]->getStDev() <= this->minSigma &&
					ptCoord[2]->getStDev() <= this->minSigma)
					names += std::string(" ---Fixed full control point---\t");

				screenout(names, this->resultLog);
				screenout(values, this->resultLog);
			}
		}

		std::string analysisCorrelation(const math::Matrixd& correlation)
		{
			std::stringstream retMsg;
			retMsg.str("");

			/// Collected parameter names
			std::vector<std::string> paramNames(correlation.getRows());

			/// 1. get parameter names
			unsigned int xIdx = 0;

			for (unsigned int camIdx = 0; camIdx < this->camIds.size(); ++camIdx)
			{
				for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
				{
					auto cam = photos[phoIdx]->getCam();

					if (camIds[camIdx] == cam->getId())
					{
						auto camParams = cam->getParameters();
						for (unsigned int p = 0; p < cam->getParameters().size(); ++p)
						{
							paramNames[xIdx] = std::to_string(camIds[camIdx]) + std::string(" Camera ") + camParams[p]->getName();
							++xIdx;
						}

						/// Next camera
						break;
					}
				}
			}

			for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
			{
				auto& photo = photos[phoIdx];

				auto photoParams = photo->getEop();
				for (unsigned int p = 0; p < photoParams->getParameters().size(); ++p)
				{
					paramNames[xIdx] = std::to_string(photo->getId()) + std::string(" Photo ") + photoParams->getParameters()[p]->getName();
					++xIdx;
				}
			}

			for (unsigned int objIdx = 0; objIdx < collectedObj.size(); ++objIdx)
			{
				auto& objPt = collectedObj[objIdx];

				for (unsigned int p = 0; p < objPt->coord.size(); ++p)
				{
					paramNames[xIdx] = std::to_string(objPt->id) + std::string(" ObjectPoint ") + objPt->coord[p]->getName();
					++xIdx;
				}
			}

			/// 2. Correlation analysis 
			screenout("[Correlation]", this->resultLog);
			for (unsigned int r = 0; r < correlation.getRows(); ++r)
			{
				for (unsigned int c = r+1; c < correlation.getCols(); ++c)
				{
					if (r != c && fabs(correlation(r, c)) > this->correlationTh)
					{
						std::string msg;
						msg = std::string("Correlation(") + std::to_string(r) + std::string(", ") + std::to_string(c) + std::string(") : ") + std::to_string(correlation(r, c));
						msg += std::string(" between ") + paramNames[r] + std::string(" and ") + paramNames[c];

						logmsg(msg, this->resultLog);
						retMsg << msg << std::endl;

					}
				}
			}

			return retMsg.str();
		}

		void updateUnknowns(const math::Matrixd& X,
			std::stringstream& camParameters,
			std::stringstream& eopParameters,
			std::stringstream& objParameters)
		{
			/// Collected parameter names
			std::vector<std::string> paramNames(X.getRows());

			/// unknown index
			unsigned int xIdx = 0;

			for (unsigned int camIdx = 0; camIdx < this->camIds.size(); ++camIdx)
			{
				for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
				{
					auto cam = photos[phoIdx]->getCam();

					if (camIds[camIdx] == cam->getId())
					{
						auto camParams = cam->getParameters();
						for (unsigned int p = 0; p < cam->getParameters().size(); ++p)
						{
							auto oldVal = camParams[p]->get();
							camParams[p]->set(oldVal + X(xIdx, 0));
							paramNames[xIdx] = std::to_string(camIds[camIdx]) + std::string(" Camera ") + camParams[p]->getName();

							camParameters << camParams[p]->get() << "\t";

							++xIdx;
						}

						/// Next camera
						break;
					}
				}

				camParameters << std::endl;
			}

			for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
			{
				auto& photo = photos[phoIdx];

				auto photoParams = photo->getEop();
				for (unsigned int p = 0; p < photoParams->getParameters().size(); ++p)
				{
					auto oldVal = photoParams->getParameters()[p]->get();
					photoParams->getParameters()[p]->set(oldVal + X(xIdx, 0));
					paramNames[xIdx] = std::to_string(photo->getId()) + std::string(" Photo ") + photoParams->getParameters()[p]->getName();

					eopParameters << photoParams->getParameters()[p]->get() << "\t";

					++xIdx;
				}

				eopParameters << std::endl;
			}

			for (unsigned int objIdx = 0; objIdx < collectedObj.size(); ++objIdx)
			{
				auto& objPt = collectedObj[objIdx];

				for (unsigned int p = 0; p < objPt->coord.size(); ++p)
				{
					auto oldVal = objPt->coord[p]->get();
					objPt->coord[p]->set(oldVal + X(xIdx, 0));
					paramNames[xIdx] = std::to_string(objPt->id) + std::string(" ObjectPoint ") + objPt->coord[p]->getName();

					objParameters << objPt->coord[p]->get() << "\t";

					++xIdx;
				}

				objParameters << std::endl;
			}
		}

		void doForCollinearity(ssm::LeastSquare& LS, const int numEqs, const double prioriVariance = 1.0, const unsigned int pdeMethod = 0)
		{
			/// Numeric partial differential equation type
			const math::NPDETYPE npdeType = math::NPDETYPE::FirstOrder;
			//const math::NPDETYPE npdeType = math::NPDETYPE::FourthOrder;

			std::vector<LeastSquare::AMat> a(NUMSECTION);
			a[CAM].sect = CAM;
			a[PHO].sect = PHO;
			a[OBJ].sect = OBJ;

			for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
			{
				auto& pho = photos[phoIdx];
				unsigned int camIdx = camIdxOfPhotos[phoIdx];

				a[CAM].index = camIdx;
				a[PHO].index = phoIdx;

				std::vector<std::shared_ptr<param::Parameter>>& camParams = pho->getCam()->getCamParameters();
				std::vector<std::shared_ptr<param::Parameter>>& distParams = pho->getCam()->getDistortionModel()->getParameters();
				std::vector<std::shared_ptr<param::Parameter>>& eopParams = pho->getEop()->getParameters();

				const auto pitchParam = param::getParameter(camParams, util::uni2multi(data::cam::_pixel_pitch));
				if (pitchParam == nullptr)
					throw std::runtime_error("Error from doForCollinearity, failed in getting a pixel pitch parameter");

				double benchVar = pitchParam->get() * pitchParam->get();
				if (pitchParam->get() < std::numeric_limits<double>::epsilon())
					throw std::runtime_error("Error from doForCollinearity, zero or too small pixel pitch");

				math::Matrixd w(numEqs, numEqs);
				w.makeIdentityMat();
				w *= prioriVariance;
				w /= benchVar;

				for (unsigned int imgPtIdx = 0; imgPtIdx < pho->getPts().size(); ++imgPtIdx)
				{
					auto& imgPt = pho->accessImgPts()[imgPtIdx];

					a[OBJ].index = imgPt.getObjPtId();

					std::vector<std::shared_ptr<param::Parameter>>& objPtParams = collectedObj[imgPt.getObjPtId()]->coord;
					std::vector<std::shared_ptr<param::Parameter>>& givenObs = imgPt.coord;
					/// Prepare camera, lens distortion, eop and object point parameters
					std::vector<std::shared_ptr<param::Parameter>> givenParams;
					givenParams.insert(givenParams.end(), camParams.begin(), camParams.end());
					givenParams.insert(givenParams.end(), distParams.begin(), distParams.end());
					givenParams.insert(givenParams.end(), eopParams.begin(), eopParams.end());
					givenParams.insert(givenParams.end(), objPtParams.begin(), objPtParams.end());

					math::Matrixd amat, bmat, lmat;
					switch (pdeMethod)
					{
					case 0:
						math::getNumericPDEWithParameters(ssm::collinearityEqWithAlignment, givenParams, givenObs, amat, bmat, lmat, npdeType);
						break;
					case 1:
						///
						break;
					default:
						math::getNumericPDEWithParameters(ssm::collinearityEqWithAlignment, givenParams, givenObs, amat, bmat, lmat, npdeType);
					}

					unsigned int numCols = 0;
					a[CAM].a = amat.getSubset(0, numCols, numEqs, numParams[CAM]);
					numCols += numParams[CAM];
					a[PHO].a = amat.getSubset(0, numCols, numEqs, numParams[PHO]);
					numCols += numParams[PHO];
					a[OBJ].a = amat.getSubset(0, numCols, numEqs, numParams[OBJ]);

					LS.fillNmat(a, w, lmat);
				}

			}
		}

		std::vector<unsigned int> doForParameters(ssm::LeastSquare& LS, const double prioriVariance = 1.0)
		{
			std::vector<unsigned int> fixedParameterIdx;

			//
			// Camera contribution
			//
#ifdef _DEBUG
			LOG(INFO) << "doForParameters";
			LOG(INFO) << "camera contribution";
#endif
			for (unsigned int camIdx = 0; camIdx < scui(camIds.size()); ++camIdx)
			{
				unsigned int idxOffset = this->numParams[CAM] * camIdx;

				for (auto& pho : photos)
				{
					const auto& cam = pho->getCam();

					if (cam->getId() == camIds[camIdx])
					{
						unsigned int numCamParams = scui(cam->getParameters().size());

						math::Matrixd wCam(numCamParams, numCamParams);
						wCam.makeIdentityMat();
						math::Matrixd aCam(numCamParams, numCamParams);
						aCam.makeIdentityMat();
						math::Matrixd lCam(numCamParams, 1);

						const auto& camParams = cam->getParameters();

						int numFullUnknowns = 0;

						for (unsigned int idx = 0; idx < scui(camParams.size()); ++idx)
						{
							double sd = camParams[idx]->getStDev();
#ifdef _DEBUG
							if (sd <= std::numeric_limits<double>::epsilon())
							{
								throw std::runtime_error(std::string("Wrong standard deviation of ") +
									std::to_string(camIdx) +
									std::string(" camera ") +
									camParams[idx]->getName());
							}
							else
							{
								LOG(INFO) << idx << "th camera parameter sd: " << sd;
								std::clog << idx << "th camera parameter sd: " << sd << std::endl;
							}
#endif
							///
							/// Full unknown check with the predefined maximum standard deviation
							/// 
							if (sd >= maxSigma)
							{
								/// Full unknowns
								wCam(idx, idx) = 0.0;
								++numFullUnknowns;
							}
							else if (sd <= minSigma)
							{
								/// Add fixed parameters
								fixedParameterIdx.push_back(idxOffset + idx);
							}
							else
							{
								/// Stochastic parameters
								wCam(idx, idx) = prioriVariance / (sd * sd);
							}

							lCam(idx, 0) = camParams[idx]->getInitVal() - camParams[idx]->get();
						}

						std::vector<LeastSquare::AMat> a(1);
						a[0].sect = CAM;
						a[0].index = camIdx;
						a[0].a = aCam;

						int adjustedNumEqs = -numFullUnknowns;
						LS.fillNmat(a, wCam, lCam, adjustedNumEqs);

						break;
					}
				}
			}

			//
			// Pos and attitude contribution
			//
			for (unsigned int phoIdx = 0; phoIdx < scui(photos.size()); ++phoIdx)
			{
				unsigned int idxOffset = this->numParams[CAM] * this->numDatasets[CAM];
				idxOffset += this->numParams[PHO] * phoIdx;

				math::Matrixd wEop(numEOPs, numEOPs);
				wEop.makeIdentityMat();
				math::Matrixd aEop(numEOPs, numEOPs);
				aEop.makeIdentityMat();
				math::Matrixd lEop(numEOPs, 1);

				auto& eopParams = photos[phoIdx]->getEop()->getParameters();
				int numFullUnknowns = 0;

				for (unsigned int idx = 0; idx < eopParams.size(); ++idx)
				{
					double sd = eopParams[idx]->getStDev();
#ifdef _DEBUG
					if (sd <= std::numeric_limits<double>::epsilon())
						throw std::runtime_error(std::string("Wrong standard deviation of ") +
							std::to_string(phoIdx) +
							std::string(" photo ") +
							eopParams[idx]->getName());
#endif
					///
					/// Full unknown check with the predefined maximum standard deviation
					/// 
					if (sd >= maxSigma)
					{
						/// Full unknowns
						wEop(idx, idx) = 0.0;
						++numFullUnknowns;
					}
					else if (sd <= minSigma)
					{
						/// Add fixed parameters
						fixedParameterIdx.push_back(idxOffset + idx);
					}
					else
					{
						/// Stocahstic parameters
						wEop(idx, idx) = prioriVariance / (sd * sd);
					}

					lEop(idx, 0) = eopParams[idx]->getInitVal() - eopParams[idx]->get();
				}

				std::vector<LeastSquare::AMat> a(1);
				a[0].sect = PHO;
				a[0].index = phoIdx;
				a[0].a = aEop;

				int adjustedNumEqs = -numFullUnknowns;
				LS.fillNmat(a, wEop, lEop, adjustedNumEqs);
			}

			//
			// Object point contribution
			//
			for (unsigned int objIdx = 0; objIdx < scui(collectedObj.size()); ++objIdx)
			{
				unsigned int idxOffset = this->numParams[CAM] * this->numDatasets[CAM]
					+ this->numParams[PHO] * this->numDatasets[PHO]
					+ this->numParams[OBJ] * objIdx;

				math::Matrixd wObj(objDimension, objDimension);
				wObj.makeIdentityMat();
				math::Matrixd aObj(objDimension, objDimension);
				aObj.makeIdentityMat();
				math::Matrixd lObj(objDimension, 1);

				auto& objCoord = collectedObj[objIdx]->coord;

				int numFullUnknowns = 0;

				for (unsigned int idx = 0; idx < objCoord.size(); ++idx)
				{
					double sd = objCoord[idx]->getStDev();
#ifdef _DEBUG
					if (sd <= std::numeric_limits<double>::epsilon())
						throw std::runtime_error(std::string("Wrong standard deviation of ") +
							std::to_string(objIdx) +
							std::string(" object point ") +
							objCoord[idx]->getName());
#endif
					///
					/// Full unknown check with the predefined maximum standard deviation
					/// 
					if (sd >= maxSigma)
					{
						/// Full unknowns
						wObj(idx, idx) = 0.0;
						++numFullUnknowns;
					}
					else if (sd <= minSigma)
					{
						/// Add fixed parameters
						fixedParameterIdx.push_back(idxOffset + idx);
					}
					else
					{
						/// Stocastic parameters
						wObj(idx, idx) = prioriVariance / (sd * sd);
					}

					lObj(idx, 0) = objCoord[idx]->getInitVal() - objCoord[idx]->get();
				}

				std::vector<LeastSquare::AMat> a(1);
				a[0].sect = OBJ;
				a[0].index = objIdx;
				a[0].a = aObj;

				int adjustedNumEqs = -numFullUnknowns;
				LS.fillNmat(a, wObj, lObj, adjustedNumEqs);
			}

			return fixedParameterIdx;
		}

		void prepareRunning(const std::string& operationName)
		{
			/// Initialize output message
			this->resultLog.str("");
			resultLog << std::string("----- Log message for ") << operationName << std::string(" -----") << std::endl;

			/// Initial check procedures
			screenout("collect available object points", this->resultLog);
			collectedObj = CheckTiePoints();
			screenout(std::string("Number of given object points: ") + std::to_string(this->objPoints0.size()), this->resultLog);
			screenout(std::string("Number of collected object points: ") + std::to_string(this->collectedObj.size()), this->resultLog);

			/// Number of lens distortion models, number of cameras and camera-index for each photo
			numDistParams = scui(photos[0]->getCam()->getDistParameters().size());
			camIds.clear();
			camIdxOfPhotos.clear();
			camIdxOfPhotos.resize(photos.size());

			/// The camera of the first photo
			camIds.push_back(photos[0]->getCam()->getId());
			camIdxOfPhotos[0] = 0;

			for (unsigned int i = 1; i < photos.size(); ++i)
			{
				if (numDistParams != photos[i]->getCam()->getDistParameters().size())
				{
					std::stringstream msgstr;
					msgstr << "Numbers of lens distortion parameters are not identical. For now, all camera should have a same distortion model.";
					screenout(msgstr.str(), this->resultLog);
					return;
				}

				bool found = false;
				for (unsigned int j = 0; j < camIds.size(); ++j)
				{
					const auto& cam = photos[i]->getCam();
					if (camIds[j] == cam->getId())
					{
						found = true;
						/// Save the camera index for each photo
						camIdxOfPhotos[i] = j;
						break;
					}
				}

				if (found == false)
				{
					camIds.push_back(photos[i]->getCam()->getId());
					/// Save the new camera index for a photo
					camIdxOfPhotos[i] = scui(camIds.size() - 1);
				}
			}

			/// Number of cameras
			numCameras = scui(camIds.size());

			/// Set an object point index to a image point of each photo
			for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
			{
				auto& imgPts = photos[phoIdx]->accessImgPts();

				for (unsigned int imgPtIdx = 0; imgPtIdx < imgPts.size(); ++imgPtIdx)
				{
					unsigned int objPtIdx;
					auto ptId = imgPts[imgPtIdx].getObjPtId();
					if (getObjPtIdx(ptId, objPtIdx))
					{
						imgPts[imgPtIdx].setObjPtIndex(objPtIdx);
					}
					else
					{
						throw std::runtime_error("Failed in finding object point id");
					}
				}
			}
		}

		bool getObjPtIdx(const unsigned int ptId, unsigned int& returnIdx)
		{
			for (unsigned int ptIdx = 0; ptIdx < collectedObj.size(); ++ptIdx)
			{
				if (ptId == collectedObj[ptIdx]->id)
				{
					returnIdx = ptIdx;
					return true;
				}
			}

			return false;
		}

		bool getObjPtIdx(const std::vector< std::shared_ptr<data::ObjectPointParam>>& objParams, const unsigned int ptId, unsigned int& returnIdx)
		{
			for (unsigned int ptIdx = 0; ptIdx < objParams.size(); ++ptIdx)
			{
				if (ptId == objParams[ptIdx]->id)
				{
					returnIdx = ptIdx;
					return true;
				}
			}

			return false;
		}

		std::vector<std::shared_ptr<data::ObjectPointParam>> CheckTiePoints()
		{
			///
			/// This procedure is not essential 
			/// because BundleBlockData::CheckData() already conduct a similar procedure.
			/// Todo: later, upgrade data integrity check including variance and the lack of datum, 
			/// and all the checks should be done in the data handling class, not here.
			///

			/// Delete unavailable imge points
			for (unsigned int i = 0; i < photos.size(); ++i)
			{
				auto copy_pts_i = photos[i]->accessImgPts();
				std::vector<data::ImagePointParam> new_pts_i;
				new_pts_i.reserve(copy_pts_i.size());
				unsigned int count = 0;

				for (size_t ptIdx = 0; ptIdx < copy_pts_i.size(); ++ptIdx)
				{
					const auto i_objId = copy_pts_i[ptIdx].getObjPtId();

					/// find an image pt id in object points
					bool found = false;
					for (size_t objPtIdx = 0; objPtIdx < this->objPoints0.size(); ++objPtIdx)
					{
						if (i_objId == this->objPoints0[objPtIdx]->id)
						{
							found = true;
							break;
						}
					}

					if (found)
					{
						/// check multiple observations for tie points
						count = 0;
						for (unsigned int j = 0; j < photos.size(); ++j)
						{
							auto& pts_j = photos[j]->getPts();
							for (unsigned int k = 0; k < pts_j.size(); ++k)
							{
								const auto j_objId = pts_j[k].getObjPtId();
								if (i_objId == j_objId)
									count++;
							}
						}
						/// Lack of observations and check if it is a fully free point
						if (count < 2 && checkFreePoint(copy_pts_i[ptIdx].getObjPtId()))
						{
							/// Delete unavailable image point
							std::stringstream str;
							str << photos[i]->getId() << " photo - " << copy_pts_i[ptIdx].getObjPtId() << " point is not available.";
							screenout(str.str(), this->resultLog);
						}
						else
							new_pts_i.push_back(copy_pts_i[ptIdx]);
					}
				}

				auto& old_pts_i = photos[i]->accessImgPts();
				old_pts_i = new_pts_i;
			}

			/// Collect available object points
			std::vector<std::shared_ptr<data::ObjectPointParam>> obj;

			for (size_t objPtIdx = 0; objPtIdx < this->objPoints0.size(); ++objPtIdx)
			{
				bool found = false;

				for (unsigned int phoIdx = 0; phoIdx < photos.size(); ++phoIdx)
				{
					auto& imgPts = photos[phoIdx]->getPts();

					for (size_t ptIdx = 0; ptIdx < imgPts.size(); ++ptIdx)
					{
						if (imgPts[ptIdx].getObjPtId() == this->objPoints0[objPtIdx]->id)
						{
							found = true;
							break;
						}
					}
					if (found)
						break;
				}

				if (found)
					obj.push_back(this->objPoints0[objPtIdx]);
#ifdef _DEBUG
				else
					std::cout << "Not found object point id " << this->objPoints0[objPtIdx]->id << std::endl;
#endif
			}

			return obj;
		}

		bool checkFixedPoint(const unsigned int id)
		{
			unsigned int returnIdx;
			if (getObjPtIdx(objPoints0, id, returnIdx))
			{
				for (const auto& p : objPoints0[returnIdx]->coord)
				{
					/// If given sd is greater than minSd, then it is not a fixed control.
					if (p->getStDev() > this->minSigma)
						return false;
				}
				return true;
			}
			return false;
		}

		bool checkFreePoint(const unsigned int id)
		{
			unsigned int returnIdx;
			if (getObjPtIdx(objPoints0, id, returnIdx))
			{
				for (const auto& p : objPoints0[returnIdx]->coord)
				{
					/// If given sd is smaller than maxSd, then it is not a fully free point.
					if (p->getStDev() < this->maxSigma)
						return false;
				}

				return true;
			}

			return false;
		}

		std::vector<std::shared_ptr<data::ObjectPointParam>> collectObjPts(const unsigned int photoIdx)
		{
			std::shared_ptr<data::PhotoData> photo = photos[photoIdx];
			std::vector<std::shared_ptr<data::ObjectPointParam>> obj;

			for (unsigned int i = 0; i < scui(photo->getPts().size()); )
			{
				bool exist = false;
				unsigned int searchIdx = 0;
				for (unsigned int g = 0; g < scui(objPoints0.size()); ++g)
				{
					if (objPoints0[g]->id == photo->getPts()[i].getObjPtId())
					{
						exist = true;
						searchIdx = g;
						break;
					}
				}

				if (exist)
				{
					obj.push_back(objPoints0[searchIdx]);
					++i;
				}
				else
				{
					photo->accessImgPts().erase(photo->getPts().begin() + i);
					continue;
				}
			}

			if (photo->getPts().size() != obj.size())
				throw std::runtime_error(std::string("There an is an image point not connected with any object points: ")
					+ std::to_string(photoIdx)
					+ std::string(" th photo."));

			return obj;
		}

	private:
		/// Photo data
		std::vector<std::shared_ptr<data::PhotoData>> photos;
		/// Given object points
		std::vector< std::shared_ptr<data::ObjectPointParam>> objPoints0;
		/// Collected object points
		std::vector< std::shared_ptr<data::ObjectPointParam>> collectedObj;
		/// Threshold of a sigma values for determining fixed parameters
		const double minSigma;
		/// Threshold of a sigma values for determining free parameters (fully unknown)
		const double maxSigma;
		/// Maximum iteration number
		const unsigned int maxIteration;
		/// Max sigma change between iterations
		const double sigmaTh;
		/// Correlation threshold
		const double correlationTh;
		/// Camera indices for photos
		std::vector<unsigned int> camIdxOfPhotos;
		/// Camera index array
		std::vector<unsigned int> camIds;
		/// Number of parameters for each section
		std::vector<unsigned int> numParams;
		/// Number of dataset in each section (num of cameras, num of photos, num of objPoints, ....)
		std::vector<unsigned int> numDatasets;
		/// Output message
		std::stringstream resultLog;
		/// Final normal matrix
		math::Matrixd lastNMat;
		/// Final normal matrix inverse
		math::Matrixd lastNinvMat;
		/// Last posteriori variance
		double lastPosVar;
		/// Least squares method
		SOLVEMETHOD slvMethod;

		///
		/// numbers for configuration
		/// 
		const unsigned int NUMSECTION = 3;
		const unsigned int CAM = 0;
		const unsigned int PHO = 1;
		const unsigned int OBJ = 2;
		const unsigned int numEOPs = 6;
		const unsigned int objDimension = 3;
		unsigned int numDistParams = 0;
		unsigned int numCameras = 0;
	};
}