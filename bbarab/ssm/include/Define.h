#pragma once

#pragma warning (disable : 4099)

namespace geo
{
	template<typename T>
	struct Point2
	{
		T x;
		T y;

		Point2<T>()
		{
			this->x = 0.0;
			this->y = 0.0;
		}

		Point2<T>(const Point2<T>& p)
		{
			this->x = p.x;
			this->y = p.y;
		}

		Point2<T> operator = (const Point2<T>& p)
		{
			this->x = p.x;
			this->y = p.y;

			return *this;
		}

		Point2<T> operator - (const Point2<T>& p) const
		{
			Point2<T> newP;
			newP.x = this->x - p.x;
			newP.y = this->y - p.y;
			return newP;
		}

		Point2<T> operator + (const Point2<T>& p) const
		{
			Point2<T> newP;
			newP.x = this->x + p.x;
			newP.y = this->y + p.y;
			return newP;
		}

		Point2<T> operator - () const
		{
			Point2<T> newP;
			newP.x = -this->x;
			newP.y = -this->y;
			return newP;
		}
	};

	//using Point2f = Point2<float>;
	class Point2f
	{
	public:
		float x;
		float y;
	};

	struct Size
	{
		int w;
		int h;
	};

	class Line2D
	{
	public:
		geo::Point2f s;
		geo::Point2f e;
	};
}

enum class DESCRIPTOR { SURF = 0, SIFT = 1, AKAZE = 2, ORB = 3 };
enum class MATCHER { BF = 0/*Brute force with hamming distance*/, FLANN = 1 };

const double akaze_thresh = 3e-4; // AKAZE detection threshold set to locate about 1000 keypoints
const double ransac_thresh = 2.5f; // RANSAC inlier threshold
const double nn_match_ratio = 0.45f; // Nearest-neighbor matching ratio
const int bb_min_inliers = 100; // Minimal number of inliers to draw bounding box
const int stats_update_period = 10; // On-screen statistics are updated every 10 frames
const unsigned int numDividFactor = 3; // number of sub-sections

struct Stats
{
	int matches;
	int inliers;
	double ratio;
	int keypoints;
	double fps;

	Stats() : matches(0),
		inliers(0),
		ratio(0),
		keypoints(0),
		fps(0.)
	{}

	Stats& operator+=(const Stats& op)
	{
		matches += op.matches;
		inliers += op.inliers;
		ratio += op.ratio;
		keypoints += op.keypoints;
		fps += op.fps;
		return *this;
	}

	Stats& operator/=(int num)
	{
		matches /= num;
		inliers /= num;
		ratio /= num;
		keypoints /= num;
		fps /= num;
		return *this;
	}
};