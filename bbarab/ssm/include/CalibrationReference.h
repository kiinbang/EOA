#pragma once

#include <fstream>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>

#include <ssm/include/Define.h>

namespace data
{
	const float fullFrameSize_w = 36.0;
	const float fullFrameSize_h = 24.0;

	const float a4h = 297.0f; /// mm
	const float a4w = 210.0f; /// mm

	const std::string _xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	const std::string _object_points_s = "<object_points unit=\"m\">";
	const std::string _object_points_e = "</object_points>";
	const std::string _point_s = "<point>";
	const std::string _point_e = "</point>";
	const std::string _point_id_s = "<point_id>";
	const std::string _point_id_e = "</point_id>";
	const std::string _objxyz_s = "<xyz>";
	const std::string _objxyz_e = "</xyz>";
	const std::string _variance_s = "<variance>";
	const std::string _variance_e = "</variance>";
	const std::string _fixed_variance = "1.0e-12 0.0 0.0 0.0 1.0e-12 0.0 0.0 0.0 1.0e-12";
	const std::string _coordinates_s = "<coordinates>";
	const std::string _coordinates_e = "</coordinates>";
	const std::string _cameras_s = "<cameras>";
	const std::string _cameras_e = "</cameras>";
	const std::string _camera_s = "<camera type=\"frame\">";
	const std::string _camera_e = "</camera>";
	const std::string _camera_id_s = "<camera_id>";
	const std::string _camera_id_e = "</camera_id>";
	const std::string _focal_length_s = "<focal_length unit=\"mm\">";
	const std::string _focal_length_e = "</focal_length>";
	const std::string _value_s = "<value>";
	const std::string _value_e = "</value>";
	const std::string _principla_point_s = "<principal_point unit = \"mm\" type = \"photo_coordinates\">";
	const std::string _principla_point_e = "</principal_point>";
	const std::string _pixel_pitch_s = "<pixel_pitch unit = \"mm\">";
	const std::string _pixel_pitch_e = "</pixel_pitch>";
	const std::string _sensor_size_s = "<sensor_size>";
	const std::string _sensor_size_e = "</sensor_size>";
	const std::string _lens_distortion_s = "<lens_distortion model_name=\"smac\">";
	const std::string _lens_distortion_e = "</lens_distortion>";
	const std::string _description = "<description>[k0, k1, k2, k3, p1, p2, p3] dr = k0 + k1*r^3 + k2^r5 + k3^r^7) dx_tan = (1 + p3^2*r^2)(p1(r^2 + 2x^2) + 2p2*xy) dy_tan = (1 + p3^2*r^2)(p2(r^2 + 2y^2) + 2p1*xy)</description>";
	const std::string _parameter_s = "<parameter>";
	const std::string _parameter_e = "</parameter>";
	const std::string _alignment_s = "<alignment>";
	const std::string _alignment_e = "</alignment>";
	const std::string _offset_s = "<offset unit=\"m\">";
	const std::string _offset_e = "</offset>";
	const std::string _boresight_s = "<boresight unit=\"degrees\">";
	const std::string _boresight_e = "</boresight>";
	const std::string _image_observation_s = "<image_observation>";
	const std::string _image_observation_e = "</image_observation>";
	const std::string _image_s1 = "<image path=\"";
	const std::string _image_s2 = "\">";
	const std::string _image_e = "</image>";
	const std::string _image_id_s = "<image_id>";
	const std::string _image_id_e = "</image_id>";
	const std::string _time_s = "<time>";
	const std::string _time_e = "</time>";
	const std::string _eop_s = "<eop>";
	const std::string _eop_e = "</eop>";
	const std::string _eopxyz_s = "<xyz unit=\"m\">";
	const std::string _eopxyz_e = "</xyz>";
	const std::string _opk_s = "<opk unit=\"degrees\">";
	const std::string _opk_e = "</opk>";
	const std::string _points_s = "<points unit=\"mm\" type=\"photo_coordinates\">";
	const std::string _points_e = "</points>";

	struct IMAGEINFO
	{
		std::string imgPath;
		unsigned int imgId;
		unsigned int cameraId;
		std::string name;
		double f;
		double pitch;
		double xp = 0.0, yp = 0.0;
		double smac[7] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		double boresight[3] = { 0.0, 0.0, 0.0 };
		double leverarm[3] = { 0.0, 0.0, 0.0 };
		unsigned int sizeW, sizeH;
		std::string timeStamp;
		unsigned int orientation;
	};

	std::string getOrientation(const unsigned int orientation);

	double getPixelPitch(const unsigned int imgWidth, const unsigned int imgHeight, const double f35mm);

	void getCamerInfo(const std::string& jpgPath,
		std::string& timeStamp,
		std::string& camModel,
		double& f,
		double& f35mm,
		double& pixPitch,
		double& pixPitch35mm,
		unsigned int& imgW,
		unsigned int& imgH,
		unsigned int& orientation);
	
	geo::Point2f getPhotoCoordinate(const geo::Point2f& imgCoord, const float resW, const float resH, const geo::Point2f& center);

	/// Rotate displayed image coordinates to encoded image coordinates
	geo::Point2f getEncodedImageCoordinates(const geo::Point2f& imgCoord, const unsigned int orientation, const unsigned int width, const unsigned int height);

	/// Rotate displayed image coordinates to encoded image coordinates
	std::vector<geo::Point2f> getEncodedImageCoordinates(const std::vector<geo::Point2f>& imgCoord, const unsigned int orientation, const unsigned int width, const unsigned int height);

	float calInitialKappa(const std::vector<geo::Point2f>& centers, const std::vector<geo::Point2f>& centersInRed, const std::vector<float>& refRedAreaOrientation, const int imgOrientation, const int sensorW, const int sensorH);

	class CalibrationReference
	{
	public:
		CalibrationReference(const int sizeW, const int sizeH, const float boardSizeW, const float boardSizeH);

		bool writeRefBoardTargets(const std::vector<geo::Point2f>& refTargets, double& diagDist, const std::string& outPath);

	private:
		const int width;
		const int height;
		float resW;
		float resH;
		geo::Point2f center;
	};

	class CalibrationData
	{
	public:
		CalibrationData(const std::string& folderPath);

		bool openCameraFile();

		void addCameraInfo(const IMAGEINFO& cam, const unsigned int id);

		void closeCameraFile();

		bool openPhoFile();

		geo::Point2f getCenter(const std::vector<geo::Point2f>& phoObs);

		void reverse(std::vector<geo::Point2f>& photoPts);

		std::vector<geo::Point2f> calTargetPhotoCoordinates(const IMAGEINFO& imgInfo, const std::vector<geo::Point2f>& imgObs, const float kappa0, const double refDiagDist);

		void calTargetPhotoCoordinates_Old(const IMAGEINFO& imgInfo, const std::vector<geo::Point2f>& imgObs, geo::Point2f& imgCtRed, const double refDiagDist);

		void closePhoFile();

		std::string getCamFilePath() { return camFilePath; }
		std::string getPhoFilePath() { return phoFilePath; }

	private:
		const std::string folder;
		std::string camFilePath;
		std::string phoFilePath;
		std::fstream camFile;
		std::fstream phoFile;
	};

	std::string calPrjWriter(const std::string& folderPath, const std::string& phopath, const std::string& objpath, const std::string& campath);
	
	bool exportFoundTargets(const std::vector<geo::Point2f>& targets, const std::string& outPath);
}