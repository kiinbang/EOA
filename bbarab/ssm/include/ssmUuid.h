/**
* @file		ssmUuid.h
* @date		since 2020
* @author	kiinbang
* @brief	header file for uuid class
*/

#pragma once

#include <Rpc.h>
#pragma comment(lib, "Rpcrt4.lib")

//#include <ssm/include/ssmStr.h>
#include <string>

namespace util
{
	/**
	* @class	uuid
	* @date		initial version: 2020
	* @author	kiinbang
	* @brief	class for uuid
	*/
	//class __declspec (dllexport) SsmUuid
	class SsmUuid
	{
	public:
		SsmUuid();

		SsmUuid(const SsmUuid& copy);

		SsmUuid(const UUID& copy);

		void regenId();

		void operator = (const SsmUuid& copy);

		void operator = (const UUID& copy);

		bool operator == (const SsmUuid& id0) const;

		bool operator == (const UUID& id0) const;

		bool operator != (const SsmUuid& id0) const;

		bool operator != (const UUID& id0) const;

		UUID getId() const;

		std::string printIdToHex() const;

	private:
		UUID id;
	};
}