/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <memory>
#include <vector>

#include <ssm/include/primitives.h>
#include <ssm/include/SSMConstants.h>
#include <ssm/include/ssmUuid.h>
#include <ssm/include/SSMParam.h>
//#include <ssm/ssmlist.h>

#define M_PI 3.14159265358979323846   // pi

namespace data
{
	class ImagePointParam
	{
	public:
		std::vector<std::shared_ptr<param::Parameter>> coord;

	private:
		unsigned int objPtIdx;

	public:
		ImagePointParam()
		{
			coord.resize(2);

			objPtIdx = -1;

			coord[0] = std::make_shared<param::Parameter>();
			coord[0]->set(0.0);
			coord[0]->setDelta(1.0e-6);
			coord[0]->setDescription("2D image coordinate x");
			coord[0]->setInitVal(0.0);
			coord[0]->setName(constant::photo::_xi_);
			coord[0]->setStDev(0.0);

			coord[1] = std::make_shared<param::Parameter>();
			coord[1]->set(0.0);
			coord[1]->setDelta(1.0e-6);
			coord[1]->setDescription("2D image coordinate y");
			coord[1]->setInitVal(0.0);
			coord[1]->setName(constant::photo::_yi_);
			coord[1]->setStDev(0.0);
		}

		ImagePointParam(const ImagePointParam& newVal)
		{
			coord.resize(2);
			coord[0] = std::make_shared<param::Parameter>();
			coord[1] = std::make_shared<param::Parameter>();

			copy(newVal);
		}

		unsigned int getObjPtId() const { return objPtIdx; }
		void setObjPtIndex(const unsigned int idx) { objPtIdx = idx; }

		double& operator () (const unsigned int r)
		{
			if (r == 0 || r == 1)
				return this->coord[r]->get();
			else
			{
#ifdef _DEBUG
				std::cout << "Error: wrong index in ImagePoint::operator(index)" << std::endl;
#endif
				return this->coord[0]->get();
			}
		}

		double val(const unsigned int r) const
		{
			if (r == 0 || r == 1)
				return this->coord[r]->get();
			else
			{
#ifdef _DEBUG
				std::cout << "Error: wrong index in ImagePoint::operator(index)" << std::endl;
#endif
				return this->coord[0]->get();
			}
		}

		ImagePointParam operator = (const ImagePointParam& newVal)
		{
			copy(newVal);
			return *this;
		}

		void copy(const ImagePointParam& newVal)
		{
			*(this->coord[0]) = *(newVal.coord[0]);
			*(this->coord[1]) = *(newVal.coord[1]);
			this->objPtIdx = newVal.objPtIdx;
		}
	};

	class ObjectPointParam
	{
	public:
		std::vector<std::shared_ptr<param::Parameter>> coord;
		unsigned int id;

	public:
		ObjectPointParam()
		{
			id = 0;

			coord.resize(3);

			coord[0] = std::make_shared<param::Parameter>();
			coord[0]->set(0.0);
			coord[0]->setDelta(1.0e-6);
			coord[0]->setDescription("3D image coordinate X");
			coord[0]->setInitVal(0.0);
			coord[0]->setName(constant::object::_Xa_);
			coord[0]->setStDev(0.0);

			coord[1] = std::make_shared<param::Parameter>();
			coord[1]->set(0.0);
			coord[1]->setDelta(1.0e-6);
			coord[1]->setDescription("3D image coordinate Y");
			coord[1]->setInitVal(0.0);
			coord[1]->setName(constant::object::_Ya_);
			coord[1]->setStDev(0.0);

			coord[2] = std::make_shared<param::Parameter>();
			coord[2]->set(0.0);
			coord[2]->setDelta(1.0e-6);
			coord[2]->setDescription("3D image coordinate Y");
			coord[2]->setInitVal(0.0);
			coord[2]->setName(constant::object::_Za_);
			coord[2]->setStDev(0.0);
		}

		ObjectPointParam(const ObjectPointParam& copy)
		{
			operator = (copy);
		}

		double& operator () (const unsigned int r)
		{
			if (r == 0 || r == 1 || r == 2)
				return this->coord[r]->get();
			else
			{
#ifdef _DEBUG
				std::cout << "Error: wrong index in ObjectPoint::operator(index)" << std::endl;
#endif
				return this->coord[0]->get();
			}
		}

		double val(const unsigned int r) const
		{
			if (r == 0 || r == 1 || r == 2)
				return this->coord[r]->get();
			else
			{
#ifdef _DEBUG
				std::cout << "Error: wrong index in ImagePoint::operator(index)" << std::endl;
#endif
				return this->coord[0]->get();
			}
		}

		ObjectPointParam operator = (const ObjectPointParam& copy)
		{
			*(this->coord[0]) = *(copy.coord[0]);
			*(this->coord[1]) = *(copy.coord[1]);
			*(this->coord[2]) = *(copy.coord[2]);
			this->id = copy.id;

			return *this;
		}
	};

	struct ImagePointData
	{
		unsigned int imgId;
		/// point coordinate unit
		std::wstring pointsUnit;
		/// coordinate type
		std::wstring coordinateType;
		/// points
		std::vector<ImagePointParam> pts;
	};

	struct ObjectPoint
	{
		Point3D val;
		Point3D sd;
		unsigned int id;

		ObjectPoint() 
		{
			id = 0;
		}

		ObjectPoint(const ObjectPoint& newVal)
		{
			copy(newVal);
		}

		ObjectPoint(const ObjectPointParam& param)
		{
			copy(param);
		}

		ObjectPoint operator = (const ObjectPoint& newVal)
		{
			copy(newVal);
			return *this;
		}

		ObjectPoint operator = (const ObjectPointParam& param)
		{
			copy(param);
			return *this;
		}

		void copy(const ObjectPoint& copy)
		{
			this->val = copy.val;
			this->sd = copy.sd;
			this->id = copy.id;
		}

		void copy(const ObjectPointParam& param)
		{
			val(0) = param.val(0);
			val(1) = param.val(1);
			val(2) = param.val(2);

			sd(0) = param.coord[0]->getStDev();
			sd(1) = param.coord[1]->getStDev();
			sd(2) = param.coord[2]->getStDev();

			this->id = param.id;
		}

		double& operator () (const unsigned int r)
		{
			return val(r);
		}
	};

	using ObjectPoints = std::vector<ObjectPoint>;

	using TiePoint = std::pair<ImagePointParam&, ImagePointParam&>;

	struct NormalizationParameters
	{
		/// Normalization
		double scaleX = 1.0;
		double scaleY = 1.0;
		double scaleZ = 1.0;
		double scaleCol = 1.0;
		double scaleRow = 1.0;

		double shiftX = 0.0;
		double shiftY = 0.0;
		double shiftZ = 0.0;
		double shiftCol = 0.0;
		double shiftRow = 0.0;
	};

	template<typename T, typename S>
	struct Pair
	{
		T val0;
		T val1;
	};
}

namespace math
{
	using RotationMatrix = data::RotationMatrix;
}
