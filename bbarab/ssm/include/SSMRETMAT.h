/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <vector>

#include <ssm/include/SSMMatrixd.h>

namespace ssm
{
	/**@class RetMat
	*@brief class for return data of LS
	*/
	//class __declspec (dllexport) RetMat
	class RetMat
	{
	public:
		/**<Constructor */
		RetMat() 
		{
			sd = 0.0;
			variance = 0.;
			maxCorrection = 0.;
		}

		math::Matrixd A; /**<Design matrix */
		math::Matrixd L; /**<Observation matrix */
		math::Matrixd W; /**<Weight matrix */
		math::Matrixd AT; /**<A transpose matrix */
		math::Matrixd N; /**<Normal matrix */
		math::Matrixd Ninv; /**<Normal inverse matrix */
		math::Matrixd VTV; /**<Sum of residual square*/
		math::Matrixd X; /**<Unknown matrix */
		math::Matrixd V; /**<Residual matrix */
		math::Matrixd XVarCov; /**<Variance-covariance matrix of unknown parameters */
		math::Matrixd LVarCov; /**<Variance-covariance matrix of observation */
		math::Matrixd LVar; /**<Variance matrix of observation */
		math::Matrixd XCorrelation; /**<Correlation matrix of unknown parameters */
		math::Matrixd VCorrelation; /**<Correlation matrix of observations */
		double sd=0.0; /**<Standard deviation */
		double variance=0.; /**<Variance */
		double maxCorrection=0.;
	};
}