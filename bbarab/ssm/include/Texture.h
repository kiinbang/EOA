/*
* Copyright(c) 2019-2021 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <array>
#include <limits>
#include <memory.h>
#include <string>
#include <vector>

#include <ssm/include/SSMPhoto.h>

#include <tinyobj/ObjData.h>

namespace texture
{
	struct CamInfo
	{
		float f;
		float xp, yp;
		float pitch;
	};

	struct PhoInfo
	{
		float o, p, k;
		float X, Y, Z;
	};

	using VerticesXY = std::array<data::Point2D, 3>;
	using VerticesUV = std::array<data::Point2D, 3>; /// texture coordinate u, v (0 to 1) right hand coordinate system
	using VerticesCR = std::array<data::Point2D, 3>; /// texture coordinate col, row (0 to width and 0 to height) left hand coordinate system

	struct TextureInfo
	{
		std::string imgName;
		size_t faceIdx;
		VerticesUV verticesUV;
		VerticesCR verticesCR;
		bool availability;
	};

	class Texture
	{
	public:
		Texture();

		bool buildTexture(const std::vector<tobj::Face>& faces,
			const std::vector<tobj::Normal>& normals,
			const std::vector<std::string>& imgNames,
			const std::vector<std::shared_ptr<data::EOP>>& eops,
			const std::vector<std::shared_ptr<sensor::Camera>>& cameras,
			const double maxRes);

		const std::vector<TextureInfo>& getTexture();

	private:
		void buildTexture();

		bool findImages(const std::vector<data::Point3D>& face, const tobj::Normal& normal, const double maxRes, TextureInfo& textureInfo);

		double computeRes(const double dist0, const double incidenceAngle, const double dTheta);

	private:
		std::vector<std::string> imgNames;
		std::vector<tobj::Face> faces;
		std::vector<tobj::Vertex> normals;
		std::vector<TextureInfo> textureInfo;
		std::vector<std::shared_ptr<data::EOP>> eops;
		std::vector<data::Point3D> pcs;
		std::vector<data::RotationMatrix> rotBsM;
		std::vector<data::RotationMatrix> rotBodyM;
		std::vector<std::shared_ptr<sensor::Camera>> cameras;
		double maxRes; /// max texture image resolution
	};

	/// Get faces from an obj file
	std::vector<tobj::Face> getFaces(const std::string& objPath);

	/// Get faces from an obj file
	std::vector<tobj::Face> getFaces(const std::string& objPath, const std::string& metaPath);

	/// Create TextureInfo interface using class BasicTexture
	//std::shared_ptr<TextureInterface> createBasicTexture();

	/// Create a texture info file
	bool exportTextureInfo(const std::wstring& texInfoPath, const std::vector<TextureInfo>& textureInfo);
}
