/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/SSMParam.h>
#include <SSM/include/SSMCamera.h>

using namespace constant;

namespace sensor
{
	/// Number of camera parameters: f, xp, yp, w, h, pixel-pitch, lever-arm(3), boresight(3)
	const unsigned int NUM_CAM_PARAM = 12;
	const unsigned fIdx = 0;
	const unsigned xpIdx = 1;
	const unsigned ypIdx = 2;
	const unsigned wIdx = 3;
	const unsigned hIdx = 4;
	const unsigned pitIdx = 5;
	const unsigned bsoIdx = 6;///Boresight omega
	const unsigned bspIdx = 7;///Boresight phi
	const unsigned bskIdx = 8;///Boresight kappa
	const unsigned dxIdx = 9;///Leverarm dX
	const unsigned dyIdx = 10;///Leverarm dY
	const unsigned dzIdx = 11;///Leverarm dZ
	
	/// 1.0e-6
	const double pixTolerance = 0.000001;

	/** FrameCamera
	* @brief: FrameCamera, implementation of the Camera interface<br>
	* It is a class for a common 2D frame sensor camera.
	*/
	class FrameCamera : public Camera
	{
	public:
		FrameCamera();

		FrameCamera(const unsigned int cameraId,
			const double focalLength,
			const data::Point2D& xpyp,
			const unsigned int w,
			const unsigned int h,
			const double pixelPitch,
			std::shared_ptr<DistortionModel> distortion);

		FrameCamera(const std::vector<std::shared_ptr<param::Parameter>> params, const std::shared_ptr<DistortionModel> distortion);

		FrameCamera(const std::vector<std::shared_ptr<param::Parameter>>& parameters);

	// Inherited via Camera
	public:
		/**getRefinedPhotoCoord
		*@brief Get the refined(undistorted) photo coordinates from the distorted photo coordinates.
		*@param const data::Point2D& distortedPhotoCoord : distorted photo coordinates
		*@return data::Point2D : refined photo coordinates
		*/
		data::Point2D getRefinedPhotoCoord(const data::Point2D& distortedPhotoCoord) const override;

		/**getRefinedPhotoCoord
		*@brief Get the refined(undistorted) photo coordinates from the distorted photo coordinates.
		*@param const std::vector<std::shared_ptr<param::Parameter>>& distortedPhotoCoord : distorted photo coordinates
		*@return data::Point2D : refined photo coordinates
		*/
		data::Point2D getRefinedPhotoCoord(const std::vector<std::shared_ptr<param::Parameter>>& distortedPhotoCoord) const override;

		/**getPhoto2ImgCoord
		*@brief Get image coordinates from photo coordinates
		*@param const data::Point2D& photoCoord : photo coordinates
		*@return data::Point2D : image coordinates
		*/
		data::Point2D getPhoto2ImgCoord(const data::Point2D& photoCoord) const override;

		/**getPhoto2ImgCoord
		*@brief Get image coordinates from photo coordinates
		*@param const std::vector<param::Parameter>& photoCoord
		*@return data::Point2D : image coordinates
		*/
		data::Point2D getPhoto2ImgCoord(const std::vector<std::shared_ptr<param::Parameter>>& photoCoord) const override;

		/**getImg2PhotoCoord
		*@brief Get photo coordinates from image coordinates
		*@param const data::Point2D& imgCoord : image coordinates
		*@return data::Point2D : photo coordinates
		*/
		data::Point2D getImg2PhotoCoord(const data::Point2D& imgCoord) const override;

		/**getRefinedPhotoCoordFromImgCoord
		*@brief Transform distorted image coordinates to get refined(undistorted) photo coordinates.
		*@param const data::Point2D& distortedImgCoord : distorted image coordinates
		*@return data::Point2D : refined photo coordinates
		*/
		data::Point2D getRefinedPhotoCoordFromImgCoord(const data::Point2D& distortedImgCoord) const override;

		/**getDistortedPhotoCoordFromRefinedPhotoCoord
		*@brief Get the distorted photo coordinates from the refined(undistorted) photo coordinates.
		*@param const data::Point2D& refinedPhotoCoord : refined image coordinates
		*@return data::Point2D : distorted photo coordinates
		*/
		data::Point2D getDistortedPhotoCoordFromRefinedPhotoCoord(const data::Point2D& refinedPhotoCoord) const override;

		/**getDistortedImgCoordFromRefinedPhotoCoord
		*@brief Get the distorted image coordinates from the refined(undistorted) photo coordinates.
		*@param const data::Point2D& refinedPhotoCoord : refined photo coordinates
		*@return data::Point2D : distorted image coordinates
		*/
		data::Point2D getDistortedImgCoordFromRefinedPhotoCoord(const data::Point2D& refinedPhotoCoord) const override;

		/**getDistortedImgCoordFromRefinedPhotoCoord
		*@brief Get the distorted image coordinates from the refined(undistorted) photo coordinates.
		*@param const std::vector<data::Point2D>& refinedPhotoCoord : refined photo coordinates
		*@return std::vector<data::Point2D> : distorted image coordinates
		*/
		std::vector<data::Point2D> getDistortedImgCoordFromRefinedPhotoCoord(const std::vector<data::Point2D>& refinedPhotoCoord) const override;

	public:
		/**getFL
		*@brief Get a focal length
		*@return double : focal length
		*/
		double getFL() const override;

		/**setFL
		*@brief Set a focal length
		*@param double : focal length
		*@return void
		*/
		void setFL(const double focalLength) override;

		/**setFL
		*@brief Set a focal length
		*@param parameter : focal length
		*@return void
		*/
		void setFL(const param::Parameter& focalLength) override;

		/**getPP
		*@brief Get a principal point(pp)
		*@return data::Point2D : principal point(pp)
		*/
		data::Point2D getPP() const override;

		/**setPP
		*@brief Set a principal point(pp)
		*@param double : principal point(pp)
		*@return void
		*/
		void setPP(const data::Point2D& principalPoint) override;

		/**setPP
		*@brief Set a principal point(pp)
		*@param parameter : principal point
		*@return void
		*/
		void setPP(const std::vector<param::Parameter>& principalPoint) override;

		/**getSensorSize
		*@brief Get a sensor size (width and height)
		*@return math::VectorMat<unsigned int, 2> : sensor width and height
		*/
		math::VectorMat<unsigned int, 2> getSensorSize()  const override;

		/**setSensorSize
		*@brief Set a sensor size (width and height)
		*@param const unsigned int width : width
		*@param const unsigned int height : height
		*@return void
		*/
		void setSensorSize(const unsigned int width, const unsigned int height) override;

		/**setSensorSize
		*@brief Set a sensor size (width and height)
		*@param const Parameter : width
		*@param const Parameter : height
		*@return void
		*/
		void setSensorSize(const param::Parameter width, const param::Parameter height) override;

		/**getPixPitch
		*@brief Get a pixel pitch (pixel interval size)
		*@return double : pixel pitch
		*/
		double getPixPitch() const override;

		/**setPixPitch
		*@brief Set a pixel pitch (pixel interval size)
		*@param const double pixelPitch: pixel pitch
		*@return void
		*/
		void setPixPitch(const double pixelPitch) override;

		/**setPixPitch
		*@brief Set a pixel pitch (pixel interval size)
		*@param const parameter pixelPitch: pixel pitch
		*@return void
		*/
		void setPixPitch(const param::Parameter& pixelPitch) override;

		/**getLeverArm
		*@brief Get lever-arm (dx, dy, dz)
		*@return Point3D: lever-arm
		*/
		data::Point3D getLeverArm() const override;

		/**setLeverArm
		*@brief Set lever-arm
		*@param Point3D: lever-arm (dx, dy, dz)
		*@return void
		*/
		void setLeverArm(const data::Point3D& la) override ;

		/**setLeverArm
		*@brief Set lever-arm
		*@param const std::vector<param::ParameterPtr>& la: lever-arm (dx, dy, dz)
		*@return void
		*/
		void setLeverArm(const std::vector<param::ParameterPtr>& la) override;

		/**getBoresight
		*@brief Get boresight
		*@return EulerAngles (omega, phi, kappa)
		*/
		data::EulerAngles getBoresight() const override ;

		/**setBoresight
		*@brief Set boresight
		*@param EulerAngles (omega, phi, kappa)
		*@return void
		*/
		void setBoresight(const data::EulerAngles& bs) override;

		/**setBoresight
		*@brief Set boresight
		*@param std::vector<param::ParameterPtr>& bs: omega, phi and kappa
		*@return void
		*/
		void setBoresight(const std::vector<param::ParameterPtr>& bs) override;

		/**getDistortionModel
		*@brief Get a distortion model
		*@return sensor::DistortionModel : distortion model
		*/
		std::shared_ptr<sensor::DistortionModel> getDistortionModel() const override;

		/**setDistortionModel
		*@brief Set a distortion model
		*@param const sensor::DistortionModel : distortion model
		*@return void
		*/
		void setDistortionModel(const std::shared_ptr<sensor::DistortionModel> newDistModel) override;

		/**getParameters
		*@brief get all parameters (camera, alignment and distortion)
		*@return std::vector<std::shared_ptr<param::Parameter>>
		*/
		std::vector<std::shared_ptr<param::Parameter>> getParameters() override;

		/**setParameters
		*@brief set all parameters (camera, alignment and distortion)
		*@param const std::vector<std::shared_ptr<param::Parameter>>& : new parameters
		*/
		void setParameters(const std::vector<std::shared_ptr<param::Parameter>>& collection) override;

		/**getCamParameters
		*@brief get camera parameters
		*@return std::vector<std::shared_ptr<param::Parameter>>&
		*/
		std::vector<std::shared_ptr<param::Parameter>>& getCamParameters() override;

		/**getDistParameters
		*@brief get distortion parameters
		*@return std::vector<std::shared_ptr<param::Parameter>>&
		*/
		std::vector<std::shared_ptr<param::Parameter>>& getDistParameters() override;

		/**getId
		*@brief get a camera id
		*@return unsigned int id
		*/
		unsigned int getId() const override;

		/**setId
		*@brief set a camera id
		*@param unsigned int id
		*/
		void setId(unsigned int id) override;

	// Static functions
	public:
		/**
		*@brief Get the distorted image coordinates from the refined(undistorted) photo coordinates.<br>
		*@Note for future works:<br>
		*@It supports only smac now, and this function does not belong to interface functions.<br>
		*@For more generalization, it should be modified for accepting camera and distortion interfaces instead of instances.
		*@param parameters for distortion
		*@param Point2D pp: xp and yp
		*@param unsigned int width
		*@param unsigned int height
		*@param double pixPitch: pixel pitch 
		*@param refinedPhotoCoord0: refined photo coordinates as input data
		*@return distorted image coordinates
		*/
		static data::Point2D getDistortedImgCoordFromRefinedPhotoCoord(const std::vector<std::shared_ptr<param::Parameter>>& distParams,
			const data::Point2D& pp,
			unsigned int width,
			unsigned int height,
			const double pixPitch,
			const data::Point2D& refinedPhotoCoord0);

		/**
		*@brief Get the distorted image coordinates from the refined(undistorted) photo coordinates.<br>
		*@Note for future works:<br>
		*@It supports only smac now, and this function does not belong to interface functions.<br>
		*@For more generalization, it should be modified for accepting camera and distortion interfaces instead of instances.
		*@param camParams: parameter collections for camera parameters
		*@param disParams: parameter collections for distortion parameters
		*@param refinedPhotoCoord0: refined photo coordinates as input data
		*@return distorted image coordinates
		*/
		static data::Point2D getDistortedImgCoordFromRefinedPhotoCoord(const std::vector<std::shared_ptr<param::Parameter>>& camParams,
			const std::vector<std::shared_ptr<param::Parameter>>& disParams,
			const data::Point2D& refinedPhotoCoord0);

		/**
		*@brief Get the distorted image coordinates from the refined(undistorted) photo coordinates.<br>
		*@Note for future works:<br>
		*@It supports only smac now, and this function does not belong to interface functions.<br>
		*@For more generalization, it should be modified for accepting camera and distortion interfaces instead of instances.
		*@param parameters: parameter array
		*@param refinedPhotoCoord0: refined photo coordinates as input data
		*@return distorted image coordinates
		*/
		static data::Point2D getDistortedImgCoordFromRefinedPhotoCoord(const std::vector<std::shared_ptr<param::Parameter>>& parameters, const data::Point2D& refinedPhotoCoord0);

		/**
		*@brief Get refined photo coordinates from distorted image coordinates.
		*@Note for future works:<br>
		*@It supports only smac now, and this function does not belong to interface functions.<br>
		*@For more generalization, it should be modified for accepting camera and distortion interfaces instead of instances.
		*@param collections: parameter collections for camera and distortion parameters
		*@param distortedImgCoord0: distorted (observed) image coordinates as input data
		*@return refined photo coordinates
		*/
		static data::Point2D getRefinedPhotoCoordFromDistortedImgCoord(const std::vector<std::shared_ptr<param::Parameter>>& parameters, const data::Point2D& distortedImgCoord0);

		/** 
		*@brief Get the distorted photo coordinates from the refined(undistorted) photo coordinates.
		*@param const std::shared_ptr<sensor::DistortionModel>& distModel : distion model interface
		*@param const data::Point2D& refinedPhotoCoord : refined image coordinates
		*@return data::Point2D : distorted photo coordinates
		*/
		static data::Point2D getDistortedPhotoCoordFromRefinedPhotoCoord(const std::shared_ptr<sensor::DistortionModel>& distModel, const data::Point2D& refinedPhotoCoord0);

	private:
		/** Get the x coordinate of a principal point */
		double getXp() const;
		/** Get the y coordinate of a principal point */
		double getYp() const;
		/** Set the x coordinate of a principal point */
		void setXp(const double newXp);
		/** Set the y coordinate of a principal point */
		void setYp(const double newYp);
		/** Initialize parameters*/
		void initParameters();
		/** Collect camera parameters from a parameter array*/
		void collectCameraParameters(const std::vector<std::shared_ptr<param::Parameter>>& parameters);

	private:
		/** Distortion model */
		std::shared_ptr<sensor::DistortionModel> distModel;
		/** Camera parameters */
		std::vector<std::shared_ptr<param::Parameter>> cameraParams;
		/** Distortion parameters */
		std::vector<std::shared_ptr<param::Parameter>> distParams;
		/** Camera id*/
		unsigned int id;
	};
}
