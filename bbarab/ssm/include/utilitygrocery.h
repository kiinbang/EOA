/*
* Copyright (c) 2005-2006, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include <fstream>
#include <math.h>
#include <string>
#include <vector>

using namespace std;

namespace math
{
	class Matrixd;
}

namespace util
{
	
	const unsigned int MAX_LINE_LENGTH = 512;
	const double PI = 3.14159265358979;
	
	/**GetPI
	* Description	    : to get pi(3.14159265....)
	*@param void
	*@return double : PI
	*/
	double GetPI(void);
	
	/**Rad2Deg
	* Description	    : to convert the degree to the radian
	*@param double rad
	*@return double 
	*/
	double Rad2Deg(double rad);
	
	/**Deg2Rad
	* Description	    : to convert the radian to the degree
	*@param double deg
	*@return double 
	*/
	double Deg2Rad(double deg);
	
	/**RemoveCommentLine
	* Description	    : function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@return void 
	*/
	void RemoveCommentLine(fstream &file, char delimiter);
	
	/**RemoveCommentLine
	* Description	    : function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@param in num: number of delimiters
	*@return void 
	*/
	void RemoveCommentLine(fstream &file, char* delimiter, int num);
	
	/**RemoveCommentLine
	* Description	    : function for removing comments in the input file
	*@param FILE* file : FILE struct pointer
	*@param char delimiter: delimiter
	*@return void 
	*/
	void RemoveCommentLine(FILE* file, char delimiter);
	
	/**FindNumber
	* Description	    : function for removing delimiter in the input file
	*@param fstream file : file stream
	*@return void 
	*/
	void FindNumber(fstream &file);
	
	/**FindString
	* Description	    : function for finding given string in the input file
	*@param fstream file : file stream
	*@param char* target : target string
	*@return void 
	*/
	bool FindString(fstream &fin, char target[], int &result);
	
	/**FindString
	* Description	    : checking version of the given text file
	*@param fstream file : file stream
	*@return float: version number 
	*/
	float VersionCheck(fstream &fin);
	
	/**CalAzimuth
	* Description	    : calculate azimuth with 2 points
	*@param double Xa	: X coordinate
	*@param double Ya	: Y coordinate
	*@param double Xb	: X coordinate
	*@param double Yb	: Y coordinate
	*@return double: azimuth
	*/
	double CalAzimuth(double Xa, double Ya, double Xb, double Yb);
	
	/**MeridianConversion
	* Description	    : calculate azimuth with 2 points
	*@param double Lon	: longitude
	*@param double Lat	: latitude
	*@param double CenterLon	: longitude of center meridian
	*@return double: meridian conversion
	*/
	double MeridianConversion(double Lon, double Lat, double CenterLon);
	
	/**CentralMeridian
	* Description	    : calculate azimuth with 2 points
	*@param int zone	: UTM zone
	*@return double: central meridian (rad)
	*/
	double CentralMeridian(int zone);
	
	/**Line2DIntersect
	* Description	    :  find intersect point between two lines
	*@param double x1, y1 : given point of line
	*@param double x2, y2 : given point of line
	*@param double x3, y3 : given point of line
	*@param double x4, y4 : given point of line
	*@param double &x, &y : intersection point
	*@return int : -3 (parallel) -2(same) -1 (Not line) 0 (out of ranges) 1(intersection within ranges) 2 (end point)
	*/
	int Line2DIntersect(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double &x, double &y);
	
	/**Inside2DPolygon
	* Description	    :  topology between line and point
	*@param double *x, *y	: vertices
	*@param double n		  : number of vertices
	*@param double Tx, Ty	: target point
	*@return int	: -1 (not polygon) 0 (outside) 1 (inside)
	*/
	int Inside2DPolygon(double *x, double* y, int n, double Tx, double Ty);

	/**Inside2DPolygon
	* Description	    :  topology between line and point
	*@param double x, y	: vertices
	*@param double tx, ty	: target point
	*@return int	: -1 (not polygon) 0 (outside) 1 (inside)
	*/
	int Inside2DPolygon(const std::vector<double>& x, const std::vector<double>& y, const double tx, const double ty);
	
	/**IntersectPlaneLine
	* Description	    :  intersection between a line and a plane
	*@param double *N: normal vector of a plane
	*@param double *V: a point on a plane
	*@param double *P1, P2: two point on a line
	*@param double *P: intersection point
	*@return bool: [false] if the intersection point is out of two point or line is parallel to the plane, then return false
	*/
	bool IntersectPlaneLine(double *N, double *V, double *P1, double *P2, double *P);

	/**NEW IntersectPlaneLine
	* @brief : find a intersection point of a plane and a line
	* @param double* N : normal vector of a plane
	* @param double* V : a point on the plane
	* @param double* P1 : the firat point on the line
	* @param double* P2 : the second point on the line
	* @param double* P : computed intersection point
	* @param double& u : scale factor of a intersection line, u < 0 : negative direction, 0 < u < 1 : the intersection point locates between two points
	* @return bool : [FALSE] line is parallel to the plane then return false
	*/
	bool IntersectPlaneLine(double* N, double* V, double* P1, double* P2, double* P, double& u);

	/**IntersectPlaneLine
	* @brief : find a intersection point of a plane and a line
	* @param double A0 : it is the first parameter of parameters of a plane, A0*X + A1*Y + A2*Z = 1.0
	* @param double A1 : it is the second parameter of parameters of a plane, A0*X + A1*Y + A2*Z = 1.0
	* @param double A2 : it is the third parameter of parameters of a plane, A0*X + A1*Y + A2*Z = 1.0
	* @param double* P1 : the firat point on the line
	* @param double* P2 : the second point on the line
	* @param double* P : computed intersection point
	* @return bool: [false] if the intersection point is out of two point or line is parallel to the plane, then return false
	*/
	bool IntersectPlaneLine(double A0, double A1, double A2, double* P1, double* P2, double* P);

	/**IntersectPlaneLine
	* @brief : find a intersection point of a plane and a line
	* @param double A0 : it is the first parameter of parameters of a plane, A0*X + A1*Y + A2*Z = 1.0
	* @param double A1 : it is the second parameter of parameters of a plane, A0*X + A1*Y + A2*Z = 1.0
	* @param double A2 : it is the third parameter of parameters of a plane, A0*X + A1*Y + A2*Z = 1.0
	* @param double* P1 : the firat point on the line
	* @param double* P2 : the second point on the line
	* @param double* P : computed intersection point
	* @param double& u : scale factor of a intersection line, u < 0 : negative direction, 0 < u < 1 : the intersection point locates between two points
	* @return bool : [FALSE] line is parallel to the plane then return false
	*/
	bool IntersectPlaneLine(double A0, double A1, double A2, double* P1, double* P2, double* P, double& u);

	/**IntersectPointBetweenOnePointandOneLine
	* Description	    :  intersection between a line and a point
	*@param double *P0: given one point
	*@param double *PA, *PB: two end points of a line
	*@param double *P: intersection of P0 and a line
	*@return bool: false and true
	*/
	bool IntersectPointBetweenOnePointandOneLine(double *P0, double *Pa, double *Pb, double *P);

	/**IntersectPointBetweenOnePointandOneLine
	* @brief : find a intersection point of a point and a line
	* @param[in] const double p0x : it is a given point x coordinate
	* @param[in] const double p0y : it is a given point y coordiante
	* @param[in] const double p0z : it is a given point z coordinate
	* @param[in] const double pax : it is a x doordinate of the point A of the given line
	* @param[in] const double pay : it is a y doordinate of the point A of the given line
	* @param[in] const double paz : it is a z doordinate of the point A of the given line
	* @param[in] const double pbx : it is a x doordinate of the point B of the given line
	* @param[in] const double pby : it is a y doordinate of the point B of the given line
	* @param[in] const double pbz : it is a z doordinate of the point B of the given line
	* @param[out] double& px : it is a x doordinate of the intersection
	* @param[out] double& py : it is a y doordinate of the intersection
	* @param[out] double& pz : it is a z doordinate of the intersection
	* @param[out] double& norm : it is a normal distance between the point and the line
	* @return bool
	*/
	bool IntersectPointBetweenOnePointandOneLine(const double p0x, const double p0y, const double p0z,
		const double pax, const double pay, const double paz,
		const double pbx, const double pby, const double pbz,
		double& px, double& py, double& pz,
		double& norm);

	/**IntersectPointBetweenOnePointandOneLine in 2D space
	* @brief : find a intersection point of a point and a line
	* @param[in] const double p0x : it is a given point x coordinate
	* @param[in] const double p0y : it is a given point y coordiante
	* @param[in] const double pax : it is a x doordinate of the point A of the given line
	* @param[in] const double pay : it is a y doordinate of the point A of the given line
	* @param[in] const double pbx : it is a x doordinate of the point B of the given line
	* @param[in] const double pby : it is a y doordinate of the point B of the given line
	* @param[out] double& px : it is a x doordinate of the intersection
	* @param[out] double& py : it is a y doordinate of the intersection
	* @param[out] double& norm : it is a normal distance between the point and the line
	* @return bool
	*/
	bool IntersectPointBetweenOnePointandOneLine(const double p0x, const double p0y,
		const double pax, const double pay,
		const double pbx, const double pby,
		double& px, double& py,
		double& norm);
	
	/**Area_of_Polygon2D
	* Description	    :  area of a polygon
	*@param double *x: list of x coordinates
	*@param double *y: list of y coordinates
	*@param int n: number of vertices
	*@return double: area
	*/
	double Area_of_Polygon2D(double* x, double* y, int n);
	
	//Extract angle from normal vector
	/**Area_of_Polygon2D
	* Description	    :  area of a polygon
	*@param double *x: list of x coordinates
	*@param double *y: list of y coordinates
	*@param int n: number of vertices
	*@return double: area
	*/
	bool ExtractEulerAngles(double dX, double dY, double dZ, double &O, double &P);
	
	//Randomly select string lines from a txt file
	bool RandomTXTLIneExtraction(int numlines, char* ptspath, char* outpath);
	
	/// File extention check
	bool ExtCheck(char *path, char* ext, bool bMatchCase);
	
	/// Txt file open for reading
	bool ASCIIReadOpen(fstream &infile, char* path);
	
	//T/ xt file open for writing
	bool ASCIISaveOpen(fstream &outfile, char* path);
	
	/// Binary file open for reading
	bool BINReadOpen(fstream &infile, char* path);
	
	/// Binary file open for writing
	bool BINSaveOpen(fstream &outfile, char* path);
	
	/// Precision of txt file setting
	void ASCIIFilePrecision(fstream &file, unsigned int precision);

	/// Get file size
	long long fileLength(const string& filename);

	/// Print a debug message
	void debugMsg(std::string msg);

	/// Conduct plane fitting
	bool estimatePlane(const std::vector<double>& x, const std::vector<double>& y, const std::vector<double>& z);

	/// wstring to string
	std::string uni2multi(const std::wstring& wst);

	/// string to wstring
	std::wstring multi2uni(const std::string& mst);

	/// to upper string
	std::string toupper(const std::string& str);

	/// to lower string
	std::string tolower(const std::string& str);

	/// angle between two vectors (using a inner product)
	double angleBetweenTwoVectors(const double x1,
		const double y1,
		const double z1,
		const double x2,
		const double y2,
		const double z2);

	/// angle between two vectors (using a cross product)
	double angleBetweenTwoVectors2(const double x1,
		const double y1,
		const double z1,
		const double x2,
		const double y2,
		const double z2);

	/**getPlaneRotationMatrix
	* @brief : get rotation matrix of a normal vector of a 3D triangular patch
	* @param[in] const double (&v1)[3] : #1 vertex
	* @param[in] const double (&v2)[3] : #2 vertex
	* @param[in] const double (&v3)[3] : #3 vertex
	* @return Matrixd : rotation matrix making the normal vector of a plane a new Z' axis
	*/
	math::Matrixd getPlaneRotationMatrix(const double(&v1)[3], const double(&v2)[3], const double(&v3)[3]);

	/**getNormalDistance
	* @brief : get normal distance between 3D point and 3D plane
	* @param[in] const Matrixd& R : rotation matrix making the normal vector of a plane a new Z' axis
	* @param[in] const double (&v1)[3] : #1 vertex
	* @param[in] const double (&v2)[3] : #2 vertex
	* @param[in] const double (&v3)[3] : #3 vertex
	* @param[in] const double (&p)[3] : target point
	* @param[in] double (&q)[3] : projected point
	* @param[out] double& normDist :distance from a point to a plane
	* @return bool
	*/
	bool getNormalDistance(const math::Matrixd& R,
		const double(&v1)[3],
		const double(&v2)[3],
		const double(&v3)[3],
		const double(&p)[3],
		double(&q)[3],
		double& normDist);

	/**getNormalDistance
	* @brief : get normal distance between 3D point and 3D plane
	* @param[in] const Matrixd& R : rotation matrix making the normal vector of a plane a new Z' axis
	* @param[in] const double (&v1)[3] : #1 vertex
	* @param[in] const double (&v2)[3] : #2 vertex
	* @param[in] const double (&v3)[3] : #3 vertex
	* @param[out] const double xa :target point x coordinate
	* @param[out] const double ya :target point y coordinate
	* @param[out] const double za :target point z coordinate
	* @param[out] double& normDist :distance from a point to a plane
	* @return bool
	*/
	bool getNormalDistance(const math::Matrixd& R, 
		const double(&v1)[3], 
		const double(&v2)[3], 
		const double(&v3)[3], 
		const double xa, 
		const double ya, 
		const double za, 
		double& normDist);
	
	/**getNormalDistance
	* @brief : get normal distance between 3D point and 3D plane
	* @param[in] const double (&v1)[3] : #1 vertex
	* @param[in] const double (&v2)[3] : #2 vertex
	* @param[in] const double (&v3)[3] : #3 vertex
	* @param[in] const double (&p)[3] : target point
	* @param[in] double (&q)[3] : projected point
	* @param[out] double& normDist :distance from a point to a plane
	* @return bool
	*/
	bool getNormalDistance(const double(&v1)[3], 
		const double(&v2)[3], 
		const double(&v3)[3], 
		const double(&p)[3],
		double(&q)[3],
		double& normDist);

	/**getNormalDistance
	* @brief : get normal distance between 3D point and 3D plane
	* @param[in] const double (&v1)[3] : #1 vertex
	* @param[in] const double (&v2)[3] : #2 vertex
	* @param[in] const double (&v3)[3] : #3 vertex
	* @param[out] const double xa :target point x coordinate
	* @param[out] const double ya :target point y coordinate
	* @param[out] const double za :target point z coordinate
	* @param[out] double& normDist :distance from a point to a plane
	* @return bool
	*/
	bool getNormalDistance(const double(&v1)[3], const double(&v2)[3], const double(&v3)[3], const double xa, const double ya, const double za, double& normDist);
}