/*
* Copyright(c) 2001-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <SSM/include/SSMMatrix.h>

namespace math
{
	/**
	*@class VectorMat Class
	*@brief Class for supporting a fixed-size 1d matrix (vector)
	*/
	template<size_t d>
	class VectorMatd : public Matrixd
	{
	public:
		VectorMatd(const double val = T(0))
			: Matrixd(d, 1, val)
		{
		}

		VectorMatd(const VectorMatd<d>& mat)
			: Matrixd(d, 1, T(0))
		{
			for (unsigned int i = 0; i < d; ++i)
				this->data[i] = mat.data[i];
		}

		VectorMatd(const Matrixd& mat)
			: Matrixd(d, 1, T(0))
		{
			if (d != mat.getRows() || mat.getCols() != 1)
				throw std::runtime_error("Wrong size error from VectorMat constructor");

			for (unsigned int i = 0; i < d; ++i)
				this->data[i] = mat.data[i];
		}

		void operator = (const VectorMatd<d>& mat)
		{
			for (unsigned int i = 0; i < d; ++i)
				this->data[i] = mat.data[i];
		}

		void operator = (const Matrixd& mat)
		{
			if (d != mat.getRows() || mat.getCols() != 1)
				throw std::runtime_error("Wrong size error from VectorMat constructor");

			for (unsigned int i = 0; i < d; ++i)
				this->data[i] = mat.data[i];
		}

		// vector operations
	public:
		VectorMatd<d> cross(const VectorMatd<d>& mat) const
		{
			VectorMatd<d> result;

			for (unsigned int i = 0; i < d; i++)
			{
				unsigned int idx1, idx2;
				idx1 = static_cast<unsigned int>(fmod(i + 1, d));
				idx2 = static_cast<unsigned int>(fmod(i + 2, d));

				result(i, 0) = this->data[idx1] * mat.data[idx2] - mat.data[idx1] * this->data[idx2];
			}

			return result;
		}

		double dot(const VectorMatd<d>& mat) const
		{
			double result = 0.0;

			for (unsigned int i = 0; i < d; i++)
			{
				result += this->data[i] * mat.data[i];
			}

			return result;
		}
	};
}