/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <iostream>
#include <vector>

namespace mesh
{
    /// Vertex3D: 3D vertex data
    class Vertex3D
    {
    public:
        Vertex3D()
        {
            xyz[0] = xyz[1] = xyz[2] = 0.0;
        }

        Vertex3D(const double x, const double y, const double z)
        {
            xyz[0] = x;
            xyz[1] = y;
            xyz[2] = z;
        }

        Vertex3D(const Vertex3D& val)
        { 
            copy(val);
            faceId = val.faceId;
        }

        inline Vertex3D operator=(const Vertex3D& val)
        {
            copy(val);
            return *this;
        }

		inline Vertex3D operator+=(const Vertex3D& val)
		{
            this->xyz[0] += val.xyz[0];
            this->xyz[1] += val.xyz[1];
            this->xyz[2] += val.xyz[2];
			return *this;
		}

		inline Vertex3D operator/=(const double& val)
		{
            try
            {
                this->xyz[0] /= val;
                this->xyz[1] /= val;
                this->xyz[2] /= val;
            }
            catch (...)
            {
                throw std::runtime_error("Error from Vertex3D operator /=");
            }
			return *this;
		}

		inline Vertex3D operator-(const Vertex3D& val) const
		{
            Vertex3D retVal;
            retVal.xyz[0] = this->xyz[0] - val.xyz[0];
            retVal.xyz[1] = this->xyz[1] - val.xyz[1];
            retVal.xyz[2] = this->xyz[2] - val.xyz[2];
            return retVal;
		}

		inline Vertex3D operator+(const Vertex3D& val) const
		{
			Vertex3D retVal;
			retVal.xyz[0] = this->xyz[0] + val.xyz[0];
			retVal.xyz[1] = this->xyz[1] + val.xyz[1];
			retVal.xyz[2] = this->xyz[2] + val.xyz[2];
			return retVal;
		}

		inline Vertex3D operator/(const double val) const
		{
            Vertex3D retVal;

            try {
                retVal.xyz[0] = this->xyz[0] / val;
                retVal.xyz[1] = this->xyz[1] / val;
                retVal.xyz[2] = this->xyz[2] / val;
            }
            catch (...)
            {
                throw std::runtime_error("Fatal error from Vertex3D operator ""/""");
            }

			return retVal;
		}

		inline Vertex3D operator*(const double val) const
		{
			Vertex3D retVal;

			retVal.xyz[0] = this->xyz[0] * val;
			retVal.xyz[1] = this->xyz[1] * val;
			retVal.xyz[2] = this->xyz[2] * val;

			return retVal;
		}

        inline void copy(const Vertex3D& val)
        {
            ::memcpy(xyz, val.xyz, 3 * sizeof(double));
            faceId = val.faceId;
        }

        inline bool operator==(const Vertex3D& pt) const
        { 
            return pt.x() == xyz[0] && pt.y() == xyz[1] && pt.z() == xyz[2];
        }

        inline const double& x() const { return xyz[0]; }
        inline const double& y() const { return xyz[1]; }
        inline const double& z() const { return xyz[2]; }

        inline double& x() { return xyz[0]; }
        inline double& y() { return xyz[1]; }
        inline double& z() { return xyz[2]; }

        inline double& operator[] (const std::size_t idx)
        { 
            try
            {
                return xyz[idx];
            }
            catch (...)
            {
                throw std::runtime_error("Error in Vertex3D::operator[]");
            }
        }

        inline const double& operator[] (const std::size_t idx) const
        {
            try
            {
                return xyz[idx];
            }
            catch (...)
            {
                throw std::runtime_error("Error in Vertex3D::operator[]");
            }
        }

        inline void set(const double newX, const double newY, const double newZ)
        {
            xyz[0] = newX;
            xyz[1] = newY;
            xyz[2] = newZ;
        }

        inline const std::vector<int>& getFaceId() const { return faceId; }
        inline std::vector<int>& getFaceId() { return faceId; }
        void setFaceId(const std::vector<int>& newFaceId) { faceId = newFaceId; }
        inline void addFaceId(const int idx) 
        { 
            faceId.push_back(idx); 
        }

    private:
        double xyz[3];
        std::vector<int> faceId;
    };
}