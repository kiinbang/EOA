/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <string>

namespace constant
{
	/**Pi*/
	const double _PI_ = 3.14159265358979323846;

	/**Min standard deviation*/
	const double MIN_SD = 1.0e-9;

	/**Max standard deviation*/
	const double MAX_SD = 1.0e+9;

	/**Number of parameters defining a datum*/
	const unsigned int numDatumParams = 7;

	/**Min number of tie poijnts*/
	const unsigned int minNumTies = 6;

	namespace distmdlnames
	{
		const std::string _smac_ = "SMAC";
	}

	namespace smac
	{
		const unsigned int NUM_SMAC_PARMS = 7;

		///SMAC parameters
		const std::string _k0_ = "k0";
		const std::string _k1_ = "k1";
		const std::string _k2_ = "k2";
		const std::string _k3_ = "k3";
		const std::string _p1_ = "p1";
		const std::string _p2_ = "p2";
		const std::string _p3_ = "p3";
	}

	namespace camera
	{	///Camera parameters
		const std::string _f_ = "f";
		const std::string _xp_ = "xp";
		const std::string _yp_ = "yp";
		const std::string _width_ = "width";
		const std::string _height_ = "height";
		const std::string _pixel_pitch_ = "pixel_pitch";
		///Boresight and lever-arm
		const std::string _omega_bs_ = "omega_bs";
		const std::string _phi_bs_ = "phi_bs";
		const std::string _kappa_bs_ = "kappa_bs";
		const std::string _dX_ = "dX";
		const std::string _dY_ = "dY";
		const std::string _dZ_ = "dZ";
	}

	namespace photo
	{
		///EOP
		const std::string _omega_ = "omega";
		const std::string _phi_ = "phi";
		const std::string _kappa_ = "kappa";
		const std::string _X0_ = "X0";
		const std::string _Y0_ = "Y0";
		const std::string _Z0_ = "Z0";

		///photo point coordinates (xi, yi)
		const std::string _xi_ = "xi";
		const std::string _yi_ = "yi";
	}

	namespace object
	{
		///Object point (X, Y, Z)
		const std::string _Xa_ = "Xa";
		const std::string _Ya_ = "Ya";
		const std::string _Za_ = "Za";
	}
}