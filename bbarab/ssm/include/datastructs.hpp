#pragma once

#include <tinyobj/ObjLoader.h>

#include <ssm/include/SSMOpenQuaternion.hpp>

namespace globj
{
	class CPoint2
	{
	public:
		CPoint2() : u(x[0]), v(x[1]) { u = v = 0.0; }
		CPoint2(const double _u, const double _v) : u(x[0]), v(x[1]) { u = _u; v = _v; }
		CPoint2(const CPoint2& c) : u(x[0]), v(x[1]) { copy(c); }
		CPoint2 operator = (const CPoint2& c) { copy(c); return *this; }

	private:
		void copy(const CPoint2& c)
		{
			x[0] = c.x[0];
			x[1] = c.x[1];
		}

	public:
		double x[2];
		double& u;
		double& v;
	};

	struct VC
	{
		float r, g, b;
	};

	using Pos3D = tobj::Vertex;
	using CPoint = Pos3D;
	
	class CArcball
	{
	public:
		CArcball() {}

		CArcball(int win_width, int win_height, int ox, int oy)
		{
			m_radius = (win_width < win_height) ? win_width / 2 : win_height / 2;

			m_center = CPoint2(win_width / 2, win_height / 2);

			CPoint2 p(ox, oy);

			_plane2sphere(p, m_position);
		}

		/// Update the arcball
		/// nx current x-position of mouse
		/// ny current y-position of mouse
		/// return quoternion of the roation from the old position to the new position
		SSMATICS_EOA::CSMQuaternion update(int nx, int ny)
		{
			Pos3D position;
			_plane2sphere(CPoint2(nx, ny), position);
			Pos3D cp = m_position ^ position;
			auto innerP = m_position & position;
			SSMATICS_EOA::CSMQuaternion r(innerP, cp[0], cp[1], cp[2]);
			m_position = position;

			return r;
		};


	private:
		/// mapping a planar point v to the unit sphere point r
		/// v input planar point
		/// r output point on the unit sphere
		void _plane2sphere(const CPoint2& v, CPoint& r)
		{
			CPoint2 f = v;

			f.u /= m_radius;
			f.v /= m_radius;

			double l = sqrt(f.u * f.u + f.v * f.v);

			if (l > 1.0) {
				r = CPoint(f.u / l, f.v / l, 0);
				return;
			}

			double fz = sqrt(1 - l * l);

			r = CPoint(f.u, f.v, fz);
		};

		/// current position on the unit sphere
		Pos3D m_position;
		/// radius of the sphere
		double   m_radius;
		/// center of sphere on the plane
		CPoint2 m_center;
	};
}