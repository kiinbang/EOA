/*
* Copyright(c) 2005-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <ssm/include/SSMLeastSquares.h>

namespace ssm
{
	NdotMat::NdotMat()
	{
		const unsigned int nParams = 0;
		const unsigned int nSets = 0;

		config(nParams, nSets);
	}

	NdotMat::NdotMat(const unsigned int nParams, const unsigned int nSets)
	{
		config(nParams, nSets);
	}

	void NdotMat::config(const unsigned int nParams, const unsigned int nSets)
	{
		numSets = nSets;
		numParams = nParams;

		dot.resize(numSets);
		for (unsigned int i = 0; i < dot.size(); ++i)
		{
			dot[i].resize(nParams, nParams, 0.0);
		}
	}

	util::SsmList<math::Matrixd>& NdotMat::get() { return dot; }

	const util::SsmList<math::Matrixd>& NdotMat::readOnly() const { return dot; }

	unsigned int NdotMat::getNumParams() { return numParams; }

	unsigned int NdotMat::getNumSets() { return numSets; }

	CdotMat::CdotMat() : NdotMat() {}

	CdotMat::CdotMat(const unsigned int nParams, const unsigned int nSets) : NdotMat(nParams, nSets) {}

	void CdotMat::config(const unsigned int nParams, const unsigned int nSets)
	{
		numSets = nSets;
		numParams = nParams;

		dot.resize(numSets);
		for (unsigned int i = 0; i < dot.size(); ++i)
		{
			dot[i].resize(nParams, 1, 0.0);
		}
	}

	NbarMat::NbarMat()
	{
		const unsigned int nSets1 = 0;
		const unsigned int nSets2 = 0;

		config(nSets1, nSets1);
	}

	NbarMat::NbarMat(const unsigned int nSets1, const unsigned int nSets2)
	{
		config(nSets1, nSets2);
	}

	void NbarMat::config(const unsigned int nSets1, const unsigned int nSets2)
	{
		numSets1 = nSets1;
		numSets2 = nSets2;

		nbar.resize(numSets1);
		for (unsigned int i = 0; i < nbar.size(); ++i)
		{
			auto& nbari = nbar[i];

			nbari.resize(numSets2);
			for (unsigned int j = 0; j < nbar[i].size(); ++j)
			{
				nbar[i][j].del();
			}
			/// Make the matrix empty because only some of sub-parts of N-bar matrix are not empty and it need to save memory.
		}
	}

	util::SsmList<util::SsmList<math::Matrixd>>& NbarMat::get() { return nbar; }

	const util::SsmList<util::SsmList<math::Matrixd>>& NbarMat::readOnly() const { return nbar; }

	unsigned int NbarMat::getNumSets1() { return numSets1; }
	unsigned int NbarMat::getNumSets2() { return numSets2; }

	/** Constructor */
	LeastSquare::LeastSquare()
	{
		init();
	}

	/** Constructor */
	LeastSquare::LeastSquare(const unsigned int nCameras,
		const unsigned int nIOPs,
		const unsigned int nImages,
		const unsigned int nEOPs,
		const unsigned int nPoints,
		const unsigned int nLines,
		const unsigned int nTriangles)
	{
		numParameters.resize(numSections);
		numParameters[scui(SectIdx::IOP)] = nIOPs;
		numParameters[scui(SectIdx::EOP)] = nEOPs;
		numParameters[scui(SectIdx::PT)] = 3;
		numParameters[scui(SectIdx::LINE)] = 6;
		numParameters[scui(SectIdx::TRI)] = 9;

		numSets.resize(numSections);
		numSets[scui(SectIdx::IOP)] = nCameras;
		numSets[scui(SectIdx::EOP)] = nImages;
		numSets[scui(SectIdx::PT)] = nPoints;
		numSets[scui(SectIdx::LINE)] = nLines;
		numSets[scui(SectIdx::TRI)] = nTriangles;

		numUnknown = nCameras * nIOPs
			+ nImages * nEOPs
			+ nPoints * 3
			+ nLines * 6
			+ nTriangles * 9;

		Error.clear();

		numEq = 0;

		PosVar = 0;

		///
		/// New approach using the matrices below
		///
		NdotList.resize(numSections);
		for (unsigned int i = 0; i < numSections; ++i)
		{
			NdotList[i].config(numParameters[i], numSets[i]);
		}

		NbarList.resize(numSections);
		for (unsigned int i = 0; i < numSections; ++i)
		{
			NbarList[i].resize(numSections);
			for (unsigned int j = i + 1/*consider only the upper triangle*/; j < numSections; ++j)
			{
				NbarList[i][j].config(numSets[i], numSets[j]);
			}
		}

		CdotList.resize(numSections);
		for (unsigned int i = 0; i < numSections; ++i)
		{
			CdotList[i].config(numParameters[i], numSets[i]);
		}
	}

	/**destructor*/
	LeastSquare::~LeastSquare()
	{
	}

	/**initialized the object*/
	void LeastSquare::init()
	{
		PosVar = 0.0;
		numEq = 0;
		numUnknown = 0;
	}

	/**simple LS without weight matrix*/
	RetMat LeastSquare::runLeastSquare(const math::Matrixd& A, const math::Matrixd& L)
	{
		unsigned int _size_ = A.getRows();
		math::Matrixd W(_size_, _size_);
		W.makeIdentityMat();
		return runLeastSquare(A, L, W);
	}

	/**simple and fast(simple result, only X matrix) LS*/
	math::Matrixd LeastSquare::runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L)
	{
		unsigned int _size_ = A.getRows();
		math::Matrixd W(_size_, _size_);
		W.makeIdentityMat();
		return runLeastSquare_Fast(A, L, W);
	}

	/**LS using weight matrix, not using reduced normal matrix for simple modeling*/
	RetMat LeastSquare::runLeastSquare(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W)
	{
		RetMat retval;
		retval.A = A;
		retval.L = L;
		retval.W = W;
		retval.AT = retval.A.transpose();
		retval.N = retval.AT%retval.W%retval.A;
		retval.Ninv = retval.N.inverse();
		retval.X = retval.Ninv%retval.AT%retval.W%retval.L;
		retval.V = retval.A%retval.X - retval.L;
		retval.VTV = retval.V.transpose() % retval.W%retval.V;
		retval.variance = retval.VTV(0, 0) / (retval.A.getRows() - retval.A.getCols());
		retval.sd = sqrt(retval.variance);
		retval.XVarCov = retval.Ninv*retval.variance;
		retval.LVarCov = (retval.A%retval.Ninv%retval.AT)*retval.variance;

		return retval;
	}

	/**LS using weight matrix, not using reduced normal matrix for simple modeling<br>
	*simple result, only X matrix, fast a little.
	*/
	math::Matrixd LeastSquare::runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W)
	{
		RetMat retval;
		retval.A = A;
		retval.L = L;
		retval.W = W;

		retval.AT = retval.A.transpose();
		retval.N = retval.AT%retval.W%retval.A;
		retval.Ninv = retval.N.inverse();
		retval.X = retval.Ninv%retval.AT%retval.W%retval.L;

		return retval.X;
	}

	math::Matrixd LeastSquare::runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W, math::Matrixd& N)
	{
		RetMat retval;
		retval.A = A;
		retval.L = L;
		retval.W = W;

		retval.AT = retval.A.transpose();
		retval.N = retval.AT%retval.W%retval.A;
		retval.Ninv = retval.N.inverse();
		retval.X = retval.Ninv%retval.AT%retval.W%retval.L;

		N = retval.N;

		return retval.X;
	}

	/**Get correlation matrix from variance-covariance matrix*/
	math::Matrixd LeastSquare::getCorrelation(const math::Matrixd& VarCov)
	{
		unsigned int i, j;
		//Correlation of Unknown Matrix
		math::Matrixd Correlation;
		Correlation.resize(VarCov.getRows(), VarCov.getCols(), 0.);

		for (i = 0; i<Correlation.getRows(); i++)
		{
			double sigma_i, sigma_j;
			sigma_i = sqrt(VarCov(i, i));

			for (j = i; j<Correlation.getCols(); j++)
			{
				sigma_j = sqrt(VarCov(j, j));
				Correlation(i, j) = VarCov(i, j) / sigma_i / sigma_j;
			}
		}
		return Correlation;
	}

	/**Extract variance(diagonal element) matrix from variance-covariance matrix*/
	math::Matrixd LeastSquare::extractVar(const math::Matrixd& VarCov)
	{
		unsigned int i;
		//Correlation of Unknown Matrix
		math::Matrixd Variance(VarCov.getRows(), 1, 0.);

		for (i = 0; i<Variance.getRows(); i++)
		{
			Variance(i, 0) = VarCov(i, i);
		}
		return Variance;
	}

	/**GetErrorList*/
	util::SsmList<double> LeastSquare::getErrorList() const
	{
		util::SsmList<double> retval;

		unsigned int num_mat = static_cast<int>(Error.size());

		//DATA<MatrixData> *pos = NULL;
		int count = 0;
		for (unsigned int i = 0; i<num_mat; i++)
		{
			count += Error[i].getRows();
		}

		retval.resize(count);

		int index = 0;
		for (unsigned int i = 0; i<num_mat; i++)
		{
			int nrows = Error[i].getRows();
			for (int j = 0; j<nrows; j++)
			{
				retval[index] = Error[i](j, 0);
				index++;
			}
		}

		return retval;
	}

	////////////////////////////
	///Reduced Normal Matrix////
	////////////////////////////
	/**LS for bundle using reduced normal matrix<br>
	*EO parameters, point data and straight line data.
	*/
	/// Note that  this is for the case including iops, eops, points, lines represented two points and planar patches represented three points,
	/// and it corresponds to the collinearity equations.
	/// For the use of other features, unknown parameters and models (such as a coplanarity condition), it should be modified and add additional functions.
	math::Matrixd LeastSquare::runLeastSquare_RN(double &var, math::Matrixd &N_dot, math::Matrixd &C_dot)
	{
		/// N_dot
		N_dot = make_N_dot();

		/// N_bar
		const unsigned int NbarRows1 = numParameters[scui(SectIdx::IOP)] * numSets[scui(SectIdx::IOP)];
		const unsigned int NbarRows2 = numParameters[scui(SectIdx::EOP)] * numSets[scui(SectIdx::EOP)];
		const unsigned int NbarRows = NbarRows1 + NbarRows2;
		const unsigned int NbarCols1 = numParameters[scui(SectIdx::PT)] * numSets[scui(SectIdx::PT)];
		const unsigned int NbarCols2 = numParameters[scui(SectIdx::LINE)] * numSets[scui(SectIdx::LINE)];
		const unsigned int NbarCols3 = numParameters[scui(SectIdx::TRI)] * numSets[scui(SectIdx::TRI)];
		const unsigned int NbarCols = NbarCols1 + NbarCols2 + NbarCols3;
		math::Matrixd N_bar(NbarRows, NbarCols, 0.0);

		if (numSets[scui(SectIdx::IOP)] > 0)
		{
			if (numSets[scui(SectIdx::PT)] > 0) N_bar.insert(0, 0, getNbar(SectIdx::IOP, SectIdx::PT));
			if (numSets[scui(SectIdx::LINE)] > 0) N_bar.insert(0, NbarCols1, getNbar(SectIdx::IOP, SectIdx::LINE));
			if (numSets[scui(SectIdx::TRI)] > 0) N_bar.insert(0, NbarCols1+NbarCols2, getNbar(SectIdx::IOP, SectIdx::TRI));
		}

		if (numSets[scui(SectIdx::EOP)] > 0)
		{
			if (numSets[scui(SectIdx::PT)] > 0) N_bar.insert(NbarRows1, 0, getNbar(SectIdx::EOP, SectIdx::PT));
			if (numSets[scui(SectIdx::LINE)] > 0)N_bar.insert(NbarRows1, NbarCols1, getNbar(SectIdx::EOP, SectIdx::LINE));
			if (numSets[scui(SectIdx::TRI)] > 0)N_bar.insert(NbarRows1, NbarCols1+NbarCols2, getNbar(SectIdx::EOP, SectIdx::TRI));
		}
		
		///N_bar_transe
		math::Matrixd N_bar_trans;
		N_bar_trans = N_bar.transpose();

		///C_dot(K_dot)
		C_dot = make_C_dot();

		///C_2dot(K_2dot)
		math::Matrixd C_2dot = make_C_2dot();

		math::Matrixd N_bar_N_2dot_inv(N_bar.getRows(), N_bar.getCols(), 0.0);

		///N_2dot_inverse
		NdotMat N_2dot_inv_pt = NdotList[scui(SectIdx::PT)];
		NdotMat N_2dot_inv_ln = NdotList[scui(SectIdx::LINE)];
		NdotMat N_2dot_inv_tr = NdotList[scui(SectIdx::TRI)];

		///Make N_2dot_inverse matrix (Points)
		unsigned int ptSize = numParameters[scui(SectIdx::PT)];
		unsigned int indexOffset = 0;
		for (unsigned int point_index = 0; point_index<numSets[scui(SectIdx::PT)]; point_index++)
		{
			math::Matrixd N2dot_i_inv = N_2dot_inv_pt.get()[point_index].inverse();
			N_2dot_inv_pt.get()[point_index] = N2dot_i_inv;
			unsigned int index = indexOffset + point_index * ptSize;
			math::Matrixd Nbar_i = N_bar.getSubset(0, index, N_bar.getRows(), ptSize);
			math::Matrixd Nbar_N2dot_i_inv = Nbar_i % N2dot_i_inv;
			N_bar_N_2dot_inv.insertPlus(0, index, Nbar_N2dot_i_inv);
		}

		///Make N_2dot_inverse matrix (Lines)
		unsigned int lnSize = numParameters[scui(SectIdx::LINE)];
		indexOffset = numSets[scui(SectIdx::PT)] * ptSize;
		for (unsigned int line_index = 0; line_index<numSets[scui(SectIdx::LINE)]; line_index++)
		{
			math::Matrixd N2dot_i_inv = N_2dot_inv_ln.get()[line_index].inverse();
			N_2dot_inv_ln.get()[line_index] = N2dot_i_inv;
			unsigned int index = indexOffset + line_index * lnSize;
			math::Matrixd Nbar_i = N_bar.getSubset(0, index, N_bar.getRows(), lnSize);
			math::Matrixd Nbar_N2dot_i_inv = Nbar_i % N2dot_i_inv;
			N_bar_N_2dot_inv.insertPlus(0, index, Nbar_N2dot_i_inv);
		}

		///Make N_2dot_inverse matrix (Triangle)
		unsigned int trSize = numParameters[scui(SectIdx::TRI)];
		indexOffset = numSets[scui(SectIdx::PT)] * ptSize + numSets[scui(SectIdx::LINE)] * lnSize;
		for (unsigned int tri_index = 0; tri_index<numSets[scui(SectIdx::TRI)]; tri_index++)
		{
			math::Matrixd N2dot_i_inv = N_2dot_inv_tr.get()[tri_index].inverse();
			N_2dot_inv_tr.get()[tri_index] = N2dot_i_inv;
			unsigned int index = indexOffset + tri_index * trSize;
			math::Matrixd Nbar_i = N_bar.getSubset(0, index, N_bar.getRows(), trSize);
			math::Matrixd Nbar_N2dot_i_inv = Nbar_i % N2dot_i_inv;
			N_bar_N_2dot_inv.insertPlus(0, index, Nbar_N2dot_i_inv);
		}

		math::Matrixd S;
		math::Matrixd N_2bar_N_2dot_inv_N_bar_trans = N_bar_N_2dot_inv % N_bar_trans;

		S = (N_dot - N_2bar_N_2dot_inv_N_bar_trans);

		math::Matrixd E;

		E = (C_dot - (N_bar_N_2dot_inv % C_2dot));

		///
		/// Solve X-dot for IOPs and EOPs
		///
		math::Matrixd X_dot = S.inverse() % E;

		///
		/// Solve X-2dot for points, lines and triangle patches
		///

		math::Matrixd N_bar_trans_X = N_bar_trans % X_dot;
		
		/// X for points
		math::Matrixd X_pt(ptSize* numSets[scui(SectIdx::PT)], 1, 0.0);
		for (unsigned int point_index = 0; point_index < numSets[scui(SectIdx::PT)]; ++point_index)
		{
			unsigned int index = point_index * ptSize;
			//auto N_bar_trans_X_i = N_bar_trans_X.getSubset(index, 0, ptSize, 1);
			auto C_2dot_i = CdotList[scui(SectIdx::PT)].readOnly()[point_index];
			auto N_2dot_inv_pt_i = N_2dot_inv_pt.get()[point_index];
			X_pt.insertPlus(index, 0, N_2dot_inv_pt_i % (C_2dot_i - N_bar_trans_X.getSubset(index, 0, ptSize, 1)));
		}

		/// X for lines
		math::Matrixd X_ln(lnSize* numSets[scui(SectIdx::LINE)], 1, 0.0);
		indexOffset = numSets[scui(SectIdx::PT)] * ptSize;
		for (unsigned int line_index = 0; line_index < numSets[scui(SectIdx::LINE)]; ++line_index)
		{
			unsigned int index = line_index * lnSize;
			//auto& N_bar_trans_X_i = N_bar_trans_X.getSubset(indexOffset + index, 0, lnSize, 1);
			auto& C_2dot_i = CdotList[scui(SectIdx::LINE)].readOnly()[line_index];
			auto& N_2dot_inv_ln_i = N_2dot_inv_ln.get()[line_index];
			X_ln.insertPlus(index, 0, N_2dot_inv_ln_i % (C_2dot_i - N_bar_trans_X.getSubset(indexOffset + index, 0, lnSize, 1)));
		}

		/// X for triangle patches
		math::Matrixd X_tr(trSize* numSets[scui(SectIdx::TRI)], 1, 0.0);
		indexOffset = numSets[scui(SectIdx::PT)] * ptSize + numSets[scui(SectIdx::LINE)] * lnSize;
		for (unsigned int tri_index = 0; tri_index < numSets[scui(SectIdx::TRI)]; ++tri_index)
		{
			unsigned int index = tri_index * trSize;
			//auto& N_bar_trans_X_i = N_bar_trans_X.getSubset(indexOffset + index, 0, trSize, 1);
			auto& C_2dot_i = CdotList[scui(SectIdx::TRI)].readOnly()[tri_index];
			auto& N_2dot_inv_tr_i = N_2dot_inv_tr.get()[tri_index];
			X_tr.insertPlus(index, 0, N_2dot_inv_tr_i % (C_2dot_i - N_bar_trans_X.getSubset(indexOffset + index, 0, trSize, 1)));
		}
		
		//X_2dot add to X_dot
		math::Matrixd& X = X_dot;
		X.addRows(X_pt);
		X.addRows(X_ln);
		X.addRows(X_tr);	

		//standard deviation of unit weight
		var = PosVar / (numEq - numUnknown);

		return X_dot;
	}

	math::Matrixd LeastSquare::runLeastSquare_RN_New(double& var, math::Matrixd& N_dot, math::Matrixd& C_dot, math::Matrixd& X, const SectIdx boundarySect)
	{
		unsigned int sizeOfUpper = 0;
		unsigned int sizeOfLower = 0;

		for (unsigned int i = 0; i <= scui(boundarySect); ++i)
		{
			sizeOfUpper += numParameters[i] * numSets[i];
		}

		for (unsigned int i = scui(boundarySect)+1; i <= lastSectionIdx+1; ++i)
		{
			sizeOfLower += numParameters[i] * numSets[i];
		}

		/// N_dot
		N_dot = make_N_dot_New(sizeOfUpper, boundarySect);

		/// Number of parameters of each section
		util::SsmList<unsigned int> NbarRows(scui(boundarySect) + 1);
		unsigned int allRows = 0;
		for (unsigned int i = 0; i <= scui(boundarySect); ++i)
		{
			NbarRows[i] = numParameters[i] * numSets[i];
			allRows += NbarRows[i];
		}

		util::SsmList<unsigned int> NbarCols(lastSectionIdx - scui(boundarySect));
		unsigned int allCols = 0;
		for (unsigned int j = scui(boundarySect)+1; j <= lastSectionIdx+1; ++j)
		{
			NbarCols[j - scui(boundarySect) - 1] = numParameters[j] * numSets[j];
			allCols += NbarCols[j - scui(boundarySect) - 1];
		}

		/// N bar matrix
		math::Matrixd N_bar(allRows, allCols, 0.0);

		unsigned int rowPos = 0;
		for (unsigned int i = 0; i <= scui(boundarySect); ++i)
		{
			SectIdx idxR = static_cast<SectIdx>(i);

			if (numSets[i] > 0)
			{
				unsigned int colPos = 0;
				for (unsigned int j = scui(boundarySect) + 1; j <= lastSectionIdx+1; ++j)
				{
					SectIdx idxC = static_cast<SectIdx>(j);
					
					if (numSets[j] > 0)
					{
						N_bar.insert(rowPos, colPos, getNbar(idxR, idxC));
					}

					colPos += NbarCols[j - scui(boundarySect) - 1];
				}
			}

			rowPos += NbarRows[i];
		}

		///N_bar_transe
		math::Matrixd N_bar_trans;
		N_bar_trans = N_bar.transpose();

		///C_dot(K_dot)
		C_dot = make_C_dot_New(sizeOfUpper, boundarySect);

		///C_2dot(K_2dot)
		math::Matrixd C_2dot = make_C_2dot_New(sizeOfLower, boundarySect);

		math::Matrixd N_bar_N_2dot_inv(N_bar.getRows(), N_bar.getCols(), 0.0);

		unsigned int indexOffset = 0;
		
		///N_2dot_inverse
		util::SsmList<NdotMat> N2dotInv(NdotList.size());
		for (unsigned int i = scui(boundarySect)+1; i <= lastSectionIdx; ++i)
		{
			///Make N_2dot_inverse matrix
			unsigned int sizeOfi = numParameters[i];
			
			for (unsigned int j = 0; j < numSets[i]; ++j)
			{
				math::Matrixd N2dot_i_inv = NdotList[i].get()[j].inverse();
				N2dotInv[i].get()[j] = N2dot_i_inv;
				unsigned int idx = indexOffset + j * sizeOfi;
				math::Matrixd Nbar_i = N_bar.getSubset(0, idx, N_bar.getRows(), sizeOfi);
				math::Matrixd Nbar_N2dot_i_inv = Nbar_i % N2dot_i_inv;
				N_bar_N_2dot_inv.insertPlus(0, idx, Nbar_N2dot_i_inv);
			}

			indexOffset += numSets[i] * sizeOfi;
		}

		math::Matrixd S;
		math::Matrixd N_2bar_N_2dot_inv_N_bar_trans = N_bar_N_2dot_inv % N_bar_trans;

		S = (N_dot - N_2bar_N_2dot_inv_N_bar_trans);

		math::Matrixd E;

		E = (C_dot - (N_bar_N_2dot_inv % C_2dot));

		///
		/// Solve X-dot for IOPs and EOPs
		///
		math::Matrixd X_dot = S.inverse() % E;

		///
		/// Solve X-2dot for points, lines and triangle patches
		///

		math::Matrixd N_bar_trans_X = N_bar_trans % X_dot;

		/// X2dot
		util::SsmList<math::Matrixd> x2dot(lastSectionIdx - scui(boundarySect));
		indexOffset = 0;
		for (unsigned int i = scui(boundarySect) + 1; i <= lastSectionIdx; ++i)
		{
			unsigned int sizeOfi = numParameters[i];

			auto& xi = x2dot[i - scui(boundarySect) - 1];
			xi.resize(sizeOfi* numSets[i], 1, 0.0);
			
			for (unsigned int j = 0; j < numSets[i]; ++j)
			{
				unsigned int index = j * sizeOfi;
				const auto& C_2dot_i = CdotList[i].readOnly()[j];
				const auto& N_2dot_inv_j = N2dotInv[i].get()[j];
				xi.insertPlus(index, 0, N_2dot_inv_j % (C_2dot_i - N_bar_trans_X.getSubset(index, 0, sizeOfi, 1)));
			}

			indexOffset += numSets[i] * sizeOfi;
		}

		//Estimated unknowns
		X = X_dot;
		for (unsigned int i = 0; i < x2dot.size(); ++i)
			X.addRows(x2dot[i]);

		//standard deviation of unit weight
		var = PosVar / (numEq - numUnknown);

		//return x_dot
		return X_dot;
	}

	/**Parameters' constraints*/
	double LeastSquare::fill_Nmat_IOP(math::Matrixd aIop, math::Matrixd w, math::Matrixd l, int index)
	{
		util::SsmList<AMat> a(1);
		a[0].a = aIop;
		a[0].index = index; /// Camera index
		a[0].sect = SectIdx::IOP;
		const unsigned int nEqs = numParameters[scui(SectIdx::IOP)]; /// l.getRows()

		return fill_Nmat(a, w, l, nEqs, this->NdotList, this->NbarList, this->CdotList);
	}

	/**Parameters' constraints*/
	double LeastSquare::fill_Nmat_EOP(math::Matrixd aEop, math::Matrixd w, math::Matrixd l, int index)
	{
		util::SsmList<AMat> a(1);
		a[0].a = aEop;
		a[0].index = index; /// Image index
		a[0].sect = SectIdx::EOP;
		const unsigned int nEqs = numParameters[scui(SectIdx::EOP)]; /// l.getRows()

		return fill_Nmat(a, w, l, nEqs, this->NdotList, this->NbarList, this->CdotList);
	}

	/**fill the normal matrix for the point data*/
	double LeastSquare::fill_Nmat_Data_Point(const math::Matrixd& aEop,
		const math::Matrixd& aObj,
		const math::Matrixd& w,
		const math::Matrixd& l,
		const int point_index,
		const int image_index)
	{
		math::Matrixd aIop;
		unsigned int camera_index = 0;

		return fill_Nmat_Data_Point(aIop, aEop, aObj, w, l, point_index, image_index, camera_index);
	}

	/**fill the normal matrix for the point data*/
	double LeastSquare::fill_Nmat_Data_Point(const math::Matrixd& aIop,
		const math::Matrixd& aEop,
		const math::Matrixd& aObj,
		const math::Matrixd& w,
		const math::Matrixd& l,
		const int point_index,
		const int image_index,
		const int camera_index)
	{
		util::SsmList<AMat> a(3);

		a[0].a = aIop;
		a[0].index = camera_index;
		a[0].sect = SectIdx::IOP;

		a[1].a = aEop;
		a[1].index = image_index;
		a[1].sect = SectIdx::EOP;

		a[2].a = aObj;
		a[2].index = point_index;
		a[2].sect = SectIdx::PT;

		const unsigned int nEqs = 2;

		return fill_Nmat(a, w, l, nEqs, this->NdotList, this->NbarList, this->CdotList);
	}

	/**fill the normal matrix for the line data*/
	/// For points on a line
	double LeastSquare::fill_Nmat_Data_Line(const math::Matrixd& aEop, 
		const math::Matrixd& aLine, 
		const math::Matrixd& w, 
		const math::Matrixd& l, 
		const int line_index, 
		const int image_index)
	{
		math::Matrixd aIop;
		unsigned int camera_index = 0;

		return fill_Nmat_Data_Line(aIop, aEop, aLine, w, l, line_index, image_index, camera_index);
	}

	/**fill the normal matrix for the line data*/
	/// For points on a line
	double LeastSquare::fill_Nmat_Data_Line(const math::Matrixd& aIop, 
		const math::Matrixd& aEop, 
		const math::Matrixd& aLine,
		const math::Matrixd& w, 
		const math::Matrixd& l, 
		const int line_index, 
		const int image_index, 
		const int camera_index)
	{
		util::SsmList<AMat> a(3);

		a[0].a = aIop;
		a[0].index = camera_index;
		a[0].sect = SectIdx::IOP;

		a[1].a = aEop;
		a[1].index = image_index;
		a[1].sect = SectIdx::EOP;

		a[2].a = aLine;
		a[2].index = line_index;
		a[2].sect = SectIdx::LINE;

		const unsigned int nEqs = 1;

		return fill_Nmat(a, w, l, nEqs, this->NdotList, this->NbarList, this->CdotList);
	}

	/**3D ground point constraints*/
	double LeastSquare::fill_Nmat_Data_XYZ(const math::Matrixd& aObj, 
		const math::Matrixd& w,
		const math::Matrixd& l,
		int point_index)
	{
		util::SsmList<AMat> a(1);
		a[0].a = aObj;
		a[0].index = point_index;
		a[0].sect = SectIdx::PT;
		const unsigned int nEqs = 3;

		return fill_Nmat(a, w, l, nEqs, this->NdotList, this->NbarList, this->CdotList);
	}

	/**3D ending point of the line constraints*/
	double LeastSquare::fill_Nmat_Data_XYZofLine(const math::Matrixd& a2, 
		const math::Matrixd& w, 
		const math::Matrixd& l,
		const int line_index)
	{
		util::SsmList<AMat> a(1);
		a[0].a = a2;
		a[0].index = line_index;
		a[0].sect = SectIdx::LINE;
		const unsigned int nEqs = 6;

		return fill_Nmat(a, w, l, nEqs, this->NdotList, this->NbarList, this->CdotList);
	}

	/**Set configuration data for RunLeastSquare_RN and camera model*/
	void LeastSquare::setDataConfig(const unsigned int nCameras,
		const unsigned int nIOPs,
		const unsigned int nImages,
		const unsigned int nEOPs,
		const unsigned int nPoints,
		const unsigned int nLines,
		const unsigned int nTriangles)
	{
		numParameters.resize(numSections);
		numParameters[scui(SectIdx::IOP)] = nIOPs;
		numParameters[scui(SectIdx::EOP)] = nEOPs;
		numParameters[scui(SectIdx::PT)] = 3;
		numParameters[scui(SectIdx::LINE)] = 6;
		numParameters[scui(SectIdx::TRI)] = 9;

		numSets.resize(numSections);
		numSets[scui(SectIdx::IOP)] = nCameras;
		numSets[scui(SectIdx::EOP)] = nImages;
		numSets[scui(SectIdx::PT)] = nPoints;
		numSets[scui(SectIdx::LINE)] = nLines;
		numSets[scui(SectIdx::TRI)] = nTriangles;

		numUnknown = nCameras * nIOPs
			+ nImages * nEOPs
			+ nPoints * 3
			+ nLines * 6
			+ nTriangles * 9;

		Error.clear();

		numEq = 0;

		PosVar = 0;

		///
		/// New approach using the matrices below
		///
		NdotList.resize(numSections);
		for (unsigned int i=0; i<numSections; ++i)
		{
			NdotList[i].config(numParameters[i], numSets[i]);
		}

		NbarList.resize(numSections);
		for (unsigned int i = 0; i < numSections; ++i)
		{
			NbarList[i].resize(numSections);
			for (unsigned int j = i+1/*consider only the upper triangle*/; j < numSections; ++j)
			{
				NbarList[i][j].config(numSets[i], numSets[j]);
			}
		}

		CdotList.resize(numSections);
		for (unsigned int i = 0; i < numSections; ++i)
		{
			CdotList[i].config(numParameters[i], numSets[i]);
		}
	}

	/**Make N_dot matrix from N_dot elements list*/
	math::Matrixd LeastSquare::make_N_dot() const
	{
		/// Note:
		/// It is for bundle adjustment based on collinearity.
		/// For the use of coplanarity condition, it need to be modified, 
		/// especially, for N dot matrix part because two images (EOP sets) corresponded in the same coplanarity condition equation.
		/// So off-diagonal parts of Ndot-EOP matrix are not completely empty.

		unsigned int sizeMatIOP = numParameters[scui(SectIdx::IOP)] * numSets[scui(SectIdx::IOP)];
		unsigned int sizeMatEOP = numParameters[scui(SectIdx::EOP)] * numSets[scui(SectIdx::EOP)];

		math::Matrixd retmat(sizeMatIOP + sizeMatEOP, sizeMatIOP + sizeMatEOP, 0.0);
		
		/// Keep this order: IOP - EOP
		retmat.insert(0, 0, getNdot(SectIdx::IOP));
		retmat.insert(sizeMatIOP, sizeMatIOP, getNdot(SectIdx::EOP));
		auto Nbar = getNbar(SectIdx::IOP, SectIdx::EOP);
		retmat.insert(0, sizeMatIOP, Nbar);
		retmat.insert(sizeMatIOP, 0, Nbar.transpose());
		
		return retmat;
	}

	/**Make N_dot matrix from N_dot elements list*/
	math::Matrixd LeastSquare::make_N_dot_New(const unsigned int sizeOfUpper, const SectIdx sect) const
	{
		math::Matrixd Ndot(sizeOfUpper, sizeOfUpper, 0.0);

		unsigned int pos0 = 0;

		for (unsigned int i = 0; i <= scui(sect); ++i)
		{
			SectIdx idx0 = static_cast<SectIdx>(i);

			Ndot.insert(pos0, pos0, getNdot(idx0));
						
			unsigned int pos1 = pos0;

			for (unsigned int j = i + 1; j <= scui(sect); ++j)
			{
				SectIdx idx1 = static_cast<SectIdx>(j);
				auto Nbar = getNbar(idx0, idx1);

				pos1 += numParameters[j-1] * numSets[j-1];
				Ndot.insert(pos0, pos1, Nbar);
			}

			pos0 += numParameters[i] * numSets[i];
		}

		return Ndot;
	}

	/**Make C_dot matrix from C_dot elements list*/
	math::Matrixd LeastSquare::make_C_dot() const
	{
		unsigned int sizeMatIOP = numParameters[scui(SectIdx::IOP)] * numSets[scui(SectIdx::IOP)];
		unsigned int sizeMatEOP = numParameters[scui(SectIdx::EOP)] * numSets[scui(SectIdx::EOP)];

		math::Matrixd retmat(sizeMatIOP + sizeMatEOP, 1, 0.0);

		/// Keep this order: IOP - EOP
		retmat.insert(0, 0, getCdot(SectIdx::IOP));
		retmat.insert(sizeMatIOP, 0, getCdot(SectIdx::EOP));

		return retmat;
	}

	/**Make C_dot matrix from C_dot elements list*/
	math::Matrixd LeastSquare::make_C_dot_New(const unsigned int sizeOfUpper, const SectIdx sect) const
	{
		math::Matrixd cDot(sizeOfUpper, 1, 0.0);
		unsigned int pos0 = 0;

		for (unsigned int i = 0; i <= scui(sect); ++i)
		{
			SectIdx idx0 = static_cast<SectIdx>(i);
			cDot.insert(pos0, 0, getCdot(idx0));
			pos0 += numParameters[i] * numSets[i];
		}

		return cDot;
	}

	/**Make C_2dot matrix from C_2dot_point and C_2dot_line elements list & triangle patches for LIDAR*/
	math::Matrixd LeastSquare::make_C_2dot() const
	{
		unsigned int sizeMatPT = numParameters[scui(SectIdx::PT)] * numSets[scui(SectIdx::PT)];
		unsigned int sizeMatLINE = numParameters[scui(SectIdx::LINE)] * numSets[scui(SectIdx::LINE)];
		unsigned int sizeMatTRI = numParameters[scui(SectIdx::TRI)] * numSets[scui(SectIdx::TRI)];		

		math::Matrixd retmat(sizeMatPT + sizeMatLINE + sizeMatTRI, 1, 0.0);
		
		/// Keep this order: PT - LINE - TRI
		retmat.insert(0, 0, getCdot(SectIdx::PT));
		retmat.insert(sizeMatPT, 0, getCdot(SectIdx::LINE));
		retmat.insert(sizeMatPT + sizeMatLINE, 0, getCdot(SectIdx::TRI));

		return retmat;
	}

	/**Make C_2dot matrix from C_2dot_point and C_2dot_line elements list & triangle patches for LIDAR*/
	math::Matrixd LeastSquare::make_C_2dot_New(const unsigned int sizeOfLower, const SectIdx sect) const
	{
		math::Matrixd c2Dot(sizeOfLower, 1, 0.0);
		unsigned int pos0 = 0;

		for (unsigned int i = scui(sect)+1; i <= lastSectionIdx+1; ++i)
		{
			SectIdx idx0 = static_cast<SectIdx>(i);
			c2Dot.insert(pos0, 0, getCdot(idx0));
			pos0 += numParameters[i] * numSets[i];
		}

		return c2Dot;
	}

	/**fill the normal matrix*/
	double LeastSquare::fill_Nmat(const util::SsmList<AMat>& a,
		const math::Matrixd& w,
		const math::Matrixd& l,
		const unsigned int nEqs,
		util::SsmList<NdotMat>& Ndots,
		util::SsmList<util::SsmList<NbarMat>>& Nbars,
		util::SsmList<CdotMat>& Cdots)
	{
		for (unsigned int i = 0; i < static_cast<unsigned int>(a.size()); ++i)
		{
			const math::Matrixd& ai = a[i].a;

			math::Matrixd ai_t = ai.transpose();

			/// AT W L
			Cdots[scui(a[i].sect)].get()[a[i].index] += ai % w % l;

			for (unsigned int j = i; j < static_cast<unsigned int>(a.size()); ++j)
			{
				const math::Matrixd& aj = a[j].a;
				math::Matrixd aj_t = aj.transpose();

				/// N-dot, diagonal parts of a normal matrix
				if (i == j)
				{
					math::Matrixd& Ndoti = Ndots[scui(a[i].sect)].get()[a[i].index];
					/// AT W A
					/// All elements were initialized as a empty sub normal matrix
					Ndoti += ai_t % w % aj;
				}
				/// N-bar, off-diagonal parts of a normal matrix
				else
				{
					math::Matrixd& Nbarij = Nbars[scui(a[i].sect)][scui(a[j].sect)].get()[a[i].index][a[j].index];

					/// AT1 W A2
					/// Need to check it empty because all elements were initialized as a zero-size matrix
					if (Nbarij.getRows() == 0) Nbarij = ai_t % w % aj;
					else Nbarij += ai_t % w % aj;
				}
			}
		}

		//Posteriori variance
		double etpe = (l.transpose() % w % l)(0, 0);
		PosVar += etpe;
		//Error list
		Error.addTail(l);

		//Number of equations
		numEq += nEqs;

		return etpe;
	}

	/**get Cdot matrix for each section*/
	math::Matrixd LeastSquare::getCdot(const SectIdx idx_) const
	{
		const unsigned int idx = scui(idx_);

		unsigned int matSize = numParameters[idx] * numSets[idx];
		math::Matrixd retmat(matSize, 1, 0.0);

		const CdotMat& cdot = CdotList[idx];

		for (unsigned int i = 0; i < numSets[idx]; ++i)
		{
			retmat.insert(i * numParameters[idx], 0, cdot.readOnly()[i]);
		}

		return retmat;
	}

	/**get Ndot matrix for each section*/
	math::Matrixd LeastSquare::getNdot(const SectIdx idx_) const
	{
		const unsigned int idx = scui(idx_);

		unsigned int matSize = numParameters[idx] * numSets[idx];
		math::Matrixd retmat(matSize, matSize, 0.0);

		const NdotMat& ndot = NdotList[idx];

		for (unsigned int i = 0; i < numSets[idx]; ++i)
		{
			retmat.insert(i * numParameters[idx], i * numParameters[idx], ndot.readOnly()[i]);
		}

		return retmat;
	}

	/**get Nbar matrix for each off-diagonal section*/
	math::Matrixd LeastSquare::getNbar(const SectIdx idx1_, const SectIdx idx2_) const
	{
		const unsigned int idx1 = scui(idx1_);
		const unsigned int idx2 = scui(idx2_);

		const NbarMat& NbarSection = NbarList[idx1][idx2];

		math::Matrixd retmat(numParameters[idx1] * numSets[idx1], numParameters[idx2] * numSets[idx2], 0.0);

		for (unsigned int i = 0; i < numSets[idx1]; ++i)
		{
			for (unsigned int j = 0; j < numSets[idx2]; ++j)
			{
				const auto& Nbarij = NbarSection.readOnly()[i][j];
				/// Need to check it empty because all elements were initialized as a zero-size matrix
				if (Nbarij.getRows() > 0) /// not empty
				{
					retmat.insert(i*numParameters[idx1], j*numParameters[idx2], Nbarij);
				}
			}
		}

		return retmat;
	}

	/**write given string on given file*/
	void LeastSquare::fileOut(const std::string& fname, const std::string& st) const
	{
		std::fstream outfile;
		outfile.open(fname, std::ios::app);

		if (!outfile)
		{
			outfile.open(fname, std::ios::out);

			if (!outfile)
			{
				std::string msg = std::string("error in writing in the file: ") + fname;
				throw std::runtime_error(msg.c_str());
			}
		}

		outfile << st.c_str();
		outfile.close();
	}
}