/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <ssm/include/SSMConstants.h>
using namespace constant;
#include <SSM/include/SSMSMACModel.h>

namespace sensor
{
	/// 'The New Camera Calibration System at the U.S. Geological Survey', Donald L. Light
	/// PE&RS Vol. 58, No. 2, Februrary 1992, pp. 185-188.
	/// [Refer to 187p.]
	/// Radial lens distortion
	/// r = sqrt((x-xp)^2 + (y-yp)^2)
	/// dr = k0 + k1*r^3 + k2^r5 + k3^r^7 + ...
	/// If there is a change(df) in the computed focal length,
	/// dr = (df/f) + (k1+df/f)*r^3 + (k2+df/f)^r5 + (k3+df/f)^r^7 + ...
	///    = k0 + k1*r^3 + k2^r5 + k3^r^7 + ...
	/// dx = dr * (x/r) = x(k0/r + k1*r^2 + k2^r4 + k3^r^6 + ...)
	/// dy = dr * (y/r) = y(k0/r + k1*r^2 + k2^r4 + k3^r^6 + ...)
	/// where, set k0 = 0, and  typically k4 is  insignificantly different from zero.
	/// 

	/// De-centering distortion
	/// dx = (1 + p3^2*r^2)(p1(r^2 + 2x^2) + 2p2*xy)
	/// dy = (1 + p3^2*r^2)(p2(r^2 + 2y^2) + 2p1*xy)
	/// where, p3 is insignificantly different from zero.

	SMACModel::SMACModel()
	{
		this->smacPtr.resize(constant::smac::NUM_SMAC_PARMS);
		for(auto& p : smacPtr)
			p = std::make_shared<param::Parameter>();

		///Radial lens distortion
		this->smacPtr[0]->setName(constant::smac::_k0_);
		this->smacPtr[0]->setDescription("SMAC parameter k0 for radial lens distortion. k0 is usually zero(0) if there is no change in the computed focal length.");
		this->smacPtr[0]->set(0.0);
		this->smacPtr[0]->setDelta(1.0e-12);

		this->smacPtr[1]->setName(constant::smac::_k1_);
		this->smacPtr[1]->setDescription("SMAC parameter k1 for radial lens distortion; (df/f) + (k1+df/f)*r^3 + (k2+df/f)^r5 + (k3+df/f)^r^7 + ...");
		this->smacPtr[1]->set(0.0);
		this->smacPtr[1]->setDelta(1.0e-12);

		this->smacPtr[2]->setName(constant::smac::_k2_);
		this->smacPtr[2]->setDescription("SMAC parameter k2 for radial lens distortion; (df/f) + (k1+df/f)*r^3 + (k2+df/f)^r5 + (k3+df/f)^r^7 + ...");
		this->smacPtr[2]->set(0.0);
		this->smacPtr[2]->setDelta(1.0e-12);

		this->smacPtr[3]->setName(constant::smac::_k3_);
		this->smacPtr[3]->setDescription("SMAC parameter k3 for radial lens distortion; (df/f) + (k1+df/f)*r^3 + (k2+df/f)^r5 + (k3+df/f)^r^7 + ...");
		this->smacPtr[3]->set(0.0);
		this->smacPtr[3]->setDelta(1.0e-12);

		//Decentering lens distortion
		this->smacPtr[4]->setName(constant::smac::_p1_);
		this->smacPtr[4]->setDescription("SMAC parameter p1 for decentering lens distortion; dx = (1 + p3^2*r^2)(p1(r^2 + 2x^2) + 2p2*xy); dy = (1 + p3^2*r^2)(p2(r^2 + 2y^2) + 2p1*xy)");
		this->smacPtr[4]->set(0.0);
		this->smacPtr[4]->setDelta(1.0e-12);

		this->smacPtr[5]->setName(constant::smac::_p2_);
		this->smacPtr[5]->setDescription("SMAC parameter p2 for decentering lens distortion; dx = (1 + p3^2*r^2)(p1(r^2 + 2x^2) + 2p2*xy); dy = (1 + p3^2*r^2)(p2(r^2 + 2y^2) + 2p1*xy)");
		this->smacPtr[5]->set(0.0);
		this->smacPtr[5]->setDelta(1.0e-12);

		this->smacPtr[6]->setName(constant::smac::_p3_);
		this->smacPtr[6]->setDescription("SMAC parameter p3 for decentering lens distortion; dx = (1 + p3^2*r^2)(p1(r^2 + 2x^2) + 2p2*xy); dy = (1 + p3^2*r^2)(p2(r^2 + 2y^2) + 2p1*xy)");
		this->smacPtr[6]->set(0.0);
		this->smacPtr[6]->setDelta(1.0e-12);
	}

	SMACModel::SMACModel(const double(&k0Tok3)[4], const double(&p1Top3)[3])
	{
		this->smacPtr.resize(constant::smac::NUM_SMAC_PARMS);
		for (auto& p : smacPtr)
			p = std::make_shared<param::Parameter>();

		// Radial lens distortion
		auto& k = this->smacPtr;

		k[0]->setName(constant::smac::_k0_);
		k[0]->setDescription("SMAC parameter k0 for radial lens distortion. k0 is usually zero(0) if there is no change in the computed focal length.");
		k[0]->set(k0Tok3[0]);
		k[0]->setDelta(1.0e-12);

		k[1]->setName(constant::smac::_k1_);
		k[1]->setDescription("SMAC parameter k1 for radial lens distortion; (df/f) + (k1+df/f)*r^3 + (k2+df/f)^r5 + (k3+df/f)^r^7 + ...");
		k[1]->set(k0Tok3[1]);
		k[1]->setDelta(1.0e-12);

		k[2]->setName(constant::smac::_k2_);
		k[2]->setDescription("SMAC parameter k2 for radial lens distortion; (df/f) + (k1+df/f)*r^3 + (k2+df/f)^r5 + (k3+df/f)^r^7 + ...");
		k[2]->set(k0Tok3[2]);
		k[2]->setDelta(1.0e-12);

		k[3]->setName(constant::smac::_k3_);
		k[3]->setDescription("SMAC parameter k3 for radial lens distortion; (df/f) + (k1+df/f)*r^3 + (k2+df/f)^r5 + (k3+df/f)^r^7 + ...");
		k[3]->set(k0Tok3[3]);
		k[3]->setDelta(1.0e-12);

		// Tangential lens distortion
		auto& p = this->smacPtr;

		p[4]->setName(constant::smac::_p1_);
		p[4]->setDescription("SMAC parameter p1 for decentering lens distortion; dx = (1 + p3^2*r^2)(p1(r^2 + 2x^2) + 2p2*xy); dy = (1 + p3^2*r^2)(p2(r^2 + 2y^2) + 2p1*xy)");
		p[4]->set(p1Top3[0]);
		p[4]->setDelta(1.0e-12);

		p[5]->setName(constant::smac::_p2_);
		p[5]->setDescription("SMAC parameter p2 for decentering lens distortion; dx = (1 + p3^2*r^2)(p1(r^2 + 2x^2) + 2p2*xy); dy = (1 + p3^2*r^2)(p2(r^2 + 2y^2) + 2p1*xy)");
		p[5]->set(p1Top3[1]);
		p[5]->setDelta(1.0e-12);

		p[6]->setName(constant::smac::_p3_);
		p[6]->setDescription("SMAC parameter p3 for decentering lens distortion; dx = (1 + p3^2*r^2)(p1(r^2 + 2x^2) + 2p2*xy); dy = (1 + p3^2*r^2)(p2(r^2 + 2y^2) + 2p1*xy)");
		p[6]->set(p1Top3[2]);
		p[6]->setDelta(1.0e-12);
	}

	SMACModel::SMACModel(const std::vector<std::shared_ptr<param::Parameter>>& parameters)
	{
		this->smacPtr.clear();

		std::shared_ptr<param::Parameter> found;
		if (param::findParameter(parameters, constant::smac::_k0_, found))
			this->smacPtr.push_back(found);
		else
			throw std::runtime_error("Failed in finding a parameter");
		
		if (param::findParameter(parameters, constant::smac::_k1_, found))
			this->smacPtr.push_back(found);
		else
			throw std::runtime_error("Failed in finding a parameter");
		
		if (param::findParameter(parameters, constant::smac::_k2_, found))
			this->smacPtr.push_back(found);
		else
			throw std::runtime_error("Failed in finding a parameter");
		
		if (param::findParameter(parameters, constant::smac::_k3_, found))
			this->smacPtr.push_back(found);
		else
			throw std::runtime_error("Failed in finding a parameter");
		
		if (param::findParameter(parameters, constant::smac::_p1_, found))
			this->smacPtr.push_back(found);
		else
			throw std::runtime_error("Failed in finding a parameter");
		
		if (param::findParameter(parameters, constant::smac::_p2_, found))
			this->smacPtr.push_back(found);
		else
			throw std::runtime_error("Failed in finding a parameter");
		
		if (param::findParameter(parameters, constant::smac::_p3_, found))
			this->smacPtr.push_back(found);
		else
			throw std::runtime_error("Failed in finding a parameter");
	}

	SMACModel::SMACModel(const SMACModel& copy)
	{
		copyParameterValues(copy.smacPtr);
	}

	void SMACModel::operator = (const SMACModel& copy)
	{
		copyParameterValues(copy.smacPtr);
	}

	std::string SMACModel::getModelName()
	{
		return std::string(constant::distmdlnames::_smac_);
	}

	std::vector<std::shared_ptr<param::Parameter>>& SMACModel::getParameters()
	{
		return this->smacPtr;
	}

	void SMACModel::setParameters(const std::vector<std::shared_ptr<param::Parameter>>& collection)
	{
		this->smacPtr = collection;
	}

	data::Point2D SMACModel::getDistortion(const data::Point2D& measuredPhotoCoord)
	{
		data::Point2D distortion(0.0);

		double x = static_cast<double>(measuredPhotoCoord(0));
		double y = static_cast<double>(measuredPhotoCoord(1));
		double x2 = x*x;
		double y2 = y*y;
		double r = sqrt(x2 + y2);

		if (r < std::numeric_limits<double>::epsilon())
			return distortion;

		// Radial lens distortion
		double k[4];///k0, k1, k2, k3
		k[0] = this->smacPtr[0]->get();
		k[1] = this->smacPtr[1]->get();
		k[2] = this->smacPtr[2]->get();
		k[3] = this->smacPtr[3]->get();
		double p[3];
		p[0] = this->smacPtr[4]->get();
		p[1] = this->smacPtr[5]->get();
		p[2] = this->smacPtr[6]->get();

		double r2 = r*r;
		double r3 = r2*r;
		double r5 = r3*r2;
		double r7 = r5*r2;
		double dr = k[0] + k[1]*r3 + k[2]*r5 + k[3]*r7;
		double p32 = p[2]*p[2];///p3^2

		distortion(0) = static_cast<double>(dr * x / r + (1. + p32 * r2)*(p[0]*(r2 + 2 * x*x) + 2 * p[1]*x*y));
		distortion(1) = static_cast<double>(dr * y / r + (1. + p32 * r2)*(p[1]*(r2 + 2 * y*y) + 2 * p[0]*x*y));

		return distortion;
	}

	void SMACModel::copyParameterValues(const std::vector<std::shared_ptr<param::Parameter>>& collection)
	{
		this->smacPtr.resize(collection.size());
		
		for (auto& p : this->smacPtr)
			p = std::make_shared<param::Parameter>();

		for (unsigned int i = 0; i < collection.size(); ++i)
		{
			*(this->smacPtr[i]) = *(collection[i]);
		}
	}

	data::Point2D SMACModel::getDistortion(const data::Point2D& measuredPhotoCoord, const data::Point2D& pp)
	{
		data::Point2D coord;
		coord(0) = measuredPhotoCoord(0) - pp(0);
		coord(1) = measuredPhotoCoord(1) - pp(1);

		return getDistortion(coord);
	}
}