/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
#include <iostream>
#include <string>

#include <ssm/include/SSMConstants.h>
#include <ssm/include/SsmFrameCamera.h>
#include <ssm/include/SSMSMACModel.h>

namespace sensor
{
	/// Maximum iteration number
	const unsigned int MAX_ITER = 100;

	/// Assign names and descriptions of parameters
	void setParamNamesDscrpt(std::vector<std::shared_ptr<param::Parameter>>& cameraParams)
	{
		if(cameraParams.size() != NUM_CAM_PARAM)
			throw std::runtime_error("setParamNamesDscrpt: wrong number of camera parameters");

		cameraParams[fIdx]->setName(constant::camera::_f_);
		cameraParams[xpIdx]->setName(constant::camera::_xp_);
		cameraParams[ypIdx]->setName(constant::camera::_yp_);
		cameraParams[wIdx]->setName(constant::camera::_width_);
		cameraParams[hIdx]->setName(constant::camera::_height_);
		cameraParams[pitIdx]->setName(constant::camera::_pixel_pitch_);
		cameraParams[bsoIdx]->setName(constant::camera::_omega_bs_);
		cameraParams[bspIdx]->setName(constant::camera::_phi_bs_);
		cameraParams[bskIdx]->setName(constant::camera::_kappa_bs_);
		cameraParams[dxIdx]->setName(constant::camera::_dX_);
		cameraParams[dyIdx]->setName(constant::camera::_dY_);
		cameraParams[dzIdx]->setName(constant::camera::_dZ_);

		cameraParams[fIdx]->setDescription("Lens focal length");
		cameraParams[xpIdx]->setDescription("x of a principal point");
		cameraParams[ypIdx]->setDescription("y of a principal point");
		cameraParams[wIdx]->setDescription("Sensor width");
		cameraParams[hIdx]->setDescription("Sensor height");
		cameraParams[pitIdx]->setDescription("Pixel pitch of a detector");
		cameraParams[bsoIdx]->setDescription("boresight: omega");
		cameraParams[bspIdx]->setDescription("boresight: phi");
		cameraParams[bskIdx]->setDescription("boresight: kappa");
		cameraParams[dxIdx]->setDescription("lever-arm: dX");
		cameraParams[dyIdx]->setDescription("lever-arm: dY");
		cameraParams[dzIdx]->setDescription("lever-arm: dZ");
	}

	FrameCamera::FrameCamera()
	{
		initParameters();
	}

	FrameCamera::FrameCamera(const unsigned int cameraId,
		const double focalLength,
		const data::Point2D& xpyp,
		const unsigned int w,
		const unsigned int h,
		const double pixelPitch,
		const std::shared_ptr<DistortionModel> distortion)
		: distModel(distortion),
		id(cameraId)
	{
		if (focalLength < std::numeric_limits<double>::epsilon())
			throw std::runtime_error("Given focal length is wrong.");

		if (w < 1 || h < 1)
			throw std::runtime_error("Given width and/or height are wrong.");

		if (pixelPitch < std::numeric_limits<double>::epsilon()) 
			throw std::runtime_error("Given pixel size is wrong.");
		
		/// Distortion parameters
		this->distParams = this->distModel->getParameters();

		/// f, xp, yp, w, h, pixPitch
		/// lever-arm(3), boresight(3)
		this->cameraParams.resize(NUM_CAM_PARAM);
		for (auto& p : this->cameraParams)
			p = std::make_shared<param::Parameter>();

		/// Assign parameter names and descriptions
		setParamNamesDscrpt(this->cameraParams);

		/// Zero(default) values to all parameters
		for (auto& p : this->cameraParams)
			p->set(0.0);

		/// 1.0e-6 delta to all parameters
		for (auto& p : this->cameraParams)
			p->setDelta(1.0e-6);
		
		this->cameraParams[fIdx]->set(focalLength);
		this->cameraParams[xpIdx]->set(xpyp(0));
		this->cameraParams[ypIdx]->set(xpyp(1));
		this->cameraParams[wIdx]->set(static_cast<double>(w));
		this->cameraParams[hIdx]->set(static_cast<double>(h));
		this->cameraParams[pitIdx]->set(pixelPitch);

		
	}

	FrameCamera::FrameCamera(const std::vector<std::shared_ptr<param::Parameter>> params, const std::shared_ptr<DistortionModel> distortion)
	{
		this->distModel = distortion;
		this->cameraParams.resize(NUM_CAM_PARAM);
		collectCameraParameters(params);
		this->distParams = this->distModel->getParameters();
	}

	FrameCamera::FrameCamera(const std::vector<std::shared_ptr<param::Parameter>>& parameters)
	{
		/// Camera parameters
		this->cameraParams.resize(NUM_CAM_PARAM);
		for (auto& p : this->cameraParams)
			p = std::make_shared<param::Parameter>();

		collectCameraParameters(parameters);

		/// Distortion model
		std::shared_ptr<sensor::SMACModel> smac(new sensor::SMACModel(parameters));
		this->distModel = std::static_pointer_cast<sensor::DistortionModel>(smac);

		/// Distortion parameters
		this->distParams = this->distModel->getParameters();
	}

	void FrameCamera::initParameters()
	{
		id = 0;

		/// As default, assign a smac model with zero-set parameters (no distortion)
		this->distModel.reset();
		this->distModel = std::make_shared<sensor::SMACModel>();
		this->distParams = this->distModel->getParameters();

		/// f, xp, yp, w, h, pixPitch
		/// lever-arm(3), boresight(3)
		this->cameraParams.resize(NUM_CAM_PARAM);
		for (auto& p : this->cameraParams)
			p = std::make_shared<param::Parameter>();

		setParamNamesDscrpt(this->cameraParams);
	}

	/** Get the refined(undistorted) photo coordinates from the distorted photo coordinates.*/
	data::Point2D FrameCamera::getRefinedPhotoCoord(const data::Point2D& distortedPhotoCoord) const
	{
		/// Origin shift using the principal point
		//data::Point2D photoCoord0 = distortedPhotoCoord - getPP(); <--- Do not shift origin, remove only distortion here

		/// Get distortion
		data::Point2D distortion = distModel->getDistortion(distortedPhotoCoord);

		data::Point2D refinedPhotoCoord = distortedPhotoCoord + distortion;

		return refinedPhotoCoord;
	}

	/** Get the refined(undistorted) photo coordinates from the distorted photo coordinates.*/
	data::Point2D FrameCamera::getRefinedPhotoCoord(const std::vector<std::shared_ptr<param::Parameter>>& distortedPhotoCoord) const
	{
		data::Point2D distortedPhotoCoord0;
		distortedPhotoCoord0(0) = distortedPhotoCoord[0]->get();
		distortedPhotoCoord0(1) = distortedPhotoCoord[1]->get();

		return this->getRefinedPhotoCoord(distortedPhotoCoord0);
	}

	/** [Photo to image] transformation between image coordinate system and photo coordinate system, (row, col) and (x, y) */
	data::Point2D FrameCamera::getPhoto2ImgCoord(const data::Point2D& photoCoord) const
	{
		data::Point2D imgCoord;
		/// distorted photo coordinate to distorted image coordinates
		const double pixPitch = getPixPitch();
		auto wh = this->getSensorSize();
		double width = static_cast<double>(wh(0));
		double height = static_cast<double>(wh(1));
		/// Shift an origin to the center of an image
		auto pt = photoCoord + getPP();
		imgCoord(0) = pt(0) / pixPitch + (width / 2.0);
		imgCoord(1) = (height / 2.0) - pt(1) / pixPitch;

		return imgCoord;
	}

	/** [Photo to image] transformation between image coordinate system and photo coordinate system, (row, col) and (x, y) */
	data::Point2D FrameCamera::getPhoto2ImgCoord(const std::vector<std::shared_ptr<param::Parameter>>& photoCoord) const
	{
		data::Point2D pt;
		pt(0) = photoCoord[0]->get();
		pt(1) = photoCoord[1]->get();

		return getPhoto2ImgCoord(pt);
	}

	/** [Image to photo] transformation between image coordinate system and photo coordinate system, (row, col) and (x, y) */
	data::Point2D FrameCamera::getImg2PhotoCoord(const data::Point2D& imgCoord) const
	{
		const double pixPitch = getPixPitch();
		auto wh = this->getSensorSize();
		double width = static_cast<double>(wh(0));
		double height = static_cast<double>(wh(1));
		/// Distorted image coordinate to distorted photo coordinates
		data::Point2D photoCoord;
		photoCoord(0) = (imgCoord(0) - (width / 2.0)) * pixPitch;
		photoCoord(1) = ((height / 2.0) - imgCoord(1)) * pixPitch;
		/// Shift an origin to P.P (perspective center)
		auto pp = getPP();
		photoCoord = photoCoord - pp;
		return photoCoord;
	}

	/** get refined(undistorted) photo coordinates from distorted image coordinates.*/
	data::Point2D FrameCamera::getRefinedPhotoCoordFromImgCoord(const data::Point2D& distortedImgCoord) const
	{
		data::Point2D distortedPhotoCoord = getImg2PhotoCoord(distortedImgCoord);
		data::Point2D refinedPhotoCoord = getRefinedPhotoCoord(distortedPhotoCoord);

		return refinedPhotoCoord;
	}

	/** Get the distorted  photo coordinates from the refined(undistorted) photo coordinates.*/
	data::Point2D FrameCamera::getDistortedPhotoCoordFromRefinedPhotoCoord(const std::shared_ptr<sensor::DistortionModel>& distModel, const data::Point2D& refinedPhotoCoord0)
	{
		bool bContinue = true;
		unsigned int count = 0;

		data::Point2D estimatedDistortion;
		estimatedDistortion(0) = estimatedDistortion(1) = 0.;

		/// Approximate distortion and estimated distorted coordinates
		data::Point2D distortedPhotoCoord0 = refinedPhotoCoord0;

		/// Iterative distortion estimation
		do {
			/// New estimated distortion
			estimatedDistortion = distModel->getDistortion(distortedPhotoCoord0);

			/// New estimated distorted photo coordinates
			data::Point2D distortedPhotoCoord1 = refinedPhotoCoord0 - estimatedDistortion;

			/// Estimation difference
			data::Point2D diff = distortedPhotoCoord1 - distortedPhotoCoord0;
			auto diff_r = sqrt(diff(0) * diff(0) + diff(1) * diff(1));

			if (diff_r < pixTolerance)
			{
				bContinue = false;
			}

			distortedPhotoCoord0 = distortedPhotoCoord1;

			++count;

			if (count > MAX_ITER)
				throw std::runtime_error("Endless iteration in getDistortedPhotoCoordFromRefinedPhotoCoord");

		} while (bContinue);

		/// Adopt only distortion, not principal point
		//data::Point2D distortedPhotoCoord = refinedPhotoCoord0 + getPP() - estimatedDistortion; <--- Old  definition
		data::Point2D distortedPhotoCoord = refinedPhotoCoord0 - estimatedDistortion; /// <--- New definition

		return distortedPhotoCoord;
	}

	/** Get the distorted photo coordinates from the refined(undistorted) photo coordinates.*/
	data::Point2D FrameCamera::getDistortedPhotoCoordFromRefinedPhotoCoord(const data::Point2D& refinedPhotoCoord0) const
	{
		data::Point2D distPhoPt;
		
		try
		{
			distPhoPt = getDistortedPhotoCoordFromRefinedPhotoCoord(this->distModel, refinedPhotoCoord0);
		}
		catch (std::runtime_error err)
		{
			throw err;
		}
		catch (...)
		{
			throw std::runtime_error("FrameCamera::getDistortedPhotoCoordFromRefinedPhotoCoord");
		}

		return distPhoPt;
;
	}

	/** Get the distorted image coordinates from the refined(undistorted) photo coordinates.*/
	data::Point2D FrameCamera::getDistortedImgCoordFromRefinedPhotoCoord(const data::Point2D& refinedPhotoCoord) const
	{
		data::Point2D distortedPhotoCoord = getDistortedPhotoCoordFromRefinedPhotoCoord(refinedPhotoCoord);
		return getPhoto2ImgCoord(distortedPhotoCoord);
	}

	/** Get the distorted image coordinates from the refined(undistorted) photo coordinates.*/
	std::vector<data::Point2D> FrameCamera::getDistortedImgCoordFromRefinedPhotoCoord(const std::vector<data::Point2D>& refinedPhotoCoord) const
	{
		std::vector<data::Point2D> distortedImgPts;
		distortedImgPts.reserve(refinedPhotoCoord.size());
		for (const data::Point2D& phoPt : refinedPhotoCoord)
		{
			distortedImgPts.push_back(getDistortedImgCoordFromRefinedPhotoCoord(phoPt));
		}

		return distortedImgPts;
	}

	/** Get the distorted image coordinates from the refined(undistorted) photo coordinates.*/
	/// Note for future works:
	/// It supports only smac now, and this function does not belong to interface functions.
	/// For more generalization, it should be modified for accepting camera and distortion interfaces instead of instances.
	data::Point2D FrameCamera::getDistortedImgCoordFromRefinedPhotoCoord(const std::vector<std::shared_ptr<param::Parameter>>& distParams,
		const data::Point2D& principalPoint,
		unsigned int width,
		unsigned int height,
		const double pixPitch,
		const data::Point2D& refinedPhotoCoord0)
	{
		/// set the given distortion model parameters to this camera
		std::shared_ptr<sensor::DistortionModel> distModel(new sensor::SMACModel(distParams));

		data::Point2D distortedPhotoCoord;

		try
		{
			distortedPhotoCoord = getDistortedPhotoCoordFromRefinedPhotoCoord(distModel, refinedPhotoCoord0);
		}
		catch (std::runtime_error err)
		{
			throw err;
		}
		catch (...)
		{
			throw std::runtime_error("FrameCamera::getDistortedImgCoordFromRefinedPhotoCoord");
		}

		/// Photo 2 image coordinates
		auto pt = distortedPhotoCoord + principalPoint; /// shift origin to the center of an image
		data::Point2D distortedImgCoord;
		distortedImgCoord(0) = pt(0) / pixPitch + (width / 2.0);
		distortedImgCoord(1) = (height / 2.0) - pt(1) / pixPitch;

		return distortedImgCoord;
	}

	/** Get the distorted image coordinates from the refined(undistorted) photo coordinates.*/
	/// Note for future works:
	/// It supports only smac now, and this function does not belong to interface functions.
	/// For more generalization, it should be modified for accepting camera and distortion interfaces instead of instances.
	data::Point2D FrameCamera::getDistortedImgCoordFromRefinedPhotoCoord(const std::vector<std::shared_ptr<param::Parameter>>& camParams,
		const std::vector<std::shared_ptr<param::Parameter>>& distParams,
		const data::Point2D& refinedPhotoCoord0)
	{
		data::Point2D refinedPhotoCoord;
		refinedPhotoCoord(0) = refinedPhotoCoord0(0);
		refinedPhotoCoord(1) = refinedPhotoCoord0(1);

		/// set the given distortion model parameters to this camera
		std::shared_ptr<sensor::DistortionModel> distModel(new sensor::SMACModel(distParams));

		/// get distorted photo coordinates
		data::Point2D distortedPhotoCoord;

		try
		{
			distortedPhotoCoord = getDistortedPhotoCoordFromRefinedPhotoCoord(distModel, refinedPhotoCoord0);
		}
		catch (std::runtime_error err)
		{
			throw err;
		}
		catch (...)
		{
			throw std::runtime_error("FrameCamera::getDistortedImgCoordFromRefinedPhotoCoord");
		}

		data::Point2D pp;
		try
		{
			pp(0) = camParams[param::findIndex(camParams, constant::camera::_xp_)]->get();
			pp(1) = camParams[param::findIndex(camParams, constant::camera::_yp_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a camera xp and yp parameters indices");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		const double pixPitch = param::get(camParams, constant::camera::_pixel_pitch_);
		const double width = param::get(camParams, constant::camera::_width_);
		const double height = param::get(camParams, constant::camera::_height_);

		/// Photo 2 image coordinates
		auto pt = distortedPhotoCoord + pp; /// shift origin to the center of an image
		data::Point2D distortedImgCoord;
		distortedImgCoord(0) = pt(0) / pixPitch + (width / 2.0);
		distortedImgCoord(1) = (height / 2.0) - pt(1) / pixPitch;

		return distortedImgCoord;
	}

	/** Get the distorted image coordinates from the refined(undistorted) photo coordinates.*/
	/// Note for future works:
	/// It supports only smac now, and this function does not belong to interface functions.
	/// For more generalization, it should be modified for accepting camera and distortion interfaces instead of instances.
	data::Point2D FrameCamera::getDistortedImgCoordFromRefinedPhotoCoord(const std::vector<std::shared_ptr<param::Parameter>>& parameters, const data::Point2D& refinedPhotoCoord0)
	{
		data::Point2D refinedPhotoCoord;
		refinedPhotoCoord(0) = refinedPhotoCoord0(0);
		refinedPhotoCoord(1) = refinedPhotoCoord0(1);

		/// set the given distortion model parameters to this camera
		std::shared_ptr<sensor::DistortionModel> distModel(new sensor::SMACModel(parameters));

		/// get distorted photo coordinates
		data::Point2D distortedPhotoCoord;

		try
		{
			distortedPhotoCoord = getDistortedPhotoCoordFromRefinedPhotoCoord(distModel, refinedPhotoCoord);
		}
		catch (std::runtime_error err)
		{
			throw err;
		}
		catch (...)
		{
			throw std::runtime_error("FrameCamera::getDistortedImgCoordFromRefinedPhotoCoord");
		}

		data::Point2D pp;
		try
		{
			pp(0) = parameters[param::findIndex(parameters, constant::camera::_xp_)]->get();
			pp(1) = parameters[param::findIndex(parameters, constant::camera::_yp_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a camera xp and yp parameters indices");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		const double pixPitch = param::get(parameters, constant::camera::_pixel_pitch_);
		const double width = param::get(parameters, constant::camera::_width_);
		const double height = param::get(parameters, constant::camera::_height_);

		/// Photo 2 image coordinates
		auto pt = distortedPhotoCoord + pp; /// shift origin to the center of an image
		data::Point2D distortedImgCoord;
		distortedImgCoord(0) = pt(0) / pixPitch + (width / 2.0);
		distortedImgCoord(1) = (height / 2.0) - pt(1) / pixPitch;

		return distortedImgCoord;
	}

	/**
	*@brief Get refined photo coordinates from distorted image coordinates.
	*@Note for future works:<br>
	*@It supports only smac now, and this function does not belong to interface functions.<br>
	*@For more generalization, it should be modified for accepting camera and distortion interfaces instead of instances.
	*@param collections: parameter collections for camera and distortion parameters
	*@param distortedImgCoord0: distorted (observed) image coordinates as input data
	*@return refined photo coordinates
	*/
	data::Point2D FrameCamera::getRefinedPhotoCoordFromDistortedImgCoord(const std::vector<std::shared_ptr<param::Parameter>>& parameters, const data::Point2D& distortedImgCoord0)
	{
		const double pixPitch = param::get(parameters, constant::camera::_pixel_pitch_);
		const double width = param::get(parameters, constant::camera::_width_);
		const double height = param::get(parameters, constant::camera::_height_);

		/// Principal point
		data::Point2D pp;
		pp(0) = param::get(parameters, constant::camera::_xp_);
		pp(1) = param::get(parameters, constant::camera::_yp_);

		/// distorted image coordinate to distorted photo coordinates
		data::Point2D DistortedPhotoCoord0;
		DistortedPhotoCoord0(0) = (distortedImgCoord0(0) - (width / 2.0)) * pixPitch;
		DistortedPhotoCoord0(1) = (distortedImgCoord0(1) - (height / 2.0)) * (-pixPitch);
		/// Origin shift using the principal point
		DistortedPhotoCoord0 = DistortedPhotoCoord0 - pp;

		/// set the given distortion model parameters to this camera
		std::shared_ptr<sensor::DistortionModel> distModel(new sensor::SMACModel(parameters));

		/// Get distortion
		data::Point2D distortion = distModel->getDistortion(DistortedPhotoCoord0);

		data::Point2D refinedPhotoCoord = DistortedPhotoCoord0 + distortion;

		return refinedPhotoCoord;
	}

	/// Get a focal length
	double FrameCamera::getFL() const
	{
		try
		{
			return this->cameraParams[param::findIndex(this->cameraParams, constant::camera::_f_)]->get();
		}
		catch (...)
		{
			std::string outmsg;
			outmsg = "Error: cannot find parameter named by";
			outmsg += std::string(constant::camera::_f_) + std::string("\n");
			std::cout << outmsg;
			throw std::runtime_error(outmsg);
		}
	}

	/// Set a focal length
	void FrameCamera::setFL(const double focalLength)
	{
		param::set(this->cameraParams, constant::camera::_f_, focalLength);
	}

	/// Set a focal length parameter
	void FrameCamera::setFL(const param::Parameter& focalLength)
	{
		*cameraParams[param::findIndex(cameraParams, constant::camera::_f_)] = focalLength;
	}

	/// Get a principal point
	data::Point2D FrameCamera::getPP() const
	{
		/// Principal point
		data::Point2D pp;
		pp(0) = param::get(this->cameraParams, constant::camera::_xp_);
		pp(1) = param::get(this->cameraParams, constant::camera::_yp_);

		return pp;
	}

	/// Set a principal point
	void FrameCamera::setPP(const data::Point2D& principalPoint)
	{
		try
		{
			///xp
			this->setXp(principalPoint(0));
			///yp
			this->setYp(principalPoint(1));
		}
		catch (...)
		{
			throw std::runtime_error("Error in FrameCamera::setPP");
		}
	}

	/// Set a principal point
	void FrameCamera::setPP(const std::vector<param::Parameter>& principalPoint)
	{
		try
		{
			///xp
			*cameraParams[param::findIndex(cameraParams, constant::camera::_xp_)] = principalPoint[0];
			///yp
			*cameraParams[param::findIndex(cameraParams, constant::camera::_yp_)] = principalPoint[1];
		}
		catch (...)
		{
			throw std::runtime_error("Error in FrameCamera::setPP");
		}
	}

	/// Get a sensor size(width and height)
	math::VectorMat<unsigned int, 2> FrameCamera::getSensorSize()  const
	{
		math::VectorMat<unsigned int, 2> sensorSize;
		sensorSize(0) = static_cast<unsigned int>(this->cameraParams[wIdx]->get());
		sensorSize(1) = static_cast<unsigned int>(this->cameraParams[hIdx]->get());
		return sensorSize;
	}
	
	/// Get a sensor size(width and height)
	void FrameCamera::setSensorSize(const unsigned int width, const unsigned int height)
	{
		param::set(this->cameraParams, constant::camera::_width_, static_cast<double>(width));
		param::set(this->cameraParams, constant::camera::_height_, static_cast<double>(height));
	}

	/// Set Sensor Size
	void FrameCamera::setSensorSize(const param::Parameter width, const param::Parameter height)
	{
		*cameraParams[param::findIndex(cameraParams, constant::camera::_width_)] = width;
		*cameraParams[param::findIndex(cameraParams, constant::camera::_height_)] = height;
	}

	/// Get a pixel pitch
	double FrameCamera::getPixPitch() const
	{
		return this->cameraParams[pitIdx]->get();
	}

	/// Set a pixel pitch
	void FrameCamera::setPixPitch(const double pixelPitch)
	{
		this->cameraParams[pitIdx]->set(pixelPitch);
	}

	/**setPixPitch
	*@brief Set a pixel pitch (pixel interval size)
	*@param const parameter pixelPitch: pixel pitch
	*@return void
	*/
	void FrameCamera::setPixPitch(const param::Parameter& pixelPitch)
	{
		*cameraParams[param::findIndex(cameraParams, constant::camera::_pixel_pitch_)] = pixelPitch;
	}

	/**getLeverArm
		*@brief Get lever-arm (dx, dy, dz)
		*@return Point3D: lever-arm
		*/
	data::Point3D FrameCamera::getLeverArm() const
	{
		data::Point3D la;
		la(0) = param::get(this->cameraParams, constant::camera::_dX_);
		la(1) = param::get(this->cameraParams, constant::camera::_dY_);
		la(2) = param::get(this->cameraParams, constant::camera::_dZ_);

		return la;
	}

	/**setLeverArm
	*@brief Set lever-arm
	*@param Point3D: lever-arm (dx, dy, dz)
	*@return void
	*/
	void FrameCamera::setLeverArm(const data::Point3D& la)
	{
		param::set(this->cameraParams, constant::camera::_dX_, la(0));
		param::set(this->cameraParams, constant::camera::_dY_, la(1));
		param::set(this->cameraParams, constant::camera::_dZ_, la(2));
	}

	/**setLeverArm
	*@brief Set lever-arm
	*@param ParameterSet: lever-arm (dx, dy, dz)
	*@return void
	*/
	void FrameCamera::setLeverArm(const std::vector<param::ParameterPtr>& la)
	{
		*cameraParams[param::findIndex(cameraParams, constant::camera::_dX_)] = *la[param::findIndex(la, constant::camera::_dX_)];
		*cameraParams[param::findIndex(cameraParams, constant::camera::_dY_)] = *la[param::findIndex(la, constant::camera::_dY_)];
		*cameraParams[param::findIndex(cameraParams, constant::camera::_dZ_)] = *la[param::findIndex(la, constant::camera::_dZ_)];
	}

	/**getBoresight
	*@brief Get boresight
	*@return EulerAngles (omega, phi, kappa)
	*/
	data::EulerAngles FrameCamera::getBoresight() const
	{
		data::EulerAngles bs;
		bs(0) = param::get(this->cameraParams, constant::camera::_omega_bs_);
		bs(1) = param::get(this->cameraParams, constant::camera::_phi_bs_);
		bs(2) = param::get(this->cameraParams, constant::camera::_kappa_bs_);

		return bs;
	}

	/**setBoresight
	*@brief Set boresight
	*@param EulerAngles (omega, phi, kappa)
	*@return void
	*/
	void FrameCamera::setBoresight(const data::EulerAngles& bs)
	{
		param::set(this->cameraParams, constant::camera::_omega_bs_, bs(0));
		param::set(this->cameraParams, constant::camera::_phi_bs_, bs(1));
		param::set(this->cameraParams, constant::camera::_kappa_bs_, bs(2));
	}

	/**setBoresight
	*@brief Set boresight
	*@param ParameterSet (omega, phi, kappa)
	*@return void
	*/
	void FrameCamera::setBoresight(const std::vector<param::ParameterPtr>& bs)
	{
		*cameraParams[param::findIndex(cameraParams, constant::camera::_omega_bs_)]	= *bs[param::findIndex(bs, constant::camera::_omega_bs_)];
		*cameraParams[param::findIndex(cameraParams, constant::camera::_phi_bs_)]		= *bs[param::findIndex(bs, constant::camera::_phi_bs_)];
		*cameraParams[param::findIndex(cameraParams, constant::camera::_kappa_bs_)]	= *bs[param::findIndex(bs, constant::camera::_kappa_bs_)];
	}

	/// Get a distortion model
	std::shared_ptr<sensor::DistortionModel> FrameCamera::getDistortionModel() const
	{
		return this->distModel;
	}

	/// Get a distortion model
	void FrameCamera::setDistortionModel(const std::shared_ptr<sensor::DistortionModel> newDistModel)
	{
		this->distModel = newDistModel;
		this->distParams = this->distModel->getParameters();
	}

	std::vector<std::shared_ptr<param::Parameter>> FrameCamera::getParameters()
	{
		std::vector<std::shared_ptr<param::Parameter>> allParams;

		for (auto& p : this->cameraParams)
		{
			allParams.push_back(p);
		}
		
		for (auto& p : this->distModel->getParameters())
		{
			allParams.push_back(p);
		}

		return allParams;
	}

	void FrameCamera::setParameters(const std::vector<std::shared_ptr<param::Parameter>>& collection)
	{
		for (auto& p : collection)
		{
			std::shared_ptr<param::Parameter> found;

			if (param::findParameter(this->cameraParams, p->getName(), found))
			{
				found = p;
				continue;
			}

			if (param::findParameter(this->distModel->getParameters(), p->getName(), found))
			{
				found = p;
				continue;
			}
		}

		/// Is may not required because parameter smart point is already shared between distModel and distParams in this class.
		/// Check it later.
		this->distParams = this->distModel->getParameters();
	}

	std::vector<std::shared_ptr<param::Parameter>>& FrameCamera::getCamParameters()
	{
		return this->cameraParams;
	}

	std::vector<std::shared_ptr<param::Parameter>>& FrameCamera::getDistParameters()
	{
		return this->distParams;
	}

	unsigned int FrameCamera::getId() const
	{
		return id;
	}

	void FrameCamera::setId(const unsigned int newId)
	{
		this->id = newId;
	}

	/// Get the x coordinate of a principal point
	double FrameCamera::getXp() const
	{
		return param::get(this->cameraParams, constant::camera::_xp_);
	}

	/// Get the y coordinate of a principal point
	double FrameCamera::getYp() const
	{
		return param::get(this->cameraParams, constant::camera::_yp_);
	}

	/// Set the x coordinate of a principal point
	void FrameCamera::setXp(const double newXp)
	{
		param::set(this->cameraParams, constant::camera::_xp_, newXp);
	}

	/// Set the y coordinate of a principal point
	void FrameCamera::setYp(const double newYp)
	{
		param::set(this->cameraParams, constant::camera::_yp_, newYp);
	}

	/// Collect camera parameters from a parameter array
	void FrameCamera::collectCameraParameters(const std::vector<std::shared_ptr<param::Parameter>>& parameters)
	{
		bool retval;

		this->cameraParams[fIdx] = param::findParameter(parameters, constant::camera::_f_, retval);
		if(!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[xpIdx] = param::findParameter(parameters, constant::camera::_xp_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[ypIdx] = param::findParameter(parameters, constant::camera::_yp_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[wIdx] = param::findParameter(parameters, constant::camera::_width_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[hIdx] = param::findParameter(parameters, constant::camera::_height_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[pitIdx] = param::findParameter(parameters, constant::camera::_pixel_pitch_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[bsoIdx] = param::findParameter(parameters, constant::camera::_omega_bs_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[bspIdx] = param::findParameter(parameters, constant::camera::_phi_bs_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[bskIdx] = param::findParameter(parameters, constant::camera::_kappa_bs_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[dxIdx] = param::findParameter(parameters, constant::camera::_dX_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[dyIdx] = param::findParameter(parameters, constant::camera::_dY_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");

		this->cameraParams[dzIdx] = param::findParameter(parameters, constant::camera::_dZ_, retval);
		if (!retval) throw std::runtime_error("Failed in finding a parameter");
	}
}