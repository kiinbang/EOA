/*
* Copyright(c) 2000-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

//////////////////////////
//RONonlinear.cpp(Non Linear Collinearity EQ)
//made by BbaraB
//revision date 2000-5-17
//revision date 2000-5-20
//revision date 2000-5-21
//revision date 2000-5-24
//revision date 2000-5-29
//revision date 2000-5-31
//revision date 2020-1-16
//////////////////////////

#pragma warning(disable: 4996)

#include <ssm/include/RONonlinear.h>

#include <iostream>

#include <ssm/SSMParallaxEq.h>
#include <ssm/include/SSMRotation.h>

//////////
//Photo
//////////
CPhoto::CPhoto()
{
}

CPhoto::~CPhoto()
{
}

/////////
//Model
/////////
//Relative Orientation
CModel::CModel()
{
	maxIteration = 10;			//Iteration Limit
	maxCorrection = 0.000001;	//Maximum Correction Limit
	maxSD = 0.000001;
	diffVar = 0.0;
	GaApproximation_Option = PARALLAXEQ;	//model space 3D-coord approximation option
	ParallaxEQ_Option = TWOPHOTOMEAN;		//parallax Equation option
		
}
CModel::CModel(char* infile)
{
	char st[100];
	FILE* indata;
	int i;
	//file open
	indata = fopen(infile,"r");
	//trash memory
	fgets(st,100,indata);

	fgets(st,100,indata);
	//Left Photo PPA, PPBS
	double x, y;
	fscanf(indata, "%lf", &x);
	fscanf(indata, "%lf", &y);
	LPhoto.camera.PPA(0) = x;
	LPhoto.camera.PPA(1) = y;
	fscanf(indata, "%lf", &x);
	fscanf(indata, "%lf", &y);
	LPhoto.camera.PPBS(0) = x;
	LPhoto.camera.PPBS(1) = y;
	fgets(st,100,indata);

	fgets(st,100,indata);
	//Left Photo Focal Length
	fscanf(indata,"%lf",&LPhoto.camera.f);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Left Photo number of RO Point
	fscanf(indata,"%d",&LPhoto.num_ROP);
	fgets(st,100,indata);

	fgets(st,100,indata);
	
	LPhoto.ROPoint.resize(LPhoto.num_ROP);
	//Left Photo RO Point Coordinate
	for(i=0;i<LPhoto.num_ROP;i++)
	{
		fscanf(indata, "%lf", &x);
		fscanf(indata, "%lf", &y);
		LPhoto.ROPoint[i](0) = x;
		LPhoto.ROPoint[i](1) = y;
	}
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Right Photo PPA, PPBS
	fscanf(indata, "%lf", &x);
	fscanf(indata, "%lf", &y);
	RPhoto.camera.PPA(0) = x;
	RPhoto.camera.PPA(1) = y;
	fscanf(indata, "%lf", &x);
	fscanf(indata, "%lf", &y);
	RPhoto.camera.PPBS(0) = x;
	RPhoto.camera.PPBS(1) = y;
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Right Photo Focal Length
	fscanf(indata,"%lf",&RPhoto.camera.f);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Right Photo number of RO Point
	fscanf(indata,"%d",&RPhoto.num_ROP);
	fgets(st,100,indata);

	fgets(st,100,indata);
	//Right Photo RO Point Coordinate
	RPhoto.ROPoint.resize(RPhoto.num_ROP);
	for(i=0;i<RPhoto.num_ROP;i++)
	{
		fscanf(indata, "%lf", &x);
		fscanf(indata, "%lf", &y);
		RPhoto.ROPoint[i](0) = x;
		RPhoto.ROPoint[i](1) = y;
	}
	fgets(st,100,indata);

	fgets(st,100,indata);
	fgets(st,100,indata);
	//Maximum Iteration
	fscanf(indata,"%d",&maxIteration);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Correction Limit
	fscanf(indata,"%lf",&maxCorrection);
	fgets(st,100,indata);

	fgets(st,100,indata);
	//Standard Deviation Limit
	fscanf(indata,"%lf",&maxSD);
	fgets(st,100,indata);

	fgets(st,100,indata);
	//Difference Variance(Old_Var - New_Var)
	fscanf(indata,"%lf",&maxdiffVar);
	fgets(st,100,indata);

	fgets(st,100,indata);
	fgets(st,100,indata);
	fscanf(indata,"%d",&GaApproximation_Option);
	fgets(st,100,indata);

	fgets(st,100,indata);
	fgets(st,100,indata);
	fscanf(indata,"%d",&ParallaxEQ_Option);
	fgets(st,100,indata);

	fclose(indata);

}
CModel::CModel(std::string modelID, CPhoto L, CPhoto R, int maxi, double correction, double sd, double diffvar)
{
	ID = modelID;				//Model ID
	LPhoto = L;					//Left Photo
	RPhoto = R;					//Right Photo
	maxIteration = maxi;		//Iteration Limit(default=10)
	maxCorrection = correction;	//Maximum Correction Limit(default=0.000001)
	maxSD = sd;					//Maximum Standard Deviation Limit(default=0.000001)
	maxdiffVar = diffvar;
	GaApproximation_Option = PARALLAXEQ;	//model space 3D-coord approximation option
	ParallaxEQ_Option = TWOPHOTOMEAN;		//parallax Equation option
}
CModel::~CModel()
{
	
}

//Dependent Relative Orientation
bool CModel::RO_dependent(void)
{
	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	bool increase_old = false,increase_new = false;	//monitoring variance
	bool iteration_end = false;						//Iteration Stop
	//좌우 ?�진???�호?�정??�?�� 검??
	if(LPhoto.num_ROP == RPhoto.num_ROP)
	{
		num_ROPoint = LPhoto.num_ROP;
		ROmat.DF = num_ROPoint - 5;
	}
	else
	{
		return false;
	}

	//Initialize parameter
	InitApp_de();
	result = "[Relative Orientation(Dependent)]\r\n\r\n\r\n";
	//Start Iteration
	do
	{
		std::string temp;
		iteration++;					//Increase Iteration Number(From 1)
		increase_old = increase_new;
		
		//make matrix A & L
		makeA_Ldependent();
		
		//compute X matrix
		ROmat.N = ROmat.A.transpose()%ROmat.A;
		ROmat.Ninv = ROmat.N.inverse();
		ROmat.X = ROmat.Ninv%ROmat.A.transpose()%ROmat.L;
		//compute V matrix
		ROmat.V = (ROmat.A%ROmat.X) - ROmat.L;
		//compute VTV
		ROmat.VTV = ROmat.V.transpose()%ROmat.V;
		//compute posteriori variance
		newvar = ROmat.pos_var = ROmat.VTV(0,0)/ROmat.DF;
		//compute standard deviation
		ROmat.SD = sqrt(ROmat.pos_var);
		//compute variance-covariance matrix
		ROmat.Var_Co = ROmat.Ninv*ROmat.pos_var;
		//compute variance matrix
		ROmat.Var.resize(5+num_ROPoint*3,1);
		for(i=0;i<(5+num_ROPoint*3);i++)
		{
			ROmat.Var(i,0) = ROmat.Var_Co(i,i);
		}

		Param_de.Romega += ROmat.X(0);
		Param_de.Rphi   += ROmat.X(1);
		Param_de.Rkappa += ROmat.X(2);
		Param_de.Yr     += ROmat.X(3);
		Param_de.Zr     += ROmat.X(4);
		for(i=0;i<num_ROPoint;i++)
		{
			Ga(i,0) += ROmat.X(5+i*3);
			Ga(i,1) += ROmat.X(5+i*3+1);
			Ga(i,2) += ROmat.X(5+i*3+2);
		}
		
		//maximum correction(X matrix)
		ROmat.maxX = fabs(ROmat.X(0));
		for(i=1;i<(int)ROmat.X.getRows();i++)
		{
			if(fabs(ROmat.X(i))>ROmat.maxX)
				ROmat.maxX = fabs(ROmat.X(i));
		}		
		
		//Print Result
		temp = std::string("Iteration--->") + std::to_string(iteration) + std::string("\n\n\n");
		result += temp;
		result += PrintResult_de();
		
		//reference variance(posteriori variance)monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			increase_new = true;
		}
		else
		{
			increase_new = false;
		}
		
		//iteration end condition
		//variance difference monitoring 
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = true;
			result += std::string("[Difference Between Old_Variance And New_Variance < Difference Variance limit !!!]\n");
		}
		else
		{
			oldvar = newvar;
		}
		//variance monitoring
		if( increase_old == true && increase_new == true )
		{
			iteration_end = true;
			result += std::string("[Variance Continuously(2?�연?? Increase!!!]\n");
		}
		//SD monitoring
		if(fabs(ROmat.SD)<fabs(maxSD))
		{
			iteration_end = true;
			result += std::string("[Standard Deviation < SD limit !!!]\n");
		}
		//max correction
		if(ROmat.maxX<fabs(maxCorrection))
		{
			iteration_end = true;
			result += std::string("[Maximum Correction < correction limit !!!]\n");
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = true;
			result += std::string("[Iteration Max_Num Excess!!!]\n");
		}
		

	}while(iteration_end == false);
	
	return true;
}

void CModel::InitApp_de(void)
{
	int i;
	double H,f;
	double sum = 0.0;
	for(i=0;i<num_ROPoint;i++)
	{
		sum += LPhoto.ROPoint[i](0) - RPhoto.ROPoint[i](0);
	}
	
	//Initialize RO Parameters
	Param_de.Lomega = Param_de.Lphi = Param_de.Lkappa = 0.0;
	Param_de.Xl = Param_de.Yl = 0.0;
	Param_de.Zl = LPhoto.camera.f;
	Param_de.Romega = Param_de.Rphi = Param_de.Rkappa = 0.0;
	Param_de.Xr = sum/LPhoto.num_ROP;
	Param_de.Yr = 0.0;
	Param_de.Zr = RPhoto.camera.f;
	
	//Initialize Model Space Point
	Ga.resize(num_ROPoint,3);
	H = f = (LPhoto.camera.f + RPhoto.camera.f)/2.0;
		
	switch(GaApproximation_Option)
	{
	case LPHOTOCOORDINATE:
		for(i=0;i<num_ROPoint;i++)
		{
			//Left photo coordinate
			Ga(i,0) = LPhoto.ROPoint[i](0);
			Ga(i,1) = LPhoto.ROPoint[i](1);
			Ga(i,2) = 0.0;
		}
		break;
	case RPHOTOCOORDINATE:
		for(i=0;i<num_ROPoint;i++)
		{
			//Right photo coordinate
			Ga(i,0) = RPhoto.ROPoint[i](0) + Param_de.Xr;
			Ga(i,1) = RPhoto.ROPoint[i](1);
			Ga(i,2) = 0.0;
		}
		break;
	case TWOPHOTOMEAN:
		for(i=0;i<num_ROPoint;i++)
		{
			//Two Photo mean
			Ga(i,0) = (LPhoto.ROPoint[i](0) + RPhoto.ROPoint[i](0) + Param_de.Xr)/2;
			Ga(i,1) = (LPhoto.ROPoint[i](1) + RPhoto.ROPoint[i](1))/2;
			Ga(i,2) = 0.0;
		}
		break;
	default:
		for(i=0;i<num_ROPoint;i++)
		{
			//parallax EQ
			data::Point3D ga;
			ga = ssm::ParallaxEQ(ParallaxEQ_Option,H,Param_de.Xr,f,LPhoto.ROPoint[i],RPhoto.ROPoint[i]);
			Ga(i,0) = ga(0);
			Ga(i,1) = ga(1);
			Ga(i,2) = ga(2);
		}
		break;
	}//End Switch
}

void CModel::InitApp_inde(void)
{
	int i;
	double H,f;
	double sum = 0.0;
	for(i=0;i<num_ROPoint;i++)
	{
		sum += LPhoto.ROPoint[i](0) - RPhoto.ROPoint[i](0);
	}
	
	//Initialize RO Parameters
	Param_inde.Lomega = Param_inde.Lphi = Param_inde.Lkappa = 0.0;
	Param_inde.Xl = Param_inde.Yl = 0.0;
	Param_inde.Zl = LPhoto.camera.f;
	Param_inde.Romega = Param_inde.Rphi = Param_inde.Rkappa = 0.0;
	Param_inde.Xr = sum/LPhoto.num_ROP;
	Param_inde.Yr = 0.0;
	Param_inde.Zr = RPhoto.camera.f;
	
	//Initialize Model Space Point
	Ga.resize(num_ROPoint,3);
	H = f = (LPhoto.camera.f + RPhoto.camera.f)/2.0;
		
	switch(GaApproximation_Option)
	{
	case LPHOTOCOORDINATE:
		for(i=0;i<num_ROPoint;i++)
		{
			//Left photo coordinate
			Ga(i,0) = LPhoto.ROPoint[i](0);
			Ga(i,1) = LPhoto.ROPoint[i](1);
			Ga(i,2) = 0.0;
		}
		break;
	case RPHOTOCOORDINATE:
		for(i=0;i<num_ROPoint;i++)
		{
			//Right photo coordinate
			Ga(i,0) = RPhoto.ROPoint[i](0) + Param_inde.Xr;
			Ga(i,1) = RPhoto.ROPoint[i](1);
			Ga(i,2) = 0.0;
		}
		break;
	case TWOPHOTOMEAN:
		for(i=0;i<num_ROPoint;i++)
		{
			//Two Photo mean
			Ga(i,0) = (LPhoto.ROPoint[i](0) + RPhoto.ROPoint[i](0) + Param_inde.Xr)/2;
			Ga(i,1) = (LPhoto.ROPoint[i](1) + RPhoto.ROPoint[i](1))/2;
			Ga(i,2) = 0.0;
		}
		break;
	default:
		for(i=0;i<num_ROPoint;i++)
		{
			//parallax EQ
			data::Point3D ga;
			ga = ssm::ParallaxEQ(ParallaxEQ_Option,H,Param_inde.Xr,f,LPhoto.ROPoint[i],RPhoto.ROPoint[i]);
			Ga(i,0) = ga(0);
			Ga(i,1) = ga(1);
			Ga(i,2) = ga(2);
		}
		break;
	}//End Switch
}

void CModel::makeA_Ldependent(void)
{
	int i;
	ROmat.A.resize(4*num_ROPoint,5+LPhoto.num_ROP*3,0.0);
	ROmat.L.resize(4*num_ROPoint,1,0.0);
	
	
	//Left Photo
	{
		double xo = LPhoto.camera.PPA(0), yo = LPhoto.camera.PPA(1);
		double omega = Param_de.Lomega, phi = Param_de.Lphi, kappa = Param_de.Lkappa;
		double f = LPhoto.camera.f;
		data::RotationMatrix m = math::rotation::getMMat(omega, phi, kappa); //rotation coefficient
		for(i=0;i<num_ROPoint;i++)
		{
			double deltaX,deltaY,deltaZ;
			double xa = LPhoto.ROPoint[i](0), ya = LPhoto.ROPoint[i](1);
			deltaX = Ga(i,0) - Param_de.Xl;
			deltaY = Ga(i,1) - Param_de.Yl;
			deltaZ = Ga(i,2) - Param_de.Zl;
			
			b.q = m(2,0)*deltaX + m(2,1)*deltaY + m(2,2)*deltaZ;
			b.r = m(0,0)*deltaX + m(0,1)*deltaY + m(0,2)*deltaZ;
			b.s = m(1,0)*deltaX + m(1,1)*deltaY + m(1,2)*deltaZ;
			
			b.b14 = f/(b.q*b.q)*(b.r*m(2,0) - b.q*m(0,0));
			b.b15 = f/(b.q*b.q)*(b.r*m(2,1) - b.q*m(0,1));
			b.b16 = f/(b.q*b.q)*(b.r*m(2,2) - b.q*m(0,2));

			b.J = xa - xo + (f*b.r/b.q);
	
			b.b24 = f/(b.q*b.q)*(b.s*m(2,0) - b.q*m(1,0));
			b.b25 = f/(b.q*b.q)*(b.s*m(2,1) - b.q*m(1,1));
			b.b26 = f/(b.q*b.q)*(b.s*m(2,2) - b.q*m(1,2));

			b.K = ya - yo + (f*b.s/b.q);
			//A matrix
			//Model Space Point
			ROmat.A(i*4,5+i*3) = b.b14;
			ROmat.A(i*4,5+i*3+1) = b.b15;
			ROmat.A(i*4,5+i*3+2) = b.b16;

			ROmat.A(i*4+1,5+i*3) = b.b24;
			ROmat.A(i*4+1,5+i*3+1) = b.b25;
			ROmat.A(i*4+1,5+i*3+2) = b.b26;
			//L matrix
			ROmat.L(i*4,0) = b.J;
			
			ROmat.L(i*4+1,0) = b.K;
			
		}
	}//Left Photo End

	//Right Photo	
	{
		double xo = RPhoto.camera.PPA(0), yo = RPhoto.camera.PPA(1);
		double omega = Param_de.Romega, phi = Param_de.Rphi, kappa = Param_de.Rkappa;
		double f = RPhoto.camera.f;
		data::RotationMatrix m = math::rotation::getMMat(omega, phi, kappa); //rotation coefficient
		for(i=0;i<num_ROPoint;i++)
		{
			double deltaX,deltaY,deltaZ;
			double xa = RPhoto.ROPoint[i](0), ya = RPhoto.ROPoint[i](1);
			deltaX = Ga(i,0) - Param_de.Xr;
			deltaY = Ga(i,1) - Param_de.Yr;
			deltaZ = Ga(i,2) - Param_de.Zr;
				
			b.q = m(2,0)*deltaX + m(2,1)*deltaY + m(2,2)*deltaZ;
			b.r = m(0,0)*deltaX + m(0,1)*deltaY + m(0,2)*deltaZ;
			b.s = m(1,0)*deltaX + m(1,1)*deltaY + m(1,2)*deltaZ;
		
			b.b11 = f/(b.q*b.q)*(b.r*(-m(2,2)*deltaY + m(2,1)*deltaZ) - b.q*(-m(0,2)*deltaY + m(0,1)*deltaZ));
			b.b12 = f/(b.q*b.q)*(b.r*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(-sin(phi)*cos(kappa)*deltaX + sin(omega)*cos(phi)*cos(kappa)*deltaY - cos(omega)*cos(phi)*cos(kappa)*deltaZ));
			b.b13 = -f/(b.q)*(m(1,0)*deltaX + m(1,1)*deltaY + m(1,2)*deltaZ);
			b.b14 = f/(b.q*b.q)*(b.r*m(2,0) - b.q*m(0,0));
			b.b15 = f/(b.q*b.q)*(b.r*m(2,1) - b.q*m(0,1));
			b.b16 = f/(b.q*b.q)*(b.r*m(2,2) - b.q*m(0,2));

			b.J = xa - xo + (f*b.r/b.q);

			b.b21 = f/(b.q*b.q)*(b.s*(-m(2,2)*deltaY+m(2,1)*deltaZ) - b.q*(-m(1,2)*deltaY+m(1,1)*deltaZ));
			b.b22 = f/(b.q*b.q)*(b.s*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(sin(phi)*sin(kappa)*deltaX - sin(omega)*cos(phi)*sin(kappa)*deltaY + cos(omega)*cos(phi)*sin(kappa)*deltaZ));
			b.b23 = f/(b.q)*(m(0,0)*deltaX + m(0,1)*deltaY + m(0,2)*deltaZ);
			b.b24 = f/(b.q*b.q)*(b.s*m(2,0) - b.q*m(1,0));
			b.b25 = f/(b.q*b.q)*(b.s*m(2,1) - b.q*m(1,1));
			b.b26 = f/(b.q*b.q)*(b.s*m(2,2) - b.q*m(1,2));

			b.K = ya - yo + (f*b.s/b.q);
			//A matrix
			//Rotation Angle
			ROmat.A(i*4+2,0) = b.b11;
			ROmat.A(i*4+2,1) = b.b12;
			ROmat.A(i*4+2,2) = b.b13;
			
			ROmat.A(i*4+3,0) = b.b21;
			ROmat.A(i*4+3,1) = b.b22;
			ROmat.A(i*4+3,2) = b.b23;
			//Perspective Center
			ROmat.A(i*4+2,3) = -b.b15;
			ROmat.A(i*4+2,4) = -b.b16;
			
			ROmat.A(i*4+3,3) = -b.b25;
			ROmat.A(i*4+3,4) = -b.b26;
			//Model Space Point
			ROmat.A(i*4+2,5+i*3+0) = b.b14;
			ROmat.A(i*4+2,5+i*3+1) = b.b15;
			ROmat.A(i*4+2,5+i*3+2) = b.b16;

			ROmat.A(i*4+3,5+i*3+0) = b.b24;
			ROmat.A(i*4+3,5+i*3+1) = b.b25;
			ROmat.A(i*4+3,5+i*3+2) = b.b26;
			//L matrix
			ROmat.L(i*4+2,0) = b.J;

			ROmat.L(i*4+3,0) = b.K;
		}
	}//Right Photo End

}

//Independent Relative Orientation
bool CModel::RO_independent(void)
{
	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	bool increase_old = false, increase_new = false;	//monitoring variance
	bool iteration_end = false;						//iteration stop
	if(LPhoto.num_ROP == RPhoto.num_ROP)
	{
		num_ROPoint = LPhoto.num_ROP;
		ROmat.DF = num_ROPoint - 5;
	}
	else
	{
		return false;
	}
	//Initialize parameter
	InitApp_inde();
	result = std::string("[Relative Orientation(Independent)]\n\n\n");
	//Start Iteration
	do
	{
		std::string temp;
		iteration++;	//Increase Iteration Number (From 1)
		increase_old = increase_new;
		//make matrix A & L
		makeA_Lindependent();
		//compute X matrix
		ROmat.N = ROmat.A.transpose()%ROmat.A;
		ROmat.Ninv = ROmat.N.inverse();
		ROmat.X = ROmat.Ninv%ROmat.A.transpose()%ROmat.L;
		//compute V matrix
		ROmat.V = (ROmat.A%ROmat.X) - ROmat.L;
		//compute VTV
		ROmat.VTV = ROmat.V.transpose()%ROmat.V;
		//compute posteriori variance
		newvar = ROmat.pos_var = ROmat.VTV(0,0)/ROmat.DF;
		//compute standard deviation
		ROmat.SD = sqrt(ROmat.pos_var);
		//compute variance-covariance matrix
		ROmat.Var_Co = ROmat.Ninv*ROmat.pos_var;
		//compute variance matrix
		ROmat.Var.resize(5+num_ROPoint*3,1);
		for(i=0;i<(5+num_ROPoint*3);i++)
		{
			ROmat.Var(i,0) = ROmat.Var_Co(i,i);
		}
	
		Param_inde.Lphi   += ROmat.X(0);
		Param_inde.Lkappa += ROmat.X(1);
		Param_inde.Romega += ROmat.X(2);
		Param_inde.Rphi   += ROmat.X(3);
		Param_inde.Rkappa += ROmat.X(4);
		for(i=0;i<LPhoto.num_ROP;i++)
		{
			Ga(i,0) += ROmat.X(5+i*3);
			Ga(i,1) += ROmat.X(5+i*3+1);
			Ga(i,2) += ROmat.X(5+i*3+2);
		}
		
		//maximum correction (X matrix)
		ROmat.maxX = fabs(ROmat.X(0));
		for(i=1;i<(int)ROmat.X.getRows();i++)
		{
			if(fabs(ROmat.X(i))>ROmat.maxX)
				ROmat.maxX = fabs(ROmat.X(i));
		}

		//Print Result
		temp = std::string("Iteration--->") + std::to_string(iteration) + std::string("\n\n\n");
		result += temp;
		result += PrintResult_inde();

		//reference variance(posteriori variance) monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			increase_new = true;
		}
		else
		{
			increase_new = false;
		}

		//iteration end condition
		//variance difference monitoring
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = true;
			result += std::string("[Difference Between Old_Variance And New_Variance < Difference Variance Limit !!!]\n");
		}
		else
		{
			oldvar = newvar;
		}
		
		//variance monitoring
		if(increase_old && increase_new)
		{
			iteration_end = true;
			result += std::string("[Variance Continuously(2ȸ ����) Increase!!!]\n");
		}
		//SD monitoring
		if(fabs(ROmat.SD)<fabs(maxSD))
		{
			iteration_end = true;
			result += std::string("[Standard Deviation < SD Limit !!!]\n");
		}
		//max correction
		if(ROmat.maxX<fabs(maxCorrection))
		{
			iteration_end = true;
			result += std::string("[Maximum Correction < Correction Limit !!!]\n");
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = true;
			result += std::string("[Iteration Max_Num Excess !!!]\n");
		}
		
	}while(iteration_end == false);
	
			
	return true;
}

void CModel::makeA_Lindependent(void)
{
	int i;
	ROmat.A.resize(4*LPhoto.num_ROP,5+LPhoto.num_ROP*3,0.0);
	ROmat.L.resize(4*LPhoto.num_ROP,1,0.0);

	
	//Left Photo
	{
		double xo = LPhoto.camera.PPA(0), yo = LPhoto.camera.PPA(1);
		double omega = Param_inde.Lomega, phi = Param_inde.Lphi, kappa = Param_inde.Lkappa;
		double f = LPhoto.camera.f;
		data::RotationMatrix m = math::rotation::getMMat(omega, phi, kappa); //rotation coefficient
		for(i=0;i<num_ROPoint;i++)
		{
			double deltaX,deltaY,deltaZ;
			double xa = LPhoto.ROPoint[i](0), ya = LPhoto.ROPoint[i](1);
			deltaX = Ga(i,0) - Param_inde.Xl;
			deltaY = Ga(i,1) - Param_inde.Yl;
			deltaZ = Ga(i,2) - Param_inde.Zl;
			
			b.q = m(2,0)*deltaX + m(2,1)*deltaY + m(2,2)*deltaZ;
			b.r = m(0,0)*deltaX + m(0,1)*deltaY + m(0,2)*deltaZ;
			b.s = m(1,0)*deltaX + m(1,1)*deltaY + m(1,2)*deltaZ;
			
			b.b12 = f/(b.q*b.q)*(b.r*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(-sin(phi)*cos(kappa)*deltaX + sin(omega)*cos(phi)*cos(kappa)*deltaY - cos(omega)*cos(phi)*cos(kappa)*deltaZ));
			b.b13 = -f/(b.q)*(m(1,0)*deltaX + m(1,1)*deltaY + m(1,2)*deltaZ);
			b.b14 = f/(b.q*b.q)*(b.r*m(2,0) - b.q*m(0,0));
			b.b15 = f/(b.q*b.q)*(b.r*m(2,1) - b.q*m(0,1));
			b.b16 = f/(b.q*b.q)*(b.r*m(2,2) - b.q*m(0,2));

			b.J = xa - xo + (f*b.r/b.q);
			
			b.b21 = f/(b.q*b.q)*(b.s*(-m(2,2)*deltaY+m(2,1)*deltaZ) - b.q*(-m(1,2)*deltaY+m(1,1)*deltaZ));
			b.b22 = f/(b.q*b.q)*(b.s*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(sin(phi)*sin(kappa)*deltaX - sin(omega)*cos(phi)*sin(kappa)*deltaY + cos(omega)*cos(phi)*sin(kappa)*deltaZ));
			b.b23 = f/(b.q)*(m(0,0)*deltaX + m(0,1)*deltaY + m(0,2)*deltaZ);
			b.b24 = f/(b.q*b.q)*(b.s*m(2,0) - b.q*m(1,0));
			b.b25 = f/(b.q*b.q)*(b.s*m(2,1) - b.q*m(1,1));
			b.b26 = f/(b.q*b.q)*(b.s*m(2,2) - b.q*m(1,2));

			b.K = ya - yo + (f*b.s/b.q);
			//A matrix
			//Rotation Angle
			ROmat.A(i*4,0) = b.b12;
			ROmat.A(i*4,1) = b.b13;

			ROmat.A(i*4+1,0) = b.b22;
			ROmat.A(i*4+1,1) = b.b23;
			//Model Space Point
			ROmat.A(i*4,5+i*3) = b.b14;
			ROmat.A(i*4,5+i*3+1) = b.b15;
			ROmat.A(i*4,5+i*3+2) = b.b16;

			ROmat.A(i*4+1,5+i*3) = b.b24;
			ROmat.A(i*4+1,5+i*3+1) = b.b25;
			ROmat.A(i*4+1,5+i*3+2) = b.b26;
			//L matrix
			ROmat.L(i*4,0) = b.J;
			
			ROmat.L(i*4+1,0) = b.K;
			
		}
	}//Left Photo End

	//Right Photo	
	{
		double xo = RPhoto.camera.PPA(0), yo = RPhoto.camera.PPA(1);
		double omega = Param_inde.Romega, phi = Param_inde.Rphi, kappa = Param_inde.Rkappa;
		double f = RPhoto.camera.f;
		data::RotationMatrix m = math::rotation::getMMat(omega, phi, kappa); //rotation coefficient
		for(i=0;i<num_ROPoint;i++)
		{
			double deltaX,deltaY,deltaZ;
			double xa = RPhoto.ROPoint[i](0), ya = RPhoto.ROPoint[i](1);
			deltaX = Ga(i,0) - Param_inde.Xr;
			deltaY = Ga(i,1) - Param_inde.Yr;
			deltaZ = Ga(i,2) - Param_inde.Zr;
				
			b.q = m(2,0)*deltaX + m(2,1)*deltaY + m(2,2)*deltaZ;
			b.r = m(0,0)*deltaX + m(0,1)*deltaY + m(0,2)*deltaZ;
			b.s = m(1,0)*deltaX + m(1,1)*deltaY + m(1,2)*deltaZ;
		
			b.b11 = f/(b.q*b.q)*(b.r*(-m(2,2)*deltaY + m(2,1)*deltaZ) - b.q*(-m(0,2)*deltaY + m(0,1)*deltaZ));
			b.b12 = f/(b.q*b.q)*(b.r*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(-sin(phi)*cos(kappa)*deltaX + sin(omega)*cos(phi)*cos(kappa)*deltaY - cos(omega)*cos(phi)*cos(kappa)*deltaZ));
			b.b13 = -f/(b.q)*(m(1,0)*deltaX + m(1,1)*deltaY + m(1,2)*deltaZ);
			b.b14 = f/(b.q*b.q)*(b.r*m(2,0) - b.q*m(0,0));
			b.b15 = f/(b.q*b.q)*(b.r*m(2,1) - b.q*m(0,1));
			b.b16 = f/(b.q*b.q)*(b.r*m(2,2) - b.q*m(0,2));

			b.J = xa - xo + (f*b.r/b.q);

			b.b21 = f/(b.q*b.q)*(b.s*(-m(2,2)*deltaY+m(2,1)*deltaZ) - b.q*(-m(1,2)*deltaY+m(1,1)*deltaZ));
			b.b22 = f/(b.q*b.q)*(b.s*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(sin(phi)*sin(kappa)*deltaX - sin(omega)*cos(phi)*sin(kappa)*deltaY + cos(omega)*cos(phi)*sin(kappa)*deltaZ));
			b.b23 = f/(b.q)*(m(0,0)*deltaX + m(0,1)*deltaY + m(0,2)*deltaZ);
			b.b24 = f/(b.q*b.q)*(b.s*m(2,0) - b.q*m(1,0));
			b.b25 = f/(b.q*b.q)*(b.s*m(2,1) - b.q*m(1,1));
			b.b26 = f/(b.q*b.q)*(b.s*m(2,2) - b.q*m(1,2));

			b.K = ya - yo + (f*b.s/b.q);
			//A matrix
			//Rotation Angle
			ROmat.A(i*4+2,2) = b.b11;
			ROmat.A(i*4+2,3) = b.b12;
			ROmat.A(i*4+2,4) = b.b13;
			
			ROmat.A(i*4+3,2) = b.b21;
			ROmat.A(i*4+3,3) = b.b22;
			ROmat.A(i*4+3,4) = b.b23;
			//Model Space Point
			ROmat.A(i*4+2,5+i*3+0) = b.b14;
			ROmat.A(i*4+2,5+i*3+1) = b.b15;
			ROmat.A(i*4+2,5+i*3+2) = b.b16;

			ROmat.A(i*4+3,5+i*3+0) = b.b24;
			ROmat.A(i*4+3,5+i*3+1) = b.b25;
			ROmat.A(i*4+3,5+i*3+2) = b.b26;
			//L matrix
			ROmat.L(i*4+2,0) = b.J;

			ROmat.L(i*4+3,0) = b.K;
		}
	}//Right Photo End

}

std::string CModel::PrintResult_de()
{
	std::string temp;
	std::string result;
	result += std::string("[A matrix]\n");
	result += matrixout(ROmat.A);
	result += std::string("\n\n");
	result += std::string("[L matrix]\n");
	result += matrixout(ROmat.L);
	result += std::string("\n\n");
	result += std::string("[X matrix]\n");
	result += matrixout(ROmat.X);
	result += std::string("\n\n");
	result += std::string("[V matrix]\n");
	result += matrixout(ROmat.V);
	result += std::string("\n\n");
	result += std::string("[VTV matrix]\n");
	result += matrixout(ROmat.VTV);
	result += std::string("\n\n");
	temp = std::string("[A-Posteriori Variance]\n") + std::to_string(ROmat.pos_var) + std::string("\n\n\n");
	result += temp;
	temp = std::string("Maximum element of X matrix : ") + std::to_string(ROmat.maxX) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
	result += std::string("[Variance_Corvariance matrix]\n");
	result += matrixout(ROmat.Var_Co);
	result += std::string("\n\n");
	result += std::string("[Variance matrix]\n");
	result += matrixout(ROmat.Var);
	result += std::string("\n\n");
	result += std::string("[RO_point(3D-model space) Coordinate]\n");
	result += matrixout(Ga);
	result += std::string("\n\n");
	
	result +="[RO Parameter(Dependent)]\r\n";
	temp = std::string("LPhoto omega : ") + std::to_string(Param_de.Lomega) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto phi : ") + std::to_string(Param_de.Lphi) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto kappa : ") + std::to_string(Param_de.Lkappa) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Xl : ") + std::to_string(Param_de.Xl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Yl : ") + std::to_string(Param_de.Yl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Zl : ") + std::to_string(Param_de.Zl) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto omega : ") + std::to_string(Param_de.Romega) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto phi : ") + std::to_string(Param_de.Rphi) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto kappa : ") + std::to_string(Param_de.Rkappa) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Xr : ") + std::to_string(Param_de.Xr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Yr : ") + std::to_string(Param_de.Yr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Zr : ") + std::to_string(Param_de.Zr) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
		
	return result;
}

std::string CModel::PrintResult_inde()
{
	std::string temp;
	std::string result;
	result += std::string("[A matrix]\n");
	result += matrixout(ROmat.A);
	result += std::string("\n\n");
	result += std::string("[L matrix]\n");
	result += matrixout(ROmat.L);
	result += std::string("\n\n");
	result += std::string("[X matrix]\n");
	result += matrixout(ROmat.X);
	result += std::string("\n\n");
	result += std::string("[V matrix]\n");
	result += matrixout(ROmat.V);
	result += std::string("\n\n");
	result += std::string("[VTV matrix]\n");
	result += matrixout(ROmat.VTV);
	result += std::string("\n\n");
	temp = std::string("[A-Posteriori Variance]\n") + std::to_string(ROmat.pos_var) + std::string("\n\n\n");
	result += temp;
	temp = std::string("Maximum element of X matrix : ") + std::to_string(ROmat.maxX) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
	result += std::string("[Variance_Corvariance matrix]\n");
	result += matrixout(ROmat.Var_Co);
	result += std::string("\n\n");
	result += std::string("[Variance matrix]\n");
	result += matrixout(ROmat.Var);
	result += std::string("\n\n");
	result += std::string("[RO_point(3D-model space) Coordinate]\n");
	result += matrixout(Ga);
	result += std::string("\n\n");
	
	result += std::string("[RO Parameter(Independent)]\n");
	temp = std::string("LPhoto omega : ") + std::to_string(Param_inde.Lomega) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto phi : ") + std::to_string(Param_inde.Lphi) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto kappa : ") + std::to_string(Param_inde.Lkappa) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Xl : ") + std::to_string(Param_inde.Xl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Yl : ") + std::to_string(Param_inde.Yl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Zl : ") + std::to_string(Param_inde.Zl) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto omega : ") + std::to_string(Param_inde.Romega) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto phi : ") + std::to_string(Param_inde.Rphi) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto kappa : ") + std::to_string(Param_inde.Rkappa) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Xr : ") + std::to_string(Param_inde.Xr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Yr : ") + std::to_string(Param_inde.Yr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Zr : ") + std::to_string(Param_inde.Zr) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
	
	return result;
}

void CModel::FilePrintROResult(char* outfile)
{
	FILE* outdata;
	outdata = fopen(outfile,"w");
	fputs(result.c_str(), outdata);
	fclose(outdata);
}

bool CModel::DependentTOIndependent(void)
{
	double dkappa, dphi;
	math::Matrix<double> Rotation_L,Rotation_R;
	dkappa = -atan2(Param_de.Yr,Param_de.Xr);
	dphi = atan2((Param_de.Zr-Param_de.Zl),sqrt(pow(Param_de.Xr,2)+pow(Param_de.Yr,2)));
		
	//RL and RR: orientation for dependent RO (original axis->photo axis)
	data::RotationMatrix RL = math::rotation::getMMat(Param_de.Lomega, Param_de.Lphi, Param_de.Lkappa);
	data::RotationMatrix RR = math::rotation::getMMat(Param_de.Romega, Param_de.Rphi, Param_de.Rkappa);
	
	//RB: rotation matrix for base_line(base line->original axis)
	data::RotationMatrix RB = math::rotation::getMMat(0, dphi, dkappa);
	
	//Rotation_L and Rotation_R are independent orientation matrix derived from dependent rotation matrix
	//base line->photo axis
	Rotation_L = RL % RB;
	Rotation_R = RR % RB;

	//rotation angles derived from a rotation matrix
	Param_inde.Lomega = atan(-Rotation_L(2,1)/Rotation_L(2,2));
	Param_inde.Lphi = asin(Rotation_L(2,0));
	Param_inde.Lkappa = atan(-Rotation_L(1,0)/Rotation_L(0,0));
	
	Param_inde.Romega = atan(-Rotation_R(2,1)/Rotation_R(2,2));
	Param_inde.Rphi = asin(Rotation_R(2,0));
	Param_inde.Rkappa = atan(-Rotation_R(1,0)/Rotation_R(0,0));

	// Perspective centers of left and right photos
	Param_inde.Xl = Param_de.Xl;
	Param_inde.Yl = Param_de.Yl;
	Param_inde.Zl = Param_de.Zl;
	
	Param_inde.Xr = Param_de.Xr;
	Param_inde.Yr = Param_de.Yl;
	Param_inde.Zr = Param_de.Zl;

	return true;
}

bool CModel::IndependentTODependent(void)
{
	math::Matrix<double> Rotation_L,Rotation_R;
				
	//RL and RR: orientation derived by the independent RO
	data::RotationMatrix RL = math::rotation::getMMat(Param_inde.Lomega, Param_inde.Lphi, Param_inde.Lkappa);
	data::RotationMatrix RR = math::rotation::getMMat(Param_inde.Romega, Param_inde.Rphi, Param_inde.Rkappa);
	
	//Base line rotation matrix (which is derived by inverse rotation of a left photo)
	math::Matrix<double> RB;
	RB = RL.transpose();

	//Rotation_L and Rotation_R : dependent orientation derived by the independent method
	Rotation_L = RL % RB;
	Rotation_R = RR % RB;

	Param_de.Lomega = atan(-Rotation_L(2,1)/Rotation_L(2,2));
	Param_de.Lphi = asin(Rotation_L(2,0));
	Param_de.Lkappa = atan(-Rotation_L(1,0)/Rotation_L(0,0));
	
	Param_de.Romega = atan(-Rotation_R(2,1)/Rotation_R(2,2));
	Param_de.Rphi = asin(Rotation_R(2,0));
	Param_de.Rkappa = atan(-Rotation_R(1,0)/Rotation_R(0,0));

	Param_de.Xl = Param_inde.Xl;
	Param_de.Yl = Param_inde.Yl;
	Param_de.Zl = Param_inde.Zl;

	Param_de.Xr = Param_inde.Xr;
	Param_de.Yr = Param_inde.Xr*(-tan(Param_inde.Lkappa));
	Param_de.Zr = Param_inde.Zr + sqrt(Param_de.Yr*Param_de.Yr+Param_inde.Xr*Param_inde.Xr)*tan(Param_inde.Lphi);

	return true;
}
