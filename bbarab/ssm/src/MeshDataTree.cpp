/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <ssm/include/MeshDataTree.h>

namespace mesh
{
    SSMFlann::SSMFlann(const double maxLength, const PSEUDOTYPE pType)
	{
		this->maxDist = maxLength;
        pseudoType = pType;
	};

	void SSMFlann::setNumberOfPoints(const unsigned int size)
	{
		meshData.vertices.resize(size);
	}

    bool SSMFlann::setVertex(const unsigned int index, const double x, const double y, const double z)
    {
        if (!(index < meshData.vertices.size()))
            return false;

        try
        {
            meshData.vertices[index].set(x, y, z);
            return true;
        }
        catch (...)
        {
            std::cerr << "Error in setVertex of SSMFlann" << std::endl;
            return false;
        }
    }

    void SSMFlann::setNumberOfFaces(const unsigned int size)
	{
		meshData.faces.resize(size);
        meshData.fNormals.resize(size);
	}

    /**setPseudoType
    *@brief set the type of generating pseudo points
    *@param pType: type
    */
    void SSMFlann::setPseudoType(const PSEUDOTYPE pType)
    {
        pseudoType = pType;
    }

    bool SSMFlann::setFace(const unsigned int faceIndex, const std::vector<unsigned int>& vtxIndex)
    {
		if (!(faceIndex < meshData.faces.size()))
		{
			std::cerr << "Error in setFace of SSMFlann: check the index of a face" << std::endl;
			return false;
		}

		if (vtxIndex.size() < 3)
		{
			std::cerr << "Error in setFace of SSMFlann: check the number of vertices consisting a face (should be greater than 2)" << std::endl;
			return false;
		}

		try
		{
            meshData.faces[faceIndex] = vtxIndex;
            return defineFace(faceIndex, vtxIndex);
		}
		catch (...)
		{
			std::cerr << "Error in setFace of SSMFlann" << std::endl;
			return false;
		}
	}

    bool SSMFlann::setFaceNormal(const unsigned int faceIndex, const double fnx, const double fny, const double fnz)
    {
        if (!(faceIndex < meshData.fNormals.size()))
        {
            std::cerr << "Error in setFaceNormal of SSMFlann: check the index of a face-normal" << std::endl;
            return false;
        }

        try
        {
            meshData.fNormals[faceIndex][0] = fnx;
            meshData.fNormals[faceIndex][1] = fny;
            meshData.fNormals[faceIndex][2] = fnz;
        }
        catch (...)
        {
            std::cerr << "Error in setFaceNormal of SSMFlann" << std::endl;
            return false;
        }

        return true;
    }

	Vertex3D SSMFlann::getFaceNormal(const unsigned int faceIndex) const
	{
		try
		{
			return this->meshData.fNormals[faceIndex];
		}
		catch (...)
		{

			std::cerr << "Error in getting a face normal vector" << std::endl;
            Vertex3D temp;
            return temp;
		}
	}

    void SSMFlann::clearVertexData()
    {
        meshData.vertices.clear();
    }

    bool SSMFlann::defineFace(const unsigned int faceIndex, const std::vector<unsigned int>& vertexIndices)
    {
        try
        {
            for (const unsigned int index : vertexIndices)
            {
                meshData.vertices[index].addFaceId(faceIndex);
            }

            /// divide a face into sub-triangles
            switch (this->pseudoType)
            {
            case PSEUDOTYPE::CENTROID:
                insertPseudoPoint(vertexIndices, faceIndex);
                break;
            case PSEUDOTYPE::MIDPOINT:
                insertPseudoPointMid(vertexIndices, faceIndex);
                break;
            default:
                break;
            }
            
            return true;
        }
        catch (...)
        {

            std::cerr << "Error in defineFace of SSMFlann" << std::endl;
            return false;
        }
    }

    void SSMFlann::buildTree(const unsigned int maxLeaf)
    {
        try
        {
            /// Add additional pseudo points
            meshData.vertices.insert(meshData.vertices.end(), pseudoPts.begin(), pseudoPts.end());

            /// Larger maxLeaf result in fast search, but takes long time for tree construction
            tree.reset(new KDTree(mesh::dimension, meshData, nanoflann::KDTreeSingleIndexAdaptorParams(maxLeaf)));
            tree->buildIndex();
        }
        catch (...)
        {
            std::cerr << "Error in buildTree of SSMFlann" << std::endl;
        }
    }

	bool SSMFlann::buildTree(const std::string& idxFilePath, const unsigned int maxLeaf)
	{
		try
		{
			/// Add additional pseudo points
			meshData.vertices.insert(meshData.vertices.end(), pseudoPts.begin(), pseudoPts.end());

			/// Larger maxLeaf result in fast search, but takes long time for tree construction
			tree.reset(new KDTree(mesh::dimension, meshData, nanoflann::KDTreeSingleIndexAdaptorParams(maxLeaf)));

			FILE* idxFile = fopen(idxFilePath.c_str(), "rb");
            if (!idxFile) return false;
			tree->loadIndex(idxFile);
			fclose(idxFile);
		}
		catch (...)
		{
			std::cerr << "Error in buildTree(import index info from a file) of SSMFlann" << std::endl;
		}

        return true;
	}

    bool SSMFlann::search(const double x0, const double y0, const double z0,
        std::vector<double>& distances,
        std::vector<std::size_t>& indices,
        const unsigned int numClosest,
        const double distThreshold) const
    {
        mesh::MeshDataType::CoordValueType searchPt[] = { x0, y0, z0 };

        try
        {
            indices.resize(numClosest, 0);
            std::vector<double> squareDist(numClosest);
            size_t numFound = tree->knnSearch(searchPt, numClosest, indices.data(), squareDist.data());

            distances.reserve(numFound);
            for (const auto sqD : squareDist)
            {
                distances.push_back(sqrt(sqD));
            }

            if (numFound < numClosest)
                indices.erase(indices.begin() + numFound, indices.end());
        }
        catch (...)
        {
            std::cerr << "Error in search of SSMFlann" << std::endl;
            return false;
        }

        /// if distThreshold == 0.0, it means ignoring the distance threshold
        if (distances[0] < distThreshold || distThreshold == 0.0)
            return true;
        else
            return false;
    }

	/**raytrace
	*@brief find the closest point along the given ray
	*@param sx: x coordinate of a start point of a ray
	*@param sy: y coordinate of a start point of a ray
	*@param sz: z coordinate of a start point of a ray
	*@param ex: x coordinate of a end point of a ray
	*@param ey: y coordinate of a end point of a ray
	*@param ez: z coordinate of a end point of a ray
	*@param radius: search radius
	*@param foundPts: point indices and distances, result values
	*@return bool
	*/
	bool SSMFlann::raytrace(const double sx, const double sy, const double sz,
		const double ex, const double ey, const double ez,
		const double radius,
        std::vector<std::pair<size_t, double>>& foundPts)
	{
        double dx = ex - sx;
        double dy = ey - sy;
        double dz = ez - sz;
        double maxDist = sqrt(dx * dx + dy * dy + dz * dz);
        if (maxDist < std::numeric_limits<double>::epsilon())
            return false;
        double nx = dx / maxDist;
        double ny = dy / maxDist;
        double nz = dz / maxDist;

        unsigned int count = 0;
        do 
        {   ++ count;
            double fwdDist = radius * count;
            
            if (fwdDist > maxDist)
                break;

            mesh::MeshDataType::CoordValueType searchPt[] = { sx + nx * fwdDist, sy + ny * fwdDist, sz + nz * fwdDist };
			nanoflann::SearchParams params;
            /// Search nearest point in radius
			const size_t nMatches = tree->radiusSearch(&searchPt[0], radius, foundPts, params);
            if (nMatches > 0)
                return true;

        } while (1);

        return false;
	}

    Vertex3D SSMFlann::getVertex(const std::size_t vtxIndex) const
    {
        return meshData.vertices[vtxIndex];
    }

    /**computeCentroid
    *@param vertices : vertices consisting a face
    *@return Vertex3D : centroid 3D coordinates
    */
    Vertex3D SSMFlann::computeCentroid(const std::vector<Vertex3D>& vertices)
    {
        double sumX = 0.0;
        double sumY = 0.0;
        double sumZ = 0.0;
        for (const auto& vtx : vertices)
        {
            sumX += vtx.x();
            sumY += vtx.y();
            sumZ += vtx.z();
        }

        return Vertex3D(sumX / vertices.size(), sumY / vertices.size(), sumZ / vertices.size());
    }

    /**checkDist2Center
    *@param vertices : vertices consisting a face
    *@param centroid : centroid of a polygon
    *@return bool : [false] there is a too long side (>maxSide) in the vertices defining a face
    */
    bool SSMFlann::checkDist2Center(const std::vector<Vertex3D>& vertices, const Vertex3D& centroid)
    {
        for (unsigned int i = 0; i < vertices.size(); ++i)
        {
            auto dx = vertices[i].x() - centroid.x();
            auto dy = vertices[i].y() - centroid.y();
            auto dz = vertices[i].z() - centroid.z();
            auto l = sqrt(dx * dx + dy * dy + dz * dz);
            if (l < maxDist) return true;
        }

        return false; /// no long-size
    }

    /**defineSubTriangles
    *@brief define sub-polygons (triangles)
    *@param vertices : vertices consisting a face
    *@param centroid : centroid of a polygon
    */
    std::vector< std::vector<Vertex3D>> SSMFlann::defineSubTriangles(const std::vector<Vertex3D>& vertices, const Vertex3D& centroid)
    {
        std::vector< std::vector<Vertex3D>> triangles(vertices.size());

        for (unsigned int i = 0; i < vertices.size(); ++i)
        {
            unsigned int j = i + 1;
            if (j == vertices.size()) j = 0;

            triangles[i].resize(3);
            triangles[i][0] = centroid;
            triangles[i][1] = vertices[i];
            triangles[i][2] = vertices[j];
        }

        return triangles;
    }

    /**insertPseudoPoint: generate pseudo points in a face with centroid points
    *@param vertexIndices : vertex indices consisting a face
    *@param faceIndex : face index
    */
    void SSMFlann::insertPseudoPoint(const std::vector<unsigned int>& vertexIndices, const unsigned int faceIndex)
    {
        if (this->maxDist < std::numeric_limits<double>::epsilon())
            return;

        /// Collect vertices using indices
        std::vector<std::vector<Vertex3D>> verticesGroup(1);
        verticesGroup[0].reserve(vertexIndices.size());
        for (auto idx : vertexIndices)
            verticesGroup[0].push_back(meshData.vertices[idx]);

        std::vector<std::vector<Vertex3D>> newCollectedVertices;

        /// Check size of a face and put centroid points if need
        do
        {
            /// New polygons defined by centroid points
            newCollectedVertices.clear();

            for (auto vertices : verticesGroup)
            {
                /// compute a centroid
                Vertex3D centroid = computeCentroid(vertices);

                /// check if there is a long side
                if (checkDist2Center(vertices, centroid))
                    continue;

                /// Assign face index and save the centroid (pseudoPts)
                centroid.addFaceId(faceIndex);
                pseudoPts.push_back(centroid);

                /// Define new sub-triangles
                std::vector<std::vector<Vertex3D>> triangles = defineSubTriangles(vertices, centroid);

                /// Collect newly defined triangle
                newCollectedVertices.insert(newCollectedVertices.end(), triangles.begin(), triangles.end());
            }

            if (newCollectedVertices.size() < 1)
                break;
            else
                verticesGroup = newCollectedVertices;

        } while (1);
    }

    /**insertPseudoPointMid: generate pseudo points
    *@param vertexIndices : vertex indices consisting a face
    *@param faceIndex : face index
    */
    void SSMFlann::insertPseudoPointMid(const std::vector<unsigned int>& vertexIndices, const unsigned int faceIndex)
    {
        if (this->maxDist < std::numeric_limits<double>::epsilon())
            return;

        /// Collect vertices for a face
		std::vector<Vertex3D> faceVtx;
        faceVtx.reserve(vertexIndices.size());
		for (auto idx : vertexIndices)
			faceVtx.push_back(meshData.vertices[idx]);

        /// Generate pseudo points by dividing a face into sub-triangles
        std::vector<Vertex3D> midPts;
        divideFace(faceVtx, this->maxDist, midPts);
        for (auto& pt : midPts)
            pt.addFaceId(faceIndex);
        pseudoPts.insert(pseudoPts.end(), midPts.begin(), midPts.end());
    }

    /**addMidPts: add mid points to a given line
    *@param p0 : the start point of a line
    *@param p1 : the end point of a line
    *@param maxLength : maximum length threshold
    *@param pseudoPts : bucket for storing pseudo point (midPts)
    *@param faceId: face Id
    *@return bool: if true, the line is large, else, short
    */
    bool SSMFlann::addMidPts(const Vertex3D& p0, const Vertex3D& p1, const double maxLength, std::vector<Vertex3D>& pseudoPts)
    {
        std::vector<Vertex3D> addedPts;

        /// check distances
        Vertex3D v = p1 - p0;
        auto dist = sqrt(v.x() * v.x() + v.y() * v.y() + v.z() * v.z());
        if ((dist / maxLength) > 1.0)
        {
            Vertex3D dir = v / dist;
            unsigned int num = static_cast<int>(dist / maxLength + 1.0);
            double dLength = dist / static_cast<double>(num);
            addedPts.reserve(num - 1);
            for (unsigned int i = 1; i < num; ++i)
            {
                Vertex3D tempPt = dir * (dLength * static_cast<double>(i)) + p0;
                addedPts.push_back(tempPt);
            }
        }

        if (addedPts.size() > 0)
		{
			pseudoPts.insert(pseudoPts.end(), addedPts.begin(), addedPts.end());
            return true;
        }
        else
            return false;
    }

    /**divideTriangle: divide a triangle into three sub-triangles
    *@param triVtx : three vertices
    *@param maxLength : maximum length threshold
    *@param pseudoPts : bucket for storing pseudo point (midPts)
    *@param centroid : center of a triangle
    *@return bool: if true, triangle is large, else, small
    */
    bool SSMFlann::divideTriangle(const std::vector<Vertex3D>& triVtx, const double maxLength, std::vector<Vertex3D>& pseudoPts, Vertex3D& centroid)
    {
        /// compute a centroid
        centroid = Vertex3D((triVtx[0].x() + triVtx[1].x() + triVtx[2].x()) / 3.,
            (triVtx[0].y() + triVtx[1].y() + triVtx[2].y()) / 3.,
            (triVtx[0].z() + triVtx[1].z() + triVtx[2].z()) / 3.);

        std::vector<Vertex3D> midPts;
        bool longSide = true;

        /// Mid-points
        if (!addMidPts(triVtx[0], centroid, maxLength, midPts))
            longSide = false;
        if (!addMidPts(triVtx[1], centroid, maxLength, midPts))
            longSide = false;
        if (!addMidPts(triVtx[2], centroid, maxLength, midPts))
            longSide = false;

		pseudoPts.push_back(centroid);
		pseudoPts.insert(pseudoPts.end(), midPts.begin(), midPts.end());

        return longSide;
    }

    /**divideFace: divide a face (polygon)
    *@param faceVtx : face vertices
    *@param maxLength : maximum length threshold
    *@param pseudoPts : bucket for storing pseudo point (midPts)
    */
    void SSMFlann::divideFace(const std::vector<Vertex3D>& faceVtx, const double maxLength, std::vector<Vertex3D>& pseudoPts)
    {
        if (faceVtx.size() < 3)
            return;

		Vertex3D centroid; /// Centroid

        /// Divide sides
		for (unsigned int i = 0; i < faceVtx.size(); ++i)
		{
			unsigned int j;
			if (i == faceVtx.size() - 1) j = 0;
			else j = i + 1;

			/// Add pseudo points to the inside
			addMidPts(faceVtx[i], faceVtx[j], maxLength, pseudoPts);

            centroid += faceVtx[i];
		}

        if (pseudoPts.size() < 1)
            return;

        centroid /= static_cast<double>(faceVtx.size());

		bool bigSize = false;

		for (unsigned int i = 0; i < faceVtx.size(); ++i)
		{
			auto dP = centroid - faceVtx[i];
			auto dist = sqrt(dP.x() * dP.y() + dP.x() * dP.y() + dP.z() * dP.z());
			if (dist > maxLength)
			{
                pseudoPts.push_back(centroid);
				bigSize = true;
				break;
			}
		}

		if (!bigSize)
			return;

		/// First sub-triangles from a face
		std::vector<std::vector<Vertex3D>> subTri(faceVtx.size());

		for (unsigned int i = 0; i < faceVtx.size(); ++i)
		{
			/// Add pseudo points to the inside
			addMidPts(centroid, faceVtx[i], maxLength, pseudoPts);

			/// Add pseudo point to the sides
			Vertex3D side;
			unsigned int j;
			if (i == faceVtx.size() - 1) j = 0;
			else j = i + 1;
			
            std::vector<Vertex3D> tri(3);
			tri[0] = faceVtx[i];
			tri[1] = faceVtx[j];
			tri[2] = centroid;

			subTri[i] = tri;
		}

        /// Check sub-triangles
        unsigned int count = 0;
		do
		{
            ++ count;

            auto subTriCopy = subTri;
			subTri.clear();

			for (const auto& sub : subTriCopy)
			{
				Vertex3D triCenter;
				if (divideTriangle(sub, maxLength, pseudoPts, triCenter))
				{
					std::vector<Vertex3D> dividedTri(3);
                    dividedTri[0] = sub[0];
                    dividedTri[1] = sub[1];
                    dividedTri[2] = triCenter;
					subTri.push_back(dividedTri);
                    dividedTri[0] = sub[1];
                    dividedTri[1] = sub[2];
                    dividedTri[2] = triCenter;
					subTri.push_back(dividedTri);
                    dividedTri[0] = sub[2];
                    dividedTri[1] = sub[0];
                    dividedTri[2] = triCenter;
					subTri.push_back(dividedTri);
				}
			}

            if (subTri.size() < 1)
				break;

		} while (1);
    }

    /**exportIndex
    */
    bool SSMFlann::exportIndex(const std::string& idxFilePath)
    {
        if (!tree)
            return false;

        FILE* idxFile = fopen(idxFilePath.c_str(), "wb");
        if (!idxFile) return false;

        tree->saveIndex(idxFile);

        fclose(idxFile);

        return true;
    }
};