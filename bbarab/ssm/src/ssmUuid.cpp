#include <iostream>

//#include <ssm/include/ssmStr.h>
#include <ssm/include/ssmUuid.h>

namespace util
{
	const unsigned int sizeData4 = 8;
	
	SsmUuid::SsmUuid()
	{
		regenId();
	}

	SsmUuid::SsmUuid(const SsmUuid& newId)
	{
		operator =(newId);
	}

	SsmUuid::SsmUuid(const UUID& newId)
	{
		operator =(newId);
	}

	void SsmUuid::regenId()
	{
		if (RPC_S_OK != UuidCreate(&id))
			throw std::runtime_error("No OK from UuidCreate()");
	}

	void SsmUuid::operator = (const SsmUuid& copy)
	{
		this->id.Data1 = copy.id.Data1;
		this->id.Data2 = copy.id.Data2;
		this->id.Data3 = copy.id.Data3;
		memcpy(this->id.Data4, copy.id.Data4, 8);
	}

	void SsmUuid::operator = (const UUID& copy)
	{
		this->id.Data1 = copy.Data1;
		this->id.Data2 = copy.Data2;
		this->id.Data3 = copy.Data3;
		memcpy(this->id.Data4, copy.Data4, 8);
	}

	bool SsmUuid::operator == (const SsmUuid& id0) const
	{
		if (this->id.Data1 != id0.id.Data1)
			return false;

		if (this->id.Data2 != id0.id.Data2)
			return false;

		if (this->id.Data3 != id0.id.Data3)
			return false;

		for (unsigned int i = 0; i < sizeData4; ++i)
		{
			if (this->id.Data4[i] != id0.id.Data4[i])
				return false;
		}
		
		return true;
	}

	bool SsmUuid::operator == (const UUID& id0) const
	{
		if (this->id.Data1 != id0.Data1)
			return false;

		if (this->id.Data2 != id0.Data2)
			return false;

		if (this->id.Data3 != id0.Data3)
			return false;

		for (unsigned int i = 0; i < sizeData4; ++i)
		{
			if (this->id.Data4[i] != id0.Data4[i])
				return false;
		}

		return true;
	}

	bool SsmUuid::operator != (const SsmUuid& id0) const
	{
		if (this->id.Data1 == id0.id.Data1)
			return false;

		if (this->id.Data2 == id0.id.Data2)
			return false;

		if (this->id.Data3 == id0.id.Data3)
			return false;

		for (unsigned int i = 0; i < sizeData4; ++i)
		{
			if (this->id.Data4[i] == id0.id.Data4[i])
				return false;
		}

		return true;
	}

	bool SsmUuid::operator != (const UUID& id0) const
	{
		if (this->id.Data1 == id0.Data1)
			return false;

		if (this->id.Data2 == id0.Data2)
			return false;

		if (this->id.Data3 == id0.Data3)
			return false;

		for (unsigned int i = 0; i < sizeData4; ++i)
		{
			if (this->id.Data4[i] == id0.Data4[i])
				return false;
		}

		return true;
	}

	UUID SsmUuid::getId() const
	{
		return id;
	}

	std::string SsmUuid::printIdToHex() const
	{
		std::string str;

		char szHex[16];
		// Decimal number to hexadecimal number
		sprintf_s(szHex, 16, "%X", id.Data1);
		str += szHex;
		
		sprintf_s(szHex, 16, "-%X", id.Data2);
		str += szHex;
		
		sprintf_s(szHex, 16, "-%X", id.Data3);
		str += szHex;
		
		for (const auto ch : id.Data4)
		{
			sprintf_s(szHex, 16, "-%X", ch);
			str += szHex;
		}

		return str;
	}
}