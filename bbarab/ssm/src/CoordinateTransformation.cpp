/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <ssm/include/CoordinateTransformation.h>

namespace math
{
	namespace geo
	{
		/**
		* @brief  Get a central meridian
		*/
		double getCentralMeridian(const unsigned int noZone)
		{
			double centerMeri;
			if (noZone <= 30)
				centerMeri = static_cast<double>(noZone) * 6. + 180. - 3.;
			else
				centerMeri = static_cast<double>(noZone) * 6. - 180. - 3.;

			centerMeri = centerMeri / 180.0 * _PI_;

			return centerMeri;
		}

		/**
		* @brief  Get a UTM zone number
		*/
		unsigned int getZone(const double longitude)
		{
			int zl = static_cast<int>(longitude / _PI_ * 180.0);
			if (zl >= 180)
				zl = static_cast<int>((longitude * r2d) - 360.0);

			unsigned int noZone;

			if (zl >= 0)
				noZone = (int)(zl / 6) + 31;
			else
				noZone = (int)(zl / 6) + 30;

			return noZone;
		}

		/// <summary>
		/// geodetic (lat-lon-h) to geocentric (x-y-z) coordinates
		/// </summary>
		/// <param name="lat">latitude</param>
		/// <param name="lon">longitude</param>
		/// <param name="h">ellipsoid height</param>
		/// <returns></returns>
		ECEF LLH2ECEF(const LLH& llh, const ReferenceEllipsoid& ellips)
		{
			double& lat = llh[0];
			double& lon = llh[1];
			double& h = llh[2];

			/// e : first eccentricity
			const double e = ellips.eccentricity;
			/// n : radius of curvature in the prime vertical
			const double n = ellips.eliParam.semiMajor / sqrt(1. - e * e * sin(lat) * sin(lat));

			ECEF ecef(3); /// x, y, z
			ecef[0] = (n + h) * cos(lat) * cos(lon);
			ecef[1] = (n + h) * cos(lat) * sin(lon);
			ecef[2] = (n * (1. - e * e) + h) * sin(lat);

			return ecef;
		}

		/// <summary>
		/// geocentric (x-y-z) coordinates to geodetic (lat-lon-h) 
		/// </summary>
		/// <param name="x">ECEF x</param>
		/// <param name="y">ECEF y</param>
		/// <param name="z">ECEF z</param>
		/// <returns></returns>
		LLH ECEF2LLH(const ECEF& ecef, const ReferenceEllipsoid& ellips)
		{
			double& x = ecef[0];
			double& y = ecef[1];
			double& z = ecef[2];

			const double e = ellips.eccentricity;
			const double a = ellips.eliParam.semiMajor;
			const double b = ellips.eliParam.semiMinor;

			LLH llh(3); /// (lat, lon, h)
			llh[1] = atan2(y, x);

			/// initial value of phi
			double p_i = atan(z / ((1. - e * e) * sqrt(x * x + y * y)));
			/// estimated value of height
			double h_e = z / sin(p_i) - a * (1. - e * e) / sqrt(1 - e * e * sin(p_i) * sin(p_i));
			/// radius of curvature in the prime vertical
			double n = a / sqrt(1. - e * e * sin(p_i) * sin(p_i));
			/// estimated value of phi
			double p_e = atan((n + h_e) * z / (((1. - e * e) * n + h_e) * sqrt(x * x + y * y)));

			while (!almostSame(p_i, p_e))
			{
				h_e = z / sin(p_e) - a * (1. - e * e) / sqrt(1 - e * e * sin(p_e) * sin(p_e));

				p_i = p_e;

				n = a / sqrt(1. - e * e * sin(p_i) * sin(p_i));
				p_e = atan((n + h_e) * z / (((1. - e * e) * n + h_e) * sqrt(x * x + y * y)));
			}

			llh[0] = p_e;
			llh[2] = h_e;

			return llh;
		}

		/// <summary>
		/// geodetic (lat-lon-H) to UTM cartesian coordinates (E-N-Up & Zone#)
		/// </summary>
		/// <param name="ll0">lat and long</param>
		/// <returns>UTM East, North and zone number</returns>
		ENUZnHemi LLH2ENUZnHm(const LLH& llh0, const ReferenceEllipsoid& ellips)
		{
			LLH llh = llh0;

			/// if the latitude is out of the range(84N - 80S)
			if (llh[0] * r2d > 84. || llh[0] * r2d < -80)
			{
				std::cout << "Error in utmProj: out of the latitude range (-80 ~ +84)" << std::endl;
				throw std::runtime_error("Error in utmProj: out of the latitude range (-80 ~ +84)");
			}

			if (llh[1] < 0)
				llh[1] += 360.0 * d2r;

			/// Gauss-Kruger
			/// determine the zone and central meridian
			ENUZnHemi enuznhm(5);
			unsigned int zoneNo = getZone(llh[1]);
			enuznhm[3] = zoneNo;
			double centmeri = getCentralMeridian(zoneNo);

			double a = ellips.eliParam.semiMajor;
			double b = ellips.eliParam.semiMinor;
			double e = ellips.eccentricity;
			double e2 = ellips.eccentricity2;

			/// n: radius of curvature in the prime vertical
			double n = a / sqrt(1. - e * e * sin(llh[0]) * sin(llh[0]));

			/// compute the distance(bl) of the ellipsiodal meridian from the equator to the latitude
			const double& ns = ellips.n.ns;
			const double& ns2 = ellips.n.ns2;
			const double& ns3 = ellips.n.ns3;
			const double& ns4 = ellips.n.ns4;
			double a0 = 1. + (ns2 / 4.) + (ns4 / 64.);
			double a2 = (3. / 2.) * (ns - (ns3 / 8.));
			double a4 = (15. / 16.) * (ns2 - (ns4 / 4.));
			double a6 = (35. / 48.) * ns3;
			double a8 = (315. / 512.) * ns4;

			double bl = ((a + b) / 2.) * (a0 * llh[0] - a2 * sin(2. * llh[0]) + a4 * sin(4. * llh[0])
				- a6 * sin(6. * llh[0]) + a8 * sin(8. * llh[0]));

			double t = tan(llh[0]);
			double eta = e2 * cos(llh[0]);
			double dl = llh[1] - centmeri;
			double dl2 = dl * dl;
			double dl3 = dl * dl2;
			double dl4 = dl2 * dl2;
			double dl5 = dl3 * dl2;
			double dl6 = dl5 * dl;

			/* compute East coord. */
			double xterm1 = dl * cos(llh[0]);
			double xterm2 = dl3 * pow(cos(llh[0]), 3.) * (1 - t * t + eta * eta) / 6.;
			double xterm3 = dl5 * pow(cos(llh[0]), 5.) * (5. - 18. * t * t + pow(t, 4.) + 14. * eta * eta - 58. * t * t * eta * eta) / 120.;
			enuznhm[0] = n * (xterm1 + xterm2 + xterm3);

			/* compute North coord. */
			double yterm1 = dl2 * sin(llh[0]) * cos(llh[0]) / 2.;
			double yterm2 = dl4 * sin(llh[0]) * pow(cos(llh[0]), 3.) * (5. - t * t + 9. * eta * eta + 4. * pow(eta, 4.)) / 24.;
			double yterm3 = dl6 * sin(llh[0]) * pow(cos(llh[0]), 5.) * (61. - 58. * t * t + pow(t, 4.) + 270. * eta * eta - 330. * t * t * eta * eta) / 720.;
			enuznhm[1] = bl + n * (yterm1 + yterm2 + yterm3);

			/// correction the coord. for the scale factor of central meridan and virtual coord. of origin
			enuznhm[0] = enuznhm[0] * UTM::scale + UTM::falseEasting;

			if (llh[0] < 0.) enuznhm[1] = enuznhm[1] * UTM::scale + UTM::falseNorthing;
			else enuznhm[1] = enuznhm[1] * UTM::scale;

			/* height */
			enuznhm[2] = llh[2];

			/* hemisphere */
			if (llh[0] > 0)
				enuznhm[4] = static_cast<double>(HEMISPHERE::NORTHERN);
			else
				enuznhm[4] = static_cast<double>(HEMISPHERE::SOUTHERN);

			return enuznhm;
		}

		/**
		* @brief  FootpointLatitude
		*/
		double FootpointLatitude(const double y, const ReferenceEllipsoid& ellips)
		{
			const double& a = ellips.eliParam.semiMajor;
			const double& b = ellips.eliParam.semiMinor;
			const double& ns = ellips.n.ns;
			const double& ns2 = ellips.n.ns2;
			const double& ns3 = ellips.n.ns3;
			const double& ns4 = ellips.n.ns4;
			const double& ns5 = ellips.n.ns5;

			const double alpha_ = ((a + b) / 2.0) * (1 + (ns2 / 4) + (ns4 / 64));
			const double y_ = y / alpha_;
			const double beta_ = (3.0 * ns / 2.0) + (-27.0 * ns3 / 32.0) + (269.0 * ns5 / 512.0);
			const double gamma_ = (21.0 * ns2 / 16.0) + (-55.0 * ns4 / 32.0);
			const double delta_ = (151.0 * ns3 / 96.0) + (-417.0 * ns5 / 128.0);
			const double epsilon_ = (1097.0 * ns4 / 512.0);
			const double result = y_ + (beta_ * sin(2.0 * y_))
				+ (gamma_ * sin(4.0 * y_))
				+ (delta_ * sin(6.0 * y_))
				+ (epsilon_ * sin(8.0 * y_));

			return result;
		}

		/**
		* @brief  MapXYToLatLon
		*/
		LL MapXYToLatLon(const double x0, const double y0, const double lon0, const ReferenceEllipsoid& ellips)
		{
			const double& sm_a = ellips.eliParam.semiMajor;
			const double& sm_b = ellips.eliParam.semiMinor;
			const double phif = FootpointLatitude(y0, ellips);
			const double ep2 = (pow(sm_a, 2.0) - pow(sm_b, 2.0)) / pow(sm_b, 2.0);
			const double cf = cos(phif);
			const double nuf2 = ep2 * pow(cf, 2.0);
			const double Nf = pow(sm_a, 2.0) / (sm_b * sqrt(1 + nuf2));
			double Nfpow = Nf;
			const double tf = tan(phif);
			const double tf2 = tf * tf;
			const double tf4 = tf2 * tf2;
			const double x1frac = 1.0 / (Nfpow * cf);
			Nfpow *= Nf;
			const double x2frac = tf / (2.0 * Nfpow);
			Nfpow *= Nf;
			const double x3frac = 1.0 / (6.0 * Nfpow * cf);
			Nfpow *= Nf;
			const double x4frac = tf / (24.0 * Nfpow);
			Nfpow *= Nf;
			const double x5frac = 1.0 / (120.0 * Nfpow * cf);
			Nfpow *= Nf;
			const double x6frac = tf / (720.0 * Nfpow);
			Nfpow *= Nf;
			const double x7frac = 1.0 / (5040.0 * Nfpow * cf);
			Nfpow *= Nf;
			const double x8frac = tf / (40320.0 * Nfpow);
			const double x2poly = -1.0 - nuf2;
			const double x3poly = -1.0 - 2 * tf2 - nuf2;
			const double x4poly = 5.0 + 3.0 * tf2 + 6.0 * nuf2 - 6.0 * tf2 * nuf2 - 3.0 * (nuf2 * nuf2) - 9.0 * tf2 * (nuf2 * nuf2);
			const double x5poly = 5.0 + 28.0 * tf2 + 24.0 * tf4 + 6.0 * nuf2 + 8.0 * tf2 * nuf2;
			const double x6poly = -61.0 - 90.0 * tf2 - 45.0 * tf4 - 107.0 * nuf2 + 162.0 * tf2 * nuf2;
			const double x7poly = -61.0 - 662.0 * tf2 - 1320.0 * tf4 - 720.0 * (tf4 * tf2);
			const double x8poly = 1385.0 + 3633.0 * tf2 + 4095.0 * tf4 + 1575 * (tf4 * tf2);

			LL latlon(2);

			latlon[0] = phif + x2frac * x2poly * (x0 * x0)
				+ x4frac * x4poly * pow(x0, 4.0)
				+ x6frac * x6poly * pow(x0, 6.0)
				+ x8frac * x8poly * pow(x0, 8.0);

			latlon[1] = lon0 + x1frac * x0
				+ x3frac * x3poly * pow(x0, 3.0)
				+ x5frac * x5poly * pow(x0, 5.0)
				+ x7frac * x7poly * pow(x0, 7.0);

			return latlon;
		}

		/// <summary>
		/// UTM (E-N-U) to lat-lon-h
		/// </summary>
		/// <param name="enuz0">E-N-U-Zone#</param>
		/// <param name="hemi">northern or sourthern hemisphere</param>
		/// <returns>lat-lon-h</returns>
		LLH ENUZnHm2LLH(const ENUZnHemi& enuznhm, const ReferenceEllipsoid& ellips)
		{
			double& E = enuznhm[0];
			double& N = enuznhm[1];
			double& U = enuznhm[2];
			double& Zn = enuznhm[3];
			HEMISPHERE hemi = static_cast<HEMISPHERE>(enuznhm[4]);

			bool isSouthern = false;
			if (hemi == HEMISPHERE::SOUTHERN)
				isSouthern = true;

			double east, north;
			east = E - UTM::falseEasting;
			east /= UTM::scale;

			if (hemi == HEMISPHERE::SOUTHERN)
				north = N - UTM::falseNorthing;
			else
				north = N;
			north /= UTM::scale;

			auto cmeridian = getCentralMeridian(static_cast<unsigned int>(Zn));

			LL latlon(2);
			latlon = MapXYToLatLon(east, north, cmeridian, ellips);

			return latlon;
		}

		/**
		* @brief  ENU+Zone#+Hemisphere to ECEF
		*/
		ECEF ENUZnHm2ECEF(const ENUZnHemi& enuznhm, const ReferenceEllipsoid& ellips)
		{
			auto llh = ENUZnHm2LLH(enuznhm, ellips);
			return LLH2ECEF(llh, ellips);
		}

		/**
		* @brief  ECEF to ENU+Zone#+Hemisphere
		*/
		ENUZnHemi ECEF2ENUZnHm(const ECEF& ecef, const ReferenceEllipsoid& ellips)
		{
			auto llh = ECEF2LLH(ecef, ellips);
			return LLH2ENUZnHm(llh, ellips);
		}

		COORDSYSTYPE getCoordSysType(const std::string& typeName)
		{
			if (typeName == _ECEF)
				return COORDSYSTYPE::ECEF;
			else if (typeName == _ENU)
				return COORDSYSTYPE::ENU;
			else if (typeName == _LLH_DEG || typeName == _LLH_RAD)
				return COORDSYSTYPE::LLH;
			else
			{
				std::cout << "Error from getCoordSysType: wrong coordinate system type name: No match for " << typeName << std::endl;
				throw std::runtime_error("Error from getCoordSysType: wrong coordinate system type name");
			}
		}

		/**
		* @brief  general definition of a coordinate transformation function type
		*/
		typedef math::Arrayd(*transformCoord)(const math::Arrayd&, const ReferenceEllipsoid&);

		using PipeNode = std::pair<transformCoord, ReferenceEllipsoid>;

		bool selectFuncForECEF(const COORDSYSTYPE type, std::vector<PipeNode>& pipeline, const ReferenceEllipsoid& ellips)
		{
			PipeNode node;

			switch (type)
			{
			case COORDSYSTYPE::ECEF:
				break;
			case COORDSYSTYPE::ENU:
				node.first = ECEF2ENUZnHm;
				node.second = ellips;
				pipeline.push_back(node);
				break;
			case COORDSYSTYPE::LLH:
				node.first = ECEF2LLH;
				node.second = ellips;
				pipeline.push_back(node);
				break;
			default:
				std::cout << "Error from setPipline: wrong coordnate system type name" << std::endl;
				return false;
			}

			return true;
		}

		bool selectFuncForENU(const COORDSYSTYPE type, std::vector<PipeNode>& pipeline, const ReferenceEllipsoid& ellips)
		{
			PipeNode node;

			switch (type)
			{
			case COORDSYSTYPE::ECEF:
				node.first = ENUZnHm2ECEF;
				node.second = ellips;
				pipeline.push_back(node);
				break;
			case COORDSYSTYPE::ENU:
				break;
			case COORDSYSTYPE::LLH:
				node.first = ENUZnHm2LLH;
				node.second = ellips;
				pipeline.push_back(node);
				break;
			default:
				std::cout << "Error from setPipline: wrong coordnate system type name" << std::endl;
				return false;
			}

			return true;
		}

		bool selectFuncForLLH(const COORDSYSTYPE type, std::vector<PipeNode>& pipeline, const ReferenceEllipsoid& ellips)
		{
			PipeNode node;

			switch (type)
			{
			case COORDSYSTYPE::ECEF:
				node.first = LLH2ECEF;
				node.second = ellips;
				pipeline.push_back(node);
				break;
			case COORDSYSTYPE::ENU:
				node.first = LLH2ENUZnHm;
				node.second = ellips;
				pipeline.push_back(node);
				break;
			case COORDSYSTYPE::LLH:
				break;
			default:
				std::cout << "Error from setPipline: wrong coordnate system type name" << std::endl;
				return false;
			}

			return true;
		}

		/**
		* @brief  set a pipeline for sequential multiple coordinate transformation functions
		*/
		bool setPipeline(const std::string& inCoordSys, const ReferenceEllipsoid& inEllips, const std::string& outCoordSys, const ReferenceEllipsoid& outEllips, std::vector<PipeNode>& pipeline)
		{
			PipeNode node;

			if (!(inEllips == outEllips))
			{
				switch (getCoordSysType(inCoordSys))
				{
				case COORDSYSTYPE::ECEF:
					break;
				case COORDSYSTYPE::ENU:
					node.first = ENUZnHm2ECEF;
					node.second = inEllips;
					pipeline.push_back(node);
					break;
				case COORDSYSTYPE::LLH:
					node.first = LLH2ECEF;
					node.second = inEllips;
					pipeline.push_back(node);
					break;
				default:
					std::cout << "Error from setPipline: wrong coordnate system type name" << std::endl;
					throw std::runtime_error("Error from setPipline: wrong coordnate system type name");
					break;
				}

				switch (getCoordSysType(outCoordSys))
				{
				case COORDSYSTYPE::ECEF:
					break;
				case COORDSYSTYPE::ENU:
					node.first = ECEF2ENUZnHm;
					node.second = outEllips;
					pipeline.push_back(node);
					break;
				case COORDSYSTYPE::LLH:
					node.first = ECEF2LLH;
					node.second = outEllips;
					pipeline.push_back(node);
					break;
				default:
					std::cout << "Error from setPipline: wrong coordnate system type name" << std::endl;
					throw std::runtime_error("Error from setPipline: wrong coordnate system type name");
					break;
				}
			}
			else
			{
				switch (getCoordSysType(inCoordSys))
				{
				case COORDSYSTYPE::ECEF:
					if (!selectFuncForECEF(getCoordSysType(outCoordSys), pipeline, outEllips))
						return false;
					break;
				case COORDSYSTYPE::ENU:
					if (!selectFuncForENU(getCoordSysType(outCoordSys), pipeline, outEllips))
						return false;
					break;
				case COORDSYSTYPE::LLH:
					if (!selectFuncForLLH(getCoordSysType(outCoordSys), pipeline, outEllips))
						return false;
					break;
				default:
					std::cout << "Error from setPipline: wrong coordnate system type name" << std::endl;
					throw std::runtime_error("Error from setPipline: wrong coordnate system type name");
					break;
				}
			}

			return true;
		}

		/**
		* @brief  sequential coordinate transformation
		*/
		math::Arrayd sequentialTransform(const math::Arrayd& inCoord, const std::vector<PipeNode>& sequentialFuncs)
		{
			math::Arrayd retVal;
			retVal = inCoord;

			for (unsigned int i = 0; i < sequentialFuncs.size(); ++i)
			{
				retVal = sequentialFuncs[i].first(retVal, sequentialFuncs[i].second);
			}

			return retVal;
		}
	}
}