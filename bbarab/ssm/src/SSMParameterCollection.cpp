/*
* Copyright(c) 1999-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <ssm/include/SSMParameterCollection.h>

namespace param
{
	const std::string EulerAngles = std::string("Euler Angles");
	const std::string ENUPosition = std::string("ENU Position");

	ParameterCollection::ParameterCollection()
		: name(std::string("N/A")),
		description(std::string("N/A"))
	{
		collection.resize(1);
	};

	ParameterCollection::ParameterCollection(const ParameterCollection &copy) { getCopy(copy); }

	void ParameterCollection::operator = (const ParameterCollection &copy) { getCopy(copy); }

	ParameterCollection ParameterCollection::operator - (const ParameterCollection &copy) const
	{
		ParameterCollection retVal(*this);

		for (unsigned int i = 0; i < collection.size(); ++i)
		{
			retVal.collection[i] = collection[i] - copy.collection[i];
		}

		return retVal;
	}

	ParameterSet& ParameterCollection::operator [] (const unsigned int idx) { return collection[idx]; }

	unsigned int ParameterCollection::size() const { return static_cast<unsigned int>(collection.size()); }
	void ParameterCollection::resize(const unsigned int n) { collection.resize(n); }

	ParameterSet ParameterCollection::get(const unsigned int idx) const { return collection[idx]; }

	std::vector<ParameterSet> ParameterCollection::get() const { return collection; }
	void ParameterCollection::set(const std::vector<ParameterSet> &newVals) { collection = newVals; }

	std::string ParameterCollection::getName() const { return name; }
	void ParameterCollection::setName(const std::string &newName) { name = newName; }

	std::string ParameterCollection::getDescription() const { return name; }
	void ParameterCollection::setDescription(const std::string &newDescription) { description = newDescription; }

	unsigned int ParameterCollection::getAllNumParameters() const
	{
		unsigned int count = 0;

		for (unsigned int i = 0; i < collection.size(); ++i)
		{
			count += collection[i].size();
		}

		return count;
	}

	std::vector<double> ParameterCollection::getAllParameters() const
	{
		std::vector<double> params;

		for (unsigned int i = 0; i < collection.size(); ++i)
		{
			const auto& set = collection[i];

			for (unsigned int j = 0; j < set.get().size(); ++j)
			{
				const param::Parameter& p = set.get()[j];
				params.push_back(p.get());
			}
		}

		return params;
	}

	/// Copy member variables
	void ParameterCollection::getCopy(const ParameterCollection& copy)
	{
		this->collection = copy.collection;
		this->name = copy.name;
		this->description = copy.description;
	}

	void ParameterCollection::add(const ParameterCollection& copy)
	{
		for (unsigned int i = 0; i < copy.collection.size(); ++i)
		{
			add(copy.collection[i]);
		}
	}

	void ParameterCollection::add(const ParameterSet& copySet)
	{
		this->collection.push_back(copySet);
	}
}