/*
* Copyright(c) 1999-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/SSMCollinearity.h>
#include <ssm/include/SsmPhoto.h>

using namespace constant;

namespace ssm
{
	/**Collinearity equations for getting refined photo coordinates using 6 eop, camera, and object point*/
	data::Point2D Collinearity::getUndistortedPhotoCoordinates(const double omega, const double phi, const double kappa, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point3D& objPt)
	{
		const data::RotationMatrix mmat = math::rotation::getMMat(omega, phi, kappa);
		return getUndistortedPhotoCoordinates(mmat, pos, cam, objPt);
	}

	/**Collinearity equations for getting refined photo coordinates using M-rotation matrix, perspective center, camera, and object point*/
	data::Point2D Collinearity::getUndistortedPhotoCoordinates(const data::RotationMatrix& matM, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point3D& objPt)
	{
		auto rotatedObjVec = matM % (objPt - pos);

		data::Point2D refinedPhotoPt;
		try
		{
			refinedPhotoPt(0) = static_cast<double>(-cam->getFL()*(rotatedObjVec(0) / rotatedObjVec(2)));
			refinedPhotoPt(1) = static_cast<double>(-cam->getFL()*(rotatedObjVec(1) / rotatedObjVec(2)));
		}
		catch (...)
		{
			throw std::runtime_error("Divided by zero in runCollinearityEq(...)");
		}

		return refinedPhotoPt;
	}

	/**Collinearity equations for getting measured(distorted) photo coordinates using 6 eop, camera, and object point*/
	data::Point2D Collinearity::getDistortedPhotoCoordinates(const double omega, const double phi, const double kappa, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point3D& objPt)
	{
		auto mmat = math::rotation::getMMat(omega, phi, kappa);
		return getDistortedPhotoCoordinates(mmat, pos, cam, objPt);
	}

	/**Collinearity equations for getting measured(distorted) photo coordinates using M-rotation matrix, perspective center, camera, and object point*/
	data::Point2D Collinearity::getDistortedPhotoCoordinates(const data::RotationMatrix& matM, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point3D& objPt)
	{
		data::Point2D refinedPhotoPt = getUndistortedPhotoCoordinates(matM, pos, cam, objPt);
		data::Point2D distortedPhotoPt = cam->getDistortedPhotoCoordFromRefinedPhotoCoord(refinedPhotoPt);
		return distortedPhotoPt;
	}

	/**Collinearity equations for getting measured(distorted) image coordinates using 6 eop, camera, and object point*/
	data::Point2D Collinearity::getDistortedImageCoordinates(const double omega, const double phi, const double kappa, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point3D& objPt, bool& availability, const unsigned int widthMargin, const unsigned int heightMargin, const double targetGSD)
	{
		auto mmat = math::rotation::getMMat(omega, phi, kappa);
		return getDistortedImageCoordinates(mmat, pos, cam, objPt, availability, widthMargin, heightMargin, targetGSD);
	}

	/**Collinearity equations for getting measured(distorted) image coordinates using M-rotation matrix, perspective center, camera, and object point*/
	data::Point2D Collinearity::getDistortedImageCoordinates(const data::RotationMatrix& matM, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point3D& objPt, bool& availability, const unsigned int widthMargin, const unsigned int heightMargin, const double targetGSD)
	{
		data::Point2D refinedPhotoPt = getUndistortedPhotoCoordinates(matM, pos, cam, objPt);
		data::Point2D distortedImagePt = cam->getDistortedImgCoordFromRefinedPhotoCoord(refinedPhotoPt);

		/// Check scale and photo ray direction
		if(!checkAvailability(matM.transpose(), pos, cam, refinedPhotoPt, objPt, targetGSD))
			return false;

		/// Check an image boundary
		data::Point2D sensorSize = cam->getSensorSize();
				
		if (distortedImagePt(0) < widthMargin || 
			distortedImagePt(1) < heightMargin || 
			distortedImagePt(0) >= (sensorSize(0)-widthMargin) || 
			distortedImagePt(1) >= (sensorSize(1)-heightMargin))
			availability = false;
		else
			availability = true;

		return distortedImagePt;
	}

	/**Collinearity equations for getting measured(distorted) image coordinates using M-rotation matrix, perspective center, camera, and object point*/
	void Collinearity::getDistortedImageCoordinates(const std::vector<std::shared_ptr<param::Parameter>>& parameters, std::vector<double>& retVals)
	{
		/// Orientation angles
		double eopO, eopP, eopK;
		/// perspective center position coordinates
		const data::Point3D pos;
		/// object point coordinates
		const data::Point3D objPt;
		/// focal length
		double f;
		///Principal point
		data::Point2D pp;

		std::shared_ptr<param::Parameter> foundParam;
		bool found;
		
		foundParam = param::findParameter(parameters, constant::photo::_omega_, found);
		if(!found) throw std::runtime_error("Failed in finding a parameter");
		eopO = foundParam->get();

		foundParam = param::findParameter(parameters, constant::photo::_phi_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		eopP = foundParam->get();


		foundParam = param::findParameter(parameters, constant::photo::_kappa_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		eopK = foundParam->get();

		foundParam = param::findParameter(parameters, constant::photo::_X0_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		pos(0) = foundParam->get();

		foundParam = param::findParameter(parameters, constant::photo::_Y0_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		pos(1) = foundParam->get();

		foundParam = param::findParameter(parameters, constant::photo::_Z0_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		pos(2) = foundParam->get();

		foundParam = param::findParameter(parameters, constant::object::_Xa_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		objPt(0) = foundParam->get();

		foundParam = param::findParameter(parameters, constant::object::_Ya_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		objPt(1) = foundParam->get();

		foundParam = param::findParameter(parameters, constant::object::_Za_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		objPt(2) = foundParam->get();

		foundParam = param::findParameter(parameters, constant::camera::_f_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		f = foundParam->get();

		foundParam = param::findParameter(parameters, constant::camera::_xp_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		pp(0) = foundParam->get();

		foundParam = param::findParameter(parameters, constant::camera::_yp_, found);
		if (!found) throw std::runtime_error("Failed in finding a parameter");
		pp(1) = foundParam->get();

		/// M rotation matrix
		const data::RotationMatrix matM = math::rotation::getMMat(eopO, eopP, eopK);

		auto rotatedObjVec = matM % (objPt - pos);

		data::Point2D refinedPhotoPt;	

		try
		{
			refinedPhotoPt(0) = static_cast<double>(-f*(rotatedObjVec(0) / rotatedObjVec(2)));
			refinedPhotoPt(1) = static_cast<double>(-f*(rotatedObjVec(1) / rotatedObjVec(2)));
		}
		catch (...)
		{
			throw std::runtime_error("Divided by zero in runCollinearityEq(...)");
		}
				
		data::Point2D distortedImagePt = sensor::FrameCamera::getDistortedImgCoordFromRefinedPhotoCoord(parameters, refinedPhotoPt);

		retVals.resize(2);
		retVals[0] = distortedImagePt(0);
		retVals[1] = distortedImagePt(1);
	}

	/**Check scale and direction for availability*/
	bool Collinearity::checkAvailability(const double omega, const double phi, const double kappa, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point2D& photoCoord, const data::Point3D& obj, const double targetGSD)
	{
		const auto rMat = math::rotation::getRMat(omega, phi, kappa);
		return checkAvailability(rMat, pos, cam, photoCoord, obj, targetGSD);
	}

	/**Check scale and direction for availability*/
	bool Collinearity::checkAvailability(const data::RotationMatrix& rMat, const data::Point3D& pos, const std::shared_ptr<sensor::Camera> cam, const data::Point2D& photoCoord, const data::Point3D& obj, const double targetGSD)
	{
		double f = cam->getFL();
		data::Point3D photoVec0;
		photoVec0(0) = photoCoord(0);
		photoVec0(1) = photoCoord(1);
		photoVec0(2) = -f;

		const auto photoRay = rMat % photoVec0;
		const auto objVec = obj - pos;
		const double scale = objVec(2) / photoRay(2);

		if (scale < 0)
			return false;

		if (targetGSD != 0.0)
		{
			const auto objVec2 = objVec * objVec;
			double objDist = sqrt(objVec2(0) + objVec2(1) + objVec2(2));
			const auto photoCoord2 = photoCoord * photoCoord;
			double phoDist = sqrt(photoCoord2(0) + photoCoord2(1) + f*f);
			double GSD = cam->getPixPitch() * objDist / phoDist;

			if (GSD > targetGSD)
				return false;
		}

		return true;
	}

	math::Matrixd collinearityEqWithoutAlignment(const std::vector<std::shared_ptr<param::Parameter>>& givenParams, const std::vector<std::shared_ptr<param::Parameter>>& givenObs)
	{
		/// Camera and distortion model
		sensor::FrameCamera cam(givenParams);
		std::shared_ptr<sensor::SMACModel> smac(new sensor::SMACModel(givenParams));
		std::shared_ptr<sensor::DistortionModel> distModel = std::static_pointer_cast<sensor::DistortionModel>(smac);
		cam.setDistortionModel(distModel);

		/// Eop (or body-frame-origin)
		data::Point3D pc;
		data::EulerAngles opk;
		try 
		{
			pc(0) = givenParams[param::findIndex(givenParams, constant::photo::_X0_)]->get();
			pc(1) = givenParams[param::findIndex(givenParams, constant::photo::_Y0_)]->get();
			pc(2) = givenParams[param::findIndex(givenParams, constant::photo::_Z0_)]->get();

			opk(0) = givenParams[param::findIndex(givenParams, constant::photo::_omega_)]->get();
			opk(1) = givenParams[param::findIndex(givenParams, constant::photo::_phi_)]->get();
			opk(2) = givenParams[param::findIndex(givenParams, constant::photo::_kappa_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a eop parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}		

		/// M-rotation matrix
		const auto rotM = math::rotation::getMMat(opk(0), opk(1), opk(2));

		/// Object point coordinates
		data::Point3D objP;
		try
		{
			objP(0) = givenParams[param::findIndex(givenParams, constant::object::_Xa_)]->get();
			objP(1) = givenParams[param::findIndex(givenParams, constant::object::_Ya_)]->get();
			objP(2) = givenParams[param::findIndex(givenParams, constant::object::_Za_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a object point parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// Given observed image coordinates
		data::Point2D mesauredPhotoCoord;
		try
		{
			mesauredPhotoCoord(0) = givenObs[param::findIndex(givenObs, constant::photo::_xi_)]->get();
			mesauredPhotoCoord(1) = givenObs[param::findIndex(givenObs, constant::photo::_yi_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a image point parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// Residuals
		math::Matrixd res(2, 1);

		/// q, r, and s
		data::Point3D objP_Pos = objP - pc;
		double q =
			rotM(2, 0) * objP_Pos(0) +
			rotM(2, 1) * objP_Pos(1) +
			rotM(2, 2) * objP_Pos(2);

		if (fabs(q) < std::numeric_limits<double>::epsilon())
		{
			throw std::runtime_error("collinearityEq: q is less than double-epsilon.");
		}

		double r =
			rotM(0, 0) * objP_Pos(0) +
			rotM(0, 1) * objP_Pos(1) +
			rotM(0, 2) * objP_Pos(2);
		double s =
			rotM(1, 0) * objP_Pos(0) +
			rotM(1, 1) * objP_Pos(1) +
			rotM(1, 2) * objP_Pos(2);

		/// focal length
		double f = cam.getFL();

		//refined photo coordinates from the given distorted image coordinates
		data::Point2D refinedPhotoCoord = cam.getRefinedPhotoCoord(mesauredPhotoCoord);

		res(0) = refinedPhotoCoord(0) + f * r / q;
		res(1) = refinedPhotoCoord(1) + f * s / q;

		return res;
	}

	math::Matrix<double> makeM(const double omega, const double phi, const double kappa)
	{
		// Mmatrix = M(0,0,kappa)%M(0,phi,0)%M(omega,0,0)
		math::Matrix<double> Mmatrix;
		Mmatrix.resize(3, 3);

		Mmatrix(0, 0) = cos(phi) * cos(kappa);
		Mmatrix(0, 1) = sin(omega) * sin(phi) * cos(kappa) + cos(omega) * sin(kappa);
		Mmatrix(0, 2) = -cos(omega) * sin(phi) * cos(kappa) + sin(omega) * sin(kappa);

		Mmatrix(1, 0) = -cos(phi) * sin(kappa);
		Mmatrix(1, 1) = -sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
		Mmatrix(1, 2) = cos(omega) * sin(phi) * sin(kappa) + sin(omega) * cos(kappa);

		Mmatrix(2, 0) = sin(phi);
		Mmatrix(2, 1) = -sin(omega) * cos(phi);
		Mmatrix(2, 2) = cos(omega) * cos(phi);

		return Mmatrix;
	}

	math::Matrix<double> makeR(const double omega, const double phi, const double kappa)
	{
		// Rmatrix = R(omega,0,0)%R(0,phi,0)%R(0,0,kappa)
		math::Matrix<double> Rmatrix;
		Rmatrix.resize(3, 3);
		Rmatrix(0, 0) = cos(phi) * cos(kappa);
		Rmatrix(0, 1) = -cos(phi) * sin(kappa);
		Rmatrix(0, 2) = sin(phi);

		Rmatrix(1, 0) = sin(omega) * sin(phi) * cos(kappa) + cos(omega) * sin(kappa);
		Rmatrix(1, 1) = -sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
		Rmatrix(1, 2) = -sin(omega) * cos(phi);

		Rmatrix(2, 0) = -cos(omega) * sin(phi) * cos(kappa) + sin(omega) * sin(kappa);
		Rmatrix(2, 1) = cos(omega) * sin(phi) * sin(kappa) + sin(omega) * cos(kappa);
		Rmatrix(2, 2) = cos(omega) * cos(phi);

		return Rmatrix;
	}

	void ExtractRotation_Mmat(const math::Matrix<double> Mmatrix, double& omega, double& phi, double& kappa)
	{
		double omega_1, phi_1, kappa_1;
		double omega_2, phi_2, kappa_2;

		phi = asin(Mmatrix(2, 0));

		// two possible values for phi
		phi_1 = phi;
		if (phi < 0) phi_2 = -M_PI - phi;
		else phi_2 = M_PI - phi;

		omega_1 = atan2(-Mmatrix(2, 1) / cos(phi_1), Mmatrix(2, 2) / cos(phi_1));
		kappa_1 = atan2(-Mmatrix(1, 0) / cos(phi_1), Mmatrix(0, 0) / cos(phi_1));

		omega_2 = atan2(-Mmatrix(2, 1) / cos(phi_2), Mmatrix(2, 2) / cos(phi_2));
		kappa_2 = atan2(-Mmatrix(1, 0) / cos(phi_2), Mmatrix(0, 0) / cos(phi_2));

		math::Matrix<double> temp_1Mmatrix = makeM(omega_1, phi_1, kappa_1);
		math::Matrix<double> temp_2Mmatrix = makeM(omega_2, phi_2, kappa_2);

		double sum_1 = 0, sum_2 = 0;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				sum_1 += fabs(Mmatrix(i, j) - temp_1Mmatrix(i, j));
				sum_2 += fabs(Mmatrix(i, j) - temp_2Mmatrix(i, j));
			}
		}

		if (sum_1 < sum_2)
		{
			phi = phi_1;
			omega = omega_1;
			kappa = kappa_1;
		}
		else
		{
			phi = phi_2;
			omega = omega_2;
			kappa = kappa_2;
		}
	}

	void ExtractRotation_Rmat(const math::Matrix<double> Rmatrix, double& omega, double& phi, double& kappa)
	{
		double omega_1, phi_1, kappa_1;
		double omega_2, phi_2, kappa_2;

		phi = asin(Rmatrix(0, 2));

		if (phi < 0)
		{
			phi_1 = phi;
			phi_2 = -M_PI - phi;
		}
		else
		{
			phi_1 = phi;
			phi_2 = M_PI - phi;
		}

		omega_1 = atan2(-Rmatrix(1, 2) / cos(phi_1), Rmatrix(2, 2) / cos(phi_1));
		kappa_1 = atan2(-Rmatrix(0, 1) / cos(phi_1), Rmatrix(0, 0) / cos(phi_1));

		omega_2 = atan2(-Rmatrix(1, 2) / cos(phi_2), Rmatrix(2, 2) / cos(phi_2));
		kappa_2 = atan2(-Rmatrix(0, 1) / cos(phi_2), Rmatrix(0, 0) / cos(phi_2));

		math::Matrix<double> temp_1Rmatrix = makeR(omega_1, phi_1, kappa_1);
		math::Matrix<double> temp_2Rmatrix = makeR(omega_2, phi_2, kappa_2);

		double sum_1 = 0, sum_2 = 0;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				sum_1 += fabs(Rmatrix(i, j) - temp_1Rmatrix(i, j));
				sum_2 += fabs(Rmatrix(i, j) - temp_2Rmatrix(i, j));
			}
		}

		if (sum_1 < sum_2)
		{
			phi = phi_1;
			omega = omega_1;
			kappa = kappa_1;
		}
		else
		{
			phi = phi_2;
			omega = omega_2;
			kappa = kappa_2;
		}
	}

	math::Matrixd collinearityEqWithAlignment(const std::vector<std::shared_ptr<param::Parameter>>& givenParams, const std::vector<std::shared_ptr<param::Parameter>>& givenObs)
	{
		/// geo-referencing with alignment (boresight(Rb2m), leverarm(T)) 
		/// Xa = X0 + Rb2m * T + S * Rb2m * Rc2b * (x, y, -f) 
		/// S * Rb2m * Rc2b * (x, y, -f) = Xa -  X0 - Rb2m * T 
		/// S *  (x, y, -f) = (Rb2c * Rm2b)(Xa -  X0) - (Rb2c * Rm2b) * Rb2m * T 
		/// S *  (x, y, -f) = (q, r, s) = (Rb2c * Rm2b)(Xa -  X0) - Rb2c * T 
		/// x = -f (r/q) 
		/// y = -f (s/q)

		/// EOP derivation 
		/// S* (x, y, -f) = (Rb2c * Rm2b)(Xa - X0) - Rb2c * T 
		/// S * (x, y, -f) = (Rb2c * Rm2b)((Xa - X0) - Rb2m * T) 
		/// S * (x, y, -f) = (Rb2c * Rm2b)(Xa - (X0 + Rb2m * T)) 
		/// Orientation Matrix: Rb2c * Rm2b 
		/// Perspective center: (X0 + Rb2m * T)

		/// Camera and distortion model
		std::shared_ptr<sensor::SMACModel> smac(new sensor::SMACModel(givenParams));
		std::shared_ptr<sensor::DistortionModel> distModel = std::static_pointer_cast<sensor::DistortionModel>(smac);
		sensor::FrameCamera cam(givenParams);
		cam.setDistortionModel(distModel);

		///Bore-sight
		data::EulerAngles boresight_opk;

		try
		{
			boresight_opk(0) = givenParams[param::findIndex(givenParams, constant::camera::_omega_bs_)]->get();
			boresight_opk(1) = givenParams[param::findIndex(givenParams, constant::camera::_phi_bs_)]->get();
			boresight_opk(2) = givenParams[param::findIndex(givenParams, constant::camera::_kappa_bs_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a boresight parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// M-rotation matrix for boresight
		const auto rotBsM = math::rotation::getMMat(boresight_opk(0), boresight_opk(1), boresight_opk(2));

		///Lever-arm
		data::Point3D leverarm;
		try
		{
			leverarm(0) = givenParams[param::findIndex(givenParams, constant::camera::_dX_)]->get();
			leverarm(1) = givenParams[param::findIndex(givenParams, constant::camera::_dY_)]->get();
			leverarm(2) = givenParams[param::findIndex(givenParams, constant::camera::_dZ_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a leverarm parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// Eop (or body-frame-origin)
		data::Point3D pc;
		/// Rotation angles for bodyframe pose
		data::EulerAngles opk;

		try
		{
			pc(0) = givenParams[param::findIndex(givenParams, constant::photo::_X0_)]->get();
			pc(1) = givenParams[param::findIndex(givenParams, constant::photo::_Y0_)]->get();
			pc(2) = givenParams[param::findIndex(givenParams, constant::photo::_Z0_)]->get();

			opk(0) = givenParams[param::findIndex(givenParams, constant::photo::_omega_)]->get();
			opk(1) = givenParams[param::findIndex(givenParams, constant::photo::_phi_)]->get();
			opk(2) = givenParams[param::findIndex(givenParams, constant::photo::_kappa_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a eop parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// M-rotation matrix for bodyframe pose
		const auto rotBodyM = math::rotation::getMMat(opk(0), opk(1), opk(2));

		/// Object point coordinates
		data::Point3D objP;
		try
		{
			objP(0) = givenParams[param::findIndex(givenParams, constant::object::_Xa_)]->get();
			objP(1) = givenParams[param::findIndex(givenParams, constant::object::_Ya_)]->get();
			objP(2) = givenParams[param::findIndex(givenParams, constant::object::_Za_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a object point parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// Pa - Pc
		data::Point3D objP_Pos = objP - pc;

		/// (r, s, q)
		auto rsq = rotBsM % rotBodyM % objP_Pos - rotBsM % leverarm;

		if (fabs(rsq(2)) < std::numeric_limits<double>::epsilon())
		{
			throw std::runtime_error("collinearityEq: q is less than double-epsilon.");
		}

		/// Given observed image coordinates
		data::Point2D mesauredPhotoCoord;
		try
		{
			mesauredPhotoCoord(0) = givenObs[param::findIndex(givenObs, constant::photo::_xi_)]->get();
			mesauredPhotoCoord(1) = givenObs[param::findIndex(givenObs, constant::photo::_yi_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a image point parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// focal length
		double f = cam.getFL();

		/// Refined photo coordinates from the given measured photo coordinates
		data::Point2D refinedPhotoCoord = cam.getRefinedPhotoCoord(mesauredPhotoCoord);

		/// Residuals
		math::Matrixd res(2, 1);
		res(0) = refinedPhotoCoord(0) + f * rsq(0) / rsq(2);
		res(1) = refinedPhotoCoord(1) + f * rsq(1) / rsq(2);

		return res;
	}

	data::Point2D collinearityEqWithAlignment(const double f,
		const data::RotationMatrix& rotBsM,
		const data::Point3D& leverarm,
		const data::Point3D pc,
		const data::RotationMatrix& rotBodyM,
		const data::Point3D& objP)
	{
		/// geo-referencing with alignment (boresight(Rb2m), leverarm(T)) 
		/// Xa = X0 + Rb2m * T + S * Rb2m * Rc2b * (x, y, -f) 
		/// S * Rb2m * Rc2b * (x, y, -f) = Xa -  X0 - Rb2m * T 
		/// S *  (x, y, -f) = (Rb2c * Rm2b)(Xa -  X0) - (Rb2c * Rm2b) * Rb2m * T 
		/// S *  (x, y, -f) = (q, r, s) = (Rb2c * Rm2b)(Xa -  X0) - Rb2c * T 
		/// x = -f (r/q) 
		/// y = -f (s/q)

		/// EOP derivation 
		/// S* (x, y, -f) = (Rb2c * Rm2b)(Xa - X0) - Rb2c * T 
		/// S * (x, y, -f) = (Rb2c * Rm2b)((Xa - X0) - Rb2m * T) 
		/// S * (x, y, -f) = (Rb2c * Rm2b)(Xa - (X0 + Rb2m * T)) 
		/// Orientation Matrix: Rb2c * Rm2b 
		/// Perspective center: (X0 + Rb2m * T)

		/// Pa - Pc
		data::Point3D objP_Pos = objP - pc;

		/// (s, r, q)
		auto rsq = rotBsM % rotBodyM % objP_Pos - rotBsM % leverarm;

		if (fabs(rsq(2)) < std::numeric_limits<double>::epsilon())
		{
			throw std::runtime_error("collinearityEq: q is less than double-epsilon.");
		}

		/// Undistorted photo coordinates
		data::Point2D pho;
		pho(0) = -(f * rsq(0) / rsq(2));
		pho(1) = -(f * rsq(1) / rsq(2));

		return pho;
	}

	std::vector<data::Point2D> collinearityEqWithAlignment(const double f,
		const data::RotationMatrix& rotBsM,
		const data::Point3D& leverarm,
		const data::Point3D pc,
		const data::RotationMatrix& rotBodyM,
		const std::vector<data::Point3D>& objP)
	{
		std::vector<data::Point2D> phoPts(objP.size());

		for (unsigned int i = 0; i < objP.size(); ++i)
		{
			auto retVal = collinearityEqWithAlignment(f, rotBsM, leverarm, pc, rotBodyM, objP[i]);
			phoPts[i] = retVal;
		}

		return phoPts;
	}

	std::vector<data::Point2D> photo2DistortedPhoto(const std::shared_ptr<sensor::Camera>& cam, const std::vector<data::Point2D>& phoPts)
	{
		std::vector<data::Point2D> distortedPts(phoPts.size());

		for (unsigned int i = 0; i < phoPts.size(); ++i)
		{
			distortedPts[i] = cam->getDistortedPhotoCoordFromRefinedPhotoCoord(phoPts[i]);
		}
		return distortedPts;
	}

	std::vector<data::Point2D> photo2DistortedImg(const std::shared_ptr<sensor::Camera>& cam, const std::vector<data::Point2D>& phoPts)
	{
		/// Image coordinate (col, row)
		std::vector<data::Point2D> distortedImgPts(phoPts.size());

		for (unsigned int i = 0; i < phoPts.size(); ++i)
		{
			distortedImgPts[i] = cam->getDistortedImgCoordFromRefinedPhotoCoord(phoPts[i]);
		}

		return distortedImgPts;
	}

	math::Matrix<double> getPartialdMdO(const double omega, const double phi, const double kappa)
	{
		math::Matrix<double> dMdO(3, 3);

		dMdO(0, 0) = 0;
		dMdO(1, 0) = 0;
		dMdO(2, 0) = 0;

		dMdO(0, 1) = cos(omega) * sin(phi) * cos(kappa) - sin(omega) * sin(kappa);
		dMdO(1, 1) = -cos(omega) * sin(phi) * sin(kappa) - sin(omega) * cos(kappa);
		dMdO(2, 1) = -cos(omega) * cos(phi);

		dMdO(0, 2) = sin(omega) * sin(phi) * cos(kappa) + cos(omega) * sin(kappa);
		dMdO(1, 2) = -sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
		dMdO(2, 2) = -sin(omega) * cos(phi);

		return dMdO;
	}

	math::Matrix<double> getPartialdMdP(const double omega, const double phi, const double kappa)
	{
		math::Matrix<double> dMdP(3, 3);

		dMdP(0, 0) = -sin(phi) * cos(kappa);
		dMdP(1, 0) = sin(phi) * sin(kappa);
		dMdP(2, 0) = cos(phi);

		dMdP(0, 1) = sin(omega) * cos(phi) * cos(kappa);
		dMdP(1, 1) = -sin(omega) * cos(phi) * sin(kappa);
		dMdP(2, 1) = sin(omega) * sin(phi);

		dMdP(0, 2) = -cos(omega) * cos(phi) * cos(kappa);
		dMdP(1, 2) = cos(omega) * cos(phi) * sin(kappa);
		dMdP(2, 2) = -cos(omega) * sin(phi);

		return dMdP;
	}

	math::Matrix<double> getPartialdMdK(const double omega, const double phi, const double kappa)
	{
		math::Matrix<double> dMdK(3, 3);

		dMdK(0, 0) = -cos(phi) * sin(kappa);
		dMdK(1, 0) = -cos(phi) * cos(kappa);
		dMdK(2, 0) = 0.;

		dMdK(0, 1) = -sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
		dMdK(1, 1) = -sin(omega) * sin(phi) * cos(kappa) - cos(omega) * sin(kappa);
		dMdK(2, 1) = 0.;

		dMdK(0, 2) = cos(omega) * sin(phi) * sin(kappa) + sin(omega) * cos(kappa);
		dMdK(1, 2) = cos(omega) * sin(phi) * cos(kappa) - sin(omega) * sin(kappa);
		dMdK(2, 2) = 0.;

		return dMdK;
	}

	math::Matrixd collinearityPDForEop(const std::vector<std::shared_ptr<param::Parameter>>& givenParams, const std::vector<std::shared_ptr<param::Parameter>>& givenObs)
	{
		/// Camera and distortion model
		std::shared_ptr<sensor::SMACModel> smac(new sensor::SMACModel(givenParams));
		std::shared_ptr<sensor::DistortionModel> distModel = std::static_pointer_cast<sensor::DistortionModel>(smac);
		sensor::FrameCamera cam(givenParams);
		cam.setDistortionModel(distModel);

		///Bore-sight
		data::EulerAngles boresight_opk;

		try
		{
			boresight_opk(0) = givenParams[param::findIndex(givenParams, constant::camera::_omega_bs_)]->get();
			boresight_opk(1) = givenParams[param::findIndex(givenParams, constant::camera::_phi_bs_)]->get();
			boresight_opk(2) = givenParams[param::findIndex(givenParams, constant::camera::_kappa_bs_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a boresight parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// M-rotation matrix for boresight
		const auto rotBsM = math::rotation::getMMat(boresight_opk(0), boresight_opk(1), boresight_opk(2));

		///Lever-arm
		data::Point3D leverarm;
		try
		{
			leverarm(0) = givenParams[param::findIndex(givenParams, constant::camera::_dX_)]->get();
			leverarm(1) = givenParams[param::findIndex(givenParams, constant::camera::_dY_)]->get();
			leverarm(2) = givenParams[param::findIndex(givenParams, constant::camera::_dZ_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a leverarm parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// Eop (or body-frame-origin)
		data::Point3D pc;
		/// Rotation angles for bodyframe pose
		data::EulerAngles opk;

		try
		{
			pc(0) = givenParams[param::findIndex(givenParams, constant::photo::_X0_)]->get();
			pc(1) = givenParams[param::findIndex(givenParams, constant::photo::_Y0_)]->get();
			pc(2) = givenParams[param::findIndex(givenParams, constant::photo::_Z0_)]->get();

			opk(0) = givenParams[param::findIndex(givenParams, constant::photo::_omega_)]->get();
			opk(1) = givenParams[param::findIndex(givenParams, constant::photo::_phi_)]->get();
			opk(2) = givenParams[param::findIndex(givenParams, constant::photo::_kappa_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a eop parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// M-rotation matrix for bodyframe pose
		const auto rotBodyM = math::rotation::getMMat(opk(0), opk(1), opk(2));

		math::Matrix<double> pdPos(3, 3);
		pdPos(0, 0) = -1.0;
		pdPos(1, 0) = 0.0;
		pdPos(2, 0) = 0.0;

		pdPos(0, 1) = 0.0;
		pdPos(1, 1) = -1.0;
		pdPos(2, 1) = 0.0;

		pdPos(0, 2) = 0.0;
		pdPos(1, 2) = 0.0;
		pdPos(2, 2) = -1.0;

		/// Differential coefficients for position (X, Y, and Z)
		auto rsqdXYZ = rotBsM % rotBodyM % pdPos;
		/// Differential coefficients for an object point
		auto rsqdObjPt = rotBsM % rotBodyM % (pdPos * -1.0);

		/// M-rotation matrix for bodyframe pose
		const auto rotBodydMdO = getPartialdMdO(opk(0), opk(1), opk(2));
		const auto rotBodydMdP = getPartialdMdP(opk(0), opk(1), opk(2));
		const auto rotBodydMdK = getPartialdMdK(opk(0), opk(1), opk(2));

		/// Object point coordinates
		data::Point3D objP;
		try
		{
			objP(0) = givenParams[param::findIndex(givenParams, constant::object::_Xa_)]->get();
			objP(1) = givenParams[param::findIndex(givenParams, constant::object::_Ya_)]->get();
			objP(2) = givenParams[param::findIndex(givenParams, constant::object::_Za_)]->get();
		}
		catch (...)
		{
			std::string msg("Error in finding a object point parameter index");
			std::cout << msg << std::endl;
			throw std::runtime_error(msg);
		}

		/// Pa - Pc
		data::Point3D objP_Pos = objP - pc;

		auto rsq = rotBsM % rotBodyM % objP_Pos - rotBsM % leverarm;

		if (fabs(rsq(2)) < std::numeric_limits<double>::epsilon())
		{
			throw std::runtime_error("collinearityEq (partial derivatives): q(2) is less than double-epsilon.");
		}

		/// Differential coefficients for orientation angles (o, p, and k)
		auto rsqdO = rotBsM % rotBodydMdO % objP_Pos;
		auto rsqdP = rotBsM % rotBodydMdP % objP_Pos;
		auto rsqdK = rotBsM % rotBodydMdK % objP_Pos;

		math::Matrix<double> rsqPd(3, 9);
		rsqPd.insert(0, 0, rsqdO);///omega
		rsqPd.insert(0, 1, rsqdP);///phi
		rsqPd.insert(0, 2, rsqdK);///kappa
		rsqPd.insert(0, 3, rsqdXYZ);///x, y, z
		rsqPd.insert(0, 6, rsqdObjPt);///xa, ya, za

		/// focal length
		double f = cam.getFL();

		/// Partial derivatives for 6 eop parameters (o, p, k, x, y, z) and object point (xa, ya, za)
		math::Matrixd pd(2, 9);
		for (unsigned int i = 0; i < 9; ++i)
		{
			pd(0, i) = (f / rsq(2)) * (rsqPd(0, i) - rsq(0) * rsqPd(2, i) / rsq(2));
			pd(1, i) = (f / rsq(2)) * (rsqPd(1, i) - rsq(1) * rsqPd(2, i) / rsq(2));
		}

		return pd;
	}
}