/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <iostream>

#include <ssm/include/ssmParam.h>

namespace param
{
	/// Constructor
	Parameter::Parameter()
		: val(double(0)),
		val0(val),
		stDev(double(0)),
		delta(double(1.0e-6)),
		name(std::string("N/A")),
		description(std::string("N/A"))
	{
	}

	/// Copy constructor
	Parameter::Parameter(const Parameter& copy)
	{
		getCopy(copy);
	}

	/// Assignment operator
	void Parameter::operator = (const Parameter& copy)
	{
		getCopy(copy);
	}

	Parameter Parameter::operator - (const Parameter& copy) const
	{
		Parameter retVal(*this);
		retVal.val = this->val - copy.val;
		return retVal;
	}

	double& Parameter::get() { return val; }
	double Parameter::get() const { return val; }
	void Parameter::set(const double newVal) { val = newVal; }

	double Parameter::getInitVal() const { return val0; }
	void Parameter::setInitVal(const double newVal0) { val0 = newVal0; }

	double Parameter::getDelta() const { return delta; }
	void Parameter::setDelta(const double newDelta) { delta = newDelta; }

	double Parameter::getStDev() const { return stDev; }
	void Parameter::setStDev(const double newStDev) { stDev = newStDev; }

	std::string Parameter::getName() const { return name; }
	void Parameter::setName(const std::string& newName) { name = newName; }

	std::string Parameter::getDescription() const { return name; }
	void Parameter::setDescription(const std::string& newDescription) { description = newDescription; }

	void Parameter::getCopy(const Parameter& copy)
	{
		this->val = copy.val;
		this->val0 = copy.val0;
		this->stDev = copy.stDev;
		this->delta = copy.delta;
		this->name = copy.name;
		this->description = copy.description;
	}

	Parameter getParameter(const std::vector<Parameter>& parameters, const std::string& name)
	{
		for (auto& p : parameters)
		{
			if (p.getName() == name)
				return p;
		}

		return Parameter();
	}
	
	std::shared_ptr<Parameter> getParameter(std::vector<std::shared_ptr<Parameter>>& parameters, const std::string& name)
	{
		for (auto& p : parameters)
		{
			if (p->getName() == name)
			{
				return p;
			}
		}

		return nullptr;
	}

	bool findParameter(const std::vector<Parameter>& parameters, const std::string& name, param::Parameter& found)
	{
		for (auto& p : parameters)
		{
			if (p.getName() == name)
			{
				found = p;
				return true;
			}
		}

		return false;
	}

	std::shared_ptr<param::Parameter> findParameter(const std::vector< std::shared_ptr<Parameter>>& parameters, const std::string& name, bool& found)
	{

		for (auto& p : parameters)
		{
			if (p->getName() == name)
			{
				found = true;
				return p;
			}
		}

		found = false;

		std::shared_ptr<Parameter> temp;
		return temp;/// nullptr;
	}

	bool findParameter(const std::vector< std::shared_ptr<Parameter>>& parameters, const std::string& name, std::shared_ptr<param::Parameter>& found)
	{
		for (auto& p : parameters)
		{
			if (p->getName() == name)
			{
				found = p;
				return true;
			}
		}

		found = nullptr;
		return false;
	}

	bool compareParameters(std::vector<std::shared_ptr<param::Parameter>>& a, std::vector<std::shared_ptr<param::Parameter>>& b)
	{
		if (a.size() != b.size()) return false;

		for (unsigned int i = 0; i < a.size(); ++i)
		{
			if (a[i]->get() != b[i]->get())
				return false;
		}

		return true;
	}

	/// Get a parameter value using a parameter name
	double get(const std::vector<std::shared_ptr<Parameter>>& parameters, const std::string& name)
	{
		try
		{
			return parameters[findIndex(parameters, name)]->get();
		}
		catch (...)
		{
			throw std::runtime_error("Refer to a wrong index in param::get()");
		}
	}

	/// set a new parameter value using a parameter name
	void set(const std::vector<std::shared_ptr<Parameter>>& parameters, const std::string& name, const double value)
	{
		try
		{
			parameters[findIndex(parameters, name)]->set(value);
		}
		catch (...)
		{
			throw std::runtime_error("Refer to a wrong index in param::set()");
		}
	}

	/// Find an index of parameter using a parameter name
	int findIndex(const std::vector<std::shared_ptr<Parameter>>& parameters, const std::string& name)
	{
		for (unsigned int i = 0; i < parameters.size(); ++i)
		{
			if (parameters[i]->getName() == name)
			{
				return i;
			}
		}

		return -1;
	}
}