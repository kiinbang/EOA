/*
* Copyright(c) 2006-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

////////////////////////////////////////////////
//ROCoplanarity.cpp(공면조건식을 이용한 상호표정)
//Coplanarity condition Relative Orientation
//made by BbaraB
//revision date 2000-6-8
//revision date 2000-6-13
//revision date 2020-1-16
////////////////////////////////////////////////

#include <ssm/include/ROCoplanarity.h>


CModelCoplanarity::CModelCoplanarity(char *infile) : CModel(infile)
{
}

bool CModelCoplanarity::RO_dependent_PseudoObs()
{
	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	bool Increase_old = false,Increase_new = false;	//monitoring variance
	bool iteration_end = false;						//Iteration Stop
	//좌우 사진의 상호표정점 갯수 검사
	if(LPhoto.num_ROP == RPhoto.num_ROP)
	{
		num_ROPoint = LPhoto.num_ROP;
		ROmat.DF = num_ROPoint - 5;
	}
	else
	{
		return false;
	}

	//Initialize parameter
	InitApp_de();

	result = "[Relative Orientation(Dependent) By Coplanarity Condition(Pseudo Observation EQ)]\r\n\r\n\r\n";
	
	//Start Iteration
	do
	{
		std::string temp;
		iteration++;					//Increase Iteration Number(From 1)
		Increase_old = Increase_new;
		
		//make matrix A & L
		makeA_Ldependent();
		
		//compute X matrix
		ROmat.N = ROmat.A.transpose()%ROmat.A;
		ROmat.Ninv = ROmat.N.inverse();
		ROmat.X = ROmat.Ninv%ROmat.A.transpose()%ROmat.L;
		//compute V matrix
		ROmat.V = (ROmat.A%ROmat.X) - ROmat.L;
		//compute VTV
		ROmat.VTV = ROmat.V.transpose()%ROmat.V;
		//compute posteriori variance
		newvar = ROmat.pos_var = ROmat.VTV(0,0)/ROmat.DF;
		//compute standard deviation
		ROmat.SD = sqrt(ROmat.pos_var);
		//compute variance-covariance matrix
		ROmat.Var_Co = ROmat.Ninv*ROmat.pos_var;
		//compute variance matrix
		ROmat.Var.resize(5,1);
		for(i=0;i<(5);i++)
		{
			ROmat.Var(i,0) = ROmat.Var_Co(i,i);
		}

		//초기값 갱신
		Param_de.Romega += ROmat.X(0);
		Param_de.Rphi   += ROmat.X(1);
		Param_de.Rkappa += ROmat.X(2);
		Param_de.Yr     += Param_de.Xr*ROmat.X(3);
		Param_de.Zr     += Param_de.Xr*ROmat.X(4);
				
		//maximum correction(X matrix)
		ROmat.maxX = fabs(ROmat.X(0));
		for(i=1;i<(int)ROmat.X.getRows();i++)
		{
			if(fabs(ROmat.X(i))>ROmat.maxX)
				ROmat.maxX = fabs(ROmat.X(i));
		}		
		
		//Print Result
		temp = std::string("Iteration--->") + std::to_string(iteration) + std::string("\n\n\n");
		result +=temp;
		result += PrintResult_de_PseudoObs();
		
		//reference variance(posteriori variance)monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			Increase_new = true;
		}
		else
		{
			Increase_new = false;
		}
		
		//iteration end condition
		//variance difference monitoring 
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = true;
			result += "[Difference Between Old_Variance And New_Variance < Difference Variance limit !!!]\r\n";
		}
		else
		{
			oldvar = newvar;
		}
		//variance monitoring
		if(((Increase_old == true)&&(Increase_new == true)))
		{
			iteration_end = true;
			result += "[Variance Continuously(2회연속) Increase!!!]\r\n";
		}
		//SD monitoring
		if(fabs(ROmat.SD)<fabs(maxSD))
		{
			iteration_end = true;
			result += "[Standard Deviation < SD limit !!!]\r\n";
		}
		//max correction
		if(ROmat.maxX<fabs(maxCorrection))
		{
			iteration_end = true;
			result += "[Maximum Correction < correction limit !!!]\r\n";
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = true;
			result += "[Iteration Max_Num Execss!!!]\r\n";
		}
		

	}while(iteration_end == false);
		

	return true;
}

bool CModelCoplanarity::RO_independent_PseudoObs()
{

	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	bool Increase_old = false,Increase_new = false;	//monitoring variance
	bool iteration_end = false;						//Iteration Stop
	//좌우 사진의 상호표정점 갯수 검사
	if(LPhoto.num_ROP == RPhoto.num_ROP)
	{
		num_ROPoint = LPhoto.num_ROP;
		ROmat.DF = num_ROPoint - 5;
	}
	else
	{
		return false;
	}

	//Initialize parameter
	InitApp_inde();

	result = "[Relative Orientation(Independent) By Coplanarity Condition(Pseudo Observation EQ)]\r\n\r\n\r\n";
	
	//Start Iteration
	do
	{
		std::string temp;
		iteration++;					//Increase Iteration Number(From 1)
		Increase_old = Increase_new;
		
		//make matrix A & L
		makeA_Lindependent();
		
		//compute X matrix
		ROmat.N = ROmat.A.transpose()%ROmat.A;
		ROmat.Ninv = ROmat.N.inverse();
		ROmat.X = ROmat.Ninv%ROmat.A.transpose()%ROmat.L;
		//compute V matrix
		ROmat.V = (ROmat.A%ROmat.X) - ROmat.L;
		//compute VTV
		ROmat.VTV = ROmat.V.transpose()%ROmat.V;
		//compute posteriori variance
		newvar = ROmat.pos_var = ROmat.VTV(0,0)/ROmat.DF;
		//compute standard deviation
		ROmat.SD = sqrt(ROmat.pos_var);
		//compute variance-covariance matrix
		ROmat.Var_Co = ROmat.Ninv*ROmat.pos_var;
		//compute variance matrix
		ROmat.Var.resize(5,1);
		for(i=0;i<(5);i++)
		{
			ROmat.Var(i,0) = ROmat.Var_Co(i,i);
		}

		//초기값 갱신
		Param_inde.Lphi    += ROmat.X(0);
		Param_inde.Lkappa  += ROmat.X(1);
		Param_inde.Romega  += ROmat.X(2);
		Param_inde.Rphi    += ROmat.X(3);
		Param_inde.Rkappa  += ROmat.X(4);
				
		//maximum correction(X matrix)
		ROmat.maxX = fabs(ROmat.X(0));
		for(i=1;i<(int)ROmat.X.getRows();i++)
		{
			if(fabs(ROmat.X(i))>ROmat.maxX)
				ROmat.maxX = fabs(ROmat.X(i));
		}		
		
		//Print Result
		temp = std::string("Iteration--->") + std::to_string(iteration) + std::string("\n\n\n");
		result +=temp;
		result += PrintResult_inde_PseudoObs();
		
		//reference variance(posteriori variance)monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			Increase_new = true;
		}
		else
		{
			Increase_new = false;
		}
		
		//iteration end condition
		//variance difference monitoring 
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = true;
			result += "[Difference Between Old_Variance And New_Variance < Difference Variance limit !!!]\r\n";
		}
		else
		{
			oldvar = newvar;
		}
		//variance monitoring
		if(((Increase_old == true)&&(Increase_new == true)))
		{
			iteration_end = true;
			result += "[Variance Continuously(2회연속) Increase!!!]\r\n";
		}
		//SD monitoring
		if(fabs(ROmat.SD)<fabs(maxSD))
		{
			iteration_end = true;
			result += "[Standard Deviation < SD limit !!!]\r\n";
		}
		//max correction
		if(ROmat.maxX<fabs(maxCorrection))
		{
			iteration_end = true;
			result += "[Maximum Correction < correction limit !!!]\r\n";
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = true;
			result += "[Iteration Max_Num Excess!!!]\r\n";
		}
		

	}while(iteration_end == false);
		

	return true;
}


void CModelCoplanarity::makeA_Ldependent()
{
	int i;
	double xlo, ylo, xro, yro;//PPA
	double fl,fr;//focal length
	double by, bz;
	ROmat.A.resize(num_ROPoint,5,0.0);
	ROmat.L.resize(num_ROPoint,1,0.0);
	
	xlo = LPhoto.GetCamera().PPA(0); ylo = LPhoto.GetCamera().PPA(1);
	xro = RPhoto.GetCamera().PPA(0); yro = RPhoto.GetCamera().PPA(1);

	fl = LPhoto.GetCamera().f; fr =RPhoto.GetCamera().f;
	by = Param_de.Yr/Param_de.Xr; bz = (Param_de.Zr-Param_de.Zl)/Param_de.Xr;
	
	double omega = Param_de.Romega, phi = Param_de.Rphi, kappa = Param_de.Rkappa;
	data::RotationMatrix RT = math::rotation::getMMat(omega, phi, kappa); //rotation matrix(Ground->Photo)
	math::Matrix<double> R,dRdO,dRdP,dRdK;//rotation matrix(Photo->Ground)and partial derivatives w.r.t. rotation angles
	R = RT.transpose();
	
	dRdO = makedRdO(R);
	dRdP = makedRdP(omega,phi,kappa);
	dRdK = makedRdK(omega,phi,kappa);

	for(i=0;i<num_ROPoint;i++)
	{
		double xl = LPhoto.ROPoint[i](0)-xlo, yl = LPhoto.ROPoint[i](1)-ylo, zl = -fl;
		double xr = RPhoto.ROPoint[i](0)-xro, yr = RPhoto.ROPoint[i](1)-yro, zr = -fr;
		
		ROmat.A(i,0) = (bz*xl-zl)*(dRdO(1,0)*xr+dRdO(1,1)*yr+dRdO(1,2)*zr)+(yl-by*xl)*(dRdO(2,0)*xr+dRdO(2,1)*yr+dRdO(2,2)*zr);
		ROmat.A(i,1) = (by*zl-bz*yl)*(dRdP(0,0)*xr+dRdP(0,1)*yr+dRdP(0,2)*zr)+(bz*xl-zl)*(dRdP(1,0)*xr+dRdP(1,1)*yr+dRdP(1,2)*zr)+(yl-by*xl)*(dRdP(2,0)*xr+dRdP(2,1)*yr+dRdP(2,2)*zr);
		ROmat.A(i,2) = (by*zl-bz*yl)*(dRdK(0,0)*xr+dRdK(0,1)*yr)+(bz*xl-zl)*(dRdK(1,0)*xr+dRdK(1,1)*yr)+(yl-by*xl)*(dRdK(2,0)*xr+dRdK(2,1)*yr);
		ROmat.A(i,3) = zl*(R(0,0)*xr+R(0,1)*yr+R(0,2)*zr)-xl*(R(2,0)*xr+R(2,1)*yr+R(2,2)*zr);
		ROmat.A(i,4) = xl*(R(1,0)*xr+R(1,1)*yr+R(1,2)*zr)-yl*(R(0,0)*xr+R(0,1)*yr+R(0,2)*zr);

		ROmat.L(i,0) = -((by*zl-bz*yl)*(R(0,0)*xr+R(0,1)*yr+R(0,2)*zr)+(bz*xl-zl)*(R(1,0)*xr+R(1,1)*yr+R(1,2)*zr)+(yl-by*xl)*(R(2,0)*xr+R(2,1)*yr+R(2,2)*zr));

	}
	
}

void CModelCoplanarity::makeA_Lindependent()
{
	int i;
	double xlo, ylo, xro, yro;//PPA
	double fl,fr;//focal length
	ROmat.A.resize(num_ROPoint,5,0.0);
	ROmat.L.resize(num_ROPoint,1,0.0);
		
	xlo = LPhoto.camera.PPA(0); ylo = LPhoto.camera.PPA(1);
	xro = RPhoto.camera.PPA(0); yro = RPhoto.camera.PPA(1);

	fl = LPhoto.camera.f; fr =RPhoto.camera.f;
	
	double Lomega = Param_inde.Lomega, Lphi = Param_inde.Lphi, Lkappa = Param_inde.Lkappa;
	double Romega = Param_inde.Romega, Rphi = Param_inde.Rphi, Rkappa = Param_inde.Rkappa;
	data::RotationMatrix LRT = math::rotation::getMMat(Lomega, Lphi, Lkappa); //Left rotation matrix(Ground->Photo)
	data::RotationMatrix RRT = math::rotation::getMMat(Romega, Rphi, Rkappa); //Right rotation matrix(Ground->Photo)
	math::Matrix<double> LR,dLRdP,dLRdK;//Left rotation matrix (Photo->Ground)and 
	math::Matrix<double> RR,dRRdO,dRRdP,dRRdK;//Right rotation matrix (Photo->Ground)와 partial derivatives w.r.t. rotation angles
	LR = LRT.transpose();
	RR = RRT.transpose();
	
	dLRdP = makedRdP(Lomega,Lphi,Lkappa);
	dLRdK = makedRdK(Lomega,Lphi,Lkappa);

	dRRdO = makedRdO(RR);
	dRRdP = makedRdP(Romega,Rphi,Rkappa);
	dRRdK = makedRdK(Romega,Rphi,Rkappa);

	for(i=0;i<num_ROPoint;i++)
	{
		double xl = LPhoto.ROPoint[i](0)-xlo, yl = LPhoto.ROPoint[i](1)-ylo, zl = -fl;
		double xr = RPhoto.ROPoint[i](0)-xro, yr = RPhoto.ROPoint[i](1)-yro, zr = -fr;
		
		double yl_,zl_,yr_,zr_;
		yl_ = LR(1,0)*xl + LR(1,1)*yl + LR(1,2)*zl;
		zl_ = LR(2,0)*xl + LR(2,1)*yl + LR(2,2)*zl;
		yr_ = RR(1,0)*xr + RR(1,1)*yr + RR(1,2)*zr;
		zr_ = RR(2,0)*xr + RR(2,1)*yr + RR(2,2)*zr;
		
		//Left omega & phi
		ROmat.A(i,0) = (dLRdP(1,0)*xl+dLRdP(1,1)*yl+dLRdP(1,2)*zl)*zr_ - (dLRdP(2,0)*xl+dLRdP(2,1)*yl+dLRdP(2,2)*zl)*yr_;
		ROmat.A(i,1) = (dLRdK(1,0)*xl+dLRdK(1,1)*yl)*zr_ - (dLRdK(2,0)*xl+dLRdK(2,1)*yl)*yr_;
		//Right omega & phi & kappa
		ROmat.A(i,2) = (dRRdO(2,0)*xr + dRRdO(2,1)*yr + dRRdO(2,2)*zr)*yl_ - (dRRdO(1,0)*xr + dRRdO(1,1)*yr + dRRdO(1,2)*zr)*zl_;
		ROmat.A(i,3) = (dRRdP(2,0)*xr + dRRdP(2,1)*yr + dRRdP(2,2)*zr)*yl_ - (dRRdP(1,0)*xr + dRRdP(1,1)*yr + dRRdP(1,2)*zr)*zl_;
		ROmat.A(i,4) = (dRRdK(2,0)*xr + dRRdK(2,1)*yr)*yl_ - (dRRdK(1,0)*xr + dRRdK(1,1)*yr)*zl_;

		
		ROmat.L(i,0) = -((yl_*zr_) - (yr_*zl_));

	}
}

void CModelCoplanarity::InitApp_de()
{
	int i;
	double sum = 0.0;
	for(i=0;i<num_ROPoint;i++)
	{
		sum += LPhoto.ROPoint[i](0) - RPhoto.ROPoint[i](0);
	}
	
	//Initialize RO Parameters
	Param_de.Lomega = Param_de.Lphi = Param_de.Lkappa = 0.0;
	Param_de.Xl = Param_de.Yl = 0.0;
	Param_de.Zl = LPhoto.camera.f;
	Param_de.Romega = Param_de.Rphi = Param_de.Rkappa = 0.0;
	Param_de.Xr = sum/LPhoto.num_ROP;
	Param_de.Yr = 0.0;
	Param_de.Zr = RPhoto.camera.f;
}

void CModelCoplanarity::InitApp_inde()
{
	int i;
	double sum = 0.0;
	for(i=0;i<num_ROPoint;i++)
	{
		sum += LPhoto.ROPoint[i](0) - RPhoto.ROPoint[i](0);
	}
	
	//Initialize RO Parameters
	Param_inde.Lomega = Param_inde.Lphi = Param_inde.Lkappa = 0.0;
	Param_inde.Xl = Param_inde.Yl = 0.0;
	Param_inde.Zl = LPhoto.camera.f;
	Param_inde.Romega = Param_inde.Rphi = Param_inde.Rkappa = 0.0;
	Param_inde.Xr = sum/LPhoto.num_ROP;
	Param_inde.Yr = 0.0;
	Param_inde.Zr = RPhoto.camera.f;
}

std::string CModelCoplanarity::PrintResult_de_PseudoObs()
{
	std::string temp;
	std::string result;
	result += std::string("[A matrix]\n");
	result += matrixout(ROmat.A);
	result += std::string("\n\n");
	result += std::string("[L matrix]\n");
	result += matrixout(ROmat.L);
	result += std::string("\n\n");
	result += std::string("[X matrix]\n");
	result += matrixout(ROmat.X);
	result += std::string("\n\n");
	result += std::string("[V matrix]\n");
	result += matrixout(ROmat.V);
	result += std::string("\n\n");
	result += std::string("[VTV matrix]\n");
	result += matrixout(ROmat.VTV);
	result += std::string("\n\n");
	temp = std::string("[A-Posteriori Variance]\n") + std::to_string(ROmat.pos_var) + std::string("\n\n\n");
	result += temp;
	temp = std::string("Maximum element of X matrix : ") + std::to_string(ROmat.maxX) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
	result += std::string("[Variance_Corvariance matrix]\n");
	result += matrixout(ROmat.Var_Co);
	result += std::string("\n\n");
	result += std::string("[Variance matrix]\n");
	result += matrixout(ROmat.Var);
	result += std::string("\n\n");
	result += std::string("[RO_point(3D-model space) Coordinate]\n");
	result += matrixout(Ga);
	result += std::string("\n\n");
		
	result += "[RO Parameter(Dependent)]\r\n";
	temp = std::string("LPhoto omega : ") + std::to_string(Param_de.Lomega) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto phi : ") + std::to_string(Param_de.Lphi) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto kappa : ") + std::to_string(Param_de.Lkappa) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Xl : ") + std::to_string(Param_de.Xl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Yl : ") + std::to_string(Param_de.Yl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Zl : ") + std::to_string(Param_de.Zl) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto omega : ") + std::to_string(Param_de.Romega) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto phi : ") + std::to_string(Param_de.Rphi) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto kappa : ") + std::to_string(Param_de.Rkappa) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Xr : ") + std::to_string(Param_de.Xr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Yr : ") + std::to_string(Param_de.Yr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Zr : ") + std::to_string(Param_de.Zr) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
		
	return result;

}

std::string CModelCoplanarity::PrintResult_inde_PseudoObs()
{
	std::string temp;
	std::string result;
	result += std::string("[A matrix]\n");
	result += matrixout(ROmat.A);
	result += std::string("\n\n");
	result += std::string("[L matrix]\n");
	result += matrixout(ROmat.L);
	result += std::string("\n\n");
	result += std::string("[X matrix]\n");
	result += matrixout(ROmat.X);
	result += std::string("\n\n");
	result += std::string("[V matrix]\n");
	result += matrixout(ROmat.V);
	result += std::string("\n\n");
	result += std::string("[VTV matrix]\n");
	result += matrixout(ROmat.VTV);
	result += std::string("\n\n");
	temp = std::string("[A-Posteriori Variance]\n") + std::to_string(ROmat.pos_var) + std::string("\n\n\n");
	result += temp;
	temp = std::string("Maximum element of X matrix : ") + std::to_string(ROmat.maxX) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
	result += std::string("[Variance_Corvariance matrix]\n");
	result += matrixout(ROmat.Var_Co);
	result += std::string("\n\n");
	result += std::string("[Variance matrix]\n");
	result += matrixout(ROmat.Var);
	result += std::string("\n\n");
	result += std::string("[RO_point(3D-model space) Coordinate]\n");
	result += matrixout(Ga);
	result += std::string("\n\n");

	result += std::string("[RO Parameter(Independent)]\n");
	temp = std::string("LPhoto omega : ") + std::to_string(Param_inde.Lomega) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto phi : ") + std::to_string(Param_inde.Lphi) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto kappa : ") + std::to_string(Param_inde.Lkappa) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Xl : ") + std::to_string(Param_inde.Xl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Yl : ") + std::to_string(Param_inde.Yl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Zl : ") + std::to_string(Param_inde.Zl) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto omega : ") + std::to_string(Param_inde.Romega) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto phi : ") + std::to_string(Param_inde.Rphi) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto kappa : ") + std::to_string(Param_inde.Rkappa) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Xr : ") + std::to_string(Param_inde.Xr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Yr : ") + std::to_string(Param_inde.Yr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Zr : ") + std::to_string(Param_inde.Zr) + std::string("\n");
	result += temp;
	result += std::string("\n\n");

	return result;

}

std::string CModelCoplanarity::PrintResult_de_General()
{
	std::string temp;
	std::string result;
	result += std::string("[A matrix]\n");
	result += matrixout(ROmat.A);
	result += std::string("\n\n");
	result += std::string("[B matrix]\n");
	result += matrixout(ROmat.B);
	result += std::string("\n\n");
	result += std::string("[L matrix]\n");
	result += matrixout(ROmat.L);
	result += std::string("\n\n");
	result += std::string("[X matrix]\n");
	result += matrixout(ROmat.X);
	result += std::string("\n\n");
	result += std::string("[Ve matrix]\n");
	result += matrixout(ROmat.Ve);
	result += std::string("\n\n");
	result += std::string("[V matrix]\n");
	result += matrixout(ROmat.V);
	result += std::string("\n\n");
	result += std::string("[VTV matrix]\n");
	result += matrixout(ROmat.VTV);
	result += std::string("\n\n");
	temp = std::string("[A-Posteriori Variance]\n") + std::to_string(ROmat.pos_var) + std::string("\n\n\n");
	result += temp;
	temp = std::string("Maximum element of X matrix : ") + std::to_string(ROmat.maxX) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
	result += std::string("[Variance_Corvariance matrix]\n");
	result += matrixout(ROmat.Var_Co);
	result += std::string("\n\n");
	result += std::string("[Variance matrix]\n");
	result += matrixout(ROmat.Var);
	result += std::string("\n\n");
			
	result += "[RO Parameter(Dependent)]\r\n";
	temp = std::string("LPhoto omega : ") + std::to_string(Param_de.Lomega) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto phi : ") + std::to_string(Param_de.Lphi) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto kappa : ") + std::to_string(Param_de.Lkappa) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Xl : ") + std::to_string(Param_de.Xl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Yl : ") + std::to_string(Param_de.Yl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Zl : ") + std::to_string(Param_de.Zl) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto omega : ") + std::to_string(Param_de.Romega) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto phi : ") + std::to_string(Param_de.Rphi) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto kappa : ") + std::to_string(Param_de.Rkappa) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Xr : ") + std::to_string(Param_de.Xr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Yr : ") + std::to_string(Param_de.Yr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Zr : ") + std::to_string(Param_de.Zr) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
		
	return result;

}

std::string CModelCoplanarity::PrintResult_inde_General()
{
	std::string temp;
	std::string result;
	result += std::string("[A matrix]\n");
	result += matrixout(ROmat.A);
	result += std::string("\n\n");
	result += std::string("[B matrix]\n");
	result += matrixout(ROmat.B);
	result += std::string("\n\n");
	result += std::string("[L matrix]\n");
	result += matrixout(ROmat.L);
	result += std::string("\n\n");
	result += std::string("[X matrix]\n");
	result += matrixout(ROmat.X);
	result += std::string("\n\n");
	result += std::string("[Ve matrix]\n");
	result += matrixout(ROmat.Ve);
	result += std::string("\n\n");
	result += std::string("[V matrix]\n");
	result += matrixout(ROmat.V);
	result += std::string("\n\n");
	result += std::string("[VTV matrix]\n");
	result += matrixout(ROmat.VTV);
	result += std::string("\n\n");
	temp = std::string("[A-Posteriori Variance]\n") + std::to_string(ROmat.pos_var) + std::string("\n\n\n");
	result += temp;
	temp = std::string("Maximum element of X matrix : ") + std::to_string(ROmat.maxX) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
	result += std::string("[Variance_Corvariance matrix]\n");
	result += matrixout(ROmat.Var_Co);
	result += std::string("\n\n");
	result += std::string("[Variance matrix]\n");
	result += matrixout(ROmat.Var);
	result += std::string("\n\n");
		
	result += std::string("[RO Parameter(Independent)]\n");
	temp = std::string("LPhoto omega : ") + std::to_string(Param_inde.Lomega) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto phi : ") + std::to_string(Param_inde.Lphi) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto kappa : ") + std::to_string(Param_inde.Lkappa) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Xl : ") + std::to_string(Param_inde.Xl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Yl : ") + std::to_string(Param_inde.Yl) + std::string("\n");
	result += temp;
	temp = std::string("LPhoto Zl : ") + std::to_string(Param_inde.Zl) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto omega : ") + std::to_string(Param_inde.Romega) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto phi : ") + std::to_string(Param_inde.Rphi) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto kappa : ") + std::to_string(Param_inde.Rkappa) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Xr : ") + std::to_string(Param_inde.Xr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Yr : ") + std::to_string(Param_inde.Yr) + std::string("\n");
	result += temp;
	temp = std::string("RPhoto Zr : ") + std::to_string(Param_inde.Zr) + std::string("\n");
	result += temp;
	result += std::string("\n\n");
		
	return result;

}

bool CModelCoplanarity::RO_dependent_General()
{
	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	bool Increase_old = false,Increase_new = false;	//monitoring variance
	bool iteration_end = false;						//Iteration Stop
	/// check number of tie points
	if(LPhoto.num_ROP == RPhoto.num_ROP)
	{
		num_ROPoint = LPhoto.num_ROP;
		ROmat.DF = num_ROPoint - 5;
	}
	else
	{
		return false;
	}

	//Initialize parameter
	InitApp_de();

	result = "[Relative Orientation(Dependent) By Coplanarity Condition(General Lease Square)]\r\n\r\n\r\n";
	
	//Start Iteration
	do
	{
		std::string temp;
		iteration++;					//Increase Iteration Number(From 1)
		Increase_old = Increase_new;
		
		//make matrix A & L & B
		makeA_L_Bdependent();
		
		//compute We(equivalent weight matrix)
		ROmat.We = (ROmat.B%ROmat.B.transpose()).inverse();
		//compute X matrix
		ROmat.N = ROmat.A.transpose()%ROmat.We%ROmat.A;
		ROmat.Ninv = ROmat.N.inverse();
		ROmat.X = ROmat.Ninv%ROmat.A.transpose()%ROmat.We%ROmat.L;
		//compute Ve(equivalent residuals) matrix
		ROmat.Ve = (ROmat.A%ROmat.X) - ROmat.L;
		//compute V(residual)
		ROmat.V = ROmat.B.transpose()%ROmat.We%ROmat.Ve;
		//compute VTV
		ROmat.VTV = ROmat.V.transpose()%ROmat.V;
		//compute posteriori variance
		newvar = ROmat.pos_var = ROmat.VTV(0,0)/ROmat.DF;
		//compute standard deviation
		ROmat.SD = sqrt(ROmat.pos_var);
		//compute variance-covariance matrix
		ROmat.Var_Co = ROmat.Ninv*ROmat.pos_var;
		//compute variance matrix
		ROmat.Var.resize(5,1);
		for(i=0;i<(5);i++)
		{
			ROmat.Var(i,0) = ROmat.Var_Co(i,i);
		}

		//초기값 갱신
		Param_de.Romega += ROmat.X(0);
		Param_de.Rphi   += ROmat.X(1);
		Param_de.Rkappa += ROmat.X(2);
		Param_de.Yr     += Param_de.Xr*ROmat.X(3);
		Param_de.Zr     += Param_de.Xr*ROmat.X(4);
				
		//maximum correction(X matrix)
		ROmat.maxX = fabs(ROmat.X(0));
		for(i=1;i<(int)ROmat.X.getRows();i++)
		{
			if(fabs(ROmat.X(i))>ROmat.maxX)
				ROmat.maxX = fabs(ROmat.X(i));
		}		
		
		//Print Result
		temp = std::string("Iteration--->") + std::to_string(iteration) + std::string("\n\n\n");
		result +=temp;
		result += PrintResult_de_General();
		
		//reference variance(posteriori variance)monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			Increase_new = true;
		}
		else
		{

		}
		{
			Increase_new = false;
		}
		
		//iteration end condition
		//variance difference monitoring 
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = true;
			result += "[Difference Between Old_Variance And New_Variance < Difference Variance limit !!!]\r\n";
		}
		else
		{
			oldvar = newvar;
		}
		//variance monitoring
		if(((Increase_old == true)&&(Increase_new == true)))
		{
			iteration_end = true;
			result += "[Variance Continuously(2회연속) Increase!!!]\r\n";
		}
		//SD monitoring
		if(fabs(ROmat.SD)<fabs(maxSD))
		{
			iteration_end = true;
			result += "[Standard Deviation < SD limit !!!]\r\n";
		}
		//max correction
		if(ROmat.maxX<fabs(maxCorrection))
		{
			iteration_end = true;
			result += "[Maximum Correction < correction limit !!!]\r\n";
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = true;
			result += "[Iteration Max_Num Excess!!!]\r\n";
		}
		

	}while(iteration_end == false);
	
	
	return true;
}

bool CModelCoplanarity::RO_independent_General()
{
	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	bool Increase_old = false,Increase_new = false;	//monitoring variance
	bool iteration_end = false;						//Iteration Stop
	//좌우 사진의 상호표정점 갯수 검사
	if(LPhoto.num_ROP == RPhoto.num_ROP)
	{
		num_ROPoint = LPhoto.num_ROP;
		ROmat.DF = num_ROPoint - 5;
	}
	else
	{
		return false;
	}

	//Initialize parameter
	InitApp_inde();

	result = "[Relative Orientation(Independent) By Co-planarity Condition(General Lease Square)]\r\n\r\n\r\n";
	
	//Start Iteration
	do
	{
		std::string temp;
		iteration++;					//Increase Iteration Number(From 1)
		Increase_old = Increase_new;
		
		//make matrix A & L & B
		makeA_L_Bindependent();
		
		//compute We(equivalent weight matrix)
		ROmat.We = (ROmat.B%ROmat.B.transpose()).inverse();
		//compute X matrix
		ROmat.N = ROmat.A.transpose()%ROmat.We%ROmat.A;
		ROmat.Ninv = ROmat.N.inverse();
		ROmat.X = ROmat.Ninv%ROmat.A.transpose()%ROmat.We%ROmat.L;
		//compute Ve(equivalent residuals) matrix
		ROmat.Ve = (ROmat.A%ROmat.X) - ROmat.L;
		//compute V(residual)
		ROmat.V = ROmat.B.transpose()%ROmat.We%ROmat.Ve;
		//compute VTV
		ROmat.VTV = ROmat.V.transpose()%ROmat.V;
		//compute posteriori variance
		newvar = ROmat.pos_var = ROmat.VTV(0,0)/ROmat.DF;
		//compute standard deviation
		ROmat.SD = sqrt(ROmat.pos_var);
		//compute variance-covariance matrix
		ROmat.Var_Co = ROmat.Ninv*ROmat.pos_var;
		//compute variance matrix
		ROmat.Var.resize(5,1);
		for(i=0;i<(5);i++)
		{
			ROmat.Var(i,0) = ROmat.Var_Co(i,i);
		}

		//초기값 갱신
		Param_inde.Lphi    += ROmat.X(0);
		Param_inde.Lkappa  += ROmat.X(1);
		Param_inde.Romega  += ROmat.X(2);
		Param_inde.Rphi    += ROmat.X(3);
		Param_inde.Rkappa  += ROmat.X(4);
				
		//maximum correction(X matrix)
		ROmat.maxX = fabs(ROmat.X(0));
		for(i=1;i<(int)ROmat.X.getRows();i++)
		{
			if(fabs(ROmat.X(i))>ROmat.maxX)
				ROmat.maxX = fabs(ROmat.X(i));
		}		
		
		//Print Result
		temp = std::string("Iteration--->") + std::to_string(iteration) + std::string("\n\n\n");
		result +=temp;
		result += PrintResult_inde_General();
		
		//reference variance(posteriori variance)monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			Increase_new = true;
		}
		else
		{
			Increase_new = false;
		}
		
		//iteration end condition
		//variance difference monitoring 
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = true;
			result += "[Difference Between Old_Variance And New_Variance < Difference Variance limit !!!]\r\n";
		}
		else
		{
			oldvar = newvar;
		}
		//variance monitoring
		if(((Increase_old == true)&&(Increase_new == true)))
		{
			iteration_end = true;
			result += "[Variance Continuously(2회연속) Increase!!!]\r\n";
		}
		//SD monitoring
		if(fabs(ROmat.SD)<fabs(maxSD))
		{
			iteration_end = true;
			result += "[Standard Deviation < SD limit !!!]\r\n";
		}
		//max correction
		if(ROmat.maxX<fabs(maxCorrection))
		{
			iteration_end = true;
			result += "[Maximum Correction < correction limit !!!]\r\n";
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = true;
			result += "[Iteration Max_Num Excess!!!]\r\n";
		}
		

	}while(iteration_end == false);
		
	return true;
}

void CModelCoplanarity::makeA_L_Bdependent()
{
	int i;
	double xlo, ylo, xro, yro;//PPA
	double fl,fr;//focal length
	double by, bz;
	ROmat.A.resize(num_ROPoint,5,0.0);
	ROmat.L.resize(num_ROPoint,1,0.0);
	ROmat.B.resize(num_ROPoint,4*num_ROPoint,0.0);
		
	
	xlo = LPhoto.camera.PPA(0); ylo = LPhoto.camera.PPA(1);
	xro = RPhoto.camera.PPA(0); yro = RPhoto.camera.PPA(1);

	fl = LPhoto.camera.f; fr =RPhoto.camera.f;
	by = Param_de.Yr/Param_de.Xr; bz = (Param_de.Zr-Param_de.Zl)/Param_de.Xr;
	
	double omega = Param_de.Romega, phi = Param_de.Rphi, kappa = Param_de.Rkappa;
	data::RotationMatrix RT = math::rotation::getMMat(omega, phi, kappa); //rotation matrix(Ground->Photo)
	math::Matrix<double> R,dRdO,dRdP,dRdK;/// rotation matrix Photo->Ground)와 partial derivatives w.r.t. rotation angles
	R = RT.transpose();
	
	dRdO = makedRdO(R);
	dRdP = makedRdP(omega,phi,kappa);
	dRdK = makedRdK(omega,phi,kappa);

	for(i=0;i<num_ROPoint;i++)
	{
		double xl = LPhoto.ROPoint[i](0)-xlo, yl = LPhoto.ROPoint[i](1)-ylo, zl = -fl;
		double xr = RPhoto.ROPoint[i](0)-xro, yr = RPhoto.ROPoint[i](1)-yro, zr = -fr;
		
		ROmat.A(i,0) = (bz*xl-zl)*(dRdO(1,0)*xr+dRdO(1,1)*yr+dRdO(1,2)*zr)+(yl-by*xl)*(dRdO(2,0)*xr+dRdO(2,1)*yr+dRdO(2,2)*zr);
		ROmat.A(i,1) = (by*zl-bz*yl)*(dRdP(0,0)*xr+dRdP(0,1)*yr+dRdP(0,2)*zr)+(bz*xl-zl)*(dRdP(1,0)*xr+dRdP(1,1)*yr+dRdP(1,2)*zr)+(yl-by*xl)*(dRdP(2,0)*xr+dRdP(2,1)*yr+dRdP(2,2)*zr);
		ROmat.A(i,2) = (by*zl-bz*yl)*(dRdK(0,0)*xr+dRdK(0,1)*yr)+(bz*xl-zl)*(dRdK(1,0)*xr+dRdK(1,1)*yr)+(yl-by*xl)*(dRdK(2,0)*xr+dRdK(2,1)*yr);
		ROmat.A(i,3) = zl*(R(0,0)*xr+R(0,1)*yr+R(0,2)*zr)-xl*(R(2,0)*xr+R(2,1)*yr+R(2,2)*zr);
		ROmat.A(i,4) = xl*(R(1,0)*xr+R(1,1)*yr+R(1,2)*zr)-yl*(R(0,0)*xr+R(0,1)*yr+R(0,2)*zr);

		ROmat.L(i,0) = -((by*zl-bz*yl)*(R(0,0)*xr+R(0,1)*yr+R(0,2)*zr)+(bz*xl-zl)*(R(1,0)*xr+R(1,1)*yr+R(1,2)*zr)+(yl-by*xl)*(R(2,0)*xr+R(2,1)*yr+R(2,2)*zr));

		ROmat.B(i,i*2) = bz*(R(1,0)*xr+R(1,1)*yr+R(1,2)*zr)-by*(R(2,0)*xr+R(2,1)*yr+R(2,2)*zr);
		ROmat.B(i,i*2+1) = (R(2,0)*xr+R(2,1)*yr+R(2,2)*zr)-bz*(R(0,0)*xr+R(0,1)*yr+R(0,2)*zr);

		ROmat.B(i,2*num_ROPoint+i*2) = (by*zl-bz*yl)*R(0,0)+(bz*xl-zl)*R(1,0)+(yl-by*xl)*R(2,0);
		ROmat.B(i,2*num_ROPoint+i*2+1) = (by*zl-bz*yl)*R(0,1)+(bz*xl-zl)*R(1,1)+(yl-by*xl)*R(2,1);

	}
	
}

void CModelCoplanarity::makeA_L_Bindependent()
{
	int i;
	double xlo, ylo, xro, yro;//PPA
	double fl,fr;//focal length
	ROmat.A.resize(num_ROPoint,5,0.0);
	ROmat.L.resize(num_ROPoint,1,0.0);
	ROmat.B.resize(num_ROPoint,4*num_ROPoint,0.0);
		
	
	xlo = LPhoto.camera.PPA(0); ylo = LPhoto.camera.PPA(1);
	xro = RPhoto.camera.PPA(0); yro = RPhoto.camera.PPA(1);

	fl = LPhoto.camera.f; fr =RPhoto.camera.f;
	
	double Lomega = Param_inde.Lomega, Lphi = Param_inde.Lphi, Lkappa = Param_inde.Lkappa;
	double Romega = Param_inde.Romega, Rphi = Param_inde.Rphi, Rkappa = Param_inde.Rkappa;
	data::RotationMatrix LRT = math::rotation::getMMat(Lomega, Lphi, Lkappa); //Left rotation matrix(Ground->Photo)
	data::RotationMatrix RRT = math::rotation::getMMat(Romega, Rphi, Rkappa); //Right rotation matrix(Ground->Photo)
	math::Matrix<double> LR,dLRdP,dLRdK;//Left rotation matrix Photo->Ground) and partial derivatives w.r.t phi and kappa
	math::Matrix<double> RR,dRRdO,dRRdP,dRRdK;//Right rotation matrix (Photo->Ground)and partial derivatives w.r.t. omega, phi, and kappa
	LR = LRT.transpose();
	RR = RRT.transpose();
	
	dLRdP = makedRdP(Lomega,Lphi,Lkappa);
	dLRdK = makedRdK(Lomega,Lphi,Lkappa);

	dRRdO = makedRdO(RR);
	dRRdP = makedRdP(Romega,Rphi,Rkappa);
	dRRdK = makedRdK(Romega,Rphi,Rkappa);

	for(i=0;i<num_ROPoint;i++)
	{
		double xl = LPhoto.ROPoint[i](0)-xlo, yl = LPhoto.ROPoint[i](1)-ylo, zl = -fl;
		double xr = RPhoto.ROPoint[i](0)-xro, yr = RPhoto.ROPoint[i](1)-yro, zr = -fr;
		
		double yl_,zl_,yr_,zr_;
		yl_ = LR(1,0)*xl + LR(1,1)*yl + LR(1,2)*zl;
		zl_ = LR(2,0)*xl + LR(2,1)*yl + LR(2,2)*zl;
		yr_ = RR(1,0)*xr + RR(1,1)*yr + RR(1,2)*zr;
		zr_ = RR(2,0)*xr + RR(2,1)*yr + RR(2,2)*zr;
		
		//Left phi & kappa
		ROmat.A(i,0) = (dLRdP(1,0)*xl+dLRdP(1,1)*yl+dLRdP(1,2)*zl)*zr_ - (dLRdP(2,0)*xl+dLRdP(2,1)*yl+dLRdP(2,2)*zl)*yr_;
		ROmat.A(i,1) = (dLRdK(1,0)*xl+dLRdK(1,1)*yl)*zr_ - (dLRdK(2,0)*xl+dLRdK(2,1)*yl)*yr_;
		//Right omega & phi & kappa
		ROmat.A(i,2) = (dRRdO(2,0)*xr + dRRdO(2,1)*yr + dRRdO(2,2)*zr)*yl_ - (dRRdO(1,0)*xr + dRRdO(1,1)*yr + dRRdO(1,2)*zr)*zl_;
		ROmat.A(i,3) = (dRRdP(2,0)*xr + dRRdP(2,1)*yr + dRRdP(2,2)*zr)*yl_ - (dRRdP(1,0)*xr + dRRdP(1,1)*yr + dRRdP(1,2)*zr)*zl_;
		ROmat.A(i,4) = (dRRdK(2,0)*xr + dRRdK(2,1)*yr)*yl_ - (dRRdK(1,0)*xr + dRRdK(1,1)*yr)*zl_;

		
		ROmat.L(i,0) = -((yl_*zr_) - (yr_*zl_));

		ROmat.B(i,i*2) = LR(1,0)*zr_ - LR(2,0)*yr_;
		ROmat.B(i,i*2+1) = LR(1,1)*zr_ - LR(2,1)*yr_;

		ROmat.B(i,2*num_ROPoint+i*2) = RR(2,0)*yl_ - RR(1,0)*zl_;
		ROmat.B(i,2*num_ROPoint+i*2+1) = RR(2,1)*yl_ - RR(1,1)*zl_;

	}
	
}

math::Matrix<double> CModelCoplanarity::makedRdO(math::Matrix<double> R)
{
	math::Matrix<double> M(3,3,0.0);
	M(0,0) = 0.0;		M(0,1) = 0.0;		M(0,2) = 0.0;
	M(1,0) = -R(2,0);	M(1,1) = -R(2,1);	M(1,2) = -R(2,2);
	M(2,0) = R(1,0);	M(2,1) = R(1,1);	M(2,2) = R(1,2);
	return M;
}
math::Matrix<double> CModelCoplanarity::makedRdP(double o,double p,double k)
{
	math::Matrix<double> M(3,3,0.0);
	M(0,0) = -cos(k)*sin(p);		M(0,1) = sin(p)*sin(k);			M(0,2) = cos(p);
	M(1,0) = cos(k)*cos(p)*sin(o);	M(1,1) = -sin(k)*cos(p)*sin(o);	M(1,2) = sin(p)*sin(o);
	M(2,0) = -cos(k)*cos(o)*cos(p);	M(2,1) = cos(o)*cos(p)*sin(k);	M(2,2) = -sin(p)*cos(o);
	return M;
}
math::Matrix<double> CModelCoplanarity::makedRdK(double o,double p,double k)
{
	math::Matrix<double> M(3,3,0.0);
	M(0,0) = -sin(k)*cos(p);						M(0,1) = -cos(p)*cos(k);						M(0,2) = 0.0;
	M(1,0) = cos(o)*cos(k)-sin(k)*sin(p)*sin(o);	M(1,1) = -sin(k)*cos(o)-cos(k)*sin(p)*sin(o);	M(1,2) = 0.0;
	M(2,0) = cos(k)*sin(o)+sin(k)*cos(o)*sin(p);	M(2,1) = -sin(o)*sin(k)+cos(o)*sin(p)*cos(k);	M(2,2) = 0.0;
	return M;
}
