/*
* Copyright(c) 2019-2021 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
#include <ssm/include/Texture.h>

#include <fstream>

#include <ssm/include/SSMCollinearity.h>

/// Build texture information
namespace texture
{
	const double deg2rad = acos(-1.0) / 180.0;
	const double rad2deg = 180.0 / acos(-1.0);
	const double doubleMax = 1.0e99;// std::numeric_limits<double>::max();


	Texture::Texture()
	{
		this->maxRes = 0.01;
	}

	bool Texture::buildTexture(const std::vector<tobj::Face>& faces,
		const std::vector<tobj::Normal>& normals,
		const std::vector<std::string>& imgNames,
		const std::vector<std::shared_ptr<data::EOP>>& eops,
		const std::vector<std::shared_ptr<sensor::Camera>>& cameras,
		const double maxRes)
	{
		if (faces.size() != normals.size())
			return false;

		if (eops.size() != imgNames.size())
			return false;

		if (eops.size() != cameras.size())
			return false;

		this->eops = eops;
		this->cameras = cameras;
		this->faces = faces;
		this->normals = normals;
		this->imgNames = imgNames;
		this->eops = eops;
		this->textureInfo.resize(faces.size());
		this->maxRes = maxRes;

		/// Body-frame rotation matrices and perspective centers
		rotBodyM.reserve(eops.size());
		pcs.reserve(eops.size());

		for (const auto& eop : eops)
		{
			data::EulerAngles opk = eop->getOri();
			data::RotationMatrix m = math::rotation::getMMat(opk(0), opk(1), opk(2));
			rotBodyM.push_back(m);

			auto pc = eop->getPC();
			pcs.push_back(pc);
		}

		/// Bore-sight rotation matrices
		rotBsM.reserve(cameras.size());

		for (const auto& cam : cameras)
		{
			data::EulerAngles bs = cam->getBoresight();
			data::RotationMatrix m = math::rotation::getMMat(bs(0), bs(1), bs(2));
			rotBsM.push_back(m);
		}

		buildTexture();

		return true;
	}

	const std::vector<TextureInfo>& Texture::getTexture()
	{
		return textureInfo;
	}

	void Texture::buildTexture()
	{
		auto nImages = imgNames.size();

		for (size_t f = 0; f < this->faces.size(); ++f)
		{
			std::vector<data::Point3D> objPts(3);
			for (size_t i = 0; i < 3; ++i)
			{
				objPts[i](0) = faces[f][i][0];
				objPts[i](1) = faces[f][i][1];
				objPts[i](2) = faces[f][i][2];

				this->textureInfo[f].faceIdx = f;

				if (findImages(objPts, this->normals[f], this->maxRes, this->textureInfo[f]))
				{
					std::cout << f << "th face found a proper texture" << std::endl;
				}
				else
				{
					std::cout << f << "th face could not find a proper texture" << std::endl;
				}
			}
		}
	}

	bool Texture::findImages(const std::vector<data::Point3D>& face, const tobj::Normal& normal, const double maxRes, TextureInfo& textureInfo)
	{
		if (face.size() != 3)
			throw std::string("Error: the number of vertices in a face is not 3.");

		data::Point3D faceCenter;
		faceCenter(0) = faceCenter(1) = faceCenter(2) = 0.0;

		for (const auto& pt : face)
		{
			faceCenter(0) += pt[0];
			faceCenter(1) += pt[1];
			faceCenter(2) += pt[2];
		}

		faceCenter(0) /= 3;
		faceCenter(1) /= 3;
		faceCenter(2) /= 3;

		auto nImages = imgNames.size();

		std::vector<size_t> imgIndices;
		imgIndices.reserve(nImages);

		/// Computed image coordinates (c, r) in each image for the given object face vertices
		std::vector<VerticesCR> imgPtsData(nImages);

		std::vector<double> dists(nImages);
		for (auto& dist : dists)
			dist = doubleMax;
		std::vector<double> angs(nImages);
		for (auto& ang : angs)
			ang = 90.0 * deg2rad;

		std::vector<double> resolutions(nImages);
		for (auto& resolution : resolutions)
			resolution = doubleMax;

		for (size_t imgIdx = 0; imgIdx < nImages; ++imgIdx)
		{
			std::vector<data::Point2D> phoPts = ssm::collinearityEqWithAlignment(cameras[imgIdx]->getFL(), rotBsM[imgIdx], cameras[imgIdx]->getLeverArm(), pcs[imgIdx], rotBodyM[imgIdx], face);
			std::vector<data::Point2D> imgPts = cameras[imgIdx]->getDistortedImgCoordFromRefinedPhotoCoord(phoPts);

			if (imgPts.size() != 3)
				continue;

			auto imgSize = cameras[imgIdx]->getSensorSize();
			double w(imgSize(0));
			double h(imgSize(1));
			double f = this->cameras[imgIdx]->getFL();/// mm focal length
			double pitch = this->cameras[imgIdx]->getPixPitch();/// mm pixel pitch
			double dTheta = pitch / f * 0.5;

			bool targetImg = true;

			for (size_t ptIdx = 0; ptIdx < 3; ++ptIdx)
			{
				if (0 > imgPts[ptIdx](0) || w <= imgPts[ptIdx](0) ||
					0 > imgPts[ptIdx](1) || h <= imgPts[ptIdx](1))
				{
					targetImg = false;
					break;
				}
			}

			if (targetImg)
			{
				imgIndices.push_back(imgIdx);

				/// Store the computed image coordinates
				for (size_t ptIdx = 0; ptIdx < 3; ++ptIdx)
					imgPtsData[imgIdx][ptIdx] = imgPts[ptIdx];

				data::Point3D face2pc;
				face2pc(0) = pcs[imgIdx](0) - faceCenter(0);
				face2pc(1) = pcs[imgIdx](1) - faceCenter(1);
				face2pc(2) = pcs[imgIdx](2) - faceCenter(2);

				/// Distance between pc and face center
				dists[imgIdx] = sqrt(
					face2pc(0) * face2pc(0) +
					face2pc(1) * face2pc(1) +
					face2pc(2) * face2pc(2));

				double size1 = sqrt(face2pc(0) * face2pc(0) + face2pc(1) * face2pc(1) + face2pc(2) * face2pc(2));
				double size2 = sqrt(normal[0] * normal[0] + normal[1] * normal[1] + normal[2] * normal[2]);

				double crossProduct
					= face2pc(0) * normal[1] - normal[0] * face2pc(1)
					+ face2pc(1) * normal[2] - normal[1] * face2pc(2)
					+ face2pc(2) * normal[0] - normal[2] * face2pc(0);

				if (size1 < std::numeric_limits<double>::epsilon() || size2 < std::numeric_limits<double>::epsilon())
					throw std::string("Error: a vector size is zero.");

				/// Angle between optical axis and face normal (two vectors)
				/// angle = asin(cross-product / |a||b|), and (+) is CCW and (-) is CW
				angs[imgIdx] = asin(crossProduct / size1 / size2);

				if (angs[imgIdx] * rad2deg > 30.0 * deg2rad)
					continue;

				resolutions[imgIdx] = computeRes(dists[imgIdx], angs[imgIdx], dTheta);
			}
		}

		/// Choose the best one based on the angle and distance (resolution)
		double minRes = doubleMax;

		size_t chosenIdx = resolutions.size();
		for (size_t i = 0; i < resolutions.size(); ++i)
		{
			if (minRes > fabs(resolutions[i]))
				chosenIdx = i;
		}

		if (chosenIdx < resolutions.size() || minRes > maxRes)
		{
			/// Image path
			textureInfo.imgName = imgNames[chosenIdx];
			/// Vertices (c,r)
			textureInfo.verticesCR = imgPtsData[chosenIdx];

			auto imgSize = cameras[chosenIdx]->getSensorSize();
			double w(imgSize(0));
			double h(imgSize(1));

			/// Vertices (u, v)
			for (size_t ptIdx = 0; ptIdx < 3; ++ptIdx)
			{
				textureInfo.verticesUV[ptIdx][0] = textureInfo.verticesCR[ptIdx][0] / w;
				textureInfo.verticesUV[ptIdx][1] = 1. - textureInfo.verticesCR[ptIdx][1] / h;
			}

			textureInfo.availability = true;

			return true;
		}
		else
		{
			textureInfo.availability = false;

			return false;
		}
	}

	double Texture::computeRes(const double dist0, const double incidenceAngle, const double dTheta)
	{
		/// Perpendicular distance H = dist0 * cos(a)
		/// dist1 = H*tan(a + d) = H*(tan(a) - tan(d))/(1 + tan(a)*tan(d))
		/// dist2 = H*tan(a - d) = H*(tan(a) + tan(d))/(1 - tan(a)*tan(d))
		/// resolution for the small angle 2d, 
		/// res = dist1 - dist2
		/// = 2*H*(tan(a)^2*tan(d) + tan(d)) / (1 + tan(a)*tan(d)) / (1 - tan(a)*tan(d))
		/// if d is small, res is approximately 2*(dist0*cos(a))*(d*tan(a)^2 + d) / (1 - d*d*tan(a)^2)
		double H = dist0 * cos(incidenceAngle);
		double tana2 = tan(incidenceAngle) * tan(incidenceAngle);
		double temp = (1 - dTheta * dTheta * tana2);

		if (fabs(temp) < std::numeric_limits<double>::epsilon())
			return doubleMax;
		else
			return double(2.0 * H * (dTheta * tana2 + dTheta) / temp);
	}

	//std::shared_ptr<TextureInterface> createBasicTexture()
	//{
	//	std::shared_ptr<TextureInterface> textureInterface(new BasicTexture());
	//	return textureInterface;
	//}
}

/// Export texture info
namespace texture
{
	struct TextureOutputContents
	{
		std::string imgName;
		std::string materialBegin;
		std::string material;
		std::string materialEnd;
		std::string facegroupBegin;
		std::string facegroup;
		std::string facegroupEnd;
	};

	std::string getMtrlName(const std::string& imgName)
	{
		return std::string("mtrl_") + imgName;
	}

	std::string getfgrpName(const std::string& imgName)
	{
		return imgName;
	}

	TextureOutputContents createInitTextureOutputContents(const std::string& imgName)
	{
		TextureOutputContents tex;

		tex.imgName = imgName;

		tex.materialBegin = std::string("[material] # material begin");
		tex.material = std::string("name ") + getMtrlName(imgName) + std::string(" # material name");
		tex.material += std::string("map ") + imgName + std::string(" # image file name");
		tex.materialEnd = "[/material] # material end";

		tex.facegroupBegin = "[facegroup] # facegroup begin";
		tex.facegroup = std::string("name ") + getfgrpName(imgName) + std::string(" # facegroup name") + std::string("\n");
		tex.facegroup += std::string("mtl ") + getMtrlName(imgName) + std::string(" # corresponding material name") + std::string("\n");
		tex.facegroup += std::string("# f3 face_index u1/v1 u2/v2 u3/v3") + std::string("\n");
		tex.facegroupEnd = "[/facegroup] # facegroup end";

		return tex;
	}

	size_t findImgName(const std::string& imgName, std::vector<TextureOutputContents>& texOutputs)
	{
		for (size_t t = 0; t < texOutputs.size(); ++t)
		{
			if (imgName == texOutputs[t].imgName)
				return t;
		}

		TextureOutputContents temp = createInitTextureOutputContents(imgName);
		texOutputs.push_back(temp);

		return  (texOutputs.size() - 1);
	}

	/// Create a texture info file
	bool exportTextureInfo(const std::wstring& texInfoPath, const std::vector<TextureInfo>& textureInfo)
	{
		std::vector<TextureOutputContents> texOutputs;

		for (size_t t = 0; t < textureInfo.size(); ++t)
		{
			if (!textureInfo[t].availability)
				continue;

			auto idx = findImgName(textureInfo[t].imgName, texOutputs);

			texOutputs[idx].facegroup += std::string("f ");
			texOutputs[idx].facegroup += std::to_string(textureInfo[t].faceIdx) + std::string(" ");
			texOutputs[idx].facegroup += std::to_string(textureInfo[t].verticesUV[0][0]) + std::string("/") + std::to_string(textureInfo[t].verticesUV[0][1]) + std::string(" ");
			texOutputs[idx].facegroup += std::to_string(textureInfo[t].verticesUV[1][0]) + std::string("/") + std::to_string(textureInfo[t].verticesUV[1][1]) + std::string(" ");
			texOutputs[idx].facegroup += std::to_string(textureInfo[t].verticesUV[2][0]) + std::string("/") + std::to_string(textureInfo[t].verticesUV[2][1]) + std::string("\n");
		}

		std::fstream tex;
		tex.open(texInfoPath, std::ios::out);

		if (!tex)
			return false;

		for (const auto& f : texOutputs)
		{
			tex << f.materialBegin << std::endl;
			tex << f.material << std::endl;
			tex << f.materialEnd << std::endl;
			tex << f.facegroupBegin << std::endl;
			tex << f.facegroup << std::endl;
			tex << f.facegroupEnd << std::endl;
		}

		tex.close();

		return true;
	}
}