#include <math.h>
#include <iostream>

#include <ssm/include/ReferenceEllipsoid.h>

namespace math
{
	namespace geo
	{
		ReferenceEllipsoid::ReferenceEllipsoid()
			:eliParam(wgs84)
		{
			initUsefulParams();
		}

		ReferenceEllipsoid::ReferenceEllipsoid(const REP& eliInfo)
			: eliParam(eliInfo)
		{
			initUsefulParams();
		}

		void ReferenceEllipsoid::initUsefulParams()
		{
			const double a = this->eliParam.semiMajor;
			const double b = this->eliParam.semiMinor;

			if (a < std::numeric_limits<double>::epsilon() || b < std::numeric_limits<double>::epsilon())
			{
				std::cout << "Error: semimajor or semiminor is less than the numerical limit of double." << std::endl;
				throw std::runtime_error("Error: semimajor or semiminor is less than the numerical limit of double.");
			}

			double aa = a * a;
			double bb = b * b;
			double aa_bb = aa - bb;
			double sqrt_aa_bb = sqrt(aa_bb);

			this->eccentricity = sqrt_aa_bb / a;
			this->eccentricity2 = sqrt_aa_bb / b;
			this->flattening = (a - b) / a;

			this->n.ns = (a - b) / (a + b);
			this->n.ns2 = this->n.ns * this->n.ns;
			this->n.ns3 = this->n.ns * this->n.ns2;
			this->n.ns4 = this->n.ns2 * this->n.ns2;
			this->n.ns5 = this->n.ns * this->n.ns4;
		}

		bool ReferenceEllipsoid::operator == (const ReferenceEllipsoid& compare) const
		{
			if (this->eliParam.name == compare.eliParam.name
				&& this->eliParam.semiMajor == compare.eliParam.semiMajor
				&& this->eliParam.semiMinor == compare.eliParam.semiMinor)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}