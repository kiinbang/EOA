/*
* Copyright(c) 1999-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <SSM/include/SSMRotation.h>

namespace math
{
	namespace rotation
	{
		/**2D Rotation matrix*/
		math::Matrix<double> getRMat(const double angle)
		{
			//R-rotation-matrix

			const double coskappa = cos(angle);
			const double sinkappa = sin(angle);

			math::Matrix<double> r_matrix(2, 2);

			r_matrix(0, 0) = coskappa;
			r_matrix(0, 1) = -sinkappa;

			r_matrix(1, 0) = sinkappa;
			r_matrix(1, 1) = coskappa;

			return r_matrix;
		}

		/**Rotation matrix for a specific axis*/
		data::RotationMatrix getRMat(const AxisName axisName, const double angle)
		{
			int axis;

			switch (axisName)
			{
			case AxisName::_X_:
				axis = 0;
				break;
			case AxisName::_Y_:
				axis = 1;
				break;
			case AxisName::_Z_:
				axis = 2;
				break;
			default:
				throw std::runtime_error("Wrong axis name");
			}
			//R-rotation-matrix
			data::RotationMatrix rmat;

			const double cos_ = cos(angle);
			const double sin_ = sin(angle);

			unsigned int j1 = (axis + 1) % 3;
			unsigned int j2 = (axis + 2) % 3;

			rmat(axis, axis) = 1.0;
			rmat(axis, j1) = 0.0;
			rmat(axis, j2) = 0.0;

			rmat(j1, axis) = 0.0;
			rmat(j1, j1) = cos_;
			rmat(j1, j2) = -sin_;


			rmat(j2, axis) = 0.0;
			rmat(j2, j1) = sin_;
			rmat(j2, j2) = cos_;

			return rmat;
		}

		/**Rotation matrix for three Euler angles*/
		data::RotationMatrix getRMat(const double omega, const double phi, const double kappa)
		{
			/// R-rotation-matrix
			/// R(o)R(p)R(k)

			const double cosomega = cos(omega);
			const double sinomega = sin(omega);
			const double cosphi = cos(phi);
			const double sinphi = sin(phi);
			const double coskappa = cos(kappa);
			const double sinkappa = sin(kappa);

			data::RotationMatrix r_matrix;

			r_matrix(0, 0) = cosphi * coskappa;
			r_matrix(0, 1) = -cosphi * sinkappa;
			r_matrix(0, 2) = sinphi;

			r_matrix(1, 0) = sinomega * sinphi * coskappa + cosomega * sinkappa;
			r_matrix(1, 1) = -sinomega * sinphi * sinkappa + cosomega * coskappa;
			r_matrix(1, 2) = -sinomega * cosphi;

			r_matrix(2, 0) = -cosomega * sinphi * coskappa + sinomega * sinkappa;
			r_matrix(2, 1) = cosomega * sinphi * sinkappa + sinomega * coskappa;
			r_matrix(2, 2) = cosomega * cosphi;

			return r_matrix;
		}

		/**Partial derivative of omega for R-matrix*/
		data::RotationMatrix dRdO(const double omega, const double phi, const double kappa)
		{
			data::RotationMatrix drdo;

			drdo(0, 0) = 0;
			drdo(0, 1) = 0;
			drdo(0, 2) = 0;

			drdo(1, 0) = cos(omega) * sin(phi) * cos(kappa) - sin(omega) * sin(kappa);
			drdo(1, 1) = -cos(omega) * sin(phi) * sin(kappa) - sin(omega) * cos(kappa);
			drdo(1, 2) = -cos(omega) * cos(phi);

			drdo(2, 0) = sin(omega) * sin(phi) * cos(kappa) + cos(omega) * sin(kappa);
			drdo(2, 1) = -sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
			drdo(2, 2) = -sin(omega) * cos(phi);

			return drdo;
		}

		/**Partial derivative of phi for R-matrix*/
		data::RotationMatrix dRdP(const double omega, const double phi, const double kappa)
		{
			data::RotationMatrix drdp;

			drdp(0, 0) = -sin(phi) * cos(kappa);
			drdp(0, 1) = sin(phi) * sin(kappa);
			drdp(0, 2) = cos(phi);

			drdp(1, 0) = sin(omega) * cos(phi) * cos(kappa);
			drdp(1, 1) = -sin(omega) * cos(phi) * sin(kappa);
			drdp(1, 2) = sin(omega) * sin(phi);

			drdp(2, 0) = -cos(omega) * cos(phi) * cos(kappa);
			drdp(2, 1) = cos(omega) * cos(phi) * sin(kappa);
			drdp(2, 2) = -cos(omega) * sin(phi);

			return drdp;
		}

		/**Partial derivative of kappa for R-matrix*/
		data::RotationMatrix dRdK(const double omega, const double phi, const double kappa)
		{
			data::RotationMatrix drdk;

			drdk(0, 0) = -cos(phi) * sin(kappa);
			drdk(0, 1) = -cos(phi) * cos(kappa);
			drdk(0, 2) = 0.;

			drdk(1, 0) = -sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
			drdk(1, 1) = -sin(omega) * sin(phi) * cos(kappa) - cos(omega) * sin(kappa);
			drdk(1, 2) = 0.;

			drdk(2, 0) = cos(omega) * sin(phi) * sin(kappa) + sin(omega) * cos(kappa);
			drdk(2, 1) = cos(omega) * sin(phi) * cos(kappa) - sin(omega) * sin(kappa);
			drdk(2, 2) = 0.;

			return drdk;
		}

		/**Rotation matrix for three Euler angles*/
		data::RotationMatrix getMMat(const double omega, const double phi, const double kappa)
		{
			/// M-rotation-matrix
			/// M(k)M(p)M(o)

			const double cosomega = cos(omega);
			const double sinomega = sin(omega);
			const double cosphi = cos(phi);
			const double sinphi = sin(phi);
			const double coskappa = cos(kappa);
			const double sinkappa = sin(kappa);

			data::RotationMatrix m_matrix;

			m_matrix(0, 0) = cosphi * coskappa;
			m_matrix(1, 0) = -cosphi * sinkappa;
			m_matrix(2, 0) = sinphi;

			m_matrix(0, 1) = sinomega * sinphi * coskappa + cosomega * sinkappa;
			m_matrix(1, 1) = -sinomega * sinphi * sinkappa + cosomega * coskappa;
			m_matrix(2, 1) = -sinomega * cosphi;

			m_matrix(0, 2) = -cosomega * sinphi * coskappa + sinomega * sinkappa;
			m_matrix(1, 2) = cosomega * sinphi * sinkappa + sinomega * coskappa;
			m_matrix(2, 2) = cosomega * cosphi;

			return m_matrix;
		}

		/**Partial derivative of omega for M-matrix*/
		data::RotationMatrix dMdO(const double omega, const double phi, const double kappa)
		{
			data::RotationMatrix dmdo;

			dmdo(0, 0) = 0;
			dmdo(1, 0) = 0;
			dmdo(2, 0) = 0;

			dmdo(0, 1) = cos(omega) * sin(phi) * cos(kappa) - sin(omega) * sin(kappa);
			dmdo(1, 1) = -cos(omega) * sin(phi) * sin(kappa) - sin(omega) * cos(kappa);
			dmdo(2, 1) = -cos(omega) * cos(phi);

			dmdo(0, 2) = sin(omega) * sin(phi) * cos(kappa) + cos(omega) * sin(kappa);
			dmdo(1, 2) = -sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
			dmdo(2, 2) = -sin(omega) * cos(phi);

			return dmdo;
		}

		/**Partial derivative of phi for M-matrix*/
		data::RotationMatrix dMdP(const double omega, const double phi, const double kappa)
		{
			data::RotationMatrix dmdp;

			dmdp(0, 0) = -sin(phi) * cos(kappa);
			dmdp(1, 0) = sin(phi) * sin(kappa);
			dmdp(2, 0) = cos(phi);

			dmdp(0, 1) = sin(omega) * cos(phi) * cos(kappa);
			dmdp(1, 1) = -sin(omega) * cos(phi) * sin(kappa);
			dmdp(2, 1) = sin(omega) * sin(phi);

			dmdp(0, 2) = -cos(omega) * cos(phi) * cos(kappa);
			dmdp(1, 2) = cos(omega) * cos(phi) * sin(kappa);
			dmdp(2, 2) = -cos(omega) * sin(phi);

			return dmdp;
		}

		/**Partial derivative of kap for M-matrix*/
		data::RotationMatrix dMdK(const double omega, const double phi, const double kappa)
		{
			data::RotationMatrix dmdk;

			dmdk(0, 0) = -cos(phi) * sin(kappa);
			dmdk(1, 0) = -cos(phi) * cos(kappa);
			dmdk(2, 0) = 0.;

			dmdk(0, 1) = -sin(omega) * sin(phi) * sin(kappa) + cos(omega) * cos(kappa);
			dmdk(1, 1) = -sin(omega) * sin(phi) * cos(kappa) - cos(omega) * sin(kappa);
			dmdk(2, 1) = 0.;

			dmdk(0, 2) = cos(omega) * sin(phi) * sin(kappa) + sin(omega) * cos(kappa);
			dmdk(1, 2) = cos(omega) * sin(phi) * cos(kappa) - sin(omega) * sin(kappa);
			dmdk(2, 2) = 0.;

			return dmdk;
		}

		/**Extract Euler angles from R-rotation matrix*/
		void ExtractEulerFromR(const data::RotationMatrix& Rmatrix, double& omega, double& phi, double& kappa)
		{
			ExtractEulerFromM(Rmatrix.transpose(), omega, phi, kappa);
		}

		/**Extract Euler angles from M-rotation matrix*/
		void ExtractEulerFromM(const data::RotationMatrix& Mmatrix, double& omega, double& phi, double& kappa)
		{
			double omega_1, phi_1, kappa_1;
			double omega_2, phi_2, kappa_2;

			phi = asin(Mmatrix(2, 0));

			// two possible values for phi
			phi_1 = phi;
			if (phi < 0) phi_2 = -constant::_PI_ - phi;
			else phi_2 = constant::_PI_ - phi;

			omega_1 = atan2(-Mmatrix(2, 1) / cos(phi_1), Mmatrix(2, 2) / cos(phi_1));
			kappa_1 = atan2(-Mmatrix(1, 0) / cos(phi_1), Mmatrix(0, 0) / cos(phi_1));

			omega_2 = atan2(-Mmatrix(2, 1) / cos(phi_2), Mmatrix(2, 2) / cos(phi_2));
			kappa_2 = atan2(-Mmatrix(1, 0) / cos(phi_2), Mmatrix(0, 0) / cos(phi_2));

			data::RotationMatrix temp_1 = getMMat(omega_1, phi_1, kappa_1);
			data::RotationMatrix temp_2 = getMMat(omega_2, phi_2, kappa_2);

			double sum_1 = 0, sum_2 = 0;
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					sum_1 += fabs(Mmatrix(i, j) - temp_1(i, j));
					sum_2 += fabs(Mmatrix(i, j) - temp_2(i, j));
				}
			}

			if (sum_1 < sum_2)
			{
				phi = phi_1;
				omega = omega_1;
				kappa = kappa_1;
			}
			else
			{
				phi = phi_2;
				omega = omega_2;
				kappa = kappa_2;
			}
		}

		/**Arbitrary axis rotation*/
		// https://www.eng.uc.edu/~beaucag/Classes/Properties/OptionalProjects/CoordinateTransformationCode/Rotate%20about%20an%20arbitrary%20axis%20(3%20dimensions).html
		math::Matrix<double> rotateArbitraryAxis(const double x, const double y, const double z,
			const double rx, const double ry, const double rz,
			const double theta)
		{
			data::RotationMatrix R;
			double cost = cos(theta);
			double sint = sin(theta);

			math::Matrix<double> axis(3, 1);
			axis(0, 0) = rx;
			axis(1, 0) = ry;
			axis(2, 0) = rz;
			auto size = sqrt(axis.getSumOfSquares());
			axis /= size;

			math::Matrix<double> matP(3, 1), matQ;
			matP(0, 0) = x;
			matP(1, 0) = y;
			matP(2, 0) = z;

			if (fabs(axis(1, 0)) < std::numeric_limits<double>::epsilon() && fabs(axis(2, 0)) < std::numeric_limits<double>::epsilon())
			{
				///rotation about x axis
				if (axis(0, 0) > 0.0)
					R = getRMat(AxisName::_X_, theta);
				else
					R = getRMat(AxisName::_X_, -theta);
			}
			else if (fabs(axis(2, 0)) < std::numeric_limits<double>::epsilon() && fabs(axis(0, 0)) < std::numeric_limits<double>::epsilon())
			{
				///rotation about y axis
				if (axis(1, 0) > 0.0)
					R = getRMat(AxisName::_Y_, theta);
				else
					R = getRMat(AxisName::_Y_, -theta);
			}
			else if (fabs(axis(0, 0)) < std::numeric_limits<double>::epsilon() && fabs(axis(1, 0)) < std::numeric_limits<double>::epsilon())
			{
				///rotation about z axis
				if (axis(2, 0) > 0.0)
					R = getRMat(AxisName::_Z_, theta);
				else
					R = getRMat(AxisName::_Z_, -theta);
			}
			else
			{
				const auto& a = axis(0, 0);
				const auto& b = axis(1, 0);
				const auto& c = axis(2, 0);
				auto d = sqrt(b * b + c * c);

				math::Matrix<double> Rx(3, 3);
				Rx(0, 0) = 1.0;
				Rx(0, 1) = 0.0;
				Rx(0, 2) = 0.0;

				Rx(1, 0) = 0.0;
				Rx(1, 1) = c / d;
				Rx(1, 2) = -b / d;
				
				Rx(2, 0) = 0.0;
				Rx(2, 1) = b / d;
				Rx(2, 2) = c / d;

				math::Matrix<double> Ry(3, 3);
				Ry(0, 0) = d;
				Ry(0, 1) = 0.0;
				Ry(0, 2) = -a;

				Ry(1, 0) = 0;
				Ry(1, 1) = 1.0;
				Ry(1, 2) = 0;
				
				Ry(2, 0) = a;
				Ry(2, 1) = 0.0;
				Ry(2, 2) = d;

				math::Matrix<double> Rz(3, 3);
				Rz(0, 0) = cost;
				Rz(0, 1) = -sint;
				Rz(0, 2) = 0.0;

				Rz(1, 0) = sint;
				Rz(1, 1) = cost;
				Rz(1, 2) = 0;
				
				Rz(2, 0) = 0;
				Rz(2, 1) = 0.0;
				Rz(2, 2) = 1;

				R = Rx.transpose() % Ry.transpose() % Rz % Ry % Rx;
			}

			matQ = R % matP;

			return matQ;
		}
	}
}