/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <ssm/include/NumericPDE.h>

namespace math
{
	/**getNumericPDE
	*@brief get a, b and l matrices with partial derivatives
	*@param eq: given equation
	*@param givenParams: given parameters
	*@param givenObs: given observations
	*@param a design matrix
	*@param b coefficient matrix of residuals
	*@param l -F0
	*/
	void getNumericPDE(funcCollection eq, 
		const std::vector<std::shared_ptr<param::ParameterCollection>>& givenParams,
		const std::vector<std::shared_ptr<param::ParameterCollection>>& givenObs,
		Matrixd& a, 
		Matrixd& b, 
		Matrixd& l,
		const NPDETYPE npdeType)
	{
		/// Initial function result
		l = -eq(givenParams, givenObs);

		/// Initialize PD (design matrix)
		unsigned int numParams = 0;
		for (unsigned int c = 0; c < givenParams.size(); ++c)
			numParams += givenParams[c]->getAllNumParameters();

		const unsigned int numEqs = static_cast<unsigned int>(l.getRows());

		a.resize(numEqs, numParams);

		unsigned int paramIdx = 0;

		for (unsigned int c = 0; c < givenParams.size(); ++c)
		{
			for (unsigned int i = 0; i < givenParams[c]->size(); ++i)
			{
				param::ParameterSet& paramset = givenParams[c]->operator[](i);

				for (unsigned int j = 0; j < paramset.size(); ++j)
				{
					param::Parameter& param = paramset[j];

					switch(static_cast<int>(npdeType))
					{
					case static_cast<int>(NPDETYPE::FirstOrder):
					/// 1st order
					{
						param.set(param.get() + param.getDelta());
						auto out = eq(givenParams, givenObs);

						for (unsigned int k = 0; k < out.getRows(); ++k)
						{
							a(k, paramIdx) = (out(k, 0) - (-l(k, 0))) / param.getDelta();
						}

						param.set(param.get() - param.getDelta());
					}
						break;
					case static_cast<int>(NPDETYPE::FourthOrder):
					/// 4th order
					{
						param.set(param.get() + param.getDelta());
						auto out1 = eq(givenParams, givenObs);

						param.set(param.get() + param.getDelta());
						auto out2 = eq(givenParams, givenObs);

						param.set(param.get() - param.getDelta() * 3.0);
						auto out_1 = eq(givenParams, givenObs);

						param.set(param.get() - param.getDelta());
						auto out_2 = eq(givenParams, givenObs);

						for (unsigned int k = 0; k < out_2.getRows(); ++k)
						{
							a(k, paramIdx) = (8. * out1(k, 0) + out_2(k, 0) - out2(k, 0) - 8. * out_1(k, 0)) / (12.0 * param.getDelta());
						}

						param.set(param.get() + param.getDelta() * 2.0);
					}
						break;
					default:
						throw std::runtime_error("Error in NPDETYPE option");
					}

					++paramIdx;
				}
			}
		}

		unsigned int numObs = 0;
		for (unsigned int c = 0; c < givenObs.size(); ++c)
			numParams += givenObs[c]->getAllNumParameters();

		b.resize(numEqs, numObs);

		paramIdx = 0;

		for (unsigned int c = 0; c < givenObs.size(); ++c)
		{
			for (unsigned int i = 0; i < givenObs[c]->size(); ++i)
			{
				param::ParameterSet& obsSet = givenObs[c]->operator[](i);

				for (unsigned int j = 0; j < obsSet.size(); ++j)
				{
					param::Parameter& obs = obsSet[j];

					switch (static_cast<int>(npdeType))
					{
						case static_cast<int>(NPDETYPE::FirstOrder) :
						/// 1st order
						{
							obs.set(obs.get() + obs.getDelta());
							auto out = eq(givenParams, givenObs);

							for (unsigned int k = 0; k < out.getRows(); ++k)
							{
								b(k, paramIdx) = (out(k, 0) - (-l(k, 0))) / obs.getDelta();
							}

							obs.set(obs.get() - obs.getDelta());
						}
						break;
						case static_cast<int>(NPDETYPE::FourthOrder) :
						/// 4th order
						{
							obs.set(obs.get() + obs.getDelta());
							auto out1 = eq(givenParams, givenObs);

							obs.set(obs.get() + obs.getDelta());
							auto out2 = eq(givenParams, givenObs);

							obs.set(obs.get() - obs.getDelta() * 3.0);
							auto out_1 = eq(givenParams, givenObs);

							obs.set(obs.get() - obs.getDelta());
							auto out_2 = eq(givenParams, givenObs);

							for (unsigned int k = 0; k < out_2.getRows(); ++k)
							{
								b(k, paramIdx) = (8. * out1(k, 0) + out_2(k, 0) - out2(k, 0) - 8. * out_1(k, 0)) / (12.0 * obs.getDelta());
							}

							obs.set(obs.get() + obs.getDelta() * 2.0);
						}
						break;
						default:
							throw std::runtime_error("Error in NPDETYPE option");
					}

					++paramIdx;
				}
			}
		}
	}

	/**getNumericPDE
	*@brief get a, b and l matrices with partial derivatives
	*@param eq: given equation
	*@param givenParams: given parameters
	*@param givenObs: given observations
	*@param a design matrix
	*@param b coefficient matrix of residuals
	*@param l -F0
	*/
	void getNumericPDEWithParameters(funcParameter eq,
		const std::vector<std::shared_ptr<param::Parameter>>& givenParams,
		const std::vector<std::shared_ptr<param::Parameter>>& givenObs,
		Matrixd& a,
		Matrixd& b,
		Matrixd& l,
		const NPDETYPE npdeType)
	{
		/// Initial function result
		l = -eq(givenParams, givenObs);

		/// Initialize PD (design matrix)
		unsigned int numParams = static_cast<unsigned int>(givenParams.size());

		const unsigned int numEqs = static_cast<unsigned int>(l.getRows());

		a.resize(numEqs, numParams);

		for (unsigned int paramIdx = 0; paramIdx < givenParams.size(); ++paramIdx)
		{
			std::shared_ptr<param::Parameter> param = givenParams[paramIdx];

			switch (static_cast<int>(npdeType))
			{
				case static_cast<int>(NPDETYPE::FirstOrder) :
				/// 1st order
				{
					param->set(param->get() + param->getDelta());
					auto out = eq(givenParams, givenObs);

					for (unsigned int k = 0; k < out.getRows(); ++k)
					{
						a(k, paramIdx) = (out(k, 0) - (-l(k, 0))) / param->getDelta();
					}

					param->set(param->get() - param->getDelta());
				}
				break;
				case static_cast<int>(NPDETYPE::FourthOrder) :
					/// 4th order
				{
					param->set(param->get() + param->getDelta());
					auto out1 = eq(givenParams, givenObs);

					param->set(param->get() + param->getDelta());
					auto out2 = eq(givenParams, givenObs);

					param->set(param->get() - param->getDelta() * 3.0);
					auto out_1 = eq(givenParams, givenObs);

					param->set(param->get() - param->getDelta());
					auto out_2 = eq(givenParams, givenObs);

					for (unsigned int k = 0; k < out_2.getRows(); ++k)
					{
						a(k, paramIdx) = (8. * out1(k, 0) + out_2(k, 0) - out2(k, 0) - 8. * out_1(k, 0)) / (12.0 * param->getDelta());
					}

					param->set(param->get() + param->getDelta() * 2.0);
				}
				break;
				default:
					throw std::runtime_error("Error in NPDETYPE option");
			}
		}

		unsigned int numObs = static_cast<unsigned int>(givenObs.size());

		b.resize(numEqs, numObs);

		for (unsigned int paramIdx = 0; paramIdx < givenObs.size(); ++paramIdx)
		{
			std::shared_ptr<param::Parameter> obs = givenObs[paramIdx];

			switch (static_cast<int>(npdeType))
			{
				case static_cast<int>(NPDETYPE::FirstOrder) :
				/// 1st order
				{
					obs->set(obs->get() + obs->getDelta());
					auto out = eq(givenParams, givenObs);

					for (unsigned int k = 0; k < out.getRows(); ++k)
					{
						b(k, paramIdx) = (out(k, 0) - (-l(k, 0))) / obs->getDelta();
					}

					obs->set(obs->get() - obs->getDelta());
				}
				break;
				case static_cast<int>(NPDETYPE::FourthOrder) :
				/// 4th order
				{
					obs->set(obs->get() + obs->getDelta());
					auto out1 = eq(givenParams, givenObs);

					obs->set(obs->get() + obs->getDelta());
					auto out2 = eq(givenParams, givenObs);

					obs->set(obs->get() - obs->getDelta() * 3.0);
					auto out_1 = eq(givenParams, givenObs);

					obs->set(obs->get() - obs->getDelta());
					auto out_2 = eq(givenParams, givenObs);

					for (unsigned int k = 0; k < out_2.getRows(); ++k)
					{
						b(k, paramIdx) = (8. * out1(k, 0) + out_2(k, 0) - out2(k, 0) - 8. * out_1(k, 0)) / (12.0 * obs->getDelta());
					}

					obs->set(obs->get() + obs->getDelta() * 2.0);
				}
				break;
				default:
					throw std::runtime_error("Error in NPDETYPE option");
			}
		}
	}
}