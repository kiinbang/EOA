/*
* Copyright(c) 2005-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
#include <iostream>

#include <ssm/include/SSMLS.h>

namespace ssm
{
	/** Constructor */
	LeastSquare::LeastSquare()
	{
		std::vector<unsigned int> numParams;
		std::vector<unsigned int> numDatasets;
		config(numParams, numDatasets);
	}

	/** Constructor */
	LeastSquare::LeastSquare(const std::vector<unsigned int> numParams,/**<number of parameters for each section*/
		const std::vector<unsigned int> numDatasets /**<number of input datasets for each section*/)
	{
		config(numParams, numDatasets);
	}

	/** Configuration */
	void LeastSquare::config(const std::vector<unsigned int> numParams,/**<number of parameters for each section*/
		const std::vector<unsigned int> numDatasets /**<number of input datasets for each section*/)
	{
		if (numParams.size() != numDatasets.size())
			throw std::runtime_error("Error caused by numParams.size() != numDatasets.size()");

		numSections = static_cast<unsigned int>(numParams.size()); /// Number of sections
		numParameters = numParams; /// Number of parameters for each section
		numSets = numDatasets; /// Number of datset of each section

		if (numParameters.size() != numSets.size())
			throw std::runtime_error("Wrong number of parameters and datasets.");

		this->Error.clear();
		this->numEq = 0;
		this->sumEtpe = 0;

		///
		/// New approach using the matrices below
		///
		NdotList.resize(numSections);
		for (unsigned int i = 0; i < numSections; ++i)
		{
			NdotList[i].config(numParameters[i], numSets[i]);
		}

		NbarList.resize(numSections);
		for (unsigned int i = 0; i < numSections; ++i)
		{
			NbarList[i].resize(numSections);
			for (unsigned int j = i + 1/*consider only the upper triangle*/; j < numSections; ++j)
			{
				NbarList[i][j].config(numSets[i], numSets[j]);
			}
		}

		CdotList.resize(numSections);
		for (unsigned int i = 0; i < numSections; ++i)
		{
			CdotList[i].config(numParameters[i], numSets[i]);
		}
	}

	/**destructor*/
	LeastSquare::~LeastSquare()
	{
	}

	/**initialized the object*/
	void LeastSquare::init()
	{
		sumEtpe = 0.0;
		numEq = 0;
		numUnknown = 0;
	}

	/**simple LS without weight matrix*/
	RetMat LeastSquare::runLeastSquare(const math::Matrixd& A, const math::Matrixd& L)
	{
		unsigned int _size_ = A.getRows();
		math::Matrixd W(_size_, _size_);
		W.makeIdentityMat();
		return runLeastSquare(A, L, W);
	}

	/**simple and fast(simple result, only X matrix) LS*/
	math::Matrixd LeastSquare::runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L)
	{
		unsigned int _size_ = A.getRows();
		math::Matrixd W(_size_, _size_);
		W.makeIdentityMat();
		return runLeastSquare_Fast(A, L, W);
	}

	/**LS using weight matrix, not using reduced normal matrix for simple modeling*/
	RetMat LeastSquare::runLeastSquare(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W)
	{
		RetMat retval;
		retval.A = A;
		retval.L = L;
		retval.W = W;
		retval.AT = retval.A.transpose();
		retval.N = retval.AT%retval.W%retval.A;
		retval.Ninv = retval.N.inverse();
		retval.X = retval.Ninv%retval.AT%retval.W%retval.L;
		retval.V = retval.A%retval.X - retval.L;
		retval.VTV = retval.V.transpose() % retval.W%retval.V;
		retval.variance = retval.VTV(0, 0) / (retval.A.getRows() - retval.A.getCols());
		retval.sd = sqrt(retval.variance);
		retval.XVarCov = retval.Ninv*retval.variance;
		retval.LVarCov = (retval.A%retval.Ninv%retval.AT)*retval.variance;

		return retval;
	}

	/**LS using weight matrix, not using reduced normal matrix for simple modeling<br>
	*simple result, only X matrix, fast a little.
	*/
	math::Matrixd LeastSquare::runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W)
	{
		RetMat retval;
		retval.A = A;
		retval.L = L;
		retval.W = W;

		retval.AT = retval.A.transpose();
		retval.N = retval.AT%retval.W%retval.A;
		retval.Ninv = retval.N.inverse();
		retval.X = retval.Ninv%retval.AT%retval.W%retval.L;

		return retval.X;
	}

	math::Matrixd LeastSquare::runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W, math::Matrixd& N)
	{
		RetMat retval;
		retval.A = A;
		retval.L = L;
		retval.W = W;

		retval.AT = retval.A.transpose();
		retval.N = retval.AT%retval.W%retval.A;
		retval.Ninv = retval.N.inverse();
		retval.X = retval.Ninv%retval.AT%retval.W%retval.L;

		N = retval.N;

		return retval.X;
	}

	/**Get correlation matrix from variance-covariance matrix*/
	math::Matrixd LeastSquare::getCorrelation(const math::Matrixd& VarCov)
	{
		unsigned int i, j;
		//Correlation of Unknown Matrix
		math::Matrixd Correlation;
		Correlation.resize(VarCov.getRows(), VarCov.getCols(), 0.);

		for (i = 0; i<Correlation.getRows(); i++)
		{
			double sigma_i, sigma_j;
			sigma_i = sqrt(VarCov(i, i));

			for (j = i; j<Correlation.getCols(); j++)
			{
				sigma_j = sqrt(VarCov(j, j));
				Correlation(i, j) = VarCov(i, j) / sigma_i / sigma_j;
			}
		}
		return Correlation;
	}

	/**Extract variance(diagonal element) matrix from variance-covariance matrix*/
	math::Matrixd LeastSquare::extractVar(const math::Matrixd& VarCov)
	{
		unsigned int i;
		//Correlation of Unknown Matrix
		math::Matrixd Variance(VarCov.getRows(), 1, 0.);

		for (i = 0; i<Variance.getRows(); i++)
		{
			Variance(i, 0) = VarCov(i, i);
		}
		return Variance;
	}

	/**GetErrorList*/
	std::vector<double> LeastSquare::getErrorList() const
	{
		std::vector<double> retval;

		unsigned int num_mat = static_cast<int>(Error.size());

		//DATA<MatrixData> *pos = NULL;
		int count = 0;
		for (unsigned int i = 0; i<num_mat; i++)
		{
			count += Error[i].getRows();
		}

		retval.resize(count);

		int index = 0;
		for (unsigned int i = 0; i<num_mat; i++)
		{
			int nrows = Error[i].getRows();
			for (int j = 0; j<nrows; j++)
			{
				retval[index] = Error[i](j, 0);
				index++;
			}
		}

		return retval;
	}

	math::Matrixd LeastSquare::runLeastSquare(double& var, math::Matrixd& X, const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect, math::Matrixd& retN)
	{
		unsigned int sizeOfUpper = 0;
		
		for (unsigned int i = 0; i < (boundarySect+1); ++i)
		{
			sizeOfUpper += numParameters[i] * numSets[i];
		}

		unsigned int sizeOfLower = 0;

		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			sizeOfLower += numParameters[i] * numSets[i];
		}

		/// N_dot
		math::Matrixd N_dot = make_N_dot_New(sizeOfUpper, boundarySect);
		///C_dot(K_dot)
		math::Matrixd C_dot = make_C_dot_New(sizeOfUpper, boundarySect);
		///C_2dot(K_2dot)
		math::Matrixd C_2dot = make_C_2dot_New(sizeOfLower, boundarySect);
		/// N bar matrix
		math::Matrixd N_bar = make_N_bar(boundarySect);
		/// N_2dot
		math::Matrixd N_2dot = make_N_2dot_New(sizeOfLower, boundarySect);
		
		math::Matrixd N, C;
		N.insert(0, 0, N_dot);
		N.insert(N.getRows(), N.getCols(), N_2dot);
		C.insert(0, 0, C_dot);
		C.insert(C.getRows(), 0, C_2dot);
		N.insert(0, N_dot.getCols(), N_bar);
		N.insert(N_dot.getRows(), 0, N_bar.transpose());

		/// Fix parameters in N and C matrices
		this->fixParameters(N, C, fixedParamIdx);

		auto Ninv = N.inverse();
		X = Ninv % C;

		/// standard deviation of unit weight
		var = this->sumEtpe / (this->numEq - this->numUnknown);

		/// return normal matrix
		retN = N;

		return Ninv;
	}

	/**Get correlation matrix from variance-covariance matrix*/
	math::Matrixd LeastSquare::getNmat(const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect)
	{
		unsigned int sizeOfUpper = 0;

		for (unsigned int i = 0; i < (boundarySect + 1); ++i)
		{
			sizeOfUpper += numParameters[i] * numSets[i];
		}

		unsigned int sizeOfLower = 0;

		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			sizeOfLower += numParameters[i] * numSets[i];
		}

		/// N_dot
		math::Matrixd N_dot = make_N_dot_New(sizeOfUpper, boundarySect);
		/// N bar matrix
		math::Matrixd N_bar = make_N_bar(boundarySect);
		/// N_2dot
		math::Matrixd N_2dot = make_N_2dot_New(sizeOfLower, boundarySect);

		math::Matrixd N;
		N.insert(0, 0, N_dot);
		N.insert(N.getRows(), N.getCols(), N_2dot);
		N.insert(0, N_dot.getCols(), N_bar);
		N.insert(N_dot.getRows(), 0, N_bar.transpose());

		/// Fix parameters in N
		this->fixParameters(N, fixedParamIdx);

		return N;
	}

	/**Get correlation matrix from variance-covariance matrix*/
	std::vector<math::Matrixd> LeastSquare::getPartitionedNmat(const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect)
	{
		unsigned int sizeOfUpper = 0;

		for (unsigned int i = 0; i < (boundarySect + 1); ++i)
		{
			sizeOfUpper += numParameters[i] * numSets[i];
		}

		unsigned int sizeOfLower = 0;

		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			sizeOfLower += numParameters[i] * numSets[i];
		}

		std::vector<math::Matrixd> N(4);
		/// N_dot
		N[0] = make_N_dot_New(sizeOfUpper, boundarySect);
		/// N bar matrix
		N[1] = make_N_bar(boundarySect);
		/// N_2dot
		N[3] = make_N_2dot_New(sizeOfLower, boundarySect);

		/// Fix parameters in N
		fixParametersPartitioned(N[0], N[3], N[1], fixedParamIdx);

		/// N bar transpose
		N[2] = N[1].transpose();

		return N;
	}

	/**Reduced Normal Matrix*/
	void LeastSquare::runLeastSquare_RN(double& var, math::Matrixd& N_dot, math::Matrixd& C_dot, math::Matrixd& C_2dot, math::Matrixd& N_bar, math::Matrixd& X, const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect)
	{
		unsigned int sizeOfUpper = 0;

		for (unsigned int i = 0; i < (boundarySect + 1); ++i)
		{
			sizeOfUpper += numParameters[i] * numSets[i];
		}

		unsigned int sizeOfLower = 0;

		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			sizeOfLower += numParameters[i] * numSets[i];
		}

		/// N_dot
		N_dot = make_N_dot_New(sizeOfUpper, boundarySect);
		///C_dot(K_dot)
		C_dot = make_C_dot_New(sizeOfUpper, boundarySect);
		///C_2dot(K_2dot)
		C_2dot = make_C_2dot_New(sizeOfLower, boundarySect);
		/// N bar matrix
		N_bar = make_N_bar(boundarySect);

		//
		/// Fix parameters (full control parameters)
		//
		fixParametersRN(N_dot, N_bar, C_dot, C_2dot, boundarySect, fixedParamIdx);

		///N_bar_transe
		math::Matrixd N_bar_trans;
		N_bar_trans = N_bar.transpose();

		math::Matrixd N_bar_N_2dot_inv(N_bar.getRows(), N_bar.getCols(), 0.0);

		unsigned int indexOffset = 0;

		///N_2dot_inverse
		std::vector<NdotMat> N2dotInv(NdotList.size());
		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			N2dotInv[i].get().resize(NdotList[i].get().size());

			///Make N_2dot_inverse matrix
			unsigned int sizeOfi = numParameters[i];

			for (unsigned int j = 0; j < numSets[i]; ++j)
			{
				try
				{
					N2dotInv[i].get()[j] = NdotList[i].get()[j].inverse();
				}
				catch (std::runtime_error e)
				{
					std::cout << e.what() << std::endl;
					std::cout << i << "section " << j << "th set" << std::endl;
					continue;
				}

				unsigned int idx = indexOffset + j * sizeOfi;
				math::Matrixd Nbar_i = N_bar.getSubset(0, idx, N_bar.getRows(), sizeOfi);
				math::Matrixd Nbar_N2dot_i_inv = Nbar_i % N2dotInv[i].get()[j];
				N_bar_N_2dot_inv.insertPlus(0, idx, Nbar_N2dot_i_inv);
			}

			indexOffset += numSets[i] * sizeOfi;
		}

		math::Matrixd S;
		math::Matrixd N_2bar_N_2dot_inv_N_bar_trans = N_bar_N_2dot_inv % N_bar_trans;

		S = (N_dot - N_2bar_N_2dot_inv_N_bar_trans);

		math::Matrixd E;

		E = (C_dot - (N_bar_N_2dot_inv % C_2dot));

		///
		/// Solve X-dot for IOPs and EOPs
		///
		math::Matrixd X_dot = S.inverse() % E;

		///
		/// Solve X-2dot for points, lines and triangle patches
		///

		math::Matrixd N_bar_trans_X = N_bar_trans % X_dot;

		/// X2dot
		std::vector<math::Matrixd> x2dot(numSections - (boundarySect + 1));
		indexOffset = 0;
		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			unsigned int sizeOfi = numParameters[i];

			auto& xi = x2dot[i - (boundarySect + 1)];
			xi.resize(sizeOfi * numSets[i], 1, 0.0);

			for (unsigned int j = 0; j < numSets[i]; ++j)
			{
				unsigned int index = j * sizeOfi;
				const auto C_2dot_i = C_2dot.getSubset(indexOffset + index, 0, sizeOfi, 1);
				const auto& N_2dot_inv_j = N2dotInv[i].get()[j];
				xi.insertPlus(index, 0, N_2dot_inv_j % (C_2dot_i - N_bar_trans_X.getSubset(index, 0, sizeOfi, 1)));
			}

			indexOffset += numSets[i] * sizeOfi;
		}

		/// Estimated unknowns
		X = X_dot;
		for (unsigned int i = 0; i < x2dot.size(); ++i)
			X.addRows(x2dot[i]);

		/// variance of unit weight
		var = this->sumEtpe / (this->numEq - this->numUnknown);
	}

	math::Matrixd LeastSquare::runLeastSquare_Partitioned(double& var, math::Matrixd& X, const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect, math::Matrixd& retN)
	{
		unsigned int sizeOfUpper = 0;

		for (unsigned int i = 0; i < (boundarySect + 1); ++i)
		{
			sizeOfUpper += numParameters[i] * numSets[i];
		}

		unsigned int sizeOfLower = 0;

		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			sizeOfLower += numParameters[i] * numSets[i];
		}

		/// N_dot
		math::Matrixd N_dot = make_N_dot_New(sizeOfUpper, boundarySect);
		///C_dot(K_dot)
		math::Matrixd C_dot = make_C_dot_New(sizeOfUpper, boundarySect);
		///C_2dot(K_2dot)
		math::Matrixd C_2dot = make_C_2dot_New(sizeOfLower, boundarySect);
		/// N bar matrix
		math::Matrixd N_bar = make_N_bar(boundarySect);
		/// N_2dot
		math::Matrixd N_2dot = make_N_2dot_New(sizeOfLower, boundarySect);
		/// C matrix
		math::Matrixd C;
		C.insert(0, 0, C_dot);
		C.insert(C.getRows(), 0, C_2dot);

		//
		/// Fix parameters (full control parameters)
		//
		fixParametersPartitioned(N_dot, N_2dot, N_bar, C, fixedParamIdx);

		/// Solve
		auto Ninv = math::inversePartitionedMatrixd(N_dot, N_bar, N_bar.transpose(), N_2dot);
		X = Ninv % C;

		/// standard deviation of unit weight
		var = this->sumEtpe / (this->numEq - this->numUnknown);

		/// return a Normal matrix
		retN.resize(N_dot.getRows() + N_2dot.getRows(), N_dot.getCols() + N_2dot.getCols());
		retN.insert(0, 0, N_dot);
		retN.insert(0, N_dot.getCols(), N_bar);
		retN.insert(N_dot.getRows(), 0, N_bar.transpose());
		retN.insert(N_dot.getRows(), N_dot.getCols(), N_2dot);

		return Ninv;
	}

	/**Parameters' constraints*/
	double LeastSquare::fill_Nmat_Param(const math::Matrixd& aParam, const math::Matrixd& w, const math::Matrixd& l, const unsigned int index, const unsigned int section)
	{
		std::vector<AMat> a(1);
		a[0].a = aParam;
		a[0].index = index;
		a[0].sect = section;
		const unsigned int nEqs = numParameters[section]; /// l.getRows()

		return fillNmat(a, w, l);
	}

	/**Make N_dot matrix from N_dot elements list*/
	math::Matrixd LeastSquare::make_N_dot_New(const unsigned int sizeOfUpper, const unsigned int sect) const
	{
		math::Matrixd Ndot(sizeOfUpper, sizeOfUpper, 0.0);

		unsigned int pos0 = 0;

		for (unsigned int i = 0; i <= sect; ++i)
		{
			unsigned int idx0 = static_cast<unsigned int>(i);

			Ndot.insert(pos0, pos0, getNdot(idx0));
						
			unsigned int pos1 = pos0;

			for (unsigned int j = i + 1; j <= sect; ++j)
			{
				unsigned int idx1 = static_cast<unsigned int>(j);
				auto Nbar = getNbar(idx0, idx1);

				pos1 += numParameters[j-1] * numSets[j-1];
				Ndot.insert(pos0, pos1, Nbar);
			}

			pos0 += numParameters[i] * numSets[i];
		}

		return Ndot;
	}

	/**Make N_2dot matrix from N_dot elements list*/
	math::Matrixd LeastSquare::make_N_2dot_New(const unsigned int sizeOfLower, const unsigned int sect) const
	{
		math::Matrixd Ndot(sizeOfLower, sizeOfLower, 0.0);

		unsigned int pos0 = 0;

		for (unsigned int i = sect + 1; i < numSections; ++i)
		{
			unsigned int idx0 = static_cast<unsigned int>(i);

			Ndot.insert(pos0, pos0, getNdot(idx0));

			unsigned int pos1 = pos0;

			for (unsigned int j = i + 1; j <= sect; ++j)
			{
				unsigned int idx1 = static_cast<unsigned int>(j);
				auto Nbar = getNbar(idx0, idx1);

				pos1 += numParameters[j - 1] * numSets[j - 1];
				Ndot.insert(pos0, pos1, Nbar);
			}

			pos0 += numParameters[i] * numSets[i];
		}

		return Ndot;
	}

	/**Make C_dot matrix from C_dot elements list*/
	math::Matrixd LeastSquare::make_C_dot_New(const unsigned int sizeOfUpper, const unsigned int sect) const
	{
		math::Matrixd cDot(sizeOfUpper, 1, 0.0);
		unsigned int pos0 = 0;

		for (unsigned int i = 0; i <= sect; ++i)
		{
			unsigned int idx0 = static_cast<unsigned int>(i);
			cDot.insert(pos0, 0, getCdot(idx0));
			pos0 += numParameters[i] * numSets[i];
		}

		return cDot;
	}

	/**Make C_2dot matrix from C_2dot_point and C_2dot_line elements list & triangle patches for LIDAR*/
	math::Matrixd LeastSquare::make_C_2dot_New(const unsigned int sizeOfLower, const unsigned int sect) const
	{
		math::Matrixd c2Dot(sizeOfLower, 1, 0.0);
		unsigned int pos0 = 0;

		for (unsigned int i = (sect + 1); i < numSections; ++i)
		{
			unsigned int idx0 = static_cast<unsigned int>(i);
			c2Dot.insert(pos0, 0, getCdot(idx0));
			pos0 += numParameters[i] * numSets[i];
		}

		return c2Dot;
	}

	/**fill the normal matrix*/
	double LeastSquare::fillNmat(const std::vector<AMat>& a, const math::Matrixd& w, const math::Matrixd& l, const int adjustedNumEq)
	{
		const int eqCount = adjustedNumEq + l.getRows();

		for (unsigned int i = 0; i < static_cast<unsigned int>(a.size()); ++i)
		{
			const math::Matrixd& ai = a[i].a;

			math::Matrixd ai_t = ai.transpose();

			/// AT W L
			this->CdotList[a[i].sect].get()[a[i].index] += ai_t % w % l;

			for (unsigned int j = i; j < static_cast<unsigned int>(a.size()); ++j)
			{
				const math::Matrixd& aj = a[j].a;
				math::Matrixd aj_t = aj.transpose();

				/// N-dot, diagonal parts of a normal matrix
				if (i == j)
				{
					math::Matrixd& Ndoti = this->NdotList[a[i].sect].get()[a[i].index];
					/// AT W A
					/// All elements were initialized as a empty sub normal matrix
					Ndoti += ai_t % w % aj;
				}
				/// N-bar, off-diagonal parts of a normal matrix
				else
				{
					math::Matrixd& Nbarij = this->NbarList[a[i].sect][a[j].sect].get()[a[i].index][a[j].index];

					/// AT1 W A2
					/// Need to check it empty because all elements were initialized as a zero-size matrix
					if (Nbarij.getRows() == 0) Nbarij = ai_t % w % aj;
					else Nbarij += ai_t % w % aj;
				}
			}
		}

		//Posteriori variance
		double etpe = (l.transpose() % w % l)(0, 0);
		this->sumEtpe += etpe;
		//Error list
		Error.push_back(l);

		//Number of equations
		numEq += eqCount;

		return etpe;
	}

	/**get Cdot matrix for each section*/
	math::Matrixd LeastSquare::getCdot(const unsigned int idx) const
	{
		unsigned int matSize = numParameters[idx] * numSets[idx];
		math::Matrixd retmat(matSize, 1, 0.0);

		const CdotMat& cdot = CdotList[idx];

		for (unsigned int i = 0; i < numSets[idx]; ++i)
		{
			retmat.insert(i * numParameters[idx], 0, cdot.get()[i]);
		}

		return retmat;
	}

	/**get Ndot matrix for each section*/
	math::Matrixd LeastSquare::getNdot(const unsigned int idx) const
	{
		unsigned int matSize = numParameters[idx] * numSets[idx];
		math::Matrixd retmat(matSize, matSize, 0.0);

		const NdotMat& ndot = NdotList[idx];

		for (unsigned int i = 0; i < numSets[idx]; ++i)
		{
			retmat.insert(i * numParameters[idx], i * numParameters[idx], ndot.get()[i]);
		}

		return retmat;
	}

	/**get Nbar matrix for each off-diagonal section*/
	math::Matrixd LeastSquare::getNbar(const unsigned int idx1, const unsigned int idx2) const
	{
		const NbarMat& NbarSection = NbarList[idx1][idx2];

		math::Matrixd retmat(numParameters[idx1] * numSets[idx1], numParameters[idx2] * numSets[idx2], 0.0);

		for (unsigned int i = 0; i < numSets[idx1]; ++i)
		{
			for (unsigned int j = 0; j < numSets[idx2]; ++j)
			{
				const auto& Nbarij = NbarSection.readOnly()[i][j];
				/// Need to check it empty because all elements were initialized as a zero-size matrix
				if (Nbarij.getRows() > 0) /// not empty
				{
					retmat.insert(i*numParameters[idx1], j*numParameters[idx2], Nbarij);
				}
			}
		}

		return retmat;
	}

	math::Matrixd LeastSquare::make_N_bar(const unsigned int boundarySect) const
	{
		/// Number of parameters of each section
		std::vector<unsigned int> NbarRows(static_cast<size_t>(boundarySect + 1));
		unsigned int allRows = 0;
		for (unsigned int i = 0; i < (boundarySect + 1); ++i)
		{
			NbarRows[i] = numParameters[i] * numSets[i];
			allRows += NbarRows[i];
		}

		std::vector<unsigned int> NbarCols(numSections - (boundarySect + 1));
		unsigned int allCols = 0;
		for (unsigned int j = (boundarySect + 1); j < numSections; ++j)
		{
			NbarCols[j - (boundarySect + 1)] = numParameters[j] * numSets[j];
			allCols += NbarCols[j - (boundarySect + 1)];
		}

		/// N bar matrix
		math::Matrixd N_bar(allRows, allCols, 0.0);

		unsigned int rowPos = 0;
		for (unsigned int i = 0; i < (boundarySect + 1); ++i)
		{
			unsigned int idxR = static_cast<unsigned int>(i);

			if (numSets[i] > 0)
			{
				unsigned int colPos = 0;
				for (unsigned int j = (boundarySect + 1); j < numSections; ++j)
				{
					unsigned int idxC = static_cast<unsigned int>(j);

					if (numSets[j] > 0)
					{
						N_bar.insert(rowPos, colPos, getNbar(idxR, idxC));
					}

					colPos += NbarCols[j - (boundarySect + 1)];
				}
			}

			rowPos += NbarRows[i];
		}

		return N_bar;
	}

	/**write given string on given file*/
	void LeastSquare::fileOut(const std::string& fname, const std::string& st) const
	{
		std::fstream outfile;
		outfile.open(fname, std::ios::app);

		if (!outfile)
		{
			outfile.open(fname, std::ios::out);

			if (!outfile)
			{
				std::string msg = std::string("error in writing in the file: ") + fname;
				throw std::runtime_error(msg.c_str());
			}
		}

		outfile << st.c_str();
		outfile.close();
	}

	/**fix parameters*/
	void LeastSquare::fixParametersRN(math::Matrixd& Ndot, math::Matrixd& Nbar, math::Matrixd& Cdot, math::Matrixd& C2dot, const unsigned int boundarySect, const std::vector<unsigned int>& fixedParamIdx)
	{
		for (const auto& pIdx : fixedParamIdx)
		{
			/// Fix parameters in a [Ndot] matrix
			if (pIdx < Ndot.getRows())
			{
				for (unsigned int i = 0; i < Ndot.getRows(); ++i)
				{
					try
					{
						if (i == pIdx)
						{
							Ndot(i, pIdx) = 1.0;
						}
						else
						{
							Ndot(i, pIdx) = 0.0;
							Ndot(pIdx, i) = 0.0;
						}
					}
					catch (...)
					{
						std::cout << "Error from fixParameters: " << pIdx << " th parameter in Ndot matrix (" << i << ", " << pIdx << ")" << std::endl;
						std::cout << "Ndot size: " << Ndot.getRows() << " rows and " << Ndot.getCols() << " cols" << std::endl;
					}
				}

				Cdot(pIdx, 0) = 0.0;
			}
			/// Fix parameters in a [N2dot] matrix
			else
			{
				const unsigned int dotSize = Ndot.getRows();				
				const unsigned int dot2Idx = pIdx - dotSize;

				C2dot(dot2Idx, 0) = 0.0;

				unsigned int indexOffset = 0;
				unsigned int sectionUpperBound = 0;
				for (unsigned int sectIdx = (boundarySect + 1); sectIdx < numSections; ++sectIdx)
				{
					sectionUpperBound += numSets[sectIdx] * numParameters[sectIdx];

					if (dot2Idx < sectionUpperBound)
					{
						unsigned int do2Idx_sect = dot2Idx - indexOffset;
						
						if (numParameters[sectIdx] == 0)
							continue;

						unsigned int set = do2Idx_sect / numParameters[sectIdx];
						unsigned int paramIdx = do2Idx_sect % numParameters[sectIdx];

						math::Matrixd& N2dot_i = NdotList[sectIdx].get()[set];

						for (unsigned int p = 0; p < N2dot_i.getRows(); ++p)
						{
							try
							{
								if (p == paramIdx)
								{
									N2dot_i(p, paramIdx) = 1.0;
								}
								else
								{
									N2dot_i(p, paramIdx) = 0.0;
									N2dot_i(paramIdx, p) = 0.0;
								}
							}
							catch (...)
							{
								std::cout << "Error from fixParameters: " << paramIdx << " th parameter in N2dot_i matrix (" << p << ", " << paramIdx << ")" << std::endl;
								std::cout << "N2dot_i size: " << N2dot_i.getRows() << " rows and " << N2dot_i.getCols() << " cols" << std::endl;
							}
						}

						break;
					}

					indexOffset = sectionUpperBound;
				}
			}

			/// Fix parameters in a [Nbar] matrix
			if (pIdx < Nbar.getRows())
			{
				const unsigned int row = pIdx;
				for (unsigned int i = 0; i < Nbar.getCols(); ++i)
					Nbar(pIdx, i) = 0.0;
			}
			else
			{
				const unsigned int col = pIdx - Nbar.getRows();
				for (unsigned int i = 0; i < Nbar.getRows(); ++i)
					Nbar(i, col) = 0.0;
			}
		}
	}

	/**fix parameters*/
	void LeastSquare::fixParametersPartitioned(math::Matrixd& Ndot, math::Matrixd& N2dot, math::Matrixd& Nbar, math::Matrixd& C, const std::vector<unsigned int>& fixedParamIdx)
	{
		const unsigned int dotSize = Ndot.getRows();

		for (const auto& pIdx : fixedParamIdx)
		{
			/// Fix parameters in a [CDot] matrix
			C(pIdx, 0) = 0.0;

			if (pIdx < Ndot.getRows())
			{
				/// Fix parameters in a [NDot] matrix
				for (unsigned int idx = 0; idx < Ndot.getRows(); ++idx)
				{
					if (idx == pIdx)
					{
						Ndot(idx, pIdx) = 1.0;
					}
					else
					{
						Ndot(idx, pIdx) = 0.0;
						Ndot(pIdx, idx) = 0.0;
					}
				}

				/// Fix parameters in a [Nbar] matrix
				const unsigned int row = pIdx;
				for (unsigned int i = 0; i < Nbar.getCols(); ++i)
					Nbar(row, i) = 0.0;
			}
			/// 2dot
			else
			{
				const unsigned int pIdx2 = pIdx - dotSize;

				/// Fix parameters in a [N2Dot] matrix
				for (unsigned int idx2 = 0; idx2 < N2dot.getRows(); ++idx2)
				{
					if (idx2 == pIdx2)
					{
						N2dot(idx2, idx2) = 1.0;
					}
					else
					{
						N2dot(idx2, pIdx2) = 0.0;
						N2dot(pIdx2, idx2) = 0.0;
					}
				}

				/// Fix parameters in a [Nbar] matrix
				for (unsigned int i = 0; i < Nbar.getRows(); ++i)
					Nbar(i, pIdx2) = 0.0;
			}
		}
	}

	/**fix parameters*/
	void LeastSquare::fixParametersPartitioned(math::Matrixd& Ndot, math::Matrixd& N2dot, math::Matrixd& Nbar, const std::vector<unsigned int>& fixedParamIdx)
	{
		const unsigned int dotSize = Ndot.getRows();

		for (const auto& pIdx : fixedParamIdx)
		{
			if (pIdx < Ndot.getRows())
			{
				/// Fix parameters in a [NDot] matrix
				for (unsigned int idx = 0; idx < Ndot.getRows(); ++idx)
				{
					if (idx == pIdx)
					{
						Ndot(idx, pIdx) = 1.0;
					}
					else
					{
						Ndot(idx, pIdx) = 0.0;
						Ndot(pIdx, idx) = 0.0;
					}
				}

				/// Fix parameters in a [Nbar] matrix
				const unsigned int row = pIdx;
				for (unsigned int i = 0; i < Nbar.getCols(); ++i)
					Nbar(row, i) = 0.0;
			}
			/// 2dot
			else
			{
				const unsigned int pIdx2 = pIdx - dotSize;

				/// Fix parameters in a [N2Dot] matrix
				for (unsigned int idx2 = 0; idx2 < N2dot.getRows(); ++idx2)
				{
					if (idx2 == pIdx2)
					{
						N2dot(idx2, idx2) = 1.0;
					}
					else
					{
						N2dot(idx2, pIdx2) = 0.0;
						N2dot(pIdx2, idx2) = 0.0;
					}
				}

				/// Fix parameters in a [Nbar] matrix
				for (unsigned int i = 0; i < Nbar.getRows(); ++i)
					Nbar(i, pIdx2) = 0.0;
			}
		}
	}

	/**fix parameters*/
	void LeastSquare::fixParameters(math::Matrixd& N, math::Matrixd& C, const std::vector<unsigned int>& fixedParamIdx)
	{
		for (const auto& pIdx : fixedParamIdx)
		{
			for (unsigned int i = 0; i < N.getRows(); ++i)
			{
				try
				{
					if (i == pIdx)
					{
						N(i, pIdx) = 1.0;
					}
					else
					{
						N(i, pIdx) = 0.0;
						N(pIdx, i) = 0.0;
					}
				}
				catch (...)
				{
					std::cout << "Error from fixParameters: " << pIdx << " th parameter in Ndot matrix (" << i << ", " << pIdx << ")" << std::endl;
					std::cout << "Ndot size: " << N.getRows() << " rows and " << N.getCols() << " cols" << std::endl;
				}

				C(pIdx, 0) = 0.0;
			}
		}
	}

	/**fix parameters*/
	void LeastSquare::fixParameters(math::Matrixd& N, const std::vector<unsigned int>& fixedParamIdx)
	{
		for (const auto& pIdx : fixedParamIdx)
		{
			for (unsigned int i = 0; i < N.getRows(); ++i)
			{
				try
				{
					if (i == pIdx)
					{
						N(i, pIdx) = 1.0;
					}
					else
					{
						N(i, pIdx) = 0.0;
						N(pIdx, i) = 0.0;
					}
				}
				catch (...)
				{
					std::cout << "Error from fixParameters: " << pIdx << " th parameter in Ndot matrix (" << i << ", " << pIdx << ")" << std::endl;
					std::cout << "Ndot size: " << N.getRows() << " rows and " << N.getCols() << " cols" << std::endl;
				}
			}
		}
	}

	/**Levenberg-Marquardt*/
	const double up = 10.0;
	const double down = 0.1;
	const double lambda0 = 0.0001;

	void LeastSquare::runLeastSquare_RN_LM(double& var, math::Matrixd& N_dot, math::Matrixd& C_dot, math::Matrixd& C_2dot, math::Matrixd& N_bar, math::Matrixd& X, const std::vector<unsigned int>& fixedParamIdx, const unsigned int boundarySect, const double& mult)
	{
		unsigned int sizeOfUpper = 0;

		for (unsigned int i = 0; i < (boundarySect + 1); ++i)
		{
			sizeOfUpper += numParameters[i] * numSets[i];
		}

		unsigned int sizeOfLower = 0;

		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			sizeOfLower += numParameters[i] * numSets[i];
		}

		/// N_dot
		N_dot = make_N_dot_New(sizeOfUpper, boundarySect);
		for (unsigned int r = 0; r < N_dot.getRows(); ++r)
			N_dot(r, r) = N_dot(r, r) * mult;
		///C_dot(K_dot)
		C_dot = make_C_dot_New(sizeOfUpper, boundarySect);
		///C_2dot(K_2dot)
		C_2dot = make_C_2dot_New(sizeOfLower, boundarySect);
		/// N bar matrix
		N_bar = make_N_bar(boundarySect);

		//
		/// Fix parameters (full control parameters)
		//
		fixParametersRN(N_dot, N_bar, C_dot, C_2dot, boundarySect, fixedParamIdx);

		///N_bar_transe
		math::Matrixd N_bar_trans;
		N_bar_trans = N_bar.transpose();

		math::Matrixd N_bar_N_2dot_inv(N_bar.getRows(), N_bar.getCols(), 0.0);

		unsigned int indexOffset = 0;

		///N_2dot_inverse
		std::vector<NdotMat> N2dotInv(NdotList.size());
		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			N2dotInv[i].get().resize(NdotList[i].get().size());

			///Make N_2dot_inverse matrix
			unsigned int sizeOfi = numParameters[i];

			for (unsigned int j = 0; j < numSets[i]; ++j)
			{
				try
				{
					N2dotInv[i].get()[j] = NdotList[i].get()[j].inverse();
				}
				catch (std::runtime_error e)
				{
					std::cout << e.what() << std::endl;
					std::cout << i << "section " << j << "th set" << std::endl;
					continue;
				}

				unsigned int idx = indexOffset + j * sizeOfi;
				math::Matrixd Nbar_i = N_bar.getSubset(0, idx, N_bar.getRows(), sizeOfi);
				math::Matrixd Nbar_N2dot_i_inv = Nbar_i % N2dotInv[i].get()[j];
				N_bar_N_2dot_inv.insertPlus(0, idx, Nbar_N2dot_i_inv);
			}

			indexOffset += numSets[i] * sizeOfi;
		}

		math::Matrixd S;
		math::Matrixd N_2bar_N_2dot_inv_N_bar_trans = N_bar_N_2dot_inv % N_bar_trans;

		S = (N_dot - N_2bar_N_2dot_inv_N_bar_trans);

		math::Matrixd E;

		E = (C_dot - (N_bar_N_2dot_inv % C_2dot));

		///
		/// Solve X-dot for IOPs and EOPs
		///
		math::Matrixd X_dot = S.inverse() % E;

		///
		/// Solve X-2dot for points, lines and triangle patches
		///

		math::Matrixd N_bar_trans_X = N_bar_trans % X_dot;

		/// X2dot
		std::vector<math::Matrixd> x2dot(numSections - (boundarySect + 1));
		indexOffset = 0;
		for (unsigned int i = (boundarySect + 1); i < numSections; ++i)
		{
			unsigned int sizeOfi = numParameters[i];

			auto& xi = x2dot[i - (boundarySect + 1)];
			xi.resize(sizeOfi * numSets[i], 1, 0.0);

			for (unsigned int j = 0; j < numSets[i]; ++j)
			{
				unsigned int index = j * sizeOfi;
				const auto C_2dot_i = C_2dot.getSubset(indexOffset + index, 0, sizeOfi, 1);
				const auto& N_2dot_inv_j = N2dotInv[i].get()[j];
				xi.insertPlus(index, 0, N_2dot_inv_j % (C_2dot_i - N_bar_trans_X.getSubset(index, 0, sizeOfi, 1)));
			}

			indexOffset += numSets[i] * sizeOfi;
		}

		/// Estimated unknowns
		X = X_dot;
		for (unsigned int i = 0; i < x2dot.size(); ++i)
			X.addRows(x2dot[i]);

		/// variance of unit weight
		var = this->sumEtpe / (this->numEq - this->numUnknown);
	}
}