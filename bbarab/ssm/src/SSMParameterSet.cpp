/*
* Copyright(c) 1999-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/SSMParameterSet.h>

namespace param
{
	ParameterSet::ParameterSet()
		: name(std::string("N/A")),
		description(std::string("N/A"))
	{
		this->vals.resize(1);
	};

	ParameterSet::ParameterSet(const ParameterSet &copy)
	{ 
		getCopy(copy); 
	}

	void ParameterSet::operator = (const ParameterSet &copy) 
	{ 
		getCopy(copy); 
	}

	ParameterSet ParameterSet::operator - (const ParameterSet &copy) const
	{
		ParameterSet retVal(*this);

		for (unsigned int i = 0; i < this->vals.size(); ++i)
		{
			retVal.vals[i] = this->vals[i] - copy.vals[i];
		}

		return retVal;
	}

	Parameter& ParameterSet::operator [] (const unsigned int idx) 
	{ 
		return this->vals[idx];
	}

	const Parameter& ParameterSet::operator [] (const unsigned int idx) const
	{
		return this->vals[idx];
	}

	unsigned int ParameterSet::size() const 
	{ 
		return static_cast<unsigned int>(this->vals.size());
	}
	
	void ParameterSet::resize(const unsigned int n) 
	{ 
		this->vals.resize(n);
	}

	std::vector<Parameter> ParameterSet::get() const
	{ 
		return this->vals;
	}
	
	void ParameterSet::set(const std::vector<Parameter> &newVals)
	{ 
		this->vals = newVals;
	}

	std::string ParameterSet::getName() const 
	{ 
		return this->name;
	}
	
	void ParameterSet::setName(const std::string &newName) 
	{ 
		this->name = newName; 
	}

	std::string ParameterSet::getDescription() const 
	{ 
		return this->name;
	}

	void ParameterSet::setDescription(const std::string &newDescription) 
	{ 
		this->description = newDescription;
	}

	void ParameterSet::getCopy(const ParameterSet& copy)
	{
		this->vals = copy.vals;
		this->name = copy.name;
		this->description = copy.description;
		this->varcov = copy.varcov;
	}

	math::Matrixd ParameterSet::getVariance() const
	{
		return this->varcov;
	}

	void ParameterSet::setVariance(const math::Matrixd& newVals)
	{
		this->varcov = newVals;
	}
}