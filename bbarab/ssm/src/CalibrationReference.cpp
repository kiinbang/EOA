#pragma once

#include <ssm/include/calibrationReference.h>
#include <ssm/include/utilitygrocery.h>

#include <TinyEXIF/TinyEXIF.h>

namespace data
{
	std::string getOrientation(const unsigned int orientation)
	{
		std::string imgOri;
		switch (orientation)
		{
		case 0:
			return std::string("0: unspecified in EXIF data");
		case 1:
			return std::string("1: upper left of image");
		case 3:
			return std::string("3: lower right of image");
		case 6:
			return std::string("6: upper right of image");
		case 8:
			return std::string("8: lower left of image");
		case 9:
			return std::string("9: undefined");
		default:
			return std::string("N/A");
		}
	}

	double getPixelPitch(const unsigned int imgWidth, const unsigned int imgHeight, const double f35mm)
	{
		double fFrameWidth = fullFrameSize_w, fFrameHeight = fullFrameSize_h; /// mm
		double diagonal = sqrt(fFrameWidth * fFrameWidth + fFrameHeight * fFrameHeight);
		double imgDiagonal = sqrt(double(imgWidth * imgWidth) + double(imgHeight * imgHeight));
		double pixPitch = diagonal / imgDiagonal; /// mm for 35mm frull frame format
		return pixPitch;
	}

	void getCamerInfo(const std::string& jpgPath,
		std::string& timeStamp,
		std::string& camModel,
		double& f,
		double& f35mm,
		double& pixPitch,
		double& pixPitch35mm,
		unsigned int& imgW,
		unsigned int& imgH,
		unsigned int& orientation)
	{
		uint8_t* data;
		std::streampos length;
		std::ifstream file(jpgPath.c_str(), std::ifstream::in | std::ifstream::binary);
		file.seekg(0, std::ios::end);
		length = file.tellg();
		file.seekg(0, std::ios::beg);
		data = new uint8_t[static_cast<unsigned int>(length)];
		file.read((char*)data, length);
		file.close();
		TinyEXIF::EXIFInfo imageEXIF(data, static_cast<unsigned int>(length));

		if (imageEXIF.Fields)
			std::cout
			<< "GPSTimeStamp " << imageEXIF.GeoLocation.GPSTimeStamp << "\n"
			<< "DateTime " << imageEXIF.DateTime << "\n"
			<< "Exposure time " << imageEXIF.ExposureTime << " sec" << "\n"
			<< "Image Description " << imageEXIF.ImageDescription << "\n"
			<< "Image Resolution " << imageEXIF.ImageWidth << "x" << imageEXIF.ImageHeight << " pixels\n"
			<< "Camera Model " << imageEXIF.Make << " - " << imageEXIF.Model << "\n"
			<< "Camera Serial# " << imageEXIF.SerialNumber << "\n"
			<< "Camera Lens Make# " << imageEXIF.LensInfo.Make << "\n"
			<< "Camera Lens Model# " << imageEXIF.LensInfo.Model << "\n"
			<< "Image Orientation: " << getOrientation(imageEXIF.Orientation) << "\n"
			<< "GeoLocation.LatComponents.Latitude: " << imageEXIF.GeoLocation.Latitude << "\n"
			<< "GeoLocation.Longitude: " << imageEXIF.GeoLocation.Longitude << "\n"
			<< "GeoLocation.Altitude: " << imageEXIF.GeoLocation.Altitude << "\n"
			<< "GeoLocation.GimbalRollDegree: " << imageEXIF.GeoLocation.GimbalRollDegree << "\n"
			<< "GeoLocation.GimbalPitchDegree: " << imageEXIF.GeoLocation.GimbalPitchDegree << "\n"
			<< "GeoLocation.GimbalYawDegree: " << imageEXIF.GeoLocation.GimbalYawDegree << "\n"
			<< "Focal Length in 35mm format " << imageEXIF.LensInfo.FocalLengthIn35mm << " mm" << "\n"
			<< "Pixel pitch (equivalent to 35mm full frame) " << getPixelPitch(imageEXIF.ImageWidth, imageEXIF.ImageHeight, imageEXIF.LensInfo.FocalLengthIn35mm) << " mm" << "\n"
			<< "Focal Length " << imageEXIF.FocalLength << " mm" << std::endl;

		if (imageEXIF.GeoLocation.GPSTimeStamp == std::string(""))
			timeStamp = imageEXIF.DateTime;
		else
			timeStamp = imageEXIF.GeoLocation.GPSTimeStamp;

		camModel = imageEXIF.Make + std::string("-") + imageEXIF.Model;
		f35mm = imageEXIF.LensInfo.FocalLengthIn35mm;
		f = imageEXIF.FocalLength;
		pixPitch35mm = getPixelPitch(imageEXIF.ImageWidth, imageEXIF.ImageHeight, imageEXIF.LensInfo.FocalLengthIn35mm);
		pixPitch = pixPitch35mm / f35mm * f;
		imgW = imageEXIF.ImageWidth;
		imgH = imageEXIF.ImageHeight;
		auto lensInfo = imageEXIF.LensInfo;

		// Image orientation, start of data corresponds to
		// 0: unspecified in EXIF data
		// 1: upper left of image (0)
		// 3: lower right of image (180 deg)
		// 6: upper right of image
		// 8: lower left of image
		// 9: undefined
		orientation = imageEXIF.Orientation;

		delete[] data;
	}
	
	geo::Point2f getPhotoCoordinate(const geo::Point2f& imgCoord, const float resW, const float resH, const geo::Point2f& center)
	{
		geo::Point2f photoCoord;
		photoCoord.x = imgCoord.x * resW - center.x;
		photoCoord.y = -imgCoord.y * resH + center.y;
		return photoCoord;
	}

	/// Rotate displayed image coordinates to encoded image coordinates
	geo::Point2f getEncodedImageCoordinates(const geo::Point2f& imgCoord, const unsigned int orientation, const unsigned int width, const unsigned int height)
	{
		// 0: unspecified in EXIF data
		// 1: col#0:left	row#0:top
		// 3: col#0:right	row#0:bottom (180 deg)
		// 6: col#0:top		row#0:right (90 deg CCW (Counter Clock Wise))
		// 8: col#0:bottom	row#0:left (90 deg CW (Clock Wise))
		// 9: undefined

		geo::Point2f rotPt;

		switch (orientation)
		{
		case 0:
			rotPt = imgCoord;
			break;
		case 1:
			rotPt = imgCoord;
			break;
		case 3:
			rotPt.x = (width - 1) - imgCoord.x;
			rotPt.y = (height - 1) - imgCoord.y;
			break;
		case 6:
			rotPt.x = imgCoord.y;
			rotPt.y = (height - 1) - imgCoord.x;
			break;
		case 8:
			rotPt.x = (width - 1) - imgCoord.y;
			rotPt.y = imgCoord.x;
			break;
		case 9:
			rotPt = imgCoord;
			break;
		default:
			rotPt = imgCoord;
			break;
		}

		return rotPt;
	}

	/// Rotate displayed image coordinates to encoded image coordinates
	std::vector<geo::Point2f> getEncodedImageCoordinates(const std::vector<geo::Point2f>& imgCoord, const unsigned int orientation, const unsigned int width, const unsigned int height)
	{
		// 0: unspecified in EXIF data
		// 1: col#0:left	row#0:top
		// 3: col#0:right	row#0:bottom (180 deg)
		// 6: col#0:top		row#0:right (90 deg CCW (Counter Clock Wise))
		// 8: col#0:bottom	row#0:left (90 deg CW (Clock Wise))
		// 9: undefined

		std::vector<geo::Point2f> rotatedPoint;
		rotatedPoint.reserve(imgCoord.size());
		geo::Point2f rotPt;

		for (const geo::Point2f& imgPt : imgCoord)
		{
			rotPt = getEncodedImageCoordinates(imgPt, orientation, width, height);

			rotatedPoint.push_back(rotPt);
		}

		return rotatedPoint;
	}

	float calInitialKappa(const std::vector<geo::Point2f>& centers, const std::vector<geo::Point2f>& centersInRed, const std::vector<float>& refRedAreaOrientation, const int imgOrientation, const int sensorW, const int sensorH)
	{
		float kappa0 = 0.0;
		if (centers.size() > 1)
		{
			float sumKappa = 0.0;

			for (unsigned int i = 0; i < centers.size(); ++i)
			{
				auto encodedCenter = getEncodedImageCoordinates(centers[i], imgOrientation, sensorW, sensorH);
				auto encodedCenterInRed = getEncodedImageCoordinates(centersInRed[i], imgOrientation, sensorW, sensorH);
				float dc = encodedCenterInRed.x - encodedCenter.x;
				float dr = encodedCenterInRed.y - encodedCenter.y;
				float dx = dc;
				float dy = -dr;
				sumKappa += -(static_cast<float>(static_cast<double>(-atan2(dx, dy)) - static_cast<double>(refRedAreaOrientation[i])));
			}

			kappa0 = sumKappa / centers.size();
		}
		else
		{
			auto encodedCenter = getEncodedImageCoordinates(centers[0], imgOrientation, sensorW, sensorH);
			auto encodedCenterInRed = getEncodedImageCoordinates(centersInRed[0], imgOrientation, sensorW, sensorH);
			float dc = encodedCenterInRed.x - encodedCenter.x;
			float dr = encodedCenterInRed.y - encodedCenter.y;
			float dx = dc;
			float dy = -dr;
			kappa0 = -(static_cast<float>(static_cast<double>(-atan2(dx, dy)) - static_cast<double>(refRedAreaOrientation[0])));
		}

		return kappa0;
	}

	CalibrationReference::CalibrationReference(const int sizeW, const int sizeH, const float boardSizeW, const float boardSizeH)
		: width(sizeW),
		height(sizeH)
	{
		resW = boardSizeW / float(sizeW);
		resH = boardSizeH / float(sizeH);
		center.x = (float(sizeW) * resW) * 0.5f;
		center.y = (float(sizeH) * resH) * 0.5f;
	}

	bool CalibrationReference::writeRefBoardTargets(const std::vector<geo::Point2f>& refTargets, double& diagDist, const std::string& outPath)
	{
		if (refTargets.size() < 1)
			return false;

		std::vector<geo::Point2f> phoP;
		int index = 0;
		for (const auto& imgP : refTargets)
		{
			auto p = getPhotoCoordinate(imgP, this->resW, this->resH, this->center);
			phoP.push_back(p);

			++index;
		}

		std::fstream f;
		f.open(outPath, std::ios::out);
		if (!f)
			return false;

		f << _xmlHeader.c_str() << std::endl;
		f << _object_points_s.c_str() << std::endl;
		for (unsigned int i = 0; i < phoP.size(); ++i)
		{
			auto& p = phoP[i];

			f << "\t";
			f << _point_s.c_str();
			f << "\t";
			f << _point_id_s.c_str();
			f << "\t";
			f << i;
			f << "\t";
			f << _point_id_e.c_str();
			f << "\t";
			f << _objxyz_s.c_str();
			f << "\t";
			f << p.x;
			f << "\t";
			f << p.y;
			f << "\t";
			f << "0.0";
			f << "\t";
			f << _objxyz_e.c_str();
			f << "\t";
			f << _variance_s.c_str();
			f << "\t";
			f << _fixed_variance;
			f << "\t";
			f << _variance_e.c_str();
			f << "\t";
			f << _point_e.c_str();
			f << std::endl;
		}
		f << _object_points_e.c_str();
		f << std::endl;

		f.close();

		double dx = static_cast<double>(phoP[phoP.size() - 1].x) - static_cast<double>(phoP[0].x);
		double dy = static_cast<double>(phoP[phoP.size() - 1].y) - static_cast<double>(phoP[0].y);
		diagDist = sqrt(dx * dx + dy * dy);

		return true;
	}

	CalibrationData::CalibrationData(const std::string& folderPath)
		: folder(folderPath)
	{
	}

	bool CalibrationData::openCameraFile()
	{
		camFilePath = folder + std::string("\\calibration.cam");
		camFile.open(camFilePath, std::ios::out);
		if (!camFile)
			return false;

		camFile << _xmlHeader << std::endl;
		camFile << std::endl;
		camFile << _cameras_s << std::endl;

		return true;
	}

	void CalibrationData::addCameraInfo(const IMAGEINFO& cam, const unsigned int id)
	{
		camFile << "\t" << _camera_s << std::endl;
		camFile << "\t\t" << _camera_id_s << id << _camera_id_e << std::endl;
		camFile << "\t\t" << _focal_length_s << std::endl;
		camFile << "\t\t\t" << _value_s << cam.f << _value_e << std::endl;
		camFile << "\t\t\t" << _variance_s << "1.0e+12" << _variance_e << std::endl;
		camFile << "\t\t" << _focal_length_e << std::endl;
		camFile << "\t\t" << _principla_point_s << std::endl;
		camFile << "\t\t\t" << _value_s << cam.xp << "\t" << cam.yp << _value_e << std::endl;
		camFile << "\t\t\t" << _variance_s << "1.0e+12 0.0 0.0 1.0e+12" << _variance_e << std::endl;
		camFile << "\t\t" << _principla_point_e << std::endl;
		camFile << "\t\t" << _pixel_pitch_s << cam.pitch << _pixel_pitch_e << std::endl;
		camFile << "\t\t" << _sensor_size_s << cam.sizeW << "\t" << cam.sizeH << _sensor_size_e << std::endl;
		camFile << "\t\t" << _lens_distortion_s << std::endl;
		camFile << "\t\t\t" << _description << std::endl;
		camFile << "\t\t\t" << _parameter_s << std::endl;
		camFile << "\t\t\t\t" << _value_s << std::endl;
		camFile << "\t\t\t\t\t" << cam.smac[0] << "\t" << cam.smac[1] << "\t" << cam.smac[2] << "\t" << cam.smac[3] << std::endl;
		camFile << "\t\t\t\t\t" << cam.smac[4] << "\t" << cam.smac[5] << "\t" << cam.smac[6] << std::endl;
		camFile << "\t\t\t\t" << _value_e << std::endl;
		camFile << "\t\t\t\t" << _variance_s << std::endl;
		camFile << "\t\t\t\t\t" << "1.0e-12 0.0 0.0 0.0 0.0 0.0 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 1.0e+12 0.0 0.0 0.0 0.0 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 0.0 1.0e+12 0.0 0.0 0.0 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 0.0 0.0 1.0e+12 0.0 0.0 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 0.0 0.0 0.0 1.0e+12 0.0 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 0.0 0.0 0.0 0.0 1.0e+12 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 0.0 0.0 0.0 0.0 0.0 1.0e-12" << std::endl;
		camFile << "\t\t\t\t" << _variance_e << std::endl;
		camFile << "\t\t\t" << _parameter_e << std::endl;
		camFile << "\t\t" << _lens_distortion_e << std::endl;
		camFile << "\t\t" << _alignment_s << std::endl;
		camFile << "\t\t\t" << _offset_s << std::endl;
		camFile << "\t\t\t\t" << _value_s << "0.0 0.0 0.0" << _value_e << std::endl;
		camFile << "\t\t\t\t" << _variance_s << std::endl;
		camFile << "\t\t\t\t\t" << "1.0e-12 0.0 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 1.0e-12 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 0.0 1.0e-12" << std::endl;
		camFile << "\t\t\t\t" << _variance_e << std::endl;
		camFile << "\t\t\t" << _offset_e << std::endl;
		camFile << "\t\t\t" << _boresight_s << std::endl;
		camFile << "\t\t\t\t" << _value_s << "0.0 0.0 0.0" << _value_e << std::endl;
		camFile << "\t\t\t\t" << _variance_s << std::endl;
		camFile << "\t\t\t\t\t" << "1.0e-12 0.0 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 1.0e-12 0.0" << std::endl;
		camFile << "\t\t\t\t\t" << "0.0 0.0 1.0e-12" << std::endl;
		camFile << "\t\t\t\t" << _variance_e << std::endl;
		camFile << "\t\t\t" << _boresight_e << std::endl;
		camFile << "\t\t" << _alignment_e << std::endl;
		camFile << "\t" << _camera_e << std::endl;
		camFile << std::endl;
	}

	void CalibrationData::closeCameraFile()
	{
		camFile << _cameras_e << std::endl;
		camFile.close();
	}

	bool CalibrationData::openPhoFile()
	{
		phoFilePath = folder + std::string("\\calibration.pho");
		phoFile.open(phoFilePath, std::ios::out);
		if (!phoFile)
			return false;

		phoFile << _xmlHeader << std::endl;
		phoFile << std::endl;
		phoFile << _image_observation_s << std::endl;

		return true;
	}

	geo::Point2f CalibrationData::getCenter(const std::vector<geo::Point2f>& phoObs)
	{
		double sumx = 0., sumy = 0.;
		for (const auto& p : phoObs)
		{
			sumx += p.x;
			sumy += p.y;
		}

		geo::Point2f ctr;
		ctr.x = static_cast<float>(sumx) / static_cast<float>(phoObs.size());
		ctr.y = static_cast<float>(sumy) / static_cast<float>(phoObs.size());

		return ctr;
	}

	void CalibrationData::reverse(std::vector<geo::Point2f>& photoPts)
	{
		std::vector<geo::Point2f> copy = photoPts;
		size_t inverseIdx = copy.size() - 1;
		for (unsigned int i = 0; i < copy.size(); ++i)
		{
			photoPts[inverseIdx - i] = copy[i];
		}
	}

	std::vector<geo::Point2f> CalibrationData::calTargetPhotoCoordinates(const IMAGEINFO& imgInfo, const std::vector<geo::Point2f>& imgObs, const float kappa0, const double refDiagDist)
	{
		std::string time;
		if (imgInfo.timeStamp == "")
			time = std::string("0.0");
		else
			time = imgInfo.timeStamp;

		geo::Point2f center;
		center.x = (float(imgInfo.sizeW) * float(imgInfo.pitch)) * 0.5f;
		center.y = (float(imgInfo.sizeH) * float(imgInfo.pitch)) * 0.5f;

		std::vector<geo::Point2f> photoPts;
		photoPts.reserve(imgObs.size());

		for (unsigned int i = 0; i < imgObs.size(); ++i)
		{
			/// Image to photo coordinates
			auto p = getPhotoCoordinate(imgObs[i], float(imgInfo.pitch), float(imgInfo.pitch), center);
			photoPts.push_back(p);
		}

		/// The center (in photo-coordinate) of detected target points
		auto tarCenter = getCenter(photoPts);

		std::cout << "kappa[deg]: " << util::Rad2Deg(kappa0) << std::endl;

		/// Scale and initial position
		geo::Point2f sPt = photoPts[0];
		geo::Point2f ePt = photoPts[photoPts.size() - 1];
		const float dx = ePt.x - sPt.x;
		const float dy = ePt.y - sPt.y;
		const double dist = sqrt(dx * dx + dy * dy);
		const double sf = refDiagDist * 1000.0 / dist;///mm

		const double Z0 = sf * imgInfo.f * 0.001;
		std::cout << "Approximate scale: " << sf << std::endl;
		std::cout << "Approaixmate dist photo to object: " << Z0 << std::endl;
		double rotatedTarCenterX = (cos(kappa0) * static_cast<double>(tarCenter.x) + sin(kappa0) * static_cast<double>(tarCenter.y)) * sf * 0.001;///meter
		double rotatedTarCenterY = (-sin(kappa0) * static_cast<double>(tarCenter.x) + cos(kappa0) * static_cast<double>(tarCenter.y)) * sf * 0.001;///meter
		double eopX0 = -rotatedTarCenterX;
		double eopY0 = -rotatedTarCenterY;
		double eopZ0 = Z0;
		std::string eopxyz = std::to_string(eopX0) + " " + std::to_string(eopY0) + " " + std::to_string(eopZ0);
		std::cout << "Inintial PC: " << eopxyz << std::endl;

		phoFile << "\t" << _image_s1 << imgInfo.imgPath << _image_s2 << std::endl;
		phoFile << "\t\t" << _image_id_s << imgInfo.imgId << _image_id_e << std::endl;
		phoFile << "\t\t" << _camera_id_s << imgInfo.cameraId << _camera_id_e << " " << std::endl;
		phoFile << "\t\t" << _time_s << time << _time_e << std::endl;
		phoFile << "\t\t" << _eop_s << std::endl;
		phoFile << "\t\t\t" << _eopxyz_s << std::endl;
		phoFile << "\t\t\t\t" << _value_s << eopxyz << _value_e << std::endl;
		phoFile << "\t\t\t\t" << _variance_s << std::endl;
		phoFile << "\t\t\t\t\t" << "1.0e+12 0.0 0.0" << std::endl;
		phoFile << "\t\t\t\t\t" << "0.0 1.0e+12 0.0" << std::endl;
		phoFile << "\t\t\t\t\t" << "0.0 0.0 1.0e+12" << std::endl;
		phoFile << "\t\t\t\t" << _variance_e << std::endl;
		phoFile << "\t\t\t" << _eopxyz_e << std::endl;
		phoFile << "\t\t\t" << _opk_s << std::endl;
		phoFile << "\t\t\t\t" << _value_s << " 0.0 0.0 " << std::to_string(util::Rad2Deg(kappa0)) << " " << _value_e << std::endl;
		phoFile << "\t\t\t\t" << _variance_s << std::endl;
		phoFile << "\t\t\t\t\t" << "1.0e+12 0.0 0.0" << std::endl;
		phoFile << "\t\t\t\t\t" << "0.0 1.0e+12 0.0" << std::endl;
		phoFile << "\t\t\t\t\t" << "0.0 0.0 1.0e+12" << std::endl;
		phoFile << "\t\t\t\t" << _variance_e << std::endl;
		phoFile << "\t\t\t" << _opk_e << std::endl;
		phoFile << "\t\t" << _eop_e << std::endl;
		phoFile << "\t\t" << _points_s << std::endl;

		for (unsigned int i = 0; i < photoPts.size(); ++i)
		{
			phoFile << "\t\t\t";
			phoFile << _point_s;
			phoFile << _point_id_s << " " << i << " " << _point_id_e;
			phoFile << _coordinates_s << " " << photoPts[i].x << " " << photoPts[i].y << " " << _coordinates_e;
			phoFile << _variance_s << " 1.0 0.0 0.0 1.0 " << _variance_e;
			phoFile << _point_e << std::endl;
		}

		phoFile << "\t\t" << _points_e << std::endl;
		phoFile << "\t" << _image_e << std::endl;

		return photoPts;
	}

	void CalibrationData::calTargetPhotoCoordinates_Old(const IMAGEINFO& imgInfo, const std::vector<geo::Point2f>& imgObs, geo::Point2f& imgCtRed, const double refDiagDist)
	{
		std::string time;
		if (imgInfo.timeStamp == "")
			time = std::string("0.0");
		else
			time = imgInfo.timeStamp;

		geo::Point2f center;
		center.x = (float(imgInfo.sizeW) * float(imgInfo.pitch)) * 0.5f;
		center.y = (float(imgInfo.sizeH) * float(imgInfo.pitch)) * 0.5f;

		std::vector<geo::Point2f> photoPts;

		for (unsigned int i = 0; i < imgObs.size(); ++i)
		{
			/// Image to photo coordinates
			auto p = getPhotoCoordinate(imgObs[i], float(imgInfo.pitch), float(imgInfo.pitch), center);
			photoPts.push_back(p);
		}

		/// The center of detected target points
		auto tarCenter = getCenter(photoPts);
		/// Azimuth angle of vector center to red-mark
		auto phoCtRed = getPhotoCoordinate(imgCtRed, float(imgInfo.pitch), float(imgInfo.pitch), center);
		double az = util::CalAzimuth(tarCenter.x, tarCenter.y, phoCtRed.x, phoCtRed.y);
		std::cout << "First point: " << photoPts[0].x << " " << photoPts[0].y << std::endl;
		std::cout << "last point: " << photoPts[photoPts.size() - 1].x << " " << photoPts[photoPts.size() - 1].y << std::endl;
		std::cout << "Center of the mark-in-red: " << phoCtRed.x << " " << phoCtRed.y << std::endl;
		std::cout << "Azimuth[deg] of a mark-in-red: " << util::Rad2Deg(az) << std::endl;
		double kappa = -(az + util::Deg2Rad(45.0)); /// Since the reference (chess-board) azimuth -45 deg
		std::cout << "kappa[deg]: " << util::Rad2Deg(kappa) << std::endl;

		/// check index order
		geo::Point2f sPt = photoPts[0];
		geo::Point2f ePt = photoPts[photoPts.size() - 1];
		const float dx = ePt.x - sPt.x;
		const float dy = ePt.y - sPt.y;
		const double dist = sqrt(dx * dx + dy * dy);

		double x0_ = cos(kappa) * sPt.x + sin(kappa) * sPt.y;
		double y0_ = -sin(kappa) * sPt.x + cos(kappa) * sPt.y;

		double x1_ = cos(kappa) * ePt.x + sin(kappa) * ePt.y;
		double y1_ = -sin(kappa) * ePt.x + cos(kappa) * ePt.y;

		if (false == (x1_ < x0_ && y1_ < y0_))
		{
			reverse(photoPts);
		}

		/// Scale
		const double sf = refDiagDist * 1000.0 / dist;///mm
		const double Z0 = sf * imgInfo.f * 0.001;
		std::cout << "Approximate scale: sf" << std::endl;
		std::cout << "Approaixmate dist photo to object: " << Z0 << std::endl;
		double rotatedTarCenterX = (cos(kappa) * tarCenter.x + sin(kappa) * tarCenter.y) * sf * 0.001;///meter
		double rotatedTarCenterY = (-sin(kappa) * tarCenter.x + cos(kappa) * tarCenter.y) * sf * 0.001;///meter
		std::string eopxyz = std::to_string(rotatedTarCenterX) + " " + std::to_string(rotatedTarCenterY) + " " + std::to_string(Z0);
		std::cout << "Inintial PC: " << eopxyz << std::endl;

		phoFile << "\t" << _image_s1 << imgInfo.imgPath << _image_s2 << std::endl;
		phoFile << "\t\t" << _image_id_s << imgInfo.imgId << _image_id_e << std::endl;
		phoFile << "\t\t" << _camera_id_s << imgInfo.cameraId << _camera_id_e << " " << std::endl;
		phoFile << "\t\t" << _time_s << time << _time_e << std::endl;
		phoFile << "\t\t" << _eop_s << std::endl;
		phoFile << "\t\t\t" << _eopxyz_s << std::endl;
		phoFile << "\t\t\t\t" << _value_s << eopxyz << _value_e << std::endl;
		phoFile << "\t\t\t\t" << _variance_s << std::endl;
		phoFile << "\t\t\t\t\t" << "1.0e+12 0.0 0.0" << std::endl;
		phoFile << "\t\t\t\t\t" << "0.0 1.0e+12 0.0" << std::endl;
		phoFile << "\t\t\t\t\t" << "0.0 0.0 1.0e+12" << std::endl;
		phoFile << "\t\t\t\t" << _variance_e << std::endl;
		phoFile << "\t\t\t" << _eopxyz_e << std::endl;
		phoFile << "\t\t\t" << _opk_s << std::endl;
		phoFile << "\t\t\t\t" << _value_s << " 0.0 0.0 " << std::to_string(util::Rad2Deg(kappa)) << " " << _value_e << std::endl;
		phoFile << "\t\t\t\t" << _variance_s << std::endl;
		phoFile << "\t\t\t\t\t" << "1.0e+12 0.0 0.0" << std::endl;
		phoFile << "\t\t\t\t\t" << "0.0 1.0e+12 0.0" << std::endl;
		phoFile << "\t\t\t\t\t" << "0.0 0.0 1.0e+12" << std::endl;
		phoFile << "\t\t\t\t" << _variance_e << std::endl;
		phoFile << "\t\t\t" << _opk_e << std::endl;
		phoFile << "\t\t" << _eop_e << std::endl;
		phoFile << "\t\t" << _points_s << std::endl;

		for (unsigned int i = 0; i < photoPts.size(); ++i)
		{
			phoFile << "\t\t\t";
			phoFile << _point_s;
			phoFile << _point_id_s << " " << i << " " << _point_id_e;
			phoFile << _coordinates_s << " " << photoPts[i].x << " " << photoPts[i].y << " " << _coordinates_e;
			phoFile << _variance_s << " 1.0 0.0 0.0 1.0 " << _variance_e;
			phoFile << _point_e << std::endl;
		}

		phoFile << "\t\t" << _points_e << std::endl;
		phoFile << "\t" << _image_e << std::endl;
	}

	void CalibrationData::closePhoFile()
	{
		phoFile << _image_observation_e << std::endl;
		phoFile.close();
	}

	std::string calPrjWriter(const std::string& folderPath, const std::string& phopath, const std::string& objpath, const std::string& campath)
	{
		std::string prjPath = folderPath + std::string("\\calibration.prj");

		std::fstream prjFile;
		prjFile.open(prjPath, std::ios::out);
		if (!prjFile)
		{
			std::cout << "Error in creating a project file." << std::endl;
			return std::string("");
		}

		prjFile << _xmlHeader << std::endl;
		prjFile << std::endl;
		prjFile << "<project>" << std::endl;
		prjFile << "\t" << "<name> " << folderPath << " calibration project </name>" << std::endl;
		prjFile << "\t" << "<type> bundle_block </type>" << std::endl;
		prjFile << "\t" << "<config>" << std::endl;
		prjFile << "\t\t" << "<min_sigma> 1.0e-6 </min_sigma>" << std::endl;
		prjFile << "\t\t" << "<max_sigma> 1.0e+6 </max_sigma>" << std::endl;
		prjFile << "\t\t" << "<min_correlation> 0.95 </min_correlation>" << std::endl;
		prjFile << "\t" << "</config>" << std::endl;
		prjFile << "\t" << "<iteration>" << std::endl;
		prjFile << "\t\t" << "<max_iteration> 30 </max_iteration>" << std::endl;
		prjFile << "\t\t" << "<sigma_threshold>1.0e-9</sigma_threshold>" << std::endl;
		prjFile << "\t" << "</iteration>" << std::endl;
		prjFile << "\t" << "<datapath>" << std::endl;
		prjFile << "\t\t" << "<pho_path>" << phopath << "</pho_path>" << std::endl;
		prjFile << "\t\t" << "<obj_path>" << objpath << "</obj_path>" << std::endl;
		prjFile << "\t\t" << "<cam_path>" << campath << "</cam_path>" << std::endl;
		prjFile << "\t" << "</datapath>" << std::endl;
		prjFile << "</project>" << std::endl;

		prjFile.close();

		return prjPath;
	}

	bool exportFoundTargets(const std::vector<geo::Point2f>& targets, const std::string& outPath)
	{
		std::fstream f;
		f.open(outPath, std::ios::out);
		if (!f)
			return false;

		f << "<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>" << std::endl;
		f << "<points>" << std::endl;
		int index = 0;
		for (const auto& p : targets)
		{
			f << "\t";
			f << "<point>\t<point_id>\t" << index << "\t</point_id>\t<coordinates>\t" << p.x << "\t" << p.y << "\t</coordinates>\t</point>\t" << std::endl;

			++index;
		}
		f << "</points>" << std::endl;

		f.close();

		return true;
	}
}