/*
* Copyright(c) 2019-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

/**
Space-intersection linear model
For ith image observation
[X,Y,Z]^T = [Xi,Yi,Zi]^T + Si*Ri*[xi,yi,-fi]^T
where, [X,Y,Z]^T denote object point coordinates,
[Xi,Yi,Zi]^T

Assume that there are two image, stereo;
[X,Y,Z]^T = [X1,Y1,Z1]^T + S1*R1*[x1,y1,-f1]^T
[X,Y,Z]^T = [X2,Y2,Z2]^T + S2*R2*[x2,y2,-f2]^T

[X2-X1,Y2-Y1,Z2-Z1]^T = S1*R1*[x1,y1,-f1]^T - S2*R2*[x2,y2,-f2]^T
[X2-X1,Y2-Y1,Z2-Z1]^T = [r11_1*x1+r12_1*y1-r13_1*f1 -r11_2*x2-r12_2*y2+r13_2*f2;
						 r21_1*x1+r22_1*y1-r23_1*f1 -r21_2*x2-r22_2*y2+r23_2*f2;
						 r31_1*x1+r32_1*y1-r33_1*f1 -r31_2*x2-r32_2*y2+r33_2*f2] [S1 S2]^T
There are two unknowns, S1 and S2, and 3 equations. It is a linear form.
(If there is only one camera, then f1 == f2)

General case: n images
[X,Y,Z] = [X1,Y1,Z1] + S1*R1*[x1,y1,-f1]
[X,Y,Z] = [X2,Y2,Z2] + S2*R2*[x2,y2,-f2]
.
.
.
[X,Y,Z] = [Xn,Yn,Zn] + Sn*Rn*[xn,yn,-fn]


[X1 Y1 Z1 X2 Y2 Z2 ... Xn Yn Zn]^T = [1 0 0 -r11_1*x1-r12_1*y1+r13_1*f1 ...;
									  0 1 0 -r21_1*x1-r22_1*y1+r23_1*f1 ...;
									  0 0 1 -r31_1*x1-r32_1*y1+r33_1*f1 ...;
									  1 0 0 -r11_2*x2-r12_2*y2+r13_2*f2 ...;
									  0 1 0 -r21_2*x2-r22_2*y2+r23_2*f2 ...;
									  0 0 1 -r31_2*x2-r32_2*y2+r33_2*f2 ...;
									  ...
									  1 0 0 -r11_n*xn-r12_n*yn+r13_n*fn ...;
									  0 1 0 -r21_n*xn-r22_n*yn+r23_n*fn ...;
									  0 0 1 -r31_n*xn-r32_n*yn+r33_n*fn ...] [X Y Z S1 S2 ... Sn]
There are 3 + n unknowns and 3*n equations.

**/
#include <assert.h>
#include <iostream>

#include <ssm/include/SSMIntersectionLinear.h>
#include <ssm/include/SSMMatrix.h>
#include <ssm/include/SSMRotation.h>

namespace ssm
{
	data::Point3D runLinearIntersectionStereo(const data::PhotoData& photoLeft, const data::PhotoData& photoRight)
	{
		const unsigned int numUnknowns = 2;
		const unsigned int numEquations = 3;

		math::Matrix<double> A(numEquations, numUnknowns, 0.0);
		math::Matrix<double> L(numEquations, 1, 0.0);

		math::Matrix<double> ptL(3, 1, 0.0);
		math::Matrix<double> ptR(3, 1, 0.0);

		try
		{
			// refine the photo coordinates
			const auto refiendPtL = photoLeft.getCam()->getRefinedPhotoCoord(photoLeft.getPts()[0].coord);
			const auto refiendPtR = photoRight.getCam()->getRefinedPhotoCoord(photoRight.getPts()[0].coord);

			ptL(0, 0) = refiendPtL(0);
			ptL(1, 0) = refiendPtL(1);
			ptL(2, 0) = -photoLeft.getCam()->getFL();

			ptR(0, 0) = refiendPtR(0);
			ptR(1, 0) = refiendPtR(1);
			ptR(2, 0) = -photoRight.getCam()->getFL();
		}
		catch (...)
		{
			throw std::runtime_error("Error in <runLinearIntersectionStereo> when getting refined photo coordinates.");
		}

		double omg, phi, kap;
		photoLeft.getEop()->getEulerAngles(omg, phi, kap);
		const auto& rmat = math::rotation::getRMat(omg, phi, kap);

		const auto lRPt = rmat % ptL;
		const auto rRPt = rmat % ptR;

		A(0, 0) = lRPt(0, 0);
		A(0, 1) = -rRPt(0, 0);

		A(1, 0) = lRPt(1, 0);
		A(1, 1) = -rRPt(1, 0);

		A(2, 0) = lRPt(2, 0);
		A(2, 1) = -rRPt(2, 0);

		L(0, 0) = photoRight.getEop()->getPC()(0) - photoLeft.getEop()->getPC()(0);
		L(1, 0) = photoRight.getEop()->getPC()(1) - photoLeft.getEop()->getPC()(1);
		L(2, 0) = photoRight.getEop()->getPC()(2) - photoLeft.getEop()->getPC()(2);

		math::Matrix<double> AT = A.transpose();

		/// Scale factor derived from both images
		math::Matrix<double> X;

		try
		{
			auto N = AT % A;
			auto C = AT % L;
			auto Ninv = N.inverse();
			X = Ninv % C;

#ifdef _DEBUG
			std::cout << math::matrixout(N);
			std::cout << math::matrixout(C);
#endif
		}
		catch (...)
		{
			throw std::runtime_error("Error in <runLinearIntersectionStereo> when computing X = (AT*A).inverse()*AT*L.");
		}

		/// two object points calculated by estimated two scale factors
		const auto objPtL = lRPt * X(0, 0) + photoLeft.getEop()->getPC();
		const auto objPtR = rRPt * X(1, 0) + photoRight.getEop()->getPC();

		/// final object point coordinates as a mean of two points with an assumption both images have same precision.
		data::Point3D retVal = (objPtL + objPtR) / 2.0;

		return retVal;
	}

	data::Point3D runLinearIntersectionMultiNormalMat(const std::vector<data::PhotoData>& photos)
	{
		if (photos.size() < 2)
			throw std::runtime_error("Error in runLinearIntersectionMultiNormalMat: number of photos is less than 2.");

		const unsigned int numUnknowns = static_cast<unsigned int>(photos.size() + 3);

		math::Matrix<double> N(numUnknowns, numUnknowns, 0.0);
		math::Matrix<double> C(numUnknowns, 1, 0.0);

		for (unsigned int p = 0; p < photos.size(); ++p)
		{
			const data::PhotoData& photo = photos[p];

			math::Matrix<double> a(3, numUnknowns, 0.0);
			math::Matrix<double> l(3, 1, 0.0);

			// refine the photo coordinates
			data::Point2D pt = photo.getCam()->getRefinedPhotoCoord(photo.getPts()[0].coord);

			double omg, phi, kap;
			photo.getEop()->getEulerAngles(omg, phi, kap);
			const auto& rMat = math::rotation::getRMat(omg, phi, kap);

			a(0, 0) = 1.0; a(0, 3 + p) = -rMat(0, 0)*pt(0) - rMat(0, 1)*pt(1) + rMat(0, 2)*photo.getCam()->getFL();
			a(1, 1) = 1.0; a(1, 3 + p) = -rMat(1, 0)*pt(0) - rMat(1, 1)*pt(1) + rMat(1, 2)*photo.getCam()->getFL();
			a(2, 2) = 1.0; a(2, 3 + p) = -rMat(2, 0)*pt(0) - rMat(2, 1)*pt(1) + rMat(2, 2)*photo.getCam()->getFL();

			const auto& pc = photo.getEop()->getPC();

			l(0, 0) = pc(0);
			l(1, 0) = pc(1);
			l(2, 0) = pc(2);

			math::Matrix<double> n = a.transpose()%a;
			math::Matrix<double> c = a.transpose()%l;

			N += n;
			C += c;
		}

		const auto X = N.inverse() % C;

		data::Point3D retVal = X.getSubset(0, 0, 3, 1);

		return retVal;
	}

	data::Point3D runLinearIntersectionMultiNormalMat(std::vector<std::shared_ptr<data::PhotoData>> photos)
	{
		if (photos.size() < 2)
			throw std::runtime_error("Error in runLinearIntersectionMultiNormalMat: number of photos is less than 2.");

		const unsigned int numUnknowns = static_cast<unsigned int>(photos.size() + 3);

		math::Matrix<double> N(numUnknowns, numUnknowns, 0.0);
		math::Matrix<double> C(numUnknowns, 1, 0.0);

		for (unsigned int p = 0; p < photos.size(); ++p)
		{
			math::Matrix<double> a(3, numUnknowns, 0.0);
			math::Matrix<double> l(3, 1, 0.0);

			// refine the photo coordinates
			std::shared_ptr<data::PhotoData> photo(photos[p]);
			const std::vector<data::ImagePointParam>& pts = photo->getPts();
			std::shared_ptr<sensor::Camera> cam = photo->getCam();
			std::shared_ptr<data::EOP> eop = photo->getEop();
			data::Point2D pt = cam->getRefinedPhotoCoord(pts[0].coord);

			double omg, phi, kap;
			eop->getEulerAngles(omg, phi, kap);
			const auto& rMat = math::rotation::getRMat(omg, phi, kap);

			a(0, 0) = 1.0; a(0, 3 + p) = -rMat(0, 0) * pt(0) - rMat(0, 1) * pt(1) + rMat(0, 2) * cam->getFL();
			a(1, 1) = 1.0; a(1, 3 + p) = -rMat(1, 0) * pt(0) - rMat(1, 1) * pt(1) + rMat(1, 2) * cam->getFL();
			a(2, 2) = 1.0; a(2, 3 + p) = -rMat(2, 0) * pt(0) - rMat(2, 1) * pt(1) + rMat(2, 2) * cam->getFL();

			const auto& pc = eop->getPC();

			l(0, 0) = pc(0);
			l(1, 0) = pc(1);
			l(2, 0) = pc(2);

			math::Matrix<double> n = a.transpose() % a;
			math::Matrix<double> c = a.transpose() % l;

			N += n;
			C += c;
		}

		auto Ninv = N.inverse();

#ifdef _DEBUG
		std::cout << math::matrixout(N);
		std::cout << math::matrixout(C);
#endif

		const auto X = N.inverse() % C;

		data::Point3D retVal = X.getSubset(0, 0, 3, 1);

		return retVal;
	}
}