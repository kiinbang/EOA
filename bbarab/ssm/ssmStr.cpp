/**
* @file		ssmStr.cpp
* @date		since 2000
* @author	kiinbang
* @brief	implementation of string class exported in dll
*/

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include <ssm/include/ssmStr.h>

namespace util
{
	/**constructor*/
	SsmStr::SsmStr(const SsmStr& str)
	{
		if (str == 0)
		{
			m_nLength = 0;
			m_pString = nullptr;
		}
		else
		{
			m_nLength = str.m_nLength;
			const int buffLength = m_nLength + 1;
			m_pString = new char[static_cast<long long>(buffLength)];
			assert(m_pString != nullptr);
			strcpy_s(m_pString, buffLength, str.m_pString);
		}
	}

	/**constructor*/
	SsmStr::SsmStr(const char* str)
	{
		if (str == nullptr)
		{
			m_nLength = 0;
			m_pString = nullptr;
		}
		else
		{
			m_nLength = static_cast<int>(strlen(str));
			const int buffLength = m_nLength + 1;
			m_pString = new char[buffLength];
			assert(m_pString != nullptr);
			strcpy_s(m_pString, buffLength, str);
		}
	}

	/**constructor*/
	SsmStr::SsmStr() 
	{ 
		m_nLength = 0; 
		m_pString = nullptr; 
	}

	/**destructor*/
	SsmStr::~SsmStr() 
	{ 
		if (m_pString) delete m_pString; 
	}

	// operator overloading helper
	/**operator overloading*/
	SsmStr SsmStr::operator +(const SsmStr& str) const
	{
		if (!str.m_pString) return *this;
		if (!this->m_pString) return str;

		SsmStr newStr;
		newStr.m_nLength = static_cast<int>(strlen(this->m_pString) + strlen(str.m_pString));
		const int newBuffLength = newStr.m_nLength + 1;
		newStr.m_pString = new char[newBuffLength];
		const int buffLength = this->m_nLength + 1;
		strcpy_s(newStr.m_pString, buffLength, this->m_pString);
		strcat_s(newStr.m_pString, newBuffLength, str.m_pString);
		
		return newStr;
	}

	/**operator overloading*/
	void SsmStr::operator =(const SsmStr& str)
	{
		if (!str.m_pString) return;

		if (m_pString) delete m_pString;
		m_nLength = static_cast<int>(strlen(str.m_pString));
		const int buffLength = m_nLength + 1;
		m_pString = new char[buffLength];
		assert(m_pString != nullptr);
		strcpy_s(m_pString, buffLength, str.m_pString);
	}

	/**operator overloading*/
	void SsmStr::operator =(const char* str)
	{
		if(false == (str != 0)) return;

		if (m_pString) delete m_pString;
		m_nLength = static_cast<int>(strlen(str));
		const int buffLength = m_nLength + 1;
		m_pString = new char[buffLength];
		assert(m_pString != nullptr);
		strcpy_s(m_pString, buffLength, str);
	}

	void SsmStr::operator +=(const char* str) 
	{ 
		if (this->m_nLength < 1)
			*this = str;
		else
			*this += SsmStr(str);
	}

	void SsmStr::operator +=(const SsmStr& str)
	{
		SsmStr newStr = *this + str;
		*this = newStr;
	}

	// add more logic comparison operators as following, for example, although not efficient
	bool SsmStr::operator !=(const SsmStr& str) 
	{ 
		return (!operator == (str));
	}

	bool SsmStr::operator ==(const SsmStr& str) 
	{ 
		if (!this->m_pString && !str.m_pString)
			return true;
		else if (!this->m_pString || !str.m_pString)
			return false;
		else
		{
			return strcmp(str.m_pString, m_pString) == 0;
		}
	}

	// c type string conversion
	SsmStr::operator char* () 
	{ 
		return m_pString; 
	}

	SsmStr::operator const char* ()	const 
	{ 
		return m_pString; 
	}

	char* SsmStr::GetChar() 
	{ 
		return m_pString; 
	}

	int SsmStr::GetLength() 
	{ 
		return m_nLength; 
	}

	// format string
	int SsmStr::Format(const char* format, ...)
	{
		assert(format != 0);

		int buffSize;
		int len;
		char* MaxBuf;
		for (int i = 5; ; i++)
		{
			buffSize = (int)pow(2.0, i);
			MaxBuf = new char[buffSize];
			if (!MaxBuf) return 0;
			// some UNIX's do not support vsnprintf and snprintf
			len = _vsnprintf_s(MaxBuf, static_cast<size_t>(buffSize * sizeof(char)), _TRUNCATE, format, (char*)(&format + 1));
			if (len > 0) break;
			delete[]MaxBuf;
			if (len == 0) return 0;
		}

		if (!m_pString)
		{
			m_nLength = len;
			m_pString = new char[m_nLength + 1];
		}
		else if (m_nLength < len)
		{
			delete m_pString;
			m_nLength = len;
			m_pString = new char[m_nLength + 1];
		}
		if (m_pString)
			strcpy_s(m_pString, m_nLength + 1, MaxBuf);
		else
			len = 0;
		delete[]MaxBuf;

		return len;
	}
}