#pragma once

#include <cmath>
#include <limits>
#include <stdexcept>

#include <ssm/include/SSMMatrix.h>

namespace ssmprj
{
	/*const double minScale = 0.0001;
	const double maxScale = 100000.0;*/

	struct Params
	{
		double s;/// default 1.0
		double tx, ty; /// default 0.0 in pixels
		double p, r, h; /// default 0.0 in radians
	};

	/// 2D Projective transformation manipulated by 6 geometric parameters:
	/// two translations, three rotations and scale

	class PrjTrans
	{
	public:
		PrjTrans()
		{
			setDefault();
		}

		void setDefault()
		{
			m.resize(3, 3);
			m.makeIdentityMat();
			r = m;
			s = 1.0;
			x0 = 0.0;
			y0 = 0.0;
			x02 = x0 + s * m(0, 2) / m(2, 2);
			y02 = y0 + s * m(1, 2) / m(2, 2);
		}

		void updateCoefficients(const Params& newParams)
		{
			double* mData = this->m.getDataHandle();

			double cosp, sinp, cosr, sinr, cosh, sinh;

			cosp = cos(newParams.p);
			sinp = sin(newParams.p);
			cosr = cos(newParams.r);
			sinr = sin(newParams.r);
			cosh = cos(newParams.h);
			sinh = sin(newParams.h);

			mData[0] = cosr*cosh;
			mData[1] = sinp*sinr*cosh + cosp*sinh;
			mData[2] = -cosp*sinr*cosh + sinp*sinh;

			mData[3] = -cosr*sinh;
			mData[4] = -sinp*sinr*sinh + cosp*cosh;
			mData[5] = cosp*sinr*sinh + sinp*cosh;

			mData[6] = sinr;
			mData[7] = -sinp*cosr;
			mData[8] = cosp*cosr;

			r = m.transpose();

			this->s = newParams.s;

			/// Minimum scale
			/*if (this->s < 0.01) this->s = minScale;
			/// Maximum scale
			if (this->s > 1000.0) this->s = maxScale;*/

			x0 = newParams.tx;
			y0 = newParams.ty;

			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		void setdp(const double dp)
		{
			math::Matrix<double> mdp(3, 3);
			double* mData = mdp.getDataHandle();

			double cosp, sinp;

			cosp = cos(dp);
			sinp = sin(dp);

			mData[0] = 1.0;
			mData[1] = 0.0;
			mData[2] = 0.0;

			mData[3] = 0.0;
			mData[4] = cosp;
			mData[5] = sinp;

			mData[6] = 0.0;
			mData[7] = -sinp;
			mData[8] = cosp;

			m = mdp % m;
			r = m.transpose();

			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		void setdr(const double dr)
		{
			math::Matrix<double> mdr(3, 3);
			double* mData = mdr.getDataHandle();

			double cosr, sinr;

			cosr = cos(dr);
			sinr = sin(dr);
			
			mData[0] = cosr;
			mData[1] = 0.0;
			mData[2] = -sinr;

			mData[3] = 0.0;
			mData[4] = 1.0;
			mData[5] = 0.0;

			mData[6] = sinr;
			mData[7] = 0.0;
			mData[8] = cosr;

			m = mdr % m;
			r = m.transpose();

			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		void setdh(const double dh)
		{
			math::Matrix<double> mdh(3, 3);
			double* mData = mdh.getDataHandle();

			double cosh, sinh;

			cosh = cos(dh);
			sinh = sin(dh);

			mData[0] = cosh;
			mData[1] = sinh;
			mData[2] = 0.0;

			mData[3] = -sinh;
			mData[4] = cosh;
			mData[5] = 0.0;

			mData[6] = 0.0;
			mData[7] = 0.0;
			mData[8] = 1.0;

			m = mdh % m;
			r = m.transpose();

			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		void setdS(const double dS)
		{
			this->s *= dS;
			
			/// Minimum scale
		/*	if (this->s < 0.01) this->s = minScale;
			/// Maximum scale
			if (this->s > 1000.0) this->s = maxScale;*/

			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		void setdTx(const double dTx)
		{
			x0 += dTx;
			x02 += dTx;
		}

		void setdTy(const double dTy)
		{
			y0 += dTy;
			y02 += dTy;
		}


		void runTransformation(const double x, const double y, double& u, double& v) const
		{
			math::Matrix<double> p(3, 1, 0.0);
			p(0, 0) = x;
			p(1, 0) = y;
			p(2, 0) = -1.0; /// -f and f ios 1.0
			math::Matrix<double> q = r % p;

			u = q(0, 0) / q(2, 0) * -s + x02;
			v = q(1, 0) / q(2, 0) * -s + y02;
		}

		void runInverseTransformation(const double u, const double v, double& x, double& y) const
		{
			math::Matrix<double> p(3, 1, 0.0);
			p(0, 0) = u - x02;
			p(1, 0) = v - y02;
			p(2, 0) = -s;
			math::Matrix<double> q = m % p;
			x = -q(0, 0) / q(2, 0);
			y = -q(1, 0) / q(2, 0);
		}

	private:
		math::Matrix<double> m;
		math::Matrix<double> r;
		double s;
		double x0, y0;
		double x02, y02;
	};
}