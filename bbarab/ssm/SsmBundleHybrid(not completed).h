﻿/*
* Copyright(c) 2005-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/SSMConstants.h>
#include <ssm/include/SSMPhoto.h>
#include <ssm/ssmlist.h>

namespace ssm
{
	class BundleHybrid
	{
	public:
		BundleHybrid();

		bool runBundle(const std::shared_ptr<util::SsmList<std::shared_ptr<data::PhotoData>>> inputPhotos,
			const std::shared_ptr<util::SsmList<std::shared_ptr<data::ObjectPoint>>> inputObjPoints,
			const unsigned int maxIteration);

	private:
		/// Find available and unavailable points and add unknown object points to the list
		bool checkObservations();

		/// Check duplicated ids for points
		void checkDuplicatedImgPointIds();

		/// Check duplicated ids for photos
		void checkDuplicatedPhotoIds();

		/// Check duplicated ids for object points
		void checkDuplicatedObjPointIds();

		/// Check non-multiple observations and define object points
		bool checkNonMultipleObservationsAndDefineObjPts();

		/// Check lack of observation for each photo
		bool checkLackOfObservations();

		/// Check number of datum constraints
		bool checkDatumConstraints();

		/// Check approximate redundancy
		bool checkPseudoRedundancy();

		/// Find a camera using uuid
		static unsigned int find(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<sensor::Camera>>> cameras);

		/// Find an image point
		static unsigned int find(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<data::ImagePoint>>> imgPoints);

		/// Find an object point
		static unsigned int find(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<data::ObjectPoint>>> objPoints);

		/// Find an object point
		static unsigned int find(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<data::PhotoData>>> photos);

		/// Check coplanarity condition equations
		void checkStereoModels();

	private:
		/// Camera data
		std::shared_ptr<util::SsmList<std::shared_ptr<sensor::Camera>>> cameras;
		/// Photo data
		std::shared_ptr<util::SsmList<std::shared_ptr<data::PhotoData>>> photos;
		/// Object points
		std::shared_ptr<util::SsmList<std::shared_ptr<data::ObjectPoint>>> objPoints;
		/// Set availability for object points
		void setAvailability(const util::SsmUuid& id, const std::shared_ptr<util::SsmList<std::shared_ptr<data::ObjectPoint>>> objPts, const bool availability) const;
		/// Tie points of stereo piar
		util::SsmList<util::SsmList<data::Pair<std::shared_ptr<data::ImagePoint>, std::shared_ptr<data::ImagePoint>>>> modelTiePts;
		/// Tie point parameters
		util::SsmList<util::SsmList<std::shared_ptr<param::ParameterCollection>>> tieObsParams;
	};
}
