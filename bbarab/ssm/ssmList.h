/*
 * Copyright (c) since 2000, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <assert.h>

namespace util
{
	/**
	* @class	ListData
	* @data		renew: 2020
	* @brief	template class for list class data.
	*/
	template <typename T>
	class ListData
	{
	public:
		ListData()
		{
			forward = nullptr;
			next = nullptr;
			nID = -1;
			bAvailable = true;
			value = T();
		}

		ListData(ListData<T> &copy)
		{
			forward = copy.forward;
			next = copy.next;
			nID = copy.nID;
			bAvailable = copy.bAvailable;
			value = copy.value;
		}

		void operator = (ListData<T> &copy)
		{
			forward = copy.forward;
			next = copy.next;
			nID = copy.nID;
			bAvailable = copy.bAvailable;
			value = copy.value;
		}

		ListData<T> *forward;/**<forward address*/
		ListData<T> *next;/**<next address*/
		int nID;
		bool bAvailable;
		T value;/**<this place data*/
	};

	/**@class SsmList
	*@brief template class for list data[class ListData] management.
	*/
	template <typename T>
	class SsmList
	{
	public:
		/**constructor*/
		SsmList()
		{
			first = nullptr;
			last = nullptr;
			recent = nullptr;
			recent_sort = 0;
			m_nNumData = 0;
			ID_count = -1;
		}

		/**constructor*/
		SsmList(const unsigned int n)
		{
			first = nullptr;
			last = nullptr;
			recent = nullptr;
			recent_sort = 0;
			m_nNumData = 0;
			ID_count = -1;

			resize(n);
		}

		/**copy constructor*/
		SsmList(const SsmList<T>& copy)
		{
			m_nNumData = 0;
			first = nullptr;
			last = nullptr;
			recent = nullptr;
			recent_sort = 0;

			ListData<T> *pos = nullptr;
			for (unsigned long i = 0; i < copy.m_nNumData; i++)
			{
				//addTail() 함수를 이용한 복사
				//addTail(copy.GetAt(i));
				pos = copy.getNextPos(pos);
				T value = pos->value;
				addTail(value);
			}

			ID_count = copy.ID_count;
		}

		/**destructor*/
		~SsmList()
		{
			removeAll();
		}

		/**Set availability*/
		void setAvailability(const unsigned int sort, bool bAvailability)
		{
			ListData<T> *target = getDataHandle(sort);
			target->bAvailable = bAvailability;
		}

		/**Get availability*/
		bool getAvailability(const unsigned int sort) const
		{
			ListData<T>* target = getDataHandle(sort);
			return target->bAvailable;
		}

		/**Get availabliity*/
		const bool getAvailability(const unsigned int sort)
		{
			ListData<T> *target = getDataHandle(sort);
			return target->bAvailable;
		}

		/**Delete un-available data*/
		unsigned int deleteUnavailableData()
		{
			ListData<T> *pos = nullptr;

			unsigned int count = 0;

			pos = getFirstPos();
			while (pos != nullptr)
			{
				if (pos->bAvailable == false)
				{
					pos = deleteDatabyHandle(pos);
					count++;
				}
				else
				{
					pos = getNextPos(pos);
				}
			}

			return count;
		}

		/**Get number of items*/
		unsigned int getNumItem() const
		{
			return m_nNumData;
		}

		/**Get number of items*/
		unsigned int getSize() const
		{
			return getNumItem();
		}

		/**Get number of items*/
		unsigned int size() const
		{
			return m_nNumData;
		}

		/**Set void data*/
		void setVoidData(const unsigned int num)
		{
			T* data;
			data = new T;
			for (unsigned int i = 0; i < num; i++)
				addTail(*data);
		}
		/**fill the data with default value*/
		void setVoidData(const unsigned int num, const T data)
		{
			for (unsigned int i = 0; i < num; i++)
				addTail(data);
		}

		/**Add data to tail*/
		void addTail(const T data)
		{
			addTail();
			last->value = data;
		}

		/**Add data to tail*/
		void addTail()
		{
			if (m_nNumData == 0)//First data
			{
				last = new ListData<T>;
				first = last;
			}
			else
			{
				last->next = new ListData<T>;
				last->next->forward = last;
				last = last->next;
			}

			ID_count++;
			last->nID = ID_count;

			m_nNumData++;
		}

		/**Add data to head*/
		bool addHead(const T data)
		{
			if (m_nNumData == 0)//First data
			{
				first = new ListData<T>;
				last = first;
			}
			else
			{
				first->forward = new ListData<T>;
				first->forward->next = first;
				first = first->forward;
			}

			first->value = data;
			ID_count++;
			first->nID = ID_count;

			m_nNumData++;

			recent = nullptr;
			recent_sort = 0;

			return true;
		}

		/**Delete item by index which starts from #0*/
		bool deleteData(const unsigned int sort)
		{
			if (sort >= m_nNumData)
				return false;

			ListData<T>* target;
			target = getDataHandle(sort);

			if (target != nullptr)
			{
				if (nullptr == deleteDatabyHandle(target))
					return false;
				else
					return true;
			}
			else
				return false;
		}

		/**Delete item by index which starts from #0*/
		ListData<T>* deleteDatabyHandle(ListData<T> *target)
		{
			if (target != nullptr)
			{
				recent = nullptr;
				recent_sort = 0;

				if (target == first)//first data
				{
					first = target->next;
					delete target;
					target = nullptr;

					if (first != nullptr)
						first->forward = nullptr;
					m_nNumData--;
					return first;
				}
				else if (target == last)//last data
				{
					last = target->forward;
					delete target;
					target = nullptr;

					if (last != nullptr)
						last->next = nullptr;
					m_nNumData--;
					return last;
				}
				else
				{
					ListData<T> *Pre, *Pos;
					Pre = target->forward;
					Pos = target->next;
					delete target;
					target = nullptr;

					Pre->next = Pos;
					Pos->forward = Pre;
					m_nNumData--;
					return Pos;
				}

				recent = nullptr;
				recent_sort = 0;
			}
			else
				return nullptr;
		}

		/**Insert item by index which starts from #0*/
		bool insertData(const unsigned int sort, const T data)
		{
			if (sort > m_nNumData)
			{
				return false;
			}
			else if (sort == 0)
			{
				addHead(data);
				return true;
			}
			else if (sort == m_nNumData)
			{
				addTail(data);
				return true;
			}

			//forward link
			ListData<T>* target1;
			target1 = getDataHandle(sort - 1);

			//backward link
			ListData<T>* target2;
			target2 = getDataHandle(sort);

			//new link
			ListData<T> *newlink = new ListData<T>;
			newlink->value = data;

			//insert
			target1->next = newlink;
			newlink->forward = target1;

			target2->forward = newlink;
			newlink->next = target2;

			m_nNumData += 1;

			//recent = NULL;
			//recent_sort = 0;
			if (recent_sort >= sort)
				recent_sort++;

			return true;
		}

		/**Empty list*/
		void removeAll()
		{
			recent = nullptr;
			recent_sort = 0;

			ListData<T> *pos = first;

			while (pos != nullptr)
			{
				pos = deleteDatabyHandle(pos);

				if (pos == nullptr)
				{
					break;
				}

				assert(m_nNumData >= 0 && "m_nNumData < 0 in removeAll");
			}

			assert(m_nNumData == 0 && "m_nNumData not zero in removeAll");

			ID_count = -1;
		}

		/**Empty list*/
		void clear()
		{
			removeAll();
		}

		/**Resize list*/
		void resize(const unsigned int newSize)
		{
			removeAll();
			
			m_nNumData = 0;
			first = nullptr;
			last = nullptr;

			for (unsigned int i = 0; i < newSize; ++i)
				this->addTail();
		}

		/**Remove first data*/
		bool removeHead(void)
		{
			return deleteData(0);
		}

		/**Remove last data*/
		bool removeTail(void)
		{
			return deleteData(m_nNumData - 1);
		}

		/**Get specific data by index(sort)*/
		void setIDAt(const unsigned int sort, int ID) const
		{
			ListData<T>* target = getDataHandle(sort);
			target->ID = ID;
		}

		/**Get specific data by index(sort)*/
		int getIDAt(const unsigned int sort) const
		{
			ListData<T>* target = getDataHandle(sort);
			return target->ID;
		}

		T getAt(const unsigned int sort)
		{
			ListData<T>* target = getDataHandle(sort);
			return target->value;
		}

		/**Get the next post*/
		ListData<T>* getNextPos(const ListData<T>* pos) const
		{
			if (pos == nullptr)
			{
				return first;
			}
			else if (pos == last)
			{
				return nullptr;
			}
			else
			{
				ListData<T>* target;
				target = pos->next;
				return target;
			}
		}

		/**Get the first position*/
		ListData<T>* getFirstPos() const
		{
			return first;
		}

		/**Get specific handle by index(sort)*/
		T* getHandleAt(const unsigned int sort)
		{
			ListData<T>* target = getDataHandle(sort);
			return &(target->value);
		}

		/**Set specific data by index(sort)*/
		bool setAt(const unsigned int sort, const T newvalue)
		{
			ListData<T> *target = getDataHandle(sort);
			target->value = newvalue;
			return true;
		}

		/**[] operator overloading*/
		const T& operator [] (const unsigned int sort) const
		{
			const ListData<T> *target = getDataHandle(sort);
			return target->value;
		}

		/**[] operator overloading*/
		T& operator [] (const unsigned int sort)
		{
			ListData<T> *target = getDataHandle(sort);
			return target->value;
		}

		/**= operator overloading*/
		void operator=(const SsmList<T>& copy)
		{
			removeAll();
			m_nNumData = 0;
			first = nullptr;
			last = nullptr;

			ListData<T> *pos = nullptr;
			for (unsigned int i = 0; i < copy.m_nNumData; i++)
			{
				//addTail() 함수를 이용한 복사
				//addTail(copy.GetAt(i));
				pos = copy.getNextPos(pos);
				T value = pos->value;
				addTail(value);
			}
		}

		/**+= operator overloading*/
		void operator+=(const SsmList<T>& copy)
		{
			ListData<T> *pos = NULL;
			for (unsigned int i = 0; i < copy.m_nNumData; i++)
			{
				pos = copy.getNextPos(pos);
				T value = pos->value;

				addTail(value);
			}
		}

	private:
		/**Get ListData handle*/
		const ListData<T>* getDataHandle(const unsigned int sort) const
		{
			if (sort >= m_nNumData)
				return nullptr;

			ListData<T> *target;

			if (recent == nullptr)
				target = first;
			else
				target = recent;

			if ((int)sort > recent_sort)
			{
				for (unsigned int i = recent_sort; i < sort; i++)
				{
					target = target->next;
				}

			}
			else
			{
				for (unsigned int i = recent_sort; i > sort; i--)
				{
					target = target->forward;
				}

			}

			return target;
		}

		ListData<T>* getDataHandle(const unsigned int sort)
		{
			if (sort >= m_nNumData)
				return nullptr;

			ListData<T> *target;

			if (recent == nullptr)
				target = first;
			else
				target = recent;

			if ((int)sort > recent_sort)
			{
				for (unsigned int i = recent_sort; i < sort; i++)
				{
					target = target->next;
				}

			}
			else
			{
				for (unsigned int i = recent_sort; i > sort; i--)
				{
					target = target->forward;
				}

			}

			recent = target;//update recent data address
			recent_sort = sort;

			return target;
		}

	private:
		//member variable
		ListData<T> *first;/**<pointer of first data*/
		ListData<T> *last; /**<pointer of last data*/
		unsigned int m_nNumData; /**<number of data(elements)*/
		int ID_count;
		ListData<T> *recent;
		unsigned int recent_sort;
	};
}
