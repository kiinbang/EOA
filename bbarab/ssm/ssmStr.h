/**
* @file		ssmStr.h
* @date		since 2000
* @author	kiinbang
* @brief	string class exported in dll
*/

#pragma once

namespace util
{
	/**
	* @class	SsmStr
	* @date		first version: since 2000
	* @date		renewer: 2020
	* @author	kiinbang
	* @brief	class for string manipulation<br>
	* It is useful instead of std::string for class interface export.
	*/
	//class __declspec (dllexport) SsmStr
	class SsmStr
	{
	public:
		/**constructor*/
		SsmStr(const SsmStr& str);

		/**constructor*/
		SsmStr(const char* str);

		/**constructor*/
		SsmStr();

		/**destructor*/
		~SsmStr();

		// operator overloading helper
		/**operator overloading*/
		SsmStr operator +(const SsmStr& str) const;
		
		/**operator overloading*/
		void operator =(const SsmStr& str);
		
		/**operator overloading*/
		void operator =(const char* str);

		void operator +=(const char* str);
		
		void operator +=(const SsmStr& str);

		// add more logic comparison operators as following, for example, although not efficient
		bool operator !=(const SsmStr& str);

		bool operator ==(const SsmStr& str);

		// c type string conversion
		operator char* ();
		
		operator const char* ()	const;
		
		char* GetChar();
		char* getChar() { return GetChar(); }
		
		int GetLength();
		int getLength() { return GetLength(); }

		// format string
		int Format(const char* format, ...);

	protected:
		// data block
		int m_nLength;
		char* m_pString;
	};
}