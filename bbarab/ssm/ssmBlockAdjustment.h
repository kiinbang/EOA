/*
* Copyright(c) 2005-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssmBlockAdjustMatrixType.h>

#define scui static_cast<unsigned int>

namespace ssm
{
	///<sections[5]: Iop, Eop, Obj pt(3), Obj line(6), Obj triangle patch(9)
	const unsigned int numSections = 5;

	/**
	*@class BlockAdjustment
	*@remarks Note that  this is for the case including iops, eops, points, lines represented two points and planar patches represented three points,
	*\and it corresponds to the collinearity equations.<br>
	*\For the use of other features, unknown parameters and models (such as a coplanarity condition), it should be modified and add additional functions.
	*/
	//class __declspec (dllexport) BlockAdjustment
	class BlockAdjustment
	{
	public:
		/**constructor*/
		BlockAdjustment::BlockAdjustment()
		{
			init();
		}

		/**initialized the object*/
		void BlockAdjustment::init()
		{
			PosVar = 0.0;
			numEq = 0;
			numUnknown = 0;
		}

		/**Get correlation matrix from variance-covariance matrix*/
		math::Matrix<double> BlockAdjustment::getCorrelation(const math::Matrix<double>& VarCov)
		{
			unsigned int i, j;
			//Correlation of Unknown Matrix
			math::Matrix<double> Correlation;
			Correlation.resize(VarCov.getRows(), VarCov.getCols(), 0.);

			for (i = 0; i < Correlation.getRows(); i++)
			{
				double sigma_i, sigma_j;
				sigma_i = sqrt(VarCov(i, i));

				for (j = i; j < Correlation.getCols(); j++)
				{
					sigma_j = sqrt(VarCov(j, j));
					Correlation(i, j) = VarCov(i, j) / sigma_i / sigma_j;
				}
			}
			return Correlation;
		}

		/**Extract variance(diagonal element) matrix from variance-covariance matrix*/
		math::Matrix<double> BlockAdjustment::extractVar(const math::Matrix<double>& VarCov)
		{
			unsigned int i;
			//Correlation of Unknown Matrix
			math::Matrix<double> Variance(VarCov.getRows(), 1, 0.);

			for (i = 0; i < Variance.getRows(); i++)
			{
				Variance(i, 0) = VarCov(i, i);
			}
			return Variance;
		}

		/**GetErrorList*/
		math::Matrix<double> BlockAdjustment::getErrorList() const
		{
			math::Matrix<double> retval;

			unsigned int num_mat = static_cast<int>(Error.size());

			//DATA<MatrixData> *pos = NULL;
			int count = 0;
			for (unsigned int i = 0; i < num_mat; i++)
			{
				count += Error[i].getRows();
			}

			retval.resize(count, 1);

			int index = 0;
			for (unsigned int i = 0; i < num_mat; i++)
			{
				int nrows = Error[i].getRows();
				for (int j = 0; j < nrows; j++)
				{
					retval(index, 0) = Error[i](j, 0);
					index++;
				}
			}

			return retval;
		}

		/**LS for bundle using reduced normal matrix<br>
		*EO parameters, point data and straight line data.<br>
		*Note that  this is for the case including iops, eops, points, lines represented two points and planar patches represented three points,<br>
		*and it corresponds to the collinearity equations.<br>
		*For the use of other features, unknown parameters and models (such as a coplanarity condition), it should be modified and add additional functions.<br>
		*/
		void BlockAdjustment::runLeastSquareRN(double& var, math::Matrix<double>& N_dot, math::Matrix<double>& C_dot)
		{
			/// N_dot
			N_dot = make_N_dot();

			/// N_bar
			const unsigned int NbarRows1 = numParameters[scui(SectIdx::IOP)] * numSets[scui(SectIdx::IOP)];
			const unsigned int NbarRows2 = numParameters[scui(SectIdx::EOP)] * numSets[scui(SectIdx::EOP)];
			const unsigned int NbarRows = NbarRows1 + NbarRows2;
			const unsigned int NbarCols1 = numParameters[scui(SectIdx::PT)] * numSets[scui(SectIdx::PT)];
			const unsigned int NbarCols2 = numParameters[scui(SectIdx::LINE)] * numSets[scui(SectIdx::LINE)];
			const unsigned int NbarCols3 = numParameters[scui(SectIdx::TRI)] * numSets[scui(SectIdx::TRI)];
			const unsigned int NbarCols = NbarCols1 + NbarCols2 + NbarCols3;
			math::Matrix<double> N_bar(NbarRows, NbarCols, 0.0);

			if (numSets[scui(SectIdx::IOP)] > 0)
			{
				if (numSets[scui(SectIdx::PT)] > 0) N_bar.insert(0, 0, getNbar(SectIdx::IOP, SectIdx::PT));
				if (numSets[scui(SectIdx::LINE)] > 0) N_bar.insert(0, NbarCols1, getNbar(SectIdx::IOP, SectIdx::LINE));
				if (numSets[scui(SectIdx::TRI)] > 0) N_bar.insert(0, NbarCols2, getNbar(SectIdx::IOP, SectIdx::TRI));
			}

			if (numSets[scui(SectIdx::EOP)] > 0)
			{
				if (numSets[scui(SectIdx::PT)] > 0) N_bar.insert(NbarRows1, 0, getNbar(SectIdx::EOP, SectIdx::PT));
				if (numSets[scui(SectIdx::LINE)] > 0)N_bar.insert(NbarRows1, NbarCols1, getNbar(SectIdx::EOP, SectIdx::LINE));
				if (numSets[scui(SectIdx::TRI)] > 0)N_bar.insert(NbarRows1, NbarCols2, getNbar(SectIdx::EOP, SectIdx::TRI));
			}
		}

	private:
		/**Make N_dot matrix from N_dot elements list*/
		math::Matrix<double> BlockAdjustment::make_N_dot() const
		{
			/// Note:
			/// It is for bundle adjustment based on collinearity.
			/// For the use of coplanarity condition, it need to be modified, 
			/// especially, for N dot matrix part because two images (EOP sets) corresponded in the same coplanarity condition equation.
			/// So off-diagonal parts of Ndot-EOP matrix are not completely empty.

			unsigned int sizeMatIOP = numParameters[scui(SectIdx::IOP)] * numSets[scui(SectIdx::IOP)];
			unsigned int sizeMatEOP = numParameters[scui(SectIdx::EOP)] * numSets[scui(SectIdx::EOP)];

			math::Matrix<double> retmat(sizeMatIOP + sizeMatEOP, sizeMatIOP + sizeMatEOP, 0.0);

			/// Keep this order: IOP - EOP
			retmat.insert(0, 0, getNdot(SectIdx::IOP));
			retmat.insert(sizeMatIOP, sizeMatIOP, getNdot(SectIdx::EOP));
			auto Nbar = getNbar(SectIdx::IOP, SectIdx::EOP);
			retmat.insert(0, sizeMatIOP, Nbar);
			retmat.insert(sizeMatIOP, 0, Nbar.transpose());

			return retmat;
		}

		/**get Ndot matrix for each section*/
		math::Matrix<double> BlockAdjustment::getNdot(const SectIdx idx_) const
		{
			const unsigned int idx = scui(idx_);

			unsigned int matSize = numParameters[idx] * numSets[idx];
			math::Matrix<double> retmat(matSize, matSize, 0.0);

			const NdotMat& ndot = NdotList[idx];

			for (unsigned int i = 0; i < numSets[idx]; ++i)
			{
				retmat.insert(i * numParameters[idx], i * numParameters[idx], ndot.readOnly()[i]);
			}

			return retmat;
		}

		/**get Nbar matrix for each off-diagonal section*/
		math::Matrix<double> BlockAdjustment::getNbar(const SectIdx idx1_, const SectIdx idx2_) const
		{
			const unsigned int idx1 = scui(idx1_);
			const unsigned int idx2 = scui(idx2_);

			const NbarMat& NbarSection = NbarList[idx1][idx2];

			math::Matrix<double> retmat(numParameters[idx1] * numSets[idx1], numParameters[idx2] * numSets[idx2], 0.0);

			for (unsigned int i = 0; i < numSets[idx1]; ++i)
			{
				for (unsigned int j = 0; j < numSets[idx2]; ++j)
				{
					const auto& Nbarij = NbarSection.readOnly()[i][j];
					/// Need to check it empty because all elements were initialized as a zero-size matrix
					if (Nbarij.getRows() > 0) /// not empty
					{
						retmat.insert(i * numParameters[idx1], j * numParameters[idx2], Nbarij);
					}
				}
			}

			return retmat;
		}

	private:
		util::SsmList<math::Matrix<double>> Error;
		double PosVar;/**<posteriori variance*/
		unsigned int numUnknown;/**<number of all unknown parameters*/
		unsigned int numEq;/**<number of equations and constraints*/
		util::SsmList<unsigned int> numParameters;/**<number of parameters for each section*/
		util::SsmList<unsigned int> numSets;/**<number of input datasets for each section*/
		util::SsmList<NdotMat> NdotList; ///< size is numSections.
		util::SsmList<util::SsmList<NbarMat>> NbarList; ///< size is numSections X numSections, where diagonal parts and some of off-diagonal parts are empty
		util::SsmList<CdotMat> CdotList;
	};
}