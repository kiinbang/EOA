#pragma once

#ifdef _DEBUG
#include <iostream>
#endif

#include <cmath>
#include <limits>
#include <stdexcept>
#include <vector>

#include <ssm/SSMMatrix.h>

namespace math
{
#define scui static_cast<unsigned int>

	const double pi = acos(-1.0);

	const double minScale = 0.01;
	const double maxScale = 1000.0;
	const unsigned int numParams = 6;

	struct ProjectionParams
	{
		double s;/// default 1.0
		double tx, ty; /// default 0.0 in pixels
		double p, r, h; /// default 0.0 in radians
	};

	/// 2D Projective transformation manipulated by 6 geometric parameters:
	/// two translations, three rotations and scale

	class Projection
	{
	public:
		Projection()
		{
			setDefault();
		}

		/// Set default parameters
		void setDefault()
		{
			m.resize(3, 3);
			m.makeIdentityMat();
			r = m;
			s = 1.0;
			x0 = 0.0;
			y0 = 0.0;
			x02 = x0 + s * m(0, 2) / m(2, 2);
			y02 = y0 + s * m(1, 2) / m(2, 2);
		}

		/// Set new parameters
		void updateCoefficients(const ProjectionParams& newParams)
		{
			double* mData = this->m.getDataHandle();

			double cosp, sinp, cosr, sinr, cosh, sinh;

			cosp = cos(newParams.p);
			sinp = sin(newParams.p);
			cosr = cos(newParams.r);
			sinr = sin(newParams.r);
			cosh = cos(newParams.h);
			sinh = sin(newParams.h);

			mData[0] = cosr*cosh;
			mData[1] = sinp*sinr*cosh + cosp*sinh;
			mData[2] = -cosp*sinr*cosh + sinp*sinh;

			mData[3] = -cosr*sinh;
			mData[4] = -sinp*sinr*sinh + cosp*cosh;
			mData[5] = cosp*sinr*sinh + sinp*cosh;

			mData[6] = sinr;
			mData[7] = -sinp*cosr;
			mData[8] = cosp*cosr;

			r = m.transpose();

			this->s = newParams.s;

			/// Minimum scale
			if (this->s < 0.01) this->s = minScale;
			/// Maximum scale
			if (this->s > 1000.0) this->s = maxScale;

			x0 = newParams.tx;
			y0 = newParams.ty;

			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		/// Set the incremental of a pitch angle
		void setdp(const double dp)
		{
			math::Matrix<double> mdp(3, 3);
			double* mData = mdp.getDataHandle();

			double cosp, sinp;

			cosp = cos(dp);
			sinp = sin(dp);

			mData[0] = 1.0;
			mData[1] = 0.0;
			mData[2] = 0.0;

			mData[3] = 0.0;
			mData[4] = cosp;
			mData[5] = sinp;

			mData[6] = 0.0;
			mData[7] = -sinp;
			mData[8] = cosp;

			m = mdp % m;
			r = m.transpose();

			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		/// Set the incremental of a roll angle
		void setdr(const double dr)
		{
			math::Matrix<double> mdr(3, 3);
			double* mData = mdr.getDataHandle();

			double cosr, sinr;

			cosr = cos(dr);
			sinr = sin(dr);
			
			mData[0] = cosr;
			mData[1] = 0.0;
			mData[2] = -sinr;

			mData[3] = 0.0;
			mData[4] = 1.0;
			mData[5] = 0.0;

			mData[6] = sinr;
			mData[7] = 0.0;
			mData[8] = cosr;

			m = mdr % m;
			r = m.transpose();

			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		/// Set the incremental of a heading angle
		void setdh(const double dh)
		{
			math::Matrix<double> mdh(3, 3);
			double* mData = mdh.getDataHandle();

			double cosh, sinh;

			cosh = cos(dh);
			sinh = sin(dh);

			mData[0] = cosh;
			mData[1] = sinh;
			mData[2] = 0.0;

			mData[3] = -sinh;
			mData[4] = cosh;
			mData[5] = 0.0;

			mData[6] = 0.0;
			mData[7] = 0.0;
			mData[8] = 1.0;

			m = mdh % m;
			r = m.transpose();

			/// Note that 'mdh' does not change in r(0,2), r(1,2) and r(2,2),
			/// but 'mdh % m' may have changes there.
			/// Therefore, re-calculate x02 and y02
			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		/// Set the incremental of a scale
		void setdS(const double dS)
		{
			this->s *= dS;
			
			/// Minimum scale
			if (this->s < 0.01) this->s = minScale;
			/// Maximum scale
			if (this->s > 1000.0) this->s = maxScale;

			x02 = x0 + s * r(0, 2) / r(2, 2);
			y02 = y0 + s * r(1, 2) / r(2, 2);
		}

		/// Set the incremental of a shift-X
		void setdTx(const double dTx)
		{
			x0 += dTx;
			x02 += dTx;
		}

		/// Set the incremental of a shift-Y
		void setdTy(const double dTy)
		{
			y0 += dTy;
			y02 += dTy;
		}

		/// Run transformation from xy to uv
		void runTransformation(const double x, const double y, double& u, double& v) const
		{
			math::Matrix<double> p(3, 1, 0.0);
			p(0, 0) = x;
			p(1, 0) = y;
			p(2, 0) = -1.0; /// -f and f ios 1.0
			math::Matrix<double> q = r % p;

			u = q(0, 0) / q(2, 0) * -s + x02;
			v = q(1, 0) / q(2, 0) * -s + y02;
		}

		/// Run transformation from xy to uv after updating parameters
		void runTransformation(const ProjectionParams& params, double x, const double y, double& u, double& v)
		{
			updateCoefficients(params);

			math::Matrix<double> p(3, 1, 0.0);
			p(0, 0) = x;
			p(1, 0) = y;
			p(2, 0) = -1.0; /// -f and f ios 1.0
			math::Matrix<double> q = r % p;

			u = q(0, 0) / q(2, 0) * -s + x02;
			v = q(1, 0) / q(2, 0) * -s + y02;
		}

		/// Run inverse-transformation from uv to xy
		void runInverseTransformation(const double u, const double v, double& x, double& y) const
		{
			math::Matrix<double> p(3, 1, 0.0);
			p(0, 0) = u - x02;
			p(1, 0) = v - y02;
			p(2, 0) = -s;
			math::Matrix<double> q = m % p;
			x = -q(0, 0) / q(2, 0);
			y = -q(1, 0) / q(2, 0);
		}

		/// Estimate parameters using corresponding 2D point pairs
		bool estimateParameters(std::vector<std::pair<double, double>>& xy, const std::vector<std::pair<double, double>>& uv)
		{
			ProjectionParams initParams;

			if (xy.size() != uv.size())
				throw std::runtime_error("The sizes of xy and uv pairs are different.");

			if (xy.size() < 4)
				throw std::runtime_error("The number of xy and uv pairs is less than 4.");

			double sumScale = 0.0;
			double sumDx = 0.0;
			double sumDy = 0.0;
			unsigned int count = 0;

			for (unsigned int i = 1; i < xy.size(); ++i)
			{
				if (fabs(xy[i].first - xy[i - 1].first) > std::numeric_limits<double>::epsilon())
				{
					sumScale += fabs((uv[i].first - uv[i - 1].first) / (xy[i].first - xy[i - 1].first));
					++count;
				}

				if (fabs(xy[i].second - xy[i - 1].second) > std::numeric_limits<double>::epsilon())
				{
					sumScale += fabs((uv[i].second - uv[i - 1].second) / (xy[i].second - xy[i - 1].second));
					++count;
				}

				sumDx = uv[i].first - xy[i].first;
				sumDy = uv[i].second - xy[i].second;
			}

			initParams.s = sumScale / static_cast<double>(count);
			
			initParams.tx = sumDx / xy.size();
			initParams.ty = sumDy / xy.size();
			
			initParams.p = 0.0;
			initParams.r = 0.0;
			initParams.h = 0.0;

			return estimateParameters(initParams, xy, uv);
		}

		/// Estimate parameters using corresponding 2D point pairs and initial parameters
		bool estimateParameters(const ProjectionParams& initParams, const std::vector<std::pair<double, double>>& xy, const std::vector<std::pair<double, double>>& uv)
		{
			if (xy.size() != uv.size())
				throw std::runtime_error("Wrong sizes of xy and uv vectors from estimateParameters in PrjTrans");

			ProjectionParams prm0 = initParams;

			unsigned int numPts = scui(xy.size());

			if (xy.size() < 4)
				throw std::runtime_error("Not enough points for estimateParameters in PrjTrans");

			const double h1 = 1.e-6;
			const double h2 = 1.e-6;
			const double h3 = 1.e-9;
			
			unsigned int iteration = 0;
			const unsigned int maxIteration = 10;
			double s0 = 0.0;
			bool estimationResult;

#ifdef _DEBUG
			std::cout << "s0:\t" << prm0.s << std::endl;
			std::cout << "tx0:\t" << prm0.tx << std::endl;
			std::cout << "ty0:\t" << prm0.ty << std::endl;
			std::cout << "p0:\t" << prm0.p / pi*180.0 << std::endl;
			std::cout << "r0:\t" << prm0.r / pi*180.0 << std::endl;
			std::cout << "h0:\t" << prm0.h / pi*180.0 << std::endl;
			std::cout << std::endl;
#endif

			do
			{
				math::Matrix<double> a(2 * numPts, numParams);
				math::Matrix<double> l(2 * numPts, 1);

				for (unsigned int i = 0; i < numPts; ++i)
				{
					std::pair<double, double> uv0;
					const auto& uv00 = uv[i];

					runTransformation(prm0, xy[i].first, xy[i].second, uv0.first, uv0.second);
					l(i*2 + 0, 0) = uv00.first - uv0.first;
					l(i*2 + 1, 0) = uv00.second - uv0.second;

					std::pair<double, double> uv1;

					ProjectionParams prm1 = prm0;

					prm1.s += h1;
					runTransformation(prm1, xy[i].first, xy[i].second, uv1.first, uv1.second);
					a(i*2 + 0, 0) = (uv1.first - uv0.first) / h1;
					a(i*2 + 1, 0) = (uv1.second - uv0.second) / h1;
					prm1.s -= h1;

					prm1.tx += h2;
					runTransformation(prm1, xy[i].first, xy[i].second, uv1.first, uv1.second);
					a(i * 2 + 0, 1) = (uv1.first - uv0.first) / h2;
					a(i * 2 + 1, 1) = (uv1.second - uv0.second) / h2;
					prm1.tx -= h2;

					prm1.ty += h2;
					runTransformation(prm1, xy[i].first, xy[i].second, uv1.first, uv1.second);
					a(i * 2 + 0, 2) = (uv1.first - uv0.first) / h2;
					a(i * 2 + 1, 2) = (uv1.second - uv0.second) / h2;
					prm1.ty -= h2;

					prm1.p += h3;
					runTransformation(prm1, xy[i].first, xy[i].second, uv1.first, uv1.second);
					a(i * 2 + 0, 3) = (uv1.first - uv0.first) / h3;
					a(i * 2 + 1, 3) = (uv1.second - uv0.second) / h3;
					prm1.p -= h3;

					prm1.r += h3;
					runTransformation(prm1, xy[i].first, xy[i].second, uv1.first, uv1.second);
					a(i * 2 + 0, 4) = (uv1.first - uv0.first) / h3;
					a(i * 2 + 1, 4) = (uv1.second - uv0.second) / h3;
					prm1.r -= h3;

					prm1.h += h3;
					runTransformation(prm1, xy[i].first, xy[i].second, uv1.first, uv1.second);
					a(i * 2 + 0, 5) = (uv1.first - uv0.first) / h3;
					a(i * 2 + 1, 5) = (uv1.second - uv0.second) / h3;
				}

				auto at = a.transpose();
				auto n = at % a;
				auto c = at % l;

				bool fixed[numParams];
				fixed[0] = false;
				fixed[1] = false;
				fixed[2] = false;
				fixed[3] = false;
				fixed[4] = false;
				fixed[5] = false;

				for (int k1 = 0; k1 < numParams; ++k1)
				{
					if (fixed[k1])
					{
						for (int k2 = 0; k2 < numParams; ++k2)
						{
							if (k1 == k2)
								n(k1, k2) = 1.0;
							else
							{
								n(k1, k2) = 0.0;
								n(k2, k1) = 0.0;
							}
						}

						c(k1, 0) = 0.0;
					}
				}

#ifdef _DEBUG
				std::cout << "[ITERATION: " << iteration << " ]" << std::endl;
				std::cout << std::endl;
				std::cout << math::matrixout(n) << std::endl;
#endif
				auto nInv = n.inverse();
#ifdef _DEBUG
				std::cout << math::matrixout(nInv) << std::endl;
#endif
#ifdef _DEBUG
				std::cout << math::matrixout(c) << std::endl;
#endif
				auto dx = nInv % c;
				auto e = (a % dx) - l;
				auto ete = e.transpose() % e;
				double s = ete(0);

				prm0.s += dx(0);
				prm0.tx += dx(1);
				prm0.ty += dx(2);
				prm0.p += dx(3);
				prm0.r += dx(4);
				prm0.h += dx(5);

#ifdef _DEBUG
				std::cout << "s:\t" << prm0.s << std::endl;
				std::cout << "tx:\t" << prm0.tx << std::endl;
				std::cout << "ty:\t" << prm0.ty << std::endl;
				std::cout << "p:\t" << prm0.p / pi*180.0 << std::endl;
				std::cout << "r:\t" << prm0.r / pi*180.0 << std::endl;
				std::cout << "h:\t" << prm0.h / pi*180.0 << std::endl;
				std::cout << std::endl;

				std::cout << math::matrixout(dx) << std::endl;
				std::cout << "SD: " << sqrt(s) << std::endl;
				std::cout << std::endl;
#endif

				++ iteration;

				if (iteration > 1 && fabs(s0 - s) < 1.0e-6)
				{
					estimationResult = true;
					break;
				}
				else
					s0 = s;

				if (iteration >= maxIteration)
				{
					estimationResult = false;
					break;
				}

			} while (1);

			/// Update all member variables using the estimated parameters
			this->updateCoefficients(prm0);

			return estimationResult;
		}

	private:
		/// Rotation matrix for transformation
		math::Matrix<double> m;
		/// Rotation matrix for inverse-transformation
		math::Matrix<double> r;
		/// Scale
		double s;
		/// Shift
		double x0, y0;
		/// Origin shift for removing centroid moving effect
		double x02, y02;
	};
}