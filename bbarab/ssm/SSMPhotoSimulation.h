/*
* Copyright(c) 2008-2019 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <time.h>
#include <vector>

#include <SSM/include/SSMCollinearity.h>
#include <SSM/include/SSMMatrix.h>
#include <SSM/include/SSMRotation.h>
#include <utilitygrocery_2.0.h>

#define POSARTIFACT 5.0 ///meters
#define ORIARTIFACT 2.0 ///deg
#define REGIONMARGIN 500 ///meters

#define CONTROL_ID 99000

#define MIN_IMG_OBS 6

struct Point3DID : public data::Point3D
{
	int ID;
};

struct Point2DID : public data::Point2D
{
	int ID;
};

struct GroundPoint : public Point3DID
{
	///false: objected points measured in only one image 
	///true: objected points measured in more than one image
	bool bConjugate;
};

struct PositionOrientation
{
	double pos[3], ori[3];
};

struct GPSINS : public PositionOrientation
{
	double t;
};

struct Photo
{
	std::vector<Point2DID> img_point;
	std::vector<Point2DID> img_point_noise;
	int ID;
};

struct StripData
{
	std::vector<GPSINS> eop;
	std::vector<Photo> photo;
};

typedef std::vector<StripData> BlockData;

typedef std::vector<GPSINS> Trajectory;

struct TrjectoryParameters
{
	double a[4], b[4], c[4];///X, Y, and Z
	double d[4], e[4], f[4];///Omg, Phi, and Kap
};

enum VERSION {V10, V11};

class CSMPhotoSimulation
{
public:
	CSMPhotoSimulation();
	~CSMPhotoSimulation();
	bool RunSimulation(const std::string& simpath, const std::string& outfilepath);

private:

	bool ReadSimFile(const std::string& simpath);

	void GroundPointSimulation(const unsigned int numPoints, std::vector<GroundPoint>& groundPoints, const unsigned int idGain);

	void GroundPointSimulationRegular(const unsigned int rows, const unsigned int cols, std::vector<GroundPoint>& groundPoints, const unsigned int idGain);

	void GPSINS_Simulation();

	void EOP_Simulation(unsigned int camera_index, std::vector<BlockData>& photoBlock, const std::vector<Trajectory>& trj);

	void ImgMeasurement_Simulation(int camera_index);

	void ImgMeasurement_Simulation(GPSINS& eop_j, Photo& photo_j, const int camera_index);

	double GetRandNum();

	void GetRotationfromMatrix(math::Matrix<double> R, double &Omg, double &Phi, double &Kap);

	bool CheckPhotoCoordAvailability(data::Point2D P, int camera_index);

	bool FindImgPoint(Photo photo, int ID);

	bool FindGroundPoint(int ID, GroundPoint &G, std::vector<GroundPoint> &Points);

	void CheckMultipleCapturedTiePoints();

	void CheckMultipleCapturedControlPoints();

	void AddRandomErrors_To_Trajectory();

	void AddRandomErrors_To_TIE();

	void AddRandomError_To_Control();

	void SaveSimulationData(const std::string& outfilepath);

	void PrintOut_gpsins(fstream &gpsins_file, bool bNoise);

	void PrintOut_icf(fstream &icf_file, bool bNoise);

	void PrintOut_gcp(fstream &gcp_file, bool bNoise);

	void PrintOut_ori(fstream &ORI_file, bool bNoise);

	void PrintOut_cam(fstream &CAM_file);

	void PrintOut_EOP(fstream &EOP_file, fstream &ImgBND_file);

private:
	///
	///version
	///
	VERSION ver;

	///
	///options
	///
	bool regularDistribution;

	///
	///camera
	///
	unsigned int num_cam;///number of cameras

	std::vector<std::shared_ptr<sensor::FrameCamera>> cam;
	std::vector<std::string> camera_id;///camera ID

	///lever-arm/bore-sight
	std::vector<PositionOrientation> LA_BS;///meter, rad

	///
	///surveying area and ground points
	///
	double LT_X, LT_Y, RB_X, RB_Y;///meter
	/// Note that its unit is m^2/point; it is a inverse of points/meter^2 (number of points per square meter)
	double point_density;///(m^2/point)
	double min_ground_H, max_ground_H;///meter
	std::vector<GroundPoint> Ground_tie_points;
	std::vector<GroundPoint> Ground_tie_points_error;
	std::vector<GroundPoint> Ground_control_points;
	std::vector<GroundPoint> Ground_control_points_error;
	unsigned int NumControl;

	///
	///flight lines
	///
	unsigned int num_flight_lines;
	std::vector<double> time_offset;///sec
	std::vector<unsigned int> num_images;
	std::vector<double> time_gap;///sec
	std::vector<TrjectoryParameters> trajectory_parameter;///meter, rad
	std::vector<Trajectory> trajectory;
	std::vector<Trajectory> trajectory_noise;

	std::vector<BlockData> block;
	std::vector<BlockData> block_noise;

	///
	///Random errors
	///
	double sX, sY, sZ; ///GPS random errors (meters)
	double sO, sP, sK; ///INS random errors (rad)
	double scol, srow;///image measurement errors (pixel)
	double Noise_GCP_X, Noise_GCP_Y, Noise_GCP_Z;///control point noises (meters)
	double Error_TIE_X, Error_TIE_Y, Error_TIE_Z;///tie point errors (meters)
};
