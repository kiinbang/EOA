﻿/*
* Copyright (c) 1999-2019, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include <omp.h>

#include <ssm/include/NumericPDE.h>
#include <ssm/include/SSMConstants.h>
#include <ssm/include/SSMCoplanarity.h>
#include <ssm/include/SsmFrameCamera.h>
#include <ssm/include/SSMLS.h>
#include <ssm/include/SSMRotation.h>
#include <ssm/include/SSMSMACModel.h>

namespace ssm
{
	CoplanarityModel::CoplanarityModel()
	{
		numCam = 1;
	}

	void CoplanarityModel::initROParamsSimple(const std::shared_ptr<data::ModelData> modelData)
	{
		auto photo0 = model->getPhoto0();
		auto photo1 = model->getPhoto1();
		auto cam0 = photo0->getCam();
		auto cam1 = photo1->getCam();

		numCam = (cam0->getId() == cam1->getId()) ? 1 : 2;
				
		if (numCam == 2 && 
			(
			cam0->getDistortionModel()->getParameters()->getAllNumParameters() != 
			cam1->getDistortionModel()->getParameters()->getAllNumParameters())
			)
		{
			throw std::runtime_error("Two cameras have different distortion models.");
		}

		/// First photo exteorior orientation parameters
		auto eo0 = photo0->getEop()->getParameters();
		eo0->operator[](0)[0].set(0.0); /// omega
		eo0->operator[](0)[1].set(0.0); /// phi
		eo0->operator[](0)[2].set(0.0); /// kappa

		eo0->operator[](1)[0].set(0.0); /// X0
		eo0->operator[](1)[1].set(0.0); /// Y0
		eo0->operator[](1)[2].set(0.0); /// Z0

		eo0->operator[](0)[0].setDelta(1.0e-9); /// omega delta
		eo0->operator[](0)[1].setDelta(1.0e-9); /// phi delta
		eo0->operator[](0)[2].setDelta(1.0e-9); /// kappa delta

		eo0->operator[](1)[0].setDelta(1.0e-6); /// X0 delta
		eo0->operator[](1)[1].setDelta(1.0e-6); /// Y0 delta
		eo0->operator[](1)[2].setDelta(1.0e-6); /// Z0 delta

		eo0->operator[](0)[0].setStDev(constant::MIN_SD); /// omega sd
		eo0->operator[](0)[1].setStDev(constant::MAX_SD); /// phi sd
		eo0->operator[](0)[2].setStDev(constant::MAX_SD); /// kappa sd
			
		eo0->operator[](1)[0].setStDev(constant::MIN_SD); /// X0 sd
		eo0->operator[](1)[1].setStDev(constant::MIN_SD); /// Y0 sd
		eo0->operator[](1)[2].setStDev(constant::MIN_SD); /// Z0 sd

		/// second photo exteorior orientation parameters
		auto eo1 = photo1->getEop()->getParameters();
		eo1->operator[](0)[0].set(0.0); /// omega
		eo1->operator[](0)[1].set(0.0); /// phi
		eo1->operator[](0)[2].set(0.0); /// kappa

		eo1->operator[](1)[0].set(0.0); /// X0
		eo1->operator[](1)[1].set(0.0); /// Y0
		eo1->operator[](1)[2].set(0.0); /// Z0

		eo1->operator[](0)[0].setDelta(1.0e-9); /// omega delta
		eo1->operator[](0)[1].setDelta(1.0e-9); /// phi delta
		eo1->operator[](0)[2].setDelta(1.0e-9); /// kappa delta

		eo1->operator[](1)[0].setDelta(1.0e-6); /// X0 delta
		eo1->operator[](1)[1].setDelta(1.0e-6); /// Y0 delta
		eo1->operator[](1)[2].setDelta(1.0e-6); /// Z0 delta

		eo1->operator[](0)[0].setStDev(constant::MAX_SD); /// omega sd
		eo1->operator[](0)[1].setStDev(constant::MAX_SD); /// phi sd
		eo1->operator[](0)[2].setStDev(constant::MAX_SD); /// kappa sd

		eo1->operator[](1)[0].setStDev(constant::MIN_SD); /// X0 sd
		eo1->operator[](1)[1].setStDev(constant::MIN_SD); /// Y0 sd
		eo1->operator[](1)[2].setStDev(constant::MIN_SD); /// Z0 sd

		/// Camera parameters: f, xp, yp, w, h, pixel pitch
		std::shared_ptr<param::ParameterCollection> cam0ParamPtr = photo0->getCam()->getParameters();
		cam0ParamPtr->operator[](0)[0].setStDev(constant::MIN_SD); // set max-sd to f
		cam0ParamPtr->operator[](0)[1].setStDev(constant::MIN_SD); // set max-sd to xp
		cam0ParamPtr->operator[](0)[2].setStDev(constant::MIN_SD); // set max-sd to yp
		cam0ParamPtr->operator[](0)[3].setStDev(constant::MIN_SD); // set min-sd to w [pixels]
		cam0ParamPtr->operator[](0)[4].setStDev(constant::MIN_SD); // set min-sd to h [pixels]
		cam0ParamPtr->operator[](0)[5].setStDev(constant::MIN_SD); // set max-sd to pixel pitch

		cam0ParamPtr->operator[](0)[0].setDelta(1.0e-6); // set max-sd to f
		cam0ParamPtr->operator[](0)[1].setDelta(1.0e-6); // set max-sd to xp
		cam0ParamPtr->operator[](0)[2].setDelta(1.0e-6); // set max-sd to yp
		cam0ParamPtr->operator[](0)[3].setDelta(1.0e-6); // set min-sd to w [pixels]
		cam0ParamPtr->operator[](0)[4].setDelta(1.0e-6); // set min-sd to h [pixels]
		cam0ParamPtr->operator[](0)[5].setDelta(1.0e-6); // set max-sd to pixel pitch

		auto cam0DistParamPtr = photo0->getCam()->getDistortionModel()->getParameters();

		for (unsigned int i = 0; i < cam0DistParamPtr->size(); ++i)
		{
			auto& set = cam0DistParamPtr->operator[](i);
			for (unsigned int j = 0; j < set.size(); ++j)
			{
				set[j].setDelta(1.0e-9);
				set[j].setStDev(constant::MIN_SD);
			}
		}

		if (numCam == 2)
		{
			/// Camera parameters: f, xp, yp, w, h, pixel pitch
			std::shared_ptr<param::ParameterCollection> cam1ParamPtr = photo1->getCam()->getParameters();
			cam1ParamPtr->operator[](0)[0].setStDev(constant::MIN_SD); // set max-sd to f
			cam1ParamPtr->operator[](0)[1].setStDev(constant::MIN_SD); // set max-sd to xp
			cam1ParamPtr->operator[](0)[2].setStDev(constant::MIN_SD); // set max-sd to yp
			cam1ParamPtr->operator[](0)[3].setStDev(constant::MIN_SD); // set min-sd to w [pixels]
			cam1ParamPtr->operator[](0)[4].setStDev(constant::MIN_SD); // set min-sd to h [pixels]
			cam1ParamPtr->operator[](0)[5].setStDev(constant::MIN_SD); // set max-sd to pixel pitch

			cam1ParamPtr->operator[](0)[0].setDelta(1.0e-6); // set max-sd to f
			cam1ParamPtr->operator[](0)[1].setDelta(1.0e-6); // set max-sd to xp
			cam1ParamPtr->operator[](0)[2].setDelta(1.0e-6); // set max-sd to yp
			cam1ParamPtr->operator[](0)[3].setDelta(1.0e-6); // set min-sd to w [pixels]
			cam1ParamPtr->operator[](0)[4].setDelta(1.0e-6); // set min-sd to h [pixels]
			cam1ParamPtr->operator[](0)[5].setDelta(1.0e-6); // set max-sd to pixel pitch

			auto cam1DistParamPtr = photo1->getCam()->getDistortionModel()->getParameters();

			for (unsigned int i = 0; i < cam1DistParamPtr->size(); ++i)
			{
				auto& set = cam1DistParamPtr->operator[](i);
				for (unsigned int j = 0; j < set.size(); ++j)
				{
					set[j].setDelta(1.0e-9);
					set[j].setStDev(constant::MIN_SD);
				}
			}
		}


		/// First camera and distortion: f, xp, yp, w, h, pixel pitch and distortion parameters
		paramCltPtr.insert(paramCltPtr.begin(), photo0->getCam()->getParameters());
		paramCltPtr.insert(paramCltPtr.begin(), photo0->getCam()->getDistortionModel()->getParameters());

		if (numCam == 2)
		{
			/// Second camera and distortion: f, xp, yp, w, h, pixel pitch and distortion parameters
			paramCltPtr.insert(paramCltPtr.begin(), photo1->getCam()->getParameters());
			paramCltPtr.insert(paramCltPtr.begin(), photo1->getCam()->getDistortionModel()->getParameters());
		}

		/// First photo eop
		paramCltPtr.insert(paramCltPtr.begin(), photo0->getEop()->getParameters());
		/// Second photo eop
		paramCltPtr.insert(paramCltPtr.begin(), photo1->getEop()->getParameters());

		/// IOP and EOP for coplanarity condition
		unsigned int numCollections = static_cast<unsigned int>(paramCltPtr.size());
		numParameters.resize(numCollections);
		numSets.resize(numCollections);

		for (unsigned int i = 0; i < numCollections; ++i)
		{
			numParameters[i] = paramCltPtr[i]->getAllNumParameters();
			numSets[i] = 1;
		}

		std::cout << "Number of parameter collections: " << paramCltPtr.size() << std::endl;

		/// Find tie points
		model->findTiePts();
		std::cout << "Find tie points using point id" << std::endl;
	}

	bool CoplanarityModel::runRO(const std::shared_ptr<data::ModelData> modelData, const unsigned int maxIteration)
	{
		const double minSdDiff = 1.0e-6;

		model = modelData;

		/// Find tie poijnts
		model->findTiePts();

		std::vector<data::TiePoint>& tiePts = model->getTiePts();

		const unsigned int minNumTiePts = 5;

		if (tiePts.size() < minNumTiePts)
		{
			std::cout << "number of tie points is less than " << minNumTiePts << std::endl;
			return false;
		}

		/// Setup and initialize
		initROParamsSimple(modelData);

		ssm::LeastSquare lS(numParameters, numSets);
		lS.init();

		std::vector<std::shared_ptr<param::ParameterCollection>> tieObsParams(tiePts.size());

		for (unsigned int i = 0; i < tieObsParams.size(); ++i)
		{
			tieObsParams[i] = std::make_shared<param::ParameterCollection>();
			/// i th collection has two parameter sets
			tieObsParams[i]->resize(2);
			/// parameter set
			param::ParameterSet& set0 = tieObsParams[i]->operator[](0);
			param::ParameterSet& set1 = tieObsParams[i]->operator[](1);
			/// both parameter sets have two parameters, 2D point coordinates
			set0.resize(2);
			set1.resize(2);
			/// put coordinates and sd to parameters
			set0[0].set(tiePts[i].first.val(0));
			set0[1].set(tiePts[i].first.val(1));
			set0[0].setStDev(tiePts[i].first.sd(0));
			set0[1].setStDev(tiePts[i].first.sd(1));

			set1[0].set(tiePts[i].second.val(0));
			set1[1].set(tiePts[i].second.val(1));
			set1[0].setStDev(tiePts[i].second.sd(0));
			set1[1].setStDev(tiePts[i].second.sd(1));
		}

		std::cout << "Start RO iteration" << std::endl;

		unsigned int numParams = 0;
		for (unsigned int i = 0; i < numParameters.size(); ++i)
		{
			numParams += numParameters[i] * numSets[i];
		}

		/// Single observation
		std::vector<std::shared_ptr<param::ParameterCollection>> obsi(1);

		/// Iteration count
		unsigned int iterationNum = 0;
		bool repeat = true;
		/// Previous sd
		double preSd;

		do
		{
			++iterationNum;

std::cout << "Iteration #: " << iterationNum << " of " << maxIteration << std::endl;

			/// AX + BV = K
			/// F = determinant_func ≈ F0 + (∂f/∂x1)dx1 + (∂f/∂x2)r
			/// (∂f/∂x1)dx1 + (∂f/∂x2)r = -F0
			/// A: (∂f/∂x1)
			/// X: dx1
			/// B: (∂f/∂x2)
			/// V: r
			/// K: -F0
			/// Parameter observations
			/// X0: approximation
			/// dX: correction
			/// X00: observation
			/// V: residual
			/// X0 + dX = X00 + V
			/// -dX + V = (X0 - X00)

			for (unsigned int i = 0; i < tieObsParams.size(); ++i)
			{
				/// extract single observation
				obsi[0] = tieObsParams[i];

				/// Numeric partial derivatives
				math::Matrixd a, b, l;
				math::getNumericPDE(getDeterminant, paramCltPtr, obsi, a, b, l);

				math::Matrixd q(4, 4);
				q(0, 0) = obsi[0]->operator[](0)[0].getStDev();
				q(1, 1) = obsi[0]->operator[](0)[1].getStDev();
				q(2, 2) = obsi[0]->operator[](1)[0].getStDev();
				q(3, 3) = obsi[0]->operator[](1)[1].getStDev();

				auto bt = b.transpose();
				auto bqbt = b % q % bt;

				unsigned int colPos = 0;
				std::vector<ssm::LeastSquare::AMat> aMatList(paramCltPtr.size());
				for (unsigned int p = 0; p < paramCltPtr.size(); ++p)
				{
					aMatList[p].a = a.getSubset(0, colPos, 1, numParameters[p]);
					aMatList[p].index = 0;
					aMatList[p].index = p;

					colPos += numParameters[p];
				}

				lS.fillNmat(aMatList, bqbt.inverse(), l);
			}

			for (unsigned int i = 0; i < paramCltPtr.size(); ++i)
			{
				auto& collectionPtr = paramCltPtr[i];
				for (unsigned int j = 0; j < collectionPtr->size(); ++j)
				{
					for (unsigned int k = 0; k < collectionPtr->operator[](j).size(); ++k)
					{
						///
					}
				}
			}

			//lS.fill_Nmat_IOP(aIop, w, l, index);
			//lS.fill_Nmat_EOP(aEop, w, l, index);

			double pstVrn;
			math::Matrixd X;
			math::Matrixd nDot;
			math::Matrixd cDot;
			math::Matrixd xDot = lS.runLeastSquare_RN_New(pstVrn, nDot, cDot, X, eop_section);

			if (iterationNum > maxIteration)
				repeat = false;

			if (iterationNum > 1)
			{
				double sd = sqrt(pstVrn);
				
				std::cout << "Current Posterior sd: " << sd << std::endl;
				std::cout << "Previous posterior sd: " << preSd << std::endl;

				if (fabs(sd - preSd) < minSdDiff)
					repeat = false;
			}

			preSd = sqrt(pstVrn);

			std::cout << "Iteration is done." << std::endl;

		} while (repeat);

		return true;
	}

	/**
	*@brief get determinant of three 3D points
	*@param[in] base the first vector, base line vector
	*@param[in] r0 first image pt vector
	*@param[in] r1 second image pt vector
	*@return double determinant
	*/
	double CoplanarityModel::getDeterminant(const data::Point3D& base, const data::Point3D& r0, const data::Point3D& r1)
	{
		std::cout << "static function, getDeterminant, for base, r0 and r1 vectors." << std::endl;

		double det =
			- base(0) * (r0(1) * r1(2) - r0(2) * r1(1))
			- base(1) * (r0(2) * r1(0) - r0(0) * r1(2))
			- base(2) * (r0(0) * r1(1) - r0(1) * r1(0));

		return det;
	}

	/** getDeterminant
	*@brief get determinant
	*@param[in] givenObs given observations (2D image points)
	*@return matrix of a determinant
	*/
	math::Matrixd CoplanarityModel::getDeterminant(const std::vector<std::shared_ptr<param::ParameterCollection>>& givenParams, const std::vector<std::shared_ptr<param::ParameterCollection>>& givenObs)
	{
		unsigned int numCam;

		if (givenParams.size() == 4)
			numCam = 1;
		else if (givenParams.size() == 6)
			numCam = 2;
		else
			throw std::runtime_error("Wrong number of parameter collections");

		if(givenObs.size() < 1)
			throw std::runtime_error("Wrong number of observation collections");

		/// Camera and distortion model
		sensor::FrameCamera cam0, cam1;
		cam0.setParameters(givenParams[0]);
		cam0.setDistortionModel(std::static_pointer_cast<sensor::DistortionModel>(std::make_shared<sensor::SMACModel>(givenParams[1])));
		if (numCam == 2)
		{
			cam1.setParameters(givenParams[2]);
			cam1.setDistortionModel(std::static_pointer_cast<sensor::DistortionModel>(std::make_shared<sensor::SMACModel>(givenParams[3])));
		}
		else
			cam1 = cam0;

		/// Eop
		data::EOP eop0, eop1;
		eop0.setParameterCltPtr(givenParams[4]);
		eop1.setParameterCltPtr(givenParams[5]);

		auto baseVec = eop1.getPC() - eop0.getPC();

		/// R-rotation matrix
		auto ori0 = eop0.getOri();
		auto ori1 = eop1.getOri();
		const auto rotR0 = math::Rotation::getRMat(ori0(0), ori0(1), ori0(2));
		const auto rotR1 = math::Rotation::getRMat(ori1(0), ori1(1), ori1(2));

		math::Matrixd det(static_cast<int>(givenObs.size()), 1);

		for (unsigned int i = 0; i < givenObs.size(); ++i)
		{
			/// Given observed image coordinates
			data::Point2D imgPt0, imgPt1;

#ifdef _DEBUG
			if (givenObs[i]->size() != 2)
				throw std::runtime_error("Wrong number of observation parameter sets");

			if (givenObs[i]->operator[](0).size() != 2)
				throw std::runtime_error("It is not 2D-coordinate point data.");
			
			if (givenObs[i]->operator[](1).size() != 2)
				throw std::runtime_error("It is not 2D-coordinate point data.");
#endif
			imgPt0(0) = givenObs[i]->operator[](0)[0].get();
			imgPt0(1) = givenObs[i]->operator[](0)[1].get();

			imgPt1(0) = givenObs[i]->operator[](1)[0].get();
			imgPt1(1) = givenObs[i]->operator[](1)[1].get();

			/// Refined photo coordinates
			auto refinedPt0 = cam0.getRefinedPhotoCoordFromImgCoord(imgPt0);
			auto refinedPt1 = cam1.getRefinedPhotoCoordFromImgCoord(imgPt1);

			data::Point3D vec0;
			vec0(0) = refinedPt0(0);
			vec0(1) = refinedPt0(1);
			vec0(2) = cam0.getFL();

			data::Point3D vec1;
			vec1(0) = refinedPt1(0);
			vec1(1) = refinedPt1(1);
			vec1(2) = cam1.getFL();

			auto rVec0 = rotR0 % vec0;
			auto rVec1 = rotR1 % vec1;

			det(i, 0) = getDeterminant(baseVec, rVec0, rVec1);
		}	
		
		return det;
	}
}