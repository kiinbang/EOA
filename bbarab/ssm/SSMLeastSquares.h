/*
* Copyright(c) 2005-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <fstream>
#include <stdio.h>

#include <ssm/ssmList.h>
#include <ssm/include/SSMRETMAT.h>

namespace ssm
{
	#define scui static_cast<unsigned int>

	/// Section index
	enum class SectIdx
	{
		IOP = 0, ///< Interior orientation parameters
		EOP = 1, ///< Exterior orientation parameters
		PT = 2, ///< Point observations
		LINE = 3, ///< Straight line observations
		TRI = 4 ///< Triangular patch observations
	};

	struct AMat
	{
		math::Matrixd a;
		unsigned int index = 0;
		SectIdx sect;
	};

	class NdotMat
	{
		friend class CdotMat;
	public:
		NdotMat();

		NdotMat(const unsigned int nParams, const unsigned int nSets);

		void config(const unsigned int nParams, const unsigned int nSets);

		util::SsmList<math::Matrixd>& get();

		const util::SsmList<math::Matrixd>& readOnly() const;

		unsigned int getNumParams();

		unsigned int getNumSets();

	protected:
		util::SsmList<math::Matrixd> dot;
		unsigned int numParams;
		unsigned int numSets;
	};

	class CdotMat : public NdotMat
	{
	public:
		CdotMat();

		CdotMat(const unsigned int nParams, const unsigned int nSets);

		void config(const unsigned int nParams, const unsigned int nSets);
	};

	class NbarMat
	{
	public:
		NbarMat();

		NbarMat(const unsigned int nSets1, const unsigned int nSets2);

		void config(const unsigned int nSets1, const unsigned int nSets2);

		util::SsmList<util::SsmList<math::Matrixd>>& get();

		const util::SsmList<util::SsmList<math::Matrixd>>& readOnly() const;

		unsigned int getNumSets1();
		unsigned int getNumSets2();

	private:
		util::SsmList<util::SsmList<math::Matrixd>> nbar;
		unsigned int numSets1;
		unsigned int numSets2;
	};

	/// Note that  this is for the case including iops, eops, points, lines represented two points and planar patches represented three points,
	/// and it corresponds to the collinearity equations.
	/// For the use of other features, unknown parameters and models (such as a coplanarity condition), it should be modified and add additional functions.
	class LeastSquare
	{
	public:

		/** Constructor */
		LeastSquare();

		/** Constructor */
		LeastSquare(const unsigned int nCameras,
			const unsigned int nIOPs,
			const unsigned int nImages,
			const unsigned int nEOPs,
			const unsigned int nPoints,
			const unsigned int nLines,
			const unsigned int nTriangles);

		/**destructor*/
		virtual ~LeastSquare();

		/**initialized the object*/
		void init();

		/**simple LS without weight matrix*/
		RetMat runLeastSquare(const math::Matrixd& A, const math::Matrixd& L);

		/**simple and fast(simple result, only X matrix) LS*/
		math::Matrixd runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L);

		/**LS using weight matrix, not using reduced normal matrix for simple modeling*/
		RetMat runLeastSquare(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W);

		/**LS using weight matrix, not using reduced normal matrix for simple modeling<br>
		*simple result, only X matrix, fast a little.
		*/
		math::Matrixd runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W);

		math::Matrixd runLeastSquare_Fast(const math::Matrixd& A, const math::Matrixd& L, const math::Matrixd& W, math::Matrixd& N);

		/**Get correlation matrix from variance-covariance matrix*/
		static math::Matrixd getCorrelation(const math::Matrixd& VarCov);

		/**Extract variance(diagonal element) matrix from variance-covariance matrix*/
		static math::Matrixd extractVar(const math::Matrixd& VarCov);

		/**GetErrorList*/
		util::SsmList<double> getErrorList() const;

		////////////////////////////
		///Reduced Normal Matrix////
		////////////////////////////
		/**LS for bundle using reduced normal matrix<br>
		*EO parameters, point data and straight line data.
		*/
		math::Matrixd runLeastSquare_RN(double& var, math::Matrixd& N_dot, math::Matrixd& C_dot);

		/**Make N_dot matrix from N_dot elements list*/
		math::Matrixd runLeastSquare_RN_New(double& var, math::Matrixd& N_dot, math::Matrixd& C_dot, math::Matrixd& X, const SectIdx boundarySect = SectIdx::EOP);

		/**Parameters' constraints: IOPs*/
		double fill_Nmat_IOP(math::Matrixd aIop, math::Matrixd w, math::Matrixd l, int index);

		/**Parameters' constraints: EOPs*/
		double fill_Nmat_EOP(math::Matrixd aEop, math::Matrixd w, math::Matrixd l, int index);

		/**fill the normal matrix for the point data*/
		double fill_Nmat_Data_Point(const math::Matrixd& aEop, 
			const math::Matrixd& aObj, 
			const math::Matrixd& w, 
			const math::Matrixd& l, 
			const int point_index, 
			const int image_index);

		/**fill the normal matrix for the point data*/
		double fill_Nmat_Data_Point(const math::Matrixd& aIop, 
			const math::Matrixd& aEop, 
			const math::Matrixd& aObj, 
			const math::Matrixd& w, 
			const math::Matrixd& l, 
			const int point_index, 
			const int image_index, 
			const int camera_index);

		/**fill the normal matrix for the line data*/
		double fill_Nmat_Data_Line(const math::Matrixd& aEop, 
			const math::Matrixd& aLine,
			const math::Matrixd& w, 
			const math::Matrixd& l, 
			const int line_index, 
			const int image_index);

		/**fill the normal matrix for the line data*/
		double fill_Nmat_Data_Line(const math::Matrixd& aIop, 
			const math::Matrixd& aEop, 
			const math::Matrixd& aLine,
			const math::Matrixd& w, 
			const math::Matrixd& l, 
			const int line_index, 
			const int image_index, 
			const int camera_index);

		/**3D ground point constraints*/
		double fill_Nmat_Data_XYZ(const math::Matrixd& aObj, const math::Matrixd& w, const math::Matrixd& l, int point_index);

		/**3D ending point of the line constraints*/
		double fill_Nmat_Data_XYZofLine(const math::Matrixd& a2, const math::Matrixd& w, const math::Matrixd& l, const int line_index);

		/**Set configuration data for runLeastSquare_RN and camera model*/
		void setDataConfig(const unsigned int nCameras,
			const unsigned int nIOPs,
			const unsigned int nImages,
			const unsigned int nEOPs,
			const unsigned int nPoints,
			const unsigned int nLines,
			const unsigned int nTriangles);

	protected:
		/**Make N_dot matrix from N_dot elements list*/
		math::Matrixd make_N_dot() const;

		/**Make N_dot matrix from N_dot elements list*/
		math::Matrixd make_N_dot_New(const unsigned int sizeOfUpper, const SectIdx boundarySect) const;

		/**Make C_dot matrix from C_dot elements list*/
		math::Matrixd make_C_dot() const;

		/**Make C_dot matrix from C_dot elements list*/
		math::Matrixd make_C_dot_New(const unsigned int sizeOfUpper, const SectIdx sect) const;

		/**Make C_2dot matrix from C_2dot_point and C_2dot_line elements list & triangle patches for LIDAR*/
		math::Matrixd make_C_2dot() const;

		/**Make C_2dot matrix from C_2dot_point and C_2dot_line elements list & triangle patches for LIDAR*/
		math::Matrixd make_C_2dot_New(const unsigned int sizeOfLower, const SectIdx sect) const;

		/**write given string on given file*/
		void fileOut(const std::string& fname, const std::string& st) const;

		/**fill the normal matrix*/
		double fill_Nmat(const util::SsmList<AMat>& a,
			const math::Matrixd& w,
			const math::Matrixd& l,
			const unsigned int nEqs,
			util::SsmList<NdotMat>& Ndots,
			util::SsmList<util::SsmList<NbarMat>>& Nbars,
			util::SsmList<CdotMat>& Cdots);

		/**get Cdot matrix for each section*/
		math::Matrixd getCdot(const SectIdx idx) const;

		/**get Ndot matrix for each section*/
		math::Matrixd getNdot(const SectIdx idx) const;

		/**get Nbar matrix for each off-diagonal section*/
		math::Matrixd getNbar(const SectIdx idx1, const SectIdx idx2) const;

	protected:
		const unsigned int lastSectionIdx = scui(SectIdx::TRI);
		/// sections[5]: Iop, Eop, Obj pt(3), Obj line(6), Obj triangle patch(9)
		const unsigned int numSections = 5;

		util::SsmList<math::Matrixd> Error;
		double PosVar;/**<posteriori variance*/
		unsigned int numUnknown;/**<number of all unknown parameters*/
		unsigned int numEq;/**<number of equations and constraints*/
		util::SsmList<unsigned int> numParameters;/**<number of parameters for each section*/
		util::SsmList<unsigned int> numSets;/**<number of input datasets for each section*/
		util::SsmList<NdotMat> NdotList; /// size is numSections.
		util::SsmList<util::SsmList<NbarMat>> NbarList; /// size is numSections X numSections, where diagonal parts and some of off-diagonal parts are empty
		util::SsmList<CdotMat> CdotList;
	};
}