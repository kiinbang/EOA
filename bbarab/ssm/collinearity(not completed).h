#pragma once

#include <ssm/include/SSMParameterCollection.h>
#include <ssm/include/SsmFrameCamera.h>
#include <ssm/include/SSMMatrixd.h>
#include <ssm/include/SsmPhoto.h>
#include <ssm/include/SSMRotation.h>
#include <ssm/include/SSMSMACModel.h>

namespace ssm
{
	이건 스테레오 모델에서 공면조건식에 대한 것으로 보임.
	완성된 코드인지 확인이 필요함.
	
	/** getMeasuredImageCoordinates
	*@brief run collinearity function
	*@param[in] givenParams given parameters (iop, eop and object point)
	*@param[in] givenObs given observations (2D image point)
	*@return matrix of a determinant
	*/
	math::Matrixd getMeasuredImageCoordinates(
		const std::vector<std::shared_ptr<param::ParameterCollection>>& givenParams,
		const std::vector<std::shared_ptr<param::ParameterCollection>>& givenObs);
}
