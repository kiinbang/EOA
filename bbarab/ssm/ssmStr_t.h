/**
* @file		ssmStr.h
* @date		since 2000
* @author	kiinbang
* @brief	string class exported in dll
*/

#pragma once

#include <wchar.h>

namespace util
{
	/**
	* @class	SsmStr_t
	* @brief	class for wchat_t string
	*/
	//class __declspec (dllexport) SsmStr
	class SsmStr_t
	{
	public:
		/**constructor*/
		SsmStr_t(const SsmStr_t& str);

		/**constructor*/
		SsmStr_t(const wchar_t* str);

		/**constructor*/
		SsmStr_t();

		/**destructor*/
		~SsmStr_t();

		/**
		 * @brief operator +
		 * @param str string to be added
		 * @return result after operation
		*/
		SsmStr_t operator +(const SsmStr_t& str) const;

		/**
		 * @brief operator =
		 * @param str string to be added
		 * @return result after operation
		*/
		void operator =(const SsmStr_t& str);

		/**
		 * @brief operator +=
		 * @param wchar_t str string to be attached
		*/
		void operator +=(const wchar_t* str);

		/**
		 * @brief operator +=
		 * @param SsmStr_t str string to be attached
		*/
		void operator +=(const SsmStr_t& str);

		/**
		 * @brief operator !=, not equal
		 * @param SsmStr_t str string to be compared
		 * @return bool
		*/
		bool operator !=(const SsmStr_t& str);

		/**
		 * @brief operator ==, equal
		 * @param SsmStr_t str string to be compared
		 * @return bool
		*/
		bool operator ==(const SsmStr_t& str);

		/**
		 * @brief get string access to m_pString as wchar_t* type
		 * @return wchar_t*
		*/
		wchar_t* getChar();

		/**
		 * @brief get length of the string
		 * @return int length
		*/
		int getLength();

		/// format string
		int Format(const wchar_t* format, ...);

	protected:
		// data block
		int m_nLength;
		wchar_t* m_pString;
	};
}