/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <SSM/SSMNumericPDE.h>

namespace math
{
	/// https://www.mps.mpg.de/phd/numerical-integration-partial-differential-equations-pde-semi-analytical-problems-finite-differences

	/**getNumericPDs
	* Description : it is a function for getting numeric PDE
	*@param std::vector<double>& f0 : default values of the given function
	*@param generalFunction func : a function given
	*@param const std::vector<param::ParameterCollection>& collections0: parameters used in the given function
	*@param std::vector<std::vector<param::ParameterCollection>>& retPds : storage for saving the calculate partial derivatives
	*@param const NPDETYPE npdeType : type of the used numerical PDE's method
	*@return void
	*/
	void getNumericPDs(const std::vector<double>& f0,
		const ssm::generalFunction func,
		const std::vector<param::ParameterCollection>& collections0,
		std::vector<std::vector<param::ParameterCollection>>& retPds,
		const NPDETYPE npdeType)
	{
		/// Copy the given parameters for changing values
		std::vector<param::ParameterCollection> collections = collections0;

		/// initialize return values, PDs for each functions
		retPds.resize(f0.size()); /// Number of functions

		for (auto& retPdsi : retPds)
		{
			retPdsi = collections0;
		}
		
		for (unsigned int c = 0; c < static_cast<unsigned int>(collections.size()); ++c)
		{
			param::ParameterCollection& collect = collections[c];

			for (unsigned int s = 0; s < static_cast<unsigned int>(collect.size()); ++s)
			{
				param::ParameterSet& set = collect[s];

				for (unsigned int p = 0; p < static_cast<unsigned int>(set.size()); ++p)
				{
					param::Parameter& param = set[p];

					try
					{
						switch (npdeType)
						{
						case FirstOrder:
						{
							std::vector<double> f1;

							param.set(param.get() + param.getDelta());
							func(collections, f1);
							param.set(param.get() - param.getDelta());

							for (size_t e = 0; e < f1.size(); ++e)
							{
								/// e th equation return value
								/// p th parameter
								/// of s th parameter-set
								/// in the c th parameter-collection
								retPds[e][c][s][p].set((f1[e] - f0[e]) / param.getDelta());
							}
						}
						break;

						case FourthOrder:
						{
							std::vector<double> f1, f2, f_1, f_2;
							param.set(param.get() + param.getDelta());
							func(collections, f1);
							param.set(param.get() + param.getDelta());
							func(collections, f2);
							param.set(param.get() - param.getDelta() * 3.0);
							func(collections, f_1);
							param.set(param.get() - param.getDelta());
							func(collections, f_2);
							param.set(param.get() + param.getDelta() * 2.0);

							for (size_t e = 0; e < f1.size(); ++e)
							{
								/// k th function return value
								/// j th parameter
								/// of i th parameter set
								/// in the parameter collection
								retPds[e][c][s][p].set((8.*f1[e] + f_2[e] - f2[e] - 8.*f_1[e]) / (12.0 * param.getDelta()));
							}
						}
						break;
						default:
							std::string msg = std::string("Error in getNumericPDs with a wrong NPDE type.\n");
							throw std::runtime_error(msg);
							break;
						}
					}
					catch (...)
					{
						std::string msg = std::string("Error in getNumericPDs ");
						msg += std::string("with ") + std::to_string(c) + std::string("th collection: ") + collect.getName();
						msg += std::string(", ") + std::to_string(s) + std::string("th parameter-set: ") + set.getName();
						msg += std::string(", ") + std::to_string(p) + std::string("th parameter: ") + param.getName();
						msg += std::string(" (value: ") + std::to_string(param.get()) + std::string(") ");
						msg += std::string("(delta: ") + std::to_string(param.getDelta()) + std::string(") ");
						msg += std::string("\n");
						throw std::runtime_error(msg);
					}
				}
			}
		}
	}
}