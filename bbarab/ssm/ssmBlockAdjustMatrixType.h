/*
* Copyright(c) 2005-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <SSMMatrix.h>
#include <ssmList.h>

namespace ssm
{
	class CdotMat;

	/**
	*@class SectIdx
	*@brief Section (sub-block) index
	*/
	enum class SectIdx
	{
		IOP		= 0, ///< Interior orientation parameters
		EOP		= 1, ///< Exterior orientation parameters
		PT		= 2, ///< Point observations
		LINE	= 3, ///< Straight line observations
		TRI		= 4 ///< Triangular patch observations
	};

	/**
	*@struct AMat
	*@brief element of a whole design matrix
	*/
	struct AMat
	{
		///< submatrix
		math::Matrix<double> a;
		///< index for sub-item
		unsigned int index = 0;
		///< subblock index
		SectIdx sect;
	};

	class NdotMat
	{
		friend class CdotMat;
	public:
		NdotMat()
		{
			const unsigned int nParams = 0;
			const unsigned int nSets = 0;

			config(nParams, nSets);
		}

		NdotMat(const unsigned int nParams, const unsigned int nSets)
		{
			config(nParams, nSets);
		}

		void config(const unsigned int nParams, const unsigned int nSets)
		{
			numSets = nSets;
			numParams = nParams;

			dot.resize(numSets);
			for (unsigned int i = 0; i < dot.size(); ++i)
			{
				dot[i].resize(nParams, nParams, 0.0);
			}
		}

		util::SsmList<math::Matrix<double>>& get() { return dot; }

		const util::SsmList<math::Matrix<double>>& readOnly() const { return dot; }

		unsigned int getNumParams() { return numParams; }

		unsigned int getNumSets() { return numSets; }

	protected:
		util::SsmList<math::Matrix<double>> dot;
		unsigned int numParams;
		unsigned int numSets;
	};

	class CdotMat : public NdotMat
	{
	public:
		CdotMat() : NdotMat() {}

		CdotMat(const unsigned int nParams, const unsigned int nSets) : NdotMat(nParams, nSets) {}

		void config(const unsigned int nParams, const unsigned int nSets)
		{
			numSets = nSets;
			numParams = nParams;

			dot.resize(numSets);
			for (unsigned int i = 0; i < dot.size(); ++i)
			{
				dot[i].resize(nParams, 1, 0.0);
			}
		}
	};

	class NbarMat
	{
	public:
		NbarMat()
		{
			const unsigned int nSets1 = 0;
			const unsigned int nSets2 = 0;

			config(nSets1, nSets1);
		}

		NbarMat(const unsigned int nSets1, const unsigned int nSets2)
		{
			config(nSets1, nSets2);
		}

		void config(const unsigned int nSets1, const unsigned int nSets2)
		{
			numSets1 = nSets1;
			numSets2 = nSets2;

			nbar.resize(numSets1);
			for (unsigned int i = 0; i < nbar.size(); ++i)
			{
				auto& nbari = nbar[i];

				nbari.resize(numSets2);
				for (unsigned int j = 0; j < nbar[i].size(); ++j)
				{
					nbar[i][j].del();
				}
				/// Make the matrix empty because only some of sub-parts of N-bar matrix are not empty and it need to save memory.
			}
		}

		util::SsmList<util::SsmList<math::Matrix<double>>>& get() { return nbar; }

		const util::SsmList<util::SsmList<math::Matrix<double>>>& readOnly() const { return nbar; }

		unsigned int getNumSets1() { return numSets1; }
		unsigned int getNumSets2() { return numSets2; }

	private:
		util::SsmList<util::SsmList<math::Matrix<double>>> nbar;
		unsigned int numSets1;
		unsigned int numSets2;
	};
}