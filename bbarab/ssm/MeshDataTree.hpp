/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#include <ssm/nanoflann/nanoflann.hpp>

namespace mesh
{
    /// data dimension
    const std::size_t dimension = 3;

    /// Vertex3D: 3D vertex data
    class Vertex3D
    {
    public:
        Vertex3D()
        {
            xyz[0] = xyz[1] = xyz[2] = 0.0;
        }

        Vertex3D(const Vertex3D& copy)
        { 
            ::memcpy(xyz, copy.xyz, 3*sizeof(double)); 
        }

        bool operator==(const Vertex3D& pt) const
        { 
            return pt.x() == xyz[0] && pt.y() == xyz[1] && pt.z() == xyz[2];
        }

        const double& x() const { return xyz[0]; }
        const double& y() const { return xyz[1]; }
        const double& z() const { return xyz[2]; }

        double& x() { return xyz[0]; }
        double& y() { return xyz[1]; }
        double& z() { return xyz[2]; }

        double& operator[] (const std::size_t idx) 
        { 
            try
            {
                return xyz[idx];
            }
            catch (...)
            {
                throw std::runtime_error("Error in Vertex3D::operator[]");
            }
        }

        const double& operator[] (const std::size_t idx) const
        {
            try
            {
                return xyz[idx];
            }
            catch (...)
            {
                throw std::runtime_error("Error in Vertex3D::operator[]");
            }
        }

        void set(const double newX, const double newY, const double newZ)
        {
            xyz[0] = newX;
            xyz[1] = newY;
            xyz[2] = newZ;
        }

        const std::vector<int>& getFaceId() const { return faceId; }
        std::vector<int>& getFaceId() { return faceId; }
        void addFaceId(const int idx) { faceId.push_back(idx); }

    private:
        double xyz[3];
        std::vector<int> faceId;
    };


    ///  MeshData: structure for mesh data
    template<class T> struct MeshData
    {        
    public:
        typedef T CoordValueType;

        /// Must return the number of data points
        inline std::size_t kdtree_get_point_count() const { return vertices.size(); }

        /// Returns the distance between the vector "p1[0:size-1]" and the data point with index "idx_p2" stored in the class:
        inline T kdtree_distance(const T* p1, const size_t idx_p2, size_t size) const
        {
            T s = 0;
            for (size_t i = 0; i < size; ++i)
            {
                const T d = p1[i] - vertices[idx_p2][i];
                s += d * d;
            }
            return s;
        }

        /// Returns the dim'th component of the idx'th point in the class:
        inline T kdtree_get_pt(const size_t idx, int dim) const
        {
            return vertices[idx][dim];
        }

        /// Optional bounding-box computation: return false to default to a standard bbox computation loop.
        /// Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
        /// Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
        template <class BBOX> bool kdtree_get_bbox(BBOX& /*bb*/) const 
        {
            return false;
        }

    public:
        std::vector<Vertex3D> vertices;
    };

    using MeshDataType = MeshData<double>;

    /// KD Tree type to use for the mesh node data
    using KDTree = nanoflann::KDTreeSingleIndexAdaptor<nanoflann::L2_Simple_Adaptor<MeshDataType::CoordValueType, MeshDataType>, MeshDataType, 3>;

    /// Build KDTree from vertex data
    void buildTree(std::shared_ptr<mesh::KDTree> index, MeshDataType& meshData, const int maxLeaf = 10)
    {
        /// Larger maxLeaf result in fast search, but takes loing time for tree construction
        index.reset(new KDTree(mesh::dimension, meshData, nanoflann::KDTreeSingleIndexAdaptorParams(maxLeaf)));
        index->buildIndex();
    }

    /// Search
    bool search(mesh::MeshDataType::CoordValueType searchPt[], 
        std::shared_ptr<mesh::KDTree> index, 
        std::vector<double>& distances,
        std::vector<std::size_t>& indices,
        const int numClosest, 
        const double minDist)
    {
        distances.resize(numClosest, 0.0);
        indices.resize(numClosest, 0);

        try
        {
            index->knnSearch(searchPt, numClosest, indices.data(), distances.data());
        }
        catch (...)
        {
            throw std::runtime_error("Error in KDTree search");
        }

        if (distances[0] < minDist)
            return true;
        else
            return false;
    }
}