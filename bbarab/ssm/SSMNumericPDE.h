/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <string>
#include <vector>

#include "ssm/include/SSMDerivedMatrix.h"
#include "SSMFunctionsDef.h"

namespace math
{
	enum NPDETYPE { FirstOrder/*first order*/, FourthOrder/*fourth order cenrtral differences*/ };
	
	/**getNumericPDs
	* Description : it is a function for getting numeric PDE
	*@param std::vector<double>& f0 : default values of the given function
	*@param generalFunction func : a function given
	*@param const std::vector<param::ParameterCollection>& collections0: parameters used in the given function
	*@param std::vector<std::vector<param::ParameterCollection>>& retPds : storage for saving the calculate partial derivatives
	*@param const NPDETYPE npdeType : type of the used numerical PDE's method
	*@return void
	*/
	void getNumericPDs(const std::vector<double>& f0,
		const ssm::generalFunction func,
		const std::vector<param::ParameterCollection>& collections0,
		std::vector<std::vector<param::ParameterCollection>>& retPds,
		const NPDETYPE npdeType);
}
