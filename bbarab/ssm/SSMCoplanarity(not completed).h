/*
* Copyright (c) 1999-2019, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include <SSM/include/SSMModel.h>

namespace ssm
{
	/**
	*@class CoplanarityModel
	*@date initial version: 2020/06/29
	*@brief it is a class for a coplanarity condition for stereo images.
	*/
	class CoplanarityModel
	{
	public:
		/** CoplanarityModel
		 *@brief constructor
		*/
		CoplanarityModel();

		/** runRO
		 *@brief run relative orientation
		 *@param model it is a model data consisting of two photos
		 *@return bool
		*/
		bool runRO(const std::shared_ptr<data::ModelData> model, const unsigned int maxIteration);

		/** getDeterminant
		*@brief get determinant of three 3D points
		*@param[in] base the first vector, base line vector
		*@param[in] r0 first image pt vector
		*@param[in] r1 second image pt vector
		*@return double determinant
		*/
		static double getDeterminant(const data::Point3D& base, const data::Point3D& r0, const data::Point3D& r1);

		/** getDeterminant
		*@brief get determinant
		*@param[in] givenObs given observations (2D image points)
		*@return matrix of a determinant
		*/
		static math::Matrixd getDeterminant(const std::vector<std::shared_ptr<param::ParameterCollection>>& givenParams, const std::vector<std::shared_ptr<param::ParameterCollection>>& givenObs);
		
	private:
		/** initROParamsSimple
		 * @brief initialize RO parameters for two photos captured with similar orientation angles
		*/
		void initROParamsSimple(const std::shared_ptr<data::ModelData> modelData);

	private:
		/**
		 * @brief model data pointer
		*/
		std::shared_ptr<data::ModelData> model;

		/**
		 * @brief parameter collection pointer
		*/
		std::vector<std::shared_ptr<param::ParameterCollection>> paramCltPtr;
		
		unsigned int numCam;
		const int camera_section = 0;
		const int eop_section = 1;
		std::vector<unsigned int> numParameters;
		std::vector<unsigned int> numSets;
	};
}
