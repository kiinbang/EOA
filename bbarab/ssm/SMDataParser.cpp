// SMDataParser.cpp: implementation of the CSMDataParser class.
//
//////////////////////////////////////////////////////////////////////

#include "SMDataParser.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
using namespace SMATICS_BBARAB;

CSMDataParser::CSMDataParser()
{

}

CSMDataParser::CSMDataParser(CSMDataParser &copy)
{
	m_FileContents = copy.m_FileContents;
	
	ICP_List = copy.ICP_List;
	GCP_List = copy.GCP_List;
	ICL_List = copy.ICL_List;
	GCL_List = copy.GCL_List;
	SCENE_List = copy.SCENE_List;

	for(int i=0; i<NUM_NORMALIZE_COEFF; i++) coeff[i] = copy.coeff[i];
};

CSMDataParser::~CSMDataParser() { Init(); }

void CSMDataParser::operator=(CSMDataParser &copy)
{
	m_FileContents = copy.m_FileContents;
	
	ICP_List = copy.ICP_List;
	GCP_List = copy.GCP_List;
	ICL_List = copy.ICL_List;
	GCL_List = copy.GCL_List;
	SCENE_List = copy.SCENE_List;

	for(int i=0; i<NUM_NORMALIZE_COEFF; i++) coeff[i] = copy.coeff[i];
}

void CSMDataParser::DataNormalization_ICP_ICL(CSMList<_ICP_> ICP_List, CSMList<_ICL_> ICL_List, double coeff[4])
{
	double col, row;
	double maxcol, mincol, maxrow, minrow;

	unsigned int i;

	_ICP_ icp;
	_ICL_ icl;
	
	if(ICP_List.GetNumItem() != 0)
	{
		icp = ICP_List.GetAt(0);
		col = icp.col;
		row = icp.row;
		
		maxcol = mincol = col;
		maxrow = minrow = row;
		
		for(i=1; i<ICP_List.GetNumItem(); i++)
		{
			icp = ICP_List.GetAt(i);
			col = icp.col;
			row = icp.row;
			
			if(maxcol < col)
				maxcol = col;
			if(mincol > col)
				mincol = col;
			
			if(maxrow < row)
				maxrow = row;
			if(minrow > row)
				minrow = row;
		}
	}
	else if(ICL_List.GetNumItem() != 0)
	{
		icl = ICL_List.GetAt(0);
		col = icl.col;
		row = icl.row;

		maxcol = mincol = col;
		maxrow = minrow = row;
	}
	
	if(ICL_List.GetNumItem() != 0)
	{
		for(i=0; i<ICL_List.GetNumItem(); i++)
		{
			icl = ICL_List.GetAt(i);
			col = icl.col;
			row = icl.row;
			
			if(maxcol < col)
				maxcol = col;
			if(mincol > col)
				mincol = col;
			
			if(maxrow < row)
				maxrow = row;
			if(minrow > row)
				minrow = row;
		}
	}

	//Normalized Coord = (Original Coord + SHIFT)*SCALE
	//Original Coord = (Normalized Coord/SCALE) - SHIFT

	//SCALE = 1/(max-min)
	//SHIFT = -(max-min)/2
	
	/**maxrow[0], minrow[1], maxcol[2], mincol*/
	if((ICP_List.GetNumItem() != 0)||(ICL_List.GetNumItem() != 0))
	{
		coeff[0] = maxrow;
		coeff[1] = minrow;
		coeff[2] = maxcol;
		coeff[3] = mincol;
	}

	//So many "if", stupid process, but no problem, not important process...
}

void CSMDataParser::DataNormalization_GCP_MatlabGCL(CSMList<_GCP_> GCP_List, CSMList<_GCL_> GCL_List, double coeff[6])
{
	/**<SHIFT_X[0], SCALE_X[1], SHIFT_Y[2], SCALE_Y[3], SHIFT_Z[4], SCALE_Z[5], SHIFT_col[6], SCALE_col[7], SHIFT_row[8], SCALE_row[9] */

	unsigned int i;
	
	double X, Y, Z;
	double maxX, minX, maxY, minY, maxZ, minZ;
	_GCP_ gcp;
	_GCL_ gcl;

	if(GCP_List.GetNumItem() != 0)
	{		
		gcp = GCP_List.GetAt(0);
		X = gcp.X;
		Y = gcp.Y;
		Z = gcp.Z;
		
		maxX = minX = X;
		maxY = minY = Y;
		maxZ = minZ = Z;
		
		for(i=1; i<GCP_List.GetNumItem(); i++)
		{
			gcp = GCP_List.GetAt(i);
			X = gcp.X;
			Y = gcp.Y;
			Z = gcp.Z;
			
			if(maxX < X)
				maxX = X;
			if(minX > X)
				minX = X;
			
			if(maxY < Y)
				maxY = Y;
			if(minY > Y)
				minY = Y;
			
			if(maxZ < Z)
				maxZ = Z;
			if(minZ > Z)
				minZ = Z;
		}
	}
	else if(GCL_List.GetNumItem() != 0)
	{
		gcl = GCL_List.GetAt(0);
		X = gcl.X;
		Y = gcl.Y;
		Z = gcl.Z;
		
		maxX = minX = X;
		maxY = minY = Y;
		maxZ = minZ = Z;
	}

	if(GCL_List.GetNumItem() != 0)
	{
		
		for(i=0; i<GCL_List.GetNumItem(); i++)
		{
			gcl = GCL_List.GetAt(i);
			X = gcl.X;
			Y = gcl.Y;
			Z = gcl.Z;
			
			if(maxX < X)
				maxX = X;
			if(minX > X)
				minX = X;
			
			if(maxY < Y)
				maxY = Y;
			if(minY > Y)
				minY = Y;
			
			if(maxZ < Z)
				maxZ = Z;
			if(minZ > Z)
				minZ = Z;	
		}
	}

	/**<max_X[0], min_X[1], max_Y[2], min_Y[3], max_Z[4], min_Z[5]*/
	if((GCP_List.GetNumItem() != 0)||(GCL_List.GetNumItem() != 0))
	{
		coeff[0] = maxX;
		coeff[1] = minX;
		coeff[2] = maxY;
		coeff[3] = minY;
		coeff[4] = maxZ;
		coeff[5] = minZ;
	}

	//So many "if", stupid process, but no problem, not important process...
}