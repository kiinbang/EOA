#pragma once

#include <ssm/include/SSMParameterCollection.h>

namespace ssm
{
	/**General function definition using ParameterCollection
	* Description : Function pointer declaration
	*@param std::vector<param::ParameterCollection>& collections: input parameters (function coefficients) consist of one or more than one collections
	*@param std::vector<double>& retVals
	*@return bool
	*/
	typedef void(*generalFunction)(const std::vector<param::ParameterCollection>& collections, std::vector<double>& retVals);
}
