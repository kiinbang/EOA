/*
* Copyright(c) 2019-2020 KI IN Bang
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files(the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions :

* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

#pragma once

#include <ssm/include/SSMDataStructures.h>
#include <ssm/include/SSMFrameCamera.h>
#include <ssm/include/SSMParameterCollection.h>

namespace data
{
	class EOP
	{
	public:
		EOP()
		{
			/// orientation and position
			params.resize(2);
			/// 3 Euler angles
			params[0].resize(3);
			/// 3D coordinates
			params[1].resize(3);

			params[0][0].setName("Omega");
			params[0][0].setDescription("Photogrammetric rotation omega about X axis");
			params[0][0].setDelta(1.0e-6);
			params[0][0].setStDev(0.0);
			params[0][0].set(0.0);

			params[0][1].setName("Phi");
			params[0][1].setDescription("Photogrammetric rotation phi about Y axis");
			params[0][1].setDelta(1.0e-6);
			params[0][1].setStDev(0.0);
			params[0][1].set(0.0);

			params[0][2].setName("Kappa");
			params[0][2].setDescription("Photogrammetric rotation kappa about Z axis");
			params[0][2].setDelta(1.0e-6);
			params[0][2].setStDev(0.0);
			params[0][2].set(0.0);

			params[1][0].setName("X0");
			params[1][0].setDescription("EOP position X0");
			params[1][0].setDelta(1.0e-6);
			params[1][0].setStDev(0.0);
			params[1][0].set(0.0);

			params[1][1].setName("Y0");
			params[1][1].setDescription("EOP position Y0");
			params[1][1].setDelta(1.0e-6);
			params[1][1].setStDev(0.0);
			params[1][1].set(0.0);

			params[1][2].setName("Z0");
			params[1][2].setDescription("EOP position Z0");
			params[1][2].setDelta(1.0e-6);
			params[1][2].setStDev(0.0);
			params[1][2].set(0.0);
		}

		void setPC(const Point3D& perspectiveCenter)
		{
			params[1][0].set(perspectiveCenter(0));
			params[1][1].set(perspectiveCenter(1));
			params[1][2].set(perspectiveCenter(2));
		}

		Point3D getPC() const
		{
			Point3D pc;
			pc(0) = params.get(1)[0].get();
			pc(1) = params.get(1)[1].get();
			pc(2) = params.get(1)[2].get();

			return pc;
		}

		void setEulerAngles(const double omega, const double phi, const double kappa)
		{
			params[0][0].set(omega);
			params[0][1].set(phi);
			params[0][2].set(kappa);
		}

		void getEulerAngles(double& omega, double& phi, double& kappa) const
		{
			omega = params.get(0)[0].get();
			phi = params.get(0)[1].get();
			kappa = params.get(0)[2].get();
		}

		param::ParameterCollection getParameters() const
		{
			return params;
		}

		void setParameters(const param::ParameterCollection& newParams)
		{
			params = newParams;
		}

	private:
		/// Parameters
		param::ParameterCollection params;
	};

	struct Photo
	{
		/// Camera
		std::shared_ptr<sensor::Camera> cam;
		/// Exterior orientation parameters
		EOP eop;
		/// 2D points observed in a photo
		PhotoObs pts;
	};

	typedef std::vector<Photo> PhotoData;
}
