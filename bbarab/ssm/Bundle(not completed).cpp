#include <ssm/include/Bundle.h>

#include <sstream>

#define PI 3.141592654

//////////////////////////////////////////////////////////////////////
// Construction/Destruction  [SMParam]
//////////////////////////////////////////////////////////////////////

void clear(std::stringstream& str)
{
	str.clear();
	str.str("");
}

SMParam::SMParam()
{
	A = B = C = NULL;
	Initialize();
}

SMParam::SMParam(unsigned int Anum, unsigned int Bnum, unsigned int Cnum)
{
	A = B = C = NULL;
	Initialize(Anum, Bnum, Cnum);
}

SMParam::SMParam(const SMParam& copy)
{
	A = B = C = NULL;
	Copy(copy);
}

void SMParam::Copy(const SMParam& copy)
{
	A_num_param = copy.A_num_param;
	B_num_param = copy.B_num_param;
	C_num_param = copy.C_num_param;

	Initialize(A_num_param, B_num_param, C_num_param);
	
	if(copy.A != NULL)
	{
		for(unsigned int i=0; i<A_num_param; i++)
		{A[i] = copy.A[i];}
	}
	if(copy.B != NULL)
	{
		for(unsigned int i=0; i<B_num_param; i++)
		{B[i] = copy.B[i];}
	}
	if(copy.C != NULL)
	{
		for(unsigned int i=0; i<C_num_param; i++)
		{C[i] = copy.C[i];}
	}

	c = copy.c;
	psi = copy.psi;

	SHIFT_X = copy.SHIFT_X;
	SCALE_X = copy.SCALE_X;
	SHIFT_Y = copy.SHIFT_Y;
	SCALE_Y = copy.SCALE_Y;
	SHIFT_Z = copy.SHIFT_Z;
	SCALE_Z = copy.SCALE_Z;
	SHIFT_col = copy.SHIFT_col;
	SCALE_col = copy.SCALE_col;
	SHIFT_row = copy.SHIFT_row;
	SCALE_row = copy.SCALE_row;
}

SMParam::~SMParam()
{
	Initialize(0,0,0);
}

void SMParam::operator=(const SMParam& copy)
{
	Copy(copy);
}

void SMParam::Initialize(unsigned int Anum, unsigned int Bnum, unsigned int Cnum)
{
	A_num_param = Anum; B_num_param = Bnum; C_num_param = Cnum;

	if(A != NULL)
	{delete[] A;}
	if(B != NULL)
	{delete[] B;}
	if(C != NULL)
	{delete[] C;}

	if(0 != A_num_param)
	{A = new double[A_num_param];}
	if(0 != B_num_param)
	{B = new double[B_num_param];}
	if(0 != C_num_param)
	{C = new double[C_num_param];}
	
	for(unsigned int i=0;i<A_num_param;i++) 
	{A[i]=0.;}
	for(unsigned int i=0;i<B_num_param;i++) 
	{B[i]=0.;}
	for(unsigned int i=0;i<C_num_param;i++)
	{C[i]=0.;}

	c=1.;
	psi = 0.;

	SHIFT_X = SHIFT_Y = SHIFT_Z = SHIFT_col = SHIFT_row = 0.;
	SCALE_X = SCALE_Y = SCALE_Z = SCALE_col = SCALE_row = 1.;
}

void SMParam::Initialize()
{
	Initialize(_NUM_PARAM_, _NUM_PARAM_, _NUM_PARAM_);
}
//////////////////////////////////////////////////////////////////////
// Construction/Destruction [SMModifiedParallel]
//////////////////////////////////////////////////////////////////////
//[modified parallel projection]
//x = A1X + A2Y + A3Z + A4
//y = (A5X + A6Y + A7Z + A8)/(1+(tan(p)/c)(A5X + A6Y + A7Z + A8))
	
//******************************************************************
//x+Vx-Fo = (px/pA1)dA1 + (px/pA2)dA2 + (px/pA3)dA3 + (px/pA4)dA4
//y+Vy-Go = (py/pA5)dA5 + (py/pA6)dA6 + (py/pA7)dA7 + (py/pA8)dA8
//******************************************************************

SMModifiedParallel::SMModifiedParallel()
{
	bNormalized = false;
	GCP_sd_threshold = 1.0;
}

SMModifiedParallel::~SMModifiedParallel()
{
}

void SMModifiedParallel::Cal_PDs(_GCP_ G, double* ret1, double* ret2, unsigned int index)
{
	SMParam param = param_list[index];
	double dG_dA5, dG_dA6, dG_dA7, dG_dA8;
	double dG_dalpha;

	double p = param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7];
	double q = 1 + param.A[8]*p;
	double q2 = q*q;
	double p2 = p*p;

	dG_dA5 = (G.X)/q2;
	dG_dA6 = (G.Y)/q2;
	dG_dA7 = (G.Z)/q2;
	dG_dA8 = 1.0/q2;
	dG_dalpha = -p2/q2;

	//A1~A4
	ret1[0] = G.X;
	ret1[1] = G.Y;
	ret1[2] = G.Z;
	ret1[3] = 1.;
	//A5~A8, alpha
	ret1[4] = 0.;
	ret1[5] = 0.;
	ret1[6] = 0.;
	ret1[7] = 0.;
	ret1[8] = 0.;

	//X,Y,Z
	ret1[9]  = param.A[0];
	ret1[10] = param.A[1];
	ret1[11] = param.A[2];

	//A1~A4
	ret2[0] = 0.;
	ret2[1] = 0.;
	ret2[2] = 0.;
	ret2[3] = 0.;
	//A5~A8, alpha
	ret2[4] = dG_dA5;
	ret2[5] = dG_dA6;
	ret2[6] = dG_dA7;
	ret2[7] = dG_dA8;
	ret2[8] = dG_dalpha;

	//X,Y,Z
	ret2[9]  = param.A[4]/q2;
	ret2[10] = param.A[5]/q2;
	ret2[11] = param.A[6]/q2;
}

void SMModifiedParallel::Cal_PDs_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int scene_index, unsigned int num_param)
{
	SMParam param = param_list[scene_index];

	_ICP_ icp1, icp2;
	_GCP_ gcp1, gcp2;
	
	gcp1.X = GL1.X;
	gcp1.Y = GL1.Y;
	gcp1.Z = GL1.Z;
	
	gcp2.X = GL2.X;
	gcp2.Y = GL2.Y;
	gcp2.Z = GL2.Z;
	
	icp1.col = Func_col(gcp1, scene_index);
	icp1.row = Func_row(gcp1, scene_index);
	
	icp2.col = Func_col(gcp2, scene_index);
	icp2.row = Func_row(gcp2, scene_index);
	
	double p1, p2, q1, q2;
	double p12, p22, q12, q22;
	p1 = param.A[4]*gcp1.X + param.A[5]*gcp1.Y + param.A[6]*gcp1.Z + param.A[7];
	p2 = param.A[4]*gcp2.X + param.A[5]*gcp2.Y + param.A[6]*gcp2.Z + param.A[7];
	q1 = 1 + param.A[8]*p1;
	q2 = 1 + param.A[8]*p2;
	p12 = p1*p1;
	p22 = p2*p2;
	q12 = q1*q1;
	q22 = q2*q2;

	//(u,v) = (row,col)
	double v_v2 = IL.col-icp2.col;
	double v_v1 = IL.col-icp1.col;
	ret[0] = gcp1.X*v_v2 - gcp2.X*v_v1;
	ret[1] = gcp1.Y*v_v2 - gcp2.Y*v_v1;
	ret[2] = gcp1.Z*v_v2 - gcp2.Z*v_v1;
	ret[3] = -(icp2.col-icp1.col);
	double u_u1 = IL.row-icp1.row;
	double u2_u = icp2.row-IL.row;
	ret[4] = (u_u1)*gcp2.X/q22 + (u2_u)*gcp1.X/q12;
	ret[5] = (u_u1)*gcp2.Y/q22 + (u2_u)*gcp1.Y/q12;
	ret[6] = (u_u1)*gcp2.Z/q22 + (u2_u)*gcp1.Z/q12;
	ret[7] = (IL.row-icp1.row)/q22 + (icp2.row-IL.row)/q12;
		
	ret[8] = (IL.row-icp2.row)*(-p22/q22) + (icp2.row-icp1.row)*(-p12/q12);

	//X1,Y1,Z1, X2,Y2,Z2
	//*
	double *ret11, *ret12;
	double *ret21, *ret22;
	ret11 = new double[num_param+3];
	ret12 = new double[num_param+3];
	ret21 = new double[num_param+3];
	ret22 = new double[num_param+3];
	Cal_PDs(gcp1, ret11, ret12, scene_index);
	Cal_PDs(gcp2, ret21, ret22, scene_index);
	ret[9]  = (icp2.row-IL.row)*ret12[9 ]+(IL.col-icp2.col)*param.A[0];
	ret[10] = (icp2.row-IL.row)*ret12[10]+(IL.col-icp2.col)*param.A[1];
	ret[11] = (icp2.row-IL.row)*ret12[11]+(IL.col-icp2.col)*param.A[2];
	
	ret[12] = (IL.row-icp1.row)*ret22[9 ]+(icp1.col-IL.col)*param.A[0];
	ret[13] = (IL.row-icp1.row)*ret22[10]+(icp1.col-IL.col)*param.A[1];
	ret[14] = (IL.row-icp1.row)*ret22[11]+(icp1.col-IL.col)*param.A[2];
}

_GCP_ SMModifiedParallel::GetImage2Ground(_GCP_ G, _ICP_ I1, _ICP_ I2, unsigned int index1, unsigned int index2, unsigned int maxiter, double sd, unsigned int num_param)
{
	double *ret1, *ret2;
	ret1 = new double[num_param+3];
	ret2 = new double[num_param+3];

	math::Matrixd A,L,X;
	A.resize(4,3); L.resize(4,1);

	unsigned int iter=0;
	do{
		iter++;
		Cal_PDs(G, ret1, ret2, index1);
		
		A(0,0) = ret1[9];
		A(0,1) = ret1[10];
		A(0,2) = ret1[11];
		
		A(1,0) = ret2[9];
		A(1,1) = ret2[10];
		A(1,2) = ret2[11];
		
		Cal_PDs(G, ret1, ret2, index2);
		
		A(2,0) = ret1[9];
		A(2,1) = ret1[10];
		A(2,2) = ret1[11];
		
		A(3,0) = ret2[9];
		A(3,1) = ret2[10];
		A(3,2) = ret2[11];
		
		L(0,0) = I1.row - Func_row(G,index1);
		L(1,0) = I1.col - Func_col(G,index1);
		
		L(2,0) = I2.row - Func_row(G,index2);
		L(3,0) = I2.col - Func_col(G,index2);
		
		ssm::LeastSquare LSM;
		X = LSM.runLeastSquare_Fast(A, L);
		
		G.X += X(0,0);
		G.Y += X(1,0);
		G.Z += X(2,0);
		
	}while(iter<maxiter&&(X(0,0)>sd||X(1,0)>sd||X(2,0)>sd));
	
	return G;
}

SMParam SMModifiedParallel::ParamDenormalization(const SMParam& param0, const unsigned int num_param)
{
	SMParam param = param0;

	//double* Nparam = SMData.DataNoramlization();
	//SHIFT_X[0], SCALE_X[1], 
	//SHIFT_Y[2], SCALE_Y[3], 
	//SHIFT_Z[4], SCALE_Z[5], 
	//SHIFT_col[6], SCALE_col[7], 
	//SHIFT_row[8], SCALE_row[9]

	double* A = new double[num_param]; 
	for(unsigned int i=0; i<num_param; i++) 
		A[i] = param.A[i];

	double alpha = param.A[8];

	param.A[0] = A[0]*param.SCALE_X/param.SCALE_row;
	param.A[1] = A[1]*param.SCALE_Y/param.SCALE_row;
	param.A[2] = A[2]*param.SCALE_Z/param.SCALE_row;
	param.A[3] = ((A[3] 
		+ A[0]*param.SHIFT_X*param.SCALE_X 
		+ A[1]*param.SHIFT_Y*param.SCALE_Y 
		+ A[2]*param.SHIFT_Z*param.SCALE_Z)/param.SCALE_row) 
		- param.SHIFT_row;

	double k = (1/param.SCALE_col) - alpha*param.SHIFT_col;

	param.A[8] = alpha/k;
	
	double l = k/(1+((alpha/k)*param.SHIFT_col));
	
	param.A[4] = l*A[4]*param.SCALE_X;
	param.A[5] = l*A[5]*param.SCALE_Y;
	param.A[6] = l*A[6]*param.SCALE_Z;
	param.A[7] = l*(A[4]*param.SHIFT_X*param.SCALE_X
				  + A[5]*param.SHIFT_Y*param.SCALE_Y 
				  + A[6]*param.SHIFT_Z*param.SCALE_Z
				  + A[7] - param.SHIFT_col/k);
	
	param.SHIFT_X = 0;
	param.SCALE_X = 1;
	param.SHIFT_Y = 0;
	param.SCALE_Y = 1;
	param.SHIFT_Z = 0;
	param.SCALE_Z = 1;
	param.SHIFT_col = 0;
	param.SCALE_col = 1;
	param.SHIFT_row = 0;
	param.SCALE_row = 1;

	return param;
}

std::string SMModifiedParallel::DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, std::string outfilename, double pixelsize)
{
	unsigned int num_param = 9;

	std::string retval;

	retval = "\r\n\r\n[Modified Parallel Projection Modeling]\r\n";
	retval += "x = A1X + A2Y + A3Z + A4\r\n";
	retval += "y = (A5X + A6Y + A7Z + A8)/(1+(alpha)(A5X + A6Y + A7Z + A8))\r\n";
	retval += "(alpha = tan(psi)/c)\r\n\r\n";
	
	if(bNormalized == true) retval += "(***Data is normalized***)\r\n";
	
	/**
	*Calculate parameter approximation
	*/
	//retval += Cal_Param_Init_Value(); <--This func is useless if there is not enough point data.
	retval += "\r\n\r\n[Init_value of Parameters]\r\n";
	retval += "///////////////////////////////////////////////////////////////////\r\n";
	///////////////////////////////
	//Original Data backup
	///////////////////////////////
	retval += DoInitModeling(num_param-1);
	///////////////////////////////////////
	//Initial data copy for the iteration
	///////////////////////////////////////
	twin_SMData = SMData;
	///////////////////////////////////////
	retval += "///////////////////////////////////////////////////////////////////\r\n";

	FileOut(outfilename,retval);
		
	retval += Run(num_param, num_max_iter, maximum_sd, maximum_diff, outfilename, pixelsize);
	
	return retval;
}

std::string SMModifiedParallel::Run(unsigned int num_param, unsigned int num_max_iter, double maximum_sd, double maximum_diff, std::string outfilename, double pixelsize)
{
	std::string param_out, gcp_out, gcl_out, Nimg, Corrimg;
	GenerateFileName(outfilename, param_out, gcp_out, gcl_out, Nimg, Corrimg);

	std::string eachiterout="";
	std::string retval;

	/**
	*Nonlinear Least square
	*AX = L + V
	*A: Jacobian matrix
	*/
	
	/*******************************************************************
	*[Modified Parallel Model]
	*x = A1X + A2Y + A3Z + A4
	*y = A5X + A6Y + A7Z + A8/(1+(alpha)(A5X + A6Y + A7Z + A8))
	********************************************************************/
	
	double before_var, current_var;
	double diff_var;
	bool var_increase = false;
	ssm::LeastSquare LS;
	/*****************************************************************************************/
	//Iteration termination[criteria]
	/*****************************************************************************************/
	// 1) maximum iterations check
	// 2) maximum correction(elements of X matrix) check
	// 3) reference variance(or SD) increasing check : diverging
	// 4) checking the change in the reference variance between iterations (less than 1%)
	/*****************************************************************************************/
	for(unsigned int num_iter=0;num_iter<num_max_iter;num_iter++)
	{
		std::stringstream stream;
		eachiterout = "";
		math::Matrixd Xmat;
		stream << std::endl;
		stream << "****************************************" << std::endl;
		stream << "[Iteration(" << num_iter << ")]" << std::endl;
		stream << "****************************************" << std::endl;
		eachiterout += stream.str();
		
		/***********************************
		*Iteration
		************************************/
		double var;
		if(num_iter>0)
		{
		}
		LS = Iteration(Xmat,num_param,var);
		double sd = sqrt(var);
		sd = sd*pixelsize; //mm
		std::vector<double> error = LS.getErrorList();
		/***********************************/
		
		/**************************************************************************************************************/
		std::stringstream Paramst;
		Paramst << std::endl;
		Paramst << "[Iteration(" << num_iter << ")]" << std::endl;
		Paramst << UpdateUnknownParam(Xmat,num_param);
		FileOut(param_out,Paramst.str());

		if(SMData.GCP_List.size() != 0)
		{
			std::stringstream points;
			points << std::endl;
			points << "[Iteration(" << num_iter << ")]" << std::endl;
			points << UpdateUnknownPoint(Xmat,SMData,num_param);
			FileOut(gcp_out, points.str());
		}
		
		if(SMData.GCL_List.size() != 0)
		{
			std::stringstream lines;
			lines << std::endl;
			lines << "[Iteration(" << num_iter << ")]" << std::endl;
			lines << UpdateUnknownLine(Xmat,SMData,num_param);
			FileOut(gcl_out,lines.str());
		}
		
		/**************************************************************************************************************/
		
		double maxX;
		maxX = Xmat.getMaxabsElement();
		std::stringstream temp;
		temp << std::endl;
		temp << "[Max Xmat element] " << maxX << std::endl;
		eachiterout += temp.str();
		
		clear(temp);
		temp << std::endl << "[SD] " << sd << std::endl;
		eachiterout += temp.str();
		
		if(num_iter == 0.)
		{
			current_var = var;
		}
		else
		{
			before_var = current_var;
			current_var = var;
			diff_var = current_var - before_var;
			/*
			if(fabs(diff_var)<(0.001*current_var))
			{
				temp.Format("\r\n[Iteration STOP: The change of the reference variance is less than threshold ( 0.001*current_variance ) ]\r\n");
				eachiterout += temp;
				FileOut(outfilename,eachiterout);
				retval += eachiterout;
				break;
			}
			*/
						
			if(diff_var>0.)
			{
				if(var_increase == true)
				{
					//temp.Format("\r\n[Iteration STOP: The reference variance is increasing.]\r\n");
					//eachiterout += temp;
					//FileOut(outfilename,eachiterout);
					//retval += eachiterout;
					//break;
				}
				else
				{
					var_increase = true;
				}
				
			}
			else
			{
				var_increase = false;
				if(sd<maximum_sd)
				{
					clear(temp);
					temp << std::endl;
					temp << "[Iteration STOP: The SD is less than threshold ( " << maximum_sd << " ) ]" << std::endl;
					eachiterout += temp.str();
					FileOut(outfilename,eachiterout);
					retval += eachiterout;
					break;
				}
			}
		}
		
		if(maximum_diff>maxX/(param_list[0].SCALE_X+param_list[0].SCALE_Y+param_list[0].SCALE_Z)*3)
		{
			clear(temp);
			temp << std::endl;
			temp << "[Iteration STOP: The maximum correction(" << maxX / (param_list[0].SCALE_X + param_list[0].SCALE_Y + param_list[0].SCALE_Z) * 3;
			temp << ") is less than threshold(" << maximum_diff << ") ]" << std::endl;
			eachiterout += temp.str();
			FileOut(outfilename,eachiterout);
			retval += eachiterout;
			break;
		}
				
		FileOut(outfilename,eachiterout);
		retval += eachiterout;

	}//End of iteration

	/*****************************************************************************************/
	//Modeling Result[Points]
	/*****************************************************************************************/
	std::string deNormalCoord_editbox;
	deNormalCoord_editbox = "\n[Modeling Result]\n";
	if(SMData.GCP_List.size()>0)
	{
		std::stringstream temp;
		temp << std::endl << "[GCP List]" << std::endl;
		deNormalCoord_editbox += temp.str();
		
		for(unsigned int gcp_index=0; gcp_index<SMData.GCP_List.size(); gcp_index++)
		{
			if(bNormalized==true)
			{
				DeNormalizedCoord(SMData.GCP_List[gcp_index], param_list[0]);
			}
			clear(temp);
			temp << "[" << SMData.GCP_List[gcp_index].PID << "]\t";
			temp << SMData.GCP_List[gcp_index].X << "\t";
			temp << SMData.GCP_List[gcp_index].Y << "\t";
			temp << SMData.GCP_List[gcp_index].Z << std::endl;
			deNormalCoord_editbox += temp.str();
		}
		/////////////////////////////////////////////
		FileOut(outfilename,deNormalCoord_editbox);
		retval += deNormalCoord_editbox;
		/////////////////////////////////////////////
	}

	/*****************************************************************************************/
	//Modeling Result[Lines]
	/*****************************************************************************************/
	deNormalCoord_editbox = "\n[Modeling Result]\n";
	if(SMData.GCL_List.size()>0)
	{
		std::stringstream temp;
		temp << std::endl << "[GCL List]" << std::endl;
		deNormalCoord_editbox += temp.str();
		
		for(unsigned int gcl_index=0; gcl_index<SMData.GCL_List.size(); gcl_index++)
		{
			if(bNormalized==true)
			{
				DeNormalizedCoord(SMData.GCL_List[gcl_index], param_list[0]);
			}
			clear(temp);
			temp << "[" << SMData.GCL_List[gcl_index].LID << "(" << SMData.GCL_List[gcl_index].flag << ")]\t";
			temp << SMData.GCL_List[gcl_index].X << "\t";
			temp << SMData.GCL_List[gcl_index].Y << "\t";
			temp << SMData.GCL_List[gcl_index].Z << std::endl;
			deNormalCoord_editbox += temp.str();	
		}
		//////////////////////////////////////////////
		FileOut(outfilename,deNormalCoord_editbox);
		retval += deNormalCoord_editbox;
		//////////////////////////////////////////////
	}

	/*****************************************************************************************/
	//Modeling Result[Parameters]
	/*****************************************************************************************/
	std::string deNormalParam_editbox="";
	std::stringstream stParam;
	
	stParam << std::endl << "------[Result: " << num_param << " Parameters]------" << std::endl;
	deNormalParam_editbox += stParam.str();

	for(unsigned int param_index=0; param_index<num_param; param_index++)
	{
		clear(stParam);
		stParam << "[" << param_index << "%d],";
		deNormalParam_editbox += stParam.str();
	}
	clear(stParam);
	stParam << std::endl;
	deNormalParam_editbox += stParam.str();

	for(unsigned int n_scene=0; n_scene<SMData.SCENE_List.size(); n_scene++)
	{
		/****************************
		*Parameters de-normalizing.
		****************************/
		param_list_ori = param_list;
		if(bNormalized == true)
		{ 
			param_list_ori = param_list;
			param_list_ori[n_scene] = ParamDenormalization(param_list[n_scene], num_param);
		}
		clear(stParam);
		stParam << "[Parameters List(" << n_scene << "): " << SMData.SCENE_List[n_scene].SID << "]" << std::endl;
		deNormalParam_editbox += stParam.str();
		for(unsigned int param_index=0; param_index<num_param; param_index++)
		{
			clear(stParam);
			stParam << param_list_ori[n_scene].A[param_index] << "\t";
			deNormalParam_editbox += stParam.str();
		}
		deNormalParam_editbox += "\n";
	}
	//////////////////////////////////////////////
	FileOut(outfilename,deNormalParam_editbox);
	retval += deNormalParam_editbox;

	return retval;
}

ssm::LeastSquare SMModifiedParallel::Iteration(math::Matrixd &Xmat, unsigned int num_param, double &var)
{	
	unsigned int n_image, n_point, n_line;
	n_image = SMData.SCENE_List.size();
	n_point = SMData.GCP_List.size();
	n_line = SMData.GCL_List.size();

	ssm::LeastSquare LS;

	/// Todo: initialize least square before starting iteration
	//LS.SetDataConfig(n_image,num_param,n_point,n_line/2);

	FillMatrix_Point(LS, num_param);
	FillMatrix_Line(LS, num_param);
	FillMatrix_XYZ(LS);
	
	math::Matrixd N_dot, C_dot;
	///
	/// Todo: modify boundarySect
	///
	unsigned int boundarySect = 0;
	math::Matrixd Xdot = LS.runLeastSquare_RN_New(var, N_dot, C_dot, Xmat, boundarySect);

	return LS;	
}

void SMModifiedParallel::FillMatrix_Point_i(ssm::LeastSquare &LS, _GCP_ GP, _ICP_ IP, unsigned scene_index, math::Matrixd &a1, math::Matrixd &a2, math::Matrixd &l, math::Matrixd &w, unsigned int num_param)
{
	//**************************************
	//A point Data
	//**************************************
	l.resize(2,1,0.);
	a1.resize(2,num_param,0.);
	a2.resize(2,3,0.);
	w.resize(2,2,0.);
	
	SMParam param = param_list[scene_index];
	
	/************************
	* Elements of L matrix.
	* (row - Fo) + v
	* (col - Go) + v
	************************/
	
	l(0,0) = IP.row - Func_row(GP,scene_index);
	l(1,0) = IP.col - Func_col(GP,scene_index);
	
	/************************
	* Elements of A matrix.
	* Jacobian  matrix
	************************/
	double *PDs1 = new double[num_param+3];
	double *PDs2 = new double[num_param+3];
	Cal_PDs(GP, PDs1, PDs2, scene_index);

	for(unsigned int i=0; i<num_param; i++)
	{
		a1(0,i) = PDs1[i];
		a1(1,i) = PDs2[i];
	}

	for(unsigned int i=0; i<3; i++)
	{
		a2(0,i) = PDs1[num_param+i];
		a2(1,i) = PDs2[num_param+i];
	}
	
	//weight
	w(0,0) = 1./IP.s[0]/IP.s[0]; //s(1,1)
	w(1,1) = 1./IP.s[3]/IP.s[3]; //s(2,2)
}

void SMModifiedParallel::FillMatrix_Point(ssm::LeastSquare &LS, unsigned int num_param)
{
	unsigned int SCENE_Num = SMData.SCENE_List.size();
	unsigned int GCP_Num = SMData.GCP_List.size();
	
	for(unsigned int gcp_index=0; gcp_index<GCP_Num; gcp_index++)
	{
		_GCP_ GP = SMData.GCP_List[gcp_index];

		for(unsigned int scene_index=0; scene_index<SCENE_Num; scene_index++)
		{
			for(unsigned int point_index=0; 
			point_index<SMData.SCENE_List[scene_index].ICP_List.size();
			point_index++)
			{
				_ICP_ IP = SMData.SCENE_List[scene_index].ICP_List[point_index];
				if(GP.PID == IP.PID)
				{
					math::Matrixd a1, a2, l, w;
					l.resize(2,1,0.);
					a1.resize(2,num_param,0.);
					a2.resize(2,3,0.);
					w.resize(2,2,0.);

					FillMatrix_Point_i(LS,GP,IP,scene_index,a1,a2,l,w,num_param);

					LS.Fill_Nmat_Data_Point(a1,a2,w,l,point_index,scene_index);
					LS.fillNmat(a, w, l);
				}
			}
		}
	}
}


void SMModifiedParallel::FillMatrix_Line_i(ssm::LeastSquare &LS, _ICL_ IL, _GCL_ GLA, _GCL_ GLB, unsigned scene_index, math::Matrixd &a1, math::Matrixd &a2, math::Matrixd &a3, math::Matrixd &l, math::Matrixd &w, unsigned int num_param)
{
	//*************************
	//A Line Data
	//*************************
	l.resize(1,1,0.);
	a1.resize(1,num_param,0.);
	a2.resize(1,3,0.);
	a3.resize(1,3,0.);
	w.resize(1,1,0.);
	
	SMParam param = param_list[scene_index];
		
	double *PDs = new double[num_param+6];
	Cal_PDs_Line(GLA, GLB, IL, PDs, scene_index, num_param);

	l(0, 0) = -Func_Line(GLA, GLB, IL,scene_index);

	for(unsigned int i=0; i<num_param; i++)
	{
		a1(0,i) = PDs[i];
	}
	for(i=0; i<3; i++)
	{
		a2(0,i) = PDs[num_param+i];
	}
	for(i=0; i<3; i++)
	{
		a3(0,i) = PDs[num_param+3+i];
	}

	/**
	*Equivalent weight
	*/
	Matrixd B(1,2,0.), Q(2,2,0.);
	
	double v_1 = Func_col(GLA,scene_index);
	double u_1 = Func_row(GLA,scene_index);
	
	double v_2 = Func_col(GLB,scene_index);
	double u_2 = Func_row(GLB,scene_index);
	
	Q(0,0)=IL.s[0]*IL.s[0];
	Q(1,1)=IL.s[3]*IL.s[3];
	
	B(0,0) = v_2-v_1;
	B(0,1) = u_1-u_2;
	
	w = B%Q%(B.Transpose());
	w(0,0) = 1./w(0,0);
}

void SMModifiedParallel::FillMatrix_Line(ssm::LeastSquare &LS, unsigned int num_param)
{
	unsigned int SCENE_Num = SMData.SCENE_List.size();
	unsigned int GCL_Num = SMData.GCL_List.size();
	unsigned int GCP_Num = SMData.GCP_List.size();
	
	for(unsigned int gcl_index=0; gcl_index<GCL_Num; gcl_index++)
	{
		_GCL_ GLA = SMData.GCL_List[gcl_index];
		if(GLA.flag == "A")
		{
			for(unsigned int gcl_index2=0; gcl_index2<GCL_Num; gcl_index2++)
			{
				_GCL_ GLB = SMData.GCL_List[gcl_index2];
				if((GLA.LID == GLB.LID)&&(GLB.flag == "B"))
				{
					for(unsigned int scene_index=0; scene_index<SCENE_Num; scene_index++)
					{
						for(unsigned int icl_index=0; 
						icl_index<SMData.SCENE_List[scene_index].ICL_List.size();
						icl_index++)
						{
							unsigned int line_index;
							_ICL_ IL = SMData.SCENE_List[scene_index].ICL_List[icl_index];
							if(GLA.LID == IL.LID)
							{								
								math::Matrixd a1, a2, a3, l, w;								
								
								if(IL.flag != "C")
								{
									_GCP_ GP;
									unsigned int index_ab;
									
									if(IL.flag == "A")
									{
										GP._COPY_(GLA);
										index_ab = gcl_index;
									}
									else if(IL.flag == "B")
									{
										GP._COPY_(GLB);
										index_ab = gcl_index2;
									}
									else{break;}

									_ICP_ IP(IL);
									
									l.resize(2,1,0.);
									a1.resize(2,num_param,0.);
									a2.resize(2,3,0.);
									w.resize(2,2,0.);
									
									FillMatrix_Point_i(LS,GP,IP,scene_index,a1,a2,l,w,num_param);

									math::Matrixd a2_line;
									a2_line.resize(1,6,0.);
									
									if(IL.flag == "A")
									{
										a2_line.insert(0,0,a2);
										line_index = index_ab/2;
									}
									else if(IL.flag == "B")
									{
										a2_line.insert(0,3,a2);
										line_index = (index_ab-1)/2;
									}

									LS.Fill_Nmat_Data_PointofLine(a1,a2_line,w,l,line_index,scene_index);
								}
								else
								{
									l.resize(1,1,0.);
									a1.resize(1,num_param,0.);
									a2.resize(1,3,0.);
									a3.resize(1,3,0.);
									w.resize(1,1,0.);
									
									FillMatrix_Line_i(LS,IL,GLA,GLB,scene_index,a1,a2,a3,l,w,num_param);

									a2.AddCol(a3);
									LS.Fill_Nmat_Data_Line(a1,a2,w,l,line_index,scene_index);
								}								
							}
						}
					}
				}
			}
		}
	}
}

void SMModifiedParallel::FillMatrix_XYZ(ssm::LeastSquare &LS)
{
	unsigned int GCP_Num = SMData.GCP_List.size();
	unsigned int GCL_Num = SMData.GCL_List.size();
	unsigned int SCENE_Num = SMData.SCENE_List.size();

	for(unsigned int gcp_index=0; gcp_index<GCP_Num; gcp_index++)
	{
		_GCP_ GP_init = SMData.GCP_List[gcp_index];
		_GCP_ GP_ori = twin_SMData.GCP_List[gcp_index];
		math::Matrixd a2, l, w;
		l.resize(3,1,0.);
		a2.resize(3,3,0.);
		w.resize(3,3,0.);

		if((GP_init.s[0]<10)&&(GP_init.s[4]<10)&&(GP_init.s[8]<10))
		{
			FillMatrix_XYZ_i(LS,GP_init,GP_ori,0,a2,l,w);
			LS.Fill_Nmat_Data_XYZ(a2,w,l,gcp_index);
		}
		
	}

	for(unsigned int gcl_index=0; gcl_index<GCL_Num; gcl_index++)
	{
		_GCL_ GL_init = SMData.GCL_List[gcl_index];
		_GCL_ GL_ori = twin_SMData.GCL_List[gcl_index];
		_GCP_ GP_init, GP_ori;
		GP_init._COPY_(GL_init);
		GP_ori._COPY_(GL_ori);

		math::Matrixd a2, l, w;
		l.resize(3,1,0.);
		a2.resize(3,3,0.);
		w.resize(3,3,0.);
		
		if((GP_init.s[0]<1)&&(GP_init.s[4]<1)&&(GP_init.s[8]<1))
		{
			
			FillMatrix_XYZ_i(LS,GP_init,GP_ori,0,a2,l,w);
			
			unsigned int line_index;
			math::Matrixd a2_line;
			a2_line.resize(1,6,0.);
			
			if(GL_init.flag == "A")
			{
				a2_line.insert(0,0,a2);
				line_index = gcl_index/2;
			}
			else if(GL_init.flag == "B")
			{
				a2_line.insert(0,3,a2);
				line_index = (gcl_index-1)/2;
			}
			
			LS.Fill_Nmat_Data_XYZofLine(a2_line,w,l,line_index);
		}
	}
}

void SMModifiedParallel::FillMatrix_XYZ_i(ssm::LeastSquare &LS, _GCP_ GP_init, _GCP_ GP_ori, unsigned scene_index, math::Matrixd &a2, math::Matrixd &l, math::Matrixd &w)
{
	//**************************************
	//A point Data
	//**************************************
	//00:obs
	//0:init
	//X0+dX=X00+V
	//dx=(X00-X0)+V
	a2.resize(3,3,0.);
	l.resize(3,1,0.);
	w.resize(3,3,0.);

	SMParam param = param_list[scene_index];
	
	a2.Identity(1.);

	l(0,0)=GP_ori.X - GP_init.X;
	l(1,0)=GP_ori.Y - GP_init.Y;
	l(2,0)=GP_ori.Z - GP_init.Z;

	w(0,0) = 1/GP_init.s[0]/GP_init.s[0];
	w(1,1) = 1/GP_init.s[4]/GP_init.s[4];
	w(2,2) = 1/GP_init.s[8]/GP_init.s[8];
}

std::string SMModifiedParallel::DoInitModeling(unsigned int num_param_)
{
	std::string retval;

		retval="[Modified Parallel Projection Init_Params (BA)]\r\n";
		retval="[Approximate the parameters using Parallel Projection(A1~A8)]\r\n";
		
		retval += "[parallel projection]\r\n";
		retval += "x = A1X + A2Y + A3Z + A4\r\n";
		retval += "y = A5X + A6Y + A7Z + A8\r\n\r\n";
	
	/*********************************
	* Data Normalization
	*********************************/
	
	unsigned int i, j, k;

	if(bNormalized == true) 
	{
		retval += "(***Data is normalized***)\r\n";
		DataNormalization();	
	}
	else 
	{		
		for(i=0; i<SMData.SCENE_List.size(); i++)
		{
			SMParam temp;
			param_list.AddTail(temp);
		}
	} 

	//**************************************
	//x = A1X + A2Y + A3Z + A4
	//y = A5X + A6Y + A7Z + A8
	//**************************************
	//Make A and L matrix
	//Matrix Group
	
	unsigned int SCENE_Num = SMData.SCENE_List.size();

	
	std::vector<_GCP_> &gcp = SMData.GCP_List;
	unsigned int GCP_Num = gcp.size();
	
	unsigned int cur_rows;
	unsigned int GP_position;
	
	math::Matrixd Amat, Lmat, Wmat;
	Amat.resize(0,0,0.);
	Lmat.resize(0,0,0.);
	Wmat.resize(0,0,0.);
	
	math::Matrixd a, l, w;
	
	for(k=0; k<SCENE_Num; k++)
	{
		math::Matrixd a1, a2, l, w;
		
		_SCENE_ scene = SMData.SCENE_List[k];
		
		std::vector<_ICP_> &icp = scene.ICP_List;
		unsigned int ICP_Num = icp.size();
		
		SMParam _param_ = param_list[k];
		
		//**************************************
		//Point Data
		//**************************************
		if(GCP_Num != 0)
		{
			for(i=0; i<GCP_Num; i++)
			{
				l.resize(2,1,0.);
				a1.resize(2,num_param_,0.);
				a2.resize(2,3,0.);
				w.resize(2,2,0.);
				
				_GCP_ GP = gcp.GetAt(i);
				
				double s_sum = 0.;
				for(unsigned int index=0; index<9; index++)
					s_sum += GP.s[index];
				if((s_sum/3)>GCP_sd_threshold)
				{
					continue;
				}
				
				for(j=0; j<ICP_Num; j++)
				{
					_ICP_ IP = icp.GetAt(j);
					
					if(IP.PID == GP.PID)
					{
						GP_position = i;
						
						l(0,0) = IP.row;
						l(1,0) = IP.col;
						
						//u(row)
						a1(0,0) = GP.X;
						a1(0,1) = GP.Y;
						a1(0,2) = GP.Z;
						a1(0,3) = 1.;
						//v(col)
						a1(1,4) = GP.X;
						a1(1,5) = GP.Y;
						a1(1,6) = GP.Z;
						a1(1,7) = 1.;
						//X,Y,Z
						a2(0,0) = _param_.A[0];
						a2(0,1) = _param_.A[1];
						a2(0,2) = _param_.A[2];
						
						a2(1,0) = _param_.A[0];
						a2(1,1) = _param_.A[0];
						a2(0,2) = _param_.A[0];
						
						//weight
						w(0,0) = 1./IP.s[0]; //s(1,1)
						//w(0,1) = 1./IP.s[1]; //s(1,2)
						//w(1,0) = 1./IP.s[2]; //s(2,1)
						w(1,1) = 1./IP.s[3]; //s(2,2)
						
						cur_rows = Amat.getRows();
						GP_position = SCENE_Num*num_param_ + GP_position*3;
						Amat.insert(cur_rows,k*num_param_,a1);
						/**
						*�ϴ�...������Ÿ�Կ����� �ʱⰪ �����Ҷ� �������� ��꿡�� �����Ѵ�.
						*Matlab ������ ���Ͽ� �������� ���� �ʱⰪ�� �Է��� �ǰ� �ְ�...
						*�����ϰ� �ۼ��� �� �׽�Ʈ�� ���ļ�, �������� ���� �κ��� �����ϴ°��� ���ķ� �̷��.
						*Amat.insert(cur_rows,GP_position,a2);
						*/
						Lmat.insert(cur_rows,0,l);
						Wmat.insert(cur_rows,cur_rows,w);
						
						break;
					} //if(IP.PID == GP.PID)
				} //for(j=0; j<ICP_Num; j++)
				
			} //for(i=0; i<GCP_Num; i++)
		} //if(GCP_Num != 0)
		
		//******************************************************************************
		//Linear Feature(ICL) Data
		//We can get the two eqs.(constraints) from one ICL data
		//(Xi,Yi,Zi) and (Xj,Yj,Zj) are the ground coord of ground control line
		//(ROWm,COLm) and (ROWn,COLn) are the image coord of linear feature on the image
		//[constraint 1]
		//k(A1(Xi-Xj)+A2(Yi-Yj)+A3(Zi-Zj))
		//-(A5(Xi-Xj)+A6(Yi-Yj)+A7(Zi-Zj))=0
		//where, k=(COLn-COLn)/(ROWn-ROWm)
		//[constraint 2]
		//k'-k(A1Xi+A2Yi+A3Zi+A4)-(A5Xi+A6Yi+A7Zi+A8)=0
		//where, k'=(COLn-COLn)/(ROWn-ROWm)*(-ROWm)+COLm
		//k'=k*(-ROWm)+COLm
		//******************************************************************************
		
		std::vector<_GCL_> &gcl = SMData.GCL_List;
		unsigned int GCL_Num = gcl.size();
		_GCL_ GL1, GL2;
		
		std::vector<_ICL_> &icl = scene.ICL_List;
		unsigned int ICL_Num = icl.size();
		
		_ICL_ IL1, IL2;
		
		if(GCL_Num != 0)
		{
			a1.resize(2,num_param_,0.);
			l.resize(2,1,0.);
			w.resize(2,2,0.);
			
			for(i=0; i<GCL_Num; i++)
			{
				int data_check = 0;
				if(gcl[i].flag == "A")
				{
					GL1 = gcl[i];
					
					double s_sum = 0.;
					for(unsigned int index=0; index<9; index++)
						s_sum += GL1.s[index];
					
					if((s_sum/3)>GCP_sd_threshold)
					{
						continue;
					}					
					
					for(j=0; j<GCL_Num; j++)
					{
						if((GL1.LID==gcl[j].LID)&&(gcl[j].flag == "B"))
						{
							GL2 = gcl[j];
							
							double s_sum = 0.;
							for(unsigned int index=0; index<9; index++)
								s_sum += GL2.s[index];
							
							if((s_sum/3)>GCP_sd_threshold)
							{
								continue;
							}	
							data_check++; //data_check == 1
							break;
						} //if((GL1.LID==gcl[j].LID)&&(gcl[j].flag == "B"))
					} //for(j=i+1; j<GCL_Num; j++)
				} //if(gcl[i].flag == "A")
				
				if(data_check == 1)
				{
					for(j=0; j<ICL_Num; j++)
					{
						if(GL1.LID == icl[j].LID)
						{
							_ICL_ temp = icl[j];
							//if("C" == icl[j].flag)<-A,B�� �����Ͽ� ���
							{
								IL1 = icl[j];
								data_check++;//data_check == 2
								for(unsigned int m=j+1; m<ICL_Num; m++)
								{
									if(GL1.LID == icl[m].LID)
									{
										//if("C" == icl[m].flag)<-A,B�� �����Ͽ� ���
										{
											IL2 = icl[m];
											data_check++; //data_check == 3
											break;
										}
									}
									
								}
							}
							
							if(data_check == 3)
								break;
						} //if(IL1.LID == gcl[j].LID)
					} //for(j=0; j<GCL_Num; j++)
				} //if(data_check == 1)				
				
				if(data_check == 3)
				{					
				/**
				*k=(v2-v1)/(u2-u1)<-----------------------------------------------------(eq1)
				*0=k(A1(X2-X1)+A2(Y2-Y1)+A3(Z2-Z1))-(A5(X2-X1)+A6(Y2-Y1)+A7(Z2-Z1))<----(eq2)
					*/
					double k1 = (IL2.col-IL1.col)/(IL2.row-IL1.row);
					
					l(0,0) = 0;
					
					a1(0,0) = k1*(GL2.X-GL1.X);
					a1(0,1) = k1*(GL2.Y-GL1.Y);
					a1(0,2) = k1*(GL2.Z-GL1.Z);
					a1(0,3) = 0.;
					a1(0,4) = -(GL2.X-GL1.X);
					a1(0,5) = -(GL2.Y-GL1.Y);
					a1(0,6) = -(GL2.Z-GL1.Z);
					a1(0,7) = 0.;
					
					/**
					*k2=k1(-u1)+v1
					*k2=k1(A1(X2)+A2(Y2)+A3(Z2)+A4)-(A5(X2)+A6(Y2)+A7(Z2)+A8)
					*/
					double k2 = k1*(-IL1.row)+IL1.col;
					
					l(1,0) = k2;
					
					a1(1,0) = -k1*GL1.X;
					a1(1,1) = -k1*GL1.Y;
					a1(1,2) = -k1*GL1.Z;
					a1(1,3) = -k1;
					a1(1,4) = GL1.X;
					a1(1,5) = GL1.Y;
					a1(1,6) = GL1.Z;
					a1(1,7) = 1.;
					
					//weight(equavalent weignt�� �����ؾ���.)
					w(0,0) = ((1./IL1.s[0])+(1./IL2.s[0]))/2; //s(1,1)
					w(1,1) = ((1./IL1.s[3])+(1./IL2.s[3]))/2; //s(2,2)
					
					cur_rows = Amat.getRows();
					Amat.insert(cur_rows,k*num_param_,a1);
					Lmat.insert(cur_rows,0,l);
					Wmat.insert(cur_rows,cur_rows,w);
					
				}//if(data_check == 2)
				} //for(i=0; i<GCL_Num; i++)
			} //if(GCL_Num != 0)
			
		} //for(k=0; k<SCENE_Num; k++)
		
		ssm::LeastSquare LS;
		RETMAT res;
		
		res = LS.RunLeastSquare(Amat, Lmat);
		
		std::string stParam;
		stParam.Format("[--INITIAL VALUE--]\r\n");
		//Unknown Matrix
		for(i=0; i<SCENE_Num; i++)
		{
			stParam.Format("---Scene\t[%d]---\r\n",i);
			retval += stParam;
			for(j=0; j<num_param_; j++)
			{
				double value = res.X(i*num_param_+j);
				param_list[i].A[j] = value;
			}
			
			stParam.Format("[A1]%le\t[A2]%le\t[A3]%le\t[A4]%le\r\n",param_list[i].A[0],param_list[i].A[1],param_list[i].A[2],param_list[i].A[3]);
			retval += stParam;
			stParam.Format("[A5]%le\t[A6]%le\t[A7]%le\t[A8]%le\r\n",param_list[i].A[4],param_list[i].A[5],param_list[i].A[6],param_list[i].A[7]);
			retval += stParam;
			stParam.Format("[tan(roll)/c]%le\r\n",param_list[i].A[8]);
			retval += stParam;
		}
		
		return retval;
}

void SMModifiedParallel::FileOut(std::string fname, std::string st)
{
	FILE* outfile;
	outfile = NULL;
	outfile = fopen(fname,"a");
	if(outfile == NULL) 
		outfile = fopen(fname,"w");
	fprintf(outfile,st);
	fclose(outfile);
}

void SMModifiedParallel::DeNormalizedCoord(_GCP_ &G, SMParam param)
{
	G.X = G.X/param.SCALE_X - param.SHIFT_X;
	G.Y = G.Y/param.SCALE_Y - param.SHIFT_Y;
	G.Z = G.Z/param.SCALE_Z - param.SHIFT_Z;
}
void SMModifiedParallel::DeNormalizedCoord(_ICP_ &I, SMParam param)
{
	I.row = I.row/param.SCALE_row - param.SHIFT_row;
	I.col = I.col/param.SCALE_col - param.SHIFT_col;
}
void SMModifiedParallel::DeNormalizedCoord(_GCL_ &G, SMParam param)
{
	G.X = G.X/param.SCALE_X - param.SHIFT_X;
	G.Y = G.Y/param.SCALE_Y - param.SHIFT_Y;
	G.Z = G.Z/param.SCALE_Z - param.SHIFT_Z;
}

void SMModifiedParallel::DataNormalization()
{
	//Normalized Coord = (Original Coord + SHIFT)*SCALE
	//Original Coord = (Normalized Coord/SCALE) - SHIFT
	//SCALE = 1/(max-min)
	//SHIFT = -(max-min)/2

	unsigned int i;

	SMParam param;

	_SCENE_ scene = SMData.SCENE_List.GetAt(0);
	/**maxrow[0], minrow[1], maxcol[2], mincol[3]*/
	double coeff1[4];
	CSMDataParser::DataNormalization_ICP_ICL(scene.ICP_List,scene.ICL_List,coeff1);

	for(i=1; i<SMData.SCENE_List.size(); i++)
	{
		double temp[4];
		scene = SMData.SCENE_List.GetAt(i);
		/**maxrow[0], minrow[1], maxcol[2], mincol[3]*/
		CSMDataParser::DataNormalization_ICP_ICL(scene.ICP_List,scene.ICL_List,temp);

		if(coeff1[0]<temp[0])
			coeff1[0] = temp[0];
		if(coeff1[1]>temp[1])
			coeff1[1] = temp[1];
		if(coeff1[2]<temp[2])
			coeff1[2] = temp[2];
		if(coeff1[3]>temp[3])
			coeff1[3] = temp[3];
	}
	/*
	param.SHIFT_row = -(coeff1[0]+coeff1[1])/2;
	param.SCALE_row = 1./(coeff1[0]+param.SHIFT_row);

	param.SHIFT_col = -(coeff1[2]+coeff1[3])/2;
	param.SCALE_col = 1./(coeff1[2]+param.SHIFT_col);
	*/
	double max, min;
	if(coeff1[0]>coeff1[2]) max = coeff1[0]; else max = coeff1[2];
	if(coeff1[1]<coeff1[3]) min = coeff1[1]; else min = coeff1[3];
	
	param.SHIFT_row = param.SHIFT_col = -(max+min)/2;
	param.SCALE_row = param.SCALE_col = 2./(max-min);

	double coeff2[6];
	/**<max_X[0], min_X[1], max_Y[2], min_Y[3], max_Z[4], min_Z[5]*/
	CSMDataParser::DataNormalization_GCP_MatlabGCL(SMData.GCP_List,SMData.GCL_List,coeff2);

	
	param.SHIFT_X = -(coeff2[0]+coeff2[1])/2;
	param.SCALE_X = 2./(coeff2[0]-coeff2[1]);

	param.SHIFT_Y = -(coeff2[2]+coeff2[3])/2;
	param.SCALE_Y = 2./(coeff2[2]-coeff2[3]);

	param.SHIFT_Z = -(coeff2[4]+coeff2[5])/2;
	param.SCALE_Z = 2./(coeff2[4]-coeff2[5]);
/*
	double max_, min_;
	if(coeff2[0]<coeff2[2])
	{
		if(coeff2[2]<coeff2[4])
		{
			max_ = coeff2[4];
		}
		else
		{
			max_ = coeff2[2];
		}
	}
	else
	{
		if(coeff2[0]<coeff2[4])
		{	
			max_ = coeff2[4];
		}
		else
		{
			max_ = coeff2[0];
		}

	}

	if(coeff2[1]>coeff2[3])
	{
		if(coeff2[3]>coeff2[5])
		{
			min_ = coeff2[5];
		}
		else
		{
			min_ = coeff2[3];
		}
	}
	else
	{
		if(coeff2[1]>coeff2[5])
		{	
			min_ = coeff2[5];
		}
		else
		{
			min_ = coeff2[1];
		}

	}

	param.SHIFT_X = -(max+min)/2;
	param.SCALE_X = 2./(max-min);

	param.SHIFT_Y = -(max+min)/2;
	param.SCALE_Y = 2./(max-min);

	param.SHIFT_Z = -(max+min)/2;
	param.SCALE_Z = 2./(max-min);
*/
	
	for(i=0; i<SMData.SCENE_List.size(); i++)
	{
		param_list.AddTail(param);
	}

	GenNormalizedData();
}

void SMModifiedParallel::GenNormalizedData()
{
	//Normalized Coord = (Original Coord + SHIFT)*SCALE

	SMParam param;
	
	unsigned int Num_Scene = SMData.SCENE_List.size();
	for(unsigned int scene_index=0; scene_index<Num_Scene; scene_index++)
	{
		param = param_list[scene_index];
		_SCENE_ &scene = SMData.SCENE_List[scene_index];

		unsigned int Num_ICP = scene.ICP_List.size();
		for(unsigned int icp_index=0; icp_index<Num_ICP; icp_index++)
		{
			double d = (scene.ICP_List[icp_index].row+param.SHIFT_row)*param.SCALE_row;
			scene.ICP_List[icp_index].row = d;
			d = (scene.ICP_List[icp_index].col+param.SHIFT_col)*param.SCALE_col;
			scene.ICP_List[icp_index].col = d;
			d = scene.ICP_List[icp_index].s[0]*param.SCALE_row;
			scene.ICP_List[icp_index].s[0] = d;
			d = scene.ICP_List[icp_index].s[3]*param.SCALE_col;
			scene.ICP_List[icp_index].s[3] = d;
		}

		unsigned int Num_ICL = scene.ICL_List.size();
		for(unsigned int icl_index=0; icl_index<Num_ICL; icl_index++)
		{
			scene.ICL_List[icl_index].row = (scene.ICL_List[icl_index].row+param.SHIFT_row)*param.SCALE_row;
			scene.ICL_List[icl_index].col = (scene.ICL_List[icl_index].col+param.SHIFT_col)*param.SCALE_col;
			double d;
			d = scene.ICL_List[icl_index].s[0]*param.SCALE_row;
			scene.ICL_List[icl_index].s[0] = d;
			d = scene.ICL_List[icl_index].s[3]*param.SCALE_col;
			scene.ICL_List[icl_index].s[3] = d;
		}
	}
	unsigned int Num_GCP = SMData.GCP_List.size();
	for(unsigned int gcp_index=0; gcp_index<Num_GCP; gcp_index++)
	{
		double d = (SMData.GCP_List[gcp_index].X+param.SHIFT_X)*param.SCALE_X;
		SMData.GCP_List[gcp_index].X = d;
		d = (SMData.GCP_List[gcp_index].Y+param.SHIFT_Y)*param.SCALE_Y;
		SMData.GCP_List[gcp_index].Y = d;
		d = (SMData.GCP_List[gcp_index].Z+param.SHIFT_Z)*param.SCALE_Z;
		SMData.GCP_List[gcp_index].Z = d;
		d = SMData.GCP_List[gcp_index].s[0]*param.SCALE_X;
		SMData.GCP_List[gcp_index].s[0] = d;
		d = SMData.GCP_List[gcp_index].s[4]*param.SCALE_Y;
		SMData.GCP_List[gcp_index].s[4] = d;
		d = SMData.GCP_List[gcp_index].s[8]*param.SCALE_Z;
		SMData.GCP_List[gcp_index].s[8] = d;
	}

	unsigned int Num_GCL = SMData.GCL_List.size();
	for(unsigned int gcl_index=0; gcl_index<Num_GCL; gcl_index++)
	{
		SMData.GCL_List[gcl_index].X = (SMData.GCL_List[gcl_index].X+param.SHIFT_X)*param.SCALE_X;
		SMData.GCL_List[gcl_index].Y = (SMData.GCL_List[gcl_index].Y+param.SHIFT_Y)*param.SCALE_Y;
		SMData.GCL_List[gcl_index].Z = (SMData.GCL_List[gcl_index].Z+param.SHIFT_Z)*param.SCALE_Z;
		double d;
		d = SMData.GCL_List[gcl_index].s[0]*param.SCALE_X;
		SMData.GCL_List[gcl_index].s[0] = d;
		d = SMData.GCL_List[gcl_index].s[4]*param.SCALE_Y;
		SMData.GCL_List[gcl_index].s[4] = d;
		d = SMData.GCL_List[gcl_index].s[8]*param.SCALE_Z;
		SMData.GCL_List[gcl_index].s[8] = d;
	}
}

/**
*Open ICP file<br>
*IF bDataString is true, return the contents of file in std::string format.
*/
std::string SMModifiedParallel::InputMatlabICPFile(std::string fname, bool bDataString)
{
	SMData.ParserforMatlabICPFile(fname, bDataString);
	return SMData.m_FileContents;
}

/**
*Open ICL file<br>
*IF bDataString is true, return the contents of file in std::string format.
*/
std::string SMModifiedParallel::InputMatlabICLFile(std::string fname, bool bDataString)
{
	SMData.ParserforMatlabICLFile(fname, bDataString); 
	return SMData.m_FileContents;
}

/**
*Open GCP file<br>
*IF bDataString is true, return the contents of file in std::string format.
*/
std::string SMModifiedParallel::InputMatlabGCPFile(std::string fname, bool bDataString)
{
	SMData.ParserforMatlabGCPFile(fname, bDataString); 
	return SMData.m_FileContents;
}

/**
*Open GCL file<br>
*IF bDataString is true, return the contents of file in std::string format.
*/
std::string SMModifiedParallel::InputMatlabGCLFile(std::string fname, bool bDataString)
{
	SMData.ParserforMatlabGCLFile(fname, bDataString); 
	return SMData.m_FileContents;
}

void SMModifiedParallel::SetConfigData(std::string ppppath, double c)
{
	focal_length = c;
}

void SMModifiedParallel::Init() 
{
	param_list.RemoveAll(); 
	param_list_ori.RemoveAll();
	bNormalized = FALSE;
	SMData.Init();
	twin_SMData.Init();
}

std::string SMModifiedParallel::UpdateUnknownParam(math::Matrixd &Xmat, unsigned int num_param)
{
	std::string temp;
	std::string ret="";
	
	for(unsigned int param_index=0; param_index<num_param; param_index++)
	{
		temp.Format("[%d],",param_index);
		ret += temp;
	}
	temp.Format("\n");
	ret += temp;

	for(unsigned int scene_index=0; scene_index<SMData.SCENE_List.size(); scene_index++)
	{
		temp.Format("[Parameters List(%d): %s]\n",scene_index, SMData.SCENE_List[scene_index].SID);
		ret += temp;
		for(unsigned int param_index=0; param_index<num_param; param_index++)
		{
			param_list[scene_index].A[param_index] += Xmat(scene_index*num_param+param_index);
			temp.Format("%le,",param_list[scene_index].A[param_index]);
			ret += temp;
		}
		temp.Format("\n");
		ret += temp;
	}
	return ret;
}

std::string SMModifiedParallel::UpdateUnknownPoint(math::Matrixd &Xmat, CSMDataParser& SMData, unsigned int num_param)
{
	std::string ret="";
	std::string temp;
	if(SMData.GCP_List.size()>0) {temp.Format("[GCP List]\r\n"); ret += temp;}
	for(unsigned int gcp_index=0; gcp_index<SMData.GCP_List.size(); gcp_index++)
	{
		SMData.GCP_List[gcp_index].X += Xmat(SMData.SCENE_List.size()*num_param+gcp_index*3+0);
		SMData.GCP_List[gcp_index].Y += Xmat(SMData.SCENE_List.size()*num_param+gcp_index*3+1);
		SMData.GCP_List[gcp_index].Z += Xmat(SMData.SCENE_List.size()*num_param+gcp_index*3+2);
		
		temp.Format("[%s],%lf,%lf,%lf\n",SMData.GCP_List[gcp_index].PID
			,SMData.GCP_List[gcp_index].X
			,SMData.GCP_List[gcp_index].Y
			,SMData.GCP_List[gcp_index].Z);
		ret += temp;
		
	}
	
	return ret;
}

std::string SMModifiedParallel::UpdateUnknownLine(math::Matrixd &Xmat, CSMDataParser& SMData, unsigned int num_param)
{
	std::string ret="";
	std::string temp;
	if(SMData.GCL_List.size()>0) {temp.Format("[GCL List]\r\n"); ret += temp;}
	for(unsigned int gcl_index=0; gcl_index<SMData.GCL_List.size(); gcl_index++)
	{
		SMData.GCL_List[gcl_index].X += Xmat(SMData.SCENE_List.size()*num_param+SMData.GCP_List.size()*3+gcl_index*3+0);
		SMData.GCL_List[gcl_index].Y += Xmat(SMData.SCENE_List.size()*num_param+SMData.GCP_List.size()*3+gcl_index*3+1);
		SMData.GCL_List[gcl_index].Z += Xmat(SMData.SCENE_List.size()*num_param+SMData.GCP_List.size()*3+gcl_index*3+2);
		
		temp.Format("[%s(%s)],%lf,%lf,%lf\n",SMData.GCL_List[gcl_index].LID
			,SMData.GCL_List[gcl_index].flag
			,SMData.GCL_List[gcl_index].X
			,SMData.GCL_List[gcl_index].Y
			,SMData.GCL_List[gcl_index].Z);
		ret += temp;
		
	}
	return ret;
}