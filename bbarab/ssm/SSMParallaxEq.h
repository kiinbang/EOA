#pragma once

#include <ssm/include/SSMDataStructures.h>

namespace ssm
{
	inline data::Point3D ParallaxEQ(int option, double H, double bm, double f, data::Point2D aL, data::Point2D aR)
	{
		double temp = 0.0;
		double pa;//parallax
		data::Point3D A;
		pa = aL(0) - aR(0);
		switch (option)
		{
		case 1:
			//왼쪽사진을 이용
			A(0) = bm*aL(0) / pa;
			A(1) = bm*aL(1) / pa;
			break;
		case 2:
			//오른쪽사진을 이용
			A(0) = bm + bm*aR(0) / pa;
			A(1) = bm*aR(1) / pa;
			break;
		case 3:
			//양쪽 사진의 평균을 이용
			temp = bm*aL(0) / pa;
			temp += bm + bm*aR(0) / pa;
			A(0) = temp / 2;
			temp = bm*aL(1) / pa;
			temp += bm*aR(1) / pa;
			A(1) = temp / 2;
			break;
		default:
			//양쪽 사진의 평균을 이용
			temp = bm*aL(0) / pa;
			temp += bm + bm*aR(0) / pa;
			A(0) = temp / 2;
			temp = bm*aL(1) / pa;
			temp += bm*aR(1) / pa;
			A(1) = temp / 2;
			break;
		}

		A(2) = H - bm*f / pa;

		return A;
	}
}