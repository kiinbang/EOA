/**
* @file		ssmStr.cpp
* @date		since 2000
* @author	kiinbang
* @brief	implementation of string class exported in dll
*/

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdarg.h> // for va_start
#include <string.h>
#include <strsafe.h> // for StringCbVPrintfW

#include <ssm/include/ssmStr_t.h>

namespace util
{
	/**constructor*/
	SsmStr_t::SsmStr_t(const SsmStr_t& str)
	{
		if (str.m_nLength == 0)
		{
			m_nLength = 0;
			m_pString = nullptr;
		}
		else
		{
			m_nLength = str.m_nLength;
			const int buffLength = m_nLength + 1;
			m_pString = new wchar_t[static_cast<long long>(buffLength)];
			assert(m_pString != nullptr);
			wcscpy_s(m_pString, buffLength, str.m_pString);
		}
	}

	/**constructor*/
	SsmStr_t::SsmStr_t(const wchar_t* str)
	{
		if (str == nullptr)
		{
			m_nLength = 0;
			m_pString = nullptr;
		}
		else
		{
			m_nLength = static_cast<int>(wcslen(str));
			const int buffLength = m_nLength + 1;
			m_pString = new wchar_t[buffLength];
			assert(m_pString != nullptr);
			wcscpy_s(m_pString, buffLength, str);
		}
	}

	/**constructor*/
	SsmStr_t::SsmStr_t()
	{
		m_nLength = 0;
		m_pString = nullptr;
	}

	/**destructor*/
	SsmStr_t::~SsmStr_t()
	{
		if (m_pString) delete m_pString;
	}

	/**
	 * @brief operator ==
	 * @param str string to be compared
	 * @return bool
	*/
	bool SsmStr_t::operator ==(const SsmStr_t& str)
	{
		if (!this->m_pString && !str.m_pString)
			return true;
		else if (!this->m_pString || !str.m_pString)
			return false;
		else
		{
			return wcscmp(str.m_pString, m_pString) == 0;
		}
	}

	// add more logic comparison operators as following, for example, although not efficient
	bool SsmStr_t::operator !=(const SsmStr_t& str)
	{
		return (!operator == (str));
	}

	SsmStr_t SsmStr_t::operator +(const SsmStr_t& str) const
	{
		if (!str.m_pString) return *this;
		if (!this->m_pString) return str;

		SsmStr_t newStr;
		newStr.m_nLength = static_cast<int>(wcslen(this->m_pString) + wcslen(str.m_pString));
		const int newBuffLength = newStr.m_nLength + 1;
		newStr.m_pString = new wchar_t[newBuffLength];
		const int buffLength = this->m_nLength + 1;
		wcscpy_s(newStr.m_pString, buffLength, this->m_pString);
		wcscat_s(newStr.m_pString, newBuffLength, str.m_pString);

		return newStr;
	}

	/**operator overloading*/
	void SsmStr_t::operator =(const SsmStr_t& str)
	{
		if (!str.m_pString) return;

		if (m_pString) delete m_pString;
		m_nLength = static_cast<int>(wcslen(str.m_pString));
		const int buffLength = m_nLength + 1;
		m_pString = new wchar_t[buffLength];
		assert(m_pString != nullptr);
		wcscpy_s(m_pString, buffLength, str.m_pString);
	}

	void SsmStr_t::operator +=(const wchar_t* str)
	{
		if (this->m_nLength < 1)
			*this = str;
		else
			*this += SsmStr_t(str);
	}

	void SsmStr_t::operator +=(const SsmStr_t& str)
	{
		SsmStr_t newStr = *this + str;
		*this = newStr;
	}

	wchar_t* SsmStr_t::getChar()
	{
		return m_pString;
	}

	int SsmStr_t::getLength()
	{
		return m_nLength;
	}

	// format string
	int SsmStr_t::Format(const wchar_t* format, ...)
	{
		assert(format != 0);

		int buffSize;
		int len;
		wchar_t* MaxBuf;
		for (int i = 5; ; i++)
		{
			buffSize = (int)pow(2.0, i);
			MaxBuf = new wchar_t[buffSize];
			if (!MaxBuf) 
				return 0;

			// some UNIX's do not support vsnprintf and snprintf
			va_list vl;
			va_start(vl, format);
			len = _vsnwprintf_s(MaxBuf, static_cast<size_t>(buffSize), _TRUNCATE, format, vl);
			
			if (len > 0)
				break;

			delete[] MaxBuf;
			
			if (len == 0)
				return 0;
		}

		if (!m_pString)
		{
			m_nLength = len;
			m_pString = new wchar_t[m_nLength + 1];
		}
		else if (m_nLength < len)
		{
			delete m_pString;
			m_nLength = len;
			m_pString = new wchar_t[m_nLength + 1];
		}
		if (m_pString)
			wcscpy_s(m_pString, m_nLength + 1, MaxBuf);
		else
			len = 0;

		delete[] MaxBuf;

		return len;
	}
}