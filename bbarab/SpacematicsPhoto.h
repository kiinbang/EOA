/*
 * Copyright (c) 2000-2001, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//////////////////////////
//SpacematicsPhoto.h
//made by BbaraB
//revision date 2000-06-15
//revision date 2001-03-19
//revision date 2001-04-30 photo class 복사생성자, 대입연산자 추가
//////////////////////////
//내부표정을 위한 변환함수를 통합하지 않았음.
//변환 함수를 연결시켜야함...
//////////////////////////
//Inverse변환 완성후....Photo Class 수정....요망....
//Inverse기능 추진중임...Transform참조바람...
//////////////////////////
//revision date 2002-10-28 photo class 복사생성자, 대입연산자 추가
//////////////////////////
//Camera ID 추가
//////////////////////////
#ifndef __SPACEMATICS_PHOTO_CLASS_H__
#define __SPACEMATICS_PHOTO_CLASS_H__

#include "SMMatrix.h"
#include "SpacematicsCamera.h"
#include "Transformation.h"
#include "Collinearity.h"

#define CONFORMAL 1
#define AFFINE 2
#define BILINEAR 3
#define PSEUDOAFFINE 3
#define LINEARPROJECTIVE 4
#define NONLINEARPROJECTIVE 5

//class for photo
class CPhoto
{
public:
	CPhoto();
	CPhoto(CPhoto &copy);
	void operator = (CPhoto &copy);
	virtual ~CPhoto();

public:
	//4 column: ID, col, row
	CSMMatrix<double> ROPoint;			//(Matrix)


private:
	//1: conformal 2: affine 3: bilinear(pseudoaffine)
	//4: linearprojective 5: nonlinearprojective
	int IO_Option;	//IO(2D Transformation) Method
	bool IO_Complete;	//Check IO Process
	Transformation* IO_ImageToPhoto_Transform;	//Transform(Image TO Photo)
	Transformation* IO_PhotoToImage_Transform;	//Transform(Photo TO Image)
	Conformal conformal;
	Conformal conformal_inv;
	Affine affine;
	Affine affine_inv;
	Bilinear bilinear;
	Bilinear bilinear_inv;
	Projective_linear projective_linear;
	Projective_linear projective_linear_inv;
	Projective_nonlinear projective_nonlinear;
	Projective_nonlinear projective_nonlinear_inv;
	CCamera camera;						//Camera
	Point2D<double> *fiducial_image;	//ficucial mark
	double FlightHeight;				//Mean Flight Height
	int ID;								//Photo ID
	CSMStr Name;						//Photo Name
	int num_ROP;//Number Of RO Point
	int num_ImagePoint; //Number of point
	CSMMatrix<double> m_ImagePoint;

	Affine ImageToBlock; //Coord-trans for aerotriangulation init-value
	
public:
	void SetNumROP(int num) { num_ROP = num; ROPoint.Resize(num_ROP,3); }
	int GetNumROP(void) { return num_ROP; }
	void SetNumImagePoint(int num) { num_ImagePoint = num; m_ImagePoint.Resize(num_ImagePoint,4); }
	int GetNumImagePoint(void) { return num_ImagePoint; }
	bool SetImagePoint(int index, int id, int property, double x, double y);
	CSMMatrix<double> GetImagePoint() {return m_ImagePoint;}
	CSMMatrix<double> GetROPoint(void) { return ROPoint; }
	bool IsIOComplete(void);
	bool IO(int option=2);//conformal(1), affine(2), bilinear(3), linearPR(4), nonlinearPR(5)

	//Read camera file(TXT file)
	bool ReadFiducialMarkFile(CString fname);
		
	void SetCamera(CCamera C);
	void SetFlightHeight(double FH);
	void SetCameraName(CSMStr name);
	void SetPhotoID(int id);
	bool SetFiducialImageCoord(int index, Point2D<double> fiducial);

	CCamera GetCamera(void);
	double GetFlightHeight(void);
	CSMStr GetCameraName(void);
	int GetPhotoID(void);
	int GetIOOption(void);
	Point2D<double> GetFiducial(int index);
	Point2D<double> GetImageToPhotoCoord(Point2D<double> imagecoord);
	Point2D<double> GetPhotoToImageCoord(Point2D<double> photocoord);

	//Transformation for aerotriangulation Initvalue
	Point2D<double> GetImageToBlockCoord(Point2D<double> imagecoord) {return ImageToBlock.CoordTransform(imagecoord);}
	Point2D<double> GetBlockToImageCoord(Point2D<double> blockcoord) {return ImageToBlock.InverseCoordTransform(blockcoord);}
	bool SetBlockCoordSystem(Affine trans) {ImageToBlock = trans;}

private:
	bool IO_Conformal(KnownPoint& FM, KnownPoint& FM_inv);
	bool IO_Affine(KnownPoint& FM, KnownPoint& FM_inv);
	bool IO_Bilinear(KnownPoint& FM, KnownPoint& FM_inv);
	bool IO_LinearProjective(KnownPoint& FM, KnownPoint& FM_inv);
	bool IO_NonLinearProjective(KnownPoint& FM, KnownPoint& FM_inv);
	
};

//////////////////////////////////////////////////////////////////////
#endif