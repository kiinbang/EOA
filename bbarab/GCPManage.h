/*
 * Copyright (c) 2001-2002, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/////////////////////////////////////////////////////////////////////
//GCPManage.h
//made by BangBaraBang
/////////////////////////////////////////////////////////////////////
//revision: 2001-12-03
///////////////////////////////////////////////////////////////////////
//CGCPManage : GCP management class
//CICPManage : image point management class
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
//revision: 2001-12-21
///////////////////////////////////////////////////////////////////////
//add delete method
/////////////////////////////////////////////////////////////////////////
//revision: 2002-01-17
///////////////////////////////////////////////////////////////////////
//add phto class
/////////////////////////////////////////////////////////////////////////
//revision: 2002-01-29
///////////////////////////////////////////////////////////////////////
//Normalization수정
//각축에 대한 스케일을 동일하게 적용하도록 수정
/////////////////////////////////////////////////////////////////////////
//revision: 2002-10-16
///////////////////////////////////////////////////////////////////////
//Sort 번호로 ID를 쿼리하는 경우 범위를 벗어나면 -9999값을 반환하고
//ID로 데이터의 핸들을 반환하는 함수는 ID가 -9999이면 NULL을 반환한다.
/////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GCPMANAGE_H__19F4C1A5_855E_4046_9B84_50ABEED90325__INCLUDED_)
#define AFX_GCPMANAGE_H__19F4C1A5_855E_4046_9B84_50ABEED90325__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGCPManage;
class CICPManage;

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//Ground control point coordinates class
class CGCPCoord
{
	friend class CGCPManage;
public:
	CGCPCoord();
	~CGCPCoord();

//Member variable
public:
	//East
	double x;
	//North
	double y;
	//Height
	double z;
	bool enable;
	CGCPCoord *next;
	CGCPCoord *forward;
	long ID;

//Member function
public:
	void Init(void);

};

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//Image control point coordinates class
class CICPCoord
{
	friend class CICPManage;
public:
	CICPCoord();
	~CICPCoord();

//Member variable
public:
	//column
	double x;
	//row
	double y;
	
	bool enable;
	CICPCoord *next;
	CICPCoord *forward;

//Member variable
private:
	long ID;

//Member function
private:
	void Init(void);

};

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//GCP management class
class CGCPManage  
{
public:
	CGCPManage();
	CGCPManage(CGCPManage& copy);
	virtual ~CGCPManage();

//Member variable
private:
	long num_GCP;
	CGCPCoord firstgcp;
	CGCPCoord *nextgcp;

public:
	bool normalization;
	double Scale_G_X, Scale_G_Y, Scale_G_Z;
	double Offset_G_X, Offset_G_Y, Offset_G_Z;

//Member function
public:
	bool InsertGCP(double X, double Y, double Z, long ID);
	bool DeleteGCP(long ID);
	bool GetGCPCoord(long ID, double *X, double *Y, double *Z);
	bool GetGCPCoordbySort(long sort, double *X, double *Y, double *Z);
	bool SetGCPCoord(long ID, double X, double Y, double Z);
	bool ChangeGCPID(long old_id, long new_id);
	bool GetGCPID(double X, double Y, double Z, long *ID);
	long GetGCPID(long sort);
	bool SetEnableGCP(long ID, bool boolvalue);
	bool GetEnableGCP(long ID);
	bool EmptyGCP(void);
	long GetGCPNum(void);
	void SetGCPNum(long num);
	long GetEnabledGCPNum(void);
	void GetNormalCoord(double x, double y, double z, double &nx, double &ny, double &nz);
	void GetUnNormalCoord(double nx, double ny, double nz, double &x, double &y, double &z);

	//coord normalization
	bool CoordNormalize();
	bool CoordNormalize(double scale, double OX, double OY, double OZ);
	
	
	//substitution operator
	void operator=(CGCPManage& copy);

	//Same ID check
	bool SameIDCheck(long ID);

//Member function
private:
	CGCPCoord* GetGCPHandle(long ID);
};

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//ICP management class
class CICPManage  
{
public:
	CICPManage();
	CICPManage(CICPManage& copy);
	virtual ~CICPManage();

//Member variable
private:
	long num_ICP;
	CICPCoord firsticp;
	CICPCoord *nexticp;

public:
	bool normalization;
	double Scale_I_X, Scale_I_Y;
	double Offset_I_X, Offset_I_Y;

//Member function
public:
	bool InsertICP(double X, double Y, long ID);
	bool DeleteICP(long ID);
	bool GetICPCoord(long ID, double *X, double *Y);
	bool GetICPCoordbySort(long sort, double *X, double *Y);
	bool SetICPCoord(long ID, double X, double Y);
	bool ChangeICPID(long old_id, long new_id);
	bool GetICPID(double X, double Y, long *ID);
	long GetICPID(long sort);
	bool SetEnableICP(long ID, bool boolvalue);
	bool GetEnableICP(long ID);
	bool EmptyICP(void);
	long GetICPNum(void);
	void SetICPNum(long num);
	long GetEnabledICPNum(void);
	void GetNormalCoord(double x, double y, double &nx, double &ny);
	void GetUnNormalCoord(double nx, double ny, double &x, double &y);

	//coord normalization
	void CoordNormalize();
	bool CoordNormalize(double scale, double OX, double OY);

	//substitution operator
	void operator=(CICPManage& copy);

	//Same ID check
	bool SameIDCheck(long ID);

//Member function
private:
	CICPCoord* GetICPHandle(long ID);
};

#endif // !defined(AFX_GCPMANAGE_H__19F4C1A5_855E_4046_9B84_50ABEED90325__INCLUDED_)
