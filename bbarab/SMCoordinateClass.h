/*
* Copyright (c) 1999-2001, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _COORDINATE_CLASS_
#define _COORDINATE_CLASS_

#include <iostream>
#include <math.h>
#include <string>

#define CString std::string

namespace SMATICS_BBARAB
{

	class C3DPoint
	{
	public:
		C3DPoint() {X=Y=Z=0.0f;}
		C3DPoint(double X, double Y, double Z) {this->X=X, this->Y=Y, this->Z=Z;}
		double X, Y, Z;
	};

	class C2DPoint
	{
	public:
		C2DPoint() {X=Y=0.0f;}
		C2DPoint(double X, double Y) {this->X=X, this->Y=Y;}
		double X, Y;
	};

	/**@class Point2D
	*@brief for 2D coordinate
	*/
	template<typename T>
	class Point2D  // Declare 2D coordinate struct type
	{
	public:
		T x;			// x
		T y;			// y
		Point2D() { x = T(0); y = T(0); }
		Point2D(const Point2D<T>& coord)	{ x = coord.x; y = coord.y;}
		Point2D(const T& _x, const T& _y) { x = _x; y = _y; }
		Point2D(const int n) { x = T(n); y = T(n); }
		virtual ~Point2D() { }
		//operator
		Point2D<T>& operator=(const Point2D<T>& coord) { x = coord.x; y = coord.y; return *this; }
		Point2D<T> operator+(const Point2D<T>& coord) {
			Point2D<T> r;
			r.x = x + coord.x;
			r.y = y + coord.y;
			return r;
		}
		Point2D<T> operator-(const Point2D<T>& coord) {
			Point2D<T> r;
			r.x = x - coord.x;
			r.y = y - coord.y;
			return r;
		}
		Point2D<T> operator*(const double s) {
			Point2D<T> r;
			r.x = T((double)x*s);
			r.y = T((double)y*s);
			return r;
		}
		Point2D<T> operator/(const double s) {
			Point2D<T> r;
			if(s != 0.0)
			{
				r.x = T((double)x/s);
				r.y = T((double)y/s);
			}
			else
			{
				std::runtime_error("Error in Point2D Class: Divided zero");
			}
			return r;
		}

		void operator()(const T& _x, const T& _y)	{ x = _x; y = _y; }
		void operator()(const T value) { x = value; y = value; }
	};

	/**@class Point3D
	*@brief for 3D coordinate
	*/
	template<class T>
	class Point3D  // Declare 3D coordinate struct type
	{
	public:
		T x;			// x
		T y;			// y
		T z;			// z
		Point3D() { x = T(0); y = T(0); z = T(0); }
		Point3D(const Point3D<T>& coord)	{ x = coord.x; y = coord.y; z = coord.z; }
		Point3D(const T& _x, const T& _y, const T& _z) { x = _x; y = _y; z = _z; }
		Point3D(const int n) { x = T(n); y = T(n); z = T(n);}
		virtual ~Point3D() { }
		//operator
		Point3D<T>& operator=(const Point3D<T>& coord) { x = coord.x; y = coord.y; z = coord.z; return *this; }
		Point3D<T>& operator+=(const Point3D<T>& coord) { x += coord.x; y += coord.y; z += coord.z; return *this; }
		Point3D<T> operator+(const Point3D<T>& coord) {
			Point3D<T> r;
			r.x = x + coord.x;
			r.y = y + coord.y;
			r.z = z + coord.z;
			return r;
		}

		Point3D<T> operator-(const Point3D<T>& coord) {
			Point3D<T> r;
			r.x = x - coord.x;
			r.y = y - coord.y;
			r.z = z - coord.z;
			return r;
		}
		Point3D<T> operator*(const double s) {
			Point3D<T> r;
			r.x = T((double)x*s);
			r.y = T((double)y*s);
			r.z = T((double)z*s);
			return r;
		}
		Point3D<T> operator/(const double s) {
			Point3D<T> r;
			if(s != 0.0)
			{
				r.x = T((double)x/s);
				r.y = T((double)y/s);
				r.z = T((double)z/s);
			}
			else
			{
				std::runtime_error("Error in Point3D Class: Divided zero");
			}
			return r;
		}

		void operator()(const T& _x, const T& _y, const T& _z)	{ x = _x; y = _y; z = _z; }
		void operator()(const T value) { x = value; y = value; z = value; }
		T operator*(const Point3D<T> &V)
		{
			return x * V.x +y * V.y + z * V.z;
		}

	};

	/////////////////////////////////////////////////////////////////////
	/*namespace SMATICS_BBARAB
	* 2D and 3D coordinate class without template; None Template(NT)
	*/
	//revision date: 2006.01.13
	/////////////////////////////////////////////////////////////////////
	//BANG, KI IN
	/////////////////////////////////////////////////////////////////////


	/**@class Point2D
	*@brief for 2D coordinate. NT means "no template".
	*/
	class NTPoint2D  // Declare 2D coordinate struct type
	{
	public:
		double x;			// x
		double y;			// y
		CString SceneID;
		CString PointID;
		NTPoint2D() { x = double(0); y = double(0); PointID="0"; SceneID = "";}
		NTPoint2D(const NTPoint2D& coord)	
		{ x = coord.x; y = coord.y; PointID = coord.PointID;  SceneID = coord.SceneID;}
		NTPoint2D(const double& _x, const double& _y, const CString _PointID, const CString _SceneID) 
		{ x = _x; y = _y; PointID = _PointID;   SceneID = _SceneID;}
		NTPoint2D(const int n, const CString _PointID, const CString _SceneID) 
		{ x = double(n); y = double(n); PointID = _PointID; SceneID = _SceneID;}
		virtual ~NTPoint2D() { }
		//operator
		NTPoint2D& operator=(const NTPoint2D& coord) { x = coord.x; y = coord.y; PointID = coord.PointID; SceneID = coord.SceneID; return *this; }
		NTPoint2D operator+(const NTPoint2D& coord) {
			NTPoint2D r;
			r.x = x + coord.x;
			r.y = y + coord.y;
			r.PointID = PointID;
			r.SceneID = SceneID;
			return r;
		}
		NTPoint2D operator-(const NTPoint2D& coord) {
			NTPoint2D r;
			r.x = x - coord.x;
			r.y = y - coord.y;
			r.PointID = PointID;
			r.SceneID = SceneID;
			return r;
		}
		NTPoint2D operator*(const double s) {
			NTPoint2D r;
			r.x = double(x*s);
			r.y = double(y*s);
			r.PointID = PointID;
			r.SceneID = SceneID;
			return r;
		}
		NTPoint2D operator/(const double s) {
			NTPoint2D r;
			if(s != 0.0)
			{
				r.x = double(x/s);
				r.y = double(y/s);
				r.PointID = PointID;
				r.SceneID = SceneID;
			}
			else
			{
				std::runtime_error("Error in NTPoint2D Class: Divided zero");
			}
			return r;
		}

		void operator()(const double& _x, const double& _y, const CString _PointID, const CString _SceneID)	
		{ x = _x; y = _y; PointID = _PointID; SceneID = _SceneID;}
		void operator()(const double value, const CString _PointID, const CString _SceneID) 
		{ x = value; y = value;  PointID = _PointID; SceneID = _SceneID;}

	};

	/**@class Point3D
	*@brief for 3D coordinate
	*/
	class NTPoint3D  // Declare 3D coordinate struct type
	{
	public:
		double x;			// x
		double y;			// y
		double z;			// z
		CString ID;			// point ID
		NTPoint3D() { x = double(0); y = double(0); z = double(0); ID = "0";}
		NTPoint3D(const NTPoint3D& coord)	{ x = coord.x; y = coord.y; z = coord.z; ID = coord.ID;}
		NTPoint3D(const double& _x, const double& _y, const double& _z, const CString _ID) { x = _x; y = _y; z = _z; ID = _ID;}
		NTPoint3D(const int n, const CString _ID) { x = double(n); y = double(n); z = double(n); ID = _ID;}
		virtual ~NTPoint3D() { }
		//operator
		NTPoint3D& operator=(const NTPoint3D& coord) { x = coord.x; y = coord.y; z = coord.z; ID = coord.ID; return *this; }
		NTPoint3D& operator=(const Point3D<double>& coord) { x = coord.x; y = coord.y; z = coord.z; ID = "0"; return *this; }
		NTPoint3D operator+(const NTPoint3D& coord) {
			NTPoint3D r;
			r.x = x + coord.x;
			r.y = y + coord.y;
			r.z = z + coord.z;
			r.ID = ID;
			return r;
		}

		NTPoint3D operator-(const NTPoint3D& coord) {
			NTPoint3D r;
			r.x = x - coord.x;
			r.y = y - coord.y;
			r.z = z - coord.z;
			r.ID = ID;
			return r;
		}
		NTPoint3D operator*(const double s) {
			NTPoint3D r;
			r.x = double(x*s);
			r.y = double(y*s);
			r.z = double(z*s);
			r.ID = ID;
			return r;
		}
		NTPoint3D operator/(const double s) {
			NTPoint3D r;
			if(s != 0.0)
			{
				r.x = double(x/s);
				r.y = double(y/s);
				r.z = double(z/s);
				r.ID = ID;
			}
			else
			{
				std::runtime_error("Error in NTPoint3D Class: Divided zero");
			}
			return r;
		}

		void operator()(const double& _x, const double& _y, const double& _z, const CString _ID)	{ x = _x; y = _y; z = _z; ID = _ID;}
		void operator()(const double value, const CString _ID) { x = value; y = value; z = value; ID = _ID;}

	};

}//namespace


////////////////////////////////////////////////////////////////////
#endif