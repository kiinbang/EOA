/*
 * Copyright (c) 2005-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(__BBARAB_DATA_STRUCT__)
#define __BBARAB_DATA_STRUCT__

#include <fstream>
#include "SMList.h"
#include "SMMatrix.h"

namespace SMATICS_BBARAB
{
	class ReferenceEllipsoid
	{
	public:
		ReferenceEllipsoid()
		{
			Name = "WGS84";

			//SemiMajor = 6378137.0;
			//SemiMinor = 6356752.312254;
			//Flattening = 1.0/298.257223563;

			SemiMajor = 6378137.000;
			SemiMinor = 6356752.3142;
			Flattening = 1.0/298.257223563;
		}

		CString Name;
		double SemiMajor;
		double SemiMinor;
		double Flattening;
		int zone;
		int hemi;
	};

	class CRectDouble
	{
	public:
		double bottom, top, left, right;
	};

	class _Point_
	{
	public:
		double X, Y, Z;
		int intensity;
		//CString ID;
		int nID;
		int sort;
	};

	typedef class _SM3DPOINT_ : public _Point_
	{
	public:
		double cov[9];
		//int nID;
	} SM3DPoint;
		
	class _PTS_Point_ : public _Point_
	{
	public:
		double time;
		double Pitch, Roll, Yaw;
		double GPS_X, GPS_Y, GPS_Z;
		double beta;
		double dist;
	};

	class FILEMEM
	{
	public:
		FILEMEM(int size);
		virtual ~FILEMEM();
		//Free();
		
		void Get(long pos, BYTE *temp);
		void Set(long pos, BYTE *temp);

		void Open(char* fname);
		void Close();
		/////////////////////////////////////////////////
		char* temp_path;
		int n_Size;
		fstream tempfile;
	};

	class POLYGON
	{
	public:
		virtual ~POLYGON() {vertices.RemoveAll();}
		CSMList<_Point_> vertices;
	};

	class CIRCLE
	{
	public:
		double radius;
		double X, Y, Z;
	};
	
	class CLIDARRawPoint  
	{
	public:
		CLIDARRawPoint()
		{
			dist = alpha = beta = GPS_X = GPS_Y = GPS_Z = INS_O = INS_P = INS_K = time = intensity = 0;
		}
		double dist;/**<Ranging distance between laser beam unit center to object */
		
		double alpha;/**<Rotation angle for the X axis (along the flight direction) */
		double beta;/**<Rotation angle for the Y axis (across the flight direction) */
		
		double GPS_X;/**<Coordinate X of GPS phase center */
		double GPS_Y;/**<Coordinate Y of GPS phase center */
		double GPS_Z;/**<Coordinate Z of GPS phase center */
		
		double INS_O;/**<Rotation angle for the X axis in the ground coordinate system */
		double INS_P;/**<Rotation angle for the Y axis in the ground coordinate system */
		double INS_K;/**<Rotation angle for the Z axis in the ground coordinate system */

		double time;/**<Scan time */

		int intensity;/**<intensity*/
	};

	class CLIDARCalibrationPoint : public CLIDARRawPoint
	{
	public:
		CLIDARCalibrationPoint()  : CLIDARRawPoint() {P.Resize(3,3); P.Identity(); PlaneAttitude[0] = PlaneAttitude[1] = PlaneAttitude[2] = 0.0;}

	public:
		double X, Y, Z; //ground position
		CSMMatrix<double> P;//weight matrix
		double PlaneAttitude[3];//plane attitude
	};
	
	class CLIDARPlanarPatch  
	{
	public:
		CLIDARPlanarPatch() {Availability = true;}
		//int nID;
		CString stID;
		double Vertex[9];
		double SD[9];
		CSMList<CLIDARRawPoint> PointList;
		double coeff[3];//aX + bY + cZ + 1 = 0
		double Area;//area of a patch
		char ID_A[256];
		char ID_B[256];
		char ID_C[256];
		bool Availability;
		
		double maxX, maxY, minX, minY, minZ, maxZ;
	};

	class LIDAR_CALIBRATION_PRJ_STRUCT
	{
	public:
		LIDAR_CALIBRATION_PRJ_STRUCT(){
			EQType = 1;
			MC = 1;
			Small_Sigma = 1.0e-6;
			Large_Sigma = 1.0e6;
			Diff_Sigma = 1.0e-10;
			Num_Iter = 20;
		}
	public:
		CString ConfigPath;
		CString ControlPointsPath;
		CString ControlPointsContentsPath;
		CString ControlPatchICPointPath;
		CString ControlPatchICPointContentsPath;
		CString ControlPatchICPatchPath;
		CString ControlPatchICPatchContentsPath;
		int EQType;
		int MC;
		CString ResultPath;
		double Small_Sigma;
		double Large_Sigma;
		double Diff_Sigma;
		int Num_Iter;
		ReferenceEllipsoid ellipsoid;
		///////////////////////////////////////////////////////////////////////////////
	};

}//namespace SMATICS_BBARAB

#endif // !defined(__BBARAB_DATA_STRUCT__)