/*
 * Copyright (c) 2002-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(_SPACEMATICS_TRUE_ORTHOIMAGE_)
#define _SPACEMATICS_TRUE_ORTHOIMAGE_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "./Image_Util/imagepixel.h"
#include <SMMatrix.h>
#include <utilitygrocery.h>
#include <satorthoclass.h>
#include <SMList.h>

/////////////////////////////////////////////////////////////////////////
//[Second Modification]
//
//2005: Add CSatOrthoImage Class <--Ortho gen with rigorous param
//          1)Search scan-line number
//          2)Nearest and Bilinear
//          3)Remove occlusion area(Maximum Slope/Z-buffer/Enhanced Z-buffer)
//          4)Ortho merge
//
//
//
//
//
//
// smorthoimage.h : header file
// Created by BANG, KI IN

/**
* @class CSatOrthoImage
* @brief Revision: 2005-11\n
* This class is modified for the functions of the new true ortho-photo method.\n
* @author Bang, Ki In
* @version 1.2
* @Since 2002~
* @bug N/A.
* @warning N/A.
*
* \nThis class has three true ortho-photo resampling methods:
* <ul> 
* <li> Slope method\n
*		 Slope method uses the line from each DEM point to perspective center to fine the occlusion area.\n
* <li> Z-buffer method
*	<ol>
*	<li> Normal Z-buffer method
*	<li> Enhanced Z-buffer method\n
*		 This method search the building height within searching boundary.</ol>
* </ul>\n
*/

#include <UtilityGrocery.h>
using namespace SMATICS_BBARAB;

class CSatOrthoImage : public CSatOrthoClass
{
friend class CSatOrthoImageFrame;
public:
	//CSMDEM LiDAR;/**< LIDAR DEM*/
	CString DEM_Path;
	CString DEMHDR_Path;

	CImage *OriginImage;/**< Original Image*/

	CSMList<int> List_X_index;//DEM x index list for the sorted DEM method
	CSMList<int> List_Y_index;//DEM y index list for the sorted DEM method
	
	float* m_pData;//DEM (sorted DEM)
	int* idx_x;//index for sorted DEM
	int* idx_y;//index for sorted DEM

public:
	
	/**CSatOrthoImage
	* Description	    : default constructor
	*@return : N/A
	*/
	CSatOrthoImage();

	/**~CSatOrthoImage
	* Description	    : default destructor
	*@return : N/A
	*/
	virtual ~CSatOrthoImage();

	/**CSatOrthoImage::MakeOrtho
	* Description	    : start function for the ortho-image generation 	    : 
	*@param CImage &oriImage: original image
	*@param CString parampath: EO parameters' file path
	*@param CString DEMPath: DEM file path
	*@param CString HDRPath: DEM header file path
	*@param CString OrthoPath: Ortho-image(output) file path
	*@param bool bZBuf: availability of Z-buffer method
	*@param bool bBilinear: availability of bilinear resampling
	*@param bool bRemoveOcclu: availability of removing occlusion (Enhanced Z-buffer, Line-Trace Method)
	*@param bool angle_availability: angle value instead of height in the Line-trace method
	*@param bool bHeightBuffer: height index instead Z-buffer(distances)
	*@param bool bForward: Forward projection
	*@return bool 
	*/
	bool CSatOrthoImage::MakeOrtho(CImage &oriImage, CString parampath, CString DEMPath, CString HDRPath, CString OrthoPath, bool bZBuf, bool bBilinear, bool bRemoveOcclu, bool angle_availability, bool bHeightBuffer, bool bForward, double val_fail = 0.0);

	/**MakeOrtho_SortedDEM
	* Description	    : start function for the ortho-image generation 
	*@param CImage &oriImage: original image
	*@param CString parampath: EO parameters' file path
	*@param CString OrthoPath: Ortho-image(output) file path
	*@param bool bBilinear: availability of bilinear resampling
	*@return bool 
	*/
	bool MakeOrtho_SortedDEM(CImage &oriImage, CString parampath, CString DEMPath, CString HDRPath, CString OrthoPath, bool bBilinear, double val_fail = 0.0);
	
	/**TXT2BIN
	* Description	    : To convert TXT format DEM to Binary DEM 
	*@param CString txtpath: txt file path
	*@param CString hdrpath: DEM header file path
	*@param CString binpath: Binary DEM(output) path
	*@return void
	*/
	void TXT2BIN(CString txtpath, CString hdrpath, CString binpath);
	
	/**OrthoMerge
	* Description	    : to merge same size two ortho images
	*@param CImage &image1: image1
	*@param CImage &image2: image2
	*@param CString path: parameters' file path
	*@param bool bFillHalls: availability of filling halls
	*@return bool 
	*/
	bool OrthoMerge(CImage &image1, CImage &image2, CString path, bool bFillHoles);

	/**QuickSort
	* Description		: to sort DEM (from lower point to from higher point)
	*@param SourceDEMPath : source DEM path
	*@param SortDEMPath : New sorted DEM Path
	*@return void
	*/
	void QuickSort(char* DEMPath, char* DEMHDRPath, char* SortedDEMPath);

	/**ReadEOParam
	* Description	    : to read EO parameters
	*@param CString parampath: parameters' file path
	*@return void 
	*/
	bool ReadEOParam(CString parampath);
	bool ReadOrientationImageParam(fstream &infile);
	bool ReadTimeFunctionParam(fstream &infile);

	/**ReadGCParam
	* Description	    : to read GCP coordinates
	*@param CString parampath: file path
	*@return void 
	*/
	bool ReadGCPFile(CString path, CSMList<double> *rowlist, CSMList<double> *collist);
	
protected:

	/**GetTargetBoundary
	* Description	    : to read EO parameters
	*
	*image boundary for making z-buffer and index buffer
	*to reduce the needed memory, full image size is not effective.
	*For that reason, to calculate boundary is required.
	*BOUNDARY_MARGIN is required because a image(scene) has relief displacement
	*
	*@param unsigned int &h: height of image
	*@param unsigned int &w: width of image
	*@param unsigned int &sr: starting row index of image patch
	*@param unsigned int &sc: starting column index of image patch
	*@param unsigned int &er: ending row index of image patch
	*@param unsigned int &ec: ending column index of image patch
	*@return void 
	*/
	bool GetTargetBoundary(int &h, int &w, int &sr, int &sc, int &er, int &ec);

	/**SortDEM
	* Description	    : to sort DEM
	*@return void 
	*/
	void SortDEM_Insert_SortMethod(void);
		
	/**ResampleOrtho_Slope_DiffZ
	* Description	    : ortho image resampling by slope method
	*@param CString OrthoPath: ortho image file path
	*@param bool bBilinear: availability of bilinear resampling
	*@param double MaxDiffZ: searching limit
	*@return bool 
	*/
	bool ResampleOrtho_Slope_DiffZ(CString OrthoPath, bool bBilinear, bool bOcclusion);
	bool ResampleColorOrtho_Slope_DiffZ(CString OrthoPath, bool bBilinear, bool bOcclusion);

	/**ResampleOrtho_Slope_Angle
	* Description	    : ortho image resampling by slope method using angles instead of height differences
	*@param CString OrthoPath: ortho image file path
	*@param bool bBilinear: availability of bilinear resampling
	*@param double MaxDiffZ: searching limit
	*@return bool 
	*/
	bool ResampleOrtho_Slope_Angle(CString OrthoPath, bool bBilinear, bool bOcclusion);
	bool ResampleColorOrtho_Slope_Angle(CString OrthoPath, bool bBilinear, bool bOcclusion);
		
	/**ResampleOrtho_ZBuffer_Height
	* Description	    : ortho image resampling by z-buffer method
	*@param CString OrthoPath: ortho image file path
	*@param bool bBilinear: availability of bilinear resampling
	*@param double MaxDiffZ: searching limit
	*@return bool 
	*/
	bool ResampleOrtho_ZBuffer_Height(CString OrthoPath, bool bBilinear);
	bool ResampleColorOrtho_ZBuffer_Height(CString OrthoPath, bool bBilinear);
	
	/**ResampleOrtho_EnhancedZBuffer
	* Description	    : ortho image generation by enhanced z-buffer method
	*@param CString OrthoPath: ortho image file path
	*@param bool bBilinear: availability of bilinear resampling
	*@param double MaxDiffZ: searching limit
	*@param double SearchInterval: searching iterval
	*@return bool 
	*/
	bool ResampleOrtho_EnhancedZBuffer(CString OrthoPath, bool bBilinear, bool bEnhanced );
	bool ResampleColorOrtho_EnhancedZBuffer(CString OrthoPath, bool bBilinear, bool bEnhanced);

	/**ResampleOrtho_SortedDEM
	* Description	    : ortho image generation by sorted DEM method
	*@param CString OrthoPath: ortho image file path
	*@param bool bBilinear: availability of bilinear resampling
	*@param double MaxDiffZ: searching limit
	*@return bool 
	*/
	bool ResampleOrtho_SortedDEM(CString OrthoPath, bool bBilinear);
	bool ResampleColorOrtho_SortedDEM(CString OrthoPath, bool bBilinear);

	/**ResampleOrtho_SortedDEM
	* Description	    : ortho image generation by sorted DEM method
	*@param CString OrthoPath: ortho image file path
	*@param bool bBilinear: availability of bilinear resampling
	*@param double MaxDiffZ: searching limit
	*@param double SearchInterval: searching iterval
	*@return bool 
	*/
	bool ResampleOrtho_SortedDEM_Enhanced(CString OrthoPath, bool bBilinear, double MaxDiffZ, double SearchInterval);
	bool ResampleColorOrtho_SortedDEM_Enhanced(CString OrthoPath, bool bBilinear, double MaxDiffZ, double SearchInterval);
	
	/**Readlidardem
	* Description	    : to read lidar dem
	*@param CString DEMPath: Binary DEM file path
	*@param CString DEMHDRPath: DEM header file path
	*@return bool 
	*/
	bool Readlidardem(CString DEMPath, CString DEMHDRPath);

	/**ReadSorteddem
	* Description	    : to read sorted txt dem
	*@param CString DEMPath: Binary DEM file path
	*@param CString DEMHDRPath: DEM header file path
	*@return bool 
	*/
	bool ReadSorteddem(CString DEMPath, CString DEMHDRPath);
	
	/**FillHalls
	* Description	    : to fill the halls
	*@param CImage &Image : image having halls
	*@param CImage &NewImage : new image filled the halls
	*@param long bound : window size for the filling
	*@return void 
	*/
	void FillHoles(CImage &Image, CImage &NewImage, long bound);


};

/////////////////////////////////////////////////////////////////////////////

#endif // !defined(_SPACEMATICS_TRUE_ORTHOIMAGE_)
