/////////////////////////////////////////////////////////////////////
//PhotoDataManage.h
//made by BangBaraBang
/////////////////////////////////////////////////////////////////////
//revision: 2002-10-25
///////////////////////////////////////////////////////////////////////
//GCPManage.h 에 통합되있던 photo data 관련 부분을 분리 독립시킴.
/////////////////////////////////////////////////////////////////////////

#if !defined(__SPACEMATICS_PHOTODATAMANAGE_H__)
#define __SPACEMATICS_PHOTODATAMANAGE_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GCPManage.h"

class CSMPhotoManage;

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//camera struct
typedef struct Camera
{
	//camera ID
	long ID;

	//radial distortion
	//dr = k1r^3 + k2r^5 + k3r^7
	double k1, k2, k3;
	//tangential distortion
	//dx = p1((r^2)+2(x-xp)^2)+2p2((x-xp)(y-yp))
	//dy = p2((r^2)+2(y-yp)^2)+2p1((x-xp)(y-yp))
	double p1, p2;
	//affinity
	//x' = x
	//y' = a1x + a2y
	//double a1, a2;
	//PPA
	double xp, yp;
	//fiducial mark
	double x1, y1;
	double x2, y2;
	double x3, y3;
	double x4, y4;
	double x5, y5;
	double x6, y6;
	double x7, y7;
	double x8, y8;
	//focal length
	double f;

}Camera;

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//Photo class
class CSMPhoto
{
	friend class CSMPhotoManage;
public:
	CSMPhoto();
	CSMPhoto(CSMPhoto& copy);
	~CSMPhoto();

//Member variable
public:

	//photo ID
	long ID;
	//camera
	long CameraID;
	//attitude
	double omega, phi, kappa;
	//perspective center
	double X, Y, Z;
	//photo point
	CICPManage icp;
	CICPManage originalicp;

	bool enable;

	//next photo pointer
	CSMPhoto *next;
	//forward photo pointer
	CSMPhoto *forward;

//Member function
public:
	//substitution operator
	void operator = (CSMPhoto& copy);

//Member variable
private:
	
//Member function
private:
	void Init(void);

};

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

//Photo management class
class CSMPhotoManage
{
public:
	CSMPhotoManage();
	virtual ~CSMPhotoManage();

//Member variable
private:
	long num_Photo;
	CSMPhoto firstphoto;
	CSMPhoto *nextphoto;


//Member function
public:
	bool InsertPhoto(CSMPhoto photo);
	bool DeletePhoto(long ID);
	CSMPhoto* GetPhotobySort(long sort);
	long GetPhotoID(long sort);
	CSMPhoto* GetPhotobyID(long ID);

	bool SetEnablePhoto(long ID, bool boolvalue);
	bool GetEnablePhoto(long ID);
	bool EmptyPhoto(void);
	long GetPhotoNum(void);
	long GetEnabledPhotoNum(void);

//Member function
private:
	CSMPhoto* GetPhotoHandle(long ID);
};


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_GCPMANAGE_H__19F4C1A5_855E_4046_9B84_50ABEED90325__INCLUDED_)