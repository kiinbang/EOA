// ApproxiLiCaliD.cpp: implementation of the ApproxiLiCaliD class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LiCaliD.h"
#include "ApproxiLiCaliD.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define Rad2Sec ((180.*3600.)/acos(-1.0))
#define ENLARGE_SCALE (1000.0)

#define MAX_LINE_LENGTH 512

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void GetPlaneParameters(double* V1, double* V2, double* V3, double XA, double YA, double ZA, double *Normal_distance, double *th, CSMMatrix<double> &R)
{
	
	double V2x, V2y, V2z;
	double V3x, V3y, V3z;
	
	double Dx, Dy, Dz;
	
	V2x = V2[0] - V1[0];
	V2y = V2[1] - V1[1];
	V2z = V2[2] - V1[2];
	
	V3x = V3[0] - V1[0];
	V3y = V3[1] - V1[1];
	V3z = V3[2] - V1[2];
	
	Dx = sqrt (V2x * V2x + V2y * V2y + V2z * V2z); 
	
	//X axis
	double XX1 = V2x / Dx;
	double XX2 = V2y / Dx;
	double XX3 = V2z / Dx;
	
	//Z axis
	double ZZ1 = V2y * V3z - V2z * V3y;
	double ZZ2 = V2z * V3x - V2x * V3z;
	double ZZ3 = V2x * V3y - V2y * V3x;
	
	Dz = sqrt (ZZ1 * ZZ1 + ZZ2 * ZZ2 + ZZ3 * ZZ3); 
	
	ZZ1 = ZZ1 / Dz;
	ZZ2 = ZZ2 / Dz;
	ZZ3 = ZZ3 / Dz;
	
	//Y axis
	
	double YY1 = ZZ2 * XX3 - ZZ3 * XX2;
	double YY2 = ZZ3 * XX1 - ZZ1 * XX3;
	double YY3 = ZZ1 * XX2 - ZZ2 * XX1;
	
	Dy = sqrt (YY1 * YY1 + YY2 * YY2 + YY3 * YY3); 
	
	YY1 = YY1 / Dy;
	YY2 = YY2 / Dy;
	YY3 = YY3 / Dy;
	
	double X1, Y1, Z1, X2, Y2, Z2, X3, Y3, Z3;
	
	X1 = Y1 = Z1 = 0.0;
	
	X2 = XX1 * V2x + XX2 * V2y + XX3 * V2z;
	Y2 = YY1 * V2x + YY2 * V2y + YY3 * V2z;
	Z2 = ZZ1 * V2x + ZZ2 * V2y + ZZ3 * V2z;
	
	X3 = XX1 * V3x + XX2 * V3y + XX3 * V3z;
	Y3 = YY1 * V3x + YY2 * V3y + YY3 * V3z;
	Z3 = ZZ1 * V3x + ZZ2 * V3y + ZZ3 * V3z;
	
	double X  = XX1 * (XA - V1[0]) + XX2 * (YA - V1[1]) + XX3 * (ZA - V1[2]);
	double Y  = YY1 * (XA - V1[0]) + YY2 * (YA - V1[1]) + YY3 * (ZA - V1[2]);
	double Z  = ZZ1 * (XA - V1[0]) + ZZ2 * (YA - V1[1]) + ZZ3 * (ZA - V1[2]);
	
	*Normal_distance = fabs(Z);
	
	if ((Y < 0.0) || (Y > Y3))
		*th = 0.0;
	else
	{
		double xi_13 = Y * X3 / Y3;
		double xi_23 = X3 - (X3 - X2) * (Y3 - Y) / (Y3 - Y2);
		
		if ((X > xi_13) && (X < xi_23))
			*th = 360.0;
		else
			*th = 0.0;
	}
	
	R(0,0) = XX1; R(0,1) = XX2; R(0,2) = XX3;
	R(1,0) = YY1; R(1,1) = YY2; R(1,2) = YY3;
	R(2,0) = ZZ1; R(2,1) = ZZ2; R(2,2) = ZZ3;
}

void RemoveCommentLine(fstream file, char* delimiter, int num = 1)
{
	char line[MAX_LINE_LENGTH];
	
	int bstop = true; 
	do 
	{
		bstop = true; 
		
		file.eatwhite();
		char target = file.peek();
		
		for(int i=0; i<num; i++)
		{
			char comment = delimiter[i];
			if (target == comment )	//comment line
			{
				file.getline(line,MAX_LINE_LENGTH);
				bstop = false;
			}
		}
		
		file.eatwhite();
		
	}while( (!file.eof()) &&  (bstop==false) ); 
	
	file.eatwhite();
}

ApproxiLiCaliD::ApproxiLiCaliD()
{
	option_corresponding_point = 0;
	option_printout_sf1_sf2 = 0;
	Max_Tri_Side = 2.5;
	max_ND = 0.25;
	PointCloud = NULL;
	bLogFiles = false;
	radius = 5.0;
}

ApproxiLiCaliD::~ApproxiLiCaliD()
{
	if(PointCloud != NULL)
		delete[] PointCloud;	
}

bool ApproxiLiCaliD::ReadTXYZ(CString TXYZFormatPath, int iOverlap)
{
	fstream BrazilFormatFile;
	BrazilFormatFile.open(TXYZFormatPath, ios::in);
	
	while(!BrazilFormatFile.eof())
	{
		BrazilTXYZI record;
		
		BrazilFormatFile>>record.time>>record.East_Last>>record.North_Last>>record.EH_Last;
		BrazilFormatFile.eatwhite();
		
		PointCloud[iOverlap].AddTail(record);
	}
	
	BrazilFormatFile.close();
	
	double x = PointCloud[iOverlap][0].East_Last;
	double y = PointCloud[iOverlap][0].North_Last;
	double z = PointCloud[iOverlap][0].EH_Last;
	
	return true;
}

bool ApproxiLiCaliD::ReadBrazilFormat(CString BrazilFormatPath, int iOverlap)
{
	fstream BrazilFormatFile;
	BrazilFormatFile.open(BrazilFormatPath, ios::in);
	
	char line[512];
	while(!BrazilFormatFile.eof())
	{
		//[Brazil data contents]
		//GPS time,
		//East_first, North_first, EH_first, Intensity_first
		//East_last, North_last, EH_last, Internsity_last
		
		BrazilTXYZI record;
		//double East_First, North_First, EH_First, Intensity_First;
		
		BrazilFormatFile>>record.time;
		
		BrazilFormatFile>>record.East_Last>>record.North_Last>>record.EH_Last>>record.Intensity_Last;
		
		//BrazilFormatFile>>East_First>>North_First>>EH_First>>Intensity_First;
		BrazilFormatFile.getline(line, 512);

		BrazilFormatFile.eatwhite();
		
		PointCloud[iOverlap].AddTail(record);
	}
	
	BrazilFormatFile.close();
	
	return true;
}

bool ApproxiLiCaliD::ReadTINFormat(CString TINPath, int iOverlap)
{
	fstream TINFormatFile;


	//Ki In Dec-15 2008
	TINFormatFile.open(TINPath, ios::in);

	double tri_sides=0;
	int num_sides = 0;

	while(!TINFormatFile.eof())
	{
		//[TIN data contents]
		//GPS time1, X1, Y1, Z1 GPS time2, X2, Y2, Z2 GPS time3, X3, Y3, Z3
		Triangle record;
		
		TINFormatFile>>record.A[3]>>record.A[0]>>record.A[1]>>record.A[2];
		TINFormatFile>>record.B[3]>>record.B[0]>>record.B[1]>>record.B[2];
		TINFormatFile>>record.C[3]>>record.C[0]>>record.C[1]>>record.C[2];
		TINFormatFile.eatwhite();
		
		double dX, dY, dZ;
		
		dX = record.A[0]-record.B[0];
		dY = record.A[1]-record.B[1];
		dZ = record.A[2]-record.B[2];

		tri_sides += sqrt((dX*dX) + (dY*dY) + (dZ*dZ));
		
		dX = record.A[0]-record.C[0];
		dY = record.A[1]-record.C[1];
		dZ = record.A[2]-record.C[2];
		
		tri_sides += sqrt((dX*dX) + (dY*dY) + (dZ*dZ));
		
		dX = record.C[0]-record.B[0];
		dY = record.C[1]-record.B[1];
		dZ = record.C[2]-record.B[2];
		
		tri_sides += sqrt((dX*dX) + (dY*dY) + (dZ*dZ));
		
		num_sides += 3;
	}
	
	TINFormatFile.close();

	double ave_side = tri_sides/num_sides;
	ave_side_List.AddTail(ave_side);

	if(ave_side/100.0 > Max_Tri_Side)
	{
		Max_Tri_Side = ave_side*2.0;
	}

	///////////////////////////////////////Ki In Dec-15 2008
	
	TINFormatFile.open(TINPath, ios::in);

	int TINcount=0;
	
	while(!TINFormatFile.eof())
	{
		//[TIN data contents]
		//GPS time1, X1, Y1, Z1 GPS time2, X2, Y2, Z2 GPS time3, X3, Y3, Z3
		Triangle record;
		
		TINFormatFile>>record.A[3]>>record.A[0]>>record.A[1]>>record.A[2];
		TINFormatFile>>record.B[3]>>record.B[0]>>record.B[1]>>record.B[2];
		TINFormatFile>>record.C[3]>>record.C[0]>>record.C[1]>>record.C[2];
		TINFormatFile.eatwhite();

		//Ki In Dec-14 2008
		double dX, dY, dZ;
		
		dX = record.A[0]-record.B[0];
		dY = record.A[1]-record.B[1];
		dZ = record.A[2]-record.B[2];

		if( sqrt((dX*dX) + (dY*dY) + (dZ*dZ)) > Max_Tri_Side )//Ki In Dec-15 2008
			continue;

		dX = record.A[0]-record.C[0];
		dY = record.A[1]-record.C[1];
		dZ = record.A[2]-record.C[2];
		
		if( sqrt((dX*dX) + (dY*dY) + (dZ*dZ)) > Max_Tri_Side )//Ki In Dec-15 2008
			continue;

		dX = record.C[0]-record.B[0];
		dY = record.C[1]-record.B[1];
		dZ = record.C[2]-record.B[2];
		
		if( sqrt((dX*dX) + (dY*dY) + (dZ*dZ)) > Max_Tri_Side )//Ki In Dec-15 2008
			continue;

		//////////////////Ki In Dec-14 2008
		
		double* x = new double[3];
		x[0] = (record.A[0]+record.B[0]+record.C[0])/3.0;
		x[1] = (record.A[1]+record.B[1]+record.C[1])/3.0;
		x[2] = (record.A[2]+record.B[2]+record.C[2])/3.0;
		
		double* contents = new double[12];
		contents[0] = record.A[0];
		contents[1] = record.A[1];
		contents[2] = record.A[2];
		contents[3] = record.A[3];
		
		contents[4] = record.B[0];
		contents[5] = record.B[1];
		contents[6] = record.B[2];
		contents[7] = record.B[3];
		
		contents[8]  = record.C[0];
		contents[9]  = record.C[1];
		contents[10] = record.C[2];
		contents[11] = record.C[3];
		
		TINList[iOverlap].add(x, contents, TINcount);
		
		TINcount++;
		
		//Ki In Dec 2008
		delete[] x;
		delete[] contents;
	}
	
	TINFormatFile.close();
	
	return true;
}

bool ApproxiLiCaliD::ReadTrj(CString TrjPath, CSMList<TrjRecord> &Trajectory)
{
	int i;
	fstream TrjFile;
	TrjFile.open(TrjPath, ios::in);

	KDTrjTime.InitialSetup(1);
	
	char line[MAX_LINE_LENGTH];
	for (i = 0; i < 28; i++)
	{
		TrjFile.getline(line, MAX_LINE_LENGTH);
	}

	while(!TrjFile.eof())
	{
		TrjRecord record;
		double* x = new double[1];

		//[Trajectory file contents]
		//TIME, 
		//DISTANCE, 
		//EASTING, NORTHING, ELLIPSOID HEIGHT, 
		//LATITUDE, LONGITUDE, ELLIPSOID HEIGHT, 
		//ROLL, PITCH, HEADING, 
		//EAST VELOCITY, NORTH VELOCITY, UP VELOCITY, 
		//EAST SD, NORTH SD, HEIGHT SD, 
		//ROLL SD, PITCH SD, HEADING SD
		
		double Distance, Lat, Lon, EH2, Roll, Pitch, Heading, SD_East, SD_North, SD_EH, SD_Roll, SD_Pitch, SD_Heading;
		
		TrjFile>>record.time;
		TrjFile>>Distance;
		TrjFile>>record.East>>record.North>>record.EH1;
		record.East += East_Offset_TRJ;
		TrjFile>>Lat>>Lon>>EH2;
		TrjFile>>Roll>>Pitch>>Heading;
		TrjFile>>record.V_East>>record.V_North>>record.V_Up;
		TrjFile>>SD_East>>SD_North>>SD_EH;
		TrjFile>>SD_Roll>>SD_Pitch>>SD_Heading;

 		Trajectory.AddTail(record);

		x[0] = record.time;
		KDTrjTime.add(x);
		
		TrjFile.eatwhite();

		//Ki In Dec 2008
		delete[] x;
	}
	
	TrjFile.close();
 	
	return true;
}

bool ApproxiLiCaliD::ReadTrj2(CString TrjPath, CSMList<TrjRecord> &Trajectory)
{
	fstream TrjFile;
	TrjFile.open(TrjPath, ios::in);
	
	KDTrjTime.InitialSetup(1);

	while (!TrjFile.eof())
	{
		TrjRecord record;
		double* x = new double[1];
		
		TrjFile>>record.East>>record.North>>record.EH1>>record.time;
		
		Trajectory.AddTail(record);

		x[0] = record.time;
		KDTrjTime.add(x);
		
		TrjFile.eatwhite();

		//Ki In Dec 2008
		delete[] x;
	}	
	
	TrjFile.close();
	
	return true;
}

void ApproxiLiCaliD::FindPseudoConjugate(int nOverlap, fstream &ResFile)
{
	fstream MatchFileA; MatchFileA.open(MatchPointPath, ios::out);
	fstream MatchFileB; MatchFileB.open(MatchTriPath, ios::out);
	fstream UnMatchFileA; UnMatchFileA.open(UnMatchPointPath, ios::out);
	fstream KappaFile; KappaFile.open(KappaPath, ios::out);
	
	UnMatchFileA.setf(ios::fixed, ios::floatfield);

	sum_ND_List[nOverlap] = 0.0;
	match_count_List[nOverlap] = 0;	

	//Ki In Dec-14 2008
	//TriPointList.RemoveAll();

	for (int iPoint = 0; iPoint < (int)PointCloud[nOverlap].GetNumItem(); iPoint++)
	{
		//Ki In Dec-14 2008
		Point3D G;
		G.East		= PointCloud[nOverlap][iPoint].East_Last;
		G.North		= PointCloud[nOverlap][iPoint].North_Last;
		G.EH		= PointCloud[nOverlap][iPoint].EH_Last;
		/////////////////////////////Ki In Dec-14 2008
				
		CSMList<KDNode> SubTIN;

		//select region
		double *low, *high;
		int level=0;
		low = new double[3];
		high = new double[3];
		
		//Ki In Dec-14 2008
		low[0] = PointCloud[nOverlap][iPoint].East_Last - radius;
		low[1] = PointCloud[nOverlap][iPoint].North_Last - radius;
		low[2] = PointCloud[nOverlap][iPoint].EH_Last - radius;
		
		high[0] = PointCloud[nOverlap][iPoint].East_Last + radius;
		high[1] = PointCloud[nOverlap][iPoint].North_Last + radius;
		high[2] = PointCloud[nOverlap][iPoint].EH_Last + radius;
		
		TINList[nOverlap].search(low, high, TINList[nOverlap].Root, level, SubTIN);
		////////////////////////////////Ki In Dec-14 2008

		//Ki In Dec 2008
		delete[] low;
		delete[] high;

		int num = SubTIN.GetNumItem();

		int iSelect = -1;
		double min_ND = 1.0e99;
		CSMMatrix<double> lastR;

		for (int iTIN2 = 0; iTIN2 < num; iTIN2++)
		{
			double A[4], B[4], C[4];

			//Ki In Dec 2008
			A[0] = SubTIN[iTIN2].Contents[0];
			A[1] = SubTIN[iTIN2].Contents[1];
			A[2] = SubTIN[iTIN2].Contents[2];
			A[3] = SubTIN[iTIN2].Contents[3];
			
			B[0] = SubTIN[iTIN2].Contents[4+0];
			B[1] = SubTIN[iTIN2].Contents[4+1];
			B[2] = SubTIN[iTIN2].Contents[4+2];
			B[3] = SubTIN[iTIN2].Contents[4+3];
			
			C[0] = SubTIN[iTIN2].Contents[8+0];
			C[1] = SubTIN[iTIN2].Contents[8+1];
			C[2] = SubTIN[iTIN2].Contents[8+2];
			C[3] = SubTIN[iTIN2].Contents[8+3];


			double normal_dist;
			double th;
			CSMMatrix<double> R(3,3);
			GetPlaneParameters(A, B, C, G.East, G.North, G.EH, &normal_dist, &th, R);
						
			if(fabs(360-th) > 1.0)
			{
				continue;
			}

			if(fabs(normal_dist) < fabs(max_ND))
			{
				if(fabs(min_ND) > fabs(normal_dist))
				{
					min_ND = normal_dist;
					iSelect = iTIN2;
					lastR = R;
				}
			}
			/////////////Ki In Dec 2008
		}
		
		if (iSelect != -1)
		{
			Point3D pos;
			double Kappa, Kappa2;

			double time;
			time = PointCloud[nOverlap][iPoint].time;
			double lateral_coord;
			
			if (true == SearchTrajectory(time, G, pos, Kappa, Kappa2, lateral_coord))
			{
				double A[4], B[4], C[4];

				A[0] = SubTIN[iSelect].Contents[0];
				A[1] = SubTIN[iSelect].Contents[1];
				A[2] = SubTIN[iSelect].Contents[2];
				A[3] = SubTIN[iSelect].Contents[3];
				
				B[0] = SubTIN[iSelect].Contents[4+0];
				B[1] = SubTIN[iSelect].Contents[4+1];
				B[2] = SubTIN[iSelect].Contents[4+2];
				B[3] = SubTIN[iSelect].Contents[4+3];
				
				C[0] = SubTIN[iSelect].Contents[8+0];
				C[1] = SubTIN[iSelect].Contents[8+1];
				C[2] = SubTIN[iSelect].Contents[8+2];
				C[3] = SubTIN[iSelect].Contents[8+3];
				
				Triangle_Point record;
				
				//Ki In Dec-22, 2008
				//record.xA		= CalLateralCoordinate(Kappa, pos, G);
				record.xA		= lateral_coord;
				record.zA		= -pos.EH + G.EH;
				record.Kappa	= Kappa;
				
				//Ki In Dec-15 2008
				record.PA		= G;
				///////////////////Ki In Dec-15 2008

				record.R = lastR.Transpose();				

				/////////////////////Ki In Dec 2008

				if(bLogFiles == true)
					KappaFile<<G.East<<"\t"<<G.North<<"\t"<<G.EH<<"\t"<<time<<"\t"<<Kappa/acos(-1.0)*180.0<<"\t"<<Kappa2/acos(-1.0)*180.0<<"\t"<<lateral_coord<<endl;
								
				Point3D P_G;
				Point3D Apos;
				double AKappa, AKappa2;
				double Alateral_coord;

				//Ki In Dec-15 2008
				if(option_corresponding_point == 1)
				{
					Point3D P_0, P_1;
					Point3D pos0, pos1;
					double Kappa0, Kappa1;
					double Kappa0_2, Kappa1_2;
					double lateral_coord0, lateral_coord1;

					CSMMatrix<double> P(3,1), Q(3,1), V(3,1);
					V(0,0) = A[0];
					V(1,0) = A[1];
					V(2,0) = A[2];

					P(0,0) = G.East;
					P(1,0) = G.North;
					P(2,0) = G.EH;

					CSMMatrix<double> PV = P -V;

					Q = lastR%PV;
					
					Q(2,0) = 0.0;
					
					Q = lastR.Transpose()%Q;
					Q = Q + V;

					P_G.East = Q(0,0);
					P_G.North = Q(1,0);
					P_G.EH = Q(2,0);

					double time0, time1;

					//MIN
					if( A[3] < B[3])
					{
						time0 = A[3];

						P_0.East	= A[0];
						P_0.North	= A[1];
						P_0.EH		= A[2];
					}
					else
					{
						time0 = B[3];

						P_0.East	= B[0];
						P_0.North	= B[1];
						P_0.EH		= B[2];
					}

					if( time0 > C[3])
					{
						time0 = C[3];

						P_0.East	= C[0];
						P_0.North	= C[1];
						P_0.EH		= C[2];
					}

					//MAX
					if( A[3] > B[3])
					{
						time1 = A[3];
						
						P_1.East	= A[0];
						P_1.North	= A[1];
						P_1.EH		= A[2];
					}
					else
					{
						time1 = B[3];
						
						P_1.East	= B[0];
						P_1.North	= B[1];
						P_1.EH		= B[2];
					}
					
					if( time1 < C[3])
					{
						time1 = C[3];
						
						P_1.East	= C[0];
						P_1.North	= C[1];
						P_1.EH		= C[2];
					}


					if (false == SearchTrajectory(time0, P_0, pos0, Kappa0, Kappa0_2, lateral_coord0))
					{					
						continue;
					}

					if(bLogFiles == true)
						KappaFile<<P_0.East<<"\t"<<P_0.North<<"\t"<<P_0.EH<<"\t"<<time0<<"\t"<<Kappa0/acos(-1.0)*180.0<<"\t"<<Kappa0_2/acos(-1.0)*180.0<<"\t"<<lateral_coord0<<endl;

					if (false == SearchTrajectory(time1, P_1, pos1, Kappa1, Kappa1_2, lateral_coord1))
					{					
						continue;
					}

					if(bLogFiles == true)
						KappaFile<<P_1.East<<"\t"<<P_1.North<<"\t"<<P_1.EH<<"\t"<<time1<<"\t"<<Kappa1/acos(-1.0)*180.0<<"\t"<<Kappa1_2/acos(-1.0)*180.0<<"\t"<<lateral_coord1<<endl;

					if (false == SearchTrajectory(P_G, pos0, pos1, Apos, AKappa, AKappa2, Alateral_coord))
					{					
						continue;
					}

					if(bLogFiles == true)
						KappaFile<<P_G.East<<"\t"<<P_G.North<<"\t"<<P_G.EH<<"\t"<<AKappa/acos(-1.0)*180.0<<"\t"<<AKappa2/acos(-1.0)*180.0<<"\t"<<Alateral_coord<<endl;
				}////////////////////////////////////////////////////////////////////////////////////Ki In Dec-15 2008
				else
				{
					P_G.East	= A[0];
					P_G.North	= A[1];
					P_G.EH		= A[2];
					
					time = A[3];
					if (false == SearchTrajectory(time, P_G, Apos, AKappa, AKappa2, Alateral_coord))
					{					
						continue;
					}

					if(bLogFiles == true)
						KappaFile<<P_G.East<<"\t"<<P_G.North<<"\t"<<P_G.EH<<"\t"<<time<<"\t"<<AKappa/acos(-1.0)*180.0<<"\t"<<AKappa2/acos(-1.0)*180.0<<"\t"<<Alateral_coord<<endl;
				}
				/////////////////////////Ki In Dec-15 2008
				
				record.PB = P_G;

				//record.A_xB		= CalLateralCoordinate(AKappa, Apos, P_G);
				record.A_xB		= Alateral_coord;
				record.A_zB		= -Apos.EH + P_G.EH;
				record.A_KappaB = AKappa;
				
				record.VA[0] = A[0];
				record.VA[1] = A[1];
				record.VA[2] = A[2];
				record.VA[3] = A[3];
				
				record.VB[0] = B[0];
				record.VB[1] = B[1];
				record.VB[2] = B[2];
				record.VB[3] = B[3];
				
				record.VC[0] = C[0];
				record.VC[1] = C[1];
				record.VC[2] = C[2];
				record.VC[3] = C[3];
				
				TriPointList.AddTail(record);
				
				if(bLogFiles == true)
				{
					MatchFileB.precision(7);
					MatchFileB<<SubTIN[iSelect].GetID()<<"\t\t\t";
					MatchFileB<<A[3]<<"\t\t\t";
					MatchFileB.precision(4);
					MatchFileB<<A[0]<<"\t\t\t"<<A[1]<<"\t\t\t"<<A[2]<<"\t\t\t";
					MatchFileB.precision(7);
					MatchFileB<<B[3]<<"\t\t\t";
					MatchFileB.precision(4);
					MatchFileB<<B[0]<<"\t\t\t"<<B[1]<<"\t\t\t"<<B[2]<<"\t\t\t";
					MatchFileB.precision(7);
					MatchFileB<<C[3]<<"\t\t\t";
					MatchFileB.precision(4);
					MatchFileB<<C[0]<<"\t\t\t"<<C[1]<<"\t\t\t"<<C[2]<<endl;
					
					MatchFileA.precision(7);
					MatchFileA<<iPoint<<"\t\t\t";
					MatchFileA<<PointCloud[nOverlap][iPoint].time<<"\t\t\t";
					MatchFileA.precision(4);
					MatchFileA<<G.East<<"\t\t\t"<<G.North<<"\t\t\t"<<G.EH<<"\t\t\t"<<min_ND<<endl;
					
					MatchFileA.flush();
					MatchFileB.flush();
				}

				sum_ND_List[nOverlap] += min_ND;
				match_count_List[nOverlap] ++;
				
			}
		}
		else
		{
			if(bLogFiles == true)
			{
				UnMatchFileA.precision(7);
				UnMatchFileA<<iPoint<<"\t\t\t";
				UnMatchFileA<<PointCloud[nOverlap][iPoint].time<<"\t\t\t";
				UnMatchFileA.precision(4);
				UnMatchFileA<<G.East<<"\t\t\t"<<G.North<<"\t\t\t"<<G.EH<<endl;
			}
		}

		SubTIN.RemoveAll();
	}

	MatchFileA.close();
	UnMatchFileA.close();
	MatchFileB.close();
	KappaFile.close();
	
}

double ApproxiLiCaliD::CalTriangleArea(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3)
{
	double dist1, dist2, dist3;
	dist1 = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2));
	dist2 = sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3) + (z2 - z3) * (z2 - z3));
	dist3 = sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3) + (z1 - z3) * (z1 - z3));
	
	double area;
	double s;
	double K;
	s = (dist1 + dist2 + dist3) / 2;
	K = (s * (s - dist1) * (s - dist2) * (s - dist3));
	if (K < 1.0e-9) K = 0.0;
	area = sqrt(K);

	return area;
}

//Ki In Dec-22, 2008
bool ApproxiLiCaliD::SearchTrajectory(double time, Point3D point, Point3D &pos, double &Kappa, double &Kappa2, double &lateral_coord)
{
	double* x = new double[1];
	
	x[0] = time;
	
	int time_index = KDTrjTime.find_nearest(x)->GetID();
	int index0, index1;

	delete[] x;

	if ( (time - KDTrjTime.GetAt(time_index)->X[0]) > 0.0 )
	{
		index0 = time_index;

		if( index0 >= ((int)Trajectory.GetNumItem()-1) )
			return false;
		else
			index1 = index0 + 1;
	}
	else
	{
		index1 = time_index;

		if( index1 <= 0 )
			return false;
		else
			index0 = index1 - 1;
	}
		
	double temp0 = Trajectory[index0].time;
	double temp1 = Trajectory[index1].time;
	double dTime1 = temp1 - temp0;
	
	if(fabs(dTime1) > 2.0) 
	{
		return false;
	}
	
	CSMMatrix<double> pos0(3,1);
	pos0(0,0) = Trajectory[index0].East;
	pos0(1,0) = Trajectory[index0].North;
	pos0(2,0) = Trajectory[index0].EH1;
	
	CSMMatrix<double> pos1(3,1);
	pos1(0,0) = Trajectory[index1].East;
	pos1(1,0) = Trajectory[index1].North;
	pos1(2,0) = Trajectory[index1].EH1;
	
	CSMMatrix<double> dpos = pos1 - pos0;
	
	double Azimuth = atan2(dpos(0,0), dpos(1,0));//bearing
	
	Kappa = -Azimuth;
	
	_Mmat_ M(0, 0, Kappa);
	
	CSMMatrix<double> P(3,1);
	P(0,0) = point.East;
	P(1,0) = point.North;
	P(2,0) = 0.0;
	
	P = P - pos0;

	CSMMatrix<double> dpos2 = M.Mmatrix%dpos;//check x coordinate
	CSMMatrix<double> P2 = M.Mmatrix%P;

	lateral_coord = P2(0,0);//laser unit coordinate (X)
	
	CSMMatrix<double> target2(3,1, 0.0);
	target2(1,0) = P2(1,0);
	target2(2,0) = target2(1,0) / dpos2(1,0) * dpos(2,0);
	
	CSMMatrix<double> target = M.Mmatrix.Transpose()%target2;

	CSMMatrix<double> pos_t =  pos0 + target;
	
	pos.East = pos_t(0,0);
	pos.North = pos_t(1,0);
	pos.EH = pos_t(2,0);

	Point3D point2pos;
	point2pos = point - pos;
	double Azimuth2 = atan2(point2pos.East, point2pos.North);//bearing
	Kappa2 = -Azimuth2;
	
	return true;
}

//Ki In Dec-22, 2008
bool ApproxiLiCaliD::SearchTrajectory(Point3D point, Point3D pos0, Point3D pos1, Point3D &pos, double &Kappa, double &Kappa2, double &lateral_coord)
{
	CSMMatrix<double> dpos(3,1);
	dpos(0,0)= pos1.East - pos0.East;
	dpos(1,0)= pos1.North - pos0.North;
	dpos(2,0)= pos1.EH - pos0.EH;
	
	double Azimuth = atan2(dpos(0,0), dpos(1,0));//bearing
	
	Kappa = -Azimuth;

	_Mmat_ M(0, 0, Kappa);
	
	CSMMatrix<double> P(3,1);
	P(0,0) = point.East - pos0.East;
	P(1,0) = point.North - pos0.North;
	P(2,0) = 0.0;

	CSMMatrix<double> dpos2 = M.Mmatrix%dpos;//check x coordinate
	CSMMatrix<double> P2 = M.Mmatrix%P;
	
	lateral_coord = P2(0,0);//laser unit coordinate (X)
	
	CSMMatrix<double> target2(3,1, 0.0);
	target2(1,0) = P2(1,0);
	target2(2,0) = target2(1,0) / dpos2(1,0) * dpos(2,0);
	
	CSMMatrix<double> target = M.Mmatrix.Transpose()%target2;

	pos.East	= pos0.East + target(0,0);
	pos.North	= pos0.North + target(1,0);
	pos.EH		= pos0.EH + target(2,0);

	Point3D point2pos;
	point2pos = point - pos;
	double Azimuth2 = atan2(point2pos.East, point2pos.North);//bearing
	Kappa2 = -Azimuth2;
		
	return true;
}

bool ApproxiLiCaliD::RunCalibration(CString prjpath, CString outpath)
{
	UnMatchPointPath = prjpath; 
	UnMatchPointPath += "_unmatched_point.tmp";
	
	MatchPointPath = prjpath; 
	MatchPointPath += "_matched_point.tmp";
	
	MatchTriPath = prjpath; 
	MatchTriPath += "_matched_tin.tmp";
	
	KappaPath = prjpath; 
	KappaPath += "_Kappa_.tmp";

	fstream ResFile; ResFile.open(outpath, ios::out);
	
	int option;
	
	if( false == ReadPrj(option, prjpath))
		return false;

	ResFile<<"###Current status: ReadPrj() is done."<<endl<<endl;

	if (0 == option)
	{
		//Ki In Dec-14 2008
		ReadTrj(TrajectoryPath, Trajectory);

		TriPointList.RemoveAll();

		for(int i=0; i<num_overlap_pairs; i++)
		{
			//Brazil data format
			ReadBrazilFormat(BrazilFormatDataPath[i], i);

			ReadTINFormat(TINPath[i], i);
			
			FindPseudoConjugate(i, ResFile);
		}
		/////////////////////////////Ki In Dec-14 2008
	}
	else if (3 == option)
	{
		//Ki In Dec-14 2008
		ReadTrj2(TrajectoryPath, Trajectory);

		TriPointList.RemoveAll();
		
		for(int i=0; i<num_overlap_pairs; i++)
		{
			ReadTXYZ(BrazilFormatDataPath[i], i);
			
			ReadTINFormat(TINPath[i], i);
			
			FindPseudoConjugate(i, ResFile);
		}
		/////////////////////////////Ki In Dec-14 2008
	}
	else
		return false;

	ResFile<<"###Current status: Read data(point-cloud and TIN) files and 1st FindPseudoConjugate() are done."<<endl<<endl;
	
	double old_var = 0.0;
	
	int num_repeat = 1;
	do
	{
		double new_var;
		new_var = Solve(ResFile, num_repeat);

		ResFile<<"###Current status: Solve() is done."<<endl<<endl;

		UpdateCoordinates();

		ResFile<<"###Current status: Coordinates are updated."<<endl<<endl;

		if(num_repeat > 1)
		{
			old_dParams.dPx += dParams.dPx;
			old_dParams.dPy += dParams.dPy;
			old_dParams.dDO += dParams.dDO;
			old_dParams.dDP += dParams.dDP;
			old_dParams.dDK += dParams.dDK;
			old_dParams.dp += dParams.dp;
			old_dParams.dS += dParams.dS;

			dParams.Init();
		}
		else
		{
			old_dParams = dParams;
		
			dParams.Init();
		}

		ResFile<<"[Updated parameters]"<<endl;
		ResFile<<"Px[m]: "<<old_dParams.dPx<<endl;
		ResFile<<"Py[m]: "<<old_dParams.dPy<<endl;
		ResFile<<"dO[deg]: "<<old_dParams.dDO * 180.0 / acos(-1.0)<<endl;
		ResFile<<"dP[deg]: "<<old_dParams.dDP * 180.0 / acos(-1.0)<<endl;
		ResFile<<"dK[deg]: "<<old_dParams.dDK * 180.0 / acos(-1.0)<<endl;
		ResFile<<"dp[m]: "<<old_dParams.dp<<endl;
		ResFile<<"dS: "<<old_dParams.dS<<endl<<endl<<endl;

		if( fabs(sqrt(old_var) - sqrt(new_var)) < diff_sigma)
		{
			ResFile<<endl<<"---Converged [ ";
			ResFile<<fabs(sqrt(old_var) - sqrt(new_var));
			ResFile<<" ]---"<<endl;
			break;
		}

		old_var = new_var;

		if( num_repeat < max_iteration )
		{
			TriPointList.RemoveAll();
			for(int i=0; i<num_overlap_pairs; i++)
			{
				FindPseudoConjugate(i, ResFile);
			}

		}

		ResFile<<"###Current status: FindPseudoConjugate() is done."<<endl<<endl;
		
		num_repeat ++;
		
	}while(num_repeat <= max_iteration);
	
	if( option_printout_sf1_sf2 == 1)
	{
		PrintOutSf1andSf2();

		ResFile<<"###Current status: PrintOutSf1andSf2() is done."<<endl<<endl;
	}

	ResFile.close();

	return true;
}

void ApproxiLiCaliD::UpdateCoordinates()
{
	for(int nOverlap=0; nOverlap<num_overlap_pairs; nOverlap++)
	{
		for (int i = 0; i < (int)TINList[nOverlap].nList; i++)
		{
			double A[4], B[4], C[4];
			A[0] = TINList[nOverlap].GetAt(i)->Contents[0];
			A[1] = TINList[nOverlap].GetAt(i)->Contents[1];
			A[2] = TINList[nOverlap].GetAt(i)->Contents[2];
			A[3] = TINList[nOverlap].GetAt(i)->Contents[3];
			
			B[0] = TINList[nOverlap].GetAt(i)->Contents[4+0];
			B[1] = TINList[nOverlap].GetAt(i)->Contents[4+1];
			B[2] = TINList[nOverlap].GetAt(i)->Contents[4+2];
			B[3] = TINList[nOverlap].GetAt(i)->Contents[4+3];
			
			C[0] = TINList[nOverlap].GetAt(i)->Contents[8+0];
			C[1] = TINList[nOverlap].GetAt(i)->Contents[8+1];
			C[2] = TINList[nOverlap].GetAt(i)->Contents[8+2];
			C[3] = TINList[nOverlap].GetAt(i)->Contents[8+3];
			
			double Kappa, Kappa2, zA, xA, oldX, oldY, oldZ, SB, newX, newY, newZ;
			Point3D P_G, pos;
			double time;
			
			//Vertex A
			P_G.East = A[0];
			P_G.North = A[1];
			P_G.EH = A[2];
			time = A[3];
			
			if( false == SearchTrajectory(time, P_G, pos, Kappa, Kappa2, xA) ) continue;
			
			//xA = CalLateralCoordinate(Kappa, pos, P_G);
			zA = -pos.EH + P_G.EH;
			
			oldX = P_G.East;
			oldY = P_G.North;
			oldZ = P_G.EH;
			
			SB = atan(xA / zA);
			
			ReconstructCoordinate(oldX, oldY, oldZ, Kappa, zA, xA, SB, newX, newY, newZ);
			
			TINList[nOverlap].GetAt(i)->Contents[0] = newX;
			TINList[nOverlap].GetAt(i)->Contents[1] = newY;
			TINList[nOverlap].GetAt(i)->Contents[2] = newZ;
			
			//Vertex B
			P_G.East = B[0];
			P_G.North = B[1];
			P_G.EH = B[2];
			time = B[3];
			
			if( false == SearchTrajectory(time, P_G, pos, Kappa, Kappa2, xA) ) continue;
			
			//xA = CalLateralCoordinate(Kappa, pos, P_G);
			zA = -pos.EH + P_G.EH;
			
			oldX = P_G.East;
			oldY = P_G.North;
			oldZ = P_G.EH;
			
			SB = atan(xA / zA);
			
			ReconstructCoordinate(oldX, oldY, oldZ, Kappa, zA, xA, SB, newX, newY, newZ);
			
			TINList[nOverlap].GetAt(i)->Contents[4+0] = newX;
			TINList[nOverlap].GetAt(i)->Contents[4+1] = newY;
			TINList[nOverlap].GetAt(i)->Contents[4+2] = newZ;
			
			//Vertex C
			P_G.East = C[0];
			P_G.North = C[1];
			P_G.EH = C[2];
			time = C[3];
			
			if ( false == SearchTrajectory(time, P_G, pos, Kappa, Kappa2, xA) ) continue;
			
			//xA = CalLateralCoordinate(Kappa, pos, P_G);
			zA = -pos.EH + P_G.EH;
			
			oldX = P_G.East;
			oldY = P_G.North;
			oldZ = P_G.EH;
			
			SB = atan(xA / zA);
			
			ReconstructCoordinate(oldX, oldY, oldZ, Kappa, zA, xA, SB, newX, newY, newZ);
			
			TINList[nOverlap].GetAt(i)->Contents[8+0] = newX;
			TINList[nOverlap].GetAt(i)->Contents[8+1] = newY;
			TINList[nOverlap].GetAt(i)->Contents[8+2] = newZ;
		}
		
		DATA<BrazilTXYZI>* pos_Point = NULL;
		for (i = 0; i < (int)PointCloud[nOverlap].GetNumItem(); i++)
		{
			pos_Point = PointCloud[nOverlap].GetNextPos(pos_Point);
			
			double Kappa, Kappa2, zA, xA, oldX, oldY, oldZ, SB, newX, newY, newZ;
			Point3D P_G, pos;
			double time;
			
			//Point
			P_G.East = pos_Point->value.East_Last;
			P_G.North = pos_Point->value.North_Last;
			P_G.EH = pos_Point->value.EH_Last;
			time = pos_Point->value.time;
			
			if (false == SearchTrajectory(time, P_G, pos, Kappa, Kappa2, xA) ) continue;
			
			//xA = CalLateralCoordinate(Kappa, pos, P_G);
			zA = -pos.EH + P_G.EH;
			
			oldX = P_G.East;
			oldY = P_G.North;
			oldZ = P_G.EH;
			
			SB = atan(xA / zA);
			
			ReconstructCoordinate(oldX, oldY, oldZ, Kappa, zA, xA, SB, newX, newY, newZ);
			
			pos_Point->value.East_Last = newX;
			pos_Point->value.North_Last = newY;
			pos_Point->value.EH_Last = newZ;
		}
	}

}

double ApproxiLiCaliD::Solve(fstream &ResFile, int num_repeat)
{
	int i;
	int num_Pairs = (int)TriPointList.GetNumItem();
	
	ResFile<<"["<<num_repeat<<"]"<<endl;

	ResFile.precision(7);
	
	for(int nOverlap=0; nOverlap<num_overlap_pairs; nOverlap++)
	{
		ResFile<<"Point cloud: "<<BrazilFormatDataPath[nOverlap]<<endl;
		ResFile<<"TIN data: "<<TINPath[nOverlap]<<endl;
		ResFile<<"[Average normal distance (m)] "<<sum_ND_List[nOverlap]/match_count_List[nOverlap]<<endl;
		ResFile<<"[no of matched pairs] "<<match_count_List[nOverlap]<<endl<<endl;
		ResFile<<"[Average length of triangle sides (m)] "<<ave_side_List[nOverlap]<<endl;
		ResFile<<"--------------------------------------------------------------------------------------------------"<<endl;
	}	
	
	int num_eq = 0;
	
	CSMMatrix<double> N(7, 7, 0.0);
	CSMMatrix<double> C(7, 1, 0.0);
	CSMMatrix<double> X(7, 1, 0.0);
	CSMMatrix<double> Cx_inv(7, 7, 0.0);	

	Cx_inv(0, 0) = priori_var / dParams.sdPx / dParams.sdPx;
	Cx_inv(1, 1) = priori_var / dParams.sdPy / dParams.sdPy;
	Cx_inv(2, 2) = priori_var / dParams.sdDO*Rad2Sec / dParams.sdDO*Rad2Sec;
	Cx_inv(3, 3) = priori_var / dParams.sdDP*Rad2Sec / dParams.sdDP*Rad2Sec;
	Cx_inv(4, 4) = priori_var / dParams.sdDK*Rad2Sec / dParams.sdDK*Rad2Sec;
	Cx_inv(5, 5) = priori_var / dParams.sdp / dParams.sdp;
	Cx_inv(6, 6) = priori_var / dParams.sdS*ENLARGE_SCALE / dParams.sdS*ENLARGE_SCALE;
	
	num_eq += 7;
	
	for (i = 0; i < 7; i++)
	{
		if (Cx_inv(i, i) < (priori_var / max_sigma / max_sigma))
		{
			num_eq--;
			Cx_inv(i, i) = 0.0;
		}
	}	
	
	DATA<Triangle_Point>* pos_TriPoint = NULL;

	for (i = 0; i < num_Pairs; i++)
	{
		pos_TriPoint = TriPointList.GetNextPos(pos_TriPoint);

		CSMMatrix<double> ai(3, 7, 0.0);
		
		CSMMatrix<double> yi(3, 1, 0.0);
				
		double precision = (sX + sY + sZ) / 3.0;
		double precision2 = 2*precision * precision;
		
		if (precision2 == 0.0) precision2 = min_sigma * min_sigma;
		
		CSMMatrix<double> Puvw(3, 3, 0.0);
		CSMMatrix<double> Pxyz(3, 3, 0.0);
		
		//Ki In Dec 2008
		CSMMatrix<double> R = pos_TriPoint->value.R;
		Puvw(2, 2) = priori_var / precision2;
		
		Pxyz = (R%Puvw) % R.Transpose();
		//
		
		DiscrepancyFunction(ai, yi, pos_TriPoint->value);
		
		CSMMatrix<double> ni, ci;
		ci = ai.Transpose() % Pxyz % yi;
		ni = ai.Transpose() % Pxyz % ai;
		
		N = N + ni;
		C = C + ci;

		//ResFile<<"["<<i<<"]"<<endl;
		//MatrixOut(ResFile, ni, "[ni matrix]", 6, 20);
		//MatrixOut(ResFile, ci, "[ci matrix]", 6, 20);
		
		num_eq++;
	}
	
	CSMMatrix<double> dParamMat(7, 1, 0.0);
	
	dParamMat(0, 0) = dParams.dPx;
	dParamMat(1, 0) = dParams.dPy;
	dParamMat(2, 0) = dParams.dDO*Rad2Sec;
	dParamMat(3, 0) = dParams.dDP*Rad2Sec;
	dParamMat(4, 0) = dParams.dDK*Rad2Sec;
	dParamMat(5, 0) = dParams.dp;
	dParamMat(6, 0) = dParams.dS*ENLARGE_SCALE;
	
	N = N + Cx_inv;
	
	C = C + (Cx_inv % dParamMat);
	
	int num_fixed_params = 0;
	
	for (i = 0; i < 7; i++)
	{
		double val = Cx_inv(i, i);
		if ( val > (priori_var / (min_sigma * min_sigma)) )
		{
			for (int j = 0; j < 7; j++)
			{
				N(i, j) = 0.0;
				N(j, i) = 0.0;
			}
			
			N(i, i) = 1.0;
			C(i, 0) = dParamMat(i, 0);
			
			num_fixed_params++;
		}
	}
	
	MatrixOut(ResFile, N, "[N matrix]", 6, 20);
	MatrixOut(ResFile, C, "[C matrix]", 6, 20);
	
	CSMMatrix<double> Ninv = N.Inverse();
	
	CSMMatrix<double> Correlation(7, 7, 0.0);
	for (i = 0; i < 7; i++)
	{
		for (int j = 0; j < 7; j++)
		{
			Correlation(i, j) = Ninv(i, j) / sqrt(Ninv(i, i)) / sqrt(Ninv(j, j));
		}
	}
	
	MatrixOut(ResFile, Correlation, "[Correlation matrix]", 7, 15);
	
	X = Ninv % C;
	
	UpdateUnknowns(X);
	
	MatrixOut(ResFile, X, "[X matrix]", 5, 8);

	CSMMatrix<double> sum_ei(3,1,0);
	
	//ResFile<<"Residuals"<<endl<<endl;

	pos_TriPoint = NULL;
	double etpe = 0;

	for (i = 0; i < num_Pairs; i++)
	{
		pos_TriPoint = TriPointList.GetNextPos(pos_TriPoint);

		CSMMatrix<double> ai(3, 7, 0.0);
		
		CSMMatrix<double> yi(3, 1, 0.0);
		
		CSMMatrix<double> ei(3, 1, 0.0);
		
		CSMMatrix<double> etpei(1, 1, 0.0);
		
		double precision = (sX + sY + sZ) / 3.0;
		double precision2 = 2 * precision * precision;
		
		if (precision2 == 0.0) precision2 = min_sigma * min_sigma;
		
		CSMMatrix<double> Puvw(3, 3, 0.0);
		CSMMatrix<double> Pxyz(3, 3, 0.0);
		
		//Ki In Dec 2008
		CSMMatrix<double> R = pos_TriPoint->value.R;
		Puvw(2, 2) = priori_var / precision2;
		
		Pxyz = (R%Puvw) % R.Transpose();
		//
				
		DiscrepancyFunction(ai, yi, pos_TriPoint->value);
		
		ei = (ai % X) - yi;
		etpei = ei.Transpose() % Pxyz % ei;
		etpe += etpei(0, 0);

		//ResFile<<etpei(0,0)<<endl;

		sum_ei += ei;

		////////////////////////////////////////////
		double temp1=etpei(0,0);
		CSMMatrix<double> etei = ei.Transpose()%ei;
		double temp2 = etei(0,0);
		////////////////////////////////////////////
		
	}

	ResFile<<endl;
	MatrixOut(ResFile, sum_ei/num_Pairs, "[Mean of residual]", 6, 10);
	
	double posteriori_var = etpe / (num_eq - 7 + num_fixed_params);
	
	CSMMatrix<double> Var_X(7, 7, 0.0);
	Var_X = Ninv*posteriori_var;
	
	for (i = 0; i < 7; i++)
	{
		double val = Cx_inv(i, i);
		if (val > (priori_var / min_sigma / min_sigma))
		{
			for (int j = 0; j < 7; j++)
			{
				Var_X(i, j) = 0.0;
				Var_X(j, i) = 0.0;
			}
		}
	}
	
	MatrixOut(ResFile, Var_X, "[Variance matrix]", 6, 15);
	
	double sigma = sqrt(posteriori_var);
	
	ResFile<<endl<<"Posteriori_var: "<<posteriori_var<<endl<<endl;
	
	ResFile<<"[Estimated values in this iteration]"<<endl;
	ResFile<<"Px[m]: "<<dParams.dPx<<"\t"<<sqrt(Var_X(0,0))<<endl;
	ResFile<<"Py[m]: "<<dParams.dPy<<"\t"<<sqrt(Var_X(1,1))<<endl;
	ResFile<<"dO[deg]: "<<dParams.dDO * 180.0 / acos(-1.0)<<"\t"<<sqrt(Var_X(2,2))/Rad2Sec*180.0/acos(-1.0)<<endl;
	ResFile<<"dP[deg]: "<<dParams.dDP * 180.0 / acos(-1.0)<<"\t"<<sqrt(Var_X(3,3))/Rad2Sec*180.0/acos(-1.0)<<endl;
	ResFile<<"dK[deg]: "<<dParams.dDK * 180.0 / acos(-1.0)<<"\t"<<sqrt(Var_X(4,4))/Rad2Sec*180.0/acos(-1.0)<<endl;
	ResFile<<"dp[m]: "<<dParams.dp<<"\t"<<sqrt(Var_X(5,5))<<endl;
	ResFile<<"dS: "<<dParams.dS<<"\t"<<sqrt(Var_X(6,6))/ENLARGE_SCALE<<endl<<endl<<endl;
	
	return posteriori_var;
}

bool ApproxiLiCaliD::ReadPrj(int &option, CString PrjPath)
{
	fstream PrjFile; 
	
	PrjFile.open(PrjPath, ios::in);

	//Ki In Dec-15 2008

	//version check
	int pos;
	double ver_num = 1.0;
	bool bVersion = FindString(PrjFile, "VER", pos);
	if(bVersion == true)
	{
		//To move to begin of file
		PrjFile.seekg(0,ios::beg);
		char temp[MAX_LINE_LENGTH];//"VER"
		PrjFile>>temp>>ver_num;//version
	}
	else
	{
		//To move to begin of file
		PrjFile.seekg(0,ios::beg);//old version without version number(before ver 1.1)
	}

	///////////////////////////////////////////Ki In Dec-15 2008

	//Option: file type
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>option;
	PrjFile.eatwhite();

	//Trajectory path
	RemoveCommentLine(PrjFile, "#!",2);
	char line[MAX_LINE_LENGTH];
	PrjFile.getline(line, MAX_LINE_LENGTH);
	TrajectoryPath = line;

	//Number of overlap cases
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>num_overlap_pairs;
	PrjFile.eatwhite();

	PointCloud = new CSMList<BrazilTXYZI>[num_overlap_pairs];
	
	if(num_overlap_pairs > MAX_NUM_OVERLAPS)
		return false;

	for(int i=0; i<num_overlap_pairs; i++)
	{
		TINList[i].InitialSetup(3, 16);

		sum_ND_List.AddTail(0.0);
		match_count_List.AddTail(0);

		//Point-cloud path
		RemoveCommentLine(PrjFile, "#!",2);
		PrjFile.getline(line, MAX_LINE_LENGTH);
		BrazilFormatDataPath.AddTail(line);
		
		//TIN path
		RemoveCommentLine(PrjFile, "#!",2);
		PrjFile.getline(line, MAX_LINE_LENGTH);
		TINPath.AddTail(line);
	}

	//Ki In Dec-15 2008
	if(ver_num > 2.1) //for ver 2.2
	{
		//Max length of a triangle side
		RemoveCommentLine(PrjFile, "#!",2);
		PrjFile>>Max_Tri_Side;
		PrjFile.eatwhite();
	}

	if(ver_num > 2.2)//for ver 2.3
	{
		//Option: corresponding points: [0] Vertex A [1] Prjected point
		RemoveCommentLine(PrjFile, "#!",2);
		PrjFile>>option_corresponding_point;
		PrjFile.eatwhite();
	}
	/////////////////////////////////Ki In Dec-15 2008

	if(ver_num > 2.3)//for ver 2.4
	{
		//Option: sf1 and sf2 print-out
		RemoveCommentLine(PrjFile, "#!",2);
		PrjFile>>option_printout_sf1_sf2;
		PrjFile.eatwhite();

		//Max normal distance
		RemoveCommentLine(PrjFile, "#!",2);
		PrjFile>>max_ND;
		PrjFile.eatwhite();
	}
	/////////////////////////////////Ki In Dec-16 2008

	if(ver_num > 2.5)//for ver 2.6
	{
		//Option: sf1 and sf2 print-out
		RemoveCommentLine(PrjFile, "#!",2);
		PrjFile>>radius;
		PrjFile.eatwhite();
	}
	/////////////////////////////////Ki In Dec-24 2008
	
	//East coordinate offset for Brazil trajectory file
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>East_Offset_TRJ; PrjFile.eatwhite();

	//Priori value
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>priori_var; PrjFile.eatwhite();

	//Precision (X)
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>sX; PrjFile.eatwhite();
	//Precision (Y)
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>sY; PrjFile.eatwhite();
	//Precision (Z)
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>sZ; PrjFile.eatwhite();

	//dDX
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.dPx;PrjFile.eatwhite();
	//dDY
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.dPy; PrjFile.eatwhite();

	//dDOmega
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.dDO; PrjFile.eatwhite();
	dParams.dDO = dParams.dDO/ 180.0 * acos(-1.0);
	//dDPhi
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.dDP; PrjFile.eatwhite();
	dParams.dDP= dParams.dDP/ 180.0 * acos(-1.0);
	//dDKappa
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.dDK; PrjFile.eatwhite();
	dParams.dDK = dParams.dDK / 180.0 * acos(-1.0);
	
	//dp
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.dp; PrjFile.eatwhite();
	//dS
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.dS; PrjFile.eatwhite();
	
	//sigma dDX
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.sdPx; PrjFile.eatwhite();
	//sigma dDY
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.sdPy; PrjFile.eatwhite();
	
	//sigma dDOmega
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.sdDO; PrjFile.eatwhite();
	dParams.sdDO = dParams.sdDO/ 180.0 * acos(-1.0);
	//sigma dDOmega
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.sdDP; PrjFile.eatwhite();
	dParams.sdDP = dParams.sdDP/ 180.0 * acos(-1.0);
	//sigma dDKappa
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.sdDK; PrjFile.eatwhite();
	dParams.sdDK = dParams.sdDK/ 180.0 * acos(-1.0);
	//sigma dp
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.sdp; PrjFile.eatwhite();
	//sigma dS
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>dParams.sdS; PrjFile.eatwhite();

	//diff sigma (for iteration stop)
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>diff_sigma; PrjFile.eatwhite();
	//min sigma
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>min_sigma; PrjFile.eatwhite();
	//max sigma
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>max_sigma; PrjFile.eatwhite();
	//max iteration number
	RemoveCommentLine(PrjFile, "#!",2);
	PrjFile>>max_iteration; PrjFile.eatwhite();

	//Log files
	if(ver_num > 2.4)//for ver 2.5
	{
		int option_logfiles;
		RemoveCommentLine(PrjFile, "#!",2);
		PrjFile>>option_logfiles; PrjFile.eatwhite();
		if(option_logfiles == 0) bLogFiles = false;
		else if(option_logfiles == 1) bLogFiles = true;
		else bLogFiles = true;
	}

	PrjFile.close();

	return true;
}

bool ApproxiLiCaliD::DiscrepancyFunction(CSMMatrix<double> &ai, CSMMatrix<double> &yi, Triangle_Point TriPointPair)
{
	double KA = TriPointPair.Kappa;
	double KB = TriPointPair.A_KappaB;
	
	double xA, xB, zA, zB;
	xA = TriPointPair.xA;
	xB = TriPointPair.A_xB;
	zA = TriPointPair.zA;
	zB = TriPointPair.A_zB;
	
	double SBA = atan(xA / zA);
	double SBB = atan(xB / zB);
	
	double cosKA = cos(KA);
	double cosKB = cos(KB);
	double sinKA = sin(KA);
	double sinKB = sin(KB);
	double sinSBA = sin(SBA);
	double sinSBB = sin(SBB);
	double cosSBA = cos(SBA);
	double cosSBB = cos(SBB);

	//CheckFile_1<<"Point_A: "<<TriPointPari.East<<"[Xa] "<<record.xA<<"[Za] "<<record.zA<<"[Kappa] "<<record.Kappa;
	
	//Ki In Dec-15 2008
	Point3D GA_GB;
	GA_GB = TriPointPair.PA	- TriPointPair.PB;
	//////////////////////Ki In Dec-15 2008
	
	/*
	Point3D F0, F0_1, F0_2, F0_3, F0_4, F0_5;
	
	F0_1.East = (cosKA - cosKB) * dParams.dPx - (sinKA - sinKB) * dParams.dPy;
	F0_1.North = (sinKA - sinKB) * dParams.dPx + (cosKA - cosKB) * dParams.dPy;
	F0_1.EH = 0.0;
	
	F0_2.East = (sinKA * zA - sinKB * zB) * dParams.dDO + (cosKA * zA - cosKB * zB) * dParams.dDP;
	F0_2.North = -(cosKA * zA - cosKB * zB) * dParams.dDO + (sinKA * zA - sinKB * zB) * dParams.dDP;
	F0_2.EH = (-xA + xB) * dParams.dDP;
	
	F0_3.East = -(sinKA * xA - sinKB * xB) * dParams.dDK;
	F0_3.North = (cosKA * xA - cosKB * xB) * dParams.dDK;
	F0_3.EH = 0.0;
	
	F0_4.East = -(cosKA * sinSBA - cosKB * sinSBB) * dParams.dp;
	F0_4.North = -(sinKA * sinSBA - sinKB * sinSBB) * dParams.dp;
	F0_4.EH = -(cosSBA - cosSBB) * dParams.dp;
	
	F0_5.East = (cosKA * zA * SBA - cosKB * zB * SBB) * dParams.dS;
	F0_5.North = (sinKA * zA * SBA - sinKB * zB * SBB) * dParams.dS;
	F0_5.EH = -(xA * SBA - xB * SBB) * dParams.dS;
	
	F0 = F0_1 + F0_2 + F0_3 + F0_4 + F0_5;
	*/
	
	Point3D F0_dPx, F0_1_dPx;
	
	F0_1_dPx.East = (cosKA - cosKB);
	F0_1_dPx.North = (sinKA - sinKB);
	F0_1_dPx.EH = 0.0;
	
	F0_dPx = F0_1_dPx;
	
	Point3D F0_dPy, F0_1_dPy;
	
	F0_1_dPy.East = -(sinKA - sinKB);
	F0_1_dPy.North = (cosKA - cosKB);
	F0_1_dPy.EH = 0.0;
	
	F0_dPy = F0_1_dPy;
	
	Point3D F0_dDO, F0_2_dDO;
	F0_2_dDO.East = (sinKA * zA - sinKB * zB)/Rad2Sec;
	F0_2_dDO.North = -(cosKA * zA - cosKB * zB)/Rad2Sec;
	F0_2_dDO.EH = 0.0;
	
	F0_dDO = F0_2_dDO;
	
	Point3D F0_dDP, F0_2_dDP;
	F0_2_dDP.East = (cosKA * zA - cosKB * zB)/Rad2Sec;
	F0_2_dDP.North = (sinKA * zA - sinKB * zB)/Rad2Sec;
	F0_2_dDP.EH = -(xA - xB)/Rad2Sec;
	
	F0_dDP = F0_2_dDP;
	
	Point3D F0_dDK, F0_3_dDK;
	
	F0_3_dDK.East = -(sinKA * xA - sinKB * xB)/Rad2Sec;
	F0_3_dDK.North = (cosKA * xA - cosKB * xB)/Rad2Sec;
	F0_3_dDK.EH = 0.0;
	
	F0_dDK = F0_3_dDK;
	
	Point3D F0_dp, F0_4_dp;
	
	F0_4_dp.East = -(cosKA * sinSBA - cosKB * sinSBB);
	F0_4_dp.North = -(sinKA * sinSBA - sinKB * sinSBB);
	F0_4_dp.EH = -(cosSBA - cosSBB);
	
	F0_dp = F0_4_dp;
	
	Point3D F0_dS, F0_5_dS;
	
	F0_5_dS.East = (cosKA * zA * SBA - cosKB * zB * SBB)/ENLARGE_SCALE;
	F0_5_dS.North = (sinKA * zA * SBA - sinKB * zB * SBB)/ENLARGE_SCALE;
	F0_5_dS.EH = -(xA * SBA - xB * SBB)/ENLARGE_SCALE;
	
	F0_dS = F0_5_dS;
	
	ai(0, 0) = F0_dPx.East;
	ai(0, 1) = F0_dPy.East;
	ai(0, 2) = F0_dDO.East;
	ai(0, 3) = F0_dDP.East;
	ai(0, 4) = F0_dDK.East;
	ai(0, 5) = F0_dp.East;
	ai(0, 6) = F0_dS.East;
	
	ai(1, 0) = F0_dPx.North;
	ai(1, 1) = F0_dPy.North;
	ai(1, 2) = F0_dDO.North;
	ai(1, 3) = F0_dDP.North;
	ai(1, 4) = F0_dDK.North;
	ai(1, 5) = F0_dp.North;
	ai(1, 6) = F0_dS.North;
	
	ai(2, 0) = F0_dPx.EH;
	ai(2, 1) = F0_dPy.EH;
	ai(2, 2) = F0_dDO.EH;
	ai(2, 3) = F0_dDP.EH;
	ai(2, 4) = F0_dDK.EH;
	ai(2, 5) = F0_dp.EH;
	ai(2, 6) = F0_dS.EH;
	
	yi(0, 0) = GA_GB.East;
	yi(1, 0) = GA_GB.North;
	yi(2, 0) = GA_GB.EH;
	
	return true;
}

void ApproxiLiCaliD::MatrixOut(fstream &ResFile, CSMMatrix<double> M, CString title, int pre, int width)
{
	ResFile.precision(pre);

	ResFile<<title<<endl;
	
	for(int i=0; i < (int)M.GetRows(); i++)
	{
		for(int j=0; j < (int)M.GetCols(); j++)
		{
			ResFile<<setw(width)<<M(i, j)<<"\t";
		}
		ResFile<<endl;
	}
	
	ResFile<<endl;
	
	ResFile.flush();
}

void ApproxiLiCaliD::UpdateUnknowns(CSMMatrix<double> X)
{
	dParams.dPx = X(0, 0);
	dParams.dPy = X(1, 0);
	dParams.dDO = X(2, 0)/Rad2Sec;
	dParams.dDP = X(3, 0)/Rad2Sec;
	dParams.dDK = X(4, 0)/Rad2Sec;
	dParams.dp = X(5, 0);
	dParams.dS = X(6, 0)/ENLARGE_SCALE;
}

void ApproxiLiCaliD::ReconstructCoordinate(double oldX, double oldY, double oldZ, double Kappa, double zA, double xA, double SB, double &newX, double &newY, double &newZ)
{
	double cosK = cos(Kappa);
	double sinK = sin(Kappa);
	
	double sinSB = sin(SB);
	double cosSB = cos(SB);
	
	newX = oldX;
	newX -= + cosK*dParams.dPx;
	newX -= -sinK * dParams.dPy;
	newX -= +sinK * zA * dParams.dDO;
	newX -= +cosK * zA * dParams.dDP;
	newX -= -sinK * xA * dParams.dDK;
	newX -= -cosK * sinSB * dParams.dp;
	newX -= +cosK * zA * SB * dParams.dS;
	
	newY = oldY;
	newY -= +sinK * dParams.dPx;
	newY -= +cosK * dParams.dPy;
	newY -= -cosK * zA * dParams.dDO;
	newY -= +sinK * zA * dParams.dDP;
	newY -= +cosK * xA * dParams.dDK;
	newY -= -sinK * sinSB * dParams.dp;
	newY -= +sinK * zA * SB * dParams.dS;//Ki In Dec 2008
	
	newZ = oldZ;
	newZ -= -xA * dParams.dDP;
	newZ -= -cosSB * dParams.dp;
	newZ -= -xA * SB * dParams.dS;
}

bool ApproxiLiCaliD::FindString(fstream fin, char target[], int &result)
{
	char string[MAX_LINE_LENGTH];
	if(fin.getline(string, MAX_LINE_LENGTH))
	{
		char *pdest =  NULL;
		pdest = strstr(string, target);
		result = pdest - string + 1;
		if(pdest != NULL)
			return true;
		else
			return false;
	}
	else
		return false;
	
	
}

void ApproxiLiCaliD::PrintOutSf1andSf2()
{
	fstream sf1_file, sf2_file;

	for(int i=0; i<num_overlap_pairs; i++)
	{
		CString sf1_path, sf2_path;
		
		sf1_path = BrazilFormatDataPath[i];
		sf1_path += ".sf1";
		
		sf2_path = TINPath[i];
		sf2_path += ".sf2";

		sf1_file.open(sf1_path, ios::out);
		sf1_file.setf(ios::fixed, ios::floatfield);

		sf2_file.open(sf2_path, ios::out);
		sf2_file.setf(ios::fixed, ios::floatfield);

		int num_points = PointCloud[i].GetNumItem();
		for(int iPoint=0; iPoint<num_points; iPoint++)
		{
			sf1_file<<iPoint+1<<"\t";
			sf1_file<<PointCloud[i][iPoint].East_Last<<"\t";
			sf1_file<<PointCloud[i][iPoint].North_Last<<"\t";
			sf1_file<<PointCloud[i][iPoint].EH_Last<<endl;
		}
		
		sf1_file.close();

		int num_tris = TINList[i].nList;
		for(int iTri=0; iTri<num_tris; iTri++)
		{
			sf2_file<<"Patch_"<<iTri+1<<"  1   1"<<endl;
			sf2_file<<TINList[i].GetAt(iTri)->Contents[0]<<"\t";
			sf2_file<<TINList[i].GetAt(iTri)->Contents[1]<<"\t";
			sf2_file<<TINList[i].GetAt(iTri)->Contents[2]<<endl;

			sf2_file<<TINList[i].GetAt(iTri)->Contents[4+0]<<"\t";
			sf2_file<<TINList[i].GetAt(iTri)->Contents[4+1]<<"\t";
			sf2_file<<TINList[i].GetAt(iTri)->Contents[4+2]<<endl;

			sf2_file<<TINList[i].GetAt(iTri)->Contents[8+0]<<"\t";
			sf2_file<<TINList[i].GetAt(iTri)->Contents[8+1]<<"\t";
			sf2_file<<TINList[i].GetAt(iTri)->Contents[8+2]<<endl;
		}

		
		sf2_file.close();
	}

	
}