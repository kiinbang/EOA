// SMParallel.h interface for the CParallel class.
//
//////////////////////////////////////////////////////////////////////

/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/

#if !defined(AFX_PARALLEL_H__1CB3824A_BFFB_4646_A91D_6D9FC73DA415__INCLUDED_)
#define AFX_PARALLEL_H__1CB3824A_BFFB_4646_A91D_6D9FC73DA415__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMModifiedParallel.h"

/**
*@class CSMParallel
*@brief This is the class for parallel projection<br>
*and derived from SMModifiedParallel class<br>
*[Parallel Projection Model]<br>
*row = (A1X + A2Y + A3Z + A4)<br>
*col = (A5X + A6Y + A7Z + A8).
*/
class CSMParallel:public SMModifiedParallel
{
public:
	CSMParallel();
	virtual ~CSMParallel();
	virtual CString DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize);

protected:
	/**
	*Bundle Adjustment(Parallel Projection)
	*/
	virtual CString DoInitModeling(unsigned int num_param);
	/**
	*Get row value using approximate parameters.
	*/
	virtual double Func_row(_GCP_ G, unsigned int index);
	virtual double Func_row(_GCL_ G, unsigned int index);
	/**
	*Get col value using approximate parameters.
	*/
	virtual double Func_col(_GCP_ G, unsigned int index);
	virtual double Func_col(_GCL_ G, unsigned int index);

	/**Cal_PDs<br>
	*Partial derivatives
	*@param G ground point coord
	*@param ret1 partial derivative(row)
	*@param ret2 partial derivative(col)
	*@param num_param number of parameters of sensor model
	*@param index scene #
	*/
	virtual void Cal_PDs(_GCP_ G, double* ret, unsigned int index);
	virtual void Cal_PDs(_GCP_ G, double* ret1, double* ret2, unsigned int index);
	/**
	*Partial derivatives for a line
	*@param GL1 ground control line
	*@param GL2 ground control line
	*@param IL image control line
	*@param ret partial derivatives
	*@param index scene #
	*@param num_param number of parameters of sensor model
	*/
	virtual void Cal_PDs_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int image_index, unsigned int num_param);
	
	/**
	*Parameters de-normalization.
	*/
	virtual SMParam ParamDenormalization(SMParam param, unsigned int num_param);

};

#endif // !defined(AFX_PARALLEL_H__1CB3824A_BFFB_4646_A91D_6D9FC73DA415__INCLUDED_)
