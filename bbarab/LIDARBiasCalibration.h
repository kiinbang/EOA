/*
 * Copyright (c) 2006-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// LIDARBiasCalibration.h: interface for the CLIDARBiasCalibration class.
//
//////////////////////////////////////////////////////////////////////
//
//Written by Bang, Ki In
//
//Since 2006/06/28
//
//This file is for the CLIDARBiasCalibration class which support LiDAR data calibration (bias).
//	1) biases in boresight: bias_X, bias_Y, bias_Z, bias_O, bias_P, bias_K
//	2) bias in ranging distance: bias_ranging
//
//
//
//
#if !defined(__INTERSOLUTION__SPACEMATICS__BBARAB__LIDAR__BIAS_CALIBRATION__)
#define __INTERSOLUTION__SPACEMATICS__BBARAB__LIDAR__BIAS_CALIBRATION__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "collinearity.h"
#include "LIDARRawPointList.h"
#include "SMMatrix.h"
#include "SMDataStruct.h"
#include "leastsquare.h"

//#include "../Image_Util/Image.h"
//#include "../Image_Util/ImagePixel.h"
#include "../bbarab/Image_Util/BasicImage.h"

#include <iomanip>
#include "Collinearity.h"
#include "UtilityGrocery.h"

//#include <vld.h>//memory leak detector

#include <fstream>

using namespace SMATICS_BBARAB;


namespace LIDARCALIBRATION_BBARAB
{

#define NUM_UNKNOWNS 7

/**
* @class CLIDARBiasCalibration
* @brief class for LIDAR bias calibration\n
* @author Bang, Ki In
* @version 1.0
* @Since 2006-06-28
* @bug N/A.
* @warning N/A.
*
* \nThis is a class of LIDAR bias calibration program.
* <ul> 
* <li> boresight biases
*	<ol>
*	<li> Spatial(3) and rotational(3) biases in boresight
*	<li> ranging bias (1)
*	</ol>
* </ul>\n
*/
class CLIDARBiasCalibration
{
public:

	CLIDARBiasCalibration();
	virtual ~CLIDARBiasCalibration();

public:
	CSMList<CLIDARPlanarPatch> PatchList;/**< List of patches */
	CSMList<CLIDARPlanarPatch> OriginPatchList;/**< List of original patches */
	CSMList<SM3DPoint> ControlPoints;/**< List of patches */
	CSMList<SM3DPoint> OriginalControlPoints;/**< List of patches */
	CSMList<CLIDARRawPoint> RawPointList;
	CLIDARConfig Config;/**< List of config info */
	CLIDARConfig OriginConfig;/**< List of original config info */

	CSMList<int> filecontents;//text file contents information
	
	//vertex points
	double XA, YA, ZA;
	double XB, YB, ZB;
	double XC, YC, ZC;
	double K1, K2, K3, K4;
	CSMMatrix<double> K123;
	
	//boresight vectior (spatial offset + spatial bias)
	CSMMatrix<double> Pvector;

	//offset angle + bias angle
	double dO, dP, dK;
	CRotationcoeff2 Offset_R;
	//partial derivatives for boresight angles
	CRotationcoeff2 dOffsetR_dO;
	CRotationcoeff2 dOffsetR_dP;
	CRotationcoeff2 dOffsetR_dK;

public:
	/**ReadIndexFile
	* Description	    : To read information of input file contents
	*@param CString fileinfopath:	: input file contents information
	*@param CSMList<int> &index	: index of contents
	*@return bool
	*/
	bool ReadIndexFile(CString fileinfopath, CSMList<int>& index);

	/**ReadRecord
	* Description	    : To read each record (point) in the input file
	*@param fstream& infile	: fstream of the input file
	*@param CLIDARCalibrationPoint& point	: LIDAR calibration point
	*@return bool
	*/
	bool ReadRecord(fstream &infile, CSMList<int>& index, CLIDARCalibrationPoint &point);
	
	/**ExtractPlanarPatch
	* Description	    : To extract points belong to patches from LIDAR raw data
	*@param CString Cfgpath: LIDAR config file path
	*@param CString Lidarpath: Raw LIDAR data file path
	*@param CString Vertexpath: Vertex file path
	*@param CString Planarpath: Output file path
	*@return void
	*/
	void ExtractPlanarPatch(CString Cfgpath, CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision);
	void ExtractPlanarPatch_Brazil(CString Cfgpath, CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision);
	void ExtractSelectedRegion_Brazil(CString Cfgpath, CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision);
	void ExtractSelectedRegion_TXT(CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision, double areathreshold, bool bIntensity, CString rawpath);
	void ExtractSelectedRegion_TXT(CString Lidarpath, CString Vertexpath, CString Planarpath, double lidarprecision, bool bIntensity, double areathreshold=2.0, CString rawpath="");
	void BatchExtractPlanarPatch(CString Cfgpath, CString Prjpath, double lidarprecision);
	
	/**RunCalibration
	* Description	    : To run a LIDAR calibration process
	*@param CString cfgfilepath:
	*@param CString vtxfilpath
	*@param CString planarpointspath
	*@return void
	*/
	bool RunCalibration(CString cfgfilepath, CString vtxfilpath, CString planarpointspath, const char resultfile[], double lidarprecison, double Small_threshold_sigam, double Big_threshold_sigam, int max_iter);
	bool RunCalibration_Brazil(CString cfgfilepath, CString vtxfilpath, CString planarpointspath, const char resultfile[], double lidarprecison, double Small_threshold_sigam, double Big_threshold_sigam, int max_iter);
	bool RunCalibration_Titan(CString cfgfilepath, CString inputfilepath, CString indexfile, CString vertexfile, CString resultfile, double lidarprecison, double Small_threshold_sigam, double Big_threshold_sigam, int max_iter, bool bPoint);
	bool RunCalibration(CString cfgfilepath, const char resultfile[], double lidarprecison, double threshold_sigam, double Big_threshold_sigam, int max_iter);

	/**ReadPlanarPoints
	* Description	    : To read LIDAR raw data for the points in planar patches. This is called by ReadPlaneVertex.
	*@param CString planarpoints: file path of LIDAR points in planar patches
	*@return void
	*/
	void ReadPlanarPoints(CString planarpoints);

	void ReadBrazilStripTxt(CString striptxtpath, CString rawpath);
	void ReadBrazilStripTxt_old(CString striptxtpath, CString trajectorypath, CString configpath, CString rawpath, double Tthreshold);

	void RemoveOutlier(CString stripxyzpath, CString newpath, double threshold);

	void BrazilTrajectoryDraw(CString trajectorypath, CString imgpath, double res);

	void RawDataTrajectoryDraw(CString trajectorypath, CString imgpath, double res);

	void SeparateBrazilStripTxt(CString striptxtpath, CString newpath, double Tthreshold);

	void BrazilStrip2Raw(CString striptxtpath, CString trajectorypath, CString rawpath);

	void ExtractIntensityFromBrazilStrip(CString striptxtpath, CString trajectorypath, CString configpath, CString intensitypath);

private:

	void MatrixOut(fstream &file, CLeastSquareLIDAR &LS);

	/**LIDAREQ
	* Description	    : To calculate parameters of a plane equation
	*@param CLIDARRawPoint point: LIDAR raw point
	*@param CLIDARConfig config: LIDAR config information
	*@return CSMMatrix<double>: calculated ground position
	*/
	CSMMatrix<double> LIDAREQ(CLIDARRawPoint point, CLIDARConfig config);
	CSMMatrix<double> LIDAREQ_Brazil1(CLIDARRawPoint point, CLIDARConfig config);
	CSMMatrix<double> LIDAREQ_Brazil2(CLIDARRawPoint point, CLIDARConfig config);
	CSMMatrix<double> LIDAREQ_Brazil3(CLIDARRawPoint point, CLIDARConfig config);
	CSMMatrix<double> LIDAREQ_Point_Titan(CLIDARRawPoint point, CLIDARConfig config);

	/**Cal_PDs<br>
	*Partial derivatives
	*@param CLIDARRawPoint point: LIDAR raw point
	*@param CLIDARConfig config: LIDAR config information
	*@param double* coeff: parameters of a plane equation
	*@param CSMMatrix<double> &jmat: jma(element of JMAT)
	*@return void
	*/
	void Cal_PDs(CLIDARRawPoint point, CLIDARConfig config, double* coeff, CSMMatrix<double> &jmat);

	/**Cal_PDs_New<br>
	*Partial derivatives
	*@param CLIDARRawPoint point: LIDAR raw point
	*@param CLIDARConfig config: LIDAR config information
	*@param double* coeff: parameters of a plane equation
	*@param CSMMatrix<double> &jmat: jma(element of JMAT)
	*@return void
	*/
	void Cal_PDs_New(CLIDARRawPoint point, CLIDARConfig config, double* coeff, CSMMatrix<double> &jmat);
	
	/**Cal_PDsWithVolume
	* Description	    : 
	*@param CLIDARRawPoint point: LIDAR raw point
	*@param CLIDARConfig config: LIDAR config information
	*@param CLIDARPlanarPatch patch
	*@param CSMMatrix<double> &jmat
	*@param CSMMatrix<double> &bmat
	*@param CSMMatrix<double> &kmat
	*@param double area : area of a patche
	*@return void 
	*/
	void Cal_PDsWithVolume(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat);
	void Cal_PDsWithPoint_Titan(SM3DPoint P, CLIDARRawPoint point, CLIDARConfig config, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_Point, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat);
	void Cal_PDsWithVolume_Titan(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat);
	void Cal_PDsWithVolume_Brazil1(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat);
	void Cal_PDsWithVolume_Brazil2(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat);
	void Cal_PDsWithVolume_Brazil3(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_vertexA, CSMMatrix<double> &jmat_vertexB, CSMMatrix<double> &jmat_vertexC, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat);
	void Cal_PDsWithVolume_old(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat);
	void Cal_PDsWithND(CLIDARRawPoint point, CLIDARConfig config, CLIDARPlanarPatch patch, CSMMatrix<double> &jmat, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat, double AREA);

	/**Solve
	* Description	    : To read LIDAR raw data for the points in planar patches. This is called by ReadPlaneVertex.
	*@param double LIDAR_PRECISION: precision of LIDAR data
	*@param const char resultfile[]: file path of output
	*@return void
	*/
	bool Solve(double LIDAR_PRECISION,  const char resultfile[]);
	bool Solve_New(double LIDAR_PRECISION,  const char resultfile[]);
	bool SolveWithVolume_RNM(double LIDAR_PRECISION, const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold);
	bool SolveWithVolume_RNM_Brazil(double LIDAR_PRECISION, const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold);
	bool SolveWithPoint_RNM_Titan(double LIDAR_PRECISION, const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold);
	bool SolveWithVolume_RNM_Titan(double LIDAR_PRECISION, const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold);
	bool SolveWithVolume(double LIDAR_PRECISION,  const char resultfile[], int max_iter=20, double QThreshold = 1.0e-6);
	bool SolveWithVolume_old(double LIDAR_PRECISION, const char resultfile[]);
	bool SolveWithND(double LIDAR_PRECISION,  const char resultfile[]);

	/**Get correlation matrix from variance-covariance matrix*/
	CSMMatrix<double> Correlation(CSMMatrix<double> VarCov);

	int FindCStringID(CSMList<CString> &list, CString ID);

public: //static member functions

	/**BiasCalibrationResultTest
	* Description	    : To calculate parameters of a plane equation
	*@param CString AdjustedCfgpath	: Adjusted LIDAR configuration file
	*@param CString TrueCfgpath	: True LIDAR configuration file
	*@param CString Lidarpath	: LIDAR raw data path
	*@param CString outpath	: Output file path
	*@return void
	*/
	static CString BiasCalibrationResultTest(CString AdjustedCfgpath, CString TrueCfgpath, CString Lidarpath, CString outpath);
	static CString BiasCalibrationResultTest_Brazil(CString AdjustedCfgpath, CString TrueCfgpath, CString Lidarpath, CString outpath);
	void BiasCalibrationResultTestwithPlaneFitting(CString AdjustedCfgpath, CString TrueCfgpath, CString Patchpath, CString VTXpath, CString outpath, bool bBrazil);

	/**CalPlaneCoeff
	* Description	    : To calculate parameters of a plane equation
	*@param double* points: vertex points
	*@param int num_points: number of vertex points
	*@param double* coeff: Parameters of a plane equation
	*@return void
	*/
	static void CalPlaneCoeff(double* points, int num_points, double* coeff);
	static void CalPlaneCoeff_New(double* points, int num_points, double* coeff);

	/**CalTriangleArea
	* Description	    : To calculate area of a triangle.
	*@param double x1: coordinate (x1)
	*@param double y1: coordinate (y1)
	*@param double x2: coordinate (x2)
	*@param double y2: coordinate (y2)
	*@param double x3: coordinate (x3)
	*@param double y3: coordinate (y3)
	*@return void
	*/
	static double CalTriangleArea(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3);

	/**ReadPlaneVertex
	* Description	    : To read vertex data
	*@param CString vtxfile: vertex file path
	*@param CLIDARPlanarPatchList& PatchList: list of LIDAR planar patches
	*@return void
	*/
	static void ReadPlaneVertex(CString vtxfile, CSMList<CLIDARPlanarPatch> &PatchList, double min[], double max[]);

	/**Read3DPoints
	* Description							: To read 3D points
	*@param CString filepath				: point-file path
	*@param CSMList<SM3DPoint> &PointList	: list of points
	*@return void
	*/
	static void ReadTerraPoints(CString filepath, CSMList<SM3DPoint> &PointList, CSMList<CLIDARRawPoint> &LidarRawList);

	void ReadPatch(CString filepath, CSMList<SM3DPoint> &PointList, CSMList<CLIDARRawPoint> &LidarRawList);

	static CSMMatrix<double> CLIDARBiasCalibration::Cal_ErrorPropagation(
		/*GPS[m]*/double sigma_GPSX, double sigma_GPSY, double sigma_GPSZ,
		/*INS angle[deg]*/double INS_O, double sigma_INS_O, double INS_P, double sigma_INS_P, double INS_K, double sigma_INS_K, 
		/*Offset vector[m]*/double Px, double sigma_Px, double Py, double sigma_Py, double Pz, double sigma_Pz,
		/*Offset angle[deg]*/double dO, double sigma_dO, double dP, double sigma_dP, double dK, double sigma_dK,
		/*Range[m]*/double dist, double sigma_dist,
		/*Scan angles[deg]*/double alpha, double sigma_alpha, double beta, double sigma_beta
		);
};

} //namespace LIDARCALIBRATION_BBARAB

#endif // !defined(__INTERSOLUTION__SPACEMATICS__BBARAB__LIDAR__BIAS_CALIBRATION__)