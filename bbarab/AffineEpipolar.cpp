// AffineEpipolar.cpp: implementation of the AffineEpipolar class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AffineEpipolar.h"

#include <math.h>
#include <fstream.h>

#include "이미지관련/ImagePixel.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

AffineEpipolar::AffineEpipolar(CString infilename)
{	
	//1. 데이터 읽기
	
	ReadData(infilename);	//데이터를 읽는다.
	ReadImage();			//영상을 메모리로 불러온다. 2의 좌표변환에 사용되는 translation 계산한다.
	Coordinate_Print("1_입력된_영상좌표.txt",LeftCoord,RightCoord);	//입력된 좌표출력 

	//2. LeftTop이 원점인 영상좌표를 영상의 중심이 원점인 영상좌표로 바꾼다.

	Coord2Temp();			//Coord의 좌표를 Coord_Temp에 저장 
	All_Image2Photo();		//LeftTop이 원점인 영상좌표를 영상의 중심이 원점인 영상좌표로 바꾼다.
	Coordinate_Print("2_영상의_중심을_원점으로_좌표변환된_영상좌표.txt",LeftCoord,RightCoord);
	

	//3. 좌우 영상의 좌표에 대하여 ScanLine을 따라 Approxmate Affine Image를 만든다.

	Coord2Temp();					//Coord의 좌표를 Coord_Temp에 저장 
	MakeAffineImage_ScanLine();		//ScanLine을 따라 Approxmate Affine Image를 만든다(Parallel Projection).
	Coordinate_Print("3_Approximate_Affine_영상좌표.txt",LeftCoord,RightCoord);


	//4. Approximate Epipolar Direction을 결정한다.	즉, C1,C2,C3,C4를 구한다.

	GetEpipolarDirection(LeftCoord,RightCoord);

	
	//5. 원래의 영상의 좌표로 EpipolarLine을 따라 Affine Image를 만든다.	
	
	MakeAffineImage_EpipolarLine();
	Coordinate_Print("4_Approximate_Epipolar_Line_영상좌표.txt",LeftCoord,RightCoord);

	
	//6. Affine Transformation Coefficient를 구한다. 즉 K1,K2,K3,K4,K5,K6를 구한다.

	GetK(LeftCoord,RightCoord);

	
	//7. Affine Transformation 왼쪽영상을 virtual plane에 투영

	Coord2Temp();
	ProjectLeftImageToVirtualPlane();
	Coordinate_Print("5_LeftImage_on_the_Virtual_Plane_영상좌표.txt",LeftCoord,RightCoord);


	//8. Accurate Epipolar Direction을 결정한다.	즉, C1,C2,C3,C4를 구한다.

	GetEpipolarDirection(LeftCoord, RightCoord);


	//9. 좌표를 Epipolar Line 방향으로 회전시킨다.

	Coord2Temp();
	RotationAlongEpipolarLine();
	Coordinate_Print("6_Rotate_along_Epipolar_Line_영상좌표.txt",LeftCoord,RightCoord);
	Y_Parallax_Print("7_Check_Y_Parallax_영상좌표.txt",LeftCoord,RightCoord);

	//10. Resampling
	GetEpipolarImageSize();		//Direct로 영상크기 결정
	AlignEpipolarImage();		//두 영상의 Row를 맞춰준다.
	ResamplingEpipolarImage(LeftEpipolarName,RightEpipolarName);	//Indirect로 Resampling(Bilinear)

//	DrawLeftEpipolarLine();

}

AffineEpipolar::~AffineEpipolar()
{
	if(LeftCoord)
		delete [] LeftCoord;
	if(RightCoord)
		delete [] RightCoord;
	if(LeftCoord_Temp)
		delete [] LeftCoord_Temp;
	if(RightCoord_Temp)
		delete [] RightCoord_Temp;
	if(ControlPoint)
		delete [] ControlPoint;		
}

//////////////////////////////////////////////////////////////////////
// Functions
//////////////////////////////////////////////////////////////////////

BOOL AffineEpipolar::ReadData(CString infilename)
{
	char garbage[200];	//주석을 읽는 쓰레기 변수

	ifstream ReadFile(infilename);

	ReadFile>>garbage;	//Affine-Epipolar-Data-File
	
	ReadFile>>garbage;	//Input_Image_File(Left,Right)
	
//	ReadFile>>LeftImageName.GetBuffer(40)>>RightImageName.GetBuffer(40);
	ReadFile.getline(garbage,200,'\n');
	ReadFile.getline(LeftImageName.GetBuffer(200),200,'\n');
	ReadFile.getline(RightImageName.GetBuffer(200),200,'\n');

	ReadFile>>garbage;	//Output_Image_File(Left,Right)
//	ReadFile>>LeftEpipolarName.GetBuffer(40)>>RightEpipolarName.GetBuffer(40);
	ReadFile.getline(garbage,200,'\n');
	ReadFile.getline(LeftEpipolarName.GetBuffer(200),200,'\n');
	ReadFile.getline(RightEpipolarName.GetBuffer(200),200,'\n');

	ReadFile>>garbage;	//Principal_Distance(um)	
	ReadFile>>c_um;

	ReadFile>>garbage;	//CCD_Size(um)
	ReadFile>>CCD_Size;	

	c = c_um / CCD_Size;	//c의 단위를 pixel로 바꾼다.
	
	ReadFile>>garbage;	//Left Omega, Right Omega (Deg)
	ReadFile>>Left_Omega>>Right_Omega;

	Left_Omega = Left_Omega * 3.141592654 / 180.0;		//Deg2Rad
	Right_Omega = Right_Omega * 3.141592654 /180.0;

	ReadFile>>garbage;	//Number_of_Observations
	ReadFile>>Number_of_Observations;

	//좌표에 대한 변수를 관측수만큼 동적할당
	ControlPoint	= new GCP [Number_of_Observations];
	LeftCoord		= new Point2D [Number_of_Observations];
	RightCoord		= new Point2D [Number_of_Observations];
	LeftCoord_Temp  = new Point2D [Number_of_Observations];
	RightCoord_Temp = new Point2D [Number_of_Observations];
	
	ReadFile>>garbage>>garbage>>garbage>>garbage>>garbage;
//	ReadFile>>garbage>>garbage>>garbage;	

	CString Temp;
	
	for(int i=0;i<Number_of_Observations;i++)
	{
		ReadFile>>ControlPoint[i].ID>>LeftCoord[i].y>>LeftCoord[i].x>>RightCoord[i].y>>RightCoord[i].x;	//좌우 영상의 x,y 읽기
//		ReadFile>>ControlPoint[i].x>>ControlPoint[i].y>>ControlPoint[i].z;		//GCP 읽기 
	}

	ReadFile.close();
	
	return TRUE;
}

BOOL AffineEpipolar::ReadImage()
{
	//영상을 읽는다.
	Left_Original_Image.Load(LeftImageName);
	Right_Original_Image.Load(RightImageName);

	Left_half_Row = Left_Original_Image.GetHeight() / 2.0;
	Left_half_Col = Left_Original_Image.GetWidth() / 2.0;
		
	Right_half_Row = Right_Original_Image.GetHeight() / 2.0;
	Right_half_Col = Right_Original_Image.GetWidth() / 2.0;

	return TRUE;
}

BOOL AffineEpipolar::Coordinate_Print(CString outfilename,Point2D *Left, Point2D *Right)
{
	ofstream WriteFile(outfilename);
	CString Temp;
	Temp.Format(outfilename);
	WriteFile<<Temp<<endl<<endl;
	Temp.Format("   ID       Left X       Left Y	     Right X      Right Y");
	WriteFile<<Temp<<endl;

	for(int i=0;i<Number_of_Observations;i++)
	{
		Temp.Format("%5s %12.5lf %12.5lf %12.5lf %12.5lf",ControlPoint[i].ID,Left[i].x,Left[i].y,Right[i].x,Right[i].y);
		WriteFile<<Temp<<endl;		
	}	
	WriteFile.close();

	return TRUE;
}

BOOL AffineEpipolar::Y_Parallax_Print(CString outfilename,Point2D *Left, Point2D *Right)
{
	ofstream WriteFile(outfilename);
	CString Temp;
	Temp.Format(outfilename);
	WriteFile<<Temp<<endl<<endl;
	Temp.Format("   ID       Left X       Left Y	     Right X      Right Y	 Left X diff Right X diff     Residual");
	WriteFile<<Temp<<endl;

	double *Left_x_diff;
	double *Right_x_diff;;
	Left_x_diff  = new double [Number_of_Observations];
	Right_x_diff = new double [Number_of_Observations];

	for(int i=0;i<Number_of_Observations;i++)
	{
		Left_x_diff[i]  = LeftCoord[i].x  - LeftCoord[Number_of_Observations-1].x;
		Right_x_diff[i] = RightCoord[i].x - RightCoord[Number_of_Observations-1].x;
	}

	//Standard deviation 구하기
	double SD=0.0;
	double *V;
	V = new double [Number_of_Observations];

	//V[Number_of_Observations-1]=0 이므로 더해도 관계없다.
	for(i=0;i<Number_of_Observations;i++)
	{
		V[i] = Left_x_diff[i] - Right_x_diff[i];
		SD += V[i] * V[i];
	}

	SD = sqrt(SD/(Number_of_Observations-1));	

	for(i=0;i<Number_of_Observations;i++)
	{
		Temp.Format("%5s %12.5lf %12.5lf %12.5lf %12.5lf %12.5lf %12.5lf %12.5lf",ControlPoint[i].ID,Left[i].x,Left[i].y,Right[i].x,Right[i].y,Left_x_diff[i],Right_x_diff[i],V[i]);
		WriteFile<<Temp<<endl;		
	}
	WriteFile<<endl<<"Standard Deviation = +/- "<<SD<<endl;
	WriteFile.close();
	
	delete [] V;
	delete [] Left_x_diff;
	delete [] Right_x_diff;

	return TRUE;
}

BOOL AffineEpipolar::Coord2Temp()
{
	for(int i=0;i<Number_of_Observations;i++)
	{
		LeftCoord_Temp[i]  = LeftCoord[i];
		RightCoord_Temp[i] = RightCoord[i];
	}

	return TRUE;
}

BOOL AffineEpipolar::All_Image2Photo()
{
	for(int i=0;i<Number_of_Observations;i++)
	{
		LeftCoord[i]  = Left_Image2Photo(LeftCoord_Temp[i]);
		RightCoord[i] = Right_Image2Photo(RightCoord_Temp[i]);
	}

	return TRUE;
}

Point2D	AffineEpipolar::Left_Image2Photo(Point2D Image)
{
	Point2D Photo;

	Photo.x = Image.x - Left_half_Row;
	Photo.y = Image.y - Left_half_Col;

	return Photo;
}

Point2D	AffineEpipolar::Right_Image2Photo(Point2D Image)
{
	Point2D Photo;

	Photo.x = Image.x - Right_half_Row;
	Photo.y = Image.y - Right_half_Col;

	return Photo;
}

BOOL AffineEpipolar::MakeAffineImage_ScanLine()
{
	for(int i=0;i<Number_of_Observations;i++)
	{
		LeftCoord[i].y  = Make_Parallel_Projection(LeftCoord_Temp[i].y,Left_Omega); 		
		RightCoord[i].y = Make_Parallel_Projection(RightCoord_Temp[i].y,Right_Omega); 		
	}
	
	return TRUE;
}

double AffineEpipolar::Make_Parallel_Projection(double yp, double omega)
{
	return yp / ( 1 - tan(omega) * yp / c );	
}

BOOL AffineEpipolar::GetEpipolarDirection(Point2D *Left, Point2D *Right)
{
	Matrix <double> A(Number_of_Observations,4);
	Matrix <double> L(Number_of_Observations,1);
	Matrix <double> X;

	//왼쪽 영상에 대하여
	for(int i=0;i<Number_of_Observations;i++)
	{
		A(i,0) = Right[i].x ;
		A(i,1) = Left[i].y ;
		A(i,2) = Left[i].x ;
		A(i,3) = 1.0;

		L(i,0) = Right[i].y;
	}

	X = (A.Transpose()*A).Inverse()*(A.Transpose()*L);

	for(i=0;i<4;i++)
		C[i] = X(i,0);

	C_[0] = -C[2] / C[1];
	C_[1] =  1.0  / C[1];
	C_[2] = -C[0] / C[1];
	C_[3] = -C[3] / C[1];

	Left_angle  = atan(1.0/C_[0]);	
	Right_angle = atan(1.0/C[0]);

	return TRUE;	
}

BOOL AffineEpipolar::MakeAffineImage_EpipolarLine()
{
	for(int i=0;i<Number_of_Observations;i++)
	{
		LeftCoord[i].y  =  C_[0] * LeftCoord_Temp[i].x  + C_[1] * RightCoord_Temp[i].y + C_[2] * RightCoord_Temp[i].x + C_[3];
		RightCoord[i].y =  C[0]  * RightCoord_Temp[i].x + C[1]  * LeftCoord_Temp[i].y +  C[2]  * LeftCoord_Temp[i].x  + C[3];
	}
	
	return TRUE;
}

BOOL AffineEpipolar::GetK(Point2D *Left,Point2D* Right)
{
	Matrix <double> A(Number_of_Observations*2,6);
	Matrix <double> L(Number_of_Observations*2,1);
	Matrix <double> X;

	//Right에서 Left으로 가는 Affine parameters
	for(int i=0;i<Number_of_Observations;i++)
	{
		A(i*2  ,0) = Right[i].x;
		A(i*2  ,1) = Right[i].y;
		A(i*2  ,2) = 1.0;
		A(i*2  ,3) = 0.0;
		A(i*2  ,4) = 0.0;
		A(i*2  ,5) = 0.0;

		A(i*2+1,0) = 0.0;
		A(i*2+1,1) = 0.0;
		A(i*2+1,2) = 0.0;
		A(i*2+1,3) = Right[i].x;
		A(i*2+1,4) = Right[i].y;
		A(i*2+1,5) = 1.0;

		L(i*2  ,0) = Left[i].x;
		L(i*2+1,0) = Left[i].y;
	}

	X = (A.Transpose()*A).Inverse()*(A.Transpose()*L);

	for(i=0;i<6;i++)
		K[i] = X(i,0);

	//Left에서 Right으로 가는 Afiine parameters
	double deno;
	deno = K[1] * K[3] - K[0] * K[4];

	K_[0] = -K[4] / deno;
	K_[1] =  K[1] / deno;
	K_[2] = (K[2] * K[4] - K[1] * K[5]) / deno;

	K_[3] =  K[3] / deno;
	K_[4] = -K[0] / deno;
	K_[5] = (K[0] * K[5] - K[2] * K[3]) / deno;

	return TRUE;
}

BOOL AffineEpipolar::ProjectLeftImageToVirtualPlane()
{
	for(int i=0;i<Number_of_Observations;i++)
		LeftCoord[i] = Affine_Left2Right(LeftCoord_Temp[i]);

	return TRUE;
}

BOOL AffineEpipolar::RotationAlongEpipolarLine()
{
	for(int i=0;i<Number_of_Observations;i++)
	{
		LeftCoord[i]  = Left_Direct_Rotation(LeftCoord_Temp[i]);
		RightCoord[i] = Right_Direct_Rotation(RightCoord_Temp[i]);
	}

	return TRUE;
}

BOOL AffineEpipolar::GetEpipolarImageSize()
{
	GetLeftEpipolarImageSize();
	GetRightEpipolarImageSize();

	return TRUE;
}

BOOL AffineEpipolar::AlignEpipolarImage()
{	
	V_diff = Left_N_Translation.x - Right_N_Translation.x;
	
	if(V_diff>0)
		Right_N_Translation.x += V_diff;
	else if(V_diff<0)
		Left_N_Translation.x += V_diff;

	Left_Epipolar_Height  += (int)(V_diff+0.5);
	Right_Epipolar_Height += (int)(V_diff+0.5);

	(Left_Epipolar_Height > Right_Epipolar_Height) ? Epipolar_Height = Left_Epipolar_Height : Epipolar_Height = Right_Epipolar_Height;
	(Left_Epipolar_Width  > Right_Epipolar_Width)  ? Epipolar_Width  = Left_Epipolar_Width  : Epipolar_Width  = Right_Epipolar_Width;

	return TRUE;
}

BOOL AffineEpipolar::ResamplingEpipolarImage(LPCTSTR LeftEpipolarName,LPCTSTR RightEpipolarName)	
{
	ReSampling_LeftImage(LeftEpipolarName);
	ReSampling_RightImage(RightEpipolarName);

	return TRUE;
}

BOOL AffineEpipolar::GetLeftEpipolarImageSize()
{
	//왼쪽영상이 virtual plane에 투영된 영상의 끝 좌표를 저장하는 변수
	Point2D Left_N_LT;	
	Point2D Left_N_RT;
	Point2D Left_N_LB;
	Point2D Left_N_RB;

	Left_N_LT = Left_Direct_Rotation(Affine_Left2Right(Left_Image2Photo(0,0)));
	Left_N_RT = Left_Direct_Rotation(Affine_Left2Right(Left_Image2Photo(0,Left_Original_Image.GetWidth())));
	Left_N_LB = Left_Direct_Rotation(Affine_Left2Right(Left_Image2Photo(Left_Original_Image.GetHeight(),0)));
	Left_N_RB = Left_Direct_Rotation(Affine_Left2Right(Left_Image2Photo(Left_Original_Image.GetHeight(),Left_Original_Image.GetWidth())));

	//왼쪽영상이 virtual plane에 투영된 영상의 좌표의 최대 최소 값을 저장
	Point2D Max, Min;

	(Left_N_LB.x>Left_N_RB.x) ? Max.x = Left_N_LB.x : Max.x = Left_N_RB.x; 
	(Left_N_RT.y>Left_N_RB.y) ? Max.y = Left_N_RT.y : Max.y = Left_N_RB.y;
	(Left_N_LT.x<Left_N_RT.x) ? Min.x = Left_N_LT.x : Min.x = Left_N_RT.x;
	(Left_N_LT.y<Left_N_LB.y) ? Min.y = Left_N_LT.y : Min.y = Left_N_LB.y;

	Left_Epipolar_Width  = (int)(Max.y - Min.y + 0.5);
	Left_Epipolar_Height = (int)(Max.x - Min.x + 0.5);

	Left_N_Translation.x = - (Min.x + 0.5);
	Left_N_Translation.y = - (Min.y + 0.5);

	return TRUE;
}

Point2D	AffineEpipolar::Left_Image2Photo(double x,double y)
{
	Point2D Photo;

	Photo.x = x - Left_half_Row;
	Photo.y = y - Left_half_Col;

	return Photo;
}

Point2D AffineEpipolar::Affine_Left2Right(Point2D Left)
{
	Point2D Right;

	Right.x = K_[0] * Left.x + K_[1] * Left.y + K_[2];
	Right.y = K_[3] * Left.x + K_[4] * Left.y + K_[5];

	return Right;
}

Point2D AffineEpipolar::Affine_Right2Left(Point2D Right)
{
	Point2D Left;

	Left.x = K[0] * Right.x + K[1] * Right.y + K[2];
	Left.y = K[3] * Right.x + K[4] * Right.y + K[5];

	return Left;
}

Point2D AffineEpipolar::Left_Direct_Rotation(Point2D photo)
{
	Point2D Rotated_photo;

	//반시계 +
	Rotated_photo.x = photo.x * cos(Left_angle) - photo.y * sin(Left_angle);
	Rotated_photo.y = photo.x * sin(Left_angle) + photo.y * cos(Left_angle);

	return Rotated_photo;
}

BOOL AffineEpipolar::ReSampling_LeftImage(LPCTSTR LeftEpipolarName)
{
	Left_Epipolar_Image.Create(Epipolar_Width,Epipolar_Height,8);

	CPixelPtr LeftOriginalPixel(Left_Original_Image);	
	CPixelPtr LeftEpipolarPixel(Left_Epipolar_Image);

	Point2D Temp;
	Point2D Temp_int;
	Point2D Delta;

	double a,b,c,d,p,q;

	//Resampling
	for(int i=0;i<Left_Epipolar_Height;i++)
		for(int j=0;j<Left_Epipolar_Width;j++)
		{
			Temp = Left_Photo2Image(Affine_Right2Left(Left_Indirect_Rotation(Left_Epipolar_Image2Photo(i,j)))); 

			if(Temp.x<0.5||Temp.y<0.5||Temp.x>Left_Original_Image.GetHeight()-0.5||Temp.y>Left_Original_Image.GetWidth()-0.5)
				LeftEpipolarPixel[i][j] = 0;
			else
			{
				//Bilinear		
				Temp_int.x = (int)(Temp.x - 0.5);
				Temp_int.y = (int)(Temp.y - 0.5);
				
				Delta.x = Temp.x - Temp_int.x - 0.5;
				Delta.y = Temp.y - Temp_int.y - 0.5;	

				a = LeftOriginalPixel[(int)Temp_int.x][(int)Temp_int.y];
				b = LeftOriginalPixel[(int)Temp_int.x][(int)Temp_int.y + 1];
				c = LeftOriginalPixel[(int)Temp_int.x + 1][(int)Temp_int.y];
				d = LeftOriginalPixel[(int)Temp_int.x + 1][(int)Temp_int.y +1];
						
				p = a+(b-a) * Delta.y;
				q = c+(d-c) * Delta.y;

				LeftEpipolarPixel[i][j] = (unsigned char)(p+(q-p)*Delta.x+0.5);				
			}
		}

	Left_Epipolar_Image.Save(LeftEpipolarName);

	Left_Epipolar_Image.Free();

	return TRUE;
}

Point2D AffineEpipolar::Left_Photo2Image(Point2D Photo)
{
	Point2D Image;

	Image.x = Photo.x + Left_half_Row;
	Image.y = Photo.y + Left_half_Col;

	return Image;
}

Point2D AffineEpipolar::Left_Indirect_Rotation(Point2D rotated_photo)
{	
	Point2D Photo;

	Photo.x =   cos(Left_angle) * rotated_photo.x + sin(Left_angle) * rotated_photo.y;
	Photo.y = - sin(Left_angle) * rotated_photo.x + cos(Left_angle) * rotated_photo.y;

	return Photo;
}

Point2D AffineEpipolar::Left_Epipolar_Image2Photo(double x, double y)
{
	Point2D Photo;

	Photo.x = x - Left_N_Translation.x;
	Photo.y = y - Left_N_Translation.y;

	return Photo;
}

Point2D AffineEpipolar::Left_Epipolar_Photo2Image(Point2D photo)
{
	Point2D Image;

	Image.x = photo.x + Left_N_Translation.x;
	Image.y = photo.y + Left_N_Translation.y;

	return Image;
}

BOOL AffineEpipolar::GetRightEpipolarImageSize()
{
	Point2D Right_N_LT;	
	Point2D Right_N_RT;
	Point2D Right_N_LB;
	Point2D Right_N_RB;

	Right_N_LT = Right_Direct_Rotation(Right_Image2Photo(0,0));
	Right_N_RT = Right_Direct_Rotation(Right_Image2Photo(0,Right_Original_Image.GetWidth()));
	Right_N_LB = Right_Direct_Rotation(Right_Image2Photo(Right_Original_Image.GetHeight(),0));
	Right_N_RB = Right_Direct_Rotation(Right_Image2Photo(Right_Original_Image.GetHeight(),Right_Original_Image.GetWidth()));

	Point2D Max, Min;

	(Right_N_LB.x>Right_N_RB.x) ? Max.x = Right_N_LB.x : Max.x = Right_N_RB.x; 
	(Right_N_RT.y>Right_N_RB.y) ? Max.y = Right_N_RT.y : Max.y = Right_N_RB.y;
	(Right_N_LT.x<Right_N_RT.x) ? Min.x = Right_N_LT.x : Min.x = Right_N_RT.x;
	(Right_N_LT.y<Right_N_LB.y) ? Min.y = Right_N_LT.y : Min.y = Right_N_LB.y;

	Right_Epipolar_Width  = (int)(Max.y - Min.y + 0.5);
	Right_Epipolar_Height = (int)(Max.x - Min.x + 0.5);

	Right_N_Translation.x = - (Min.x + 0.5);
	Right_N_Translation.y = - (Min.y + 0.5);

	return TRUE;
}

Point2D	AffineEpipolar::Right_Image2Photo(double x,double y)
{
	Point2D Photo;

	Photo.x = x - Right_half_Row;
	Photo.y = y - Right_half_Col;

	return Photo;
}

Point2D AffineEpipolar::Right_Direct_Rotation(Point2D photo)
{
	Point2D Rotated_photo;

	//반시계 +
	Rotated_photo.x = photo.x * cos(Right_angle) - photo.y * sin(Right_angle);
	Rotated_photo.y = photo.x * sin(Right_angle) + photo.y * cos(Right_angle);

	return Rotated_photo;
}

BOOL AffineEpipolar::ReSampling_RightImage(LPCTSTR RightEpipolarName)
{
	Right_Epipolar_Image.Create(Epipolar_Width,Epipolar_Height,8);

	CPixelPtr RightOriginalPixel(Right_Original_Image);	
	CPixelPtr RightEpipolarPixel(Right_Epipolar_Image);

	Point2D Temp;
	Point2D Temp_int;
	Point2D Delta;

	double a,b,c,d,p,q;

	//Resampling
	for(int i=0;i<Right_Epipolar_Height;i++)
		for(int j=0;j<Right_Epipolar_Width;j++)
		{
			Temp = Right_Photo2Image(Right_Indirect_Rotation(Right_Epipolar_Image2Photo(i,j))); 

			if(Temp.x<0.5||Temp.y<0.5||Temp.x>Right_Original_Image.GetHeight()-0.5||Temp.y>Right_Original_Image.GetWidth()-0.5)
				RightEpipolarPixel[i][j] = 0;
			else
			{
				//Bilinear		
				Temp_int.x = (int)(Temp.x - 0.5);
				Temp_int.y = (int)(Temp.y - 0.5);
				
				Delta.x = Temp.x - Temp_int.x - 0.5;
				Delta.y = Temp.y - Temp_int.y - 0.5;	

				a = RightOriginalPixel[(int)Temp_int.x][(int)Temp_int.y];
				b = RightOriginalPixel[(int)Temp_int.x][(int)Temp_int.y + 1];
				c = RightOriginalPixel[(int)Temp_int.x + 1][(int)Temp_int.y];
				d = RightOriginalPixel[(int)Temp_int.x + 1][(int)Temp_int.y +1];
						
				p = a+(b-a) * Delta.y;
				q = c+(d-c) * Delta.y;

				RightEpipolarPixel[i][j] = (unsigned char)(p+(q-p)*Delta.x+0.5);				
			}			
		}

	Right_Epipolar_Image.Save(RightEpipolarName);

	Right_Epipolar_Image.Free();

	return TRUE;
}

Point2D AffineEpipolar::Right_Epipolar_Image2Photo(double x, double y)
{
	Point2D Photo;

	Photo.x = x - Right_N_Translation.x;
	Photo.y = y - Right_N_Translation.y;

	return Photo;
}

Point2D AffineEpipolar::Right_Epipolar_Photo2Image(Point2D photo)
{
	Point2D Image;	

	Image.x = photo.x + Right_N_Translation.x;
	Image.y = photo.y + Right_N_Translation.y;

	return Image;
}

Point2D AffineEpipolar::Right_Indirect_Rotation(Point2D rotated_photo)
{
	Point2D Photo;

	Photo.x =   cos(Right_angle) * rotated_photo.x + sin(Right_angle) * rotated_photo.y;
	Photo.y = - sin(Right_angle) * rotated_photo.x + cos(Right_angle) * rotated_photo.y;

	return Photo;
}

Point2D AffineEpipolar::Right_Photo2Image(Point2D Photo)
{
	Point2D Image;

	Image.x = Photo.x + Right_half_Row;
	Image.y = Photo.y + Right_half_Col;

	return Image;
}
/*
BOOL AffineEpipolar::DrawLeftEpipolarLine()
{
	CPixelPtr LeftEpipolarPixel(Left_Epipolar_Image);

	double b;
	int y;

	for(int i=0;i<Number_of_Observations;i++)
	{
		b = C_[1] * RightCoord[i].y + C_[2] * RightCoord[i].x + C_[3];
		
		for(int j=0;j<Left_Epipolar_Height;j++)
		{
			y = (int)(C_[0] * j + b + 0.5);

			if(y>0 && y<Left_Epipolar_Width)
				LeftEpipolarPixel[j][y] = 0;				
		}
			
	}

	Left_Epipolar_Image.Save(LeftEpipolarName);

	return TRUE;
}
*/
int AffineEpipolar::GetHorizontalDifference()
{
	double diff;
	diff=0;	

	for(int i=0;i<Number_of_Observations;i++)
		diff += (Right_Epipolar_Photo2Image(RightCoord[i]).y - Left_Epipolar_Photo2Image(LeftCoord[i]).y);

	diff /= Number_of_Observations;
	
	return (int)(diff+0.5);
}



























