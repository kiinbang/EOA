/*
 * Copyright (c) 2016-2017, KI IN Bang
 * All rights reserved.
 * SPATIAL&SPATIAL-MATICS for EarthOnAir
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <Eigen/Dense>
#include <cmath>

namespace EOA
{
	enum RotationAxis { Axis_1st = 0, Axis_2nd = 1, Axis_3rd = 2 };

	Eigen::MatrixXd GetRotationMat(const RotationAxis axis, const double angleRad)
	{
		Eigen::MatrixXd RotationMat(3, 3);

		const double cosAng = cos(angleRad);
		const double sinAng = sin(angleRad);

		unsigned int j1 = (axis + 1) % 3;
		unsigned int j2 = (axis + 2) % 3;

		RotationMat(axis, axis) = 1.0;
		RotationMat(axis, j1) = 0.0;
		RotationMat(axis, j2) = 0.0;

		RotationMat(j1, axis) = 0.0;
		RotationMat(j1, j1) = cosAng;
		RotationMat(j1, j2) = -sinAng;


		RotationMat(j2, axis) = 0.0;
		RotationMat(j2, j1) = sinAng;
		RotationMat(j2, j2) = cosAng;

		return RotationMat;
	}

	class Rmat
	{
	public:
		Rmat()
		{
			mRmat.resize(3, 3);
		}

		Rmat(const double omega, const double phi, const double kappa)
		{
			ang2Mat(omega, phi, kappa);
		}

		virtual ~Rmat() {}

		void ang2Mat(const double omega, const double phi, const double kappa)
		{
			mRmat = GetRotationMat(omega, phi, kappa);
		}

		static Eigen::MatrixXd GetRotationMat(const double omega, const double phi, const double kappa)
		{
			Eigen::MatrixXd RotationMat(3, 3);
			RotationMat(0, 0) = cos(phi)*cos(kappa);
			RotationMat(1, 0) = sin(omega)*sin(phi)*cos(kappa) + cos(omega)*sin(kappa);
			RotationMat(2, 0) = -cos(omega)*sin(phi)*cos(kappa) + sin(omega)*sin(kappa);

			RotationMat(0, 1) = -cos(phi)*sin(kappa);
			RotationMat(1, 1) = -sin(omega)*sin(phi)*sin(kappa) + cos(omega)*cos(kappa);
			RotationMat(2, 1) = cos(omega)*sin(phi)*sin(kappa) + sin(omega)*cos(kappa);

			RotationMat(0, 2) = sin(phi);
			RotationMat(1, 2) = -sin(omega)*cos(phi);
			RotationMat(2, 2) = cos(omega)*cos(phi);

			return RotationMat;
		}

		double& operator () (const unsigned int r, const unsigned int c)
		{
			return mRmat(r, c);
		}

		void Identity() { mRmat.resize(3, 3); mRmat.setIdentity(); }

	private:
		Eigen::MatrixXd mRmat;
	};
}