// SMPushbroomDLT.h interface for the CParallel class.
//
//////////////////////////////////////////////////////////////////////

/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/

#if !defined(__BBARAB__PUSHBROOM__DLT__GUPTA__HARTLEY__)
#define __BBARAB__PUSHBROOM__DLT__GUPTA__HARTLEY__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMModifiedParallel.h"

/**
*@class CSMPushDLT
*@brief This is the class for pushbroom DLT(Gupta&Hartley)<br>
*and derived from SMModifiedParallel class<br>
*[Pushbroom DLT(Gupta&Hatrley)]<br>
*row = (A1X + A2Y + A3Z + A4)<br>
*col = (A5X + A6Y + A7Z + A8)/(A9X + A10Y + A11Z +1).
*/
class CSMPushDLT:public SMModifiedParallel
{
public:
	//row = (A1X + A2Y + A3Z + A4)
	//
	//      (A5X + A6Y + A7Z + A8)
	//col = ----------------------
	//      (A9X + A10Y + A11Z +1)
	CSMPushDLT();
	virtual ~CSMPushDLT();
public:
	/**
	*Bundle Adjustment(Pushbroom DLT(Gupta&Hartley Model))
	*/
	virtual CString DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize);
private:
	/**
	*Calculate the initial value<br>
	*This func is called by DoModeling().
	*/
	virtual CString DoInitModeling(unsigned int num_param);

	/**
	*Get row value using approximate parameters.
	*/
	virtual double Func_row(_GCP_ G, unsigned int index);
	virtual double Func_row(_GCL_ G, unsigned int index);

	/**
	*Get col value using approximate parameters.
	*/
	virtual double Func_col(_GCP_ G, unsigned int index);
	virtual double Func_col(_GCL_ G, unsigned int index);

	/**Cal_PDs<br>
	*Partial derivatives
	*@param G ground point coord
	*@param I image point coord
	*@param ret1 partial derivative(row)
	*@param ret2 partial derivative(col)
	*@param num_param number of parameters of sensor model
	*@param index scene #
	*/
	virtual void Cal_PDs(_GCP_ G, double* ret1, double* ret2, unsigned int index);
	virtual void Cal_PDs_linear(_GCP_ G, _ICP_ I, double* ret, unsigned int scene_index);
	/**
	*Partial derivatives for a line
	*@param GL1 ground control line
	*@param GL2 ground control line
	*@param IL image control line
	*@param ret partial derivatives
	*@param index scene #
	*@param num_param number of parameters of sensor model
	*/
	virtual void Cal_PDs_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int image_index, unsigned int num_param);
	virtual void Cal_PDs_Line_linear(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int image_index, unsigned int num_param);

	/**
	*Denormalization for the 3D ground coords.
	*/
	virtual SMParam ParamDenormalization(SMParam param, unsigned int num_param);
private:
	/**
	*Get initial approximate value by only using points
	*/
	virtual void PointOBSInit(CSMList<_GCP_> &gcp, _SCENE_ &scene, unsigned int num_param, Matrix<double> &A, Matrix<double> &L, Matrix<double> &W, unsigned int scene_index);
	/**
	*Find line flag "B"
	*/
	bool FindFlagB(unsigned int GCL_Num, CSMList<_GCL_> gcl, CString LID, unsigned int nStart, _GCL_ &GLB);
	/**
	*Find line flag "C"
	*/
	bool FindFlagC(CSMList<_ICL_> &icl, CString LID, _ICL_ &ILA, _ICL_ &ILB);

};
#endif // !defined(__BBARAB__PUSHBROOM__DLT__GUPTA__HARTLEY__)