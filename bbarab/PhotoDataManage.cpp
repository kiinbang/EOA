/////////////////////////////////////////////////////////////////////
//PhotoDataManage.cpp
//made by BangBaraBang
/////////////////////////////////////////////////////////////////////
//CSMPhotoManage : Photo management class
/////////////////////////////////////////////////////////////////////////
//revision: 2002-10-25
///////////////////////////////////////////////////////////////////////
//GCPManage.h 에 통합되있던 photo data 관련 부분을 분리 독립시킴.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PhotoDataManage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
//#define new DEBUG_NEW <--MFC
#endif

//////////////////////////////////////////////////////////////////////
// CSMPhoto
//////////////////////////////////////////////////////////////////////

//Constructor
CSMPhoto::CSMPhoto()
{
	next = NULL;
	forward = NULL;
	Init();
}

//copy constructor
CSMPhoto::CSMPhoto(CSMPhoto& copy)
{
	next = NULL;
	forward = NULL;
	Init();

	CameraID = copy.CameraID;
	enable = copy.enable;
	icp = copy.icp;
	ID = copy.ID;
	kappa = copy.kappa;
	omega = copy.omega;
	phi = copy.phi;
	X = copy.X;
	Y = copy.Y;
	Z = copy.Z;
}

//Destructor
CSMPhoto::~CSMPhoto()
{
	icp.EmptyICP();
}

//Initialize
void CSMPhoto::Init(void)
{
	enable = true;
	CameraID = -999;

	icp.EmptyICP();

	ID = -999;

	omega = phi = kappa = 0.0;
	X = Y = Z = 0.0;
}

//substitution operator
void CSMPhoto::operator = (CSMPhoto& copy)
{
	CameraID = copy.CameraID;
	enable = copy.enable;
	icp = copy.icp;
	ID = copy.ID;
	kappa = copy.kappa;
	omega = copy.omega;
	phi = copy.phi;
	X = copy.X;
	Y = copy.Y;
	Z = copy.Z;
}

//////////////////////////////////////////////////////////////////////
// CSMPhotoManage
//////////////////////////////////////////////////////////////////////

//Constructor
CSMPhotoManage::CSMPhotoManage()
{
	num_Photo = 0;
	nextphoto = NULL;
	firstphoto.next = nextphoto;

}

//Destructor
CSMPhotoManage::~CSMPhotoManage()
{

}

//Delete Photo
bool CSMPhotoManage::DeletePhoto(long ID)
{
	CSMPhoto* targetphoto = GetPhotoHandle(ID);
	if(targetphoto != NULL)
	{
		targetphoto->forward->next = targetphoto->next;
		targetphoto->next->forward = targetphoto->forward;
		
		delete targetphoto;
		
		num_Photo--;
		
		return true;
	}
	return false;
}

//Get enabled Photo number
long CSMPhotoManage::GetEnabledPhotoNum(void)
{
	long num_disabledPhoto= 0;

	for(long i=0; i<num_Photo; i++)
	{
		bool value;
		value = GetEnablePhoto(i);
		if(false == value)
			num_disabledPhoto++;
	}

	return (num_Photo - num_disabledPhoto);
}

//Photo enabled true getting
bool CSMPhotoManage::GetEnablePhoto(long ID)
{
	CSMPhoto *targetphoto = NULL;

	targetphoto = GetPhotoHandle(ID);
	if(targetphoto != NULL)
	{		
		return targetphoto->enable;
	}
	else
	{
		return false;
	}
}

//Get Photo by ID
CSMPhoto* CSMPhotoManage::GetPhotobyID(long ID)
{
	return GetPhotoHandle(ID);
}

//Get Photo by sort number
CSMPhoto* CSMPhotoManage::GetPhotobySort(long sort)
{
	return GetPhotoHandle(GetPhotoID(sort));
}

//Get Photo ID
long CSMPhotoManage::GetPhotoID(long sort)
{
	CSMPhoto *targetphoto;
	targetphoto = &firstphoto;

	if((sort>num_Photo)||(sort<1))
		return -9999;
		
	for(long i=0; i<sort; i++)
	{
		targetphoto = targetphoto->next;
	}
	
	return targetphoto->ID;
}

//Get Photo number
long CSMPhotoManage::GetPhotoNum(void)
{
	return num_Photo;
}

//Photo enabled setting
bool CSMPhotoManage::SetEnablePhoto(long ID, bool boolvalue)
{
	CSMPhoto *targetphoto = NULL;
	
	targetphoto = GetPhotoHandle(ID);
	if(targetphoto != NULL)
	{
		targetphoto->enable = boolvalue;
		return true;
	}
	else
	{
		return false;
	}
}

//Insert Photo
bool CSMPhotoManage::InsertPhoto(CSMPhoto photo)
{
	if(num_Photo == 0)
	{
		firstphoto.next = new CSMPhoto;
		nextphoto = firstphoto.next;
		nextphoto->forward = &firstphoto;
	}
	
	num_Photo++;
	(*nextphoto) = photo;
	nextphoto->next = new CSMPhoto;
	nextphoto->next->forward = new CSMPhoto;
	nextphoto->next->forward = nextphoto;
	nextphoto = nextphoto->next;
	
	return true;
	
}

//Empty Photo Buffer
bool CSMPhotoManage::EmptyPhoto(void)
{
	CSMPhoto *targetphoto;
	
	//last gcp pointer
	if(num_Photo == 0)
	{
		return TRUE;
	}

	targetphoto = nextphoto->forward;

	if(targetphoto == NULL)
	{
		return false;
	}
	
	for(long i=0; i<num_Photo; i++)
	{
		targetphoto = targetphoto->forward;
		delete targetphoto->next;
		targetphoto->next = NULL;
	}
	
	targetphoto->Init();
	
	num_Photo = 0;

	return true;
}

//Private member function

//Get a certain Photo handle(pointer)
CSMPhoto* CSMPhotoManage::GetPhotoHandle(long ID)
{
	CSMPhoto* photobag;
	photobag = &firstphoto;

	if(-9999 == ID)
		return NULL;
		
	for(long i=0; i<num_Photo; i++)
	{
		photobag = photobag->next;
		if(ID == photobag->ID)
		{
			return photobag;
		}
	}
	
	return NULL;
}
