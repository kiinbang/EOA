#pragma once

//#include <stdio.h>

#define VLR VLR11
#define EVLR EVLR14

#define RecordSignature Reserved

typedef class VARIABLE_LENGTH_RECORDS_Header
{
public:
	//unsigned short	RecordSignature; VER 1.0 VLR
	unsigned short	Reserved;
	char			UserID[16];
	unsigned short	RecordID;
	unsigned short	RecordLengthAfterHeader;
	char			Description[32];
public:
	VARIABLE_LENGTH_RECORDS_Header() {}

	VARIABLE_LENGTH_RECORDS_Header(VARIABLE_LENGTH_RECORDS_Header& copy) { DataCopy(copy); }

	VARIABLE_LENGTH_RECORDS_Header& operator = (VARIABLE_LENGTH_RECORDS_Header& copy) { DataCopy(copy); return (*this); }

	ErrorMsg ReadVLR(CFile &lasfile)
	{
		if (!lasfile.Read((void*)&Reserved, 2))		return VLR_READ_ERROR;
		if (!lasfile.Read((void*)UserID, 16))	return VLR_READ_ERROR;
		if (!lasfile.Read((void*)&RecordID, 2))		return VLR_READ_ERROR;
		if (!lasfile.Read((void*)&RecordLengthAfterHeader, 2))		return VLR_READ_ERROR;
		if (!lasfile.Read((void*)Description, 32))	return VLR_READ_ERROR;

		return ERROR_FREE;
	}

	void DataCopy(VARIABLE_LENGTH_RECORDS_Header& copy)
	{
		Reserved = copy.Reserved;
		memcpy(UserID, copy.UserID, 16);
		RecordID = copy.RecordID;
		RecordLengthAfterHeader = copy.RecordLengthAfterHeader;
		memcpy(Description, copy.Description, 32);
	}
} VLR;

typedef class EXTENDED_VARIABLE_LENGTH_RECORDS_Header
{
public:
	unsigned short		Reserved;
	char				UserID[16];
	unsigned short		RecordID;
	unsigned long long	RecordLengthAfterHeader;
	char				Description[32];
public:
	EXTENDED_VARIABLE_LENGTH_RECORDS_Header() {}

	EXTENDED_VARIABLE_LENGTH_RECORDS_Header(EXTENDED_VARIABLE_LENGTH_RECORDS_Header& copy) { DataCopy(copy); }

	EXTENDED_VARIABLE_LENGTH_RECORDS_Header& operator = (EXTENDED_VARIABLE_LENGTH_RECORDS_Header& copy) { DataCopy(copy); return (*this); }

	ErrorMsg ReadVLR(CFile &lasfile)
	{
		if (!lasfile.Read((void*)&Reserved, 2))						return VLR_READ_ERROR;
		if (!lasfile.Read((void*)UserID, 16))					return VLR_READ_ERROR;
		if (!lasfile.Read((void*)&RecordID, 2))						return VLR_READ_ERROR;
		if (!lasfile.Read((void*)&RecordLengthAfterHeader, 8))		return VLR_READ_ERROR;
		if (!lasfile.Read((void*)Description, 32))					return VLR_READ_ERROR;

		return ERROR_FREE;
	}

	void DataCopy(EXTENDED_VARIABLE_LENGTH_RECORDS_Header& copy)
	{
		Reserved = copy.Reserved;
		memcpy(UserID, copy.UserID, 16);
		RecordID = copy.RecordID;
		RecordLengthAfterHeader = copy.RecordLengthAfterHeader;
		memcpy(Description, copy.Description, 32);
	}
} EVLR;
