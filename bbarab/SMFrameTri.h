/////////////////////////////////////////////////////////
//PushbroomTri.h
//made by bbarab
//revision: 2000-9-29
//revision: 2000-10-4
//인하대학교 토목공학과 지형정보연구실
//phshbroom image의 triangulation
///////////////////////////////////////////////////////////////
//revision: 2001-11-12
//ETRI: 위성영상 에서 항공사진용으로 개조
//Aerial Triangulation Program
///////////////////////////////////////////////////////////////
//revision: 2002-10-25
//CFrameBundle2002 버전 제작
//Aerial Triangulation Program
///////////////////////////////////////////////////////////////
#if !defined(__FRAME_TRIANGULATION__)
#define __FRAME_TRIANGULATION__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Collinearity.h"
#include "SMFrameIntersection.h"


class CFRameBundle;

//Class for Scene Point
class CScenePoint
{
public:
	CScenePoint() {}
	virtual ~CScenePoint() {}
	unsigned long PointID; //Point ID
	int PointProperty; //Point Property(1:GCP, 2: Check Point, 3: Pass Point)
	Point2D<double> coord;
};

//Class for GCP
class CGroundPoint
{
public:
	CGroundPoint() {}
	virtual ~CGroundPoint() {}
	unsigned long PointID;
	char *PointName;

	Point3D<double> coord;
};

//Class for Frame
class CFrame
{
public:
	CFrame()
	{
		framepoint = NULL;
	}
	virtual ~CFrame()
	{
		if(framepoint != NULL)
		{
			delete[] framepoint;
			framepoint = NULL;
		}
	}

	unsigned long FrameID; //Frame ID
	unsigned long StripID; //Strip ID

	char *FrameName; //Frame Name
	char *StripName;//Strip Name

	double f; //focal length
	double FH; //flying height
	double FilmWidth; //Film Width
	
	//외부표정요소
	Point3D<double> PC;//Perspcetive Center
	double Omega, Phi, Kappa;
	CScenePoint *framepoint;
	unsigned long num_point;

	void DeleteData()
	{
		if(framepoint != NULL)
		{
			delete[] framepoint;
			framepoint = NULL;
		}
	}

};

//Class for Scene
class CScene
{
public:
	CScene() 
	{
		scenepoint = NULL;
		a1=0.0; a2=0.0; a3=0.0;
		a4=0.0; a5=0.0; a6=0.0;

		b1=0.0; b2=0.0; b3=0.0;
		b4=0.0; b5=0.0; b6=0.0;
		
		c1=0.0; c2=0.0; c3=0.0;
		c4=0.0; c5=0.0; c6=0.0;
	}
	virtual ~CScene()
	{
		if(scenepoint != NULL)
		{
			delete[] scenepoint;
			scenepoint = NULL;
		}
	}
	int ID;
	//시간함수의 계수
	double a1, a2, a3;
	double a4, a5, a6;
	double b1, b2, b3;
	double b4, b5, b6;
	double c1, c2, c3;
	double c4, c5, c6;
	//외부표정요소
	Point3D<double> PC;//Satellite Position(center line)
	double Omega, Phi, Kappa;
	CScenePoint *scenepoint;
	long num_point;
};

////////////////////////////////////////////
//Frame Image Triangulation
////////////////////////////////////////////
class CFrameBundle
{
public:
	//////////////
	//constructor
	//////////////
	CFrameBundle();
	/////////////
	//destructor
	/////////////	
	virtual ~CFrameBundle();

//member variable
public:
	CGroundPoint *gcp;  //GCP Point
	CGroundPoint *check; //Check Point (Calculated)
	CGroundPoint *check_T; //Check Point (True)
	CGroundPoint *pass; //Pass(Tie) Point
	CFrame *frame; //Photo(Frame)

	unsigned int num_Camera; //number of Camera
	unsigned int num_GCP; //number of GCP
	unsigned int num_Check; //number of check point
	unsigned int num_Pass; //number of pass point
	unsigned int num_Frame; //number of Scene
	unsigned int num_ScenePoint; //number of Scene_Point

	CSMStr result; //result output buffer

	long DF; //degree of freedom

	unsigned short maxiteration;
	double maxcorrection;

	//Matrix
	//Matrix<double> N; //Normal Matrix
	//Matrix<double> *N_dot; //N_dot Matrix (belong to camera parameters)
	//Matrix<double> *W_dot; //W_dot Matrix (belong to camera parameters)
	//Matrix<double> *N_bar; //N_bar Matrix
	//Matrix<double> *N_2dot; //N_2dot Matrix (belong to 3D Points)

	Matrix<double> A, X, V, L;
	//편미분 계수
	double b11, b12, b13, b14, b15, b16;
	double b21, b22, b23, b24, b25, b26;
	double J, K;
	
	//member function
public:
	bool OpenDataFile(char *fname); //Read input file
	bool Solve(void); //execute bundle
	bool Make_A_L_Matrix(void); //manipulate A and L matrix
	bool InitEOParameters(void); //initialize exterior orientation parameters
	void Manipulate_A_L(unsigned long offset1, unsigned long offset2);
	bool Make_b_J_K(Point3D<double> PC, Point3D<double> GP, Point2D<double> p, double Omega, double Phi, double Kappa, double f); //calculate b, J, K coefficient 
	virtual void ResetData();
};




/*********************************************************/
/*                                                       */
/*항공사진 스트립을 위한 항공삼각 측량 프로그램 2002 버전*/
/*                                                       */
/*********************************************************/

#include "GCPManage.h"
#include "PhotoDataManage.h"
#include "SpaceMaticsModel.h"
#include "SpaceMaticsPhoto.h"


/************************/
/*Class CFrameBundle2002*/
/************************/
class CFrameBundle2002 
{
public:
	//////////////
	//constructor
	//////////////
	CFrameBundle2002();

	/////////////
	//destructor
	/////////////	
	virtual ~CFrameBundle2002();

//member variable
public:


private:
	//Camera Data
	CCamera *m_CameraData;
	int num_Camera;

	//Photo Data
	CPhoto *m_PhotoData;
	int num_Photo;

	//GCP
	int num_GCP;
	CGCPManage m_GCP;

	//Check point
	int num_Check;
	CGCPManage m_Check;
	//Pass Point
	CGCPManage m_Pass;

	//Iteration condition
	int MAX_ITERATION;
	double MAX_CORRECTION;


public:
	//Data file input
	bool OpenDataFile(char *fname);

	//
	//Camera Data
	//
	//Set number of cameras
	void SetNumCamera(int num) {num_Camera = num; m_CameraData = new CCamera[num_Camera];} 
	//Get number of camera
	int GetNumCamera() {return num_Camera;}
	//Set camera
	bool SetCamera(int index, CCamera camera);
	//Get camera
	bool GetCamera(int index, CCamera *camera);
	
	//
	//Photo Data
	//
	//Set number of photos
	void SetNumPhoto(int num);
	//Get number of photos
	int GetNumPhoto() {return num_Photo;}
	//Insert photo
	bool SetPhoto(int index, CPhoto photo);
	//Get photo
	bool GetPhoto(int index, CPhoto *photo);
	//Set camera to photo
	bool SetCameraToPhoto(int camera_index, int photo_index);
	//Get camera on photo
	bool GetCameraOnPhoto(int index, CCamera *camera);
	//Set photo fiducial image coord
	bool SetPhotoFMImageCoord(int photo_index, int fm_index, Point2D<double> coord);
	//Get photo fiducial image coord
	bool GetPhotoFMImageCoord(int photo_index, int fm_index, Point2D<double> *coord);
	//Do IO
	//Get Image coord to Photo coord
	bool GetImage2Photo(int photo_index, Point2D<double> Icoord, Point2D<double> *Pcoord);
	//IO Check
	bool IsIOComplete(int photo_index, bool *check);

	//
	//Pass data
	//
	bool InsertPassData(int id, double x, double y, double z) {m_Pass.InsertGCP(x, y, z, id);}
	CGCPManage* GetPassData(void) {return &m_Pass;}
	
	//Reset data
	void ResetData();

private:

	//parameter approximation
	bool Approximation(int IO_option = 2);

	//IO
	bool RunIO(int option);
	//1: conformal 2: affine 3: bilinear 4: projective 5: non_linear projective

	//RO

};

#endif // !defined(__FRAME_TRIANGULATION__)