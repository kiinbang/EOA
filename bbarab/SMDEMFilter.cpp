// SMDEMFilter.cpp: implementation of the CSMDEMFilter class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SMDEMFilter.h"
#include "./Image_Util/Image.h"
#include "./Image_Util/ImagePixel.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define empty -999.999

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMDEMFilter::CSMDEMFilter()
{

}

CSMDEMFilter::CSMDEMFilter(CSMDEMFilter &copy)
{
	Copy(copy);
}

CSMDEMFilter::CSMDEMFilter(CString dempath, CString hdrpath)
{
	m_DEM.ReadDEM(dempath, hdrpath);
}

CSMDEMFilter::CSMDEMFilter(CTemplateDEM<double> &copydem)
{
	m_DEM = copydem;
}

CSMDEMFilter::~CSMDEMFilter()
{

}

CSMDEMFilter& CSMDEMFilter::operator=(CSMDEMFilter &copy)
{
	return Copy(copy);
}

CSMDEMFilter& CSMDEMFilter::Copy(CSMDEMFilter &copy)
{
	m_DEM = copy.m_DEM;
	return *this;
}

void CSMDEMFilter::ReadDEM(CString dempath, CString hdrpath)
{
	m_DEM.ReadDEM(dempath, hdrpath);
}

bool CSMDEMFilter::PlaneSearching_OneOrigin(int method, unsigned int searchsize, double variance, unsigned int max_iter_num, CString outpath, double dQuantizing, unsigned int min_num_points, bool bOutParam)
{
	stOutFile = outpath;
	limit_var = variance;

	if(bOutParam == true)
	{
		CString fname;
		fname = stOutFile;
		fname.Replace(".txt","_param.txt");
		paramfile = fopen(fname,"w");
		fprintf(paramfile, "[i\tj\ta\tb\tc]\r\n");
	}

	unsigned int i, j;

	//window size error check
	unsigned int half_size;
	if(searchsize%2 != 1)
	{
		searchsize = searchsize -1;
		half_size = searchsize/2;
	}
	else
	{
		half_size = searchsize/2;
	}
	
	//accumulator
	double *accu = new double[(m_DEM.height)*(m_DEM.width)*4];
	for(i=0; i<(m_DEM.height)*(m_DEM.width); i++)
	{
		int offset = (i*4);
		accu[offset+0] = (float)empty;
		accu[offset+1] = (float)empty;
		accu[offset+2] = (float)empty;
		accu[offset+3] = (float)+3.402823466e+30;//3.402823466 E + 38: float maximum value	
	}
		
	int max_buf_size = (m_DEM.height)*(m_DEM.width)*3;

	int num_non_planar=0;
	for(i=0; i<m_DEM.height; i++)//Y direction
	{
		for(j=0; j<m_DEM.width; j++)//X direction
		{
			double a, b, c;
			bool bRet;
			double pos_var;

			//
			//aX+bY+cZ+1=0
			//
			if(method == 0)
				bRet = GLSforCoeff((int)j, (int)i, (int)half_size, a, b, c, max_iter_num, pos_var);
			else
				//bRet = RobustGLSforCoeff((int)j, (int)i, (int)half_size, a, b, c, max_iter_num, pos_var, method);
				bRet = RobustLSforCoeff((int)j, (int)i, (int)half_size, a, b, c, max_iter_num, pos_var, method);

			if(bRet == true)
			{
				FillAccumulator_OneOrigin(true, i, j, accu, half_size, a, b, c, dQuantizing, max_buf_size, bOutParam);				
			}
			else
			{
				FillAccumulator_OneOrigin(false, i, j, accu, half_size, a, b, c, dQuantizing, max_buf_size, bOutParam);
				num_non_planar ++;
			}//if(bRet == true)
			
		}//end of j
	}//end of i

	min_a = max_a = (float)empty;
	min_b = max_b = (float)empty;
	min_c = max_c = (float)empty;

	//min and max (from each origin)
	for(i=0; i<m_DEM.height; i++)//Y direction
	{
		for(j=0; j<m_DEM.width; j++)//X direction
		{
			int index = i*m_DEM.width+j;
			int offset = (index*4);

			if(accu[offset+0] != (float)empty)
			{
				if(min_a == (float)empty)
				{
					min_a = max_a = accu[offset+0];
					min_b = max_b = accu[offset+1];
					min_c = max_c = accu[offset+2];
				}
				else
				{
					if(min_a>accu[offset+0]) min_a = accu[offset+0];
					if(max_a<accu[offset+0]) max_a = accu[offset+0];
					
					if(min_b>accu[offset+1]) min_b = accu[offset+1];
					if(max_b<accu[offset+1]) max_b = accu[offset+1];
					
					if(min_c>accu[offset+2]) min_c = accu[offset+2];
					if(max_c<accu[offset+2]) max_c = accu[offset+2];
				}
			}
		}
	}

	range_a = max_a - min_a;
	range_b = max_b - min_b;
	range_c = max_c - min_c;

	//quantization
	CString path = outpath;
	path.Replace(".txt","_frequencymap.bmp");

	Qinterval = dQuantizing;//<--no used

	QuantizingAccumulator_OneOrigin(accu, min_num_points, path);

	delete[] accu;

	if(bOutParam == true)
	{
		fclose(paramfile);
	}

	AfxMessageBox("--Completed!--");

	return true;
}

bool CSMDEMFilter::PlaneSearching_OneOrigin_old(int method, unsigned int searchsize, double variance, unsigned int max_iter_num, CString outpath, double dQuantizing, unsigned int min_num_points, bool bOutParam)
{
	stOutFile = outpath;
	limit_var = variance;

	if(bOutParam == true)
	{
		CString fname;
		fname = stOutFile;
		fname.Replace(".txt","_param.txt");
		paramfile = fopen(fname,"w");
		fprintf(paramfile, "[i\tj\ta\tb\tc]\r\n");
	}

	unsigned int i, j;

	//window size error check
	unsigned int half_size;
	if(searchsize%2 != 1)
	{
		searchsize = searchsize -1;
		half_size = searchsize/2;
	}
	else
	{
		half_size = searchsize/2;
	}
	
	//accumulator
	double *accu = new double[(m_DEM.height)*(m_DEM.width)*3];
		
	int max_buf_size = (m_DEM.height)*(m_DEM.width)*3;

	int num_non_planar=0;
	for(i=0; i<m_DEM.height; i++)//Y direction
	{
		for(j=0; j<m_DEM.width; j++)//X direction
		{
			double a, b, c;
			bool bRet;
			double pos_var;
			
			if(method == 0)
				bRet = GLSforCoeff((int)j, (int)i, (int)half_size, a, b, c, max_iter_num, pos_var);
			else
				//bRet = RobustGLSforCoeff((int)j, (int)i, (int)half_size, a, b, c, max_iter_num, pos_var, method);
				bRet = RobustLSforCoeff((int)j, (int)i, (int)half_size, a, b, c, max_iter_num, pos_var, method);

			//vector and distance
			//lx + my + nz = p
			//(l, m, n)에 수직이고, 원점에서 거리가 P인 평면.
			//ax + by + cz + 1 = 0
			double dist;
			double abc = sqrt(a*a + b*b + c*c);
			dist = 1.0/abc;
			//평면의 방향 코사인
			double l = -a*dist;
			double m = -b*dist;
			double n = -c*dist;

			a = l*dist; b = m*dist; c = n*dist;

			if(bRet == true)
			{
				FillAccumulator_OneOrigin_old(true, i, j, accu, a, b, c, max_buf_size, bOutParam);				
			}
			else
			{
				FillAccumulator_OneOrigin_old(false, i, j, accu, a, b, c, max_buf_size, bOutParam);
				num_non_planar ++;
			}//if(bRet == true)
			
		}//end of j
	}//end of i

	min_a = max_a = accu[0];
	min_b = max_b = accu[1];
	min_c = max_c = accu[2];

	//min and max (from each origin)
	for(i=0; i<m_DEM.height; i++)//Y direction
	{
		for(j=0; j<m_DEM.width; j++)//X direction
		{
			int index = i*m_DEM.width+j;
			int offset = (index*3);

			if(accu[offset+0]<0) continue;

			if(min_a>accu[offset+0]) min_a = accu[offset+0];
			if(max_a<accu[offset+0]) max_a = accu[offset+0];

			if(min_b>accu[offset+1]) min_b = accu[offset+1];
			if(max_b<accu[offset+1]) max_b = accu[offset+1];

			if(min_c>accu[offset+2]) min_c = accu[offset+2];
			if(max_c<accu[offset+2]) max_c = accu[offset+2];
		}
	}

	range_a = max_a - min_a;
	range_b = max_b - min_b;
	range_c = max_c - min_c;

	//quantization
	CString path = outpath;
	path.Replace(".txt","_frequencymap.bmp");

	Qinterval = dQuantizing;//<--no used

	QuantizingAccumulator_OneOrigin(accu, min_num_points, path);

	delete[] accu;

	if(bOutParam == true)
	{
		fclose(paramfile);
	}

	AfxMessageBox("--Completed!--");

	return true;
}

bool CSMDEMFilter::PlaneSearching_ThreeOrigins(int method, unsigned int searchsize, double variance, unsigned int max_iter_num, CString outpath, double dQuantizing, unsigned int min_num_points, bool bOutParam)
{
	fstream debugfile;

	CString debugfname = outpath;
	debugfname.Replace(".txt","_debug.tmp");
	debugfile.open(debugfname,ios::out);

	stOutFile = outpath;
	limit_var = variance;

	if(bOutParam == true)
	{
		CString fname;
		fname = stOutFile;
		fname.Replace(".txt","_param.txt");
		paramfile = fopen(fname,"w");
	}

	unsigned int i, j;

	//window size error check
	unsigned int half_size;
	if(searchsize%2 != 1)
	{
		searchsize = searchsize -1;
		half_size = searchsize/2;
	}
	else
	{
		half_size = searchsize/2;
	}
	
	//accumulator
	//double *accu = new double[(m_DEM.height)*(m_DEM.width)*searchsize*searchsize*3];
	double *accu = new double[(m_DEM.height)*(m_DEM.width)*3];
	double *accu_dist = new double[(m_DEM.height)*(m_DEM.width)*3];
	//for(i=0; i<(m_DEM.height)*(m_DEM.width)*searchsize*searchsize*3; i++)
	//{
	//	accu[i] = empty;/*< accumulator initializing*/
	//}

	//int max_buf_size = (m_DEM.height)*(m_DEM.width)*searchsize*searchsize*3;
	int max_buf_size = (m_DEM.height)*(m_DEM.width)*3;

	double origin[9];
	double value;

	//center_left
	m_DEM.GetX((m_DEM.width-1)/3, value);						origin[0] = value-10;
	m_DEM.GetY((m_DEM.height-1)/2, value);						origin[1] = value;
	m_DEM.GetZ((m_DEM.width-1)/3, (m_DEM.height-1)/2, value);	origin[2] = value+10;
	
	//right_center
	m_DEM.GetX((m_DEM.width-1)*2/3, value);						origin[3] = value+10;
	m_DEM.GetY((m_DEM.height-1)/2, value);						origin[4] = value;
	m_DEM.GetZ((m_DEM.width-1)*2/3, (m_DEM.height-1)/2, value);	origin[5] = value-10;

	//center
	m_DEM.GetX((m_DEM.width-1)/2, value);						origin[6] = value;
	m_DEM.GetY((m_DEM.height-1)/2, value);						origin[7] = value;
	m_DEM.GetZ((m_DEM.width-1)/2, (m_DEM.height-1)/2, value);	origin[8] = value;

	int num_non_planar=0;
	for(i=0; i<m_DEM.height; i++)//Y direction
	{
		for(j=0; j<m_DEM.width; j++)//X direction
		{
			double a, b, c;
			bool bRet;
			double pos_var;
			
			if(method == 0)
				bRet = GLSforCoeff(debugfile, (int)j, (int)i, (int)half_size, a, b, c, max_iter_num, pos_var);
			else
				//bRet = RobustGLSforCoeff_L1((int)j, (int)i, (int)half_size, a, b, c, max_iter_num, max_dist, pos_var, method);
				bRet = RobustLSforCoeff((int)j, (int)i, (int)half_size, a, b, c, max_iter_num, pos_var, method);

			if(bRet == true)
			{
				FillAccumulator_ThreeOrigins(true, i, j, accu, accu_dist, origin, a, b, c, max_buf_size, bOutParam);				
			}
			else
			{
				FillAccumulator_ThreeOrigins(false, i, j, accu, accu_dist, origin, a, b, c, max_buf_size, bOutParam);
				num_non_planar ++;
			}//if(bRet == true)
			
		}//end of j
	}//end of i

	min_dist1 = max_dist1 = accu_dist[0];
	min_dist2 = max_dist2 = accu_dist[1];
	min_dist3 = max_dist3 = accu_dist[2];

	//min and max (from each origin)
	for(i=0; i<m_DEM.height; i++)//Y direction
	{
		for(j=0; j<m_DEM.width; j++)//X direction
		{
			int index = i*m_DEM.width+j;
			int offset = (index*3);

			if(accu_dist[offset+0]<0) continue;

			if(min_dist1>accu_dist[offset+0]) min_dist1 = accu_dist[offset+0];
			if(max_dist1<accu_dist[offset+0]) max_dist1 = accu_dist[offset+0];

			if(min_dist2>accu_dist[offset+1]) min_dist2 = accu_dist[offset+1];
			if(max_dist2<accu_dist[offset+1]) max_dist2 = accu_dist[offset+1];

			if(min_dist3>accu_dist[offset+2]) min_dist3 = accu_dist[offset+2];
			if(max_dist3<accu_dist[offset+2]) max_dist3 = accu_dist[offset+2];
		}
	}
	range1 = max_dist1 - min_dist1;
	range2 = max_dist2 - min_dist2;
	range3 = max_dist3 - min_dist3;

	//quantization
	CString path = outpath;
	path.Replace(".txt","_frequencymap.bmp");

	Qinterval = dQuantizing;

	QuantizingAccumulator_ThreeOrigins(accu_dist, min_num_points, path);

	delete[] accu;
	delete[] accu_dist;

	if(bOutParam == true)
	{
		fclose(paramfile);
	}
	
	debugfile.close();

	AfxMessageBox("--Completed!--");

	return true;
}

void CSMDEMFilter::QuantizingAccumulator_ThreeOrigins(double *accu_dist, unsigned int min_num_points, CString outpath)
{
	int i, j;
	double *Q_accu_dist = new double[(m_DEM.height)*(m_DEM.width)*3];
	//unsigned int *Qaccu1;
	//unsigned int *Qaccu2;
	//unsigned int *Qaccu3;

	int num1 = int((max_dist1 - min_dist1)/Qinterval + 0.5);
	int num2 = int((max_dist2 - min_dist2)/Qinterval + 0.5);
	int num3 = int((max_dist3 - min_dist3)/Qinterval + 0.5);

	//Qaccu1 = new unsigned int[num1];
	//Qaccu2 = new unsigned int[num2];
	//Qaccu3 = new unsigned int[num3];

	//for(i=0; i<num1; i++) Qaccu1[i] = 0;
	//for(i=0; i<num2; i++) Qaccu2[i] = 0;
	//for(i=0; i<num3; i++) Qaccu3[i] = 0;
	
	for(i=0; i<(int)m_DEM.height; i++)
	{
		for(j=0; j<(int)m_DEM.width; j++)
		{
			int index = i*m_DEM.width+j;
			int offset = (index*3);

			if(accu_dist[offset+0]<0) continue;
			else
			{
				double dist1 = accu_dist[offset+0] - min_dist1;
				double dist2 = accu_dist[offset+1] - min_dist2;
				double dist3 = accu_dist[offset+2] - min_dist3;

				int index1 = int((dist1)/Qinterval + 0.5); if(index1 >= num1) continue;
				int index2 = int((dist2)/Qinterval + 0.5); if(index2 >= num2) continue;
				int index3 = int((dist3)/Qinterval + 0.5); if(index3 >= num3) continue;

				Q_accu_dist[offset+0] = index1;
				Q_accu_dist[offset+1] = index2;
				Q_accu_dist[offset+2] = index3;

				//Qaccu1[index1] = Qaccu1[index1] + 1;
				//Qaccu2[index2] = Qaccu2[index2] + 1;
				//Qaccu3[index3] = Qaccu3[index3] + 1;
			}
		}
	}

	CString path = outpath;
	path.Replace("_frequencymap.bmp","_distmap.bmp");
	DistMap_ThreeOrigins(path, Q_accu_dist);

	//FrequencyMap(outpath,Qaccu1, num1, Qaccu2, num2, Qaccu3, num3);
	FrequencyMap_ThreeOrigins(outpath,Q_accu_dist,min_num_points);

	delete[] Q_accu_dist;
	//delete[] Qaccu1;
	//delete[] Qaccu2;
	//delete[] Qaccu3;
}

void CSMDEMFilter::QuantizingAccumulator_OneOrigin(double *accu, unsigned int min_num_points, CString outpath)
{
	int i, j;

	CImage result;
	result.Create(m_DEM.width, m_DEM.height,24);
	CColorPixelPtr pixel(result);
	COLOR c;
	CColorPixel color;

	for(i=0; i<(int)m_DEM.height; i++)
	{
		for(j=0; j<(int)m_DEM.width; j++)
		{
			int index = i*m_DEM.width+j;
			int offset = (index*4);

			if(accu[offset+0]==(float)empty)
			{
				c.R = (BYTE)(0);
				c.G = (BYTE)(0);
				c.B = (BYTE)(0);
			}
			else
			{
				int index1 = int((accu[offset+0]-min_a)/range_a*255);
				int index2 = int((accu[offset+1]-min_b)/range_b*255);
				int index3 = int((accu[offset+2]-min_c)/range_c*255);

				c.R = (BYTE)(index1);
				c.G = (BYTE)(index2);
				c.B = (BYTE)(index3);
			}

			color << c;
			color >> pixel[(int)i][(int)j];
		}
	}

	CString path = outpath;
	path.Replace("_frequencymap.bmp","_distmap.bmp");
	
	result.Save(path);
}

void CSMDEMFilter::QuantizingAccumulator_OneOrigin_old(double *accu, unsigned int min_num_points, CString outpath)
{
	int i, j;
	double *Q_accu = new double[(m_DEM.height)*(m_DEM.width)*3];

	double ave_a=0., ave_b=0., ave_c=0.;
	double sum_da2=0., sum_db2=0., sum_dc2=0.;

	for(i=0; i<(int)m_DEM.height; i++)
	{
		for(j=0; j<(int)m_DEM.width; j++)
		{
			int index = i*m_DEM.width+j;
			int offset = (index*3);
			
			ave_a += accu[offset+0];
			ave_b += accu[offset+1];
			ave_c += accu[offset+2];
		}
	}

	ave_a = ave_a/(m_DEM.height)/(m_DEM.width);
	ave_b = ave_b/(m_DEM.height)/(m_DEM.width);
	ave_c = ave_c/(m_DEM.height)/(m_DEM.width);

	for(i=0; i<(int)m_DEM.height; i++)
	{
		for(j=0; j<(int)m_DEM.width; j++)
		{
			int index = i*m_DEM.width+j;
			int offset = (index*3);
			
			double da = ave_a - accu[offset+0];
			double db = ave_b - accu[offset+1];
			double dc = ave_c - accu[offset+2];

			sum_da2 += da*da;
			sum_db2 += db*db;
			sum_dc2 += dc*dc;
		}
	}

	double sd_a = sqrt(sum_da2/(m_DEM.height)/(m_DEM.width));
	double sd_b = sqrt(sum_db2/(m_DEM.height)/(m_DEM.width));
	double sd_c = sqrt(sum_dc2/(m_DEM.height)/(m_DEM.width));

	sd_a = .5; sd_b = .5; sd_c = .5;

	int num1 = int((max_a - min_a)/sd_a + 0.5);
	int num2 = int((max_b - min_b)/sd_b + 0.5);
	int num3 = int((max_c - min_c)/sd_c + 0.5);

	for(i=0; i<(int)m_DEM.height; i++)
	{
		for(j=0; j<(int)m_DEM.width; j++)
		{
			int index = i*m_DEM.width+j;
			int offset = (index*3);

			if(accu[offset+0]==0) continue;
			else
			{
				double d_a = accu[offset+0] - min_a;
				double d_b = accu[offset+1] - min_b;
				double d_c = accu[offset+2] - min_c;

				int index1 = int((d_a)/sd_a + 0.5); if(index1 >= num1) continue;
				int index2 = int((d_b)/sd_b + 0.5); if(index2 >= num2) continue;
				int index3 = int((d_c)/sd_c + 0.5); if(index3 >= num3) continue;

				Q_accu[offset+0] = index1;
				Q_accu[offset+1] = index2;
				Q_accu[offset+2] = index3;
			}
		}
	}

	//double *Q_accu_hist = new double[(m_DEM.height)*(m_DEM.width)*3];

	CString path = outpath;
	path.Replace("_frequencymap.bmp","_distmap.bmp");
	DistMap_OneOrigin_old(path, Q_accu);
	
	//FrequencyMap(outpath,Qaccu1, num1, Qaccu2, num2, Qaccu3, num3);
	//FrequencyMap_OneOrigins(outpath,Q_accu_dist,min_num_points);

	delete[] Q_accu;
	//delete[] Q_accu_hist;
}

void CSMDEMFilter::FillAccumulator_OneOrigin(bool bAvailable, 
								   unsigned int index_i, unsigned int index_j, 
								   double *accu, int half_size,
								   double a, double b, double c, double Threshold,
								   int max_buf_size, bool bOutParam)
{
	int height = m_DEM.height;
	int width = m_DEM.width;
	
	//vector and distance
	//lx + my + nz = p
	//(l, m, n)에 수직이고, 원점에서 거리가 P인 평면.
	//ax + by + cz + 1 = 0
	double dist;
	double abc = sqrt(a*a + b*b + c*c);
	dist = 1.0/abc;
	//평면의 방향 코사인
	double l = -a*dist;
	double m = -b*dist;
	double n = -c*dist;

	double a_, b_, c_;
	
	a_ = l*dist; b_ = m*dist; c_ = n*dist;
	
	for(int i=(int)index_i-half_size; i<=(int)index_i+half_size; i++)//Y direction
	{
		for(int j=index_j-half_size; j<=(int)index_j+half_size; j++)//X direction
		{
			if(((i)<0)||((i)>=height)) continue;
			if(((j)<0)||((j)>=width)) continue;

			int index = i*width+j;
			int offset = (index*4);

			double dist1;
			double X, Y, Z;

			if(false == m_DEM.GetX(j, X)) continue;
			if(false == m_DEM.GetY(i, Y)) continue;
			if(false == m_DEM.GetZ(j,i, Z)) continue;

			//dist1 = |aX+bY+cZ+1|/sqrt(a^2+b^2+c^2)

			dist1 = fabs(a*X+b*Y+c*Z+1)/sqrt(a*a+b*b+c*c);
					
			if((dist1<accu[offset+3])&&(dist1<Threshold))
			{
				accu[offset+0] = a_;
				accu[offset+1] = b_;
				accu[offset+2] = c_;
				accu[offset+3] = dist1;
			}			
			
		}//end of j
	}//end of i
}

void CSMDEMFilter::FillAccumulator_OneOrigin_old(bool bAvailable, 
								   unsigned int i, unsigned int j, 
								   double *accu,
								   double a, double b, double c,
								   int max_buf_size, bool bOutParam)
{
	int index = i*m_DEM.width+j;
	int offset = (index*3);
	
	if(offset<0) return;
	if(offset>max_buf_size) return;
	
	if(bAvailable == false)
	{
		accu[offset+0] = 0.;
		accu[offset+1] = 0.;
		accu[offset+2] = 0.;
		
		if(bOutParam == true)
		{
			fprintf(paramfile, "%lf\t%lf\t%le\t%le\t%le\r\n",(double)i,(double)j,a, b, c);
		}
		
		return;
	}
	else
	{
		accu[offset+0] = a;
		accu[offset+1] = b;
		accu[offset+2] = c;
		
		if(bOutParam == true)
		{
			fprintf(paramfile, "%lf\t%lf\t%le\t%le\t%le\r\n",(double)i,(double)j,a, b, c);
		}
	}
	
}

void CSMDEMFilter::FillAccumulator_ThreeOrigins(bool bAvailable, 
								   unsigned int i, unsigned int j, 
								   double *accu, double *accu_dist,
								   double *origin,
								   double a, double b, double c,
								   int max_buf_size, bool bOutParam)
{
	int index = i*m_DEM.width+j;
	int offset = (index*3);
	
	if(offset<0) return;
	if(offset>max_buf_size) return;
	
	if(bAvailable == false)
	{
		accu[offset+0] = 0.;
		accu[offset+1] = 0.;
		accu[offset+2] = 0.;
		
		accu_dist[offset+0] = -1.;
		accu_dist[offset+1] = -1.;
		accu_dist[offset+2] = -1.;
		
		if(bOutParam == true)
		{
			fprintf(paramfile, "%lf\t%lf\t%le\t%le\t%le\r\n",(double)i,(double)j,a, b, c);
		}
		
		return;
	}
	else
	{
		accu[offset+0] = a;
		accu[offset+1] = b;
		accu[offset+2] = c;
		
		double dist1, dist2, dist3;
		double a_, b_, c_;
		
		a_ = a/(1+(a*origin[0]+b*origin[1]+c*origin[2]));
		b_ = b/(1+(a*origin[0]+b*origin[1]+c*origin[2]));
		c_ = c/(1+(a*origin[0]+b*origin[1]+c*origin[2]));
		
		dist1 = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
		
		a_ = a/(1+(a*origin[3]+b*origin[4]+c*origin[5]));
		b_ = b/(1+(a*origin[3]+b*origin[4]+c*origin[5]));
		c_ = c/(1+(a*origin[3]+b*origin[4]+c*origin[5]));
		
		dist2 = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
		
		a_ = a/(1+(a*origin[6]+b*origin[7]+c*origin[8]));
		b_ = b/(1+(a*origin[6]+b*origin[7]+c*origin[8]));
		c_ = c/(1+(a*origin[6]+b*origin[7]+c*origin[8]));
		
		dist3 = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
		
		accu_dist[offset+0] = dist1;
		accu_dist[offset+1] = dist2;
		accu_dist[offset+2] = dist3;
		
		if(bOutParam == true)
		{
			fprintf(paramfile, "%lf\t%lf\t%le\t%le\t%le\r\n",(double)i,(double)j,a, b, c);
		}
	}
	
}

void CSMDEMFilter::DistMap_OneOrigin_old(CString path, double *accu)
{
	CImage result;
	result.Create(m_DEM.width, m_DEM.height,24);
	CColorPixelPtr pixel(result);
	COLOR c;
	CColorPixel color;

	unsigned int i, j;
	for(i=0; i<m_DEM.height; i++)//Y direction
	{
		for(j=0; j<m_DEM.width; j++)//X direction
		{
			int index = i*m_DEM.width+j;
			int offset = (index*3);

			if(accu[offset+0]<0)//blunder or non-point of a plane
			{
				c.R = (BYTE)0; c.G = (BYTE)0; c.B = (BYTE)0;
			}
			else
			{				
				c.R = (BYTE)((accu[offset+0]-min_a)/range_a*255);
				c.G = (BYTE)((accu[offset+1]-min_b)/range_b*255);
				c.B = (BYTE)((accu[offset+2]-min_c)/range_c*255);
			}

			color << c;
			color >> pixel[(int)i][(int)j];
		}
	}

	result.Save(path);
}

void CSMDEMFilter::DistMap_ThreeOrigins(CString path, double *accu_dist)
{
	CImage result;
	result.Create(m_DEM.width, m_DEM.height,24);
	CColorPixelPtr pixel(result);
	COLOR c;
	CColorPixel color;

	unsigned int i, j;
	for(i=0; i<m_DEM.height; i++)//Y direction
	{
		for(j=0; j<m_DEM.width; j++)//X direction
		{
			int index = i*m_DEM.width+j;
			int offset = (index*3);

			if(accu_dist[offset+0]<0)//blunder or non-point of a plane
			{
				c.R = (BYTE)0; c.G = (BYTE)0; c.B = (BYTE)0;
			}
			else
			{				
				c.R = (BYTE)((accu_dist[offset+0]-min_dist1)/range1*Qinterval*255);
				c.G = (BYTE)((accu_dist[offset+1]-min_dist2)/range2*Qinterval*255);
				c.B = (BYTE)((accu_dist[offset+2]-min_dist3)/range3*Qinterval*255);
			}

			color << c;
			color >> pixel[(int)i][(int)j];
		}
	}

	result.Save(path);
}

void CSMDEMFilter::FrequencyMap_ThreeOrigins(CString path, double *Q_accu_dist, unsigned int min_num_points)
{
	int i, j;

	//size of accumulator
	int num1 = int((max_dist1 - min_dist1)/Qinterval + 0.5);
	int num2 = int((max_dist2 - min_dist2)/Qinterval + 0.5);

	//accumulator buffer
	//long *_2D_Buf_ = new long[num1*num2];
	long **_2D_Buf_ = new long*[num2];

	for(i=0; i<num2; i++) _2D_Buf_[i] = new long[num1];

	//initialize
	for(i=0; i<num2; i++)
	{
		for(j=0; j<num1; j++)
		{
			_2D_Buf_[i][j] = (long)0;
		}
	}

	//count

	int r, c, index, offset;
	for(i=0; i<(int)m_DEM.height; i++)
	{
		for(j=0; j<(int)m_DEM.width; j++)
		{
			index = i*m_DEM.width+j;
			offset = (index*3);

			c = (int)Q_accu_dist[offset+0];
			r = (int)Q_accu_dist[offset+1];
			
			//_2D_Buf_[r*num1+c] = _2D_Buf_[r*num1+c] + (long)1;
			_2D_Buf_[r][c] = _2D_Buf_[r][c] + (long)1;
		} 
	}

	//maximum number and txt out
	long maxnum=0;

	CFile f;
	CFileException e;
	if( !f.Open( stOutFile, CFile::modeCreate | CFile::modeWrite, &e ) )
	{
	#ifdef _DEBUG
		afxDump << "File could not be opened " << e.m_cause << "\n";
	#endif
	}

	for(i=0; i<num2; i++)
	{
		for(j=0; j<num1; j++)
		{
			//text out
			CString temp;
			temp.Format("%lf %lf %lf\r\n",(double)i,(double)j, (double)_2D_Buf_[i][j]);
			f.Write( temp, temp.GetLength() );

			//maximum number(count)
			if(_2D_Buf_[i][j]>maxnum) maxnum = _2D_Buf_[i][j];
		}
	}

	f.Close();//file close

	CImage result;
	result.Create(num1,num2,8);
	CPixelPtr pixel(result);

	for(i=0; i<(int)num2; i++)
	{
		for(j=0; j<(int)num1; j++)
		{
			long count = _2D_Buf_[i][j];
			if(count < (long)min_num_points)
			{
				pixel[i][j] = (BYTE)(255);
			}
			else
			{
				pixel[i][j] = 255 - (BYTE)((double)count/(double)maxnum*255.0+0.5);
			}
		} 
	}

	result.Save(path);

	//delete 2D array
	for(i=0; i<num2; i++)
	{
		delete[] _2D_Buf_[i];
	}
	
	delete[] _2D_Buf_;
}

void CSMDEMFilter::Taging(CImage &image)
{
	CPixelPtr pixel(image);

	LONG *sort_r = new LONG[image.GetWidth()*image.GetHeight()];
	LONG *sort_c = new LONG[image.GetWidth()*image.GetHeight()];
	BYTE *sort = new BYTE[image.GetWidth()*image.GetHeight()];
	
	//copy
	int i, j, count=0;
	for(i=0; i<image.GetHeight(); i++)
	{
		for(j=0; j<image.GetWidth(); j++)
		{
			sort[count] = (BYTE)pixel[i][j];
			sort_r[count] = i;
			sort_c[count] = j;
			count++;
		} 
	}

	//bubble sort
	for(i=0; i<image.GetHeight()*image.GetWidth()-1; i++)
	{
		count = 0;
		for(j=0; j<image.GetHeight()*image.GetWidth()-1; j++)
		{
			if(sort[j]<sort[j+1])
			{
				BYTE temp;
				temp = sort[j+1];
				sort[j+1] = sort[j];
				sort[j] = temp;

				LONG temp2;
				temp2 = sort_r[j+1];
				sort_r[j+1] = sort_r[j];
				sort_r[j] = temp2;

				temp2 = sort_c[j+1];
				sort_c[j+1] = sort_c[j];
				sort_c[j] = temp2;

				count++;
			}
		}
		if(count == 0) break;
	}
}

bool CSMDEMFilter::RobustGLSforCoeff(int index_x, int index_y, 
							  int half_size, 
							  double &a, double &b, double &c, 
							  unsigned int max_iter_num,
							  double &pos_var,
							  int method)
{
	CSMMatrix<double> Bmat, Jmat, Kmat, Lmat, Qmat;
	CSMMatrix<double> bmat, jmat, kmat, lmat, qmat;
	CSMMatrix<double> We;
	CSMMatrix<double> JT;
	CSMMatrix<double> X;
	CSMMatrix<double> Ve, V;

	unsigned int searchsize = half_size*2+1;
	int num_data = searchsize*searchsize;

	jmat.Resize(1, 3);
	lmat.Resize(1, 1);

	unsigned int r_index=0;
	
	unsigned int iteration = 0;

	//Make J, L
	Jmat.Resize(0,0);
	Lmat.Resize(0,0);
	for(int i=-half_size; i<=half_size; i++)//Y direction
	{
		for(int j=-half_size; j<=half_size; j++)//X direction
		{
			double X, Y, Z;
			
			if(false == m_DEM.GetX(j+index_x, X)) continue;
			if(false == m_DEM.GetY(i+index_y, Y)) continue;
			if(false == m_DEM.GetZ(j+index_x,i+index_y, Z)) continue;
			
			if(Z>3)
			{
				int ii=0;
			}
			
			jmat(0,0) = X;
			jmat(0,1) = Y;
			jmat(0,2) = Z;
			
			lmat(0,0) = Z;
			
			Jmat.Insert(r_index,0,jmat);
			Lmat.Insert(r_index,0,lmat);
			
			r_index++;
			
		}//end of j
	}//end of i
	
	if(Jmat.GetRows()<3) return false;

	X = (Jmat.Transpose()%Jmat).Inverse()%Jmat.Transpose()%Lmat;
	V = Jmat%X - Lmat;

	pos_var = (V.Transpose()%V)(0,0)/(Jmat.GetRows()-Jmat.GetCols());

	a=X(0,0); b=X(1,0); c=X(2,0);

	bmat.Resize(1, 3);
	kmat.Resize(1, 1);
	qmat.Resize(3, 3, 0.);
	
	bool bStop = true;
	double old_variance = -99999.0;
	int count_eq = 0;
	do
	{
		iteration ++;
		//Make B, K
		Bmat.Resize(0,0);
		Kmat.Resize(0,0);
		Qmat.Resize(0,0);

		r_index=0;
		count_eq=0;
		for(int i=-half_size; i<=half_size; i++)//Y direction
		{
			for(int j=-half_size; j<=half_size; j++)//X direction
			{
				double X, Y, Z;
				
				if(false == m_DEM.GetX(j+index_x, X)) continue;
				if(false == m_DEM.GetY(i+index_y, Y)) continue;
				if(false == m_DEM.GetZ(j+index_x,i+index_y, Z)) continue;
				
				//if(j+index_x==22)
				//{
				//	if(i+index_y == 1)
				//		continue;
				//}
				
				bmat(0,0) = a;
				bmat(0,1) = b;
				bmat(0,2) = c;

				
				kmat(0,0) = -a*X -b*Y -c*Z -1;
				
				if(iteration > 1)
				{

					switch(method)
					{
					
					case 1://L1
						{
							double res1, res2, res3;
							res1 = fabs(V(r_index*3+0, 0));
							res2 = fabs(V(r_index*3+1, 0));
							res3 = fabs(V(r_index*3+2, 0));

							qmat(0,0) = res1;
							qmat(1,1) = res2;
							qmat(2,2) = res3;
																					
							if(res1 > sqrt(old_variance)) qmat(0,0) = 1.0e10; else qmat(0,0) = res1;
							if(res2 > sqrt(old_variance)) qmat(1,1) = 1.0e10; else qmat(1,1) = res2;
							if(res3 > sqrt(old_variance)) qmat(2,2) = 1.0e10; else qmat(2,2) = res3;

							if((res1 < sqrt(old_variance))&&(res2 < sqrt(old_variance))&&(res3 < sqrt(old_variance)))
								count_eq++;
							
						}
						break;
					case 2://Huber's
						{
							double k=1.345;
							
							double res1 = fabs(V(r_index*3+0, 0));
							double res2 = fabs(V(r_index*3+1, 0));
							double res3 = fabs(V(r_index*3+2, 0));

							if(res1 <= k) qmat(0,0) = 1.0; else qmat(0,0) = res1/k;
							if(res2 <= k) qmat(1,1) = 1.0; else qmat(1,1) = res2/k;
							if(res3 <= k) qmat(2,2) = 1.0; else qmat(2,2) = res3/k;
							
							//else w = k/fabs(V(i,0));
							//else w = 0;//?   가오노트에 이렇게 하라고 되있?
						}
						break;
					case 3://Andrew's(c=1.5)
						{
							double c=1.5;
							double pi = 3.141592654;

							double res1 = fabs(V(r_index*3+0, 0));
							double res2 = fabs(V(r_index*3+1, 0));
							double res3 = fabs(V(r_index*3+2, 0));
							
							if(res1 <= c*pi/2) 
								qmat(0,0) = 1.0;
							else if((res1>c*pi/2) && (res1<=c*pi)) 
								qmat(0,0) = 1./sin(c/res1);
							else 
								qmat(0,0) = 1.0e99;

							if(res2 <= c*pi/2) 
								qmat(1,1) = 1.0;
							else if((res2>c*pi/2)&&(res2<=c*pi)) 
								qmat(1,1) = 1./sin(c/res2);
							else 
								qmat(1,1) = 1.0e99;

							if(res3 <= c*pi/2) 
								qmat(2,2) = 1.0;
							else if((res3 > c*pi/2)&&(res3<=c*pi)) 
								qmat(2,2)  = 1./sin(c/res3);
							else 
								qmat(2,2) = 1.0e99;
							
						}
						break;
					case 4://Andrew's(c=1.8)
						{
							double c=1.8;
							double pi = 3.141592654;
							
							double res1 = fabs(V(r_index*3+0, 0));
							double res2 = fabs(V(r_index*3+1, 0));
							double res3 = fabs(V(r_index*3+2, 0));
							
							if(res1 <= c*pi/2) qmat(0,0) = 1.0;
							else if((res1 > c*pi/2)&&(res1<=c*pi)) qmat(0,0) = sin(c/res1);
							else qmat(0,0) = 1.0e99;
							
							if(res2 <= c*pi/2) qmat(1,1) = 1.0;
							else if((res2 > c*pi/2)&&(res2<=c*pi)) qmat(1,1) = sin(c/res2);
							else qmat(1,1) = 1.0e99;
							
							if(res3 <= c*pi/2) qmat(2,2) = 1.0;
							else if((res3 > c*pi/2)&&(res3<=c*pi)) qmat(2,2) = sin(c/res3);
							else qmat(2,2) = 1.0e99;
							
						}
						break;
					default:
						{
						}
						break;
					}

				}
				else
				{
					qmat(0,0) = 1.;
					qmat(1,1) = 1.;
					qmat(2,2) = 1.;
				}

				Bmat.Insert(r_index,r_index*3,bmat);
				Kmat.Insert(r_index,0,kmat);
				Qmat.Insert(r_index*3, r_index*3, qmat);
				
				r_index++;
				
			}//end of j
		}//end of i
				
		////////////////
		//

		//Available point number
		if(iteration>1&&(count_eq < 4)) return false;
		
		We = Bmat%Qmat%Bmat.Transpose();
		We = We.Inverse();
		
		JT = Jmat.Transpose();
		X = (JT%We%Jmat).Inverse()%JT%We%Kmat;

		a = a + X(0,0);
		b = b + X(1,0);
		c = c + X(2,0);
		
		Ve = Jmat%X - Kmat;
		V = Bmat.Transpose()%We%Ve;
		//pos_var = (V.Transpose()%Qmat%V)(0,0)/(Jmat.GetRows()-Jmat.GetCols());
		pos_var = (Ve.Transpose()%We%Ve)(0,0)/(Jmat.GetRows()-Jmat.GetCols());//(Same result)

		if(old_variance == -99999.0)
		{
			old_variance = fabs(pos_var);
		}
		else
		{
			if((fabs(pos_var)-fabs(old_variance))<limit_var) break;
			else old_variance = pos_var;
		}
		
		//if(fabs(pos_var) < limit_var) bStop = false;
		if(iteration > max_iter_num) bStop = false;
		
	} while(bStop);//end of do-while
/*
	double dist1;
	double a_, b_, c_;

	double x, y, z;
	m_DEM.GetX(index_x,x); m_DEM.GetY(index_y,y); m_DEM.GetZ(index_x,index_y,z);
	
	a_ = a/(1+(a*x + b*y + c*z));
	b_ = b/(1+(a*x + b*y + c*z));
	c_ = c/(1+(a*x + b*y + c*z));
	
	dist1 = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
	
	if(fabs(dist1)>max_dist) return false;
	else return true;
*/
	return true;
}

bool CSMDEMFilter::RobustLSforCoeff(int index_x, int index_y, 
							  int half_size, 
							  double &a, double &b, double &c, 
							  unsigned int max_iter_num,
							  double &pos_var,
							  int method)
{
	CSMMatrix<double> Amat, Lmat;
	CSMMatrix<double> amat, lmat;
	CSMMatrix<double> Xmat;
	CSMMatrix<double> V;

	unsigned int searchsize = half_size*2+1;
	int num_data = searchsize*searchsize;

	amat.Resize(1, 3);
	lmat.Resize(1, 1);

	unsigned int r_index=0;
	
	unsigned int iteration = 0;

	//Make J, L
	Amat.Resize(0,0);
	Lmat.Resize(0,0);
	for(int i=-half_size; i<=half_size; i++)//Y direction
	{
		for(int j=-half_size; j<=half_size; j++)//X direction
		{
			double X, Y, Z;
			
			if(false == m_DEM.GetX(j+index_x, X)) continue;
			if(false == m_DEM.GetY(i+index_y, Y)) continue;
			if(false == m_DEM.GetZ(j+index_x,i+index_y, Z)) continue;
			//aX + bY + c = Z
			amat(0,0) = X;
			amat(0,1) = Y;
			amat(0,2) = 1;

			lmat(0,0) = Z;
			
			Amat.Insert(r_index,0,amat);
			Lmat.Insert(r_index,0,lmat);
			
			r_index++;
			
		}//end of j
	}//end of i
	
	if(Amat.GetRows()<3) return false;

	CSMMatrix<double> AT = Amat.Transpose();
	Xmat = (AT%Amat).Inverse()%AT%Lmat;
	V = Amat%Xmat - Lmat;

	a=Xmat(0,0); b=Xmat(1,0); c=Xmat(2,0);

	pos_var = (V.Transpose()%V)(0,0)/(Amat.GetRows()-Amat.GetCols());

	double old_a = a;
	double old_b = b;
	double old_c = c;
	CSMMatrix<double> Wmat;
	Wmat.Resize(r_index,r_index);

	double old_variance = -99999.0;
	do
	{	
		int count_eq = 0;
		for(int i=0; i<(int)r_index; i++)
		{
			
			switch(method)
			{
			case 1://L1
				{
					if(fabs(V(i, 0))>fabs(5.0*sqrt(fabs(old_variance)))) Wmat(i, i) = 1.0e-99;
					else {Wmat(i, i) = 1./fabs(V(i, 0)); count_eq ++;}
					//else {Wmat(i, i) = 1.0; count_eq ++;}
					//Wmat(i, i) = 1.;
				}
				break;
			case 2://Huber's
				{
					double k=1.345;
					
					Wmat(i, i) = fabs(V(i, 0))/k;
				}
				break;
			case 3://Andrew's(c=1.5)
				{
					double c=1.5;
					double pi = 3.141592654;

					double res = fabs(V(i, 0));

					Wmat(i, i) = 1./fabs(V(i, 0));
					
					if(res <= c*pi/2) 
						Wmat(i, i) = 1.0;
					else if((res>c*pi/2) && (res<=c*pi)) 
						Wmat(i, i) = sin(c/res);
					else 
						Wmat(i, i) = 1.0e-99;				
				}
				break;
			case 4://Andrew's(c=1.8)
				{
					double c=1.8;
					double pi = 3.141592654;
					
					double res = fabs(V(i, 0));

					Wmat(i, i) = 1./fabs(V(i, 0));
					
					if(res <= c*pi/2) 
						Wmat(i, i) = 1.0;
					else if((res>c*pi/2) && (res<=c*pi)) 
						Wmat(i, i) = sin(c/res);
					else 
						Wmat(i, i) = 1.0e-99;		
					
				}
				break;
			default:
				{
					Wmat(i, i) = 1./fabs(V(i, 0));
				}
				break;
			}
			
		}//end of i
		
		//Available point number
		if(count_eq < 4) return false;

		CSMMatrix<double> AT = Amat.Transpose();
		Xmat = (AT%Wmat%Amat).Inverse()%AT%Wmat%Lmat;
		V = Amat%Xmat - Lmat;
	
		double pos_var = (V.Transpose()%Wmat%V)(0,0)/(Amat.GetRows()-Amat.GetCols());
		
		a=Xmat(0,0); b=Xmat(1,0); c=Xmat(2,0);

		/*
		if(fabs(a-old_a)<1.0e-10) 
		{
			if(fabs(b-old_b)<1.0e-10)
			{
				if(fabs(b-old_b)<1.0e-10)
				{
					break;
				}
			}
		}
		*/

		if(old_variance == -99999.0)
		{
			old_variance = fabs(pos_var);
		}
		else
		{
			if((fabs(pos_var)-fabs(old_variance))<limit_var) break;
			else old_variance = pos_var;
		}

		old_a = a;
		old_b = b;
		old_c = c;
		
	} while(1);//end of do-while

	//aX+bY+cZ+1=0
	c = -1/c;
	a = -a*c;
	b = -b*c;

/*
	double dist1;
	double a_, b_, c_;

	double x, y, z;
	m_DEM.GetX(index_x,x); m_DEM.GetY(index_y,y); m_DEM.GetZ(index_x,index_y,z);

	double e = a*x + b*y + c - z;
		
	e = 1+a*x + b*y + c*z;
		
	a_ = a/(1+(a*x + b*y + c*z));
	b_ = b/(1+(a*x + b*y + c*z));
	c_ = c/(1+(a*x + b*y + c*z));
	
	dist1 = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
*/
	
	//if(fabs(dist1)>max_dist) return false;
	//else return true;

	return true;
}

bool CSMDEMFilter::RobustLSforCoeff_old(int index_x, int index_y, 
							  int half_size, 
							  double &a, double &b, double &c, 
							  unsigned int max_iter_num,
							  double max_dist,
							  double &pos_var,
							  int method)
{
	CSMMatrix<double> Amat, Bmat, Lmat, Jmat, Kmat;
	CSMMatrix<double> amat, bmat, lmat, jmat, kmat;
	CSMMatrix<double> Qmat;
	CSMMatrix<double> Xmat;
	CSMMatrix<double> V;

	unsigned int searchsize = half_size*2+1;
	int num_data = searchsize*searchsize;

	amat.Resize(1, 3);
	jmat.Resize(1, 3);
	lmat.Resize(1, 1);

	if(index_x == 22)
		if(index_y == 1)
			int ii=0;

	unsigned int r_index=0;
	
	unsigned int iteration = 0;

	//Make J, L
	Amat.Resize(0,0);
	Lmat.Resize(0,0);
	Jmat.Resize(0,0);
	for(int i=-half_size; i<=half_size; i++)//Y direction
	{
		for(int j=-half_size; j<=half_size; j++)//X direction
		{
			double X, Y, Z;
			
			if(false == m_DEM.GetX(j+index_x, X)) continue;
			if(false == m_DEM.GetY(i+index_y, Y)) continue;
			if(false == m_DEM.GetZ(j+index_x,i+index_y, Z)) continue;
			//aX + bY + c = Z
			amat(0,0) = X;
			amat(0,1) = Y;
			amat(0,2) = 1;

			jmat(0,0) = X;
			jmat(0,1) = Y;
			jmat(0,2) = Z;
			
			lmat(0,0) = Z;
			
			Amat.Insert(r_index,0,amat);
			Lmat.Insert(r_index,0,lmat);
			Jmat.Insert(r_index,0,jmat);
			
			r_index++;
			
		}//end of j
	}//end of i
	
	if(Amat.GetRows()<3) return false;

	CSMMatrix<double> AT = Amat.Transpose();
	Xmat = (AT%Amat).Inverse()%AT%Lmat;
	V = Amat%Xmat - Lmat;

	a=Xmat(0,0); b=Xmat(1,0); c=Xmat(2,0);

	pos_var = (V.Transpose()%V)(0,0)/(Amat.GetRows()-Amat.GetCols());

	double old_a = a;
	double old_b = b;
	double old_c = c;
	CSMMatrix<double> Wmat;
	Wmat.Resize(r_index,r_index);
	do
	{	for(int i=0; i<(int)r_index; i++)
		{
			
			Wmat(i, i) = 1./fabs(V(i, 0));			
			
		}//end of i
		
		CSMMatrix<double> AT = Amat.Transpose();
		Xmat = (AT%Wmat%Amat).Inverse()%AT%Wmat%Lmat;
		V = Amat%Xmat - Lmat;
	
		double pos_var = (V.Transpose()%Wmat%V)(0,0)/(Amat.GetRows()-Amat.GetCols());
		
		a=Xmat(0,0); b=Xmat(1,0); c=Xmat(2,0);

		if(fabs(a-old_a)<1.0e-10) 
		{
			if(fabs(b-old_b)<1.0e-10)
			{
				if(fabs(b-old_b)<1.0e-10)
				{
					break;
				}
			}
		}

		old_a = a;
		old_b = b;
		old_c = c;
		
	} while(1);//end of do-while


	a=Xmat(0,0); b=Xmat(1,0); c=Xmat(2,0);

	//aX+bY+cZ+1=0
	c = -1/c;
	a = -a*c;
	b = -b*c;
		
	bmat.Resize(1, 3);
	kmat.Resize(1, 1);
	
	bool bStop = true;
	//double old_variance;
	double old_maxX;
	do
	{
		iteration ++;
		//Make B, K
		Bmat.Resize(0,0);
		Kmat.Resize(0,0);
		
		r_index=0;
		for(int i=-half_size; i<=half_size; i++)//Y direction
		{
			for(int j=-half_size; j<=half_size; j++)//X direction
			{
				double X, Y, Z;
				
				if(false == m_DEM.GetX(j+index_x, X)) continue;
				if(false == m_DEM.GetY(i+index_y, Y)) continue;
				if(false == m_DEM.GetZ(j+index_x,i+index_y, Z)) continue;
				
				bmat(0,0) = a;
				bmat(0,1) = b;
				bmat(0,2) = c;
				
				kmat(0,0) = -a*X -b*Y -c*Z -1;

				Bmat.Insert(r_index,r_index*3,bmat);
				Kmat.Insert(r_index,0,kmat);

				r_index++;
			}
		}
		
		Qmat.Resize(r_index*3,r_index*3);
		
		for(int i=0; i<(int)r_index; i++)
		{
			
			if(iteration > 1)
			{
				double res1 = fabs(V(i*3+0,0));
				double res2 = fabs(V(i*3+1,0));
				double res3 = fabs(V(i*3+2,0));
				
				switch(method)
				{
				case 1://L1
					{
						//if(res1 > max_dist*3) qmat(0,0) = 1.0e10; else qmat(0,0) = res1;
						//if(res2 > max_dist*3) qmat(1,1) = 1.0e10; else qmat(1,1) = res2;
						//if(res3 > max_dist*3) qmat(2,2) = 1.0e10; else qmat(2,2) = res3;
												
						Qmat(i*3+0, i*3+0) = res1;
						Qmat(i*3+1, i*3+1) = res2;
						Qmat(i*3+2, i*3+2) = res3;
					}
					break;
				case 2://Huber's
					{
						double k=1.345;
						
						//if(res1 <= k) wmat(0,0) = 1.0; else wmat(0,0) = k/res1;
						//if(res2 <= k) wmat(1,1) = 1.0; else wmat(1,1) = k/res2;
						//if(res3 <= k) wmat(2,2) = 1.0; else wmat(2,2) = k/res3;
						
						//else w = k/fabs(V(i,0));
						//else w = 0;//?   가오노트에 이렇게 하라고 되있?
						
						Qmat(i*3+0, i*3+0) = res1/k;
						Qmat(i*3+1, i*3+1) = res2/k;
						Qmat(i*3+2, i*3+2) = res3/k;
					}
					break;
				case 3://Andrew's(c=1.5)
					{
						double c=1.5;
						double pi = 3.141592654;
						
						if(res1 <= c*pi/2) 
							Qmat(i*3+0,i*3+0) = 1.0;
						else if((res1>c*pi/2) && (res1<=c*pi)) 
							Qmat(i*3+0,i*3+0) = 1./sin(c/res1);
						else 
							Qmat(i*3+0,i*3+0) = 1.0e99;							

						if(res2 <= c*pi/2) 
							Qmat(i*3+1,i*3+1) = 1.0;
						else if((res2>c*pi/2) && (res2<=c*pi)) 
							Qmat(i*3+1,i*3+1) = 1./sin(c/res2);
						else 
							Qmat(i*3+1,i*3+1) = 1.0e99;							

						if(res3 <= c*pi/2) 
							Qmat(i*3+2,i*3+2) = 1.0;
						else if((res3>c*pi/2) && (res3<=c*pi)) 
							Qmat(i*3+2,i*3+2) = 1./sin(c/res3);
						else 
							Qmat(i*3+2,i*3+2) = 1.0e99;					
					}
					break;
				case 4://Andrew's(c=1.8)
					{
						double c=1.8;
						double pi = 3.141592654;
						
						if(res1 <= c*pi/2) 
							Qmat(i*3+0,i*3+0) = 1.0;
						else if((res1>c*pi/2) && (res1<=c*pi)) 
							Qmat(i*3+0,i*3+0) = 1./sin(c/res1);
						else 
							Qmat(i*3+0,i*3+0) = 1.0e99;							

						if(res2 <= c*pi/2) 
							Qmat(i*3+1,i*3+1) = 1.0;
						else if((res2>c*pi/2) && (res2<=c*pi)) 
							Qmat(i*3+1,i*3+1) = 1./sin(c/res2);
						else 
							Qmat(i*3+1,i*3+1) = 1.0e99;							

						if(res3 <= c*pi/2) 
							Qmat(i*3+2,i*3+2) = 1.0;
						else if((res3>c*pi/2) && (res3<=c*pi)) 
							Qmat(i*3+2,i*3+2) = 1./sin(c/res3);
						else 
							Qmat(i*3+2,i*3+2) = 1.0e99;			
						
					}
					break;
				default:
					{
						Qmat(i*3+0, i*3+0) = res1;
						Qmat(i*3+1, i*3+1) = res2;
						Qmat(i*3+2, i*3+2) = res3;
					}
					break;
				}
				
			}
			else
			{
				Qmat(i*3+0, i*3+0) = 1.;
				Qmat(i*3+1, i*3+1) = 1.;
				Qmat(i*3+2, i*3+2) = 1.;
			}
		}

		CSMMatrix<double> We = Bmat%Qmat%Bmat.Transpose();
		We = We.Inverse();
		
		CSMMatrix<double> JT = Jmat.Transpose();
		Xmat = (JT%We%Jmat).Inverse()%JT%We%Kmat;

		a = a + Xmat(0,0);
		b = b + Xmat(1,0);
		c = c + Xmat(2,0);

		CSMMatrix<double> Ve = Kmat - Jmat%Xmat;
		V = Bmat.Transpose()%We%Ve;
		pos_var = (Ve.Transpose()%We%Ve)(0,0)/(Jmat.GetRows()-Jmat.GetCols());
		
		double maxX;
		if(fabs(Xmat(0,0))>fabs(Xmat(1,0))) maxX = fabs(Xmat(0,0));
		else
			maxX = fabs(Xmat(1,0));
		if(maxX<fabs(Xmat(2,0))) maxX = fabs(Xmat(2,0));

		if(iteration>1)
			if(fabs(maxX-old_maxX)<0.000001) bStop = false;
			else old_maxX = maxX;
		else
			old_maxX = maxX;
		
		//if(iteration == 1)
		//{
		//	old_variance = fabs(pos_var);
		//}
		//else
		//{
			//if(fabs(pos_var)>fabs(old_variance)) bStop = false;
			//else old_variance = fabs(pos_var);
		//}
		
		if(fabs(pos_var) < limit_var) bStop = false;
		if(iteration > max_iter_num) bStop = false;
		
	} while(bStop);//end of do-while

/*
	double dist1;
	double a_, b_, c_;

	double x, y, z;
	m_DEM.GetX(index_x,x); m_DEM.GetY(index_y,y); m_DEM.GetZ(index_x,index_y,z);

	double e = a*x + b*y + c - z;
		
	e = 1+a*x + b*y + c*z;
		
	a_ = a/(1+(a*x + b*y + c*z));
	b_ = b/(1+(a*x + b*y + c*z));
	c_ = c/(1+(a*x + b*y + c*z));
	
	dist1 = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
*/
	
	//if(fabs(dist1)>max_dist) return false;
	//else return true;

	return true;
}

bool CSMDEMFilter::GLSforCoeff(int index_x, int index_y, 
							  int half_size, 
							  double &a, double &b, double &c, 
							  unsigned int max_iter_num,
							  double &pos_var)
{
	CSMMatrix<double> Bmat, Jmat, Kmat, Lmat;
	CSMMatrix<double> bmat, jmat, kmat, lmat;
	CSMMatrix<double> We;
	CSMMatrix<double> JT;
	CSMMatrix<double> X;
	CSMMatrix<double> Ve, V;

	unsigned int searchsize = half_size*2+1;
	int num_data = searchsize*searchsize;

	jmat.Resize(1, 3);
	lmat.Resize(1, 1);

	unsigned int r_index=0;
	
	unsigned int iteration = 0;
	
	//Make J, L
	Jmat.Resize(0,0);
	Lmat.Resize(0,0);
	for(int i=-half_size; i<=half_size; i++)//Y direction
	{
		for(int j=-half_size; j<=half_size; j++)//X direction
		{
			double X, Y, Z;
			
			if(false == m_DEM.GetX(j+index_x, X)) continue;
			if(false == m_DEM.GetY(i+index_y, Y)) continue;
			if(false == m_DEM.GetZ(j+index_x,i+index_y, Z)) continue;
			
			jmat(0,0) = X;
			jmat(0,1) = Y;
			jmat(0,2) = Z;
			
			lmat(0,0) = -1;
			
			Jmat.Insert(r_index,0,jmat);
			Lmat.Insert(r_index,0,lmat);
			
			r_index++;
			
		}//end of j
	}//end of i
	
	if(Jmat.GetRows()<3) return false;

	X = (Jmat.Transpose()%Jmat).Inverse()%Jmat.Transpose()%Lmat;
	V = Jmat%X - Lmat;
	pos_var = (V.Transpose()%V)(0,0)/(Jmat.GetRows()-Jmat.GetCols());

	a=X(0,0); b=X(1,0); c=X(2,0);

	bmat.Resize(1, 3);
	kmat.Resize(1, 1);
	
	double old_variance = 0;
	bool bStop = true;
	do
	{
		iteration ++;
		//Make B, K
		Bmat.Resize(0,0);
		Kmat.Resize(0,0);
		r_index=0;
		for(int i=-half_size; i<=half_size; i++)//Y direction
		{
			for(int j=-half_size; j<=half_size; j++)//X direction
			{
				double X, Y, Z;
				
				if(false == m_DEM.GetX(j+index_x, X)) continue;
				if(false == m_DEM.GetY(i+index_y, Y)) continue;
				if(false == m_DEM.GetZ(j+index_x,i+index_y, Z)) continue;
				
				bmat(0,0) = a;
				bmat(0,1) = b;
				bmat(0,2) = c;
				
				kmat(0,0) = -a*X -b*Y -c*Z -1;
				
				Bmat.Insert(r_index,r_index*3,bmat);
				Kmat.Insert(r_index,0,kmat);
				
				r_index++;
				
			}//end of j
		}//end of i
		
		We = Bmat%Bmat.Transpose();
		We = We.Inverse();
		
		JT = Jmat.Transpose();
		X = (JT%We%Jmat).Inverse()%JT%We%Kmat;
		
		a = a + X(0,0);
		b = b + X(1,0);
		c = c + X(2,0);
		
		Ve = Jmat%X - Kmat;
		V = Bmat.Transpose()%We%Ve;
		//pos_var = (V.Transpose()%V)(0,0)/(Jmat.GetRows()-Jmat.GetCols());
		pos_var = (Ve.Transpose()%We%Ve)(0,0)/(Jmat.GetRows()-Jmat.GetCols());//(same value)

		if(iteration == 1)
		{
			old_variance = fabs(pos_var);
		}
		else
		{
			if((fabs(pos_var)-fabs(old_variance))<limit_var) bStop = false;
			else old_variance = pos_var;
		}
		
		//if(fabs(pos_var) < limit_var) bStop = false;
		if(iteration > max_iter_num) bStop = false;
		
	} while(bStop);//end of do-while
/*
	double dist1;
	double a_, b_, c_;
	
	double x, y, z;
	m_DEM.GetX(index_x,x); m_DEM.GetY(index_y,y); m_DEM.GetZ(index_x,index_y,z);
	
	a_ = a/(1+(a*x + b*y + c*z));
	b_ = b/(1+(a*x + b*y + c*z));
	c_ = c/(1+(a*x + b*y + c*z));
	
	dist1 = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
	
	double e = a*x + b*y + c*z+1;

	if(dist1>sqrt(max_dist)) return false;
	else return true;
*/
	return true;
}

bool CSMDEMFilter::GLSforCoeff(fstream &debugfile, int index_x, int index_y, 
							  int half_size, 
							  double &a, double &b, double &c, 
							  unsigned int max_iter_num,
							  double &pos_var)
{
	CSMMatrix<double> Bmat, Jmat, Kmat, Lmat;
	CSMMatrix<double> bmat, jmat, kmat, lmat;
	CSMMatrix<double> We;
	CSMMatrix<double> JT;
	CSMMatrix<double> X;
	CSMMatrix<double> Ve, V;

	unsigned int searchsize = half_size*2+1;
	int num_data = searchsize*searchsize;

	jmat.Resize(1, 3);
	lmat.Resize(1, 1);

	unsigned int r_index=0;
	
	unsigned int iteration = 0;
	
	//Make J, L
	Jmat.Resize(0,0);
	Lmat.Resize(0,0);
	for(int i=-half_size; i<=half_size; i++)//Y direction
	{
		for(int j=-half_size; j<=half_size; j++)//X direction
		{
			double X, Y, Z;
			
			if(false == m_DEM.GetX(j+index_x, X)) continue;
			if(false == m_DEM.GetY(i+index_y, Y)) continue;
			if(false == m_DEM.GetZ(j+index_x,i+index_y, Z)) continue;
			
			jmat(0,0) = X;
			jmat(0,1) = Y;
			jmat(0,2) = Z;
			
			lmat(0,0) = -1;
			
			Jmat.Insert(r_index,0,jmat);
			Lmat.Insert(r_index,0,lmat);
			
			r_index++;
			
		}//end of j
	}//end of i
	
	if(Jmat.GetRows()<3) return false;

	X = (Jmat.Transpose()%Jmat).Inverse()%Jmat.Transpose()%Lmat;
	V = Jmat%X - Lmat;
	pos_var = (V.Transpose()%V)(0,0)/(Jmat.GetRows()-Jmat.GetCols());

	a=X(0,0); b=X(1,0); c=X(2,0);

	//Defug File
	debugfile<<"[Initial]\t"<<index_x<<"\t"<<index_y<<"\t"<<a<<"\t"<<b<<"\t"<<c<<"\t"<<endl;
	debugfile.flush();

	bmat.Resize(1, 3);
	kmat.Resize(1, 1);
	
	double old_variance = 0;
	bool bStop = true;
	do
	{
		iteration ++;
		//Make B, K
		Bmat.Resize(0,0);
		Kmat.Resize(0,0);
		r_index=0;
		for(int i=-half_size; i<=half_size; i++)//Y direction
		{
			for(int j=-half_size; j<=half_size; j++)//X direction
			{
				double X, Y, Z;
				
				if(false == m_DEM.GetX(j+index_x, X)) continue;
				if(false == m_DEM.GetY(i+index_y, Y)) continue;
				if(false == m_DEM.GetZ(j+index_x,i+index_y, Z)) continue;
				
				bmat(0,0) = a;
				bmat(0,1) = b;
				bmat(0,2) = c;
				
				kmat(0,0) = -a*X -b*Y -c*Z -1;
				
				Bmat.Insert(r_index,r_index*3,bmat);
				Kmat.Insert(r_index,0,kmat);
				
				r_index++;
				
			}//end of j
		}//end of i
		
		We = Bmat%Bmat.Transpose();
		We = We.Inverse();
		
		JT = Jmat.Transpose();
		X = (JT%We%Jmat).Inverse()%JT%We%Kmat;
		
		a = a + X(0,0);
		b = b + X(1,0);
		c = c + X(2,0);
		
		Ve = Jmat%X - Kmat;
		V = Bmat.Transpose()%We%Ve;
		//pos_var = (V.Transpose()%V)(0,0)/(Jmat.GetRows()-Jmat.GetCols());
		pos_var = (Ve.Transpose()%We%Ve)(0,0)/(Jmat.GetRows()-Jmat.GetCols());//(same value)

		if(iteration == 1)
		{
			old_variance = fabs(pos_var);
		}
		else
		{
			if((fabs(pos_var)-fabs(old_variance))<limit_var) bStop = false;
			else old_variance = pos_var;
		}
		
		//if(fabs(pos_var) < limit_var) bStop = false;
		if(iteration > max_iter_num) bStop = false;
		
	} while(bStop);//end of do-while

	//Defug File
	debugfile<<"[Final]\t"<<index_x<<"\t"<<index_y<<"\t"<<a<<"\t"<<b<<"\t"<<c<<"\t"<<iteration<<"\t"<<pos_var<<endl;
	debugfile.flush();
/*
	double dist1;
	double a_, b_, c_;
	
	double x, y, z;
	m_DEM.GetX(index_x,x); m_DEM.GetY(index_y,y); m_DEM.GetZ(index_x,index_y,z);
	
	a_ = a/(1+(a*x + b*y + c*z));
	b_ = b/(1+(a*x + b*y + c*z));
	c_ = c/(1+(a*x + b*y + c*z));
	
	dist1 = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
	
	double e = a*x + b*y + c*z+1;

	if(dist1>sqrt(max_dist)) return false;
	else return true;
*/
	return true;
}