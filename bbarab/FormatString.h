#pragma once

#include <string>

#pragma warning(disable: 4996)

static std::string Format(const char* format, ...)
{
	if (format == 0)
		return std::string("");

	int len;
	char* MaxBuf;
	for (int i = 5; ; i++)
	{
		len = (int)pow(2.0, i);
		MaxBuf = new char[len];
		if (!MaxBuf)
			return std::string("");
		// some UNIX's do not support vsnprintf and snprintf
		len = _vsnprintf(MaxBuf, len, format, (char*)(&format + 1));
		if (len > 0) break;
		delete[]MaxBuf;
		if (len == 0)
			return std::string("");
	}

	return std::string(MaxBuf);
}