// SMLiDARSim.cpp: implementation of the CLIDARBiasCalibration class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SMLiDARSim.h"
#include "SMMatrix.h"
//#include <iomanip.h>
#include "../bbarab/Image_Util/BasicImage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "../bbarab/UtilityGrocery.h"
using namespace SMATICS_BBARAB;

////////////////////////////////////////////////////
//USING NAME-SPACE
//
using namespace ISSM_BBARAB;
//
//

CBBARABLiDARSim::CBBARABLiDARSim()
{
	
}

CBBARABLiDARSim::~CBBARABLiDARSim()
{
	
}

bool CBBARABLiDARSim::MakeLiDARSimData(const char* fname)
{
	fstream debugfile;//debug file
	debugfile.precision(10);
	debugfile.setf(ios::fixed, ios::floatfield);
				
	int DEBUG_TAG = config.GetDEBUG_TAG();
	
	////////////////////////////////////////////////////////////////
	//
	if((DEBUG_TAG==1)||(DEBUG_TAG==2)) {
		CSMStr debugname = fname;
		debugname += ".TMP";
		
		debugfile.open(debugname.GetChar(),ios::out);
		debugfile<<"Debug File..."<<endl<<endl;
		debugfile.flush();			
	}
	//
	////////////////////////////////////////////////////////////////
	
	int i, j;
	//
	//Footprint image
	//(flat terrain)
	//
	CBMPImage footprint_fixed_Z;//footprint image projected on fixed z(maximum Z)
	CBMPImage footprint_real_terrain;//footprint image projected on real terrain
	footprint_fixed_Z.Create((int)m_RefDEM.width,(int)m_RefDEM.height,8);
	footprint_real_terrain.Create((int)m_RefDEM.width,(int)m_RefDEM.height,8);
	CBMPPixelPtr footprintPixel_fixed_Z(footprint_fixed_Z);//footprint image projected on fixed z(maximum Z)
	CBMPPixelPtr footprintPixel_real_terrain(footprint_real_terrain);//footprint image projected on real terrain
	for(i=0; i<(int)m_RefDEM.width; i++)
	{
		for(j=0; j<(int)m_RefDEM.height; j++)
		{
			footprintPixel_fixed_Z[j][i] = (BYTE)255;
			footprintPixel_real_terrain[j][i] = (BYTE)255;
		}
	}
	
	//
	//Footprint image with error
	//(referenced real terrain)
	//
	CBMPImage footprint_error_real_terrain;
	footprint_error_real_terrain.Create((int)m_RefDEM.width,(int)m_RefDEM.height,8);
	CBMPPixelPtr footprintPixelError_real_terrain(footprint_error_real_terrain);
	if(bErrorModeling==true)
	{
		for(i=0; i<(int)m_RefDEM.width; i++)
		{
			for(j=0; j<(int)m_RefDEM.height; j++)
			{
				footprintPixelError_real_terrain[j][i] = (BYTE)255;
			}
		}
	}
	
	//LiDAR data file
	CString temp_fname = fname;
	temp_fname.MakeLower();
	temp_fname.Replace(".txt", ".tpt");

	fstream LidarFile;//Lidar point clouds without error on real terrain
	LidarFile.open(temp_fname,ios::out);
	
	fstream LidarFile_Raw; //Raw Lidar data
	fstream LidarFile_Raw_Error; //Raw Lidar data with error
	if(DEBUG_TAG==3)
	{
		CSMStr fname_Raw = fname;
		fname_Raw += ".raw";
		LidarFile_Raw.open(fname_Raw.GetChar(),ios::out);
		
		LidarFile_Raw<<"#GPS_X(M)\t"<<"GPS_Y(M)\t"<<"GPS_Z(M)\t";
		LidarFile_Raw<<"INS_O(deg)\t"<<"INS_P(deg)\t"<<"INS_K(deg)\t";
		LidarFile_Raw<<"alpha(deg)\t"<<"beta(deg)\t";
		LidarFile_Raw<<"dist(M)\t"<<"time(sec)"<<endl;
		LidarFile_Raw<<"###################################################################";
		LidarFile_Raw<<"###################################################################"<<endl;
		LidarFile_Raw.flush();
		
		if(bErrorModeling == true)
		{
			CSMStr fname_Raw_Error = fname;
			fname_Raw_Error += "_Error.raw";
			LidarFile_Raw_Error.open(fname_Raw_Error.GetChar(),ios::out);
			
			LidarFile_Raw_Error<<"#GPS_X(M)\t"<<"GPS_Y(M)\t"<<"GPS_Z(M)\t";
			LidarFile_Raw_Error<<"INS_O(deg)\t"<<"INS_P(deg)\t"<<"INS_K(deg)\t";
			LidarFile_Raw_Error<<"alpha(deg)\t"<<"beta(deg)\t";
			LidarFile_Raw_Error<<"dist(M)\t"<<"time(sec)"<<endl;
			LidarFile_Raw_Error<<"###################################################################";
			LidarFile_Raw_Error<<"###################################################################"<<endl;
			LidarFile_Raw_Error.flush();
		}
	}
	
	fstream LidarFile_Error;//Lidar point clouds with error on real terrain
	fstream LidarFile_Diff;//Differences between without error and with error
	if(bErrorModeling==true)
	{
		CSMStr fname_error = fname;
		fname_error += "_Error.tpt";
		LidarFile_Error.open(fname_error.GetChar(),ios::out);
		
		CSMStr fname_diff = fname;
		fname_diff += "_Diff.txt";
		LidarFile_Diff.open(fname_diff.GetChar(),ios::out);
	}
	
	////////////////////////////////////////////////////////////////
	//
	if((DEBUG_TAG==1)||(DEBUG_TAG==2)) 
	{
		debugfile<<"LiDAR data file:"<<fname<<endl;
		debugfile<<"Zmax and Zmin:"<<config.GetMax_Zval()<<"\t"<<config.GetMin_Zval()<<endl;
		debugfile.flush();
	}
	//
	////////////////////////////////////////////////////////////////
	
	double st = config.GetStartTime();
	double et = config.GetEndTime();
	double gt = config.GetTimeGap();
	double num_p = (et-st)/gt;
	
	int nrecord = 0;

	double sum_dx2=0, sum_dy2=0, sum_dz2=0;
	
	for(int count=0; count<num_p; count++)
	{
		double t=st+gt*count;
		
		////////////////////////////////////////////////////////////////
		//
		if((DEBUG_TAG==1)||(DEBUG_TAG==2)) 
		{
			debugfile<<endl<<"[Time:"<<t<<"]"<<endl;
			debugfile.flush();
		}
		//
		////////////////////////////////////////////////////////////////
		
		double X, Y, Z;
		//maximum Z value
		Z = config.GetMax_Zval();
		
		//Time setting
		config.GetOrientation(t);

		double a, b;//swing angles
		config.GetLBAngle(a,b);

		////////////////////////////////////////////////////////////////
		//
		if((DEBUG_TAG==1)||(DEBUG_TAG==2)) 
		{
			double O, P, K;
			config.GetINSAngle(O,P,K);
			debugfile<<"INS angle:"<<O<<"\t"<<P<<"\t"<<K<<endl;
			debugfile<<"Laser beam angle(deg):"<<Rad2Deg(a)<<"\t"<<Rad2Deg(b)<<endl;
			debugfile.flush();
			if(DEBUG_TAG==2)
			{
				debugfile<<"Time, alpha(t) and beta(t) (deg):"<<t<<"\t"<<Rad2Deg(a)<<"\t"<<Rad2Deg(b)<<endl;
				debugfile.flush();
			}
		}
		//
		////////////////////////////////////////////////////////////////
		
		//To find ground coord(X,Y) using maxZ(given Z)
		FindGroundCoord(config.GetMG(),config.GetT(),X,Y,Z);
		
		//footprint (on maxZ plane)
		DWORD Ix, Iy;			
		if((true==m_RefDEM.GetIndexX(X,Ix))&&(true==m_RefDEM.GetIndexY(Y,Iy)))
		{
			footprintPixel_fixed_Z[(int)Iy][(int)Ix] = (BYTE)0;
		}
		
		//Laser unit center
		CSMMatrix<double> L_Unit = config.GetL_Unit();
		
		////////////////////////////////////////////////////////////////
		//
		if(DEBUG_TAG==2) 
		{
			debugfile<<"Laser Unit Position:"<<L_Unit(0,0)<<"\t"<<L_Unit(1,0)<<"\t"<<L_Unit(2,0)<<endl;
			debugfile.flush();
		}
		//
		////////////////////////////////////////////////////////////////
		
		//slope
		double dx = X - L_Unit(0,0);
		double dy = Y - L_Unit(1,0);
		double dz = Z - L_Unit(2,0);
		
		//distance from nadir to object
		double XY_R = sqrt(dx*dx + dy*dy);
		
		if(XY_R<m_RefDEM.offset) 
		{
			XY_R=m_RefDEM.offset;
		}
		
		//DEM searching offset
		double offsetZ;
		offsetZ = m_RefDEM.offset*(0.5)*dz/XY_R;
		
		////////////////////////////////////////////////////////////////
		//
		if(DEBUG_TAG==2)
		{
			debugfile<<"Footprint position:"<<X<<"\t"<<Y<<"\t"<<Ix<<"\t"<<Iy<<"\t"<<endl;
			debugfile<<"Time: "<<t<<"\t"<<"dx, dy dz R:"<<dx<<"\t"<<dy<<"\t"<<dz<<"\t"<<XY_R<<endl;
			debugfile.flush();
		}
		//
		////////////////////////////////////////////////////////////////
		
		double DEM_Z = -1.0;
		DWORD indexX, indexY;
		DWORD oldindexX, oldindexY;
		
		int loop_index=0;
		bool bStop = true;
		double old_Z, old_DEM_Z =Z;
		double old_X = X, old_Y = Y;
		
		bool bRecord = false;
		
		do
		{
			//To increase loop index
			loop_index ++;
			
			double DEM_X, DEM_Y;
			
			////////////////////////////////////////////////////////////////
			//
			if(DEBUG_TAG==2) {
				debugfile<<endl<<"Loop for finding the ground coordinates:"<<loop_index<<endl;
				debugfile<<"-----------------------------------------------------------"<<endl;
				debugfile.flush();
			}
			//
			////////////////////////////////////////////////////////////////
			
			//To find Z value using given X and Y
			if(m_DEMInterpolationMethod == -1)
			{
				if((fabs(a) <= fabs(m_RefDEM.offset/dz/2))&&(fabs(b) <= fabs(m_RefDEM.offset/dz/2)))
				{
					FindGroundCoord(config.GetMG(),config.GetT(),X,Y,0);

					if(true == m_RefDEM.GetNearestZ(X,Y,indexX, indexY, DEM_Z, DEM_X, DEM_Y))
					{
						FindGroundCoord(config.GetMG(),config.GetT(),X,Y,DEM_Z);
						Z = DEM_Z;
						bRecord = true;
						break;
					}
				}

				if(true == m_RefDEM.GetNearestZ(X,Y,indexX, indexY, DEM_Z))
				{
					////////////////////////////////////////////////////////////////
					//
					if(DEBUG_TAG==2)
					{
						debugfile<<"X,Y,indexX,indexY,DEM_Z"<<X<<"\t"<<Y<<"\t"<<indexX<<"\t"<<indexY<<"\t"<<DEM_Z<<endl;
						debugfile<<"-----------------------------------------------------------"<<endl;
						debugfile.flush();
					}
					//
					////////////////////////////////////////////////////////////////
					//Height comparing
					if(DEM_Z >= Z)
					{
						if(fabs((DEM_Z - Z))<LIDAR_PRECISION)
						{
							bRecord = true;
							break;
						}
						else
						{
							if(true == SearchingInterMediatePoint(DEM_Z,old_Z,
								X,Y,Z,
								indexX,indexY,
								debugfile,DEBUG_TAG))
							{
								bRecord = true;
							}
							else
							{
								bRecord = false;
							}
							break;
						}							
					}
					else
					{
						if(fabs((DEM_Z - Z))<LIDAR_PRECISION)
						{
							bRecord = true;
							break;
						}
						else
						{
							old_Z = Z;
						}
					}
				}
			}
			else if(m_DEMInterpolationMethod == -1)
			{
				if((fabs(a) <= fabs(m_RefDEM.offset/dz/2))&&(fabs(b) <= fabs(m_RefDEM.offset/dz/2)))
				{
					FindGroundCoord(config.GetMG(),config.GetT(),X,Y,0);

					if(true == m_RefDEM.GetNearestZ(X,Y,indexX, indexY, DEM_Z, DEM_X, DEM_Y))
					{
						FindGroundCoord(config.GetMG(),config.GetT(),X,Y,DEM_Z);
						Z = DEM_Z;
						bRecord = true;
						break;
					}
				}

				if(true == m_RefDEM.GetNearestZ(X,Y,indexX, indexY, DEM_Z, DEM_X, DEM_Y))
				{
					////////////////////////////////////////////////////////////////
					//
					if(DEBUG_TAG==2)
					{
						debugfile<<"X,Y,indexX,indexY,DEM_Z"<<X<<"\t"<<Y<<"\t"<<indexX<<"\t"<<indexY<<"\t"<<DEM_Z<<endl;
						debugfile<<"-----------------------------------------------------------"<<endl;
						debugfile.flush();
					}
					//
					////////////////////////////////////////////////////////////////
					//Height comparing
					if(DEM_Z >= Z)
					{
						if(fabs((DEM_Z - Z))<LIDAR_PRECISION)
						{
							bRecord = true;
							break;
						}
						else
						{
							double oldDEMX, oldDEMY;
							m_RefDEM.GetX(oldindexX, oldDEMX);
							m_RefDEM.GetX(oldindexY, oldDEMY);

							if(true == SearchingInterMediatePoint(DEM_Z,old_Z,
								X,Y,Z,
								indexX,indexY,
								debugfile,DEBUG_TAG))
							{
								bRecord = true;
							}
							else
							{
								bRecord = false;
							}
							break;						
						}							
					}
				}
			}
			else if(m_DEMInterpolationMethod == 0)
			{
//				if((fabs(a) <= fabs(m_RefDEM.offset/dz))&&(fabs(b) <= fabs(m_RefDEM.offset/dz)))
//				{
//					if(true == m_RefDEM.GetNearestZ(L_Unit(0,0),L_Unit(1,0),indexX, indexY, DEM_Z, DEM_X, DEM_Y))
//					{
//						X = L_Unit(0,0); Y = L_Unit(1,0); Z = DEM_Z;
//						bRecord = true;
//						break;
//					}
//				}

				if(true == m_RefDEM.GetNearestZ(X,Y,indexX, indexY, DEM_Z, DEM_X, DEM_Y))
				{
					////////////////////////////////////////////////////////////////
					//
					if(DEBUG_TAG==2)
					{
						debugfile<<"X,Y,indexX,indexY,DEM_Z"<<X<<"\t"<<Y<<"\t"<<indexX<<"\t"<<indexY<<"\t"<<DEM_Z<<endl;
						debugfile<<"-----------------------------------------------------------"<<endl;
						debugfile.flush();
					}
					//
					////////////////////////////////////////////////////////////////
					//Height comparing
					if(DEM_Z >= Z)
					{
						Z = DEM_Z;
						FindGroundCoord(config.GetMG(),config.GetT(),X,Y,Z);
						bRecord = true;
						break;

//						if(fabs((DEM_Z - Z))<LIDAR_PRECISION)
//						{
//							bRecord = true;
//							break;
//						}
//						else
//						{
//							double newX, newY, newZ;
//							
//							do
//							{
//								newX = old_X + 0.1;
//								newY = old_Y + 0.1;
//								newZ = FindGroundCoord(config.GetMG(), config.GetT(), newX, newY);
//
//								if(newZ > DEM_Z)
//								{
//									X = (old_X + newX)/2;
//									Y = (old_Y + newY)/2;
//									Z = FindGroundCoord(config.GetMG(), config.GetT(), X, Y);
//
//									bRecord = true;
//									break;
//								}
//								else
//								{
//									old_X = newX;
//									old_Y = newY;
//									old_Z = newZ;
//								}
//							}while(fabs(newX-X)>0.1&&fabs(newY-Y)>0.1);
//							
//							break;
//						}							
					}
				}
			}
			else if(m_DEMInterpolationMethod == 1)
			{
				if((fabs(a) <= fabs(m_RefDEM.offset/dz))&&(fabs(b) <= fabs(m_RefDEM.offset/dz)))
				{
					if(true == m_RefDEM.GetBilinearZ(L_Unit(0,0),L_Unit(1,0),indexX, indexY, DEM_Z))
					{
						X = L_Unit(0,0); Y = L_Unit(1,0); Z = DEM_Z;
						bRecord = true;
						break;
					}
				}

				if(true == m_RefDEM.GetBilinearZ(X,Y,indexX, indexY, DEM_Z))
				{
					////////////////////////////////////////////////////////////////
					//
					if(DEBUG_TAG==2)
					{
						debugfile<<"X,Y,indexX,indexY,DEM_Z"<<X<<"\t"<<Y<<"\t"<<indexX<<"\t"<<indexY<<"\t"<<DEM_Z<<endl;
						debugfile<<"-----------------------------------------------------------"<<endl;
						debugfile.flush();
					}
					//
					////////////////////////////////////////////////////////////////
					//Height comparing
					if(DEM_Z >= Z)
					{
						if(fabs((DEM_Z - Z))<LIDAR_PRECISION)
						{
							bRecord = true;
							break;
						}
						else
						{
							if(true == SearchingInterMediatePoint(DEM_Z,old_Z,
								X,Y,Z,
								indexX,indexY,
								debugfile,DEBUG_TAG))
							{
								bRecord = true;
							}
							else
							{
								bRecord = false;
							}
							break;
						}							
					}
					else
					{
						if(fabs((DEM_Z - Z))<LIDAR_PRECISION)
						{
							bRecord = true;
							break;
						}
						else
						{
							old_Z = Z;
						}
					}
				}
			}
			else
			{
				CString msg;
				msg.Format("Wrong Interpolation Tag: %d",m_DEMInterpolationMethod);
				AfxMessageBox(msg);
				return false;
			}
			
			if(Z < config.GetMin_Zval())
			{
				////////////////////////////////////////////////////////////////
				//
				if((DEBUG_TAG==1)||(DEBUG_TAG==2))
				{
					debugfile<<Z<<"<"<<config.GetMin_Zval()<<endl;
					debugfile.flush();
				}
				//
				////////////////////////////////////////////////////////////////
				bStop = false;
				break;
			}
			
			//To change Z value
			old_Z = Z;
			old_X = X;
			old_Y = Y;
			old_DEM_Z = DEM_Z;

			oldindexX = indexX;
			oldindexY = indexY;
			
			Z = Z + offsetZ;
			
			//To find ground coord(X,Y) using maxZ(given Z)
			FindGroundCoord(config.GetMG(),config.GetT(),X,Y,Z);
			
			////////////////////////////////////////////////////////////////
			//
			if(DEBUG_TAG==2)
			{
				debugfile<<"X, Y, and Z:"<<X<<"\t"<<Y<<"\t"<<Z<<endl;
				debugfile.flush();
			}
			//
			////////////////////////////////////////////////////////////////
			
			//Loop index check
			if(loop_index > _MAX_LOOP_) 
			{
				////////////////////////////////////////////////////////////////
				//
				if((DEBUG_TAG==1)||(DEBUG_TAG==2))
				{
					debugfile<<"Loop index excesses the maximum number."<<endl;
					debugfile.flush();
				}
				//
				////////////////////////////////////////////////////////////////
				bStop = false;
				break;
			}
			
		} while(bStop);
		
		if((bStop == true)&&(bRecord == true)&&(Z>=0))
		{
			//
			//Without Error
			//
			//debugfile.width(24);
			LidarFile.precision(10);
			LidarFile.setf(ios::fixed, ios::floatfield);
			LidarFile<<setw(10)<<t<<"\t"<<setw(10)<<X<<"\t"<<setw(10)<<Y<<"\t"<<setw(10)<<Z<<endl;
			LidarFile.flush();
			
			if(DEBUG_TAG==3)
			{
				LidarFile_Raw.precision(10);
				LidarFile_Raw.setf(ios::fixed, ios::floatfield);
				
				double INS_O, INS_P, INS_K;
				config.GetINSAngle(INS_O,INS_P,INS_K);
				double alpha, beta;
				config.GetLBAngle(alpha,beta);
				double GPS_X, GPS_Y, GPS_Z;
				config.GetGPSPosition(GPS_X,GPS_Y,GPS_Z);
				double dist_X, dist_Y, dist_Z;
				dist_X = X-L_Unit(0,0); dist_Y = Y-L_Unit(1,0); dist_Z = Z-L_Unit(2,0);
				double dist = sqrt(dist_X*dist_X+dist_Y*dist_Y+dist_Z*dist_Z);					
				
				LidarFile_Raw<<setw(10)<<GPS_X<<"\t"<<setw(10)<<GPS_Y<<"\t"<<setw(10)<<GPS_Z<<"\t";
				LidarFile_Raw<<setw(10)<<Rad2Deg(INS_O)<<"\t"<<setw(10)<<Rad2Deg(INS_P)<<"\t"<<setw(10)<<Rad2Deg(INS_K)<<"\t";
				LidarFile_Raw<<setw(10)<<Rad2Deg(alpha)<<"\t"<<setw(10)<<Rad2Deg(beta)<<"\t";
				LidarFile_Raw<<setw(10)<<dist<<"\t"<<setw(10)<<t<<endl;
				LidarFile_Raw.flush();
			}
			
			//footprint
			if((true==m_RefDEM.GetIndexX(X,Ix))&&(true==m_RefDEM.GetIndexY(Y,Iy)))
				footprintPixel_real_terrain[(int)Iy][(int)Ix] = (BYTE)0;
			////////////////////////////////////////////////////////////////
			//
			if((DEBUG_TAG==1)||(DEBUG_TAG==2))
			{
				double dist_X, dist_Y, dist_Z;
				dist_X = X-L_Unit(0,0); dist_Y = Y-L_Unit(1,0); dist_Z = Z-L_Unit(2,0);
				double dist = sqrt(dist_X*dist_X+dist_Y*dist_Y+dist_Z*dist_Z);
				
				debugfile<<"X,Y,Z and distance: "<<X<<"\t"<<Y<<"\t"<<Z<<"\t"<<dist<<endl;
				debugfile<<"Footprint col and row:"<<Ix<<"\t"<<Iy<<endl;
				debugfile.flush();
			}
			//
			////////////////////////////////////////////////////////////////
			//
			//With Error(Bias)
			//
			if(bErrorModeling == true)
			{
				double EX, EY, EZ;
				CSMMatrix<double> MG_E, T_E;
				double dist_E;
				config.GetOrientationWithError();
				MG_E = config.GetMG_Error();
				T_E = config.GetT_Error();
				double dx = X - L_Unit(0,0);
				double dy = Y - L_Unit(1,0);
				double dz = Z - L_Unit(2,0);
				dist_E = sqrt(dx*dx + dy*dy + dz*dz);
				//dist_E += *config.GetHandleBiasBeam();
				dist_E = (dist_E*(*config.GetHandleScaleRanging())) +config.RE_range + (*config.GetHandleBiasBeam());

				FindGroundCoord(MG_E, T_E, dist_E, EX, EY, EZ);
				LidarFile_Error.precision(10);
				LidarFile_Error.setf(ios::fixed, ios::floatfield);
				LidarFile_Error<<setw(10)<<t<<"\t"<<setw(10)<<EX<<"\t"<<setw(10)<<EY<<"\t"<<setw(10)<<EZ<<endl;
				LidarFile_Error.flush();
				
				//differences of coordinates
				LidarFile_Diff.precision(10);
				LidarFile_Diff.setf(ios::fixed, ios::floatfield);
				LidarFile_Diff<<setw(10)<<EX-X<<"\t"<<setw(10)<<EY-Y<<"\t"<<setw(10)<<EZ-Z<<endl;
				LidarFile_Diff.flush();
				
				sum_dx2 += (EX-X)*(EX-X);
				sum_dy2 += (EY-Y)*(EY-Y);
				sum_dz2 += (EZ-Z)*(EZ-Z);
				
				//footprint
				DWORD IxE, IyE;
				if((true==m_RefDEM.GetIndexX(EX,IxE))&&(true==m_RefDEM.GetIndexY(EY,IyE)))
					footprintPixelError_real_terrain[(int)IyE][(int)IxE] = (BYTE)0;
				
				////////////////////////////////////////////////////////////////
				//
				if((DEBUG_TAG==1)||(DEBUG_TAG==2))
				{
					debugfile<<"EX,EY,EZ:"<<"\t"<<EX<<"\t"<<EY<<"\t"<<EZ<<endl;
					debugfile<<"Footprint E_col and E_row:"<<IxE<<"\t"<<IyE<<endl;
					debugfile.flush();
				}
				//
				////////////////////////////////////////////////////////////////
				
				if(DEBUG_TAG==3)
				{
					LidarFile_Raw_Error.precision(10);
					LidarFile_Raw_Error.setf(ios::fixed, ios::floatfield);
					
					double INS_O_Error, INS_P_Error, INS_K_Error;
					config.GetINSErrorAngle(INS_O_Error,INS_P_Error,INS_K_Error);
					double alpha_Error, beta_Error;
					config.GetLBErrorAngle(alpha_Error,beta_Error);
					double GPS_X_Error, GPS_Y_Error, GPS_Z_Error;
					config.GetGPSErrorPosition(GPS_X_Error,GPS_Y_Error,GPS_Z_Error);
					double dist_Error = dist_E;
					
					LidarFile_Raw_Error<<setw(10)<<GPS_X_Error<<"\t"<<setw(10)<<GPS_Y_Error<<"\t"<<setw(10)<<GPS_Z_Error<<"\t";
					LidarFile_Raw_Error<<setw(10)<<Rad2Deg(INS_O_Error)<<"\t"<<setw(10)<<Rad2Deg(INS_P_Error)<<"\t"<<setw(10)<<Rad2Deg(INS_K_Error)<<"\t";
					LidarFile_Raw_Error<<setw(10)<<Rad2Deg(alpha_Error)<<"\t"<<setw(10)<<Rad2Deg(beta_Error)<<"\t";
					LidarFile_Raw_Error<<setw(10)<<dist_Error<<"\t"<<setw(10)<<t<<endl;
					LidarFile_Raw_Error.flush();
				}
				
			}
		}
	}
	
	//Footprint image save
	CSMStr temp = fname;
	temp += "_flatterrain.jpg";
	footprint_fixed_Z.SaveBMP(temp.GetChar());
	//Footprint image2 save
	temp = fname; temp += "_real_terrain.jpg";
	footprint_real_terrain.SaveBMP(temp.GetChar());
	//Lidar file
	LidarFile.close();
	
	//Footprint image and Lidar file with error save
	if(bErrorModeling==true)
	{
		temp = fname; temp += "_error_real_terrain.jpg";
		footprint_error_real_terrain.SaveBMP(temp.GetChar());
		
		LidarFile_Error.close();

		LidarFile_Diff<<"[STDEV]"<<endl;
		LidarFile_Diff<<"sx: "<<sqrt(sum_dx2/num_p)<<endl;
		LidarFile_Diff<<"sy: "<<sqrt(sum_dy2/num_p)<<endl;
		LidarFile_Diff<<"sz: "<<sqrt(sum_dz2/num_p)<<endl;

		LidarFile_Diff.close();		
	}
	
	if(DEBUG_TAG==3)
	{
		LidarFile_Raw.close();
		
		if(bErrorModeling == true)
		{
			LidarFile_Raw_Error.close();
		}
	}
	
	////////////////////////////////////////////////////////////////
	//
	if((DEBUG_TAG==1)||(DEBUG_TAG==2)) {		
		debugfile.close();
	}
	//
	////////////////////////////////////////////////////////////////
	
	return true;
}

bool CBBARABLiDARSim::MakeBatchLiDARSimData_MathematicalSurface(double resolution, const char* prjfile, const char* simfile, double default_Z, bool bGround)
{	
	fstream vtxlistfile;
	vtxlistfile.open(prjfile, ios::in);
	
	if( false == InputData(simfile,default_Z) ) return false;

	double dH=0;

	char line[512];

	double oriSW = 2*config.Total_b*config.sZ;

	//double resolution = oriSW/((config.BFTime/2)/config.TimeGap);

	int countH=0;
	int countA=0;
	int count=0;

	while(!vtxlistfile.eof())
	{
		double angle, height;
		vtxlistfile>>angle>>height;
		angle = Deg2Rad(angle);
		vtxlistfile>>ws;
		vtxlistfile.getline(line, 512);
		vtxlistfile>>ws;

		config.sZ = height;

		if(config.Total_a != 0)
			config.Total_a = angle;
		if(config.Total_b != 0)
			config.Total_b = angle;

		config.TimeGap = resolution*(config.BFTime/2)/(2*config.Total_b*config.sZ);

		config.EndTime = config.StartTime + 0.1 + (2*config.Total_b*config.sZ)/sqrt(config.A1*config.A1+config.B1*config.B1+config.C1*config.C1);
		
		CLIDARBiasCalibration::ReadPlaneVertex(line, PatchList, vertex_min, vertex_max);

		CString fname = line;
		fname.MakeLower();
		fname.Replace(".vertex",".txt");
		MakeLiDARSimData_MathematicalSurface(fname, default_Z, bGround);

	}

	return true;
}

bool CBBARABLiDARSim::MakeLiDARSimData_MathematicalSurface(const char* fname, double init_distance, bool bGround)
{
	fstream debugfile;//debug file
	debugfile.precision(10);
	debugfile.setf(ios::fixed, ios::floatfield);
	
	int DEBUG_TAG = config.GetDEBUG_TAG();
	
	////////////////////////////////////////////////////////////////
	//
	if((DEBUG_TAG==1)||(DEBUG_TAG==2)) 
	{
		CSMStr debugname = fname;
		debugname += ".TMP";
		
		debugfile.open(debugname.GetChar(),ios::out);
		debugfile<<"Debug File..."<<endl<<endl;
		debugfile.flush();			
	}
	//
	////////////////////////////////////////////////////////////////

	fstream LidarFile_TXYZ;
	CSMStr txyz = fname;
	txyz += "_TXYZ.tpt";
	LidarFile_TXYZ.open(txyz,ios::out);
	
	fstream LidarFile_TXYZ_Error;
	fstream NDerrorfile;
	fstream LidarFile_Diff;//Differences between without error and with error

	CSMStr efname = fname;
	efname += "_ND_Error.txt";
	NDerrorfile.open(efname,ios::out);

	if(bErrorModeling==true)
	{
		CSMStr txyz_error = fname;
		txyz_error += "_TXYZ_Error.tpt";
		LidarFile_TXYZ_Error.open(txyz_error,ios::out);
		
		CSMStr fname_diff = fname;
		fname_diff += "_Diff.txt";
		LidarFile_Diff.open(fname_diff.GetChar(),ios::out);
	}
	

	fstream LidarFile_Raw; //Raw Lidar data
	fstream LidarFile_Raw_Error; //Raw Lidar data with error
	if(DEBUG_TAG==3)
	{
		CSMStr fname_Raw = fname;
		fname_Raw += ".raw";
		LidarFile_Raw.open(fname_Raw.GetChar(),ios::out);
		
		LidarFile_Raw<<"#GPS_X(M)\t"<<"GPS_Y(M)\t"<<"GPS_Z(M)\t";
		LidarFile_Raw<<"INS_O(deg)\t"<<"INS_P(deg)\t"<<"INS_K(deg)\t";
		LidarFile_Raw<<"alpha(deg)\t"<<"beta(deg)\t";
		LidarFile_Raw<<"dist(M)\t"<<"time(sec)"<<endl;
		LidarFile_Raw<<"###################################################################";
		LidarFile_Raw<<"###################################################################"<<endl;
		LidarFile_Raw.flush();

		if(bErrorModeling == true)
		{
			CSMStr fname_Raw_Error = fname;
			fname_Raw_Error += "_Error.raw";
			LidarFile_Raw_Error.open(fname_Raw_Error.GetChar(),ios::out);
			
			LidarFile_Raw_Error<<"#GPS_X(M)\t"<<"GPS_Y(M)\t"<<"GPS_Z(M)\t";
			LidarFile_Raw_Error<<"INS_O(deg)\t"<<"INS_P(deg)\t"<<"INS_K(deg)\t";
			LidarFile_Raw_Error<<"alpha(deg)\t"<<"beta(deg)\t";
			LidarFile_Raw_Error<<"dist(M)\t"<<"time(sec)"<<endl;
			LidarFile_Raw_Error<<"###################################################################";
			LidarFile_Raw_Error<<"###################################################################"<<endl;
			LidarFile_Raw_Error.flush();
		}
	}
	
	////////////////////////////////////////////////////////////////
	//
	if((DEBUG_TAG==1)||(DEBUG_TAG==2))  
	{
		debugfile<<"LiDAR data file:"<<fname<<endl;
		debugfile.flush();
	}
	//
	////////////////////////////////////////////////////////////////
	
	double st = config.GetStartTime();
	double et = config.GetEndTime();
	double gt = config.GetTimeGap();
	double num_p = (et-st)/gt;

	double sum_dx2=0, sum_dy2=0, sum_dz2=0;

	for(int count=0; count<num_p; count++)
	{
		double t=st+gt*count;
	
		////////////////////////////////////////////////////////////////
		//
		if((DEBUG_TAG==1)||(DEBUG_TAG==2))  
		{
			debugfile<<endl<<"[Time:"<<t<<"]"<<endl;
			debugfile.flush();
		}
		//
		////////////////////////////////////////////////////////////////
		
		double X, Y, Z;
				
		//Time setting
		config.GetOrientation(t);
		
		////////////////////////////////////////////////////////////////
		//
		if((DEBUG_TAG==1)||(DEBUG_TAG==2))  
		{
			double O, P, K;
			config.GetINSAngle(O,P,K);
			debugfile<<"INS angle:"<<O<<"\t"<<P<<"\t"<<K<<endl;
			double a, b;
			config.GetLBAngle(a,b);
			debugfile<<"Laser beam angle(deg):"<<Rad2Deg(a)<<"\t"<<Rad2Deg(b)<<endl;
			debugfile.flush();
		}
		//
		////////////////////////////////////////////////////////////////
		
		//
		//Without Error
		//
		//To find ground coordinate (X, Y, Z) using given initial distance
		FindGroundCoord(config.GetMG(),config.GetT(), init_distance,  X, Y, Z);
		
		//Laser unit center
		CSMMatrix<double> L_Unit = config.GetL_Unit();
		
		Point3D<double> P1(X, Y, Z);
		Point3D<double> P2(L_Unit(0,0), L_Unit(1,0), L_Unit(2,0));
		bool inner_point = false;
		double min_dist=1.0e99;

		for(int index_patches=0; index_patches<(int)PatchList.GetNumItem(); index_patches++)
		{
			double x1, y1, z1;
			double x2, y2, z2;
			double x3, y3, z3;
			double a, b, c;

			CLIDARPlanarPatch patch = PatchList.GetAt(index_patches);
			
			x1 = patch.Vertex[0];
			y1 = patch.Vertex[1];
			z1 = patch.Vertex[2];
			
			x2 = patch.Vertex[3];
			y2 = patch.Vertex[4];
			z2 = patch.Vertex[5];
			
			x3 = patch.Vertex[6];
			y3 = patch.Vertex[7];
			z3 = patch.Vertex[8];
			
			a = patch.coeff[0];
			b = patch.coeff[1];
			c = patch.coeff[2];

			bool bFind = false;
			double NormalVec[3];
			NormalVec[0] = patch.coeff[0];
			NormalVec[1] = patch.coeff[1];
			NormalVec[2] = patch.coeff[2];

			double Vertex0[3];
			Vertex0[0] = patch.Vertex[0];
			Vertex0[1] = patch.Vertex[1];
			Vertex0[2] = patch.Vertex[2];

			double PA[3], PB[3], P0[3];
			PA[0] = P1.x; PA[1] = P1.y; PA[2] = P1.z;
			PB[0] = P2.x; PB[1] = P2.y; PB[2] = P2.z;

			if(true == IntersectPlaneLine(NormalVec, Vertex0, PA,PB,P0))
			{
				//Polygon inside check
				double ARE,are1, are2, are3;
				ARE =  patch.Area;//CLIDARBiasCalibration::CalTriangleArea(x1 ,y1 ,z1 ,x2,y2,z2,x3,y3,z3);
				are1 = CLIDARBiasCalibration::CalTriangleArea(P0[0],P0[1],P0[2],x2,y2,z2,x3,y3,z3);
				are2 = CLIDARBiasCalibration::CalTriangleArea(P0[0],P0[1],P0[2],x1,y1,z1,x3,y3,z3);
				are3 = CLIDARBiasCalibration::CalTriangleArea(P0[0],P0[1],P0[2],x2,y2,z2,x1,y1,z1);

				double temp = a*P0[0] + b*P0[1] + c*P0[2];
				double a_ = a/(1+(temp));
				double b_ = b/(1+(temp));
				double c_ = c/(1+(temp));
				
				double fittingerror = 1.0/sqrt(a_*a_ + b_*b_ + c_*c_);
				
				double sum=0;
				for(unsigned int j=0; j<3; j++)
				{
					double sumi=0;
					sumi += patch.SD[j*3+0]*patch.SD[j*3+0];
					sumi += patch.SD[j*3+1]*patch.SD[j*3+1];
					sumi += patch.SD[j*3+2]*patch.SD[j*3+2];
					
					sum += sqrt(sumi);
				}
				double precision = sum/3.0;
				
				if((are1/ARE + are2/ARE + are3/ARE)>=(1.0+1.0e-6))//threshold for the error in the summation of areas
				{
					continue;
				}
				
				if(fittingerror>=precision) //plane fitting error
				{
					continue;
				}
				else
				{
					NDerrorfile<<fittingerror<<"\t"<<P0[0]<<"\t"<<P0[1]<<"\t"<<P0[2]<<endl;
				}
				
				double dist = sqrt( (P2.x - P0[0])*(P2.x - P0[0]) + (P2.y - P0[1])*(P2.y - P0[1]) + (P2.z - P0[2])*(P2.z - P0[2]) );
				
				if( dist < min_dist)//closest patch
				{
					min_dist = dist;
					
					X = P0[0]; Y = P0[1]; Z = P0[2];
					inner_point = true;
				}

				bFind = true;
			}
			else
			{
				continue;
			}

			if(bFind == true)
			{
				break;
			}
		}

		if(inner_point ==  false)
		{
			X = P1.x; Y = P1.y; Z = P1.z;
		}
		
		if((bGround == true)||(inner_point ==  true))
		{
			LidarFile_TXYZ.precision(10);
			LidarFile_TXYZ.setf(ios::fixed, ios::floatfield);
			LidarFile_TXYZ<<setw(10)<<t<<"\t"<<setw(10)<<X<<"\t"<<setw(10)<<Y<<"\t"<<setw(10)<<Z<<endl;
			LidarFile_TXYZ.flush();
			
			if(DEBUG_TAG==3)
			{
				LidarFile_Raw.precision(10);
				LidarFile_Raw.setf(ios::fixed, ios::floatfield);
				
				double INS_O, INS_P, INS_K;
				config.GetINSAngle(INS_O,INS_P,INS_K);
				double alpha, beta;
				config.GetLBAngle(alpha,beta);
				double GPS_X, GPS_Y, GPS_Z;
				config.GetGPSPosition(GPS_X,GPS_Y,GPS_Z);
				double dist_X, dist_Y, dist_Z;
				dist_X = X-L_Unit(0,0); dist_Y = Y-L_Unit(1,0); dist_Z = Z-L_Unit(2,0);
				double dist = sqrt(dist_X*dist_X+dist_Y*dist_Y+dist_Z*dist_Z);					
				
				LidarFile_Raw<<setw(10)<<GPS_X<<"\t"<<setw(10)<<GPS_Y<<"\t"<<setw(10)<<GPS_Z<<"\t";
				LidarFile_Raw<<setw(10)<<Rad2Deg(INS_O)<<"\t"<<setw(10)<<Rad2Deg(INS_P)<<"\t"<<setw(10)<<Rad2Deg(INS_K)<<"\t";
				LidarFile_Raw<<setw(10)<<Rad2Deg(alpha)<<"\t"<<setw(10)<<Rad2Deg(beta)<<"\t";
				LidarFile_Raw<<setw(10)<<dist<<"\t"<<setw(10)<<t<<endl;
				LidarFile_Raw.flush();
			}
			
			////////////////////////////////////////////////////////////////
			//
			if(DEBUG_TAG==2)  
			{	
				debugfile<<"Laser Unit Position:"<<L_Unit(0,0)<<"\t"<<L_Unit(1,0)<<"\t"<<L_Unit(2,0)<<endl;
				
				double dist_X, dist_Y, dist_Z;
				dist_X = X-L_Unit(0,0); dist_Y = Y-L_Unit(1,0); dist_Z = Z-L_Unit(2,0);
				double dist = sqrt(dist_X*dist_X+dist_Y*dist_Y+dist_Z*dist_Z);
				
				debugfile<<"X,Y,Z and distance: "<<X<<"\t"<<Y<<"\t"<<Z<<"\t"<<dist<<endl;
				
				debugfile.flush();
			}
		}
		//
		////////////////////////////////////////////////////////////////
		
		//
		//With Error(Bias)
		//
		if((bErrorModeling == true)&&(inner_point ==  true))
		{
			double EX, EY, EZ;
			CSMMatrix<double> MG_E, T_E;
			double dist_E;
			config.GetOrientationWithError();
			MG_E = config.GetMG_Error();
			T_E = config.GetT_Error();
			double dx = X - L_Unit(0,0);
			double dy = Y - L_Unit(1,0);
			double dz = Z - L_Unit(2,0);
			dist_E = sqrt(dx*dx + dy*dy + dz*dz);
			//dist_E += *config.GetHandleBiasBeam();
			dist_E = (dist_E*(*config.GetHandleScaleRanging())) +config.RE_range + (*config.GetHandleBiasBeam());
			
			FindGroundCoord(MG_E, T_E, dist_E, EX, EY, EZ);
			
			LidarFile_TXYZ_Error.precision(10);
			LidarFile_TXYZ_Error.setf(ios::fixed, ios::floatfield);
			LidarFile_TXYZ_Error<<setw(10)<<t<<"\t"<<setw(10)<<EX<<"\t"<<setw(10)<<EY<<"\t"<<setw(10)<<EZ<<endl;
			LidarFile_TXYZ_Error.flush();
			
			//differences of coordinates
			LidarFile_Diff.precision(10);
			LidarFile_Diff.setf(ios::fixed, ios::floatfield);
			LidarFile_Diff<<setw(10)<<EX-X<<"\t"<<setw(10)<<EY-Y<<"\t"<<setw(10)<<EZ-Z<<endl;
			LidarFile_Diff.flush();

			sum_dx2 += (EX-X)*(EX-X);
			sum_dy2 += (EY-Y)*(EY-Y);
			sum_dz2 += (EZ-Z)*(EZ-Z);
			
			////////////////////////////////////////////////////////////////
			//
			if((DEBUG_TAG==1)||(DEBUG_TAG==2)) 
			{
				debugfile<<"EX,EY,EZ:"<<"\t"<<EX<<"\t"<<EY<<"\t"<<EZ<<endl;
				debugfile.flush();
			}
			//
			////////////////////////////////////////////////////////////////

			if(DEBUG_TAG==3)
			{
				LidarFile_Raw_Error.precision(10);
				LidarFile_Raw_Error.setf(ios::fixed, ios::floatfield);
				
				double INS_O_Error, INS_P_Error, INS_K_Error;
				config.GetINSErrorAngle(INS_O_Error,INS_P_Error,INS_K_Error);
				double alpha_Error, beta_Error;
				config.GetLBErrorAngle(alpha_Error,beta_Error);
				double GPS_X_Error, GPS_Y_Error, GPS_Z_Error;
				config.GetGPSErrorPosition(GPS_X_Error,GPS_Y_Error,GPS_Z_Error);
				double dist_Error = dist_E;
				
				LidarFile_Raw_Error<<setw(10)<<GPS_X_Error<<"\t"<<setw(10)<<GPS_Y_Error<<"\t"<<setw(10)<<GPS_Z_Error<<"\t";
				LidarFile_Raw_Error<<setw(10)<<Rad2Deg(INS_O_Error)<<"\t"<<setw(10)<<Rad2Deg(INS_P_Error)<<"\t"<<setw(10)<<Rad2Deg(INS_K_Error)<<"\t";
				LidarFile_Raw_Error<<setw(10)<<Rad2Deg(alpha_Error)<<"\t"<<setw(10)<<Rad2Deg(beta_Error)<<"\t";
				LidarFile_Raw_Error<<setw(10)<<dist_Error<<"\t"<<setw(10)<<t<<endl;
				LidarFile_Raw_Error.flush();
			}
			
		}
	}
	
	LidarFile_TXYZ.close();
	
	//ND error file
	NDerrorfile.close();
		
	//Footprint image and Lidar file with error save
	if(bErrorModeling==true)
	{
		LidarFile_TXYZ_Error.close();		
		
		LidarFile_Diff<<"[STDEV]"<<endl;
		LidarFile_Diff<<"sx: "<<sqrt(sum_dx2/num_p)<<endl;
		LidarFile_Diff<<"sy: "<<sqrt(sum_dy2/num_p)<<endl;
		LidarFile_Diff<<"sz: "<<sqrt(sum_dz2/num_p)<<endl;

		LidarFile_Diff.close();
	}
	
	if(DEBUG_TAG==3)
	{
		LidarFile_Raw.close();
		
		if(bErrorModeling == true)
		{
			LidarFile_Raw_Error.close();
		}
	}
	
	////////////////////////////////////////////////////////////////
	//
	if((DEBUG_TAG==1)||(DEBUG_TAG==2)) 
	{		
		debugfile.close();
	}
	//
	////////////////////////////////////////////////////////////////

	return true;
}

bool CBBARABLiDARSim::InputData(const char* simfile, const char* dempath, const char* hdrpath, double lidar_precision)
{
	if( false == InputData(simfile, lidar_precision) ) return false;
		
	refDEMPath = (char*)dempath;
	refDEMHDRPath = (char*)hdrpath;
	
	m_RefDEM.ReadDEM(refDEMPath, refDEMHDRPath);
	
	//min and max height of DEM
	double MinZ = 99999.;
	double MaxZ = -99999.;
	for(int i=0; i<(int)m_RefDEM.height; i++)
	{
		for(int j=0; j<(int)m_RefDEM.width; j++)
		{
			double z;
			if(false == m_RefDEM.GetZ(j,i,z)) continue;
			if(z<=-999.999) continue;
			if(z<MinZ) MinZ = z;
			if(z>MaxZ) MaxZ = z;
		}
	}

	config.SetMax_Zval(MaxZ);
	config.SetMin_Zval(MinZ);

	return true;
	
}

bool CBBARABLiDARSim::InputData(const char* simfile, double lidar_precision)
{
	LIDAR_PRECISION = lidar_precision;
	
	double OffsetX, OffsetY, OffsetZ; //spatial offset
	double Sigma_OX, Sigma_OY, Sigma_OZ;//sigma of spatial offset
	double Bias_OX, Bias_OY, Bias_OZ;//bias of spatial offset
	
	double Offset_Omega, Offset_Phi, Offset_Kappa;//rotational offset
	double Sigma_OO, Sigma_OP, Sigma_OK;//sigma of rotational offset
	double Bias_OO, Bias_OP, Bias_OK;//bias of rotational offset
	
	double X0, Y0, Z0;//GPS: start position
	double Sigma_GPSX, Sigma_GPSY, Sigma_GPSZ;//sigma of GPS signal
	double a[3], b[3], c[3];//coefficients of trajectory of GPS
	
	double O0, P0, K0;//INS: start attitude
	double Sigma_O0, Sigma_P0, Sigma_K0;//sigma of INS signal
	double d[3], e[3], f[3];//coefficient of attitude model of INS
	
	double Ta, Tb;//swing angle
	double Sigma_A, Sigma_B;//sigma of swing angle
	
	double signal_interval;//interval of laser signal
	double Sigma_ranging;//sigma of ranging distance
	double Bias_Beam;//bias of ranging
	double Scale_Beam;//scale of ranging
	
	double cycle;//scanning cycle (back and forth time)
	
	double sTime, eTime;//scanning start and end time
	
	int ErrorModeling;//tag for error modeling
	
	int debug_tag;//tag for debug file generation
	
	double _PI_ = GetPI();
	fstream infile;
	infile.open(simfile, ios::in);
	
	//version check
	int pos;
	double ver_num = 1.0;
	bool bVersion = FindString(infile, "VER", pos);
	if(bVersion == true)
	{
		//To move to begin of file
		infile.seekg(0,ios::beg);
		char temp[_MAX_STRING_LENGTH_];//"VER"
		infile>>temp>>ver_num;//version
	}
	else
	{
		//To move to begin of file
		infile.seekg(0,ios::beg);//old version without version number(before ver 1.1)
	}
	
	RemoveCommentLine(infile, '#');
	//# Laser Unit Relative Attitude w.r.t INS (unit: deg)
	//# omega sigma_O phi sigma_K kappa sigma_K
	infile>>Offset_Omega>>Sigma_OO;		Offset_Omega = Offset_Omega/180.*_PI_;		Sigma_OO = Sigma_OO/180.*_PI_;
	infile>>Offset_Phi>>Sigma_OP;		Offset_Phi	 = Offset_Phi/180.*_PI_;		Sigma_OP = Sigma_OP/180.*_PI_;
	infile>>Offset_Kappa>>Sigma_OK;		Offset_Kappa = Offset_Kappa/180.*_PI_;		Sigma_OK = Sigma_OK/180.*_PI_;
	double *SOA = NULL; SOA = config.GetHandleSigmaOffsetAngle();
	SOA[0] = Sigma_OO; SOA[1] = Sigma_OP; SOA[2] = Sigma_OK;
	
	RemoveCommentLine(infile, '#');
	//# Bias of offset algle 3 real number (unit: deg)
	infile>>Bias_OO>>Bias_OP>>Bias_OK;
	Bias_OO = Bias_OO/180.*_PI_; Bias_OP = Bias_OP/180.*_PI_; Bias_OK = Bias_OK/180.*_PI_;
	double* BOA = NULL; BOA = config.GetHandleBiasOffsetAngle();
	BOA[0] = Bias_OO; BOA[1] = Bias_OP; BOA[2] = Bias_OK;
	
	RemoveCommentLine(infile, '#');
	//# Laser Unit Offset w.r.t INS (unit: meter)
	//# OX, sigma_X, OY, sigma_Y, OZ, sigma_Z
	infile>>OffsetX>>Sigma_OX;
	infile>>OffsetY>>Sigma_OY;
	infile>>OffsetZ>>Sigma_OZ;	
	double *SOV = NULL; SOV = config.GetHandleSigmaOffsetVector();
	SOV[0] = Sigma_OX; SOV[1] = Sigma_OY; SOV[2] = Sigma_OZ;
	
	RemoveCommentLine(infile, '#');
	//# Bias of offset vector: 3 real number
	infile>>Bias_OX>>Bias_OY>>Bias_OZ;
	double* BOV = NULL; BOV = config.GetHandleBiasOffsetVector();
	BOV[0] = Bias_OX; BOV[1] = Bias_OY; BOV[2] = Bias_OZ;
	
	//[Trajectory data]
	
	if(ver_num > 1.5)
	{
		RemoveCommentLine(infile, '#');
		infile>>config.trj_option;//0: parameter input 1: file input
	}
	else
	{
		config.trj_option = 0;
	}

	if(config.trj_option == 0)
	{
		RemoveCommentLine(infile, '#');
		//# GPS start position(unit: meter)
		//# X0, sigma_X, Y0, sigma_Y,Z0, sigma_Z
		infile>>X0>>Sigma_GPSX;
		infile>>Y0>>Sigma_GPSY;
		infile>>Z0>>Sigma_GPSZ;
		double *SGPS = NULL; SGPS = config.GetHandleSigmaGPS();
		SGPS[0] = Sigma_GPSX; SGPS[1] = Sigma_GPSY; SGPS[2] = Sigma_GPSZ;
		
		RemoveCommentLine(infile, '#');
		//# Trajectory(GPS signal) modeling
		//# Xt = X0 + A1*t + A2*t^2 + A3*t^3
		//# Yt = Y0 + B1*t + B2*t^2 + B3*t^3
		//# Zt = Z0 + C1*t + C2*t^2 + C3*t^3
		infile>>a[0]>>a[1]>>a[2];
		infile>>b[0]>>b[1]>>b[2];
		infile>>c[0]>>c[1]>>c[2];
		
		RemoveCommentLine(infile, '#');
		//# INS start attitude(unit: deg)
		//# 0mega, sigma_O, Phi, sigma_P,Kappa, sigma_K
		infile>>O0>>Sigma_O0;	O0 = O0/180.*_PI_;	Sigma_O0 = Sigma_O0/180.*_PI_;
		infile>>P0>>Sigma_P0;	P0 = P0/180.*_PI_;	Sigma_P0 = Sigma_P0/180.*_PI_;
		infile>>K0>>Sigma_K0;	K0 = K0/180.*_PI_;	Sigma_K0 = Sigma_K0/180.*_PI_;
		double *SINS = NULL; SINS = config.GetHandleSigmaINS();
		SINS[0] = Sigma_O0; SINS[1] = Sigma_P0; SINS[2] = Sigma_K0;
		
		RemoveCommentLine(infile, '#');
		//# Attitude(INS signal) modeling
		//# Ot = O0 + D1*t + D2*t^2 + D3*t^3
		//# Pt = P0 + E1*t + E2*t^2 + E3*t^3
		//# Kt = K0 + F1*t + F2*t^2 + F3*t^3
		infile>>d[0]>>d[1]>>d[2];
		infile>>e[0]>>e[1]>>e[2];
		infile>>f[0]>>f[1]>>f[2];
		
		d[0] = d[0]/180.*_PI_;
		d[1] = d[1]/180.*_PI_;
		d[2] = d[2]/180.*_PI_;
		
		e[0] = e[0]/180.*_PI_;
		e[1] = e[1]/180.*_PI_;
		e[2] = e[2]/180.*_PI_;
		
		f[0] = f[0]/180.*_PI_;
		f[1] = f[1]/180.*_PI_;
		f[2] = f[2]/180.*_PI_;
	}
	else if(config.trj_option == 1)
	{
		char trjfilename[512];
		RemoveCommentLine(infile, '#');
		infile.getline(trjfilename, 512);

		CString stSimPath = simfile;
		CString strFolderPath = stSimPath.Mid(0, stSimPath.ReverseFind('\\'));
		CString trjpath = strFolderPath + "\\";
		trjpath += trjfilename;

		if( false == ReadTrajectoryData(trjpath) ) return false;

		RemoveCommentLine(infile, '#');
		//# noises of position data
		infile>>Sigma_GPSX;
		infile>>Sigma_GPSY;
		infile>>Sigma_GPSZ;

		double *SGPS = NULL; SGPS = config.GetHandleSigmaGPS();
		SGPS[0] = Sigma_GPSX; SGPS[1] = Sigma_GPSY; SGPS[2] = Sigma_GPSZ;
		
		RemoveCommentLine(infile, '#');
		//# noises of attitute data
		infile>>Sigma_O0;	Sigma_O0 = Sigma_O0/180.*_PI_;
		infile>>Sigma_P0;	Sigma_P0 = Sigma_P0/180.*_PI_;
		infile>>Sigma_K0;	Sigma_K0 = Sigma_K0/180.*_PI_;
		double *SINS = NULL; SINS = config.GetHandleSigmaINS();
		SINS[0] = Sigma_O0; SINS[1] = Sigma_P0; SINS[2] = Sigma_K0;
	}
	else
	{
		return false;
	}

	RemoveCommentLine(infile, '#');
	//# Laser beam angle setting (unit: deg)
	//# alpha and beta: total field of angle for X and Y axis respectively
	//# alpha, sigma_alpha, beta, sigma_beta
	infile>>Ta>>Sigma_A>>Tb>>Sigma_B;
	Ta = Ta/180.*_PI_;	Sigma_A = Sigma_A/180.*_PI_;
	Tb = Tb/180.*_PI_;	Sigma_B = Sigma_B/180.*_PI_;
	double *SA, *SB; SA = NULL, SB = NULL;
	SA = config.GetHandleSigmaAlpha(); SB = config.GetHandleSigmaBeta();
	*SA = Sigma_A; *SB = Sigma_B;
	
	if(ver_num >= 1.1)
	{
		RemoveCommentLine(infile, '#');
		//# alpha and beta bias
		//#2 real number [deg]
		double BA, BB; double *pBA, *pBB;
		pBA = config.GetHandleBiasAlpha();
		pBB = config.GetHandleBiasBeta();
		infile>>BA>>BB;
		BA = Deg2Rad(BA); BB = Deg2Rad(BB);
		*pBA = BA; *pBB = BB;
	}
	else
	{
		//# alpha and beta bias
		//#2 real number [deg]
		double *pBA, *pBB;
		pBA = config.GetHandleBiasAlpha();
		pBB = config.GetHandleBiasBeta();
		*pBA = 0.0; *pBB = 0.0;
	}

	if(ver_num >= 1.5)
	{
		RemoveCommentLine(infile, '#');
		//# alpha and beta scales
		//#2 real number [unitless]
		double A_Scale, B_Scale;
		double *pA_Scale, *pB_Scale;
		pA_Scale = config.GetHandleScaleAlpha();
		pB_Scale = config.GetHandleScaleBeta();
		infile>>A_Scale>>B_Scale;
		*pA_Scale = A_Scale;
		*pB_Scale = B_Scale;
	}
	else
	{
		double *pA_Scale, *pB_Scale;
		pA_Scale = config.GetHandleScaleAlpha();
		pB_Scale = config.GetHandleScaleBeta();
		*pA_Scale = 1.0;
		*pB_Scale = 1.0;
	}
	
	RemoveCommentLine(infile, '#');
	//# Laser beam signal interval (unit: sec)
	//# 1 real number
	infile>>signal_interval;

	if(ver_num >= 1.3)
	{
		//# sigma of Laser ranging distance(unit: meter)
		//# 1 real number
		RemoveCommentLine(infile, '#');
		infile>>Sigma_ranging;
	}
	else
	{
		Sigma_ranging = 0;
	}
	double *S_ranging = config.GetHandleSigmaRanging();
	*S_ranging = Sigma_ranging;
	
	RemoveCommentLine(infile, '#');
	//# Bias of laser beam distance
	infile>>Bias_Beam;
	double* BLB = NULL; BLB = config.GetHandleBiasBeam();
	*BLB = Bias_Beam;

	if(ver_num >= 1.4)
	{
		//# scale of laser ranging distance
		//# 1 real number
		RemoveCommentLine(infile, '#');
		infile>>Scale_Beam;
	}
	else
	{
		Scale_Beam = 1.0;
	}
	double *scale_beam = config.GetHandleScaleRanging();
	*scale_beam = Scale_Beam;
	
	RemoveCommentLine(infile, '#');
	//# Laser beam swing period (cycle time): back and forth (unit: sec)
	infile>>cycle;
	
	RemoveCommentLine(infile, '#');
	//# Total scanning time (unit: sec)
	//# start time and end time
	infile>>sTime>>eTime;
	
	if(ver_num >= 1.2)
	{
		//# DEM interpolation
		//# 0: Nearest 1: Bilinear
		RemoveCommentLine(infile, '#');
		infile>>m_DEMInterpolationMethod;
	}
	else
	{
		//# DEM interpolation
		//# 0: Nearest 1: Bilinear
		m_DEMInterpolationMethod = 0;
	}
	
	RemoveCommentLine(infile, '#');
	//# Debugging file tag
	//# 0: No log file 1: simple log file 2: detail log file(not recommended)
	infile>>ErrorModeling;
	if(ErrorModeling==1) bErrorModeling = true; else bErrorModeling = false;
	
	RemoveCommentLine(infile, '#');
	//# Debugging file tag
	//# 0: No debugging file 1: simple debugging file 2: detail debugging file
	infile>>debug_tag;
	
	infile.close();
	
	config.SetData(Offset_Omega, Offset_Phi, Offset_Kappa,
		OffsetX, OffsetY, OffsetZ,
		X0, Y0, Z0,
		a,b,c,
		O0, P0, K0,
		d,e,f,
		Ta, Tb,
		cycle,signal_interval,
		sTime, eTime,
		debug_tag);

	return true;
}

bool CBBARABLiDARSim::InputData(const char* simfile, const char* vertexfile)
{
	if( false == InputData(simfile, 0.0) ) return false;

	PatchList.RemoveAll();
	CLIDARBiasCalibration::ReadPlaneVertex(vertexfile, PatchList, vertex_min, vertex_max);

	return true;
}

bool CBBARABLiDARSim::MakeLiDARSimData_FlatTerrain(const char* fname, double given_Z)
{
	fstream debugfile;//debug file
	debugfile.precision(10);
	debugfile.setf(ios::fixed, ios::floatfield);
				
	int DEBUG_TAG = config.GetDEBUG_TAG();
	
	////////////////////////////////////////////////////////////////
	//
	if((DEBUG_TAG==1)||(DEBUG_TAG==2)) 
	{
		CSMStr debugname = fname;
		debugname += ".TMP";
		
		debugfile.open(debugname.GetChar(),ios::out);
		debugfile<<"Debug File..."<<endl<<endl;
		debugfile.flush();			
	}
	//
	////////////////////////////////////////////////////////////////
	
	//LiDAR data file without error
	CString temp = fname;
	temp.MakeLower();
	temp.Replace(".txt", ".tpt");
	fstream LidarFile;//Lidar point clouds without error
	LidarFile.open(temp,ios::out);
	
	fstream LidarFile_Raw; //Raw Lidar data
	fstream LidarFile_Raw_Error; //Raw Lidar data with error
	if(DEBUG_TAG==3)
	{
		CSMStr fname_Raw = fname;
		fname_Raw += ".raw";
		LidarFile_Raw.open(fname_Raw.GetChar(),ios::out);
		
		LidarFile_Raw<<"#GPS_X(M)\t"<<"GPS_Y(M)\t"<<"GPS_Z(M)\t";
		LidarFile_Raw<<"INS_O(deg)\t"<<"INS_P(deg)\t"<<"INS_K(deg)\t";
		LidarFile_Raw<<"alpha(deg)\t"<<"beta(deg)\t";
		LidarFile_Raw<<"dist(M)\t"<<"time(sec)"<<endl;
		LidarFile_Raw<<"###################################################################";
		LidarFile_Raw<<"###################################################################"<<endl;
		LidarFile_Raw.flush();

		if(bErrorModeling == true)
		{
			CSMStr fname_Raw_Error = fname;
			fname_Raw_Error += "_Error.raw";
			LidarFile_Raw_Error.open(fname_Raw_Error.GetChar(),ios::out);
			
			LidarFile_Raw_Error<<"#GPS_X(M)\t"<<"GPS_Y(M)\t"<<"GPS_Z(M)\t";
			LidarFile_Raw_Error<<"INS_O(deg)\t"<<"INS_P(deg)\t"<<"INS_K(deg)\t";
			LidarFile_Raw_Error<<"alpha(deg)\t"<<"beta(deg)\t";
			LidarFile_Raw_Error<<"dist(M)\t"<<"time(sec)"<<endl;
			LidarFile_Raw_Error<<"###################################################################";
			LidarFile_Raw_Error<<"###################################################################"<<endl;
			LidarFile_Raw_Error.flush();
		}
	}
	
	//LiDAR data file with error
	fstream LidarFile_Error;//Lidar point clouds with error
	fstream LidarFile_Diff;//Differences between without error and with error
	if(bErrorModeling==true)
	{
		CSMStr fname_error = fname;
		fname_error += "_Error.tpt";
		LidarFile_Error.open(fname_error.GetChar(),ios::out);
		CSMStr fname_diff = fname;
		fname_diff += "_Diff.txt";
		LidarFile_Diff.open(fname_diff.GetChar(),ios::out);
	}
	
	////////////////////////////////////////////////////////////////
	//
	if((DEBUG_TAG==1)||(DEBUG_TAG==2))  
	{
		debugfile<<"LiDAR data file:"<<fname<<endl;
		debugfile.flush();
	}
	//
	////////////////////////////////////////////////////////////////

	double sum_dx2=0, sum_dy2=0, sum_dz2=0;
	
	double st = config.GetStartTime();
	double et = config.GetEndTime();
	double gt = config.GetTimeGap();
	double num_p = (et-st)/gt;
	
	//for(double t=config.GetStartTime(); t<config.GetEndTime(); t=t+config.GetTimeGap())
	for(int count=0; count<num_p; count++)
	{
		double t=st+gt*count;

		////////////////////////////////////////////////////////////////
		//
		if((DEBUG_TAG==1)||(DEBUG_TAG==2))  {
			debugfile<<endl<<"[Time:"<<t<<"]"<<endl;
			debugfile.flush();
		}
		//
		////////////////////////////////////////////////////////////////
		
		double X, Y, Z;
		Z = given_Z;
		
		//Time setting
		config.GetOrientation(t);
		
		////////////////////////////////////////////////////////////////
		//
		if((DEBUG_TAG==1)||(DEBUG_TAG==2))  {
			double O, P, K;
			config.GetINSAngle(O,P,K);
			debugfile<<"INS angle:"<<O<<"\t"<<P<<"\t"<<K<<endl;
			double a, b;
			config.GetLBAngle(a,b);
			debugfile<<"Laser beam angle(deg):"<<Rad2Deg(a)<<"\t"<<Rad2Deg(b)<<endl;
			debugfile.flush();
		}
		//
		////////////////////////////////////////////////////////////////
		
		//
		//Without Error
		//
		//To find ground coord(X,Y) using maxZ(given Z)
		FindGroundCoord(config.GetMG(),config.GetT(),X,Y,Z);

		//double temp_X, temp_Y;
		//config.FindGround(temp_X, temp_Y, Z);//���Լ��� ��������. ���� �Լ��� ���� ���.

		
		//Laser unit center
		CSMMatrix<double> L_Unit = config.GetL_Unit();
		
		//debugfile.width(24);
		LidarFile.precision(10);
		LidarFile.setf(ios::fixed, ios::floatfield);
		LidarFile<<setw(10)<<t<<"\t"<<setw(10)<<X<<"\t"<<setw(10)<<Y<<"\t"<<setw(10)<<Z<<endl;
		LidarFile.flush();
		
		double dist;
		double dist_X, dist_Y, dist_Z;
		dist_X = X-L_Unit(0,0); dist_Y = Y-L_Unit(1,0); dist_Z = Z-L_Unit(2,0);
		dist = sqrt(dist_X*dist_X+dist_Y*dist_Y+dist_Z*dist_Z);					

		if(DEBUG_TAG==3)
		{
			LidarFile_Raw.precision(10);
			LidarFile_Raw.setf(ios::fixed, ios::floatfield);
			
			double INS_O, INS_P, INS_K;
			config.GetINSAngle(INS_O,INS_P,INS_K);
			double alpha, beta;
			config.GetLBAngle(alpha,beta);
			double GPS_X, GPS_Y, GPS_Z;
			config.GetGPSPosition(GPS_X,GPS_Y,GPS_Z);
						
			LidarFile_Raw<<setw(10)<<GPS_X<<"\t"<<setw(10)<<GPS_Y<<"\t"<<setw(10)<<GPS_Z<<"\t";
			LidarFile_Raw<<setw(10)<<Rad2Deg(INS_O)<<"\t"<<setw(10)<<Rad2Deg(INS_P)<<"\t"<<setw(10)<<Rad2Deg(INS_K)<<"\t";
			LidarFile_Raw<<setw(10)<<Rad2Deg(alpha)<<"\t"<<setw(10)<<Rad2Deg(beta)<<"\t";
			LidarFile_Raw<<setw(10)<<dist<<"\t"<<setw(10)<<t<<endl;
			LidarFile_Raw.flush();
		}
		
		////////////////////////////////////////////////////////////////
		//
		if(DEBUG_TAG==2)  {
			
			debugfile<<"Laser Unit Position:"<<L_Unit(0,0)<<"\t"<<L_Unit(1,0)<<"\t"<<L_Unit(2,0)<<endl;
			
			debugfile<<"X,Y,Z and distance: "<<X<<"\t"<<Y<<"\t"<<Z<<"\t"<<dist<<endl;
			
			debugfile.flush();
		}
		//
		////////////////////////////////////////////////////////////////
		
		//
		//With Error(Bias)
		//
		if(bErrorModeling == true)
		{
			double EX, EY, EZ;
			CSMMatrix<double> MG_E, T_E;
			double dist_E;
			config.GetOrientationWithError();
			MG_E = config.GetMG_Error();
			T_E = config.GetT_Error();
			
			dist_E = dist;

			dist_E = (dist_E*(*config.GetHandleScaleRanging())) +config.RE_range + (*config.GetHandleBiasBeam());
			//dist_E = dist_E + (*config.GetHandleBiasBeam());
			FindGroundCoord(MG_E, T_E, dist_E, EX, EY, EZ);

			double rad_num = rad_num = config.GetRandNum();
			dist_E += rad_num * config.GetSigmaRanging();

			LidarFile_Error.precision(10);
			LidarFile_Error.setf(ios::fixed, ios::floatfield);
			LidarFile_Error<<setw(10)<<t<<"\t"<<setw(10)<<EX<<"\t"<<setw(10)<<EY<<"\t"<<setw(10)<<EZ<<endl;
			LidarFile_Error.flush();
			
			//differences of coordinates
			LidarFile_Diff.precision(10);
			LidarFile_Diff.setf(ios::fixed, ios::floatfield);
			LidarFile_Diff<<setw(10)<<EX-X<<"\t"<<setw(10)<<EY-Y<<"\t"<<setw(10)<<EZ-Z<<endl;
			LidarFile_Diff.flush();

			sum_dx2 += (EX-X)*(EX-X);
			sum_dy2 += (EY-Y)*(EY-Y);
			sum_dz2 += (EZ-Z)*(EZ-Z);
			
			////////////////////////////////////////////////////////////////
			//
			if((DEBUG_TAG==1)||(DEBUG_TAG==2)) {
				debugfile<<"EX,EY,EZ:"<<"\t"<<EX<<"\t"<<EY<<"\t"<<EZ<<endl;
				debugfile.flush();
			}
			//
			////////////////////////////////////////////////////////////////

			if(DEBUG_TAG==3)
			{
				LidarFile_Raw_Error.precision(10);
				LidarFile_Raw_Error.setf(ios::fixed, ios::floatfield);
				
				double INS_O_Error, INS_P_Error, INS_K_Error;
				config.GetINSErrorAngle(INS_O_Error,INS_P_Error,INS_K_Error);
				double alpha_Error, beta_Error;
				config.GetLBErrorAngle(alpha_Error,beta_Error);
				double GPS_X_Error, GPS_Y_Error, GPS_Z_Error;
				config.GetGPSErrorPosition(GPS_X_Error,GPS_Y_Error,GPS_Z_Error);
				double dist_Error = dist_E;
				
				LidarFile_Raw_Error<<setw(10)<<GPS_X_Error<<"\t"<<setw(10)<<GPS_Y_Error<<"\t"<<setw(10)<<GPS_Z_Error<<"\t";
				LidarFile_Raw_Error<<setw(10)<<Rad2Deg(INS_O_Error)<<"\t"<<setw(10)<<Rad2Deg(INS_P_Error)<<"\t"<<setw(10)<<Rad2Deg(INS_K_Error)<<"\t";
				LidarFile_Raw_Error<<setw(10)<<Rad2Deg(alpha_Error)<<"\t"<<setw(10)<<Rad2Deg(beta_Error)<<"\t";
				LidarFile_Raw_Error<<setw(10)<<dist_Error<<"\t"<<setw(10)<<t<<endl;
				LidarFile_Raw_Error.flush();
			}
			
		}	
	}
	
	//Lidar file
	LidarFile.close();
	
	//Footprint image and Lidar file with error save
	if(bErrorModeling==true)
	{
		LidarFile_Error.close();

		LidarFile_Diff<<"[STDEV]"<<endl;
		LidarFile_Diff<<"sx: "<<sqrt(sum_dx2/num_p)<<endl;
		LidarFile_Diff<<"sy: "<<sqrt(sum_dy2/num_p)<<endl;
		LidarFile_Diff<<"sz: "<<sqrt(sum_dz2/num_p)<<endl;

		LidarFile_Diff.close();
	}
	
	if(DEBUG_TAG==3)
	{
		LidarFile_Raw.close();
		
		if(bErrorModeling == true)
		{
			LidarFile_Raw_Error.close();
		}
	}
	
	////////////////////////////////////////////////////////////////
	//
	if((DEBUG_TAG==1)||(DEBUG_TAG==2)) {		
		debugfile.close();
	}
	//
	////////////////////////////////////////////////////////////////
	
	return true;
}

void CBBARABLiDARSim::DEMSimulation(CString vertexfile, CString outfile) 
{
	//Read vertex data
	CSMList<CLIDARPlanarPatch> vertex;
	CLIDARBiasCalibration::ReadPlaneVertex(vertexfile,vertex, vertex_min, vertex_max);
	
	fstream testdata;
	testdata.precision(10);
	testdata.setf(ios::fixed, ios::floatfield);
	
	fstream testdata_hdr;
	testdata_hdr.precision(10);
	testdata_hdr.setf(ios::fixed, ios::floatfield);
	
	fstream testdataconfig;
	testdataconfig.precision(10);
	testdataconfig.setf(ios::fixed, ios::floatfield);
	
	CString name = outfile;
	
	CString name_config = outfile;
	name_config.MakeUpper();
	name_config.Replace(".TXT","_CONFIG.TXT");
	
	CString name_hdr = name;
	name_hdr.MakeUpper();
	name_hdr.Replace(".TXT",".HDR");
	
	testdata.open(name,ios::out);
	testdata_hdr.open(name_hdr,ios::out);
	testdataconfig.open(name_config,ios::out);
	
	CSMMatrix<double> A, L, X;
	A.Resize(4,3);
	L.Resize(4,1);
	
	////////////////////
	//AX+BY+CZ=1
	///////////////////
	
	double *a, *b, *c;
	if(vertex.GetNumItem() < 1)
		return;

	a = new double[vertex.GetNumItem()];
	b = new double[vertex.GetNumItem()];
	c = new double[vertex.GetNumItem()];
	
	double* AREA;
	AREA = new double[vertex.GetNumItem()];
	
	double max_X=-1.0e+100, max_Y=-1.0e+100, max_Z=-1.0e+100;
	double min_X=1.0e+100, min_Y=1.0e+100, min_Z=1.0e+100;
	
	testdataconfig<<"aX + bY + cZ + 1 = 0"<<endl;
	
	for(int j=0; j<(int)vertex.GetNumItem(); j++)
	{
		CLIDARPlanarPatch patch = vertex.GetAt(j);
		
		for(int i=0; i<3; i++)
		{
			A(i,0) = patch.Vertex[i*3+0];
			A(i,1) = patch.Vertex[i*3+1];
			A(i,2) = patch.Vertex[i*3+2];
			
			L(i,0) = -1;
			
			if(max_X<patch.Vertex[i*3+0]) max_X = patch.Vertex[i*3+0];
			if(max_Y<patch.Vertex[i*3+1]) max_Y = patch.Vertex[i*3+1];
			if(max_Z<patch.Vertex[i*3+2]) max_Z = patch.Vertex[i*3+2];
			
			if(min_X>patch.Vertex[i*3+0]) min_X = patch.Vertex[i*3+0];
			if(min_Y>patch.Vertex[i*3+1]) min_Y = patch.Vertex[i*3+1];
			if(min_Z>patch.Vertex[i*3+2]) min_Z = patch.Vertex[i*3+2];
		}
		
		AREA[j] = CLIDARBiasCalibration::CalTriangleArea(patch.Vertex[0],patch.Vertex[1], patch.Vertex[2],
			patch.Vertex[3],patch.Vertex[4], patch.Vertex[5],
			patch.Vertex[6],patch.Vertex[7], patch.Vertex[8]);
		
		X = (A.Transpose()%A).Inverse()%A.Transpose()%L;
		a[j] = X(0,0);
		b[j] = X(1,0);
		c[j] = X(2,0);
		
		testdataconfig.precision(10);
		testdataconfig.setf(ios::fixed, ios::floatfield);
		testdataconfig<<setw(10)<<a[j]<<"\t"<<b[j]<<"\t"<<c[j]<<"\t"<<endl;
		testdataconfig.flush();
	}
	
	testdataconfig.precision(10);
	testdataconfig.setf(ios::fixed, ios::floatfield);
	testdataconfig<<"max_X, max_Y, max_Z"<<endl;
	testdataconfig<<setw(10)<<max_X<<"\t"<<max_Y<<"\t"<<max_Z<<"\t"<<endl;
	testdataconfig<<"min_X, min_Y, min_Z"<<endl;
	testdataconfig<<setw(10)<<min_X<<"\t"<<min_Y<<"\t"<<min_Z<<"\t"<<endl;
	testdataconfig.flush();
	
	testdataconfig.close();
	
	//DEM Generation
	double sX = min_X, sY = max_Y;
	double resolution = 0.1;
	double W = 0;
	double H = 0;
	double x, y, z;
	for(y=max_Y; y>min_Y; y=y-resolution)
	{
		H++;
		W=0;
		for(x=min_X; x<max_X; x=x+resolution)
		{
			W++;
			bool available = false;
			
			for(int k=0; k<(int)vertex.GetNumItem(); k++)
			{
				CLIDARPlanarPatch patch = vertex.GetAt(k);
				
				z=(-a[k]*x-b[k]*y-1)/c[k];
				
				double are1, are2, are3;
				are1 = CLIDARBiasCalibration::CalTriangleArea(x,y,z,patch.Vertex[3],patch.Vertex[4],patch.Vertex[5],patch.Vertex[6],patch.Vertex[7],patch.Vertex[8]);
				are2 = CLIDARBiasCalibration::CalTriangleArea(x,y,z,patch.Vertex[0],patch.Vertex[1],patch.Vertex[2],patch.Vertex[6],patch.Vertex[7],patch.Vertex[8]);
				are3 = CLIDARBiasCalibration::CalTriangleArea(x,y,z,patch.Vertex[3],patch.Vertex[4],patch.Vertex[5],patch.Vertex[0],patch.Vertex[1],patch.Vertex[2]);
				
				double error_val = 0.00001;
				//if(((are1/AREA[k] + are2/AREA[k] + are3/AREA[k])<1.0)&&(are1/AREA[k] + are2/AREA[k] + are3/AREA[k])>0.99999)
				if(((are1/AREA[k] + are2/AREA[k] + are3/AREA[k])<=(1+error_val))&&are1>error_val&&are2>error_val&&are3>error_val)
				{
					available = true;
				}
			}
			
			if(available != true)
			{
				z = 0.;
			}
			
			testdata.precision(10);
			testdata.setf(ios::fixed, ios::floatfield);
			testdata<<setw(10)<<x<<"\t"<<y<<"\t"<<z<<endl;
			testdata.flush();
		}
	}
	
	testdata_hdr.precision(10);
	testdata_hdr.setf(ios::fixed, ios::floatfield);
	testdata_hdr<<setw(10)<<(int)W<<"\t"<<(int)H<<"\t"<<resolution<<"\t";
	testdata_hdr<<setw(10)<<sX<<"\t"<<sY<<endl;
	testdata_hdr.flush();
	
	testdata_hdr.close();
	testdata.close();

	delete[] a;
	delete[] b;
	delete[] c;
	delete[] AREA;
}

bool CBBARABLiDARSim::SearchingInterMediatePoint(double DEM_Z, double old_Z, double &X,  double &Y, double &Z, DWORD &indexX, DWORD &indexY, fstream &debugfile, int DEBUG_TAG)
{
	double over_Z = old_Z;
	double below_Z = Z;
	double half_Z;
	int sub_loop_index = 0;
	do 
	{
		sub_loop_index ++;
		//Loop index check
		if(sub_loop_index > _MAX_LOOP_) 
		{
			////////////////////////////////////////////////////////////////
			//
			if((DEBUG_TAG==1)||(DEBUG_TAG==2)) {
				debugfile<<endl<<"ERROR_MAX_LOOP_:"<<sub_loop_index<<endl;
				debugfile.flush();
			}
			//
			////////////////////////////////////////////////////////////////
			break;
		}
		
		half_Z = (over_Z + below_Z)/2.;
		//To find ground coord(X,Y) using maxZ(given Z)
		FindGroundCoord(config.GetMG(),config.GetT(),X,Y,half_Z);
		////////////////////////////////////////////////////////////////
		//
		if(DEBUG_TAG==2) {
			debugfile<<"over_Z, below_Z, half_Z:"<<over_Z<<"\t"<<below_Z<<"\t"<<half_Z<<endl;
			debugfile.flush();
		}
		//
		////////////////////////////////////////////////////////////////
		if(m_DEMInterpolationMethod == 0)
		{
			if(true != m_RefDEM.GetNearestZ(X,Y,indexX, indexY, DEM_Z))
			{
				return false;
			}
		}
		else if(m_DEMInterpolationMethod == 1)
		{
			if(true != m_RefDEM.GetBilinearZ(X,Y,indexX, indexY, DEM_Z))
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
		double diff_Z = DEM_Z - half_Z;

		if(fabs(diff_Z)<LIDAR_PRECISION)
		{
			Z = half_Z;
			break;
		}
		else
		{
			if(diff_Z>0)
				below_Z = half_Z;
			else
				over_Z = half_Z;
		}
		
	} while(1);
	
	return true;
}

void CBBARABLiDARSim::FindGroundCoord(CSMMatrix<double> MG, CSMMatrix<double> T, double dist, double &Xa, double &Ya, double &Za)
{
	//MG*G - T = LB
	//G = RG(LB + T)
	CSMMatrix<double> G;
	CSMMatrix<double> LB(3,1); LB(0,0) = 0; LB(1,0) = 0; LB(2,0) = -dist;
	G = MG.Transpose()%(LB + T);
	Xa = G(0,0);
	Ya = G(1,0);
	Za = G(2,0);
}

void CBBARABLiDARSim::FindGroundCoord(CSMMatrix<double> MG, CSMMatrix<double> T, double &Xa, double &Ya, double Za)
{
	//|MG11 MG12| |Xa|=|-MG13*Za + T11|
	//|MG21 MG22| |Ya|=|-MG23*Za + T21|
	CSMMatrix<double> A = MG.Subset(0,0,2,2);
	CSMMatrix<double> L(2,1);
	L(0,0) = -MG(0,2)*Za + T(0,0);
	L(1,0) = -MG(1,2)*Za + T(1,0);
	
	CSMMatrix<double> X;
	X = A.Inverse()%L;
	Xa = X(0,0);
	Ya = X(1,0);
}

double CBBARABLiDARSim::FindGroundCoord(CSMMatrix<double> MG, CSMMatrix<double> T, double Xa, double Ya)
{
	double Za;
	if(MG(0,2) !=0 ) Za = (-MG(0,0)*Xa - MG(0,1)*Ya + T(0,0))/MG(0,2);
	else if(MG(1,2) !=0 ) Za = (-MG(1,0)*Xa - MG(1,1)*Ya + T(1,0))/MG(1,2);
	else Za = -1.0;
	
	return Za;
}

bool CBBARABLiDARSim::ReadTrajectoryData(CString trjpath)
{
	
	fstream trjfile;

	trjfile.open(trjpath, ios::in);

	//get number of records
	if(!trjfile) return false;
	char line[512];
	int count=0;

	while(!trjfile.eof())
	{
		trjfile.getline(line, 512);		
		trjfile>>ws;
		count ++;
	}

	trjfile.clear();
	trjfile.seekg(0L,ios::beg);

	trjfile.close();

	//Set time data to KDTree
	config.KDTrjTime.InitialSetup(count, 1, 0);
	double* x = new double[1];

	trjfile.open(trjpath, ios::in);
	if(!trjfile) return false;


	TrjData record;
	while(!trjfile.eof())
	{
		trjfile>>record.time>>record.X>>record.Y>>record.Z>>record.O>>record.P>>record.K;
		record.O = record.O/180.0*acos(-1.0);//rad
		record.P = record.P/180.0*acos(-1.0);//rad
		record.K = record.K/180.0*acos(-1.0);//rad

		x[0] = record.time;
		config.KDTrjTime.add(x);

		config.TrjRecord.AddTail(record);

		trjfile>>ws;
	}

	delete[] x;

	trjfile.close();
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
CLiDARSimConfig::CLiDARSimConfig()
{
	/* Seed the random-number generator with current time so that
	* the numbers will be different every time we run.
    */
	srand( (unsigned)time( NULL ) );


	//Find average of rand function and stdev
	double sum=0;
	int num_data = 10000000;
	double *data = new double[num_data];
	
	double min=1.0, max=-1.0;

	for(int i=0; i<num_data; i++)
	{
		double half_RAND_MAX = (double)RAND_MAX/2.;
		double rad_num = (double)rand();//0 ~ RAND_MAX
		rad_num = rad_num/half_RAND_MAX - 1.0;//-1~1

		data[i] = rad_num;
		sum += rad_num;

		if(min>data[i]) min = data[i];
		if(max<data[i]) max = data[i];
	}
	
	rand_ave = sum/num_data;//rand average
	
	double sum2=0;
	for(int i=0; i<num_data; i++)
	{
		double diff = data[i]-rand_ave;
		sum2 += diff*diff;
	}
	
	rand_sd = sqrt(sum2/(num_data-1));//rand stdev

	delete[] data;

	///////////////////////////////////////////////////////////
		
	/*PI*/ 
	_PI_ = asin(1)*2;
}
		
CLiDARSimConfig::~CLiDARSimConfig()	{}
		
void CLiDARSimConfig::SetData(double dOmega, double dPhi, double dKappa,
			double OffsetX, double OffsetY, double OffsetZ,
			double startX, double startY, double startZ,
			double *a, double *b, double *c,
			double startO, double startP, double startK,
			double *d, double *e, double *f,
			double TA, double TB,
			double bf_time, double interval_time,
			double stime, double etime,
			int debug_tag)
{
	dO = dOmega; dP = dPhi; dK = dKappa;
	dX = OffsetX; dY = OffsetY; dZ = OffsetZ;
	sX = startX; sY = startY; sZ = startZ;
	A1 = a[0]; A2 = a[1]; A3 = a[2];
	B1 = b[0]; B2 = b[1]; B3 = b[2];
	C1 = c[0]; C2 = c[1]; C3 = c[2];
	sO = startO; sP = startP; sK = startK;
	D1 = d[0]; D2 = d[1]; D3 = d[2];
	E1 = e[0]; E2 = e[1]; E3 = e[2];
	F1 = f[0]; F2 = f[1]; F3 = f[2];
	
	Total_a = TA; Total_b = TB;
	BFTime = bf_time;
	TimeGap = interval_time;
	StartTime = stime;
	EndTime = etime;
	m_DEBUG_TAG = debug_tag;
	
	OffsetVector.Resize(3,1);
	OffsetVector(0,0) = dX;
	OffsetVector(1,0) = dY;
	OffsetVector(2,0) = dZ;
	
	OffsetVector_Error.Resize(3,1);
	OffsetVector_Error(0,0) = dX + bias_OffsetVector[0];
	OffsetVector_Error(1,0) = dY + bias_OffsetVector[1];
	OffsetVector_Error(2,0) = dZ + bias_OffsetVector[2];
	
	a2 = tan(Total_a)*tan(Total_a);//a^2 = tan(Total_a)^2(Ÿ�� ù��° �ݰ�)
	b2 = tan(Total_b)*tan(Total_b);//b^2 = tan(Total_b)^2(Ÿ�� �ι�° �ݰ�)
	
	_Rmat_ R(dO, dP, dK);
	R_LU = R.Rmatrix;
	
	_Rmat_ R_Error(dO + bias_R_LU[0], dP + bias_R_LU[1], dK + bias_R_LU[2]);
	R_LU_Error = R_Error.Rmatrix;
}

void CLiDARSimConfig::GetOrientation(double t)
{
	//G=GPS+R_INS*Offset+R_INS*R_LU*R_LB*LB
	//G - GPS - R_INS*Offset = R_INS*R_LU*R_LB*LB
	//(M_LB*M_LU*M_INS)(G - GPS - R_INS*Offset) = LB
	//(M_LB*M_LU*M_INS)(G - GPS) - (M_LB*M_LU*M_INS)R_INS*Offset = LB
	//(ML*M_INS)(G - GPS) - (M_LB*M_LU)*Offset = LB
	//MG(G - GPS) - ML*Offset = LB
	//MG*G - (MG*GPS + ML*Offset) = LB
	//MG*G - T = LB
	//|MG11 MG12| |Xa|=|-MG13*Za + T11|
	//|MG21 MG22| |Ya|=|-MG13*Za + T11|
	
	double GPSX, GPSY, GPSZ;
	
	if(trj_option == 1)
	{
		SetTime_II(t, GPSX, GPSY, GPSZ, INSO, INSP, INSK, alpha, beta);		
	}
	else
	{
		SetTime(t, GPSX, GPSY, GPSZ, INSO, INSP, INSK, alpha, beta);	
	}
	
	//GPS phase center
	GPS.Resize(3,1);
	GPS(0,0) = GPSX; 
	GPS(1,0) = GPSY; 
	GPS(2,0) = GPSZ;
	
	//Rotation of INS
	_Rmat_ R(INSO, INSP, INSK);
	R_INS = R.Rmatrix;
	
	//Laser unit optical center
	L_Unit = GPS + R_INS%OffsetVector;
	
	//Rotation of laser beam angle
	R.ReMake(alpha,beta,0);
	R_LB = R.Rmatrix;
	
	//
	//ML,MG and T
	//
	//ML = M_LB*M_LU
	ML = R_LB.Transpose()%R_LU.Transpose();
	//MG = ML*M_INS
	MG = ML%R_INS.Transpose();
	//T = MG*GPS + ML*OffsetVector
	T = MG%GPS + ML%OffsetVector;
}

void CLiDARSimConfig::FindGround(double &X, double &Y, double Z)
{
	CSMMatrix<double> GPS_Leverarm = GPS + R_INS%OffsetVector;
	CSMMatrix<double> LaserR = R_INS%R_LU%R_LB;
	CSMMatrix<double> Range(3,1, 0.0);
	Range(2,0) = (Z - GPS_Leverarm(2,0))/(LaserR(2,2));//(-dist)
	CSMMatrix<double> G = GPS_Leverarm + LaserR%Range;

	X = G(0,0);
	Y = G(1,0);
}

void CLiDARSimConfig::GetOrientationWithError()
{
	//G=GPS+R_INS*Offset+R_INS*R_LU*R_LB*LB
	//G - GPS - R_INS*Offset = R_INS*R_LU*R_LB*LB
	//(M_LB*M_LU*M_INS)(G - GPS - R_INS*Offset) = LB
	//(M_LB*M_LU*M_INS)(G - GPS) - (M_LB*M_LU*M_INS)R_INS*Offset = LB
	//(ML*M_INS)(G - GPS) - (M_LB*M_LU)*Offset = LB
	//MG(G - GPS) - ML*Offset = LB
	//MG*G - (MG*GPS + ML*Offset) = LB
	//MG*G - T = LB
	//|MG11 MG12| |Xa|=|-MG13*Za + T11|
	//|MG21 MG22| |Ya|=|-MG13*Za + T11|
	
	//Rotation of laser beam angle with random error
	double half_RAND_MAX = (double)RAND_MAX/2.;
	
	double rad_num = GetRandNum();
	double RE_alpha = rad_num * sigma_alpha;
	
	rad_num = GetRandNum();
	double RE_beta = rad_num * sigma_beta;

	alpha_error = alpha*scale_alpha + bias_alpha + RE_alpha;
	beta_error = beta*scale_beta + bias_beta + RE_beta;
	_Rmat_ R(alpha_error, beta_error, 0);
	R_LB_Error = R.Rmatrix;

	//Rotation of INS with random error
	rad_num = GetRandNum();
	double RE_O = rad_num * sigma_INS[0];

	rad_num = GetRandNum();
	double RE_P = rad_num * sigma_INS[1];
	
	rad_num = GetRandNum();
	double RE_K = rad_num * sigma_INS[2];

	INSO_Error = INSO+RE_O;
	INSP_Error = INSP+RE_P;
	INSK_Error = INSK+RE_K;

	R.ReMake(INSO_Error, INSP_Error, INSK_Error);
	R_INS_Error = R.Rmatrix;

	//GPS phase center with random error
	rad_num = GetRandNum();
	double RE_X = rad_num * sigma_GPS[0];
	rad_num = GetRandNum();
	double RE_Y = rad_num * sigma_GPS[1];
	rad_num = GetRandNum();
	double RE_Z = rad_num * sigma_GPS[2];

	GPS_Error.Resize(3,1);
	GPS_Error(0,0) = GPS(0,0)+RE_X;
	GPS_Error(1,0) = GPS(1,0)+RE_Y;
	GPS_Error(2,0) = GPS(2,0)+RE_Z;

	//Range random error
	rad_num = GetRandNum();
	RE_range = rad_num * sigma_ranging;
	
	//
	//ML,MG and T
	//
	//ML = M_LB*M_LU
	ML_Error = R_LB_Error.Transpose()%R_LU_Error.Transpose();
	//MG = ML*M_INS
	MG_Error = ML_Error%R_INS_Error.Transpose();
	//T = MG*GPS + ML*OffsetVector
	T_Error = MG_Error%GPS_Error + ML_Error%OffsetVector_Error;
}

void CLiDARSimConfig::SetTime(double t, double &GPSX, double &GPSY, double &GPSZ, double &INSO, double &INSP, double &INSK, double &alpha, double &beta)
{
	//////////////////////////////////////
	//GPS setting
	//
	GPSX = sX + A1*t + A2*t*t + A3*t*t*t;
	GPSY = sY + B1*t + B2*t*t + B3*t*t*t;
	GPSZ = sZ + C1*t + C2*t*t + C3*t*t*t;
	
	////////////////////////////////////
	//INS setting
	//
	INSO = sO + D1*t + D2*t*t + D3*t*t*t;
	INSP = sP + E1*t + E2*t*t + E3*t*t*t;
	INSK = sK + F1*t + F2*t*t + F3*t*t*t;
	
	////////////////////////////////////////
	//Rotation of laser beam angle setting
	//
	int n = int(t/(BFTime));
	double dn = t/(BFTime) - (double)n;
	
	//PI ����
	//double _PI_ = asin(1)*2;
	//
	
	//ellipse
	//
	double theta;
	if((Total_a != 0)&&(Total_b != 0))
	{
		theta = 2.*_PI_*dn; //0<theta<2*_PI_
		double _cos = cos(theta);//-1<_cos<1
		double _sin = sin(theta);//-1<_sin<1
		double _cos2=_cos*_cos;
		double _sin2=_sin*_sin;
		//double a2 = tan(Total_a)*tan(Total_a);//a^2 = tan(Total_a)^2(Ÿ�� ù��° �ݰ�)
		//double b2 = tan(Total_b)*tan(Total_b);//b^2 = tan(Total_b)^2(Ÿ�� �ι�° �ݰ�)
		double r = sqrt(a2*b2/(b2*_sin2+a2*_cos2));//�������� Ÿ������ ������ ������ �Ÿ� (�ð� dn �϶�)
		double _x = r*_cos;
		double _y = r*_sin;
		double h = sqrt(1 + _y*_y);
		alpha = atan2(_y,1);
		beta = atan2(_x,h);
	}
	else
	{
		//no rotation
		if((Total_a == 0)&&(Total_b == 0))
		{
			alpha = 0; beta = 0;
		}
		//line scanning 1(Y axis)
		else if(Total_a == 0)
		{
			theta = 2.*_PI_*dn;
			alpha = 0;
			beta = (-Total_b)*(1. - 2.*fabs((theta-_PI_)/_PI_));
		}
		//line scanning 2(X axis)
		else if(Total_b == 0) 
		{
			theta = 2.*_PI_*dn;
			beta = 0;
			alpha = (Total_a)*(1. - 2.*fabs((theta-_PI_)/_PI_));
		}
	}
}

void CLiDARSimConfig::SetTime_II(double t, double &GPSX, double &GPSY, double &GPSZ, double &INSO, double &INSP, double &INSK, double &alpha, double &beta)
{
	double x[1];
	x[0] = t;
	KDNode *node = KDTrjTime.find_nearest(x);
	TrjData record = TrjRecord.GetAt(node->GetID());

	//////////////////////////////////////
	//GPS setting
	//
	GPSX = record.X;
	GPSY = record.Y;
	GPSZ = record.Z;
	
	////////////////////////////////////
	//INS setting
	//
	INSO = record.O;
	INSP = record.P;
	INSK = record.K;
	
	////////////////////////////////////////
	//Rotation of laser beam angle setting
	//
	int n = int(t/(BFTime));
	double dn = t/(BFTime) - (double)n;
	
	//PI ����
	//double _PI_ = asin(1)*2;
	//
	
	//ellipse
	//
	double theta;
	if((Total_a != 0)&&(Total_b != 0))
	{
		theta = 2.*_PI_*dn; //0<theta<2*_PI_
		double _cos = cos(theta);//-1<_cos<1
		double _sin = sin(theta);//-1<_sin<1
		double _cos2=_cos*_cos;
		double _sin2=_sin*_sin;
		//double a2 = tan(Total_a)*tan(Total_a);//a^2 = tan(Total_a)^2(Ÿ�� ù��° �ݰ�)
		//double b2 = tan(Total_b)*tan(Total_b);//b^2 = tan(Total_b)^2(Ÿ�� �ι�° �ݰ�)
		double r = sqrt(a2*b2/(b2*_sin2+a2*_cos2));//�������� Ÿ������ ������ ������ �Ÿ� (�ð� dn �϶�)
		double _x = r*_cos;
		double _y = r*_sin;
		double h = sqrt(1 + _y*_y);
		alpha = atan2(_y,1);
		beta = atan2(_x,h);
	}
	else
	{
		//no rotation
		if((Total_a == 0)&&(Total_b == 0))
		{
			alpha = 0; beta = 0;
		}
		//line scanning 1(Y axis)
		else if(Total_a == 0)
		{
			theta = 2.*_PI_*dn;
			alpha = 0;
			beta = (-Total_b)*(1. - 2.*fabs((theta-_PI_)/_PI_));
		}
		//line scanning 2(X axis)
		else if(Total_b == 0) 
		{
			theta = 2.*_PI_*dn;
			beta = 0;
			alpha = (Total_a)*(1. - 2.*fabs((theta-_PI_)/_PI_));
		}
	}
}