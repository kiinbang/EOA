// SMFrameDLTBundle.cpp: implementation of the CParallel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SMFrameDLTBundle.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define PI 3.141592654

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMFrameDLTBA::CSMFrameDLTBA()
{
	Init();
}

CSMFrameDLTBA::~CSMFrameDLTBA()
{

}

CString CSMFrameDLTBA::DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize)
{
	unsigned int num_param = 11;

	CString retval;

	retval = "\r\n\r\n[Frame DLT Modeling]\r\n";
	retval += "row = (A1X + A2Y + A3Z + A4)/(A9X + A10Y + A11Z + 1)\r\n";
	retval += "col = (A5X + A6Y + A7Z + A8)/(A9X + A10Y + A11Z + 1)\r\n";
		
	if(bNormalized == true) retval += "(***Data is normalized***)\r\n";
	
	/**
	*Calculate parameter approximation
	*/
	retval += "\r\n\r\n[Init_value of Parameters]\r\n";
	retval += "///////////////////////////////////////////////////////////////////\r\n";
	///////////////////////////////
	//Original Data backup
	///////////////////////////////
	retval += DoInitModeling(num_param);
	///////////////////////////////////////
	//Initial data copy for the iteration
	///////////////////////////////////////
	twin_SMData = SMData;
	///////////////////////////////////////
	retval += "///////////////////////////////////////////////////////////////////\r\n";

	FileOut(outfilename,retval);
	
	retval += Run(num_param, num_max_iter, maximum_sd, maximum_diff, outfilename, pixelsize);
	
	::Beep(500,50);
	return retval;
}

CString CSMFrameDLTBA::DoInitModeling(unsigned int num_param)
{
	CString retval;

	retval="[Frame DLT Initial Parameters]\r\n";
	
	
	/*********************************
	* Data Normalization
	*********************************/
	
	if(bNormalized == true) 
	{
		retval += "(***Data is normalized***)\r\n";
		DataNormalization();	
	}
	else 
	{		
		for(unsigned int i=0; i<SMData.SCENE_List.GetNumItem(); i++)
		{
			SMParam temp;
			param_list.AddTail(temp);
		}
	} 

	unsigned int SCENE_Num = SMData.SCENE_List.GetNumItem();

	CString stParam;
	CSMList<_GCP_> &gcp = SMData.GCP_List;
	unsigned int GCP_Num = gcp.GetNumItem();
		
	Matrix<double> Amat, Lmat, Wmat, Xmat;
	Amat.Resize(0,0,0.);
	Lmat.Resize(0,0,0.);
	Wmat.Resize(0,0,0.);
	
	for(unsigned int k=0; k<SCENE_Num; k++)
	{
		_SCENE_ scene = SMData.SCENE_List[k];
		PointOBSInit(SMData.GCP_List, scene, num_param, Amat, Lmat, Wmat, k);
		CLeastSquare LS;
		Xmat = LS.RunLeastSquare_Fast(Amat,Lmat,Wmat);
	}
	for(k=0; k<SCENE_Num; k++)
	{
		for(unsigned int i=0; i<num_param; i++)
		{
			param_list[k].A[i]=Xmat(k*num_param+i,0);
			stParam.Format("[A%d]%le\t",i,Xmat(k*num_param+i,0));
			retval += stParam;
		}
		stParam.Format("\r\n");
		retval += stParam;
	}
	
	return retval;
}

/**
*Calculating initial value (Fo) using approximate parameters.
*/
double CSMFrameDLTBA::Func_row(_GCP_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double row = (param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.);
	return row;
}
double CSMFrameDLTBA::Func_row(_GCL_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double row = (param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.);
	return row;
}
/**
*Calculating initial value (Go) using approximate parameters.
*/
double CSMFrameDLTBA::Func_col(_GCP_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double col = (param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.);
	return col;
}
double CSMFrameDLTBA::Func_col(_GCL_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double col = (param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.);
	return col;
}

void CSMFrameDLTBA::Cal_PDs_linear(_GCP_ G, _ICP_ I, double* ret1, double* ret2, unsigned int image_index)
{
	SMParam param = param_list[image_index];

	//row = (A1X + A2Y + A3Z + A4) - row*(A9X + A10Y + A11Z)
	//col = (A5X + A6Y + A7Z + A8) - col*(A9X + A10Y + A11Z)
	
	//A1~A4
	ret1[0] = G.X;
	ret1[1] = G.Y;
	ret1[2] = G.Z;
	ret1[3] = 1.;
	//A5~A8
	ret1[4] = 0.;
	ret1[5] = 0.;
	ret1[6] = 0.;
	ret1[7] = 0.;
	//A9~A11
	ret1[8]  = -I.row*G.X;
	ret1[9]  = -I.row*G.Y;
	ret1[10] = -I.row*G.Z;
	//X,Y,Z(row)
	ret1[11] = param.A[0]-I.row*param.A[8];
	ret1[12] = param.A[1]-I.row*param.A[9];
	ret1[13] = param.A[2]-I.row*param.A[10];

	//A1~A4
	ret2[0] = 0.;
	ret2[1] = 0.;
	ret2[2] = 0.;
	ret2[3] = 0.;
	//A5~A8
	ret2[4] = G.X;
	ret2[5] = G.Y;
	ret2[6] = G.Z;
	ret2[7] = 1.;
	//A9~A11
	ret2[8]  = -I.col*G.X;
	ret2[9]  = -I.col*G.Y;
	ret2[10] = -I.col*G.Z;
	//X,Y,Z(col)
	ret2[11] = param.A[4]-I.col*param.A[8];
	ret2[12] = param.A[5]-I.col*param.A[9];
	ret2[13] = param.A[6]-I.col*param.A[10];
}

void CSMFrameDLTBA::Cal_PDs(_GCP_ G, double* ret1, double* ret2, unsigned int index)
{
	SMParam param = param_list[index];

	//      (A1X + A2Y + A3Z + A4)
	//row = ----------------------
	//      (A9X + A10Y + A11Z + 1)
	//
	//      (A5X + A6Y + A7Z + A8)
	//col = ----------------------
	//      (A9X + A10Y + A11Z + 1)

	double row_num = param.A[0]*G.X + param.A[1]*G.Y + param.A[2 ]*G.Z + param.A[3];
	double den     = param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.0;

	//ROW
	//A1~A4
	ret1[0] = G.X/den;
	ret1[1] = G.Y/den;
	ret1[2] = G.Z/den;
	ret1[3] = 1.0/den;
	//A5~A8
	ret1[4] = 0.;
	ret1[5] = 0.;
	ret1[6] = 0.;
	ret1[7] = 0.;
	//A9~A11
	double dd = den*den;
	double n1dd = row_num/dd;
	ret1[8]  = -G.X*n1dd;
	ret1[9]  = -G.Y*n1dd;
	ret1[10] = -G.Z*n1dd;
	//X,Y,Z
	ret1[11] = (param.A[0]*den - param.A[8 ]*row_num)/dd;
	ret1[12] = (param.A[1]*den - param.A[9 ]*row_num)/dd;
	ret1[13] = (param.A[2]*den - param.A[10]*row_num)/dd;

	double col_num = param.A[4]*G.X + param.A[5]*G.Y + param.A[6 ]*G.Z + param.A[7];
	//COL
	//A1~A4
	ret2[0] = 0.;
	ret2[1] = 0.;
	ret2[2] = 0.;
	ret2[3] = 0.;
	//A5~A8
	ret2[4] = G.X/den;
	ret2[5] = G.Y/den;
	ret2[6] = G.Z/den;
	ret2[7] = 1.0/den;
	//A9~A11
	double n2dd = col_num/dd;
	ret2[8]  = -G.X*n2dd;
	ret2[9]  = -G.Y*n2dd;
	ret2[10] = -G.Z*n2dd;
	//X,Y,Z
	ret2[11] = (param.A[4]*den - param.A[8 ]*col_num)/dd;
	ret2[12] = (param.A[5]*den - param.A[9 ]*col_num)/dd;
	ret2[13] = (param.A[6]*den - param.A[10]*col_num)/dd;

}
void CSMFrameDLTBA::Cal_PDs_Line_linear(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int image_index, unsigned int num_param)
{
	SMParam param = param_list[image_index];

	_ICP_ icp1, icp2;
	_GCP_ gcp1, gcp2;
	
	gcp1.X = GL1.X;
	gcp1.Y = GL1.Y;
	gcp1.Z = GL1.Z;
	
	gcp2.X = GL2.X;
	gcp2.Y = GL2.Y;
	gcp2.Z = GL2.Z;
	
	icp1.col = Func_col(gcp1, image_index);
	icp1.row = Func_row(gcp1, image_index);
	
	icp2.col = Func_col(gcp2, image_index);
	icp2.row = Func_row(gcp2, image_index);
	
	double d_row1[14], d_col1[14], d_row2[14], d_col2[14];
	Cal_PDs_linear(gcp1,icp1,d_row1,d_col1,image_index);
	Cal_PDs_linear(gcp2,icp2,d_row2,d_col2,image_index);
	//
	//FL=(row - row1)(col2 - col1) - (col - col1)(row2 - row1)
	//FL=(u - u1)(v2 - v1) - (v - v1)(u2 - u1)
	//
	//A1~A4
	double v_v2 = IL.col - icp2.col;
	double v1_v = icp1.col - IL.col;
	ret[0] = v1_v*d_row2[0] + v_v2*d_row1[0];
	ret[1] = v1_v*d_row2[1] + v_v2*d_row1[1];
	ret[2] = v1_v*d_row2[2] + v_v2*d_row1[2];
	ret[3] = v1_v*d_row2[3] + v_v2*d_row1[3];
	
	//A5~A8
	double u2_u = icp2.row - IL.row;
	double u_u1 = IL.row - icp1.row;
	ret[4] = u_u1*d_col2[4] + u2_u*d_col1[4];
	ret[5] = u_u1*d_col2[5] + u2_u*d_col1[5];
	ret[6] = u_u1*d_col2[6] + u2_u*d_col1[6];
	ret[7] = u_u1*d_col2[7] + u2_u*d_col1[7];
	
	//A9~A11
	double v2_v1 = icp2.col - icp1.col;
	double u_u2 = IL.row - icp2.row;
	double u2_u1 = icp2.row - icp1.row;
	ret[8]  = u_u2*(d_col2[8 ]-d_col1[8 ]) - v2_v1*(d_row2[8 ]) + u2_u1*(d_col2[8 ]) - v_v2*(d_row2[8 ]-d_row1[8 ]);
	ret[9]  = u_u2*(d_col2[9 ]-d_col1[9 ]) - v2_v1*(d_row2[9 ]) + u2_u1*(d_col2[9 ]) - v_v2*(d_row2[9 ]-d_row1[9 ]);
	ret[10] = u_u2*(d_col2[10]-d_col1[10]) - v2_v1*(d_row2[10]) + u2_u1*(d_col2[10]) - v_v2*(d_row2[10]-d_row1[10]);
	
	//X1,Y1,Z1
	double v2_v = icp2.col - IL.col;
	ret[11] = u2_u*d_col1[11]  - v2_v*d_row1[11];
	ret[12] = u2_u*d_col1[12]  - v2_v*d_row1[12];
	ret[13] = u2_u*d_col1[13]  - v2_v*d_row1[13];
	//X2,Y2,Z2
	ret[14] = u_u1*d_col2[11]  + v1_v*d_row2[11];
	ret[15] = u_u1*d_col2[12]  + v1_v*d_row2[12];
	ret[16] = u_u1*d_col2[13]  + v1_v*d_row2[13];
}

void CSMFrameDLTBA::Cal_PDs_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int image_index, unsigned int num_param)
{
	SMParam param = param_list[image_index];

	_ICP_ icp1, icp2;
	_GCP_ gcp1, gcp2;
	
	gcp1.X = GL1.X;
	gcp1.Y = GL1.Y;
	gcp1.Z = GL1.Z;
	
	gcp2.X = GL2.X;
	gcp2.Y = GL2.Y;
	gcp2.Z = GL2.Z;
	
	icp1.col = Func_col(gcp1, image_index);
	icp1.row = Func_row(gcp1, image_index);
	
	icp2.col = Func_col(gcp2, image_index);
	icp2.row = Func_row(gcp2, image_index);
	
	double d_row1[14], d_col1[14], d_row2[14], d_col2[14];
	Cal_PDs(gcp1,d_row1,d_col1,image_index);
	Cal_PDs(gcp2,d_row2,d_col2,image_index);
	//
	//FL=(row - row1)(col2 - col1) - (col - col1)(row2 - row1)
	//FL=(u - u1)(v2 - v1) - (v - v1)(u2 - u1)
	//
	//A1~A4
	double v_v2 = IL.col - icp2.col;
	double v1_v = icp1.col - IL.col;
	ret[0] = v1_v*d_row2[0] + v_v2*d_row1[0];
	ret[1] = v1_v*d_row2[1] + v_v2*d_row1[1];
	ret[2] = v1_v*d_row2[2] + v_v2*d_row1[2];
	ret[3] = v1_v*d_row2[3] + v_v2*d_row1[3];
	
	//A5~A8
	double u2_u = icp2.row - IL.row;
	double u_u1 = IL.row - icp1.row;
	ret[4] = u_u1*d_col2[4] + u2_u*d_col1[4];
	ret[5] = u_u1*d_col2[5] + u2_u*d_col1[5];
	ret[6] = u_u1*d_col2[6] + u2_u*d_col1[6];
	ret[7] = u_u1*d_col2[7] + u2_u*d_col1[7];
	
	//A9~A11
	double v2_v1 = icp2.col - icp1.col;
	double u_u2 = IL.row - icp2.row;
	double u2_u1 = icp2.row - icp1.row;
	ret[8]  = u_u2*(d_col2[8 ]-d_col1[8 ]) - v2_v1*(d_row2[8 ]) + u2_u1*(d_col2[8 ]) - v_v2*(d_row2[8 ]-d_row1[8 ]);
	ret[9]  = u_u2*(d_col2[9 ]-d_col1[9 ]) - v2_v1*(d_row2[9 ]) + u2_u1*(d_col2[9 ]) - v_v2*(d_row2[9 ]-d_row1[9 ]);
	ret[10] = u_u2*(d_col2[10]-d_col1[10]) - v2_v1*(d_row2[10]) + u2_u1*(d_col2[10]) - v_v2*(d_row2[10]-d_row1[10]);
	
	//X1,Y1,Z1
	double v2_v = icp2.col - IL.col;
	ret[11] = u2_u*d_col1[11]  - v2_v*d_row1[11];
	ret[12] = u2_u*d_col1[12]  - v2_v*d_row1[12];
	ret[13] = u2_u*d_col1[13]  - v2_v*d_row1[13];
	//X2,Y2,Z2
	ret[14] = u_u1*d_col2[11]  + v1_v*d_row2[11];
	ret[15] = u_u1*d_col2[12]  + v1_v*d_row2[12];
	ret[16] = u_u1*d_col2[13]  + v1_v*d_row2[13];
}

void CSMFrameDLTBA::PointOBSInit(CSMList<_GCP_> &gcp, _SCENE_ &scene, unsigned int num_param, Matrix<double> &A, Matrix<double> &L, Matrix<double> &W, unsigned int scene_index)
{
	unsigned int GCP_Num = gcp.GetNumItem();
	if(GCP_Num == 0) return;
	
	unsigned int i, j;
	
	unsigned int ICP_Num = scene.ICP_List.GetNumItem();
	
	if(ICP_Num == 0) return;

	for(i=0; i<GCP_Num; i++)
	{
		Matrix<double> l(2,1,0), a1(2,num_param,0), w(2,2,0);
		_GCP_ GP = gcp.GetAt(i);

		if(GP.s[0]>GCP_sd_threshold) continue;
		for(j=0; j<ICP_Num; j++)
		{
			_ICP_ IP = scene.ICP_List[j];
			if(IP.PID != GP.PID) continue;
			
			double *ret1 = new double[num_param+3];
			double *ret2 = new double[num_param+3];
			Cal_PDs_linear(GP, IP, ret1, ret2, scene_index);

			//row = (A1X + A2Y + A3Z + A4) - row*(A9X + A10Y + A11Z)
			//col = (A5X + A6Y + A7Z + A8) - col*(A9X + A10Y + A11Z)
			
			l(0,0) = IP.row;
			l(1,0) = IP.col;
			//u(row)
			//A1~A4
			for(unsigned int k=0; k<num_param; k++)
			{
				//row
				a1(0,k) = ret1[k];
				//col
				a1(1,k) = ret2[k];
			}

			//weight
			w(0,0) = 1./IP.s[0]/IP.s[0]; //s(1,1)
			w(1,1) = 1./IP.s[3]/IP.s[3]; //s(2,2)

			unsigned int pos = A.GetRows();
			A.Insert(pos,scene_index*num_param,a1);
			L.Insert(pos,0,l);
			W.Insert(pos,pos,w);
		}
	}
}

bool CSMFrameDLTBA::FindFlagB(unsigned int GCL_Num, CSMList<_GCL_> gcl, CString LID, unsigned int nStart, _GCL_ &GLB)
{
	for(unsigned int i=nStart; i<GCL_Num; i++)
	{
		GLB = gcl[i];
		if((GLB.LID == LID)&&(GLB.flag=="B"))
			return true;
	}
	return false;
}

bool CSMFrameDLTBA::FindFlagC(CSMList<_ICL_> &icl, CString LID, _ICL_ &ILA, _ICL_ &ILB)
{
	for(unsigned int i=0; i<icl.GetNumItem(); i++)
	{
		ILA = icl[i];
		if(ILA.LID == LID)
		{
			for(unsigned int j=i; j<icl.GetNumItem(); j++)
			{
				ILB = icl[j];
				if(ILB.LID == LID)
					return true;
			}
		}
	}

	return false;
}

SMParam CSMFrameDLTBA::ParamDenormalization(SMParam param, unsigned int num_param)
{
	//double* Nparam = SMData.DataNoramlization();
	//SHIFT_X[0], SCALE_X[1], 
	//SHIFT_Y[2], SCALE_Y[3], 
	//SHIFT_Z[4], SCALE_Z[5], 
	//SHIFT_col[6], SCALE_col[7], 
	//SHIFT_row[8], SCALE_row[9]

	double* A = new double[num_param]; 
	for(unsigned int i=0; i<num_param; i++) 
		A[i] = param.A[i];

	double alpha = param.A[8];

	double L = A[8] *param.SHIFT_X*param.SCALE_X
		     + A[9] *param.SHIFT_Y*param.SCALE_Y
			 + A[10]*param.SHIFT_Z*param.SCALE_Z 
			 + 1;

	double M = A[0]*param.SHIFT_X*param.SCALE_X/param.SCALE_row
		     + A[1]*param.SHIFT_Y*param.SCALE_Y/param.SCALE_row
			 + A[2]*param.SHIFT_Z*param.SCALE_Z/param.SCALE_row
			 + A[3]/param.SCALE_row;

	param.A[0] = A[0]*param.SCALE_X/param.SCALE_row/L - A[8] *param.SCALE_X*param.SHIFT_row/L;
	param.A[1] = A[1]*param.SCALE_Y/param.SCALE_row/L - A[9] *param.SCALE_Y*param.SHIFT_row/L;
	param.A[2] = A[2]*param.SCALE_Z/param.SCALE_row/L - A[10]*param.SCALE_Z*param.SHIFT_row/L;
	param.A[3] = M/L - param.SHIFT_row;

	param.A[4] = A[4]*param.SCALE_X/param.SCALE_col/L - A[8]  *param.SCALE_X*param.SHIFT_col/L;
	param.A[5] = A[5]*param.SCALE_Y/param.SCALE_col/L - A[9]  *param.SCALE_Y*param.SHIFT_col/L;
	param.A[6] = A[6]*param.SCALE_Z/param.SCALE_col/L - A[10] *param.SCALE_Z*param.SHIFT_col/L;
	param.A[7] = M/L - param.SHIFT_col;

	param.A[8]  = A[8] *param.SCALE_X/L;
	param.A[9]  = A[9] *param.SCALE_Y/L;
	param.A[10] = A[10]*param.SCALE_Z/L;
	
	param.SHIFT_X = 0;
	param.SCALE_X = 1;
	param.SHIFT_Y = 0;
	param.SCALE_Y = 1;
	param.SHIFT_Z = 0;
	param.SCALE_Z = 1;
	param.SHIFT_col = 0;
	param.SCALE_col = 1;
	param.SHIFT_row = 0;
	param.SCALE_row = 1;

	return param;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
/*********************************************************************************************************/
/*********************************************************************************************************/
/**********************************CLASS**CSMSDLTBA*******************************************************/
/*********************************************************************************************************/
/*********************************************************************************************************/
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

CSMSDLTBA::CSMSDLTBA()
{
	Init();
}

CSMSDLTBA::~CSMSDLTBA()
{

}

//    (A1X + A2Y + A3Z + A4)
//F = ---------------------- - row
//    (A9X + A10Y + A11Z + 1)
//
//                         (A5X + A6Y + A7Z + A8)
//G  = (col-A12*row*col) = ----------------------  - col
//                         (A9X + A10Y + A11Z + 1)

double CSMSDLTBA::Func_F(_GCP_ G, _ICP_ I, unsigned int index)
{
	SMParam param = param_list[index];
	double ret = (param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.)
		-I.row;
	return ret;
}
double CSMSDLTBA::Func_F(_GCL_ G, _ICL_ I, unsigned int index)
{
	SMParam param = param_list[index];
	double ret = (param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.)
		- I.row;
	return ret;
}
double CSMSDLTBA::Func_row(_GCP_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double row = (param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.);
	return row;
}
double CSMSDLTBA::Func_row(_GCL_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double row = (param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.);
	return row;
}

//       (A1X + A2Y + A3Z + A4)
//F     = ---------------------- - row
//       (A9X + A10Y + A11Z + 1)
//
//   (A5X + A6Y + A7Z + A8)
//G = ---------------------- + A12*row*col - col
//   (A9X + A10Y + A11Z + 1)

double CSMSDLTBA::Func_G(_GCP_ G, _ICP_ I, unsigned int index)
{
	SMParam param = param_list[index];
	double ret = (param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.)
		+ param.A[11]*I.row*I.col
		- I.col;
	return ret;
}
double CSMSDLTBA::Func_G(_GCL_ G, _ICL_ I, unsigned int index)
{
	SMParam param = param_list[index];
	double ret = (param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.)
		+ param.A[11]*I.row*I.col
		- I.col;
	return ret;
}
double CSMSDLTBA::Func_col(_GCL_ G, double row, unsigned int index)
{
	SMParam param = param_list[index];
	double ret = (param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3])
		/((param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.)*(1-param.A[11]*row));
	return ret;
}
double CSMSDLTBA::Func_col(_GCP_ G, double row, unsigned int index)
{
	SMParam param = param_list[index];
	double ret = (param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3])
		/((param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.)*(1-param.A[11]*row));
	return ret;
}

void CSMSDLTBA::Cal_PDs(_GCP_ G, _ICP_ I, double* ret1, double* ret2, unsigned int index, unsigned int num_param)
{
//    (A1X + A2Y + A3Z + A4)
//F = ---------------------- - row
//    (A9X + A10Y + A11Z + 1)
//
//                   (A5X + A6Y + A7Z + A8)
//G  = A12*row*col + ----------------------  - col
//                   (A9X + A10Y + A11Z + 1)
//       (A5X + A6Y + A7Z + A8)
//   ------------------------------------  = col
//   (A9X + A10Y + A11Z + 1)(1 - A12*row)

	SMParam param = param_list[index];

	double row_num = param.A[0]*G.X + param.A[1]*G.Y + param.A[2 ]*G.Z + param.A[3];
	double den     = param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.0;

	//ROW
	//A1~A4
	ret1[0] = G.X/den;
	ret1[1] = G.Y/den;
	ret1[2] = G.Z/den;
	ret1[3] = 1.0/den;
	//A5~A8
	ret1[4] = 0.;
	ret1[5] = 0.;
	ret1[6] = 0.;
	ret1[7] = 0.;
	//A9~A11
	double dd = den*den;
	double n1dd = row_num/dd;
	ret1[8]  = -G.X*n1dd;
	ret1[9]  = -G.Y*n1dd;
	ret1[10] = -G.Z*n1dd;
	//A12
	ret1[11] = 0.;
	//X,Y,Z
	ret1[12] = (param.A[0]*den - param.A[8 ]*row_num)/dd;
	ret1[13] = (param.A[1]*den - param.A[9 ]*row_num)/dd;
	ret1[14] = (param.A[2]*den - param.A[10]*row_num)/dd;

	double col_num = param.A[4]*G.X + param.A[5]*G.Y + param.A[6 ]*G.Z + param.A[7];
	//COL
	//A1~A4
	ret2[0] = 0.;
	ret2[1] = 0.;
	ret2[2] = 0.;
	ret2[3] = 0.;
	//A5~A8
	ret2[4] = G.X/den;
	ret2[5] = G.Y/den;
	ret2[6] = G.Z/den;
	ret2[7] = 1.0/den;
	//A9~A11
	double n2dd = col_num/dd;
	ret2[8]  = -G.X*n2dd;
	ret2[9]  = -G.Y*n2dd;
	ret2[10] = -G.Z*n2dd;
	//A12
	ret2[11] = I.row*I.col;
	//X,Y,Z
	ret2[12] = (param.A[4]*den - param.A[8 ]*col_num)/dd;
	ret2[13] = (param.A[5]*den - param.A[9 ]*col_num)/dd;
	ret2[14] = (param.A[6]*den - param.A[10]*col_num)/dd;
}

void CSMSDLTBA::FillMatrix_Point_i(CLeastSquare &LS, _GCP_ GP, _ICP_ IP, unsigned image_index, Matrix<double> &a1, Matrix<double> &a2, Matrix<double> &l, Matrix<double> &w, unsigned int num_param)
{
	//**************************************
	//A point Data
	//**************************************
	l.Resize(2,1,0.);
	a1.Resize(2,num_param,0.);
	a2.Resize(2,3,0.);
	w.Resize(2,2,0.);
	
	SMParam param = param_list[image_index];
	
	/************************
	* Elements of L matrix.
	* (row - Fo) + v
	* (col - Go) + v
	************************/
	l(0,0) = -Func_F(GP,IP,image_index);
	l(1,0) = -Func_G(GP,IP,image_index);
	
	/************************
	* Elements of A matrix.
	* Jacobian  matrix
	************************/
	double* PDs1 = new double[num_param+3];
	double* PDs2 = new double[num_param+3];
	//Cal_PDs_linear(GP, IP, PDs1, PDs2, image_index);
	Cal_PDs(GP, IP, PDs1, PDs2, image_index, num_param);
	
	for(unsigned int i=0; i<num_param; i++)
	{
		a1(0,i) = PDs1[i];
		a1(1,i) = PDs2[i];
	}
	//A1~A11, A12<==PDs[14]
	//dX, dY, dZ <==PDs[10],[12],[13]
	for(i=0; i<3; i++)
	{
		a2(0,i) = PDs1[num_param+i];
		a2(1,i) = PDs2[num_param+i];
	}

	/**
	*Equivalent weight
	*/
	
	Matrix<double> B(2,2,0.), Q(2,2,0.);
	
	Q(0,0)=IP.s[0]*IP.s[0];
	Q(1,1)=IP.s[3]*IP.s[3];
	
	B(0,0) = -1.;
	B(0,1) = 0.;
	B(1,0) = 1*param.A[11]*IP.col;
	B(1,1) = 1*param.A[11]*IP.row - 1.;

	w = (B%Q%(B.Transpose())).Inverse();
}

CString CSMSDLTBA::DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize)
{
	unsigned int num_param = 12;

	CString retval;

	retval = "\r\n\r\n[SDLT Modeling]\r\n";
	retval += "row = (A1X + A2Y + A3Z + A4)/(A9X + A10Y + A11Z + 1)\r\n";
	retval += "col = (A5X + A6Y + A7Z + A8)/(A9X + A10Y + A11Z + 1) + A12*row*col\r\n";
	
	if(bNormalized == true) retval += "(***Data is normalized***)\r\n";
	
	/**
	*Calculate parameter approximation
	*/
	retval += "\r\n\r\n[Init_value of Parameters]\r\n";
	retval += "///////////////////////////////////////////////////////////////////\r\n";
	///////////////////////////////
	//Original Data backup
	///////////////////////////////
	retval += DoInitModeling(num_param-1);
	///////////////////////////////////////
	//Initial data copy for the iteration
	///////////////////////////////////////
	twin_SMData = SMData;
	///////////////////////////////////////
	retval += "///////////////////////////////////////////////////////////////////\r\n";

	FileOut(outfilename,retval);
			
	retval += Run(num_param, num_max_iter, maximum_sd, maximum_diff, outfilename,pixelsize);
	
	::Beep(500,50);
	return retval;
}

SMParam CSMSDLTBA::ParamDenormalization(SMParam param, unsigned int num_param)
{
	//*
	//double* Nparam = SMData.DataNoramlization();
	//SHIFT_X[0], SCALE_X[1], 
	//SHIFT_Y[2], SCALE_Y[3], 
	//SHIFT_Z[4], SCALE_Z[5], 
	//SHIFT_col[6], SCALE_col[7], 
	//SHIFT_row[8], SCALE_row[9]

	double* A = new double[num_param]; 
	for(unsigned int i=0; i<num_param; i++) 
		A[i] = param.A[i];

	double alpha = param.A[8];

	double L = A[8] *param.SHIFT_X*param.SCALE_X
		     + A[9] *param.SHIFT_Y*param.SCALE_Y
			 + A[10]*param.SHIFT_Z*param.SCALE_Z 
			 + 1;

	double M = A[0]*param.SHIFT_X*param.SCALE_X/param.SCALE_row
		     + A[1]*param.SHIFT_Y*param.SCALE_Y/param.SCALE_row
			 + A[2]*param.SHIFT_Z*param.SCALE_Z/param.SCALE_row
			 + A[3]/param.SCALE_row;

	param.A[0] = A[0]*param.SCALE_X/param.SCALE_row/L - A[8] *param.SCALE_X*param.SHIFT_row/L;
	param.A[1] = A[1]*param.SCALE_Y/param.SCALE_row/L - A[9] *param.SCALE_Y*param.SHIFT_row/L;
	param.A[2] = A[2]*param.SCALE_Z/param.SCALE_row/L - A[10]*param.SCALE_Z*param.SHIFT_row/L;
	param.A[3] = M/L - param.SHIFT_row;

	param.A[8]  = A[8] *param.SCALE_X/L;
	param.A[9]  = A[9] *param.SCALE_Y/L;
	param.A[10] = A[10]*param.SCALE_Z/L;

	double R = -A[11]*param.SCALE_col*param.SCALE_row;

	double E = param.SCALE_col + R*param.SHIFT_row;

	double D = R/E;

	double W = param.SCALE_col*param.SHIFT_col/E + D*param.SHIFT_col*param.SHIFT_row;
	
	param.A[4] = A[4]*param.SCALE_X/L/E - D*param.SHIFT_col*param.A[0] - W*param.A[8];
	param.A[5] = A[5]*param.SCALE_Y/L/E - D*param.SHIFT_col*param.A[1] - W*param.A[9];
	param.A[6] = A[6]*param.SCALE_Z/L/E - D*param.SHIFT_col*param.A[2] - W*param.A[10];
	param.A[7] = (A[4]*param.SHIFT_X*param.SCALE_X
		       +  A[5]*param.SHIFT_Y*param.SCALE_Y
			   +  A[6]*param.SHIFT_Z*param.SCALE_Z
			   +  A[7])/L/E
			   -  D*param.SHIFT_col*param.A[3]
			   - W;

	param.A[11] = D;
	///////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////
	param.SHIFT_X = 0;
	param.SCALE_X = 1;
	param.SHIFT_Y = 0;
	param.SCALE_Y = 1;
	param.SHIFT_Z = 0;
	param.SCALE_Z = 1;
	param.SHIFT_col = 0;
	param.SCALE_col = 1;
	param.SHIFT_row = 0;
	param.SCALE_row = 1;
	//*/

	return param;
}

