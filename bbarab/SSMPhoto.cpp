/*
 * Copyright (c) 1999-2007, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// SMPhoto.cpp: implementation of the CSSMPhoto class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SSMPhoto.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSSMPhoto::CSSMPhoto()
{
	m_Dispersion.Resize(6, 6);
	m_Dispersion.Identity(1.0);
}

CSSMPhoto::~CSSMPhoto()
{
	m_ImagePoints.RemoveAll();
}

CSSMPoint CSSMPhoto::GetImagePoint(CSSMCamera camera, int index)
{
	CSSMPoint IP = m_ImagePoints.GetAt(index);
	double delta_x, delta_y;
	camera.GetSMACDistortion(IP[0], IP[1], delta_x, delta_y);
	IP[0] = IP[0] - (camera.m_xp + delta_x);
	IP[1] = IP[1] - (camera.m_yp + delta_y);

	return IP;
}

void CSSMPhoto::AddImagePoint(CSSMPoint IP)
{
	m_ImagePoints.AddTail(IP);
}

bool CSSMPhoto::SetImagePoint(int index, CSSMPoint IP)
{
	if(false == m_ImagePoints.SetAt(index, IP))
		return false;

	return true;
}

int CSSMPhoto::GetNumberofPoints()
{
	return m_ImagePoints.GetNumItem();
}

void CSSMPhoto::RemoveAllImagePoints()
{
	m_ImagePoints.RemoveAll();
}