/*
 * Copyright (c) 2005-2005, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// SMDEMFilter.h: interface for the CSMDEMFilter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__SMDEMFILTER_H__INTERSOLUTION__BBARAB__)
#define __SMDEMFILTER_H__INTERSOLUTION__BBARAB__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <templatedem.h>
#include <SMmatrix.h>
using namespace SMATICS_BBARAB;

#include "./Image_Util/Image.h"
#include <SMPseudoGridData.h>

/**@class CSMDEMFilter<br>
*@brief DEM filtering class.
*/
//이 클래스는 ENGO629 텀프로젝트를위해 작성된 것임.
//실제 사용을 위해서는 알고리즘 및 메모리 관리 부분을 개선해야 함.
class CSMDEMFilter  
{
public:
	
	/**CSMDEMFilter
	* Description	    : constructor
	* Return type		: 
	*/
	CSMDEMFilter();
	
	/**CSMDEMFilter
	* Description	    : constructor
	* Return type		: 
	*@param CSMDEMFilter &copy
	*/
	CSMDEMFilter(CSMDEMFilter &copy);
	
	/**CSMDEMFilter
	* Description	    : constructor
	* Return type		: 
	*@param CString dempath
	*@param CString hdrpath
	*/
	CSMDEMFilter(CString dempath, CString hdrpath);
	
	/**CSMDEMFilter
	* Description	    : copy constructor
	* Return type		: 
	*@param CSMDEM &copydem
	*/
	CSMDEMFilter(CTemplateDEM<double> &copydem);

	/**~CSMDEMFilter
	* Description	    : destructor
	* Return type		: 
	*/
	virtual ~CSMDEMFilter();
	
	/**operator=
	* Description	    : 
	* Return type		: CSMDEMFilter& 
	*@param CSMDEMFilter &copy
	*/
	CSMDEMFilter& operator=(CSMDEMFilter &copy);
	
	/**Copy
	* Description	    : 
	* Return type		: CSMDEMFilter& 
	*@param CSMDEMFilter &copy
	*/
	CSMDEMFilter& Copy(CSMDEMFilter &copy);
	
	/**ReadDEM
	* Description	    : 
	* Return type		: void 
	*@param CString dempath
	*@param CString hdrpath
	*/
	void ReadDEM(CString dempath, CString hdrpath);	
	
public:
	CTemplateDEM<double> m_DEM;/*< CSMDEM instance*/
	double min_dist1, max_dist1;
	double min_dist2, max_dist2;
	double min_dist3, max_dist3;
	double min_a, max_a;
	double min_b, max_b;
	double min_c, max_c;
	double range1, range2, range3;
	double range_a, range_b, range_c;
	double Qinterval;
	CString stOutFile;
	double limit_var;
	FILE *paramfile;
	CSMPseudoGridData PseudoGrid;

public:
		
	/**PlaneSearching_ThreeOrigins
	* Description	    : 
	* Return type		: bool 
	*@param int method
	*@param unsigned int searchsize
	*@param double variance
	*@param unsigned int max_iter_num
	*@param CString outpath
	*@param double max_dist
	*@param double bQuantize
	*@param unsigned int min_num_points
	*@param bool bOutParam
	*/
	bool PlaneSearching_ThreeOrigins(int method, unsigned int searchsize, double variance, unsigned int max_iter_num, CString outpath, double bQuantize, unsigned int min_num_points, bool bOutParam);

	/**PlaneSearching_OneOrigin
	* Description	    : 
	* Return type		: bool 
	*@param int method
	*@param unsigned int searchsize
	*@param double variance
	*@param unsigned int max_iter_num
	*@param CString outpath
	*@param double max_dist
	*@param double bQuantize
	*@param unsigned int min_num_points
	*@param bool bOutParam
	*/
	bool PlaneSearching_OneOrigin(int method, unsigned int searchsize, double variance, unsigned int max_iter_num, CString outpath, double bQuantize, unsigned int min_num_points, bool bOutParam);
	bool PlaneSearching_OneOrigin_old(int method, unsigned int searchsize, double variance, unsigned int max_iter_num, CString outpath, double bQuantize, unsigned int min_num_points, bool bOutParam);

private:

	/**GLSforCoeff
	* Description	    : 
	* Return type		: bool 
	*@param int index_x
	*@param int index_y
	*@param int half_size
	*@param double &a
	*@param double &b
	*@param double &c
	*@param double variance
	*@param unsigned int max_iter_num
	*@param double max_dist
	*@param double &pos_var
	*/
	bool GLSforCoeff(int index_x, int index_y, int half_size, double &a, double &b, double &c, unsigned int max_iter_num, double &pos_var);

	/**GLSforCoeff
	* Description	    : 
	* Return type		: bool 
	*@param fstream debugfile
	*@param int index_x
	*@param int index_y
	*@param int half_size
	*@param double &a
	*@param double &b
	*@param double &c
	*@param double variance
	*@param unsigned int max_iter_num
	*@param double max_dist
	*@param double &pos_var
	*/
	bool GLSforCoeff(fstream &debugfile, int index_x, int index_y, int half_size, double &a, double &b, double &c, unsigned int max_iter_num, double &pos_var);

	/**RobustGLSforCoeff_L1
	* Description	    : 
	* Return type		: bool 
	*@param int index_x
	*@param int index_y
	*@param int half_size
	*@param double &a
	*@param double &b
	*@param double &c
	*@param double variance
	*@param unsigned int max_iter_num
	*@param double max_dist
	*@param double &pos_var
	*/
	bool RobustGLSforCoeff(int index_x, int index_y, int half_size, double &a, double &b, double &c, unsigned int max_iter_num, double &pos_var, int method);
	bool RobustLSforCoeff_old(int index_x, int index_y, int half_size, double &a, double &b, double &c, unsigned int max_iter_num, double max_dist, double &pos_var, int method);
	bool RobustLSforCoeff(int index_x, int index_y, int half_size, double &a, double &b, double &c, unsigned int max_iter_num, double &pos_var, int method);
	
	/**DistMap_OneOrigin
	* Description	    : 
	* Return type		: void 
	*@param CString path
	*@param double *accu_dist
	*/
	void DistMap_OneOrigin_old(CString path, double *accu_dist);

	/**DistMap_ThreeOrigins
	* Description	    : 
	* Return type		: void 
	*@param CString path
	*@param double *accu_dist
	*/
	void DistMap_ThreeOrigins(CString path, double *accu_dist);

	/**FillAccumulator_ThreeOrigins
	* Description	    : 
	* Return type		: void 
	*@param bool bAvailable
	*@param unsigned int i
	*@param unsigned int j
	*@param double *accu
	*@param double *accu_dist
	*@param double *origin
	*@param double a
	*@param double b
	*@param double c
	*@param int max_bufsize
	*/
	void FillAccumulator_ThreeOrigins(bool bAvailable, unsigned int i, unsigned int j, double *accu, double *accu_dist, double *origin, double a, double b, double c, int max_bufsize, bool bOutParam);

	/**FillAccumulator_OneOrigin
	* Description	    : 
	* Return type		: void 
	*@param bool bAvailable
	*@param unsigned int i
	*@param unsigned int j
	*@param double *accu
	*@param double a
	*@param double b
	*@param double c
	*@param int max_bufsize
	*/
	void FillAccumulator_OneOrigin(bool bAvailable, unsigned int index_i, unsigned int index_j, double *accu, int half_size, double a, double b, double c, double Threshold, int max_bufsize, bool bOutParam);
	void FillAccumulator_OneOrigin_old(bool bAvailable, unsigned int i, unsigned int j, double *accu, double a, double b, double c, int max_bufsize, bool bOutParam);

	void QuantizingAccumulator_OneOrigin(double *accu_dist, unsigned int min_num_points, CString outpath);
	void QuantizingAccumulator_OneOrigin_old(double *accu_dist, unsigned int min_num_points, CString outpath);
	void QuantizingAccumulator_ThreeOrigins(double *accu, unsigned int min_num_points, CString outpath);

	void FrequencyMap_OneOrigin(CString path, double *Q_accu_dist, unsigned int min_num_points);
	void FrequencyMap_ThreeOrigins(CString path, double *Q_accu_dist, unsigned int min_num_points);

	void Taging(CImage &image);

	
};

#endif // !defined(__SMDEMFILTER_H__INTERSOLUTION__BBARAB__)
