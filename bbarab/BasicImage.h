// BasicImage.h: interface for the CBasicImage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(__CBASICIMAGE__CLASS__BBARAB__)
#define __CBASICIMAGE__CLASS__BBARAB__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define HDIB HGLOBAL

// DIB constants
#define PALVERSION   0x300

// DIB Macros
#define IS_WIN30_DIB(lpbi) ((*(LPDWORD)(lpbi)) == sizeof(BITMAPINFOHEADER))
#define RECTWIDTH(lpRect)  ((lpRect)->right - (lpRect)->left)
#define RECTHEIGHT(lpRect) ((lpRect)->bottom - (lpRect)->top)
#define WIDTHBYTES(bits)   (((bits) + 31) / 32 * 4)

typedef struct tagCOLOR
{
	BYTE B;
	BYTE G;
	BYTE R;
} COLOR;

typedef struct tagCOLOR* LPCOLOR;

class CBasicImage  
{
	friend class CImage;
public:
	CBasicImage();
	CBasicImage( CBasicImage &Src );
	virtual ~CBasicImage();
	void Free();

	void SetHandle(HANDLE hHandle);

	BOOL InitDIB(BOOL bCreatePalette = TRUE);
	BOOL CreateDIBPalette();
	BOOL Create(int width, int height, int depth);
	
	/**GetRainbowColor
	* Description	    : build pseudo color value (256 color)
	*@param BYTE index	: intensity value
	*@param BYTE &r		: red
	*@param BYTE &g		: green
	*@param BYTE &b		: blue
	*@return void 
	*/
	static void GetRainbowColor(BYTE index, BYTE &red, BYTE &green, BYTE &blue);
	static void GetRainbowColor_new(BYTE index, BYTE &red, BYTE &green, BYTE &blue);
	static void GetRainbowColorfromXYZ(double X, double Y, double Z, BYTE &r, BYTE &g, BYTE &b);
	
	//Operator
	CBasicImage& operator=(CBasicImage &);
	CBasicImage* operator=(CBasicImage *);

	//Undo
	BOOL Undo();
	BOOL PrepareUndo();
	BOOL CanUndo()			{return (m_hUndoImage!=NULL);}

	BOOL Draw(HDC hDC, LPRECT sourceRect, LPRECT destRect);

	// 이미지 정보를 얻는 함수
	int GetBitCount();
	HDIB GetHandle()		{return m_hImage;}
	BOOL IsDataNull()		{return (m_hImage == NULL);}
	CSize GetSize()			{return m_Size;}	
	int GetHeight()			{return m_Size.cy;}
	int GetWidth()			{return m_Size.cx;}
	int GetRealWidth()		{return WIDTHBYTES((GetWidth()*GetBitCount()));}
	HDIB GetUndoHandle()	{return m_hUndoImage;}
	CPalette *GetPalette()	{return m_pPal;}

/******************************************************
						멤버 변수
******************************************************/
protected:
	HDIB m_hImage;		//image handle
	HDIB m_hUndoImage;	//undo image handle
	CSize m_Size;		//image size
	CPalette * m_pPal;	//image palette
	CString filetype;   //image path
};

//
//CBMPImage
//
class CBMPImage : public CBasicImage
{
public:
	CBMPImage();
	
	CBMPImage( CBMPImage &Src );

	virtual ~CBMPImage() {}

	BOOL LoadBMP(LPCTSTR lpszFileName);

	BOOL SaveBMP(LPCTSTR lpszFileName);
};

//
//CPixel
//
class CPixel
{
public:
	int I;
	CPixel::CPixel() {}

	CPixel::~CPixel() {}

	CPixel::CPixel(int i) {	I = i;};

	inline CPixel operator>> (BYTE &);
	inline CPixel operator<< (BYTE);
	inline CPixel operator<< (int);
	inline operator BYTE() {return I;}
	inline operator int() {return (int)I;}
	inline CPixel operator= (int);
	inline CPixel operator* (int);
	inline CPixel operator/ (int);
	inline CPixel operator+ (int);
	inline CPixel operator- (int);
	inline CPixel operator*= (int);
	inline CPixel operator/= (int);
	inline CPixel operator+= (int);
	inline CPixel operator-= (int);

	inline CPixel operator= (double);
	inline CPixel operator* (double);
	inline CPixel operator/ (double);
	inline CPixel operator+ (double);
	inline CPixel operator- (double);
	inline CPixel operator*= (double);
	inline CPixel operator/= (double);
	inline CPixel operator+= (double);
	inline CPixel operator-= (double);

};

CPixel CPixel::operator >> (BYTE &i)
{
	i = (BYTE)__max(__min(255, I), 0);
	return *this;
};

CPixel CPixel::operator << (BYTE a)
{
	I = a;
	return *this;
};

CPixel CPixel::operator << (int a)
{
	I = a;
	return *this;
};

CPixel CPixel::operator= (int a)
{
	I = a;
	return *this;
};

CPixel CPixel::operator* (int a)
{
	CPixel r;
	r.I = I*a;
	return r;
};

CPixel CPixel::operator/ (int a)
{
	CPixel r;
	r.I = (int)((double)I/a+0.5);
	return r;
};

CPixel CPixel::operator+ (int a)
{
	CPixel r;
	r.I = I+a;
	return r;
};

CPixel CPixel::operator- (int a)
{
	CPixel r;
	r.I = I-a;
	return r;
};

CPixel CPixel::operator*= (int a)
{
	I *= a;
	return *this;
};

CPixel CPixel::operator/= (int a)
{
	I = (int)((double)I/a+0.5);
	return *this;
};

CPixel CPixel::operator+= (int a)
{
	I += a;
	return *this;
};

CPixel CPixel::operator-= (int a)
{
	I -= a;
	return *this;
};

CPixel CPixel::operator= (double a)
{
	I = (int)(a + 0.5);
	return *this;
};

CPixel CPixel::operator* (double a)
{
	CPixel r;
	r.I = (int)((double)I*a + 0.5);
	return r;
};

CPixel CPixel::operator/ (double a)
{
	CPixel r;
	r.I = (int)((double)I/a+0.5);
	return r;
};

CPixel CPixel::operator+ (double a)
{
	CPixel r;
	r.I = (int)((double)I+a+0.5);
	return r;
};

CPixel CPixel::operator- (double a)
{
	CPixel r;
	r.I = (int)((double)I-a+0.5);
	return r;
};

CPixel CPixel::operator*= (double a)
{
	I = (int)((double)I*a+0.5);
	return *this;
};

CPixel CPixel::operator/= (double a)
{
	I = (int)((double)I/a+0.5);
	return *this;
};

CPixel CPixel::operator+= (double a)
{
	I = (int)((double)I + a + 0.5);
	return *this;
};

CPixel CPixel::operator-= (double a)
{
	I = (int)((double)I - a + 0.5);
	return *this;
};

//
//CColorPixel
//
class CColorPixel
{
public:
	CColorPixel() {}
	
	CColorPixel(COLOR c) {R = c.R; G = c.G; B = c.B; };
	
	~CColorPixel()	{};

	inline int GetR() {return R;}
	inline int GetG() {return G;}
	inline int GetB() {return B;}

	inline void SetR(int value) {R=value;}
	inline void SetG(int value) {G=value;}
	inline void SetB(int value) {B=value;}

	inline CColorPixel operator= (COLOR);
	inline CColorPixel operator >>(COLOR &);
	inline CColorPixel operator <<(COLOR);
	inline CColorPixel operator= (CColorPixel);
	inline CColorPixel operator* (CColorPixel);
	inline CColorPixel operator/ (CColorPixel);
	inline CColorPixel operator+ (CColorPixel);
	inline CColorPixel operator- (CColorPixel);
	inline CColorPixel operator*= (CColorPixel);
	inline CColorPixel operator/= (CColorPixel);
	inline CColorPixel operator+= (CColorPixel);
	inline CColorPixel operator-= (CColorPixel);
	inline bool operator== (CColorPixel);

	inline CColorPixel operator= (int);
	inline CColorPixel operator* (int);
	inline CColorPixel operator/ (int);
	inline CColorPixel operator+ (int);
	inline CColorPixel operator- (int);
	inline CColorPixel operator*= (int);
	inline CColorPixel operator/= (int);
	inline CColorPixel operator+= (int);
	inline CColorPixel operator-= (int);

	inline CColorPixel operator= (double);
	inline CColorPixel operator* (double);
	inline CColorPixel operator/ (double);
	inline CColorPixel operator+ (double);
	inline CColorPixel operator- (double);
	inline CColorPixel operator*= (double);
	inline CColorPixel operator/= (double);
	inline CColorPixel operator+= (double);
	inline CColorPixel operator-= (double);

protected:
	int B, G, R;
};

CColorPixel CColorPixel::operator=(COLOR c)
{
	R = c.R;
	G = c.G;
	B = c.B;
	return *this;
};

CColorPixel CColorPixel::operator <<(COLOR c)
{
	R = c.R;
	G = c.G;
	B = c.B;
	return *this;
};

CColorPixel CColorPixel::operator >>(COLOR &c)
{
	c.R = (BYTE)__max(__min(255, R), 0);
	c.G = (BYTE)__max(__min(255, G), 0);
	c.B = (BYTE)__max(__min(255, B), 0);
	return *this;
};

CColorPixel CColorPixel::operator= (CColorPixel c)
{
	R = c.R;
	G = c.G;
	B = c.B;
	return *this;
};

CColorPixel CColorPixel::operator* (CColorPixel a)
{
	CColorPixel r;
	r.R = R*a.R;
	r.G = G*a.G;
	r.B = B*a.B;
	return r;
};

CColorPixel CColorPixel::operator/ (CColorPixel a)
{
	CColorPixel r;
	r.R = (int)((double)R/a.R+0.5);
	r.G = (int)((double)G/a.G+0.5);
	r.B = (int)((double)B/a.B+0.5);
	return r;
};

CColorPixel CColorPixel::operator+ (CColorPixel a)
{
	CColorPixel r;
	r.R = R+a.R;
	r.G = G+a.G;
	r.B = B+a.B;
	return r;
};

CColorPixel CColorPixel::operator- (CColorPixel a)
{
	CColorPixel r;
	r.R = R-a.R;
	r.G = G-a.G;
	r.B = B-a.B;
	return r;
};

CColorPixel CColorPixel::operator*= (CColorPixel a)
{
	R = R*a.R;
	G = G*a.G;
	B = B*a.B;
	return *this;
};

CColorPixel CColorPixel::operator/= (CColorPixel a)
{
	R = (int)((double)R/a.R+0.5);
	G = (int)((double)G/a.G+0.5);
	B = (int)((double)B/a.B+0.5);
	return *this;
};

CColorPixel CColorPixel::operator+= (CColorPixel a)
{
	R = R+a.R;
	G = G+a.G;
	B = B+a.B;
	return *this;
};

CColorPixel CColorPixel::operator-= (CColorPixel a)
{
	R = R-a.R;
	G = G-a.G;
	B = B-a.B;
	return *this;
};

CColorPixel CColorPixel::operator= (int a)
{
	R = a;
	G = a;
	B = a;
	return *this;
};

CColorPixel CColorPixel::operator* (int a)
{
	R = R*a;
	G = G*a;
	B = B*a;
	return *this;
};

CColorPixel CColorPixel::operator/ (int a)
{
	R = (int)((double)R/a+0.5);
	G = (int)((double)G/a+0.5);
	B = (int)((double)B/a+0.5);
	return *this;
};

CColorPixel CColorPixel::operator+ (int a)
{
	CColorPixel r;
	r.R = R+a;
	r.G = G+a;
	r.B = B+a;
	return r;
};

CColorPixel CColorPixel::operator- (int a)
{
	CColorPixel r;
	r.R = R-a;
	r.G = G-a;
	r.B = B-a;
	return r;
};

CColorPixel CColorPixel::operator*= (int a)
{
	R = R*a;
	G = G*a;
	B = B*a;
	return *this;
};

CColorPixel CColorPixel::operator/= (int a)
{
	R = (int)((double)R/a+0.5);
	G = (int)((double)G/a+0.5);
	B = (int)((double)B/a+0.5);
	return *this;
};

CColorPixel CColorPixel::operator+= (int a)
{
	R = R+a;
	G = G+a;
	B = B+a;
	return *this;
};

CColorPixel CColorPixel::operator-= (int a)
{
	R = R-a;
	G = G-a;
	B = B-a;
	return *this;
};

CColorPixel CColorPixel::operator= (double a)
{
	R = (int)(a+0.5);
	G = (int)(a+0.5);
	B = (int)(a+0.5);
	return *this;
};

CColorPixel CColorPixel::operator* (double a)
{
	R = (int)((double)R*a + 0.5);
	G = (int)((double)G*a + 0.5);
	B = (int)((double)B*a + 0.5);
	return *this;
};

CColorPixel CColorPixel::operator/ (double a)
{
	R = (int)((double)R/a+0.5);
	G = (int)((double)G/a+0.5);
	B = (int)((double)B/a+0.5);
	return *this;
};

CColorPixel CColorPixel::operator+ (double a)
{
	CColorPixel r;
	r.R = (int)((double)R + a + 0.5);
	r.G = (int)((double)G + a + 0.5);
	r.B = (int)((double)B + a + 0.5);
	return r;
};

CColorPixel CColorPixel::operator- (double a)
{
	CColorPixel r;
	r.R = (int)((double)R - a + 0.5);
	r.G = (int)((double)G - a + 0.5);
	r.B = (int)((double)B - a + 0.5);
	return r;
};

CColorPixel CColorPixel::operator*= (double a)
{
	R = (int)((double)R*a + 0.5);
	G = (int)((double)G*a + 0.5);
	B = (int)((double)B*a + 0.5);
	return *this;
};

CColorPixel CColorPixel::operator/= (double a)
{
	R = (int)((double)R/a+0.5);
	G = (int)((double)G/a+0.5);
	B = (int)((double)B/a+0.5);

	return *this;
};

CColorPixel CColorPixel::operator+= (double a)
{
	CColorPixel r;
	r.R = (int)((double)R + a + 0.5);
	r.G = (int)((double)G + a + 0.5);
	r.B = (int)((double)B + a + 0.5);
	return *this;
};

CColorPixel CColorPixel::operator-= (double a)
{
	CColorPixel r;
	r.R = (int)((double)R - a + 0.5);
	r.G = (int)((double)G - a + 0.5);
	r.B = (int)((double)B - a + 0.5);
	return *this;
};

bool CColorPixel::operator== (CColorPixel p)
{
	if((R==p.R)&&(G==p.G)&&(B==p.B))
		return true;
	else return false;
};

//
//CBMPPixelPtr
//
class CBMPPixelPtr
{
public:
	BYTE *operator[](LONG index) {
		ASSERT(index>=0 && index<m_nHeight);
		return m_pPtr[index];
	}
	BYTE *operator[](int index) {
		ASSERT(index>=0 && index<m_nHeight);
		return m_pPtr[index];
	}
	CBMPPixelPtr() {m_pPtr = NULL;}
	CBMPPixelPtr(CBMPImage &Im);
	void SetImage(CBMPImage *Im);
	virtual ~CBMPPixelPtr();

protected:
	HDIB m_hHandle;
	BYTE ** m_pPtr;
	int m_nHeight;
};

class CBMPColorPixelPtr
{
public:
	LPCOLOR operator[](LONG index) {
		ASSERT(index>=0 && index<m_nHeight);
		return m_pPtr[index];
	}
	LPCOLOR operator[](int index) {
		ASSERT(index>=0 && index<m_nHeight);
		return m_pPtr[index];
	}
	CBMPColorPixelPtr() {m_pPtr = NULL;}
	CBMPColorPixelPtr(CBMPImage &Im);
	void SetImage(CBMPImage *Im);
	virtual ~CBMPColorPixelPtr();

protected:
	LPCOLOR *m_pPtr;
	HDIB m_hHandle;
	int m_nHeight;
};

/******************************************************
						전역 함수
******************************************************/
LPSTR WINAPI FindDIBBits(LPSTR lpbi);
DWORD WINAPI DIBWidth(LPSTR lpDIB);
DWORD WINAPI DIBHeight(LPSTR lpDIB);
WORD WINAPI PaletteSize(LPSTR lpbi);
WORD WINAPI DIBNumColors(LPSTR lpbi);
HGLOBAL WINAPI CopyHandle (HGLOBAL h);
/////////////////////////////////////////////////////////////////////////////

#endif // !defined(__CBASICIMAGE__CLASS__BBARAB__)
