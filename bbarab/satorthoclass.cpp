// satorthoclass.cpp: implementation of the CSatOrthoClass class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "satorthoclass.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "Collinearity.h"
using namespace SMATICS_BBARAB;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSatOrthoClass::CSatOrthoClass()
{
	faileddem = backdem = DEFAULT_EMPTY_DEM;
	m_nEOPOption = 0;
	number_ver = 1.0;
	m_xp = 0.0;
	m_yp = 0.0;
	m_d = 0.0;
}

CSatOrthoClass::~CSatOrthoClass()
{
}

void CSatOrthoClass::AnalysisIndex(int w, double scale, double index, int &row, int &col, double &dist)
{
	int image_index = (int)index;
	dist = (index - (double)image_index)*scale*(image_index<0?-1:1);
	col = image_index%w;
	row = (int)((double)image_index/(double)w);
	if(image_index == 661920)
	{
		int iii=0;
	}
}

void CSatOrthoClass::MakeIndex(int w, double scale, double &index, int row, int col, double dist)
{
	int image_index = row*w + col;
	double dist_index = dist/scale*(image_index<0?-1:1);
	index = (double)image_index + dist_index;
	if(image_index == 661920)
	{
		int iii=0;
	}
}

bool CSatOrthoClass::CheckOcclusion_Angle(double Xl, double Yl, double Zl, int indexX, int indexY)
{
	//Ground point (DEM point)
	double Xa = m_DEM->GetX(indexX);
	double Ya = m_DEM->GetY(indexY);
	double Za = m_DEM->GetZ(indexX,indexY);

	int dem_W = m_DEM->GetWidth(), dem_H = m_DEM->GetHeight();

	double dX = Xl - Xa;
	double dY = Yl - Ya;
	
	double limit_Angle = atan((Zl-Za)/sqrt((Xl-Xa)*(Xl-Xa)+(Yl-Ya)*(Yl-Ya)));
	
	if(fabs(dX) > fabs(dY))
	{
		double offsetZ = (Zl - Za)/fabs(dX)*m_DEM->resolution_X;
		double limitZ = Za;

		int offsetX = 1;
		if(dX < 0)
			offsetX = -1;

		double offsetY = 1.0*fabs(dY/dX);
		//DEM Grid는 행번호의 증가와 Y좌표의 증가가 서로 반대이다.
		if(dY > 0)
			offsetY *= -1.0;

		double offsetdist = sqrt(offsetX*m_DEM->resolution_X*offsetX*m_DEM->resolution_X + offsetY*m_DEM->resolution_Y*offsetY*m_DEM->resolution_Y);

		int ix;
		ix = indexX;

		double d_iy = double(indexY);
		int iy;

		double Z;
		double dist = sqrt(dX*dX + dY*dY);

		do
		{
			dist -= offsetdist;
			limitZ += offsetZ;
			
			if( limitZ >= MaxZ )
				return true;

			ix += offsetX;
			d_iy += offsetY;
			iy = int(d_iy + 0.5);
			
			if(ix < 0 || ix >= dem_W)
				return true;
			
			if(iy < 0 || iy >= dem_H)
				return true;
			
			if(false == m_DEM->GetZ(ix, iy, Z))continue;
			
			double angle = atan((Zl-Z)/dist);

			if(angle < limit_Angle) 
				return false;//invisible
						
			if( Z >= MaxZ )
				return true;
			
		}while(1);

	}
	else
	{
		double offsetZ = (Zl-Za)/fabs(dY)*m_DEM->resolution_Y;
		double limitZ = Za;

		int offsetY = 1;
		//DEM Grid는 행번호의 증가와 Y좌표의 증가가 서로 반대이다.
		if(dY > 0)
			offsetY = -1;
		
		double offsetX = 1.0*fabs(dX/dY);
		if(dX < 0)
			offsetX *= -1.0;

		double offsetdist = sqrt(offsetX*m_DEM->resolution_X*offsetX*m_DEM->resolution_X + offsetY*m_DEM->resolution_Y*offsetY*m_DEM->resolution_Y);

		int iy;
		iy = indexY;
		
		double d_ix = double(indexX);
		int ix;
		
		double Z;
		double dist = sqrt(dX*dX + dY*dY);

		do
		{
			dist -= offsetdist;
			limitZ += offsetZ;
			
			if( limitZ > MaxZ)
				return true;

			iy += offsetY;
			d_ix += offsetX;
			ix = int(d_ix + 0.5);
			
			if(ix < 0 || ix >= dem_W)
				return true;
			
			if(iy < 0 || iy >= dem_H)
				return true;
			
			if(false == m_DEM->GetZ(ix, iy, Z))continue;
			
			double angle = atan((Zl-Z)/dist);

			if(angle < limit_Angle) 
				return false;//invisible
			
			if( Z >= MaxZ )
				return true;
			
		}while (1);

	}
	
}

bool CSatOrthoClass::CheckOcclusion_DiffZ(double Xl, double Yl, double Zl, int indexX, int indexY)
{
	//Ground point (DEM point)
	double Xa = m_DEM->GetX(indexX);
	double Ya = m_DEM->GetY(indexY);
	double Za = m_DEM->GetZ(indexX,indexY);

	int dem_W = m_DEM->GetWidth(), dem_H = m_DEM->GetHeight();

	double dX = Xl - Xa;
	double dY = Yl - Ya;
	double dZ = Zl - Za;
	
	if(fabs(dX) > fabs(dY))
	{
		double offsetZ = dZ/fabs(dX)*m_DEM->resolution_X;

		int offsetX = 1;//1 cell
		if(dX < 0)
			offsetX = -1;

		double offsetY = 1.0*fabs(dY/dX);
		
		//DEM Grid는 행번호의 증가와 Y좌표의 증가가 서로 반대이다.
		if(dY > 0)
			offsetY *= -1.0;

		int ix;
		ix = indexX;

		double d_iy = double(indexY);
		int iy;

		double Z;

		double limitZ = Za;

		do
		{
			limitZ += offsetZ;

			ix += offsetX;
			d_iy += offsetY;
			iy = int(d_iy + 0.5);
			
			if(ix < 0 || ix >= dem_W)
				return true;
			
			if(iy < 0 || iy >= dem_H)
				return true;
			
			if(false == m_DEM->GetZ(ix, iy, Z))continue;

			if(limitZ >= MaxZ)
				return true;


			if(Z > limitZ)
				return false;

			if( Z >= MaxZ )
				return true;
			
		}while(1);

	}
	else
	{
		double offsetZ = dZ/fabs(dY)*m_DEM->resolution_Y;

		int offsetY = 1;
		//DEM Grid는 행번호의 증가와 Y좌표의 증가가 서로 반대이다.
		if(dY > 0)
			offsetY = -1;
		
		double offsetX = 1.0*fabs(dX/dY);
		if(dX < 0)
			offsetX *= -1.0;

		int iy;
		iy = indexY;
		
		double d_ix = double(indexX);
		int ix;
		
		double Z;

		double limitZ = Za;

		do
		{
			limitZ += offsetZ;

			iy += offsetY;
			d_ix += offsetX;
			ix = int(d_ix + 0.5);
			
			if(ix < 0 || ix >= dem_W)
				return true;
			
			if(iy < 0 || iy >= dem_H)
				return true;

			if(false == m_DEM->GetZ(ix, iy, Z))continue;
			
			if(limitZ >= MaxZ)
				return true;
			
			
			if(Z > limitZ)
				return false;

			if( Z >= MaxZ )
				return true;
			
		}while (1);

	}
	
}

bool  CSatOrthoClass::FindEOP(double row, double &O, double &P, double &K, double &X, double &Y, double &Z)
{	
	O=0.; P=0.; K=0.; X=0.; Y=0.; Z=0.;

	if(m_nEOPOption != 0 && m_nEOPOption != 1)
	{
		return false;
	}
	
	if((number_ver >= 1.0)&&(number_ver < 1.1))
	{
		X = EOParam[0] + EOParam[1]*row;
		Y = EOParam[2] + EOParam[3]*row;
		Z = EOParam[4] + EOParam[5]*row;
		
		O = EOParam[6 ] + EOParam[7 ]*row;
		P = EOParam[8 ] + EOParam[9 ]*row;
		K = EOParam[10] + EOParam[11]*row;
	}
	else
	{		
		if(m_nEOPOption == 0)
		{
			//select two orientation images close to row_number
			OIEOP eop1, eop2;
			if(num_OI == 1)
			{
				eop1 = m_OIEOP.GetAt(0);
				O = eop1.O;
				P = eop1.P;
				K = eop1.K;
				X = eop1.X;
				Y = eop1.Y;
				Z = eop1.Z;
			}
			else if(num_OI == 2)
			{
				eop1 = m_OIEOP.GetAt(0);
				eop2 = m_OIEOP.GetAt(1);
				double line_gap = eop2.n_line - eop1.n_line;
				double line_x = row - eop1.n_line;
				O = eop1.O + line_x *(eop2.O - eop1.O)/line_gap;
				P = eop1.P + line_x *(eop2.P - eop1.P)/line_gap;
				K = eop1.K + line_x *(eop2.K - eop1.K)/line_gap;
				X = eop1.X + line_x *(eop2.X - eop1.X)/line_gap;
				Y = eop1.Y + line_x *(eop2.Y - eop1.Y)/line_gap;
				Z = eop1.Z + line_x *(eop2.Z - eop1.Z)/line_gap;
			}
			else if(num_OI >= 3)
			{
				double line1 = m_OIEOP.GetAt(0).n_line;
				eop1 = m_OIEOP.GetAt(0);
				eop2.n_line = 999.999E10;
				for(int i=0; i<(int)m_OIEOP.GetNumItem();i++)
				{
					OIEOP temp = m_OIEOP.GetAt(i);
					double gap = fabs(temp.n_line - row);
					
					if(gap<fabs(eop1.n_line - row))
					{
						eop1 = temp;
					}
					else
					{
						if(temp.n_line != eop1.n_line)
						{
							if(eop2.n_line == 999.999E10) eop2 = temp;
							else if(gap<fabs(eop2.n_line - row))
							{
								eop2 = temp;
							}
						}
					}
				}
				
				double line_gap = eop2.n_line - eop1.n_line;
				double line_x = row - eop1.n_line;
				O = eop1.O + line_x *(eop2.O - eop1.O)/line_gap;
				P = eop1.P + line_x *(eop2.P - eop1.P)/line_gap;
				K = eop1.K + line_x *(eop2.K - eop1.K)/line_gap;
				X = eop1.X + line_x *(eop2.X - eop1.X)/line_gap;
				Y = eop1.Y + line_x *(eop2.Y - eop1.Y)/line_gap;
				Z = eop1.Z + line_x *(eop2.Z - eop1.Z)/line_gap;
			}
			else
			{
				return false;
			}
		}
		else if(m_nEOPOption == 1)
		{
			for(int i=0; i<=m_SEOP.n_Order; i++)
			{
				O += m_SEOP.O.GetAt(i)*pow(row,i);
				P += m_SEOP.P.GetAt(i)*pow(row,i);
				K += m_SEOP.K.GetAt(i)*pow(row,i);
				X += m_SEOP.X.GetAt(i)*pow(row,i);
				Y += m_SEOP.Y.GetAt(i)*pow(row,i);
				Z += m_SEOP.Z.GetAt(i)*pow(row,i);
			}
		}
	}

	return true;
}

bool CSatOrthoClass::GroundCoord2SceneCoord(double X, double Y, double Z, double &Xs, double &Ys, double &Zs, double &row, double &col, double init_r)
{
	double Os, Ps, Ks;
	
	double c = IOParam[2];
	row = init_r;
	
	double descripancy;
	do
	{
		if((number_ver >= 1.0)&&(number_ver < 1.1))
		{
			Xs = EOParam[0] + EOParam[1]*row;
			Ys = EOParam[2] + EOParam[3]*row;
			Zs = EOParam[4] + EOParam[5]*row;
			
			Os = EOParam[6 ] + EOParam[7 ]*row;
			Ps = EOParam[8 ] + EOParam[9 ]*row;
			Ks = EOParam[10] + EOParam[11]*row;
		}
		else if(number_ver >= 1.1)
		{
			row = row - m_xp + m_d;//image row coord to scan line
			if(false == FindEOP(row, Os, Ps, Ks, Xs, Ys, Zs)) return false;//line_number = line_number' - yp + offset
		}
		
		double q, r, s;
		//col = -c(r/q)
		//row = -c(s/q)
		SMATICS_BBARAB::CollinearityEQ_LineCCD(Os, Ps, Ks, Xs, Ys, Zs, X, Y, Z, q, r, s);

		descripancy = -c*r/q; //x: flight direction, [descripancy]

		//update line number
		row = row + descripancy + m_xp - m_d;//scan line to image row coord

		double col_p =  IOParam[0]/2;//center of the each scan line
		col = -c*s/q + col_p + m_yp;
		
		//
		//                 |         /|
		//                 |        / |
		//                 |  drow /  |
		//        ----------------------------
		//                 |     / row_
		//                 |    /
		//                 |   /
		//                 |  /
		//                 | /
		//                 |/
		
	}while(fabs(descripancy)>=_MAXDIFFROW_);
	
	if((number_ver >= 1.0)&&(number_ver < 1.1))
	{
		Xs = EOParam[0] + EOParam[1]*row;
		Ys = EOParam[2] + EOParam[3]*row;
		Zs = EOParam[4] + EOParam[5]*row;
		
		Os = EOParam[6 ] + EOParam[7 ]*row;
		Ps = EOParam[8 ] + EOParam[9 ]*row;
		Ks = EOParam[10] + EOParam[11]*row;
	}
	else if(number_ver >= 1.1)
	{
		if(false == FindEOP(row, Os, Ps, Ks, Xs, Ys, Zs)) return false;
	}
	
	return true;
}

bool CSatOrthoClass::CalSInterval(double &SInterval, double line_num, double col_num)
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//double SInterval = (m_DEM->resolution_X+m_DEM->resolution_Y)/2.*0.5;
	
	CRotationcoeff2 Rmat;
	double O, P, K, X, Y, Z;

	if(number_ver >= 1.1)
	{
		line_num = line_num - m_xp + m_d;//image row coord to scan line
	}
	
	if((number_ver >= 1.0)&&(number_ver < 1.1))
	{
		X = EOParam[0] + EOParam[1]*line_num;
		Y = EOParam[2] + EOParam[3]*line_num;
		Z = EOParam[4] + EOParam[5]*line_num;
		
		O = EOParam[6 ] + EOParam[7 ]*line_num;
		P = EOParam[8 ] + EOParam[9 ]*line_num;
		K = EOParam[10] + EOParam[11]*line_num;
	}
	else if(number_ver >= 1.1)
	{
		line_num = line_num - m_xp + m_d;//image row coord to scan line
		if(false == FindEOP(line_num, O, P, K, X, Y, Z)) return false;//line_number = line_number' - yp + offset
	}
	
	Rmat.ReMake(O, P, K);
	CSMMatrix<double> Point(3,1);
	Point(0,0) = col_num - IOParam[0]/2.0;
	Point(1,0) = 0.0;
	Point(2,0) = -IOParam[2];
	CSMMatrix<double> Point2 = Rmat.Rmatrix%Point;
	double r = sqrt(Point2(0,0)*Point2(0,0) + Point2(1,0)*Point2(1,0));
	
	SInterval = Z/r;
	SInterval = SInterval * 0.5;

	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return true;
}