/////////////////////////////////////////////////////////////////////
//GCPManage.cpp
//made by BangBaraBang
/////////////////////////////////////////////////////////////////////
//CGCPManage : GCP management class
/////////////////////////////////////////////////////////////////////////
//revision: 2001-12-03
///////////////////////////////////////////////////////////////////////
//revision: 2002-01-17
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "..\SMDLT.h"
#include "GCPManage.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
//#define new DEBUG_NEW <--MFC
#endif

//////////////////////////////////////////////////////////////////////
// CGCPCoord
//////////////////////////////////////////////////////////////////////

//Constructor
CGCPCoord::CGCPCoord()
{
	next = NULL;
	forward = NULL;
	Init();
}

//Destructor
CGCPCoord::~CGCPCoord()
{
}

//Initialize
void CGCPCoord::Init(void)
{
	enable = true;
	x = y = z = 0;
	ID = 0;
}


//////////////////////////////////////////////////////////////////////
// CICPCoord
//////////////////////////////////////////////////////////////////////

//Constructor
CICPCoord::CICPCoord()
{
	next = NULL;
	forward = NULL;
	Init();
}

//Destructor
CICPCoord::~CICPCoord()
{
}

//Initialize
void CICPCoord::Init(void)
{
	enable = true;
	x = y = 0;
	ID = 0;
}

//////////////////////////////////////////////////////////////////////
// CGCPManage
//////////////////////////////////////////////////////////////////////

//Constructor
CGCPManage::CGCPManage()
{
	num_GCP = 0;
	nextgcp = NULL;
	firstgcp.next = nextgcp;
	normalization = false;
}

//Copy constructor
CGCPManage::CGCPManage(CGCPManage& copy)
{
	num_GCP = 0;
	nextgcp = NULL;
	firstgcp.next = nextgcp;
	normalization = false;

	CGCPCoord* gcpbag;
	gcpbag = &(copy.firstgcp);
	
	for(long i=0;i<copy.GetGCPNum();i++)
	{
		gcpbag = gcpbag->next;
		
		//Insert 함수를 이용한 GCP복사
		InsertGCP(gcpbag->x,gcpbag->y,gcpbag->z,gcpbag->ID);
		//enable 복사
		SetEnableGCP(gcpbag->ID,gcpbag->enable);
	}

}

//Destructor
CGCPManage::~CGCPManage()
{
	EmptyGCP();
}

//substitution operator
void CGCPManage::operator = (CGCPManage& copy)
{
	EmptyGCP();
	CGCPCoord* gcpbag;
	gcpbag = &(copy.firstgcp);
	
	for(long i=0;i<copy.GetGCPNum();i++)
	{
		gcpbag = gcpbag->next;
		
		//Insert 함수를 이용한 GCP복사
		InsertGCP(gcpbag->x,gcpbag->y,gcpbag->z,gcpbag->ID);
		//enable 복사
		SetEnableGCP(gcpbag->ID,gcpbag->enable);
	}

}

//Public member function

//coord normalize
bool CGCPManage::CoordNormalize()
{
	int i;
	double maxX, maxY, maxZ;
	double minX, minY, minZ;

	//coordinates backup and get offset, scale (Image Coordinagtes)
	double X, Y, Z;
	GetGCPCoordbySort(1,&X,&Y,&Z);

	maxX = minX = X;
	maxY = minY = Y;
	maxZ = minZ = Z;
	
	for(i=0; i<(num_GCP); i++)
	{
		GetGCPCoordbySort(i+1,&X,&Y,&Z);

		if(maxX < X)
			maxX = X;
		if(minX > X)
			minX = X;

		if(maxY < Y)
			maxY = Y;
		if(minY > Y)
			minY = Y;

		if(maxZ < Z)
			maxZ = Z;
		if(minZ > Z)
			minZ = Z;
	}

	//Scale_G_X = 2/(maxX - minX);
	//Scale_G_Y = 2/(maxY - minY);
	//Scale_G_Z = 2/(maxZ - minZ);
	//////////////////////////////////
	if((maxX - minX)<(maxY - minY))
	{
		if((maxZ - minZ)<(maxX - minX))
		{
			Scale_G_X = Scale_G_Y = Scale_G_Z = 2/(maxZ - minZ);
		}
		else
		{
			Scale_G_X = Scale_G_Y = Scale_G_Z = 2/(maxX - minX);
		}
	}
	else
	{
		if((maxZ - minZ)<(maxY - minY))
		{
			Scale_G_X = Scale_G_Y = Scale_G_Z = 2/(maxZ - minZ);
		}
		else
		{
			Scale_G_X = Scale_G_Y = Scale_G_Z = 2/(maxY - minY);
		}
	}
	//////////////////////////////////

	Offset_G_X = -(maxX + minX)/2;
	Offset_G_Y = -(maxY + minY)/2;
	Offset_G_Z = -(maxZ + minZ)/2;

	for(i=0; i<(num_GCP); i++)
	{
		CGCPCoord *gcp = GetGCPHandle(GetGCPID(i+1));
		
		gcp->x = (gcp->x + Offset_G_X)*Scale_G_X;
		gcp->y = (gcp->y + Offset_G_Y)*Scale_G_Y;
		gcp->z = (gcp->z + Offset_G_Z)*Scale_G_Z;
	}

	normalization = true;

	return true;
}

bool CGCPManage::CoordNormalize(double scale, double OX, double OY, double OZ)
{
	Scale_G_X = Scale_G_Y = Scale_G_Z = scale;

	Offset_G_X = OX;
	Offset_G_Y = OY;
	Offset_G_Z = OZ;

	for(long i=0; i<(num_GCP); i++)
	{
		CGCPCoord *gcp = GetGCPHandle(GetGCPID(i+1));
		
		gcp->x = (gcp->x + Offset_G_X)*Scale_G_X;
		gcp->y = (gcp->y + Offset_G_Y)*Scale_G_Y;
		gcp->z = (gcp->z + Offset_G_Z)*Scale_G_Z;
	}

	normalization = true;

	return true;
}

//Insert GCP
bool CGCPManage::InsertGCP(double X, double Y, double Z, long ID)
{
	if(num_GCP == 0)
	{
		firstgcp.x = -999;
		firstgcp.y = -999;
		firstgcp.z = -999;
		firstgcp.ID = -999;
		firstgcp.next = new CGCPCoord;
		nextgcp = firstgcp.next;
		nextgcp->forward = &firstgcp;
	}

	if(false == SameIDCheck(ID))
		return false;
	
	num_GCP++;
	nextgcp->x = X;
	nextgcp->y = Y;
	nextgcp->z = Z;
	nextgcp->ID = ID;
	nextgcp->next = new CGCPCoord;
	nextgcp->next->forward = new CGCPCoord;
	nextgcp->next->forward = nextgcp;
	nextgcp = nextgcp->next;
		
	return true;
}

//Delete GCP
bool CGCPManage::DeleteGCP(long ID)
{
	CGCPCoord* targetgcp = GetGCPHandle(ID);
	if(targetgcp != NULL)
	{
		targetgcp->forward->next = targetgcp->next;
		targetgcp->next->forward = targetgcp->forward;
		
		delete targetgcp;
		
		num_GCP--;
		
		return true;
	}
	return false;
}

//Get GCP Coordinates
bool CGCPManage::GetGCPCoord(long ID, double *X, double *Y, double *Z)
{
	CGCPCoord *targetgcp = NULL;
	
	targetgcp = GetGCPHandle(ID);
	if(targetgcp != NULL)
	{
		*X = targetgcp->x;
		*Y = targetgcp->y;
		*Z = targetgcp->z;
	}
	else
	{
		return false;
	}

	return true;
}

//Get GCP Coordinates by sort number
bool CGCPManage::GetGCPCoordbySort(long sort, double *X, double *Y, double *Z)
{
	CGCPCoord *targetgcp = NULL;
	
	targetgcp = GetGCPHandle(GetGCPID(sort));
	if(targetgcp != NULL)
	{
		*X = targetgcp->x;
		*Y = targetgcp->y;
		*Z = targetgcp->z;
	}
	else
	{
		return false;
	}

	return true;
}

//Get GCP Coordinates
bool CGCPManage::SetGCPCoord(long ID, double X, double Y, double Z)
{
	CGCPCoord *targetgcp = NULL;
	
	targetgcp = GetGCPHandle(ID);
	if(targetgcp != NULL)
	{
		targetgcp->x = X;
		targetgcp->y = Y;
		targetgcp->z = Z;		
	}
	else
	{
		return false;
	}

	return true;
}

//Change GCP ID
bool CGCPManage::ChangeGCPID(long old_id, long new_id)
{
	CGCPCoord *targetgcp = NULL;
	
	targetgcp = GetGCPHandle(old_id);
	if(targetgcp != NULL)
	{
		targetgcp->ID = new_id;
	}
	else
	{
		return false;
	}

	return true;
}

//Get GCP ID
bool CGCPManage::GetGCPID(double X, double Y, double Z, long *ID)
{
	CGCPCoord *targetgcp;
	targetgcp = &firstgcp;
	
	for(long i=0; i<num_GCP; i++)
	{
		targetgcp = targetgcp->next;
		if((X==targetgcp->x)&&(Y==targetgcp->y)&&(Z==targetgcp->z))
		{
			*ID = targetgcp->ID;
			return true;
		}
	}
	
	return false;
}

//Get GCP ID
long CGCPManage::GetGCPID(long sort)
{
	if((sort>num_GCP)||(sort<1))
		return -9999;

	CGCPCoord *targetgcp;
	targetgcp = &firstgcp;
		
	for(long i=0; i<sort; i++)
	{
		targetgcp = targetgcp->next;
	}
	
	return targetgcp->ID;
}

//GCP enabled true setting
bool CGCPManage::SetEnableGCP(long ID, bool boolvalue)
{
	CGCPCoord *targetgcp = NULL;
	
	targetgcp = GetGCPHandle(ID);
	if(targetgcp != NULL)
	{
		targetgcp->enable = boolvalue;
	}
	else
	{
		return false;
	}

	return true;
}

//GCP enabled true getting
bool CGCPManage::GetEnableGCP(long ID)
{
	CGCPCoord *targetgcp = NULL;

	targetgcp = GetGCPHandle(ID);
	if(targetgcp != NULL)
	{		
		return targetgcp->enable;
	}
	else
	{
		return false;
	}
}

//Empty GCP Buffer
bool CGCPManage::EmptyGCP(void)
{
	CGCPCoord *targetgcp;
	
	//last gcp pointer
	if(num_GCP == 0)
	{
		return TRUE;
	}

	targetgcp = nextgcp->forward;

	if(targetgcp == NULL)
	{
		return false;
	}
	
	for(long i=0; i<num_GCP; i++)
	{
		targetgcp = targetgcp->forward;
		delete targetgcp->next;
		targetgcp->next = NULL;
	}

	targetgcp->Init();
	
	num_GCP = 0;
	
	return true;
}

//Get GCP number
long CGCPManage::GetGCPNum(void)
{
	return num_GCP;
}

//Set GCP number
void CGCPManage::SetGCPNum(long num)
{
	EmptyGCP();

	for(long i=0; i<num; i++)
	{
		InsertGCP(0.0,0.0,0.0,i);
	}
}

//Get enabled GCP number
long CGCPManage::GetEnabledGCPNum(void)
{
	long num_disabledGCP= 0;

	for(long i=0; i<num_GCP; i++)
	{
		bool value;
		value = GetEnableGCP(GetGCPID(i+1));
		if(false == value)
			num_disabledGCP++;
	}

	return (num_GCP - num_disabledGCP);
}

//Get Normalized Coord
void CGCPManage::GetNormalCoord(double x, double y, double z,
					double &nx, double &ny, double &nz)
{
	nx = (x + Offset_G_X)*Scale_G_X;
	ny = (y + Offset_G_Y)*Scale_G_Y;
	nz = (z + Offset_G_Z)*Scale_G_Z;
}

//Get Original Coord
void CGCPManage::GetUnNormalCoord(double nx, double ny, double nz,
								  double &x, double &y, double &z)
{
	x = (nx/Scale_G_X) - Offset_G_X;
	y = (ny/Scale_G_Y) - Offset_G_Y;
	z = (nz/Scale_G_Z) - Offset_G_Z;
}

//Private member function

//Get a certain GCP handle(pointer)
CGCPCoord* CGCPManage::GetGCPHandle(long ID)
{
	CGCPCoord* gcpbag;
	gcpbag = &firstgcp;

	if(-9999 == ID)
		return NULL;
		
	for(long i=0; i<num_GCP; i++)
	{
		gcpbag = gcpbag->next;
		if(ID == gcpbag->ID)
		{
			return gcpbag;
		}
	}
	
	return NULL;
}

//Check Same GCP ID
bool CGCPManage::SameIDCheck(long ID)
{
	CGCPCoord* gcpbag;
	gcpbag = &firstgcp;

	if(-9999 == ID)
		return false;
		
	for(long i=0; i<num_GCP; i++)
	{
		gcpbag = gcpbag->next;
		if(ID == gcpbag->ID)
		{
			return false;
		}
	}
	
	return true;
}


//////////////////////////////////////////////////////////////////////
// CICPManage
//////////////////////////////////////////////////////////////////////

//Constructor
CICPManage::CICPManage()
{
	num_ICP = 0;
	nexticp = NULL;
	firsticp.next = nexticp;
	normalization = false;
}

//copy constructor
CICPManage::CICPManage(CICPManage& copy)
{
	num_ICP = 0;
	nexticp = NULL;
	firsticp.next = nexticp;
	normalization = false;

	CICPCoord* icpbag;
	icpbag = &(copy.firsticp);
	
	for(long i=0;i<copy.GetICPNum();i++)
	{
		icpbag = icpbag->next;
		
		//Insert 함수를 이용한 ICP복사
		InsertICP(icpbag->x, icpbag->y, icpbag->ID);
		//enable 복사
		SetEnableICP(icpbag->ID,icpbag->enable);
	}

}

//Destructor
CICPManage::~CICPManage()
{
	EmptyICP();
}

//substitution operator
void CICPManage::operator = (CICPManage& copy)
{
	EmptyICP();
	CICPCoord* icpbag;
	icpbag = &(copy.firsticp);
	
	for(long i=0;i<copy.GetICPNum();i++)
	{
		icpbag = icpbag->next;
		
		//Insert 함수를 이용한 ICP복사
		InsertICP(icpbag->x, icpbag->y, icpbag->ID);
		//enable 복사
		SetEnableICP(icpbag->ID,icpbag->enable);
	}

}

//Public member function


//coord normalize
void CICPManage::CoordNormalize()
{
	int i;
	double maxX, maxY;
	double minX, minY;

	//coordinates backup and get offset, scale (Image Coordinagtes)
	double X, Y;
	GetICPCoordbySort(1,&X,&Y);

	maxX = minX = X;
	maxY = minY = Y;
		
	for(i=0; i<(num_ICP); i++)
	{
		GetICPCoordbySort(i+1,&X,&Y);

		if(maxX < X)
			maxX = X;
		if(minX > X)
			minX = X;

		if(maxY < Y)
			maxY = Y;
		if(minY > Y)
			minY = Y;
	}

	//Scale_I_X = 2/(maxX - minX);
	//Scale_I_Y = 2/(maxY - minY);
	//////////////////////////////////
	if((maxX - minX)<(maxY - minY))
	{
		Scale_I_X = Scale_I_Y = 2/(maxX - minX);
	}
	else
	{
		Scale_I_X = Scale_I_Y = 2/(maxY - minY);
	}
	//////////////////////////////////

	Offset_I_X = -(maxX + minX)/2;
	Offset_I_Y = -(maxY + minY)/2;

	for(i=0; i<(num_ICP); i++)
	{
		CICPCoord *icp = GetICPHandle(GetICPID(i+1));
		
		icp->x = (icp->x + Offset_I_X)*Scale_I_X;
		icp->y = (icp->y + Offset_I_Y)*Scale_I_Y;
	}

	normalization = true;
}

bool CICPManage::CoordNormalize(double scale, double OX, double OY)
{
	Scale_I_X = Scale_I_Y = scale;

	Offset_I_X = OX;
	Offset_I_Y = OY;
	
	for(long i=0; i<(num_ICP); i++)
	{
		CICPCoord *icp = GetICPHandle(GetICPID(i+1));
		
		icp->x = (icp->x + Offset_I_X)*Scale_I_X;
		icp->y = (icp->y + Offset_I_Y)*Scale_I_Y;
	}

	normalization = true;

	return true;
}

//Insert ICP
bool CICPManage::InsertICP(double X, double Y, long ID)
{
	if(num_ICP == 0)
	{
		firsticp.x = -999;
		firsticp.y = -999;
		firsticp.ID = -999;
		firsticp.next = new CICPCoord;
		nexticp = firsticp.next;
		nexticp->forward = &firsticp;
	}

	if(false == SameIDCheck(ID))
		return false;
	
	num_ICP++;
	nexticp->x = X;
	nexticp->y = Y;
	nexticp->ID = ID;
	nexticp->next = new CICPCoord;
	nexticp->next->forward = new CICPCoord;
	nexticp->next->forward = nexticp;
	nexticp = nexticp->next;
	
	return true;
}

//Delete ICP
bool CICPManage::DeleteICP(long ID)
{
	CICPCoord* targeticp = GetICPHandle(ID);
	if(targeticp != NULL)
	{
		targeticp->forward->next = targeticp->next;
				
		targeticp->next->forward = targeticp->forward;
		
		delete targeticp;

		num_ICP--;
		
		return true;
	}
	return false;
}

//Get ICP Coordinates
bool CICPManage::GetICPCoord(long ID, double *X, double *Y)
{
	CICPCoord *targeticp = NULL;
	
	targeticp = GetICPHandle(ID);
	if(targeticp != NULL)
	{
		*X = targeticp->x;
		*Y = targeticp->y;
	}
	else
	{
		return false;
	}

	return true;
}

//Get ICP Coordinates by sort number
bool CICPManage::GetICPCoordbySort(long sort, double *X, double *Y)
{
	CICPCoord *targeticp = NULL;
	
	targeticp = GetICPHandle(GetICPID(sort));
	if(targeticp != NULL)
	{
		*X = targeticp->x;
		*Y = targeticp->y;
	}
	else
	{
		return false;
	}

	return true;
}

//Get ICP Coordinates
bool CICPManage::SetICPCoord(long ID, double X, double Y)
{
	CICPCoord *targeticp = NULL;
	
	targeticp = GetICPHandle(ID);
	if(targeticp != NULL)
	{
		targeticp->x = X;
		targeticp->y = Y;
	}
	else
	{
		return false;
	}

	return true;
}

//Change ICP ID
bool CICPManage::ChangeICPID(long old_id, long new_id)
{
	CICPCoord *targeticp = NULL;
	
	targeticp = GetICPHandle(old_id);
	if(targeticp != NULL)
	{
		targeticp->ID = new_id;
	}
	else
	{
		return false;
	}

	return true;
}

//Get ICP ID
bool CICPManage::GetICPID(double X, double Y, long *ID)
{
	CICPCoord *targeticp;
	targeticp = &firsticp;
		
	for(long i=0; i<num_ICP; i++)
	{
		targeticp = targeticp->next;
		double Tx, Ty;
		Tx = targeticp->x;
		Ty = targeticp->y;
		if(X==Tx)
		{
			if(Y==Ty)
			{
				*ID = targeticp->ID;
				return true;
			}
		}
	}
	
	return false;
}

//Get ICP ID
long CICPManage::GetICPID(long sort)
{
	CICPCoord *targeticp;
	targeticp = &firsticp;

	if((sort>num_ICP)||(sort<1))
		return -9999;
		
	for(long i=0; i<sort; i++)
	{
		targeticp = targeticp->next;
	}
	
	return targeticp->ID;
}

//ICP enabled true setting
bool CICPManage::SetEnableICP(long ID, bool boolvalue)
{
	CICPCoord *targeticp = NULL;
	
	targeticp = GetICPHandle(ID);
	if(targeticp != NULL)
	{
		targeticp->enable = boolvalue;
	}
	else
	{
		return false;
	}

	return true;
}

//ICP enabled true getting
bool CICPManage::GetEnableICP(long ID)
{
	CICPCoord *targeticp = NULL;

	targeticp = GetICPHandle(ID);
	if(targeticp != NULL)
	{		
		return targeticp->enable;
	}
	else
	{
		return false;
	}
}

//Empty ICP Buffer
bool CICPManage::EmptyICP(void)
{
	CICPCoord *targeticp;
	
	//last icp pointer
	if(num_ICP == 0)
	{
		return TRUE;
	}

	targeticp = nexticp->forward;
	
	if(targeticp == NULL)
	{
		return false;
	}
	
	for(long i=0; i<num_ICP; i++)
	{
		targeticp = targeticp->forward;
		delete targeticp->next;
		targeticp->next = NULL;
	}
	targeticp->Init();
	
	num_ICP = 0;	

	return true;
}

//Get ICP number
long CICPManage::GetICPNum(void)
{
	return num_ICP;
}

//Set ICP number
void CICPManage::SetICPNum(long num)
{
	EmptyICP();

	for(long i=0; i<num; i++)
	{
		InsertICP(0.0,0.0,i);
	}
}

//Get enabled ICP number
long CICPManage::GetEnabledICPNum(void)
{
	long num_disabledICP= 0;

	for(long i=0; i<num_ICP; i++)
	{
		bool value;
		value = GetEnableICP(GetICPID(i+1));
		if(false == value)
			num_disabledICP++;
	}

	return (num_ICP - num_disabledICP);
}

//Get Normalized Coord
void CICPManage::GetNormalCoord(double x, double y,
								double &nx, double &ny)
{
	nx = (x + Offset_I_X)*Scale_I_X;
	ny = (y + Offset_I_Y)*Scale_I_Y;
}

//Get Original Coord
void CICPManage::GetUnNormalCoord(double nx, double ny, 
								  double &x, double &y)
{
	x = (nx/Scale_I_X) - Offset_I_X;
	y = (ny/Scale_I_Y) - Offset_I_Y;
}

//Private member function

//Get a certain ICP handle(pointer)
CICPCoord* CICPManage::GetICPHandle(long ID)
{
	CICPCoord* icpbag;
	icpbag = &firsticp;

	if(-9999 == ID)
		return NULL;
		
	for(long i=0; i<num_ICP; i++)
	{
		icpbag = icpbag->next;
		if(ID == icpbag->ID)
		{
			return icpbag;
		}
	}
	
	return NULL;
}

//Check Same GCP ID
bool CICPManage::SameIDCheck(long ID)
{
	CICPCoord* icpbag;
	icpbag = &firsticp;

	if(-9999 == ID)
		return false;
		
	for(long i=0; i<num_ICP; i++)
	{
		icpbag = icpbag->next;
		if(ID == icpbag->ID)
		{
			return false;
		}
	}
	
	return true;
}
