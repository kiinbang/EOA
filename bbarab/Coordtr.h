/******************************************************************
*                                                                 *
*   coordtr.h                                                     *
*                                                                 *
*******************************************************************
*                                                                 *
*   Header file for Coordinate Transformation.                    *
*                                                                 *
*   This header file contains functions as follows,               *
*                                                                 *
*     void geodet2geocen(lambda, phi, h, &x, &y, &z, a, b)        *
*     {  geodetic coord. -> geocentric coord. }                   *
*                                                                 *
*     void geocen2geodet(x, y, z, &lambda, &phi, &h, a, b)        *
*     {  geocentric coord. -> geodetic coord. }                   *
*                                                                 *
*     void wgs2bessel(xw,yw,zw,xb,yb,zb)                          *
*     {  WGS84 geocen. coord. -> Bessel geocen. coord. }          *
*                                                                 *
*     void bessel2wgs(xb,yb,zb,xw,yw,zw)                          *
*     {  Bessel geocen. coord. -> WGS84 geocen. coord. }          *
*                                                                 *
*     int tmproj(lambda, phi, xnorth, yeast)                      *
*     {  geodetic coord. -> plane coord.                          *
*        (using the TM projection for the Korean datum)  }        *
*                                                                 *
*     int utmproj(lambda, phi, north, east, a, b)                 *
*     {  geodetic coord. -> UTM coord. }                          *
*                                                                 *
*     void gauss_kruger(lambda, phi, x, y, centmeri, a, b)        *
*     {  geodetic coord. -> TM projected plane coord. }           *
*                                                                 *
*     int inv_tmproj(xnorth, yeast, lambda, phi, oflag)           *
*     {  plane coord. -> geodetic coord.                          *
*        (using the TM projection for the Korean datum)           *
*                                                                 *
*     int inv_utmproj(lambda, phi, north, east, a, b)             *
*     {  UTM coord. -> geodetic coord. }                          *
*                                                                 *
*     void inv_gauss_kruger(lambda, phi, x, y, centmeri, a, b)    *
*     {  TM projected plane coord. -> geodetic coord. }           *
*                                                                 *
*******************************************************************
*                                                                 *
*        Programmed by Jeong, Soo                                 *
*        Date : FEB. 1996.                                        *
*                                                                 *
******************************************************************/

#if !defined(___COORDTR___)
#define ___COORDTR___

#include <stdio.h>
#include <math.h>

#define   PI      (atan((double)1.)*4.)
#define   D2R     (PI/180.)
#define   R2D     (180./PI)

void geodet2geocen(double lambda, double phi, double h, double* x, double* y, double* z, double a, double b);
void geocen2geodet(double x, double y, double z, double* lambda, double* phi, double* h, double a, double b);
void wgs2bessel(double xw,double yw,double zw,double* xb,double* yb, double* zb);
void bessel2wgs(double xb,double yb,double zb,double* xw,double* yw,double* zw);
int tmproj(double lambda, double phi, double* xnorth, double* yeast);
void geodet2geocen(double lambda, double phi, double h, double* x, double* y, double* z, double a, double b);
void geocen2geodet(double x, double y, double z, double* lambda, double* phi, double* h, double a, double b);
void wgs2bessel(double xw,double yw,double zw,double* xb,double* yb, double* zb);
void bessel2wgs(double xb,double yb,double zb,double* xw,double* yw,double* zw);
int tmproj(double lambda, double phi, double* xnorth, double* yeast);
int utmproj(double lambda, double phi, double* north, double* east, double a, double b);
bool utmproj(double lambda, double phi, double* north, double* east, double a, double b, int given_zone);
void gauss_kruger(double lambda, double phi, double* x, double* y, double centmeri, double a, double b);
int inv_tmproj(double xnorth, double yeast, double* lambda, double* phi, int oflag);
int inv_utmproj( double north, double east, double* lambda, double* phi, double a, double b, int zone, int hemi);
void inv_gauss_kruger(double x, double y, double* lambda, double *phi, double centmeri, double a, double b);
double ARCTAN(double a, double b);
double deg2dms(double degree);
double dms2deg(double dms);


#endif // !defined(___COORDTR___)