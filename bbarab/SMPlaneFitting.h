/*
 * Copyright (c) 2005-2007, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(__PLANE_FITTING__BBARAB__)
#define __PLANE_FITTING__BBARAB__

#include "SMmatrix.h"
#include "SMList.h"
#include "Collinearity.h"
#include "SMDataStruct.h"
#include "KDTree.h"

using namespace SMATICS_BBARAB;

/**@class CSMPlaneFitting<br>
*@brief point cloud plane fitting class
*/
class CSMPlaneFitting
{
public:

	/**CSMPlaneFitting
	* Description	    : constructor
	* Return type		: 
	*/
	CSMPlaneFitting() {}

	/**~CSMPlaneFitting
	* Description	    : destructor
	* Return type		: 
	*/
	virtual ~CSMPlaneFitting() {}

	/**~PlaneFitting
	* Description	    : plane fitting function
	*@param int num_point	: number of points
	*@param double* X		: X coordinates list
	*@param double* Y		: Y coordinates list
	*@param double* Z		: Z coordinates list
	*@param double &a		: plane parameter
	*@param double &b		: plane parameter
	*@param double &c		: plane parameter
	*@param double &sigma	: fitting precision
	*@param double* dist	: distance of each point to the extracted plane
	* Return type bool 
	*/
	static bool PlaneFitting(bool brepeat, double threshold, int num_points, double* X, double* Y, double* Z, double &a, double &b, double &c, double &pos_var, double &dist2origin, double *errordist, int maxiteration)
	{
		///////////////////////////
		//aX + bY + cZ + 1 = 0
		///////////////////////////
		if(num_points < 4) return false;
		
		int i;
		
		CSMMatrix<double> Amat(num_points, 3), Lmat(num_points, 1, -1.0), Xmat;
		CSMMatrix<double> AT, V;
		CSMMatrix<double> ATWeA, ATWeL, We(num_points, num_points, 0.0);
		We.Identity(1.0);
		
		int num_availpoints = num_points;

		double pos_var0 = 1.0e99;

		double max_dist = 0.0, max_dist0 = 0.0;

		int num_iter = 0;

		_Rmat_ Rot(0, 0, 0);

		while(num_iter < maxiteration)
		{
			num_availpoints = num_points;

			max_dist = 0.0;

			CSMMatrix<double> P(3,1);
			P(0,0) = X[i]; P(1,0) = Y[i]; P(2,0) = Z[i];
			P = Rot.Rmatrix%P;

			for(i=0; i<num_points; i++)
			{
				Amat(i,0) = P(0,0);
				Amat(i,1) = P(1,0);
				Amat(i,2) = P(2,0);
			}

			AT = Amat.Transpose();
			ATWeA = AT%We%Amat;
			ATWeL = AT%We%Lmat;
			Xmat = ATWeA.Inverse()%ATWeL;
			a = Xmat(0,0); b = Xmat(1,0); c = Xmat(2,0);
			V = Amat%Xmat - Lmat;
			pos_var = (V.Transpose()%We%V)(0,0)/(num_points - 3);
			
			//origin to plane
			double sqrtabc = sqrt(a*a + b*b + c*c);
			dist2origin = 1.0/sqrtabc;
			
			for(i=0; i<num_points; i++)
			{
				//error distance (point to plane)
				errordist[i] = (a*X[i] + b*Y[i] + c*Z[i] + 1)/sqrtabc;

				if(max_dist<fabs(errordist[i])) max_dist = fabs(errordist[i]);
				
				if(fabs(errordist[i]) > threshold)
				{
					We(i, i) = 0.0;
					num_availpoints --;
				}
				else
				{
					We(i, i) = 1/fabs(errordist[i]);
				}
			}

			if(num_availpoints < 4) break;
			if(fabs(pos_var - pos_var0) < 1.0e-20) break;
			pos_var0 = pos_var;
			if(fabs(max_dist - max_dist0) < 0.000001) break; 
			max_dist0 = max_dist;
			
			//normal vector and distance
			//ax + by + cz + 1 = 0
			double l = -a*dist2origin;
			double m = -b*dist2origin;
			double n = -c*dist2origin;
			
			CSMMatrix<double> dXYZ(3,1);
			dXYZ(0,0) = l*dist2origin; dXYZ(1,0) = m*dist2origin; dXYZ(2,0) = n*dist2origin;

			//rotation to the reference
			dXYZ = Rot.Rmatrix.Transpose()%dXYZ;

			//a, b, c in reference coordinate system
			double dist2origin_2 = dist2origin*dist2origin;
			a = -dXYZ(0,0)/dist2origin_2;
			b = -dXYZ(1,0)/dist2origin_2;
			c = -dXYZ(2,0)/dist2origin_2;

			//rotation angle to the plane
			double dYZ = sqrt(dXYZ(1,0)*dXYZ(1,0) + dXYZ(2,0)*dXYZ(2,0));
			double OofX = -atan2(dXYZ(1,0), dXYZ(2,0));
			double PofY = atan2(dXYZ(0,0), dYZ);

			Rot.ReMake(OofX, PofY, 0);

			num_iter ++;

			if(brepeat == false) break;
		}
		
		return true;
	}

	static bool PlaneFitting_old(bool brepeat, double threshold, CSMList<_Point_> pointlist, double &a, double &b, double &c, double &pos_var, double &dist2origin, double *errordist, int maxiteration)
	{
		///////////////////////////
		//aX + bY + cZ + 1 = 0
		///////////////////////////
		int num_points = pointlist.GetNumItem();
		
		if(num_points < 4) return false;
		
		int i;
		
		CSMMatrix<double> Xmat;
		CSMMatrix<double> Nmat, Cmat;
		CSMMatrix<double> We(num_points, num_points, 0.0);
		We.Identity(1.0);
		
		int num_availpoints = num_points;

		double pos_var0 = 1.0e99;

		double max_dist = 0.0, max_dist0 = 0.0;

		int num_iter = 0;

		_Mmat_ Rot(0, 0, 0);

		while(num_iter < maxiteration)
		{
			num_availpoints = num_points;

			max_dist = 0.0;

			Nmat.Resize(3,3,0);

			Cmat.Resize(3,1,0);
			
			CSMMatrix<double> P(3,1);
			DATA<_Point_>* pos = NULL;
			for(i=0; i<num_points; i++)
			{
				pos = pointlist.GetNextPos(pos);
				
				P(0,0) = pos->value.X;
				P(1,0) = pos->value.Y;
				P(2,0) = pos->value.Z;
				
				P = Rot.Mmatrix%P;
				
				CSMMatrix<double> amat(1,3);
				amat(0,0) = P(0,0);
				amat(0,1) = P(1,0);
				amat(0,2) = P(2,0);

				CSMMatrix<double> lmat(1,1);
				lmat(0,0) = -1;
				
				CSMMatrix<double> we = We.Subset(i, i, 1, 1);

				CSMMatrix<double> n = amat.Transpose()%we%amat;

				CSMMatrix<double> c = amat.Transpose()%we%lmat;

				Nmat = Nmat + n;
				Cmat = Cmat + c;
			}

			Xmat = Nmat.Inverse()%Cmat;
			a = Xmat(0,0); b = Xmat(1,0); c = Xmat(2,0);
			CSMMatrix<double> E = Nmat%Xmat - Cmat;

			pos_var = (E.Transpose()%E)(0,0)/(num_points - 3);
			
			//origin to plane
			double sqrtabc = sqrt(a*a + b*b + c*c);
			dist2origin = 1.0/sqrtabc;
			
			pos = NULL;

			for(i=0; i<num_points; i++)
			{
				pos = pointlist.GetNextPos(pos);

				//error distance (point to plane)
				errordist[i] = (a*pos->value.X + b*pos->value.Y + c*pos->value.Z + 1)/sqrtabc;

				if(max_dist<fabs(errordist[i])) max_dist = fabs(errordist[i]);
				
				if(fabs(errordist[i]) > threshold)
				{
					We(i, i) = 0.0;
					num_availpoints --;
				}
				else
				{
					We(i, i) = 1/fabs(errordist[i]);
				}
			}

			if(num_availpoints < 4) break;
			if(fabs(pos_var - pos_var0) < 1.0e-20) break;
			pos_var0 = pos_var;
			if(fabs(max_dist - max_dist0) < 0.000001) break; 
			max_dist0 = max_dist;
			
			//normal vector and distance
			//ax + by + cz + 1 = 0
			double l = -a*dist2origin;
			double m = -b*dist2origin;
			double n = -c*dist2origin;
			
			CSMMatrix<double> dXYZ(3,1);
			dXYZ(0,0) = l*dist2origin; dXYZ(1,0) = m*dist2origin; dXYZ(2,0) = n*dist2origin;

			//rotation to the reference
			dXYZ = Rot.Mmatrix.Transpose()%dXYZ;

			//a, b, c in reference coordinate system
			double dist2origin_2 = dist2origin*dist2origin;
			a = -dXYZ(0,0)/dist2origin_2;
			b = -dXYZ(1,0)/dist2origin_2;
			c = -dXYZ(2,0)/dist2origin_2;

			//rotation angle to the plane
			double dYZ = sqrt(dXYZ(1,0)*dXYZ(1,0) + dXYZ(2,0)*dXYZ(2,0));
			double OofX = -atan2(dXYZ(1,0), dXYZ(2,0));
			double PofY = atan2(dXYZ(0,0), dYZ);

			Rot.ReMake(OofX, PofY, 0);

			num_iter ++;

			if(brepeat == false) break;
		}
		
		return true;
	}

	static bool PlaneFitting(double threshold, CSMList<_Point_> pointlist, double &a, double &b, double &c, double &pos_var, double &dist2origin, double *errordist, int maxiteration)
	{
		///////////////////////////
		//aX + bY + cZ + 1 = 0
		///////////////////////////
		int num_points = pointlist.GetNumItem();
		
		if(num_points < 4) return false;
		
		int i;
		
		CSMMatrix<double> Xmat;
		CSMMatrix<double> Nmat, Cmat;
		CSMMatrix<double> We(num_points, num_points, 0.0);
		We.Identity(1.0);
		
		int num_availpoints = num_points;

		double pos_var0 = 1.0e99;
		pos_var = 1.0e99;
		bool bContinue = true;
		bool retval = true;

		double max_dist = 0.0, max_dist0 = 0.0;

		int num_iter = 0;

		_Mmat_ Rot(0, 0, 0);

		double a0=0, b0=0, c0=1;

		//
		//centroid
		//
		double sum_x=0, sum_y=0, sum_z=0;
		double cx, cy, cz;
		
		DATA<_Point_>* pos = NULL;
		
		for(i=0; i<num_points; i++)
		{
			pos = pointlist.GetNextPos(pos);
			
			sum_x += pos->value.X;
			sum_y += pos->value.Y;
			sum_z += pos->value.Z;
		}
		
		cx = sum_x/num_points;
		cy = sum_y/num_points;
		cz = sum_z/num_points;
		
		num_availpoints = num_points;

		bool bIterNum = true;
		bool bNumAvailablePoints = true;

		do
		{
			pos_var0 = pos_var;			

			max_dist = 0.0;

			Nmat.Resize(3,3,0);

			Cmat.Resize(3,1,0);

			CSMMatrix<double> amat(1,3);
			CSMMatrix<double> lmat(1,1);
			CSMMatrix<double> Ni, Ci;
			
			CSMMatrix<double> P(3,1);

			CSMMatrix<double> W(1,1,0);

			CSMMatrix<double> etpe(1,1,0);

			pos = NULL;
			
			for(i=0; i<num_points; i++)
			{
				pos = pointlist.GetNextPos(pos);
				
				P(0,0) = pos->value.X - cx;
				P(1,0) = pos->value.Y - cy;
				P(2,0) = pos->value.Z - cz;

				//Rotate points
				P = Rot.Mmatrix%P;
				
				amat(0,0) = P(0,0);
				amat(0,1) = P(1,0);
				amat(0,2) = P(2,0);

				//ax + by + cz = 0

				lmat(0,0) = -a0*P(0,0) -b0*P(1,0) -c0*P(2,0);
				
				CSMMatrix<double> we = We.Subset(i, i, 1, 1);
				W(0,0) += we(0,0);

				Ni = amat.Transpose()%we%amat;
				Ci = amat.Transpose()%we%lmat;
				etpe(0,0) += (lmat.Transpose()%we%lmat)(0,0);

				Nmat = Nmat + Ni;
				Cmat = Cmat + Ci;
			}

			amat(0,0) = 2*a0;
			amat(0,1) = 2*b0;
			amat(0,2) = 2*c0;

			lmat(0,0) = 1 - (a0*a0 + b0*b0 + c0*c0);

			Ni = amat.Transpose()%W%amat;
			Ci = amat.Transpose()%W%lmat;
			etpe(0,0) += (lmat.Transpose()%W%lmat)(0,0);

			Nmat = Nmat + Ni;
			Cmat = Cmat + Ci;

			Xmat = Nmat.Inverse()%Cmat;
			a0 = a0 + Xmat(0,0);
			b0 = b0 + Xmat(1,0);
			c0 = c0 + Xmat(2,0);

			pos_var = etpe(0,0)/(num_availpoints - 3);

			double sd1 = sqrt(pos_var);

			if(fabs(pos_var-pos_var0) < 1.0e-6)
			{
				bContinue = false;
				retval = true;
			}
			
			//ax+by+cz+d=0 where d is zero
			//Dist point2plane: fabs(ax1 + by1 + cz1 + d=0)/sqrt(a*a + b*b + c*c) 
			//                = fabs(ax1 + by1 + cz1 + d=0) because sqrt(a*a + b*b + c*c) = 1.0
			
			pos = NULL;

			num_availpoints = num_points;//reset num_available points

			//
			//Weight modification using studentized residual for robust LS
			//
			for(i=0; i<num_points; i++)
			{
				pos = pointlist.GetNextPos(pos);
				
				P(0,0) = pos->value.X - cx;
				P(1,0) = pos->value.Y - cy;
				P(2,0) = pos->value.Z - cz;
				
				//Rotate points
				P = Rot.Mmatrix%P;

				//error distance (point to plane)
				errordist[i] = a0*P(0,0) + b0*P(1,0) + c0*P(2,0);
				errordist[i] /= sqrt(a0*a0 + b0*b0 + c0*c0);

				if(max_dist<fabs(errordist[i])) max_dist = fabs(errordist[i]);
				
				if(fabs(errordist[i]) > threshold)
				{
					We(i, i) = 0.0;
					num_availpoints --;
				}
				else
				{
					We(i, i) = sd1/fabs(errordist[i]);//using studentized residuals
				}
			}

			if(num_availpoints < 4) 
			{
				bContinue = false;
				bNumAvailablePoints = false;
				retval = false;
			}

			if(fabs(max_dist - max_dist0) < 0.000001) 
			{
				bContinue = false;
				retval = true;
			}

			max_dist0 = max_dist;
			
			//normal vector and distance
			//ax + by + cz = 0
			
			CSMMatrix<double> norm(3,1);
			norm(0,0) = a0;
			norm(1,0) = b0;
			norm(2,0) = c0;

			//inverse-rotation for normal vector
			norm = Rot.Mmatrix.Transpose()%norm;

			//a, b, c in mapping frame
			a = norm(0,0);
			b = norm(1,0);
			c = norm(2,0);

			//rotation angle to the plane
			double dYZ = sqrt(b*b + c*c);
			double OofX = -atan2(b, c);
			double PofY = atan2(a, dYZ);

			Rot.ReMake(OofX, PofY, 0);

			if(num_iter >= maxiteration)
			{
				bContinue = false;
				bIterNum = false;
				retval = false;
			}
			else
			{
				num_iter ++;
			}			

		}while(bContinue);

		//ax + by + cz + (-a*cx - b*cy - c*cz) = 0
		dist2origin = fabs(-a*cx - b*cy - c*cz);

		//a'x + b'y + c'z + 1 = 0
		double temp = -a*cx - b*cy - c*cz;

		a = a/temp;
		b = b/temp;
		c = c/temp;
				
		return retval;
	}
};

#endif // !defined(__PLANE_FITTING__BBARAB__)