////////////////////////////////////////////////////////////////
//DLT.cpp
//made by bbarab
//revision: 2001-09-17
///////////////////////////////////////////////////////////////
//revision: 2001-12-04
///////////////////////////////////////////////////////////////////////
//GCP Management update
/////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SMaticsDLT.h"

#define NUM_DLT_PARAM 11
#define NUM_SATDLT_PARAM_LEVEL1 11
#define NUM_SATDLT_PARAM_LEVEL2 17
#define NUM_SATDLT_PARAM_LEVEL2_2 13
#define NUM_SATDLT_PARAM_LEVEL2_3 15

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CDLT
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

//Constructor
CDLT::CDLT()
{
	GCPCoord = NULL;
	ImageCoord = NULL;
	GCPCoord_Old = NULL;
	ImageCoord_Old = NULL;
	correlation_coefficient = NULL;
	end_iter = -1;
	enable_normalization = false;
}

//Destructor
CDLT::~CDLT()
{
	if(GCPCoord != NULL)
	{
		delete[] GCPCoord;
		GCPCoord = NULL;
	}
	if(ImageCoord != NULL)
	{
		delete[] ImageCoord;
		ImageCoord = NULL;
	}
	if(GCPCoord_Old != NULL)
	{
		delete[] GCPCoord_Old;
		GCPCoord_Old = NULL;
	}
	if(ImageCoord_Old != NULL)
	{
		delete[] ImageCoord_Old;
		ImageCoord_Old = NULL;
	}
	if(correlation_coefficient != NULL)
	{
		delete[] correlation_coefficient;
		correlation_coefficient = NULL;
	}

	gcp.EmptyGCP();

	icp.EmptyICP();
}

//Initialize DLT Module
void CDLT::InitDLTModule()
{
	if(GCPCoord != NULL)
	{
		delete[] GCPCoord;
		GCPCoord = NULL;
	}
	if(ImageCoord != NULL)
	{
		delete[] ImageCoord;
		ImageCoord = NULL;
	}
	if(GCPCoord_Old != NULL)
	{
		delete[] GCPCoord_Old;
		GCPCoord_Old = NULL;
	}
	if(ImageCoord_Old != NULL)
	{
		delete[] ImageCoord_Old;
		ImageCoord_Old = NULL;
	}
	if(correlation_coefficient != NULL)
	{
		delete[] correlation_coefficient;
		correlation_coefficient = NULL;
	}

	gcp.EmptyGCP();

	icp.EmptyICP();

}

//Do modeling DLT
void CDLT::Solve(bool nonlinear, bool coordnormal, unsigned int maxiteration, double maxcorrection)
{
	SetCoordNormalize(coordnormal);

	SetGCP();

	Solve_linear();
	if(nonlinear == true)
	{
		Solve_nonlinear(maxiteration, maxcorrection);
	}
}

//Do modeling linear_DLT
void CDLT::Solve_linear()
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
	
	int i;
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_DLT_PARAM);
	L.Resize(num_GCP*2);

	//A and L matrix
	for(i=0;i<num_GCP;i++)
	{
		//image coordinates
		IP.x = *(ImageCoord + i*2);
		IP.y = *(ImageCoord + i*2 + 1);
		//ground coordinates
		GP.x = *(GCPCoord + i*3);
		GP.y = *(GCPCoord + i*3 + 1);
		GP.z = *(GCPCoord + i*3 + 2);

		A(i*2,0) = GP.x;
		A(i*2,1) = GP.y;
		A(i*2,2) = GP.z;
		A(i*2,3) = 1;
		A(i*2,4) = 0;
		A(i*2,5) = 0;
		A(i*2,6) = 0;
		A(i*2,7) = 0;
		A(i*2,8) =  -(GP.x * IP.x);
		A(i*2,9) =  -(GP.y * IP.x);
		A(i*2,10) = -(GP.z * IP.x);

		L(i*2) = IP.x;

		A(i*2+1,0) = 0;
		A(i*2+1,1) = 0;
		A(i*2+1,2) = 0;
		A(i*2+1,3) = 0;
		A(i*2+1,4) = GP.x;
		A(i*2+1,5) = GP.y;
		A(i*2+1,6) = GP.z;
		A(i*2+1,7) = 1;
		A(i*2+1,8) =  -(GP.x * IP.y);
		A(i*2+1,9) =  -(GP.y * IP.y);
		A(i*2+1,10) = -(GP.z * IP.y);

		L(i*2+1) = IP.y;
	}

	AT = A.Transpose();
	Ninv = (AT%A).Inverse();
	ATL = AT%L;
	X = Ninv%ATL;

	param.A1 = X(0);
	param.B1 = X(1);
	param.C1 = X(2);
	param.D1 = X(3);
	param.A2 = X(4);
	param.B2 = X(5);
	param.C2 = X(6);
	param.D2 = X(7);
	param.A3 = X(8);
	param.B3 = X(9);
	param.C3 = X(10);
	
	AX = A%X;
	//residual
	V = L - AX;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_DLT_PARAM;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);

}

//Do modeling nonlinear_DLT
void CDLT::Solve_nonlinear(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;

	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;

	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_DLT_PARAM);
	L.Resize(num_GCP*2);

	do{
		iteration ++;
		//A and L matrix
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = *(ImageCoord + i*2);
			IP.y = *(ImageCoord + i*2 + 1);
			//ground coordinates
			GP.x = *(GCPCoord + i*3);
			GP.y = *(GCPCoord + i*3 + 1);
			GP.z = *(GCPCoord + i*3 + 2);

			double denominator;
			double numerator1;
			double numerator2;
			double n1_d;
			double n2_d;
			double n1_dd;
			double n2_dd;

			denominator  = (param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1);
			numerator1   = (param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1);
			numerator2   = (param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2);

			n1_d = numerator1/denominator;
			n2_d = numerator2/denominator;

			n1_dd = numerator1/denominator/denominator;
			n2_dd = numerator2/denominator/denominator;

			
			A(i*2,0) = GP.x/denominator;
			A(i*2,1) = GP.y/denominator;
			A(i*2,2) = GP.z/denominator;
			A(i*2,3) = 1/denominator;
			A(i*2,4) = 0;
			A(i*2,5) = 0;
			A(i*2,6) = 0;
			A(i*2,7) = 0;
			A(i*2,8) =  -(GP.x)*n1_dd;
			A(i*2,9) =  -(GP.y)*n1_dd;
			A(i*2,10) = -(GP.z)*n1_dd;

			L(i*2) = IP.x - n1_d;
			
			A(i*2+1,0) = 0;
			A(i*2+1,1) = 0;
			A(i*2+1,2) = 0;
			A(i*2+1,3) = 0;
			A(i*2+1,4) = GP.x/denominator;
			A(i*2+1,5) = GP.y/denominator;
			A(i*2+1,6) = GP.z/denominator;
			A(i*2+1,7) = 1/denominator;
			A(i*2+1,8) =  -(GP.x)*n2_dd;
			A(i*2+1,9) =  -(GP.y)*n2_dd;
			A(i*2+1,10) = -(GP.z)*n2_dd;

			L(i*2+1) = IP.y - n2_d;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);

		maxX = fabs(X(0));
		for(int k=1;k<NUM_DLT_PARAM;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;

	}while(stop == false);

	end_iter = iteration;
	
	AX = A%X;
	//residual
	V = L - AX;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_DLT_PARAM;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

//Get image coord from ground coord
Point2D<double> CDLT::GetGround2Image(double X, double Y, double Z)
{
	Point2D<double> a;
	
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1) / (param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2) / (param.A3*X + param.B3*Y + param.C3*Z + 1);
		
		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1) / (param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2) / (param.A3*X + param.B3*Y + param.C3*Z + 1);
	}


	return a;
}

//Coordinates normalization
void CDLT::GCPNormalization(void)
{
	int i;
	double maxX, maxY, maxZ;
	double minX, minY, minZ;

	//backup coordinate generate
	GCPCoord_Old = new double[num_GCP*3];
	ImageCoord_Old = new double[num_GCP*2];
	
	//coordinates backup and get offset, scale (Image Coordinagtes)
	maxX = minX = ImageCoord[0];
	maxY = minY = ImageCoord[1];
	for(i=0; i<(num_GCP); i++)
	{
		ImageCoord_Old[i*2] = ImageCoord[i*2];
		if(maxX < ImageCoord_Old[i*2])
			maxX = ImageCoord_Old[i*2];
		if(minX > ImageCoord_Old[i*2])
			minX = ImageCoord_Old[i*2];

		ImageCoord_Old[i*2+1] = ImageCoord[i*2+1];
		if(maxY < ImageCoord_Old[i*2+1])
			maxY = ImageCoord_Old[i*2+1];
		if(minY > ImageCoord_Old[i*2+1])
			minY = ImageCoord_Old[i*2+1];
	}

	Scale_I_X = 2/(maxX - minX);
	Scale_I_Y = 2/(maxY - minY);
	Offset_I_X = -minX - (maxX - minX)/2;
	Offset_I_Y = -minY - (maxY - minY)/2;

	//coordinate backup and get offset, scale (GCP Coordinates)
	maxX = minX = GCPCoord[0];
	maxY = minY = GCPCoord[1];
	maxZ = minZ = GCPCoord[2];
	for(i=0; i<(num_GCP); i++)
	{
		GCPCoord_Old[i*3] = GCPCoord[i*3];
		if(maxX < GCPCoord_Old[i*3])
			maxX = GCPCoord_Old[i*3];
		if(minX > GCPCoord_Old[i*3])
			minX = GCPCoord_Old[i*3];

		GCPCoord_Old[i*3+1] = GCPCoord[i*3+1];
		if(maxY < GCPCoord_Old[i*3+1])
			maxY = GCPCoord_Old[i*3+1];
		if(minY > GCPCoord_Old[i*3+1])
			minY = GCPCoord_Old[i*3+1];

		GCPCoord_Old[i*3+2] = GCPCoord[i*3+2];
		if(maxZ < GCPCoord_Old[i*3+2])
			maxZ = GCPCoord_Old[i*3+2];
		if(minZ > GCPCoord_Old[i*3+2])
			minZ = GCPCoord_Old[i*3+2];
	}

	Scale_G_X = 20/(maxX - minX);
	Scale_G_Y = 20/(maxY - minY);
	Scale_G_Z = 20/(maxZ - minZ);
	Offset_G_X = -minX - (maxX - minX)/2;
	Offset_G_Y = -minY - (maxY - minY)/2;
	Offset_G_Z = -minZ - (maxZ - minZ)/2;

	for(i=0; i<(num_GCP); i++)
	{
		ImageCoord[i*2]   = (ImageCoord[i*2]   + Offset_I_X)*Scale_I_X;
		ImageCoord[i*2+1] = (ImageCoord[i*2+1] + Offset_I_Y)*Scale_I_Y;
	}

	for(i=0; i<(num_GCP); i++)
	{
		GCPCoord[i*3]   = (GCPCoord[i*3]   + Offset_G_X)*Scale_G_X;
		GCPCoord[i*3+1] = (GCPCoord[i*3+1] + Offset_G_Y)*Scale_G_Y;
		GCPCoord[i*3+2] = (GCPCoord[i*3+2] + Offset_G_Z)*Scale_G_Z;
	}

}

//Getting Correlation coefficient of parameters
double* CDLT::Getcorrelation_coefficient(void)
{
	int i, j, k;
	correlation_coefficient = new double[NUM_DLT_PARAM*(NUM_DLT_PARAM-1)/2];
	k = -1;
	for(i=0; i<NUM_DLT_PARAM; i++)
	{
		for(j=i+1; j<NUM_DLT_PARAM; j++)
		{
			k ++;
			correlation_coefficient[k] = var_X(i,j)/sqrt(var_X(i,i)*var_X(j,j));
		}
	}

	return correlation_coefficient;
}


//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

//Coordinates normalization
void CDLT::SetCoordNormalize(bool value)
{
	enable_normalization = value;
}

//Insert GCP
void CDLT::InsertGCP(double col, double row, double X, double Y, double Z, long ID)
{
	icp.InsertICP(col, row, ID);
	gcp.InsertGCP(X, Y, Z, ID);
}

//Setting GCP
void CDLT::SetGCP()
{
	unsigned short i;
	
	num_GCP = gcp.GetEnabledGCPNum();
	
	if(GCPCoord != NULL)
	{
		delete[] GCPCoord;
		GCPCoord = NULL;
	}
	GCPCoord = new double[num_GCP*3];
	
	if(ImageCoord != NULL)
	{
		delete[] ImageCoord;
		ImageCoord = NULL;
	}
	ImageCoord = new double[num_GCP*2];
	
	
	for(i=0; i<gcp.GetGCPNum(); i++)
	{
		long id = icp.GetICPID(i+1);

		if(gcp.GetEnableGCP(id))
		{
			icp.GetICPCoord(id,(ImageCoord + i*2),(ImageCoord + i*2 +1));
			gcp.GetGCPCoord(id,(GCPCoord + i*3),(GCPCoord + i*3 + 1),(GCPCoord + i*3 + 2));
		}
	}

	if(true == enable_normalization)
		GCPNormalization();
}

//Get GCP Coordinates
bool CDLT::GetGCPCoord(unsigned short ID, double *col, double *row, double *X, double *Y, double *Z)
{
	bool b_value;

	b_value = icp.GetICPCoord(ID,col,row);
	if(false == b_value)
		return b_value;
	
	b_value = gcp.GetGCPCoord(ID,X,Y,Z);
	if(false == b_value)
		return b_value;
	
	return b_value;
}

//Set GCP Coordinates
bool CDLT::SetGCPCoord(unsigned short ID, double col, double row, double X, double Y, double Z)
{
	bool b_value;

	b_value = icp.SetICPCoord(ID,col,row);
	if(false == b_value)
		return b_value;
	
	b_value = gcp.SetGCPCoord(ID,X,Y,Z);
	if(false == b_value)
		return b_value;
	
	return b_value;
}

//Get DLT parameters
DLTParam CDLT::GetDLTParam()
{
	return param;
}



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CSatDLT
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

//Constructor
CSatDLT::CSatDLT()
{
}

//Destructor
CSatDLT::~CSatDLT()
{
}

//void CSatDLT::Solve(bool nonlinear=false, unsigned int maxiteration = 10, double maxcorrection = 0.000001)
void CSatDLT::Solve(bool nonlinear, unsigned int maxiteration, double maxcorrection)
{
	Solve_linear();
	if(nonlinear == true)
	{
		Solve_nonlinear(maxiteration, maxcorrection);
	}
}

void CSatDLT::Solve_linear()
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL1);
	L.Resize(num_GCP*2);
	
	
	//A and L matrix
	for(i=0;i<num_GCP;i++)
	{
		//image coordinates
		IP.x = *(ImageCoord + i*2);
		IP.y = *(ImageCoord + i*2 + 1);
		//ground coordinates
		GP.x = *(GCPCoord + i*3);
		GP.y = *(GCPCoord + i*3 + 1);
		GP.z = *(GCPCoord + i*3 + 2);

		A(i*2,0) = GP.x;
		A(i*2,1) = GP.y;
		A(i*2,2) = GP.z;
		A(i*2,3) = 1;
		A(i*2,4) = 0;
		A(i*2,5) = 0;
		A(i*2,6) = 0;
		A(i*2,7) = 0;
		A(i*2,8) =  -(GP.x * IP.x);
		A(i*2,9) =  -(GP.y * IP.x);
		A(i*2,10) = -(GP.z * IP.x);

		L(i*2) = IP.x;
		
		A(i*2+1,0) = 0;
		A(i*2+1,1) = 0;
		A(i*2+1,2) = 0;
		A(i*2+1,3) = 0;
		A(i*2+1,4) = GP.x;
		A(i*2+1,5) = GP.y;
		A(i*2+1,6) = GP.z;
		A(i*2+1,7) =  1;
		A(i*2+1,8) =  0;
		A(i*2+1,9) =  0;
		A(i*2+1,10) = 0;

		L(i*2+1) = IP.y;
	}
		
	AT = A.Transpose();
	Ninv = (AT%A).Inverse();
	ATL = AT%L;
	X = Ninv%ATL;
	
	param.A1 = X(0);
	param.B1 = X(1);
	param.C1 = X(2);
	param.D1 = X(3);
	param.A2 = X(4);
	param.B2 = X(5);
	param.C2 = X(6);
	param.D2 = X(7);
	param.A3 = X(8);
	param.B3 = X(9);
	param.C3 = X(10);
	
	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL1;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

void CSatDLT::Solve_nonlinear(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL1);
	L.Resize(num_GCP*2);
	
	do{
		iteration ++;
		//A and L matrix
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = *(ImageCoord + i*2);
			IP.y = *(ImageCoord + i*2 + 1);
			//ground coordinates
			GP.x = *(GCPCoord + i*3);
			GP.y = *(GCPCoord + i*3 + 1);
			GP.z = *(GCPCoord + i*3 + 2);
			
			double denominator;
			double numerator1;
			double numerator2;
			double n1_dd, n1_d;
			
			denominator = (param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1);
			numerator1   = (param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1);
			numerator2   = (param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2);
			n1_d = numerator1/denominator;
			n1_dd = n1_d/denominator;
			
			A(i*2,0) = GP.x/denominator;
			A(i*2,1) = GP.y/denominator;
			A(i*2,2) = GP.z/denominator;
			A(i*2,3) = 1/denominator;
			A(i*2,4) = 0;
			A(i*2,5) = 0;
			A(i*2,6) = 0;
			A(i*2,7) = 0;
			A(i*2,8) =  -(GP.x)*n1_dd;
			A(i*2,9) =  -(GP.y)*n1_dd;
			A(i*2,10) = -(GP.z)*n1_dd;

			L(i*2) = IP.x - n1_d;
			
			A(i*2+1,0) = 0;
			A(i*2+1,1) = 0;
			A(i*2+1,2) = 0;
			A(i*2+1,3) = 0;
			A(i*2+1,4) = GP.x;
			A(i*2+1,5) = GP.y;
			A(i*2+1,6) = GP.z;
			A(i*2+1,7) = 1;
			A(i*2+1,8) =  0;
			A(i*2+1,9) =  0;
			A(i*2+1,10) = 0;

			L(i*2+1) = IP.y - numerator2;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);

		maxX = fabs(X(0));
		for(int k=1;k<NUM_SATDLT_PARAM_LEVEL1;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;

	}while(stop == false);

	end_iter = iteration;
	
	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL1;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

Point2D<double> CSatDLT::GetGround2Image(double X, double Y, double Z)
{
	Point2D<double> a;
	//c(column) = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)
	//l(scan line) = (A2X + B2Y + C2Z + D2)
	
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1) / (param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2);
	
		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1) / (param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2);
	}

	return a;
}

double * CSatDLT::Getcorrelation_coefficient(void)
{
	int i, j, k;
	correlation_coefficient = new double[NUM_SATDLT_PARAM_LEVEL1*(NUM_SATDLT_PARAM_LEVEL1-1)/2];
	k = -1;
	for(i=0; i<NUM_SATDLT_PARAM_LEVEL1; i++)
	{
		for(j=i+1; j<NUM_SATDLT_PARAM_LEVEL1; j++)
		{
			k ++;
			correlation_coefficient[k] = var_X(i,j)/sqrt(var_X(i,i)*var_X(j,j));
		}
	}

	return correlation_coefficient;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CSatDLT_Level2
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
CSatDLT_Level2::CSatDLT_Level2()
{
}

CSatDLT_Level2::~CSatDLT_Level2()
{
}

void CSatDLT_Level2::Solve(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;
	
	////////////////////////////////////////////////////////////////////////////
	//Initialize Parameter
	////////////////////////////////////////////////////////////////////////////
	Solve_linear(); //base class(DSatDLT_Level1) Solve(nonlinear = false) function
	param.A4 = param.B4 = param.C4 = 0;
	param.A5 = param.B5 = param.C5 = 0;
	
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL2);
	L.Resize(num_GCP*2);

	do{
		iteration ++;
		
		/////////////////
		//A and L matrix
		/////////////////
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = ImageCoord[i*2];
			IP.y = ImageCoord[i*2 +1];
			//ground coordinates
			GP.x = GCPCoord[i*3];
			GP.y = GCPCoord[i*3 +1];
			GP.z = GCPCoord[i*3 +2];
			
			double denominator;
			double numerator1, numerator2;
			double n1_dd, n2_dd;
			double Fo, Go;
			
			//0 = (A1*X + B1*Y + C1*Z + D1)/(A3*X + B3*Y + C3*Z + 1) + A4*X + B4*Y + C4*Z - x(column)
			//0 = (A2*X + B2*Y + C2*Z + D2)/(A3*X + B3*Y + C3*Z + 1) + A5*X + B5*Y + C5*Z - y(row)
			denominator = param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1;
			numerator1  = param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1;
			numerator2  = param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2;
			
			Fo = numerator1/denominator + (param.A4*GP.x + param.B4*GP.y + param.C4*GP.z) - IP.x;
			Go = numerator2/denominator + (param.A5*GP.x + param.B5*GP.y + param.C5*GP.z) - IP.y;
			
			n1_dd = numerator1/denominator/denominator;
			n2_dd = numerator2/denominator/denominator;
			
			A(i*2,0) = GP.x/denominator;
			A(i*2,1) = GP.y/denominator;
			A(i*2,2) = GP.z/denominator;
			A(i*2,3) = 1/denominator;
			A(i*2,4) = 0;
			A(i*2,5) = 0;
			A(i*2,6) = 0;
			A(i*2,7) = 0;
			A(i*2,8) =  -(GP.x)*n1_dd;
			A(i*2,9) =  -(GP.y)*n1_dd;
			A(i*2,10) = -(GP.z)*n1_dd;
			A(i*2,11) = GP.x;
			A(i*2,12) = GP.y;
			A(i*2,13) = GP.z;
			A(i*2,14) = 0;
			A(i*2,15) = 0;
			A(i*2,16) = 0;

			L(i*2) = -Fo;
			
			A(i*2+1,0) = 0;
			A(i*2+1,1) = 0;
			A(i*2+1,2) = 0;
			A(i*2+1,3) = 0;
			A(i*2+1,4) = GP.x/denominator;
			A(i*2+1,5) = GP.y/denominator;
			A(i*2+1,6) = GP.z/denominator;
			A(i*2+1,7) = 1/denominator;
			A(i*2+1,8) =  -(GP.x)*n2_dd;
			A(i*2+1,9) =  -(GP.y)*n2_dd;
			A(i*2+1,10) = -(GP.z)*n2_dd;
			A(i*2+1,11) = 0;
			A(i*2+1,12) = 0;
			A(i*2+1,13) = 0;
			A(i*2+1,14) = GP.x;
			A(i*2+1,15) = GP.y;
			A(i*2+1,16) = GP.z;

			L(i*2+1) = -Go;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);
		param.A4 += X(11);
		param.B4 += X(12);
		param.C4 += X(13);
		param.A5 += X(14);
		param.B5 += X(15);
		param.C5 += X(16);

		maxX = fabs(X(0));
		for(int k=1;k<NUM_SATDLT_PARAM_LEVEL2;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;

	}while(stop == false);

	end_iter = iteration;

	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL2;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
	
}

//x - alpha*y = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)
//-beta*x + y = (A2X + B2Y + C2Z + D2)
//Affine SatDLT
void CSatDLT_Level2::Solve2(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;
	
	////////////////////////////////////////////////////////////////////////////
	//Initialize Parameter
	////////////////////////////////////////////////////////////////////////////
	Solve_linear(); //base class(DSatDLT_Level1) Solve(nonlinear = false) function
	param.alpha = param.beta = 0.0;

	
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL2_2);
	L.Resize(num_GCP*2);

	do{
		iteration ++;
		//A and L matrix
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = *(ImageCoord + i*2);
			IP.y = *(ImageCoord + i*2 + 1);
			//ground coordinates
			GP.x = *(GCPCoord + i*3);
			GP.y = *(GCPCoord + i*3 + 1);
			GP.z = *(GCPCoord + i*3 + 2);
			
			double denominator;
			double numerator1;
			double n1_dd;
			double Fo, Go;
			
			denominator = (param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1);
			numerator1   = (param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1);
						
			Fo = numerator1/denominator + param.alpha*IP.y - IP.x;
			Go = (param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2) + param.beta*IP.x - IP.y;
			
			n1_dd = numerator1/denominator/denominator;
						
			A(i*2,0) = GP.x/denominator;
			A(i*2,1) = GP.y/denominator;
			A(i*2,2) = GP.z/denominator;
			A(i*2,3) = 1/denominator;
			A(i*2,4) = 0;
			A(i*2,5) = 0;
			A(i*2,6) = 0;
			A(i*2,7) = 0;
			A(i*2,8) =  -(GP.x)*n1_dd;
			A(i*2,9) =  -(GP.y)*n1_dd;
			A(i*2,10) = -(GP.z)*n1_dd;
			A(i*2,11) = IP.y;
			A(i*2,12) = 0;
			
			L(i*2) = -Fo;
			
			A(i*2+1,0) = 0;
			A(i*2+1,1) = 0;
			A(i*2+1,2) = 0;
			A(i*2+1,3) = 0;
			A(i*2+1,4) = GP.x;
			A(i*2+1,5) = GP.y;
			A(i*2+1,6) = GP.z;
			A(i*2+1,7) = 1;
			A(i*2+1,8) =  0;
			A(i*2+1,9) =  0;
			A(i*2+1,10) = 0;
			A(i*2+1,11) = 0;
			A(i*2+1,12) = IP.x;
			
			L(i*2+1) = -Go;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);
		param.alpha += X(11);
		param.beta  += X(12);
		
		maxX = fabs(X(0));
		for(int k=1;k<NUM_SATDLT_PARAM_LEVEL2_2;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;

	}while(stop == false);

	end_iter = iteration;
	
	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL2_2;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

void CSatDLT_Level2::Solve3(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;
	
	////////////////////////////////////////////////////////////////////////////
	//Initialize Parameter
	////////////////////////////////////////////////////////////////////////////
	Solve_linear(); //base class(DSatDLT_Level1) Solve(nonlinear = false) function
	param.alpha = param.beta = 0.0;

	
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL2_2);
	L.Resize(num_GCP*2);

	do{
		iteration ++;
		//A and L matrix
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = *(ImageCoord + i*2);
			IP.y = *(ImageCoord + i*2 + 1);
			//ground coordinates
			GP.x = *(GCPCoord + i*3);
			GP.y = *(GCPCoord + i*3 + 1);
			GP.z = *(GCPCoord + i*3 + 2);
			
			double denominator;
			double numerator1;
			double numerator2;
			double n1_dd;
			double Fo, Go;
			
			denominator = (param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1);
			numerator1   = (param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1);
			numerator2   = (param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2);
						
			Fo = param.alpha*numerator2 + param.beta*numerator1/denominator - IP.x;
			Go = numerator1/denominator + numerator2 - IP.y;
			
			n1_dd = numerator1/denominator/denominator;
						
			A(i*2,0) = param.beta*GP.x/denominator;
			A(i*2,1) = param.beta*GP.y/denominator;
			A(i*2,2) = param.beta*GP.z/denominator;
			A(i*2,3) = param.beta*1/denominator;
			A(i*2,4) = param.alpha*GP.x;
			A(i*2,5) = param.alpha*GP.y;
			A(i*2,6) = param.alpha*GP.z;
			A(i*2,7) = param.alpha;
			A(i*2,8)  = -param.beta*(GP.x)*n1_dd;
			A(i*2,9)  = -param.beta*(GP.y)*n1_dd;
			A(i*2,10) = -param.beta*(GP.z)*n1_dd;
			A(i*2,11) = numerator2;
			A(i*2,12) = numerator1/denominator;
			
			L(i*2) = -Fo;
			
			A(i*2+1,0) = GP.x/denominator;
			A(i*2+1,1) = GP.y/denominator;
			A(i*2+1,2) = GP.z/denominator;
			A(i*2+1,3) = 1/denominator;
			A(i*2+1,4) = GP.x;
			A(i*2+1,5) = GP.y;
			A(i*2+1,6) = GP.z;
			A(i*2+1,7) = 1;
			A(i*2+1,8) =  -(GP.x)*n1_dd;
			A(i*2+1,9) =  -(GP.y)*n1_dd;
			A(i*2+1,10) = -(GP.z)*n1_dd;
			A(i*2+1,11) = 0;
			A(i*2+1,12) = 0;
			
			L(i*2+1) = -Go;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);
		param.alpha += X(11);
		param.beta  += X(12);
		
		maxX = fabs(X(0));
		for(int k=1;k<NUM_SATDLT_PARAM_LEVEL2_2;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;
		
	}while(stop == false);

	end_iter = iteration;
	
	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL2_2;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

void CSatDLT_Level2::Solve4(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;
	
	////////////////////////////////////////////////////////////////////////////
	//Initialize Parameter
	////////////////////////////////////////////////////////////////////////////
	Solve_linear(); //base class(DSatDLT_Level1) Solve(nonlinear = false) function
	param.A4 = param.B4 = param.C4 = 0.0;
	param.A5 = param.B5 = param.C5 = 0.0;

	
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL2);
	L.Resize(num_GCP*2);

	do{
		iteration ++;
		//A and L matrix
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = ImageCoord[i*2];
			IP.y = ImageCoord[i*2 +1];
			//ground coordinates
			GP.x = GCPCoord[i*3];
			GP.y = GCPCoord[i*3 +1];
			GP.z = GCPCoord[i*3 +2];
			
			double denominator;
			double numerator1,numerator2;
			double n1_dd, n2_dd;
			double Fo, Go;
						
			//x = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + (A4X + B4Y + C4Z)
			//y = (A2X + B2Y + C2Z + D2)/(A3X + B3Y + C3Z + 1) + (A5X + B5Y + C5Z)
			denominator = param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1;
			numerator1 = param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1;
			numerator2 = param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2;

			n1_dd = numerator1/denominator/denominator;
			n2_dd = numerator2/denominator/denominator;
						
			Fo = numerator1/denominator + (param.A4*GP.x + param.B4*GP.y + param.C4*GP.z) - IP.x;
			Go = numerator2/denominator + (param.A5*GP.x + param.B5*GP.y + param.C5*GP.z) - IP.y;

			A(i*2,0) = GP.x/denominator;
			A(i*2,1) = GP.y/denominator;
			A(i*2,2) = GP.z/denominator;
			A(i*2,3) = 1/denominator;
			A(i*2,4) = 0;
			A(i*2,5) = 0;
			A(i*2,6) = 0;
			A(i*2,7) = 0;
			A(i*2,8) =  -(GP.x)*n1_dd;
			A(i*2,9) =  -(GP.y)*n1_dd;
			A(i*2,10) = -(GP.z)*n1_dd;
			A(i*2,11) = GP.x;
			A(i*2,12) = GP.y;
			A(i*2,13) = GP.z;
			A(i*2,14) = 0;
			A(i*2,15) = 0;
			A(i*2,16) = 0;
			
			L(i*2) = -Fo;
			
			A(i*2+1,0) = 0;
			A(i*2+1,1) = 0;
			A(i*2+1,2) = 0;
			A(i*2+1,3) = 0;
			A(i*2+1,4) = GP.x/denominator;
			A(i*2+1,5) = GP.y/denominator;
			A(i*2+1,6) = GP.z/denominator;
			A(i*2+1,7) = 1/denominator;
			A(i*2+1,8) =  -(GP.x)*n2_dd;
			A(i*2+1,9) =  -(GP.y)*n2_dd;
			A(i*2+1,10) = -(GP.z)*n2_dd;
			A(i*2+1,11) = 0;
			A(i*2+1,12) = 0;
			A(i*2+1,13) = 0;
			A(i*2+1,14) = GP.x;
			A(i*2+1,15) = GP.y;
			A(i*2+1,16) = GP.z;
			
			L(i*2+1) = -Go;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);
		param.A4 += X(11);
		param.B4 += X(12);
		param.C4 += X(13);
		param.A5 += X(14);
		param.B5 += X(15);
		param.C5 += X(16);
		
		maxX = fabs(X(0));
		for(int k=1;k<NUM_SATDLT_PARAM_LEVEL2;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;

	}while(stop == false);

	end_iter = iteration;
	
	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL2;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

void CSatDLT_Level2::Solve5(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;
	
	////////////////////////////////////////////////////////////////////////////
	//Initialize Parameter
	////////////////////////////////////////////////////////////////////////////
	Solve_linear(); //base class(DSatDLT_Level1) Solve(nonlinear = false) function
	param.alpha = param.beta = 0.0;
		
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL2_2);
	L.Resize(num_GCP*2);

	do{
		iteration ++;
		//A and L matrix
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = ImageCoord[i*2];
			IP.y = ImageCoord[i*2 +1];
			//ground coordinates
			GP.x = GCPCoord[i*3];
			GP.y = GCPCoord[i*3 +1];
			GP.z = GCPCoord[i*3 +2];
			
			double denominator;
			double numerator1,numerator2;
			double n1_d, n1_dd;
			double Fo, Go;
				
			//x = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + alpha*(A2X + B2Y + C2Z + D2)
			//y = beta*(A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + (A2X + B2Y + C2Z + D2)
			denominator = param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1;
			numerator1 = param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1;
			numerator2 = param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2;

			n1_d = numerator1/denominator;
			n1_dd = n1_d/denominator;
									
			Fo = n1_d + param.alpha*numerator2 - IP.x;
			Go = param.beta*n1_d + numerator2 - IP.y;

			A(i*2,0) = GP.x/denominator;//A1
			A(i*2,1) = GP.y/denominator;//B1
			A(i*2,2) = GP.z/denominator;//C1
			A(i*2,3) = 1/denominator;//D1
			A(i*2,4) = param.alpha*GP.x;//A2
			A(i*2,5) = param.alpha*GP.y;//B2
			A(i*2,6) = param.alpha*GP.z;//C2
			A(i*2,7) = param.alpha;//D2
			A(i*2,8) =  -(GP.x)*n1_dd;//A3
			A(i*2,9) =  -(GP.y)*n1_dd;//B3
			A(i*2,10) = -(GP.z)*n1_dd;//D3
			A(i*2,11) = numerator2;//alpha
			A(i*2,12) = 0;//beta
						
			L(i*2) = -Fo;
			
			A(i*2+1,0) = param.beta*GP.x/denominator;//A1
			A(i*2+1,1) = param.beta*GP.y/denominator;//B1;
			A(i*2+1,2) = param.beta*GP.z/denominator;//C1;
			A(i*2+1,3) = param.beta/denominator;//D1
			A(i*2+1,4) = GP.x;//A2
			A(i*2+1,5) = GP.y;//B2
			A(i*2+1,6) = GP.z;//C2
			A(i*2+1,7) = 1;//D2
			A(i*2+1,8) =  -param.beta*(GP.x)*n1_dd;//A3
			A(i*2+1,9) =  -param.beta*(GP.y)*n1_dd;//B3
			A(i*2+1,10) = -param.beta*(GP.z)*n1_dd;//C3
			A(i*2+1,11) = 0;
			A(i*2+1,12) = n1_d;
						
			L(i*2+1) = -Go;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);
		param.alpha += X(11);
		param.beta  += X(12);
				
		maxX = fabs(X(0));
		for(int k=1;k<NUM_SATDLT_PARAM_LEVEL2_2;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;

	}while(stop == false);

	end_iter = iteration;
	
	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL2_2;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

//0 = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)+alpha*x*y - x
//0 = (A2X + B2Y + C2Z + D2) + beta*x*y - y
//Polynomial I
void CSatDLT_Level2::Solve6(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;
	
	////////////////////////////////////////////////////////////////////////////
	//Initialize Parameter
	////////////////////////////////////////////////////////////////////////////
	Solve_linear(); //base class(DSatDLT_Level1) Solve(nonlinear = false) function
	param.alpha = param.beta = 0.0;

	
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL2_2);
	L.Resize(num_GCP*2);

	do{
		iteration ++;
		//A and L matrix
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = *(ImageCoord + i*2);
			IP.y = *(ImageCoord + i*2 + 1);
			//ground coordinates
			GP.x = *(GCPCoord + i*3);
			GP.y = *(GCPCoord + i*3 + 1);
			GP.z = *(GCPCoord + i*3 + 2);
			
			double denominator;
			double numerator1;
			double n1_dd;
			double Fo, Go;
			
			denominator = (param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1);
			numerator1   = (param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1);
						
			Fo = numerator1/denominator + param.alpha*IP.x*IP.y - IP.x;
			Go = (param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2) + param.beta*IP.x*IP.y - IP.y;
			
			n1_dd = numerator1/denominator/denominator;
						
			A(i*2,0) = GP.x/denominator;
			A(i*2,1) = GP.y/denominator;
			A(i*2,2) = GP.z/denominator;
			A(i*2,3) = 1/denominator;
			A(i*2,4) = 0;
			A(i*2,5) = 0;
			A(i*2,6) = 0;
			A(i*2,7) = 0;
			A(i*2,8) =  -(GP.x)*n1_dd;
			A(i*2,9) =  -(GP.y)*n1_dd;
			A(i*2,10) = -(GP.z)*n1_dd;
			A(i*2,11) = IP.x*IP.y;
			A(i*2,12) = 0;
			
			L(i*2) = -Fo;
			
			A(i*2+1,0) = 0;
			A(i*2+1,1) = 0;
			A(i*2+1,2) = 0;
			A(i*2+1,3) = 0;
			A(i*2+1,4) = GP.x;
			A(i*2+1,5) = GP.y;
			A(i*2+1,6) = GP.z;
			A(i*2+1,7) = 1;
			A(i*2+1,8) =  0;
			A(i*2+1,9) =  0;
			A(i*2+1,10) = 0;
			A(i*2+1,11) = 0;
			A(i*2+1,12) = IP.x*IP.y;
			
			L(i*2+1) = -Go;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);
		param.alpha += X(11);
		param.beta  += X(12);
		
		maxX = fabs(X(0));
		for(int k=1;k<NUM_SATDLT_PARAM_LEVEL2_2;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;

	}while(stop == false);

	end_iter = iteration;
	
	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL2_2;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

//0 = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)+alpha*x*y + alpha2*y - x
//0 = (A2X + B2Y + C2Z + D2) + beta*x*y + beta2*x - y
//Polynomial II
void CSatDLT_Level2::Solve7(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;
	
	////////////////////////////////////////////////////////////////////////////
	//Initialize Parameter
	////////////////////////////////////////////////////////////////////////////
	Solve_linear(); //base class(DSatDLT_Level1) Solve(nonlinear = false) function
	param.alpha = param.beta = 0.0;
	param.alpha2 = param.beta2 = 0.0;

	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL2_3);
	L.Resize(num_GCP*2);

	do{
		iteration ++;
		//A and L matrix
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = *(ImageCoord + i*2);
			IP.y = *(ImageCoord + i*2 + 1);
			//ground coordinates
			GP.x = *(GCPCoord + i*3);
			GP.y = *(GCPCoord + i*3 + 1);
			GP.z = *(GCPCoord + i*3 + 2);
			
			double denominator;
			double numerator1;
			double n1_dd;
			double Fo, Go;
			
			denominator = (param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1);
			numerator1   = (param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1);
						
			Fo = (numerator1/denominator) + (param.alpha*IP.x*IP.y) + (param.alpha2*IP.y) - IP.x;
			Go = (param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2) + (param.beta*IP.x*IP.y) + (param.beta2*IP.x) - IP.y;
			
			n1_dd = numerator1/denominator/denominator;
			
			A(i*2,0) = GP.x/denominator;
			A(i*2,1) = GP.y/denominator;
			A(i*2,2) = GP.z/denominator;
			A(i*2,3) = 1/denominator;
			A(i*2,4) = 0;
			A(i*2,5) = 0;
			A(i*2,6) = 0;
			A(i*2,7) = 0;
			A(i*2,8) =  -(GP.x)*n1_dd;
			A(i*2,9) =  -(GP.y)*n1_dd;
			A(i*2,10) = -(GP.z)*n1_dd;
			A(i*2,11) = IP.x*IP.y;
			A(i*2,12) = 0;
			A(i*2,13) = IP.y;
			A(i*2,14) = 0;
			
			L(i*2) = -Fo;
			
			A(i*2+1,0) = 0;
			A(i*2+1,1) = 0;
			A(i*2+1,2) = 0;
			A(i*2+1,3) = 0;
			A(i*2+1,4) = GP.x;
			A(i*2+1,5) = GP.y;
			A(i*2+1,6) = GP.z;
			A(i*2+1,7) = 1;
			A(i*2+1,8) =  0;
			A(i*2+1,9) =  0;
			A(i*2+1,10) = 0;
			A(i*2+1,11) = 0;
			A(i*2+1,12) = IP.x*IP.y;
			A(i*2+1,13) = 0;
			A(i*2+1,14) = IP.x;
			
			L(i*2+1) = -Go;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);
		param.alpha += X(11);
		param.beta  += X(12);
		param.alpha2 += X(13);
		param.beta2  += X(14);
		
		maxX = fabs(X(0));
		for(int k=1;k<NUM_SATDLT_PARAM_LEVEL2_3;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;

	}while(stop == false);

	end_iter = iteration;
	
	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL2_3;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

void CSatDLT_Level2::Solve8(unsigned int maxiteration, double maxcorrection)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Ninv, VTV;

	Point2D<double> IP;
	Point3D<double> GP;
		
	int i;
	double maxX;
	unsigned char iteration = 0;
	bool stop = false;
	
	////////////////////////////////////////////////////////////////////////////
	//Initialize Parameter
	////////////////////////////////////////////////////////////////////////////
	Solve_linear(); //base class(DSatDLT_Level1) Solve(nonlinear = false) function
	param.alpha = param.beta = 0.0;
	
	//matrix(A, L) resizing
	A.Resize(num_GCP*2,NUM_SATDLT_PARAM_LEVEL2_2);
	L.Resize(num_GCP*2);

	do{
		iteration ++;
		//A and L matrix
		for(i=0;i<num_GCP;i++)
		{
			//image coordinates
			IP.x = *(ImageCoord + i*2);
			IP.y = *(ImageCoord + i*2 + 1);
			//ground coordinates
			GP.x = *(GCPCoord + i*3);
			GP.y = *(GCPCoord + i*3 + 1);
			GP.z = *(GCPCoord + i*3 + 2);
			
			double denominator;
			double numerator1, numerator2;
			double n1_dd, n2_dd;
			double Fo, Go;
			
			denominator = (param.A3*GP.x + param.B3*GP.y + param.C3*GP.z + 1);
			numerator1   = (param.A1*GP.x + param.B1*GP.y + param.C1*GP.z + param.D1);
			numerator2   = (param.A2*GP.x + param.B2*GP.y + param.C2*GP.z + param.D2);
						
			Fo = (numerator1/denominator) + (param.alpha*IP.x*IP.y) - IP.x;
			Go = (numerator2/denominator) + (param.beta*IP.y*IP.y) - IP.y;;
			
			n1_dd = numerator1/denominator/denominator;
			n2_dd = numerator2/denominator/denominator;
			
			A(i*2,0) = GP.x/denominator;
			A(i*2,1) = GP.y/denominator;
			A(i*2,2) = GP.z/denominator;
			A(i*2,3) = 1/denominator;
			A(i*2,4) = 0;
			A(i*2,5) = 0;
			A(i*2,6) = 0;
			A(i*2,7) = 0;
			A(i*2,8) =  -(GP.x)*n1_dd;
			A(i*2,9) =  -(GP.y)*n1_dd;
			A(i*2,10) = -(GP.z)*n1_dd;
			A(i*2,11) = IP.x*IP.y;
			A(i*2,12) = 0;
						
			L(i*2) = -Fo;
			
			A(i*2+1,0) = 0;
			A(i*2+1,1) = 0;
			A(i*2+1,2) = 0;
			A(i*2+1,3) = 0;
			A(i*2+1,4) = GP.x/denominator;
			A(i*2+1,5) = GP.y/denominator;
			A(i*2+1,6) = GP.z/denominator;
			A(i*2+1,7) = 1/denominator;
			A(i*2+1,8) =  -(GP.x)*n2_dd;
			A(i*2+1,9) =  -(GP.y)*n2_dd;
			A(i*2+1,10) = -(GP.z)*n2_dd;
			A(i*2+1,11) = 0;
			A(i*2+1,12) = IP.y*IP.y;
						
			L(i*2+1) = -Go;
		}
		
		AT = A.Transpose();
		Ninv = (AT%A).Inverse();
		ATL = AT%L;
		X = Ninv%ATL;
		
		param.A1 += X(0);
		param.B1 += X(1);
		param.C1 += X(2);
		param.D1 += X(3);
		param.A2 += X(4);
		param.B2 += X(5);
		param.C2 += X(6);
		param.D2 += X(7);
		param.A3 += X(8);
		param.B3 += X(9);
		param.C3 += X(10);
		param.alpha += X(11);
		param.beta  += X(12);
		
		maxX = fabs(X(0));
		for(int k=1;k<NUM_SATDLT_PARAM_LEVEL2_2;k++)
		{
			if(maxX < fabs(X(k)))
				maxX = fabs(X(k));
		}

		if(iteration >= maxiteration)
			stop = true;
		if(maxX <= maxcorrection)
			stop = true;

	}while(stop == false);

	end_iter = iteration;
	
	AX = A%X;
	//residual
	V = AX - L;
	VTV = V.Transpose()%V;
	DF = num_GCP*2 - NUM_SATDLT_PARAM_LEVEL2_2;
	//posteriori variance
	posteriori_var = VTV(0)/(double)DF;
	//standard deviation(RMSE)
	SD = sqrt(posteriori_var);
	//variance of unknown
	var_X = Ninv*posteriori_var;
	//variance of observation
	var_La = (A%var_X%AT);
}

//c(column) = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + A4X + B4Y + C4Z
//l(scan line) = (A2X + B2Y + C2Z + D2)/(A3X + B3Y + C3Z + 1) + A5X + B5Y + C5Z
Point2D<double> CSatDLT_Level2::GetGround2Image(double X, double Y, double Z)
{
	Point2D<double> a;
		
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1) / (param.A3*X + param.B3*Y + param.C3*Z + 1) + (param.A4*X + param.B4*Y + param.C4*Z);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2) / (param.A3*X + param.B3*Y + param.C3*Z + 1) + (param.A5*X + param.B5*Y + param.C5*Z);
		
		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1) / (param.A3*X + param.B3*Y + param.C3*Z + 1) + (param.A4*X + param.B4*Y + param.C4*Z);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2) / (param.A3*X + param.B3*Y + param.C3*Z + 1) + (param.A5*X + param.B5*Y + param.C5*Z);
	}

	return a;
}

//x - alpha*y = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)
//-beta*x + y = (A2X + B2Y + C2Z + D2)
//Affine SatDLT
Point2D<double> CSatDLT_Level2::GetGround2Image2(double X, double Y, double Z)
{
	Point2D<double> a;
		
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		
		a.x = ((param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*(param.A2*X + param.B2*Y + param.C2*Z + param.D2))/(1-param.alpha*param.beta);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2) + param.beta*a.x;

		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		a.x = ((param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*(param.A2*X + param.B2*Y + param.C2*Z + param.D2))/(1-param.alpha*param.beta);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2) + param.beta*a.x;
	}

	return a;
}

//x = alpha*(A2X + B2Y + C2Z + D2) + beta*(A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)
//y = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + (A2X + B2Y + C2Z + D2)
Point2D<double> CSatDLT_Level2::GetGround2Image3(double X, double Y, double Z)
{
	Point2D<double> a;
		
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		
		a.x = param.alpha*(param.A2*X + param.B2*Y + param.C2*Z + param.D2) + param.beta*(param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + (param.A2*X + param.B2*Y + param.C2*Z + param.D2);

		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		
		a.x = param.alpha*(param.A2*X + param.B2*Y + param.C2*Z + param.D2) + param.beta*(param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + (param.A2*X + param.B2*Y + param.C2*Z + param.D2);
	}

	return a;
}

//x = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + (A4X + B4Y + C4Z)
//y = (A2X + B2Y + C2Z + D2)/(A3X + B3Y + C3Z + 1) + (A5X + B5Y + C5Z)
Point2D<double> CSatDLT_Level2::GetGround2Image4(double X, double Y, double Z)
{
	Point2D<double> a;
	
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.A4*X + param.B4*Y + param.C4*Z;
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.A5*X + param.B5*Y + param.C5*Z;

		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.A4*X + param.B4*Y + param.C4*Z;
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.A5*X + param.B5*Y + param.C5*Z;
	}

	return a;
}

//x = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + alpha*(A2X + B2Y + C2Z + D2)
//y = beta*(A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + (A2X + B2Y + C2Z + D2)
Point2D<double> CSatDLT_Level2::GetGround2Image5(double X, double Y, double Z)
{
	Point2D<double> a;
	
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		
		a.x =            (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*(param.A2*X + param.B2*Y + param.C2*Z + param.D2);
		a.y = param.beta*(param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) +             (param.A2*X + param.B2*Y + param.C2*Z + param.D2);

		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		a.x =            (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*(param.A2*X + param.B2*Y + param.C2*Z + param.D2);
		a.y = param.beta*(param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) +             (param.A2*X + param.B2*Y + param.C2*Z + param.D2);
	}

	return a;
}

//0 = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)+alpha*x*y - x
//0 = (A2X + B2Y + C2Z + D2) + beta*x*y - y
//Polynomial I
Point2D<double> CSatDLT_Level2::GetGround2Image6(double X, double Y, double Z)
{
	Point2D<double> a;
	
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2);

		bool end;
		CSMMatrix<double> A, L, matX;
		int iter = 0;
		do
		{
			iter ++;
			A.Resize(2,2);
			L.Resize(2,1);
			
			A(0,0) = param.alpha*a.y - 1;
			A(0,1) = param.alpha*a.x;
			A(1,0) = param.beta*a.y;
			A(1,1) = param.beta*a.x - 1;
			
			L(0,0) = -((param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*a.x*a.y - a.x);
			L(1,0) = -((param.A2*X + param.B2*Y + param.C2*Z + param.D2) + param.beta*a.x*a.y - a.y);
			
			//matX = (A.Transpose()%A).Inverse()%A.Transpose()%L;
			matX = A.Inverse()%L;
			
			if(matX(0,0)<0.000001 && matX(1,0)<0.000001)
				end = true;
			if(iter >= 10)
				end = true;

			a.x += matX(0,0);
			a.y += matX(1,0);

		}while(end == false);
		

		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2);

		bool end;
		CSMMatrix<double> A, L, matX;
		int iter = 0;
		do
		{
			iter ++;
			A.Resize(2,2);
			L.Resize(2,1);
			
			A(0,0) = param.alpha*a.y - 1;
			A(0,1) = param.alpha*a.x;
			A(1,0) = param.beta*a.y;
			A(1,1) = param.beta*a.x - 1;
			
			L(0,0) = -((param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*a.x*a.y - a.x);
			L(1,0) = -((param.A2*X + param.B2*Y + param.C2*Z + param.D2) + param.beta*a.x*a.y - a.y);
			
			matX = (A.Transpose()%A).Inverse()%A.Transpose()%L;
			
			if(matX(0,0)<0.000001 && matX(1,0)<0.000001)
				end = true;
			if(iter >= 10)
				end = true;

			a.x += matX(0,0);
			a.y += matX(1,0);

		}while(end == false);
	}

	return a;
}

//0 = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1)+alpha*x*y + alpha2*y - x
//0 = (A2X + B2Y + C2Z + D2) + beta*x*y + beta2*x - y
//Polynomial II
Point2D<double> CSatDLT_Level2::GetGround2Image7(double X, double Y, double Z)
{
	Point2D<double> a;
		
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2);

		bool end;
		CSMMatrix<double> A, L, matX;
		int iter = 0;
		do
		{
			iter ++;
			A.Resize(2,2);
			L.Resize(2,1);
			
			A(0,0) = param.alpha*a.y - 1;
			A(0,1) = param.alpha*a.x + param.alpha2;
			A(1,0) = param.beta*a.y + param.beta2;
			A(1,1) = param.beta*a.x - 1;
						
			L(0,0) = -((param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*a.x*a.y + param.alpha2*a.y - a.x);
			L(1,0) = -((param.A2*X + param.B2*Y + param.C2*Z + param.D2) + param.beta*a.x*a.y + param.beta2*a.x - a.y);
			
			//matX = (A.Transpose()%A).Inverse()%A.Transpose()%L;
			matX = A.Inverse()%L;
			
			if(matX(0,0)<0.000001 && matX(1,0)<0.000001)
				end = true;
			if(iter >= 10)
				end = true;

			a.x += matX(0,0);
			a.y += matX(1,0);

		}while(end == false);
		

		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2);

		bool end;
		CSMMatrix<double> A, L, matX;
		int iter = 0;
		do
		{
			iter ++;
			A.Resize(2,2);
			L.Resize(2,1);
			
			A(0,0) = param.alpha*a.y - 1;
			A(0,1) = param.alpha*a.x + param.alpha2;
			A(1,0) = param.beta*a.y + param.beta2;
			A(1,1) = param.beta*a.x - 1;
						
			L(0,0) = -((param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*a.x*a.y + param.alpha2*a.y - a.x);
			L(1,0) = -((param.A2*X + param.B2*Y + param.C2*Z + param.D2) + param.beta*a.x*a.y + param.beta2*a.x - a.y);
			
			//matX = (A.Transpose()%A).Inverse()%A.Transpose()%L;
			matX = A.Inverse()%L;
			
			if(matX(0,0)<0.000001 && matX(1,0)<0.000001)
				end = true;
			if(iter >= 10)
				end = true;

			a.x += matX(0,0);
			a.y += matX(1,0);

		}while(end == false);
	}

	return a;
}

//0 = (A1X + B1Y + C1Z + D1)/(A3X + B3Y + C3Z + 1) + alpha*x*y - x
//0 = (A2X + B2Y + C2Z + D2)/(A3X + B3Y + C3Z + 1) + beta*y*y - y
//Okamoto's Extended DLT
Point2D<double> CSatDLT_Level2::GetGround2Image8(double X, double Y, double Z)
{
	Point2D<double> a;
		
	if(true == enable_normalization)
	{
		X = (X+Offset_G_X)*Scale_G_X;
		Y = (Y+Offset_G_Y)*Scale_G_Y;
		Z = (Z+Offset_G_Z)*Scale_G_Z;
		
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2)/(param.A3*X + param.B3*Y + param.C3*Z + 1);

		bool end;
		CSMMatrix<double> A, L, matX;
		int iter = 0;
		do
		{
			iter ++;
			A.Resize(2,2);
			L.Resize(2,1);
			
			A(0,0) = param.alpha*a.y - 1;
			A(0,1) = param.alpha*a.x;
			A(1,0) = 0;
			A(1,1) = 2*param.beta*a.y - 1;
						
			L(0,0) = -((param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*a.x*a.y - a.x);
			L(1,0) = -((param.A2*X + param.B2*Y + param.C2*Z + param.D2)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.beta*a.y*a.y - a.y);
			
			//matX = (A.Transpose()%A).Inverse()%A.Transpose()%L;
			matX = A.Inverse()%L;
			
			if(matX(0,0)<0.000001 && matX(1,0)<0.000001)
				end = true;
			if(iter >= 10)
				end = true;

			a.x += matX(0,0);
			a.y += matX(1,0);

		}while(end == false);
		

		a.x = (a.x/Scale_I_X)-Offset_I_X;
		a.y = (a.y/Scale_I_Y)-Offset_I_Y;
	}
	else
	{
		a.x = (param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1);
		a.y = (param.A2*X + param.B2*Y + param.C2*Z + param.D2)/(param.A3*X + param.B3*Y + param.C3*Z + 1);

		bool end;
		CSMMatrix<double> A, L, matX;
		int iter = 0;
		do
		{
			iter ++;
			A.Resize(2,2);
			L.Resize(2,1);
			
			A(0,0) = param.alpha*a.y - 1;
			A(0,1) = param.alpha*a.x;
			A(1,0) = 0;
			A(1,1) = 2*param.beta*a.y - 1;
						
			L(0,0) = -((param.A1*X + param.B1*Y + param.C1*Z + param.D1)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.alpha*a.x*a.y - a.x);
			L(1,0) = -((param.A2*X + param.B2*Y + param.C2*Z + param.D2)/(param.A3*X + param.B3*Y + param.C3*Z + 1) + param.beta*a.y*a.y - a.y);
			
			//matX = (A.Transpose()%A).Inverse()%A.Transpose()%L;
			matX = A.Inverse()%L;
			
			if(matX(0,0)<0.000001 && matX(1,0)<0.000001)
				end = true;
			if(iter >= 10)
				end = true;

			a.x += matX(0,0);
			a.y += matX(1,0);

		}while(end == false);
	}

	return a;
}

double * CSatDLT_Level2::Getcorrelation_coefficient(void)
{
	int i, j, k;
	correlation_coefficient = new double[NUM_SATDLT_PARAM_LEVEL2_2*(NUM_SATDLT_PARAM_LEVEL2_2-1)/2];
	k = -1;
	for(i=0; i<NUM_SATDLT_PARAM_LEVEL2_2; i++)
	{
		for(j=i+1; j<NUM_SATDLT_PARAM_LEVEL2_2; j++)
		{
			k ++;
			correlation_coefficient[k] = var_X(i,j)/sqrt(var_X(i,i)*var_X(j,j));
		}
	}

	return correlation_coefficient;
}

double * CSatDLT_Level2::Getcorrelation_coefficient2(void)
{
	int i, j, k;
	correlation_coefficient = new double[NUM_SATDLT_PARAM_LEVEL2*(NUM_SATDLT_PARAM_LEVEL2-1)/2];
	k = -1;
	for(i=0; i<NUM_SATDLT_PARAM_LEVEL2; i++)
	{
		for(j=i+1; j<NUM_SATDLT_PARAM_LEVEL2; j++)
		{
			k ++;
			correlation_coefficient[k] = var_X(i,j)/sqrt(var_X(i,i)*var_X(j,j));
		}
	}

	return correlation_coefficient;
}

double * CSatDLT_Level2::Getcorrelation_coefficient3(void)
{
	int i, j, k;
	correlation_coefficient = new double[NUM_SATDLT_PARAM_LEVEL2_3*(NUM_SATDLT_PARAM_LEVEL2_3-1)/2];
	k = -1;
	for(i=0; i<NUM_SATDLT_PARAM_LEVEL2_3; i++)
	{
		for(j=i+1; j<NUM_SATDLT_PARAM_LEVEL2_3; j++)
		{
			k ++;
			correlation_coefficient[k] = var_X(i,j)/sqrt(var_X(i,i)*var_X(j,j));
		}
	}

	return correlation_coefficient;
}
