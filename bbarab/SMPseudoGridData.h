/*
 * Copyright (c) 2005-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(__PSEUDO_GRID_HANDLER__BBARAB__)
#define __PSEUDO_GRID_HANDLER__BBARAB__

#include "SMPointCloud.h"
#include "SMPlaneFitting.h"
#include "KDTree.h"

//#define _NORMAL_ 0
//#define _ROOFS_ 1
//#define _WALLS_ 2

#define _IN_ 0
#define _SELECTION_ROOF_ 1
#define _SELECTION_WALL_ 2
#define _OUT_ 3
#define _GROUND_ 4

class _GridElement_
{
public:
	_GridElement_() {type = _IN_; PlaneID = -1;}
	~_GridElement_() {point.RemoveAll();}
	CSMList<_Point_> point;
	int type;//0: _NORMAL_ 1: _ROOF_ 2: _WALL_
	int PlaneID;
};

class _GridElement_PTS_
{
public:
	_GridElement_PTS_() {type = _IN_; PlaneID = -1;}
	~_GridElement_PTS_() {point.RemoveAll();}
	CSMList<_PTS_Point_> point;
	int type;//0: _NORMAL_ 1: _ROOF_ 2: _WALL_
	int PlaneID;
};

template<typename T>
class _Accumulator_
{
public:
	_Accumulator_() {memory = NULL;}

	_Accumulator_(int h, int w, int d, int n) 
	{
		memory = NULL;
		SetMemory(h, w, d, n);
	}
	
	~_Accumulator_()
	{
		Free();
	}

	void Free()
	{
		if(memory != NULL)
			delete[] memory;
	}

	bool SetMemory(int h, int w, int d, int n)
	{
		Free();
		width = w; height = h; depth = d; num = n;
		int size = w*h*d*n;
		if(size>=1)
			memory = new T[size];
		else
			return false;
		
		return true;
	}
	
	T& operator()(int iy, int ix=0, int iz=0, int index=0)
	{
		return memory[(width*height*iz + width*iy + ix)*num + index];
	}

private:
	T *memory;
	int width, height, depth, num;
};

class CSMAuxForXYZ
{
public:
	int count;
	double minX, maxX;
	double minY, maxY;
	double minZ, maxZ;
};

class IdxGroup
{
public:
	virtual ~IdxGroup() {idx_list.RemoveAll();}

	CSMList<int> idx_list;
	int group_id;
};

/**@class CSMPseudoGridData<br>
*@brief pseudo grid data handler.
*/
class CSMPseudoGridData
{
public:

	/**CSMPseudoGridData
	* Description	    : constructor
	* Return type		: 
	*/
	CSMPseudoGridData();

	/**~CSMPseudoGridData
	* Description	    : destructor
	* Return type		: 
	*/
	virtual ~CSMPseudoGridData();

	/**RemoveDuplication
	* Description	    : To remove duplicated data
	* Return type		: 
	*/
	void RemoveDuplication(CString fname, CString outname, double offset_X, double offset_Y, double offset_Z);

	/**SaveData
	* Description	    : To save data
	* Return type		: 
	*/
	void SaveData(CString fname);

	/**PlaneSearching_3DGrid_OneOrigin
	* Description	    : 
	* Return type		: bool 
	*@param int method
	*@param unsigned int searchsize
	*@param double variance
	*@param unsigned int max_iter_num
	*@param CString outpath
	*@param double max_dist
	*@param double bQuantize
	*@param unsigned int min_num_points
	*@param bool bOutParam
	*/
	//Using slope and aspect
	bool PlaneSearching_3DGrid_OneOrigin(int method, unsigned int W_x, unsigned int W_y, unsigned int W_z, unsigned int max_iter_num, CString outpath, double threshold, double Thresholdratio, unsigned int min_num_points, int ObjectType);
	//Using plane parameters
	bool PlaneSearching_3DGrid_OneOrigin_old(int method, unsigned int W_x, unsigned int W_y, unsigned int W_z, unsigned int max_iter_num, CString outpath, double threshold, double Thresholdratio, unsigned int min_num_points, int ObjectType);

	bool BuildingExtractionwith3DPseudoGrid(CString pointsfilepath, CString rawpath, double oX, double oY, double oZ, int method, unsigned int W_x, unsigned int W_y, unsigned int W_z, unsigned int max_iter_num, CString outpath, double threshold, double Thresholdratio, unsigned int min_num_points);

	/**ReadTXTPoints
	* Description			: To read cloud data
	*@param CString fname	: in file name
	*@param double offset_X	: X offset
	*@param double offset_Y	: Y offset
	*@param double offset_Z	: Z offset
	*@param bool bNormal	: data normalization(only shift)
	* Return type			: void
	*/
	void ReadTXTPoints(CString fname, double offset_X, double offset_Y, double offset_Z, bool bNormal);

	void ReadTXTPoints_Tree(CString fname, double &cx, double &cy, double &cz);

	void ReadCLSPoints_Tree(CString fname, double &cx, double &cy, double &cz);


	/**ReadRawData
	* Description			: To read cloud raw data
	*@param CString fname	: raw file path
	*@param CString cfgfile	: configuration file path
	* Return type			: void
	*/

	/**ReadTXTPoints
	* Description				: To save point cloud as a grid data
	*@param CString infname		: in file name
	*@param CString outfname	: Out file name
	*@param bool bAutoRes		: Availability of automatic determination of GRID resolution
	*@param double offset_X		: X offset
	*@param double offset_Y		: Y offset
	* Return type				: void
	*/
	void SaveAsGrid(CString infname, CString outfname, bool bAutoRes, double offset_X, double offset_Y, bool bcolor, bool bInterpolation, bool bIntensity, double Intensity_res);

	void SaveAsIntensityGrid(CString infname, CString outfname, double offset_X, double offset_Y, bool bcolor, bool bStretch=false);

	void VerticalPointsFreezing();
	void HorizontalPointsFreezing();
	void PointsMelting();

	bool CheckList(CSMList<int>& list, int ID);
	void MarkGround(int ID);

	void GroundPointsRemove(CString outpath, double offsetZ, double heightthreshold);
	int GroundSearch(CSMList<int> &GList, int half_size_y, int half_size_x, int half_size_z, int index_x, int index_y, int index_z);
	
	void FinalPlaneFitting(double res_threshold, double buildingheight, int max_iteration, int min_num_points, CString mergefilename);
	void FinalPlaneFitting_PTS(CString ptspath, double res_threshold, double buildingheight, int max_iteration, int min_num_points, CString mergefilename);

	/**ReadTXTPoints
	* Description				: To save point cloud as grid data
	*@param CString xyzfname	: input xyz file
	*@param CString gridfname	: output grid data file
	*@param double resX			: resolution X
	*@param double resY			: resolution Y
	*@param double resZ			: resolution Y
	* Return type				: void
	*/
	void XYZ2PseudoGrid(CString xyzfname, CString gridfname, double resX, double resY, double resZ, int w_c, int w_r, int w_d, int minNum);

	/**ReadAux
	* Description				: To read aux data for a given xyz file
	*@param CString xyzfilename	: input path (xyz file)
	* Return					: CSMAuxForXYZ
	*/
	CSMAuxForXYZ ReadAux(CString xyzfname);

	/**CheckOutlierinGrid
	* Description				: To check outlier included in a grid
	*@param double *grid		: grid data
	*@param int width			: width of a grid
	*@param int height			: height of a grid
	*@param int depth			: depth of a grid
	*@param int w_c				: search window size (column direction)
	*@param int w_r				: search window size (row direction)
	*@param int w_d				: search window size (depth direction)
	*@param int minNum			: minimum number of None zero cells
	* Return type				: void
	*/
	void CheckOutlierinGrid(FILEMEM *grid, int width, int height, int depth, int w_c, int w_r, int w_d, int minNum);

	/**CheckAloneCell
	* Description				: To check alone cell
	*@param double *grid		: grid data
	*@param int cur_c			: current position of search window (col)
	*@param int cur_r			: current position of search window (row)
	*@param int cur_d			: current position of search window (depth)
	*@param int width			: width of a grid
	*@param int height			: height of a grid
	*@param int depth			: depth of a grid
	*@param int w_c				: search window size (column direction)
	*@param int w_r				: search window size (row direction)
	*@param int w_d				: search window size (depth direction)
	* Return type int			: count of none-zero cells
	*/
	int CountNoneZeroCells(FILEMEM *grid, int cur_c, int cur_r, int cur_d, int width, int height, int depth, int w_c, int w_r, int w_d);

	/**CheckEmptyLayer
	* Description				: To check alone cell
	*@param double *grid		: grid data
	*@param int width			: width of a grid
	*@param int height			: height of a grid
	*@param int depth			: depth of a grid
	*@param int &minW			: boundary grid(min width index)
	*@param int &maxW			: boundary grid(max width index)
	*@param int &minH			: boundary grid(min height index)
	*@param int &maxH			: boundary grid(max height index)
	*@param int &minZ			: boundary grid(min depth index)
	*@param int &maxZ			: boundary grid(max depth index)
	* Return type				: void
	*/
	void CheckEmptyLayer(FILEMEM *grid, int width, int height, int depth, int &minW, int &maxW, int &minH, int &maxH, int &minD, int &maxD);

	void MakePseudoGridFromPTS(double offset_X, double offset_Y, double offset_Z, bool bNormal);

	void HorizontalPointsFreezing_PTS(CString outfilename);

	void ReadPTS(CString ptsfile);
	
	bool PlaneFittingForRoofs_old(CString pointsfilepath, double BS_x, double BS_y, double BS_z, unsigned int max_iter_num, CString outpath, double threshold);

	bool PlaneFittingForRoofs(CString pointsfilepath, double BS_x, double BS_y, double BS_z, unsigned int max_iter_num, CString outpath, double threshold);

	void NormalVectorClassification(CString norvectorfile, CString outpath, float Thresholdratio, float buffer_size_x, float buffer_size_y, float buffer_size_z, int min_num_points);
	void NormalVectorClassification_neighbor(int grp_index, CSMList<int> &class_list, int Pindex, double buffer[3], float Thresholdratio);

private:

	/**Point2GridPos
	* Description	    : To convert 3D coordinates to position in grid data
	* Return type		: bool
	*/
	bool Point2GridPos(double X, double Y, double Z, int &i, int &j, int &k);
	bool Point2GridPos(_Point_ p, int &i, int &j, int &k);

	/**PutPoint
	* Description	    : To input point data into grid data
	* Return type		: bool
	*/
	bool PutPoint(_Point_ p, int &i, int &j, int &k);

	bool PutPoint(_PTS_Point_ p, int &i, int &j, int &k);

	/**GLSforCoeff
	* Description	    : 
	* Return type		: bool 
	*@param int index_x
	*@param int index_y
	*@param int index_z
	*@param int half_size
	*@param double &a
	*@param double &b
	*@param double &c
	*@param double variance
	*@param unsigned int max_iter_num
	*@param double max_dist
	*@param double &pos_var
	*@param double limit_var
	*/
	bool GLSforCoeff(int index_x, int index_y, int index_z, double &a, double &b, double &c, unsigned int max_iter_num, double &pos_var, double limit_var);
	
	bool LSforCoeff(int index_x, int index_y, int index_z, double &a, double &b, double &c, unsigned int max_iter_num, double &pos_var);
	
	bool RobustLSforCoeff(int index_x, int index_y, int index_z, double &a, double &b, double &c, unsigned int max_iter_num, double &pos_var, double res_threshold);

	bool RobustLSforCoeff_Tree_old(double X, double Y, double Z, double BSX, double BSY, double BSZ, unsigned int max_iter_num, double res_threshold, double &a0, double &b0, double &c0);
	bool RobustLSforCoeff_Tree(double X, double Y, double Z, double BSX, double BSY, double BSZ, unsigned int max_iter_num, double res_threshold, CSMList<KDNode> &pointlist);
	bool RobustLSforCoeff_Tree_nonlinear(double X, double Y, double Z, double BSX, double BSY, double BSZ, unsigned int max_iter_num, double res_threshold);
	bool RobustLSforCoeff_Tree(CSMList<KDNode> pointlist, unsigned int max_iter_num, double res_threshold);


	//old one: recursive function
	void DistanceMapClassification_neighbor_old(CSMList<int> &num_element, int &index, int index_y, int index_x, int index_z, float Thresholdratio, _Accumulator_<int> &indexmap, _Accumulator_<float> &accu);

	//non-recursive function
	void DistanceMapClassification_neighbor(CSMList<int> &class_list, int &index, int index_y, int index_x, int index_z, float Thresholdratio, _Accumulator_<int> &indexmap, _Accumulator_<float> &accu, float Ta, float Tb, float Tc, float Tdist);

	//Re-arrange class codes

	//old one: using recursive function
	void DistanceMapClassification_old(float Threshold, _Accumulator_<float> &accu, CString outpath, int min_num_points, CString strProgressBar, int ObjectType);

	//new one
	void DistanceMapClassification(float Threshold, _Accumulator_<float> &accu, CString outpath, int min_num_points, CString strProgressBar, int ObjectType);

	void UpdateDistance(_Accumulator_<float> &accu, int index_y, int index_x, int index_z, double a, double b, double c);

	void UpdateDistance_old(_Accumulator_<float> &accu, int index_y, int index_x, int index_z, double A, double B, double C);

	void DeleteMemory();

	void ReAssignClassCode(CSMList<int> &class_list, _Accumulator_<int> &indexmap);

private:
	CSMPointCloud m_PointsCloud;
	double sX, sY, sZ;
	double oX, oY, oZ;
	int width, height, depth;
	_GridElement_ ***m_Grid;
	_GridElement_PTS_ ***m_Grid_PTS;
	float min_a, max_a;
	float min_b, max_b;
	float min_c, max_c;
	float range_a, range_b, range_c;
	int half_size_x, half_size_y, half_size_z;
	bool bNormalize;
	double ShiftX, ShiftY, ShiftZ;
	int nPlaneIndex;
	//bool bRaw;
	CString RawPath;

	KDTree tree;
};

#endif // !defined(__PSEUDO_GRID_HANDLER__BBARAB__)