/*
* AtmosphericCorrection.h
*
*  Created on: May 08, 2017
*      Author: kiinbang
*/
#pragma once

#include <math.h>
#include <Eigen/Dense>

const double tempGradient = -0.0065; // temperature gradient k/m
const double molecularWeight = 28.9644; // constant molecular weight of air
const double univGasConst = 8314.32; // universal gas constant
const double mHalfSpeedOfLight = 149896229.0; // half of the speed of light
const double na0 = 1.000277; //Nominal refractive index of air
const double vt0 = mHalfSpeedOfLight / na0;
const double v1 = 114000.0;
const double v2 = 62400.0;
const double deltat = 0.000001;

class AtmosphericCorrectionModel
{
public:
	AtmosphericCorrectionModel(const double temperatureH0, const double h0/*Height given temperature*/, const double pMSL, const double waveLength);

	~AtmosphericCorrectionModel();

	Eigen::Vector3d calRefractiveVectorNED(const double travelTime, const double FH, const double latitudeRad, const double& lasDirVecN, const double& lasDirVecE, const double& lasDirVecD);

	Eigen::Vector3d calRefractiveVector(const double travelTime, const double FH, const double latitudeRad, const double& lasDirVecE, const double& lasDirVecN, const double& lasDirVecU);

protected:
	// Ground temperature in celsius
	const double m_groundTemperature;
	//Temperature at MSL in Kelvin
	const double m_temperatureKlvMSL;
	// Air pressure at MSL in kpa
	const double m_airPressureMSL;
	// Laser wave length in nm
	const double m_waveLength;
	// wavenumber(cm^-1) with respect to the specific wavelength
	const double m_waveNum;
	const double m_C2;
};