/*
 * Copyright (c) 2008-2008, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice and this list of conditions.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice and this list of conditions in the documentation and/or 
 *    other materials provided with the distribution.
 */

// LIDARStripAdjustment.h
//
//////////////////////////////////////////////////////////////////////
//
//Written by Bang, Ki In
//
//Since 2008/08/29
//
#if !defined(___LiDAR_Strip_Adjustment_SSMATICS___)
#define ___LiDAR_Strip_Adjustment_SSMATICS___

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include <vld.h>//memory leak detector

#include "KDTree.h"
#include <fstream.h>
#include "UtilityGrocery.h"
#include "SMMatrix.h"
#include "SMRotationMatrix.h"

#define MAXTXTLENGTH 512

namespace SMATICS_BBARAB
{

/**
* @class CSMLiDARStripAdjustment
* @brief class for LiDAR strip adjustment\n
* @author Bang, Ki In
* @version 1.0
* @Since 2008-08-29
* @bug N/A.
* @warning N/A.
*
* \nThis is a class of LiDAR strip adjustment program.
* <ul> 
* <li> Transformation functions
*	<ol>
*	<li> 1. 4 parameters: 3 shifts and 1 rotation(roll angle)
*	<li> 2. 7 parameters: 3 shifts, 3 rotations, and 1 scale factor
*	</ol>
* </ul>\n
*/
struct _3DPOINT_
{
	double p[3];
};

struct _MatchedPair_
{
	double tar[3];
	double ref[3];
};

class CSMLiDARStripAdjustment
{
public:

	CSMLiDARStripAdjustment() { Init(); }

	virtual ~CSMLiDARStripAdjustment(){}

	void Init()
	{
		s0 = 1.0;
		sigma_Tar[0] = sigma_Tar[1] = sigma_Tar[2] = s0;
		sigma_Ref[0] = sigma_Ref[1] = sigma_Ref[2] = s0;
		
		Trans.Resize(3,1,0.0);
		Unshifted_Trans.Resize(3,1,0.0);
		
		Omega = Phi = Kappa = 0.0;
		
		Scale = 1.0;

		refsurf.RemoveAll();
		tarsurf.RemoveAll();
		pairs_ICP.RemoveAll();

		bconfigured = false;
	}

	void Configuration(double sigma_target[], double sigma_reference[], double priori_s, double dist_threshold, unsigned int max_iter, double max_diff_sigma, double init_S, double init_O, double init_P, double init_K, double init_Tx, double init_Ty, double init_Tz)
	{
		memcpy((void*)sigma_Ref, (void*)sigma_reference, sizeof(double)*3);
		memcpy((void*)sigma_Tar, (void*)sigma_target, sizeof(double)*3);

		s0 = priori_s;
		this->dist_threshold = dist_threshold;
		max_iteration = max_iter;
		this->max_diff_sigma = max_diff_sigma;

		Scale = init_S;
		Omega = Deg2Rad(init_O/3600);
		Phi = Deg2Rad(init_P/3600);
		Kappa = Deg2Rad(init_K/3600);
		Unshifted_Trans(0,0) = init_Tx;
		Unshifted_Trans(1,0) = init_Ty;
		Unshifted_Trans(2,0) = init_Tz;

		bconfigured = true;
	}

	void ReDefineCenter(void)
	{
		unsigned int num_tar_points = tarsurf.GetNumItem(), num_ref_points = refsurf.nList, i;
		//Calculate centroid
		data_center[0] = 0;
		data_center[1] = 0;
		data_center[2] = 0;
		
		for(i=0; i<num_tar_points; i++)
		{
			data_center[0] += tarsurf[i].p[0];
			data_center[1] += tarsurf[i].p[1];
			data_center[2] += tarsurf[i].p[2];
		}

		for(i=0; i<num_ref_points; i++)
		{
			data_center[0] += refsurf.GetAt(i)->Get_x_(0);
			data_center[1] += refsurf.GetAt(i)->Get_x_(1);
			data_center[2] += refsurf.GetAt(i)->Get_x_(2);
		}
		
		data_center[0] /= num_tar_points + num_ref_points;
		data_center[1] /= num_tar_points + num_ref_points;
		data_center[2] /= num_tar_points + num_ref_points;

		for(i=0; i<num_tar_points; i++)
		{
			tarsurf[i].p[0] -= data_center[0];
			tarsurf[i].p[1] -= data_center[1];
			tarsurf[i].p[2] -= data_center[2];
		}
		
		for(i=0; i<num_ref_points; i++)
		{
			refsurf.GetAt(i)->Set_x_(0, refsurf.GetAt(i)->Get_x_(0) - data_center[0]);
			refsurf.GetAt(i)->Set_x_(1, refsurf.GetAt(i)->Get_x_(1) - data_center[1]);
			refsurf.GetAt(i)->Set_x_(2, refsurf.GetAt(i)->Get_x_(2) - data_center[2]);
		}

		_Rmat_ _R_(Omega, Phi, Kappa); //rotation matrix
		CSMMatrix<double> shift(3,1);
		shift(0,0) = data_center[0];
		shift(1,0) = data_center[1];
		shift(2,0) = data_center[2];
		
		Trans = (_R_.Rmatrix%shift)*Scale - shift + Unshifted_Trans;

		resfile<<"New center for the given datasets: "<<data_center[0]<<"\t"<<data_center[1]<<"\t"<<data_center[2]<<endl<<endl;		
	}

	bool RunStripAdjustment(char* tarpath, char* refpath, char* respath, int method)
	{
		if(false == FileOpen(tarpath, refpath, respath))
			return false;
		
		resfile<<"[Target surface: "<<tarpath<<" ]"<<endl;
		resfile<<"[Reference surface: "<<refpath<<" ]"<<endl<<endl;

		resfile<<"-Reading tararget surface-"<<endl;

		TarSurfRead(tarfile);

		resfile<<tarsurf.GetNumItem()<<" points"<<endl;
		
		resfile<<"-Reading reference surface-"<<endl;

		RefSurfRead(reffile);

		resfile<<refsurf.nList<<" points"<<endl;

		ReDefineCenter();

		if(method == 0)
		{
			bool bContinue = true;
			unsigned int repeat_count = 0;
			double current_sigma;
			double old_sigma;
			do
			{
				resfile<<endl<<endl;
				resfile<<"*********************************************************"<<endl;
				resfile<<"[ "<<++repeat_count<<" ]th ICP procedure"<<endl;
				resfile<<"*********************************************************"<<endl;

				resfile<<"-Adjust target surface using current parameters-"<<endl;
				
				AdjustTarsurf();
				
				resfile<<"-Surface match by ICP-"<<endl;
				
				double ave_dist;
				if( false == SurfaceMatch_ICP(ave_dist) )
					return false;
				
				resfile<<"-Estimate tranformation parameters-"<<endl;
				
				EstimateTransformation_ICP_GLS(current_sigma);
				//EstimateTransformation_ICP_GLS_II(current_sigma);

				if(repeat_count > 1)
				{
					if(fabs(current_sigma - old_sigma) < max_diff_sigma)
						bContinue = false;
				}

				old_sigma = current_sigma;
				
				if(repeat_count >= this->max_iteration)
					bContinue = false;

				CSMMatrix<double> shift(3,1);
				shift(0,0) = data_center[0];
				shift(1,0) = data_center[1];
				shift(2,0) = data_center[2];
				
				resfile<<"Centroid for the given datasets: "<<data_center[0]<<"\t"<<data_center[1]<<"\t"<<data_center[2]<<endl<<endl;		
				
				_Rmat_ _R_(Omega, Phi, Kappa); //rotation matrix
				Unshifted_Trans = shift - (_R_.Rmatrix%shift)*Scale + Trans;

				resfile<<"[Unshifted transformation parameters]"<<endl;
				resfile<<"Scale: \t"<<Scale<<endl;
				resfile<<"Omega(sec): \t"<<Rad2Deg(Omega*3600)<<endl;
				resfile<<"Phi(sec): \t"<<Rad2Deg(Phi*3600)<<endl;
				resfile<<"Kappa(sec): \t"<<Rad2Deg(Kappa*3600)<<endl;
				
				resfile<<"Tx: \t"<<Unshifted_Trans(0,0)<<endl;
				resfile<<"Ty: \t"<<Unshifted_Trans(1,0)<<endl;
				resfile<<"Tz: \t"<<Unshifted_Trans(2,0)<<endl<<endl;

			} while(bContinue);
			
		}
		else if(method == 1)
		{

		}
		else 
			return false;

		AdjustTarsurf();

		reffile<<endl<<"[Final adjusted_tarsurf (shifted coordinates)]"<<endl;
		for(unsigned int i=0; i<adjusted_tarsurf.GetNumItem(); i++)
		{
			resfile<<adjusted_tarsurf[i].p[0]<<"\t";
			resfile<<adjusted_tarsurf[i].p[1]<<"\t";
			resfile<<adjusted_tarsurf[i].p[2]<<endl;
		}
		
		CSMMatrix<double> P(3,1), Q;
		_Rmat_ R(Omega, Phi, Kappa);

		reffile<<endl<<"[Transformed original coordinates]"<<endl;

		for(i=0; i<adjusted_tarsurf.GetNumItem(); i++)
		{
			P(0,0) = tarsurf[i].p[0] + data_center[0];
			P(1,0) = tarsurf[i].p[1] + data_center[1];
			P(2,0) = tarsurf[i].p[2] + data_center[2];

			Q = (R.Rmatrix%P)*Scale + Unshifted_Trans;

			resfile<<Q(0,0)<<"\t";
			resfile<<Q(1,0)<<"\t";
			resfile<<Q(2,0)<<endl;
		}

		resfile.close();

		return true;
	}

private:
	bool bconfigured;

	KDTree refsurf;//KD-tree data (reference surface)
	CSMList<_3DPOINT_> tarsurf;//List data (target surface)
	CSMList<_3DPOINT_> adjusted_tarsurf;//List data (adjusted target surface)
	CSMList<_MatchedPair_> pairs_ICP;//Matched pairs for ICP porcedure
	double sigma_Ref[3];//precision(sigma) for refsurf
	double sigma_Tar[3];//precision(sigma) for tarsurf

	double s0;//priori sigma (default: 1.0)

	double tar_center[3];	
	double ref_center[3];
	double data_center[3];

	double dist_threshold;

	//7 parameters
	double Omega, Phi, Kappa, Scale;
	CSMMatrix<double> Trans;
	CSMMatrix<double> Unshifted_Trans;

	//LS configuration
	unsigned int max_iteration;
	double max_diff_sigma;

	//input and output files
	fstream tarfile, reffile, resfile;

private:
	
	bool FileOpen(char* tarpath, char* refpath, char* respath)
	{
		if(false == SMATICS_BBARAB::ASCIIReadOpen(tarfile, tarpath))
		{
			::MessageBox(NULL, "tarfile open error!", "Error", MB_OK);
			return false;
		}
		
		if(false == SMATICS_BBARAB::ASCIIReadOpen(reffile, refpath))
		{
			::MessageBox(NULL, "reffile open error!", "Error", MB_OK);
			return false;
		}
		
		if(false == SMATICS_BBARAB::ASCIISaveOpen(resfile, respath))
		{
			::MessageBox(NULL, "resfile open error!", "Error", MB_OK);
			return false;
		}
		else
		{
			SMATICS_BBARAB::ASCIIFilePrecision(resfile, 6);
		}
		
		return true;
	}

	void TarSurfRead(fstream &tarfile)
	{
		_3DPOINT_ x;
		char line[MAXTXTLENGTH];

		while(!tarfile.eof())
		{
			
			tarfile>>x.p[0]>>x.p[1]>>x.p[2];
			
			tarfile.getline(line, MAXTXTLENGTH);
			
			tarfile.eatwhite();
			
			tarsurf.AddTail(x);
		}

		tarfile.close();
	}

	void RefSurfRead(fstream &reffile)
	{
		//KD-tree configuration
		refsurf.InitialSetup(KD_MAX_POINTS,3,0);
		double x[3];
		char line[MAXTXTLENGTH];

		while(!reffile.eof())
		{
			
			reffile>>x[0]>>x[1]>>x[2];
			
			reffile.getline(line, MAXTXTLENGTH);
			
			reffile.eatwhite();
			
			refsurf.add(x);
		}

		reffile.close();

	}

	void AdjustTarsurf()
	{
		adjusted_tarsurf.RemoveAll();

		unsigned int num_tar_points = tarsurf.GetNumItem();

		_Rmat_ R(Omega, Phi, Kappa);
		CSMMatrix<double> P(3,1), Q;
		_3DPOINT_ q;

		for(unsigned int i=0; i<num_tar_points; i++)
		{
			P(0,0) = tarsurf[i].p[0];
			P(1,0) = tarsurf[i].p[1];
			P(2,0) = tarsurf[i].p[2];

			Q = (R.Rmatrix%P)*Scale + Trans;
			
			q.p[0] = Q(0,0);
			q.p[1] = Q(1,0);
			q.p[2] = Q(2,0);

			adjusted_tarsurf.AddTail(q);
		}
	}

	bool MatchPair_ICP(double tar[], double ref[], double &dist, double dist_threshold)
	{
		KDNode *node = refsurf.find_nearest(tar);

		double dx = tar[0] - node->Get_x_(0);
		double dy = tar[1] - node->Get_x_(1);
		double dz = tar[2] - node->Get_x_(2);

		dist = sqrt(dx*dx + dy*dy + dz*dz);

		if(dist < dist_threshold)
		{
			ref[0] = node->Get_x_(0);
			ref[1] = node->Get_x_(1);
			ref[2] = node->Get_x_(2);

			return true;
		}
		else
			return false;
	}

	bool SurfaceMatch_ICP(double &ave_dist)
	{
		pairs_ICP.RemoveAll();

		unsigned num_tar_points = tarsurf.GetNumItem();

		_MatchedPair_ pair;
		double sum_dist = 0;
		double dist;
		double tar[3], ref[3];

		for(unsigned int i=0; i<num_tar_points; i++)
		{
			memcpy((void*)tar, (void*)adjusted_tarsurf[i].p, sizeof(double)*3);

			if( true == MatchPair_ICP(tar, ref, dist, dist_threshold) )
			{
				sum_dist += dist;

				memcpy((void*)pair.tar, (void*)tarsurf[i].p, sizeof(double)*3);
				memcpy((void*)pair.ref, (void*)ref, sizeof(double)*3);

				pairs_ICP.AddTail(pair);
			}
		}

		if( pairs_ICP.GetNumItem() < 8)
		{
			resfile<<"Matched pairs: "<<pairs_ICP.GetNumItem()<<endl;
			return false;
		}
		else
		{
			ave_dist = sum_dist / pairs_ICP.GetNumItem();
			
			resfile<<"Distance threshold: "<<dist_threshold<<endl;
			resfile<<"Matched pairs: "<<pairs_ICP.GetNumItem()<<endl;
			resfile<<"Average distance between matched pairs: "<<ave_dist<<endl<<endl;
			
			return true;
		}
	}

	void CorrelationMat(CSMMatrix<double> &Nmat, CSMMatrix<double> &Corrmat)
	{
		unsigned int Cols = Nmat.GetCols();
		unsigned int Rows = Nmat.GetRows();
		Corrmat.Resize(Rows, Cols, 0.0);
		CSMMatrix<double> Nmat_inv = Nmat.Inverse();

		for(unsigned int r = 0; r < Rows; r++)
		{
			for(unsigned int c = 0; c < Cols; c++)
			{
				Corrmat(r,c) = Nmat_inv(r,c)/sqrt(Nmat_inv(r,r))/sqrt(Nmat_inv(c,c));
			}
		}

	}

	char* GetParameterName(unsigned int index)
	{
		switch(index)
		{
		case 0:
			return "Scale";
		case 1:
			return "Omega";
		case 2:
			return "Phi";
		case 3:
			return "Kappa";
		case 4:
			return "Tx";
		case 5:
			return "Ty";
		case 6:
			return "Tz";
		default:
			return "";
		}
	}

	void EstimateTransformation_ICP_GLS(double &final_sigma)//shift to center
	{
		//Ref = S.R.Tar + T;
		//Ref_ave = S.R.Tar_ave + T
		//Ref = S.R.Tar + S.R.Tar_ave - S.R.Tar_ave + T
		//Ref = S.R.(Tar - Tar_ave) + S.R.Tar_ave + T
		//Ref - (S.R.Tar_ave + T) = S.R.(Tar - Tar_ave)
		//therefore, Ref - Ref_ave = S.R.(Tar - Tar_ave) -----(1)
		//and, T = Ref_ave - S.R.Tar_ave -----(2)

		unsigned int i, j;
		unsigned int num_pairs = pairs_ICP.GetNumItem();

		//Calculate centroid
		tar_center[0] = 0;
		tar_center[1] = 0;
		tar_center[2] = 0;
		
		ref_center[0] = 0;
		ref_center[1] = 0;
		ref_center[2] = 0;
		
		for(i=0; i<num_pairs; i++)
		{
			tar_center[0] += pairs_ICP[i].tar[0];
			tar_center[1] += pairs_ICP[i].tar[1];
			tar_center[2] += pairs_ICP[i].tar[2];
			
			ref_center[0] += pairs_ICP[i].ref[0];
			ref_center[1] += pairs_ICP[i].ref[1];
			ref_center[2] += pairs_ICP[i].ref[2];
		}
		
		ref_center[0] /= num_pairs;
		ref_center[1] /= num_pairs;
		ref_center[2] /= num_pairs;
		
		tar_center[0] /= num_pairs;
		tar_center[1] /= num_pairs;
		tar_center[2] /= num_pairs;
		
		resfile<<"Centroid of matched target points: "<<tar_center[0]<<','<<tar_center[1]<<','<<tar_center[2]<<endl;
		resfile<<"Centroid of matched referecne points: "<<ref_center[0]<<','<<ref_center[1]<<','<<ref_center[2]<<endl<<endl;

		CSMMatrix<double> A, X, L, B, N, C, Q, P, minusI;
		
		Q.Resize(6,6,0.0);
		Q(0,0) = sigma_Tar[0]*sigma_Tar[0]/s0/s0;
		Q(1,1) = sigma_Tar[1]*sigma_Tar[1]/s0/s0;
		Q(2,2) = sigma_Tar[2]*sigma_Tar[2]/s0/s0;

		Q(3,3) = sigma_Ref[0]*sigma_Ref[0]/s0/s0;
		Q(4,4) = sigma_Ref[1]*sigma_Ref[1]/s0/s0;
		Q(5,5) = sigma_Ref[2]*sigma_Ref[2]/s0/s0;

		minusI.Resize(3,3,0.0);
		minusI.Identity(-1.0);

		bool bContinue = true;
		double vtv;
		double sigma, old_sigma;
		unsigned int iter = 0;

		do
		{
			iter ++;

			resfile<<"----------------------------"<<endl;
			resfile<<"Iteration #: "<<iter<<endl;
			resfile<<"----------------------------"<<endl<<endl;

			A.Resize(3,4,0.0);
			L.Resize(3,1,0.0);
			B.Resize(3,6,0.0);
			
			N.Resize(4,4,0.0);
			C.Resize(4,1,0.0);
			
			_Rmat_ _R_(Omega, Phi, Kappa); //rotation matrix
			_Rmat_ _dRdO_, _dRdP_, _dRdK_;
			_dRdO_.Partial_dRdO(Omega, Phi, Kappa);
			_dRdP_.Partial_dRdP(Omega, Phi, Kappa);
			_dRdK_.Partial_dRdK(Omega, Phi, Kappa);
			
			CSMMatrix<double> R, dRdO, dRdP, dRdK;
			R = _R_.Rmatrix;
			dRdO = _dRdO_.Rmatrix;
			dRdP = _dRdP_.Rmatrix;
			dRdK = _dRdK_.Rmatrix;		
			
			//B matrix
			B.Insert(0,0,R*Scale);
			B.Insert(0,3,minusI);
			
			P = (B%Q%B.Transpose()).Inverse();

			vtv = 0.0;
			
			for(i=0;i<num_pairs;i++)
			{
				_MatchedPair_ pair = pairs_ICP[i];
				
				pair.ref[0] -= ref_center[0];
				pair.ref[1] -= ref_center[1];
				pair.ref[2] -= ref_center[2];

				pair.tar[0] -= tar_center[0];
				pair.tar[1] -= tar_center[1];
				pair.tar[2] -= tar_center[2];
				
				for(j=0;j<3;j++)
				{
					//A matrix(S,omega,phi,kappa)
					A(j,0) = R(j,0)*pair.tar[0] + R(j,1)*pair.tar[1] + R(j,2)*pair.tar[2];
					A(j,1) = Scale*(dRdO(j,0)*pair.tar[0] + dRdO(j,1)*pair.tar[1] + dRdO(j,2)*pair.tar[2]);
					A(j,2) = Scale*(dRdP(j,0)*pair.tar[0] + dRdP(j,1)*pair.tar[1] + dRdP(j,2)*pair.tar[2]);
					A(j,3) = Scale*(dRdK(j,0)*pair.tar[0] + dRdK(j,1)*pair.tar[1] + dRdK(j,2)*pair.tar[2]);

					//L matrix
					L(j,0) = pair.ref[j] - Scale*(R(j,0)*pair.tar[0] + R(j,1)*pair.tar[1] + R(j,2)*pair.tar[2]);
				}
				
				N += A.Transpose()%P%A;
				C += A.Transpose()%P%L;

				vtv += (L.Transpose()%P%L)(0,0);
			}
			
			X = N.Inverse()%C;

			resfile<<"[Unknowns]"<<endl;
			resfile.precision(7);
			resfile<<"Scale: \t"<<Scale<<"\t + ( "<<X(0,0)<<" )"<<endl;
			resfile<<"Omega(sec): \t"<<Rad2Deg(Omega*3600)<<"\t + ( "<<Rad2Deg(X(1,0)*3600)<<" )"<<endl;
			resfile<<"Phi(sec): \t"<<Rad2Deg(Phi*3600)<<"\t + ( "<<Rad2Deg(X(2,0)*3600)<<" )"<<endl;
			resfile<<"Kappa(sec): \t"<<Rad2Deg(Kappa*3600)<<"\t + ( "<<Rad2Deg(X(3,0)*3600)<<" )"<<endl;
			
			Scale += X(0,0);
			Omega += X(1,0);
			Phi   += X(2,0);
			Kappa += X(3,0);

			sigma = sqrt(vtv/(num_pairs*3-4));

			if(iter >= max_iteration)
			{
				bContinue = false;
			}

			if(iter > 1)
			{
				if( fabs(sigma - old_sigma) < max_diff_sigma)
				{
					bContinue = false;
				}

				resfile<<"[Previous sigma: "<<old_sigma<<" ]"<<endl;
			}

			resfile<<"[Sigma: "<<sigma<<" ]"<<endl;

			old_sigma = sigma;
			
		}while(bContinue);

		final_sigma = sigma;

		CSMMatrix<double> Corrmat;
		this->CorrelationMat(N, Corrmat);
		resfile<<endl<<"[Final correlation]"<<endl;
		for(i=0; i<Corrmat.GetRows(); i++)
		{
			for(j=i+1; j<Corrmat.GetCols(); j++)
			{
				resfile<<'['<<GetParameterName(i)<<','<<GetParameterName(j)<<']'<<"\t"<<Corrmat(i,j)<<endl;
			}
		}
		resfile<<endl<<Corrmat.matrixout()<<endl;

		CSMMatrix<double> Ref_ave(3,1), Tar_ave(3,1);

		Ref_ave(0,0) = ref_center[0];
		Ref_ave(1,0) = ref_center[1];
		Ref_ave(2,0) = ref_center[2];
		
		Tar_ave(0,0) = tar_center[0];
		Tar_ave(1,0) = tar_center[1];
		Tar_ave(2,0) = tar_center[2];

		_Rmat_ _R_(Omega, Phi, Kappa); //rotation matrix

		Trans = Ref_ave - (_R_.Rmatrix%Tar_ave)*Scale;

		resfile<<endl<<"[Final estimation]"<<endl;
		resfile<<"Scale: \t"<<Scale<<endl;
		resfile<<"Omega(sec): \t"<<Rad2Deg(Omega*3600)<<endl;
		resfile<<"Phi(sec): \t"<<Rad2Deg(Phi*3600)<<endl;
		resfile<<"Kappa(sec): \t"<<Rad2Deg(Kappa*3600)<<endl;
		resfile<<"Tx: \t"<<Trans(0,0)<<endl;
		resfile<<"Ty: \t"<<Trans(1,0)<<endl;
		resfile<<"Tz: \t"<<Trans(2,0)<<endl<<endl;
	}

	void EstimateTransformation_ICP_GLS_II(double &final_sigma)//no shift
	{
		//Ref = S.R.Tar + T;

		unsigned int num_pairs = pairs_ICP.GetNumItem();
		
		unsigned int i, j;

		CSMMatrix<double> A, X, L, B, N, C, Q, P, minusI;
		Q.Resize(6,6,0.0);
		Q(0,0) = sigma_Tar[0]*sigma_Tar[0]/s0/s0;
		Q(1,1) = sigma_Tar[1]*sigma_Tar[1]/s0/s0;
		Q(2,2) = sigma_Tar[2]*sigma_Tar[2]/s0/s0;
		
		Q(3,3) = sigma_Ref[0]*sigma_Ref[0]/s0/s0;
		Q(4,4) = sigma_Ref[1]*sigma_Ref[1]/s0/s0;
		Q(5,5) = sigma_Ref[2]*sigma_Ref[2]/s0/s0;
		
		minusI.Resize(3,3,0.0);
		minusI.Identity(-1.0);
		
		bool bContinue = true;
		double vtv;
		double sigma, old_sigma;
		unsigned int iter = 0;
		
		do
		{
			iter ++;

			resfile<<"----------------------------"<<endl;
			resfile<<"Iteration #: "<<iter<<endl;
			resfile<<"----------------------------"<<endl<<endl;
			
			A.Resize(3,7,0.0);
			L.Resize(3,1,0.0);
			B.Resize(3,6,0.0);
			
			N.Resize(7,7,0.0);
			C.Resize(7,1,0.0);
			
			_Rmat_ _R_(Omega, Phi, Kappa); //rotation matrix
			_Rmat_ _dRdO_, _dRdP_, _dRdK_;
			_dRdO_.Partial_dRdO(Omega, Phi, Kappa);
			_dRdP_.Partial_dRdP(Omega, Phi, Kappa);
			_dRdK_.Partial_dRdK(Omega, Phi, Kappa);
			
			CSMMatrix<double> R, dRdO, dRdP, dRdK;
			R = _R_.Rmatrix;
			dRdO = _dRdO_.Rmatrix;
			dRdP = _dRdP_.Rmatrix;
			dRdK = _dRdK_.Rmatrix;		
			
			//B matrix
			B.Insert(0,0,R*Scale);
			B.Insert(0,3,minusI);
			
			P = (B%Q%B.Transpose()).Inverse();
			
			vtv = 0.0;
			
			for(i=0;i<num_pairs;i++)
			{
				_MatchedPair_ pair = pairs_ICP[i];

				for(j=0;j<3;j++)
				{
					//A matrix(S,omega,phi,kappa)
					A(j,0) = R(j,0)*pair.tar[0] + R(j,1)*pair.tar[1] + R(j,2)*pair.tar[2];
					A(j,1) = Scale*(dRdO(j,0)*pair.tar[0] + dRdO(j,1)*pair.tar[1] + dRdO(j,2)*pair.tar[2]);
					A(j,2) = Scale*(dRdP(j,0)*pair.tar[0] + dRdP(j,1)*pair.tar[1] + dRdP(j,2)*pair.tar[2]);
					A(j,3) = Scale*(dRdK(j,0)*pair.tar[0] + dRdK(j,1)*pair.tar[1] + dRdK(j,2)*pair.tar[2]);

					//A matrix(T)
					A(j,4+j) = 1.0;
					
					//L matrix
					L(j,0) = pair.ref[j] - Scale*(R(j,0)*pair.tar[0] + R(j,1)*pair.tar[1] + R(j,2)*pair.tar[2]) - Trans(j,0);
				}
				
				N += A.Transpose()%P%A;
				C += A.Transpose()%P%L;
				
				vtv += (L.Transpose()%P%L)(0,0);
			}
			
			X = N.Inverse()%C;

			resfile<<"[Unknowns]"<<endl;
			resfile.precision(7);
			resfile<<"Scale: \t"<<Scale<<"\t + ( "<<X(0,0)<<" )"<<endl;
			resfile<<"Omega(sec): \t"<<Rad2Deg(Omega*3600)<<"\t + ( "<<Rad2Deg(X(1,0)*3600)<<" )"<<endl;
			resfile<<"Phi(sec): \t"<<Rad2Deg(Phi*3600)<<"\t + ( "<<Rad2Deg(X(2,0)*3600)<<" )"<<endl;
			resfile<<"Kappa(sec): \t"<<Rad2Deg(Kappa*3600)<<"\t + ( "<<Rad2Deg(X(3,0)*3600)<<" )"<<endl;
			resfile<<"Tx: \t"<<Trans(0,0)<<"\t + ( "<<X(4,0)<<" )"<<endl;
			resfile<<"Ty: \t"<<Trans(1,0)<<"\t + ( "<<X(5,0)<<" )"<<endl;
			resfile<<"Tz: \t"<<Trans(2,0)<<"\t + ( "<<X(6,0)<<" )"<<endl<<endl<<endl;
			
			Scale += X(0,0);
			Omega += X(1,0);
			Phi   += X(2,0);
			Kappa += X(3,0);
			Trans(0,0) += X(4,0);
			Trans(1,0) += X(5,0);
			Trans(2,0) += X(6,0);
			
			sigma = sqrt(vtv/(num_pairs*3-7));
			
			if(iter >= max_iteration)
			{
				bContinue = false;
			}
			
			if(iter > 1)
			{
				if( fabs(sigma - old_sigma) < max_diff_sigma)
				{
					bContinue = false;
				}
				
				resfile<<"[Previous sigma: "<<old_sigma<<" ]"<<endl;
			}
			
			resfile<<"[Sigma: "<<sigma<<" ]"<<endl;
			
			old_sigma = sigma;
				
		}while(bContinue);

		final_sigma = sigma;

		CSMMatrix<double> Corrmat;
		this->CorrelationMat(N, Corrmat);
		resfile<<endl<<"[Final correlation]"<<endl;
		for(i=0; i<Corrmat.GetRows(); i++)
		{
			for(j=i+1; j<Corrmat.GetCols(); j++)
			{
				resfile<<'['<<GetParameterName(i)<<','<<GetParameterName(j)<<']'<<"\t"<<Corrmat(i,j)<<endl;
			}
		}

		resfile<<endl<<Corrmat.matrixout()<<endl;

		resfile<<endl<<endl<<"[Final estimation]"<<endl;
		resfile<<"Scale: \t"<<Scale<<endl;
		resfile<<"Omega(sec): \t"<<Rad2Deg(Omega*3600)<<endl;
		resfile<<"Phi(sec): \t"<<Rad2Deg(Phi*3600)<<endl;
		resfile<<"Kappa(sec): \t"<<Rad2Deg(Kappa*3600)<<endl;
		resfile<<"Tx: \t"<<Trans(0,0)<<endl;
		resfile<<"Ty: \t"<<Trans(1,0)<<endl;
		resfile<<"Tz: \t"<<Trans(2,0)<<endl<<endl;
	}
};

} //namespace LIDARCALIBRATION_BBARAB

#endif // !defined(___LiDAR_Strip_Adjustment_SSMATICS___)