// SMDataParser.cpp: implementation of the CSMDataParser class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SMDataParser.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
using namespace SMATICS_BBARAB;

CSMDataParser::CSMDataParser()
{

}

CSMDataParser::CSMDataParser(CSMDataParser &copy)
{
	m_FileContents = copy.m_FileContents;
	
	ICP_List = copy.ICP_List;
	GCP_List = copy.GCP_List;
	ICL_List = copy.ICL_List;
	GCL_List = copy.GCL_List;
	SCENE_List = copy.SCENE_List;

	for(int i=0; i<NUM_NORMALIZE_COEFF; i++) coeff[i] = copy.coeff[i];
};

CSMDataParser::~CSMDataParser() { Init(); }

void CSMDataParser::operator=(CSMDataParser &copy)
{
	m_FileContents = copy.m_FileContents;
	
	ICP_List = copy.ICP_List;
	GCP_List = copy.GCP_List;
	ICL_List = copy.ICL_List;
	GCL_List = copy.GCL_List;
	SCENE_List = copy.SCENE_List;

	for(int i=0; i<NUM_NORMALIZE_COEFF; i++) coeff[i] = copy.coeff[i];
}

void CSMDataParser::DataNormalization_ICP_ICL(CSMList<_ICP_> ICP_List, CSMList<_ICL_> ICL_List, double coeff[4])
{
	double col, row;
	double maxcol, mincol, maxrow, minrow;

	unsigned int i;

	_ICP_ icp;
	_ICL_ icl;
	
	if(ICP_List.GetNumItem() != 0)
	{
		icp = ICP_List.GetAt(0);
		col = icp.col;
		row = icp.row;
		
		maxcol = mincol = col;
		maxrow = minrow = row;
		
		for(i=1; i<ICP_List.GetNumItem(); i++)
		{
			icp = ICP_List.GetAt(i);
			col = icp.col;
			row = icp.row;
			
			if(maxcol < col)
				maxcol = col;
			if(mincol > col)
				mincol = col;
			
			if(maxrow < row)
				maxrow = row;
			if(minrow > row)
				minrow = row;
		}
	}
	else if(ICL_List.GetNumItem() != 0)
	{
		icl = ICL_List.GetAt(0);
		col = icl.col;
		row = icl.row;

		maxcol = mincol = col;
		maxrow = minrow = row;
	}
	
	if(ICL_List.GetNumItem() != 0)
	{
		for(i=0; i<ICL_List.GetNumItem(); i++)
		{
			icl = ICL_List.GetAt(i);
			col = icl.col;
			row = icl.row;
			
			if(maxcol < col)
				maxcol = col;
			if(mincol > col)
				mincol = col;
			
			if(maxrow < row)
				maxrow = row;
			if(minrow > row)
				minrow = row;
		}
	}

	//Normalized Coord = (Original Coord + SHIFT)*SCALE
	//Original Coord = (Normalized Coord/SCALE) - SHIFT

	//SCALE = 1/(max-min)
	//SHIFT = -(max-min)/2
	
	/**maxrow[0], minrow[1], maxcol[2], mincol*/
	if((ICP_List.GetNumItem() != 0)||(ICL_List.GetNumItem() != 0))
	{
		coeff[0] = maxrow;
		coeff[1] = minrow;
		coeff[2] = maxcol;
		coeff[3] = mincol;
	}

	//So many "if", stupid process, but no problem, not important process...
}

void CSMDataParser::DataNormalization_GCP_MatlabGCL(CSMList<_GCP_> GCP_List, CSMList<_GCL_> GCL_List, double coeff[6])
{
	/**<SHIFT_X[0], SCALE_X[1], SHIFT_Y[2], SCALE_Y[3], SHIFT_Z[4], SCALE_Z[5], SHIFT_col[6], SCALE_col[7], SHIFT_row[8], SCALE_row[9] */

	unsigned int i;
	
	double X, Y, Z;
	double maxX, minX, maxY, minY, maxZ, minZ;
	_GCP_ gcp;
	_GCL_ gcl;

	if(GCP_List.GetNumItem() != 0)
	{		
		gcp = GCP_List.GetAt(0);
		X = gcp.X;
		Y = gcp.Y;
		Z = gcp.Z;
		
		maxX = minX = X;
		maxY = minY = Y;
		maxZ = minZ = Z;
		
		for(i=1; i<GCP_List.GetNumItem(); i++)
		{
			gcp = GCP_List.GetAt(i);
			X = gcp.X;
			Y = gcp.Y;
			Z = gcp.Z;
			
			if(maxX < X)
				maxX = X;
			if(minX > X)
				minX = X;
			
			if(maxY < Y)
				maxY = Y;
			if(minY > Y)
				minY = Y;
			
			if(maxZ < Z)
				maxZ = Z;
			if(minZ > Z)
				minZ = Z;
		}
	}
	else if(GCL_List.GetNumItem() != 0)
	{
		gcl = GCL_List.GetAt(0);
		X = gcl.X;
		Y = gcl.Y;
		Z = gcl.Z;
		
		maxX = minX = X;
		maxY = minY = Y;
		maxZ = minZ = Z;
	}

	if(GCL_List.GetNumItem() != 0)
	{
		
		for(i=0; i<GCL_List.GetNumItem(); i++)
		{
			gcl = GCL_List.GetAt(i);
			X = gcl.X;
			Y = gcl.Y;
			Z = gcl.Z;
			
			if(maxX < X)
				maxX = X;
			if(minX > X)
				minX = X;
			
			if(maxY < Y)
				maxY = Y;
			if(minY > Y)
				minY = Y;
			
			if(maxZ < Z)
				maxZ = Z;
			if(minZ > Z)
				minZ = Z;	
		}
	}

	/**<max_X[0], min_X[1], max_Y[2], min_Y[3], max_Z[4], min_Z[5]*/
	if((GCP_List.GetNumItem() != 0)||(GCL_List.GetNumItem() != 0))
	{
		coeff[0] = maxX;
		coeff[1] = minX;
		coeff[2] = maxY;
		coeff[3] = minY;
		coeff[4] = maxZ;
		coeff[5] = minZ;
	}

	//So many "if", stupid process, but no problem, not important process...
}

void CSMDataParser::ParserforMatlabICPFile(const CString fname, bool bDataString)
{
	FILE* indata;
	//Data file open
	indata = fopen(fname,"r");

	//ID
	char image_id[256];
	char point_id[256];

	//Image coord x and y \(column and row)
	double row, col;

	//sigma
	double s0, s1, s2, s3;

	//do~while stop switch
	bool bStop = false;

	if(bDataString == true)
	{
		//Data description
		m_FileContents = "[ICF File]\r\n\r\n";
		m_FileContents += fname;
		m_FileContents += "\r\n\r\n";
		m_FileContents += "(Image ID\tPoint ID\tROW\tCOL\tsigma(2,2))\r\n";
	}

	//start to read the data
	do{
		//Check the EOF sign and read image ID
		if(EOF == fscanf(indata,"%s",image_id)){
			bStop = true;
			break;
		}

		//search existing scene data
		unsigned int num_scene = SCENE_List.GetNumItem();
		unsigned int position = -999;
		_SCENE_ scene;
		for(unsigned int i=0; i<num_scene; i++)
		{
			if(image_id == SCENE_List.GetAt(i).SID)
			{
				position = i;
				scene = SCENE_List[i];
				break;
			}
		}

		//Read point ID
		fscanf(indata,"%s",point_id);
		//Read the y coord
		fscanf(indata,"%lf",&row);
		//Read the x coord
		fscanf(indata,"%lf",&col);
		//Read sigma
		fscanf(indata,"%lf",&s0);
		fscanf(indata,"%lf",&s1);
		fscanf(indata,"%lf",&s2);
		fscanf(indata,"%lf",&s3);

		//Insert ICP data into ICP List
		_ICP_ icp;
		icp.SID = image_id;
		icp.PID = point_id;
		icp.col = col;
		icp.row = row;
		icp.s[0] = sqrt(s0);
		icp.s[1] = sqrt(s1);
		icp.s[2] = sqrt(s2);
		icp.s[3] = sqrt(s3);

		scene.SID = image_id;
		scene.ICP_List.AddTail(icp);

		if(position != -999)
			SCENE_List.SetAt(position,scene);
		else
			SCENE_List.AddTail(scene);

		if(bDataString == true)
		{
			//Record the data to m_FileContents
			CString temp;
			temp.Format("%s\t%s\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\r\n",icp.SID,icp.PID,icp.row,icp.col,icp.s[0],icp.s[1],icp.s[2],icp.s[3]);
			m_FileContents += temp;
		}
	}while(bStop == false);
	
	//Data file close
	fclose(indata);
}

void CSMDataParser::ParserforMatlabICLFile(const CString fname, bool bDataString)
{
	FILE* indata;
	//Data file open
	indata = fopen(fname,"r");

	//ID
	char image_id[10];
	char line_id[10];
	char flag[10]; //A&B: ending point C: intermediate point

	//Image coord x and y \(column and row)
	double row, col;

	//Weight
	//double s0, s1, s2, s3;

	//do~while stop switch
	bool bStop = false;

	if(bDataString == true)
	{
		//Data description
		m_FileContents = "[ICL File]\r\n\r\n";
		m_FileContents += fname;
		m_FileContents += "\r\n\r\n";
		m_FileContents += "(Image ID\tPoint ID\tflag(A,B,C)\tROW\tCOL)\r\n";
	}

	//start to read the data
	do{
		//Check the EOF sign and read image ID
		if(EOF == fscanf(indata,"%s",image_id)){
			bStop = true;
			break;
		}

		//search existing scene data
		unsigned int num_scene = SCENE_List.GetNumItem();
		unsigned int position = -999;
		_SCENE_ scene;
		for(unsigned int i=0; i<num_scene; i++)
		{
			if(image_id == SCENE_List.GetAt(i).SID)
			{
				position = i;
				scene = SCENE_List[i];
				break;
			}
		}



		//Read point ID
		fscanf(indata,"%s",line_id);
		//Read point flag
		fscanf(indata,"%s",&flag);
		//Read the y coord
		fscanf(indata,"%lf",&row);
		//Read the x coord
		fscanf(indata,"%lf",&col);
		
		//Read sigma
		//fscanf(indata,"%lf",&s0);
		//fscanf(indata,"%lf",&s1);
		//fscanf(indata,"%lf",&s2);
		//fscanf(indata,"%lf",&s3);
		

		//Insert ICL data into ICL List
		_ICL_ icl;
		icl.SID = image_id;
		icl.LID = line_id;
		icl.col = col;
		icl.row = row;
		icl.flag = flag;
		icl.s[0] = 1.;
		icl.s[1] = 0;
		icl.s[2] = 0;
		icl.s[3] = 1.;

		scene.SID = image_id;
		scene.ICL_List.AddTail(icl);

		if(position != -999)
			SCENE_List.SetAt(position,scene);
		else
			SCENE_List.AddTail(scene);

		if(bDataString == true)
		{
			//Record the data to m_FileContents
			CString temp;
			temp.Format("%s\t%s\t%s\t%lf\t%lf\r\n",icl.SID,icl.LID,icl.flag,icl.row,icl.col);
			m_FileContents += temp;
		}
	}while(bStop == false);
	
	//Data file close
	fclose(indata);
}

void CSMDataParser::ParserforMatlabGCPFile(const CString fname, bool bDataString)
{
	FILE* indata;
	//Data file open
	indata = fopen(fname,"r");

	//do~while stop switch
	bool bStop = false;

	if(bDataString == true)
	{
		//Data description
		m_FileContents = "[GCP File]\r\n\r\n";
		m_FileContents += fname;
		m_FileContents += "\r\n\r\n";
		m_FileContents += "(ID\tLon(X)\tLat(Y)\tHig(Z))\tsigma(3,3)\r\n";
	}

	//Empty GCP List
	GCP_List.RemoveAll();

	//start to read the data
	_GCP_ gcp;
	char gcp_id[10];
	do{
		//Check the EOF sign and read point ID
		if(EOF == fscanf(indata,"%s",gcp_id)){
			bStop = true;
			break;
		}
		gcp.PID = gcp_id;
		//Read the X coord
		fscanf(indata,"%lf",&gcp.X);
		//Read the Y coord
		fscanf(indata,"%lf",&gcp.Y);
		//Read the Z coord
		fscanf(indata,"%lf",&gcp.Z);
		//Read the sigma^2
		fscanf(indata,"%lf",gcp.s);
		fscanf(indata,"%lf",gcp.s+1);
		fscanf(indata,"%lf",gcp.s+2);

		fscanf(indata,"%lf",gcp.s+3);
		fscanf(indata,"%lf",gcp.s+4);
		fscanf(indata,"%lf",gcp.s+5);

		fscanf(indata,"%lf",gcp.s+6);
		fscanf(indata,"%lf",gcp.s+7);
		fscanf(indata,"%lf",gcp.s+8);

		for(unsigned index=0; index<9; index++)
			gcp.s[index] = sqrt(gcp.s[index]);

		//Insert GCP data into GCP List
		
		GCP_List.AddTail(gcp);
		
		if(bDataString == true)
		{
			//Record the data to m_FileContents
			CString temp;
			temp.Format("%s\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\r\n"
				,gcp.PID,gcp.X,gcp.Y,gcp.Z,gcp.s[0],gcp.s[1],gcp.s[2],gcp.s[3],gcp.s[4],gcp.s[5],gcp.s[6],gcp.s[7],gcp.s[8]);
			m_FileContents += temp;
		}
	}while(bStop == false);
	
	//Data file close
	fclose(indata);
}

void CSMDataParser::ParserforMatlabGCLFile(const CString fname, bool bDataString)
{
	FILE* indata;
	//Data file open
	indata = fopen(fname,"r");

	//do~while stop switch
	bool bStop = false;

	if(bDataString == true)
	{
		//Data description
		m_FileContents = "[Matlab TGCL File]\r\n\r\n";
		m_FileContents += fname;
		m_FileContents += "\r\n\r\n";
		m_FileContents += "(Point ID\tflag(A,B)\tLon(Xs)\tLat(Ys)\tHig(Zs)\tsigma(3,3))\r\n";
	}

	//Empty GCP List
	GCL_List.RemoveAll();

	char id[10];
	char flag[10];
	_GCL_ msatgcl;

	//start to read the data
	do{
		//Check the EOF sign and read point ID
		if(EOF == fscanf(indata,"%s",id))
		{
			bStop = true;
			break;
		}
		//데이터 클래스에 직접 접근하여 파일에서 데이터를 입력하면 모든 String 데이터에 동일한 값이 세팅됨.
		//이유를 모르겠음. 
		//아마도 String 객체의 메모리 주소를 접근하는 방법에 문제가 있는것 같음.
		msatgcl.LID = id;
		
		//fscanf(indata,"%s",msatgcl.flag);
		fscanf(indata,"%s",flag);
		msatgcl.flag = flag;
		
		//Read the X1 coord
		fscanf(indata,"%lf",&msatgcl.X);
		//Read the Y1 coord
		fscanf(indata,"%lf",&msatgcl.Y);
		//Read the Z1 coord
		fscanf(indata,"%lf",&msatgcl.Z);
		//Read the sigma^2
		fscanf(indata,"%lf",msatgcl.s);
		fscanf(indata,"%lf",msatgcl.s+1);
		fscanf(indata,"%lf",msatgcl.s+2);
		
		fscanf(indata,"%lf",msatgcl.s+3);
		fscanf(indata,"%lf",msatgcl.s+4);
		fscanf(indata,"%lf",msatgcl.s+5);
			
		fscanf(indata,"%lf",msatgcl.s+6);
		fscanf(indata,"%lf",msatgcl.s+7);
		fscanf(indata,"%lf",msatgcl.s+8);

		for(unsigned index=0; index<9; index++)
			msatgcl.s[index] = sqrt(msatgcl.s[index]);
		
		if(bDataString == true)
		{
			//Record the data to m_FileContents
			CString temp;
			temp.Format("%s\t%s\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\r\n"
				,msatgcl.LID,msatgcl.flag,msatgcl.X,msatgcl.Y,msatgcl.Z,msatgcl.s[0],msatgcl.s[1],msatgcl.s[2],msatgcl.s[3],msatgcl.s[4],msatgcl.s[5],msatgcl.s[6],msatgcl.s[7],msatgcl.s[8]);
			m_FileContents += temp;
		}
				
		GCL_List.AddTail(msatgcl);
		
	}while(bStop == false);
	
	//Data file close
	fclose(indata);
}
