//////////////////////////
//SpacematicsModel.cpp
//made by BbaraB
//revision date 2000-6-15
//revision date 2001-3-16
//revision date 2001-5-1 Model class 복사생성자, 대입연산자 추가
//////////////////////////
#include "stdafx.h"
#include "SpacematicsModel.h"
//#include "SpaceIntersection.h"
#include "SMFrameIntersection.h"


/////////
//Model
/////////
//Relative Orientation
CModel::CModel()
{
	maxIteration = 10;			//Iteration Limit
	maxCorrection = 0.000001;	//Maximum Correction Limit
	maxSD = 0.000001;
	diffVar = 0.0;
	GaApproximation_Option = PARALLAXEQ;	//model space 3D-coord approximation option
	ParallaxEQ_Option = TWOPHOTOMEAN;		//parallax Equation option
	ROComplete = FALSE;
	AOComplete_General = FALSE;
	ROMethod_inde = ROMethod_de = FALSE;
	ID = -1;
	Name = "";
}

CModel::CModel(CModel &copy)
{
	AOComplete_General = copy.AOComplete_General;
	AOMPoint = copy.AOMPoint;
	b = copy.b;
	diffVar = copy.diffVar;
	GaApproximation_Option = copy.GaApproximation_Option;
	GCP = copy.GCP;
	ID = copy.ID;
	Name = copy.Name;
	LPhoto = copy.LPhoto;
	mat = copy.mat;
	maxCorrection = copy.maxCorrection;
	maxdiffVar = copy.maxdiffVar;
	maxIteration = copy.maxIteration;
	maxSD = copy.maxSD;
	num_GCP = copy.num_GCP;
	num_ROPoint = copy.num_ROPoint;
	ParallaxEQ_Option = copy.ParallaxEQ_Option;
	Param_AO = copy.Param_AO;
	Param_de = copy.Param_de;
	Param_inde = copy.Param_inde;
	result = copy.result;
	ROComplete = copy.ROComplete;
	ROMethod_de = copy.ROMethod_de;
	ROMethod_inde = copy.ROMethod_inde;
	ROMPoint = copy.ROMPoint;
	RPhoto = copy.RPhoto;
}

void CModel::operator = (CModel &copy)
{
	AOComplete_General = copy.AOComplete_General;
	AOMPoint = copy.AOMPoint;
	b = copy.b;
	diffVar = copy.diffVar;
	GaApproximation_Option = copy.GaApproximation_Option;
	GCP = copy.GCP;
	ID = copy.ID;
	Name = copy.Name;
	LPhoto = copy.LPhoto;
	mat = copy.mat;
	maxCorrection = copy.maxCorrection;
	maxdiffVar = copy.maxdiffVar;
	maxIteration = copy.maxIteration;
	maxSD = copy.maxSD;
	num_GCP = copy.num_GCP;
	num_ROPoint = copy.num_ROPoint;
	ParallaxEQ_Option = copy.ParallaxEQ_Option;
	Param_AO = copy.Param_AO;
	Param_de = copy.Param_de;
	Param_inde = copy.Param_inde;
	result = copy.result;
	ROComplete = copy.ROComplete;
	ROMethod_de = copy.ROMethod_de;
	ROMethod_inde = copy.ROMethod_inde;
	ROMPoint = copy.ROMPoint;
	RPhoto = copy.RPhoto;
}

CModel::CModel(char* infile)
{
	ROComplete = FALSE;
	AOComplete_General = FALSE;
	ROMethod_inde = ROMethod_de = FALSE;

	char st[100];
	FILE* indata;
	int i;
	//file open
	indata = fopen(infile,"r");
	//trash memory
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Left Photo ID
	{
		int ID;
		fscanf(indata,"%d",&ID);
		fgets(st,100,indata);
		
		LPhoto.SetPhotoID(ID);
	}
	
	fgets(st,100,indata);
	//Left Photo PPA, PPBS
	{
		CCamera camera;
		fscanf(indata,"%lf",&camera.PPA.x);
		fscanf(indata,"%lf",&camera.PPA.y);
		fscanf(indata,"%lf",&camera.PPBS.x);
		fscanf(indata,"%lf",&camera.PPBS.y);
		fgets(st,100,indata);
		
		fgets(st,100,indata);
		//Left Photo Focal Length
		fscanf(indata,"%lf",&camera.f);
		fgets(st,100,indata);

		LPhoto.SetCamera(camera);
	}
	
	{
		double FlightHeight;
		fgets(st,100,indata);
		//Left Photo Flight Height
		fscanf(indata,"%lf",&FlightHeight);
		fgets(st,100,indata);
		
		LPhoto.SetFlightHeight(FlightHeight);
	}
		
	{
		int num_ROP;
		fgets(st,100,indata);
		//Left Photo number of RO Point
		fscanf(indata,"%d",&num_ROP);
		fgets(st,100,indata);

		LPhoto.SetNumROP(num_ROP);
	
		fgets(st,100,indata);
		//Left Photo RO Point Coordinate
		for(i=0;i<num_ROP;i++)
			{
				double d;
				fscanf(indata,"%lf",&d);
				LPhoto.ROPoint(i,0) = d;
				fscanf(indata,"%lf",&d);
				LPhoto.ROPoint(i,1) = d;
				fscanf(indata,"%lf",&d);
				LPhoto.ROPoint(i,2) = d;
			}
	}
	fgets(st,100,indata);
	
	{
		int ID;
		fgets(st,100,indata);
		//Right Photo ID
		fscanf(indata,"%d",&ID);
		fgets(st,100,indata);

		RPhoto.SetPhotoID(ID);
	}

	{
		CCamera camera;
		fgets(st,100,indata);
		//Right Photo PPA, PPBS
		fscanf(indata,"%lf",&camera.PPA.x);
		fscanf(indata,"%lf",&camera.PPA.y);
		fscanf(indata,"%lf",&camera.PPBS.x);
		fscanf(indata,"%lf",&camera.PPBS.y);
		fgets(st,100,indata);
	
		fgets(st,100,indata);
		//Right Photo Focal Length
		fscanf(indata,"%lf",&camera.f);
		fgets(st,100,indata);

		RPhoto.SetCamera(camera);
	}
	
	{
		double FlightHeight;
		fgets(st,100,indata);
		//Right Photo Flight Height
		fscanf(indata,"%lf",&FlightHeight);
		fgets(st,100,indata);

		RPhoto.SetFlightHeight(FlightHeight);
	}
	
	{
		int num_ROP;
		fgets(st,100,indata);
		//Right Photo number of RO Point
		fscanf(indata,"%d",&num_ROP);
		fgets(st,100,indata);

		RPhoto.SetNumROP(num_ROP);

		fgets(st,100,indata);
		//Rigth Photo RO Point Coordinate
		for(i=0;i<num_ROP;i++)
			{
				double d;
				fscanf(indata,"%lf",&d);
				RPhoto.ROPoint(i,0) = d;
				fscanf(indata,"%lf",&d);
				RPhoto.ROPoint(i,1) = d;
				fscanf(indata,"%lf",&d);
				RPhoto.ROPoint(i,2) = d;
			}
	}						
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	fgets(st,100,indata);
	//Maximum Iteration
	fscanf(indata,"%d",&maxIteration);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Correction Limit
	fscanf(indata,"%lf",&maxCorrection);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Standard Deviation Limit
	fscanf(indata,"%lf",&maxSD);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Difference Variance(Old_Var - New_Var)
	fscanf(indata,"%lf",&maxdiffVar);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	fgets(st,100,indata);
	fscanf(indata,"%d",&GaApproximation_Option);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	fgets(st,100,indata);
	fscanf(indata,"%d",&ParallaxEQ_Option);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//Num of GCP
	fscanf(indata,"%d",&num_GCP);
	fgets(st,100,indata);
	
	//GCP
	GCP.Resize(num_GCP,8);
	for(i=0;i<num_GCP;i++)
	{
		//id, X, Y, Z, Lx, Ly, Rx, Ry
		double d;
		fscanf(indata,"%lf",&d);
		GCP(i,0) = d;
		fscanf(indata,"%lf",&d);
		GCP(i,1) = d;
		fscanf(indata,"%lf",&d);
		GCP(i,2) = d;
		fscanf(indata,"%lf",&d);
		GCP(i,3) = d;
		fscanf(indata,"%lf",&d);
		GCP(i,4) = d;
		fscanf(indata,"%lf",&d);
		GCP(i,5) = d;
		fscanf(indata,"%lf",&d);
		GCP(i,6) = d;
		fscanf(indata,"%lf",&d);
		GCP(i,7) = d;
	}
	fgets(st,100,indata);
	
	fclose(indata);
	
}

CModel::CModel(int modelID, CSMStr modelName, CPhoto L, CPhoto R, int maxi, double correction, double sd, double diffvar)
{
	ID = modelID;				//Model ID
	Name = modelName;			//Model Name
	LPhoto = L;					//Left Photo
	RPhoto = R;					//Right Photo
	maxIteration = maxi;		//Iteration Limit(default=10)
	maxCorrection = correction;	//Maximum Correction Limit(default=0.000001)
	maxSD = sd;					//Maximum Standard Devation Limit(default=0.000001)
	maxdiffVar = diffvar;
	GaApproximation_Option = PARALLAXEQ;	//model space 3D-coord approximation option
	ParallaxEQ_Option = TWOPHOTOMEAN;		//parallax Equation option
	ROComplete = FALSE;
	AOComplete_General = FALSE;
	ROMethod_inde = ROMethod_de = FALSE;
}
CModel::~CModel()
{
	
}

//Independent Relative Orientation
void CModel::InitApp_inde(void)
{
	int i;
	double H,f;
	double sum = 0.0;
	for(i=0;i<num_ROPoint;i++)
	{
		sum += LPhoto.ROPoint(i,1) - RPhoto.ROPoint(i,1);
	}
	
	//Initialize RO Parameters
	Param_inde.Lomega = Param_inde.Lphi = Param_inde.Lkappa = 0.0;
	Param_inde.Xl = Param_inde.Yl = 0.0;
	Param_inde.Zl = LPhoto.GetCamera().f;
	Param_inde.Romega = Param_inde.Rphi = Param_inde.Rkappa = 0.0;
	Param_inde.Xr = sum/num_ROPoint;
	Param_inde.Yr = 0.0;
	Param_inde.Zr = RPhoto.GetCamera().f;
	
	//Initialize Model Space Point
	ROMPoint.Resize(num_ROPoint,4);
	H = f = (LPhoto.GetCamera().f + RPhoto.GetCamera().f)/2.0;
	
	switch(GaApproximation_Option)
	{
	case LPHOTOCOORDINATE:
		for(i=0;i<num_ROPoint;i++)
		{
			//Left photo coordinate
			ROMPoint(i,0) = LPhoto.ROPoint(i,0);
			ROMPoint(i,1) = LPhoto.ROPoint(i,1);
			ROMPoint(i,2) = LPhoto.ROPoint(i,2);
			ROMPoint(i,3) = 0.0;
		}
		break;
	case RPHOTOCOORDINATE:
		for(i=0;i<num_ROPoint;i++)
		{
			//Right photo coordinate
			ROMPoint(i,0) = RPhoto.ROPoint(i,0);
			ROMPoint(i,1) = RPhoto.ROPoint(i,1) + Param_inde.Xr;
			ROMPoint(i,2) = RPhoto.ROPoint(i,2);
			ROMPoint(i,3) = 0.0;
		}
		break;
	case TWOPHOTOMEAN:
		for(i=0;i<num_ROPoint;i++)
		{
			//Two Photo mean
			ROMPoint(i,0) = LPhoto.ROPoint(i,0);
			ROMPoint(i,1) = (LPhoto.ROPoint(i,1) + RPhoto.ROPoint(i,1) + Param_inde.Xr)/2;
			ROMPoint(i,2) = (LPhoto.ROPoint(i,2) + RPhoto.ROPoint(i,2))/2;
			ROMPoint(i,3) = 0.0;
		}
		break;
	default:
		for(i=0;i<num_ROPoint;i++)
		{
			//parallax EQ
			Point3D<double> ga;
			Point2D<double> LP;
			Point2D<double> RP;
			LP.x = LPhoto.ROPoint(i,1);	LP.y = LPhoto.ROPoint(i,2);
			RP.x = RPhoto.ROPoint(i,1);	RP.y = RPhoto.ROPoint(i,2);
			ga = ParallaxEQ(ParallaxEQ_Option,H,Param_inde.Xr,f,LP,RP);
			ROMPoint(i,0) = LPhoto.ROPoint(i,0);
			ROMPoint(i,1) = ga.x;
			ROMPoint(i,2) = ga.y;
			ROMPoint(i,3) = ga.z;
		}
		break;
	}//End Switch
}



//Dependent Relative Orientation
void CModel::InitApp_de(void)
{

	int i;
	double H,f;
	double sum = 0.0;
	for(i=0;i<num_ROPoint;i++)
	{
		sum += LPhoto.ROPoint(i,1) - RPhoto.ROPoint(i,1);
	}
	
	//Initialize RO Parameters
	Param_de.Lomega = Param_de.Lphi = Param_de.Lkappa = 0.0;
	Param_de.Xl = Param_de.Yl = 0.0;
	Param_de.Zl = LPhoto.GetCamera().f;
	Param_de.Romega = Param_de.Rphi = Param_de.Rkappa = 0.0;
	Param_de.Xr = sum/num_ROPoint;
	Param_de.Yr = 0.0;
	Param_de.Zr = RPhoto.GetCamera().f;

	//Initialize Model Space Point
	ROMPoint.Resize(num_ROPoint,4);
	H = f = (LPhoto.GetCamera().f + RPhoto.GetCamera().f)/2.0;
	
	switch(GaApproximation_Option)
	{
	case LPHOTOCOORDINATE:
		for(i=0;i<num_ROPoint;i++)
		{
			//Left photo coordinate
			ROMPoint(i,0) = LPhoto.ROPoint(i,0);
			ROMPoint(i,1) = LPhoto.ROPoint(i,1);
			ROMPoint(i,2) = LPhoto.ROPoint(i,2);
			ROMPoint(i,3) = 0.0;
		}
		break;
	case RPHOTOCOORDINATE:
		for(i=0;i<num_ROPoint;i++)
		{
			//Right photo coordinate
			ROMPoint(i,0) = RPhoto.ROPoint(i,0);
			ROMPoint(i,1) = RPhoto.ROPoint(i,1) + Param_de.Xr;
			ROMPoint(i,2) = RPhoto.ROPoint(i,2);
			ROMPoint(i,3) = 0.0;
		}
		break;
	case TWOPHOTOMEAN:
		for(i=0;i<num_ROPoint;i++)
		{
			//Two Photo mean
			ROMPoint(i,0) = LPhoto.ROPoint(i,0);
			ROMPoint(i,1) = (LPhoto.ROPoint(i,1) + RPhoto.ROPoint(i,1) + Param_de.Xr)/2;
			ROMPoint(i,2) = (LPhoto.ROPoint(i,2) + RPhoto.ROPoint(i,2))/2;
			ROMPoint(i,3) = 0.0;
		}
		break;
	default:
		for(i=0;i<num_ROPoint;i++)
		{
			//parallax EQ
			Point3D<double> ga;
			Point2D<double> LP;
			Point2D<double> RP;
			LP.x = LPhoto.ROPoint(i,1);	LP.y = LPhoto.ROPoint(i,2);
			RP.x = RPhoto.ROPoint(i,1);	RP.y = RPhoto.ROPoint(i,2);
			ga = ParallaxEQ(ParallaxEQ_Option,H,Param_de.Xr,f,LP,RP);
			ROMPoint(i,0) = LPhoto.ROPoint(i,0);
			ROMPoint(i,1) = ga.x;
			ROMPoint(i,2) = ga.y;
			ROMPoint(i,3) = ga.z;
		}
		break;
	}//End Switch 
	
}

//Dependent Relative Orientation
void CModel::InitApp_de(double O, double P, double K, double Lx, double Ly, double Lz)
{

	int i;
	double sum = 0.0;
	for(i=0;i<num_ROPoint;i++)
	{
		sum += LPhoto.ROPoint(i,1) - RPhoto.ROPoint(i,1);
	}

	//Initialize RO Parameters
	Param_de.Lomega = O;
	Param_de.Lphi = P;
	Param_de.Lkappa = K;
	Param_de.Xl = Lx;
	Param_de.Yl = Ly;
	Param_de.Zl = Lz;

	Param_de.Romega = O;
	Param_de.Rphi = P;
	Param_de.Rkappa = K;
	Param_de.Xr = Lx + sum/num_ROPoint;
	Param_de.Yr = Ly;
	Param_de.Zr = Lz;

	//Initialize Model Space Point
	ROMPoint.Resize(num_ROPoint,4);

	Point3D<double> LPC, RPC;
	Point2D<double> La, Ra;
	double LO, LP, LK;
	double RO, RP, RK;
	double Lf, Rf;

	LPC.x = Lx;
	LPC.y = Ly;
	LPC.z = Lz;
	LO = O;
	LP = P;
	LK = K;

	RPC.x = Lx + sum/num_ROPoint;
	RPC.y = Ly;
	RPC.z = Lz;
	RO = O;
	RP = P;
	RK = K;

	Lf = LPhoto.GetCamera().f;
	Rf = RPhoto.GetCamera().f;

	CFrameIntersection IS;
	IS.SetEOParameter(LO,LP,LK,LPC,
		              RO,RP,RK,RPC,
					  Lf,Rf);
	IS.SetPPA(LPhoto.GetCamera().PPA.x,LPhoto.GetCamera().PPA.y,
			  RPhoto.GetCamera().PPA.x,RPhoto.GetCamera().PPA.y);

	for(i=0;i<num_ROPoint;i++)
	{
		La.x = LPhoto.ROPoint(i,1);
		La.y = LPhoto.ROPoint(i,2);
		Ra.x = RPhoto.ROPoint(i,1);
		Ra.y = RPhoto.ROPoint(i,2);

		Point3D<double> GP = IS.RunIntersection(La.x,La.y,Ra.x,Ra.y);

		ROMPoint(i,0) = LPhoto.ROPoint(i,0);
		ROMPoint(i,1) = GP.x;
		ROMPoint(i,2) = GP.y;
		ROMPoint(i,3) = GP.z;
	}


/*

	for(i=0;i<num_ROPoint;i++)
	{
		Point2D<double> La;
		Point2D<double> Ra;
		La.x = LPhoto.ROPoint(i,1);
		La.y = LPhoto.ROPoint(i,2);
		Ra.x = RPhoto.ROPoint(i,1);
		Ra.y = RPhoto.ROPoint(i,2);
		
		Point2D<double> LPPA = LPhoto.GetCamera().PPA;
		Point2D<double> RPPA = RPhoto.GetCamera().PPA;
		Point3D<double> LPC, RPC;
		double focal_length_left = LPhoto.GetCamera().f;
		double focal_length_right = RPhoto.GetCamera().f;
		
		LPC.x = Param_de.Xl;
		LPC.y = Param_de.Yl;
		LPC.z = Param_de.Zl;
		RPC.x = Param_de.Xr;
		RPC.y = Param_de.Yr;
		RPC.z = Param_de.Zr;
		
		Matrix<double> A(4,3,0.0);
		Matrix<double> L(4,1,0.0);
		Matrix<double> V, X;
		
		double LNUM_x[4], LNUM_y[4], LDEN[3];
		double RNUM_x[4], RNUM_y[4], RDEN[3];
		
		CRotationcoeff LeftR(Param_de.Lomega,Param_de.Lphi,Param_de.Lkappa);
		Matrix<double> LR = LeftR.Rmatrix;
		CRotationcoeff RightR(Param_de.Romega,Param_de.Rphi,Param_de.Rkappa);
		Matrix<double> RR = RightR.Rmatrix;
		
		double Ltemp, Rtemp;
		
		Ltemp = 1/(-(LR(2,0)*LPC.x + LR(2,1)*LPC.y + LR(2,2)*LPC.z));
		Rtemp = 1/(-(RR(2,0)*RPC.x + RR(2,1)*RPC.y + RR(2,2)*RPC.z));
		
		LNUM_x[0] = Ltemp*(LR(2,0)*LPPA.x-LR(0,0)*focal_length_left);
		LNUM_x[1] = Ltemp*(LR(2,1)*LPPA.x-LR(0,1)*focal_length_left);
		LNUM_x[2] = Ltemp*(LR(2,2)*LPPA.x-LR(0,2)*focal_length_left);
		LNUM_x[3] = LPPA.x + Ltemp*focal_length_left*(LR(0,0)*LPC.x+LR(0,1)*LPC.y+LR(0,2)*LPC.z);
		
		LNUM_y[0] = Ltemp*(LR(2,0)*LPPA.y-LR(1,0)*focal_length_left);
		LNUM_y[1] = Ltemp*(LR(2,1)*LPPA.y-LR(1,1)*focal_length_left);
		LNUM_y[2] = Ltemp*(LR(2,2)*LPPA.y-LR(1,2)*focal_length_left);
		LNUM_y[3] = LPPA.y + Ltemp*focal_length_left*(LR(1,0)*LPC.x+LR(1,1)*LPC.y+LR(1,2)*LPC.z);
		
		LDEN[0] = Ltemp*LR(2,0);
		LDEN[1] = Ltemp*LR(2,1);
		LDEN[2] = Ltemp*LR(2,2);
		
		RNUM_x[0] = Rtemp*(RR(2,0)*RPPA.x-RR(0,0)*focal_length_right);
		RNUM_x[1] = Rtemp*(RR(2,1)*RPPA.x-RR(0,1)*focal_length_right);
		RNUM_x[2] = Rtemp*(RR(2,2)*RPPA.x-RR(0,2)*focal_length_right);
		RNUM_x[3] = RPPA.x + Rtemp*focal_length_right*(RR(0,0)*RPC.x+RR(0,1)*RPC.y+RR(0,2)*RPC.z);
		
		RNUM_y[0] = Rtemp*(RR(2,0)*RPPA.y-RR(1,0)*focal_length_right);
		RNUM_y[1] = Rtemp*(RR(2,1)*RPPA.y-RR(1,1)*focal_length_right);
		RNUM_y[2] = Rtemp*(RR(2,2)*RPPA.y-RR(1,2)*focal_length_right);
		RNUM_y[3] = RPPA.y + Rtemp*focal_length_right*(RR(1,0)*RPC.x+RR(1,1)*RPC.y+RR(1,2)*RPC.z);
		
		RDEN[0] = Rtemp*RR(2,0);
		RDEN[1] = Rtemp*RR(2,1);
		RDEN[2] = Rtemp*RR(2,2);
		
		A(0,0) = LNUM_x[0] - La.x*LDEN[0];
		A(0,1) = LNUM_x[1] - La.x*LDEN[1];
		A(0,2) = LNUM_x[2] - La.x*LDEN[2];
		
		L(0,0) = La.x - LNUM_x[3];
		
		A(1,0) = LNUM_y[0] - La.y*LDEN[0];
		A(1,1) = LNUM_y[1] - La.y*LDEN[1];
		A(1,2) = LNUM_y[2] - La.y*LDEN[2];
		
		L(1,0) = La.y - LNUM_y[3];
		
		A(2,0) = RNUM_x[0] - Ra.x*RDEN[0];
		A(2,1) = RNUM_x[1] - Ra.x*RDEN[1];
		A(2,2) = RNUM_x[2] - Ra.x*RDEN[2];
		
		L(2,0) = Ra.x - RNUM_x[3];
		
		A(3,0) = RNUM_y[0] - Ra.y*RDEN[0];
		A(3,1) = RNUM_y[1] - Ra.y*RDEN[1];
		A(3,2) = RNUM_y[2] - Ra.y*RDEN[2];
		
		L(3,0) = Ra.y - RNUM_y[3];
		
		X = (A.Transpose()%A).Inverse()%A.Transpose()%L;
		
		ROMPoint(i,0) = LPhoto.ROPoint(i,0);
		ROMPoint(i,1) = X(0,0);
		ROMPoint(i,2) = X(1,0);
		ROMPoint(i,3) = X(2,0);
		
	}
	*/

}

//Dependent Relative Orientation
BOOL CModel::RO_dependent(double LO, double LP, double LK, double LX, double LY, double LZ)
{
	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	BOOL increse_old = FALSE,increse_new = FALSE;	//monitoring variance
	BOOL iteration_end = FALSE;						//Iteration Stop

	//좌우 사진의 상호표정점 갯수 검사
	if(LPhoto.GetNumROP() == RPhoto.GetNumROP())
	{
		num_ROPoint = LPhoto.GetNumROP();
		mat.DF = num_ROPoint - 5;
	}
	else
	{
		return FALSE;
	}

	//Initialize parameter
	InitApp_de(LO, LP, LK, LX, LY, LZ);
	result = "[Relative Orientation(Dependent)]\r\n\r\n\r\n";
	//Start Iteration
	do
	{
		CSMStr temp;
		iteration++;					//Increse Iteration Number(From 1)
		increse_old = increse_new;
		
		//make matrix A & L
		make_A_L_RO_dependent();
		
		//compute X matrix
		mat.N = mat.A.Transpose()%mat.A;
		mat.Ninv = mat.N.Inverse();
		mat.X = mat.Ninv%mat.A.Transpose()%mat.L;
		//compute V matrix
		mat.V = (mat.A%mat.X) - mat.L;
		//compute VTV
		mat.VTV = mat.V.Transpose()%mat.V;
		//compute posteriori variance
		newvar = mat.pos_var = mat.VTV(0,0)/mat.DF;
		//compute standard deviation
		mat.SD = sqrt(mat.pos_var);
		//compute variance-covariance matrix
		mat.Var_Co = mat.Ninv*mat.pos_var;
		//compute variance matirx
		mat.Var.Resize(5+num_ROPoint*3,1);
		for(i=0;i<(5+num_ROPoint*3);i++)
		{
			mat.Var(i,0) = mat.Var_Co(i,i);
		}
		
		//초기값 갱신
		Param_de.Romega += mat.X(0);
		Param_de.Rphi   += mat.X(1);
		Param_de.Rkappa += mat.X(2);
		Param_de.Yr     += mat.X(3);
		Param_de.Zr     += mat.X(4);
		for(i=0;i<num_ROPoint;i++)
		{
			ROMPoint(i,1) += mat.X(5+i*3);
			ROMPoint(i,2) += mat.X(5+i*3+1);
			ROMPoint(i,3) += mat.X(5+i*3+2);
		}
		
		//maximum correction(X matrix)
		mat.maxX = fabs(mat.X(0));
		for(i=1;i<(int)mat.X.GetRows();i++)
		{
			if(fabs(mat.X(i))>mat.maxX)
				mat.maxX = fabs(mat.X(i));
		}		
		
		//Print Result
		temp.Format("Iteration--->%d\r\n\r\n\r\n",iteration);
		result +=temp;
		result += PrintResult_de();
		
		//reference variance(posteriori variance)monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			increse_new = TRUE;
		}
		else
		{
			increse_new = FALSE;
		}
		
		//iteration end condition
		//variance difference monitoring 
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = TRUE;
			result += "[Difference Between Old_Variance And New_Variance < Difference Variance limit !!!]\r\n";
		}
		else
		{
			oldvar = newvar;
		}
		//variance monitoring
		if(((increse_old == TRUE)&&(increse_new == TRUE)))
		{
			iteration_end = TRUE;
			result += "[Variance Continuously(2회연속) Increse!!!]\r\n";
		}
		//SD monitoring
		if(fabs(mat.SD)<fabs(maxSD))
		{
			iteration_end = TRUE;
			result += "[Standard Deviation < SD limit !!!]\r\n";
		}
		//max correction
		if(mat.maxX<fabs(maxCorrection))
		{
			iteration_end = TRUE;
			result += "[Maximum Correction < correction limit !!!]\r\n";
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = TRUE;
			result += "[Iteration Max_Num Execss!!!]\r\n";
		}
		
		
	}while(iteration_end == FALSE);
	
	

	ROComplete = TRUE;
	ROMethod_de = TRUE;

	return TRUE;
}

//Dependent Relative Orientation
BOOL CModel::RO_dependent(void)
{
	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	BOOL increse_old = FALSE,increse_new = FALSE;	//monitoring variance
	BOOL iteration_end = FALSE;						//Iteration Stop

	//좌우 사진의 상호표정점 갯수 검사
	if(LPhoto.GetNumROP() == RPhoto.GetNumROP())
	{
		num_ROPoint = LPhoto.GetNumROP();
		mat.DF = num_ROPoint - 5;
	}
	else
	{
		return FALSE;
	}

	//Initialize parameter
	InitApp_de();
	result = "[Relative Orientation(Dependent)]\r\n\r\n\r\n";
	//Start Iteration
	do
	{
		CSMStr temp;
		iteration++;					//Increse Iteration Number(From 1)
		increse_old = increse_new;
		
		//make matrix A & L
		make_A_L_RO_dependent();
		
		//compute X matrix
		mat.N = mat.A.Transpose()%mat.A;
		mat.Ninv = mat.N.Inverse();
		mat.X = mat.Ninv%mat.A.Transpose()%mat.L;
		//compute V matrix
		mat.V = (mat.A%mat.X) - mat.L;
		//compute VTV
		mat.VTV = mat.V.Transpose()%mat.V;
		//compute posteriori variance
		newvar = mat.pos_var = mat.VTV(0,0)/mat.DF;
		//compute standard deviation
		mat.SD = sqrt(mat.pos_var);
		//compute variance-covariance matrix
		mat.Var_Co = mat.Ninv*mat.pos_var;
		//compute variance matirx
		mat.Var.Resize(5+num_ROPoint*3,1);
		for(i=0;i<(5+num_ROPoint*3);i++)
		{
			mat.Var(i,0) = mat.Var_Co(i,i);
		}
		
		//초기값 갱신
		Param_de.Romega += mat.X(0);
		Param_de.Rphi   += mat.X(1);
		Param_de.Rkappa += mat.X(2);
		Param_de.Yr     += mat.X(3);
		Param_de.Zr     += mat.X(4);
		for(i=0;i<num_ROPoint;i++)
		{
			ROMPoint(i,1) += mat.X(5+i*3);
			ROMPoint(i,2) += mat.X(5+i*3+1);
			ROMPoint(i,3) += mat.X(5+i*3+2);
		}
		
		//maximum correction(X matrix)
		mat.maxX = fabs(mat.X(0));
		for(i=1;i<(int)mat.X.GetRows();i++)
		{
			if(fabs(mat.X(i))>mat.maxX)
				mat.maxX = fabs(mat.X(i));
		}		
		
		//Print Result
		temp.Format("Iteration--->%d\r\n\r\n\r\n",iteration);
		result +=temp;
		result += PrintResult_de();
		
		//reference variance(posteriori variance)monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			increse_new = TRUE;
		}
		else
		{
			increse_new = FALSE;
		}
		
		//iteration end condition
		//variance difference monitoring 
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = TRUE;
			result += "[Difference Between Old_Variance And New_Variance < Difference Variance limit !!!]\r\n";
		}
		else
		{
			oldvar = newvar;
		}
		//variance monitoring
		if(((increse_old == TRUE)&&(increse_new == TRUE)))
		{
			iteration_end = TRUE;
			result += "[Variance Continuously(2회연속) Increse!!!]\r\n";
		}
		//SD monitoring
		if(fabs(mat.SD)<fabs(maxSD))
		{
			iteration_end = TRUE;
			result += "[Standard Deviation < SD limit !!!]\r\n";
		}
		//max correction
		if(mat.maxX<fabs(maxCorrection))
		{
			iteration_end = TRUE;
			result += "[Maximum Correction < correction limit !!!]\r\n";
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = TRUE;
			result += "[Iteration Max_Num Execss!!!]\r\n";
		}
		
		
	}while(iteration_end == FALSE);
	
	

	ROComplete = TRUE;
	ROMethod_de = TRUE;

	return TRUE;
}

//Independent Relative Orientation
BOOL CModel::RO_independent(void)
{
	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	BOOL increse_old = FALSE, increse_new = FALSE;	//monitoring variance
	BOOL iteration_end = FALSE;						//iteration stop
	//좌우 사진의 상호표정점 갯수 검사
	if(LPhoto.GetNumROP() == RPhoto.GetNumROP())
	{
		num_ROPoint = LPhoto.GetNumROP();
		mat.DF = num_ROPoint - 5;
	}
	else
	{
		return FALSE;
	}
	//Initialize parameter
	InitApp_inde();
	result = "[Relative Orientation(Independent)]\r\n\r\n\r\n";
	//Start Iteration
	do
	{
		CSMStr temp;
		iteration++;	//Increse Iteration Number (From 1)
		increse_old = increse_new;
		//make matrix A & L
		make_A_L_RO_independent();
		//compute X matrix
		mat.N = mat.A.Transpose()%mat.A;
		mat.Ninv = mat.N.Inverse();
		mat.X = mat.Ninv%mat.A.Transpose()%mat.L;
		//compute V matrix
		mat.V = (mat.A%mat.X) - mat.L;
		//compute VTV
		mat.VTV = mat.V.Transpose()%mat.V;
		//compute posteriori variance
		newvar = mat.pos_var = mat.VTV(0,0)/mat.DF;
		//compute standard deviation
		mat.SD = sqrt(mat.pos_var);
		//compute variance-covariance matrix
		mat.Var_Co = mat.Ninv*mat.pos_var;
		//compute variance matrix
		mat.Var.Resize(5+num_ROPoint*3,1);
		for(i=0;i<(5+num_ROPoint*3);i++)
		{
			mat.Var(i,0) = mat.Var_Co(i,i);
		}
		
		//초기값 갱신
		Param_inde.Lphi   += mat.X(0);
		Param_inde.Lkappa += mat.X(1);
		Param_inde.Romega += mat.X(2);
		Param_inde.Rphi   += mat.X(3);
		Param_inde.Rkappa += mat.X(4);
		for(i=0;i<num_ROPoint;i++)
		{
			ROMPoint(i,1) += mat.X(5+i*3);
			ROMPoint(i,2) += mat.X(5+i*3+1);
			ROMPoint(i,3) += mat.X(5+i*3+2);
		}
		
		//maximum correction (X matrix)
		mat.maxX = fabs(mat.X(0));
		for(i=1;i<(int)mat.X.GetRows();i++)
		{
			if(fabs(mat.X(i))>mat.maxX)
				mat.maxX = fabs(mat.X(i));
		}
		
		//Print Result
		temp.Format("Iteration--->%d\r\n\r\n\r\n",iteration);
		result +=temp;
		result +=PrintResult_inde();
		
		//reference variance(posteriori variance) monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			increse_new = TRUE;
		}
		else
		{
			increse_new = FALSE;
		}
		
		//iteration end condition
		//variance difference monitoring
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = TRUE;
			result += "[Difference Between Old_Variance And New_Variance < Difference Variance Limit !!!]\r\n";
		}
		else
		{
			oldvar = newvar;
		}
		
		//variance monitoring
		if(((increse_old==TRUE)&&(increse_new == TRUE)))
		{
			iteration_end = TRUE;
			result += "[Variance Continuously(2회 연속) Increse!!!]\r\n";
		}
		//SD monitoring
		if(fabs(mat.SD)<fabs(maxSD))
		{
			iteration_end = TRUE;
			result += "[Standard Deviation < SD Limit !!!]\r\n";
		}
		//max correction
		if(mat.maxX<fabs(maxCorrection))
		{
			iteration_end = TRUE;
			result += "[Maximum Correction < Correction Limit !!!]\r\n";
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = TRUE;
			result += "[Iteration Max_Num Execss !!!]\r\n";
		}
		
	}while(iteration_end == FALSE);
	
	ROComplete = TRUE;
	ROMethod_inde = TRUE;

	return TRUE;
}

void CModel::make_A_L_RO_independent(void)
{
	int i;
	mat.A.Resize(4*num_ROPoint,5+num_ROPoint*3,0.0);
	mat.L.Resize(4*num_ROPoint,1,0.0);
	
	
	//Left Photo
	{
		double xo = LPhoto.GetCamera().PPA.x, yo = LPhoto.GetCamera().PPA.y;
		double omega = Param_inde.Lomega, phi = Param_inde.Lphi, kappa = Param_inde.Lkappa;
		double f = LPhoto.GetCamera().f;
		CRotationcoeff m(omega, phi, kappa); //rotation coefficient
		for(i=0;i<num_ROPoint;i++)
		{
			double deltaX,deltaY,deltaZ;
			double xa = LPhoto.ROPoint(i,1), ya = LPhoto.ROPoint(i,2);
			deltaX = ROMPoint(i,1) - Param_inde.Xl;
			deltaY = ROMPoint(i,2) - Param_inde.Yl;
			deltaZ = ROMPoint(i,3) - Param_inde.Zl;
			
			b.q = m.Rmatrix(2,0)*deltaX + m.Rmatrix(2,1)*deltaY + m.Rmatrix(2,2)*deltaZ;
			b.r = m.Rmatrix(0,0)*deltaX + m.Rmatrix(0,1)*deltaY + m.Rmatrix(0,2)*deltaZ;
			b.s = m.Rmatrix(1,0)*deltaX + m.Rmatrix(1,1)*deltaY + m.Rmatrix(1,2)*deltaZ;
			
			b.b12 = f/(b.q*b.q)*(b.r*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(-sin(phi)*cos(kappa)*deltaX + sin(omega)*cos(phi)*cos(kappa)*deltaY - cos(omega)*cos(phi)*cos(kappa)*deltaZ));
			b.b13 = -f/(b.q)*(m.Rmatrix(1,0)*deltaX + m.Rmatrix(1,1)*deltaY + m.Rmatrix(1,2)*deltaZ);
			b.b14 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,0) - b.q*m.Rmatrix(0,0));
			b.b15 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,1) - b.q*m.Rmatrix(0,1));
			b.b16 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,2) - b.q*m.Rmatrix(0,2));
			
			b.J = xa - xo + (f*b.r/b.q);
			
			b.b21 = f/(b.q*b.q)*(b.s*(-m.Rmatrix(2,2)*deltaY+m.Rmatrix(2,1)*deltaZ) - b.q*(-m.Rmatrix(1,2)*deltaY+m.Rmatrix(1,1)*deltaZ));
			b.b22 = f/(b.q*b.q)*(b.s*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(sin(phi)*sin(kappa)*deltaX - sin(omega)*cos(phi)*sin(kappa)*deltaY + cos(omega)*cos(phi)*sin(kappa)*deltaZ));
			b.b23 = f/(b.q)*(m.Rmatrix(0,0)*deltaX + m.Rmatrix(0,1)*deltaY + m.Rmatrix(0,2)*deltaZ);
			b.b24 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,0) - b.q*m.Rmatrix(1,0));
			b.b25 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,1) - b.q*m.Rmatrix(1,1));
			b.b26 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,2) - b.q*m.Rmatrix(1,2));
			
			b.K = ya - yo + (f*b.s/b.q);
			//A matrix
			//Rotation Angle
			mat.A(i*4,0) = b.b12;
			mat.A(i*4,1) = b.b13;
			
			mat.A(i*4+1,0) = b.b22;
			mat.A(i*4+1,1) = b.b23;
			//Model Space Point
			mat.A(i*4,5+i*3) = b.b14;
			mat.A(i*4,5+i*3+1) = b.b15;
			mat.A(i*4,5+i*3+2) = b.b16;
			
			mat.A(i*4+1,5+i*3) = b.b24;
			mat.A(i*4+1,5+i*3+1) = b.b25;
			mat.A(i*4+1,5+i*3+2) = b.b26;
			//L matrix
			mat.L(i*4,0) = b.J;
			
			mat.L(i*4+1,0) = b.K;
			
		}
	}//Left Photo End
	
	//Right Photo	
	{
		double xo = RPhoto.GetCamera().PPA.x, yo = RPhoto.GetCamera().PPA.y;
		double omega = Param_inde.Romega, phi = Param_inde.Rphi, kappa = Param_inde.Rkappa;
		double f = RPhoto.GetCamera().f;
		CRotationcoeff m(omega, phi, kappa); //rotation coefficient
		for(i=0;i<num_ROPoint;i++)
		{
			double deltaX,deltaY,deltaZ;
			double xa = RPhoto.ROPoint(i,1), ya = RPhoto.ROPoint(i,2);
			deltaX = ROMPoint(i,1) - Param_inde.Xr;
			deltaY = ROMPoint(i,2) - Param_inde.Yr;
			deltaZ = ROMPoint(i,3) - Param_inde.Zr;
			
			b.q = m.Rmatrix(2,0)*deltaX + m.Rmatrix(2,1)*deltaY + m.Rmatrix(2,2)*deltaZ;
			b.r = m.Rmatrix(0,0)*deltaX + m.Rmatrix(0,1)*deltaY + m.Rmatrix(0,2)*deltaZ;
			b.s = m.Rmatrix(1,0)*deltaX + m.Rmatrix(1,1)*deltaY + m.Rmatrix(1,2)*deltaZ;
			
			b.b11 = f/(b.q*b.q)*(b.r*(-m.Rmatrix(2,2)*deltaY + m.Rmatrix(2,1)*deltaZ) - b.q*(-m.Rmatrix(0,2)*deltaY + m.Rmatrix(0,1)*deltaZ));
			b.b12 = f/(b.q*b.q)*(b.r*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(-sin(phi)*cos(kappa)*deltaX + sin(omega)*cos(phi)*cos(kappa)*deltaY - cos(omega)*cos(phi)*cos(kappa)*deltaZ));
			b.b13 = -f/(b.q)*(m.Rmatrix(1,0)*deltaX + m.Rmatrix(1,1)*deltaY + m.Rmatrix(1,2)*deltaZ);
			b.b14 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,0) - b.q*m.Rmatrix(0,0));
			b.b15 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,1) - b.q*m.Rmatrix(0,1));
			b.b16 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,2) - b.q*m.Rmatrix(0,2));
			
			b.J = xa - xo + (f*b.r/b.q);
			
			b.b21 = f/(b.q*b.q)*(b.s*(-m.Rmatrix(2,2)*deltaY+m.Rmatrix(2,1)*deltaZ) - b.q*(-m.Rmatrix(1,2)*deltaY+m.Rmatrix(1,1)*deltaZ));
			b.b22 = f/(b.q*b.q)*(b.s*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(sin(phi)*sin(kappa)*deltaX - sin(omega)*cos(phi)*sin(kappa)*deltaY + cos(omega)*cos(phi)*sin(kappa)*deltaZ));
			b.b23 = f/(b.q)*(m.Rmatrix(0,0)*deltaX + m.Rmatrix(0,1)*deltaY + m.Rmatrix(0,2)*deltaZ);
			b.b24 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,0) - b.q*m.Rmatrix(1,0));
			b.b25 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,1) - b.q*m.Rmatrix(1,1));
			b.b26 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,2) - b.q*m.Rmatrix(1,2));
			
			b.K = ya - yo + (f*b.s/b.q);
			//A matrix
			//Rotation Angle
			mat.A(i*4+2,2) = b.b11;
			mat.A(i*4+2,3) = b.b12;
			mat.A(i*4+2,4) = b.b13;
			
			mat.A(i*4+3,2) = b.b21;
			mat.A(i*4+3,3) = b.b22;
			mat.A(i*4+3,4) = b.b23;
			//Model Space Point
			mat.A(i*4+2,5+i*3+0) = b.b14;
			mat.A(i*4+2,5+i*3+1) = b.b15;
			mat.A(i*4+2,5+i*3+2) = b.b16;
			
			mat.A(i*4+3,5+i*3+0) = b.b24;
			mat.A(i*4+3,5+i*3+1) = b.b25;
			mat.A(i*4+3,5+i*3+2) = b.b26;
			//L matrix
			mat.L(i*4+2,0) = b.J;
			
			mat.L(i*4+3,0) = b.K;
		}
	}//Right Photo End
	
}

void CModel::make_A_L_RO_dependent(void)
{
	int i;
	mat.A.Resize(4*num_ROPoint,5+num_ROPoint*3,0.0);
	mat.L.Resize(4*num_ROPoint,1,0.0);
	
	
	//Left Photo
	{
		double xo = LPhoto.GetCamera().PPA.x, yo = LPhoto.GetCamera().PPA.y;
		double omega = Param_de.Lomega, phi = Param_de.Lphi, kappa = Param_de.Lkappa;
		double f = LPhoto.GetCamera().f;
		CRotationcoeff m(omega, phi, kappa); //rotation coefficient
		for(i=0;i<num_ROPoint;i++)
		{
			double deltaX,deltaY,deltaZ;
			double xa = LPhoto.ROPoint(i,1), ya = LPhoto.ROPoint(i,2);
			deltaX = ROMPoint(i,1) - Param_de.Xl;
			deltaY = ROMPoint(i,2) - Param_de.Yl;
			deltaZ = ROMPoint(i,3) - Param_de.Zl;
			
			b.q = m.Rmatrix(2,0)*deltaX + m.Rmatrix(2,1)*deltaY + m.Rmatrix(2,2)*deltaZ;
			b.r = m.Rmatrix(0,0)*deltaX + m.Rmatrix(0,1)*deltaY + m.Rmatrix(0,2)*deltaZ;
			b.s = m.Rmatrix(1,0)*deltaX + m.Rmatrix(1,1)*deltaY + m.Rmatrix(1,2)*deltaZ;
			
			b.b14 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,0) - b.q*m.Rmatrix(0,0));
			b.b15 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,1) - b.q*m.Rmatrix(0,1));
			b.b16 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,2) - b.q*m.Rmatrix(0,2));
			
			b.J = xa - xo + (f*b.r/b.q);
			
			b.b24 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,0) - b.q*m.Rmatrix(1,0));
			b.b25 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,1) - b.q*m.Rmatrix(1,1));
			b.b26 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,2) - b.q*m.Rmatrix(1,2));
			
			b.K = ya - yo + (f*b.s/b.q);
			//A matrix
			//Model Space Point
			mat.A(i*4,5+i*3) = b.b14;
			mat.A(i*4,5+i*3+1) = b.b15;
			mat.A(i*4,5+i*3+2) = b.b16;
			
			mat.A(i*4+1,5+i*3) = b.b24;
			mat.A(i*4+1,5+i*3+1) = b.b25;
			mat.A(i*4+1,5+i*3+2) = b.b26;
			//L matrix
			mat.L(i*4,0) = b.J;
			
			mat.L(i*4+1,0) = b.K;
			
		}
	}//Left Photo End
	
	//Right Photo	
	{
		double xo = RPhoto.GetCamera().PPA.x, yo = RPhoto.GetCamera().PPA.y;
		double omega = Param_de.Romega, phi = Param_de.Rphi, kappa = Param_de.Rkappa;
		double f = RPhoto.GetCamera().f;
		CRotationcoeff m(omega, phi, kappa); //rotation coefficient
		for(i=0;i<num_ROPoint;i++)
		{
			double deltaX,deltaY,deltaZ;
			double xa = RPhoto.ROPoint(i,1), ya = RPhoto.ROPoint(i,2);
			deltaX = ROMPoint(i,1) - Param_de.Xr;
			deltaY = ROMPoint(i,2) - Param_de.Yr;
			deltaZ = ROMPoint(i,3) - Param_de.Zr;
			
			b.q = m.Rmatrix(2,0)*deltaX + m.Rmatrix(2,1)*deltaY + m.Rmatrix(2,2)*deltaZ;
			b.r = m.Rmatrix(0,0)*deltaX + m.Rmatrix(0,1)*deltaY + m.Rmatrix(0,2)*deltaZ;
			b.s = m.Rmatrix(1,0)*deltaX + m.Rmatrix(1,1)*deltaY + m.Rmatrix(1,2)*deltaZ;
			
			b.b11 = f/(b.q*b.q)*(b.r*(-m.Rmatrix(2,2)*deltaY + m.Rmatrix(2,1)*deltaZ) - b.q*(-m.Rmatrix(0,2)*deltaY + m.Rmatrix(0,1)*deltaZ));
			b.b12 = f/(b.q*b.q)*(b.r*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(-sin(phi)*cos(kappa)*deltaX + sin(omega)*cos(phi)*cos(kappa)*deltaY - cos(omega)*cos(phi)*cos(kappa)*deltaZ));
			b.b13 = -f/(b.q)*(m.Rmatrix(1,0)*deltaX + m.Rmatrix(1,1)*deltaY + m.Rmatrix(1,2)*deltaZ);
			b.b14 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,0) - b.q*m.Rmatrix(0,0));
			b.b15 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,1) - b.q*m.Rmatrix(0,1));
			b.b16 = f/(b.q*b.q)*(b.r*m.Rmatrix(2,2) - b.q*m.Rmatrix(0,2));
			
			b.J = xa - xo + (f*b.r/b.q);
			
			b.b21 = f/(b.q*b.q)*(b.s*(-m.Rmatrix(2,2)*deltaY+m.Rmatrix(2,1)*deltaZ) - b.q*(-m.Rmatrix(1,2)*deltaY+m.Rmatrix(1,1)*deltaZ));
			b.b22 = f/(b.q*b.q)*(b.s*(cos(phi)*deltaX + sin(omega)*sin(phi)*deltaY - cos(omega)*sin(phi)*deltaZ) - b.q*(sin(phi)*sin(kappa)*deltaX - sin(omega)*cos(phi)*sin(kappa)*deltaY + cos(omega)*cos(phi)*sin(kappa)*deltaZ));
			b.b23 = f/(b.q)*(m.Rmatrix(0,0)*deltaX + m.Rmatrix(0,1)*deltaY + m.Rmatrix(0,2)*deltaZ);
			b.b24 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,0) - b.q*m.Rmatrix(1,0));
			b.b25 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,1) - b.q*m.Rmatrix(1,1));
			b.b26 = f/(b.q*b.q)*(b.s*m.Rmatrix(2,2) - b.q*m.Rmatrix(1,2));
			
			b.K = ya - yo + (f*b.s/b.q);
			//A matrix
			//Rotation Angle
			mat.A(i*4+2,0) = b.b11;
			mat.A(i*4+2,1) = b.b12;
			mat.A(i*4+2,2) = b.b13;
			
			mat.A(i*4+3,0) = b.b21;
			mat.A(i*4+3,1) = b.b22;
			mat.A(i*4+3,2) = b.b23;
			//Perspective Center
			mat.A(i*4+2,3) = -b.b15;
			mat.A(i*4+2,4) = -b.b16;
			
			mat.A(i*4+3,3) = -b.b25;
			mat.A(i*4+3,4) = -b.b26;
			//Model Space Point
			mat.A(i*4+2,5+i*3+0) = b.b14;
			mat.A(i*4+2,5+i*3+1) = b.b15;
			mat.A(i*4+2,5+i*3+2) = b.b16;
			
			mat.A(i*4+3,5+i*3+0) = b.b24;
			mat.A(i*4+3,5+i*3+1) = b.b25;
			mat.A(i*4+3,5+i*3+2) = b.b26;
			//L matrix
			mat.L(i*4+2,0) = b.J;
			
			mat.L(i*4+3,0) = b.K;
		}
	}//Right Photo End
	
}

CSMStr CModel::PrintResult_inde()
{
	CSMStr temp;
	CSMStr result;
	result +="[A matrix]\r\n";
	result +=mat.A.matrixout();
	result +="\r\n\r\n";
	result +="[L matrix]\r\n";
	result +=mat.L.matrixout();
	result +="\r\n\r\n";
	result +="[X matrix]\r\n";
	result +=mat.X.matrixout();
	result +="\r\n\r\n";
	result +="[V matrix]\r\n";
	result +=mat.V.matrixout();
	result +="\r\n\r\n";
	result +="[VTV matrix]\r\n";
	result +=mat.VTV.matrixout();
	result +="\r\n\r\n";
	temp.Format("[Posetriori Variance]\r\n%20.15le\r\n\r\n\r\n",mat.pos_var);
	result +=temp;
	temp.Format("Maximum element of X matrix : %20.15le\r\n",mat.maxX);
	result +=temp;
	result +="\r\n\r\n";
	result +="[Variance_Corvariance matrix]\r\n";
	result +=mat.Var_Co.matrixout();
	result +="\r\n\r\n";
	result +="[Variance matrix]\r\n";
	result +=mat.Var.matrixout();
	result +="\r\n\r\n";
	result +="[RO_point(3D-model space) Coordinate]\r\n";
	result +=ROMPoint.matrixout();
	result +="\r\n\r\n";
	
	result +="[RO Parameter(Independent)]\r\n";
	temp.Format("LPhoto omega : %le\r\n",Param_inde.Lomega);
	result +=temp;
	temp.Format("LPhoto phi   : %le\r\n",Param_inde.Lphi);
	result +=temp;
	temp.Format("LPhoto kappa : %le\r\n",Param_inde.Lkappa);
	result +=temp;
	temp.Format("LPhoto Xl    : %le\r\n",Param_inde.Xl);
	result +=temp;
	temp.Format("LPhoto Yl    : %le\r\n",Param_inde.Yl);
	result +=temp;
	temp.Format("LPhoto Zl    : %le\r\n",Param_inde.Zl);
	result +=temp;
	temp.Format("RPhoto omega : %le\r\n",Param_inde.Romega);
	result +=temp;
	temp.Format("RPhoto phi   : %le\r\n",Param_inde.Rphi);
	result +=temp;
	temp.Format("RPhoto kappa : %le\r\n",Param_inde.Rkappa);
	result +=temp;
	temp.Format("RPhoto Xr    : %le\r\n",Param_inde.Xr);
	result +=temp;
	temp.Format("RPhoto Yr    : %le\r\n",Param_inde.Yr);
	result +=temp;
	temp.Format("RPhoto Zr    : %le\r\n",Param_inde.Zr);
	result +=temp;
	result +="\r\n\r\n";
	
	return result;
	
}

CSMStr CModel::PrintResult_de()
{
	CSMStr temp;
	CSMStr result;
	temp.Format("[A matrix]\r\n");
	result +=temp;
	result +=mat.A.matrixout();
	result +="\r\n\r\n";
	result +="[L matrix]\r\n";
	result +=mat.L.matrixout();
	result +="\r\n\r\n";
	result +="[X matrix]\r\n";
	result +=mat.X.matrixout();
	result +="\r\n\r\n";
	result +="[V matrix]\r\n";
	result +=mat.V.matrixout();
	result +="\r\n\r\n";
	result +="[VTV matrix]\r\n";
	result +=mat.VTV.matrixout();
	result +="\r\n\r\n";
	temp.Format("[Posetriori Variance]\r\n%20.15le\r\n\r\n\r\n",mat.pos_var);
	result +=temp;
	temp.Format("Maximum element of X matrix : %20.15le\r\n",mat.maxX);
	result +=temp;
	result +="\r\n\r\n";
	result +="[Variance_Corvariance matrix]\r\n";
	result +=mat.Var_Co.matrixout();
	result +="\r\n\r\n";
	result +="[Variance matrix]\r\n";
	result +=mat.Var.matrixout();
	result +="\r\n\r\n";
	result +="[RO_point(3D-model space) Coordinate]\r\n";
	result +=ROMPoint.matrixout();
	result +="\r\n\r\n";
	
	result +="[RO Parameter(Dependent)]\r\n";
	temp.Format("LPhoto omega : %le\r\n",Param_de.Lomega);
	result +=temp;
	temp.Format("LPhoto phi   : %le\r\n",Param_de.Lphi);
	result +=temp;
	temp.Format("LPhoto kappa : %le\r\n",Param_de.Lkappa);
	result +=temp;
	temp.Format("LPhoto Xl    : %le\r\n",Param_de.Xl);
	result +=temp;
	temp.Format("LPhoto Yl    : %le\r\n",Param_de.Yl);
	result +=temp;
	temp.Format("LPhoto Zl    : %le\r\n",Param_de.Zl);
	result +=temp;
	temp.Format("RPhoto omega : %le\r\n",Param_de.Romega);
	result +=temp;
	temp.Format("RPhoto phi   : %le\r\n",Param_de.Rphi);
	result +=temp;
	temp.Format("RPhoto kappa : %le\r\n",Param_de.Rkappa);
	result +=temp;
	temp.Format("RPhoto Xr    : %le\r\n",Param_de.Xr);
	result +=temp;
	temp.Format("RPhoto Yr    : %le\r\n",Param_de.Yr);
	result +=temp;
	temp.Format("RPhoto Zr    : %le\r\n",Param_de.Zr);
	result +=temp;
	result +="\r\n\r\n";
	
	return result;
	
}

void CModel::FilePrintResult(char* outfile)
{
	FILE* outdata;
	outdata = fopen(outfile,"w");
	fputs(result.GetChar(),outdata);
	fclose(outdata);
}

BOOL CModel::DependentTOIndependent(void)
{
	double dkappa, dphi;
	Matrix<double> Rotation_L,Rotation_R;
	dkappa = -atan2(Param_de.Yr,Param_de.Xr);
	dphi = atan2((Param_de.Zr-Param_de.Zl),sqrt(pow(Param_de.Xr,2)+pow(Param_de.Yr,2)));
	
	//RL과 RR은 각각 종속법에서 구한 사진의 기하(original axis->photo axis)
	CRotationcoeff RL(Param_de.Lomega,Param_de.Lphi,Param_de.Lkappa);
	CRotationcoeff RR(Param_de.Romega,Param_de.Rphi,Param_de.Rkappa);
	
	//RB 는 base_line에 대한 회전메트릭스(base line->original axis)
	CRotationcoeff RB(0,dphi,dkappa);
	
	//Rotation_L과  Rotation_R은 각각 종속법에서 기선을 회전시켜 구한 독립법 사진의 기하
	//base line->photo axis
	Rotation_L = RL.Rmatrix%RB.Rmatrix;
	Rotation_R = RR.Rmatrix%RB.Rmatrix;
	
	//회전메트릭스에서 추출해낸 회전각 요소
	Param_inde.Lomega = atan(-Rotation_L(2,1)/Rotation_L(2,2));
	Param_inde.Lphi = asin(Rotation_L(2,0));
	Param_inde.Lkappa = atan(-Rotation_L(1,0)/Rotation_L(0,0));
	
	Param_inde.Romega = atan(-Rotation_R(2,1)/Rotation_R(2,2));
	Param_inde.Rphi = asin(Rotation_R(2,0));
	Param_inde.Rkappa = atan(-Rotation_R(1,0)/Rotation_R(0,0));
	
	//양쪽 사진의 투영중심좌표
	Param_inde.Xl = Param_de.Xl;
	Param_inde.Yl = Param_de.Yl;
	Param_inde.Zl = Param_de.Zl;
	
	Param_inde.Xr = Param_de.Xr;
	Param_inde.Yr = Param_de.Yl;
	Param_inde.Zr = Param_de.Zl;
	
	return TRUE;
}

BOOL CModel::IndependentTODependent(void)
{
	Matrix<double> Rotation_L,Rotation_R;
				
	//RL과 RR은 각각 독립법에서 구한 사진의 기하
	CRotationcoeff RL(Param_inde.Lomega,Param_inde.Lphi,Param_inde.Lkappa);
	CRotationcoeff RR(Param_inde.Romega,Param_inde.Rphi,Param_inde.Rkappa);
	
	//Base line 회전메트릭스(왼쪽 사진 회전메트릭스의 역회전으로 구성)
	Matrix<double> RB;
	RB = RL.Rmatrix.Transpose();
	
	//Rotation_L과  Rotation_R은 각각 독립법에서 회전시켜 구한 종속법 사진 기하
	Rotation_L = RL.Rmatrix%RB;
	Rotation_R = RR.Rmatrix%RB;
	
	//회전메트릭스에서 추출해낸 회전각 요소
	Param_de.Lomega = atan(-Rotation_L(2,1)/Rotation_L(2,2));
	Param_de.Lphi = asin(Rotation_L(2,0));
	Param_de.Lkappa = atan(-Rotation_L(1,0)/Rotation_L(0,0));
	
	Param_de.Romega = atan(-Rotation_R(2,1)/Rotation_R(2,2));
	Param_de.Rphi = asin(Rotation_R(2,0));
	Param_de.Rkappa = atan(-Rotation_R(1,0)/Rotation_R(0,0));
	
	//왼쪽 사진의 투영중심 좌표는 종속과 독립이 같다
	Param_de.Xl = Param_inde.Xl;
	Param_de.Yl = Param_inde.Yl;
	Param_de.Zl = Param_inde.Zl;
	
	//오른쪽 사진의 투영중심 좌표(tan(kappa)에 -1을 곱한다.)
	Param_de.Xr = Param_inde.Xr;
	Param_de.Yr = Param_inde.Xr*(-tan(Param_inde.Lkappa));
	Param_de.Zr = Param_inde.Zr + sqrt(Param_de.Yr*Param_de.Yr+Param_inde.Xr*Param_inde.Xr)*tan(Param_inde.Lphi);
	
	ROMethod_de = TRUE;

	return TRUE;
}

BOOL CModel::AO_General(void)
{
	int i;
	//iteration condition
	int iteration=0;
	double oldvar=10.0E99, newvar;					//monitoring variance
	BOOL increse_old = FALSE,increse_new = FALSE;	//monitoring variance
	BOOL iteration_end = FALSE;						//Iteration Stop
	
	//Degree of Freedom
	mat.DF = 3*num_GCP - 7;
	//Initialize AO Parameters
	InitApp_AO();
	
	result = "[Absolute Orientation(General Lease Square)]\r\n\r\n\r\n";
	
	//Start Iteration
	do
	{
		CSMStr temp;
		iteration++;					//Increse Iteration Number(From 1)
		increse_old = increse_new;
		
		//make matrix A & L & B
		make_A_L_B_AO_General();
		//compute We(equivalent weight matrix)
		mat.We = (mat.B%mat.B.Transpose()).Inverse();
		//compute X matrix
		mat.N = mat.A.Transpose()%mat.We%mat.A;
		mat.Ninv = mat.N.Inverse();
		mat.X = mat.Ninv%mat.A.Transpose()%mat.We%mat.L;
		//compute Ve(equivalent residuals) matrix
		mat.Ve = (mat.A%mat.X) - mat.L;
		//compute V(residual)
		mat.V = mat.B.Transpose()%mat.We%mat.Ve;
		//compute VTV
		mat.VTV = mat.V.Transpose()%mat.V;
		//compute posteriori variance
		newvar = mat.pos_var = mat.VTV(0,0)/mat.DF;
		//compute standard deviation
		mat.SD = sqrt(mat.pos_var);
		//compute variance-covariance matrix
		mat.Var_Co = mat.Ninv*mat.pos_var;
		//compute variance matirx
		mat.Var.Resize(7,1);
		for(i=0;i<7;i++)
		{
			mat.Var(i,0) = mat.Var_Co(i,i);
		}
		
		//초기값 갱신
		Param_AO.S		+= mat.X(0,0);//S
		Param_AO.omega	+= mat.X(1,0);//omega
		Param_AO.phi	+= mat.X(2,0);//phi
		Param_AO.kappa	+= mat.X(3,0);//kappa
		Param_AO.T(0,0)	+= mat.X(4,0);//XT
		Param_AO.T(1,0)	+= mat.X(5,0);//YT
		Param_AO.T(2,0)	+= mat.X(6,0);//ZT
		
		//maximum correction(X matrix)
		mat.maxX = fabs(mat.X(0));
		for(i=1;i<(int)mat.X.GetRows();i++)
		{
			if(fabs(mat.X(i))>mat.maxX)
				mat.maxX = fabs(mat.X(i));
		}		
		
		//Print Result
		temp.Format("Iteration--->%d\r\n\r\n\r\n",iteration);
		result +=temp;
		result += PrintResult_AO_General();
		
		//reference variance(posteriori variance)monitoring
		diffVar = (oldvar - newvar);
		if(diffVar<0)
		{
			increse_new = TRUE;
		}
		else
		{
			increse_new = FALSE;
		}
		
		//iteration end condition
		//variance difference monitoring 
		if(fabs(diffVar)<fabs(maxdiffVar))
		{
			iteration_end = TRUE;
			result += "[Difference Between Old_Variance And New_Variance < Difference Variance limit !!!]\r\n";
		}
		else
		{
			oldvar = newvar;
		}
		//variance monitoring
		if(((increse_old == TRUE)&&(increse_new == TRUE)))
		{
			iteration_end = TRUE;
			result += "[Variance Continuously(2회연속) Increse!!!]\r\n";
		}
		//SD monitoring
		if(fabs(mat.SD)<fabs(maxSD))
		{
			iteration_end = TRUE;
			result += "[Standard Deviation < SD limit !!!]\r\n";
		}
		//max correction
		if(mat.maxX<fabs(maxCorrection))
		{
			iteration_end = TRUE;
			result += "[Maximum Correction < correction limit !!!]\r\n";
		}
		//num of iteration
		if(iteration>=maxIteration)
		{
			iteration_end = TRUE;
			result += "[Iteration Max_Num Execss!!!]\r\n";
		}
		
		
	}while(iteration_end == FALSE);
	
	//calculate photo rotation and Perspective center
	CalPhotoAttitude();
	result += PrintResult_AO_Parameter();
	
	AOComplete_General = TRUE;
	
	return TRUE;
}

void CModel::InitApp_AO(void)
{
	int i;
	Point3D<double> mp1,mp2,gp1,gp2;//variable for calculating scale
	//GCP vs Model Point
	for(i=0;i<num_GCP;i++)
	{
		if(GCP(0,0) == AOMPoint(i,0))
		{
			mp1.x = AOMPoint(i,1);
			mp1.y = AOMPoint(i,2);
			mp1.z = AOMPoint(i,3);
			gp1.x = GCP(0,1);
			gp1.y = GCP(0,2);
			gp1.z = GCP(0,3);
			break;
		}
	}
	for(i=0;i<num_ROPoint;i++)
	{
		if(GCP(2,0) == AOMPoint(i,0))
		{
			mp2.x = AOMPoint(i,1);
			mp2.y = AOMPoint(i,2);
			mp2.z = AOMPoint(i,3);
			gp2.x = GCP(2,1);
			gp2.y = GCP(2,2);
			gp2.z = GCP(2,3);
			break;
		}
	}
	double d1 = sqrt(pow((mp1.x-mp2.x),2)
		+pow((mp1.y-mp2.y),2)
		+pow((mp1.z-mp2.z),2));
	double d2 = sqrt(pow((gp1.x-gp2.x),2)
		+pow((gp1.y-gp2.y),2)
		+pow((gp1.z-gp2.z),2));
	
	double k1 = atan2((mp2.x-mp1.x),(mp2.y-mp1.y));
	double k2 = atan2((gp2.x-gp1.x),(gp2.y-gp1.y));
	
	//Scale
	Param_AO.S = d2/d1;
	
	//Rotation (Ground -- > Model)
	Param_AO.omega = 0.0;
	Param_AO.phi   = 0.0;
	Param_AO.kappa = (k1 - k2);
	
	CRotationcoeff R(Param_AO.omega,Param_AO.phi,Param_AO.kappa);
	Matrix<double> T;
	Matrix<double> G(3,1),M(3,1);
	G(0,0) = GCP(0,1);
	G(1,0) = GCP(0,2);
	G(2,0) = GCP(0,3);
	M(0,0) = mp1.x;
	M(1,0) = mp1.x;
	M(2,0) = mp1.x;
	T = G - (R.Rmatrix.Transpose()%M)*Param_AO.S;
	//Origin Transfer
	Param_AO.T.Resize(3,1,0.0);
	Param_AO.T(0,0) = T(0,0);
	Param_AO.T(1,0) = T(1,0);
	Param_AO.T(2,0) = T(2,0);
	
}

void CModel::make_A_L_B_AO_General(void)
{
	int i;
	mat.A.Resize(num_GCP*3,7,0.0);
	mat.L.Resize(num_GCP*3,1,0.0);
	mat.B.Resize(num_GCP*3,num_GCP*6,0.0);
	
	double omega = Param_AO.omega, phi = Param_AO.phi, kappa = Param_AO.kappa;
	CRotationcoeff RT(omega, phi, kappa); //rotation matrix(Ground->model)
	Matrix<double> R,dRdO,dRdP,dRdK;//Right 회전메트릭스(Model->Ground)와
	//회전메트릭스 각요소를 w,p,k로 미분한 행렬
	R = RT.Rmatrix.Transpose();
	dRdO = makedRdO(R);
	dRdP = makedRdP(omega,phi,kappa);
	dRdK = makedRdK(omega,phi,kappa);
	
	for(i=0;i<num_GCP;i++)
	{
		int j;
		Point3D<double> mp = FindMPoint(i);//Model Point
		
		for(j=0;j<3;j++)
		{
			//A matrix(S,omega,phi,kappa,XT,YT,ZT)
			mat.A(i*3+j,0) = R(j,0)*mp.x + R(j,1)*mp.y + R(j,2)*mp.z;
			mat.A(i*3+j,1) = Param_AO.S*(dRdO(j,0)*mp.x + dRdO(j,1)*mp.y + dRdO(j,2)*mp.z);
			mat.A(i*3+j,2) = Param_AO.S*(dRdP(j,0)*mp.x + dRdP(j,1)*mp.y + dRdP(j,2)*mp.z);
			mat.A(i*3+j,3) = Param_AO.S*(dRdK(j,0)*mp.x + dRdK(j,1)*mp.y + dRdK(j,2)*mp.z);
			mat.A(i*3+j,4+j) = 1.0;
			
			//L matrix
			mat.L(i*3+j,0) = -(Param_AO.S*(R(j,0)*mp.x + R(j,1)*mp.y + R(j,2)*mp.z) + Param_AO.T(j,0) - GCP(i,j+1));
			
			//B matrix(Xmodel,Ymodel,Zmodel,Xgcp,Ygcp,Zgcp)
			mat.B(i*3+j,3*i) = Param_AO.S*R(j,0);
			mat.B(i*3+j,3*i+1) = Param_AO.S*R(j,1);
			mat.B(i*3+j,3*i+2) = Param_AO.S*R(j,2);
			
			mat.B(i*3+j,3*num_GCP+i*3+j) = -1;
		}
		
	}
	
}

Point3D<double> CModel::FindMPoint(int n)
{
	int i;
	Point3D<double> p;
	for(i=0;i<num_GCP;i++)
	{
		if(GCP(n,0) == AOMPoint(i,0))
		{
			p.x = AOMPoint(i,1);
			p.y = AOMPoint(i,2);
			p.z = AOMPoint(i,3);
			return p;
		}
	}
	return p;
	
}

CSMStr CModel::PrintResult_AO_General()
{
	const double P = 3.141592654;
	CSMStr temp;
	CSMStr result;
	result +="[A matrix]\r\n";
	result +=mat.A.matrixout();
	result +="\r\n\r\n";
	result +="[B matrix]\r\n";
	result +=mat.B.matrixout();
	result +="\r\n\r\n";
	result +="[L matrix]\r\n";
	result +=mat.L.matrixout();
	result +="\r\n\r\n";
	result +="[X matrix]\r\n";
	result +=mat.X.matrixout();
	result +="\r\n\r\n";
	result +="[We matrix]\r\n";
	result +=mat.We.matrixout();
	result +="\r\n\r\n";
	result +="[Ve matrix]\r\n";
	result +=mat.Ve.matrixout();
	result +="\r\n\r\n";
	result +="[V matrix]\r\n";
	result +=mat.V.matrixout();
	result +="\r\n\r\n";
	result +="[VTV matrix]\r\n";
	result +=mat.VTV.matrixout();
	result +="\r\n\r\n";
	temp.Format("[Posetriori Variance]\r\n%20.15le\r\n\r\n\r\n",mat.pos_var);
	result +=temp;
	temp.Format("[Standard Deviation]\r\n%20.15le\r\n\r\n\r\n",mat.SD);
	result +=temp;
	temp.Format("Maximum element of X matrix : %20.15le\r\n",mat.maxX);
	result +=temp;
	result +="\r\n\r\n";
	result +="[Variance_Corvariance matrix]\r\n";
	result +=mat.Var_Co.matrixout();
	result +="\r\n\r\n";
	result +="[Variance matrix]\r\n";
	result +=mat.Var.matrixout();
	result +="\r\n\r\n";
	
	result +="[3D Conformal Parameter]\r\n";
	temp.Format("Scale : %le\r\n",Param_AO.S);
	result +=temp;
	temp.Format("omega : %le\r\n",Param_AO.omega);
	result +=temp;
	temp.Format("phi   : %le\r\n",Param_AO.phi);
	result +=temp;
	temp.Format("kappa : %le\r\n",Param_AO.kappa);
	result +=temp;
	temp.Format("XT    : %le\r\n",Param_AO.T(0,0));
	result +=temp;
	temp.Format("YT    : %le\r\n",Param_AO.T(1,0));
	result +=temp;
	temp.Format("ZT    : %le\r\n",Param_AO.T(2,0));
	result +=temp;
	result +="\r\n\r\n";
	
	return result;
}

CSMStr CModel::PrintResult_AO_Parameter()
{
	const double P = 3.141592654;
	CSMStr temp;
	CSMStr result;
	
	result ="\r\n\r\n\r\n";
	result +="[AO Parameter(General)]\r\n";
	temp.Format("Lomega : %le(degree:%le)\r\n",Param_AO.Lomega,Param_AO.Lomega/P*180);
	result +=temp;
	temp.Format("Lphi : %le(degree:%le)\r\n",Param_AO.Lphi,Param_AO.Lphi/P*180);
	result +=temp;
	temp.Format("Lkappa : %le(degree:%le)\r\n",Param_AO.Lkappa,Param_AO.Lkappa/P*180);
	result +=temp;
	temp.Format("Xl    : %le\r\n",Param_AO.Xl);
	result +=temp;
	temp.Format("Yl    : %le\r\n",Param_AO.Yl);
	result +=temp;
	temp.Format("Zl    : %le\r\n",Param_AO.Zl);
	result +=temp;
	temp.Format("Romega : %le(degree:%le)\r\n",Param_AO.Romega,Param_AO.Romega/P*180);
	result +=temp;
	temp.Format("Rphi : %le(degree:%le)\r\n",Param_AO.Rphi,Param_AO.Rphi/P*180);
	result +=temp;
	temp.Format("Rkappa : %le(degree:%le)\r\n",Param_AO.Rkappa,Param_AO.Rkappa/P*180);
	result +=temp;
	temp.Format("Xr    : %le\r\n",Param_AO.Xr);
	result +=temp;
	temp.Format("Yr    : %le\r\n",Param_AO.Yr);
	result +=temp;
	temp.Format("Zr    : %le\r\n",Param_AO.Zr);
	result +=temp;
	result +="\r\n\r\n";
	
	return result;
}

BOOL CModel::CalPhotoAttitude(void)
{
	
	double Lo,Lp,Lk;
	double Ro,Rp,Rk;
	double Lx,Ly,Lz;
	double Rx,Ry,Rz;
	Matrix<double> RA;
	CRotationcoeff RAT(Param_AO.omega,Param_AO.phi,Param_AO.kappa);
	RA = RAT.Rmatrix.Transpose();
	
	if(ROMethod_inde == TRUE)
	{
		Lo = Param_inde.Lomega;
		Lp = Param_inde.Lphi;
		Lk = Param_inde.Lkappa;
		Lx = Param_inde.Xl;
		Ly = Param_inde.Yl;
		Lz = Param_inde.Zl;
		
		Ro = Param_inde.Romega;
		Rp = Param_inde.Rphi;
		Rk = Param_inde.Rkappa;
		Rx = Param_inde.Xr;
		Ry = Param_inde.Yr;
		Rz = Param_inde.Zr;
		
		Param_AO.Xl = Param_AO.S*(RA(0,0)*Param_inde.Xl + RA(0,1)*Param_inde.Yl + RA(0,2)*Param_inde.Zl) + Param_AO.T(0,0);
		Param_AO.Yl = Param_AO.S*(RA(1,0)*Param_inde.Xl + RA(1,1)*Param_inde.Yl + RA(1,2)*Param_inde.Zl) + Param_AO.T(1,0);
		Param_AO.Zl = Param_AO.S*(RA(2,0)*Param_inde.Xl + RA(2,1)*Param_inde.Yl + RA(2,2)*Param_inde.Zl) + Param_AO.T(2,0);
		
		Param_AO.Xr = Param_AO.S*(RA(0,0)*Param_inde.Xr + RA(0,1)*Param_inde.Yr + RA(0,2)*Param_inde.Zr) + Param_AO.T(0,0);
		Param_AO.Yr = Param_AO.S*(RA(1,0)*Param_inde.Xr + RA(1,1)*Param_inde.Yr + RA(1,2)*Param_inde.Zr) + Param_AO.T(1,0);
		Param_AO.Zr = Param_AO.S*(RA(2,0)*Param_inde.Xr + RA(2,1)*Param_inde.Yr + RA(2,2)*Param_inde.Zr) + Param_AO.T(2,0);
	}
	else
	{
		return FALSE;
	}
	
	CRotationcoeff LRM(Lo,Lp,Lk), RRM(Ro,Rp,Rk);
	Matrix<double> LRA, RRA;
	LRA = LRM.Rmatrix%RAT.Rmatrix;
	RRA = RRM.Rmatrix%RAT.Rmatrix;
	
	//Param_AO.Lomega = atan2(-LRA(2,1),LRA(2,2));
	//Param_AO.Lphi = asin(LRA(2,0));
	//Param_AO.Lkappa = atan2(-LRA(1,0),LRA(0,0));
	
	//Param_AO.Romega = atan2(-RRA(2,1),RRA(2,2));
	//Param_AO.Rphi = asin(RRA(2,0));
	//Param_AO.Rkappa = atan2(-RRA(1,0),RRA(0,0));
	
	Param_AO.Lphi   = asin(LRA(2,0)); //note: two choices here
	Param_AO.Lomega = atan2(-LRA(2,1)/cos(Param_AO.Lphi),LRA(2,2)/cos(Param_AO.Lphi));
	Param_AO.Lkappa = atan2(-LRA(1,0)/cos(Param_AO.Lphi),LRA(0,0)/cos(Param_AO.Lphi));
		
	Param_AO.Rphi   = asin(RRA(2,0)); //note: two choices here
	Param_AO.Romega = atan2(-RRA(2,1)/cos(Param_AO.Rphi),RRA(2,2)/cos(Param_AO.Rphi));
	Param_AO.Rkappa = atan2(-RRA(1,0)/cos(Param_AO.Rphi),RRA(0,0)/cos(Param_AO.Rphi));
	
	return TRUE;
}