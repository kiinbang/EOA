//////////////////////////
//SpacematicsPhoto.cpp
//made by BbaraB
//revision date 2000-6-15
//revision date 2001-03-09
//revision date 2001-03-19
//revision date 2001-04-30 photo class 복사생성자, 대입연산자 추가
//////////////////////////

#include "stdafx.h"
#include "SpacematicsPhoto.h"

//////////
//Photo
//////////
CPhoto::CPhoto()
{
	IO_Complete = false;
	fiducial_image = NULL;
	FlightHeight = 0.0;
	ID = -1;
	Name = "";
	IO_Option = 2;
	num_ROP = 0;
	IO_ImageToPhoto_Transform = NULL;
	IO_PhotoToImage_Transform = NULL;
}

CPhoto::CPhoto(CPhoto &copy)
{
	int i;
	IO_Complete = copy.IO_Complete;
	camera = copy.camera;
	fiducial_image = NULL;
	fiducial_image = new Point2D<double>[camera.GetNumFM()];
	for(i=0;i<camera.GetNumFM();i++)
		fiducial_image[i] = copy.fiducial_image[i];
	FlightHeight = copy.FlightHeight;
	ID = copy.ID;
	IO_Option = copy.IO_Option;
	if(IO_Complete == true)
		IO(IO_Option);
	num_ROP = copy.num_ROP;
	ROPoint = copy.ROPoint;
}

void CPhoto::operator = (CPhoto& copy)
{
	int i;
	IO_Complete = copy.IO_Complete;
	camera = copy.camera;
	fiducial_image = NULL;
	if(fiducial_image != NULL)
	{
		delete[] fiducial_image;
		fiducial_image = NULL;
	}
	fiducial_image = new Point2D<double>[camera.GetNumFM()];
	for(i=0;i<camera.GetNumFM();i++)
		fiducial_image[i] = copy.fiducial_image[i];
	FlightHeight = copy.FlightHeight;
	ID = copy.ID;
	IO_Option = copy.IO_Option;
	if(IO_Complete == true)
		IO(IO_Option);
	num_ROP = copy.num_ROP;
	ROPoint = copy.ROPoint;
}

CPhoto::~CPhoto()
{
	if(fiducial_image != NULL)
	{
		delete[] fiducial_image;
		fiducial_image = NULL;
	}
}

bool CPhoto::IsIOComplete(void)
{
	return IO_Complete;
}

//int CPhoto::IO(IO_Option option)
bool CPhoto::IO(int option)
{
	int i;
	IO_Option = option;
	//1: conformal 2: affine 3: bilenear(pseudoaffine)
	//4: linearprojective 5: nonlinearprojective
	
	KnownPoint fm(camera.GetNumFM());
	KnownPoint fm_inv(camera.GetNumFM());
	for(i=0;i<camera.GetNumFM();i++)
	{
		fm.SetPoint(i,fiducial_image[i].x,fiducial_image[i].y,
			          camera.fiducial[i].x,camera.fiducial[i].y);
		fm_inv.SetPoint(i,camera.fiducial[i].x,camera.fiducial[i].y,
			           fiducial_image[i].x,fiducial_image[i].y);
	}

	switch(option)
	{
	case CONFORMAL:
		if(!IO_Conformal(fm, fm_inv)) return false;
		break;
	case AFFINE:
		if(!IO_Affine(fm, fm_inv)) return false;		
		break;
	case BILINEAR:
		if(!IO_Bilinear(fm, fm_inv)) return false;
		break;
	case LINEARPROJECTIVE:
		if(!IO_LinearProjective(fm, fm_inv)) return false;
		break;
	case NONLINEARPROJECTIVE:
		if(!IO_NonLinearProjective(fm, fm_inv)) return false;
		break;
	default:
		if(!IO_Affine(fm, fm_inv)) return false;
		break;
	}

	IO_Complete = true;

	return true;
}

bool CPhoto::IO_Conformal(KnownPoint& FM, KnownPoint& FM_inv)
{
	if(FM.num >= 2)
	{
		conformal.Conformal_Transform(FM);
		IO_ImageToPhoto_Transform = &conformal;
		conformal_inv.Conformal_Transform(FM_inv);
		IO_PhotoToImage_Transform = &conformal_inv;
	}
	else
	{
		return false;
	}
	return true;
}

bool CPhoto::IO_Affine(KnownPoint& FM, KnownPoint& FM_inv)
{
	if(FM.num >= 3)
	{
		affine.Affine_Transform(FM);
		IO_ImageToPhoto_Transform = &affine;
		affine_inv.Affine_Transform(FM_inv);
		IO_PhotoToImage_Transform = &affine_inv;
	}
	else
	{
		return false;
	}
	return true;
}

bool CPhoto::IO_Bilinear(KnownPoint& FM, KnownPoint& FM_inv)
{
	if(FM.num >= 4)
	{
		bilinear.Bilinear_Transform(FM);
		IO_ImageToPhoto_Transform = &bilinear;
		bilinear_inv.Bilinear_Transform(FM_inv);
		IO_PhotoToImage_Transform = &bilinear_inv;
	}
	else
	{
		return false;
	}
	return true;
}

bool CPhoto::IO_LinearProjective(KnownPoint& FM, KnownPoint& FM_inv)
{
	if(FM.num >= 4)
	{
		projective_linear.Projective_linear_Transform(FM);
		IO_ImageToPhoto_Transform = &projective_linear;
		projective_linear_inv.Projective_linear_Transform(FM_inv);
		IO_PhotoToImage_Transform = &projective_linear_inv;
	}
	else
	{
		return false;
	}
	return true;
}

bool CPhoto::IO_NonLinearProjective(KnownPoint& FM, KnownPoint& FM_inv)
{
	if(FM.num >= 4)
	{
		projective_nonlinear.Projective_nonlinear_Transform(FM);
		IO_ImageToPhoto_Transform = &projective_nonlinear;
		projective_nonlinear_inv.Projective_nonlinear_Transform(FM_inv);
		IO_PhotoToImage_Transform = &projective_nonlinear_inv;
	}
	else
	{
		return false;
	}

	return true;
}

void CPhoto::SetCamera(CCamera C)
{
	camera = C;

	if(fiducial_image != NULL)
	{
		delete[] fiducial_image;
		fiducial_image = NULL;
	}

	fiducial_image = new Point2D<double>[camera.GetNumFM()];
}

void CPhoto::SetFlightHeight(double FH)
{
	FlightHeight = FH;
}

void CPhoto::SetCameraName(CSMStr name)
{
	camera.Name = name;
}

void CPhoto::SetPhotoID(int id)
{
	ID = id;
}

bool CPhoto::SetFiducialImageCoord(int index, Point2D<double> fiducial)
{
	if(index < camera.GetNumFM())
	{
		fiducial_image[index] = fiducial;
		return true;
	}
	else
	{
		return false;
	}
}

CCamera CPhoto::GetCamera(void)
{
	return camera;
}

double CPhoto::GetFlightHeight(void)
{
	return FlightHeight;
}

CSMStr CPhoto::GetCameraName(void)
{
	return camera.Name;
}

int CPhoto::GetPhotoID(void)
{
	return ID;
}

int CPhoto::GetIOOption(void)
{
	return IO_Option;
}

Point2D<double> CPhoto::GetFiducial(int index)
{
	return fiducial_image[index];
}

Point2D<double> CPhoto::GetImageToPhotoCoord(Point2D<double> imagecoord)
{
	Point2D<double> photocoord;
	photocoord = IO_ImageToPhoto_Transform->CoordTransform(imagecoord);
	photocoord.x -= camera.PPA.x;
	photocoord.y -= camera.PPA.y;
	
	if(camera.LensDistortionEnable == true)
	{
		double r,r3,r5,r7,dr;
		r = sqrt(photocoord.x*photocoord.x + photocoord.y*photocoord.y);
		r3 = r*r*r;
		r5 = r3*r*r;
		r7 = r5*r*r;
		dr = camera.k1*r + camera.k2*r3 + camera.k3*r5 + camera.k4*r7;//(positive values denote image displacement away from center(PPBS))
		double dr_r = 1 - (dr/r);
		photocoord.x = photocoord.x*dr_r;
		photocoord.y = photocoord.y*dr_r;
	}

	if(camera.SMACEnable == true)
	{
		double r2,r4,r6,r8,dr;
		double dp;
		double xy;

		r2 = photocoord.x*photocoord.x + photocoord.y*photocoord.y;
		r4 = r2*r2;
		r6 = r4*r2;
		r8 = r4*r4;
		dr = camera.smac.k0
			+camera.smac.k1*r2
			+camera.smac.k2*r4
			+camera.smac.k3*r6
			+camera.smac.k4*r8;
		dp = 1
			+camera.smac.p3*r2
			+camera.smac.p4*r4;

		xy = photocoord.x*photocoord.y;

		photocoord.x = photocoord.x*(1+dr)
			+dp*(camera.smac.p1*(r2+2*photocoord.x*photocoord.x)+2*camera.smac.p2*xy);
		photocoord.y = photocoord.y*(1+dr)
			+dp*(2*camera.smac.p1*xy+camera.smac.p2*(r2+2*photocoord.y*photocoord.y));
	}

	return photocoord;
}

Point2D<double> CPhoto::GetPhotoToImageCoord(Point2D<double> photocoord)
{
	Point2D<double> imagecoord;

	if(camera.LensDistortionEnable == true)
	{
		double r,r3,r5,r7,dr;
		r = sqrt(photocoord.x*photocoord.x + photocoord.y*photocoord.y);
		r3 = r*r*r;
		r5 = r3*r*r;
		r7 = r5*r*r;
		dr = camera.k1*r + camera.k2*r3 + camera.k3*r5 + camera.k4*r7;
		double dr_r = 1 + (dr/r);
		photocoord.x = photocoord.x*dr_r;
		photocoord.y = photocoord.y*dr_r;
	}
	
	if(camera.SMACEnable == true)
	{
		double r2,r4,r6,r8,dr;
		double dp;
		double xy;

		r2 = photocoord.x*photocoord.x + photocoord.y*photocoord.y;
		r4 = r2*r2;
		r6 = r4*r2;
		r8 = r4*r4;
		dr = camera.smac.k0
			+camera.smac.k1*r2
			+camera.smac.k2*r4
			+camera.smac.k3*r6
			+camera.smac.k4*r8;
		dp = 1
			+camera.smac.p3*r2
			+camera.smac.p4*r4;

		xy = photocoord.x*photocoord.y;

		photocoord.x = photocoord.x*(1+dr)
			+dp*(camera.smac.p1*(r2+2*photocoord.x*photocoord.x)+2*camera.smac.p2*xy);
		photocoord.y = photocoord.y*(1+dr)
			+dp*(2*camera.smac.p1*xy+camera.smac.p2*(r2+2*photocoord.y*photocoord.y));
	}
	
	photocoord.x += camera.PPA.x;
	photocoord.y += camera.PPA.y;
	imagecoord = IO_PhotoToImage_Transform->CoordTransform(photocoord);
	return imagecoord;
}

bool CPhoto::SetImagePoint(int index, int id, int property, double x, double y)
{
	if(index<num_ImagePoint)
	{
		m_ImagePoint(index, 0) = (double)id;
		m_ImagePoint(index, 1) = (double)property;
		m_ImagePoint(index, 2) = x;
		m_ImagePoint(index, 3) = y;

		return true;
	}
	else
	{
		return false;
	}
}

bool CPhoto::ReadFiducialMarkFile(CString fname)
{
	char title[128];
	int num_fm;

	fstream fmfile;
	fmfile.open(fname, ios::in);

	fmfile>>title>>num_fm;

	if(camera.GetNumFM() !=  num_fm) return false;

	for(int i=0; i<num_fm; i++)
	{
		Point2D<double> fm;
		fmfile>>fm.x>>fm.y;
		this->SetFiducialImageCoord(i, fm);
	}

	return true;
}
