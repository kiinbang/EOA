// GCPWrap.h: interface for the CGCPWrap class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GCPWRAP_H__E1FE3706_2409_4B59_B207_3EF946794367__INCLUDED_)
#define AFX_GCPWRAP_H__E1FE3706_2409_4B59_B207_3EF946794367__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGCPWrap  
{
public:
	CGCPWrap();
	virtual ~CGCPWrap();

	//Get GCP interface
	VARIANT GetGCPInterface(void);

	//Set GCP interface
	bool SetGCPInterface(VARIANT gcp);

	//GCP Insert
	bool InsertGCP(double X, double Y, double Z, double Col, double Row, long ID);
	
	//Get GCP Number
	long GetGCPNum();

	//Get GCP Coord by ID
	bool GetGCPbyID(long ID, double *X, double *Y, double *Z, double *Col, double *Row);

	//Get GCP Coord by Sort
	bool GetGCPbySort(long sort, double *X, double *Y, double *Z, double *Col, double *Row);

	//Get GCP Coord by Sort
	bool GetGCPbySort(long sort, long *ID, double *X, double *Y, double *Z, double *Col, double *Row);

	//GCP Delete by ID
	bool DeleteGCPbyID(long ID);

	//GCP Delete by sort number
	bool DeleteGCPbySort(long sort);

	//Empty GCP
	bool EmptyGCP(void);

private:

	IGCPsPtr m_GCPs;

};

#endif // !defined(AFX_GCPWRAP_H__E1FE3706_2409_4B59_B207_3EF946794367__INCLUDED_)
