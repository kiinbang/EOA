#pragma once

#include <stdio.h>

#define FlightDateJulian FileCreationDayOfYear 
#define Year FileCreationYear

enum ErrorMsg {
	ERROR_FREE,
	HEADER_READ_ERROR,
	LASFILE_OPEN_ERROR,
	VERSION_READ_ERROR,
	NOT_SUPPORTED_VERSION,
	SCALEFACTOR_ERROR,
	NOT_SUPPORTED_FORMAT,
	READ_POINTDATA_ERROR,
	OUT_OF_POINT_NUMBER,
	VLR_READ_ERROR,
	LASFILE_WRITE_OPEN_ERROR,
	POINT_START_SIGN_READ_ERROR,
	HEADER_WRITE_ERROR,
	DIFFERENT_VERSION_ERROR,
	DIFFERENT_TYPE_ERROR,
	POINT_INDEX_ERROR
};

class LASHeader_10
{
public:
	char			FileSignature[4];
	unsigned long	Reserved;
	unsigned long	GUIDdata_1;
	unsigned short	GUIDdata_2;
	unsigned short	GUIDdata_3;
	unsigned char	GUIDdata_4[8];
	unsigned char	VersionMajor;
	unsigned char	VersionMinor;
	char			SystemIdentifier[32];
	char			GeneratingSoftware[32];
	unsigned short	FileCreationDayOfYear;
	unsigned short	FileCreationYear;
	unsigned short	HeaderSize;
	unsigned long	OffsettoData;
	unsigned long	NumberOfVariableLengthRecords;
	unsigned char	PointDataFormatID;
	unsigned short	PointDataRecordLength;
	unsigned long	NumberOfPointRecords;
	unsigned long	NumberOfPointsbyReturn[5];
	double			ScaleFactor_X;
	double			ScaleFactor_Y;
	double			ScaleFactor_Z;
	double			Offset_X;
	double			Offset_Y;
	double			Offset_Z;
	double			Max_X;
	double			Min_X;
	double			Max_Y;
	double			Min_Y;
	double			Max_Z;
	double			Min_Z;
public:
	LASHeader_10() {}

	LASHeader_10(LASHeader_10 &copy) { DataCopy(copy); }

	LASHeader_10& operator = (LASHeader_10 &copy) { DataCopy(copy); return (*this); }

	void WriteHeader(CFile &lasfile)
	{
		lasfile.Write((void*)FileSignature, 4);
		lasfile.Write((void*)&Reserved, 4);
		lasfile.Write((void*)&GUIDdata_1, 4);
		lasfile.Write((void*)&GUIDdata_2, 2);
		lasfile.Write((void*)&GUIDdata_3, 2);
		lasfile.Write((void*)GUIDdata_4, 8);
		lasfile.Write((void*)&VersionMajor, 1);
		lasfile.Write((void*)&VersionMinor, 1);
		lasfile.Write((void*)SystemIdentifier, 32);
		lasfile.Write((void*)GeneratingSoftware, 32);
		lasfile.Write((void*)&FileCreationDayOfYear, 2);
		lasfile.Write((void*)&Year, 2);
		lasfile.Write((void*)&HeaderSize, 2);
		lasfile.Write((void*)&OffsettoData, 4);
		lasfile.Write((void*)&NumberOfVariableLengthRecords, 4);
		lasfile.Write((void*)&PointDataFormatID, 1);//0-99 for spec
		lasfile.Write((void*)&PointDataRecordLength, 2);
		lasfile.Write((void*)&NumberOfPointRecords, 4);
		lasfile.Write((void*)NumberOfPointsbyReturn, 20);
		lasfile.Write((void*)&ScaleFactor_X, 8);
		lasfile.Write((void*)&ScaleFactor_Y, 8);
		lasfile.Write((void*)&ScaleFactor_Z, 8);
		lasfile.Write((void*)&Offset_X, 8);
		lasfile.Write((void*)&Offset_Y, 8);
		lasfile.Write((void*)&Offset_Z, 8);
		lasfile.Write((void*)&Max_X, 8);
		lasfile.Write((void*)&Min_X, 8);
		lasfile.Write((void*)&Max_Y, 8);
		lasfile.Write((void*)&Min_Y, 8);
		lasfile.Write((void*)&Max_Z, 8);
		lasfile.Write((void*)&Min_Z, 8);
	}

	ErrorMsg ReadHeader(CFile &lasfile)
	{
		lasfile.SeekToBegin();

		if (!lasfile.Read((void*)FileSignature, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Reserved, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_1, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_2, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_3, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)GUIDdata_4, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&VersionMajor, 1))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&VersionMinor, 1))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)SystemIdentifier, 32))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)GeneratingSoftware, 32))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileCreationDayOfYear, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Year, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&HeaderSize, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&OffsettoData, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&NumberOfVariableLengthRecords, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&PointDataFormatID, 1))		return HEADER_READ_ERROR;//0-99 for spec
		if (!lasfile.Read((void*)&PointDataRecordLength, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&NumberOfPointRecords, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)NumberOfPointsbyReturn, 20))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_Z, 8))		return HEADER_READ_ERROR;

		return ERROR_FREE;
	}

	void DataCopy(LASHeader_10 &copy)
	{
		memcpy(FileSignature, copy.FileSignature, 4);
		Reserved = copy.Reserved;
		GUIDdata_1 = copy.GUIDdata_1;
		GUIDdata_2 = copy.GUIDdata_2;
		GUIDdata_3 = copy.GUIDdata_3;
		memcpy(GUIDdata_4, copy.GUIDdata_4, 8);
		VersionMajor = copy.VersionMajor;
		VersionMinor = copy.VersionMinor;
		memcpy(SystemIdentifier, copy.SystemIdentifier, 32);
		memcpy(GeneratingSoftware, copy.GeneratingSoftware, 32);
		FileCreationDayOfYear = copy.FileCreationDayOfYear;
		Year = copy.Year;
		HeaderSize = copy.HeaderSize;
		OffsettoData = copy.OffsettoData;
		NumberOfVariableLengthRecords = copy.NumberOfVariableLengthRecords;
		PointDataFormatID = copy.PointDataFormatID;
		PointDataRecordLength = copy.PointDataRecordLength;
		NumberOfPointRecords = copy.NumberOfPointRecords;
		memcpy(NumberOfPointsbyReturn, copy.NumberOfPointsbyReturn, sizeof(unsigned long) * 5);
		ScaleFactor_X = copy.ScaleFactor_X;
		ScaleFactor_Y = copy.ScaleFactor_Y;
		ScaleFactor_Z = copy.ScaleFactor_Z;
		Offset_X = copy.Offset_X;
		Offset_Y = copy.Offset_Y;
		Offset_Z = copy.Offset_Z;
		Max_X = copy.Max_X;
		Min_X = copy.Min_X;
		Max_Y = copy.Max_Y;
		Min_Y = copy.Min_Y;
		Max_Z = copy.Max_Z;
		Min_Z = copy.Min_Z;
	}
};

class LASHeader_11
{
public:
	char			FileSignature[4];
	unsigned short	FileSourceID;
	unsigned short	Reserved;
	unsigned long	GUIDdata_1;
	unsigned short	GUIDdata_2;
	unsigned short	GUIDdata_3;
	unsigned char	GUIDdata_4[8];
	unsigned char	VersionMajor;
	unsigned char	VersionMinor;
	char			SystemIdentifier[32];
	char			GeneratingSoftware[32];
	unsigned short	FileCreationDayOfYear;
	unsigned short	FileCreationYear;
	unsigned short	HeaderSize;
	unsigned long	OffsettoData;
	unsigned long	NumberOfVariableLengthRecords;
	unsigned char	PointDataFormatID;
	unsigned short	PointDataRecordLength;
	unsigned long	NumberOfPointRecords;
	unsigned long	NumberOfPointsbyReturn[5];
	double			ScaleFactor_X;
	double			ScaleFactor_Y;
	double			ScaleFactor_Z;
	double			Offset_X;
	double			Offset_Y;
	double			Offset_Z;
	double			Max_X;
	double			Min_X;
	double			Max_Y;
	double			Min_Y;
	double			Max_Z;
	double			Min_Z;
public:
	LASHeader_11() {}

	LASHeader_11(LASHeader_11 &copy) { DataCopy(copy); }

	LASHeader_11& operator = (LASHeader_11 &copy) { DataCopy(copy); return (*this); }

	void WriteHeader(CFile &lasfile)
	{
		lasfile.Write((void*)FileSignature, 4);
		lasfile.Write((void*)&FileSourceID, 2);
		lasfile.Write((void*)&Reserved, 4);
		lasfile.Write((void*)&GUIDdata_1, 4);
		lasfile.Write((void*)&GUIDdata_2, 2);
		lasfile.Write((void*)&GUIDdata_3, 2);
		lasfile.Write((void*)GUIDdata_4, 8);
		lasfile.Write((void*)&VersionMajor, 1);
		lasfile.Write((void*)&VersionMinor, 1);
		lasfile.Write((void*)SystemIdentifier, 32);
		lasfile.Write((void*)GeneratingSoftware, 32);
		lasfile.Write((void*)&FileCreationDayOfYear, 2);
		lasfile.Write((void*)&FileCreationYear, 2);
		lasfile.Write((void*)&HeaderSize, 2);
		lasfile.Write((void*)&OffsettoData, 4);
		lasfile.Write((void*)&NumberOfVariableLengthRecords, 4);
		lasfile.Write((void*)&PointDataFormatID, 1);//0-99 for spec
		lasfile.Write((void*)&PointDataRecordLength, 2);
		lasfile.Write((void*)&NumberOfPointRecords, 4);
		lasfile.Write((void*)NumberOfPointsbyReturn, 20);
		lasfile.Write((void*)&ScaleFactor_X, 8);
		lasfile.Write((void*)&ScaleFactor_Y, 8);
		lasfile.Write((void*)&ScaleFactor_Z, 8);
		lasfile.Write((void*)&Offset_X, 8);
		lasfile.Write((void*)&Offset_Y, 8);
		lasfile.Write((void*)&Offset_Z, 8);
		lasfile.Write((void*)&Max_X, 8);
		lasfile.Write((void*)&Min_X, 8);
		lasfile.Write((void*)&Max_Y, 8);
		lasfile.Write((void*)&Min_Y, 8);
		lasfile.Write((void*)&Max_Z, 8);
		lasfile.Write((void*)&Min_Z, 8);
	}

	ErrorMsg ReadHeader(CFile &lasfile)
	{
		lasfile.SeekToBegin();

		if (!lasfile.Read((void*)FileSignature, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileSourceID, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Reserved, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_1, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_2, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_3, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)GUIDdata_4, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&VersionMajor, 1))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&VersionMinor, 1))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)SystemIdentifier, 32))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)GeneratingSoftware, 32))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileCreationDayOfYear, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileCreationYear, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&HeaderSize, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&OffsettoData, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&NumberOfVariableLengthRecords, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&PointDataFormatID, 1))		return HEADER_READ_ERROR;//0-99 for spec
		if (!lasfile.Read((void*)&PointDataRecordLength, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&NumberOfPointRecords, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)NumberOfPointsbyReturn, 20))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_Z, 8))		return HEADER_READ_ERROR;

		return ERROR_FREE;
	}

	void DataCopy(LASHeader_11 &copy)
	{
		memcpy(FileSignature, copy.FileSignature, 4);
		FileSourceID = copy.FileSourceID;
		Reserved = copy.Reserved;
		GUIDdata_1 = copy.GUIDdata_1;
		GUIDdata_2 = copy.GUIDdata_2;
		GUIDdata_3 = copy.GUIDdata_3;
		memcpy(GUIDdata_4, copy.GUIDdata_4, 8);
		VersionMajor = copy.VersionMajor;
		VersionMinor = copy.VersionMinor;
		memcpy(SystemIdentifier, copy.SystemIdentifier, 32);
		memcpy(GeneratingSoftware, copy.GeneratingSoftware, 32);
		FileCreationDayOfYear = copy.FileCreationDayOfYear;
		FileCreationYear = copy.FileCreationYear;
		HeaderSize = copy.HeaderSize;
		OffsettoData = copy.OffsettoData;
		NumberOfVariableLengthRecords = copy.NumberOfVariableLengthRecords;
		PointDataFormatID = copy.PointDataFormatID;
		PointDataRecordLength = copy.PointDataRecordLength;
		NumberOfPointRecords = copy.NumberOfPointRecords;
		memcpy(NumberOfPointsbyReturn, copy.NumberOfPointsbyReturn, sizeof(unsigned long) * 5);
		ScaleFactor_X = copy.ScaleFactor_X;
		ScaleFactor_Y = copy.ScaleFactor_Y;
		ScaleFactor_Z = copy.ScaleFactor_Z;
		Offset_X = copy.Offset_X;
		Offset_Y = copy.Offset_Y;
		Offset_Z = copy.Offset_Z;
		Max_X = copy.Max_X;
		Min_X = copy.Min_X;
		Max_Y = copy.Max_Y;
		Min_Y = copy.Min_Y;
		Max_Z = copy.Max_Z;
		Min_Z = copy.Min_Z;
	}
};

class LASHeader_12
{
public:
	char			FileSignature[4];
	unsigned short	FileSourceID;
	unsigned short	GlobalEncoding;
	unsigned long	GUIDdata_1;
	unsigned short	GUIDdata_2;
	unsigned short	GUIDdata_3;
	unsigned char	GUIDdata_4[8];
	unsigned char	VersionMajor;
	unsigned char	VersionMinor;
	char			SystemIdentifier[32];
	char			GeneratingSoftware[32];
	unsigned short	FileCreationDayOfYear;
	unsigned short	FileCreationYear;
	unsigned short	HeaderSize;
	unsigned long	OffsettoData;
	unsigned long	NumberOfVariableLengthRecords;
	unsigned char	PointDataFormatID;//0-99 for spec
	unsigned short	PointDataRecordLength;
	unsigned long	NumberOfPointRecords;
	unsigned long	NumberOfPointsbyReturn[5];
	double			ScaleFactor_X;
	double			ScaleFactor_Y;
	double			ScaleFactor_Z;
	double			Offset_X;
	double			Offset_Y;
	double			Offset_Z;
	double			Max_X;
	double			Min_X;
	double			Max_Y;
	double			Min_Y;
	double			Max_Z;
	double			Min_Z;
public:
	LASHeader_12() {}

	LASHeader_12(LASHeader_12 &copy) { DataCopy(copy); }

	LASHeader_12& operator = (LASHeader_12 &copy) { DataCopy(copy); return (*this); }

	void WriteHeader(CFile &lasfile)
	{
		lasfile.Write((void*)FileSignature, 4);
		lasfile.Write((void*)&FileSourceID, 2);
		lasfile.Write((void*)&GlobalEncoding, 2);
		lasfile.Write((void*)&GUIDdata_1, 4);
		lasfile.Write((void*)&GUIDdata_2, 2);
		lasfile.Write((void*)&GUIDdata_3, 2);
		lasfile.Write((void*)GUIDdata_4, 8);
		lasfile.Write((void*)&VersionMajor, 1);
		lasfile.Write((void*)&VersionMinor, 1);
		lasfile.Write((void*)SystemIdentifier, 32);
		lasfile.Write((void*)GeneratingSoftware, 32);
		lasfile.Write((void*)&FileCreationDayOfYear, 2);
		lasfile.Write((void*)&FileCreationYear, 2);
		lasfile.Write((void*)&HeaderSize, 2);
		lasfile.Write((void*)&OffsettoData, 4);
		lasfile.Write((void*)&NumberOfVariableLengthRecords, 4);
		lasfile.Write((void*)&PointDataFormatID, 1);//0-99 for spec
		lasfile.Write((void*)&PointDataRecordLength, 2);
		lasfile.Write((void*)&NumberOfPointRecords, 4);
		lasfile.Write((void*)NumberOfPointsbyReturn, 20);
		lasfile.Write((void*)&ScaleFactor_X, 8);
		lasfile.Write((void*)&ScaleFactor_Y, 8);
		lasfile.Write((void*)&ScaleFactor_Z, 8);
		lasfile.Write((void*)&Offset_X, 8);
		lasfile.Write((void*)&Offset_Y, 8);
		lasfile.Write((void*)&Offset_Z, 8);
		lasfile.Write((void*)&Max_X, 8);
		lasfile.Write((void*)&Min_X, 8);
		lasfile.Write((void*)&Max_Y, 8);
		lasfile.Write((void*)&Min_Y, 8);
		lasfile.Write((void*)&Max_Z, 8);
		lasfile.Write((void*)&Min_Z, 8);
	}

	ErrorMsg ReadHeader(CFile &lasfile)
	{
		lasfile.SeekToBegin();

		//if(!lasfile.Open(m_stInfilePath, CFile::modeRead|CFile::shareDenyRead))
		//	return false;

		if (!lasfile.Read((void*)FileSignature, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileSourceID, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GlobalEncoding, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_1, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_2, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_3, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)GUIDdata_4, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&VersionMajor, 1))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&VersionMinor, 1))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)SystemIdentifier, 32))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)GeneratingSoftware, 32))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileCreationDayOfYear, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileCreationYear, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&HeaderSize, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&OffsettoData, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&NumberOfVariableLengthRecords, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&PointDataFormatID, 1))		return HEADER_READ_ERROR;//0-99 for spec
		if (!lasfile.Read((void*)&PointDataRecordLength, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&NumberOfPointRecords, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)NumberOfPointsbyReturn, 20))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_Z, 8))		return HEADER_READ_ERROR;

		return ERROR_FREE;
	}

	void DataCopy(LASHeader_12 &copy)
	{
		memcpy(FileSignature, copy.FileSignature, 4);
		FileSourceID = copy.FileSourceID;
		GlobalEncoding = copy.GlobalEncoding;
		GUIDdata_1 = copy.GUIDdata_1;
		GUIDdata_2 = copy.GUIDdata_2;
		GUIDdata_3 = copy.GUIDdata_3;
		memcpy(GUIDdata_4, copy.GUIDdata_4, 8);
		VersionMajor = copy.VersionMajor;
		VersionMinor = copy.VersionMinor;
		memcpy(SystemIdentifier, copy.SystemIdentifier, 32);
		memcpy(GeneratingSoftware, copy.GeneratingSoftware, 32);
		FileCreationDayOfYear = copy.FileCreationDayOfYear;
		FileCreationYear = copy.FileCreationYear;
		HeaderSize = copy.HeaderSize;
		OffsettoData = copy.OffsettoData;
		NumberOfVariableLengthRecords = copy.NumberOfVariableLengthRecords;
		PointDataFormatID = copy.PointDataFormatID;
		PointDataRecordLength = copy.PointDataRecordLength;
		NumberOfPointRecords = copy.NumberOfPointRecords;
		memcpy(NumberOfPointsbyReturn, copy.NumberOfPointsbyReturn, sizeof(unsigned long) * 5);
		ScaleFactor_X = copy.ScaleFactor_X;
		ScaleFactor_Y = copy.ScaleFactor_Y;
		ScaleFactor_Z = copy.ScaleFactor_Z;
		Offset_X = copy.Offset_X;
		Offset_Y = copy.Offset_Y;
		Offset_Z = copy.Offset_Z;
		Max_X = copy.Max_X;
		Min_X = copy.Min_X;
		Max_Y = copy.Max_Y;
		Min_Y = copy.Min_Y;
		Max_Z = copy.Max_Z;
		Min_Z = copy.Min_Z;
	}
};

class LASHeader_14
{
public:
	char			FileSignature[4];
	unsigned short	FileSourceID;
	unsigned short	GlobalEncoding;
	unsigned long	GUIDdata_1;
	unsigned short	GUIDdata_2;
	unsigned short	GUIDdata_3;
	unsigned char	GUIDdata_4[8];
	unsigned char	VersionMajor;
	unsigned char	VersionMinor;
	char			SystemIdentifier[32];
	char			GeneratingSoftware[32];
	unsigned short	FileCreationDayOfYear;
	unsigned short	FileCreationYear;
	unsigned short	HeaderSize;
	unsigned long	OffsettoData;
	unsigned long	NumberOfVariableLengthRecords;
	unsigned char	PointDataFormatID;//0-99 for spec
	unsigned short	PointDataRecordLength;
	unsigned long	LegacyNumberOfPointRecords;
	unsigned long	LegacyNumberOfPointsbyReturn[5];
	double			ScaleFactor_X;
	double			ScaleFactor_Y;
	double			ScaleFactor_Z;
	double			Offset_X;
	double			Offset_Y;
	double			Offset_Z;
	double			Max_X;
	double			Min_X;
	double			Max_Y;
	double			Min_Y;
	double			Max_Z;
	double			Min_Z;
	//Added for VER 14
	unsigned long long	StartWaveformDataPacketRecord;
	unsigned long long	StartFirstExtendedVariableLengthRecord;
	unsigned long		NumberOfExtendedVariableLengthRecords;
	unsigned long long	NumberOfPointRecords;
	unsigned long long	NumberOfPointsbyReturn[15];
public:
	LASHeader_14() {}

	LASHeader_14(LASHeader_14 &copy) { DataCopy(copy); }

	LASHeader_14& operator = (LASHeader_14 &copy) { DataCopy(copy); return (*this); }

	void WriteHeader(CFile &lasfile)
	{
		lasfile.Write((void*)FileSignature, 4);
		lasfile.Write((void*)&FileSourceID, 2);
		lasfile.Write((void*)&GlobalEncoding, 2);
		lasfile.Write((void*)&GUIDdata_1, 4);
		lasfile.Write((void*)&GUIDdata_2, 2);
		lasfile.Write((void*)&GUIDdata_3, 2);
		lasfile.Write((void*)GUIDdata_4, 8);
		lasfile.Write((void*)&VersionMajor, 1);
		lasfile.Write((void*)&VersionMinor, 1);
		lasfile.Write((void*)SystemIdentifier, 32);
		lasfile.Write((void*)GeneratingSoftware, 32);
		lasfile.Write((void*)&FileCreationDayOfYear, 2);
		lasfile.Write((void*)&FileCreationYear, 2);
		lasfile.Write((void*)&HeaderSize, 2);
		lasfile.Write((void*)&OffsettoData, 4);
		lasfile.Write((void*)&NumberOfVariableLengthRecords, 4);
		lasfile.Write((void*)&PointDataFormatID, 1);//0-99 for spec
		lasfile.Write((void*)&PointDataRecordLength, 2);
		lasfile.Write((void*)&LegacyNumberOfPointRecords, 4);
		lasfile.Write((void*)LegacyNumberOfPointsbyReturn, 20);
		lasfile.Write((void*)&ScaleFactor_X, 8);
		lasfile.Write((void*)&ScaleFactor_Y, 8);
		lasfile.Write((void*)&ScaleFactor_Z, 8);
		lasfile.Write((void*)&Offset_X, 8);
		lasfile.Write((void*)&Offset_Y, 8);
		lasfile.Write((void*)&Offset_Z, 8);
		lasfile.Write((void*)&Max_X, 8);
		lasfile.Write((void*)&Min_X, 8);
		lasfile.Write((void*)&Max_Y, 8);
		lasfile.Write((void*)&Min_Y, 8);
		lasfile.Write((void*)&Max_Z, 8);
		lasfile.Write((void*)&Min_Z, 8);
		//Added for VER 14
		lasfile.Write((void*)&StartWaveformDataPacketRecord, 8);
		lasfile.Write((void*)&StartFirstExtendedVariableLengthRecord, 8);
		lasfile.Write((void*)&NumberOfExtendedVariableLengthRecords, 4);
		lasfile.Write((void*)&NumberOfPointRecords, 8);
		lasfile.Write((void*)NumberOfPointsbyReturn, 120);
	}

	ErrorMsg ReadHeader(CFile &lasfile)
	{
		lasfile.SeekToBegin();

		//if(!lasfile.Open(m_stInfilePath, CFile::modeRead|CFile::shareDenyRead))
		//	return false;

		if (!lasfile.Read((void*)FileSignature, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileSourceID, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GlobalEncoding, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_1, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_2, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&GUIDdata_3, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)GUIDdata_4, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&VersionMajor, 1))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&VersionMinor, 1))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)SystemIdentifier, 32))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)GeneratingSoftware, 32))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileCreationDayOfYear, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&FileCreationYear, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&HeaderSize, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&OffsettoData, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&NumberOfVariableLengthRecords, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&PointDataFormatID, 1))		return HEADER_READ_ERROR;//0-99 for spec
		if (!lasfile.Read((void*)&PointDataRecordLength, 2))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&LegacyNumberOfPointRecords, 4))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)LegacyNumberOfPointsbyReturn, 20))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&ScaleFactor_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Offset_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_X, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_Y, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Max_Z, 8))		return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&Min_Z, 8))		return HEADER_READ_ERROR;
		//Added for VER 14
		if (!lasfile.Read((void*)&StartWaveformDataPacketRecord, 8))			return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&StartFirstExtendedVariableLengthRecord, 8))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&NumberOfExtendedVariableLengthRecords, 4))	return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)&NumberOfPointRecords, 8))						return HEADER_READ_ERROR;
		if (!lasfile.Read((void*)NumberOfPointsbyReturn, 120))					return HEADER_READ_ERROR;

		return ERROR_FREE;
	}

	void DataCopy(LASHeader_14 &copy)
	{
		memcpy(FileSignature, copy.FileSignature, 4);
		FileSourceID = copy.FileSourceID;
		GlobalEncoding = copy.GlobalEncoding;
		GUIDdata_1 = copy.GUIDdata_1;
		GUIDdata_2 = copy.GUIDdata_2;
		GUIDdata_3 = copy.GUIDdata_3;
		memcpy(GUIDdata_4, copy.GUIDdata_4, 8);
		VersionMajor = copy.VersionMajor;
		VersionMinor = copy.VersionMinor;
		memcpy(SystemIdentifier, copy.SystemIdentifier, 32);
		memcpy(GeneratingSoftware, copy.GeneratingSoftware, 32);
		FileCreationDayOfYear = copy.FileCreationDayOfYear;
		FileCreationYear = copy.FileCreationYear;
		HeaderSize = copy.HeaderSize;
		OffsettoData = copy.OffsettoData;
		NumberOfVariableLengthRecords = copy.NumberOfVariableLengthRecords;
		PointDataFormatID = copy.PointDataFormatID;
		PointDataRecordLength = copy.PointDataRecordLength;
		LegacyNumberOfPointRecords = copy.LegacyNumberOfPointRecords;
		memcpy(LegacyNumberOfPointsbyReturn, copy.LegacyNumberOfPointsbyReturn, sizeof(unsigned long) * 5);
		ScaleFactor_X = copy.ScaleFactor_X;
		ScaleFactor_Y = copy.ScaleFactor_Y;
		ScaleFactor_Z = copy.ScaleFactor_Z;
		Offset_X = copy.Offset_X;
		Offset_Y = copy.Offset_Y;
		Offset_Z = copy.Offset_Z;
		Max_X = copy.Max_X;
		Min_X = copy.Min_X;
		Max_Y = copy.Max_Y;
		Min_Y = copy.Min_Y;
		Max_Z = copy.Max_Z;
		Min_Z = copy.Min_Z;
		//Added for VER 14
		StartWaveformDataPacketRecord = copy.StartWaveformDataPacketRecord;
		StartFirstExtendedVariableLengthRecord = copy.StartFirstExtendedVariableLengthRecord;
		NumberOfExtendedVariableLengthRecords = copy.NumberOfExtendedVariableLengthRecords;
		NumberOfPointRecords = copy.NumberOfPointRecords;
		memcpy(NumberOfPointsbyReturn, copy.NumberOfPointsbyReturn, sizeof(unsigned long) * 15);
	}
};