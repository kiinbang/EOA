// SMPhotoSimulation.h: interface for the CSMPhotoSimulation class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SMPHOTOSIMULATION_H__55F6F494_4E08_4E31_9C9C_4133338587D0__INCLUDED_)
#define AFX_SMPHOTOSIMULATION_H__55F6F494_4E08_4E31_9C9C_4133338587D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <math.h>
#include "UtilityGrocery_2.0.h"
#include "SMList.h"
#include "SMMatrixClass.h"
#include "SMRotationMatrix.h"

#define Deg2Rad(val) val/180.0*acos(-1.0)
#define Rad2Deg(val) val*180.0/acos(-1.0)

#define POSARTIFACT 5.0 //meters
#define ORIARTIFACT 2.0 //deg
#define REGIONMARGIN 500 //meters

///
/// It is an old version
/// For the latest version, take "SSMPhotoSimulation.h and SSMPhotoSimulation.cpp"
///

using namespace EOA;

class Point3D
{
public:
	double X, Y, Z;
	int ID;
};

class Point2D
{
public:
	double x, y;
	int ID;
};

class GroundPoint : public Point3D
{
public:
	//false: objected points measured in only one image 
	//true: objected points measured in more than one image
	bool bConjugate;
};

class PositionOrientation
{
public:
	double pos[3], ori[3];
};

class GPSINS : public PositionOrientation
{
public:
	double t;
};

class Photo
{
public:
	CSMList<Point2D> img_point;
	CSMList<Point2D> img_point_noise;
	int ID;
};

class StripData
{
public:
	CSMList<GPSINS> eop;
	CSMList<Photo> photo;
};

class Trajectory
{
public:
	CSMList<GPSINS> epoch;
};

class TrjectoryParameters
{
public:
	double a[4], b[4], c[4];//X, Y, and Z
	double d[4], e[4], f[4];//Omg, Phi, and Kap
};

class CSMPhotoSimulation  
{
public:
	CSMPhotoSimulation() 
	{
		camera_c = NULL;
		camera_xp = NULL;
		camera_yp = NULL;
		pixel_size = NULL;
		width = NULL;
		height = NULL;
		LA_BS = NULL;
		block = NULL;
	}

	virtual ~CSMPhotoSimulation() 
	{
		if(camera_c != NULL)
		{
			delete[] camera_c;
			camera_c = NULL;
		}

		if(camera_xp != NULL)
		{
			delete[] camera_xp;
			camera_xp = NULL;
		}

		if(camera_yp != NULL)
		{
			delete[] camera_yp;
			camera_yp = NULL;
		}

		if(pixel_size != NULL)
		{
			delete[] pixel_size;
			pixel_size = NULL;
		}

		if(width != NULL)
		{
			delete[] width;
			width = NULL;
		}

		if(height != NULL)
		{
			delete[] height;
			height = NULL;
		}

		if(LA_BS != NULL)
		{
			delete[] LA_BS;
			LA_BS = NULL;
		}

		if(block != NULL)
		{
			delete[] block;
			block = NULL;
		}
	}

	bool RunSimulation(const std::string& simpath, const std::string& outfilepath)
	{
		srand(static_cast<unsigned int>(time(NULL)));

		if(!ReadSimFile(simpath))
			return false;

		GroundPoint_Simulation();

		GPSINS_Simulation();

		for(int i=0; i<num_cam; i++)
		{
			EOP_Simulation(i);
			
			ImgMeasurement_Simulation(i);			
		}
		
		CheckMultipleCapturedGroundPoints();		

		AddRandomErrors_To_Trajectory();

		AddRandomErrors_To_Image();

		AddRandomErrors_To_Ground();

		SaveSimulationData(outfilepath);

		return true;
	}	

protected:
	//
	//camera
	//
	int num_cam;//number of cameras
	CSMList<std::string> camera_id;//camera ID
	double* camera_c;//mm
	double* camera_xp, *camera_yp;//mm
	double* pixel_size;//mm
	double* width, *height;//pixel
	//lever-arm/bore-sight
	PositionOrientation* LA_BS;//meter, rad

	//
	//surveying area and ground points
	//
	double LT_X, LT_Y, RB_X, RB_Y;//meter
	double point_density;//(m^2/point)
	double min_ground_H, max_ground_H;//meter
	CSMList<GroundPoint> Ground_rand_points;
	CSMList<GroundPoint> Ground_rand_points_noise;
	//CSMList<GroundPoint> multiple_observed_points;
	//CSMList<GroundPoint> multiple_observed_points_noise;

	//
	//flight lines
	//
	int num_flight_lines;
	CSMList<double> time_offset;//sec
	CSMList<int> num_images;
	CSMList<double> time_gap;//sec
	CSMList<TrjectoryParameters> trajectory_parameter;//meter, rad
	CSMList<Trajectory> trajectory;
	CSMList<Trajectory> trajectory_noise;
	
	CSMList<StripData> *block;

	//
	//Random errors
	//
	double sX, sY, sZ; //GPS random errors (meters)
	double sO, sP, sK; //INS random errors (rad)
	double scol, srow;//image measurement errors (pixel)
	double sGCPX, sGCPY, sGCPZ;//ground point measurement errors (meters)

protected:

	bool ReadSimFile(const std::string& simpath)
	{
		fstream infile;
		infile.open(simpath, ios::in);

		if (!infile)
			return false;
		
		util::RemoveCommentLine(infile, '#');
		infile>>num_cam;

		if(num_cam > 0)
		{			
			camera_c = new double[num_cam];
			camera_xp = new double[num_cam];
			camera_yp = new double[num_cam];
			pixel_size = new double[num_cam];
			width = new double[num_cam];
			height = new double[num_cam];
			LA_BS = new PositionOrientation[num_cam];
			block = new CSMList<StripData>[num_cam];
			camera_id.RemoveAll();
		}
		else
		{
			return false;
		}

		for(int i=0; i<num_cam; i++)
		{
			char temp[512];
			std::string CAM_ID;

			util::RemoveCommentLine(infile, '#');
			infile.getline(temp, 512);
			CAM_ID = temp;
			camera_id.AddTail(CAM_ID);

			util::RemoveCommentLine(infile, '#');
			infile>>camera_c[i];
			
			util::RemoveCommentLine(infile, '#');
			infile>>camera_xp[i];
			
			util::RemoveCommentLine(infile, '#');
			infile>>camera_yp[i];
			
			util::RemoveCommentLine(infile, '#');
			infile>>pixel_size[i];
			
			util::RemoveCommentLine(infile, '#');
			infile>>width[i];//pixels
			
			util::RemoveCommentLine(infile, '#');
			infile>>height[i];//pixels
			
			util::RemoveCommentLine(infile, '#');
			infile>>LA_BS[i].pos[0];
			
			util::RemoveCommentLine(infile, '#');
			infile>>LA_BS[i].pos[1];
			
			util::RemoveCommentLine(infile, '#');
			infile>>LA_BS[i].pos[2];
			
			util::RemoveCommentLine(infile, '#');
			infile>>LA_BS[i].ori[0];
			LA_BS[i].ori[0] = Deg2Rad(LA_BS[i].ori[0]);
			
			util::RemoveCommentLine(infile, '#');
			infile>>LA_BS[i].ori[1];
			LA_BS[i].ori[1] = Deg2Rad(LA_BS[i].ori[1]);
			
			util::RemoveCommentLine(infile, '#');
			infile>>LA_BS[i].ori[2];
			LA_BS[i].ori[2] = Deg2Rad(LA_BS[i].ori[2]);
		}

		util::RemoveCommentLine(infile, '#');
		infile>>LT_X; LT_X -= REGIONMARGIN;
		
		util::RemoveCommentLine(infile, '#');
		infile>>LT_Y; LT_Y += REGIONMARGIN;
		
		util::RemoveCommentLine(infile, '#');
		infile>>RB_X; RB_X += REGIONMARGIN;
		
		util::RemoveCommentLine(infile, '#');
		infile>>RB_Y; RB_Y -= REGIONMARGIN;
		
		util::RemoveCommentLine(infile, '#');
		infile>>min_ground_H;
		
		util::RemoveCommentLine(infile, '#');
		infile>>max_ground_H;

		util::RemoveCommentLine(infile, '#');
		infile>>point_density;

		util::RemoveCommentLine(infile, '#');
		infile>>num_flight_lines;

		time_offset.RemoveAll();
		num_images.RemoveAll();
		time_gap.RemoveAll();
		trajectory_parameter.RemoveAll();

		double t_offset, t_gap;
		int num_img;
		TrjectoryParameters trj_param;
		for(int i=0; i<num_flight_lines; i++)
		{
			util::RemoveCommentLine(infile, '#');
			infile>>t_offset;
			time_offset.AddTail(t_offset);

			util::RemoveCommentLine(infile, '#');
			infile>>num_img;
			num_images.AddTail(num_img);

			util::RemoveCommentLine(infile, '#');
			infile>>t_gap;
			time_gap.AddTail(t_gap);

			int j;
			for(j=0; j<4; j++)
			{
				util::RemoveCommentLine(infile, '#');
				infile>>trj_param.a[j];
			}

			for(j=0; j<4; j++)
			{
				util::RemoveCommentLine(infile, '#');
				infile>>trj_param.b[j];
			}

			for(j=0; j<4; j++)
			{
				util::RemoveCommentLine(infile, '#');
				infile>>trj_param.c[j];
			}

			for(j=0; j<4; j++)
			{
				util::RemoveCommentLine(infile, '#');
				infile>>trj_param.d[j];
				trj_param.d[j] = Deg2Rad(trj_param.d[j]);
			}

			for(j=0; j<4; j++)
			{
				util::RemoveCommentLine(infile, '#');
				infile>>trj_param.e[j];
				trj_param.e[j] = Deg2Rad(trj_param.e[j]);
			}

			for(j=0; j<4; j++)
			{
				util::RemoveCommentLine(infile, '#');
				infile>>trj_param.f[j];
				trj_param.f[j] = Deg2Rad(trj_param.f[j]);
			}

			trajectory_parameter.AddTail(trj_param);
		}

		util::RemoveCommentLine(infile, '#');
		infile>>sX;

		util::RemoveCommentLine(infile, '#');
		infile>>sY;

		util::RemoveCommentLine(infile, '#');
		infile>>sZ;

		util::RemoveCommentLine(infile, '#');
		infile>>sO;
		sO = Deg2Rad(sO);

		util::RemoveCommentLine(infile, '#');
		infile>>sP;
		sP = Deg2Rad(sP);

		util::RemoveCommentLine(infile, '#');
		infile>>sK;
		sK = Deg2Rad(sK);

		util::RemoveCommentLine(infile, '#');
		infile>>scol;//pixel

		util::RemoveCommentLine(infile, '#');
		infile>>srow;//pixel

		util::RemoveCommentLine(infile, '#');
		infile>>sGCPX;//meter

		util::RemoveCommentLine(infile, '#');
		infile>>sGCPY;//meter

		util::RemoveCommentLine(infile, '#');
		infile>>sGCPZ;//meter

		return true;

	}

	void GroundPoint_Simulation()
	{
		srand( (unsigned)time( NULL ) );

		Ground_rand_points.RemoveAll();
		double ground_W=(RB_X - LT_X), ground_H=(LT_Y - RB_Y), ground_D=(max_ground_H - min_ground_H);
		int num_points = int ( (ground_W*ground_H)/point_density +0.5);

		GroundPoint rand_P;
		rand_P.bConjugate = false;

		for(int i=0; i<num_points; i++)
		{
			rand_P.X = ( GetRandNum() + 1.0 )*ground_W/2.0 + LT_X;

			rand_P.Y = ( GetRandNum() + 1.0 )*ground_H/2.0 + RB_Y;
			rand_P.Z = ( GetRandNum() + 1.0 )*ground_D/2.0 + min_ground_H;

			rand_P.ID = i;

			Ground_rand_points.AddTail(rand_P);
		}
	}

	void GPSINS_Simulation()
	{
		srand( (unsigned)time( NULL ) );
		
		TrjectoryParameters trj_param;
		double t_offset;
		double t_gap;
		_Rmat_ R;
		double epoch_time;
		int num_img;

		trajectory.RemoveAll();

		for(int i=0; i<num_flight_lines; i++)
		{
			Trajectory gpsins;			
			
			trj_param = trajectory_parameter.GetAt(i);
			t_offset = time_offset.GetAt(i);
			num_img = num_images.GetAt(i);
			t_gap = time_gap.GetAt(i);

			epoch_time = 0.0;

			for(int j=0; j<num_img; j++)
			{
				GPSINS epoch;
				
				//update epoch time
				epoch_time += t_gap;

				//update recored time (t_offset + epoch_time)
				epoch.t = t_offset + epoch_time;
				
				//trajectory is simulated using epoch_time (not epoch.t)
				epoch.pos[0]	= trj_param.a[0]
					+ trj_param.a[1]*epoch_time
					+ trj_param.a[2]*pow(epoch_time,2)
					+ trj_param.a[3]*pow(epoch_time,3);
				
				epoch.pos[1]	= trj_param.b[0]
					+ trj_param.b[1]*epoch_time
					+ trj_param.b[2]*pow(epoch_time,2)
					+ trj_param.b[3]*pow(epoch_time,3);
				
				epoch.pos[2]	= trj_param.c[0]
					+ trj_param.c[1]*epoch_time
					+ trj_param.c[2]*pow(epoch_time,2)
					+ trj_param.c[3]*pow(epoch_time,3);
				
				epoch.ori[0]	= trj_param.d[0]
					+ trj_param.d[1]*epoch_time
					+ trj_param.d[2]*pow(epoch_time,2)
					+ trj_param.d[3]*pow(epoch_time,3);
				
				epoch.ori[1]	= trj_param.e[0]
					+ trj_param.e[1]*epoch_time
					+ trj_param.e[2]*pow(epoch_time,2)
					+ trj_param.e[3]*pow(epoch_time,3);
				
				epoch.ori[2]	= trj_param.f[0]
					+ trj_param.f[1]*epoch_time
					+ trj_param.f[2]*pow(epoch_time,2)
					+ trj_param.f[3]*pow(epoch_time,3);

				epoch.pos[0] += GetRandNum()*POSARTIFACT;
				epoch.pos[1] += GetRandNum()*POSARTIFACT;
				epoch.pos[2] += GetRandNum()*POSARTIFACT;

				epoch.ori[0] += GetRandNum()*(Deg2Rad(ORIARTIFACT));
				epoch.ori[1] += GetRandNum()*(Deg2Rad(ORIARTIFACT));
				epoch.ori[2] += GetRandNum()*(Deg2Rad(ORIARTIFACT));

				gpsins.epoch.AddTail(epoch);
			}

			trajectory.AddTail(gpsins);
		}
	}

	void EOP_Simulation(int camera_index)
	{
		CSMMatrix<double> Lever_Arm(3,1);
		Lever_Arm(0,0) = LA_BS[camera_index].pos[0];
		Lever_Arm(1,0) = LA_BS[camera_index].pos[1];
		Lever_Arm(2,0) = LA_BS[camera_index].pos[2];
		
		_Rmat_ Boresight(LA_BS[camera_index].ori[0], LA_BS[camera_index].ori[1], LA_BS[camera_index].ori[2]);

		//[Definition]
		//G_vec = GPS_vec + Rins*LA + S*Rins*Boresight*IMG_vec
		//

		block[camera_index].RemoveAll();

		int num_img;
		Trajectory gpsins;
		GPSINS epoch;
		CSMMatrix<double> pos(3,1);
		StripData strip;
		GPSINS eop_j;
		Photo dumy_photo_j;
		CSMMatrix<double> EOP_pos, EOP_ori;

		_Rmat_ Rins;

		int image_count = 0;

		for(int i=0; i<num_flight_lines; i++)
		{
			strip.eop.RemoveAll();
			strip.photo.RemoveAll();

			gpsins = trajectory.GetAt(i);
			num_img = num_images.GetAt(i);
			
			for(int j=0; j<num_img; j++)
			{
				epoch = gpsins.epoch.GetAt(j);

				eop_j.t = epoch.t;

				pos(0,0) = epoch.pos[0];
				pos(1,0) = epoch.pos[1];
				pos(2,0) = epoch.pos[2];
				
				Rins.ReMake(epoch.ori[0], epoch.ori[1], epoch.ori[2]);

				//double tempO, tempP, tempK;
				//GetRotationfromMatrix(Rins.Rmatrix, tempO, tempP, tempK);
				//double tempO2, tempP2, tempK2;
				//GetRotationfromMatrix2(Rins.Rmatrix, tempO2, tempP2, tempK2);

				EOP_pos = pos + Rins.rmatrix()%Lever_Arm;//perspective center

				eop_j.pos[0] = EOP_pos(0,0);
				eop_j.pos[1] = EOP_pos(1,0);
				eop_j.pos[2] = EOP_pos(2,0);

				EOP_ori = Rins.rmatrix()%Boresight.rmatrix();

				GetRotationfromMatrix(EOP_ori, eop_j.ori[0], eop_j.ori[1], eop_j.ori[2]);

				strip.eop.AddTail(eop_j);

				dumy_photo_j.ID = image_count + camera_index*100000;
				strip.photo.AddTail(dumy_photo_j);
				image_count ++;
			}

			block[camera_index].AddTail(strip);
		}
	}

	void ImgMeasurement_Simulation(int camera_index)
	{
		int num_img;
		GPSINS eop_j;
		Photo *photo_j;
		StripData *strip_i;

		for(int i=0; i<num_flight_lines; i++)
		{
			num_img = num_images.GetAt(i);

			strip_i = block[camera_index].GetHandleAt(i);
			
			for(int j=0; j<num_img; j++)
			{
				eop_j = strip_i->eop.GetAt(j);
				photo_j = strip_i->photo.GetHandleAt(j);
				
				ImgMeasurement_Simulation(eop_j, photo_j, camera_index);
			}
		}
	}

	void ImgMeasurement_Simulation(GPSINS eop_j, Photo *photo_j, int camera_index)
	{
		//[Definition]
		//GP = EOP_pos + S*EOP_ori*IMG_vec
		//
		//S*IMG_vec = (EOP_ori_Trans)(GP - EOP_pos)

		photo_j->img_point.RemoveAll();

		int num_points = Ground_rand_points.GetNumItem();
		GroundPoint point;
		Point2D imgP;
		double x, y;
		
		CSMMatrix<double> GP(3,1), EOP_pos(3,1), ND;
		EOP_pos(0,0) = eop_j.pos[0];
		EOP_pos(1,0) = eop_j.pos[1];
		EOP_pos(2,0) = eop_j.pos[2];

		_Rmat_ R(eop_j.ori[0], eop_j.ori[1], eop_j.ori[2]);

		for(int i=0; i<num_points; i++)
		{
			point = Ground_rand_points.GetAt(i);

			GP(0,0) = point.X;
			GP(1,0) = point.Y;
			GP(2,0) = point.Z;

			ND = R.rmatrix().Transpose()%(GP - EOP_pos);

			x = -camera_c[camera_index] * (ND(0,0)/ND(2,0));
			y = -camera_c[camera_index] * (ND(1,0)/ND(2,0));

			imgP.x = x + camera_xp[camera_index];
			imgP.y = y + camera_yp[camera_index];

			if( true == Check_Img_Coord(imgP, camera_index) )
			{
				imgP.ID = point.ID;
				photo_j->img_point.AddTail(imgP);
			}
		}
	}

	double GetRandNum()
	{
		double half_RAND_MAX = (double)RAND_MAX/2.;
		double rand_num = (double)rand();//0 ~ RAND_MAX
		rand_num = rand_num/half_RAND_MAX - 1.0;//-1~1
		
		return rand_num;
	}

	//DO NOT use this one
	void GetRotationfromMatrix2(CSMMatrix<double> R, double &Omg, double &Phi, double &Kap)
	{
		Phi = asin(R(0,2));//r13: sin(phi)
		Kap = asin(R(0,1)/-cos(Phi));//r12: -cos(phi)*sin(kappa)
		Omg = asin(R(1,2)/-cos(Phi));//-sin(omega)*cos(phi)
	}

	void GetRotationfromMatrix(CSMMatrix<double> R, double &Omg, double &Phi, double &Kap)
	{
		Phi = asin(R(0,2));
		Kap = atan2(-R(0,1)/cos(Phi), R(0,0)/cos(Phi));
		Omg = atan2(-R(1,2)/cos(Phi), R(2,2)/cos(Phi));
	}

	bool Check_Img_Coord(Point2D P, int camera_index)
	{
		if(fabs(P.x) > width[camera_index]*pixel_size[camera_index]/2.0)
			return false;
		if(fabs(P.y) > height[camera_index]*pixel_size[camera_index]/2.0)
			return false;

		return true;
	}

	bool FindImgPoint(Photo photo, int ID)
	{
		int num_points = photo.img_point.GetNumItem();

		for(int i=0; i<num_points; i++)
			if(ID == photo.img_point.GetAt(i).ID)
				return true;

		return false;
	}

	bool FindGroundPoint(int ID, GroundPoint &P)
	{
		int num_points = Ground_rand_points.GetNumItem();
		
		for(int i=0; i<num_points; i++)
			if(ID == Ground_rand_points.GetAt(i).ID)
			{
				P = Ground_rand_points.GetAt(i);
				return true;
			}
			
			return false;
	}

	void CheckMultipleCapturedGroundPoints()
	{
		int num_points = Ground_rand_points.GetNumItem();
		GroundPoint *point_i;
		int obs_count;
		
		for(int i=0; i<num_points; i++)
		{
			point_i = Ground_rand_points.GetHandleAt(i);

			for(int camera_index=0; camera_index<num_cam; camera_index ++)
			{
				obs_count = 0;
				
				int num_img;
				Photo photo_k;
				StripData strip_j;
				
				for(int j=0; j<num_flight_lines; j++)
				{
					num_img = num_images.GetAt(j);
					
					strip_j = block[camera_index].GetAt(j);
					
					for(int k=0; k<num_img; k++)
					{
						photo_k = strip_j.photo.GetAt(k);
						
						if( FindImgPoint(photo_k, point_i->ID) )
						{
							obs_count ++;
						}
						
						if(obs_count >= 2) 
							break;
					}
					
					if(obs_count >= 2)
						break;
				}

				if(obs_count >= 2)
						break;				
			}			
			
			if(obs_count >= 2)
			{
				point_i->bConjugate = true;
			}
		}
	}

	void AddRandomErrors_To_Trajectory()
	{
		srand( (unsigned)time( NULL ) );

		//
		//GPSINS with noise
		//

		//copy from error free data
		trajectory_noise = trajectory;

		int num_tri = trajectory_noise.GetNumItem();
		for(int i=0; i<num_tri; i++)
		{
			Trajectory *tri_i = trajectory_noise.GetHandleAt(i);
			
			int num_epoch = tri_i->epoch.GetNumItem();

			GPSINS *epoch_j;

			for(int j=0; j<num_epoch; j++)
			{
				epoch_j = tri_i->epoch.GetHandleAt(j);

				//Add noises
				epoch_j->pos[0] += GetRandNum() * sX;
				epoch_j->pos[1] += GetRandNum() * sY;
				epoch_j->pos[2] += GetRandNum() * sZ;

				epoch_j->ori[0] += GetRandNum() * sO;
				epoch_j->ori[1] += GetRandNum() * sP;
				epoch_j->ori[2] += GetRandNum() * sK;
			}
		}
	}

	void AddRandomErrors_To_Image()
	{
		srand( (unsigned)time( NULL ) );
		
		//
		//Image measurement with noises
		//
		int num_img;
		Photo *photo_j;
		StripData *strip_i;

		for(int camera_index=0; camera_index<num_cam; camera_index ++)
		{
			for(int i=0; i<num_flight_lines; i++)
			{
				num_img = num_images.GetAt(i);
				
				strip_i = block[camera_index].GetHandleAt(i);
				
				for(int j=0; j<num_img; j++)
				{
					photo_j = strip_i->photo.GetHandleAt(j);
					
					//copy from error free data
					photo_j->img_point_noise = photo_j->img_point;
					
					int num_img_points = photo_j->img_point_noise.GetNumItem();
					Point2D *imgP_k;
					
					for(int k=0; k<num_img_points; k++)
					{
						imgP_k = photo_j->img_point_noise.GetHandleAt(k);
						
						imgP_k->x += GetRandNum() * scol*pixel_size[camera_index];
						imgP_k->y += GetRandNum() * srow*pixel_size[camera_index];	
					}
				}
			}
		}
	}

	void AddRandomErrors_To_Ground()
	{
		srand( (unsigned)time( NULL ) );
		
		//
		//ground points with noises
		//
		
		//copy from error free data
		Ground_rand_points_noise = Ground_rand_points;
		
		int num_points = Ground_rand_points_noise.GetNumItem();
		GroundPoint *GP_i;
		
		for(int i=0; i<num_points; i++)
		{
			GP_i = Ground_rand_points_noise.GetHandleAt(i);
			
			GP_i->X += GetRandNum() * sGCPX;
			GP_i->Y += GetRandNum() * sGCPY;
			GP_i->Z += GetRandNum() * sGCPZ;
		}
	}

	void SaveSimulationData(const std::string& outfilepath)
	{
		//gpsins file (noise free)
		std::string gpsins_path = outfilepath + ".gps";
		fstream gps_file; gps_file.setf(ios::fixed, ios::floatfield);
		gps_file.open(gpsins_path, ios::out);
		PrintOut_gpsins(gps_file, false);
		gps_file.close();

		//gpsins file (noise)
		std::string gpsins_noise_path = outfilepath + "_noise.gps";
		fstream gps_noise_file; gps_noise_file.setf(ios::fixed, ios::floatfield);
		gps_noise_file.open(gpsins_noise_path, ios::out);
		PrintOut_gpsins(gps_noise_file, true);
		gps_noise_file.close();

		//icf file (noise free)
		std::string icf_path = outfilepath + ".icf";
		fstream icf_file; icf_file.setf(ios::fixed, ios::floatfield);
		icf_file.open(icf_path, ios::out);
		PrintOut_icf(icf_file, false);
		icf_file.close();

		//icf file (noise)
		std::string icf_noise_path = outfilepath + "_noise.icf";
		fstream icf_noise_file; icf_noise_file.setf(ios::fixed, ios::floatfield);
		icf_noise_file.open(icf_noise_path, ios::out);
		PrintOut_icf(icf_noise_file, true);
		icf_noise_file.close();

		//gcp file (noise free)
		std::string gcp_path = outfilepath + ".gcp";
		fstream gcp_file; gcp_file.setf(ios::fixed, ios::floatfield);
		gcp_file.open(gcp_path, ios::out);
		PrintOut_gcp(gcp_file, false);
		gcp_file.close();

		//gcp file (noise)
		std::string gcp_noise_path = outfilepath + "_ERROR.gcp";
		fstream gcp_noise_file; gcp_noise_file.setf(ios::fixed, ios::floatfield);
		gcp_noise_file.open(gcp_noise_path, ios::out);
		PrintOut_gcp(gcp_noise_file, true);
		gcp_noise_file.close();

		//ori file
		std::string ORI_path = outfilepath + ".ori";
		fstream ORI_file; ORI_file.setf(ios::fixed, ios::floatfield);
		ORI_file.open(ORI_path, ios::out);
		PrintOut_ori(ORI_file);		
		ORI_file.close();

		//cam file
		std::string CAM_path = outfilepath + ".cam";
		fstream CAM_file; CAM_file.setf(ios::fixed, ios::floatfield);
		CAM_file.open(CAM_path, ios::out);
		PrintOut_cam(CAM_file);		
		CAM_file.close();

		//Save true orientation
		std::string EOP_path = outfilepath + "_TRUE_EOP.txt";
		fstream EOP_file; EOP_file.setf(ios::fixed, ios::floatfield);
		EOP_file.open(EOP_path, ios::out);

		std::string ImgBND_path = outfilepath + "_IMG_BND.txt";
		fstream ImgBND_file; ImgBND_file.setf(ios::fixed, ios::floatfield);
		ImgBND_file.open(ImgBND_path, ios::out);

		PrintOut_EOP(EOP_file, ImgBND_file);
		
		EOP_file.close();
		ImgBND_file.close();
	}

	void PrintOut_gpsins(fstream &gpsins_file, bool bNoise)
	{
		Trajectory trj_i;
		
		for(int i=0; i<num_flight_lines; i++)
		{
			if(bNoise == false)
				trj_i = trajectory.GetAt(i);
			else
				trj_i = trajectory_noise.GetAt(i);
			
			int num_epoch = trj_i.epoch.GetNumItem();

			GPSINS epoch_j;

			for(int j=0; j<num_epoch; j++)
			{
				epoch_j = trj_i.epoch.GetAt(j);

				gpsins_file.precision(7);

				gpsins_file<<epoch_j.t<<endl;//sec

				gpsins_file.precision(6);

				gpsins_file<<Rad2Deg(epoch_j.ori[0])<<"\t";//deg
				gpsins_file<<Rad2Deg(epoch_j.ori[1])<<"\t";//deg
				gpsins_file<<Rad2Deg(epoch_j.ori[2])<<endl;//deg

				if(bNoise == true)
				{
					gpsins_file<<Rad2Deg(sO)*3600*Rad2Deg(sO)*3600<<"\t";//sec^2
					gpsins_file<<"0.0"<<"\t";
					gpsins_file<<"0.0"<<endl;
					
					gpsins_file<<"0.0"<<"\t";
					gpsins_file<<Rad2Deg(sP)*3600*Rad2Deg(sP)*3600<<"\t";//sec^2
					gpsins_file<<"0.0"<<endl;
					
					gpsins_file<<"0.0"<<"\t";
					gpsins_file<<"0.0"<<"\t";
					gpsins_file<<Rad2Deg(sK)*3600*Rad2Deg(sK)*3600<<endl;//sec^2
				}
				else
				{
					gpsins_file<<"1.0e-9 0 0"<<endl;
					gpsins_file<<"0 1.0e-9 0"<<endl;
					gpsins_file<<"0 0 1.0e-9"<<endl;
				}

				gpsins_file.precision(3);

				gpsins_file<<epoch_j.pos[0]<<"\t";//m
				gpsins_file<<epoch_j.pos[1]<<"\t";//m
				gpsins_file<<epoch_j.pos[2]<<endl;//m

				if(bNoise == true)
				{
					gpsins_file<<sX*sX<<"\t";//m^2
					gpsins_file<<"0.0"<<"\t";
					gpsins_file<<"0.0"<<endl;
					
					gpsins_file<<"0.0"<<"\t";
					gpsins_file<<sY*sY<<"\t";//m^2
					gpsins_file<<"0.0"<<endl;
					
					gpsins_file<<"0.0"<<"\t";
					gpsins_file<<"0.0"<<"\t";
					gpsins_file<<sZ*sZ<<endl;//m^2

				}
				else
				{
					gpsins_file<<"1.0e-9 0 0"<<endl;
					gpsins_file<<"0 1.0e-9 0"<<endl;
					gpsins_file<<"0 0 1.0e-9"<<endl;
				}

				gpsins_file<<endl;
			}
		}		
	}

	void PrintOut_icf(fstream &icf_file, bool bNoise)
	{
		icf_file.precision(5);
		
		StripData strip_i;
		
		for(int camera_index=0; camera_index<num_cam; camera_index++)
		{
			for(int i=0; i<num_flight_lines; i++)
			{
				strip_i = block[camera_index].GetAt(i);
				
				int num_img = strip_i.photo.GetNumItem();
				
				Photo photo_j;
				
				for(int j=0; j<num_img; j++)
				{
					photo_j = strip_i.photo.GetAt(j);
					
					int num_points;
					
					if(bNoise == false)
						num_points = photo_j.img_point.GetNumItem();
					else
						num_points = photo_j.img_point_noise.GetNumItem();
					
					Point2D point;
					GroundPoint GP;
					
					if(num_points <4) continue;
					
					for(int k=0; k<num_points; k++)
					{
						if(bNoise == false)
							point = photo_j.img_point.GetAt(k);
						else
							point = photo_j.img_point_noise.GetAt(k);
						
						if(FindGroundPoint(point.ID, GP))
						{
							if(GP.bConjugate == true)
							{
								icf_file<<photo_j.ID<<"\t";
								icf_file<<point.ID<<"\t";
								icf_file<<point.x<<"\t";
								icf_file<<point.y<<"\t";
								icf_file<<"1	0	0	1"<<endl;
							}
						}
					}
					
				}
				
			}	
		}
	}

	void PrintOut_gcp(fstream &gcp_file, bool bNoise)
	{
		gcp_file.precision(5);

		int num_points = Ground_rand_points.GetNumItem();

		GroundPoint point;
		for(int i=0; i<num_points; i++)
		{
			if(bNoise == false)
				point = Ground_rand_points.GetAt(i);
			else
				point = Ground_rand_points_noise.GetAt(i);

			//if(point.bConjugate == true) continue;

			gcp_file<<point.ID<<"\t";
			gcp_file<<point.X<<"\t";
			gcp_file<<point.Y<<"\t";
			gcp_file<<point.Z<<"\t";

			if(bNoise == true)
			{
				gcp_file<<sGCPX*sGCPX<<"\t"; gcp_file<<"0"<<"\t";  gcp_file<<"0"<<"\t";
				gcp_file<<"0"<<"\t";  gcp_file<<sGCPY*sGCPY<<"\t"; gcp_file<<"0"<<"\t";
				gcp_file<<"0"<<"\t";  gcp_file<<"0"<<"\t"; gcp_file<<sGCPZ*sGCPZ<<endl;
			}
			else
			{
				gcp_file<<"1.0e-9 0 0 0 1.0e-9 0 0 0 1.0e-9"<<endl;
			}
		}
	}

	void PrintOut_ori(fstream &ORI_file)
	{
		StripData strip_i;
		
		for(int camera_index=0; camera_index<num_cam; camera_index++)
		{
			for(int i=0; i<num_flight_lines; i++)
			{
				strip_i = block[camera_index].GetAt(i);
				
				int num_img = strip_i.photo.GetNumItem();
				
				GPSINS eop_j;
				Photo photo_j;
				
				for(int j=0; j<num_img; j++)
				{
					eop_j = strip_i.eop.GetAt(j);
					photo_j = strip_i.photo.GetAt(j);
					
					ORI_file<<"!	Photo_id		Camera_id		sigma_XY		scan_time		Degree_OPK		Degree_XYZ"<<endl;
					ORI_file<<photo_j.ID<<"\t"<<camera_id[camera_index]<<"\t"<<pixel_size[camera_index]*0.5<<" 0 0 0"<<endl<<endl;
					ORI_file<<"! Alpha Coeff and it's Dispersion"<<endl;
					ORI_file<<"0"<<endl<<endl;
					
					ORI_file<<"! Imc Coef and it's Dispersion"<<endl; 
					ORI_file<<"0"<<endl<<endl;
					
					ORI_file<<"! no  Ori Images"<<endl;
					ORI_file<<"1"<<endl<<endl;
					ORI_file<<"!Ori_no	Ori_time	   omega	   phi		kappa		X		Y	  Z"<<endl;
					
					ORI_file.precision(6);
					ORI_file<<"1 \t"<<eop_j.t<<"\t";				
					
					ORI_file<<Rad2Deg(eop_j.ori[0])<<"\t";
					ORI_file<<Rad2Deg(eop_j.ori[1])<<"\t";
					ORI_file<<Rad2Deg(eop_j.ori[2])<<"\t";
					
					ORI_file.precision(4);
					ORI_file<<eop_j.pos[0]<<"\t";
					ORI_file<<eop_j.pos[1]<<"\t";
					ORI_file<<eop_j.pos[2]<<endl<<endl;
				}
			}
		}
	}

	void PrintOut_cam(fstream &CAM_file)
	{
		StripData strip_i;
		
		for(int camera_index=0; camera_index<num_cam; camera_index++)
		{
			CAM_file<<"! cameraID	type		xp	yp	c"<<endl;
			CAM_file<<camera_id[camera_index]<<"\t";
			CAM_file<<"FRAME"<<"\t";
			CAM_file<<camera_xp[camera_index]<<"\t";
			CAM_file<<camera_yp[camera_index]<<"\t";
			CAM_file<<camera_c[camera_index]<<endl<<endl;

			CAM_file<<"! dispersion of xp,yp,c"<<endl;
			CAM_file<<"1.0e-09	0.0		0.0"<<endl;
			CAM_file<<"0.0		1.0e-09	0.0"<<endl;
			CAM_file<<"0.0		0.0		1.0e-09"<<endl<<endl;

			CAM_file<<"! no fiducials"<<endl;
			CAM_file<<"0"<<endl<<endl;

			CAM_file<<"! User defined Ro"<<endl;
			CAM_file<<"0"<<endl<<endl;

			CAM_file<<"! Distortion Model used: 1=UofC, 2=SMAC"<<endl;
			CAM_file<<"1"<<endl<<endl;

			CAM_file<<"! no distortion and array elements"<<endl;
			CAM_file<<"6"<<endl<<endl;
			
			CAM_file<<"0.0 0.0 0.0 0.0 0.0 0.0"<<endl<<endl;
			
			CAM_file<<"1.0e-09	0.0		0.0		0.0		0.0		0.0"<<endl;
			CAM_file<<"0.0		1.0e-09	0.0		0.0		0.0		0.0"<<endl;
			CAM_file<<"0.0		0.0		1.0e-09	0.0		0.0		0.0"<<endl;
			CAM_file<<"0.0		0.0		0.0		1.0e-09	0.0		0.0"<<endl;
			CAM_file<<"0.0		0.0		0.0		0.0		1.0e-09	0.0"<<endl;
			CAM_file<<"0.0		0.0		0.0		0.0		0.0		1.0e-09"<<endl<<endl;

			CAM_file<<"!Lever-arm and dispersion"<<endl<<endl;
			
			CAM_file<<LA_BS[camera_index].pos[0]<<"\t";
			CAM_file<<LA_BS[camera_index].pos[1]<<"\t";
			CAM_file<<LA_BS[camera_index].pos[2]<<endl<<endl;

			CAM_file<<"1.0e-09	0.0		0.0"<<endl;
			CAM_file<<"0.0		1.0e-09	0.0"<<endl;
			CAM_file<<"0.0		0.0		1.0e-09"<<endl<<endl;

			CAM_file<<"!Boresight and dispersion"<<endl<<endl;
			
			CAM_file<<Rad2Deg(LA_BS[camera_index].ori[0])<<"\t";
			CAM_file<<Rad2Deg(LA_BS[camera_index].ori[1])<<"\t";
			CAM_file<<Rad2Deg(LA_BS[camera_index].ori[2])<<endl<<endl;
			
			CAM_file<<"1.0e-09	0.0		0.0"<<endl;
			CAM_file<<"0.0		1.0e-09	0.0"<<endl;
			CAM_file<<"0.0		0.0		1.0e-09"<<endl<<endl;
		}
	}

	void PrintOut_EOP(fstream &EOP_file, fstream &ImgBND_file)
	{
		StripData strip_i;
		
		for(int camera_index=0; camera_index<num_cam; camera_index++)
		{
			for(int i=0; i<num_flight_lines; i++)
			{
				strip_i = block[camera_index].GetAt(i);
				
				int num_img = strip_i.photo.GetNumItem();
				
				GPSINS eop_j;
				Photo photo_j;
				
				for(int j=0; j<num_img; j++)
				{
					eop_j = strip_i.eop.GetAt(j);
					photo_j = strip_i.photo.GetAt(j);
					
					EOP_file<<photo_j.ID<<"\t";
					
					EOP_file.precision(6);
					EOP_file<<Rad2Deg(eop_j.ori[0])<<"\t";
					EOP_file<<Rad2Deg(eop_j.ori[1])<<"\t";
					EOP_file<<Rad2Deg(eop_j.ori[2])<<"\t";
					
					EOP_file.precision(4);
					EOP_file<<eop_j.pos[0]<<"\t";
					EOP_file<<eop_j.pos[1]<<"\t";
					EOP_file<<eop_j.pos[2]<<endl;
					
					CSMMatrix<double> PC(3,1), coner[4];
					PC(0,0) = eop_j.pos[0]; 
					PC(1,0) = eop_j.pos[1]; 
					PC(2,0) = eop_j.pos[2]; 
					
					coner[0].Resize(3,1);
					coner[0](0,0) = -pixel_size[camera_index] * width[camera_index]/2.0;
					coner[0](1,0) = pixel_size[camera_index] * height[camera_index]/2.0;
					coner[0](2,0) = -camera_c[camera_index];
					
					coner[1].Resize(3,1);
					coner[1](0,0) = -pixel_size[camera_index] * width[camera_index]/2.0;
					coner[1](1,0) = -pixel_size[camera_index] * height[camera_index]/2.0;
					coner[1](2,0) = -camera_c[camera_index];
					
					coner[2].Resize(3,1);
					coner[2](0,0) = pixel_size[camera_index] * width[camera_index]/2.0;
					coner[2](1,0) = -pixel_size[camera_index] * height[camera_index]/2.0;
					coner[2](2,0) = -camera_c[camera_index];
					
					coner[3].Resize(3,1);
					coner[3](0,0) = pixel_size[camera_index] * width[camera_index]/2.0;
					coner[3](1,0) = pixel_size[camera_index] * height[camera_index]/2.0;
					coner[3](2,0) = -camera_c[camera_index];
					
					_Rmat_ R(eop_j.ori[0], eop_j.ori[1], eop_j.ori[2]);
					
					double scale = eop_j.pos[2]/camera_c[camera_index];//*1000.0;
					
					coner[0] *= scale;
					coner[1] *= scale;
					coner[2] *= scale;
					coner[3] *= scale;
					
					coner[0] = R.rmatrix()%coner[0];
					coner[1] = R.rmatrix()%coner[1];
					coner[2] = R.rmatrix()%coner[2];
					coner[3] = R.rmatrix()%coner[3];
					
					coner[0] += PC;
					coner[1] += PC;
					coner[2] += PC;
					coner[3] += PC;
					
					ImgBND_file<<photo_j.ID<<"_1\t"<<endl;
					ImgBND_file<<coner[0](0,0)<<"\t"<<coner[0](1,0)<<"\t"<<coner[0](2,0)<<endl;
					ImgBND_file<<coner[1](0,0)<<"\t"<<coner[1](1,0)<<"\t"<<coner[1](2,0)<<endl;
					
					ImgBND_file<<photo_j.ID<<"_2\t"<<endl;
					ImgBND_file<<coner[1](0,0)<<"\t"<<coner[1](1,0)<<"\t"<<coner[1](2,0)<<endl;
					ImgBND_file<<coner[2](0,0)<<"\t"<<coner[2](1,0)<<"\t"<<coner[2](2,0)<<endl;
					
					ImgBND_file<<photo_j.ID<<"_3\t"<<endl;
					ImgBND_file<<coner[2](0,0)<<"\t"<<coner[2](1,0)<<"\t"<<coner[2](2,0)<<endl;
					ImgBND_file<<coner[3](0,0)<<"\t"<<coner[3](1,0)<<"\t"<<coner[3](2,0)<<endl;
					
					ImgBND_file<<photo_j.ID<<"_4\t"<<endl;
					ImgBND_file<<coner[3](0,0)<<"\t"<<coner[3](1,0)<<"\t"<<coner[3](2,0)<<endl;
					ImgBND_file<<coner[0](0,0)<<"\t"<<coner[0](1,0)<<"\t"<<coner[0](2,0)<<endl;
				}
				
			}
		}
	}
};

#endif // !defined(AFX_SMPHOTOSIMULATION_H__55F6F494_4E08_4E31_9C9C_4133338587D0__INCLUDED_)
