/*
* Copyright (c) 2002-2006, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

// OrthoApproximateModelClass.h
//
//////////////////////////////////////////////////////////////////////

/**
* @class COrthoApproximateModelClass
* @brief Revision: 2012-05-18\n
* This class is for generating true ortho-photos using approximate sensor models.\n
* @author Bang, Ki In
* @version 1.0
* @Since 2012-05~
* @bug N/A.
* @warning N/A.
*
* \nThis class supports two approximate sensor models:
* <ul> 
* <li> Parallel projection model\n
*		 Slope method uses the line from each DEM point to perspective center to fine the occlusion area.\n
* <li> Line scanner DLT model\n
* </ul>\n
*/

#if !defined(AFX_COrthoApproximateModelClass_)
#define AFX_COrthoApproximateModelClass_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <SMGrid.h>
#include <SMCoordinateClass.h>
#include <Collinearity.h>
#include <SMList.h>
#include <ProgressBar.h>

#define FIRST_RECORD -999
#define BOUNDARY_MARGIN 100.0
#define _MAXDIFFROW_ 0.5
#define DEFAULT_EMPTY_DEM -99999.999
#define EXTREMEMINSIZE 1.0e-99

#define NUM_PP_PARAMS 8
#define NUM_LDLT_PARAMS 11
#define NUM_RFM_PARAMS 90

#define MINZ -99999.0
#define MAXZ 99999.0

enum ModelType {PP, MPP, FDLT, LDLT, SDLT, RFM};

class __ApproximateModel__
{
	
public:
	__ApproximateModel__ () { A = NULL;}
	virtual ~__ApproximateModel__ () { if(A != NULL) delete[] A; }
	
public:
	double * A;
	
};

class __ParallelProjection__ : public __ApproximateModel__
{
	/*[Parallel Projection]<br>
	/*row = A[0]*X + A[1]*Y + A[2]*Z + A[3]
	/*col = A[4]*X + A[5]*Y + A[6]*Z + A[7]
	*/
public:
	__ParallelProjection__ () { A = new double [NUM_PP_PARAMS]; }
	virtual ~__ParallelProjection__ () {}
};

class __LineScannerDLT__ : public __ApproximateModel__
{
	/*[Pushbroom DLT]<br>
	/*row = (A1X + A2Y + A3Z + A4)<br>
	/*col = (A5X + A6Y + A7Z + A8)/(A9X + A10Y + A11Z +1)
	*/
public:
	__LineScannerDLT__ () { A = new double [NUM_LDLT_PARAMS]; }
	virtual ~__LineScannerDLT__ () {}
};

class __LineScannerRFM__ : public __ApproximateModel__
{
	/*[RFM]<br>
	/*       f1<br>
	/*line = --<br>
	/*       f3<br>
	/*<br>
	/*         f2<br>
	/*sample = --<br>
	/*         f3<br>
	/*f = A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +<br>
	/*	  A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +<br>
	/*	  A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H<br>
	*/

public:
	__LineScannerRFM__ () { A = new double [NUM_RFM_PARAMS]; }
	virtual ~__LineScannerRFM__ () {}
public:
	double *dOffset, *dScale;
	double *dRowNUM, *dRowDEN;
	double *dColNUM, *dColDEN;
};

class COrthoApproximateModelClass
{	
public:
	COrthoApproximateModelClass() { OriginImage = NULL; ASM = NULL; m_DSM = new CSMGridDEM<double>; };
	virtual ~COrthoApproximateModelClass() { if(ASM != NULL) delete ASM; delete m_DSM; };
	
	bool MakeOrtho(CImage &oriImage, CString parampath, 
		CString DSMPath, CString HDRPath, 
		CString OrthoPath, 
		bool bBilinear, 
		bool bRemoveOcclu, 
		ModelType type)
	{
		OriginImage = &oriImage;
		
		int bitcount = OriginImage->GetBitCount();
		if((bitcount != 8)&&(bitcount != 24))
		{
			AfxMessageBox("This program supports only 8bit gray scale images and 24bit color images.");
			return false;
		}
		
		if(false==ReadModelParam(parampath, type)) return false;
		ReadDSM(DSMPath, HDRPath);
		
		dem_W = m_DSM->GetWidth();
		dem_H = m_DSM->GetHeight();
		
		DSM_res_XY = sqrt(m_DSM->resolution_X*m_DSM->resolution_X + m_DSM->resolution_Y*m_DSM->resolution_Y);
		
		//min and max height of DEM
		MinZ = MAXZ;
		MaxZ = MINZ;
		for(int i=0; i<(int)m_DSM->GetHeight(); i++)
		{
			for(int j=0; j<(int)m_DSM->GetWidth(); j++)
			{
				double z;
				if(false == m_DSM->GetZ(j,i,z)) continue;
				if(z == m_DSM->val_fail) continue;
				if(z == m_DSM->val_back) continue;
				if(z<MinZ) MinZ = z;
				if(z>MaxZ) MaxZ = z;
			}
		}
		
		//Start time
		DWORD dwStart = GetTickCount();
		
		if(bitcount == 8)
			ResampleOrtho(OrthoPath, bBilinear, bRemoveOcclu, type);
		else if(bitcount == 24)
			AfxMessageBox("Color images are not supported yet.");
		else
		{
			CString msg;
			msg.Format("Wrong bit/pixel!");
			AfxMessageBox(msg);
			return false;
		}
		
		DWORD ElapsedTime = GetTickCount() - dwStart;
		
		CString msg;
		msg.Format("Elapsed Time: %lf",(double)ElapsedTime/(double)1000);
		AfxMessageBox(msg);
		
		return true;
	}
		
protected:
		
	/**GroundCoord2SceneCoord
	* Description	    : To transform ground coordinates to scene coordinates
	*@param double X: ground X coordinate
	*@param double Y: ground Y coordinate
	*@param double Z: ground Z coordinate
	*@param __ApproximateModel__: sensor model (abstract class)
	*@param double &Ys: perspective center coordinates(Yt) at certain line
	*@param double &Zs: perspective center coordinates(Zt) at certain line
	*@param double &row: image coordinate (row)
	*@param double &col: image coordinate (col)
	*@param double init_r: Initial approximate line number
	*@return bool 
	*/
	bool GroundCoord2SceneCoord(double X, double Y, double Z, __ApproximateModel__ *ASM, ModelType type, double &col, double &row)
	{
		switch(type)
		{
		case PP:
			{
				double* A = static_cast<__ParallelProjection__*>(ASM)->A;
				
				row = A[0]*X + A[1]*Y + A[2]*Z + A[3];
				col = A[4]*X + A[5]*Y + A[6]*Z + A[7];
				
				break;
			}
		case LDLT:
			{
				double* A = static_cast<__LineScannerDLT__*>(ASM)->A;
				
				row = A[0]*X + A[1]*Y + A[2]*Z + A[3];
				col = (A[4]*X + A[5]*Y + A[6]*Z + A[7])/( A[8]*X + A[9]*Y + A[10]*Z + 1.0 );
				
				break;
			}
		case RFM:
			{
				__LineScannerRFM__ *param = static_cast<__LineScannerRFM__*>(ASM);
				
				//Lat (Y)
				//Lon (X)
				double NX = (X - param->dOffset[3])/param->dScale[3];
				double NY = (Y - param->dOffset[2])/param->dScale[2];
				double NZ = (Z - param->dOffset[4])/param->dScale[4];
				
				double row_numer	= GetPolyValue(NX, NY, NZ, param->dRowNUM);
				double col_numer	= GetPolyValue(NX, NY, NZ, param->dColNUM);
				double row_denom	= GetPolyValue(NX, NY, NZ, param->dRowDEN);
				double col_denom	= GetPolyValue(NX, NY, NZ, param->dColDEN);
				
				double Nrow = row_numer/row_denom;
				double Ncol = col_numer/col_denom;
				
				row = Nrow*param->dScale[0] + param->dOffset[0];
				col = Ncol*param->dScale[1] + param->dOffset[1];
				
				break;
			}
		default:
			{
				return false;
				break;
			}
		}
	
		return true;
	}
	
	/**GetXY_PP
	* Description	    : To calculate X and Y coordinate using sensor model parameters and Z coordinates
	*@param double * A	: sensor model parameterrs
	*@param double Z	: Z coordinates
	*@param double & X	: X coordinates to be determined
	*@param double & Y	: Y coordinates to be determined
	*@return bool
	*/
	bool GetXY_PP(double* A, double sample, double line, double Z, double &X, double &Y)
	{
		//line = A[0]*X + A[1]*Y + A[2]*Z + A[3]
		//sample = A[4]*X + A[5]*Y + A[6]*Z + A[7]

		double K1 = line - A[2]*Z - A[3];
		double K2 = sample - A[6]*Z - A[7];
		double temp = A[1]*A[4] - A[0]*A[5];

		if(temp*A[0] == 0.)
			return false;

		Y = (A[4]*K1 - A[0]*K2)/temp;
		X = (K1 - A[1]*Y)/A[0];

		return true;
	}

	/**GetXY_LDLT
	* Description	    : To calculate X and Y coordinate using sensor model parameters and Z coordinates
	*@param double * A	: sensor model parameterrs
	*@param double Z	: Z coordinates
	*@param double & X	: X coordinates to be determined
	*@param double & Y	: Y coordinates to be determined
	*@return bool
	*/
	bool GetXY_LDLT(double* A, double sample, double line, double Z, double &X, double &Y)
	{
		//line = A0X + A1Y + A2Z + A3
		//sample = (A4X + A5Y + A6Z + A7)/(A8X + A9Y + A10Z +1)

		double K1 = line - A[2]*Z - A[3];
		double K2 = (A[6]*Z + A[7]) - sample*(A[10]*Z + 1.0);
		double m1 = sample*A[8] - A[4];
		double m2 = sample*A[9] - A[5];

		if(A[1] == 0.)
			return false;

		if((m1 - m2*A[0]/A[1]) == 0.)
			return false;

		X = (K2 - K1/A[1]*m2) / (m1 - m2*A[0]/A[1]);
		Y = K1/A[1] - A[0]/A[1]*X;

		return true;
	}

	/**GetPolyValue
	* Description	    : To calculate a result of a 3rd order polynomial
	*@param double Lon	: Longitude or X coordinate
	*@param double Lat	: Latitude or Y coordinate
	*@param double H	: Height value
	*@param double * A	: sensor model parameterrs
	*@return double		: polynomial result
	*/
	double GetPolyValue(double Lon, double Lat, double H, double *A)
	{
		//Lon, Lat, H ==> X, Y, Z
		double ans;
		
		ans = A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
			A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
			A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;
		
		return ans;
	}

	/**GetDifferentialValue_Lat
	* Description	    : To calculate a patial differential coefficient w.r.t Lat
	*@param double Lon	: Longitude or X coordinate
	*@param double Lat	: Latitude or Y coordinate
	*@param double H	: Height value
	*@param double * A	: sensor model parameterrs
	*@return double		: polynomial result
	*/
	double GetDifferentialValue_Lat(double Lon, double Lat, double H, double *A)
	{
		//Lon, Lat, H ==> X, Y, Z
		double ans;
		
		ans = A[2] + A[4]*Lon + A[6]*H + 2*A[8]*Lat +
			A[10]*Lon*H + 2*A[12]*Lat*Lon + A[14]*Lon*Lon +
			3*A[15]*Lat*Lat + A[16]*H*H + 2*A[18]*Lat*H;
		
		return ans;
	}
	
	/**GetDifferentialValue_Lon
	* Description	    : To calculate a patial differential coefficient w.r.t Lon
	*@param double Lon	: Longitude or X coordinate
	*@param double Lat	: Latitude or Y coordinate
	*@param double H	: Height value
	*@param double * A	: sensor model parameterrs
	*@return double		: polynomial result
	*/
	double GetDifferentialValue_Lon(double Lon, double Lat, double H, double *A)
	{
		//Lon, Lat, H ==> X, Y, Z
		double ans;
		
		ans = A[1] + A[4]*Lat + A[5]*H + 2*A[7]*Lon +
			A[10]*Lat*H + 3*A[11]*Lon*Lon + A[12]*Lat*Lat +
			A[13]*H*H + 2*A[14]*Lat*Lon + 2*A[17]*Lon*H;
		
		return ans;
	}

	/**GetXY_RFM
	* Description					: To calculate X and Y coordinate using sensor model parameters and Z coordinates
	*@param double Z				: Z coordinates
	*@param double & X				: X coordinates to be determined
	*@param double & Y				: Y coordinates to be determined
	*@param int max_iter_num		: Maximum iteration number; Default is 10.
	*@param double max_correction	: Maximum correction for unknowns
	*@return bool
	*/
	bool GetXY_RFM(__LineScannerRFM__ *param, double Sample, double Line, double Xinit, double Yinit, double Z, double &X, double &Y, double max_correction, int max_iter_num)
	{
		//Lat (Y)
		//Lon (X)
		
		double NXinit = (Xinit - param->dOffset[3])/param->dScale[3];
		double NYinit = (Yinit - param->dOffset[2])/param->dScale[2];
		double NZ = (Z - param->dOffset[4])/param->dScale[4];


		double Nline	= (Line - param->dOffset[0])/param->dScale[0];
		double Nsample	= (Sample - param->dOffset[1])/param->dScale[1];
		
		//RFM form
		//       f1
		//line = --
		//       f3
		//
		//         f2
		//sample = --
		//         f3
		//f = A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
		//	  A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
		//	  A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;
		
		double *dRowNUM = param->dRowNUM;
		double *dColNUM = param->dColNUM;
		double *dRowDEN = param->dRowDEN;
		double *dColDEN = param->dColDEN;

		CSMMatrix<double> Amat(2,2), Xmat, Lmat(2,1), AmatT;
		double line_numer, sample_numer, line_denomi, sample_denomi, line_denomi2, sample_denomi2;

		double D_Line_Numer_Lon, D_Line_Denomi_Lon, D_Line_Numer_Lat, D_Line_Denomi_Lat;
		double D_Sample_Numer_Lon, D_Sample_Denomi_Lon, D_Sample_Numer_Lat, D_Sample_Denomi_Lat;
		double dLine_dLon, dLine_dLat, dSample_dLon, dSample_dLat;

		bool bContinue = true;

		int iter_count = 0;

		do {
			iter_count ++;

			//Lat (Y)
			//Lon (X)
			line_numer		= GetPolyValue(NXinit, NYinit, NZ, dRowNUM);
			sample_numer	= GetPolyValue(NXinit, NYinit, NZ, dColNUM);
			line_denomi		= GetPolyValue(NXinit, NYinit, NZ, dRowDEN);
			sample_denomi	= GetPolyValue(NXinit, NYinit, NZ, dColDEN);

			if(line_denomi == 0. || sample_denomi == 0.)
				return false;
			
			line_denomi2	= line_denomi*line_denomi;
			sample_denomi2	= sample_denomi*sample_denomi;
						
			D_Line_Numer_Lon	= GetDifferentialValue_Lon(NXinit, NYinit, NZ, dRowNUM);
			D_Line_Denomi_Lon	= GetDifferentialValue_Lon(NXinit, NYinit, NZ, dRowDEN);
			D_Line_Numer_Lat	= GetDifferentialValue_Lat(NXinit, NYinit, NZ, dRowNUM);
			D_Line_Denomi_Lat	= GetDifferentialValue_Lat(NXinit, NYinit, NZ, dRowDEN);
			
			D_Sample_Numer_Lon	= GetDifferentialValue_Lon(NXinit, NYinit, NZ, dColNUM);		
			D_Sample_Denomi_Lon	= GetDifferentialValue_Lon(NXinit, NYinit, NZ, dColDEN);
			D_Sample_Numer_Lat	= GetDifferentialValue_Lat(NXinit, NYinit, NZ, dColNUM);
			D_Sample_Denomi_Lat = GetDifferentialValue_Lat(NXinit, NYinit, NZ, dColDEN);
			
			dLine_dLon = (D_Line_Numer_Lon*line_denomi - D_Line_Denomi_Lon*line_numer)/line_denomi2;
			dLine_dLat = (D_Line_Numer_Lat*line_denomi - D_Line_Denomi_Lat*line_numer)/line_denomi2;
			
			dSample_dLon = (D_Sample_Numer_Lon*sample_denomi - D_Sample_Denomi_Lon*sample_numer)/sample_denomi2;
			dSample_dLat = (D_Sample_Numer_Lat*sample_denomi - D_Sample_Denomi_Lat*sample_numer)/sample_denomi2;
			
			Amat(0,0) = dLine_dLon;//Line-Lon
			Amat(0,1) = dLine_dLat;//Line-Lat
			
			Amat(1,0) = dSample_dLon;//Sample-Lon
			Amat(1,1) = dSample_dLat;//Sample-Lat
			
			Lmat(0,0) = Nline	- line_numer	/line_denomi;
			Lmat(1,0) = Nsample	- sample_numer	/sample_denomi;
			
			AmatT = Amat.Transpose();
			
			Xmat = (AmatT%Amat).Inverse()%AmatT%Lmat;
			
			NXinit += Xmat(0,0);
			NYinit += Xmat(1,0);

			if(fabs(Xmat(0,0)) < max_correction && fabs(Xmat(1,0)) < max_correction)
				bContinue = false;

		} while(bContinue && iter_count < max_iter_num);

		//Lat (Y)
		//Lon (X)
		X = NXinit*param->dScale[3] + param->dOffset[3];
		Y = NYinit*param->dScale[2] + param->dOffset[2];
		
		if(iter_count < max_iter_num)
			return true;
		else
			return false;
	}

	/**OpenIKONOSRPC
	* Description				: To read RPC file (IKONOS format)
	*@param LPCTSTR rpcpath		: file path
	*@return bool
	*/
	bool OpenIKONOSRPC(LPCTSTR rpcpath) 
	{
		FILE *RPCFile;
		
		char separate = ':';
		
		LPCTSTR st = rpcpath;
		
		RPCFile = fopen(st,"rt");
		
		char c;
		unsigned short index = 0;
		do
		{
			c = fgetc(RPCFile);
			
			if(c == EOF)
				break;
			if(separate == c)
			{
				fscanf(RPCFile,"%lf",&(ASM->A[index]));
				index++;
			}
			
		}while(c != EOF);
		
		fclose(RPCFile);
		
		__LineScannerRFM__ *param = static_cast<__LineScannerRFM__*>(ASM);

		param->dOffset = ASM->A;
		param->dScale  = ASM->A+5;
		param->dRowNUM = ASM->A+10;
		param->dRowDEN = ASM->A+30;
		param->dColNUM = ASM->A+50;
		param->dColDEN = ASM->A+70;
		
		return true;
	}
		
	/**ReadModelParam
	* Description				: To read sensor model parameters file
	*@param CString parampath	: file path
	*@param ModelType type		: sensor model type
	*@return bool
	*/
	bool ReadModelParam(CString parampath, ModelType type)
	{
		if(type == RFM)
		{
			ASM = new __LineScannerRFM__;
			
			if(false == OpenIKONOSRPC(parampath) )
				return false;
			else
				return true;
		}

		fstream infile;
		infile.open(parampath, ios::in);
		
		//version check
		int pos;
		bool bVersion = FindString(infile, "VER", pos);
		if(bVersion == true)
		{
			//To move to begin of file
			infile.seekg(0,ios::beg);	
			char temp[_MAX_STRING_LENGTH_];//"VER"
			infile>>temp>>number_ver;//version	
		}
		else
		{
			//To move to begin of file
			infile.seekg(0,ios::beg);//old version without version number(before ver 1.1)
		}
		
		
		//               row = x = flight direction
		//               ^
		//               |
		//|------------- |---------------|--------->col = y
		//|              |               |
		//|              |               |
		//|------------- |---------------|
		//|              |               |
		//|              |               |
		//|------------- |---------------|
		//
		//Flight direction = row = x
		//
		
		int num_params;

		if(type == PP)
		{
			num_params = NUM_PP_PARAMS;
			ASM = new __ParallelProjection__;
		}
		else if(type == LDLT)
		{
			num_params = NUM_LDLT_PARAMS;
			ASM = new __LineScannerDLT__;
		}
		else
			return false;
		
		for(int i=0; i<num_params; i++)
		{
			infile>>ASM->A[i];
		}
		
		return true;
	}

	/**ReadDSM
	* Description				: To read DSM
	*@param CString parampath	: file path
	*@param ModelType type		: sensor model type
	*@return bool
	*/	
	bool ReadDSM(CString DSMPath, CString DSMHDRPath) 
	{
		FILE *hdr;
		hdr = fopen(DSMHDRPath,"r");
		double offset, sX, sY;
		int w, h;
		double backval, falseval;
		
		if(EOF == fscanf(hdr,"%d",&w)) return false;
		if(EOF == fscanf(hdr,"%d",&h)) return false;
		if(EOF == fscanf(hdr,"%lf",&offset)) return false;
		if(EOF == fscanf(hdr,"%lf",&sX)) return false;
		if(EOF == fscanf(hdr,"%lf",&sY)) return false;
		if(EOF == fscanf(hdr,"%lf",&backval)) return false;
		if(EOF == fscanf(hdr,"%lf",&falseval)) return false;
		
		fclose(hdr);
		
		m_DSM->SetConfig(w,h,sX,sY,offset,offset,falseval,backval);
		
		DSMPath.MakeUpper();

		if(DSMPath.Find(".DEM") > -1) 
			return m_DSM->ReadDEM(DSMPath);
		else if(DSMPath.Find(".TXT") > -1) 
			return m_DSM->ReadTextXYZDEM(DSMPath);
		else 
			return false;
	}

	bool GetXY(double sample, double line, double X0, double Y0, double Z, double &X, double &Y, ModelType type)
	{
		bool retval;

		switch(type)
		{
		case PP:
			{
				retval = GetXY_PP(ASM->A, sample, line, Z, X, Y);
				break;
			}
		case LDLT:
			{
				retval = GetXY_LDLT(ASM->A, sample, line, Z, X, Y);
				break;
			}
		case RFM:
			{
				__LineScannerRFM__ *param = static_cast<__LineScannerRFM__*>(ASM);
				retval = GetXY_RFM(param, sample, line, X0, Y0, Z, X, Y, 0.0001, 10);
				break;
			}
		default:
			{
				return false;
			}
		}

		return retval;
	}

	bool CheckOcclusion(double col, double row, int indexX, int indexY, ModelType type)
	{
		//
		//Calculate a ray direction (direction vector)
		//

		//Target ground point (DSM point)
		double Xa = m_DSM->GetX(indexX);
		double Ya = m_DSM->GetY(indexY);
		double Za = m_DSM->GetZ(indexX,indexY);
		
		double MinX, MinY;
		if( false == GetXY(col, row, Xa, Ya, MinZ, MinX, MinY, type) ) 
			return false;

		double MaxX, MaxY;
		if( false == GetXY(col, row, Xa, Ya, MaxZ, MaxX, MaxY, type) ) 
			return false;

		double dX = MaxX - MinX;
		double dY = MaxY - MinY;
		double dZ = MaxZ - MinZ;

		double length = sqrt(dX*dX + dY*dY + dZ*dZ);
		double length_XY = sqrt(dX*dX + dY*dY);

		if(length <= 0)
			return false;

		double l, m, n;

		l = (MaxX - MinX) / length;
		m = (MaxY - MinY) / length;
		n = (MaxZ - MinZ) / length;

		//
		//Check occlusion
		//

		double X=Xa, Y=Ya;
		double limitZ = Za;

		double offset_length = length * DSM_res_XY / length_XY;

		double offsetX = offset_length * l;
		double offsetY = offset_length * m;
		double offsetZ = offset_length * n;

		do
		{
			limitZ += offsetZ;
			X += offsetX;
			Y += offsetY;

			// 1. Check a maximum height value
			if( limitZ >= MaxZ)
				return true; //Visible		

			// 2. Check index boundaries after getting thoses
			double IdxX, IdxY;
			if( false == m_DSM->GetIndex(X, Y, IdxX, IdxY) )
				return false; //In-visible (actually, it cannot be determined because of out-of-boundary)
			
			double Z;
			if( false == m_DSM->GetZ_Bilinear(IdxX, IdxY, Z) )//false or background DSM values
				continue; //Skip a non-available DSM point

			// 3. Check occluded DSM cells
			if( limitZ < Z )
				return false; //In-Visible

		}while (1);

		return false; //Something wrong !
	}

	bool CheckOcclusion_LDLT(double col, double row, int indexX, int indexY, ModelType type)
	{
		double* A = static_cast<__LineScannerDLT__*>(ASM)->A;
				
		//Target ground point (DSM point)
		double Xa = m_DSM->GetX(indexX);
		double Ya = m_DSM->GetY(indexY);
		double Za = m_DSM->GetZ(indexX,indexY);

		if(A[2] == 0.0)
			return false;

		if((A[6]-col*A[10]) == 0.0)
			return false;

		double Zl = (row - (A[0]*Xa + A[1]*Ya + A[3]))/A[2];
		double Zs = ((col*A[8]-A[4])*Xa + (col*A[9]-A[5])*Ya + (col-A[7])) / (A[6]-col*A[10]);
		
		double K  = A[1]*(col*A[10]-A[6])+A[2]*(A[5]-col*A[9]);

		if(K == 0.0)
			return false;

		double N1 = A[0]*(A[6]-col*A[10])+A[2]*(col*A[8]-A[4]);
		double N2 = row*(col*A[10]-A[6])+A[3]*(A[6]-col*A[10])+A[2]*(col-A[7]);

		//double Y = X*N1/K + N2/K;

		double abs_slope = fabs(N1/K);

		//
		//Calculate a ray direction (direction vector)
		//
		
		double MinX, MinY;
		if( false == GetXY(col, row, Xa, Ya, MinZ, MinX, MinY, type) ) 
			return false;

		double MaxX, MaxY;
		if( false == GetXY(col, row, Xa, Ya, MaxZ, MaxX, MaxY, type) ) 
			return false;

		double dX = MaxX - MinX;
		double dY = MaxY - MinY;
		double dZ = MaxZ - MinZ;

		double length = sqrt(dX*dX + dY*dY + dZ*dZ);
		double length_XY = sqrt(dX*dX + dY*dY);

		if(length <= 0)
			return false;

		double l, m, n;

		l = (MaxX - MinX) / length;
		m = (MaxY - MinY) / length;
		n = (MaxZ - MinZ) / length;

		//
		//Check occlusion
		//

		double X=Xa, Y=Ya;
		double limitZ = Za;

		double offset_length = length * DSM_res_XY / length_XY;

		double offsetX = offset_length * l;
		double offsetY = offset_length * m;
		double offsetZ = offset_length * n;

		do
		{
			limitZ += offsetZ;
			X += offsetX;
			Y += offsetY;

			double calZ_L = (row - (A[0]*X + A[1]*Y + A[3]))/A[2];
			//double calZ_S = ((col*A[8]-A[4])*X + (col*A[9]-A[5])*Y + (col-A[7])) / (A[6]-col*A[10]);
			//double limitZ_ = (calZ_L + calZ_S)/2.0;
			limitZ = calZ_L;

			// 1. Check a maximum height value
			if( limitZ >= MaxZ)
				return true; //Visible		

			// 2. Check index boundaries after getting thoses
			double IdxX, IdxY;
			if( false == m_DSM->GetIndex(X, Y, IdxX, IdxY) )
				return false; //In-visible (actually, it cannot be determined because of out-of-boundary)
			
			double Z;
			if( false == m_DSM->GetZ_Bilinear(IdxX, IdxY, Z) )//false or background DSM values
				continue; //Skip a non-available DSM point

			// 3. Check occluded DSM cells
			if( limitZ < Z )
				return false; //In-Visible

		}while (1);

		return false; //Something wrong !
	}

	bool ResampleOrtho(CString OrthoPath, bool bBilinear, bool bOcclusion, ModelType type)
	{
		CImage OrthoImage;
		OrthoImage.Create(m_DSM->GetWidth(), m_DSM->GetHeight(),8);
		CPixelPtr OrthoPixel(OrthoImage);
		CPixelPtr OriginPixel(*OriginImage);
		
		CString strProgressBar = "Resampling";
		CProgressBar bar(strProgressBar, 100, OrthoImage.GetHeight());
		
		DWORD W, H;
		W = (DWORD)OriginImage->GetWidth();
		H = (DWORD)OriginImage->GetHeight();
		
		for(DWORD i=0;i<(DWORD)m_DSM->GetHeight();i++)
		{
			for(DWORD j=0;j<(DWORD)m_DSM->GetWidth();j++)
			{
				double X, Y, Z;
								
				// 1. Get X, Y, and Z
				X = m_DSM->GetX(j);
				Y = m_DSM->GetY(i);
				Z = m_DSM->GetZ(j,i);
				
				// 2. Check flase and background values
				if((Z == m_DSM->val_fail  || Z == m_DSM->val_back)||(Z < 0.0))//failed or background values
				{
					OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//Balck
					continue;
				}

				// 3. Get image coordinates
				double row, col;
				
				if(false == GroundCoord2SceneCoord(X, Y, Z, ASM, type, col, row))
				{
					OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;
					continue;
				}

				// 4. Check an image boundary
				if(((col < 0.0) || (row < 0.0)) || ((col >= double(W-1)) || (row >= double(H-1))))//out of boundary
				{
					continue;
				}

				// 5. Check visibility
				if(bOcclusion == true)
				{
					if(type == LDLT)
					{
						if(false == CheckOcclusion_LDLT(col, row, j, i, type))//In-visible
						{
							OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//"black"
							continue;
						}
					}

					if( false == CheckOcclusion(col, row, j, i, type) )//In-visible
					{
						OrthoPixel[LONG(i)][LONG(j)] = (BYTE)0;//"black"
						continue;
					}
				}

				// 6. Get a pixel value
				if(bBilinear != true)//Nearest inerpolation
				{
					OrthoPixel[LONG(i)][LONG(j)] = OriginPixel[LONG((double)row+0.5)][LONG((double)col+0.5)];
				}
				else //Bilinear interpolation
				{
					double value1, value2, value3, value4;
					value1 = (double)OriginPixel[LONG(row)][LONG(col)];
					value2 = (double)OriginPixel[LONG(row)][LONG(col+1.0)];
					value3 = (double)OriginPixel[LONG(row+1.0)][LONG(col)];
					value4 = (double)OriginPixel[LONG(row+1.0)][LONG(col+1.0)];

					double A, B, C, D;
					A = (double)((int)col + 1) - col;
					B = 1.0 - A;
					C = (double)((int)row + 1) - row;
					D = 1.0 - C;

					OrthoPixel[LONG(i)][LONG(j)] = BYTE(value1*A*C + value2*B*C + value3*A*D + value4*B*D);
				}
			}

			bar.StepIt();
		}

		if(FALSE == OrthoImage.Save(OrthoPath))
			return false;

		return true;
	}

protected:
	__ApproximateModel__ *ASM;
	
	double number_ver;/*version number of input file*/
	
	CImage *OriginImage;/**< Original Image*/
	CSMGridDEM<double>* m_DSM;/**<DSM */
	
	double MinZ, MaxZ;
	int dem_W;
	int dem_H;
	double DSM_res_XY;
};

#endif // !defined(AFX_COrthoApproximateModelClass_)
