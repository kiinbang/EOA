#pragma once

#include <iostream>
using namespace std;

#include "KDTree.h"
#include "SMRotationMatrix.h"
#include "SMMatrixClass.h"
#include "utilitygrocery_2.0.h"
using namespace SMATICS_BBARAB;

class CSMImgLasFusion
{
public:

	CSMImgLasFusion(void)
	{
	}

	~CSMImgLasFusion(void)
	{
	}

	/* GetCentroid
	Get a centroid of a point cloud
	*/
	bool GetCentroid(CSMList<KDNode> nodes, double &cx, double &cy, double &cz)
	{
		int num_points = nodes.GetNumItem();

		double sum_x=0, sum_y=0, sum_z=0;

		KDNode node;

		for(int i=0; i<num_points; i++)
		{
			node = nodes[i];
			sum_x += node.Get_x_(0);
			sum_y += node.Get_x_(1);
			sum_z += node.Get_x_(2);
		}

		cx = sum_x/num_points;
		cy = sum_y/num_points;
		cz = sum_z/num_points;

		return true;
	}

	/* GetNormal
	Get a normal vector of a point cloud with a centroid
	*/
	bool GetNormal(CSMList<KDNode> nodes, CSMMatrix<double> &NVector, double &cx, double &cy, double &cz)
	{
		int num_points = nodes.GetNumItem();

		if(num_points <3) return false;

		cx=0;
		cy=0;
		cz=0;

		GetCentroid(nodes, cx, cy, cz);

		CSMMatrix<double> A(num_points, 3), X, L(num_points, 1);
		CSMMatrix<double> N, C;

		KDNode node;

		for(int i=0; i<num_points; i++)
		{
			node = nodes[i];
			A(i, 0) = node.Get_x_(0) - cx + 1.0;
			A(i, 1) = node.Get_x_(1) - cy + 1.0;
			A(i, 2) = node.Get_x_(2) - cz + 1.0;

			L(i, 0) = 1.0;
		}

		N = (A.Transpose()%A);
		C = (A.Transpose()%L);

		X = N.Inverse()%C;

		CSMMatrix<double> V = A%X - L;
		double sigma = sqrt((V.Transpose()%V)(0,0));

		double temp = 1-(X(0,0)+X(1,0)+X(2,0));
		X = X/temp;

		double norm = sqrt(X(0,0)*X(0,0) + X(1,0)*X(1,0) + X(2,0)*X(2,0));
		NVector = X/norm;

		return true;
	}

	/* ForwardProjection_PlaneFit
	Get a clesest point to a perspective center using KDTree buffer search and plane fitting
	*/
	bool ForwardProjection_PlaneFit(fstream &log, KDTree *pKDTree, _Rmat_ Rpho2obj, CSMMatrix<double> PC, double pix_size, double x, double y, double f, double *XYZ, double increment, double search_radius, double max_dist, double dist_th, int min_num_points)
	{
		//Camera ray vector
		CSMMatrix<double> Cam_Vec(3,1);
		Cam_Vec(0,0) = x;
		Cam_Vec(1,0) = y;
		Cam_Vec(2,0) = -f;

		//Unit vector of a ray
		CSMMatrix<double> Direction_Vec = Rpho2obj.Rmatrix % Cam_Vec;
		Direction_Vec = CalUnitVector(Direction_Vec);//Unit vector of a ray

		CSMMatrix<double> lmn;//Normal vector of a plane
		KDNode *root = pKDTree->Root;
		CSMMatrix<double> next_XYZ;

		//log << Direction_Vec(0,0) * max_dist << "\t";
		//log << Direction_Vec(1,0) * max_dist << "\t";
		//log << Direction_Vec(2,0) * max_dist << endl;

		for(double dist2obj = increment; dist2obj<max_dist; dist2obj += increment)
		{
			//1. Next search point
			next_XYZ = PC + Direction_Vec * dist2obj;
			//log << "! next_XYZ: " <<endl;
			//log << next_XYZ(0,0) << "\t" << next_XYZ(1,0) << "\t" << next_XYZ(2,0) << endl;
			///[END 1.]/////////////////////////////////////////////////////////////////////

			//2. serch points within the buff
			double low[3];
			low[0] = next_XYZ(0,0) - search_radius;
			low[1] = next_XYZ(1,0) - search_radius;
			low[2] = next_XYZ(2,0) - search_radius;

			double high[3];
			high[0] = next_XYZ(0,0) + search_radius;
			high[1] = next_XYZ(1,0) + search_radius;
			high[2] = next_XYZ(2,0) + search_radius;

			int level = 0;
			CSMList<KDNode> ptlist;

			pKDTree->search(low, high, root, level, ptlist);

			//log << "! dist2obj: " << dist2obj << endl;
			//log << "! ptlist: " << ptlist.GetNumItem() << endl;
			//for(int i=0; i<ptlist.GetNumItem(); i++)
			//{
			//log <<ptlist[i].Get_x_(0) << "\t";
			//log <<ptlist[i].Get_x_(1) << "\t";
			//log <<ptlist[i].Get_x_(2) << endl;		
			//}

			if((int)ptlist.GetNumItem() < min_num_points)
				continue;

			//4. Find a plane
			CSMMatrix<double> Centre(3,1);//centroid
			//x' = x - cx, y'= y - cy, z' = z - cz
			//l*x' + m*y' + n*z' = 0 <-- a plane through an origin of (x',y',z') coordinate system
			if(GetNormal(ptlist,lmn, Centre(0,0), Centre(1,0), Centre(2,0)) == false)
				continue;
			//origin shift: cx, cy, cz
			// l*x' + m*y' + n*z' = 0 ---> l*(x-cx) + m*(y-cy) + n*(z-cz) = 0 ---> l*x + m*y + n*z  = d = (l*cx + m*cy +n*cz)
			// where (l, m, n) normal vector and sqrt(l*l+m*m+n*n) = 1.0
			//d = (l*cx + m*cy +n*cz) which is same to distance to origin
			double d;//distance to origin
			d = lmn(0,0) * Centre(0,0) + lmn(1,0) * Centre(1,0) + lmn(2,0) * Centre(2,0);
			///[END 4.]/////////////////////////////////////////////////////////////////////

			//5. Intersection point
			//x = x0 + t.u
			//y = y0 + t.v
			//z = z0 + t.w
			//l*x + m*y + n*z  = d
			//l(x0 + u*t) + m(y0 + v*t) + n(z0 + w*t) - d = 0;
			//t = -(l*x0 + l*y0 + l*z0 - d)/(l*u + l*v + l*w)
			double t;
			double Den = lmn(0,0) * PC(0,0) + lmn(1,0) * PC(1,0) + lmn(2,0) * PC(2,0) - d;
			double Num = lmn(0,0) * Direction_Vec(0,0) + lmn(1,0) * Direction_Vec(1,0) + lmn(2,0) * Direction_Vec(2,0);

			if(Num == 0.0)
				continue;

			t = -Den/Num;

			if(t < 0)
				return false;

			CSMMatrix<double> Intersect = PC + (Direction_Vec * t);
			XYZ[0] = Intersect(0,0);
			XYZ[1] = Intersect(1,0);
			XYZ[2] = Intersect(2,0);

			//Normal distance to a plane
			//Way to the origin shift (x0, y0, z0)
			//(x2 = x-x0, y2 = y-y0, z2 = z-z0)
			//l*(x2+x0) + m*(y2+y0) + n*(z2+z0)  = d
			//l*x2 + m*y2 + n*z2 = d - (l*x0 + m*y0 + n*z0)

			double norm_dist = d - (lmn(0,0)*XYZ[0] + lmn(1,0)*XYZ[1] + lmn(2,0)*XYZ[2]);

			//if(t > increment || t < 0)
			//if(fabs(t) > increment)
			//	continue;

			//log << "Norm: " << norm_dist << "\t";
			//log << XYZ[0] << "\t"<< XYZ[1] << "\t"<< XYZ[2] << "\t";
			//log << PC(0,0) << "\t" << PC(1,0) << "\t" << PC(2,0) << "\t";
			//log << Direction_Vec(0,0) << "\t" << Direction_Vec(1,0) << "\t" << Direction_Vec(2,0) << "\t";
			//log << "t: " << t << endl;


			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			/*
			log << "Num pts: " << ptlist.GetNumItem() << endl;

			for(int i=0; i<ptlist.GetNumItem(); i++)
			{
			log <<ptlist[i].Get_x_(0) << "\t";
			log <<ptlist[i].Get_x_(1) << "\t";
			log <<ptlist[i].Get_x_(2) << "\t";		

			normal_distance = ( NVector.x * ptlist[i].Get_x_(0) + NVector.y * ptlist[i].Get_x_(1) + NVector.z * ptlist[i].Get_x_(2) + d ) / temp;

			log <<normal_distance << endl;		
			}

			log << "NVector and d: " << NVector.x << "\t" << NVector.y << "\t" << NVector.z << "\t" << d << endl;
			log << "Centre: " << Centre(0,0) << "\t" << Centre(1,0) << "\t" << Centre(2,0) << endl;
			log << "intersection: " << Intersect(0,0) << "\t" << Intersect(1,0) << "\t" << Intersect(2,0) << endl;
			log << "Normal distance: " << normal_distance << endl;
			*/

			return true;
		}

		return false;
	}

	/* ForwardProjection_Nearest
	Get a clesest point to a perspective center using KDTree nearest search and plane fitting
	It is slower than ForwardProjection_PlaneFit
	*/
	bool ForwardProjection_Nearest(fstream &log, KDTree *pKDTree, _Rmat_ Rpho2obj, CSMMatrix<double> PC, double x, double y, double f, double *XYZ, double increment, double search_radius, double max_dist, double dist_th, KDNode **pNode)
	{
		//Camera ray vector
		CSMMatrix<double> Cam_Vec(3,1);
		Cam_Vec(0,0) = x;
		Cam_Vec(1,0) = y;
		Cam_Vec(2,0) = -f;

		//Unit vector of a ray
		CSMMatrix<double> Direction_Vec = Rpho2obj.Rmatrix % Cam_Vec;
		Direction_Vec = CalUnitVector(Direction_Vec);//Unit vector of a ray

		CSMMatrix<double> NVector(3,1);//Normal vector of a plane
		KDNode *root = pKDTree->Root;
		CSMMatrix<double> next_XYZ;

		double P0[3], Pa[3], Pb[3];
		double dx, dy, dz;
		CSMMatrix<double> lmn(3,1);//Normal vector of a plane

		Pa[0] = PC(0,0);
		Pa[1] = PC(1,0);
		Pa[2] = PC(2,0);

		Pb[0] = PC(0,0) + Direction_Vec(0,0) * max_dist;
		Pb[1] = PC(1,0) + Direction_Vec(1,0) * max_dist;
		Pb[2] = PC(2,0) + Direction_Vec(2,0) * max_dist;

		double low[3], high[3];
		double dist_to_next_point;

		for(double dist2obj = increment; dist2obj<max_dist; dist2obj += increment)
		{
			//1. Next search point
			next_XYZ = PC + Direction_Vec * dist2obj;
			//log << "! next_XYZ: " <<endl;
			//log << next_XYZ(0,0) << "\t" << next_XYZ(1,0) << "\t" << next_XYZ(2,0) << endl;
			///[END 1.]/////////////////////////////////////////////////////////////////////

			//2. search the nearest point
			(*pNode) = NULL;
			*pNode = pKDTree->find_nearest(next_XYZ.GetDataHandle());

			if((*pNode) == NULL)
				continue;

			P0[0] = (*pNode)->Get_x_(0);
			P0[1] = (*pNode)->Get_x_(1);
			P0[2] = (*pNode)->Get_x_(2);

			//Distance between next_point and the nearest point
			dx = P0[0] - next_XYZ(0,0);
			dy = P0[1] - next_XYZ(1,0);
			dz = P0[2] - next_XYZ(2,0);

			//Check distance
			dist_to_next_point = sqrt(dx*dx + dy*dy + dz*dz);
			if(dist_to_next_point > dist_th)
				continue;

			//Find intersection of ray and the nearest point
			if(false == IntersectPointBetweenOnePointandOneLine(P0, Pa, Pb, XYZ))
				continue;

			//3. serch points within the buff
			low[0] = XYZ[0] - search_radius;
			low[1] = XYZ[1] - search_radius;
			low[2] = XYZ[2] - search_radius;	

			high[0] = XYZ[0] + search_radius;
			high[1] = XYZ[1] + search_radius;
			high[2] = XYZ[2] + search_radius;

			int level = 0;
			CSMList<KDNode> ptlist;

			pKDTree->search(low, high, root, level, ptlist);

			//log << "! dist2obj: " << dist2obj << endl;
			//log << "! ptlist: " << ptlist.GetNumItem() << endl;
			//for(int i=0; i<ptlist.GetNumItem(); i++)
			//{
			//log <<ptlist[i].Get_x_(0) << "\t";
			//log <<ptlist[i].Get_x_(1) << "\t";
			//log <<ptlist[i].Get_x_(2) << endl;		
			//}

			if(ptlist.GetNumItem() < 50)
				continue;

			//4. Find a plane
			CSMMatrix<double> Centre(3,1);//centroid
			//x' = x - cx, y'= y - cy, z' = z - cz
			//l*x' + m*y' + n*z' = 0 <-- a plane through an origin of (x',y',z') coordinate system
			if(GetNormal(ptlist,lmn, Centre(0,0), Centre(1,0), Centre(2,0)) == false)
				continue;
			//origin shift: cx, cy, cz
			// l*x' + m*y' + n*z' = 0 ---> l*(x-cx) + m*(y-cy) + n*(z-cz) = 0 ---> l*x + m*y + n*z  = d = (l*cx + m*cy +n*cz)
			// where (l, m, n) normal vector and sqrt(l*l+m*m+n*n) = 1.0
			//d = (l*cx + m*cy +n*cz) which is same to distance to origin
			double d;//distance to origin
			d = lmn(0,0) * Centre(0,0) + lmn(1,0) * Centre(1,0) + lmn(2,0) * Centre(2,0);
			///[END 4.]/////////////////////////////////////////////////////////////////////

			//5. Intersection point
			//x = x0 + t.u
			//y = y0 + t.v
			//z = z0 + t.w
			//l*x + m*y + n*z  = d
			//l(x0 + u*t) + m(y0 + v*t) + n(z0 + w*t) - d = 0;
			//t = -(l*x0 + l*y0 + l*z0 - d)/(l*u + l*v + l*w)
			double t;
			double Den = lmn(0,0) * PC(0,0) + lmn(1,0) * PC(1,0) + lmn(2,0) * PC(2,0) - d;
			double Num = lmn(0,0) * Direction_Vec(0,0) + lmn(1,0) * Direction_Vec(1,0) + lmn(2,0) * Direction_Vec(2,0);

			if(Num == 0.0)
				continue;

			t = -Den/Num;

			if(t < 0)
				return false;

			CSMMatrix<double> Intersect = PC + (Direction_Vec * t);
			XYZ[0] = Intersect(0,0);
			XYZ[1] = Intersect(1,0);
			XYZ[2] = Intersect(2,0);

			//Normal distance to a plane
			//Way to the origin shift (x0, y0, z0)
			//(x2 = x-x0, y2 = y-y0, z2 = z-z0)
			//l*(x2+x0) + m*(y2+y0) + n*(z2+z0)  = d
			//l*x2 + m*y2 + n*z2 = d - (l*x0 + m*y0 + n*z0)

			double norm_dist = d - (lmn(0,0)*XYZ[0] + lmn(1,0)*XYZ[1] + lmn(2,0)*XYZ[2]);

			return true;
		}

		return false;
	}
};

