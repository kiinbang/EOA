/*
 * Copyright (c) 2005-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(__POINT_CLOUD_HANDLER__BBARAB__)
#define __POINT_CLOUD_HANDLER__BBARAB__

#include "SMCoordinateClass.h"
#include "SMList.h"
#include "SMDataStruct.h"

using namespace SMATICS_BBARAB;

/**@class CSMPointCloud<br>
*@brief point cloud data handler.
*/
class CSMPointCloud
{
public:
	
	/**CSMPointCloud
	* Description	    : constructor
	* Return type		: 
	*/
	CSMPointCloud();

	/**~CSMPointCloud
	* Description	    : destructor
	* Return type		: 
	*/
	virtual ~CSMPointCloud();

	/**ReadTXTPoints
	* Description			: To read point cloud data
	*@param CString fname	: input raw data
	*@param bool Intensity	: Availability of intensity information
	* Return type			: void
	*/
	void ReadTXTPoints(CString fname, bool bIntensity, bool bNormalize);

	void ReadPTSPoints(CString fname, bool bIntensity, bool bNormal);

	bool ReadPTS(CString ptsfilename);


	
	/**ReadTXTPoints
	* Description			: To read point cloud data
	*@param CString fname	: input raw data
	*@param CString cfgfile	: input configuration data
	* Return type			: void
	*/

	/**PTS2XYZPoints
	* Description				: To read Terrapoint PTS data
	*@param CString ptsfilename	: input path (pts file)
	*@param CString xyzfilename	: output path (xyz file)
	* Return type				: void
	*/
	void PTS2XYZPoints(CString ptsfilename, CString xyzfilename);

	/**ExtractPTS
	* Description				: To extract points from PTS file using given 4 corner points
	*@param CString ptsfilename	: input path (pts file)
	*@param CString outptsfilename	: output path (pts file)
	*@param double LT	: left top coordinates
	*@param double RB	: right bottom coordinates
	* Return type				: void
	*/
	void ExtractPTS(CString ptsfilename, CString outptsfilename, double LT_X, double LT_Y, double RB_X, double RB_Y);

	/**ExtractPTS
	* Description				: To extract points from PTS file using given polygon
	*@param CString ptsfilename	: input path (pts file)
	*@param CString outptsfilename	: output path (pts file)
	*@param double* x	: x coordinates of a polygon
	*@param double* y	: y coordinates of a polygon
	*@param int num_vertices : number of vertices of a given polygon
	* Return type				: void
	*/
	void ExtractPTS(CString ptsfilename, CString outptsfilename, double* x, double* y, int num_vertices);
	

	/**ExtractPTS
	* Description				: To extract points from PTS file using given radius
	*@param CString ptsfilename	: input path (pts file)
	*@param CString outptsfilename	: output path (pts file)
	*@param int num_sphere	: number of given spheres
	*@param double* radius	: radiuses of spheres
	*@param double* CX	: X coordinates of centers of spheres
	*@param double* CY	: Y coordinates of centers of spheres
	*@param double* CZ	: Z coordinates of centers of spheres
	* Return type				: void
	*/
	void ExtractPTS(CString ptsfilename, CString outptsfilename, int num_sphere, double* radius, double* CX, double* CY, double* CZ);

	/**MakeIntensityImageFromPTS
	* Description				: To make intensity image from PTS file
	*@param CString ptspath	: input path (pts file)
	*@param CString imgpath	: output path (bmp image file)
	*@param double minX	: minimum X coordinate of Given PTS data
	*@param double maxX	: maximum X coordinate of Given PTS data
	*@param double minY	: minimum Y coordinate of Given PTS data
	*@param double maxY	: maximum Y coordinate of Given PTS data
	*@param double minintensity	: minimum intensity of Given PTS data
	*@param double maxintensity	: maximum intensity of Given PTS data
	*@param double res	: intensity image pixel resolution
	* Return type				: bool
	*/
	bool MakeIntensityImageFromPTS(CString ptspath, CString imgpath, double minX, double maxX, double minY, double maxY, double minintensity, double maxnintensity, double res, bool bStretch);

	/**MakeRangeImageFromPTS
	* Description				: To make range image from PTS file
	*@param CString ptspath	: input path (pts file)
	*@param CString imgpath	: output path (bmp image file)
	*@param double minX	: minimum X coordinate of Given PTS data
	*@param double maxX	: maximum X coordinate of Given PTS data
	*@param double minY	: minimum Y coordinate of Given PTS data
	*@param double maxY	: maximum Y coordinate of Given PTS data
	*@param double minrange	: minimum range of Given PTS data
	*@param double maxrange	: maximum range of Given PTS data
	*@param double res	: range image pixel resolution
	* Return type				: bool
	*/
	bool MakeRangeImageFromPTS(CString ptspath, CString imgpath, double minX, double maxX, double minY, double maxY, double minrange, double maxrange, double res);

	/**PTSStatistics
	* Description				: To get statistics values from PTS file (min and max values for each column)
	*@param CString ptspath	: input path (pts file)
	*@param CString outpath	: output path (txt file)	* Return type				: bool
	*/
	bool PTSStatistics(CString ptspath, CString outpath);

	/**PTS2Points
	* Description				: To get points clouds from raw data given by PTS file
	*@param CString ptspath	: input path (pts file)
	*@param CString outpath	: output path (txt file)	* Return type				: bool
	*/
	bool PTS2Points(CString ptspath, CString outpath);




	static bool SeperateTXT(int numlines, CString ptspath, CString outpath);
	static bool MergeTXT(CString ptspath1, CString ptspath2, CString outpath);

	static bool PlaneFitting(double threshold, CSMList<_PTS_Point_> &pointlist, double &a, double &b, double &c, double &omega, double &phi, double &pos_var, double &dist2origin, double *errordist, int maxiteration);


	static bool PlaneFitting(CSMList<_PTS_Point_> &pointlist, CSMList<_PTS_Point_> &resultpointlist, double a, double b, double c, double omega, double phi);

public:
	CSMList<_Point_> m_Points; /**<point cloud*/
	CSMList<_PTS_Point_> m_Pts; /**<point cloud (Terrapoint PTS data)*/
	double MaxX, MinX;
	double MaxY, MinY;
	double MaxZ, MinZ;
	int MinI, MaxI;
	double ShiftX, ShiftY, ShiftZ;
	bool bNormalCoord;
};
#endif // !defined(__POINT_CLOUD_HANDLER__BBARAB__)