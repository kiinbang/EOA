// SpacematicsRFM.cpp: implementation of the CRFM class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SpacematicsRFM.h"

/*****************************************************************************/
/************************Class for RFM Sensor Model***************************/
/*****************************************************************************/

//default constructor
CRFM::CRFM()
{
	Init();
}

//copy constructor
CRFM::CRFM(CRFM &copy)
{
	unsigned short i;

	for(i=0; i<NUM_RPC;i++)
	{
		dColDEN[i] = copy.dColDEN[i];
		dColNUM[i] = copy.dColNUM[i];
		dRowDEN[i] = copy.dRowDEN[i];
		dRowNUM[i] = copy.dRowNUM[i];

		old_dColDEN[i] = copy.old_dColNUM[i];
		old_dColNUM[i] = copy.old_dColDEN[i];
		old_dRowDEN[i] = copy.old_dRowDEN[i];
		old_dRowNUM[i] = copy.old_dRowNUM[i];
	}

	for(i=0; i<NUM_NORMALIZE_PARAM; i++)
	{
		dCoordOffset[i] = copy.dCoordOffset[i];
		dCoordScale[i] = copy.dCoordScale[i];
	}

	mGCP = copy.mGCP;
}

//substitution operator
void CRFM::operator = (CRFM &copy)
{
	unsigned short i;

	for(i=0; i<NUM_RPC;i++)
	{
		dColDEN[i] = copy.dColDEN[i];
		dColNUM[i] = copy.dColNUM[i];
		dRowDEN[i] = copy.dRowDEN[i];
		dRowNUM[i] = copy.dRowNUM[i];

		old_dColDEN[i] = copy.old_dColNUM[i];
		old_dColNUM[i] = copy.old_dColDEN[i];
		old_dRowDEN[i] = copy.old_dRowDEN[i];
		old_dRowNUM[i] = copy.old_dRowNUM[i];
	}

	for(i=0; i<NUM_NORMALIZE_PARAM; i++)
	{
		dCoordOffset[i] = copy.dCoordOffset[i];
		dCoordScale[i] = copy.dCoordScale[i];
	}

	mGCP = copy.mGCP;

}

//destructor
CRFM::~CRFM()
{
}

//Initialize RFM
void CRFM::Init(void)
{
	unsigned short i;

	additional_transform = false;

	for(i=0; i<NUM_RPC;i++)
	{
		dColDEN[i] = 0;
		dColNUM[i] = 0;
		dRowDEN[i] = 0;
		dRowNUM[i] = 0;

		old_dColDEN[i] = 0;
		old_dColNUM[i] = 0;
		old_dRowDEN[i] = 0;
		old_dRowNUM[i] = 0;
	}

	for(i=0; i<NUM_NORMALIZE_PARAM; i++)
	{
		dCoordOffset[i] = 0;
		dCoordScale[i] = 0;
	}

	mGCP.mG.EmptyGCP();
	mGCP.mI.EmptyICP();

}

//IKONOS RPC Open
bool CRFM::OpenIKONOSRPC(LPCTSTR rpcpath) 
{
	FILE *RPCFile;
	double dCoeff[90];
			
	char separate = ':';

	LPCTSTR st = rpcpath;
	
	RPCFile = fopen(st,"rt");

	char c;
	unsigned short index = 0;
	do
	{
		c = fgetc(RPCFile);
		
		if(c == EOF)
			break;
		if(separate == c)
		{
			fscanf(RPCFile,"%lf",(dCoeff+index));
			index++;
		}

	}while(c != EOF);

	fclose(RPCFile);

	//Setting RFM Parameters
	SetParameters(dCoeff+50,dCoeff+70,dCoeff+10,dCoeff+30,dCoeff,dCoeff+5);

	return true;
}

//IKONOS RPC Write
bool CRFM::WriteIKONOSRPC(LPCTSTR rpcpath) 
{
	FILE *RPCFile;
	int i;

	LPCTSTR st = rpcpath;
	
	RPCFile = fopen(st,"wt");

	fprintf(RPCFile, "LINE_OFF: %10.15le pixels\n", dCoordOffset[0]);
	fprintf(RPCFile, "SAMP_OFF: %10.15le pixels\n", dCoordOffset[1]);
	fprintf(RPCFile, "LAT_OFF: %10.15le degrees\n", dCoordOffset[2]);
	fprintf(RPCFile, "LONG_OFF: %10.15le degrees\n", dCoordOffset[3]);
	fprintf(RPCFile, "HEIGHT_OFF: %10.15le meters\n", dCoordOffset[4]);

	fprintf(RPCFile, "LINE_SCALE: %10.15le pixels\n", dCoordScale[0]);
	fprintf(RPCFile, "SAMP_SCALE: %10.15le pixels\n", dCoordScale[1]);
	fprintf(RPCFile, "LAT_SCALE: %10.15le degrees\n", dCoordScale[2]);
	fprintf(RPCFile, "LONG_SCALE: %10.15le degrees\n", dCoordScale[3]);
	fprintf(RPCFile, "HEIGHT_SCALE: %10.15le meters\n", dCoordScale[4]);

	for(i=0; i<20; i++)
	{
		fprintf(RPCFile, "LINE_NUM_COEFF_%d: %10.15le\n", i+1, dRowNUM[i]);
	}
	for(i=0; i<20; i++)
	{
		fprintf(RPCFile, "LINE_DEN_COEFF_%d: %10.15le\n", i+1, dRowDEN[i]);
	}
	for(i=0; i<20; i++)
	{
		fprintf(RPCFile, "SAMP_NUM_COEFF_%d: %10.15le\n", i+1, dColNUM[i]);
	}
	for(i=0; i<20; i++)
	{
		fprintf(RPCFile, "SAMP_DEN_COEFF_%d: %10.15le\n", i+1, dColDEN[i]);
	}

	fclose(RPCFile);

	return true;
}

//Get IKONOS RPC
CString CRFM::GetIKONOSRPCasString() 
{
	CString stRPC="";
	CString temp;
	
	temp.Format("LINE_OFF: %10.15le pixels\r\n", dCoordOffset[0]); stRPC += temp;
	temp.Format("SAMP_OFF: %10.15le pixels\r\n", dCoordOffset[1]); stRPC += temp;
	temp.Format("LAT_OFF: %10.15le degrees\r\n", dCoordOffset[2]); stRPC += temp;
	temp.Format("LONG_OFF: %10.15le degrees\r\n", dCoordOffset[3]); stRPC += temp;
	temp.Format("HEIGHT_OFF: %10.15le meters\r\n", dCoordOffset[4]); stRPC += temp;

	temp.Format("LINE_SCALE: %10.15le pixels\r\n", dCoordScale[0]); stRPC += temp;
	temp.Format("SAMP_SCALE: %10.15le pixels\r\n", dCoordScale[1]); stRPC += temp;
	temp.Format("LAT_SCALE: %10.15le degrees\r\n", dCoordScale[2]); stRPC += temp;
	temp.Format("LONG_SCALE: %10.15le degrees\r\n", dCoordScale[3]); stRPC += temp;
	temp.Format("HEIGHT_SCALE: %10.15le meters\r\n", dCoordScale[4]); stRPC += temp;

	int i;
	for(i=0; i<20; i++)
	{
		temp.Format("LINE_NUM_COEFF_%d: %10.15le\r\n", i+1, dRowNUM[i]); stRPC += temp;
	}
	for(i=0; i<20; i++)
	{
		temp.Format("LINE_DEN_COEFF_%d: %10.15le\r\n", i+1, dRowDEN[i]); stRPC += temp;
	}
	for(i=0; i<20; i++)
	{
		temp.Format("SAMP_NUM_COEFF_%d: %10.15le\r\n", i+1, dColNUM[i]); stRPC += temp;
	}
	for(i=0; i<20; i++)
	{
		temp.Format("SAMP_DEN_COEFF_%d: %10.15le\r\n", i+1, dColDEN[i]); stRPC += temp;
	}

	return stRPC;
}

//set parameters for RFM
void CRFM::SetParameters(/*array size NUM_RPC*/double *Col_NUM,
                         /*array size NUM_RPC*/double *Col_DEN,
					     /*array size NUM_RPC*/double *Row_NUM,
					     /*array size NUM_RPC*/double *Row_DEN,
				         /*offset value array size 5*/double *Offset, 
				         /*scale value array size 5*/double *Scale)
{
	unsigned int i;
	
	for(i=0; i<(NUM_RPC); i++)
	{
		dColNUM[i] = old_dColNUM[i] = Col_NUM[i];
		dColDEN[i] = old_dColDEN[i] = Col_DEN[i];
		dRowNUM[i] = old_dRowNUM[i] = Row_NUM[i];
		dRowDEN[i] = old_dRowDEN[i] = Row_DEN[i];
	}

	for(i=0; i<(NUM_NORMALIZE_PARAM); i++)
	{
		dCoordOffset[i] = Offset[i];
	}

	for(i=0; i<(NUM_NORMALIZE_PARAM); i++)
	{
		dCoordScale[i] = Scale[i];
	}
}

/*Get Image Coordinates*/
void CRFM::GetGroundCoord2ImageCoord(double Lat, double Lon, double H, double *col, double *row)
{
	double col_, row_;

	GetGroundCoord2NormalImageCoord(Lat, Lon, H, &col_, &row_);

	*col = col_*dCoordScale[1] + dCoordOffset[1];
	*row = row_*dCoordScale[0] + dCoordOffset[0];
}

/*Get Improved Image Coordinates*/
void CRFM::GetImprovedGroundCoord2ImageCoord(double Lat, double Lon, double H, double *col, double *row)
{
	if(true == additional_transform)
	{
		double col_, row_;
		double col_2, row_2;
		
		GetGroundCoord2NormalImageCoord(Lat, Lon, H, &col_, &row_);
		
		col_2 = col_*dCoordScale[1] + dCoordOffset[1];
		row_2 = row_*dCoordScale[0] + dCoordOffset[0];
		
		Point2D<double> P;
		P.x = col_2;
		P.y = row_2;
		
		Point2D<double> Q = trans->CoordTransform(P);
		
		*col = Q.x;
		*row = Q.y;
	}
}

/*Get Normalized Image Coordinates*/
void CRFM::GetGroundCoord2NormalImageCoord(double Lat, double Lon, double H, double *Ncol, double *Nrow)
{
	*Ncol = GetColNumerator(Lat,Lon,H)/GetColDenominator(Lat,Lon,H);
	*Nrow = GetRowNumerator(Lat,Lon,H)/GetRowDenominator(Lat,Lon,H);
}

/*Get Normalized Image Coordinates*/
void CRFM::GetImageCoord2NormalImageCoord(double col, double row, double *Ncol, double *Nrow)
{
	*Ncol = (col - dCoordOffset[1])/dCoordScale[1];
	*Nrow = (row - dCoordOffset[0])/dCoordScale[0];
}

/*Get Image Coordinates from normalized image coord*/
void CRFM::GetNormalImageCoord2ImageCoord(double Ncol, double Nrow, double *col, double *row)
{
	*col = Ncol*dCoordScale[1] + dCoordOffset[1];
	*row = Nrow*dCoordScale[0] + dCoordOffset[0];
}

/*Get Normalized Ground Coordinates*/
void CRFM::GetGroundCoord2NormalGroundCoord(double Lat, double Lon, double H, double *NLat, double *NLon, double *NH)
{
	*NLon = (Lon - dCoordOffset[3])/dCoordScale[3];
	*NLat = (Lat - dCoordOffset[2])/dCoordScale[2];
	*NH   = (H   - dCoordOffset[4])/dCoordScale[4];
}

/*Get Ground Coordinates from Normalized Ground Coord*/
void CRFM::GetNormalGroundCoord2GroundCoord(double NLat, double NLon, double NH, double *Lat, double *Lon, double *H)
{
	*Lon = NLon*dCoordScale[3]  + dCoordOffset[3];
	*Lat = NLat*dCoordScale[2]  + dCoordOffset[2];
	*H   = NH*dCoordScale[4]    + dCoordOffset[4];
}

/*Get Normalized Image Coordinates from Normalized Lat, Lon, H*/
void CRFM::GetNormalGroundCoord2NormalImageCoord(double NLat, double NLon, double NH, double *Ncol, double *Nrow)
{
	double CN, CD, RN, RD;

	CN = GetPolyValue(NLat,NLon,NH,dColNUM);
	CD = GetPolyValue(NLat,NLon,NH,dColDEN);
	
	RN = GetPolyValue(NLat,NLon,NH,dRowNUM);
	RD = GetPolyValue(NLat,NLon,NH,dRowDEN);

	*Ncol = CN/CD;
	*Nrow = RN/RD;
}

/*Get Col Denominator(분모)*/
double CRFM::GetColDenominator(double Lat_, double Lon_, double H_)
{
	double ans;
	double Lat, Lon, H;

	Lon = (Lon_ - dCoordOffset[3])/dCoordScale[3];
	Lat = (Lat_ - dCoordOffset[2])/dCoordScale[2];
	H   = (H_   - dCoordOffset[4])/dCoordScale[4];

	ans = GetPolyValue(Lat,Lon,H,dColDEN);
	
	return ans;
}

/*Get Col Numerator(분자)*/
double CRFM::GetColNumerator(double Lat_, double Lon_, double H_)
{
	double ans;
	double Lat, Lon, H;

	Lon = (Lon_-dCoordOffset[3])/dCoordScale[3];
	Lat = (Lat_-dCoordOffset[2])/dCoordScale[2];
	H   = (H_  -dCoordOffset[4])/dCoordScale[4];

	ans = GetPolyValue(Lat,Lon,H,dColNUM);
	
	return ans;
}

/*Get Row Denominator(분모)*/
double CRFM::GetRowDenominator(double Lat_, double Lon_, double H_)
{
	double ans;
	double Lat, Lon, H;

	Lon = (Lon_-dCoordOffset[3])/dCoordScale[3];
	Lat = (Lat_-dCoordOffset[2])/dCoordScale[2];
	H   = (H_  -dCoordOffset[4])/dCoordScale[4];

	ans = GetPolyValue(Lat,Lon,H,dRowDEN);
	
	return ans;
}

/*Get Row Numerator(분자)*/
double CRFM::GetRowNumerator(double Lat_, double Lon_, double H_)
{
	double ans;
	double Lat, Lon, H;

	Lon = (Lon_-dCoordOffset[3])/dCoordScale[3];
	Lat = (Lat_-dCoordOffset[2])/dCoordScale[2];
	H   = (H_  -dCoordOffset[4])/dCoordScale[4];

	ans = GetPolyValue(Lat,Lon,H,dRowNUM);	
	
	return ans;
}

/*Set Data Cubic*/
bool CRFM::SetDataCubic(void)
{
	int i, j, k, n;
	double x, y, z;
		
	for(i=0; i<3; i++)
	{
		z = -1.0 + 2.0*(i+1)/4.0;
		for(j=0; j<3; j++)
		{
			x = -1.0 + 2.0*(j+1)/4.0;
			for(k=0; k<3; k++)
			{
				y = -1.0 + 2.0*(k+1)/4.0;
				n = i*9 + j*3 + k;
				DC_u[n] = x;
				DC_v[n] = y;
				DC_w[n] = z;
			}
		}
	}

	DC_u[27] = -0.9;
	DC_v[27] = 0.9;
	DC_w[27] = 0.9;

	DC_u[28] = 0.9;
	DC_v[28] = -0.9;
	DC_w[28] = 0.9;

	DC_u[29] = 0.9;
	DC_v[29] = -0.9;
	DC_w[29] = -0.9;

	for(i=0; i<NUMDC; i++)
	{
		GetNormalGroundCoord2NormalImageCoord(DC_u[i],DC_v[i],DC_w[i],(DC_x+i),(DC_y+i));
	}

	return true;
}

/*Get Data Cubic*/
bool CRFM::GetDataCubic(double *u, double *v, double *w, double *x, double *y)
{
	u = DC_u;
	v = DC_v;
	w = DC_w;
	x = DC_x;
	y = DC_y;

	return true;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebyDataCubic(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	//Data Cubic Setting
	SetDataCubic();

	/****************/
	/*Additional GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "";
	double *u, *v, *w;
	double *x, *y;
	unsigned int i;

	unsigned int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();

		u = new double[numGCP];
		v = new double[numGCP];
		w = new double[numGCP];

		x = new double[numGCP];
		y = new double[numGCP];
	}
	else
	{
		return "GCP number error";
	}

	//variance 계산용
	double _col_, _row_;
	double sum_ex, sum_ey;
	sum_ex = sum_ey = 0.0;

	/*Ground Coord Normalization*/
	for(i=0; i<numGCP; i++)
	{
		double lat, lon, h;
		double c,r;

		//Get GCP coord
		mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);

		//Get Image coord
		mGCP.mI.GetICPCoordbySort(i+1,&c,&r);

		//Ground coord normalize
		GetGroundCoord2NormalGroundCoord(lat,lon,h,(u+i),(v+i),(w+i));

		//Image coord normalize
		GetImageCoord2NormalImageCoord(c,r,(x+i),(y+i));

		//Ground coord to image coord
		GetGroundCoord2ImageCoord(lat,lon,h,&_col_,&_row_);

		sum_ex += (c - _col_)*(c - _col_);
		sum_ey += (r - _row_)*(r - _row_);
	}

	sum_ex = sum_ex/numGCP;
	sum_ey = sum_ey/numGCP;

	if(numGCP == 0)
	{
		sum_ex = sum_ey = 1.0;
	}

	/**************/
	/*LS Iteration*/
	/**************/
	bool MaxCorStop = false;
	bool MaxIterStop = false;
	bool VarStop = false;

	double old_var1 = 0;
	double old_var2 = 0;
	double new_var = 0;
	
	unsigned int num_iter = 0;
	int n;
	
	Matrix<double> A, L, X, W, V, VTV;
	Matrix<double> N, Ninv;
	double variance;
	
	//Resize of ACol and LCol matrix
	A.Resize((numGCP+NUMDC)*2,NUMRPCPARAM,0.0);
	L.Resize((numGCP+NUMDC)*2,1,0.0);
	W.Resize((numGCP+NUMDC)*2,(numGCP+NUMDC)*2,0.0);

	//weight setting
	for(n=0; n<NUMDC; n++)
	{
		W(n*2,n*2) = 1/sum_ex;
		W(n*2+1,n*2+1) = 1/sum_ey;
	}

	for(n=0; n<(int)(numGCP); n++)
	{
		W(n*2+(NUMDC*2),n*2+(NUMDC*2)) = 1/(sd*sd);
		W(n*2+1+(NUMDC*2),n*2+1+(NUMDC*2)) = 1/(sd*sd);
	}
	
	do
	{
		num_iter ++;

		if(1<num_iter)
		{
			Matrix<double> W_ = VarCov_L;
			int r,c;
			r = W_.GetRows();
			c = W_.GetCols();
			double value;
			value = W_(0,0);
			value = W_(c-1,r-1);
			for(n=0; (unsigned int)n<((NUMDC+numGCP)*2); n++)
			{
				//W(n,n) = 1.0/W_(n,n)*W_(0,0);
				//W(n,n) = W_(n,n);
			}
		}

		//Put message
		msg.Format("[%d]Iteration\r\n",num_iter);
		out += msg;
				
		//Assemble of A matrix and L matrix
		MakeAandL(A,L,W,u,v,w,x,y);
		
		//Least square solution
		//tikhonov regularization
		Matrix<double> E;
		E.Resize(NUMRPCPARAM,NUMRPCPARAM,0.0);
		for(n=0;n<NUMRPCPARAM;n++)
			E(n,n) = 1.0;

		N = A.Transpose()%W%A + E*( h * h );
		Ninv = N.Inverse();
		X = Ninv%A.Transpose()%W%L;
		V = A%X - L;
		VTV = V.Transpose()%W%V;
		variance = VTV(0,0)/((NUMDC+numGCP)*2-NUMRPCPARAM);
		m_SD = sqrt(variance);
		VarCov_X = Ninv*variance;
		VarCov_L = A%VarCov_X%A.Transpose();

		//Put message
		msg.Format("-posteriori variance-\r\n%le\r\n",variance);
		out += msg;

		//RPC update
		for(n=0; n<20; n++)
		{
			dColNUM[n] += X(n,0);
		}
		for(n=20; n<40; n++)
		{
			dRowNUM[n-20] += X(n,0);
		}
		for(n=40; n<NUMRPCPARAM; n++)
		{
			dColDEN[n-39] += X(n,0);
			dRowDEN[n-39] += X(n,0);
		}

		double temp = fabs(X(0,0));
		for(n=1; n<NUMRPCPARAM; n++)
		{
			if(fabs(X(n,0))>temp)
				temp = fabs(X(n,0));
		}
		
		//Put message
		msg.Format("Maximum Correction: %lf\r\n",temp);
		out += msg;

		//variance monitoring
		//(1)old_vari2 (2)old_var1 (3)new_var
		if(num_iter == 1)
		{
			old_var1 = old_var2 = new_var = variance;
		}
		else if(num_iter == 2)
		{
			old_var1 = new_var = variance;
		}
		else
		{
			old_var2 = old_var1;
			old_var1 = new_var;
			new_var = variance;
		}

		if((old_var1>old_var2)&&(new_var>old_var1))
		{
			VarStop = true;

			msg.Format("variance continuous increase, two times: Last Variance[%le]",variance);
			out += msg;
		}

		//max correction term monitoring
		if(temp<fabs(MaxCorrection))
		{
			MaxCorStop = true;

			msg.Format("Max-Correction(%lf) OK: %lf\r\n",MaxCorrection,temp);
			out += msg;
		}
		
		//iteration number monitoring
		if(MaxIteration <= num_iter)
		{
			MaxIterStop = true;

			msg.Format("Max-Iteration Stop[%d]--> Last variance: %lf\r\n",MaxIteration,variance);
			out += msg;
		}

	}while((MaxCorStop == false)&&(MaxIterStop == false)&&(VarStop == false));

	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebyDataCubic2005(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	CSMStr out="";

	//Initial value
	out += "Initial values\r\n";
	out += DoRPCUpdatebyDataCubic2005_Init(MaxCorrection, MaxIteration, sd, h);
	//1st Order Terms
	out += "First order terms\r\n";
	out += DoRPCUpdatebyDataCubic2005_1(MaxCorrection, MaxIteration, sd, h);

	//2nd Order Terms
	out += "\r\nSecond order terms\r\n";
	out += DoRPCUpdatebyDataCubic2005_2(MaxCorrection, MaxIteration, sd, h);

	//3rd Order Terms
	out += "\r\nThird order terms\r\n";
	out += DoRPCUpdatebyDataCubic2005_3(MaxCorrection, MaxIteration, sd, h);

	//Bias remove
	DoRPCUpdatebyRemoveBias_Shift();

	return out;
}


/*RPC Update*/
CSMStr CRFM::DataNormalization()
{
	CSMStr out;
	out = "";

	double col, row;
	double maxcol, mincol, maxrow, minrow;

	unsigned int i;

	unsigned int numICP = mGCP.mI.GetICPNum();

	if(numICP != 0)
	{
		mGCP.mI.GetICPCoordbySort(1, &col,&row);

		maxcol = mincol = col;
		maxrow = minrow = row;
		
		for(i=1; i<numICP; i++)
		{
			mGCP.mI.GetICPCoordbySort(i+1, &col,&row);
			
			if(maxcol < col)
				maxcol = col;
			if(mincol > col)
				mincol = col;
			
			if(maxrow < row)
				maxrow = row;
			if(minrow > row)
				minrow = row;
		}
	}
	else
	{
		return out;
	}
	
	///////////////////////////////////////////////////////
	//*Ncol = (col - dCoordOffset[1])/dCoordScale[1];
	//*Nrow = (row - dCoordOffset[0])/dCoordScale[0];
	///////////////////////////////////////////////////////
	dCoordScale[0] = (maxrow-minrow)/2;
	dCoordScale[1] = (maxcol-mincol)/2;;

	dCoordOffset[0] = maxrow-dCoordScale[0];
	dCoordOffset[1] = maxcol-dCoordScale[1];

	CSMStr msg;
	msg.Format("ROW SCALE:%lf\r\n",dCoordScale[0]);
	out += msg;
	msg.Format("COL SCALE:%lf\r\n",dCoordScale[1]);
	out += msg;
	msg.Format("ROW OFFSET:%lf\r\n",dCoordOffset[0]);
	out += msg;
	msg.Format("COL OFFSET:%lf\r\n",dCoordOffset[1]);
	out += msg;

	double X, Y, Z;
	double maxX, minX, maxY, minY, maxZ, minZ;

	unsigned int numGCP = mGCP.mG.GetGCPNum();

	if(numGCP != 0)
	{
		mGCP.mG.GetGCPCoordbySort(1, &X,&Y,&Z);
		
		maxX = minX = X;
		maxY = minY = Y;
		maxZ = minZ = Z;
		
		for(i=1; i<numGCP; i++)
		{
			mGCP.mG.GetGCPCoordbySort(i+1, &X,&Y,&Z);
						
			if(maxX < X)
				maxX = X;
			if(minX > X)
				minX = X;
			
			if(maxY < Y)
				maxY = Y;
			if(minY > Y)
				minY = Y;
			
			if(maxZ < Z)
				maxZ = Z;
			if(minZ > Z)
				minZ = Z;
		}
	}
	else
	{
		return out;
	}

	///////////////////////////////////////////////////////
	//*NLon = (Lon - dCoordOffset[3])/dCoordScale[3];
	//*NLat = (Lat - dCoordOffset[2])/dCoordScale[2];
	//*NH   = (H   - dCoordOffset[4])/dCoordScale[4];
	//mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);
	///////////////////////////////////////////////////////
	dCoordScale[2] = (maxX-minX)/2;
	dCoordScale[3] = (maxY-minY)/2;;
	dCoordScale[4] = (maxZ-minZ)/2;;

	dCoordOffset[2] = maxX-dCoordScale[2];
	dCoordOffset[3] = maxY-dCoordScale[3];
	dCoordOffset[4] = maxZ-dCoordScale[4];

	msg.Format("Lat SCALE:%lf\r\n",dCoordScale[2]);
	out += msg;
	msg.Format("Lon SCALE:%lf\r\n",dCoordScale[3]);
	out += msg;
	msg.Format("H SCALE:%lf\r\n",dCoordScale[4]);
	out += msg;
	msg.Format("Lat OFFSET:%lf\r\n",dCoordOffset[2]);
	out += msg;
	msg.Format("Lon OFFSET:%lf\r\n",dCoordOffset[3]);
	out += msg;
	msg.Format("H OFFSET:%lf\r\n",dCoordOffset[4]);
	out += msg;
		
	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebyDataCubic2005_Init(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	CSMStr out;
	CSMStr msg;
	out = "";

	/********************/
	/*Data Noramlization*/
	/********************/
	DataNormalization();

	/********************/
	/*Parameters 0 setting*/
	/********************/
	for(int n=0; n<NUM_RPC; n++)
	{
		dColNUM[n] = 0;
		dColDEN[n] = 0;

		dRowNUM[n] = 0;
		dRowDEN[n] = 0;
	}
	
	/********************/
	/*DLT*/
	/********************/
	Matrix<double> A, L, X;
	int numGCP = mGCP.mG.GetGCPNum();
	A.Resize(2*numGCP,11,0);
	L.Resize(2*numGCP,1,0);

	//A[0] + A[1]*Lon + A[2]*Lat + A[3]*H

	for(n=0; n<numGCP; n++)
	{
		double lat, lon, h, c, r;
		double u, v, w, x, y;
		
		mGCP.mG.GetGCPCoordbySort(n+1,&lat,&lon,&h);
		mGCP.mI.GetICPCoordbySort(n+1,&c,&r);
		GetGroundCoord2NormalGroundCoord(lat,lon,h,&u,&v,&w);
		GetImageCoord2NormalImageCoord(c,r,&x,&y);

		A(n*2,0) =1;
		A(n*2,1) =u;
		A(n*2,2) =v;
		A(n*2,3) =w;
		A(n*2,8) =x*u;
		A(n*2,9) =x*v;
		A(n*2,10)=x*w;

		A(n*2+1,4) =1;
		A(n*2+1,5) =u;
		A(n*2+1,6) =v;
		A(n*2+1,7) =w;
		A(n*2+1,8) =y*u;
		A(n*2+1,9) =y*v;
		A(n*2+1,10)=y*w;

		L(n*2+0,0)=x;
		L(n*2+1,0)=y;
	}

	X = (A.Transpose()%A).Inverse()%A.Transpose()%L;

	//RPC update
	for(n=0; n<4; n++)
	{
		dColNUM[n] = X(n,0);
	}
	for(n=0; n<4; n++)
	{
		dRowNUM[n] = X(n+4,0);
	}

	dColDEN[0] = 1;
	dRowDEN[0] = 1;
	
	for(n=0; n<3; n++)
	{
		dColDEN[n+1] = X(n+8,0);
		dRowDEN[n+1] = X(n+8,0);
	}

	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebyDataCubic2005_3(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	//Before modifying all coefficients, first, remove the bias.
	//DoRPCUpdatebyRemoveBias_Shift();

	/****************/
	/*GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "";
	double *u, *v, *w;
	double *x, *y;
	unsigned int i;

	unsigned int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();

		u = new double[numGCP];
		v = new double[numGCP];
		w = new double[numGCP];

		x = new double[numGCP];
		y = new double[numGCP];
	}
	else
	{
		return "GCP number error";
	}

	//variance 계산용
	double _col_, _row_;

	/*Ground Coord Normalization*/
	for(i=0; i<numGCP; i++)
	{
		double lat, lon, h;
		double c,r;

		//Get GCP coord
		mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);

		//Get Image coord
		mGCP.mI.GetICPCoordbySort(i+1,&c,&r);

		//Ground coord normalize
		GetGroundCoord2NormalGroundCoord(lat,lon,h,(u+i),(v+i),(w+i));

		//Image coord normalize
		GetImageCoord2NormalImageCoord(c,r,(x+i),(y+i));

		//Ground coord to image coord
		GetGroundCoord2ImageCoord(lat,lon,h,&_col_,&_row_);
	}

	/**************/
	/*LS Iteration*/
	/**************/
	bool MaxCorStop = false;
	bool MaxIterStop = false;
	bool VarStop = false;

	double old_var1 = 0;
	double old_var2 = 0;
	double new_var = 0;
	
	unsigned int num_iter = 0;
	int n;
	
	Matrix<double> A, L, X, W, V, VTV;
	Matrix<double> N, Ninv;
	double variance;

	L.Resize((numGCP)*2,1,0.0);
	W.Resize((numGCP)*2,(numGCP)*2,0.0);

	for(n=0; n<(int)(numGCP); n++)
	{
		W(n*2,n*2) = 1/(sd*sd);
		W(n*2+1,n*2+1) = 1/(sd*sd);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	//3rd: 
	//A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H + A[14]*Lon*Lon*Lat + 
	//A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	MaxCorStop = false;
	MaxIterStop = false;
	VarStop = false;

	old_var1 = 0;
	old_var2 = 0;
	new_var = 0;

	num_iter = 0;


	//Resize of ACol and LCol matrix
	A.Resize((numGCP)*2,10*3,0.0);

	do
	{
		num_iter ++;

		//Put message
		msg.Format("[%d]Iteration\r\n",num_iter);
		out += msg;
				
		//Assemble of A matrix and L matrix
		MakeAandL_OnlyGCP3rd(A,L,W,u,v,w,x,y);
		
		//Least square solution
		//tikhonov regularization
		Matrix<double> E;
		E.Resize(10*3,10*3,0.0);
		for(n=0;n<10*3;n++)
			E(n,n) = 1.0;

		N = A.Transpose()%W%A + E*( h * h );
		//N = A.Transpose()%W%A;
		Ninv = N.Inverse();
		X = Ninv%A.Transpose()%W%L;
		V = A%X - L;
		VTV = V.Transpose()%W%V;
		variance = VTV(0,0)/((numGCP)*2-(10*3));
		m_SD = sqrt(variance);
		VarCov_X = Ninv*variance;
		VarCov_L = A%VarCov_X%A.Transpose();

		//Put message
		double post_sd = sqrt(variance);
		post_sd = post_sd*((dCoordScale[0]+dCoordScale[1])/2);
		msg.Format("-sd-\r\n%le\r\n",post_sd);
		out += msg;

		//RPC update
		for(n=0; n<10; n++)
		{
			dColNUM[n+10] += X(n,0);
		}
		for(n=0; n<10; n++)
		{
			dRowNUM[n+10] += X(n+10,0);
		}
		for(n=0; n<10; n++)
		{
			dColDEN[n+10] += X(n+20,0);
			dRowDEN[n+10] += X(n+20,0);
		}

		//maximum correction term
		double temp = fabs(X(0,0));
		for(n=1; n<(10*3); n++)
		{
			if(fabs(X(n,0))>temp)
				temp = fabs(X(n,0));
		}
		
		//Put message
		msg.Format("Maximum Correction: %le\r\n",temp);
		out += msg;

		//variance size monitoring
		if((post_sd*post_sd)<(sd*sd))
		{
			VarStop = true;

			msg.Format("posterior variance size(%lf) is smaller than %lf",(post_sd*post_sd),(sd*sd));
			out += msg;
		}
		//variance increasing monitoring
		
		//(1)old_vari2 (2)old_var1 (3)new_var
		if(num_iter == 1)
		{
			old_var1 = old_var2 = new_var = variance;
		}
		else if(num_iter == 2)
		{
			old_var1 = new_var = variance;
		}
		else
		{
			old_var2 = old_var1;
			old_var1 = new_var;
			new_var = variance;
		}

		if((old_var1>old_var2)&&(new_var>old_var1))
		{
			VarStop = true;

			msg.Format("variance continuous increase, two times: Last Variance[%le]",variance);
			out += msg;
		}

		//max correction term monitoring
		if(temp<fabs(MaxCorrection))
		{
			MaxCorStop = true;

			msg.Format("Max-Correction(%le) OK: %le\r\n",MaxCorrection,temp);
			out += msg;
		}
		
		//iteration number monitoring
		if(MaxIteration <= num_iter)
		{
			MaxIterStop = true;

			msg.Format("Max-Iteration Stop[%d]--> Last variance: %le\r\n",MaxIteration,variance);
			out += msg;
		}

	}while((MaxCorStop == false)&&(MaxIterStop == false)&&(VarStop == false));

	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebyDataCubic2005_2(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	//Before modifying all coefficients, first, remove the bias.
	//DoRPCUpdatebyRemoveBias_Shift();

	/****************/
	/*GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "";
	double *u, *v, *w;
	double *x, *y;
	unsigned int i;

	unsigned int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();

		u = new double[numGCP];
		v = new double[numGCP];
		w = new double[numGCP];

		x = new double[numGCP];
		y = new double[numGCP];
	}
	else
	{
		return "GCP number error";
	}

	//variance 계산용
	double _col_, _row_;

	/*Ground Coord Normalization*/
	for(i=0; i<numGCP; i++)
	{
		double lat, lon, h;
		double c,r;

		//Get GCP coord
		mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);

		//Get Image coord
		mGCP.mI.GetICPCoordbySort(i+1,&c,&r);

		//Ground coord normalize
		GetGroundCoord2NormalGroundCoord(lat,lon,h,(u+i),(v+i),(w+i));

		//Image coord normalize
		GetImageCoord2NormalImageCoord(c,r,(x+i),(y+i));

		//Ground coord to image coord
		GetGroundCoord2ImageCoord(lat,lon,h,&_col_,&_row_);
	}

	/**************/
	/*LS Iteration*/
	/**************/
	bool MaxCorStop = false;
	bool MaxIterStop = false;
	bool VarStop = false;

	double old_var1 = 0;
	double old_var2 = 0;
	double new_var = 0;
	
	unsigned int num_iter = 0;
	int n;
	
	Matrix<double> A, L, X, W, V, VTV;
	Matrix<double> N, Ninv;
	double variance;

	L.Resize((numGCP)*2,1,0.0);
	W.Resize((numGCP)*2,(numGCP)*2,0.0);

	for(n=0; n<(int)(numGCP); n++)
	{
		W(n*2,n*2) = 1/(sd*sd);
		W(n*2+1,n*2+1) = 1/(sd*sd);
	}

	////////////////////////////////////////////////////////////////////////////////////
	//2nd: 
	//A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H + A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H
	////////////////////////////////////////////////////////////////////////////////////
	
	MaxCorStop = false;
	MaxIterStop = false;
	VarStop = false;

	old_var1 = 0;
	old_var2 = 0;
	new_var = 0;

	num_iter = 0;

	//Resize of ACol and LCol matrix
	A.Resize((numGCP)*2,6*3,0.0);

	do
	{
		num_iter ++;

		//Put message
		msg.Format("[%d]Iteration\r\n",num_iter);
		out += msg;
				
		//Assemble of A matrix and L matrix
		MakeAandL_OnlyGCP2nd(A,L,W,u,v,w,x,y);
		
		//Least square solution
		//tikhonov regularization
		Matrix<double> E;
		E.Resize(6*3,6*3,0.0);
		for(n=0;n<6*3;n++)
			E(n,n) = 1.0;

		N = A.Transpose()%W%A + E*( h * h );
		//N = A.Transpose()%W%A;
		Ninv = N.Inverse();
		X = Ninv%A.Transpose()%W%L;
		V = A%X - L;
		VTV = V.Transpose()%W%V;
		variance = VTV(0,0)/((numGCP)*2-(6*3));
		m_SD = sqrt(variance);
		VarCov_X = Ninv*variance;
		VarCov_L = A%VarCov_X%A.Transpose();

		//Put message
		double post_sd = sqrt(variance);
		post_sd = post_sd*((dCoordScale[0]+dCoordScale[1])/2);
		msg.Format("-sd-\r\n%le\r\n",post_sd);
		out += msg;

		//RPC update
		for(n=0; n<6; n++)
		{
			dColNUM[n+4] += X(n,0);
		}
		for(n=0; n<6; n++)
		{
			dRowNUM[n+4] += X(n+6,0);
		}
		for(n=0; n<6; n++)
		{
			dColDEN[n+4] += X(n+12,0);
			dRowDEN[n+4] += X(n+12,0);
		}

		//maximum correction term
		double temp = fabs(X(0,0));
		for(n=1; n<(6*3); n++)
		{
			if(fabs(X(n,0))>temp)
				temp = fabs(X(n,0));
		}
		
		//Put message
		msg.Format("Maximum Correction: %le\r\n",temp);
		out += msg;

		//variance size monitoring
		if((post_sd*post_sd)<(sd*sd))
		{
			VarStop = true;

			msg.Format("posterior variance size(%lf) is smaller than %lf",(post_sd*post_sd),(sd*sd));
			out += msg;
		}

		//variance increasing monitoring
		//(1)old_vari2 (2)old_var1 (3)new_var
		if(num_iter == 1)
		{
			old_var1 = old_var2 = new_var = variance;
		}
		else if(num_iter == 2)
		{
			old_var1 = new_var = variance;
		}
		else
		{
			old_var2 = old_var1;
			old_var1 = new_var;
			new_var = variance;
		}

		if((old_var1>old_var2)&&(new_var>old_var1))
		{
			VarStop = true;

			msg.Format("variance continuous increase, two times: Last Variance[%le]",variance);
			out += msg;
		}

		//max correction term monitoring
		if(temp<fabs(MaxCorrection))
		{
			MaxCorStop = true;

			msg.Format("Max-Correction(%le) OK: %le\r\n",MaxCorrection,temp);
			out += msg;
		}
		
		//iteration number monitoring
		if(MaxIteration <= num_iter)
		{
			MaxIterStop = true;

			msg.Format("Max-Iteration Stop[%d]--> Last variance: %le\r\n",MaxIteration,variance);
			out += msg;
		}

	}while((MaxCorStop == false)&&(MaxIterStop == false)&&(VarStop == false));

	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebyDataCubic2005_1(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	//Before modifying all coefficients, first, remove the bias.
	//DoRPCUpdatebyRemoveBias_Shift();

	/****************/
	/*GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "";
	double *u, *v, *w;
	double *x, *y;
	unsigned int i;

	unsigned int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();

		u = new double[numGCP];
		v = new double[numGCP];
		w = new double[numGCP];

		x = new double[numGCP];
		y = new double[numGCP];
	}
	else
	{
		return "GCP number error";
	}

	//variance 계산용
	double _col_, _row_;

	/*Ground Coord Normalization*/
	for(i=0; i<numGCP; i++)
	{
		double lat, lon, h;
		double c,r;

		//Get GCP coord
		mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);

		//Get Image coord
		mGCP.mI.GetICPCoordbySort(i+1,&c,&r);

		//Ground coord normalize
		GetGroundCoord2NormalGroundCoord(lat,lon,h,(u+i),(v+i),(w+i));

		//Image coord normalize
		GetImageCoord2NormalImageCoord(c,r,(x+i),(y+i));

		//Ground coord to image coord
		GetGroundCoord2ImageCoord(lat,lon,h,&_col_,&_row_);
	}

	/**************/
	/*LS Iteration*/
	/**************/
	bool MaxCorStop = false;
	bool MaxIterStop = false;
	bool VarStop = false;

	double old_var1 = 0;
	double old_var2 = 0;
	double new_var = 0;
	
	unsigned int num_iter = 0;
	int n;
	
	Matrix<double> A, L, X, W, V, VTV;
	Matrix<double> N, Ninv;
	double variance;

	L.Resize((numGCP)*2,1,0.0);
	W.Resize((numGCP)*2,(numGCP)*2,0.0);

	for(n=0; n<(int)(numGCP); n++)
	{
		W(n*2,n*2) = 1/(sd*sd);
		W(n*2+1,n*2+1) = 1/(sd*sd);
	}

	////////////////////////////////////////////////////////////////////////////////////
	//1st: 
	//A[0] + A[1]*Lon + A[2]*Lat + A[3]*H
	////////////////////////////////////////////////////////////////////////////////////
		
	//Resize of ACol and LCol matrix
	A.Resize((numGCP)*2,4*3-1,0.0);
		
	do
	{
		num_iter ++;

		//Put message
		msg.Format("[%d]Iteration\r\n",num_iter);
		out += msg;
				
		//Assemble of A matrix and L matrix
		MakeAandL_OnlyGCP1st(A,L,W,u,v,w,x,y);
		
		//Least square solution
		//tikhonov regularization
		Matrix<double> E;
		E.Resize(NUMRPCPARAM,NUMRPCPARAM,0.0);
		for(n=0;n<NUMRPCPARAM;n++)
			E(n,n) = 1.0;

		//N = A.Transpose()%W%A + E*( h * h );
		N = A.Transpose()%W%A;
		Ninv = N.Inverse();
		X = Ninv%A.Transpose()%W%L;
		V = A%X - L;
		VTV = V.Transpose()%W%V;
		variance = VTV(0,0)/((numGCP)*2-(4*3-1));
		m_SD = sqrt(variance);
		VarCov_X = Ninv*variance;
		VarCov_L = A%VarCov_X%A.Transpose();

		//Put message
		double post_sd = sqrt(variance);
		post_sd = post_sd*((dCoordScale[0]+dCoordScale[1])/2);
		msg.Format("-sd-\r\n%le\r\n",post_sd);
		out += msg;

		//RPC update
		for(n=0; n<4; n++)
		{
			dColNUM[n] += X(n,0);
		}
		for(n=0; n<4; n++)
		{
			dRowNUM[n] += X(n+4,0);
		}
		for(n=0; n<3; n++)
		{
			dColDEN[n+1] += X(n+8,0);
			dRowDEN[n+1] += X(n+8,0);
		}

		//maximum correction term
		double temp = fabs(X(0,0));
		for(n=1; n<(4*3-1); n++)
		{
			if(fabs(X(n,0))>temp)
				temp = fabs(X(n,0));
		}
		
		//Put message
		msg.Format("Maximum Correction: %le\r\n",temp);
		out += msg;

		//variance size monitoring
		if((post_sd*post_sd)<(sd*sd))
		{
			VarStop = true;

			msg.Format("posterior variance size(%lf) is smaller than %lf",(post_sd*post_sd),(sd*sd));
			out += msg;
		}
		//variance increasing monitoring
		//(1)old_vari2 (2)old_var1 (3)new_var
		if(num_iter == 1)
		{
			old_var1 = old_var2 = new_var = variance;
		}
		else if(num_iter == 2)
		{
			old_var1 = new_var = variance;
		}
		else
		{
			old_var2 = old_var1;
			old_var1 = new_var;
			new_var = variance;
		}

		if((old_var1>old_var2)&&(new_var>old_var1))
		{
			VarStop = true;

			msg.Format("variance continuous increase, two times: Last Variance[%le]",variance);
			out += msg;
		}

		//max correction term monitoring
		if(temp<fabs(MaxCorrection))
		{
			MaxCorStop = true;

			msg.Format("Max-Correction(%le) OK: %le\r\n",MaxCorrection,temp);
			out += msg;
		}
		
		//iteration number monitoring
		if(MaxIteration <= num_iter)
		{
			MaxIterStop = true;

			msg.Format("Max-Iteration Stop[%d]--> Last variance: %le\r\n",MaxIteration,variance);
			out += msg;
		}

	}while((MaxCorStop == false)&&(MaxIterStop == false)&&(VarStop == false));

	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebyDataCubic_Linear(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	//Data Cubic Setting
	SetDataCubic();

	/****************/
	/*Additional GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "";
	double *u, *v, *w;
	double *x, *y;
	unsigned int i;

	unsigned int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();

		u = new double[numGCP];
		v = new double[numGCP];
		w = new double[numGCP];

		x = new double[numGCP];
		y = new double[numGCP];
	}
	else
	{
		return "GCP number error";
	}

	//Addition GCPs를 이용한 variance 계산용
	//weight 결정에 사용
	double _col_, _row_;
	double sum_ex, sum_ey;
	sum_ex = sum_ey = 0.0;

	/*Ground Coord Normalization*/
	for(i=0; i<numGCP; i++)
	{
		double lat, lon, h;
		double c,r;

		//Get GCP coord
		mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);

		//Get Image coord
		mGCP.mI.GetICPCoordbySort(i+1,&c,&r);

		//Ground coord normalize
		GetGroundCoord2NormalGroundCoord(lat,lon,h,(u+i),(v+i),(w+i));

		//Image coord normalize
		GetImageCoord2NormalImageCoord(c,r,(x+i),(y+i));

		//Ground coord to image coord
		GetGroundCoord2ImageCoord(lat,lon,h,&_col_,&_row_);

		sum_ex += (c - _col_)*(c - _col_);
		sum_ey += (r - _row_)*(r - _row_);
	}

	sum_ex = sum_ex/numGCP;
	sum_ey = sum_ey/numGCP;

	if(numGCP == 0)
	{
		sum_ex = sum_ey = 1.0;
	}

	int n;
	
	Matrix<double> A, L, X, W, V, VTV;
	Matrix<double> N, Ninv, VarCov_L, VarCov_X;
	double variance;
	
	//Resize of ACol and LCol matrix
	A.Resize((numGCP+NUMDC)*2,NUMRPCPARAM,0.0);
	L.Resize((numGCP+NUMDC)*2,1,0.0);
	W.Resize((numGCP+NUMDC)*2,(numGCP+NUMDC)*2,0.0);

	//weight setting
	for(n=0; n<NUMDC; n++)
	{
		W(n*2,n*2) = 1/sum_ex;
		W(n*2+1,n*2+1) = 1/sum_ey;
	}

	for(n=0; n<(int)(numGCP); n++)
	{
		W(n*2+(NUMDC*2),n*2+(NUMDC*2)) = 1/(sd*sd);
		W(n*2+1+(NUMDC*2),n*2+1+(NUMDC*2)) = 1/(sd*sd);
	}
	

	MakeAandL_Linear(A,L,W,u,v,w,x,y);
	
	out += "[A Matrix]\r\n";
	out += A.matrixout();
	out += "\r\n\r\n";
	out += "[L Matrix]\r\n";
	out += L.matrixout();

	//Least square solution
	//tikhonov regularization
	bool condition = false;
	h = 0.00;
	int num = 0;
	
	double old_var1 = 10e99;
	double old_var2 = 10e99;
	
	do
	{
		num ++;
		h += 0.0001;		

		

		Matrix<double> E;
		E.Resize(NUMRPCPARAM,NUMRPCPARAM,0.0);
		for(n=0;n<NUMRPCPARAM;n++)
			E(n,n) = 1.0;
		
		N = A.Transpose()%W%A + E*(h*h);
		//N = A.Transpose()%W%A;
		Ninv = N.Inverse();
		X = Ninv%A.Transpose()%W%L;
		
		V = A%X - L;
		
		VTV = V.Transpose()%W%V;
		variance = VTV(0,0)/((NUMDC+numGCP)*2-NUMRPCPARAM);

		m_SD = sqrt(variance);

		if((old_var1>old_var2)&&(variance>old_var1))
			condition = true;
		if(h>2.0)
			condition = true;

		old_var2 = old_var1;
		old_var1 = variance;

		if(num > 50)
			condition = true;

	}while(condition == false);
	
	out += "[V Matrix]\r\n";
	out += V.matrixout();
	

	VarCov_X = Ninv*variance;
	VarCov_L = A%VarCov_X%A.Transpose();

	//Put message
	msg.Format("-posteriori variance-\r\n%le\r\n",variance);
	out += msg;

	//RPC update
	for(n=0; n<20; n++)
	{
		dColNUM[n] = X(n,0);
	}
	for(n=20; n<40; n++)
	{
		dRowNUM[n-20] = X(n,0);
	}
	for(n=40; n<NUMRPCPARAM; n++)
	{
		dColDEN[n-39] = X(n,0);
		dRowDEN[n-39] = X(n,0);
	}

	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebySequentialLS_ParamObs(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	/******************************************************************/
	/*첫번째 GCP를 이용하여 Parameter Observation을 추가한 Adjustment**/
	/*두번째 GCP 부터는 Additional GCP를 이용한 Sequential LS**********/
	/*Data Cubic를 사용하지 않는다.************************************/
	/******************************************************************/
	
	/****************/
	/*Additional GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "";
	
	//normalized additional GCP Ground coord
	double *u;
	double *v;
	double *w;
	//normalized additional GCP Image coord
	double *x, *y;
	
	unsigned int i;
	
	//number of additional GCP
	unsigned int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();
		
		u = new double[numGCP];
		v = new double[numGCP];
		w = new double[numGCP];
		
		x = new double[numGCP];
		y = new double[numGCP];
	}
	else
	{
		return "GCP number error";
	}
	
	//additional GCP를 검사점으로 이용한 RPC의 variance 계산용
	double _col_, _row_;
	double sum_ex, sum_ey;
	sum_ex = sum_ey = 0.0;
	
	/*Ground Coord Normalization*/
	for(i=0; i<numGCP; i++)
	{
		double lat, lon, h;
		double c,r;
		
		//Get GCP coord
		mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);
		
		//Get Image coord
		mGCP.mI.GetICPCoordbySort(i+1,&c,&r);
		
		//Ground coord normalize
		GetGroundCoord2NormalGroundCoord(lat,lon,h,(u+i),(v+i),(w+i));
		
		//Image coord normalize
		GetImageCoord2NormalImageCoord(c,r,(x+i),(y+i));
		
		//Ground coord to image coord
		GetGroundCoord2ImageCoord(lat,lon,h,&_col_,&_row_);
		
		sum_ex += (c - _col_)*(c - _col_);
		sum_ey += (r - _row_)*(r - _row_);
	}
	
	//평균제곱오차(분산)
	sum_ex = sum_ex/numGCP;
	sum_ey = sum_ey/numGCP;
	
	//additional GCP가 없으면 error message return
	if(numGCP == 0)
	{
		return "No Additional GCP";
	}
	
	/**************/
	/*LS Iteration*/
	/**************/
	bool MaxCorStop = false;
	bool MaxIterStop = false;
	
	unsigned int num_iter = 0;
	int n;
	
	Matrix<double> A, L, X, W, V, VTV;
	Matrix<double> N, Ninv, VarCov_L, VarCov_X;
	Matrix<double> N2, N2inv;
	double variance;
	
	//Resize of ACol and LCol matrix
	A.Resize((NUMRPCPARAM+1*2),NUMRPCPARAM,0.0);
	L.Resize((NUMRPCPARAM+1*2),1,0.0);
	W.Resize((NUMRPCPARAM+1*2),(NUMRPCPARAM+1*2),0.0);
	
	//weight setting
	for(n=0; n<NUMRPCPARAM; n++)
	{
		W(n,n) = (1.0/sum_ex+1.0/sum_ey)/2.0;
	}
	
	for(n=0; n<(int)(1); n++)
	{
		W(n*2+NUMRPCPARAM,n*2+NUMRPCPARAM) = 1/(sd*sd);
		W(n*2+1+NUMRPCPARAM,n*2+1+NUMRPCPARAM) = 1/(sd*sd);
	}
	
	//1st GCP Data
	double Lat, Lon, H;
	double Col, Row;
	Lat = u[0];
	Lon = v[0];
	H = w[0];
	Col = x[0];
	Row = y[0];
	
	msg.Format("/************************************/\r\n/*[First GCP]Sequential Least Square*/\r\n/************************************/\r\n");
	out += msg;

	do
	{
		num_iter ++;

		if(1<num_iter)
		{
			Matrix<double> W_ = VarCov_L;
			int r,c;
			r = W_.GetRows();
			c = W_.GetCols();
			double value;
			value = W_(0,0);
			value = W_(c-1,r-1);
			for(n=0; (unsigned int)n<(NUMRPCPARAM+1*2); n++)
			{
				//W(n,n) = 1/W_(n,n)*W_(0,0);
				//W(n,n) = W_(n,n);
			}
		}
		
		//Put message
		msg.Format("[%d]Iteration\r\n",num_iter);
		out += msg;
		
		//Assemble of A matrix and L matrix
		MakeAandL_Param(A,L,W,Lat,Lon,H,Col,Row);
		
		//Least square solution
		//tikhonov regularization
		Matrix<double> E;
		E.Resize(NUMRPCPARAM,NUMRPCPARAM,0.0);
		for(n=0;n<NUMRPCPARAM;n++)
			E(n,n) = 1.0;
		
		N = A.Transpose()%W%A + E*(h*h);
		Ninv = N.Inverse();
		X = Ninv%A.Transpose()%W%L;
		V = A%X - L;
		VTV = V.Transpose()%W%V;
		variance = VTV(0,0)/(2);
		VarCov_X = Ninv*variance;
		VarCov_L = A%VarCov_X%A.Transpose();
		
		//weight update
		for(n=0; n<(int)(NUMRPCPARAM+2); n++)
		{
			W(n,n) = 1.0/VarCov_L(n,n);
		}
		
		//Put message
		msg.Format("-posteriori variance-\r\n%le\r\n",variance);
		out += msg;
		
		//RPC update
		for(n=0; n<20; n++)
		{
			dColNUM[n] += X(n,0);
		}
		for(n=20; n<40; n++)
		{
			dRowNUM[n-20] += X(n,0);
		}
		for(n=40; n<NUMRPCPARAM; n++)
		{
			dColDEN[n-39] += X(n,0);
			dRowDEN[n-39] += X(n,0);
		}
		
		double temp = fabs(X(0,0));
		for(n=1; n<NUMRPCPARAM; n++)
		{
			if(fabs(X(n,0))>temp)
				temp = fabs(X(n,0));
		}
		
		//Put message
		msg.Format("Maximum Correction: %lf\r\n",temp);
		out += msg;
		
		//max correction term monitoring
		if(temp<fabs(MaxCorrection))
		{
			MaxCorStop = true;
			
			msg.Format("Max-Correction(%lf) OK: %lf\r\n",MaxCorrection,temp);
			out += msg;
		}
		
		//iteration number monitoring
		if(MaxIteration <= num_iter)
		{
			MaxIterStop = true;
			
			msg.Format("Max-Iteration Stop[%d]--> Last variance: %lf\r\n",MaxIteration,variance);
			out += msg;
		}
		
	}while((MaxCorStop == false)&&(MaxIterStop == false));
	

	/****************/
	/*2번째 GCP 부터*/
	/****************/

	//i-1번째...N matrix와 X matrix
	Matrix<double> Ninv_;
	Matrix<double> X_;

	for(i=1; i<numGCP; i++)
	{
		msg.Format("/************************************/\r\n/*[%d GCP]Sequential Least Square*/\r\n/************************************/\r\n",(i+1));
		out += msg;

		//Resize of ACol and LCol matrix
		A.Resize(2,NUMRPCPARAM,0.0);
		L.Resize(2,1,0.0);
		W.Resize(2,2,0.0);
		//weight setting
		W(0,0) = 1/(sd*sd);
		W(1,1) = 1/(sd*sd);

		//i번째 GCP
		Lat = u[i];
		Lon = v[i];
		H = w[i];
		Col = x[i];
		Row = y[i];
		
		//i-1번째 N.Inverse()와 X
		Ninv_ = Ninv;
		X_ = X;

		num_iter = 0;
		MaxCorStop = false;
		MaxIterStop = false;
		
		do
		{
    		num_iter ++;
			
			//Put message
			msg.Format("[%d]Iteration\r\n",num_iter);
			out += msg;
			
			//Assemble of A matrix and L matrix
			MakeAandL_Sequential_LSi(A,L,W,Lat,Lon,H,Col,Row);

			//Ninv
			Ninv = Ninv_ - Ninv_%A.Transpose()%(A%Ninv_%A.Transpose()+W.Inverse()).Inverse()%A%Ninv_;
			
			//delta X
			X = Ninv_%A.Transpose()%(A%Ninv_%A.Transpose()+W.Inverse()).Inverse()%(L-A%X_);

			//RPC update
			for(n=0; n<20; n++)
			{
				dColNUM[n] += X(n,0);
			}
			for(n=20; n<40; n++)
			{
				dRowNUM[n-20] += X(n,0);
			}
			for(n=40; n<NUMRPCPARAM; n++)
			{
				dColDEN[n-39] += X(n,0);
				dRowDEN[n-39] += X(n,0);
			}
			
			//max correction
			double temp = fabs(X(0,0));
			for(n=1; n<NUMRPCPARAM; n++)
			{
				if(fabs(X(n,0))>temp)
					temp = fabs(X(n,0));
			}
			
			//Put message
			//msg.Format("Maximum Correction: %lf\r\n",temp);
			//out += msg;
			
			//max correction term monitoring
			if(temp<fabs(MaxCorrection))
			{
				MaxCorStop = true;
				
				msg.Format("Max-Correction(%lf) OK: %lf\r\n",MaxCorrection,temp);
				out += msg;
			}
			
			//iteration number monitoring
			if(MaxIteration <= num_iter)
			{
				MaxIterStop = true;
				
				msg.Format("Max-Iteration Stop[%d]--> Last variance: %lf\r\n",MaxIteration,variance);
				out += msg;
			}
			
		}while((MaxCorStop == false)&&(MaxIterStop == false));

	}
	
	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebySequentialLS_DataCubic(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	/******************************************************************/
	/*첫번째 GCP와 Data cubic을 추가한 Adjustment**/
	/*두번째 GCP 부터는 Additional GCP를 이용한 Sequential LS**********/
	/*Data Cubic 사용*/
	/******************************************************************/
	
	//Data Cubic Setting
	SetDataCubic();

	/****************/
	/*Additional GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "";
	
	//normalized additional GCP Ground coord
	double *u, *v, *w;

	//normalized additional GCP Image coord
	double *x, *y;
	
	unsigned int i;
	
	//number of additional GCP
	unsigned int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();
		
		u = new double[numGCP];
		v = new double[numGCP];
		w = new double[numGCP];
		
		x = new double[numGCP];
		y = new double[numGCP];
	}
	else
	{
		return "GCP number error";
	}
	
	//additional GCP를 검사점으로 이용한 RPC의 variance 계산용
	double _col_, _row_;
	double sum_ex, sum_ey;
	sum_ex = sum_ey = 0.0;
	
	/*Ground Coord Normalization*/
	for(i=0; i<numGCP; i++)
	{
		double lat, lon, h;
		double c,r;
		
		//Get GCP coord
		mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);
		
		//Get Image coord
		mGCP.mI.GetICPCoordbySort(i+1,&c,&r);
		
		//Ground coord normalize
		GetGroundCoord2NormalGroundCoord(lat,lon,h,(u+i),(v+i),(w+i));
		
		//Image coord normalize
		GetImageCoord2NormalImageCoord(c,r,(x+i),(y+i));
		
		//Ground coord to image coord
		GetGroundCoord2ImageCoord(lat,lon,h,&_col_,&_row_);
		
		sum_ex += (c - _col_)*(c - _col_);
		sum_ey += (r - _row_)*(r - _row_);
	}
	
	//평균제곱오차(분산)
	sum_ex = sum_ex/numGCP;
	sum_ey = sum_ey/numGCP;
	
	//additional GCP가 없으면 error message return
	if(numGCP == 0)
	{
		return "No Additional GCP";
	}
	
	/**************/
	/*LS Iteration*/
	/**************/
	bool MaxCorStop = false;
	bool MaxIterStop = false;
		
	unsigned int num_iter = 0;
	int n;
	
	Matrix<double> A, L, X, W, V, VTV;
	Matrix<double> N, Ninv, VarCov_L, VarCov_X;
	double variance;
	
	//Resize of ACol and LCol matrix
	A.Resize((1+NUMDC)*2,NUMRPCPARAM,0.0);
	L.Resize((1+NUMDC)*2,1,0.0);
	W.Resize((1+NUMDC)*2,(1+NUMDC)*2,0.0);

	//weight setting
	for(n=0; n<NUMDC; n++)
	{
		W(n*2,n*2) = 1/sum_ex;
		W(n*2+1,n*2+1) = 1/sum_ey;
	}

	for(n=0; n<(int)(1); n++)
	{
		W(n*2+(NUMDC*2),n*2+(NUMDC*2)) = 1/(sd*sd);
		W(n*2+1+(NUMDC*2),n*2+1+(NUMDC*2)) = 1/(sd*sd);
	}
	
	//1st GCP Data
	double Lat, Lon, H;
	double Col, Row;

	Lat = u[0];
	Lon = v[0];
	H = w[0];
	Col = x[0];
	Row = y[0];
	
	msg.Format("/************************************/\r\n/*[First GCP]Sequential Least Square*/\r\n/************************************/\r\n");
	out += msg;

	do
	{
		num_iter ++;

		if(1<num_iter)
		{
			Matrix<double> W_ = VarCov_L;
			int r,c;
			r = W_.GetRows();
			c = W_.GetCols();
			double value;
			value = W_(0,0);
			value = W_(c-1,r-1);
			for(n=0; (unsigned int)n<((NUMDC+1)*2); n++)
			{
				//W(n,n) = 1/W_(n,n)*W_(0,0);
				//W(n,n) = W_(n,n);
			}
		}

		//Put message
		msg.Format("[%d]Iteration\r\n",num_iter);
		out += msg;
				
		//Assemble of A matrix and L matrix
		MakeAandL(A,L,W,Lat,Lon,H,Col,Row);
		
		//Least square solution
		//tikhonov regularization
		Matrix<double> E;
		E.Resize(NUMRPCPARAM,NUMRPCPARAM,0.0);
		for(n=0;n<NUMRPCPARAM;n++)
			E(n,n) = 1.0;

		N = A.Transpose()%W%A + E*(h*h);
		Ninv = N.Inverse();
		X = Ninv%A.Transpose()%W%L;
		V = A%X - L;
		VTV = V.Transpose()%W%V;
		variance = VTV(0,0)/((NUMDC+1)*2-NUMRPCPARAM);

		VarCov_X = Ninv*variance;
		VarCov_L = A%VarCov_X%A.Transpose();

		//Put message
		msg.Format("-posteriori variance-\r\n%le\r\n",variance);
		out += msg;

		//RPC update
		for(n=0; n<20; n++)
		{
			dColNUM[n] += X(n,0);
		}
		for(n=20; n<40; n++)
		{
			dRowNUM[n-20] += X(n,0);
		}
		for(n=40; n<NUMRPCPARAM; n++)
		{
			dColDEN[n-39] += X(n,0);
			dRowDEN[n-39] += X(n,0);
		}

		double temp = fabs(X(0,0));
		for(n=1; n<NUMRPCPARAM; n++)
		{
			if(fabs(X(n,0))>temp)
				temp = fabs(X(n,0));
		}
		
		//Put message
		msg.Format("Maximum Correction: %lf\r\n",temp);
		out += msg;

		//variance monitoring
		//(1)old_vari2 (2)old_var1 (3)new_var
		//if(num_iter == 1)
		//{
		//	old_var1 = old_var2 = new_var = variance;
		//}
		//else if(num_iter == 2)
		//{
		//	old_var1 = new_var = variance;
		//}
		//else
		//{
		//	old_var2 = old_var1;
		//	old_var1 = new_var;
		//	new_var = variance;
		//}

		//if((old_var1>old_var2)&&(new_var>old_var1))
		//{
		//	VarStop = true;

		//	msg.Format("variance continuous increase, two times: Last Variance[%le]",variance);
		//	out += msg;
		//}

		//max correction term monitoring
		if(temp<fabs(MaxCorrection))
		{
			MaxCorStop = true;

			msg.Format("Max-Correction(%lf) OK: %lf\r\n",MaxCorrection,temp);
			out += msg;
		}
		
		//iteration number monitoring
		if(MaxIteration <= num_iter)
		{
			MaxIterStop = true;

			msg.Format("Max-Iteration Stop[%d]--> Last variance: %lf\r\n",MaxIteration,variance);
			out += msg;
		}

	}while((MaxCorStop == false)&&(MaxIterStop == false));//&&(VarStop == false));
	

	/****************/
	/*2번째 GCP 부터*/
	/****************/

	//i-1번째...N matrix와 X matrix
	Matrix<double> Ninv_;
	Matrix<double> X_;

	for(i=1; i<numGCP; i++)
	{
		msg.Format("/************************************/\r\n/*[%d GCP]Sequential Least Square*/\r\n/************************************/\r\n",(i+1));
		out += msg;

		//Resize of ACol and LCol matrix
		A.Resize(2,NUMRPCPARAM,0.0);
		L.Resize(2,1,0.0);
		W.Resize(2,2,0.0);
		//weight setting
		W(0,0) = 1/(sd*sd);
		W(1,1) = 1/(sd*sd);

		//i번째 GCP
		Lat = u[i];
		Lon = v[i];
		H = w[i];
		Col = x[i];
		Row = y[i];
		
		//i-1번째 N.Inverse()와 X
		Ninv_ = Ninv;
		X_ = X;

		num_iter = 0;
		MaxCorStop = false;
		MaxIterStop = false;
		
		do
		{
    		num_iter ++;
			
			//Put message
			msg.Format("[%d]Iteration\r\n",num_iter);
			out += msg;
			
			//Assemble of A matrix and L matrix
			MakeAandL_Sequential_LSi(A,L,W,Lat,Lon,H,Col,Row);

			//Ninv
			Ninv = Ninv_ - Ninv_%A.Transpose()%(A%Ninv_%A.Transpose()+W.Inverse()).Inverse()%A%Ninv_;
			
			//delta X
			X = Ninv_%A.Transpose()%(A%Ninv_%A.Transpose()+W.Inverse()).Inverse()%(L-A%X_);

			//RPC update
			for(n=0; n<20; n++)
			{
				dColNUM[n] += X(n,0);
			}
			for(n=20; n<40; n++)
			{
				dRowNUM[n-20] += X(n,0);
			}
			for(n=40; n<NUMRPCPARAM; n++)
			{
				dColDEN[n-39] += X(n,0);
				dRowDEN[n-39] += X(n,0);
			}
			
			//max correction
			double temp = fabs(X(0,0));
			for(n=1; n<NUMRPCPARAM; n++)
			{
				if(fabs(X(n,0))>temp)
					temp = fabs(X(n,0));
			}
			
			//Put message
			//msg.Format("Maximum Correction: %lf\r\n",temp);
			//out += msg;
			
			//max correction term monitoring
			if(temp<fabs(MaxCorrection))
			{
				MaxCorStop = true;
				
				msg.Format("Max-Correction(%lf) OK: %lf\r\n",MaxCorrection,temp);
				out += msg;
			}
			
			//iteration number monitoring
			if(MaxIteration <= num_iter)
			{
				MaxIterStop = true;
				
				msg.Format("Max-Iteration Stop[%d]--> Last variance: %lf\r\n",MaxIteration,variance);
				out += msg;
			}
			
		}while((MaxCorStop == false)&&(MaxIterStop == false));

	}
	
	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRPCUpdatebyParamObs(double MaxCorrection, unsigned short MaxIteration, double sd, double h)
{
	/******************************************************************/
	/*파라메터 관측방정식 사용*/
	/******************************************************************/

	/****************/
	/*Additional GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "";

	//normalized additional GCP Ground coord
	double *u, *v, *w;
	//normalized additional GCP Image coord
	double *x, *y;

	unsigned int i;

	//number of additional GCP
	unsigned int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();

		u = new double[numGCP];
		v = new double[numGCP];
		w = new double[numGCP];

		x = new double[numGCP];
		y = new double[numGCP];
	}
	else
	{
		return "GCP number error";
	}

	//additional GCP를 검사점으로 이용한 RPC의 variance 계산용
	double _col_, _row_;
	double sum_ex, sum_ey;
	sum_ex = sum_ey = 0.0;

	/*Ground Coord Normalization*/
	for(i=0; i<numGCP; i++)
	{
		double lat, lon, h;
		double c,r;

		//Get GCP coord
		mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);

		//Get Image coord
		mGCP.mI.GetICPCoordbySort(i+1,&c,&r);

		//Ground coord normalize
		GetGroundCoord2NormalGroundCoord(lat,lon,h,(u+i),(v+i),(w+i));

		//Image coord normalize
		GetImageCoord2NormalImageCoord(c,r,(x+i),(y+i));

		//Ground coord to image coord
		GetGroundCoord2ImageCoord(lat,lon,h,&_col_,&_row_);

		sum_ex += (c - _col_)*(c - _col_);
		sum_ey += (r - _row_)*(r - _row_);
	}

	//평균제곱오차(분산)
	sum_ex = sum_ex/numGCP;
	sum_ey = sum_ey/numGCP;

	//additional GCP가 없으면 error message return
	if(numGCP == 0)
	{
		return "No Additional GCP";
	}

	/**************/
	/*LS Iteration*/
	/**************/
	bool MaxCorStop = false;
	bool MaxIterStop = false;
	bool VarStop = false;

	double old_var1 = 0;
	double old_var2 = 0;
	double new_var = 0;
	
	unsigned int num_iter = 0;
	int n;
	
	Matrix<double> A, L, X, W, V, VTV;
	Matrix<double> N, Ninv, VarCov_L, VarCov_X;
	double variance;
	
	//Resize of ACol and LCol matrix
	A.Resize((NUMRPCPARAM+numGCP*2),NUMRPCPARAM,0.0);
	L.Resize((NUMRPCPARAM+numGCP*2),1,0.0);
	W.Resize((NUMRPCPARAM+numGCP*2),(NUMRPCPARAM+numGCP*2),0.0);

	//weight setting
	for(n=0; n<NUMRPCPARAM; n++)
	{
		W(n,n) = (1.0/sum_ex+1.0/sum_ey)/2.0;
	}

	for(n=0; n<(int)(numGCP); n++)
	{
		W(n*2+NUMRPCPARAM,n*2+NUMRPCPARAM) = 1/(sd*sd);
		W(n*2+1+NUMRPCPARAM,n*2+1+NUMRPCPARAM) = 1/(sd*sd);
	}
	
	do
	{
		num_iter ++;

		if(1<num_iter)
		{
			Matrix<double> W_ = VarCov_L;
			int r,c;
			r = W_.GetRows();
			c = W_.GetCols();
			double value;
			value = W_(0,0);
			value = W_(c-1,r-1);
			for(n=0; (unsigned int)n<(NUMRPCPARAM+numGCP*2); n++)
			{
				//W(n,n) = 1/W_(n,n)*W_(0,0);
				//W(n,n) = W_(n,n);
			}
		}

		//Put message
		msg.Format("[%d]Iteration\r\n",num_iter);
		out += msg;
				
		//Assemble of A matrix and L matrix
		MakeAandL_Param(A,L,W,u,v,w,x,y);
		
		//Least square solution
		//tikhonov regularization
		Matrix<double> E;
		E.Resize(NUMRPCPARAM,NUMRPCPARAM,0.0);
		for(n=0;n<NUMRPCPARAM;n++)
			E(n,n) = 1.0;
		
		N = A.Transpose()%W%A + E*(h*h);
		Ninv = N.Inverse();
		X = Ninv%A.Transpose()%W%L;
		V = A%X - L;
		VTV = V.Transpose()%W%V;
		variance = VTV(0,0)/(numGCP*2);

		m_SD = sqrt(variance);

		VarCov_X = Ninv*variance;
		VarCov_L = A%VarCov_X%A.Transpose();

		//weight update
		for(n=0; n<(int)(NUMRPCPARAM+numGCP*2); n++)
		{
			W(n,n) = 1.0/VarCov_L(n,n);
		}

		//Put message
		msg.Format("-posteriori variance-\r\n%le\r\n",variance);
		out += msg;

		//RPC update
		for(n=0; n<20; n++)
		{
			dColNUM[n] += X(n,0);
		}
		for(n=20; n<40; n++)
		{
			dRowNUM[n-20] += X(n,0);
		}
		for(n=40; n<NUMRPCPARAM; n++)
		{
			dColDEN[n-39] += X(n,0);
			dRowDEN[n-39] += X(n,0);
		}

		//Put message
		//out += "-Weight matrix-\r\n";
		//msg = W.matrixout();
		//out += msg;

		//Weight Update
		//for(n=0; n<(int)W.GetRows(); n++)
		//{
		//	W(n,n) = VarCov_L.Inverse()(n,n);
		//}

		//W = VarCov_L.Inverse();
				
		//Put message
		//out += "-RPC-\r\n";
		//for(n=0; n<20; n++)
		//{
			//msg.Format("dColNUM[%d]: %lf\r\n",n,dColNUM[i]);
			//out += msg;
		//}
		//for(n=0; n<20; n++)
		//{
			//msg.Format("dColDEN[%d]: %lf\r\n",n,dColDEN[i]);
			//out += msg;
		//}
		//for(n=0; n<20; n++)
		//{
			//msg.Format("dRowNUM[%d]: %lf\r\n",n,dRowNUM[i]);
			//out += msg;
		//}
		//for(n=0; n<20; n++)
		//{
			//msg.Format("dRowDEN[%d]: %lf\r\n",n,dRowDEN[i]);
			//out += msg;
		//}

		double temp = fabs(X(0,0));
		for(n=1; n<NUMRPCPARAM; n++)
		{
			if(fabs(X(n,0))>temp)
				temp = fabs(X(n,0));
		}
		
		//Put message
		msg.Format("Maximum Correction: %lf\r\n",temp);
		out += msg;

		//variance monitoring
		//(1)old_vari2 (2)old_var1 (3)new_var
		if(num_iter == 1)
		{
			old_var1 = old_var2 = new_var = variance;
		}
		else if(num_iter == 2)
		{
			old_var1 = new_var = variance;
		}
		else
		{
			old_var2 = old_var1;
			old_var1 = new_var;
			new_var = variance;
		}

		if((old_var1>old_var2)&&(new_var>old_var1))
		{
			VarStop = true;

			msg.Format("variance continuous increase, two times");
			out += msg;
		}

		//max correction term monitoring
		if(temp<fabs(MaxCorrection))
		{
			MaxCorStop = true;

			msg.Format("Max-Correction(%lf) OK: %lf\r\n",MaxCorrection,temp);
			out += msg;
		}
		
		//iteration number monitoring
		if(MaxIteration <= num_iter)
		{
			MaxIterStop = true;

			msg.Format("Max-Iteration Stop[%d]--> Last variance: %lf\r\n",MaxIteration,variance);
			out += msg;
		}

	}while((MaxCorStop == false)&&(MaxIterStop == false)&&(VarStop == false));

	return out;
}

/*RPC Update by removing bias (Rigid body)*/
CSMStr CRFM::DoRPCUpdatebyRemoveBias_Rigid(void)
{
	/*************************************************/
	/*Image coord offset adjustment for removing bias*/
	/*************************************************/

	/****************/
	/*Additional GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "[Bias Adnustment]\r\n";
	int i;

	//Additional GCP Setting
	int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();
		if(numGCP < 1)
		{
			CSMStr temp;
			temp.Format("No GCP\r\n");
			out += temp;
		}
		else if(numGCP < 2)
		{
			double GCP_lat, GCP_lon, GCp_h;
			double ICP_c, ICP_r;
			double c,r;
			long id;
			
			//Get ID
			id = mGCP.mG.GetGCPID(1);
			
			//Get GCP coord
			mGCP.mG.GetGCPCoordbySort(1,&GCP_lat,&GCP_lon,&GCp_h);
			
			//Get Image coord
			mGCP.mI.GetICPCoordbySort(1,&ICP_c,&ICP_r);
			
			//Ground coord normalize
			GetGroundCoord2ImageCoord(GCP_lat,GCP_lon,GCp_h,&c,&r);
			
			double Offset_x = ICP_c - c;
			double Offset_y = ICP_r - r;
			
			dCoordOffset[1] = dCoordOffset[1] + Offset_x;
			dCoordOffset[0] = dCoordOffset[0] + Offset_y;
			
			CSMStr temp;
			temp.Format("COL OFFSET: %f\r\n", dCoordOffset[1]);
			out += temp;
			temp.Format("ROW OFFSET: %f\r\n", dCoordOffset[0]);
			out += temp;
		}
		else
		{
			
			KP.SetPointNum(numGCP);
			
			for(i=0; i<numGCP; i++)
			{
				double GCP_lat, GCP_lon, GCp_h;
				double ICP_c, ICP_r;
				double c,r;
				long id;
				
				//Get ID
				id = mGCP.mG.GetGCPID(i+1);
				
				//Get GCP coord
				mGCP.mG.GetGCPCoordbySort(i+1,&GCP_lat,&GCP_lon,&GCp_h);
				
				//Get Image coord
				mGCP.mI.GetICPCoordbySort(i+1,&ICP_c,&ICP_r);
				
				//Ground coord normalize
				GetGroundCoord2ImageCoord(GCP_lat,GCP_lon,GCp_h,&c,&r);
				
				KP.SetPoint_p(i, c, r);
				KP.SetPoint_q(i, ICP_c, ICP_r);
			}
			
			RigidBody transform(KP);
			double Scale_x = transform.Sx;
			double Scale_y = transform.Sy;
			double Offset_x = transform.Tx;
			double Offset_y = transform.Ty;
			
			dCoordScale[1] = dCoordScale[1]*Scale_x;
			dCoordScale[0] = dCoordScale[0]*Scale_y;
			
			dCoordOffset[1] = dCoordOffset[1]*Scale_x +Offset_x;
			dCoordOffset[0] = dCoordOffset[0]*Scale_y +Offset_y;
			
			CSMStr temp;
			temp.Format("COL OFFSET: %f\r\n", dCoordOffset[1]);
			out += temp;
			temp.Format("ROW OFFSET: %f\r\n", dCoordOffset[0]);
			out += temp;
			temp.Format("COL SCALE: %f\r\n", dCoordScale[1]);
			out += temp;
			temp.Format("ROW SCALE: %f\r\n", dCoordScale[0]);
			out += temp;
		}
	}
	else
	{
		return "GCP data error";
	}
		
	return out;
}

/*RPC Update by removing bias(Shift)*/
CSMStr CRFM::DoRPCUpdatebyRemoveBias_Shift(void)
{
	/*************************************************/
	/*Image coord offset adjustment for removing bias*/
	/*************************************************/

	/****************/
	/*Additional GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "[Bias Adnustment]\r\n";
	int i;

	//Additional GCP Setting
	int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();
		if(numGCP < 1)
		{
			CSMStr temp;
			temp.Format("No GCP\r\n");
			out += temp;
		}
		else
		{			
			double GCP_lat, GCP_lon, GCp_h;
			double ICP_c, ICP_r;
			double c,r;
			long id;
			double Offset_x=0, Offset_y=0;

			Matrix<double> A, L, X, V;
			A.Resize(numGCP*2,2,0);
			L.Resize(numGCP*2,1,0);
			for(i=0; i<numGCP; i++)
			{				
				//Get ID
				id = mGCP.mG.GetGCPID(i+1);
				
				//Get GCP coord
				mGCP.mG.GetGCPCoordbySort(i+1,&GCP_lat,&GCP_lon,&GCp_h);
				
				//Get Image coord
				mGCP.mI.GetICPCoordbySort(i+1,&ICP_c,&ICP_r);
				
				//Ground coord normalize
				GetGroundCoord2ImageCoord(GCP_lat,GCP_lon,GCp_h,&c,&r);
							
				Offset_x += ICP_c - c;
				Offset_y += ICP_r - r;

				A(i*2+0,0) = 1;
				A(i*2+1,1) = 1;
				L(i*2+0,0) = ICP_c - c;
				L(i*2+1,0) = ICP_r - r;
			}			
			
			if(numGCP>1)
			{
				X = (A.Transpose()%A).Inverse()%A.Transpose()%L;
				V = A%X-L;
				double sigma;
				sigma = sqrt((V.Transpose()%V)(0,0)/(numGCP*2-2));
				CSMStr temp;
				temp.Format("COL BIAS: %f\r\n", X(0,0));
				out += temp;
				temp.Format("ROW BIAS: %f\r\n", X(1,0));
				out += temp;
				temp.Format("SIGMA: %f\r\n", sigma);
				out += temp;
				
				dCoordOffset[1] = dCoordOffset[1] + X(0,0);
				dCoordOffset[0] = dCoordOffset[0] + X(1,0);

				temp.Format("COL OFFSET: %f\r\n", dCoordOffset[1]);
				out += temp;
				temp.Format("ROW OFFSET: %f\r\n", dCoordOffset[0]);
				out += temp;
			}
			else
			{
				dCoordOffset[1] = dCoordOffset[1] + Offset_x/numGCP;
				dCoordOffset[0] = dCoordOffset[0] + Offset_y/numGCP;
				CSMStr temp;
				temp.Format("COL BIAS: %f\r\n", Offset_x/numGCP);
				out += temp;
				temp.Format("ROW BIAS: %f\r\n", Offset_y/numGCP);
				out += temp;
				temp.Format("COL OFFSET: %f\r\n", dCoordOffset[1]);
				out += temp;
				temp.Format("ROW OFFSET: %f\r\n", dCoordOffset[0]);
				out += temp;
			}			
		}
	}

	return out;
}

/*RPC Update*/
CSMStr CRFM::DoRFMUpdatebyTransform(unsigned int option, double MaxCorrection, unsigned short MaxIteration)
{
	/************************/
	/*2D Transformation 도입*/
	/************************/

	/****************/
	/*Additional GCP*/
	/****************/
	CSMStr out;
	CSMStr msg;
	out = "";
	int i;

	trans_option = option;

	//Additional GCP Setting
	int numGCP;
	if(mGCP.mG.GetGCPNum() == mGCP.mI.GetICPNum())
	{
		numGCP = mGCP.mG.GetGCPNum();
		
		KP.SetPointNum(numGCP);
		
		for(i=0; i<numGCP; i++)
		{
			double lat, lon, h;
			double c_, r_;
			double c,r;
			long id;

			//Get ID
			id = mGCP.mG.GetGCPID(i+1);

			//Get GCP coord
			mGCP.mG.GetGCPCoordbySort(i+1,&lat,&lon,&h);

			//Get Image coord
			mGCP.mI.GetICPCoordbySort(i+1,&c_,&r_);

			//Ground coord normalize
			GetGroundCoord2ImageCoord(lat,lon,h,&c,&r);

			KP.SetPoint_p(i,c,r);
			KP.SetPoint_q(i,c_,r_);
		}
	}
	else
	{
		return "GCP data error";
	}

	//Transformation
	switch(option)
	{
		case 0:
			{
				conf.Conformal_Transform(KP);
				m_SD = sqrt(conf.variance);
				trans = &conf;
				break;
			}
		case 1:
			{
				aff.Affine_Transform(KP);
				m_SD = sqrt(aff.variance);
				trans = &aff;
				break;
			}
		case 2:
			{
				bil.Bilinear_Transform(KP);
				m_SD = sqrt(bil.variance);
				trans = &bil;
				break;
			}
		case 3:
			{
				pro.Projective_nonlinear_Transform(KP);
				m_SD = sqrt(pro.variance);
				trans = &pro;
				break;
			}
		default:
			{
				aff.Affine_Transform(KP);
				m_SD = sqrt(aff.variance);
				trans = &aff;
				break;
			}
	}

	out += trans->X.matrixout();

	additional_transform = true;
	
	return out;
}

/*Assemble A and L*/
void CRFM::MakeAandL(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
						   double *u, double *v, double *w,
						   double *x, double *y)
{
	unsigned int i, j;

	//denominator에는 1.0으로 고정되는 상수 항이 있음.
	double Coeff[20];

	double DEN, C_NUM, R_NUM;
	
	//Assemble A matrix
	
	//GCP cubic
	for(j=0; j<NUMDC; j++)
	{
		C_NUM = GetPolyValue(DC_u[j],DC_v[j],DC_w[j],dColNUM);
		R_NUM = GetPolyValue(DC_u[j],DC_v[j],DC_w[j],dRowNUM);
		DEN = GetPolyValue(DC_u[j],DC_v[j],DC_w[j],dRowDEN);

		
		CalDifferentialCoeff(DC_u[j],DC_v[j],DC_w[j],Coeff);

		//numerator term
		for(i=0; i<20; i++)
		{
			A(j*2,i) = Coeff[i]/DEN;
			A(j*2+1,i+20) = Coeff[i]/DEN;
		}

		//denominator term
		for(i=40; i<NUMRPCPARAM; i++)
		{
			A(j*2,i) = -Coeff[i-39]*C_NUM/DEN/DEN;
			A(j*2+1,i) = -Coeff[i-39]*R_NUM/DEN/DEN;
		}

		/**********/
		/*L matrix*/
		/**********/
		L(j*2,0) = DC_x[j] - C_NUM/DEN;
		L(j*2+1,0) = DC_y[j] - R_NUM/DEN;
	}
	
	//additional GCP
	for(j=0; j<(unsigned int)(mGCP.mG.GetGCPNum()); j++)
	{
		C_NUM = GetPolyValue(u[j],v[j],w[j],dColNUM);
		R_NUM = GetPolyValue(u[j],v[j],w[j],dRowNUM);
		DEN = GetPolyValue(u[j],v[j],w[j],dRowDEN);

		
		CalDifferentialCoeff(u[j],v[j],w[j],Coeff);
		
		//numerator term
		for(i=0; i<20; i++)
		{
			A(j*2+(NUMDC*2),i) = Coeff[i]/DEN;
			A(j*2+1+(NUMDC*2),i+20) = Coeff[i]/DEN;
		}

		//denominator term
		for(i=40; i<NUMRPCPARAM; i++)
		{
			A(j*2+(NUMDC*2),i) = -Coeff[i-39]*C_NUM/DEN/DEN;
			A(j*2+1+(NUMDC*2),i) = -Coeff[i-39]*R_NUM/DEN/DEN;
		}

		/**********/
		/*L matrix*/
		/**********/
		L(j*2+(NUMDC*2),0) = x[j] - C_NUM/DEN;
		L(j*2+1+(NUMDC*2),0) = y[j] - R_NUM/DEN;
	}
}

/*Assemble A and L*/
void CRFM::MakeAandL_OnlyGCP1st(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
						   double *u, double *v, double *w,
						   double *x, double *y)
{
	//A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
	//A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
	//A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;

	unsigned int i, j;

	//denominator에는 1.0으로 고정되는 상수 항이 있음.
	double Coeff[20];

	double DEN, C_NUM, R_NUM;
	
	//Assemble A matrix
	for(j=0; j<(unsigned int)(mGCP.mG.GetGCPNum()); j++)
	{
		C_NUM = GetPolyValue(u[j],v[j],w[j],dColNUM);
		R_NUM = GetPolyValue(u[j],v[j],w[j],dRowNUM);
		DEN = GetPolyValue(u[j],v[j],w[j],dRowDEN);

		
		CalDifferentialCoeff(u[j],v[j],w[j],Coeff);
		
		//numerator term
		for(i=0; i<4; i++)
		{
			A(j*2,i)     = Coeff[i]/DEN;
			A(j*2+1,i+4) = Coeff[i]/DEN;
		}

		//denominator term
		for(i=8; i<11; i++)
		{
			//A[0]=1.0 + A[1]*Lon + A[2]*Lat + A[3]*H
			A(j*2,i)   = -Coeff[i-8+1]*C_NUM/DEN/DEN;
			A(j*2+1,i) = -Coeff[i-8+1]*R_NUM/DEN/DEN;
		}

		/**********/
		/*L matrix*/
		/**********/
		L(j*2,0)   = x[j] - C_NUM/DEN;
		L(j*2+1,0) = y[j] - R_NUM/DEN;
	}
}

/*Assemble A and L*/
void CRFM::MakeAandL_OnlyGCP2nd(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
						   double *u, double *v, double *w,
						   double *x, double *y)
{
	//A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
	//A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
	//A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;

	unsigned int i, j;

	double Coeff[20];

	double DEN, C_NUM, R_NUM;
	
	//Assemble A matrix
	for(j=0; j<(unsigned int)(mGCP.mG.GetGCPNum()); j++)
	{
		C_NUM = GetPolyValue(u[j],v[j],w[j],dColNUM);
		R_NUM = GetPolyValue(u[j],v[j],w[j],dRowNUM);
		DEN = GetPolyValue(u[j],v[j],w[j],dRowDEN);

		
		CalDifferentialCoeff(u[j],v[j],w[j],Coeff);
		
		//numerator term
		for(i=0; i<6; i++)
		{
			A(j*2,i)     = Coeff[i+4]/DEN;
			A(j*2+1,i+6) = Coeff[i+4]/DEN;
		}

		//denominator term
		for(i=12; i<18; i++)
		{
			A(j*2,i)   = -Coeff[i-12+4]*C_NUM/DEN/DEN;
			A(j*2+1,i) = -Coeff[i-12+4]*R_NUM/DEN/DEN;
		}

		/**********/
		/*L matrix*/
		/**********/
		L(j*2,0)   = x[j] - C_NUM/DEN;
		L(j*2+1,0) = y[j] - R_NUM/DEN;
	}
}

/*Assemble A and L*/
void CRFM::MakeAandL_OnlyGCP3rd(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
						   double *u, double *v, double *w,
						   double *x, double *y)
{
	//A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
	//A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
	//A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;

	unsigned int i, j;

	double Coeff[20];

	double DEN, C_NUM, R_NUM;
	
	//Assemble A matrix
	for(j=0; j<(unsigned int)(mGCP.mG.GetGCPNum()); j++)
	{
		C_NUM = GetPolyValue(u[j],v[j],w[j],dColNUM);
		R_NUM = GetPolyValue(u[j],v[j],w[j],dRowNUM);
		DEN = GetPolyValue(u[j],v[j],w[j],dRowDEN);

		
		CalDifferentialCoeff(u[j],v[j],w[j],Coeff);
		
		//numerator term
		for(i=0; i<10; i++)
		{
			A(j*2,i)      = Coeff[i+10]/DEN;
			A(j*2+1,i+10) = Coeff[i+10]/DEN;
		}

		//denominator term
		for(i=20; i<30; i++)
		{
			A(j*2,i)   = -Coeff[i-20+10]*C_NUM/DEN/DEN;
			A(j*2+1,i) = -Coeff[i-20+10]*R_NUM/DEN/DEN;
		}

		/**********/
		/*L matrix*/
		/**********/
		L(j*2,0)   = x[j] - C_NUM/DEN;
		L(j*2+1,0) = y[j] - R_NUM/DEN;
	}
}

/*Assemble A and L (Linear Form Function)*/
void CRFM::MakeAandL_Linear(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
						   double *u, double *v, double *w,
						   double *x, double *y)
{
	unsigned int i, j;

	//denominator에는 1.0으로 고정되는 상수 항이 있음.
	double Coeff[20];
	
	//Assemble A matrix
	
	//GCP cubic
	for(j=0; j<NUMDC; j++)
	{
		
		CalDifferentialCoeff(DC_u[j],DC_v[j],DC_w[j],Coeff);

		//numerator term
		for(i=0; i<20; i++)
		{
			A(j*2,i) = Coeff[i];
			A(j*2+1,i+20) = Coeff[i];
		}

		//denominator term
		for(i=40; i<NUMRPCPARAM; i++)
		{
			A(j*2,i) = -Coeff[i-39]*DC_x[j];
			A(j*2+1,i) = -Coeff[i-39]*DC_y[j];
		}

		/**********/
		/*L matrix*/
		/**********/
		L(j*2,0) = DC_x[j];
		L(j*2+1,0) = DC_y[j];

	}
	
	//additional GCP
	for(j=0; j<(unsigned int)(mGCP.mG.GetGCPNum()); j++)
	{
		
		CalDifferentialCoeff(u[j],v[j],w[j],Coeff);

		//numerator term
		for(i=0; i<20; i++)
		{
			A(j*2+(NUMDC*2),i) = Coeff[i];
			A(j*2+1+(NUMDC*2),i+20) = Coeff[i];
		}

		//denominator term
		for(i=40; i<NUMRPCPARAM; i++)
		{
			A(j*2+(NUMDC*2),i) = -Coeff[i-39]*x[j];
			A(j*2+1+(NUMDC*2),i) = -Coeff[i-39]*y[j];
		}

		/**********/
		/*L matrix*/
		/**********/
		L(j*2+(NUMDC*2),0) = x[j];
		L(j*2+1+(NUMDC*2),0) = y[j];
	}
}

/*Assemble A and L*/
void CRFM::MakeAandL(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
						   double u, double v, double w,
						   double x, double y)
{
	unsigned int i, j;

	//denominator에는 1.0으로 고정되는 상수 항이 있음.
	double Coeff[20];

	double DEN, C_NUM, R_NUM;
	
	//Assemble A matrix
	
	//GCP cubic
	for(j=0; j<NUMDC; j++)
	{
		C_NUM = GetPolyValue(DC_u[j],DC_v[j],DC_w[j],dColNUM);
		R_NUM = GetPolyValue(DC_u[j],DC_v[j],DC_w[j],dRowNUM);
		DEN = GetPolyValue(DC_u[j],DC_v[j],DC_w[j],dRowDEN);

		
		CalDifferentialCoeff(DC_u[j],DC_v[j],DC_w[j],Coeff);

		//numerator term
		for(i=0; i<20; i++)
		{
			A(j*2,i) = Coeff[i]/DEN;
			A(j*2+1,i+20) = Coeff[i]/DEN;
		}

		//denominator term
		for(i=40; i<NUMRPCPARAM; i++)
		{
			A(j*2,i) = -Coeff[i-39]*C_NUM/DEN/DEN;
			A(j*2+1,i) = -Coeff[i-39]*R_NUM/DEN/DEN;
		}

		/**********/
		/*L matrix*/
		/**********/
		L(j*2,0) = DC_x[j] - C_NUM/DEN;
		L(j*2+1,0) = DC_y[j] - R_NUM/DEN;

	}
	
	//additional GCP
	C_NUM = GetPolyValue(u,v,w,dColNUM);
	R_NUM = GetPolyValue(u,v,w,dRowNUM);
	DEN = GetPolyValue(u,v,w,dRowDEN);

	
	CalDifferentialCoeff(u,v,w,Coeff);

	//numerator term
	for(i=0; i<20; i++)
	{
		A((NUMDC*2),i) = Coeff[i]/DEN;
		A(1+(NUMDC*2),i+20) = Coeff[i]/DEN;
	}

	//denominator term
	for(i=40; i<NUMRPCPARAM; i++)
	{
		A((NUMDC*2),i) = -Coeff[i-39]*C_NUM/DEN/DEN;
		A(1+(NUMDC*2),i) = -Coeff[i-39]*R_NUM/DEN/DEN;
	}

	/**********/
	/*L matrix*/
	/**********/
	L((NUMDC*2),0) = x - C_NUM/DEN;
	L(1+(NUMDC*2),0) = y - R_NUM/DEN;
}

/*Assemble A and L*/
void CRFM::MakeAandL_Param(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
						   double *u, double *v, double *w,
						   double *x, double *y)
{
	unsigned int i, j;

	//denominator에는 1.0으로 고정되는 상수 항이 있음.
	double Coeff[20];

	double DEN, C_NUM, R_NUM;
	
	//Assemble A matrix
	
	//Parameter Observation
	for(i=0; i<20; i++)
	{
		A(i,i) = 1.0;
		L(i,0) = old_dColNUM[i] - dColNUM[i];

		A(i+20,i+20) = 1.0;
		L(i+20,0) = old_dRowNUM[i] - dRowNUM[i];
	}
	for(i=0; i<19; i++)
	{
		A(i+40,i+40) = 1.0;
		L(i+40,0) = old_dColDEN[i+1] - dColDEN[i+1];
	}
		
	//additional GCP
	for(j=0; j<(unsigned int)(mGCP.mG.GetGCPNum()); j++)
	{
		C_NUM = GetPolyValue(u[j],v[j],w[j],dColNUM);
		R_NUM = GetPolyValue(u[j],v[j],w[j],dRowNUM);
		DEN = GetPolyValue(u[j],v[j],w[j],dRowDEN);

		
		CalDifferentialCoeff(u[j],v[j],w[j],Coeff);

		//numerator term
		for(i=0; i<20; i++)
		{
			A(j*2+NUMRPCPARAM,i) = Coeff[i]/DEN;
			A(j*2+1+NUMRPCPARAM,i+20) = Coeff[i]/DEN;
		}

		//denominator term
		for(i=40; i<NUMRPCPARAM; i++)
		{
			A(j*2+NUMRPCPARAM,i) = -Coeff[i-39]*C_NUM/DEN/DEN;
			A(j*2+1+NUMRPCPARAM,i) = -Coeff[i-39]*R_NUM/DEN/DEN;
		}

		/**********/
		/*L matrix*/
		/**********/
		L(j*2+NUMRPCPARAM,0) = x[j] - C_NUM/DEN;
		L(j*2+1+NUMRPCPARAM,0) = y[j] - R_NUM/DEN;
	}
}

/*Assemble A and L*/
void CRFM::MakeAandL_Param(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
						   double u, double v, double w,
						   double x, double y)
{
	unsigned int i;

	//denominator에는 1.0으로 고정되는 상수 항이 있음.
	double Coeff[20];
	
	
	double DEN, C_NUM, R_NUM;
	
	//Assemble A matrix
	
	//Parameter Observation
	for(i=0; i<20; i++)
	{
		A(i,i) = 1.0;
		L(i,0) = old_dColNUM[i] - dColNUM[i];

		A(i+20,i+20) = 1.0;
		L(i+20,0) = old_dRowNUM[i] - dRowNUM[i];
	}
	for(i=0; i<19; i++)
	{
		A(i+40,i+40) = 1.0;
		L(i+40,0) = old_dColDEN[i+1] - dColDEN[i+1];
	}
		
	//additional GCP
	C_NUM = GetPolyValue(u,v,w,dColNUM);
	R_NUM = GetPolyValue(u,v,w,dRowNUM);
	DEN = GetPolyValue(u,v,w,dRowDEN);

	
	CalDifferentialCoeff(u,v,w,Coeff);

	//numerator term
	for(i=0; i<20; i++)
	{
		A(NUMRPCPARAM,i) = Coeff[i]/DEN;
		A(1+NUMRPCPARAM,i+20) = Coeff[i]/DEN;
	}

	//denominator term
	for(i=40; i<NUMRPCPARAM; i++)
	{
		A(NUMRPCPARAM,i) = -Coeff[i-39]*C_NUM/DEN/DEN;
		A(1+NUMRPCPARAM,i) = -Coeff[i-39]*R_NUM/DEN/DEN;
	}

	/**********/
	/*L matrix*/
	/**********/
	L(NUMRPCPARAM,0) = x - C_NUM/DEN;
	L(1+NUMRPCPARAM,0) = y - R_NUM/DEN;
}

/*Assemble A and L*/
void CRFM::MakeAandL_Sequential_LSi(Matrix<double> &A, Matrix<double> &L, Matrix<double> &W,
						   double u, double v, double w,
						   double x, double y)
{
	unsigned int i;

	double Coeff[20];
	//denominator에는 1.0으로 고정되는 상수 항이 있음.
	

	double DEN, C_NUM, R_NUM;
	
	//Assemble A matrix
	
	//additional GCP
	C_NUM = GetPolyValue(u,v,w,dColNUM);
	R_NUM = GetPolyValue(u,v,w,dRowNUM);
	DEN = GetPolyValue(u,v,w,dRowDEN);

	CalDifferentialCoeff(u,v,w,Coeff);

	//numerator term
	for(i=0; i<20; i++)
	{
		A(0,i) = Coeff[i]/DEN;
		A(1,i+20) = Coeff[i]/DEN;
	}

	//denominator term
	for(i=40; i<NUMRPCPARAM; i++)
	{
		A(0,i) = -Coeff[i-39]*C_NUM/DEN/DEN;
		A(1,i) = -Coeff[i-39]*R_NUM/DEN/DEN;
	}

	/**********/
	/*L matrix*/
	/**********/
	L(0,0) = x - C_NUM/DEN;
	L(1,0) = y - R_NUM/DEN;
}

/*Differential Coefficient Calculation*/
void CRFM::CalDifferentialCoeff(double u, double v, double w, double *Coeff)
{
	
	Coeff[0] = 1.0;
	Coeff[1] = v;
	Coeff[2] = u;
	Coeff[3] = w;
	Coeff[4] = u*v;

	Coeff[5] = v*w;
	Coeff[6] = u*w;
	Coeff[7] = v*v;
	Coeff[8] = u*u;
	Coeff[9] = w*w;

	Coeff[10] = u*v*w;
	Coeff[11] = v*v*v;
	Coeff[12] = v*u*u;
	Coeff[13] = v*w*w;
	Coeff[14] = u*v*v;

	Coeff[15] = u*u*u;
	Coeff[16] = u*w*w;
	Coeff[17] = v*v*w;
	Coeff[18] = u*u*w;
	Coeff[19] = w*w*w;
}

/*Insert GCP*/
bool CRFM::InsertGCP(double Lat, double Lon, double H, double Col, double Row, long id)
{
	//현재 개발된 컴포넌트는 WGS84를 기준으로 하는 IKONOS를 위한 모듈이다.
	//좌표는 Lat, Lon, H의 순서를 표준으로 사용한다.
	//X, Y, Z의 개념에서 본다면...
	//Lon, Lat, H가 맞을 수 있지만...
	//현재...Lat, Lon, H의 순서로 사용되고 있다.

	mGCP.mG.InsertGCP(Lat, Lon, H, id);
	mGCP.mI.InsertICP(Col, Row, id);
	return true;
}

/*Get GCP*/
bool CRFM::GetGCP(unsigned int index, double *Lat, double *Lon, double *H, double *Col, double *Row)
{
	mGCP.mG.GetGCPCoordbySort(index+1, Lat, Lon, H);
	mGCP.mI.GetICPCoordbySort(index+1, Col, Row);

	return true;
}




/*****************************************************************************/
/************************Class for RFM Intersection***************************/
/*****************************************************************************/

//default constructor
CRFMIntersection::CRFMIntersection()
{
}

//destructor
CRFMIntersection::~CRFMIntersection()
{
}

//Set RFM
void CRFMIntersection::SetRFM(CRFM Lrfm, CRFM Rrfm)
{
	LRFM = Lrfm;
	RRFM = Rrfm;

}

//3D Coord Initial Approximation Value
void CRFMIntersection::InitApproximation(Point2D<double> La, Point2D<double> Ra)
{
	/**********************************************/
	/*Calculate Initial approximation value by DLT*/
	/*DLT Parameter provided by RFM parameters    */
	/**********************************************/

	Matrix<double> A(4,3,0.0);
	Matrix<double> L(4,1,0.0);
	Matrix<double> V, X;

	/*RPC Parameters*/
	//x = A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
	//	  A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
	//	  A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;

	/***************************************/
	/*element of A and L matrix calculation*/
	/***************************************/
	A(0,0) = (LRFM.dColNUM[1] - La.x*LRFM.dColDEN[1])/LRFM.dCoordScale[3];
	A(0,1) = (LRFM.dColNUM[2] - La.x*LRFM.dColDEN[2])/LRFM.dCoordScale[2];
	A(0,2) = (LRFM.dColNUM[3] - La.x*LRFM.dColDEN[3])/LRFM.dCoordScale[4];

	L(0,0) = La.x*LRFM.dColDEN[0] - LRFM.dColNUM[0]
		   + (LRFM.dColNUM[1] - La.x*LRFM.dColDEN[1])*LRFM.dCoordOffset[3]/LRFM.dCoordScale[3]
		   + (LRFM.dColNUM[2] - La.x*LRFM.dColDEN[2])*LRFM.dCoordOffset[2]/LRFM.dCoordScale[2]
		   + (LRFM.dColNUM[3] - La.x*LRFM.dColDEN[3])*LRFM.dCoordOffset[4]/LRFM.dCoordScale[4];

	A(1,0) = (LRFM.dRowNUM[1] - La.y*LRFM.dRowDEN[1])/LRFM.dCoordScale[3];
	A(1,1) = (LRFM.dRowNUM[2] - La.y*LRFM.dRowDEN[2])/LRFM.dCoordScale[2];
	A(1,2) = (LRFM.dRowNUM[3] - La.y*LRFM.dRowDEN[3])/LRFM.dCoordScale[4];

	L(1,0) = La.y*LRFM.dRowDEN[0] - LRFM.dRowNUM[0]
	       + (LRFM.dRowNUM[1] - La.y*LRFM.dRowDEN[1])*LRFM.dCoordOffset[3]/LRFM.dCoordScale[3]
		   + (LRFM.dRowNUM[2] - La.y*LRFM.dRowDEN[2])*LRFM.dCoordOffset[2]/LRFM.dCoordScale[2]
		   + (LRFM.dRowNUM[3] - La.y*LRFM.dRowDEN[3])*LRFM.dCoordOffset[4]/LRFM.dCoordScale[4];

	A(2,0) = (RRFM.dColNUM[1] - Ra.x*RRFM.dColDEN[1])/RRFM.dCoordScale[3];
	A(2,1) = (RRFM.dColNUM[2] - Ra.x*RRFM.dColDEN[2])/RRFM.dCoordScale[2];
	A(2,2) = (RRFM.dColNUM[3] - Ra.x*RRFM.dColDEN[3])/RRFM.dCoordScale[4];

	L(2,0) = Ra.x*RRFM.dColDEN[0] - RRFM.dColNUM[0]
		   + (RRFM.dColNUM[1] - Ra.x*RRFM.dColDEN[1])*RRFM.dCoordOffset[3]/RRFM.dCoordScale[3]
		   + (RRFM.dColNUM[2] - Ra.x*RRFM.dColDEN[2])*RRFM.dCoordOffset[2]/RRFM.dCoordScale[2]
		   + (RRFM.dColNUM[3] - Ra.x*RRFM.dColDEN[3])*RRFM.dCoordOffset[4]/RRFM.dCoordScale[4];

	A(3,0) = (RRFM.dRowNUM[1] - Ra.y*RRFM.dRowDEN[1])/RRFM.dCoordScale[3];
	A(3,1) = (RRFM.dRowNUM[2] - Ra.y*RRFM.dRowDEN[2])/RRFM.dCoordScale[2];
	A(3,2) = (RRFM.dRowNUM[3] - Ra.y*RRFM.dRowDEN[3])/RRFM.dCoordScale[4];

	L(3,0) = Ra.y*RRFM.dRowDEN[0] - RRFM.dRowNUM[0]
	       + (RRFM.dRowNUM[1] - Ra.y*RRFM.dRowDEN[1])*RRFM.dCoordOffset[3]/RRFM.dCoordScale[3]
		   + (RRFM.dRowNUM[2] - Ra.y*RRFM.dRowDEN[2])*RRFM.dCoordOffset[2]/RRFM.dCoordScale[2]
		   + (RRFM.dRowNUM[3] - Ra.y*RRFM.dRowDEN[3])*RRFM.dCoordOffset[4]/RRFM.dCoordScale[4];
	
	/*********************/
	/*Least Square Method*/
	/*********************/
	X = (A.Transpose()%A).Inverse()%A.Transpose()%L;

	/****************************/
	/*initial value substitution*/
	/****************************/
	GLon = X(0,0);
	GLat = X(1,0);
	GH   = X(2,0);

	/*************************/
	/*Normalized Ground Coord*/
	/*************************/
	LRFM.GetGroundCoord2NormalGroundCoord(GLat,GLon,GH,&u,&v,&w);
}

/*Differential Coefficient Calculate*/
void CRFMIntersection::CalDifferentialCoeff(void)
{
	double LCOLDEN, LCOLNUM, LROWDEN, LROWNUM;
	double RCOLDEN, RCOLNUM, RROWDEN, RROWNUM;
	
	LCOLDEN = GetPolyValue(u,v,w,LRFM.dColDEN);//denominator of left column term
	LCOLNUM = GetPolyValue(u,v,w,LRFM.dColNUM);//numerator of left column term
	LROWDEN = GetPolyValue(u,v,w,LRFM.dRowDEN);//denominator of left row term
	LROWNUM = GetPolyValue(u,v,w,LRFM.dRowNUM);//numerator of left row term

	RCOLDEN = GetPolyValue(u,v,w,RRFM.dColDEN);//denominator of riwt column term
	RCOLNUM = GetPolyValue(u,v,w,RRFM.dColNUM);//numerator of riwt column term
	RROWDEN = GetPolyValue(u,v,w,RRFM.dRowDEN);//denominator of riwt row term
	RROWNUM = GetPolyValue(u,v,w,RRFM.dRowNUM);//numerator of riwt row term

	/*RPC Parameters*/
	//F[G] = A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
	//	     A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
	//	     A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;

	//u(Lat) = Y
	//v(Lon) = X
	//w(H)   = Z
	
	/*for left column differential coefficient*/
	double L_C_N_Lat, L_C_N_Lon, L_C_N_H; //numerator
	double L_C_D_Lat, L_C_D_Lon, L_C_D_H; //denominator

	/*for left row differential coefficient*/
	double L_R_N_Lat, L_R_N_Lon, L_R_N_H; //numerator
	double L_R_D_Lat, L_R_D_Lon, L_R_D_H; //denominator

	/*for riwt column differential coefficient*/
	double R_C_N_Lat, R_C_N_Lon, R_C_N_H; //numerator
	double R_C_D_Lat, R_C_D_Lon, R_C_D_H; //denominator

	/*for riwt row differential coefficient*/
	double R_R_N_Lat, R_R_N_Lon, R_R_N_H; //numerator
	double R_R_D_Lat, R_R_D_Lon, R_R_D_H; //denominator

	/*Left*/
	/*Left Column Numerator*/
	L_C_N_Lon = GetDifferentialValue_Lon(u, v, w, LRFM.dColNUM)/LRFM.dCoordScale[3];
	L_C_N_Lat = GetDifferentialValue_Lat(u, v, w, LRFM.dColNUM)/LRFM.dCoordScale[2];
	L_C_N_H   = GetDifferentialValue_H(u, v, w, LRFM.dColNUM)/LRFM.dCoordScale[4];

	/*Left Column Denominator*/
	L_C_D_Lon = GetDifferentialValue_Lon(u, v, w, LRFM.dColDEN)/LRFM.dCoordScale[3];
	L_C_D_Lat = GetDifferentialValue_Lat(u, v, w, LRFM.dColDEN)/LRFM.dCoordScale[2];
	L_C_D_H   = GetDifferentialValue_H(u, v, w, LRFM.dColDEN)/LRFM.dCoordScale[4];

	/*Left Row Numerator*/
	L_R_N_Lon = GetDifferentialValue_Lon(u, v, w, LRFM.dRowNUM)/LRFM.dCoordScale[3];
	L_R_N_Lat = GetDifferentialValue_Lat(u, v, w, LRFM.dRowNUM)/LRFM.dCoordScale[2];
	L_R_N_H   = GetDifferentialValue_H(u, v, w, LRFM.dRowNUM)/LRFM.dCoordScale[4];

	/*Left Row Denominator*/
	L_R_D_Lon = GetDifferentialValue_Lon(u, v, w, LRFM.dRowDEN)/LRFM.dCoordScale[3];
	L_R_D_Lat = GetDifferentialValue_Lat(u, v, w, LRFM.dRowDEN)/LRFM.dCoordScale[2];
	L_R_D_H   = GetDifferentialValue_H(u, v, w, LRFM.dRowDEN)/LRFM.dCoordScale[4];

	/*Riwt*/
	/*Riwt Column Numerator*/
	R_C_N_Lon = GetDifferentialValue_Lon(u, v, w, RRFM.dColNUM)/RRFM.dCoordScale[3];
	R_C_N_Lat = GetDifferentialValue_Lat(u, v, w, RRFM.dColNUM)/RRFM.dCoordScale[2];
	R_C_N_H   = GetDifferentialValue_H(u, v, w, RRFM.dColNUM)/RRFM.dCoordScale[4];

	/*Riwt Column Denominator*/
	R_C_D_Lon = GetDifferentialValue_Lon(u, v, w, RRFM.dColDEN)/RRFM.dCoordScale[3];
	R_C_D_Lat = GetDifferentialValue_Lat(u, v, w, RRFM.dColDEN)/RRFM.dCoordScale[2];
	R_C_D_H   = GetDifferentialValue_H(u, v, w, RRFM.dColDEN)/RRFM.dCoordScale[4];

	/*Riwt Row Numerator*/
	R_R_N_Lon = GetDifferentialValue_Lon(u, v, w, RRFM.dRowNUM)/RRFM.dCoordScale[3];
	R_R_N_Lat = GetDifferentialValue_Lat(u, v, w, RRFM.dRowNUM)/RRFM.dCoordScale[2];
	R_R_N_H   = GetDifferentialValue_H(u, v, w, RRFM.dRowNUM)/RRFM.dCoordScale[4];

	/*Riwt Row Denominator*/
	R_R_D_Lon = GetDifferentialValue_Lon(u, v, w, RRFM.dRowDEN)/RRFM.dCoordScale[3];
	R_R_D_Lat = GetDifferentialValue_Lat(u, v, w, RRFM.dRowDEN)/RRFM.dCoordScale[2];
	R_R_D_H   = GetDifferentialValue_H(u, v, w, RRFM.dRowDEN)/RRFM.dCoordScale[4];

	/*Left*/
	/*Left Column*/
	LdC_dLon = (L_C_N_Lon*LCOLDEN - L_C_D_Lon*LCOLNUM)/(LCOLDEN*LCOLDEN);
	LdC_dLat = (L_C_N_Lat*LCOLDEN - L_C_D_Lat*LCOLNUM)/(LCOLDEN*LCOLDEN);
	LdC_dH   = (L_C_N_H*LCOLDEN - L_C_D_H*LCOLNUM)/(LCOLDEN*LCOLDEN);

	/*Left Row*/
	LdR_dLon = (L_R_N_Lon*LROWDEN - L_R_D_Lon*LROWNUM)/(LROWDEN*LROWDEN);
	LdR_dLat = (L_R_N_Lat*LROWDEN - L_R_D_Lat*LROWNUM)/(LROWDEN*LROWDEN);
	LdR_dH   = (L_R_N_H*LROWDEN - L_R_D_H*LROWNUM)/(LROWDEN*LROWDEN);

	/*Riwt*/
	/*Riwt Column*/
	RdC_dLon = (R_C_N_Lon*RCOLDEN - R_C_D_Lon*RCOLNUM)/(RCOLDEN*RCOLDEN);
	RdC_dLat = (R_C_N_Lat*RCOLDEN - R_C_D_Lat*RCOLNUM)/(RCOLDEN*RCOLDEN);
	RdC_dH   = (R_C_N_H*RCOLDEN - R_C_D_H*RCOLNUM)/(RCOLDEN*RCOLDEN);

	/*Riwt Row*/
	RdR_dLon = (R_R_N_Lon*RROWDEN - R_R_D_Lon*RROWNUM)/(RROWDEN*RROWDEN);
	RdR_dLat = (R_R_N_Lat*RROWDEN - R_R_D_Lat*RROWNUM)/(RROWDEN*RROWDEN);
	RdR_dH   = (R_R_N_H*RROWDEN - R_R_D_H*RROWNUM)/(RROWDEN*RROWDEN);
}

/*Jand K matrix element calculation*/
void CRFMIntersection::MakeMatrixJK(void)
{
	/**************************************/
	/*calculation differential coefficient*/
	/**************************************/
	CalDifferentialCoeff();

	//J matrix: 4*3 matrix
	J(0,0) = LdC_dLon;
	J(0,1) = LdC_dLat;
	J(0,2) = LdC_dH;

	J(1,0) = LdR_dLon;
	J(1,1) = LdR_dLat;
	J(1,2) = LdR_dH;

	J(2,0) = RdC_dLon;
	J(2,1) = RdC_dLat;
	J(2,2) = RdC_dH;

	J(3,0) = RdR_dLon;
	J(3,1) = RdR_dLat;
	J(3,2) = RdR_dH;

	//K matrix: 4*1 matrix
	double L_x, L_y;
	double R_x, R_y;
	//K = F - Fo
	LRFM.GetGroundCoord2NormalImageCoord(GLat, GLon, GH, &L_x, &L_y);
	RRFM.GetGroundCoord2NormalImageCoord(GLat, GLon, GH, &R_x, &R_y);

	K(0,0) = L.x - L_x;
	K(1,0) = L.y - L_y;

	K(2,0) = R.x - R_x;
	K(3,0) = R.y - R_y;
}

/*Do RFM Intersection*/
bool CRFMIntersection::DoIntersection(double LC, double LR, double RC, double RR,
									  double *Lat, double *Lon, double *H,
									  double MaxCorrection, unsigned short MaxIteration)
{
	/*******************************/
	/*Conjugate Point Coord Setting*/
	/*******************************/
	LRFM.GetImageCoord2NormalImageCoord(LC,LR,&L.x,&L.y);

	RRFM.GetImageCoord2NormalImageCoord(RC,RR,&R.x,&R.y);

	/********************/
	/*Init-approximation*/
	/********************/
	InitApproximation(L,R);

	/**************/
	/*LS Iteration*/
	/**************/
	bool MaxCorStop = false;
	bool MaxIterStop = false;

	unsigned int num_iter = 0;

	do
	{
		num_iter ++;

		/****************/
		/*J and K matrix*/
		/****************/
		J.Resize(4,3,0.0);
		K.Resize(4,1,0.0);

		MakeMatrixJK();

		/********************/
		/*calculate X matrix*/
		/********************/
		JT = J.Transpose();
		N = JT%J;
		Ninv = N.Inverse();
		
		//X Matrix
		X = Ninv%JT%K;

		//Residual Matrix
		V = (J%X) - K;

		//Posteriori variance
		VTV = V.Transpose()%V;
		pos_var = VTV(0,0);

		//Update Unknown
		GLon += X(0,0);
		GLat += X(1,0);
		GH += X(2,0);

		LRFM.GetGroundCoord2NormalGroundCoord(GLat,GLon,GH,&u,&v,&w);

		if((fabs(X(0,0))<fabs(MaxCorrection))&&
		   (fabs(X(1,0))<fabs(MaxCorrection))&&
		   (fabs(X(2,0))<fabs(MaxCorrection)))
		{
			MaxCorStop = true;
		}

		if(MaxIteration <= num_iter)
		{
			MaxIterStop = true;
		}

	}while((MaxCorStop == false)&&(MaxIterStop == false));

	/***********************/
	/*Geo-Coordinate return*/
	/***********************/
	*Lon = GLon;
	*Lat = GLat;
	*H   = GH;

	//LRFM.GetNormalGroundCoord2GroundCoord(u,v,w,Lat,Lon,H);

	return MaxCorStop;
}

/*****************/
/*Global Function*/
/*****************/

/*Get Polynomial value*/
double GetPolyValue(double Lat, double Lon, double H, double *A)
{
	double ans;
	
	ans = A[0] + A[1]*Lon + A[2]*Lat + A[3]*H + A[4]*Lon*Lat + A[5]*Lon*H + A[6]*Lat*H +
		  A[7]*Lon*Lon + A[8]*Lat*Lat + A[9]*H*H + A[10]*Lat*Lon*H + A[11]*Lon*Lon*Lon + A[12]*Lon*Lat*Lat + A[13]*Lon*H*H +
		  A[14]*Lon*Lon*Lat + A[15]*Lat*Lat*Lat + A[16]*Lat*H*H + A[17] *Lon*Lon*H + A[18]*Lat*Lat*H + A[19]*H*H*H;

	return ans;
}

/*Get Differential value*/
double GetDifferentialValue_Lat(double Lat, double Lon, double H, double *A)
{
	double ans;

	ans = A[2] + A[4]*Lon + A[6]*H + 2*A[8]*Lat +
		  A[10]*Lon*H + 2*A[12]*Lat*Lon + A[14]*Lon*Lon +
		  3*A[15]*Lat*Lat + A[16]*H*H + 2*A[18]*Lat*H;

	return ans;
}

double GetDifferentialValue_Lon(double Lat, double Lon, double H, double *A)
{
	double ans;

	ans = A[1] + A[4]*Lat + A[5]*H + 2*A[7]*Lon +
		  A[10]*Lat*H + 3*A[11]*Lon*Lon + A[12]*Lat*Lat +
		  A[13]*H*H + 2*A[14]*Lat*Lon + 2*A[17]*Lon*H;

	return ans;
}

double GetDifferentialValue_H(double Lat, double Lon, double H, double *A)
{
	double ans;

	ans = A[3] + A[5]*Lon + A[6]*Lat + 2*A[9]*H +
		  A[10]*Lat*Lon + 2*A[13]*Lon*H + 2*A[16]*Lat*H +
		  A[17]*Lon*Lon + A[18]*Lat*Lat + 3*A[19]*H*H;

	return ans;
}
