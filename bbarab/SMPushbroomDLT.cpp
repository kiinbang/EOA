// SMPushbroomDLT.cpp: implementation of the CParallel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SMPushbroomDLT.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define PI 3.141592654

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMPushDLT::CSMPushDLT()
{

}

CSMPushDLT::~CSMPushDLT()
{

}

CString CSMPushDLT::DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize)
{
	unsigned int num_param = 11;

	CString retval;

	retval = "\r\n\r\n[Pushbroom DLT Modeling]\r\n";
	retval += "row = (A1X + A2Y + A3Z + A4)/(A5X + A6Y + A7Z + 1)\r\n";
	retval += "col = (A8X + A9Y + A10Z + A11)\r\n";
		
	if(bNormalized == true) retval += "(***Data is normalized***)\r\n";
	
	/**
	*Calculate parameter approximation
	*/
	retval += "\r\n\r\n[Init_value of Parameters]\r\n";
	retval += "///////////////////////////////////////////////////////////////////\r\n";
	///////////////////////////////
	//Original Data backup
	///////////////////////////////
	retval += DoInitModeling(num_param);
	///////////////////////////////////////
	//Initial data copy for the iteration
	///////////////////////////////////////
	twin_SMData = SMData;
	///////////////////////////////////////
	retval += "///////////////////////////////////////////////////////////////////\r\n";

	FileOut(outfilename,retval);
	
	retval += Run(num_param, num_max_iter, maximum_sd, maximum_diff, outfilename, pixelsize);
	
	::Beep(500,50);
	return retval;
}

CString CSMPushDLT::DoInitModeling(unsigned int num_param)
{
	CString retval;
	
	retval="[Pushbroom DLT Initial Parameters]\r\n";
	
	
	/*********************************
	* Data Normalization
	*********************************/
	
	if(bNormalized == true) 
	{
		retval += "(***Data is normalized***)\r\n";
		DataNormalization();	
	}
	else 
	{		
		for(unsigned int i=0; i<SMData.SCENE_List.GetNumItem(); i++)
		{
			SMParam temp;
			param_list.AddTail(temp);
		}
	} 
	
	unsigned int SCENE_Num = SMData.SCENE_List.GetNumItem();
	
	CString stParam;
	CSMList<_GCP_> &gcp = SMData.GCP_List;
	unsigned int GCP_Num = gcp.GetNumItem();
	
	Matrix<double> Amat, Lmat, Wmat, Xmat;
	Amat.Resize(0,0,0.);
	Lmat.Resize(0,0,0.);
	Wmat.Resize(0,0,0.);
	
	for(unsigned int k=0; k<SCENE_Num; k++)
	{
		_SCENE_ scene = SMData.SCENE_List[k];
		PointOBSInit(SMData.GCP_List, scene, num_param, Amat, Lmat, Wmat, k);
		CLeastSquare LS;
		Xmat = LS.RunLeastSquare_Fast(Amat,Lmat,Wmat);
	}
	for(k=0; k<SCENE_Num; k++)
	{
		for(unsigned int i=0; i<num_param; i++)
		{
			param_list[k].A[i]=Xmat(k*num_param+i,0);
			stParam.Format("[A%d]%le\t",i,Xmat(k*num_param+i,0));
			retval += stParam;
		}
		stParam.Format("\r\n");
		retval += stParam;
	}
	
	return retval;
}

/**
*Calculating initial value (Fo) using approximate parameters.
*/
double CSMPushDLT::Func_row(_GCP_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double row = param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3];
	return row;
}
double CSMPushDLT::Func_row(_GCL_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double row = param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3];
	return row;
}
/**
*Calculating initial value (Go) using approximate parameters.
*/
double CSMPushDLT::Func_col(_GCP_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double col = (param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.);
	return col;
}
double CSMPushDLT::Func_col(_GCL_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double col = (param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7])
		/(param.A[8]*G.X + param.A[9]*G.Y + param.A[10]*G.Z + 1.);
	return col;
}

void CSMPushDLT::Cal_PDs_linear(_GCP_ G, _ICP_ I, double* ret, unsigned int image_index)
{
	SMParam param = param_list[image_index];

	//A1~A4
	ret[0] = G.X;
	ret[1] = G.Y;
	ret[2] = G.Z;
	ret[3] = 1.;
	//A5~A8
	ret[4] = G.X;
	ret[5] = G.Y;
	ret[6] = G.Z;
	ret[7] = 1.;
	//A9~A11
	ret[8]  = -I.col*G.X;
	ret[9]  = -I.col*G.Y;
	ret[10] = -I.col*G.Z;

	//X,Y,Z(row)
	ret[11] = param.A[0];
	ret[12] = param.A[1];
	ret[13] = param.A[2];
	//X,Y,Z(col)
	ret[14] = param.A[4]-I.col*param.A[8];
	ret[15] = param.A[5]-I.col*param.A[9];
	ret[16] = param.A[6]-I.col*param.A[10];
}

void CSMPushDLT::Cal_PDs(_GCP_ G, double* ret1, double* ret2, unsigned int index)
{
	SMParam param = param_list[index];

	double DEN = param.A[8]*G.X+param.A[9]*G.Y+param.A[10]*G.Z+1;
	double DEN2 = DEN*DEN;
	double NUM = param.A[4]*G.X+param.A[5]*G.Y+param.A[6]*G.Z+param.A[7];

	//A1~A4
	ret1[0] = G.X;
	ret1[1] = G.Y;
	ret1[2] = G.Z;
	ret1[3] = 1.;
	//A5~A8
	ret1[4] = 0.;
	ret1[5] = 0.;
	ret1[6] = 0.;
	ret1[7] = 0.;
	//A9~A11
	ret1[8 ] = 0.;
	ret1[9 ] = 0.;
	ret1[10] = 0.;
	//X,Y,Z(row)
	ret1[11] = param.A[0];
	ret1[12] = param.A[1];
	ret1[13] = param.A[2];

	//A1~A4
	ret2[0] = 0.;
	ret2[1] = 0.;
	ret2[2] = 0.;
	ret2[3] = 0.;
	//A5~A8
	ret2[4] = G.X/DEN;
	ret2[5] = G.Y/DEN;
	ret2[6] = G.Z/DEN;
	ret2[7] = 1./DEN;
	//A9~A11
	ret2[8]  = -G.X*NUM/DEN2;
	ret2[9]  = -G.Y*NUM/DEN2;
	ret2[10] = -G.Z*NUM/DEN2;
	//X,Y,Z(col)
	ret2[11] = (param.A[4]*DEN-param.A[8] *NUM)/DEN2;
	ret2[12] = (param.A[5]*DEN-param.A[9] *NUM)/DEN2;
	ret2[13] = (param.A[6]*DEN-param.A[10]*NUM)/DEN2;
}
void CSMPushDLT::Cal_PDs_Line_linear(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int image_index, unsigned int num_param)
{
	SMParam param = param_list[image_index];

	_ICP_ icp1, icp2;
	_GCP_ gcp1, gcp2;
	
	gcp1.X = GL1.X;
	gcp1.Y = GL1.Y;
	gcp1.Z = GL1.Z;
	
	gcp2.X = GL2.X;
	gcp2.Y = GL2.Y;
	gcp2.Z = GL2.Z;
	
	icp1.col = Func_col(gcp1, image_index);
	icp1.row = Func_row(gcp1, image_index);
	
	icp2.col = Func_col(gcp2, image_index);
	icp2.row = Func_row(gcp2, image_index);
	
	//A1~A4
	double v_v2 = IL.col - icp2.col;
	double v1_v = icp1.col - IL.col;
	ret[0] = gcp2.X*v1_v + gcp1.X*v_v2;
	ret[1] = gcp2.Y*v1_v + gcp1.Y*v_v2;
	ret[2] = gcp2.Z*v1_v + gcp1.Z*v_v2;
	ret[3] = icp1.col-icp2.col;
	//A5~A8
	double u2_u = icp2.row - IL.row;
	double u_u1 = IL.row - icp1.row;
	ret[4] = u_u1*gcp2.X + u2_u*gcp1.X;
	ret[5] = u_u1*gcp2.Y + u2_u*gcp1.Y;
	ret[6] = u_u1*gcp2.Z + u2_u*gcp1.Z;
	ret[7] = icp2.row - icp1.row;
	//A9~A11
	ret[8]  = u_u1*(-icp2.col*gcp2.X) + u2_u*(-icp1.col*gcp1.X);
	ret[9]  = u_u1*(-icp2.col*gcp2.Y) + u2_u*(-icp1.col*gcp1.Y);
	ret[10] = u_u1*(-icp2.col*gcp2.Z) + u2_u*(-icp1.col*gcp1.Z);
	
	//X1,Y1,Z1
	double v2_v = icp2.col - IL.col;
	ret[11] = u2_u*(param.A[4]-icp1.col*param.A[8])  - v2_v*param.A[0];
	ret[12] = u2_u*(param.A[5]-icp1.col*param.A[9])  - v2_v*param.A[1];
	ret[13] = u2_u*(param.A[6]-icp1.col*param.A[10]) - v2_v*param.A[2];
	//X2,Y2,Z2
	ret[14] = u_u1*(param.A[4]-icp2.col*param.A[8])  + v1_v*param.A[0];
	ret[15] = u_u1*(param.A[5]-icp2.col*param.A[9])  + v1_v*param.A[1];
	ret[16] = u_u1*(param.A[6]-icp2.col*param.A[10]) + v1_v*param.A[2];
}

void CSMPushDLT::Cal_PDs_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int image_index, unsigned int num_param)
{
	SMParam param = param_list[image_index];

	_ICP_ icp1, icp2;
	_GCP_ gcp1, gcp2;
	
	gcp1.X = GL1.X;
	gcp1.Y = GL1.Y;
	gcp1.Z = GL1.Z;
	
	gcp2.X = GL2.X;
	gcp2.Y = GL2.Y;
	gcp2.Z = GL2.Z;
	
	icp1.col = Func_col(gcp1, image_index);
	icp1.row = Func_row(gcp1, image_index);
	
	icp2.col = Func_col(gcp2, image_index);
	icp2.row = Func_row(gcp2, image_index);
	
	//A1~A4
	double v_v2 = IL.col - icp2.col;
	double v1_v = icp1.col - IL.col;
	ret[0] = gcp2.X*v1_v + gcp1.X*v_v2;
	ret[1] = gcp2.Y*v1_v + gcp1.Y*v_v2;
	ret[2] = gcp2.Z*v1_v + gcp1.Z*v_v2;
	ret[3] = icp1.col-icp2.col;
	//A5~A8
	double u2_u = icp2.row - IL.row;
	double u_u1 = IL.row - icp1.row;
	double DEN_2 = param.A[8]*gcp2.X+param.A[9]*gcp2.Y+param.A[10]*gcp2.Z+1;
	double DEN_1 = param.A[8]*gcp1.X+param.A[9]*gcp1.Y+param.A[10]*gcp1.Z+1;
	ret[4] = u_u1*(gcp2.X/DEN_2) + u2_u*(gcp1.X/DEN_1);
	ret[5] = u_u1*(gcp2.Y/DEN_2) + u2_u*(gcp1.Y/DEN_1);
	ret[6] = u_u1*(gcp2.Z/DEN_2) + u2_u*(gcp1.Z/DEN_1);
	ret[7] = u_u1/DEN_2 + u2_u/DEN_1;
	//A9~A11
	double NUM_2 = param.A[4]*gcp2.X+param.A[5]*gcp2.Y+param.A[6]*gcp2.Z+param.A[7];
	double NUM_1 = param.A[4]*gcp1.X+param.A[5]*gcp1.Y+param.A[6]*gcp1.Z+param.A[7];
	ret[8]  = u_u1*(-gcp2.X*NUM_2/DEN_2/DEN_2) + u2_u*(-gcp1.X*NUM_1/DEN_1/DEN_1);
	ret[9]  = u_u1*(-gcp2.Y*NUM_2/DEN_2/DEN_2) + u2_u*(-gcp1.Y*NUM_1/DEN_1/DEN_1);
	ret[10] = u_u1*(-gcp2.Z*NUM_2/DEN_2/DEN_2) + u2_u*(-gcp1.Z*NUM_1/DEN_1/DEN_1);
	
	//X1,Y1,Z1
	double v2_v = icp2.col - IL.col;
	ret[11] = u2_u*(param.A[4]-icp1.col*param.A[8])  - v2_v*param.A[0];
	ret[12] = u2_u*(param.A[5]-icp1.col*param.A[9])  - v2_v*param.A[1];
	ret[13] = u2_u*(param.A[6]-icp1.col*param.A[10]) - v2_v*param.A[2];
	//X2,Y2,Z2
	ret[14] = u_u1*(param.A[4]*DEN_2-param.A[8] *NUM_2)/DEN_2/DEN_2 + v1_v*param.A[0];
	ret[15] = u_u1*(param.A[5]*DEN_2-param.A[9] *NUM_2)/DEN_2/DEN_2 + v1_v*param.A[1];
	ret[16] = u_u1*(param.A[6]*DEN_2-param.A[10]*NUM_2)/DEN_2/DEN_2 + v1_v*param.A[2];
}

void CSMPushDLT::PointOBSInit(CSMList<_GCP_> &gcp, _SCENE_ &scene, unsigned int num_param, Matrix<double> &A, Matrix<double> &L, Matrix<double> &W, unsigned int scene_index)
{
	unsigned int GCP_Num = gcp.GetNumItem();
	if(GCP_Num == 0) return;
	unsigned int i, j;
	unsigned int ICP_Num = scene.ICP_List.GetNumItem();
	if(ICP_Num == 0) return;

	for(i=0; i<GCP_Num; i++)
	{
		Matrix<double> l(2,1,0), a1(2,num_param,0), w(2,2,0);
		_GCP_ GP = gcp.GetAt(i);

		if(GP.s[0]>GCP_sd_threshold) continue;
		for(j=0; j<ICP_Num; j++)
		{
			_ICP_ IP = scene.ICP_List[j];
			if(IP.PID != GP.PID) continue;
			
			double *ret = new double[num_param+6];
			Cal_PDs_linear(GP, IP, ret, scene_index);
			l(0,0) = IP.row;
			l(1,0) = IP.col;
			//u(row)
			//A1~A4
			a1(0,0) = ret[0]; a1(0,1) = ret[1]; a1(0,2) = ret[2]; a1(0,3) = ret[3];
			//v(col)
			//A5~A8
			a1(1,4) = ret[4]; a1(1,5) = ret[5]; a1(1,6) = ret[6]; a1(1,7) = ret[7];
			//A9~A11
			a1(1,8) = ret[8]; a1(1,9) = ret[9]; a1(1,10) = ret[10];
			//weight
			w(0,0) = 1./IP.s[0]; //s(1,1)
			w(1,1) = 1./IP.s[3]; //s(2,2)

			unsigned int pos = A.GetRows();
			A.Insert(pos,scene_index*num_param,a1);
			L.Insert(pos,0,l);
			W.Insert(pos,pos,w);
		}
	}
}

bool CSMPushDLT::FindFlagB(unsigned int GCL_Num, CSMList<_GCL_> gcl, CString LID, unsigned int nStart, _GCL_ &GLB)
{
	for(unsigned int i=nStart; i<GCL_Num; i++)
	{
		GLB = gcl[i];
		if((GLB.LID == LID)&&(GLB.flag=="B"))
			return true;
	}
	return false;
}

bool CSMPushDLT::FindFlagC(CSMList<_ICL_> &icl, CString LID, _ICL_ &ILA, _ICL_ &ILB)
{
	for(unsigned int i=0; i<icl.GetNumItem(); i++)
	{
		ILA = icl[i];
		if(ILA.LID == LID)
		{
			for(unsigned int j=i; j<icl.GetNumItem(); j++)
			{
				ILB = icl[j];
				if(ILB.LID == LID)
					return true;
			}
		}
	}

	return false;
}

SMParam CSMPushDLT::ParamDenormalization(SMParam param, unsigned int num_param)
{
	//double* Nparam = SMData.DataNoramlization();
	//SHIFT_X[0], SCALE_X[1], 
	//SHIFT_Y[2], SCALE_Y[3], 
	//SHIFT_Z[4], SCALE_Z[5], 
	//SHIFT_col[6], SCALE_col[7], 
	//SHIFT_row[8], SCALE_row[9]

	double* A = new double[num_param]; 
	for(unsigned int i=0; i<num_param; i++) 
		A[i] = param.A[i];

	double alpha = param.A[8];

	double L = A[8] *param.SHIFT_X*param.SCALE_X
		     + A[9] *param.SHIFT_Y*param.SCALE_Y
			 + A[10]*param.SHIFT_Z*param.SCALE_Z 
			 + 1;

	double M = A[0]*param.SHIFT_X*param.SCALE_X/param.SCALE_row
		     + A[1]*param.SHIFT_Y*param.SCALE_Y/param.SCALE_row
			 + A[2]*param.SHIFT_Z*param.SCALE_Z/param.SCALE_row
			 + A[3]/param.SCALE_row;

	param.A[0] = A[0]*param.SCALE_X/param.SCALE_row/L - A[8] *param.SCALE_X*param.SHIFT_row/L;
	param.A[1] = A[1]*param.SCALE_Y/param.SCALE_row/L - A[9] *param.SCALE_Y*param.SHIFT_row/L;
	param.A[2] = A[2]*param.SCALE_Z/param.SCALE_row/L - A[10]*param.SCALE_Z*param.SHIFT_row/L;
	param.A[3] = M/L - param.SHIFT_row;

	param.A[4] = A[4]*param.SCALE_X/param.SCALE_col;
	param.A[5] = A[5]*param.SCALE_Y/param.SCALE_col;
	param.A[6] = A[6]*param.SCALE_Z/param.SCALE_col;
	param.A[7] = (A[4]*param.SCALE_X*param.SHIFT_X
		       + A[5]*param.SCALE_Y*param.SHIFT_Y
			   + A[6]*param.SCALE_Z*param.SHIFT_Z
			   + A[7]
			   - param.SHIFT_col*param.SCALE_col)/param.SCALE_col;

	param.A[8]  = A[8] *param.SCALE_X/L;
	param.A[9]  = A[9] *param.SCALE_Y/L;
	param.A[10] = A[10]*param.SCALE_Z/L;
	
	param.SHIFT_X = 0;
	param.SCALE_X = 1;
	param.SHIFT_Y = 0;
	param.SCALE_Y = 1;
	param.SHIFT_Z = 0;
	param.SCALE_Z = 1;
	param.SHIFT_col = 0;
	param.SCALE_col = 1;
	param.SHIFT_row = 0;
	param.SCALE_row = 1;

	return param;
}