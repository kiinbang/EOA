// SMOrthoimageFrame.cpp : implementation file
//

#include "stdafx.h"
#include <SMOrthoimageFrameClass.h>
#include <Collinearity.h>
#include <ProgressBar.h>

#define PATCH_ORTHO_RESOLUTION 10

#define BOUNDARY_MARGIN 300
#define FIRST_RECORD -999
#define _MAXDIFFROW_ 0.5
#define _MAX_STRING_SIZE_ 512
#define BACKGROUND 255
#define _NULLDEM_ 255
#define _OCCLUSION_ 255

using namespace SMATICS_BBARAB;

/////////////////////////////////////////////////////////////////////////////
//
//
//
//
// CFrameOrthoClass
//
//
//
//

//bool CFrameOrthoClass::MakeOrtho(CImage &oriImage, CString parampath, CString DEMPath, CString HDRPath, CString OrthoPath, bool bZBuf, bool bBilinear, double MaxDiffZ, double SInterval, bool angle_availability)
bool CFrameOrthoClass::GroundCoord2SceneCoord(double X, double Y, double Z, Point2D<double> &point)
{
	//Ground to photo(without distortion)
	point = CollinearityEQ(Oo, Po, Ko, Xo, Yo, Zo, X, Y, Z, f);
	
	//Add distortion
	double dx = 0; double dy = 0;
	double dx_, dy_;
	if(LD_tag == 0)
	{
	}
	else if(LD_tag == 1)
	{
	}
	else if (LD_tag == 2)
    {
		//radial distance
        double rr = point.x*point.x + point.y*point.y;

		//threshold
		double diff=0.0001; //0.1 micro meter
		//stop sign
		bool bStop = true;
		
		//photo coord without dirtortion
		double x = point.x, y = point.y;
		//To find distortion using try-and-false
		do{
			//add approximate distortion
			double x_ = point.x + dx; 
			double y_ = point.y + dy;

			//radial distance component
			double r2 = x_*x_ + y_*y_;
			double r4 = r2*r2;
			
			dx_ = k1*(r2-1)*x_ + k2*(r4-1)*x_ //radial
				+ d1*(r2 + 2*x_*x_) + d2*2*x_*y_ //decentric
				- a1*x_ + a2*y_;//affine
			dy_ = k1*(r2-1)*y_ + d2*(r4-1)*y_ //radial
				+ d1*2*x_*y_ + d2*(r2 + 2*y_*y_) //decentric
				+ a1*y_;//affine

			if((fabs(dx_-dx)<diff)&&(fabs(dy_-dy)<diff))
				bStop = false;
			else
				dx = dx_; dy = dy_;

		}while(bStop);
    }

	//principal point and distortion
	point.x = point.x + xp + dx;
	point.y = point.y + yp + dy;

	//photo to image
	double x, y;
	x = point.x;
	y = point.y;
		
	point.x = AffineParam[0] + x*AffineParam[1] + y*AffineParam[2];
	point.y = AffineParam[3] + x*AffineParam[4] + y*AffineParam[5];
	
	return true;
}

bool CFrameOrthoClass::ResampleOrtho_Slope_DiffZ()
{
	CString strProgressBar = "Resampling";
	CProgressBar bar(strProgressBar, 100, m_RefDEM->GetHeight());
	
	DWORD W, H;
	W = (DWORD)m_Source->GetWidth();
	H = (DWORD)m_Source->GetHeight();

	CSMGridCell<BYTE> p(m_Source->GetnBands());
	//for(DWORD count=0; count<black.nBands; count++) black.gridcell[count] = (BYTE)0;

	//To initialize ortho-photo using BACKGROUND
	for(DWORD j=0 ; j<(DWORD)m_Ortho.GetHeight() ; j++) 
	{
		for(DWORD i=0 ; i<(DWORD)m_Ortho.GetWidth(); i++)
		{
			p = BACKGROUND;
			p >> m_Ortho(i, j);
		}
	}

	double DiffZ = MaxZ - MinZ;

	for(DWORD i=0;i<(DWORD)m_RefDEM->GetHeight();i++)
	{
		for(DWORD j=0;j<(DWORD)m_RefDEM->GetWidth();j++)
		{
			double X, Y, Z;
			Point2D<double> a;
			
			m_RefDEM->GetXYZ((int)j,(int)i,X,Y,Z);

			/**************************************************************/
			GroundCoord2SceneCoord(X, Y, Z, a);
			a.x = double(int(a.x+0.5)); a.y = double(int(a.y+0.5));
			/**************************************************************/

			//Nearest Neighborhood
			if(ResamplingTag == 0)
			{
				if(((a.x >= 0) && (a.y >=0)) && ((a.x <= W-1) && (a.y <= H-1)))
				{
					if(Z == m_NullDEM)
					{
						p = _NULLDEM_;
						p >> m_Ortho(j, i);
					}
					//Method 1: Slope method (using difference of height)
					else if(MethodTag == (unsigned int)1)
					{
						//To remove occlusion
						if(false == CheckOcclusion_DiffZ(Xo,Yo,Zo,j,i,DiffZ))
						{
							p = 0;//Black
							p >> m_Ortho(j, i);
						}
						else
						{
							p << m_Source->operator()((int)a.x, (int)a.y);
							p >> m_Ortho(j, i);
						}
						
					}
					//Method 1: simple digital differential rectification
					else
					{
						p << m_Source->operator()((int)a.x, (int)a.y);
						p >> m_Ortho(j, i);
					}
					
				}
			}
			//Bilinear interpolation
			else if(ResamplingTag == 1)		
			{
				if(((a.x >= 1) && (a.y >= 1)) && ((a.x <= W-2) && (a.y <= H-2)))
				{
					if(Z == m_NullDEM)
					{
						p = _NULLDEM_;
						p >> m_Ortho(j, i);
					}
					//Method 1: Slope method (using difference of height)
					else if(MethodTag == (unsigned int)1)
					{
						//To remove occlusion
						if(false == CheckOcclusion_DiffZ(Xo,Yo,Zo,j,i,DiffZ))
						{
							p = 0;//Black
							p >> m_Ortho(j, i);
						}
						else
						{
							CSMGridCell<BYTE> value1(m_Source->GetnBands());
							CSMGridCell<BYTE> value2(m_Source->GetnBands());
							CSMGridCell<BYTE> value3(m_Source->GetnBands());
							CSMGridCell<BYTE> value4(m_Source->GetnBands());
							value1 << m_Source->operator()((int)(a.x)		, (int)(a.y));
							value2 << m_Source->operator()((int)(a.x + 1)	, (int)(a.y));
							value3 << m_Source->operator()((int)(a.x)		, (int)(a.y + 1));
							value4 << m_Source->operator()((int)(a.x + 1)	, (int)(a.y + 1));
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> m_Ortho(j, i);
						}
						
					}
					//Method 1: simple digital differential rectification
					else
					{
						CSMGridCell<BYTE> value1(m_Source->GetnBands());
							CSMGridCell<BYTE> value2(m_Source->GetnBands());
							CSMGridCell<BYTE> value3(m_Source->GetnBands());
							CSMGridCell<BYTE> value4(m_Source->GetnBands());
							value1 << m_Source->operator()((int)(a.x)		, (int)(a.y));
							value2 << m_Source->operator()((int)(a.x + 1)	, (int)(a.y));
							value3 << m_Source->operator()((int)(a.x)		, (int)(a.y + 1));
							value4 << m_Source->operator()((int)(a.x + 1)	, (int)(a.y + 1));
							
							double A, B, C, D;
							A = (double)((int)a.x + 1) - a.x;
							B = 1 - A;
							C = (double)((int)a.y + 1) - a.y;
							D = 1 - C;
							
							(value1*A*C + value2*B*C + value3*A*D + value4*B*D) >> m_Ortho(j, i);
					}
					
				}
			}
		}

		bar.StepIt();
	}

	return true;
}

bool CFrameOrthoClass::CheckOcclusion_DiffZ(double Xl, double Yl, double Zl, int indexX, int indexY, double MAXDIFFHEIGHT)
{
	//Ground point (DEM point)
	double Xa, Ya, Za; 
	m_RefDEM->GetXYZ(indexX, indexY,Xa,Ya,Za);
	
	int nX, nY;
	nX = indexX + int((Xl - Xa)/m_RefDEM->resolution_X + 0.5);
	nY = indexY + int((Yl - Ya)/m_RefDEM->resolution_Y + 0.5);

	double a, b;
	//Y = a*X + b
	double diffX = ((double)indexX - (double)nX);
	if(fabs(diffX) < 1.0e-99) {a = 1.0e99;}//a의 절대값이 무한대에 가까우면 부호는 의미가 없다.
	else
	{
		a = ((double)indexY - (double)nY)/diffX;
		//DEM Grid는 행번호의 증가와 Y좌표의 증가가 서로 반대이다.
		a = -a;
	}
	
	b = (double)indexY - a*(double)indexX;
	
	//Dist_X is longer than dist_Y;
	if(abs(indexY - nY)<abs(indexX - nX))
	{
		double limit_Z = (Zl - Za)/(double)(abs(indexX - nX));
		
		int ix, iy;
		int interval;
		int offset;
		
		if(nX>indexX) offset = +1;
		else offset = -1;
		
		interval = 0;
		
		do
		{
			//To increase the interval
			interval += offset;
			ix = indexX + interval;
			
			//Y = a*X + b
			iy = int(a*ix + b + 0.5);
			
			//check the boundary
			double Z;
			if(false == m_RefDEM->GetZ(ix, iy, Z)) break;
			if(Z == m_NullDEM) continue;

			double dZ = Z - Za;
			double maxdZ = fabs(limit_Z*interval);

			if(maxdZ>MAXDIFFHEIGHT) break;
			if(Z>MaxZ) break;
			if((dZ)>maxdZ) return false;
			if((Za+maxdZ)>MaxZ) break;
		} while(1);
	}
	else//Dist_Y is longer than Dist_X;
	{
		double limit_Z = fabs((Zl - Za)/(indexY - nY));
		
		int ix, iy;
		int interval;
		int offset;
		
		//DEM Grid는 행번호의 증가와 Y좌표의 증가가 서로 반대이다.
		if(nY>indexY) offset = -1;
		else offset = +1;
		
		interval = 0;
		
		do
		{
			//To increase the interval
			interval += offset;
			iy = indexY + interval;
			
			//Y = a*X + b
			ix = int((iy-b)/a + 0.5);
			
			double Z;
			if(false == m_RefDEM->GetZ(ix, iy, Z)) break;
			if(Z == m_NullDEM) continue;

			double dZ = Z - Za;
			double maxdZ = fabs(limit_Z*interval);

			if(maxdZ>MAXDIFFHEIGHT) break;
			if(Z>MaxZ) break;
			if((dZ)>maxdZ) return false;
			if((Za+maxdZ)>MaxZ) break;
		} while(1);
	}
	
	return true;
}

void CFrameOrthoClass::SetConfig(CSMGrid<BYTE> &source, CSMGridDEM<double> &dem, double focal, double ppx, double ppy, double *LDParam, double *Affine, double *Pos, double *Ori, int nMethod, int nResample)
{
	//LDParam[6]
	//Affine[6]
	//Pos[3]
	//Ori[3]

	m_Source = &source;
	m_RefDEM = &dem;
	
	f = focal;
	xp = ppx;
	yp = ppy;
	
	k1 = LDParam[0];
	k2 = LDParam[1];
	d1 = LDParam[2];
	d2 = LDParam[3];
	a1 = LDParam[4];
	a2 = LDParam[5];

	memcpy(AffineParam, Affine, sizeof(double)*6);

	Xo = Pos[0];
	Yo = Pos[1];
	Zo = Pos[2];

	Oo = Ori[0];
	Po = Ori[1];
	Ko = Ori[2];

	MethodTag = nMethod;
	ResamplingTag = nResample;
}
