/*
* Copyright (c) 2005-2015, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#pragma pack(push, 1)

#include <afx.h>
#include <vector>

#include "SMLASPointFormat.h"
#include "SMLASHeader.h"
#include "SMLASVariableLengthRecords.h"

#define SCALE_MM_PRECISION 1.0e-4;

#define FileMarker UserData
#define UserBitField PointSourceID

enum LAS_VERSION {V10=10, V11=11, V12=12, V14=14};

//It is not recommended to use this type for all points because of the size of this class. 
//Use this class for point data exchange between different types
class LASPoint_Common 
{
public:
	long				X;
	long				Y;
	long				Z;
	unsigned short		Intensity;
	unsigned char		ReturnNumber : 3;
	unsigned char		NumberOfReturns : 3;
	unsigned char		ClassificationFlags : 4;
	unsigned char		ScannerChannel : 2;
	unsigned char		ScanDirectionFlag : 1;
	unsigned char		EdgeofFlightLine : 1;
	unsigned char		Classification;
	short				ScanAngleRank;//-90(left) to 90(right)
	unsigned char		UserData;
	unsigned short		PointSourceID;
	//char				GPSTime[8];//should be changed as double when it is used
	double				GPSTime;
	unsigned short		Red;
	unsigned short		Green;
	unsigned short		Blue;
	unsigned short		NIR;

public:
	LASPoint_Common()
	{
		NumberOfReturns = 1;
		PointSourceID = 0;
		ReturnNumber = 1;
		ScanAngleRank = 0;
		Classification = 0;
		ScanDirectionFlag = 0;
		UserData = 0;
	}

	LASPoint_Common(LASPoint_Common &copy){ DataCopy(copy); }
	LASPoint_Common(PointFormat0 &copy){ SetPointData(copy); }
	LASPoint_Common(PointFormat1 &copy){ SetPointData(copy); }
	LASPoint_Common(PointFormat2 &copy){ SetPointData(copy); }
	LASPoint_Common(PointFormat3 &copy){ SetPointData(copy); }
	LASPoint_Common(PointFormat7 &copy) { SetPointData(copy); }
	LASPoint_Common(PointFormat8 &copy) { SetPointData(copy); }

	LASPoint_Common& operator = (LASPoint_Common &copy){ DataCopy(copy); return (*this); }
	LASPoint_Common& operator = (PointFormat0 &copy){ SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (PointFormat1 &copy){ SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (PointFormat2 &copy){ SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (PointFormat3 &copy){ SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (PointFormat7 &copy) { SetPointData(copy); return (*this); }
	LASPoint_Common& operator = (PointFormat8 &copy) { SetPointData(copy); return (*this); }

	void DataCopy(LASPoint_Common &copy)
	{
		memcpy(this, &copy, sizeof(LASPoint_Common));
	}

	void SetPointData(const PointFormat0 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		Red					= copy.Intensity;
		Green				= copy.Intensity;
		Blue				= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		UserData			= copy.UserData;
		PointSourceID		= copy.PointSourceID;
	}

	void SetPointData(const PointFormat1 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		Red					= copy.Intensity;
		Green				= copy.Intensity;
		Blue				= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		UserData			= copy.UserData;
		PointSourceID		= copy.PointSourceID;
		memcpy(&GPSTime, &(copy.GPSTime), 8);
	}

	void SetPointData(const PointFormat2 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		UserData			= copy.UserData;
		PointSourceID		= copy.PointSourceID;
		Red					= copy.Red;
		Green				= copy.Green;
		Blue				= copy.Blue;
	}

	void SetPointData(const PointFormat3 &copy)
	{
		X					= copy.X;
		Y					= copy.Y;
		Z					= copy.Z;
		Intensity			= copy.Intensity;
		ReturnNumber		= copy.ReturnNumber;
		NumberOfReturns		= copy.NumberOfReturns;
		ScanDirectionFlag	= copy.ScanDirectionFlag;
		EdgeofFlightLine	= copy.EdgeofFlightLine;
		Classification		= copy.Classification;
		ScanAngleRank		= copy.ScanAngleRank;
		UserData			= copy.UserData;
		PointSourceID		= copy.PointSourceID;
		memcpy(&GPSTime, &(copy.GPSTime), 8);
		Red					= copy.Red;
		Green				= copy.Green;
		Blue				= copy.Blue;
	}

	void SetPointData(const PointFormat7 &copy)
	{
		X = copy.X;
		Y = copy.Y;
		Z = copy.Z;
		Intensity = copy.Intensity;
		ReturnNumber = copy.ReturnNumber;
		NumberOfReturns = copy.NumberOfReturns;
		ClassificationFlags = copy.ClassificationFlags;
		ScannerChannel = copy.ScannerChannel;
		ScanDirectionFlag = copy.ScanDirectionFlag;
		EdgeofFlightLine = copy.EdgeofFlightLine;
		Classification = copy.Classification;
		ScanAngleRank = copy.ScanAngleRank;
		UserData = copy.UserData;
		PointSourceID = copy.PointSourceID;
		memcpy(&GPSTime, &(copy.GPSTime), 8);
		Red = copy.Red;
		Green = copy.Green;
		Blue = copy.Blue;
	}

	void SetPointData(const PointFormat8 &copy)
	{
		X = copy.X;
		Y = copy.Y;
		Z = copy.Z;
		Intensity = copy.Intensity;
		ReturnNumber = copy.ReturnNumber;
		NumberOfReturns = copy.NumberOfReturns;
		ClassificationFlags = copy.ClassificationFlags;
		ScannerChannel = copy.ScannerChannel;
		ScanDirectionFlag = copy.ScanDirectionFlag;
		EdgeofFlightLine = copy.EdgeofFlightLine;
		Classification = copy.Classification;
		ScanAngleRank = copy.ScanAngleRank;
		UserData = copy.UserData;
		PointSourceID = copy.PointSourceID;
		memcpy(&GPSTime, &(copy.GPSTime), 8);
		Red = copy.Red;
		Green = copy.Green;
		Blue = copy.Blue;
		NIR = copy.NIR;
	}

	void GetPointData(PointFormat0 &retval)
	{
		retval.X						= X;
		retval.Y						= Y;
		retval.Z						= Z;
		retval.Intensity				= Intensity;
		retval.ReturnNumber				= ReturnNumber;
		retval.NumberOfReturns			= NumberOfReturns;
		retval.ScanDirectionFlag		= ScanDirectionFlag;
		retval.EdgeofFlightLine			= EdgeofFlightLine;
		retval.Classification			= Classification;
		retval.ScanAngleRank			= (char)ScanAngleRank;
		retval.UserData					= UserData;
		retval.PointSourceID			= PointSourceID;
	}

	void GetPointData(PointFormat1 &retval)
	{
		retval.X = X;
		retval.Y = Y;
		retval.Z = Z;
		retval.Intensity = Intensity;
		retval.ReturnNumber = ReturnNumber;
		retval.NumberOfReturns = NumberOfReturns;
		retval.ScanDirectionFlag = ScanDirectionFlag;
		retval.EdgeofFlightLine = EdgeofFlightLine;
		retval.Classification = Classification;
		retval.ScanAngleRank = (char)ScanAngleRank;
		retval.UserData = UserData;
		retval.PointSourceID = PointSourceID;
		memcpy(&(retval.GPSTime), &GPSTime, 8);
	}

	void GetPointData(PointFormat2 &retval)
	{
		retval.X					= X;
		retval.Y					= Y;
		retval.Z					= Z;
		retval.Intensity			= Intensity;
		retval.ReturnNumber			= ReturnNumber;
		retval.NumberOfReturns		= NumberOfReturns;
		retval.ScanDirectionFlag	= ScanDirectionFlag;
		retval.EdgeofFlightLine		= EdgeofFlightLine;
		retval.Classification		= Classification;
		retval.ScanAngleRank		= (char)ScanAngleRank;
		retval.UserData				= UserData;
		retval.PointSourceID		= PointSourceID;
		retval.Red					= Red;
		retval.Green				= Green;
		retval.Blue					= Blue;
	}

	void GetPointData(PointFormat3 &retval)
	{
		retval.X = X;
		retval.Y = Y;
		retval.Z = Z;
		retval.Intensity = Intensity;
		retval.ReturnNumber = ReturnNumber;
		retval.NumberOfReturns = NumberOfReturns;
		retval.ScanDirectionFlag = ScanDirectionFlag;
		retval.EdgeofFlightLine = EdgeofFlightLine;
		retval.Classification = Classification;
		retval.ScanAngleRank = (char)ScanAngleRank;
		retval.UserData = UserData;
		retval.PointSourceID = PointSourceID;
		memcpy(&(retval.GPSTime), &GPSTime, 8);
		retval.Red = Red;
		retval.Green = Green;
		retval.Blue = Blue;
	}

	void GetPointData(PointFormat7 &retval)
	{
		retval.X = X;
		retval.Y = Y;
		retval.Z = Z;
		retval.Intensity = Intensity;
		retval.ReturnNumber = ReturnNumber;
		retval.NumberOfReturns = NumberOfReturns;
		retval.ClassificationFlags = ClassificationFlags;
		retval.ScannerChannel = ScannerChannel;
		retval.ScanDirectionFlag = ScanDirectionFlag;
		retval.EdgeofFlightLine = EdgeofFlightLine;
		retval.Classification = Classification;
		retval.ScanAngleRank = ScanAngleRank;
		retval.UserData = UserData;
		retval.PointSourceID = PointSourceID;
		memcpy(&(retval.GPSTime), &GPSTime, 8);
		retval.Red = Red;
		retval.Green = Green;
		retval.Blue = Blue;
	}

	void GetPointData(PointFormat8 &retval)
	{
		retval.X = X;
		retval.Y = Y;
		retval.Z = Z;
		retval.Intensity = Intensity;
		retval.ReturnNumber = ReturnNumber;
		retval.NumberOfReturns = NumberOfReturns;
		retval.ClassificationFlags = ClassificationFlags;
		retval.ScannerChannel = ScannerChannel;
		retval.ScanDirectionFlag = ScanDirectionFlag;
		retval.EdgeofFlightLine = EdgeofFlightLine;
		retval.Classification = Classification;
		retval.ScanAngleRank = ScanAngleRank;
		retval.UserData = UserData;
		retval.PointSourceID = PointSourceID;
		memcpy(&(retval.GPSTime), &GPSTime, 8);
		retval.Red = Red;
		retval.Green = Green;
		retval.Blue = Blue;
		retval.NIR = NIR;
	}
};

class CSMLASProvider
{
public:
	CFile				m_LASFile;
	bool				m_bOpen;

	unsigned char		m_VerMajor;
	unsigned char		m_VerMinor;
	LAS_VERSION			m_Version;
	unsigned char		m_FormatType;

	unsigned long		m_HeaderSize;
	unsigned long		m_OffsetToPointData;
	unsigned long		*m_DataSizeAfterVLR;

	LASHeader_10		m_Header_10;
	LASHeader_11		m_Header_11;
	LASHeader_12		m_Header_12;
	LASHeader_14		m_Header_14;

	unsigned long		m_NumVLR;
	VLR					*m_VLR;

	unsigned long		m_NumPoints;

	PointFormat0		*m_Points_0;
	PointFormat1		*m_Points_1;
	PointFormat2		*m_Points_2;
	PointFormat3		*m_Points_3;
	PointFormat7		*m_Points_7;
	PointFormat8		*m_Points_8;

	double				ScaleX;
	double				ScaleY;
	double				ScaleZ;
	double				OffsetX;
	double				OffsetY;
	double				OffsetZ;

	double				MinX;
	double				MinY;
	double				MinZ;

	double				MaxX;
	double				MaxY;
	double				MaxZ;

	BYTE				**m_AfterVLR;//Data size after Variable Length Records

	unsigned short		PointDataStartSignature; //For LAS Ver 1.0

	unsigned long		begin_idx;
	unsigned long		end_idx;

public:

	CSMLASProvider(void)
	{
		m_Points_0					= NULL;
		m_Points_1					= NULL;
		m_Points_2					= NULL;
		m_Points_3					= NULL;
		m_Points_7					= NULL;
		m_Points_8					= NULL;
		m_AfterVLR					= NULL;
		m_DataSizeAfterVLR			= NULL;
		m_VLR						= NULL;
		m_AfterVLR					= NULL;

		m_bOpen						= false;

		PointDataStartSignature		=0xCCDD; //For LAS Ver 1.0

		begin_idx					=0;
		end_idx						=0;
	}

	~CSMLASProvider(void)
	{
		DeletePointData();

		DeleteVLR();

		Close();
	}

	bool ChkIdx(unsigned long global_index)
	{
		if(global_index >= begin_idx && global_index < end_idx)
			return true;
		else
			return false;
	}

	ErrorMsg CreateNewLAS(LASHeader_10 &header, VLR11 *pVLR=NULL, BYTE **pAfterVLR=NULL)
	{
		ErrorMsg retval = ERROR_FREE;

		if(pVLR==NULL||pAfterVLR==NULL)
			header.NumberOfVariableLengthRecords = 0;

		m_Header_10 = header;

		SetConfigFromHeader(m_Header_10);

		SetVLR(this->m_NumVLR, pVLR, pAfterVLR);

		retval = SetDummyPoints(this->m_NumPoints, this->m_Version, this->m_FormatType);

		if(retval != ERROR_FREE)
			return retval;

		return retval;
	}

	ErrorMsg CreateNewLAS(LASHeader_11 &header, VLR *pVLR = NULL, BYTE **pAfterVLR = NULL)
	{
		ErrorMsg retval = ERROR_FREE;

		if (pVLR == NULL || pAfterVLR == NULL)
			header.NumberOfVariableLengthRecords = 0;

		m_Header_11 = header;

		SetConfigFromHeader(m_Header_11);

		SetVLR(this->m_NumVLR, pVLR, pAfterVLR);

		retval = SetDummyPoints(this->m_NumPoints, this->m_Version, this->m_FormatType);

		if (retval != ERROR_FREE)
			return retval;

		return retval;
	}

	ErrorMsg CreateNewLAS(LASHeader_12 &header, VLR *pVLR=NULL, BYTE **pAfterVLR=NULL)
	{
		ErrorMsg retval = ERROR_FREE;

		if(pVLR==NULL||pAfterVLR==NULL)
			header.NumberOfVariableLengthRecords = 0;

		m_Header_12 = header;

		SetConfigFromHeader(m_Header_12);

		SetVLR(this->m_NumVLR, pVLR, pAfterVLR);

		retval = SetDummyPoints(this->m_NumPoints, this->m_Version, this->m_FormatType);

		if(retval != ERROR_FREE)
			return retval;

		return retval;
	}

	ErrorMsg CreateNewLAS(LASHeader_14 &header, VLR *pVLR = NULL, BYTE **pAfterVLR = NULL)
	{
		ErrorMsg retval = ERROR_FREE;

		if (pVLR == NULL || pAfterVLR == NULL)
			header.NumberOfVariableLengthRecords = 0;

		m_Header_14 = header;

		SetConfigFromHeader(m_Header_14);

		SetVLR(this->m_NumVLR, pVLR, pAfterVLR);

		retval = SetDummyPoints(this->m_NumPoints, this->m_Version, this->m_FormatType);

		if (retval != ERROR_FREE)
			return retval;

		return retval;
	}

	void DeletePointData()
	{
		if(m_Points_0 != NULL)
		{
			delete[] m_Points_0;
			m_Points_0 = NULL;
		}

		if(m_Points_1 != NULL)
		{
			delete[] m_Points_1;
			m_Points_1 = NULL;
		}

		if(m_Points_2 != NULL)
		{
			delete[] m_Points_2;
			m_Points_2 = NULL;
		}

		if(m_Points_3 != NULL)
		{
			delete[] m_Points_3;
			m_Points_3 = NULL;
		}

		if (m_Points_7 != NULL)
		{
			delete[] m_Points_7;
			m_Points_7 = NULL;
		}

		if (m_Points_8 != NULL)
		{
			delete[] m_Points_8;
			m_Points_8 = NULL;
		}
	}

	void DeleteVLR()
	{
		if(m_VLR != NULL)
		{
			delete[] m_VLR;
			m_VLR = NULL;
		}

		if(m_DataSizeAfterVLR != NULL)
		{
			delete[] m_DataSizeAfterVLR;
			m_DataSizeAfterVLR = NULL;
		}

		if(this->m_AfterVLR != NULL)
		{
			for(unsigned long i=0; i<m_NumVLR; i++)
			{
				delete[] m_AfterVLR[i];
			}

			delete[] m_AfterVLR;

			m_AfterVLR = NULL;
		}
	}

	void Close()
	{
		if(m_bOpen == true)
		{
			m_LASFile.Close();

			m_bOpen = false;
		}
	}

	ErrorMsg Open(const char* inFilePath, UINT nOpenFlags = CFile::modeRead|CFile::shareDenyRead)
	{
		if (!m_LASFile.Open((LPCTSTR)inFilePath, nOpenFlags))
		{
			//AfxMessageBox("Probably, it's already open in other s/w.");
			return LASFILE_OPEN_ERROR;
		}

		m_bOpen = true;

		ErrorMsg emsg;
		//
		//Check version of LAS format
		//
		emsg = CheckVersion();

		if(ERROR_FREE != emsg)
			return emsg;

		//
		//Move to file begin
		//
		m_LASFile.SeekToBegin();

		//
		//Read header data
		//
		switch(m_Version)
		{
		case V10:
			emsg = m_Header_10.ReadHeader(m_LASFile);
			if(emsg != ERROR_FREE) return emsg;
			SetConfigFromHeader(m_Header_10);
			break;
		case V12:
			emsg = m_Header_12.ReadHeader(m_LASFile);
			if(emsg != ERROR_FREE) return emsg;
			SetConfigFromHeader(m_Header_12);
			break;
		case V14:
			emsg = m_Header_14.ReadHeader(m_LASFile);
			if (emsg != ERROR_FREE) return emsg;
			SetConfigFromHeader(m_Header_14);
			break;
		default:
			return NOT_SUPPORTED_VERSION;
			break;
		}

		if(ScaleX == 0.||ScaleY == 0.||ScaleZ == 0.)
			return SCALEFACTOR_ERROR;

		//
		//Read Variable Length Records
		//

		if(m_NumVLR > 0)
		{
			unsigned long m_VLRSize = sizeof(VLR);

			m_VLR = new VLR[m_NumVLR];
			m_DataSizeAfterVLR = new unsigned long[m_NumVLR];
			m_AfterVLR = new BYTE*[m_NumVLR];

			for(unsigned long i=0; i<m_NumVLR; i++)
			{
				m_VLR[i].ReadVLR(m_LASFile);
				m_DataSizeAfterVLR[i] = m_VLR[i].RecordLengthAfterHeader;

				m_AfterVLR[i] = new BYTE[m_DataSizeAfterVLR[i]];
				m_LASFile.Read(m_AfterVLR[i], m_DataSizeAfterVLR[i]);
			}
		}

		if(m_Version == V10)
		{
			if(!m_LASFile.Read(&PointDataStartSignature, 2)) 
				return POINT_START_SIGN_READ_ERROR;
		}

		return ERROR_FREE;
	}

	LASHeader_10 GetHeader_V10() { return m_Header_10; }

	LASHeader_12 GetHeader_V12() { return m_Header_12; }

	LASHeader_14 GetHeader_V14() { return m_Header_14; }

	ErrorMsg CheckVersion()
	{
		m_LASFile.SeekToBegin();

		char temp[26];

		if(!m_LASFile.Read(temp, 26))
			return VERSION_READ_ERROR;

		m_VerMajor = temp[24];
		m_VerMinor = temp[25];

		if(m_VerMajor == 1)
		{
			if(m_VerMinor == 0)
			{
				m_Version = V10;
			}
			else if(m_VerMinor == 2)
			{
				m_Version = V12;
			}
			else if (m_VerMinor == 4)
			{
				m_Version = V14;
			}
		}

		return ERROR_FREE;
	}

	ErrorMsg ReadAllPoints()
	{
		DeletePointData();

		begin_idx = 0;
		end_idx = m_NumPoints;

		m_LASFile.Seek(m_OffsetToPointData, CFile::begin);

		switch (m_FormatType)
		{
		case 0:
			m_Points_0 = new PointFormat0[m_NumPoints];
			if (!m_LASFile.Read(m_Points_0, sizeof(PointFormat0)*m_NumPoints)) return READ_POINTDATA_ERROR;
			break;
		case 1:
			m_Points_1 = new PointFormat1[m_NumPoints];
			if (!m_LASFile.Read(m_Points_1, sizeof(PointFormat1)*m_NumPoints)) return READ_POINTDATA_ERROR;
			break;
		case 2:
			m_Points_2 = new PointFormat2[m_NumPoints];
			if (!m_LASFile.Read(m_Points_2, sizeof(PointFormat2)*m_NumPoints)) return READ_POINTDATA_ERROR;
			break;
		case 3:
			m_Points_3 = new PointFormat3[m_NumPoints];
			if (!m_LASFile.Read(m_Points_3, sizeof(PointFormat3)*m_NumPoints)) return READ_POINTDATA_ERROR;
			break;
		case 7:
			m_Points_7 = new PointFormat7[m_NumPoints];
			if (!m_LASFile.Read(m_Points_7, sizeof(PointFormat7)*m_NumPoints)) return READ_POINTDATA_ERROR;
			break;
		case 8:
			m_Points_8 = new PointFormat8[m_NumPoints];
			if (!m_LASFile.Read(m_Points_8, sizeof(PointFormat8)*m_NumPoints)) return READ_POINTDATA_ERROR;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg ReadPoints(const unsigned long offset_idx, unsigned long max_num_pts, unsigned long &num_read_pts)
	{
		DeletePointData();

		if(offset_idx >= m_NumPoints)
		{
			num_read_pts = 0;
			return OUT_OF_POINT_NUMBER;
		}
		else if( (offset_idx + max_num_pts) >=  m_NumPoints)
		{
			num_read_pts = m_NumPoints - offset_idx;
		}
		else
		{
			num_read_pts = max_num_pts;
		}		

		switch (m_FormatType)
		{
		case 0:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat0)*offset_idx), CFile::begin);
			m_Points_0 = new PointFormat0[num_read_pts];
			if (!m_LASFile.Read(m_Points_0, sizeof(PointFormat0)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		case 1:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat1)*offset_idx), CFile::begin);
			m_Points_1 = new PointFormat1[num_read_pts];
			if (!m_LASFile.Read(m_Points_1, sizeof(PointFormat1)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		case 2:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat2)*offset_idx), CFile::begin);
			m_Points_2 = new PointFormat2[num_read_pts];
			if (!m_LASFile.Read(m_Points_2, sizeof(PointFormat2)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		case 3:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat3)*offset_idx), CFile::begin);
			m_Points_3 = new PointFormat3[num_read_pts];
			if (!m_LASFile.Read(m_Points_3, sizeof(PointFormat3)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		case 7:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat7)*offset_idx), CFile::begin);
			m_Points_7 = new PointFormat7[num_read_pts];
			if (!m_LASFile.Read(m_Points_7, sizeof(PointFormat7)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		case 8:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat8)*offset_idx), CFile::begin);
			m_Points_8 = new PointFormat8[num_read_pts];
			if (!m_LASFile.Read(m_Points_8, sizeof(PointFormat8)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		begin_idx = offset_idx;
		end_idx = begin_idx + num_read_pts;

		return ERROR_FREE;
	}

	ErrorMsg GetBuff(const unsigned long num_skip_pts, unsigned long max_num_pts, unsigned long &num_read_pts, void* point_buffer)
	{
		if(num_skip_pts >= m_NumPoints)
		{
			num_read_pts = 0;
			return OUT_OF_POINT_NUMBER;
		}
		else if( (num_skip_pts + max_num_pts) >=  m_NumPoints)
		{
			num_read_pts = m_NumPoints - num_skip_pts;
		}
		else
		{
			num_read_pts = max_num_pts;
		}

		switch (m_FormatType)
		{
		case 0:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat0)*num_skip_pts), CFile::begin);
			if (!m_LASFile.Read(point_buffer, sizeof(PointFormat0)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		case 1:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat1)*num_skip_pts), CFile::begin);
			if (!m_LASFile.Read(point_buffer, sizeof(PointFormat1)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		case 2:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat2)*num_skip_pts), CFile::begin);
			if (!m_LASFile.Read(point_buffer, sizeof(PointFormat2)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		case 3:
			m_LASFile.Seek(m_OffsetToPointData + (sizeof(PointFormat3)*num_skip_pts), CFile::begin);
			if (!m_LASFile.Read(point_buffer, sizeof(PointFormat3)*num_read_pts)) return READ_POINTDATA_ERROR;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	unsigned int getPtSize()
	{
		switch (m_FormatType)
		{
		case 0:
			return sizeof(PointFormat0);
		case 1:
			return sizeof(PointFormat1);
		case 2:
			return sizeof(PointFormat2);
		case 3:
			return sizeof(PointFormat3);
		default:
			return 0;
		}

		return 0;
	}

	ErrorMsg GetXYZ(const unsigned long global_index, double *X, double *Y, double *Z)
	{
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		double X0, Y0, Z0;

		switch (m_FormatType)
		{
		case 0:
			X0 = m_Points_0[i].X;
			Y0 = m_Points_0[i].Y;
			Z0 = m_Points_0[i].Z;
			break;
		case 1:
			X0 = m_Points_1[i].X;
			Y0 = m_Points_1[i].Y;
			Z0 = m_Points_1[i].Z;
			break;
		case 2:
			X0 = m_Points_2[i].X;
			Y0 = m_Points_2[i].Y;
			Z0 = m_Points_2[i].Z;
			break;
		case 3:
			X0 = m_Points_3[i].X;
			Y0 = m_Points_3[i].Y;
			Z0 = m_Points_3[i].Z;
			break;
		case 7:
			X0 = m_Points_7[i].X;
			Y0 = m_Points_7[i].Y;
			Z0 = m_Points_7[i].Z;
			break;
		case 8:
			X0 = m_Points_8[i].X;
			Y0 = m_Points_8[i].Y;
			Z0 = m_Points_8[i].Z;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		*X = X0*ScaleX + OffsetX;
		*Y = Y0*ScaleY + OffsetY;
		*Z = Z0*ScaleZ + OffsetZ;

		return ERROR_FREE;
	}

	ErrorMsg GetClassification(const unsigned long global_index, unsigned char& classification)
	{
		if (!ChkIdx(global_index))
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index - begin_idx;

		switch (m_FormatType)
		{
		case 0:
			classification = m_Points_0[i].Classification;
			break;
		case 1:
			classification = m_Points_1[i].Classification;
			break;
		case 2:
			classification = m_Points_2[i].Classification;
			break;
		case 3:
			classification = m_Points_3[i].Classification;
			break;
		case 7:
			classification = m_Points_7[i].Classification;
			break;
		case 8:
			classification = m_Points_8[i].Classification;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	unsigned char GetClassification(const unsigned long global_index)
	{
		unsigned char classification;
		if (ERROR_FREE == GetClassification(global_index, classification))
			return classification;
		else
			return 0;
	}

	ErrorMsg SetClassification(const unsigned long global_index, const unsigned char classification)
	{
		if (!ChkIdx(global_index))
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index - begin_idx;

		switch (m_FormatType)
		{
		case 0:
			m_Points_0[i].Classification = classification;
			break;
		case 1:
			m_Points_1[i].Classification = classification;
			break;
		case 2:
			m_Points_2[i].Classification = classification;
			break;
		case 3:
			m_Points_3[i].Classification = classification;
			break;
		case 7:
			m_Points_7[i].Classification = classification;
			break;
		case 8:
			m_Points_8[i].Classification = classification;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetXYZI(const unsigned long global_index, double *X, double *Y, double *Z, unsigned short *I)
	{
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		double X0, Y0, Z0;

		switch (m_FormatType)
		{
		case 0:
			X0 = m_Points_0[i].X;
			Y0 = m_Points_0[i].Y;
			Z0 = m_Points_0[i].Z;
			*I = m_Points_0[i].Intensity;
			break;
		case 1:
			X0 = m_Points_1[i].X;
			Y0 = m_Points_1[i].Y;
			Z0 = m_Points_1[i].Z;
			*I = m_Points_1[i].Intensity;
			break;
		case 2:
			X0 = m_Points_2[i].X;
			Y0 = m_Points_2[i].Y;
			Z0 = m_Points_2[i].Z;
			*I = m_Points_2[i].Intensity;
			break;
		case 3:
			X0 = m_Points_3[i].X;
			Y0 = m_Points_3[i].Y;
			Z0 = m_Points_3[i].Z;
			*I = m_Points_3[i].Intensity;
			break;
		case 7:
			X0 = m_Points_7[i].X;
			Y0 = m_Points_7[i].Y;
			Z0 = m_Points_7[i].Z;
			*I = m_Points_7[i].Intensity;
			break;
		case 8:
			X0 = m_Points_8[i].X;
			Y0 = m_Points_8[i].Y;
			Z0 = m_Points_8[i].Z;
			*I = m_Points_8[i].Intensity;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		*X = X0*ScaleX + OffsetX;
		*Y = Y0*ScaleY + OffsetY;
		*Z = Z0*ScaleZ + OffsetZ;

		return ERROR_FREE;
	}

	ErrorMsg SetXYZ(const unsigned long global_index, double X, double Y, double Z)
	{
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		double X0, Y0, Z0;

		X0 = (X - OffsetX) / ScaleX;
		Y0 = (Y - OffsetY) / ScaleY;
		Z0 = (Z - OffsetZ) / ScaleZ;

		switch (m_FormatType)
		{
		case 0:
			m_Points_0[i].X = (long)X0;
			m_Points_0[i].Y = (long)Y0;
			m_Points_0[i].Z = (long)Z0;
			break;
		case 1:
			m_Points_1[i].X = (long)X0;
			m_Points_1[i].Y = (long)Y0;
			m_Points_1[i].Z = (long)Z0;
			break;
		case 2:
			m_Points_2[i].X = (long)X0;
			m_Points_2[i].Y = (long)Y0;
			m_Points_2[i].Z = (long)Z0;
			break;
		case 3:
			m_Points_3[i].X = (long)X0;
			m_Points_3[i].Y = (long)Y0;
			m_Points_3[i].Z = (long)Z0;
			break;
		case 7:
			m_Points_7[i].X = (long)X0;
			m_Points_7[i].Y = (long)Y0;
			m_Points_7[i].Z = (long)Z0;
			break;
		case 8:
			m_Points_8[i].X = (long)X0;
			m_Points_8[i].Y = (long)Y0;
			m_Points_8[i].Z = (long)Z0;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetIntensity(const unsigned long global_index, unsigned short *Intensity)
	{
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		switch (m_FormatType)
		{
		case 0:
			*Intensity = m_Points_0[i].Intensity;
			break;
		case 1:
			*Intensity = m_Points_1[i].Intensity;
			break;
		case 2:
			*Intensity = m_Points_2[i].Intensity;
			break;
		case 3:
			*Intensity = m_Points_3[i].Intensity;
			break;
		case 7:
			*Intensity = m_Points_7[i].Intensity;
			break;
		case 8:
			*Intensity = m_Points_8[i].Intensity;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetIntensity(const unsigned long global_index, unsigned short Intensity)
	{
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		switch (m_FormatType)
		{
		case 0:
			m_Points_0[i].Intensity = Intensity;
			break;
		case 1:
			m_Points_1[i].Intensity = Intensity;
			break;
		case 2:
			m_Points_2[i].Intensity = Intensity;
			break;
		case 3:
			m_Points_3[i].Intensity = Intensity;
			break;
		case 7:
			m_Points_7[i].Intensity = Intensity;
			break;
		case 8:
			m_Points_8[i].Intensity = Intensity;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetRGB(const unsigned long global_index, unsigned short *R, unsigned short *G, unsigned short *B)
	{
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		switch (m_FormatType)
		{
		case 0:
			return NOT_SUPPORTED_FORMAT;
		case 1:
			return NOT_SUPPORTED_FORMAT;
		case 2:
			*R = m_Points_2[i].Red;
			*G = m_Points_2[i].Green;
			*B = m_Points_2[i].Blue;
			break;
		case 3:
			*R = m_Points_3[i].Red;
			*G = m_Points_3[i].Green;
			*B = m_Points_3[i].Blue;
			break;
		case 7:
			*R = m_Points_7[i].Red;
			*G = m_Points_7[i].Green;
			*B = m_Points_7[i].Blue;
			break;
		case 8:
			*R = m_Points_8[i].Red;
			*G = m_Points_8[i].Green;
			*B = m_Points_8[i].Blue;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetRGB(const unsigned long global_index, unsigned short R, unsigned short G, unsigned short B)
	{
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		switch (m_FormatType)
		{
		case 0:
			return NOT_SUPPORTED_FORMAT;
		case 1:
			return NOT_SUPPORTED_FORMAT;
		case 2:
			m_Points_2[i].Red = R;
			m_Points_2[i].Green = G;
			m_Points_2[i].Blue = B;
			break;
		case 3:
			m_Points_3[i].Red = R;
			m_Points_3[i].Green = G;
			m_Points_3[i].Blue = B;
			break;
		case 7:
			m_Points_7[i].Red = R;
			m_Points_7[i].Green = G;
			m_Points_7[i].Blue = B;
			break;
		case 8:
			m_Points_8[i].Red = R;
			m_Points_8[i].Green = G;
			m_Points_8[i].Blue = B;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetPoint(const unsigned long global_index, LASPoint_Common &point)//Copy FROM this TO point(common)
	{
		//This GetPoint function call "Set" function of LASPoint_Common to return LAS point value through "&point".
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		switch (m_FormatType)
		{
		case 0:
			point.SetPointData(m_Points_0[i]);
			break;
		case 1:
			point.SetPointData(m_Points_1[i]);
			break;
		case 2:
			point.SetPointData(m_Points_2[i]);
			break;
		case 3:
			point.SetPointData(m_Points_3[i]);
			break;
		case 7:
			point.SetPointData(m_Points_7[i]);
			break;
		case 8:
			point.SetPointData(m_Points_8[i]);
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetPoint(const unsigned long global_index, LASPoint_Common &point)//Copy FROM point(common) TO this
	{
		//This SetPoint function call "Get" function of LASPoint_Common to copy "&point" value to LAS point.

		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		switch (m_FormatType)
		{
		case 0:
			point.GetPointData(m_Points_0[i]);
			break;
		case 1:
			point.GetPointData(m_Points_1[i]);
			break;
		case 2:
			point.GetPointData(m_Points_2[i]);
			break;
		case 3:
			point.GetPointData(m_Points_3[i]);
			break;
		case 7:
			point.GetPointData(m_Points_7[i]);
			break;
		case 8:
			point.GetPointData(m_Points_8[i]);
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg GetReturnNumber(const unsigned long global_index, unsigned char *ReturnNumber)
	{
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		switch (m_FormatType)
		{
		case 0:
			*ReturnNumber = m_Points_0[i].ReturnNumber;
			break;
		case 1:
			*ReturnNumber = m_Points_1[i].ReturnNumber;
			break;
		case 2:
			*ReturnNumber = m_Points_2[i].ReturnNumber;
			break;
		case 3:
			*ReturnNumber = m_Points_3[i].ReturnNumber;
			break;
		case 7:
			*ReturnNumber = m_Points_7[i].ReturnNumber;
			break;
		case 8:
			*ReturnNumber = m_Points_8[i].ReturnNumber;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	ErrorMsg SetReturnNumber(const unsigned long global_index, unsigned char ReturnNumber)
	{
		if( !ChkIdx(global_index) )
			return OUT_OF_POINT_NUMBER;

		unsigned long i = global_index- begin_idx;

		switch (m_FormatType)
		{
		case 0:
			m_Points_0[i].ReturnNumber = ReturnNumber;
			break;
		case 1:
			m_Points_1[i].ReturnNumber = ReturnNumber;
			break;
		case 2:
			m_Points_2[i].ReturnNumber = ReturnNumber;
			break;
		case 3:
			m_Points_3[i].ReturnNumber = ReturnNumber;
			break;
		case 7:
			m_Points_7[i].ReturnNumber = ReturnNumber;
			break;
		case 8:
			m_Points_8[i].ReturnNumber = ReturnNumber;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	/////////////////////////////////////////////////////////////
	//
	//For creating a new las from an old one
	//
	/////////////////////////////////////////////////////////////

	//
	//Header configuration: LAS 1.2 & type 1
	//
	static void HDRSetting_LAS_12_type_1(LASHeader_12 &header_12, unsigned long numPts, const char* SystemIdentifier, const char* GeneratingSoftware, double MinX, double MinY, double MinZ, double MaxX, double MaxY, double MaxZ, double Scale, double OffsetX, double OffsetY, double OffsetZ)
	{
		unsigned long NumberOfPointsbyReturn[5];
		NumberOfPointsbyReturn[0]		= numPts;
		NumberOfPointsbyReturn[1]		= 0;
		NumberOfPointsbyReturn[2]		= 0;
		NumberOfPointsbyReturn[3]		= 0;
		NumberOfPointsbyReturn[4]		= 0;
		
		HDRSetting_LAS_12(header_12, 1, sizeof(PointFormat1), numPts, NumberOfPointsbyReturn, SystemIdentifier, GeneratingSoftware, MinX, MinY, MinZ, MaxX, MaxY, MaxZ, Scale, Scale, Scale, OffsetX, OffsetY, OffsetZ);
		//HDRSetting_LAS_12_type_1(header_12,                   numPts,                         SystemIdentifier, GeneratingSoftware, MinX, MinY, MinZ, MaxX, MaxY, MaxZ, Scale, Scale, Scale, OffsetX, OffsetY, OffsetZ);
	}

	static void HDRSetting_LAS_12_type_1(LASHeader_12 &header_12, unsigned long numPts, const char* SystemIdentifier, const char* GeneratingSoftware, double MinX, double MinY, double MinZ, double MaxX, double MaxY, double MaxZ, double ScaleX, double ScaleY, double ScaleZ, double OffsetX, double OffsetY, double OffsetZ)
	{
		time_t t = time(0);   // get time now
		struct tm now;
		localtime_s(&now, &t);

		//X = X0*m_Header_10.ScaleFactor_X + m_Header_10.Offset_X;

		unsigned short dayofyear;
		if(false == GetDayofYear(dayofyear))
			dayofyear = 1;

		header_12.FileSignature[0]				= 'L';
		header_12.FileSignature[1]				= 'A';
		header_12.FileSignature[2]				= 'S';
		header_12.FileSignature[3]				= 'F';
		header_12.FileSourceID					= 0; //Flight line number; "0" means not-assigned.
		header_12.GlobalEncoding				= 0;
		header_12.GUIDdata_1					= 0;
		header_12.GUIDdata_2					= 0;
		header_12.GUIDdata_3					= 0;
		header_12.GUIDdata_4[8]					= 0;
		header_12.VersionMajor					= unsigned char (1);
		header_12.VersionMinor					= unsigned char (2);

		//memcpy(header_12.SystemIdentifier, "6T3 Ltd. MobileLiDAR", sizeof("6T3 Ltd. MobileLiDAR"));
		memcpy(header_12.SystemIdentifier, SystemIdentifier, sizeof(SystemIdentifier));
		//memcpy(header_12.GeneratingSoftware, "6T3 Ltd. MobileLiDAR Modeler", sizeof("6T3 Ltd. MobileLiDAR Modeler"));
		memcpy(header_12.GeneratingSoftware, GeneratingSoftware, sizeof(GeneratingSoftware));

		header_12.FileCreationDayOfYear			= dayofyear;
		header_12.FileCreationYear				= ((unsigned short)now.tm_year + 1900);

		header_12.HeaderSize					= sizeof(header_12);
		header_12.OffsettoData					=  sizeof(header_12);
		header_12.NumberOfVariableLengthRecords	= 0;

		header_12.PointDataFormatID				= 1;//Version 1.2 Format Type 1
		header_12.PointDataRecordLength			= sizeof(PointFormat1);

		header_12.NumberOfPointRecords			= numPts;

		header_12.NumberOfPointsbyReturn[0]		= numPts;
		header_12.NumberOfPointsbyReturn[1]		= 0;
		header_12.NumberOfPointsbyReturn[2]		= 0;
		header_12.NumberOfPointsbyReturn[3]		= 0;
		header_12.NumberOfPointsbyReturn[4]		= 0;

		header_12.ScaleFactor_X					= ScaleX;
		header_12.ScaleFactor_Y					= ScaleY;
		header_12.ScaleFactor_Z					= ScaleZ;
		header_12.Offset_X						= OffsetX;
		header_12.Offset_Y						= OffsetY;
		header_12.Offset_Z						= OffsetZ;
		header_12.Max_X							= MaxX;
		header_12.Min_X							= MinX;
		header_12.Max_Y							= MaxY;
		header_12.Min_Y							= MinY;
		header_12.Max_Z							= MaxZ;
		header_12.Min_Z							= MinZ;
	}

	static void HDRSetting_LAS_10(LASHeader_10 &header_10, unsigned char pt_type, unsigned short pt_data_size, unsigned long numPts, unsigned long NumberOfPointsbyReturn[5], const char* SystemIdentifier, const char* GeneratingSoftware, double MinX, double MinY, double MinZ, double MaxX, double MaxY, double MaxZ, double ScaleX, double ScaleY, double ScaleZ, double OffsetX, double OffsetY, double OffsetZ)
	{
		time_t t = time(0);   // get time now
		struct tm now;
		localtime_s(&now, &t);

		//X = X0*m_Header_10.ScaleFactor_X + m_Header_10.Offset_X;

		unsigned short dayofyear;
		if(false == GetDayofYear(dayofyear))
			dayofyear = 1;

		header_10.FileSignature[0]				= 'L';
		header_10.FileSignature[1]				= 'A';
		header_10.FileSignature[2]				= 'S';
		header_10.FileSignature[3]				= 'F';
		header_10.Reserved						= 0;
		header_10.GUIDdata_1					= 0;
		header_10.GUIDdata_2					= 0;
		header_10.GUIDdata_3					= 0;
		header_10.GUIDdata_4[0]					= 0;
		header_10.GUIDdata_4[1]					= 0;
		header_10.GUIDdata_4[2]					= 0;
		header_10.GUIDdata_4[3]					= 0;
		header_10.GUIDdata_4[4]					= 0;
		header_10.GUIDdata_4[5]					= 0;
		header_10.GUIDdata_4[6]					= 0;
		header_10.GUIDdata_4[7]					= 0;
		header_10.VersionMajor					= unsigned char (1);
		header_10.VersionMinor					= unsigned char (0);

		memcpy(header_10.SystemIdentifier, SystemIdentifier, sizeof(SystemIdentifier));
		memcpy(header_10.GeneratingSoftware, GeneratingSoftware, sizeof(GeneratingSoftware));

		header_10.FlightDateJulian				= dayofyear;
		header_10.Year							= ((unsigned short)now.tm_year + 1900);

		header_10.HeaderSize					= sizeof(header_10);
		header_10.OffsettoData					= sizeof(header_10);
		header_10.NumberOfVariableLengthRecords	= 0;

		header_10.PointDataFormatID				= pt_type;
		header_10.PointDataRecordLength			= pt_data_size;

		header_10.NumberOfPointRecords			= numPts;

		header_10.NumberOfPointsbyReturn[0]		= NumberOfPointsbyReturn[0];
		header_10.NumberOfPointsbyReturn[1]		= NumberOfPointsbyReturn[1];
		header_10.NumberOfPointsbyReturn[2]		= NumberOfPointsbyReturn[2];
		header_10.NumberOfPointsbyReturn[3]		= NumberOfPointsbyReturn[3];
		header_10.NumberOfPointsbyReturn[4]		= NumberOfPointsbyReturn[4];

		header_10.ScaleFactor_X					= ScaleX;
		header_10.ScaleFactor_Y					= ScaleY;
		header_10.ScaleFactor_Z					= ScaleZ;
		header_10.Offset_X						= OffsetX;
		header_10.Offset_Y						= OffsetY;
		header_10.Offset_Z						= OffsetZ;
		header_10.Max_X							= MaxX;
		header_10.Min_X							= MinX;
		header_10.Max_Y							= MaxY;
		header_10.Min_Y							= MinY;
		header_10.Max_Z							= MaxZ;
		header_10.Min_Z							= MinZ;		
	}

	static void HDRSetting_LAS_12(LASHeader_12 &header_12, unsigned char pt_type, unsigned short pt_data_size, unsigned long numPts, unsigned long NumberOfPointsbyReturn[5], const char* SystemIdentifier, const char* GeneratingSoftware, double MinX, double MinY, double MinZ, double MaxX, double MaxY, double MaxZ, double ScaleX, double ScaleY, double ScaleZ, double OffsetX, double OffsetY, double OffsetZ)
	{
		time_t t = time(0);   // get time now
		struct tm now;
		localtime_s(&now, &t);

		//X = X0*m_Header_10.ScaleFactor_X + m_Header_10.Offset_X;

		unsigned short dayofyear;
		if(false == GetDayofYear(dayofyear))
			dayofyear = 1;

		header_12.FileSignature[0]				= 'L';
		header_12.FileSignature[1]				= 'A';
		header_12.FileSignature[2]				= 'S';
		header_12.FileSignature[3]				= 'F';
		header_12.FileSourceID					= 0; //Flight line number; "0" means not-assigned.
		header_12.GlobalEncoding				= 0;
		header_12.GUIDdata_1					= 0;
		header_12.GUIDdata_2					= 0;
		header_12.GUIDdata_3					= 0;
		header_12.GUIDdata_4[0]					= 0;
		header_12.GUIDdata_4[1]					= 0;
		header_12.GUIDdata_4[2]					= 0;
		header_12.GUIDdata_4[3]					= 0;
		header_12.GUIDdata_4[4]					= 0;
		header_12.GUIDdata_4[5]					= 0;
		header_12.GUIDdata_4[6]					= 0;
		header_12.GUIDdata_4[7]					= 0;
		header_12.VersionMajor					= unsigned char (1);
		header_12.VersionMinor					= unsigned char (2);

		memcpy(header_12.SystemIdentifier, SystemIdentifier, sizeof(SystemIdentifier));
		memcpy(header_12.GeneratingSoftware, GeneratingSoftware, sizeof(GeneratingSoftware));

		header_12.FileCreationDayOfYear			= dayofyear;
		header_12.FileCreationYear				= ((unsigned short)now.tm_year + 1900);

		header_12.HeaderSize					= sizeof(header_12);
		header_12.OffsettoData					=  sizeof(header_12);
		header_12.NumberOfVariableLengthRecords	= 0;

		header_12.PointDataFormatID				= pt_type;
		header_12.PointDataRecordLength			= pt_data_size;

		header_12.NumberOfPointRecords			= numPts;

		header_12.NumberOfPointsbyReturn[0]		= NumberOfPointsbyReturn[0];
		header_12.NumberOfPointsbyReturn[1]		= NumberOfPointsbyReturn[1];
		header_12.NumberOfPointsbyReturn[2]		= NumberOfPointsbyReturn[2];
		header_12.NumberOfPointsbyReturn[3]		= NumberOfPointsbyReturn[3];
		header_12.NumberOfPointsbyReturn[4]		= NumberOfPointsbyReturn[4];

		header_12.ScaleFactor_X					= ScaleX;
		header_12.ScaleFactor_Y					= ScaleY;
		header_12.ScaleFactor_Z					= ScaleZ;
		header_12.Offset_X						= OffsetX;
		header_12.Offset_Y						= OffsetY;
		header_12.Offset_Z						= OffsetZ;
		header_12.Max_X							= MaxX;
		header_12.Min_X							= MinX;
		header_12.Max_Y							= MaxY;
		header_12.Min_Y							= MinY;
		header_12.Max_Z							= MaxZ;
		header_12.Min_Z							= MinZ;
	}

	//
	//Copy header & VLR
	//
	ErrorMsg CopyHeaderAndVLR(CSMLASProvider &copy)
	{
		//
		//Copy header
		//
		m_Version = copy.m_Version;

		switch(m_Version)
		{
		case V10:
			m_Header_10 = copy.m_Header_10;
			SetConfigFromHeader(m_Header_10);
			break;
		case V12:
			m_Header_12 = copy.m_Header_12;
			SetConfigFromHeader(m_Header_12);
			break;
		case V14:
			m_Header_14 = copy.m_Header_14;
			SetConfigFromHeader(m_Header_14);
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		//
		//Copy Variable Length Records
		//
		DeleteVLR();

		m_NumVLR = copy.m_NumVLR;

		if(m_NumVLR > 0)
		{
			unsigned long m_VLRSize = sizeof(VLR);

			m_VLR = new VLR[m_NumVLR];
			m_DataSizeAfterVLR = new unsigned long[m_NumVLR];
			m_AfterVLR = new BYTE*[m_NumVLR];

			for(unsigned long i=0; i<m_NumVLR; i++)
			{
				m_VLR[i] = copy.m_VLR[i];
				m_DataSizeAfterVLR[i] = m_VLR[i].RecordLengthAfterHeader;

				m_AfterVLR[i] = new BYTE[m_DataSizeAfterVLR[i]];
				memcpy(m_AfterVLR[i], copy.m_AfterVLR[i], m_DataSizeAfterVLR[i]);
			}
		}

		if(m_Version == V10)
		{
			PointDataStartSignature = copy.PointDataStartSignature;
		}

		return ERROR_FREE;
	}

	//
	//Set dummy point data
	//
	ErrorMsg SetDummyPoints(unsigned long numPoints, LAS_VERSION ver, unsigned char Formattype)
	{
		if(m_Version != ver)
			return DIFFERENT_VERSION_ERROR;

		if(m_FormatType != Formattype)
			return DIFFERENT_TYPE_ERROR;

		//
		//Delete old data if exist
		//
		DeletePointData();

		//
		//Write Point Data
		//
		switch (Formattype)
		{
		case 0:
			m_Points_0 = new PointFormat0[numPoints];
			break;
		case 1:
			m_Points_1 = new PointFormat1[numPoints];
			break;
		case 2:
			m_Points_2 = new PointFormat2[numPoints];
			break;
		case 3:
			m_Points_3 = new PointFormat3[numPoints];
			break;
		case 7:
			m_Points_7 = new PointFormat7[numPoints];
			break;
		case 8:
			m_Points_8 = new PointFormat8[numPoints];
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}
		m_NumPoints = numPoints;

		switch (ver)
		{
		case V10:
			m_Header_10.NumberOfPointRecords = numPoints;
			break;
		case V12:
			m_Header_12.NumberOfPointRecords = numPoints;
			break;
		case V14:
			m_Header_14.NumberOfPointRecords = numPoints;
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		return ERROR_FREE;
	}

	//
	//Count number of points by return and Min/Max X and Y coordinates
	//
	ErrorMsg RecalNumberOfPointsByReturnAndMinMaxValues()
	{
		//
		//Count number of points by return
		//
		unsigned int numReturns;
		switch (m_Version)
		{
		case V10:
			numReturns = 5;
			break;
		case V12:
			numReturns = 5;
			break;
		case V14:
			numReturns = 15;
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		std::vector<int> numPoint;
		numPoint.resize(numReturns);
		for (unsigned int i = 0; i < numReturns; i++) numPoint[i] = 0;

		unsigned char idx;
		switch (m_FormatType)
		{
		case 0:
			for (unsigned long i = 0; i<m_NumPoints; i++)
			{
				idx = m_Points_0[i].ReturnNumber - 1;
				if (idx >= 0 && idx<numReturns)
					numPoint[idx] ++;
			}
			break;
		case 1:
			for (unsigned long i = 0; i<m_NumPoints; i++)
			{
				idx = m_Points_1[i].ReturnNumber - 1;
				if (idx >= 0 && idx<numReturns)
					numPoint[idx] ++;
			}
			break;
		case 2:
			for (unsigned long i = 0; i<m_NumPoints; i++)
			{
				idx = m_Points_2[i].ReturnNumber - 1;
				if (idx >= 0 && idx<numReturns)
					numPoint[idx] ++;
			}
			break;
		case 3:
			for (unsigned long i = 0; i<m_NumPoints; i++)
			{
				idx = m_Points_3[i].ReturnNumber - 1;
				if (idx >= 0 && idx<numReturns)
					numPoint[idx] ++;
			}
			break;
		case 7:
			for (unsigned long i = 0; i<m_NumPoints; i++)
			{
				idx = m_Points_7[i].ReturnNumber - 1;
				if (idx >= 0 && idx<numReturns)
					numPoint[idx] ++;
			}
			break;
		case 8:
			for (unsigned long i = 0; i<m_NumPoints; i++)
			{
				idx = m_Points_8[i].ReturnNumber - 1;
				if (idx >= 0 && idx<numReturns)
					numPoint[idx] ++;
			}
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		switch (m_Version)
		{
		case V10:
			for (unsigned int i = 0; i < numReturns; i++) m_Header_10.NumberOfPointsbyReturn[idx] = numPoint[idx];
			break;
		case V12:
			for (unsigned int i = 0; i < numReturns; i++) m_Header_12.NumberOfPointsbyReturn[idx] = numPoint[idx];
			break;
		case V14:
			for (unsigned int i = 0; i < numReturns; i++) m_Header_14.NumberOfPointsbyReturn[idx] = numPoint[idx];
			break;
		}		

		//
		//Min and Max
		//
		double X, Y, Z, Min_X, Max_X, Min_Y, Max_Y;

		this->GetXYZ(0, &X, &Y, &Z);
		Min_X = Max_X = X;
		Min_Y = Max_Y = Y;

		for(unsigned long i=1; i<m_NumPoints; i++)
		{
			this->GetXYZ(i, &X, &Y, &Z);

			if(Min_X > X) Min_X = X;
			if(Max_X < X) Max_X = X;

			if(Min_Y > Y) Min_Y = Y;
			if(Max_Y < Y) Max_Y = Y;
		}

		switch(m_Version)
		{
		case V10:
			m_Header_10.Min_X = Min_X;
			m_Header_10.Max_X = Max_X;
			m_Header_10.Min_Y = Min_Y;
			m_Header_10.Max_Y = Max_Y;
			break;
		case V12:
			m_Header_12.Min_X = Min_X;
			m_Header_12.Max_X = Max_X;
			m_Header_12.Min_Y = Min_Y;
			m_Header_12.Max_Y = Max_Y;
			break;
		case V14:
			m_Header_14.Min_X = Min_X;
			m_Header_14.Max_X = Max_X;
			m_Header_14.Min_Y = Min_Y;
			m_Header_14.Max_Y = Max_Y;
			break;
		}

		return ERROR_FREE;
	}

	//
	//Save current data as a new las
	//
	ErrorMsg SaveAs(char* OutLASFilePath)
	{
		CFile NewLASFile;	
		if(!NewLASFile.Open((LPCTSTR)OutLASFilePath, CFile::modeWrite|CFile::modeCreate|CFile::shareDenyWrite))
			return LASFILE_WRITE_OPEN_ERROR;

		//
		//Recalculate "OffsettoData"
		//
		unsigned long sizeofheader = sizeof(VLR)*m_NumVLR;

		for(unsigned long i=0; i<m_NumVLR; i++)
		{
			sizeofheader += m_DataSizeAfterVLR[i];
		}

		switch(m_Version)
		{
		case V10:
			sizeofheader += sizeof(m_Header_10);
			sizeofheader += 2;// Point start signature
			break;
		case V12:
			sizeofheader += sizeof(m_Header_12);
			break;
		case V14:
			sizeofheader += sizeof(m_Header_14);
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		//
		//Write Header Data
		//
		switch(m_Version)
		{
		case V10:
			m_Header_10.OffsettoData = sizeofheader;
			m_Header_10.WriteHeader(NewLASFile);
			break;
		case V12:
			m_Header_12.OffsettoData = sizeofheader;
			m_Header_12.WriteHeader(NewLASFile);
			break;
		case V14:
			m_Header_14.OffsettoData = sizeofheader;
			m_Header_14.WriteHeader(NewLASFile);
			break;
		default:
			return NOT_SUPPORTED_VERSION;
		}

		//
		//Write VLR
		//
		//
		unsigned long m_VLRSize = sizeof(VLR);

		for(unsigned long i=0; i<m_NumVLR; i++)
		{
			NewLASFile.Write(&m_VLR[i], m_VLRSize);

			NewLASFile.Write(m_AfterVLR[i], m_DataSizeAfterVLR[i]);
		}

		//
		// Point start signature
		//
		if(m_Version == V10)
		{
			NewLASFile.Write(&PointDataStartSignature, 2);
		}

		//
		//Write Point Data
		//
		switch (m_FormatType)
		{
		case 0:
			NewLASFile.Write(m_Points_0, sizeof(PointFormat0)*m_NumPoints);
			break;
		case 1:
			NewLASFile.Write(m_Points_1, sizeof(PointFormat1)*m_NumPoints);
			break;
		case 2:
			NewLASFile.Write(m_Points_2, sizeof(PointFormat2)*m_NumPoints);
			break;
		case 3:
			NewLASFile.Write(m_Points_3, sizeof(PointFormat3)*m_NumPoints);
			break;
		case 7:
			NewLASFile.Write(m_Points_7, sizeof(PointFormat7)*m_NumPoints);
			break;
		case 8:
			NewLASFile.Write(m_Points_8, sizeof(PointFormat8)*m_NumPoints);
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		NewLASFile.Close();

		return ERROR_FREE;
	}

	//
	//Point copy from source data
	//
	ErrorMsg PointCopyFromSource(unsigned long index, unsigned long source_index, CSMLASProvider &source, bool bCheckVersionIndex = true)
	{
		if(bCheckVersionIndex == true)
		{
			if( (m_Version != source.m_Version) | (m_FormatType != source.m_FormatType) )
				return DIFFERENT_TYPE_ERROR;

			if(index >= m_NumPoints)
				return POINT_INDEX_ERROR;

			if(source_index >= source.m_NumPoints)
				return POINT_INDEX_ERROR;
		}

		//
		//Copy Point Data
		//
		switch (m_FormatType)
		{
		case 0:
			memcpy(&(m_Points_0[index]), &(source.m_Points_0[source_index]), sizeof(PointFormat0));
			break;
		case 1:
			memcpy(&(m_Points_1[index]), &(source.m_Points_1[source_index]), sizeof(PointFormat1));
			break;
		case 2:
			memcpy(&(m_Points_2[index]), &(source.m_Points_2[source_index]), sizeof(PointFormat2));
			break;
		case 3:
			memcpy(&(m_Points_3[index]), &(source.m_Points_3[source_index]), sizeof(PointFormat3));
			break;
		case 7:
			memcpy(&(m_Points_7[index]), &(source.m_Points_7[source_index]), sizeof(PointFormat7));
			break;
		case 8:
			memcpy(&(m_Points_8[index]), &(source.m_Points_8[source_index]), sizeof(PointFormat8));
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	static void CopyHeader(LASHeader_12 &dst, LASHeader_10 &src)
	{
		memcpy(dst.FileSignature, src.FileSignature, 4);
		dst.FileSourceID = 0;
		dst.GlobalEncoding = 0;
		dst.GUIDdata_1						= src.GUIDdata_1;
		dst.GUIDdata_2						= src.GUIDdata_2;
		dst.GUIDdata_3						= src.GUIDdata_3;
		memcpy(dst.GUIDdata_4, src.GUIDdata_4, 8);
		dst.VersionMajor					= src.VersionMajor;
		dst.VersionMinor					= src.VersionMinor;
		memcpy(dst.SystemIdentifier, src.SystemIdentifier, 32);
		memcpy(dst.GeneratingSoftware, src.GeneratingSoftware, 32);
		dst.FileCreationDayOfYear			= src.FlightDateJulian;
		dst.FileCreationYear				= src.Year;
		dst.HeaderSize						= sizeof(LASHeader_12);
		dst.OffsettoData					= src.OffsettoData;
		dst.NumberOfVariableLengthRecords	= src.NumberOfVariableLengthRecords;
		dst.PointDataFormatID				= src.PointDataFormatID;
		dst.PointDataRecordLength			= src.PointDataRecordLength;
		memcpy(dst.NumberOfPointsbyReturn, src.NumberOfPointsbyReturn, 20);
		dst.ScaleFactor_X					= src.ScaleFactor_X;
		dst.ScaleFactor_Y					= src.ScaleFactor_Y;
		dst.ScaleFactor_Z					= src.ScaleFactor_Z;
		dst.Offset_X						= src.Offset_X;
		dst.Offset_Y						= src.Offset_Y;
		dst.Offset_Z						= src.Offset_Z;
		dst.Max_X							= src.Max_X;
		dst.Min_X							= src.Min_X;
		dst.Max_Y							= src.Max_Y;
		dst.Min_Y							= src.Min_Y;
		dst.Max_Z							= src.Max_Z;
		dst.Min_Z							= src.Min_Z;
	}

	ErrorMsg GetScanAngleRank(const unsigned long idx, char& scanAngleRank)
	{
		switch (m_FormatType)
		{
		case 0:
			scanAngleRank = m_Points_0[idx].ScanAngleRank;
			break;
		case 1:
			scanAngleRank = m_Points_1[idx].ScanAngleRank;
			break;
		case 2:
			scanAngleRank = m_Points_2[idx].ScanAngleRank;
			break;
		case 3:
			scanAngleRank = m_Points_3[idx].ScanAngleRank;
			break;
		case 7:
			scanAngleRank = (char)m_Points_7[idx].ScanAngleRank;
			break;
		case 8:
			scanAngleRank = (char)m_Points_8[idx].ScanAngleRank;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	char GetScanAngleRank(const unsigned long idx)
	{
		char scanAngleRank;

		if ( ERROR_FREE == GetScanAngleRank(idx, scanAngleRank))
			return scanAngleRank;
		else
			return 0;
	}

	ErrorMsg GetScanDirectionFlag(const unsigned long idx, unsigned char& directionFlag)
	{
		switch (m_FormatType)
		{
		case 0:
			directionFlag = m_Points_0[idx].ScanDirectionFlag;
			break;
		case 1:
			directionFlag = m_Points_1[idx].ScanDirectionFlag;
			break;
		case 2:
			directionFlag = m_Points_2[idx].ScanDirectionFlag;
			break;
		case 3:
			directionFlag = m_Points_3[idx].ScanDirectionFlag;
			break;
		case 7:
			directionFlag = m_Points_7[idx].ScanDirectionFlag;
			break;
		case 8:
			directionFlag = m_Points_8[idx].ScanDirectionFlag;
			break;
		default:
			return NOT_SUPPORTED_FORMAT;
		}

		return ERROR_FREE;
	}

	unsigned char GetScanDirectionFlag(const unsigned long idx)
	{
		unsigned char directionFlag;

		if (ERROR_FREE == GetScanDirectionFlag(idx, directionFlag))
			return directionFlag;
		else
			return 0;
	}

private:
	void SetConfigFromHeader(LASHeader_10 &m_Header_10)
	{
		m_FormatType		= m_Header_10.PointDataFormatID;
		m_NumPoints			= m_Header_10.NumberOfPointRecords;
		m_OffsetToPointData	= m_Header_10.OffsettoData;
		ScaleX				= m_Header_10.ScaleFactor_X;
		ScaleY				= m_Header_10.ScaleFactor_Y;
		ScaleZ				= m_Header_10.ScaleFactor_Z;
		OffsetX				= m_Header_10.Offset_X;
		OffsetY				= m_Header_10.Offset_Y;
		OffsetZ				= m_Header_10.Offset_Z;
		m_HeaderSize		= m_Header_10.HeaderSize;
		m_NumVLR			= m_Header_10.NumberOfVariableLengthRecords;

		MinX				= m_Header_10.Min_X;
		MinY				= m_Header_10.Min_Y;
		MinZ				= m_Header_10.Min_Z;

		MaxX				= m_Header_10.Max_X;
		MaxY				= m_Header_10.Max_Y;
		MaxZ				= m_Header_10.Max_Z;

		m_VerMajor			= m_Header_10.VersionMajor;
		m_VerMinor			= m_Header_10.VersionMinor;

		m_Version = V10;
	}


	void SetConfigFromHeader(LASHeader_11 &m_Header_11)
	{
		m_FormatType = m_Header_11.PointDataFormatID;
		m_NumPoints = m_Header_11.NumberOfPointRecords;
		m_OffsetToPointData = m_Header_11.OffsettoData;
		ScaleX = m_Header_11.ScaleFactor_X;
		ScaleY = m_Header_11.ScaleFactor_Y;
		ScaleZ = m_Header_11.ScaleFactor_Z;
		OffsetX = m_Header_11.Offset_X;
		OffsetY = m_Header_11.Offset_Y;
		OffsetZ = m_Header_11.Offset_Z;
		m_HeaderSize = m_Header_11.HeaderSize;
		m_NumVLR = m_Header_11.NumberOfVariableLengthRecords;

		MinX = m_Header_11.Min_X;
		MinY = m_Header_11.Min_Y;
		MinZ = m_Header_11.Min_Z;

		MaxX = m_Header_11.Max_X;
		MaxY = m_Header_11.Max_Y;
		MaxZ = m_Header_11.Max_Z;

		m_VerMajor = m_Header_11.VersionMajor;
		m_VerMinor = m_Header_11.VersionMinor;

		m_Version = V11;
	}

	void SetConfigFromHeader(LASHeader_12 &m_Header_12)
	{
		m_FormatType		= m_Header_12.PointDataFormatID;
		m_NumPoints			= m_Header_12.NumberOfPointRecords;
		m_OffsetToPointData = m_Header_12.OffsettoData;
		ScaleX				= m_Header_12.ScaleFactor_X;
		ScaleY				= m_Header_12.ScaleFactor_Y;
		ScaleZ				= m_Header_12.ScaleFactor_Z;
		OffsetX				= m_Header_12.Offset_X;
		OffsetY				= m_Header_12.Offset_Y;
		OffsetZ				= m_Header_12.Offset_Z;
		m_HeaderSize		= m_Header_12.HeaderSize;
		m_NumVLR			= m_Header_12.NumberOfVariableLengthRecords;

		MinX				= m_Header_12.Min_X;
		MinY				= m_Header_12.Min_Y;
		MinZ				= m_Header_12.Min_Z;

		MaxX				= m_Header_12.Max_X;
		MaxY				= m_Header_12.Max_Y;
		MaxZ				= m_Header_12.Max_Z;

		m_VerMajor			= m_Header_12.VersionMajor;
		m_VerMinor			= m_Header_12.VersionMinor;

		m_Version = V12;
	}

	void SetConfigFromHeader(LASHeader_14 &m_Header_14)
	{
		m_FormatType = m_Header_14.PointDataFormatID;
		m_NumPoints = (long)m_Header_14.NumberOfPointRecords;
		m_OffsetToPointData = m_Header_14.OffsettoData;
		ScaleX = m_Header_14.ScaleFactor_X;
		ScaleY = m_Header_14.ScaleFactor_Y;
		ScaleZ = m_Header_14.ScaleFactor_Z;
		OffsetX = m_Header_14.Offset_X;
		OffsetY = m_Header_14.Offset_Y;
		OffsetZ = m_Header_14.Offset_Z;
		m_HeaderSize = m_Header_14.HeaderSize;
		m_NumVLR = m_Header_14.NumberOfVariableLengthRecords;

		MinX = m_Header_14.Min_X;
		MinY = m_Header_14.Min_Y;
		MinZ = m_Header_14.Min_Z;

		MaxX = m_Header_14.Max_X;
		MaxY = m_Header_14.Max_Y;
		MaxZ = m_Header_14.Max_Z;

		m_VerMajor = m_Header_14.VersionMajor;
		m_VerMinor = m_Header_14.VersionMinor;

		m_Version = V14;
	}

	void SetVLR(unsigned long NumVLR, VLR *pVLR, BYTE **pAfterVLR)
	{
		if(NumVLR > 0)
		{
			unsigned long m_VLRSize = sizeof(VLR);

			m_VLR = new VLR[m_NumVLR];
			m_DataSizeAfterVLR = new unsigned long[m_NumVLR];
			m_AfterVLR = new BYTE*[m_NumVLR];

			for(unsigned long i=0; i<m_NumVLR; i++)
			{
				m_VLR[i] = pVLR[i];
				m_DataSizeAfterVLR[i] = pVLR[i].RecordLengthAfterHeader;

				m_AfterVLR[i] = new BYTE[m_DataSizeAfterVLR[i]];
				memcpy(m_AfterVLR[i], pAfterVLR[i], m_DataSizeAfterVLR[i]);
			}
		}
	}

	static bool GetDayofYear(unsigned short &dayofyear)
	{
		time_t t = time(0);   // get time now
		struct tm now;
		localtime_s(&now, &t);

		unsigned int days_Feb;

		if( (now.tm_year % 4 == 0 && now.tm_year % 100 != 0) || now.tm_year % 400 == 0)
			days_Feb = 29;
		else
			days_Feb = 28;

		switch(now.tm_mon)
		{
		case 0://Jan
			dayofyear = now.tm_mday;
			break;
		case 1://Feb
			dayofyear = 31+ now.tm_mday;
			break;
		case 2://Mar
			dayofyear = 31+days_Feb + now.tm_mday;
			break;
		case 3://Apr
			dayofyear = 62+days_Feb + now.tm_mday;
			break;
		case 4://May
			dayofyear = 92+days_Feb + now.tm_mday;
			break;
		case 5://Jun
			dayofyear = 123+days_Feb + now.tm_mday;
			break;
		case 6://Jul
			dayofyear = 153+days_Feb + now.tm_mday;
			break;
		case 7://Aug
			dayofyear = 184+days_Feb + now.tm_mday;
			break;
		case 8://Sep
			dayofyear = 215+days_Feb + now.tm_mday;
			break;
		case 9://Oct
			dayofyear = 245+days_Feb + now.tm_mday;
			break;
		case 10://Nov
			dayofyear = 276+days_Feb + now.tm_mday;
			break;
		case 11://Dec
			dayofyear = 306+days_Feb + now.tm_mday;
			break;
		default:
			return false;
		}

		return true;
	}
};

class LASPtBuff
{
public:
	LASPtBuff()
	{
		pBuf		  = NULL;
		m_Points_0 = NULL;
		m_Points_1 = NULL;
		m_Points_2 = NULL;
		m_Points_3 = NULL;
		m_Points_7 = NULL;
		m_Points_8 = NULL;

		m_Version = V12;
		m_FormatType = 0;

		ScaleFactor_X = 0.001;
		ScaleFactor_Y = 0.001;
		ScaleFactor_Z = 0.001;

		Offset_X = 0.0;
		Offset_Y = 0.0;
		Offset_Z = 0.0;
	}

	LASPtBuff(LAS_VERSION ver, unsigned char type, double Sx, double Sy, double Sz, double Ox, double Oy, double Oz)
	{
		pBuf	   = NULL;
		m_Points_0 = NULL;
		m_Points_1 = NULL;
		m_Points_2 = NULL;
		m_Points_3 = NULL;
		m_Points_7 = NULL;
		m_Points_8 = NULL;

		m_Version = ver;
		m_FormatType = type;

		ScaleFactor_X = Sx;
		ScaleFactor_Y = Sy;
		ScaleFactor_Z = Sz;

		Offset_X = Ox;
		Offset_Y = Oy;
		Offset_Z = Oz;
	}

	void SetConfig(LAS_VERSION ver, unsigned char type, double Sx, double Sy, double Sz, double Ox, double Oy, double Oz)
	{
		m_Version = ver;
		m_FormatType = type;

		ScaleFactor_X = Sx;
		ScaleFactor_Y = Sy;
		ScaleFactor_Z = Sz;

		Offset_X = Ox;
		Offset_Y = Oy;
		Offset_Z = Oz;
	}

	~LASPtBuff()
	{
		DeleteBufer();
	}

	void DeleteBufer()
	{
		if (m_Points_0 != NULL) delete[] m_Points_0;
		if (m_Points_1 != NULL) delete[] m_Points_1;
		if (m_Points_2 != NULL) delete[] m_Points_2;
		if (m_Points_3 != NULL) delete[] m_Points_3;
		if (m_Points_7 != NULL) delete[] m_Points_7;
		if (m_Points_8 != NULL) delete[] m_Points_8;

		m_Points_0 = NULL;
		m_Points_1 = NULL;
		m_Points_2 = NULL;
		m_Points_3 = NULL;
		m_Points_7 = NULL;
		m_Points_8 = NULL;

		pBuf = NULL;
	}

	bool FillBuff_from_LAS(unsigned long num_skip_pts, unsigned long max_num_pts, CSMLASProvider &las)
	{
		DeleteBufer();

		switch (m_FormatType)
		{
		case 0:
			m_Points_0 = new PointFormat0[max_num_pts]; pBuf = (void*)m_Points_0;
			break;
		case 1:
			m_Points_1 = new PointFormat1[max_num_pts]; pBuf = (void*)m_Points_1;
			break;
		case 2:
			m_Points_2 = new PointFormat2[max_num_pts]; pBuf = (void*)m_Points_2;
			break;
		case 3:
			m_Points_3 = new PointFormat3[max_num_pts]; pBuf = (void*)m_Points_3;
			break;
		case 7:
			m_Points_7 = new PointFormat7[max_num_pts]; pBuf = (void*)m_Points_7;
			break;
		case 8:
			m_Points_8 = new PointFormat8[max_num_pts]; pBuf = (void*)m_Points_8;
			break;
		default:
			return false;
		}

		if( ERROR_FREE != las.GetBuff(num_skip_pts, max_num_pts, num_read_pts, pBuf) )
			return false;

		return true;
	}
	
	bool FillBuff_Dummy(unsigned long num_pts)
	{
		DeleteBufer();

		switch (m_FormatType)
		{
		case 0:
			m_Points_0 = new PointFormat0[num_pts];
			break;
		case 1:
			m_Points_1 = new PointFormat1[num_pts];
			break;
		case 2:
			m_Points_2 = new PointFormat2[num_pts];
			break;
		case 3:
			m_Points_3 = new PointFormat3[num_pts];
			break;
		case 7:
			m_Points_7 = new PointFormat7[num_pts];
			break;
		case 8:
			m_Points_8 = new PointFormat8[num_pts];
			break;
		default:
			return false;
		}

		num_read_pts = num_pts;

		return true;
	}

	void Release()
	{
		switch (m_FormatType)
		{
		case 0:
			if (NULL != m_Points_0) { delete[] m_Points_0; m_Points_0 = NULL; }
			break;
		case 1:
			if (NULL != m_Points_1) { delete[] m_Points_1; m_Points_1 = NULL; }
			break;
		case 2:
			if (NULL != m_Points_2) { delete[] m_Points_2; m_Points_2 = NULL; }
			break;
		case 3:
			if (NULL != m_Points_3) { delete[] m_Points_3; m_Points_3 = NULL; }
			break;
		case 7:
			if (NULL != m_Points_7) { delete[] m_Points_7; m_Points_7 = NULL; }
			break;
		case 8:
			if (NULL != m_Points_8) { delete[] m_Points_8; m_Points_8 = NULL; }
			break;
		default:
			break;
		}

		pBuf = NULL;
	}

	bool GetXYZI(const unsigned long idx, double *X, double *Y, double *Z, unsigned short *I)
	{
		switch (m_FormatType)
		{
		case 0:
			*X = m_Points_0[idx].X;
			*Y = m_Points_0[idx].Y;
			*Z = m_Points_0[idx].Z;
			*I = m_Points_0[idx].Intensity;
			break;
		case 1:
			*X = m_Points_1[idx].X;
			*Y = m_Points_1[idx].Y;
			*Z = m_Points_1[idx].Z;
			*I = m_Points_1[idx].Intensity;
			break;
		case 2:
			*X = m_Points_2[idx].X;
			*Y = m_Points_2[idx].Y;
			*Z = m_Points_2[idx].Z;
			*I = m_Points_2[idx].Intensity;
			break;
		case 3:
			*X = m_Points_3[idx].X;
			*Y = m_Points_3[idx].Y;
			*Z = m_Points_3[idx].Z;
			*I = m_Points_3[idx].Intensity;
			break;
		case 7:
			*X = m_Points_7[idx].X;
			*Y = m_Points_7[idx].Y;
			*Z = m_Points_7[idx].Z;
			*I = m_Points_7[idx].Intensity;
			break;
		case 8:
			*X = m_Points_8[idx].X;
			*Y = m_Points_8[idx].Y;
			*Z = m_Points_8[idx].Z;
			*I = m_Points_8[idx].Intensity;
			break;
		default:
			return false;
		}

		*X = *X*ScaleFactor_X + Offset_X;
		*Y = *Y*ScaleFactor_Y + Offset_Y;
		*Z = *Z*ScaleFactor_Z + Offset_Z;

		return true;
	}

	bool GetXYZIR(const unsigned long idx, double *X, double *Y, double *Z, unsigned short *I, unsigned char *R)
	{
		switch (m_FormatType)
		{
		case 0:
			*X = m_Points_0[idx].X;
			*Y = m_Points_0[idx].Y;
			*Z = m_Points_0[idx].Z;
			*I = m_Points_0[idx].Intensity;
			*R = m_Points_0[idx].ReturnNumber;
			break;
		case 1:
			*X = m_Points_1[idx].X;
			*Y = m_Points_1[idx].Y;
			*Z = m_Points_1[idx].Z;
			*I = m_Points_1[idx].Intensity;
			*R = m_Points_1[idx].ReturnNumber;
			break;
		case 2:
			*X = m_Points_2[idx].X;
			*Y = m_Points_2[idx].Y;
			*Z = m_Points_2[idx].Z;
			*I = m_Points_2[idx].Intensity;
			*R = m_Points_2[idx].ReturnNumber;
			break;
		case 3:
			*X = m_Points_3[idx].X;
			*Y = m_Points_3[idx].Y;
			*Z = m_Points_3[idx].Z;
			*I = m_Points_3[idx].Intensity;
			*R = m_Points_3[idx].ReturnNumber;
			break;
		case 7:
			*X = m_Points_7[idx].X;
			*Y = m_Points_7[idx].Y;
			*Z = m_Points_7[idx].Z;
			*I = m_Points_7[idx].Intensity;
			*R = m_Points_7[idx].ReturnNumber;
			break;
		case 8:
			*X = m_Points_8[idx].X;
			*Y = m_Points_8[idx].Y;
			*Z = m_Points_8[idx].Z;
			*I = m_Points_8[idx].Intensity;
			*R = m_Points_8[idx].ReturnNumber;
			break;
		default:
			return false;
		}

		*X = *X*ScaleFactor_X + Offset_X;
		*Y = *Y*ScaleFactor_Y + Offset_Y;
		*Z = *Z*ScaleFactor_Z + Offset_Z;

		return true;
	}

	bool GetXYZIR(const unsigned long idx, double &X, double &Y, double &Z, unsigned short &I, unsigned char &R)
	{
		switch (m_FormatType)
		{
		case 0:
			X = m_Points_0[idx].X;
			Y = m_Points_0[idx].Y;
			Z = m_Points_0[idx].Z;
			I = m_Points_0[idx].Intensity;
			R = m_Points_0[idx].ReturnNumber;
			break;
		case 1:
			X = m_Points_1[idx].X;
			Y = m_Points_1[idx].Y;
			Z = m_Points_1[idx].Z;
			I = m_Points_1[idx].Intensity;
			R = m_Points_1[idx].ReturnNumber;
			break;
		case 2:
			X = m_Points_2[idx].X;
			Y = m_Points_2[idx].Y;
			Z = m_Points_2[idx].Z;
			I = m_Points_2[idx].Intensity;
			R = m_Points_2[idx].ReturnNumber;
			break;
		case 3:
			X = m_Points_3[idx].X;
			Y = m_Points_3[idx].Y;
			Z = m_Points_3[idx].Z;
			I = m_Points_3[idx].Intensity;
			R = m_Points_3[idx].ReturnNumber;
			break;
		case 7:
			X = m_Points_7[idx].X;
			Y = m_Points_7[idx].Y;
			Z = m_Points_7[idx].Z;
			I = m_Points_7[idx].Intensity;
			R = m_Points_7[idx].ReturnNumber;
			break;
		case 8:
			X = m_Points_8[idx].X;
			Y = m_Points_8[idx].Y;
			Z = m_Points_8[idx].Z;
			I = m_Points_8[idx].Intensity;
			R = m_Points_8[idx].ReturnNumber;
			break;
		default:
			return false;
		}

		X = X*ScaleFactor_X + Offset_X;
		Y = Y*ScaleFactor_Y + Offset_Y;
		Z = Z*ScaleFactor_Z + Offset_Z;

		return true;
	}

	bool Get(const unsigned long idx, LASPtBuff &buff, unsigned long outbuff_idx)
	{
		void* outbuff = buff.pBuffer();

		switch (m_FormatType)
		{
		case 0:
			((PointFormat0*)outbuff)[outbuff_idx] = m_Points_0[idx];
			break;
		case 1:
			((PointFormat1*)outbuff)[outbuff_idx] = m_Points_1[idx];
			break;
		case 2:
			((PointFormat2*)outbuff)[outbuff_idx] = m_Points_2[idx];
			break;
		case 3:
			((PointFormat3*)outbuff)[outbuff_idx] = m_Points_3[idx];
			break;
		case 7:
			((PointFormat7*)outbuff)[outbuff_idx] = m_Points_7[idx];
			break;
		case 8:
			((PointFormat8*)outbuff)[outbuff_idx] = m_Points_8[idx];
			break;
		default:
			return false;
		}

		return true;
	}

	bool PutXYZI(const unsigned long idx, double X, double Y, double Z, unsigned short I)
	{
		X = (X - Offset_X)/ScaleFactor_X;
		Y = (Y - Offset_Y)/ScaleFactor_Y;
		Z = (Z - Offset_Z)/ScaleFactor_Z;

		switch (m_FormatType)
		{
		case 0:
			m_Points_0[idx].X = (long)X;
			m_Points_0[idx].Y = (long)Y;
			m_Points_0[idx].Z = (long)Z;
			m_Points_0[idx].Intensity = I;
			break;
		case 1:
			m_Points_1[idx].X = (long)X;
			m_Points_1[idx].Y = (long)Y;
			m_Points_1[idx].Z = (long)Z;
			m_Points_1[idx].Intensity = I;
			break;
		case 2:
			m_Points_2[idx].X = (long)X;
			m_Points_2[idx].Y = (long)Y;
			m_Points_2[idx].Z = (long)Z;
			m_Points_2[idx].Intensity = I;
			break;
		case 3:
			m_Points_3[idx].X = (long)X;
			m_Points_3[idx].Y = (long)Y;
			m_Points_3[idx].Z = (long)Z;
			m_Points_3[idx].Intensity = I;
			break;
		case 7:
			m_Points_7[idx].X = (long)X;
			m_Points_7[idx].Y = (long)Y;
			m_Points_7[idx].Z = (long)Z;
			m_Points_7[idx].Intensity = I;
			break;
		case 8:
			m_Points_8[idx].X = (long)X;
			m_Points_8[idx].Y = (long)Y;
			m_Points_8[idx].Z = (long)Z;
			m_Points_8[idx].Intensity = I;
			break;
		default:
			return false;
		}

		return true;
	}

	bool PutXYZIR(const unsigned long idx, double X, double Y, double Z, unsigned short I, unsigned char R)
	{
		X = (X - Offset_X)/ScaleFactor_X;
		Y = (Y - Offset_Y)/ScaleFactor_Y;
		Z = (Z - Offset_Z)/ScaleFactor_Z;

		switch (m_FormatType)
		{
		case 0:
			m_Points_0[idx].X = (long)X;
			m_Points_0[idx].Y = (long)Y;
			m_Points_0[idx].Z = (long)Z;
			m_Points_0[idx].Intensity = I;
			m_Points_0[idx].ReturnNumber = R;
			break;
		case 1:
			m_Points_1[idx].X = (long)X;
			m_Points_1[idx].Y = (long)Y;
			m_Points_1[idx].Z = (long)Z;
			m_Points_1[idx].Intensity = I;
			m_Points_1[idx].ReturnNumber = R;
			break;
		case 2:
			m_Points_2[idx].X = (long)X;
			m_Points_2[idx].Y = (long)Y;
			m_Points_2[idx].Z = (long)Z;
			m_Points_2[idx].Intensity = I;
			m_Points_2[idx].ReturnNumber = R;
			break;
		case 3:
			m_Points_3[idx].X = (long)X;
			m_Points_3[idx].Y = (long)Y;
			m_Points_3[idx].Z = (long)Z;
			m_Points_3[idx].Intensity = I;
			m_Points_3[idx].ReturnNumber = R;
			break;
		case 7:
			m_Points_7[idx].X = (long)X;
			m_Points_7[idx].Y = (long)Y;
			m_Points_7[idx].Z = (long)Z;
			m_Points_7[idx].Intensity = I;
			m_Points_7[idx].ReturnNumber = R;
			break;
		case 8:
			m_Points_8[idx].X = (long)X;
			m_Points_8[idx].Y = (long)Y;
			m_Points_8[idx].Z = (long)Z;
			m_Points_8[idx].Intensity = I;
			m_Points_8[idx].ReturnNumber = R;
			break;
		default:
			return false;
		}

		return true;
	}

	bool Put(const unsigned long idx, LASPtBuff &inbuff, unsigned long inbuff_idx)
	{
		switch (m_FormatType)
		{
		case 0:
			m_Points_0[idx] = ((PointFormat0*)inbuff.pBuffer())[inbuff_idx];
			break;
		case 1:
			m_Points_1[idx] = ((PointFormat1*)inbuff.pBuffer())[inbuff_idx];
			break;
		case 2:
			m_Points_2[idx] = ((PointFormat2*)inbuff.pBuffer())[inbuff_idx];
			break;
		case 3:
			m_Points_3[idx] = ((PointFormat3*)inbuff.pBuffer())[inbuff_idx];
			break;
		case 7:
			m_Points_7[idx] = ((PointFormat7*)inbuff.pBuffer())[inbuff_idx];
			break;
		case 8:
			m_Points_8[idx] = ((PointFormat8*)inbuff.pBuffer())[inbuff_idx];
			break;
		default:
			return false;
		}

		return true;
	}

	void* pBuffer()
	{
		switch (m_FormatType)
		{
		case 0:
			return (void*)m_Points_0;
			break;
		case 1:
			return (void*)m_Points_1;
			break;
		case 2:
			return (void*)m_Points_2;
			break;
		case 3:
			return (void*)m_Points_3;
			break;
		case 7:
			return (void*)m_Points_7;
			break;
		case 8:
			return (void*)m_Points_8;
			break;
		default:
			return NULL;
		}
	}

	unsigned long GetNumReadPts() {return num_read_pts;}

private:
	LAS_VERSION m_Version;
	unsigned char m_FormatType;
	void* pBuf;
	PointFormat0 *m_Points_0;
	PointFormat1 *m_Points_1;
	PointFormat2 *m_Points_2;
	PointFormat3 *m_Points_3;
	PointFormat7 *m_Points_7;
	PointFormat8 *m_Points_8;

	double ScaleFactor_X, ScaleFactor_Y, ScaleFactor_Z;
	double Offset_X, Offset_Y, Offset_Z;

	unsigned long num_read_pts;
};

#pragma pack(pop)// #pragma pack(push, 1) ~ #pragma pack(pop) : handle binary data per 1 byte to error in "avoid sizeof(struct or class)"