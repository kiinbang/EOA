/*
 * Copyright (c) 2001-2002, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

//////////////////////
//Transformation3D.h
//for 3D Transformation
//made by Bang Ki In
//First release 2014/06/25
//////////////////////

#ifndef _TRANSFORMATION_3D_
#define _TRANSFORMATION_3D_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMList.h"
#include "SMRotationMatrix.h"
#include "SMMatrixClass.h"
#include "Stdlib.h"

using namespace SMATICS_BBARAB;

/**************************/
//                        //
//class for 3D Transformation
//                        //
/**************************/

class Transformation3D
{
	friend class Conformal3D;
	friend class Affine3D;
	
public:
	CSMList<CSMMatrix<double>> *pReferencePoints;
	CSMList<CSMMatrix<double>> *pTargetPoints;

protected:
	CSMList<CSMMatrix<double>> ReferencePoints;
	CSMList<CSMMatrix<double>> TargetPoints;

	unsigned int DF;//Degree of freedom

	unsigned int num_parameters;
		
public:
	Transformation3D() 
	{
		pReferencePoints = &ReferencePoints;
		pTargetPoints = &TargetPoints;
	};
	
	virtual ~Transformation3D()
	{
		ReferencePoints.RemoveAll();
		TargetPoints.RemoveAll();
	}

	unsigned int Get_DegreeOfFreedom() {return DF;}

	virtual bool Solve_Transform(double &sd) = 0;

	virtual CSMMatrix<double> CoordTransform(CSMMatrix<double> P) = 0;
	virtual CSMMatrix<double> InverseCoordTransform(CSMMatrix<double> Q) = 0;
};

/***********************************/
//                                 //
//class for 3D conformal
//                                 //
/***********************************/
//Q = S X R X (P - T)
///////////////////////////////////////////////////////////////////
//7 unknown parameters : S, Omega, Phi, Kappa, 3 Translations
///////////////////////////////////////////////////////////////////
class Conformal3D:public Transformation3D
{
public:
	double S;
	double Omega, Phi, Kappa;
	_Rmat_ R;
	CSMMatrix<double> Translation;

	double S_inv;
	_Mmat_ M;
	CSMMatrix<double> Translation_inv;

	CSMMatrix<double> Ref_center, Tar_center;
	
	bool *m_bFixedParameters;

	double Th_dSigma;
	unsigned int max_Iter;

public:
	Conformal3D() 
	{
		Ref_center.Resize(3,1);
		Tar_center.Resize(3,1);

		num_parameters = 7;

		m_bFixedParameters = new bool[num_parameters];

		for(unsigned int i=0; i<num_parameters; i++)
		{
			m_bFixedParameters[i] = false;
		}

		SetInitParameters(0., 0., 0., 0., 0., 0., 1.);

		Th_dSigma = 0.000001;
		max_Iter = 20;
	}

	virtual ~Conformal3D() 
	{
		delete[] m_bFixedParameters;
	}

	void Test(bool bNormal)
	{
		TestDataGen(1.0, 1.0, 1.0, 0.1, -0.1, 0.31, 1.001);

		double sd;
		this->Solve_Transform(sd);

		CSMMatrix<double> P, Q, temp;
		CSMMatrix<double> PN, QN;

		for(unsigned int i=0; i<TargetPoints.GetNumItem(); i++)
		{
			Q = this->CoordTransform(TargetPoints[i]);
			P = this->InverseCoordTransform(ReferencePoints[i]);

			temp = Q - ReferencePoints[i];
			temp = P - TargetPoints[i];
		}
	}

	void TestDataGen(double Tx, double Ty, double Tz, double O, double P, double K, double S)
	{
		srand( (unsigned)time( NULL ) );

		CSMMatrix<double> Pt(3,1), Qt(3,1);

		double H_max = 20.0;
		double H_min = 10.0;

		_Mmat_ M(O, P, K);
		CSMMatrix<double> T(3,1);
		T(0,0) = Tx;
		T(1,0) = Ty;
		T(2,0) = Tz;

		CSMMatrix<double> Center_Q(3,1), Center_P(3,1);

		for(unsigned int i=0; i<10; i++)
		{
			for(unsigned int j=0; j<10; j++)
			{
				Qt(0,0) = (double)i*10.;
				Qt(1,0) = (double)j*10.;
				Qt(2,0) = (double)rand()/(RAND_MAX+1) * (H_max - H_min) + H_min;

				ReferencePoints.AddTail(Qt);

				Center_Q += Qt;
			}
		}

		Center_Q = Center_Q/100.;

		Center_P = Center_Q - Tx;

		for(unsigned int i=0; i<100; i++)
		{
			Qt = ReferencePoints[i] - Center_Q;

			Pt = M.Mmatrix % Qt;

			Pt = Pt / S;

			Pt = Pt + Center_P;

			TargetPoints.AddTail(Pt);
		}
	}

	void SetFixedParameters(bool *fixed/*Tx, Ty, Tz, Om, Ph, Ka, Scale*/)
	{
		for(unsigned int i=0; i<num_parameters; i++)
		{
			m_bFixedParameters[i] = fixed[i];
		}
	}

	void SetInitParameters(double Tx_init=0., double Ty_init=0., double Tz_init=0., double O_init=0., double P_init=0., double K_init=0., double S_init=1.)
	{
		S					= S_init;
		Translation.Resize(3,1);
		Translation(0,0)	= Tx_init;
		Translation(1,0)	= Ty_init;
		Translation(2,0)	= Tz_init;
		Omega				= O_init;
		Phi					= P_init;
		Kappa				= K_init;

		R.ReMake(Omega, Phi, Kappa);
	}

	
	void SetTolerance(double diff_sigma, unsigned int numIteration)
	{
		Th_dSigma = diff_sigma;
		max_Iter = numIteration;
		
	}

	virtual bool Solve_Transform(double &sd)
	{
		//0. Check the number of tie points
		unsigned long numP;
		if( TargetPoints.GetNumItem() == ReferencePoints.GetNumItem())
			numP = TargetPoints.GetNumItem();
		else
			return false;

		// 1. Centroid
		Ref_center.Resize(3,1);
		Tar_center.Resize(3,1);

		for(unsigned long i=0; i<numP; i++)
		{
			Tar_center = (Tar_center * (double)i + TargetPoints[i])/(double)(i+1);
			Ref_center = (Ref_center * (double)i + ReferencePoints[i])/(double)(i+1);
		}

		CSMMatrix<double> TempPoint;
		double distance_Tar, distance_Ref;
		double scale_sum = 0.0;
		long count = 0;

		CSMMatrix<double> Pt_Tar(3,1), Pt_Ref(3,1);

		for(unsigned long i=0; i<numP; i++)
		{
			//Centeroid shift
			Pt_Ref = ReferencePoints[i] - Ref_center;
			Pt_Tar = TargetPoints[i] - Tar_center;

			//Calculate distances from center to point
			TempPoint = Pt_Ref;
			TempPoint = TempPoint * TempPoint;
			distance_Ref = sqrt(TempPoint(0,0) + TempPoint(1,0) + TempPoint(2,0));

			TempPoint = Pt_Tar;
			TempPoint = TempPoint * TempPoint;
			distance_Tar = sqrt(TempPoint(0,0) + TempPoint(1,0) + TempPoint(2,0));

			if(distance_Tar <= 0.)
				continue;

			scale_sum += distance_Ref/distance_Tar;
			count++;
		}

		// 2. Scale update
		if(count <= 0 || scale_sum <= 0)
			return false;
		else
		{
			if(m_bFixedParameters[6] == false)//Scale is not fixed
				S = scale_sum/count;
		}

		CSMMatrix<double> N, C;
		CSMMatrix<double> aiO(3,1), aiP(3,1), aiK(3,1), li(3,1);
		CSMMatrix<double> ai(3,3), ai_t;
		CSMMatrix<double> Ninv, VarCov, X;
		double ltl, variance;
		double sigma, sigma0=1.0e10;
		unsigned iter_count = 0;

		DF = numP*3 - num_parameters;

		for(unsigned int i=0; i<num_parameters; i++)
		{
			if(m_bFixedParameters[i] == true)
				DF--;
		}

		if(DF < 0)
			return false;

		do
		{
			_Rmat_ R(Omega, Phi, Kappa);
			_Rmat_ dRdO; dRdO.Partial_dRdO(Omega, Phi, Kappa);
			_Rmat_ dRdP; dRdP.Partial_dRdP(Omega, Phi, Kappa);
			_Rmat_ dRdK; dRdK.Partial_dRdK(Omega, Phi, Kappa);

			N.Resize(3,3);
			C.Resize(3,1);

			ltl = 0.0;

			for(unsigned int i=0; i<numP; i++)
			{
				Pt_Tar = TargetPoints[i]- Tar_center;

				Pt_Ref = ReferencePoints[i] - Ref_center;

				aiO = dRdO.Rmatrix % Pt_Tar;
				aiP = dRdP.Rmatrix % Pt_Tar;
				aiK = dRdK.Rmatrix % Pt_Tar;

				li = Pt_Ref - R.Rmatrix % (Pt_Tar*S);

				ai.Insert(0, 0, aiO);
				ai.Insert(0, 1, aiP);
				ai.Insert(0, 2, aiK);

				ai_t = ai.Transpose();

				N += ai_t % ai;
				C += ai_t % li;
				
				ltl += (li.Transpose() % li)(0,0);
			}

			// 3. Fixed rotation parameters
			for(unsigned int i=0; i<3; i++)
			{
				if(m_bFixedParameters[i+3] == false)
					continue;
				
				C(i,0) = 0.0;

				for(unsigned int j=0; j<3; j++)
				{
					if(i==j)
					{
						N(i,j) = 1.0;
					}
					else
					{
						N(i,j) = 0.0;
						N(j,i) = 0.0;
					}
				}
			}
			
			Ninv = N.Inverse();
			variance = ltl/(double)DF;
			VarCov = Ninv*variance;

			X = Ninv % C;

			Omega	+= X(0,0);
			Phi		+= X(1,0);
			Kappa	+= X(2,0);

			sigma = sqrt(ltl/(numP*3-3));

			iter_count ++;

			//Stop criteria check

			if(iter_count > 1 && fabs(sigma-sigma0) < Th_dSigma)
				break;
			else
				sigma0 = sigma;

			if(iter_count >= max_Iter)
				break;

		}while(1);

		// 4. Rotation matrix
		R.ReMake(Omega, Phi, Kappa);

		// 5. Translation update
		CSMMatrix<double> Translation_cal = R.Rmatrix.Transpose() % (Ref_center)/S - Tar_center;
		
		for(unsigned int i=0; i<3; i++)
		{
			if(m_bFixedParameters[i] == false)
				Translation(i,0) = Translation_cal(i,0);
		}
						
		//////////////////////////////////////////////////////////////////////////
		//Inverse Transform
		//////////////////////////////////////////////////////////////////////////
		
		// 6. Inverse rotation matrix
		M.ReMake(Omega, Phi, Kappa);

		//7. Inverse translation
		Translation_inv = -R.Rmatrix%Translation*S;

		//8. Inverse scale
		S_inv = 1./S;

		//9. Report sd
		sd = sigma;
		
		return true;
	}

	virtual CSMMatrix<double> CoordTransform(CSMMatrix<double> P) 
	{
		CSMMatrix<double> Q = R.Rmatrix % (P + Translation) * S;
		return Q;
	}

	virtual CSMMatrix<double> InverseCoordTransform(CSMMatrix<double> Q) 
	{
		CSMMatrix<double> P = M.Mmatrix % (Q + Translation_inv) * S_inv;
		return P;
	}
};

/***********************************/
//                                 //
//class for 3D Affine
//                                 //
/***********************************/
//A[0]X + A[1]Y + A[2]Z + A[3] = X'
//A[4]X + A[5]Y + A[6]Z + A[7] = Y'
//A[8]X + A[9]Y + A[10]Z + A[11] = Z'
///////////////////////////////////////////////////////////////////
//12 unknown parameters : A1 to A12
///////////////////////////////////////////////////////////////////
class Affine3D:public Transformation3D
{
protected:
	//Data normalization
	CSMMatrix<double> Ref_Scale;
	CSMMatrix<double> Ref_Offset;

	CSMMatrix<double> Tar_Scale;
	CSMMatrix<double> Tar_Offset;
	
	CSMList<CSMMatrix<double>> ReferencePoints_Ori;
	CSMList<CSMMatrix<double>> TargetPoints_Ori;
	
	bool bNormalized;

public:
	CSMMatrix<double> AffineParameters;
	CSMMatrix<double> AffineParameters_inv;

	bool *m_bFixedParameters;

	//A[0]X + A[1]Y + A[2]Z + A[3] = X'
	//A[4]X + A[5]Y + A[6]Z + A[7] = Y'
	//A[8]X + A[9]Y + A[10]Z + A[11] = Z'

public:
	Affine3D() 
	{
		bNormalized =  true;

		num_parameters = 12;

		m_bFixedParameters = new bool[num_parameters];

		for(unsigned int i=0; i<num_parameters; i++)
		{
			m_bFixedParameters[i] = false;
		}

		double *A = new double[num_parameters];
		for(unsigned int i=0; i<num_parameters; i++)
		{
			A[i] = 0.0;
		}
		A[0]	= 1.0;
		A[5]	= 1.0;
		A[10]	= 1.0;

		SetInitParameters(A);

		delete[] A;
	}

	virtual ~Affine3D() 
	{
		delete[] m_bFixedParameters;

		ReferencePoints_Ori.RemoveAll();
		TargetPoints_Ori.RemoveAll();
	}

	void SetNormalization(bool val)
	{
		bNormalized = val;
	}

	virtual void DataNormalization()
	{
		Ref_Offset.Resize(3,1);
		Ref_Scale.Resize(3,1);

		OriDataCopy(ReferencePoints_Ori, ReferencePoints);

		DataNormalization(ReferencePoints_Ori, ReferencePoints, Ref_Offset, Ref_Scale);

		Tar_Offset.Resize(3,1);
		Tar_Scale.Resize(3,1);

		OriDataCopy(TargetPoints_Ori, TargetPoints);

		DataNormalization(TargetPoints_Ori, TargetPoints, Tar_Offset, Tar_Scale);

		pReferencePoints = &ReferencePoints_Ori;
		pTargetPoints = &TargetPoints_Ori;
	}

	void OriDataCopy(CSMList<CSMMatrix<double>> &Copy, CSMList<CSMMatrix<double>> &Source)
	{
		unsigned int num = Source.GetNumItem();

		Copy.RemoveAll();

		for(unsigned int i=0; i<num; i++)
		{
			Copy.AddTail(Source[i]);
		}

		Source.RemoveAll();
	}

	virtual void DataNormalization(CSMList<CSMMatrix<double>> &Ori, CSMList<CSMMatrix<double>> &Nor, CSMMatrix<double> &Offset, CSMMatrix<double> &Scale)
	{
		unsigned int num = Ori.GetNumItem();

		Nor.RemoveAll();

		double maxX, maxY, maxZ;
		double minX, minY, minZ;

		CSMMatrix<double> P;

		P = Ori[0];

		maxX = minX = P(0,0);
		maxY = minY = P(1,0);
		maxZ = minZ = P(2,0);

		for(unsigned int i=0; i<num; i++)
		{
			P = Ori[i];

			if(minX > P(0,0))
				minX = P(0,0);
			if(maxX < P(0,0))
				maxX = P(0,0);

			if(minY > P(1,0))
				minY = P(1,0);
			if(maxY < P(1,0))
				maxY = P(1,0);

			if(minZ > P(2,0))
				minZ = P(2,0);
			if(maxZ < P(2,0))
				maxZ = P(2,0);
		}

		Scale(0,0) = (maxX - minX)/2.;
		Scale(1,0) = (maxY - minY)/2.;
		Scale(2,0) = (maxZ - minZ)/2.;

		if(Scale(0,0) <= 1.0e-15)
			Scale(0,0) = 1.0e-15;

		if(Scale(1,0) <= 1.0e-15)
			Scale(1,0) = 1.0e-15;

		if(Scale(2,0) <= 1.0e-15)
			Scale(2,0) = 1.0e-15;

		Offset(0,0) = (maxX - minX)/2.0 + minX;
		Offset(1,0) = (maxY - minY)/2.0 + minY;
		Offset(2,0) = (maxZ - minZ)/2.0 + minZ;

		for(unsigned int i=0; i<num; i++)
		{
			P = Ori[i];

			P = (P - Offset)/(Scale);

			Nor.AddTail(P);
		}
	}

	virtual CSMMatrix<double> GetNormal_Ref(CSMMatrix<double> Ori)
	{
		CSMMatrix<double> N;
		N = (Ori - Ref_Offset)/Ref_Scale;

		return N;
	}

	virtual CSMMatrix<double> GetNormal_Tar(CSMMatrix<double> Ori)
	{
		CSMMatrix<double> N;
		N = (Ori - Tar_Offset)/Tar_Scale;

		return N;
	}

	virtual CSMMatrix<double> GetDeNormal_Ref(CSMMatrix<double> N)
	{
		CSMMatrix<double> Ori;
		Ori = (N*Ref_Scale) + Ref_Offset;

		return Ori;
	}

	virtual CSMMatrix<double> GetDeNormal_Tar(CSMMatrix<double> N)
	{
		CSMMatrix<double> Ori;
		Ori = (N*Tar_Scale) + Tar_Offset;

		return Ori;
	}

	void SetFixedParameters(bool *fixed/*A1 to A12*/)
	{
		for(unsigned int i=0; i<num_parameters; i++)
		{
			m_bFixedParameters[i] = fixed[i];
		}
	}

	void SetInitParameters(double *init_param)
	{
		AffineParameters.Resize(num_parameters,1);

		for(unsigned int i=0; i<num_parameters; i++)
		{
			AffineParameters(i,0) = init_param[i];
		}
	}

	virtual bool Solve_Transform(double &sd)
	{
		//Data normalization
		if(bNormalized == true)
		{
			DataNormalization();
		}

		//0. Check the number of tie points
		unsigned long numP;
		if( TargetPoints.GetNumItem() == ReferencePoints.GetNumItem())
			numP = TargetPoints.GetNumItem();
		else
			return false;

		CSMMatrix<double> N, C;
		CSMMatrix<double> ai(3,num_parameters), li(3,1);
		CSMMatrix<double> ai_t;
		CSMMatrix<double> Ninv, VarCov, X;
		CSMMatrix<double> Pt_Tar(3,1), Pt_Ref(3,1);
		
		DF = numP*3 - num_parameters;

		for(unsigned int i=0; i<num_parameters; i++)
		{
			if(m_bFixedParameters[i] == true)
				DF--;
		}

		if(DF < 0)
			return false;

		N.Resize(num_parameters,num_parameters);
		C.Resize(num_parameters,1);

		// 1. Normal matrix
		for(unsigned int i=0; i<numP; i++)
		{
			Pt_Ref = ReferencePoints[i];
			Pt_Tar = TargetPoints[i];

			ai.Resize(3,num_parameters);
			
			ai(0,0)		= Pt_Tar(0,0);
			ai(0,1)		= Pt_Tar(1,0);
			ai(0,2)		= Pt_Tar(2,0);
			ai(0,3)		= 1.0;

			ai(1,4)		= Pt_Tar(0,0);
			ai(1,5)		= Pt_Tar(1,0);
			ai(1,6)		= Pt_Tar(2,0);
			ai(1,7)		= 1.0;

			ai(2,8)		= Pt_Tar(0,0);
			ai(2,9)		= Pt_Tar(1,0);
			ai(2,10)	=  Pt_Tar(2,0);
			ai(2,11)	= 1.0;

			li.Resize(3,1);

			li(0,0)	= Pt_Ref(0,0);
			li(1,0)	= Pt_Ref(1,0);
			li(2,0)	= Pt_Ref(2,0);

			ai_t = ai.Transpose();

			N += ai_t % ai;
			C += ai_t % li;
		}

		// 2. Fixed parameters
		for(unsigned int i=0; i<num_parameters; i++)
		{
			if(m_bFixedParameters[i] == false)
				continue;

			C(i,0) = 0.0;

			for(unsigned int j=0; j<num_parameters; j++)
			{
				if(i==j)
				{
					N(i,j) = 1.0;
				}
				else
				{
					N(i,j) = 0.0;
					N(j,i) = 0.0;
				}
			}
		}

		Ninv = N.Inverse();
		X = Ninv % C;
		
		// 3. Update parameters
		for(unsigned int i=0; i<num_parameters; i++)
		{
			if(m_bFixedParameters[i] == false)
				AffineParameters(i,0) = X(i,0);
		}

		// 4. fitting quality
		double ltl=0.0, variance;
		double sigma, sigma0=1.0e10;

		li.Resize(3,1);

		for(unsigned int i=0; i<numP; i++)
		{
			Pt_Ref = ReferencePoints[i];
			Pt_Tar = TargetPoints[i];

			li(0,0)	= AffineParameters(0,0)*Pt_Tar(0,0) + AffineParameters(1,0)*Pt_Tar(1,0) + AffineParameters(2,0)*Pt_Tar(2,0) + AffineParameters(3,0);
			li(1,0)	= AffineParameters(4,0)*Pt_Tar(0,0) + AffineParameters(5,0)*Pt_Tar(1,0) + AffineParameters(6,0)*Pt_Tar(2,0) + AffineParameters(7,0);
			li(2,0)	= AffineParameters(8,0)*Pt_Tar(0,0) + AffineParameters(9,0)*Pt_Tar(1,0) + AffineParameters(10,0)*Pt_Tar(2,0) + AffineParameters(11,0);

			ltl += (li.Transpose() % li)(0,0);
		}

		if(DF > 0)
			variance = ltl/(double)DF;
		else
			variance = 0.0;

		sigma = sqrt(variance);
		VarCov = Ninv*variance;

		for(unsigned int i=0; i<numP; i++)
		{
			Pt_Ref = pReferencePoints->GetAt(i);
			Pt_Tar = pTargetPoints->GetAt(i);

			li = Pt_Ref - CoordTransform(Pt_Tar);

			ltl += (li.Transpose() % li)(0,0);
		}

		if(DF > 0)
			sd = sqrt(ltl/(double)DF);
		else
			sd = 0.0;

		//////////////////////////////////////////////////////////////////////////
		//Inverse Transform
		//////////////////////////////////////////////////////////////////////////
		CSMMatrix<double> A(4,4);

		for(unsigned int i=0; i<3; i++)
		{
			for(unsigned int j=0; j<4; j++)
			{
				A(i,j) = AffineParameters(i*4+j,0);
			}
		}

		A(3,0) = 0.;
		A(3,1) = 0.;
		A(3,2) = 0.;
		A(3,3) = 1.;

		CSMMatrix<double> A_inv = A.Inverse();

		AffineParameters_inv.Resize(num_parameters,1);

		for(unsigned int i=0; i<3; i++)
		{
			for(unsigned int j=0; j<4; j++)
			{
				AffineParameters_inv(i*4+j,0) = A_inv(i,j);
			}
		}
		
		return true;
	}

	virtual CSMMatrix<double> CoordTransform(CSMMatrix<double> P) 
	{
		if(bNormalized == true)
		{
			CSMMatrix<double> PN = GetNormal_Tar(P);
			CSMMatrix<double> QN(3,1);
			
			QN(0,0)	= AffineParameters(0,0)*PN(0,0) + AffineParameters(1,0)*PN(1,0) + AffineParameters(2,0)*PN(2,0) + AffineParameters(3,0);
			QN(1,0)	= AffineParameters(4,0)*PN(0,0) + AffineParameters(5,0)*PN(1,0) + AffineParameters(6,0)*PN(2,0) + AffineParameters(7,0);
			QN(2,0)	= AffineParameters(8,0)*PN(0,0) + AffineParameters(9,0)*PN(1,0) + AffineParameters(10,0)*PN(2,0) + AffineParameters(11,0);

			CSMMatrix<double> Q = GetDeNormal_Ref(QN);
			return Q;
		}
		else
		{
			CSMMatrix<double> Q(3,1);

			Q(0,0)	= AffineParameters(0,0)*P(0,0) + AffineParameters(1,0)*P(1,0) + AffineParameters(2,0)*P(2,0) + AffineParameters(3,0);
			Q(1,0)	= AffineParameters(4,0)*P(0,0) + AffineParameters(5,0)*P(1,0) + AffineParameters(6,0)*P(2,0) + AffineParameters(7,0);
			Q(2,0)	= AffineParameters(8,0)*P(0,0) + AffineParameters(9,0)*P(1,0) + AffineParameters(10,0)*P(2,0) + AffineParameters(11,0);

			return Q;
		}
	}

	virtual CSMMatrix<double> InverseCoordTransform(CSMMatrix<double> Q) 
	{
		if(bNormalized == true)
		{
			CSMMatrix<double> QN = GetNormal_Ref(Q);
			CSMMatrix<double> PN(3,1);
			
			PN(0,0)	= AffineParameters_inv(0,0)*QN(0,0) + AffineParameters_inv(1,0)*QN(1,0) + AffineParameters_inv(2,0)*QN(2,0) + AffineParameters_inv(3,0);
			PN(1,0)	= AffineParameters_inv(4,0)*QN(0,0) + AffineParameters_inv(5,0)*QN(1,0) + AffineParameters_inv(6,0)*QN(2,0) + AffineParameters_inv(7,0);
			PN(2,0)	= AffineParameters_inv(8,0)*QN(0,0) + AffineParameters_inv(9,0)*QN(1,0) + AffineParameters_inv(10,0)*QN(2,0) + AffineParameters_inv(11,0);

			CSMMatrix<double> P = GetDeNormal_Tar(PN);
			return P;
		}
		else
		{
			CSMMatrix<double> P(3,1);

			P(0,0)	= AffineParameters_inv(0,0)*Q(0,0) + AffineParameters_inv(1,0)*Q(1,0) + AffineParameters_inv(2,0)*Q(2,0) + AffineParameters_inv(3,0);
			P(1,0)	= AffineParameters_inv(4,0)*Q(0,0) + AffineParameters_inv(5,0)*Q(1,0) + AffineParameters_inv(6,0)*Q(2,0) + AffineParameters_inv(7,0);
			P(2,0)	= AffineParameters_inv(8,0)*Q(0,0) + AffineParameters_inv(9,0)*Q(1,0) + AffineParameters_inv(10,0)*Q(2,0) + AffineParameters_inv(11,0);

			return P;
		}
	}

	void Test(bool bNormal)
	{
		TestDataGen(1.0, 1.0, 1.0, 0.1, -0.1, 0.31, 1.001);

		double sd;
		this->Solve_Transform(sd);

		CSMMatrix<double> P, Q, Qref, temp, tar, ref;

		if(bNormalized == true)
		{
			for(unsigned int i=0; i<TargetPoints.GetNumItem(); i++)
			{
				tar = TargetPoints_Ori[i];
				ref = ReferencePoints_Ori[i];

				Q = this->CoordTransform(TargetPoints_Ori[i]);
				P = this->InverseCoordTransform(ReferencePoints_Ori[i]);

				temp = Q - ReferencePoints_Ori[i];
				temp = P - TargetPoints_Ori[i];
			}
		}
		else
		{
			for(unsigned int i=0; i<TargetPoints.GetNumItem(); i++)
			{
				tar = TargetPoints[i];
				ref = ReferencePoints[i];

				Q = this->CoordTransform(TargetPoints[i]);
				P = this->InverseCoordTransform(ReferencePoints[i]);

				temp = Q - ReferencePoints[i];
				temp = P - TargetPoints[i];
			}
		}
	}

	void TestDataGen(double Tx, double Ty, double Tz, double O, double P, double K, double S)
	{
		srand( (unsigned)time( NULL ) );

		CSMMatrix<double> Pt(3,1), Qt(3,1);

		double H_max = 20.0;
		double H_min = 10.0;

		_Mmat_ M(O, P, K);
		CSMMatrix<double> T(3,1);
		T(0,0) = Tx;
		T(1,0) = Ty;
		T(2,0) = Tz;

		CSMMatrix<double> Center_Q(3,1), Center_P(3,1);

		for(unsigned int i=0; i<10; i++)
		{
			for(unsigned int j=0; j<10; j++)
			{
				Qt(0,0) = (double)i*10.;
				Qt(1,0) = (double)j*10.;
				Qt(2,0) = (double)rand()/(RAND_MAX+1) * (H_max - H_min) + H_min;

				ReferencePoints.AddTail(Qt);

				Center_Q += Qt;
			}
		}

		Center_Q = Center_Q/100.;

		Center_P = Center_Q - Tx;

		for(unsigned int i=0; i<100; i++)
		{
			Qt = ReferencePoints[i] - Center_Q;

			Pt = M.Mmatrix % Qt;

			Pt = Pt / S;

			Pt = Pt + Center_P;

			TargetPoints.AddTail(Pt);
		}
	}
};



////////////////////////////////////////////////////////////////////
#endif //_TRANSFORMATION_3D_