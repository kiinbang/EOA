#pragma once

#include "./ANN/ANN.h"
#include "SMMatrix.h"
using namespace SMATICS_BBARAB;

#include <fstream>
#include <vector>
using namespace std;

#include "TinClass.h"

#include "SMRotationMatrix.h"

#define STRING_LEN 512
#define STR_ID_LEN 20
#define	LARGE	1.0e+9

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

#define DMAX(a,b) ((double)a > (double)b ? (double)a : (double)b)

#define IMIN(a,b) ((int)a < (int)b ? (int)a : (int)b)

#define DSQR(a) ((double)a == 0.0 ? 0.0 : (double)a*(double)a)

namespace SMATICS_BBARAB
{
	typedef struct {
		double	x;
		double	y;
		double	z;
	}P_Suf1;

	typedef struct
	{
		int		WTXY, WTZ;
		double	X[3];
		double	Y[3];
		double	Z[3];
		double	min_x;
		double  min_y;
		double  max_x;
		double  max_y;
	}TIN_Surf2;

	typedef struct
	{
		double x;
		double y;
		double z;
		int    Patch_ID;
	}VERTEX;

	typedef struct
	{
		int		*surf1_index;
		int		*surf2_index;
		int	count;
	} MATCH_RES_S;

	typedef struct {
		double w[7];
		double XT, YT, ZT;
		double S;
		double om, phi, kap;//radian

		double S_XT, S_YT, S_ZT;
		double S_S;
		double S_om, S_phi, S_kap;//radian

		unsigned int no_iteration;
		double posteriori_sigma;

		double Ave_ND;

	} COMPUTEPARAM_RESULT;

	typedef struct {
		double DX, DY, DZ; //centroid coordinates
		unsigned int num_sp1_point;
		unsigned int num_sp2_patch;
		vector<COMPUTEPARAM_RESULT> computeparam_result;
	} TRANSFORM_RESULT;

	typedef struct {
		int hr1, min1, sec1;
		int hr2, min2, sec2;
		int dhr, dmin, dsec;
	} TIMECHECK;

	typedef struct
	{
		double XT, YT, ZT, S;
		double om, phi, kap; //radian
	} TransParam;

	typedef struct
	{
		double XT, YT, ZT, S;
		double om, phi, kap; //radian

		double DX, DY, DZ;//centroid
	} TransParamII;

	class CSMICPatch
	{
	public:
		CSMICPatch(void)
		{
			R = dmatrix(0, 2, 0, 2);
		}

		~CSMICPatch(void)
		{
			free_dmatrix(R, 0, 2, 0, 2);
		}

		static int compare_x(const void*a, const void*b)   //quick sort of the X coordinate, by Ruifang
		{
			VERTEX *arg1 = (VERTEX*) a;
			VERTEX *arg2 = (VERTEX*) b;
			if( arg1->x < arg2->x) return -1;
			else if(arg1->x == arg2->x) return 0;
			else return 1;
		}

		static int compare_y(const void*a, const void*b) //quick sort of the Y coordinate, by Ruifang
		{
			VERTEX *arg1 = (VERTEX*) a;
			VERTEX *arg2 = (VERTEX*) b;
			if( arg1->y < arg2->y) return -1;
			else if(arg1->y == arg2->y) return 0;
			else return 1;
		}

		static int compare_z(const void*a, const void*b)  //quick sort of the Z coordinate, by Ruifang
		{
			VERTEX *arg1 = (VERTEX*) a;
			VERTEX *arg2 = (VERTEX*) b;
			if( arg1->z < arg2->z) return -1;
			else if(arg1->z == arg2->z) return 0;
			else return 1;
		}

		static double** dmatrix( int nrl , int nrh , int ncl , int nch)
		{
			int i , nrow = nrh - nrl + 1 ;
			int ncol = nch - ncl + 1 ;
			double **m;

			m = (double **) malloc ( nrow * sizeof(double*));

			m[nrl] = (double *) malloc ( nrow * ncol * sizeof(double));

			for ( i = nrl+1 ; i <= nrh ; i++)
				m[i] = m[i-1] + ncol ;

			return m ;
		}

		static void free_dmatrix(double **m , int nrl , int nrh , int ncl , int nch)
		{
			if(m == NULL) return;

			free (m[nrl]+ncl);
			free (m+nrl);

			m = NULL;
		}

		static void mult_AV( double **a , double *y , double *c , int n , int m)
		{
			int i , j ;
			for ( i = 0 ; i < n ; i ++)
			{
				c[i] = 0.0 ;
				for ( j = 0 ; j < m ; j++)
					c[i] += a[i][j]*y[j];
			}
		}

		static void mult_VVT(double *A, double *B, double **C, int n)
		{
			int i, j;

			for (i = 0; i < n; i++)
				for (j = 0; j < n; j++)
					C[i][j] = A[i] * B[j];

		}

		static void mult_ABT(double **a, double **b, double **abt, int n, int m, int k)
		{
			int i ,j , l;

			for  (i=0 ; i< n ; ++i)
				for  (j=0 ; j< k ; ++j)
				{
					abt[i][j]=0.0;
					for (l=0 ; l< m ; ++l)
						abt[i][j] = abt[i][j] + a[i][l]*b[j][l];
				}
		}

		static void dsvdcmp(double **a, int m, int n, double *w, double **v)
		{
			int flag,i,its,j,jj,k,l,nm;

			double anorm,c,f,g,h,s,scale,x,y,z,*rv1;

			rv1 = (double *)malloc(n * sizeof(double));
			//if (!rv1) printf("allocation failure for rv1");

			g = scale = anorm = 0.0;

			for (i=0 ; i < n ; i++) {
				l=i+1;
				rv1[i] = scale * g;
				g = s = scale = 0.0;
				if (i < m) {
					for (k=i ; k<m ;k++) scale += fabs(a[k][i]);
					if (scale) {
						for (k=i ; k<m ;k++) {
							a[k][i] /= scale;
							s += a[k][i]*a[k][i];
						}
						f = a[i][i];


						g = -SIGN(sqrt(s),f);

						h = f*g-s;
						a[i][i]=f-g;
						for (j=l; j<n ;j++) {
							for (s=0.0,k=i ; k<m ; k++) s += a[k][i]*a[k][j];
							f=s/h;
							for (k=i;k<m;k++) a[k][j] += f*a[k][i];
						}
						for (k=i ; k<m ; k++) a[k][i] *= scale;
					}
				}


				w[i] = scale *g;
				g = s = scale =0.0;
				if (i < m && i != n-1) {
					for (k=l ; k<n ; k++) scale += fabs(a[i][k]);
					if (scale) {
						for (k=l ; k<n ; k++) {
							a[i][k] /= scale;
							s += a[i][k]*a[i][k];
						}
						f=a[i][l];
						g = -SIGN(sqrt(s),f);
						h=f*g-s;
						a[i][l]=f-g;
						for (k=l ; k<n ; k++) rv1[k]=a[i][k]/h;
						for (j=l ; j<m ; j++) {
							for (s=0.0,k=l ; k<n ; k++) s += a[j][k]*a[i][k];
							for (k=l ; k<n ; k++) a[j][k] += s*rv1[k];
						}
						for (k=l ; k<n ;k++) a[i][k] *= scale;
					}
				}


				anorm=DMAX( anorm, fabs(w[i])+fabs(rv1[i]) );

			}

			for (i=n-1 ; i>=0 ; i--) {

				if (i < n-1) {
					if (g) {
						for (j=l ; j<n ; j++)
							v[j][i]=(a[i][j]/a[i][l])/g;
						for (j=l ; j<n ; j++) {
							for (s=0.0,k=l ; k<n ; k++) s += a[i][k]*v[k][j];
							for (k=l ; k<n ; k++) v[k][j] += s*v[k][i];
						}
					}
					for (j=l ; j<n ;j++) v[i][j]=v[j][i]=0.0;
				}
				v[i][i]=1.0;
				g=rv1[i];
				l=i;
			}

			for (i=IMIN(m-1,n-1) ; i>=0 ;i--) {
				l=i+1;
				g=w[i];
				for (j=l ; j<n ; j++) a[i][j]=0.0;
				if (g) {
					g=1.0/g;
					for (j=l ; j<n ; j++) {
						for (s=0.0,k=l ; k<m ; k++) s += a[k][i]*a[k][j];
						f=(s/a[i][i])*g;
						for (k=i ; k<m ; k++) a[k][j] += f*a[k][i];
					}
					for (j=i ; j<m ; j++) a[j][i] *= g;
				} else for (j=i ; j<m ; j++) a[j][i]=0.0;
				++a[i][i];
			}

			for (k=n-1 ; k>=0 ; k--) {
				for (its=1 ; its<=60 ; its++) {
					flag=1;
					for (l=k ; l>=0 ;l--) {
						nm=l-1;
						if ((double)(fabs(rv1[l])+anorm) == anorm) {
							flag=0;
							break;
						}
						if ((double)(fabs(w[nm])+anorm) == anorm) break;
					}
					if (flag) {
						c=0.0;
						s=1.0;
						for (i=l ; i<=k ; i++) {
							f = s*rv1[i];
							rv1[i]=c*rv1[i];
							if ((double)(fabs(f)+anorm) == anorm) break;
							g=w[i];
							h=pythag(f,g);
							w[i]=h;
							h=1.0/h;
							c=g*h;
							s = -f*h;
							for (j=0 ; j<m ; j++) {
								y=a[j][nm];
								z=a[j][i];
								a[j][nm]=y*c+z*s;
								a[j][i]=z*c-y*s;
							}
						}
					}
					z=w[k];
					if (l == k) {
						if (z < 0.0) {
							w[k] = -z;
							for (j=0;j<n;j++) v[j][k] = -v[j][k];
						}
						break;
					}
					//if (its == 40) printf("no convergence in 40 svdcmp iterations @ k = %d\n", k);
					x=w[l];
					nm=k-1;
					y=w[nm];
					g=rv1[nm];
					h=rv1[k];
					f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
					g=pythag(f,1.0);
					f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
					c=s=1.0;
					for (j=l ; j<=nm ; j++) {
						i=j+1;
						g=rv1[i];
						y=w[i];
						h=s*g;
						g=c*g;
						z=pythag(f,h);
						rv1[j]=z;
						c=f/z;
						s=h/z;
						f=x*c+g*s;
						g = g*c-x*s;
						h=y*s;
						y *= c;
						for (jj=0 ; jj<n ; jj++) {
							x=v[jj][j];
							z=v[jj][i];
							v[jj][j]=x*c+z*s;
							v[jj][i]=z*c-x*s;
						}
						z=pythag(f,h);
						w[j]=z;
						if (z) {
							z=1.0/z;
							c=f*z;
							s=h*z;
						}
						f=c*g+s*y;
						x=c*y-s*g;
						for (jj=0 ; jj<m ; jj++) {
							y=a[jj][j];
							z=a[jj][i];
							a[jj][j]=y*c+z*s;
							a[jj][i]=z*c-y*s;
						}
					}
					rv1[l]=0.0;
					rv1[k]=f;
					w[k]=x;
				} /* end for its */
			} /* end for k */
			free(rv1);
		}

		static void Partial_dRdO(double omega,double phi,double kappa, CSMMatrix<double> &Rmatrix)
		{
			Rmatrix.Resize(3,3);
			Rmatrix(0,0) = 0;	
			Rmatrix(0,1) = 0;	
			Rmatrix(0,2) = 0;

			Rmatrix(1,0) = cos(omega)*sin(phi)*cos(kappa) - sin(omega)*sin(kappa);	
			Rmatrix(1,1) = -cos(omega)*sin(phi)*sin(kappa) - sin(omega)*cos(kappa);	
			Rmatrix(1,2) = -cos(omega)*cos(phi);

			Rmatrix(2,0) = sin(omega)*sin(phi)*cos(kappa) + cos(omega)*sin(kappa);	
			Rmatrix(2,1) = -sin(omega)*sin(phi)*sin(kappa) + cos(omega)*cos(kappa);	
			Rmatrix(2,2) = -sin(omega)*cos(phi);
		}

		static void Partial_dRdP(double omega,double phi,double kappa, CSMMatrix<double> &Rmatrix)
		{
			Rmatrix.Resize(3,3);
			Rmatrix(0,0) = -sin(phi)*cos(kappa);	
			Rmatrix(0,1) = sin(phi)*sin(kappa);	
			Rmatrix(0,2) = cos(phi);

			Rmatrix(1,0) = sin(omega)*cos(phi)*cos(kappa);	
			Rmatrix(1,1) = -sin(omega)*cos(phi)*sin(kappa);	
			Rmatrix(1,2) = sin(omega)*sin(phi);

			Rmatrix(2,0) = -cos(omega)*cos(phi)*cos(kappa);	
			Rmatrix(2,1) = cos(omega)*cos(phi)*sin(kappa);	
			Rmatrix(2,2) = -cos(omega)*sin(phi);
		}

		static void Partial_dRdK(double omega,double phi,double kappa, CSMMatrix<double> &Rmatrix)
		{
			Rmatrix.Resize(3,3);
			Rmatrix(0,0) = -cos(phi)*sin(kappa);	
			Rmatrix(0,1) = -cos(phi)*cos(kappa);	
			Rmatrix(0,2) = 0.;

			Rmatrix(1,0) = -sin(omega)*sin(phi)*sin(kappa) + cos(omega)*cos(kappa);	
			Rmatrix(1,1) = -sin(omega)*sin(phi)*cos(kappa) - cos(omega)*sin(kappa);	
			Rmatrix(1,2) = 0.;

			Rmatrix(2,0) = cos(omega)*sin(phi)*sin(kappa) + sin(omega)*cos(kappa);
			Rmatrix(2,1) = cos(omega)*sin(phi)*cos(kappa) - sin(omega)*sin(kappa);	
			Rmatrix(2,2) = 0.;
		}

		static double pythag(double a, double  b)
		{
			double absa,absb;
			absa=fabs(a);
			absb=fabs(b);
			if (absa > absb) return absa*sqrt(1.0 + DSQR(absb/absa));
			else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0 + DSQR(absa/absb)));
		}

		static int rotation_Matrix(double omega, double phi, double kappa, double R[])
		{
			double c1, s1, c2, s2, c3, s3;

			c1 = cos(omega);
			s1 = sin(omega);
			c2 = cos(phi);
			s2 = sin(phi);
			c3 = cos(kappa);
			s3 = sin(kappa);

			R[0] =  c2*c3;
			R[1] = -c2*s3;
			R[2] =  s2;
			R[3] =  c1*s3+s1*s2*c3;
			R[4] =  c1*c3-s1*s2*s3;
			R[5] = -s1*c2;
			R[6] =  s1*s3-c1*s2*c3;
			R[7] =  s1*c3+c1*s2*s3;
			R[8] =  c1*c2;

			return (1);
		}

		TransParamII abs_ort_icpatch_KdTree(TransParam InitParam, TRANSFORM_RESULT &result, TIMECHECK &elapsedtime, vector<P_Suf1> &p_sp1, double sf1_sigma[3], vector<P_Suf1> &sp2_point, int given_t, double ANGHETH=1.0e-5, double DISTTH=1.0, unsigned int max_it = 30, double max_sigma_diff=1.0e-6, bool bMWeight=true, bool bAutoCenter=true, double Center_X=0., double Center_Y=0., double Center_Z=0.)
		{
			unsigned int num_sp1_point = p_sp1.size();
			unsigned int num_sp2_point = sp2_point.size(); 

			CTINClass *lptin =new CTINClass("pczAenVQ");
			lptin->BeginAddPoints();

			for(unsigned int i=0; i<num_sp2_point; i++)
			{
				lptin->AddPoint(sp2_point[i].x, sp2_point[i].y, float(sp2_point[i].z), i);
			}

			//
			//TIN Generation
			//
			lptin->EndAddPoints();
			lptin->FastConstruct();

			//
			//Save TIN
			//
			TIN_Surf2 *sp2_patch;
			unsigned int num_sp2_patch = lptin->GetNumberOfTriangles();
			lptin->TriangleTraversalInit();

			sp2_patch = new TIN_Surf2[num_sp2_patch];

			TRIANGLE *tri;

			for(unsigned int i=0; i<num_sp2_patch; i++)
			{
				tri = lptin->TriangleTraverse();

				if(tri != NULL)
				{
					for(unsigned int j=0; j<3; j++)
					{
						sp2_patch[i].X[j] = tri->vertex[j]->x;
						sp2_patch[i].Y[j] = tri->vertex[j]->y;
						sp2_patch[i].Z[j] = tri->vertex[j]->attr;
					}
				}
			}

			delete lptin;
			int i, j;

			//Matching threshold
			Angle_Threshold = ANGHETH;
			Distance_Threshold = DISTTH;

			//Approximate Values
			XT = InitParam.XT;
			YT = InitParam.YT;
			ZT = InitParam.ZT;
			
			S = InitParam.S;
			
			om = InitParam.om;
			phi = InitParam.phi;
			kap = InitParam.kap;

			//Bang, Ki In (2007.11)
			//Precision of Points(sf1)
			V_X = sf1_sigma[0]*sf1_sigma[0];
			V_Y = sf1_sigma[1]*sf1_sigma[1];
			V_Z = sf1_sigma[2]*sf1_sigma[2];
			//Bang, Ki In

			//To Avoid dealing with large numbers.
			delta_x = delta_y = 15.0;//30.0;	//To limit the search space

			//
			//Centroid
			//
			if(bAutoCenter == false)
			{
				DX = Center_X;
				DY = Center_Y;
				DZ = Center_Z;
			}
			else
			{
				DX = DY = DZ = 0;

				for(unsigned int i=0; i<num_sp1_point; i++)
				{
					DX += p_sp1[i].x;
					DY += p_sp1[i].y;
					DZ += p_sp1[i].z;
				}

				DX /= num_sp1_point;
				DY /= num_sp1_point;
				DZ /= num_sp1_point;
			}

			for (unsigned int i = 0; i<num_sp1_point; i++)
			{
				p_sp1[i].x -= DX;
				p_sp1[i].y -= DY;
				p_sp1[i].z -= DZ;	
			}

			//Centroid
			result.DX = DX;
			result.DY = DY;
			result.DZ = DZ;

			for(unsigned int i=0; i<num_sp2_patch; i++)
			{
				for(unsigned int j=0; j<3; j++)
				{
					sp2_patch[i].WTXY = 1;
					sp2_patch[i].WTZ  = 1;

					sp2_patch[i].X[j] -= DX;
					sp2_patch[i].Y[j] -= DY;
					sp2_patch[i].Z[j] -= DZ;
				}
			}

			/************************************* by Ruifang ****************************************/
			/////////////////////////////////////////////////////////////////////////////////////////
			//build the point indexes of the second surface using the quick sort method;
			CArray<int, int> *pTriIndexes;
			pTriIndexes = new CArray<int, int>[3*num_sp2_patch];
			VERTEX *pAllVertex = new VERTEX[3*num_sp2_patch];
			for(i=0;i<num_sp2_patch;i++)
			{
				for(unsigned int j=0; j<3; j++)
				{
					pAllVertex[3*i+j].x = sp2_patch[i].X[j];
					pAllVertex[3*i+j].y = sp2_patch[i].Y[j];
					pAllVertex[3*i+j].z = sp2_patch[i].Z[j];
					pAllVertex[3*i+j].Patch_ID = i;
				}
			}

			// quick sort to X, Y, Z respectively;
			qsort(pAllVertex, 3*num_sp2_patch, sizeof(VERTEX),compare_x);  //sort the X coordinates;
			CArray<VERTEX,VERTEX> *pXcorPt = new CArray<VERTEX,VERTEX>[3*num_sp2_patch];
			CArray<VERTEX,VERTEX> *pXYcorPt = new CArray<VERTEX,VERTEX>[3*num_sp2_patch];
			int XcorIndex=0; 
			int XYcorIndex=0; 

			pXcorPt[XcorIndex].Add(pAllVertex[0]);
			for(i=1;i<3*num_sp2_patch;i++)
			{
				if(pAllVertex[i].x == pAllVertex[i-1].x)
				{
					pXcorPt[XcorIndex].Add(pAllVertex[i]);
				}
				else
				{
					XcorIndex++;
					pXcorPt[XcorIndex].Add(pAllVertex[i]);
				}
			}
			int nIndex = 0;
			int nTemp=0;
			VERTEX pQueY[1000];
			for(i=0;i<XcorIndex+1;i++)
			{
				nTemp = pXcorPt[i].GetSize();
				for(j=0;j<nTemp;j++)
				{
					pQueY[j] = pXcorPt[i].GetAt(j);
				}
				qsort(pQueY, nTemp, sizeof(VERTEX),compare_y);   // sort the y coordinates;
				for(j=0;j<nTemp;j++)
				{
					pAllVertex[nIndex+j] = pQueY[j];
				}
				nIndex+=nTemp;
			}
			for(i=0;i<3*num_sp2_patch;i++)
			{
				pXcorPt[i].RemoveAll();
			}
			if(pXcorPt) delete []pXcorPt;
			pXcorPt = NULL;

			pXYcorPt[XYcorIndex].Add(pAllVertex[0]);
			for(i=1;i<3*num_sp2_patch;i++)
			{
				if(pAllVertex[i].x == pAllVertex[i-1].x && pAllVertex[i].y == pAllVertex[i].y)
				{
					pXYcorPt[XYcorIndex].Add(pAllVertex[i]);
				}
				else
				{
					XYcorIndex++;
					pXYcorPt[XYcorIndex].Add(pAllVertex[i]);
				}
			}
			nIndex = 0;
			nTemp = 0;
			for(i=0;i<XYcorIndex+1;i++)
			{
				nTemp = pXYcorPt[i].GetSize();
				for(j=0;j<nTemp;j++)
				{
					pQueY[j] = pXYcorPt[i].GetAt(j);
				}
				qsort(pQueY, nTemp, sizeof(VERTEX),compare_z);       //sort the Z coordinates;
				for(j=0;j<nTemp;j++)
				{
					pAllVertex[nIndex+j] = pQueY[j];
				}
				nIndex+=nTemp;
			}
			for(i=0;i<3*num_sp2_patch;i++)
			{
				pXYcorPt[i].RemoveAll();
			}
			if(pXYcorPt) delete [] pXYcorPt;
			pXYcorPt = NULL;
			//////////////////end building the Vertex Index;/////////////////////////////////////////////////


			///////////////////////////////////////////////////////////////////////////////
			//Build the k-d tree of the second surface;
			int kd_k =100;
			int dim = 3;
			double eps = 0;
			ANNpointArray		dataPts;				// data points
			ANNpoint			queryPt;				// query point
			ANNkd_tree*         kdTree;                 // search structure
			queryPt = annAllocPt(dim);					// allocate query point
			dataPts = annAllocPts(3*num_sp2_patch, dim);    // allocate data points
			queryPt = annAllocPt(dim);					// allocate query point
			ANNidxArray			nnIdx;    		    	// near neighbor indices
			ANNdistArray		dists;					// near neighbor distances
			nnIdx = new ANNidx[kd_k];               // allocate near neigh indices
			dists = new ANNdist[kd_k];              // allocate near neighbor dists
			BOOL *bComputed = new BOOL[num_sp2_patch];   //whether the Triangle is computed;

			int nVertex= 0;
			pTriIndexes[nVertex].Add(pAllVertex[0].Patch_ID);
			dataPts[nVertex][0] = pAllVertex[0].x ;
			dataPts[nVertex][1] = pAllVertex[0].y;
			dataPts[nVertex][2] = pAllVertex[0].z;
			for(i=1;i<3*num_sp2_patch;i++)
			{
				if((pAllVertex[i].x == pAllVertex[i-1].x)&& (pAllVertex[i].y == pAllVertex[i-1].y)
					&&(pAllVertex[i].z == pAllVertex[i-1].z))
				{
					pTriIndexes[nVertex].Add(pAllVertex[i].Patch_ID);
				}
				else
				{
					nVertex++;
					pTriIndexes[nVertex].Add(pAllVertex[i].Patch_ID);
					dataPts[nVertex][0] = pAllVertex[i].x;
					dataPts[nVertex][1] = pAllVertex[i].y;
					dataPts[nVertex][2] = pAllVertex[i].z;
				}
			}
			if(pAllVertex) delete []pAllVertex;


			kdTree = new ANNkd_tree(dataPts, nVertex,dim);

			/************************************end************************************************/


			// Find the range for each patch
			for (i = 0; i < num_sp2_patch; i++)
				find_min_max (sp2_patch, i);

			result.num_sp1_point = num_sp1_point;
			result.num_sp2_patch = num_sp2_patch;

			//The matching process
			int flag = 0;

			CTime t;

			double check;
			CTime time1, time2;

			// Beginning Time
			time1		= CTime::GetCurrentTime();;
			elapsedtime.hr1		= time1.GetHour();
			elapsedtime.min1	= time1.GetMinute();
			elapsedtime.sec1	= time1.GetSecond();

			ITX = ITY = ITZ = ITS = ITO = ITP = ITK = 0;

			double  seg_bef = 100000.0;

			/********kd_k can be modified so as to enlarge the search area;********************/
			//	kd_k = 50;    // search the nearest kd_k points in the seconde surface, by Ruifang;

			for (int NNN = 0; NNN < max_it; NNN++)
			{
				ITX++;
				ITY++;
				ITZ++;
				ITS++;
				ITO++;
				ITP++;
				ITK++;

				//Matching surfaces
				Match_Surfaces_c (kdTree, pTriIndexes, kd_k, p_sp1, num_sp1_point, sp2_patch, num_sp2_patch);
				
				//Bang, Ki In 20071120
				if(bMWeight == false)
					check = Compute_Parameters(result, p_sp1, sp2_patch, given_t, max_sigma_diff);
				else 
					check = Compute_Parameters_MWeight(result, p_sp1, sp2_patch, given_t, max_sigma_diff, max_it);

				double dseg = check - seg_bef;
				seg_bef = check;
				if (fabs (dseg) <= max_sigma_diff) 
					break;
			}

			/*************************** by Ruifang ***********************************/
			if(kdTree) delete kdTree;
			annClose();
			for(i=0;i<3*num_sp2_patch;i++)
			{
				pTriIndexes[i].RemoveAll();
			}
			if(pTriIndexes) delete []pTriIndexes;
			pTriIndexes = NULL;
			if(bComputed) delete []bComputed;
			bComputed = NULL;
			/**************************** end *****************************************/

			time2 = CTime::GetCurrentTime();;
			elapsedtime.hr2  = time2.GetHour();
			elapsedtime.min2 = time2.GetMinute();
			elapsedtime.sec2 = time2.GetSecond();

			if (elapsedtime.sec2 >= elapsedtime.sec1)
				elapsedtime.dsec = elapsedtime.sec2 - elapsedtime.sec1;
			else
			{
				elapsedtime.dsec = elapsedtime.sec2 + 60 - elapsedtime.sec1;
				elapsedtime.min2 = elapsedtime.min2 - 1;
			}

			if (elapsedtime.min2 >= elapsedtime.min1)
				elapsedtime.dmin = elapsedtime.min2 - elapsedtime.min1;
			else
			{
				elapsedtime.dmin = elapsedtime.min2 + 60 - elapsedtime.min1;
				elapsedtime.hr2 = elapsedtime.hr2 - 1;
			}

			if (elapsedtime.hr2 >= elapsedtime.hr1)
				elapsedtime.dhr = elapsedtime.hr2 - elapsedtime.hr1;
			else
			{
				elapsedtime.dhr = elapsedtime.hr2 + 24 - elapsedtime.hr1;
			}

			delete[] sp2_patch;

			TransParamII final_param;
			unsigned int last_it = result.computeparam_result.size()-1;
			final_param.XT = result.computeparam_result[last_it].XT;
			final_param.YT = result.computeparam_result[last_it].YT;
			final_param.ZT = result.computeparam_result[last_it].ZT;
			final_param.S = result.computeparam_result[last_it].S;
			final_param.om = result.computeparam_result[last_it].om;
			final_param.phi = result.computeparam_result[last_it].phi;
			final_param.kap = result.computeparam_result[last_it].kap;
			final_param.DX = DX;
			final_param.DY = DY;
			final_param.DZ = DZ;

			return final_param;
		}

		double Match_Surfaces_c(ANNkd_tree* kdTree, CArray<int,int> *pTriIndexes, int k, vector<P_Suf1> &p_sp1, unsigned int num_sp1_point, TIN_Surf2 *sp2_patch, unsigned int num_sp2_patch)
		{
			//NOTE: we are not allowing multiple matches
			int		i, j;
			double	CHECK, CHECK1;

			rotation ();
			match_res_s.count = 0;

			/******************* by Ruifang ******************/
			ANNpoint			queryPt;				// query point
			ANNidxArray			nnIdx;    		    	// near neighbor indices
			ANNdistArray		dists;					// near neighbor distances
			int dim = 3;
			double eps = 0;
			queryPt = annAllocPt(dim);					// allocate query point
			nnIdx = new ANNidx[k];               // allocate near neigh indices
			dists = new ANNdist[k];              // allocate near neighbor dists
			int nTriID;
			int mm,m;
			int nTriangles;                      
			BOOL *bComputed = new BOOL[num_sp2_patch];   //whether the Triangle is computed;

			for (i = 0; i < num_sp1_point; i++)
			{

				xa = p_sp1[i].x;
				ya = p_sp1[i].y;
				za = p_sp1[i].z;

				double	min_dist_dif = 50.0;
				int		patch_id;

				compute_XYZA();

				/************************* by Ruifang ****************************/
				queryPt[0] = XA;
				queryPt[1] = YA;
				queryPt[2] = ZA;
				kdTree->annkSearch(queryPt, k, nnIdx, dists, eps); // search the nearest k points in the second surface, by Ruifang;
				for( mm=0; mm<num_sp2_patch; mm++)
				{
					bComputed[mm] = 0;
				}

				for( m=0; m<k; m++)
				{
					nTriangles = pTriIndexes[nnIdx[m]].GetSize();
					for(j=0;j<nTriangles;j++)
					{
						nTriID = pTriIndexes[nnIdx[m]].GetAt(j); //get the triangle ID of the query point;
						if(bComputed[nTriID] == 1) continue; // avoid computing the triangles repeatedly;

						if(queryPt[0] < (sp2_patch[nTriID].min_x - delta_x)) continue;
						if(queryPt[0] > (sp2_patch[nTriID].max_x + delta_x)) continue;
						if(queryPt[1] < (sp2_patch[nTriID].min_y - delta_y)) continue;
						if(queryPt[1] > (sp2_patch[nTriID].max_y + delta_y)) continue;

						bComputed[nTriID] = 1; 

						V1[0] = sp2_patch[nTriID].X[0];
						V1[1] = sp2_patch[nTriID].Y[0];
						V1[2] = sp2_patch[nTriID].Z[0];

						V2[0] = sp2_patch[nTriID].X[1];
						V2[1] = sp2_patch[nTriID].Y[1];
						V2[2] = sp2_patch[nTriID].Z[1];

						V3[0] = sp2_patch[nTriID].X[2];
						V3[1] = sp2_patch[nTriID].Y[2];
						V3[2] = sp2_patch[nTriID].Z[2];

						compute_XYZA();

						double checkA = 0.0;
						double checkB = 0.0;

						Compute_In_Out_NTH (&checkA, &checkB);

						if ((fabs(checkA) < min_dist_dif) && (fabs(checkB - 360) < Angle_Threshold))
						{
							min_dist_dif = fabs(checkA);
							//		            patch_id = j;
							patch_id = nTriID;
							CHECK1 = checkB;
							CHECK  = checkA;
						}
					}	
				}


				if ((min_dist_dif < Distance_Threshold) && (fabs(CHECK1 - 360) < Angle_Threshold))
				{
					match_res_s.count++;
					if (match_res_s.count == 1)
					{
						match_res_s.surf1_index = (int *) malloc (match_res_s.count * sizeof(int));
						match_res_s.surf2_index = (int *) malloc (match_res_s.count * sizeof(int));
					}
					else
					{
						match_res_s.surf1_index = (int *) realloc (match_res_s.surf1_index, match_res_s.count * sizeof(int));
						match_res_s.surf2_index = (int *) realloc (match_res_s.surf2_index, match_res_s.count * sizeof(int));
					}

					int I =  match_res_s.count - 1;
					match_res_s.surf1_index[I] = i;
					match_res_s.surf2_index[I] = patch_id;
				}
			}
			if(bComputed) delete []bComputed;
			bComputed = NULL;
			/******************************************************/
			return (1.0);
		}

		double Compute_Parameters(TRANSFORM_RESULT &result, vector<P_Suf1> &p_sp1, TIN_Surf2 *sp2_patch, int option, double max_sigma_diff)
		{
			// option == 3	Solve for XT, YT, ZT
			// option == 4	Solve for XT, YT, ZT, OM
			// option == 5	Solve for XT, YT, ZT, OM, PH
			// option == 6	Solve for XT, YT, ZT, OM, PH, KP
			// option == 7  Solve for XT, YT, ZT, SF, OM, PH, KP
			// option == 8  Solve for XT, YT, ZT, PH

			COMPUTEPARAM_RESULT compute_result;

			double	sigmo;
			double	*A, *x1, *X1;
			double	y;
			double  *xh, *C;
			double	**N, **NT, **v, *w;

			double	pi;

			sigmo = 0.0;

			double xt_a  = XT;
			double yt_a  = YT;
			double zt_a  = ZT;
			double s_a   = S;
			double om_a  = om;
			double phi_a = phi;
			double kap_a = kap;

			pi = 2.0 * asin(1.0);

			x1 = (double *) malloc (3 * sizeof(double));
			X1 = (double *) malloc (3 * sizeof(double));
			A  = (double *) malloc (7 * sizeof(double));

			N  = dmatrix(0, 6, 0, 6);
			NT = dmatrix(0, 6, 0, 6);
			v  = dmatrix(0, 6, 0, 6);

			xh  =   (double *) malloc (7 * sizeof(double));

			C	=	(double *) malloc (7 * sizeof(double));
			w	=	(double *) malloc (7 * sizeof(double));

			int		no_it, max_it;
			double	etpe, seg_new, seg_old, dif, threshold;

			seg_old = LARGE;
			max_it		= 30;

			int flag_sing = 0;

			threshold = max_sigma_diff;
			double out_liar = 10000000.0;

			int	ii, jj, i, j, n, I, J;

			n = match_res_s.count;

			if (n < 8)
			{
				//There are not enough points to compute the transformation parameters.
				return (-1.0);
			}

			no_it = 0;
			do 
			{
				rotation ();

				etpe = 0.0;

				for (i = 0; i < 7; i++)
				{
					C[i] = 0.0;
					for (j = 0; j < 7; j++)
						N[i][j] = 0.0;
				}

				n = match_res_s.count;

				for (i = 0; i < n; i++)
				{
					I = match_res_s.surf1_index[i];
					J = match_res_s.surf2_index[i];

					xa = p_sp1[I].x;
					ya = p_sp1[I].y;
					za = p_sp1[I].z;

					//xa -= DX;
					//ya -= DY;

					compute_XYZA();
					compute_XYZa();		//no scale or shift is applied 

					V1[0] = sp2_patch[J].X[0];
					V1[1] = sp2_patch[J].Y[0];
					V1[2] = sp2_patch[J].Z[0];

					//V1[0] -= DX;
					//V1[1] -= DY;

					V2[0] = sp2_patch[J].X[1];
					V2[1] = sp2_patch[J].Y[1];
					V2[2] = sp2_patch[J].Z[1];

					//V2[0] -= DX;
					//V2[1] -= DY;

					V3[0] = sp2_patch[J].X[2];
					V3[1] = sp2_patch[J].Y[2];
					V3[2] = sp2_patch[J].Z[2];

					//V3[0] -= DX;
					//V3[1] -= DY;

					compute_det();

					x1[0] =  -ya;
					x1[1] =   xa;
					x1[2] =  0.0;

					mult_AV (R, x1, X1, 3, 3);

					y		= det - XA * detx + YA * dety - ZA * detz;

					//Partial Derivative w.r.t. XT
					A[0]	=  detx;

					//Partial Derivative w.r.t. YT
					A[1]	= -dety;

					//Partial Derivative w.r.t. ZT
					A[2]	=  detz;

					//Partial Derivative w.r.t. Scale Factor
					A[3]	= (detx * Xa  - dety * Ya + detz * Za);

					//Partial Derivative w.r.t. Omega
					A[4]	= (dety * (ZA - ZT) + detz * (YA - YT));

					//Partial Derivative w.r.t. Phi
					A[5]	= (	  detx * (-(YA - YT) * sin(om) + (ZA - ZT) * cos(om))
						- dety * (XA - XT) * sin(om)
						- detz * (XA - XT) * cos(om));

					//Partial Derivative w.r.t. Kapa
					A[6]	= S * (detx * X1[0] - dety * X1[1] + detz * X1[2]);

					//We need to compute the partial derivatives w.r.t. the observed quantities

					compute_BZ();
					compute_BX();
					compute_BY();

					//Bang, Ki In 2007.11
					double p = 1.0 / (BZ1 * BZ1*V_Z + 
						BZA * BZA*V_Z + 
						BZB * BZB*V_Z + 
						BZC * BZC*V_Z + 
						BY1 * BY1*V_Y + 
						BYA * BYA*V_Y + 
						BYB * BYB*V_Y + 
						BYC * BYC*V_Y + 
						BX1 * BX1*V_X + 
						BXA * BXA*V_X + 
						BXB * BXB*V_X + 
						BXC * BXC*V_X);
					
					etpe += y * p * y;

					for (ii = 0; ii < 7; ii++)
						C[ii] += A[ii] * p * y;

					for (ii = 0; ii < 7; ii++)
						A[ii] *= sqrt(p);

					mult_VVT (A, A, NT, 7);

					for (ii = 0; ii < 7; ii++)
						for (jj = 0; jj < 7; jj++)
							N[ii][jj] += NT[ii][jj];

				}
				
				if (option == 6)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;
				}

				if (option == 5)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;

					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 6)
							N[6][jj] = 0.0;
				}

				if (option == 4)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;

					C[5] = 0.0;
					N[5][5] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 5)
							N[ii][5] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 5)
							N[5][jj] = 0.0;

					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 6)
							N[6][jj] = 0.0;
				}

				if (option == 3)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;

					C[4] = 0.0;
					N[4][4] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 4)
							N[ii][4] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 4)
							N[4][jj] = 0.0;

					C[5] = 0.0;
					N[5][5] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 5)
							N[ii][5] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 5)
							N[5][jj] = 0.0;

					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 6)
							N[6][jj] = 0.0;
				}

				if (option == 8)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;

					C[4] = 0.0;
					N[4][4] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 4)
							N[ii][4] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 4)
							N[4][jj] = 0.0;

					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 6)
							N[6][jj] = 0.0;
				}

				dsvdcmp(N, 7, 7, w, v);

				//AH Adjustment SVDcmp

				//*
				for ( ii = 0; ii < 7; ii++)
				{
					if (w[ii] < 0.00000001)
						flag_sing = 1;

					compute_result.w[ii] = w[ii];
				}

				if (flag_sing == 1)
					break;

				//*/

				for ( jj = 0 ; jj < 7 ; jj++)
					for ( ii = 0 ; ii < 7 ; ii++)
						v[ii][jj] /= sqrt(w[jj]);


				mult_ABT (v , v , N , 7 , 7 , 7);
				mult_AV  (N , C , xh , 7 , 7);

				XT	+= (xh[0]);
				YT	+= (xh[1]);
				ZT  += (xh[2]);

				S	+= (xh[3]);

				om	+= (xh[4]);
				phi	+= (xh[5]);
				kap	+= (xh[6]);

				/*
				fprintf (out, "Iteration # %5d\n", no_it);
				fprintf (out, "Estimated Variance Component = %lf\n", sqrt(seg_new));

				fprintf (out, "XT	  = %14.4lf   (%9.6lf) (m)\n", XT, sqrt(N[0][0] * seg_new));
				fprintf (out, "YT     = %14.4lf   (%9.6lf) (m)\n", YT, sqrt(N[1][1] * seg_new));
				fprintf (out, "ZT     = %14.4lf   (%9.6lf) (m)\n", ZT, sqrt(N[2][2] * seg_new));

				fprintf (out, "S      = %14.4lf   (%9.6lf) (m)\n",  S, sqrt(N[3][3] * seg_new));

				fprintf (out, "omega  = %14.4lf   (%9.6lf) (deg)\n", om  * 180.0 / pi, sqrt(N[4][4] * seg_new));
				fprintf (out, "phi    = %14.4lf   (%9.6lf) (deg)\n", phi * 180.0 / pi, sqrt(N[5][5] * seg_new));
				fprintf (out, "kapa   = %14.4lf   (%9.6lf) (deg)\n", kap * 180.0 / pi, sqrt(N[6][6] * seg_new));

				fprintf (out, "\n\n");
				//*/

				no_it++;

				seg_new = etpe / ((double) n - 7.0);
				dif = seg_new - seg_old;
				seg_old = seg_new;			 


			} while ((no_it < max_it) && (fabs(dif) > threshold));

			if (flag_sing == 1)
			{
				XT  = xt_a;
				YT  = yt_a;
				ZT  = zt_a;
				S   = s_a;
				om  = om_a;
				phi = phi_a;
				kap = kap_a;

				compute_result.no_iteration = no_it;
				compute_result.posteriori_sigma = sqrt(seg_new);

				compute_result.XT = XT;
				compute_result.YT = YT;
				compute_result.ZT = ZT;

				compute_result.S = S;

				compute_result.om = om;
				compute_result.phi = phi;
				compute_result.kap = kap;

				//Correlation matrix
				/*
				CSMMatrix<double> Correlation(7,7,0);

				for ( jj = 0 ; jj < 7 ; jj++)
				{
					for ( ii = 0 ; ii < 7 ; ii++)
					{
						Correlation(jj, ii) = N[jj][ii]/sqrt(N[jj][jj])/sqrt(N[ii][ii]);
					}
				}
				*/
			}
			else
			{
				compute_result.no_iteration = no_it;
				compute_result.posteriori_sigma = sqrt(seg_new);

				compute_result.XT = XT;
				compute_result.YT = YT;
				compute_result.ZT = ZT;

				compute_result.S = S;

				compute_result.om = om;
				compute_result.phi = phi;
				compute_result.kap = kap;

				compute_result.S_XT = sqrt(N[0][0] * seg_new);
				compute_result.S_YT = sqrt(N[1][1] * seg_new);
				compute_result.S_ZT = sqrt(N[2][2] * seg_new);

				compute_result.S_S = sqrt(N[3][3] * seg_new);

				compute_result.S_om = sqrt(N[4][4] * seg_new);
				compute_result.S_phi = sqrt(N[5][5] * seg_new);
				compute_result.S_kap = sqrt(N[6][6] * seg_new);
			}

			double NDIST;

			NDIST = 0.0;

			for (i = 0; i < n; i++)
			{
				I = match_res_s.surf1_index[i];
				J = match_res_s.surf2_index[i];

				xa = p_sp1[I].x;
				ya = p_sp1[I].y;
				za = p_sp1[I].z;

				//xa -= DX;
				//ya -= DY;

				compute_XYZA();

				V1[0] = sp2_patch[J].X[0];
				V1[1] = sp2_patch[J].Y[0];
				V1[2] = sp2_patch[J].Z[0];

				//V1[0] -= DX;
				//V1[1] -= DY;

				V2[0] = sp2_patch[J].X[1];
				V2[1] = sp2_patch[J].Y[1];
				V2[2] = sp2_patch[J].Z[1];

				//V2[0] -= DX;
				//V2[1] -= DY;

				V3[0] = sp2_patch[J].X[2];
				V3[1] = sp2_patch[J].Y[2];
				V3[2] = sp2_patch[J].Z[2];

				double checkA = 0.0;
				double checkB = 0.0;

				Compute_In_Out_NTH (&checkA, &checkB);

				NDIST += (checkA * checkA);
			}

			NDIST = sqrt (NDIST/(double)n);

			compute_result.Ave_ND = NDIST;

			free_dmatrix (N , 0, 6, 0, 6);
			free_dmatrix (NT, 0, 6, 0, 6);
			free_dmatrix (v , 0, 6, 0, 6);

			free (x1);
			free (X1);
			free (A);
			free (C);
			free (w);
			free (xh);

			if (match_res_s.count != 0)
			{
				free (match_res_s.surf1_index);
				free (match_res_s.surf2_index);
			}

			match_res_s.count = 0;

			sigmo = seg_new;

			result.computeparam_result.push_back(compute_result);

			return (sigmo);

		}

		double Compute_Parameters_MWeight(TRANSFORM_RESULT &result, vector<P_Suf1> &p_sp1, TIN_Surf2 *sp2_patch, int option, double threshold, int max_it)
		{
			// option == 3	Solve for XT, YT, ZT
			// option == 4	Solve for XT, YT, ZT, OM
			// option == 5	Solve for XT, YT, ZT, OM, PH
			// option == 6	Solve for XT, YT, ZT, OM, PH, KP
			// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
			// option == 8   Solve for XT, YT, ZT, PH

			COMPUTEPARAM_RESULT compute_result;

			double	sigmo;
			CSMMatrix<double> A;
			CSMMatrix<double> y;
			double  *xh, *C;
			double	**N, **NT, **v, *w;

			double	pi;

			sigmo = 0.0;

			double xt_a  = XT;
			double yt_a  = YT;
			double zt_a  = ZT;
			double s_a   = S;
			double om_a  = om;
			double phi_a = phi;
			double kap_a = kap;
			double p = 1.0/(V_X + V_Y + V_Z);

			pi = 2.0 * asin(1.0);

			N  = dmatrix(0, 6, 0, 6);
			NT = dmatrix(0, 6, 0, 6);
			v  = dmatrix(0, 6, 0, 6);

			xh  =   (double *) malloc (7 * sizeof(double));

			C	=	(double *) malloc (7 * sizeof(double));
			w	=	(double *) malloc (7 * sizeof(double));

			int		no_it;
			double	etpe, seg_new, seg_old, dif;

			seg_old = LARGE;

			int flag_sing = 0;

			double out_liar = 10000000.0;

			int	ii, jj, i, j, n, I, J;

			n = match_res_s.count;

			if (n < 8)
			{
				//There are not enough points to compute the transformation parameters.
				return (-1.0);
			}

			no_it = 0;
			do 
			{
				rotation ();

				etpe = 0.0;

				for (i = 0; i < 7; i++)
				{
					C[i] = 0.0;
					for (j = 0; j < 7; j++)
						N[i][j] = 0.0;
				}

				n = match_res_s.count;

				for (i = 0; i < n; i++)
				{
					A.Resize(3,7,0.0);

					y.Resize(3,1,0.0);

					I = match_res_s.surf1_index[i];
					J = match_res_s.surf2_index[i];

					xa = p_sp1[I].x;
					ya = p_sp1[I].y;
					za = p_sp1[I].z;

					compute_XYZA();
					compute_XYZa();

					V1[0] = sp2_patch[J].X[0];
					V1[1] = sp2_patch[J].Y[0];
					V1[2] = sp2_patch[J].Z[0];

					V2[0] = sp2_patch[J].X[1];
					V2[1] = sp2_patch[J].Y[1];
					V2[2] = sp2_patch[J].Z[1];

					V3[0] = sp2_patch[J].X[2];
					V3[1] = sp2_patch[J].Y[2];
					V3[2] = sp2_patch[J].Z[2];

					y(0,0)		= -XA + V1[0];
					y(1,0)		= -YA + V1[1];
					y(2,0)		= -ZA + V1[2];

					//Partial Derivative w.r.t. XT
					A(0,0)	=  1.0;

					//Partial Derivative w.r.t. YT
					A(1,1)	=  1.0;

					//Partial Derivative w.r.t. ZT
					A(2,2)	=  1.0;

					//Partial Derivative w.r.t. Scale Factor
					A(0,3)	= Xa;
					A(1,3)	= Ya;
					A(2,3)	= Za;

					//Partial Derivative w.r.t. Omega
					CSMMatrix<double> Rmat_dO;
					Partial_dRdO(om, phi, kap, Rmat_dO);
					A(0,4)	= S * (Rmat_dO(0,0) * xa + Rmat_dO(0,1) * ya + Rmat_dO(0,2) * za);
					A(1,4)	= S * (Rmat_dO(1,0) * xa + Rmat_dO(1,1) * ya + Rmat_dO(1,2) * za);
					A(2,4)	= S * (Rmat_dO(2,0) * xa + Rmat_dO(2,1) * ya + Rmat_dO(2,2) * za);

					//Partial Derivative w.r.t. Phi
					CSMMatrix<double> Rmat_dP;
					Partial_dRdP(om, phi, kap, Rmat_dP);
					A(0,5)	= S * (Rmat_dP(0,0) * xa + Rmat_dP(0,1) * ya + Rmat_dP(0,2) * za);
					A(1,5)	= S * (Rmat_dP(1,0) * xa + Rmat_dP(1,1) * ya + Rmat_dP(1,2) * za);
					A(2,5)	= S * (Rmat_dP(2,0) * xa + Rmat_dP(2,1) * ya + Rmat_dP(2,2) * za);

					//Partial Derivative w.r.t. Kappa
					CSMMatrix<double> Rmat_dK;
					Partial_dRdK(om, phi, kap, Rmat_dK);
					A(0,6)	= S * (Rmat_dK(0,0) * xa + Rmat_dK(0,1) * ya + Rmat_dK(0,2) * za);
					A(1,6)	= S * (Rmat_dK(1,0) * xa + Rmat_dK(1,1) * ya + Rmat_dK(1,2) * za);
					A(2,6)	= S * (Rmat_dK(2,0) * xa + Rmat_dK(2,1) * ya + Rmat_dK(2,2) * za);

					CSMMatrix<double> Patch_R(3,3,0.0);
					GetPlaneRotationMatrix(V1, V2, V3, Patch_R);
					Patch_R = Patch_R.Transpose();

					CSMMatrix<double> Puvw(3, 3, 0.0);
					CSMMatrix<double> Pxyz(3, 3, 0.0);

					Puvw(2, 2) = p;

					Pxyz = (Patch_R%Puvw) % Patch_R.Transpose();

					etpe += (y.Transpose() % Pxyz % y)(0,0);

					double temp = (y.Transpose() % y)(0,0);
					CSMMatrix<double> tempmat = y.Transpose() % Pxyz;

					CSMMatrix<double> ci = A.Transpose() % Pxyz % y;
					CSMMatrix<double> ni = A.Transpose() % Pxyz % A;

					for (ii = 0; ii < 7; ii++)
						C[ii] += ci(ii, 0);

					for (ii = 0; ii < 7; ii++)
						for (jj = 0; jj < 7; jj++)
							N[ii][jj] += ni(ii, jj);
				}


				if (option == 6)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;
				}

				if (option == 5)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;

					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 6)
							N[6][jj] = 0.0;
				}

				if (option == 4)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;

					C[5] = 0.0;
					N[5][5] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 5)
							N[ii][5] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 5)
							N[5][jj] = 0.0;

					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 6)
							N[6][jj] = 0.0;
				}

				if (option == 3)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;

					C[4] = 0.0;
					N[4][4] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 4)
							N[ii][4] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 4)
							N[4][jj] = 0.0;

					C[5] = 0.0;
					N[5][5] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 5)
							N[ii][5] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 5)
							N[5][jj] = 0.0;

					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 6)
							N[6][jj] = 0.0;
				}

				if (option == 8)
				{
					// option == 3	Solve for XT, YT, ZT
					// option == 4	Solve for XT, YT, ZT, OM
					// option == 5	Solve for XT, YT, ZT, OM, PH
					// option == 6	Solve for XT, YT, ZT, OM, PH, KP
					// option == 7   Solve for XT, YT, ZT, SF, OM, PH, KP
					// option == 8   Solve for XT, YT, ZT, PH
					C[3] = 0.0;
					N[3][3] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 3)
							N[ii][3] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 3)
							N[3][jj] = 0.0;

					C[4] = 0.0;
					N[4][4] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 4)
							N[ii][4] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 4)
							N[4][jj] = 0.0;

					C[6] = 0.0;
					N[6][6] = 1.0;
					for (ii = 0; ii < 7; ii++)
						if (ii != 6)
							N[ii][6] = 0.0;

					for (jj = 0; jj < 7; jj++)
						if (jj != 6)
							N[6][jj] = 0.0;
				}

				//SVD
				dsvdcmp ( N , 7, 7, w, v);

				//AH Adjustment SVDcmp

				//*
				for ( ii = 0; ii < 7; ii++)
				{
					if (w[ii] < 0.00000001)
						flag_sing = 1;

					compute_result.w[ii] = w[ii];
				}

				if (flag_sing == 1)
					break;

				for ( jj = 0 ; jj < 7 ; jj++)
					for ( ii = 0 ; ii < 7 ; ii++)
						v[ii][jj] /= sqrt(w[jj]);


				mult_ABT (v , v , N , 7 , 7 , 7);
				mult_AV  (N , C , xh , 7 , 7);

				XT	+= (xh[0]);
				YT	+= (xh[1]);
				ZT  += (xh[2]);

				S	+= (xh[3]);

				om	+= (xh[4]);
				phi	+= (xh[5]);
				kap	+= (xh[6]);

				seg_new = etpe / ((double) n - 7.0);
				dif = seg_new - seg_old;
				seg_old = seg_new;		

				/*
				fprintf (logfile, "Iteration # %5d\n", no_it);
				fprintf (logfile, "Estimated Variance Component = %lf\n", sqrt(seg_new));

				fprintf (logfile, "XT	  = %14.4lf   (%9.6lf) (m)\n", XT, sqrt(N[0][0] * seg_new));
				fprintf (logfile, "YT     = %14.4lf   (%9.6lf) (m)\n", YT, sqrt(N[1][1] * seg_new));
				fprintf (logfile, "ZT     = %14.4lf   (%9.6lf) (m)\n", ZT, sqrt(N[2][2] * seg_new));

				fprintf (logfile, "S      = %14.4lf   (%9.6lf) (m)\n",  S, sqrt(N[3][3] * seg_new));

				fprintf (logfile, "omega  = %14.4lf   (%9.6lf) (deg)\n", om  * 180.0 / pi, sqrt(N[4][4] * seg_new));
				fprintf (logfile, "phi    = %14.4lf   (%9.6lf) (deg)\n", phi * 180.0 / pi, sqrt(N[5][5] * seg_new));
				fprintf (logfile, "kapa   = %14.4lf   (%9.6lf) (deg)\n", kap * 180.0 / pi, sqrt(N[6][6] * seg_new));

				fprintf (logfile, "\n\n");
				*/

				/*
				//Correlation matrix
				fprintf (logfile, "Corelation matrix \r\n");
				CSMMatrix<double> Correlation(7,7,0);

				for ( jj = 0 ; jj < 7 ; jj++)
				{
					for ( ii = 0 ; ii < 7 ; ii++)
					{
						Correlation(jj, ii) = N[jj][ii]/sqrt(N[jj][jj])/sqrt(N[ii][ii]);
						fprintf(logfile, "%.5lf\t", Correlation(jj, ii));
					}

					fprintf(logfile, "\r\n");
				}

				fprintf (logfile, "\n\n");

				*/
				//Correlation matrix End

				no_it++;

			} while ((no_it < max_it) && (fabs(dif) > threshold));

			if (flag_sing == 1)
			{
				XT  = xt_a;
				YT  = yt_a;
				ZT  = zt_a;
				S   = s_a;
				om  = om_a;
				phi = phi_a;
				kap = kap_a;

				compute_result.no_iteration = no_it;
				compute_result.posteriori_sigma = sqrt(seg_new);

				compute_result.XT = XT;
				compute_result.YT = YT;
				compute_result.ZT = ZT;

				compute_result.S = S;

				compute_result.om = om;
				compute_result.phi = phi;
				compute_result.kap = kap;

				//Correlation matrix
				/*
				CSMMatrix<double> Correlation(7,7,0);

				for ( jj = 0 ; jj < 7 ; jj++)
				{
					for ( ii = 0 ; ii < 7 ; ii++)
					{
						Correlation(jj, ii) = N[jj][ii]/sqrt(N[jj][jj])/sqrt(N[ii][ii]);
					}
				}
				*/
			}
			else
			{
				compute_result.no_iteration = no_it;
				compute_result.posteriori_sigma = sqrt(seg_new);

				compute_result.XT = XT;
				compute_result.YT = YT;
				compute_result.ZT = ZT;

				compute_result.S = S;

				compute_result.om = om;
				compute_result.phi = phi;
				compute_result.kap = kap;

				compute_result.S_XT = sqrt(N[0][0] * seg_new);
				compute_result.S_YT = sqrt(N[1][1] * seg_new);
				compute_result.S_ZT = sqrt(N[2][2] * seg_new);

				compute_result.S_S = sqrt(N[3][3] * seg_new);

				compute_result.S_om = sqrt(N[4][4] * seg_new);
				compute_result.S_phi = sqrt(N[5][5] * seg_new);
				compute_result.S_kap = sqrt(N[6][6] * seg_new);
			}

			double NDIST;

			NDIST = 0.0;

			for (i = 0; i < n; i++)
			{
				I = match_res_s.surf1_index[i];
				J = match_res_s.surf2_index[i];

				xa = p_sp1[I].x;
				ya = p_sp1[I].y;
				za = p_sp1[I].z;

				//xa -= DX;
				//ya -= DY;

				compute_XYZA();

				V1[0] = sp2_patch[J].X[0];
				V1[1] = sp2_patch[J].Y[0];
				V1[2] = sp2_patch[J].Z[0];

				//V1[0] -= DX;
				//V1[1] -= DY;

				V2[0] = sp2_patch[J].X[1];
				V2[1] = sp2_patch[J].Y[1];
				V2[2] = sp2_patch[J].Z[1];

				//V2[0] -= DX;
				//V2[1] -= DY;

				V3[0] = sp2_patch[J].X[2];
				V3[1] = sp2_patch[J].Y[2];
				V3[2] = sp2_patch[J].Z[2];

				double checkA = 0.0;
				double checkB = 0.0;

				Compute_In_Out_NTH (&checkA, &checkB);

				NDIST += (checkA * checkA);
			}

			NDIST = sqrt (NDIST/(double)n);

			compute_result.Ave_ND = NDIST;

			free_dmatrix(N , 0, 6, 0, 6);
			free_dmatrix(NT, 0, 6, 0, 6);
			free_dmatrix(v , 0, 6, 0, 6);

			free (C);
			free (w);
			free (xh);

			if (match_res_s.count != 0)
			{
				free (match_res_s.surf1_index);
				free (match_res_s.surf2_index);
			}

			match_res_s.count = 0;

			sigmo = seg_new;

			result.computeparam_result.push_back(compute_result);

			return (sigmo);
		}

		int find_min_max (TIN_Surf2 *sp2_patch, int i)
		{
			double min_x, min_y, max_x, max_y;
			int	j;

			min_x = min_y =  1.0e+12;
			max_x = max_y = -1.0e-12;

			for (j = 0; j < 3; j++)
			{
				if (sp2_patch[i].X[j] < min_x)
					min_x = sp2_patch[i].X[j];

				if (sp2_patch[i].Y[j] < min_y)
					min_y = sp2_patch[i].Y[j];

				if (sp2_patch[i].X[j] > max_x)
					max_x = sp2_patch[i].X[j];

				if (sp2_patch[i].Y[j] > max_y)
					max_y = sp2_patch[i].Y[j];
			}

			sp2_patch[i].min_x = min_x;
			sp2_patch[i].min_y = min_y;
			sp2_patch[i].max_x = max_x;
			sp2_patch[i].max_y = max_y;

			return (1);
		}

		void ReadXYZ(char* fname, vector<P_Suf1> &points, double Centroid[3], bool bID)
		{
			char line[STRING_LEN];
			double sum_x=0, sum_y=0, sum_z=0;
			char ID[STRING_LEN];

			fstream in;
			in.open(fname, ios::in);
			
			if(!in) return;

			points.empty();

			while(!in.eof())
			{
				in.getline(line, STRING_LEN);

				P_Suf1 val;

				if(bID == true)
					sscanf (line, "%s %lf %lf %lf", ID, &val.x, &val.y, &val.z);
				else
					sscanf (line, "%lf %lf %lf", &val.x, &val.y, &val.z);

				points.push_back(val);

				sum_x += val.x;
				sum_y += val.y;
				sum_z += val.z;
			}
			unsigned int num = points.size();

			if(num == 0) return;

			Centroid[0] = sum_x/num;
			Centroid[1] = sum_y/num;
			Centroid[2] = sum_z/num;

			in.close();
		}

		void TransformFunc(TransParam param, double Centroid[3], double *P, double *res)
		{
			P[0] = P[0] - Centroid[0];
			P[1] = P[1] - Centroid[1];
			P[2] = P[2] - Centroid[2];

			_Rmat_ R(param.om, param.phi, param.kap);
			res[0] = param.S*(R.Rmatrix(0,0)*P[0] + R.Rmatrix(0,1)*P[1] + R.Rmatrix(0,2)*P[2]) + param.XT + Centroid[0];
			res[1] = param.S*(R.Rmatrix(1,0)*P[0] + R.Rmatrix(1,1)*P[1] + R.Rmatrix(1,2)*P[2]) + param.YT + Centroid[1];
			res[2] = param.S*(R.Rmatrix(2,0)*P[0] + R.Rmatrix(2,1)*P[1] + R.Rmatrix(2,2)*P[2]) + param.ZT + Centroid[2];
		}

		bool TransformFunc_Inv(TransParam param, double Centroid[3], double *Q, double *res)
		{
			if(param.S == 0.) false;

			_Rmat_ R(param.om, param.phi, param.kap);
			CSMMatrix<double> R_T = R.Rmatrix.Transpose();
			double Q2[3];
			Q2[0] = Q[0] - param.XT - Centroid[0];
			Q2[1] = Q[1] - param.YT - Centroid[1];
			Q2[2] = Q[2] - param.ZT - Centroid[2];

			res[0] = 1.0/param.S*(R_T(0,0)*Q2[0] + R_T(0,1)*Q2[1] + R_T(0,2)*Q2[2]) + Centroid[0];
			res[1] = 1.0/param.S*(R_T(1,0)*Q2[0] + R_T(1,1)*Q2[1] + R_T(1,2)*Q2[2]) + Centroid[1];
			res[2] = 1.0/param.S*(R_T(2,0)*Q2[0] + R_T(2,1)*Q2[1] + R_T(2,2)*Q2[2]) + Centroid[2];

			return true;
		}

	protected:
		int rotation(void)
		{
			double  c1,s1,c2,s2,c3,s3;

			c1 = cos(om);
			s1 = sin(om);
			c2 = cos(phi);
			s2 = sin(phi);
			c3 = cos(kap);
			s3 = sin(kap);

			R[0][0] =  c2*c3;
			R[0][1] = -c2*s3;
			R[0][2] =  s2;
			R[1][0] =  c1*s3+s1*s2*c3;
			R[1][1] =  c1*c3-s1*s2*s3;
			R[1][2] = -s1*c2;
			R[2][0] =  s1*s3-c1*s2*c3;
			R[2][1] =  s1*c3+c1*s2*s3;
			R[2][2] =  c1*c2;

			return (1);
		}

		int	compute_XYZA(void)
		{
			XA = XT + S * (R[0][0] * xa + R[0][1] * ya + R[0][2] * za);
			YA = YT + S * (R[1][0] * xa + R[1][1] * ya + R[1][2] * za);
			ZA = ZT + S * (R[2][0] * xa + R[2][1] * ya + R[2][2] * za);

			return (1);
		}

		int Compute_In_Out_NTH (double *Normal_distance, double *th)
		{

			double V2x, V2y, V2z;
			double V3x, V3y, V3z;

			double Dx, Dy, Dz;

			V2x = V2[0] - V1[0];
			V2y = V2[1] - V1[1];
			V2z = V2[2] - V1[2];

			V3x = V3[0] - V1[0];
			V3y = V3[1] - V1[1];
			V3z = V3[2] - V1[2];

			Dx = sqrt (V2x * V2x + V2y * V2y + V2z * V2z); 

			//X axis
			double XX1 = V2x / Dx;
			double XX2 = V2y / Dx;
			double XX3 = V2z / Dx;

			//Z axis
			double ZZ1 = V2y * V3z - V2z * V3y;
			double ZZ2 = V2z * V3x - V2x * V3z;
			double ZZ3 = V2x * V3y - V2y * V3x;

			Dz = sqrt (ZZ1 * ZZ1 + ZZ2 * ZZ2 + ZZ3 * ZZ3); 

			ZZ1 = ZZ1 / Dz;
			ZZ2 = ZZ2 / Dz;
			ZZ3 = ZZ3 / Dz;

			//Y axis

			double YY1 = ZZ2 * XX3 - ZZ3 * XX2;
			double YY2 = ZZ3 * XX1 - ZZ1 * XX3;
			double YY3 = ZZ1 * XX2 - ZZ2 * XX1;

			Dy = sqrt (YY1 * YY1 + YY2 * YY2 + YY3 * YY3); 

			YY1 = YY1 / Dy;
			YY2 = YY2 / Dy;
			YY3 = YY3 / Dy;

			double X1, Y1, Z1, X2, Y2, Z2, X3, Y3, Z3, X, Y, Z;

			X1 = Y1 = Z1 = 0.0;

			X2 = XX1 * V2x + XX2 * V2y + XX3 * V2z;
			Y2 = YY1 * V2x + YY2 * V2y + YY3 * V2z;
			Z2 = ZZ1 * V2x + ZZ2 * V2y + ZZ3 * V2z;

			X3 = XX1 * V3x + XX2 * V3y + XX3 * V3z;
			Y3 = YY1 * V3x + YY2 * V3y + YY3 * V3z;
			Z3 = ZZ1 * V3x + ZZ2 * V3y + ZZ3 * V3z;

			X  = XX1 * (XA - V1[0]) + XX2 * (YA - V1[1]) + XX3 * (ZA - V1[2]);
			Y  = YY1 * (XA - V1[0]) + YY2 * (YA - V1[1]) + YY3 * (ZA - V1[2]);
			Z  = ZZ1 * (XA - V1[0]) + ZZ2 * (YA - V1[1]) + ZZ3 * (ZA - V1[2]);

			*Normal_distance = fabs(Z);

			if ((Y < 0.0) || (Y > Y3))
				*th = 0.0;
			else
			{
				double xi_13 = Y * X3 / Y3;
				double xi_23 = X3 - (X3 - X2) * (Y3 - Y) / (Y3 - Y2);

				if ((X > xi_13) && (X < xi_23))
					*th = 360.0;
				else
					*th = 0.0;
			}

			return (1);
		}

		int	compute_XYZa(void)
		{
			Xa = (R[0][0] * xa + R[0][1] * ya + R[0][2] * za);
			Ya = (R[1][0] * xa + R[1][1] * ya + R[1][2] * za);
			Za = (R[2][0] * xa + R[2][1] * ya + R[2][2] * za);

			return (1);
		}

		int compute_det (void)
		{  

			detx = V1[1] * (V2[2] - V3[2]) - V1[2] * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1]);
			dety = V1[0] * (V2[2] - V3[2]) - V1[2] * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0]);
			detz = V1[0] * (V2[1] - V3[1]) - V1[1] * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0]);

			det  = V1[0] * (V2[1] * V3[2] - V2[2] * V3[1]) - V1[1] * (V2[0] * V3[2] - V2[2] * V3[0]) + V1[2] * (V2[0] * V3[1] - V2[1] * V3[0]);

			return (1);
		}

		int compute_BZ(void)
		{

			BZ1  =  (V1[1] * (V2[2] - V3[2]) - V1[2] * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1])) * S * R[0][2];
			BZ1 += -(V1[0] * (V2[2] - V3[2]) - V1[2] * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0])) * S * R[1][2];
			BZ1 +=  (V1[0] * (V2[1] - V3[1]) - V1[1] * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0])) * S * R[2][2];
			BZA  = -(XA * (V2[1] - V3[1]) - YA * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0]));
			BZB  =  (XA * (V1[1] - V3[1]) - YA * (V1[0] - V3[0]) + (V1[0] * V3[1] - V1[1] * V3[0]));
			BZC  = -(XA * (V1[1] - V2[1]) - YA * (V1[0] - V2[0]) + (V1[0] * V2[1] - V1[1] * V2[0]));

			return (1);
		}

		int compute_BY(void)
		{

			BY1  =  (V1[1] * (V2[2] - V3[2]) - V1[2] * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1])) * S * R[0][1];
			BY1 += -(V1[0] * (V2[2] - V3[2]) - V1[2] * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0])) * S * R[1][1];
			BY1 +=  (V1[0] * (V2[1] - V3[1]) - V1[1] * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0])) * S * R[2][1];

			BYA  =  (XA * (V2[2] - V3[2]) - ZA * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0]));
			BYB  = -(XA * (V1[2] - V3[2]) - ZA * (V1[0] - V3[0]) + (V1[0] * V3[2] - V1[2] * V3[0]));
			BYC  =  (XA * (V1[2] - V2[2]) - ZA * (V1[0] - V2[0]) + (V1[0] * V2[2] - V1[2] * V2[0]));

			return (1);
		}

		int compute_BX(void)
		{

			BX1  =  (V1[1] * (V2[2] - V3[2]) - V1[2] * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1])) * S * R[0][0];
			BX1 += -(V1[0] * (V2[2] - V3[2]) - V1[2] * (V2[0] - V3[0]) + (V2[0] * V3[2] - V2[2] * V3[0])) * S * R[1][0];
			BX1 +=  (V1[0] * (V2[1] - V3[1]) - V1[1] * (V2[0] - V3[0]) + (V2[0] * V3[1] - V2[1] * V3[0])) * S * R[2][0];

			BXA  = -(YA * (V2[2] - V3[2]) - ZA * (V2[1] - V3[1]) + (V2[1] * V3[2] - V2[2] * V3[1]));
			BXB  =  (YA * (V1[2] - V3[2]) - ZA * (V1[1] - V3[1]) + (V1[1] * V3[2] - V1[2] * V3[1]));
			BXC  = -(YA * (V1[2] - V2[2]) - ZA * (V1[1] - V2[1]) + (V1[1] * V2[2] - V1[2] * V2[1]));

			return (1);
		}

		void GetPlaneRotationMatrix(double* V1, double* V2, double* V3, CSMMatrix<double> &R)
		{

			double V2x, V2y, V2z;
			double V3x, V3y, V3z;

			double Dx, Dy, Dz;

			V2x = V2[0] - V1[0];
			V2y = V2[1] - V1[1];
			V2z = V2[2] - V1[2];

			V3x = V3[0] - V1[0];
			V3y = V3[1] - V1[1];
			V3z = V3[2] - V1[2];

			Dx = sqrt (V2x * V2x + V2y * V2y + V2z * V2z); 

			//X axis
			double XX1 = V2x / Dx;
			double XX2 = V2y / Dx;
			double XX3 = V2z / Dx;

			//Z axis
			double ZZ1 = V2y * V3z - V2z * V3y;
			double ZZ2 = V2z * V3x - V2x * V3z;
			double ZZ3 = V2x * V3y - V2y * V3x;

			Dz = sqrt (ZZ1 * ZZ1 + ZZ2 * ZZ2 + ZZ3 * ZZ3); 

			ZZ1 = ZZ1 / Dz;
			ZZ2 = ZZ2 / Dz;
			ZZ3 = ZZ3 / Dz;

			//Y axis

			double YY1 = ZZ2 * XX3 - ZZ3 * XX2;
			double YY2 = ZZ3 * XX1 - ZZ1 * XX3;
			double YY3 = ZZ1 * XX2 - ZZ2 * XX1;

			Dy = sqrt (YY1 * YY1 + YY2 * YY2 + YY3 * YY3); 

			YY1 = YY1 / Dy;
			YY2 = YY2 / Dy;
			YY3 = YY3 / Dy;

			R(0,0) = XX1; R(0,1) = XX2; R(0,2) = XX3;
			R(1,0) = YY1; R(1,1) = YY2; R(1,2) = YY3;
			R(2,0) = ZZ1; R(2,1) = ZZ2; R(2,2) = ZZ3;
		}

	protected:
		double	Angle_Threshold, Distance_Threshold;
		double	XT, YT, ZT, S, om, phi, kap;

		//Variances of input data (surface 1 and 2)
		double	V_X, V_Y, V_Z;

		double	delta_x, delta_y, delta_z;

		double	DX, DY, DZ;

		int		ITX, ITY, ITZ, ITS, ITO, ITP, ITK;

		double	xa, ya, za;
		double	XA, YA, ZA;
		double	Xa, Ya, Za;

		double	V1[3], V2[3], V3[3];

		MATCH_RES_S match_res_s;

		double detx, dety, detz, det;

		double **R;

		double	BZ1, BZA, BZB, BZC;
		double	BX1, BXA, BXB, BXC;
		double	BY1, BYA, BYB, BYC;
	};

	void Transformfunc(FILE *out_transformed, double* sp_x, double* sp_y, double* sp_z, unsigned int no_of_p, const double XT, const double YT, const double ZT, const double S, const double om, const double ph, const double kap, const double Centerx, const double Centery, const double Centerz);
}