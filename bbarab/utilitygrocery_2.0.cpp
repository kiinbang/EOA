/*
* Copyright (c) 2005-2006, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>

#include <ssm/include/SSMMatrix.h>
#include <utilitygrocery_2.0.h>

using namespace std;

namespace util
{
	/**GetPI
	* Description	    : to get pi(3.14159265....)
	*@param void
	*@return double : PI
	*/
	double GetPI(void)
	{
		return asin(1.0) * 2;
	}

	/**Rad2Deg
	* Description	    : to convert the degree to the radian
	*@param double rad
	*@return double
	*/
	double Rad2Deg(double rad)
	{
		double deg;
		deg = rad / GetPI()*180.0;
		if (deg>180.0) deg = deg - 360.0;
		else if (deg<-180.0) deg = deg + 360.0;
		return deg;
	}

	/**Deg2Rad
	* Description	    : to convert the radian to the degree
	*@param double deg
	*@return double
	*/
	double Deg2Rad(double deg)
	{
		double rad;
		rad = deg / 180.0*GetPI();
		if (rad>GetPI()) rad = rad - 2 * GetPI();
		else if (rad<-GetPI()) rad = rad + 2 * GetPI();
		return rad;
	}

	/**RemoveCommentLine
	* Description	    : function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@return void
	*/
	void RemoveCommentLine(fstream &file, char delimiter)
	{
		char comment = delimiter;

		char line[MAX_LINE_LENGTH];

		int bstop = false;
		do
		{
			file >> ws;

			if (file.peek() == comment)	//comment line
			{
				file.getline(line, MAX_LINE_LENGTH);
				bstop = false;
			}
			else
				bstop = true;


			file >> ws;

		} while ((!file.eof()) && (bstop == false));

		file >> ws;
	}

	/**RemoveCommentLine
	* Description	    : function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@param in num: number of delimiters
	*@return void
	*/
	void RemoveCommentLine(fstream &file, char* delimiter, int num)
	{
		char line[MAX_LINE_LENGTH];

		int bstop = true;
		do
		{
			bstop = true;

			file >> ws;
			char target = file.peek();

			for (int i = 0; i<num; i++)
			{
				char comment = delimiter[i];
				if (target == comment)	//comment line
				{
					file.getline(line, MAX_LINE_LENGTH);
					bstop = false;
				}
			}

			file >> ws;

		} while ((!file.eof()) && (bstop == false));

		file >> ws;
	}

	/**RemoveCommentLine
	* Description	    : function for removing comments in the input file
	*@param FILE* file : FILE struct pointer
	*@param char delimiter: delimiter
	*@return void
	*/
	void RemoveCommentLine(FILE* file, char delimiter)
	{
		char comment = delimiter;
		char line[MAX_LINE_LENGTH];

		bool bstop = false;
		char temp;
		do
		{
			fscanf_s(file, "%c", &temp, 1);
			if (temp == comment)//comment line
			{
				fgets(line, MAX_LINE_LENGTH, file);
				bstop = false;
			}
			//back space, tap, line feed, carriage return, Space
			else if ((8 == temp) || (9 == temp) || (10 == temp) || (13 == temp) || (32 == temp))
				bstop = false;
			else if (temp == EOF)
				bstop = true;
			else
			{
				fseek(file, (long)-1, SEEK_CUR);
				bstop = true;
			}

		} while (bstop == false);
	}

	/**FindNumber
	* Description	    : function for removing delimiter in the input file
	*@param fstream file : file stream
	*@return void
	*/
	void FindNumber(fstream &file)
	{
		char temp;
		int bstop = false;
		do
		{
			//48~57: number
			//file >> ws;
			file >> temp;
			if ((48 <= temp) && (temp <= 57))
			{
				file.seekg(-1, ios::cur);
				bstop = true;
			}
			//+ or - symbol
			else if ((45 == temp) || (43 == temp))
			{
				file >> temp;
				//48~57: number
				if ((48 <= temp) && (temp <= 57))
				{
					file.seekg(-2, ios::cur);
					bstop = true;
				}
			}
			else
				bstop = false;

		} while ((!file.eof()) && (bstop == false));
	}

	/**FindString
	* Description	    : function for finding given string in the input file
	*@param fstream file : file stream
	*@param char* target : target string
	*@return void
	*/
	bool FindString(fstream &fin, const char target[], int &result)
	{
		char string[MAX_LINE_LENGTH];
		if (fin.getline(string, MAX_LINE_LENGTH))
		{
			char *pdest = NULL;
			pdest = strstr(string, target);
			result = static_cast<int>(pdest - string + 1);
			if (pdest != NULL)
				return true;
			else
				return false;
		}
		else
			return false;


	}

	/**FindString
	* Description	    : checking version of the given text file
	*@param fstream file : file stream
	*@return double: version number
	*/
	float VersionCheck(fstream &fin)
	{
		//version check
		int pos;

		float ver_num = 1.0;

		bool bVersion = FindString(fin, "VER", pos);

		if (bVersion == true)
		{
			//To move to begin of file
			fin.seekg(0, ios::beg);
			char temp[256];//"VER"
			fin >> temp >> ver_num;//version
		}
		else
		{
			//To move to begin of file
			fin.seekg(0, ios::beg);//old version without version number(before ver 1.1)
		}

		return ver_num;
	}

	/**CalAzimuth
	* Description	    : calculate azimuth with 2 points
	*@param double Xa	: X coordinate
	*@param double Ya	: Y coordinate
	*@param double Xb	: X coordinate
	*@param double Yb	: Y coordinate
	*@return double: azimuth
	*/
	double CalAzimuth(double Xa, double Ya, double Xb, double Yb)
	{
		double Azi;

		double dX = Xb - Xa;
		double dY = Yb - Ya;

		Azi = atan2(dX, dY);

		return Azi;
	}

	/**MeridianConversion
	* Description	    : calculate meridian conversion
	*@param double Lon	: longitude
	*@param double Lat	: latitude
	*@param double CenterLon	: longitude of center meridian
	*@return double: meridian conversion
	*/
	double MeridianConversion(double Lon, double Lat, double CenterLon)
	{
		double MC;
		MC = (CenterLon - Lon)*sin(Lat);
		return MC;
	}

	/**CentralMeridian
	* Description	    : central meridian
	*@param int zone	: UTM zone
	*@return double: central meridian (rad)
	*/
	double CentralMeridian(int zone)
	{
		double centmeri;
		if (zone <= 30)
			centmeri = (double)zone*6. + 180. - 3.;
		else
			centmeri = (double)zone*6. - 180. - 3.;

		return Deg2Rad(centmeri);
	}

	/**Line2DIntersect
	* Description	    :  find intersect point between two lines
	*@param double x1, y1 : given point of line
	*@param double x2, y2 : given point of line
	*@param double x3, y3 : given point of line
	*@param double x4, y4 : given point of line
	*@param double &x, &y : intersection point
	*@return int : -3 (parallel) -2(same) -1 (Not line) 0 (out of ranges) 1(intersection within ranges) 2 (end point)
	*/
	int Line2DIntersect(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double &x, double &y)
	{
		//1 = ax + by

		double a1 = 0, b1 = 0, a2 = 0, b2 = 0;

		double dx12 = x2 - x1;
		double dy12 = y2 - y1;
		double dx34 = x4 - x3;
		double dy34 = y4 - y3;

		//
		//Specific lines (No intersection)
		/////////////////////////////////////

		//-3 (parallel) -2(same) -1 (Not line) 

		if ((dx12 == 0) && (dy12 == 0))
			return -1;//Not line

		if ((dx34 == 0) && (dy34 == 0))
			return -1;//Not line

		if ((dx12 == 0) && (dx34 == 0))//(parallel to y axis)
		{
			if (x1 == x3)
				return -2;//same lines
			else
				return -3;//parallel lines
		}

		if ((dy12 == 0) && (dy34 == 0))//(parallel to x axis)
		{
			if (y1 == y3)
				return -2;//same lines
			else
				return -3;//parallel lines
		}

		//
		//Specific lines (intersection)
		/////////////////////////////////////

		if ((dx12 == 0) && (dy34 == 0))
		{
			x = x1; y = y2;
		}

		if ((dy12 == 0) && (dx34 == 0))
		{
			x = x3; y = y1;
		}

		if ((dx12 != 0) && (dy12 != 0))
		{
			a1 = dy12 / dx12;
			b1 = y1 - a1*x1;

			if (dx34 == 0)
			{
				x = x3;
				y = a1*x3 + b1;
			}
			else if (dy34 == 0)
			{
				y = y3;
				x = (y3 - b1) / a1;
			}
		}

		if ((dx34 != 0) && (dy34 != 0))
		{
			a2 = dy34 / dx34;
			b2 = y3 - a2*x3;

			if (dx12 == 0)
			{
				x = x1;
				y = a2*x1 + b2;
			}
			else if (dy12 == 0)
			{
				y = y1;
				x = (y1 - b2) / a2;
			}
		}

		//
		//Normal lines (intersection)
		/////////////////////////////////////

		//-3 (parallel) -2(same) -1 (Not line)

		if ((dx12 != 0) && (dy12 != 0) && (dx34 != 0) && (dy34 != 0))
		{
			if (a1 == a2)
			{
				if (b1 == b2)
					return -2;//same lines
				else
					return -3;//parallel lines
			}

			x = (b2 - b1) / (a1 - a2);
			y = a1*x + b1;
		}

		//
		//Location of the intersection point
		/////////////////////////////////////
		double dx1, dx2, dx3, dx4;
		double dy1, dy2, dy3, dy4;

		bool on_line_1 = false;
		bool on_line_2 = false;
		bool end_point = false;

		dx1 = x1 - x;
		dx2 = x2 - x;
		dx3 = x3 - x;
		dx4 = x4 - x;

		dy1 = y1 - y;
		dy2 = y2 - y;
		dy3 = y3 - y;
		dy4 = y4 - y;

		if (dx1*dx2 <= 0 && dy1*dy2 <= 0)
			on_line_1 = true;

		if (dx3*dx4 <= 0 && dy3*dy4 <= 0)
			on_line_2 = true;

		if ((dx1 == 0 && dy1 == 0) || (dx2 == 0 && dy2 == 0) || (dx3 == 0 && dy3 == 0) || (dx4 == 0 && dy4 == 0))
			end_point = true;

		//0 (out of ranges) 
		//1 (intersection within ranges)
		//2 (end point)

		if (on_line_1 == true && on_line_2 == true)
		{
			if (end_point == true)
				return 2;
			else
				return 1;
		}
		else
			return 0;

	};

	/**Inside2DPolygon
	* Description	    :  topology between line and point
	*@param double *x, *y	: vertices
	*@param double n		  : number of vertices
	*@param double Tx, Ty	: target point
	*@return int	: -1 (not polygon) 0 (outside) 1 (inside)
	*/
	int Inside2DPolygon(double *x, double* y, int n, double Tx, double Ty)
	{
		if (n < 3) return -1;

		int i;
		int count = 0;

		double min_x, same_y;
		same_y = Ty;
		min_x = -1.0e20;

		int n_end_point = 0;

		for (i = 0; i<n; i++)
		{
			double x3, y3, x4, y4;

			if (i != n - 1)
			{
				x3 = x[i];
				y3 = y[i];
				x4 = x[i + 1];
				y4 = y[i + 1];
			}
			else
			{
				x3 = x[i];
				y3 = y[i];
				x4 = x[0];
				y4 = y[0];
			}

			double x, y;
			int res = Line2DIntersect(Tx, Ty, min_x, same_y, x3, y3, x4, y4, x, y);
			if (0 < res)
				count++;
			if (2 == res)
				n_end_point++;
		}

		if ((count - n_end_point / 2) % 2 == 0) return 0;
		else return 1;
	}

	bool IntersectPlaneLine(double *N, double *V, double *P1, double *P2, double *P)
	{
		//Normal vector is N, and
		//the plane includes point V

		//Plane's normal vector is N and it includes the point P3
		//Then, its plane equation is N.(P-P3)=0

		//If a line goes through two points, P1 and P2, then its line equation is
		//P=P1+u(P2-P1)

		//Intersection of a line and a plane
		//u = ((P3-P1).N)/((P2-P1).N)
		//Use "u" in a line eqaution, P=P1+u(P2-P1), we can fine the intersection

		//Point3D<double> N(patch.coeff[0], patch.coeff[1], patch.coeff[2]);
		//Point3D<double> P3(patch.Vertex[0], patch.Vertex[1], patch.Vertex[2]);                       

		double VP1[3];
		VP1[0] = V[0] - P1[0];
		VP1[1] = V[1] - P1[1];
		VP1[2] = V[2] - P1[2];

		double P21[3];
		P21[0] = P2[0] - P1[0];
		P21[1] = P2[1] - P1[1];
		P21[2] = P2[2] - P1[2];

		double numerator = VP1[0] * N[0] + VP1[1] * N[1] + VP1[2] * N[2];
		double denominator = P21[0] * N[0] + P21[1] * N[1] + P21[2] * N[2];

		double ZERO = 10e-99;
		if (fabs(denominator) < ZERO) return false;

		double u = numerator / denominator;

		// (P1)---------(P2)-------------(P)
		if (fabs(u) > 1.0) return false; //out of two points

		P[0] = P1[0] + P21[0] * u;
		P[1] = P1[1] + P21[1] * u;
		P[2] = P1[2] + P21[2] * u;

		// (P)----------(P1)-------------(P2)
		double P1P[3];
		P1P[0] = P1[0] - P[0];
		P1P[1] = P1[1] - P[1];
		P1P[2] = P1[2] - P[2];
		double P1P2[3];
		P1P2[0] = P1[0] - P2[0];
		P1P2[1] = P1[1] - P2[1];
		P1P2[2] = P1[2] - P2[2];
		double temp = P1P[0] * P1P2[0] + P1P[1] * P1P2[1] + P1P[2] * P1P2[2];
		if (temp<0.0) return false; //out of two points 

		return true;
	}

	bool IntersectPlaneLine(double A0, double A1, double A2, double *P1, double *P2, double *P)
	{
		//Plane equation: A0X + A1Y + A2Z = 1

		//If a line goes through two points, P1 and P2, then its line equation is
		//P=P1+u(P2-P1)

		//(A0, A1, A2).(P1+u(P2-P1)) = 1
		//u = ((A0, A1, A2).P1)/((A0, A1, A2).(P2-P1))    

		double P21[3];
		P21[0] = P2[0] - P1[0];
		P21[1] = P2[1] - P1[1];
		P21[2] = P2[2] - P1[2];

		double numerator = P1[0] * A0 + P1[1] * A1 + P1[2] * A2;
		double denominator = P21[0] * A0 + P21[1] * A1 + P21[2] * A2;

		double ZERO = 10e-99;
		if (fabs(denominator) < ZERO) return false;

		double u = numerator / denominator;

		// (P1)---------(P2)-------------(P)
		if (fabs(u) > 1.0) return false; //out of two points

		P[0] = P1[0] + P21[0] * u;
		P[1] = P1[1] + P21[1] * u;
		P[2] = P1[2] + P21[2] * u;

		// (P)----------(P1)-------------(P2)
		//dot product calculation
		//vertex P of two sides (vector P1-P and P1-P2) is zero
		//in case (P1)----------(P)-------------(P2)
		//else out of two points
		double P1P[3];
		P1P[0] = P1[0] - P[0];
		P1P[1] = P1[1] - P[1];
		P1P[2] = P1[2] - P[2];
		double P1P2[3];
		P1P2[0] = P1[0] - P2[0];
		P1P2[1] = P1[1] - P2[1];
		P1P2[2] = P1[2] - P2[2];
		double temp = P1P[0] * P1P2[0] + P1P[1] * P1P2[1] + P1P[2] * P1P2[2];
		if (temp < 0.0) return false; //out of two points 

		return true;
	}

	bool IntersectPointBetweenOnePointandOneLine(double *P0, double *Pa, double *Pb, double *P)
	{
		double Pba[3];
		double P0a[3];

		//Shift
		Pba[0] = Pb[0] - Pa[0];
		Pba[1] = Pb[1] - Pa[1];
		Pba[2] = Pb[2] - Pa[2];

		P0a[0] = P0[0] - Pa[0];
		P0a[1] = P0[1] - Pa[1];
		P0a[2] = P0[2] - Pa[2];

		double numerator = Pba[0] * P0a[0] + Pba[1] * P0a[1] + Pba[2] * P0a[2];
		double denominator = Pba[0] * Pba[0] + Pba[1] * Pba[1] + Pba[2] * Pba[2];

		if (fabs(denominator) < 1.0e-20)
			return false;

		double n = numerator / denominator;

		P[0] = Pa[0] + n*Pba[0];
		P[1] = Pa[1] + n*Pba[1];
		P[2] = Pa[2] + n*Pba[2];

		return true;
	}

	//return: area of polygon
	double Area_of_Polygon2D(double* x, double* y, int n)
	{
		double area = 0;
		int i;

		for (i = 1; i<n - 1; i++)
		{
			area += x[i] * (y[i + 1] - y[i - 1]);
		}
		area += x[n - 1] * (y[0] - y[n - 2]);
		area += x[0] * (y[1] - y[n - 1]);

		return area / 2.0;
	}

	//Extract angle from normal vector
	bool ExtractEulerAngles(double dX, double dY, double dZ, double &O, double &P)
	{
		if (dX == 0 && dY == 0 && dZ == 0) return false;

		double dYZ = sqrt(dY*dY + dZ*dZ);
		O = -atan2(dY, dZ);
		P = atan2(dX, dYZ);

		return true;
	}

	bool RandomTXTLIneExtraction(int numlines, char* ptspath, char* outpath)
	{
		//
		//File open
		//
		fstream PTSFile;//PTS file 
		PTSFile.open(ptspath, ios::in);

		//
		//Output file
		//
		fstream outfile;
		outfile.open(outpath, ios::out);

		int count = 0;
		char line[512];

		while (!PTSFile.eof())
		{
			int rLine = rand();
			rLine += int(rLine / RAND_MAX*numlines + 0.5);

			//Skip lines
			for (int i = 0; i<rLine; i++)
			{
				if (!PTSFile.eof())
				{
					PTSFile.getline(line, 512);
					PTSFile >> ws;
				}
				else
				{
					break;
				}
			}

			if (!PTSFile.eof())
			{
				PTSFile.getline(line, 512);
				PTSFile >> ws;
				outfile << line << endl;
				outfile.flush();
				count++;
			}
			else
			{
				break;
			}
		}

		outfile.close();

		PTSFile.close();

		return true;
	}

	bool ExtCheck(char *path, char* ext, bool bMatchCase)
	{
		char *ext_temp = new char[512];// = _strupr(ext);
		strcpy_s(ext_temp, 512, ext);
		if (bMatchCase == false) _strupr_s(ext_temp, 512);
		char *path_temp = new char[512];
		strcpy_s(path_temp, 512, path);
		if (bMatchCase == false) _strupr_s(path_temp, 512);

		int length = static_cast<int>(strlen(path_temp));
		char ext_path[5];
		strncpy_s(ext_path, &path_temp[length + 1 - 5], 5);
		int res = strcmp(ext_path, ext_temp);

		if (res == 0) return true;
		else return false;
	}

	bool ASCIIReadOpen(fstream &infile, char* path)
	{
		infile.open(path, ios::in);
		if (!infile)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	bool ASCIISaveOpen(fstream &outfile, char* path)
	{
		outfile.open(path, ios::out);
		if (!outfile)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	bool BINReadOpen(fstream &infile, char* path)
	{
		infile.open(path, ios::in | ios::binary);
		if (!infile)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	bool BINSaveOpen(fstream &outfile, char* path)
	{
		outfile.open(path, ios::out | ios::binary);
		if (!outfile)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	void ASCIIFilePrecision(fstream &file, unsigned int precision)
	{
		file.setf(ios::fixed, ios::floatfield);

		file.precision(precision);
	}

	long long fileLength(const string& filename)
	{
		fstream file(filename, ios::binary | ios::in);
		if (!file.is_open())
			throw std::runtime_error("The file does not exist.");

		file.seekg(0, ios_base::beg);
		auto pos0 = file.tellg();
		file.seekg(0, ios_base::end);
		auto  pos1 = file.tellg();
		long long fSize = pos1 - pos0;
		file.close();
		return fSize;
	}

	void debugMsg(std::string msg)
	{
#ifdef _DEBUG
		std::cout << msg << std::endl;
#endif
	}

	bool estimatePlane(const std::vector<double>& x, const std::vector<double>& y, const std::vector<double>& z)
	{
		//aX + bY + Z + c = 0

		if (x.size() != y.size() || y.size() != z.size())
			return false;

		unsigned int numPts = static_cast<unsigned int>(x.size());

		math::Matrix<double> A, L, X, B, Q;
		A.resize(numPts, 3);
		L.resize(numPts, 1);
		B.resize(numPts, numPts * 3);
		Q.resize(numPts * 3, numPts * 3);

		for (unsigned int i = 0; i < numPts; i++)
		{
			A(i, 0) = x[i];
			A(i, 1) = y[i];
			A(i, 2) = 1.0;
			L(i, 0) = -z[i];
		}

		X = (A.transpose() % A).inverse() % A.transpose() % L;

		double coeff[3];
		coeff[0] = X(0, 0);
		coeff[1] = X(1, 0);
		coeff[2] = X(2, 0);

		for (unsigned int i = 0; i < numPts; i++)
		{
			B(i, i * 3 + 0) = coeff[0];
			B(i, i * 3 + 1) = coeff[1];
			B(i, i * 3 + 2) = 1.0;
		}

		Q.makeIdentityMat();

		math::Matrix<double> bqbt = B%Q%B.transpose();
		math::Matrix<double> W = bqbt.inverse();
		X = (A.transpose() % W%A).inverse() % A.transpose() % W%L;

		coeff[0] = X(0, 0);
		coeff[1] = X(1, 0);
		coeff[2] = X(2, 0);

		return true;
	}

	/// Get system environment variable
	void getSysVariable(size_t& len, char* val, const size_t valLength, const char* name)
	{
		getenv_s(&len, val, valLength, name);
	}

}