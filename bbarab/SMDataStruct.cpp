#include "stdafx.h"
#include "SMDataStruct.h"

using namespace SMATICS_BBARAB;

FILEMEM::FILEMEM(int size)
{
	n_Size = size;
	temp_path = new char[512];
}

FILEMEM::~FILEMEM()
{
	Close();
}

void FILEMEM::Get(long pos, BYTE *temp)
{
	tempfile.seekg(pos, ios::beg);
	tempfile.read((char*)temp, n_Size);
}

void FILEMEM::Set(long pos, BYTE *temp)
{
	tempfile.seekp(pos, ios::beg);
	tempfile.write((char*)temp, n_Size);
}

void FILEMEM::Open(char* fname)
{
	strcpy(temp_path, fname);
	tempfile.open(fname, ios::in|ios::out);
}

void FILEMEM::Close()
{
	tempfile.close();
}