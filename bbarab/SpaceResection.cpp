/////////////////////////////////////////////////////////
//SpaceResection.cpp
//made by bbarab
//revision: 2000-8-2
//인하대학교 토목공학과 지형정보연구실
//항공사진의 공간후방교회법 과  위성영상의 공간후방교회법(공선조건식)
//위성영상의 mathmodel은 등속직선운동및 Const Angle로 가정
///////////////////////////////////////////////////////////////
//revision: 2001-06-25
//pushbroom위성영상의 공간후방교회를 제거
//항공사진 공간후방교회만으로 구성
//////////////////////////////////////////////////////////
//인하대학교 토목공학과 지형정보연구실
//항공사진의 공간후방교회법
///////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SpaceResection.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////
//class CSpaceResection
/////////////////////////////

CSpaceResection::CSpaceResection()
{
	ID = NULL;
	a = NULL;
	GCP = NULL;
};

CSpaceResection::CSpaceResection(char *fname)
{
	char st[100];
	FILE* indata;
	int i;
	//file open
	indata = fopen(fname,"r");
	
	//trash memory(read filename string)
	fgets(st,100,indata);

	//trash memory(read "focal length")
	fgets(st,100,indata);
	//focal length
	fscanf(indata,"%lf",&f);
	fgets(st,100,indata);

	fgets(st,100,indata);
	//flying height
	fscanf(indata,"%lf",&FH);
	fgets(st,100,indata);

	fgets(st,100,indata);
	//PPA
	fscanf(indata,"%lf",&PPA.x);
	fscanf(indata,"%lf",&PPA.y);
	fgets(st,100,indata);

	fgets(st,100,indata);
	//PPBS
	fscanf(indata,"%lf",&PPBS.x);
	fscanf(indata,"%lf",&PPBS.y);
	fgets(st,100,indata);
	
	fgets(st,100,indata);
	//number of GCP
	fscanf(indata,"%d",&num_GCP);
	fscanf(indata,"%d",&num_Check);
	fgets(st,100,indata);

	fgets(st,100,indata);
	ID = new int[num_GCP];
	a = new Point2D<double>[num_GCP];
	GCP = new Point3D<double>[num_GCP];
	//Photo Point & GCP
	for(i=0;i<num_GCP;i++)
	{
		fscanf(indata,"%d",&ID[i]);
		fscanf(indata,"%lf",&a[i].x);
		fscanf(indata,"%lf",&a[i].y);
		fscanf(indata,"%lf",&GCP[i].x);
		fscanf(indata,"%lf",&GCP[i].y);
		fscanf(indata,"%lf",&GCP[i].z);
	}
	fgets(st,100,indata);

	fgets(st,100,indata);
	//max iteration
	fscanf(indata,"%d",&maxiteration);
	fgets(st,100,indata);

	fgets(st,100,indata);
	//max correction
	fscanf(indata,"%lf",&maxcorrection);
	fgets(st,100,indata);
		
	fclose(indata);
};

CSpaceResection::~CSpaceResection()
{
	if(ID != NULL)
	{
		delete[] ID;
		ID = NULL;
	}
	if(a != NULL)
	{
		delete[] a;
		a = NULL;
	}
	if(GCP != NULL)
	{
		delete[] GCP;
		GCP = NULL;
	}
};

BOOL CSpaceResection::init()
{
	double s, d1, d2;
	//s: scale
	//d1: distance in photo
	//d2: distance in ground
	d1 = sqrt(pow((a[1].x-a[0].x),2)+pow((a[1].y-a[0].y),2));
	d2 = sqrt(pow((GCP[1].x-GCP[0].x),2)+pow((GCP[1].y-GCP[0].y),2));
	s = d2/d1;

	//kappa init_value
	double k1 = atan2((a[0].y-a[1].y),(a[0].x-a[1].x));
	double k2 = atan2((GCP[0].y-GCP[1].y),(GCP[0].x-GCP[1].x));
	double k = -(k1 - k2);
		
	//omega and phi init_value
	Omega = 0.0;
	Phi = 0.0;
	Kappa = k;

	//perspective center init_value
	PC.x = GCP[0].x - a[0].x*s;
	PC.y = GCP[0].y - a[0].y*s;
	PC.z = FH;

	return TRUE;
};

BOOL CSpaceResection::make_b_J_K(Point3D<double> PP, Point3D<double> GP, Point2D<double> p)
{
	//Rotation Matrix
	CRotationcoeff M(Omega, Phi, Kappa);
	
	double q, r, s;
	
	double dX, dY, dZ;
	
	double fqq;
	
	dX = GP.x - PP.x;
	dY = GP.y - PP.y;
	dZ = GP.z - PP.z;

	q = M.Rmatrix(2,0)*dX + M.Rmatrix(2,1)*dY + M.Rmatrix(2,2)*dZ;
	r = M.Rmatrix(0,0)*dX + M.Rmatrix(0,1)*dY + M.Rmatrix(0,2)*dZ;
	s = M.Rmatrix(1,0)*dX + M.Rmatrix(1,1)*dY + M.Rmatrix(1,2)*dZ;
	
	fqq = f/(q*q);

	b11 = fqq*(r*(-M.Rmatrix(2,2)*dY+M.Rmatrix(2,1)*dZ) - q*(-M.Rmatrix(0,2)*dY+M.Rmatrix(0,1)*dZ));
	b12 = fqq*(r*(cos(Phi)*dX+sin(Omega)*sin(Phi)*dY-cos(Omega)*sin(Phi)*dZ)
		- q*(-sin(Phi)*cos(Kappa)*dX+sin(Omega)*cos(Phi)*cos(Kappa)*dY-cos(Omega)*cos(Phi)*cos(Kappa)*dZ));
	b13 = -fqq*q*(M.Rmatrix(1,0)*dX+M.Rmatrix(1,1)*dY+M.Rmatrix(1,2)*dZ);
	b14 = fqq*(r*M.Rmatrix(2,0)-q*M.Rmatrix(0,0));
	b15 = fqq*(r*M.Rmatrix(2,1)-q*M.Rmatrix(0,1));
	b16 = fqq*(r*M.Rmatrix(2,2)-q*M.Rmatrix(0,2));
	
	J = p.x - PPA.x + f*r/q;
		
	b21 = fqq*(s*(-M.Rmatrix(2,2)*dY+M.Rmatrix(2,1)*dZ) - q*(-M.Rmatrix(1,2)*dY+M.Rmatrix(1,1)*dZ));
	b22 = fqq*(s*(cos(Phi)*dX+sin(Omega)*sin(Phi)*dY-cos(Omega)*sin(Phi)*dZ)
		- q*(sin(Phi)*sin(Kappa)*dX-sin(Omega)*cos(Phi)*sin(Kappa)*dY+cos(Omega)*cos(Phi)*sin(Kappa)*dZ));
	b23 = fqq*q*(M.Rmatrix(0,0)*dX+M.Rmatrix(0,1)*dY+M.Rmatrix(0,2)*dZ);
	b24 = fqq*(s*M.Rmatrix(2,0)-q*M.Rmatrix(1,0));
	b25 = fqq*(s*M.Rmatrix(2,1)-q*M.Rmatrix(1,1));
	b26 = fqq*(s*M.Rmatrix(2,2)-q*M.Rmatrix(1,2));

	K = p.y - PPA.y + f*s/q;
	
	return TRUE;
};

BOOL CSpaceResection::make_A_L(void)
{
	int i;
	//design matrix
	A.Resize(2*num_GCP,6,0.0);
	//observation vlaue matrix
	L.Resize(2*num_GCP,1,0.0);

	for(i=0;i<num_GCP;i++)
	{
		if(!make_b_J_K(PC,GCP[i],a[i]))
		{
			return FALSE;
		}
		A(2*i,0) = b11;
		A(2*i,1) = b12;
		A(2*i,2) = b13;
		A(2*i,3) = -b14;
		A(2*i,4) = -b15;
		A(2*i,5) = -b16;

		L(2*i,0) = J;

		A(2*i+1,0) = b21;
		A(2*i+1,1) = b22;
		A(2*i+1,2) = b23;
		A(2*i+1,3) = -b24;
		A(2*i+1,4) = -b25;
		A(2*i+1,5) = -b26;

		L(2*i+1,0) = K;

	}
	
	return TRUE;
};

BOOL CSpaceResection::solve(void)
{
	//temporary string memory for report
	CString temp;
	//maximum element of X_matrix
	double maxX;
	int i, iteration=0;
	BOOL stop = FALSE;

	//Initialize
	if(!init())
	{
		return FALSE;
	}

	result = "[Aerialphoto Space Resection (Collinearity Equation)]\r\n";
	//calculation degree of freedom
	DF = num_GCP*2 - 6;
	do
	{
		iteration ++;
		result += "\r\n\r\niteration = ";
		temp.Format("%d\r\n",iteration);
		result += temp;
		
		//A & L matrix composition
		if(!make_A_L())
		{
			return FALSE;
		}

		result += "-A Matrix-\r\n";
		//result += matrixout(A);
		result += "-L Matrix-\r\n";
		//result += matrixout(L);

		AT = A.Transpose();
		N = AT%A;
		Ninv = N.Inverse();
		X = Ninv%AT%L;
		V = A%X - L;

		result += "-AT Matrix-\r\n";
		//result += matrixout(AT);
		result += "-N Matrix-\r\n";
		//result += matrixout(N);
		result += "-Ninv Matrix-\r\n";
		//result += matrixout(Ninv);
		result += "-X Matrix-\r\n";
		//result += matrixout(X);		
		result += "-V Matrix-\r\n";
		//result += matrixout(V);		
		
		if(DF>0)
		{
			VTV = V.Transpose()%V;
			Posteriori_Var = VTV(0,0)/DF;
			SD = sqrt(Posteriori_Var);
			Covar = Ninv*Posteriori_Var;
			Var.Resize((int)Covar.GetRows(),1);
			for(int j=0;j<(int)Covar.GetRows();j++)
			{
				Var(j,0) = Covar(j,j);
			}
			
			result += "-VTV Matrix-\r\n";
			//result += matrixout(VTV);
			result += "-Covar Matrix-\r\n";
			//result += matrixout(Covar);
			result += "-Var Matrix-\r\n";
			//result += matrixout(Var);
			
			result += "Posteriori Variance = ";
			temp.Format("%lf\r\n",Posteriori_Var);
			result += temp;
			result += "SD = ";
			temp.Format("%lf\r\n",SD);
			result += temp;
		}
				
		maxX = fabs(X(0));
		for(i=1;i<(int)X.GetRows();i++)
		{
			if(maxX<fabs(X(i)))
				maxX = fabs(X(i));
		}

		result += "Max Correction = ";
		temp.Format("%lf\r\n",maxX);
		result += temp;
	
		Omega += X(0);
		Phi   += X(1);
		Kappa += X(2);
		PC.x  += X(3);
		PC.y  += X(4);
		PC.z  += X(5);

		result += "Omega = ";
		temp.Format("%lf rad (%lf deg)\r\n",Omega,Omega/3.141592654*180);
		result += temp;
		result += "Phi = ";
		temp.Format("%lf rad (%lf deg)\r\n",Phi,Phi/3.141592654*180);
		result += temp;
		result += "Kappa = ";
		temp.Format("%lf rad (%lf deg)\r\n",Kappa,Kappa/3.141592654*180);
		result += temp;
		result += "X = ";
		temp.Format("%lf\r\n",PC.x);
		result += temp;
		result += "Y = ";
		temp.Format("%lf\r\n",PC.y);
		result += temp;
		result += "Z = ";
		temp.Format("%lf\r\n",PC.z);
		result += temp;

		if(iteration == maxiteration)
		{
			stop = TRUE;
			result += "Execss Max Iteration\r\n";
		}
		if(maxX < maxcorrection)
		{
			stop = TRUE;
			result += "Max Correction < Limit Correction\r\n";
		}
	}while(stop == FALSE);
	
	//Correlation Coefficient
	result += "[Correlation Coefficient]\r\n";
	double Correlation;
	int j;
		
	for(i=0;i<6;i++)
	{
		for(j=i+1;j<6;j++)
		{
			Correlation = Covar(i,j)/sqrt(Var(i,0)*Var(j,0));
			temp.Format("[%d,%d] \t%lf\r\n",i,j,Correlation);
			result += temp;
		}
	}
	
	return TRUE;
};
