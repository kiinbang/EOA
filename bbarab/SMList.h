/*
 * Copyright (c) 2000-2003, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#if !defined(__SPACEMATICS_SMList_H__)
#define __SPACEMATICS_SMList_H__

#define NULL nullptr

/**
*@class DATA
*@brief data template class.
*/
template <typename T>
class DATA
{
public:
	DATA()
	{
		forward = nullptr;
		next = nullptr;
		nID = -1;
		bAvailable = true;
	}

	DATA( DATA<T> &copy )
	{
		forward = copy.forward;
		next = copy.next;
		nID = copy.nID;
		bAvailable = copy.bAvailable;
		value = copy.value;
	}

	virtual ~DATA() {}

	void operator = (DATA<T> &copy)
	{
		forward = copy.forward;
		next = copy.next;
		nID = copy.nID;
		bAvailable = copy.bAvailable;
		value = copy.value;
	}

	DATA<T> *forward;/**<forward address*/
	DATA<T> *next;/**<next address*/
	int nID;
	bool bAvailable;
	T value;/**<this place data*/
};


/**@class CSMList
*@brief template class for list data[class DATA] management.
*/
template<typename T>
class __declspec (dllexport) CSMList
{
public:
	/**constructor*/
	CSMList()
	{
		first = nullptr;
		last = nullptr;
		recent = nullptr;
		recent_sort = 0;
		m_nNumData = 0;
		ID_count = -1;
	}

	/**copy constructor*/
	CSMList(CSMList& copy)
	{
		m_nNumData = 0;
		first = nullptr;
		last = nullptr;
		recent = nullptr;
		recent_sort = 0;
			
		DATA<T> *pos = nullptr;
		for(unsigned long i=0; i<copy.m_nNumData; i++)
		{
			//AddTail() 함수를 이용한 복사
			//AddTail(copy.GetAt(i));
			pos = copy.GetNextPos(pos);
			T value = pos->value;
			AddTail(value);
		}

		ID_count = copy.ID_count;
	}

	/**destructor*/
	virtual ~CSMList()
	{
		RemoveAll();
	}

private:
	//member variable
	DATA<T> *first;/**<pointer of first data*/
	DATA<T> *last; /**<pointer of last data*/
	unsigned long m_nNumData; /**<number of data(elements)*/
	int ID_count;
	DATA<T> *recent;
	unsigned long recent_sort;

//member function
public:
	/**Set availability*/
	void SetAvailability(const unsigned long sort, bool bAvailability) const
	{
		DATA<T> *target = GetDataHandle(sort);
		target->bAvailable = bAvailability;
	}

	/**Get availabliity*/
	bool GetAvailability(const unsigned long sort) const
	{
		DATA<T> *target = GetDataHandle(sort);
		return target->bAvailable;
	}
	
	/**Delete un-available data*/
	unsigned long DeleteUnavailableData()
	{
		DATA<T> *pos = NULL;

		unsigned long count = 0;

		pos = GetFirstPos();
		while(pos != NULL)
		{
			if(pos->bAvailable == false)
			{
				pos = DeleteDatabyHandle(pos);
				count ++;
			}
			else
			{
				pos = GetNextPos(pos);
			}
		}

		return count;
	}

	/**Get number of items*/
	unsigned long GetNumItem() const
	{
		return m_nNumData;
	}

	/**Get number of items*/
	unsigned long size() const
	{
		return m_nNumData;
	}

	/**Set void data*/
	void SetVoidData(const unsigned int num)
	{
		T* data;
		data = new T;
		for(unsigned int i=0; i<num; i++) 
			AddTail(*data);
	}
	/**fill the data with default value*/
	void SetVoidData(const unsigned int num, const T data)
	{
		for(unsigned int i=0; i<num; i++) 
			AddTail(data);
	}

	/**Add data to tail*/
	bool AddTail(const T data)
	{
		if(m_nNumData == 0)//First data
		{
			last = new DATA<T>;
			first = last;
		}
		else
		{
			last->next = new DATA<T>;
			last->next->forward = last;
			last = last->next;
		}

		last->value = data;
		ID_count++;
		last->nID = ID_count;
		
		m_nNumData++;
		
		return true;
	}
	
	/**Add data to head*/
	bool AddHead(const T data)
	{
		if(m_nNumData == 0)//First data
		{
			first = new DATA<T>;
			last = first;
		}
		else
		{
			first->forward = new DATA<T>;
			first->forward->next = first;
			first = first->forward;
		}

		first->value = data;
		ID_count++;
		first->nID = ID_count;
		
		m_nNumData++;

		recent = nullptr;
		recent_sort = 0;
		
		return true;
	}

	/**Delete item by index which starts from #0*/
	bool DeleteData(const unsigned long sort)
	{
		if(sort >= m_nNumData)
			return false;

		DATA<T>* target;
		target = GetDataHandle(sort);

		if(target != nullptr)
		{
			if(nullptr == DeleteDatabyHandle(target))
				return false;
			else
				return true;
		}
		else
			return false;
	}

		/**Delete item by index which starts from #0*/
	DATA<T>* DeleteDatabyHandle(DATA<T> *target)
	{			
		if(target != nullptr)
		{
			recent = nullptr;
			recent_sort = 0;
			
			if(target == first)//first data
			{
				first = target->next;
				delete target;
				target = nullptr;

				if(first != nullptr)
					first->forward = nullptr;
				m_nNumData--;
				return first;
			}
			else if(target == last)//last data
			{
				last = target->forward;
				delete target;
				target = nullptr;

				if(last != nullptr)
					last->next = nullptr;
				m_nNumData --;
				return last;
			}
			else
			{
				DATA<T> *Pre, *Pos;
				Pre = target->forward;
				Pos = target->next;
				delete target;
				target = nullptr;

				Pre->next = Pos;
				Pos->forward = Pre;
				m_nNumData--;
				return Pos;
			}

			recent = nullptr;
			recent_sort = 0;
		}
		else
			return nullptr;

		
	}

	/**Insert item by index which starts from #0*/
	bool InsertData(const unsigned long sort, const T data)
	{
		if(sort > m_nNumData)
		{
			return false;
		}
		else if(sort==0)
		{
			AddHead(data); 
			return true;
		}
		else if(sort == m_nNumData)
		{
			AddTail(data); 
			return true;
		}
		
		//forward link
		DATA<T>* target1;
		target1 = GetDataHandle(sort-1);

		//backward link
		DATA<T>* target2;
		target2 = GetDataHandle(sort);
		
		//new link
		DATA<T> *newlink = new DATA<T>;
		newlink->value = data;

		//insert
		target1->next = newlink;
		newlink->forward = target1;

		target2->forward = newlink;
		newlink->next = target2;

		m_nNumData += 1;	

		//recent = NULL;
		//recent_sort = 0;
		if(recent_sort >= sort)
			recent_sort ++;

		return true;
	}

	/**Empty list*/
	bool RemoveAll()
	{
		recent = nullptr;
		recent_sort = 0;
				
		DATA<T> *pos = first;

		while(pos != nullptr)
		{
			pos = DeleteDatabyHandle(pos);

			if(pos == nullptr)
			{
				break;
			}
			else if(m_nNumData < 0)
			{
				//AfxMessageBox("CSMList::RemoveAll()--> m_nNumData < 0");
				return false;
			}
		}

		if(m_nNumData != 0)
		{
			//AfxMessageBox("CSMList::RemoveAll()--> m_nNumData != 0");
			return false;
		}

		ID_count = -1;

		return true;
	}

	/**Remove first data*/
	bool RemoveHead(void)
	{
		return DeleteData(0);
	}
	
	/**Remove last data*/
	bool RemoveTail(void)
	{
		return DeleteData(m_nNumData - 1);
	}

	/**Get specific data by index(sort)*/
	void SetIDAt(const unsigned long sort, int ID) const
	{
		DATA<T>* target = GetDataHandle(sort);
		target->ID = ID;
	}

	/**Get specific data by index(sort)*/
	int GetIDAt(const unsigned long sort) const
	{
		DATA<T>* target = GetDataHandle(sort);
		return target->ID;
	}

	T GetAt(const unsigned long sort)
	{
		DATA<T>* target = GetDataHandle(sort);
		return target->value;
	}

	/**Get the next post*/
	DATA<T>* GetNextPos(const DATA<T>* pos) const
	{
		if(pos == nullptr)
		{
			return first;
		}
		else if(pos == last)
		{
			return nullptr;
		}
		else
		{			
			DATA<T>* target;
			target = pos->next;
			return target;
		}
	}

	/**Get the first position*/
	DATA<T>* GetFirstPos() const
	{
		return first;
	}

	/**Get specific handle by index(sort)*/
	T* GetHandleAt(const unsigned long sort)
	{
		DATA<T>* target = GetDataHandle(sort);
		return &(target->value);
	}

	/**Set specific data by index(sort)*/
	bool SetAt(const unsigned long sort, const T newvalue)
	{
		DATA<T> *target = GetDataHandle(sort);
		target->value = newvalue;
		return true;
	}

	/**[] operator overloading*/
	T& operator [] (const unsigned long sort)
	{
		DATA<T> *target = GetDataHandle(sort);
		return target->value;
	}

	/**= operator overloading*/
	void operator=(const CSMList& copy)
	{
		RemoveAll();
		m_nNumData = 0;
		first = nullptr;
		last = nullptr;
		
		DATA<T> *pos = nullptr;
		for(unsigned long i=0; i<copy.m_nNumData; i++)
		{
			//AddTail() 함수를 이용한 복사
			//AddTail(copy.GetAt(i));
			pos = copy.GetNextPos(pos);
			T value = pos->value;
			AddTail(value);
		}
	}

	/**+= operator overloading*/
	void operator+=(const CSMList& copy)
	{	
		DATA<T> *pos = NULL;
		for(unsigned long i=0; i<copy.m_nNumData; i++)
		{
			pos = copy.GetNextPos(pos);
			T value = pos->value;

			AddTail(value);
		}
	}

private:
	/**Get DATA handle*/
	DATA<T>* GetDataHandle(const unsigned long sort)
	{
		if(sort >= m_nNumData)
			return NULL;

		DATA<T> *target;

		if(recent == NULL)
			target = first;
		else
			target = recent;

		if((int)sort > recent_sort)
		{
			for(unsigned long i=recent_sort; i<sort; i++)
			{
				target = target->next;
			}
			
		}
		else
		{
			for(unsigned long i=recent_sort; i>sort; i--)
			{
				target = target->forward;
			}
			
		}

		recent = target;//update recent data address
		recent_sort = sort;

		return target;
	}
};

#endif // !defined(__SPACEMATICS_SMList_H__)
