// SMParallel.h: interface for the SMParallel class.
//
//////////////////////////////////////////////////////////////////////

/*Made by Bang, Ki In*/
/*Photogrammetry group(Prof.Habib), Geomatics UofC*/

#if !defined(__BBARAB_PARALLEL_PROJECTION_CLASS__)
#define __BBARAB_PARALLEL_PROJECTION_CLASS__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMDataParser.h"
#include "SMCoordinateClass.h"
#include "LeastSquare.h"

#define _NUM_PARAM_ 20 //3rd order RFM has a 20(or 19) parameters in each denominator or numerator

/**
*@class SMParam
*@brief This is the class for approximate model parameters.
*/
class SMParam
{
public:
	SMParam();
	SMParam(unsigned int Anum, unsigned int Bnum, unsigned int Cnum);
	SMParam(SMParam &copy);
	virtual ~SMParam();
	/**
	*Initialize class members.
	*/
	void Initialize();
	void Initialize(unsigned int Anum, unsigned int Bnum, unsigned int Cnum);
	/**
	*Assignment operator.
	*/
	void operator=(SMParam &copy);
	/**
	*Copy constructor.
	*/
	void Copy(SMParam &copy);
	
public:
	double* A; /**<A: parameters*/
	double* B; /**<A: parameters*/
	double* C; /**<A: parameters*/

	unsigned int A_num_param;
	unsigned int B_num_param;
	unsigned int C_num_param;

	//MPP I
	double c; /**<focal length */
	double psi; /**<roll angle */

	/**
	*Coord normalization coefficients<br>
	*Normalized Coord = (Original Coord + SHIFT)*SCALE<br>
	*Original Coord = (Normalized Coord/SCALE) - SHIFT.
	*/
	double SHIFT_X;/**<Shift of X*/
	double SCALE_X;/**<Scale of X*/
	double SHIFT_Y;/**<Shift of Y*/
	double SCALE_Y;/**<Scale of Y*/
	double SHIFT_Z;/**<Shift of Z*/
	double SCALE_Z;/**<Scale of Z*/
	double SHIFT_col;/**<Shift of col*/
	double SCALE_col;/**<Scale of col*/
	double SHIFT_row;/**<Shift of row*/
	double SCALE_row;/**<Scale of row*/
};

/**
*@class SMModifiedParallel
*@brief This is the class for modified parallel projection model.
*[Modified Parallel Model]</br>
*row = A1X + A2Y + A3Z + A4</br>
*col = A5X + A6Y + A7Z + A8/(1+(tan(p)/c)(A5X + A6Y + A7Z + A8)).
*/
class SMModifiedParallel
{
	//friend class CSMParallel;
public:
	bool bNormalized;
protected:
	CSMDataParser SMData; /**<This data set will be changed in each iteration*/
	CSMDataParser twin_SMData; //**<This data set keeps the original value*/
	CSMList<SMParam> param_list; /**<parameters list of (modified) parallel projection*/
	double GCP_sd_threshold; /**<ground point's threshold for iteration stop*/
	CSMList<SMParam> param_list_ori; /**<if data is normalized, it is the de-normalized parameters*/
	double focal_length;
public:

	/*******************************************************************
	*[Modified Parallel Model]
	*x = A1X + A2Y + A3Z + A4
	*y = A5X + A6Y + A7Z + A8/(1+(alpha)(A5X + A6Y + A7Z + A8)).
	*alpha = tan(psi)/c
	********************************************************************/
	SMModifiedParallel();
	virtual ~SMModifiedParallel();
	/**
	*initialize object.
	*/
	void Init();

	/**
	*Bundle Adjustment(Modified Parallel Projection)
	*/
	virtual CString DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize);
	/**
	*This func is called by DoModeling().
	*/
	virtual CString Run(unsigned int num_param, unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize);
	/**
	*Calculate the initial value<br>
	*This func is called by DoModeling().
	*/
	virtual CString DoInitModeling(unsigned int num_param_);
	/**
	*Set configuration data.
	*/
	void SetConfigData(CString ppppath, double c);
	/**
	*Open ICP file<br>
	*IF bDataString is true, return the contents of file in CString format.
	*/
	virtual CString InputMatlabICPFile(CString fname, bool bDataString=true);
	/**
	*Open ICL file<br>
	*IF bDataString is true, return the contents of file in CString format.
	*/
	virtual CString InputMatlabICLFile(CString fname, bool bDataString=true);
	/**
	*Open GCP file<br>
	*IF bDataString is true, return the contents of file in CString format.
	*/
	virtual CString InputMatlabGCPFile(CString fname, bool bDataString=true);
	/**
	*Open GCL file<br>
	*IF bDataString is true, return the contents of file in CString format.
	*/
	virtual CString InputMatlabGCLFile(CString fname, bool bDataString=true);
	
protected:
	/**
	*Update parameters of model after each iteration.
	*/
	virtual CString UpdateUnknownParam(Matrix<double> &X, unsigned int num_param);
	/**
	*Update ground points after each iteration.
	*/
	virtual CString UpdateUnknownPoint(Matrix<double> &X, CSMDataParser& SMData, unsigned int num_param);
	/**
	*Update ground points for the lines after each iteration.
	*/
	virtual CString UpdateUnknownLine(Matrix<double> &X, CSMDataParser& SMData, unsigned int num_param);

	/**Cal_PDs<br>
	*Partial derivatives
	*@param G ground point coord
	*@param I image point coord
	*@param ret1 partial derivative(row)
	*@param ret2 partial derivative(col)
	*@param num_param number of parameters of sensor model
	*@param index scene #
	*/
	virtual void Cal_PDs(_GCP_ G, double* ret1, double* ret2, unsigned int index);
	
	/**
	*Partial derivatives for a line
	*@param GL1 ground control line
	*@param GL2 ground control line
	*@param IL image control line
	*@param ret partial derivatives
	*@param index scene #
	*@param num_param number of parameters of sensor model
	*/
	virtual void Cal_PDs_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int index, unsigned int num_param);
	
	/**
	*Image2Ground.
	*/
	virtual _GCP_ GetImage2Ground(_GCP_ G, _ICP_ I1, _ICP_ I2, unsigned int index1, unsigned int index2, unsigned int maxiter, double sd, unsigned int num_param);
		
	/**
	*Get row value using approximate parameters.
	*/
	virtual double Func_row(_GCP_ G, unsigned int index)
	{
		SMParam param = param_list[index];
		double row = param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3];
		return row;
	}
	virtual double Func_row(_GCL_ G, unsigned int index)
	{
		SMParam param = param_list[index];
		double row = param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3];
		return row;
	}
	/**
	*Get col value using approximate parameters.
	*/
	virtual double Func_col(_GCP_ G, unsigned int index)
	{
		SMParam param = param_list[index];
		double temp1 = param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7];
		double temp2 = param.A[8];
		double col = temp1/(1+temp2*temp1);
		return col;
	}
	/**
	*Calculating initial value (Go) using approximate parameters.
	*/
	virtual double Func_col(_GCL_ G, unsigned int index)
	{
		SMParam param = param_list[index];
		double temp1 = param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7];
		double temp2 = param.A[8];
		double col = temp1/(1+temp2*temp1);
		return col;
	}

	/**
	*Calculating initial value (linear constrain func) using approximate parameters<br>
	*H(row,col) = (col2-col1)(row-row2)-(row2-row1)(col-col2).
	*/
	virtual double Func_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, unsigned int image_index)
	{
		_ICP_ icp1, icp2;
		_GCP_ gcp1, gcp2;

		gcp1.X = GL1.X;
		gcp1.Y = GL1.Y;
		gcp1.Z = GL1.Z;

		gcp2.X = GL2.X;
		gcp2.Y = GL2.Y;
		gcp2.Z = GL2.Z;
		
		icp1.col = Func_col(gcp1, image_index);
		icp1.row = Func_row(gcp1, image_index);

		icp2.col = Func_col(gcp2, image_index);
		icp2.row = Func_row(gcp2, image_index);
		
		//Line constraint
		double H = ((icp2.col-icp1.col)*(IL.row-icp2.row)) - (icp2.row-icp1.row)*(IL.col-icp2.col);

		return H;
	}

	/**
	*Make report file.
	*/
	virtual void FileOut(CString fname, CString st);

	/**
	*Parameters de-normalization.
	*/
	virtual SMParam ParamDenormalization(SMParam param, unsigned int num_param);
	/**
	*Run iteration.
	*/
	virtual CLeastSquare Iteration(Matrix<double> &Xmat, unsigned int num_param, double &var);
	/**
	*Fill elements of Normal matrix for the image points observation.
	*/
	virtual void FillMatrix_Point(CLeastSquare &LS, unsigned int num_param);
	/**
	*Fill elements of Normal matrix for each image point observation(i).
	*/
	virtual void FillMatrix_Point_i(CLeastSquare &LS, _GCP_ GP, _ICP_ IP, unsigned image_index, Matrix<double> &a1, Matrix<double> &a2, Matrix<double> &l, Matrix<double> &w, unsigned int num_param);
	/**
	*Fill elements of Normal matrix for the line constraints.
	*/
	virtual void FillMatrix_Line(CLeastSquare &LS, unsigned int num_param);
	/**
	*Fill elements of Normal matrix for each line constraint(i).
	*/
	virtual void FillMatrix_Line_i(CLeastSquare &LS, _ICL_ IL, _GCL_ GLA, _GCL_ GLB, unsigned image_index, Matrix<double> &a1, Matrix<double> &a2, Matrix<double> &a3, Matrix<double> &l, Matrix<double> &w, unsigned int num_param);
	/**
	*Fill elements of Normal matrix for the ground point constraints.
	*/
	virtual void FillMatrix_XYZ(CLeastSquare &LS);
	/**
	*Fill elements of Normal matrix for each ground point constraint(i).
	*/
	virtual void FillMatrix_XYZ_i(CLeastSquare &LS, _GCP_ GP_init, _GCP_ GP_ori, unsigned image_index, Matrix<double> &a2, Matrix<double> &l, Matrix<double> &w);

	/**
	*Denormalization for the 3D ground coords.
	*/
	virtual void DeNormalizedCoord(_GCP_ &G, SMParam param);
	/**
	*Denormalization for the 2D image coords.
	*/
	virtual void DeNormalizedCoord(_ICP_ &I, SMParam param);
	/**
	*Denormalization for the 3D coords of lines.
	*/
	virtual void DeNormalizedCoord(_GCL_ &G, SMParam param);

	/**
	*Normalize Data.
	*/
	virtual void DataNormalization();
	/**
	*Generate the normalized data.
	*/
	virtual void GenNormalizedData();

	/**
	*Assignment of output file names.
	*/
	virtual void GenerateFileName(CString name, CString &param_list, CString &GCP_list, CString &GCL_List, CString &Nimg, CString &Corrimg)
	{
		param_list = name + "_params.csv";
		GCP_list = name + "_GCP.csv";
		GCL_List = name + "_GCL.csv";
		Nimg = name + "_Nimg.bmp";
		Corrimg = name + "_Corrimg.bmp";
	}

};

#endif // !defined(__BBARAB_PARALLEL_PROJECTION_CLASS__)
