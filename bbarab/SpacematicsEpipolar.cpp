// SpacematicsEpipolar.cpp: implementation of the CEpipolar class.
// maed by BbaraB
// revision date 2001/03/09
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SpacematicsEpipolar.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEpipolar::CEpipolar()
{
	LPixelDepth = 8;
	RPixelDepth = 8;
}

CEpipolar::~CEpipolar()
{
}

BOOL CEpipolar::SetEOParameter(double Lo, double Lp, double Lk, double Lx, double Ly, double Lz,
		           double Ro, double Rp, double Rk, double Rx, double Ry, double Rz)
{
	LO = Lo; LP = Lp; LK = Lk;
	LX = Lx; LY = Ly; LZ = Lz;

	RO = Ro; RP = Rp; RK = Rk;
	RX = Rx; RY = Ry; RZ = Rz;

	BX = RX - LX;
	BY = RY - LY;
	BZ = RZ - LZ;

	return TRUE;
}

BOOL CEpipolar::SetEOParameter(int option)
{
	EPIPOLAR_OPTION = option;
	switch(EPIPOLAR_OPTION)
	{
	case 1:
		if(model.ROMethod_inde != TRUE)
			return FALSE;
		LO = model.Param_inde.Lomega;
		LP = model.Param_inde.Lphi;
		LK = model.Param_inde.Lkappa;
		LX = model.Param_inde.Xl;
		LY = model.Param_inde.Yl;
		LZ = model.Param_inde.Zl;

		RO = model.Param_inde.Romega;
		RP = model.Param_inde.Rphi;
		RK = model.Param_inde.Rkappa;
		RX = model.Param_inde.Xr;
		RY = model.Param_inde.Yr;
		RZ = model.Param_inde.Zr;

		BX = model.Param_inde.Xr - model.Param_inde.Xl;
		BY = model.Param_inde.Yr - model.Param_inde.Yl;
		BZ = model.Param_inde.Zr - model.Param_inde.Zl;

		break;

	case 2:
		if(model.ROMethod_de != TRUE)
			return FALSE;
		LO = model.Param_de.Lomega;
		LP = model.Param_de.Lphi;
		LK = model.Param_de.Lkappa;
		LX = model.Param_de.Xl;
		LY = model.Param_de.Yl;
		LZ = model.Param_de.Zl;

		RO = model.Param_de.Romega;
		RP = model.Param_de.Rphi;
		RK = model.Param_de.Rkappa;
		RX = model.Param_de.Xr;
		RY = model.Param_de.Yr;
		RZ = model.Param_de.Zr;

		BX = model.Param_de.Xr - model.Param_de.Xl;
		BY = model.Param_de.Yr - model.Param_de.Yl;
		BZ = model.Param_de.Zr - model.Param_de.Zl;

		break;

	case 3:
		if(model.AOComplete_General != TRUE)
			return FALSE;
		LO = model.Param_AO.Lomega;
		LP = model.Param_AO.Lphi;
		LK = model.Param_AO.Lkappa;
		LX = model.Param_AO.Xl;
		LY = model.Param_AO.Yl;
		LZ = model.Param_AO.Zl;

		RO = model.Param_AO.Romega;
		RP = model.Param_AO.Rphi;
		RK = model.Param_AO.Rkappa;
		RX = model.Param_AO.Xr;
		RY = model.Param_AO.Yr;
		RZ = model.Param_AO.Zr;

		BX = model.Param_AO.Xr - model.Param_AO.Xl;
		BY = model.Param_AO.Yr - model.Param_AO.Yl;
		BZ = model.Param_AO.Zr - model.Param_AO.Zl;

		break;

	default:
		if(model.AOComplete_General != TRUE)
			return FALSE;
		LO = model.Param_AO.Lomega;
		LP = model.Param_AO.Lphi;
		LK = model.Param_AO.Lkappa;
		LX = model.Param_AO.Xl;
		LY = model.Param_AO.Yl;
		LZ = model.Param_AO.Zl;

		RO = model.Param_AO.Romega;
		RP = model.Param_AO.Rphi;
		RK = model.Param_AO.Rkappa;
		RX = model.Param_AO.Xr;
		RY = model.Param_AO.Yr;
		RZ = model.Param_AO.Zr;

		BX = model.Param_AO.Xr - model.Param_AO.Xl;
		BY = model.Param_AO.Yr - model.Param_AO.Yl;
		BZ = model.Param_AO.Zr - model.Param_AO.Zl;
		
		break;
	}

	return TRUE;
}

BOOL CEpipolar::Make_L_RTMatirx()
{
	CRotationcoeff2 rt(LO, LP, LK);
	LRT = rt.Rmatrix;

	return TRUE;
}

BOOL CEpipolar::Make_R_RTMatirx()
{
	CRotationcoeff2 rt(RO, RP, RK);
	RRT = rt.Rmatrix;

	return TRUE;
}

BOOL CEpipolar::MakeRBMatrix()
{
	//vertical image --> normal image로 변환(omega, phi, kappa 순서의 회전변환)
	//vertical image에서 normal image로 가지위한 회전량을 계산하여 O->P->K순서로 회전시킨다.

	base_omega = (LO+RO)/2;
	
	base_phi = -atan(BZ/BX);
	//base line의 회전요소 Phi는 90도가 넘지 않도록 해야한다.
	//90도가 넘으면 영상이 뒤집어진다.
	 
	double bxbz = sqrt(BX*BX+BZ*BZ);
	if(BX<0)
	{
		bxbz = bxbz * -1;
	}
	base_kappa = atan2(BY,bxbz);
	//kappa는 phi와는 달리 -180도에서 180도까지 계산해야 한다.
	//BX가 0보다 작을 경우 수직영상은 180도 회전되 있는 상태이므로 base line을 90도 이상 회전시켜야
	//상호표정과 비슷한 상태로 에피폴라 영상이 생성된다.
	
	CRotationcoeff rb1(0.0, base_phi, base_kappa);
	CRotationcoeff rb2(base_omega,0.0,0.0);
	//phi->kappa->omega회전순서....omega는 최적의 에피폴라를 위해...
	//omega를 먼저회전시키면 결과에 영향을 준다.제일 마지막에 회전시킨다.
	RB = rb2.Rmatrix%rb1.Rmatrix;

	return TRUE;
}

BOOL CEpipolar::Make_L_RNMatrix()
{
	LRN = RB%LRT;

	Make_L_ProjParam();

	return TRUE;
}

BOOL CEpipolar::Make_R_RNMatrix()
{
	RRN = RB%RRT;

	Make_R_ProjParam();

	return TRUE;
}

BOOL CEpipolar::Make_L_ProjParam()
{
	//direct
	Lc.c11 =  fn*LRN(0,0)/(fp*LRN(2,2));
	Lc.c12 =  fn*LRN(0,1)/(fp*LRN(2,2));
	Lc.c13 = -fn*LRN(0,2)/(LRN(2,2));
	
	Lc.c21 =  fn*LRN(1,0)/(fp*LRN(2,2));
	Lc.c22 =  fn*LRN(1,1)/(fp*LRN(2,2));
	Lc.c23 = -fn*LRN(1,2)/(LRN(2,2));
	
	Lc.c31 = -LRN(2,0)/(fp*LRN(2,2));
	Lc.c32 = -LRN(2,1)/(fp*LRN(2,2));

	//inverse
	Lc.c11_ = Lc.c11;
	Lc.c12_ = Lc.c21;
	Lc.c13_ = Lc.c31*fp*fn;
	
	Lc.c21_ = Lc.c12;
	Lc.c22_ = Lc.c22;
	Lc.c23_ = Lc.c32*fp*fn;

	Lc.c31_ = Lc.c13/(fp*fn);
	Lc.c32_ = Lc.c23/(fp*fn);

	return TRUE;
}

BOOL CEpipolar::Make_R_ProjParam()
{
	//direct
	Rc.c11 =  fn*RRN(0,0)/(fp*RRN(2,2));
	Rc.c12 =  fn*RRN(0,1)/(fp*RRN(2,2));
	Rc.c13 = -fn*RRN(0,2)/(RRN(2,2));
	
	Rc.c21 =  fn*RRN(1,0)/(fp*RRN(2,2));
	Rc.c22 =  fn*RRN(1,1)/(fp*RRN(2,2));
	Rc.c23 = -fn*RRN(1,2)/(RRN(2,2));
	
	Rc.c31 = -RRN(2,0)/(fp*RRN(2,2));
	Rc.c32 = -RRN(2,1)/(fp*RRN(2,2));

	//inverse
	Rc.c11_ = Rc.c11;
	Rc.c12_ = Rc.c21;
	Rc.c13_ = Rc.c31*fp*fn;
	
	Rc.c21_ = Rc.c12;
	Rc.c22_ = Rc.c22;
	Rc.c23_ = Rc.c32*fp*fn;

	Rc.c31_ = Rc.c13/(fp*fn);
	Rc.c32_ = Rc.c23/(fp*fn);

	return TRUE;
}

COORD2D<double> CEpipolar::Get_L_NormalCoord_Collinearity(COORD2D<double> OCoord)
{
	COORD2D<double> NCoord;
	NCoord.x = -fn*(LRN(0,0)*OCoord.x+LRN(0,1)*OCoord.y-LRN(0,2)*fp)
		          /(LRN(2,0)*OCoord.x+LRN(2,1)*OCoord.y-LRN(2,2)*fp);
	NCoord.y = -fn*(LRN(1,0)*OCoord.x+LRN(1,1)*OCoord.y-LRN(1,2)*fp)
		          /(LRN(2,0)*OCoord.x+LRN(2,1)*OCoord.y-LRN(2,2)*fp);

	return NCoord;
}

COORD2D<double> CEpipolar::Get_L_OriginalCoord_NormalCoord2(COORD2D<double> NCoord)
{
	COORD2D<double> OCoord;
	Matrix<double> LRNinv;
	LRNinv = LRN.Inverse();

	OCoord.x = -fp*(LRNinv(0,0)*NCoord.x+LRNinv(0,1)*NCoord.y-LRNinv(0,2)*fn)
		          /(LRNinv(2,0)*NCoord.x+LRNinv(2,1)*NCoord.y-LRNinv(2,2)*fn);
	OCoord.y = -fp*(LRNinv(1,0)*NCoord.x+LRNinv(1,1)*NCoord.y-LRNinv(1,2)*fn)
		          /(LRNinv(2,0)*NCoord.x+LRNinv(2,1)*NCoord.y-LRNinv(2,2)*fn);

	return OCoord;
}

COORD2D<double> CEpipolar::Get_R_NormalCoord_Collinearity(COORD2D<double> OCoord)
{
	COORD2D<double> NCoord;
	NCoord.x = -fn*(RRN(0,0)*OCoord.x+RRN(0,1)*OCoord.y-RRN(0,2)*fp)
		          /(RRN(2,0)*OCoord.x+RRN(2,1)*OCoord.y-RRN(2,2)*fp);
	NCoord.y = -fn*(RRN(1,0)*OCoord.x+RRN(1,1)*OCoord.y-RRN(1,2)*fp)
		          /(RRN(2,0)*OCoord.x+RRN(2,1)*OCoord.y-RRN(2,2)*fp);

	return NCoord;
}

COORD2D<double> CEpipolar::Get_R_OriginalCoord_NormalCoord2(COORD2D<double> NCoord)
{
	COORD2D<double> OCoord;
	Matrix<double> RRNinv;
	RRNinv = RRN.Inverse();
	
	OCoord.x = -fp*(RRNinv(0,0)*NCoord.x+RRNinv(0,1)*NCoord.y-RRNinv(0,2)*fn)
		          /(RRNinv(2,0)*NCoord.x+RRNinv(2,1)*NCoord.y-RRNinv(2,2)*fn);
	OCoord.y = -fp*(RRNinv(1,0)*NCoord.x+RRNinv(1,1)*NCoord.y-RRNinv(1,2)*fn)
		          /(RRNinv(2,0)*NCoord.x+RRNinv(2,1)*NCoord.y-RRNinv(2,2)*fn);

	return OCoord;
}

COORD2D<double> CEpipolar::Get_L_NormalCoord_Projective(COORD2D<double> OCoord)
{
	COORD2D<double> NCoord;
	NCoord.x = (Lc.c11*OCoord.x + Lc.c12*OCoord.y + Lc.c13)
		      /(Lc.c31*OCoord.x + Lc.c32*OCoord.y + 1);
	NCoord.y = (Lc.c21*OCoord.x + Lc.c22*OCoord.y + Lc.c23)
		      /(Lc.c31*OCoord.x + Lc.c32*OCoord.y + 1);

	return NCoord;
}

COORD2D<double> CEpipolar::Get_R_NormalCoord_Projective(COORD2D<double> OCoord)
{
	COORD2D<double> NCoord;
	NCoord.x = (Rc.c11*OCoord.x + Rc.c12*OCoord.y + Rc.c13)
		      /(Rc.c31*OCoord.x + Rc.c32*OCoord.y + 1);
	NCoord.y = (Rc.c21*OCoord.x + Rc.c22*OCoord.y + Rc.c23)
		      /(Rc.c31*OCoord.x + Rc.c32*OCoord.y + 1);

	return NCoord;
}

COORD2D<double> CEpipolar::Get_L_OriginalCoord_NormalCoord(COORD2D<double> NCoord)
{
	COORD2D<double> OCoord;
	OCoord.x = (Lc.c11_*NCoord.x + Lc.c12_*NCoord.y + Lc.c13_)
		      /(Lc.c31_*NCoord.x + Lc.c32_*NCoord.y + 1);
	OCoord.y = (Lc.c21_*NCoord.x + Lc.c22_*NCoord.y + Lc.c23_)
		      /(Lc.c31_*NCoord.x + Lc.c32_*NCoord.y + 1);

	return OCoord;
}

COORD2D<double> CEpipolar::Get_R_OriginalCoord_NormalCoord(COORD2D<double> NCoord)
{
	COORD2D<double> OCoord;
	OCoord.x = (Rc.c11_*NCoord.x + Rc.c12_*NCoord.y + Rc.c13_)
		      /(Rc.c31_*NCoord.x + Rc.c32_*NCoord.y + 1);
	OCoord.y = (Rc.c21_*NCoord.x + Rc.c22_*NCoord.y + Rc.c23_)
		      /(Rc.c31_*NCoord.x + Rc.c32_*NCoord.y + 1);

	return OCoord;
}

BOOL CEpipolar::NormalImageSize()
{
	//Determine Normalized Image Size
	
	//original photo coordinate
	COORD2D<double> Lsource_image, Lsource_photo;
	COORD2D<double> Rsource_image, Rsource_photo;
	//original image width and height
	int Lwidth, Lheight;
	int Rwidth, Rheight;
	//normalized image size(left-top, right-top, left-bottom, right-bottom)
	COORD2D<double> L_LT, L_RT, L_LB, L_RB;
	COORD2D<double> R_LT, R_RT, R_LB, R_RB;
		
	//Left
	Lwidth = LOriImage.GetWidth();
	Lheight = LOriImage.GetHeight();
		//left-top
	Lsource_image.x = 0; Lsource_image.y = 0;
	Lsource_photo = model.LPhoto.GetImageToPhotoCoord(Lsource_image);
	L_LT = Get_L_NormalCoord_Projective(Lsource_photo);
	min_Lx = L_LT.x;
	max_Lx = L_LT.x;
	max_y = L_LT.y;
	min_y = L_LT.y;
		//right-top
	Lsource_image.x = Lwidth; Lsource_image.y = 0;
	Lsource_photo = model.LPhoto.GetImageToPhotoCoord(Lsource_image);
	L_RT = Get_L_NormalCoord_Projective(Lsource_photo);
	if(min_Lx > L_RT.x) min_Lx = L_RT.x;
	if(max_Lx < L_RT.x) max_Lx = L_RT.x;
	if(max_y < L_RT.y) max_y = L_RT.y;
	if(min_y > L_RT.y) min_y = L_RT.y;
		//left-bottom
	Lsource_image.x = 0; Lsource_image.y = Lheight;
	Lsource_photo = model.LPhoto.GetImageToPhotoCoord(Lsource_image);
	L_LB = Get_L_NormalCoord_Projective(Lsource_photo);
	if(min_Lx > L_LB.x) min_Lx = L_LB.x;
	if(max_Lx < L_LB.x) max_Lx = L_LB.x;
	if(max_y < L_LB.y) max_y = L_LB.y;
	if(min_y > L_LB.y) min_y = L_LB.y;
		//right-bottom
	Lsource_image.x = Lwidth; Lsource_image.y = Lheight;
	Lsource_photo = model.LPhoto.GetImageToPhotoCoord(Lsource_image);
	L_RB = Get_L_NormalCoord_Projective(Lsource_photo);
	if(min_Lx > L_RB.x) min_Lx = L_RB.x;
	if(max_Lx < L_RB.x) max_Lx = L_RB.x;
	if(max_y < L_RB.y) max_y = L_RB.y;
	if(min_y > L_RB.y) min_y = L_RB.y;

	//Right
	Rwidth = ROriImage.GetWidth();
	Rheight = ROriImage.GetHeight();
		//lfet-top
	Rsource_image.x = 0; Rsource_image.y = 0;
	Rsource_photo = model.RPhoto.GetImageToPhotoCoord(Rsource_image);
	R_LT = Get_R_NormalCoord_Projective(Rsource_photo);
	min_Rx = R_LT.x;
	max_Rx = R_LT.x;
	if(max_y < R_LT.y) max_y = R_LT.y;
	if(min_y > R_LT.y) min_y = R_LT.y;
		//right-top
	Rsource_image.x = Rwidth; Rsource_image.y = 0;
	Rsource_photo = model.RPhoto.GetImageToPhotoCoord(Rsource_image);
	R_RT = Get_R_NormalCoord_Projective(Rsource_photo);
	if(min_Rx > R_RT.x) min_Rx = R_RT.x;
	if(max_Rx < R_RT.x) max_Rx = R_RT.x;
	if(max_y < R_RT.y) max_y = R_RT.y;
	if(min_y > R_RT.y) min_y = R_RT.y;
		//left-bottom
	Rsource_image.x = 0; Rsource_image.y = Rheight;
	Rsource_photo = model.RPhoto.GetImageToPhotoCoord(Rsource_image);
	R_LB = Get_R_NormalCoord_Projective(Rsource_photo);
	if(min_Rx > R_LB.x) min_Rx = R_LB.x;
	if(max_Rx < R_LB.x) max_Rx = R_LB.x;
	if(max_y < R_LB.y) max_y = R_LB.y;
	if(min_y > R_LB.y) min_y = R_LB.y;
		//right-bottom
	Rsource_image.x = Rwidth; Rsource_image.y = Rheight;
	Rsource_photo = model.RPhoto.GetImageToPhotoCoord(Rsource_image);
	R_RB = Get_R_NormalCoord_Projective(Rsource_photo);
	if(min_Rx > R_RB.x) min_Rx = R_RB.x;
	if(max_Rx < R_RB.x) max_Rx = R_RB.x;
	if(max_y < R_RB.y) max_y = R_RB.y;
	if(min_y > R_RB.y) min_y = R_RB.y;

	
	if((max_Lx - min_Lx) > (max_Rx - min_Rx))
		dmax = max_Lx - min_Lx;
	else
		dmax = max_Rx - min_Rx;
	if(dmax < (max_y - min_y))
	{
		dmax = max_y - min_y;
		resolution = dmax/(Lheight+Rheight)*2;
	}
	else
	{
		resolution = dmax/(Lwidth+Rwidth)*2;
	}

	LEpiWidth = int((max_Lx - min_Lx)/resolution);
	LEpiHeight = REpiHeight = int((max_y - min_y)/resolution);
	REpiWidth = int((max_Rx - min_Rx)/resolution);
		
	return TRUE;
}

COORD2D<double> CEpipolar::Get_L_NormalPhotoCoord_NormalImageCoord(COORD2D<double> NImage)
{
	COORD2D<double> NPhoto;
	NPhoto.x = min_Lx + (NImage.x * resolution);
	NPhoto.y = max_y - (NImage.y * resolution);
	return NPhoto;
}

COORD2D<double> CEpipolar::Get_R_NormalPhotoCoord_NormalImageCoord(COORD2D<double> NImage)
{
	COORD2D<double> NPhoto;
	NPhoto.x = min_Rx + (NImage.x * resolution);
	NPhoto.y = max_y - (NImage.y * resolution);
	return NPhoto;
}

COORD2D<double> CEpipolar::Get_L_NormalImageCoord_NormalPhotoCoord(COORD2D<double> NPhoto)
{
	COORD2D<double> NImage;
	NImage.x = double(NPhoto.x - min_Lx)/resolution;
	NImage.y = double(max_y - NPhoto.y)/resolution;
	return NImage;
}

COORD2D<double> CEpipolar::Get_R_NormalImageCoord_NormalPhotoCoord(COORD2D<double> NPhoto)
{
	COORD2D<double> NImage;
	NImage.x = (NPhoto.x - min_Rx)/resolution;
	NImage.y = (max_y - NPhoto.y)/resolution;
	return NImage;
}

Matrix<double> CEpipolar::GetRBMatrix()
{
	return RB;
}

Matrix<double> CEpipolar::Get_L_RTMatrix()
{
	return LRT;
}

Matrix<double> CEpipolar::Get_R_RTMatrix()
{
	return RRT;
}

Matrix<double> CEpipolar::Get_L_RNMatrix()
{
	return LRN;
}

Matrix<double> CEpipolar::Get_R_RNMatrix()
{
	return RRN;
}

BOOL CEpipolar::ResamplingL(CString Lepiname)
{
	int i,j;
	LEpiImage.Create(LEpiWidth, LEpiHeight, LPixelDepth);
	
	CPixelPtr LOPixel(LOriImage);
	
	CPixelPtr LEPixel(LEpiImage);
	
	//Left Epipolar Image Resampling
	for(i=0;i<LEpiHeight;i++)
	{
		for (j=0;j<LEpiWidth;j++)
		{
			COORD2D<double> NImage;
			COORD2D<double> NPhoto;
			COORD2D<double> OPhoto;
			COORD2D<double> OImage;

			NImage.x = j;
			NImage.y = i;
					
			NPhoto = Get_L_NormalPhotoCoord_NormalImageCoord(NImage);
			OPhoto = Get_L_OriginalCoord_NormalCoord(NPhoto);
			OImage = model.LPhoto.GetPhotoToImageCoord(OPhoto);

			//nearest neighborhood
			if((int)((OImage.y+0.5) < LOriImage.GetHeight()) && ((int)(OImage.x+0.5) < LOriImage.GetWidth()))
			{
				if((int)((OImage.y+0.5) > 0) && ((int)(OImage.x+0.5) > 0))
				{
					LEPixel[i][j] = LOPixel[(int)(OImage.y+0.5)][(int)(OImage.x+0.5)];
				}
				else
				{
					LEPixel[i][j] = (BYTE)0;
				}
			}
			else
			{
				LEPixel[i][j] = (BYTE)0;
			}
		}
	}
	
	LEpiImage.Save(Lepiname);

	return TRUE;
}

BOOL CEpipolar::ResamplingR(CString Repiname)
{
	int i,j;
	REpiImage.Create(REpiWidth, REpiHeight, RPixelDepth);

	CPixelPtr ROPixel(ROriImage);

	CPixelPtr REPixel(REpiImage);

	//Right Epipolar Image Resampling
	for(i=0;i<REpiHeight;i++)
	{
		for (j=0;j<REpiWidth;j++)
		{
			COORD2D<double> NImage;
			COORD2D<double> NPhoto;
			COORD2D<double> OPhoto;
			COORD2D<double> OImage;

			NImage.x = j;
			NImage.y = i;
			
			NPhoto = Get_R_NormalPhotoCoord_NormalImageCoord(NImage);
			OPhoto = Get_R_OriginalCoord_NormalCoord(NPhoto);
			OImage = model.RPhoto.GetPhotoToImageCoord(OPhoto);

			//nearest neighborhood
			if((int)((OImage.y+0.5) < ROriImage.GetHeight()) && ((int)(OImage.x+0.5) < ROriImage.GetWidth()))
			{
				if((int)((OImage.y+0.5) > 0) && ((int)(OImage.x+0.5) > 0))
				{
					REPixel[i][j] = ROPixel[(int)(OImage.y+0.5)][(int)(OImage.x+0.5)];
				}
				else
				{
					REPixel[i][j] = (BYTE)0;
				}
			}
			else
			{
				REPixel[i][j] = (BYTE)0;
			}
		}
	}
	
	REpiImage.Save(Repiname);


	return TRUE;
}