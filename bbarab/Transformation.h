/*
 * Copyright (c) 2001-2002, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

//////////////////////
//Transformation.h
//for Transformation
//made by Bang Ki In
//revision date 2000/05/27
//revision date 2000/08/03
//revision date 2001/02/22
//revision date 2001/03/13
//////////////////////
//Inverse변환 미완성....
//conformal, affine만 인버스 구현되있음...
//////////////////////////
//revision date 2001/07/5
//////////////////////
//Inverse변환 완성....
//모든 인버스 구현되있음...
//conformal, affine는 direct 계수를 이용한 계산이고
//나머지는 inverse 계수를 구하여 사용함.
//////////////////////////////
//revision date 2001/08/17
//////////////////////////
//인버스 변환: 모두 역변환 계수를 별도로 구하여 사용함...
///////////////////////////////////////////////////////////

#ifndef _TRANSFORMATION_
#define _TRANSFORMATION_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include<math.h>
#include "../bbarab/SMCoordinateClass.h"
#include "../bbarab/SMMatrixClass.h"
using namespace SMATICS_BBARAB;

class KnownPoint;
class Transformation;
class Conformal;
class Affine;
class Projective_linear;
class Projective_nonlinear;
class Bilinear;

/**************************/
//                        //
//class for Transformation
//                        //
/**************************/
//base class
class Transformation
{
public:
	Transformation() {}
	virtual ~Transformation() {}
	
public:
	////////////////
	//define matrix
	////////////////
	//AX = L + V
	CSMMatrix<double> A,X,L;
	CSMMatrix<double> I_A,I_X,I_L;
	CSMMatrix<double> V;
	CSMMatrix<double> I_V;
	//V_trans * V
	CSMMatrix<double> VTV;
	CSMMatrix<double> I_VTV;
	//N_inverse = (A_trans * A)_inverse
	CSMMatrix<double> Ninv;
	CSMMatrix<double> I_Ninv;
	//variance-covariance matrix
	CSMMatrix<double> VarCov;
	CSMMatrix<double> I_VarCov;
	//posteriori variance(observation value)
	CSMMatrix<double> Posteriori_La;
	CSMMatrix<double> I_Posteriori_La;
	//variance = VTV / DF
	double variance;
	double I_variance;
	//degree of freedom
	int DF;
	
public:
	virtual Point2D<double> CoordTransform(Point2D<double> P) {return P;}
	virtual Point2D<double> InverseCoordTransform(Point2D<double> Q) {return Q;}
};

/***********************************/
//                                 //
//class for conformal transformation
//                                 //
/***********************************/
//qx =  a.px + b.py + c
//qy = -b.px + a.py + d
/////////////////////////////////////
//4 unknown parameters : a, b, c, d
/////////////////////////////////////
//a -> S.cos(alpha)
//b -> S.sin(alpha)
//c -> shift x
//d -> shift y
/////////////////////////////////////
//S -> scale
//alpha -> rotation angle
/////////////////////////////////////
class Conformal:public Transformation
{
public:
	Conformal() {}
	Conformal(KnownPoint& p);
	
	virtual ~Conformal() {}
	//qx = a.px+b.py+c
	//qy = -b.px+a.py+d

	double a,b,c,d;//transformation parameter
	double I_a,I_b,I_c,I_d;//inverse transformation parameter
	//a->S.cos(alpha)
	//b->S.sin(alpha)
	//c->shift x
	//d->shift y

	BOOL Conformal_Transform(KnownPoint& p);
	Point2D<double> CoordTransform(Point2D<double> P);
	Point2D<double> InverseCoordTransform(Point2D<double> Q);
};

/********************************/
//                              //
//class for affine transformation
//                              //
/********************************/
//qx = a1.px + a2.py + c1
//qy = b1.px + b2.py + c2
//////////////////////////////////////
//a1 -> Sx.(cos(alpha)+delta.sin(alpha))
//a2 -> Sy.sin(alpha)
//b1 -> Sx(delta.cos(alpha)-sin(alpha))
//b2 -> Sy.cos(alpha)
//c1 -> shift x
//c2 -> shift y
//////////////////////////////////////
//S -> scale
//alpha -> rotation angle
//delta -> affinity
//////////////////////////////////////
class Affine:public Transformation
{
public:
	Affine() {}
	Affine(KnownPoint& p);
	virtual ~Affine() {}
	//qx = a1.px+a2.py+c1
	//qy = b1.px+b2.py+c2
	double a1,a2,b1,b2,c1,c2;//transformation parameter
	double I_a1,I_a2,I_b1,I_b2,I_c1,I_c2;//inverse transformation parameter
	//a1->Sx.(cos(alpha)+delta.sin(alpha))
	//a2->Sy.sin(alpha)
	//b1->Sx(delta.cos(alpha)-sin(alpha))
	//b2->Sy.cos(alpha)
	//c1->shift x
	//c2->shift y

	BOOL Affine_Transform(KnownPoint& p);
	Point2D<double> CoordTransform(Point2D<double> P);
	Point2D<double> InverseCoordTransform(Point2D<double> Q);
};

/********************************************/
//                                          //
//class for projective transformation(Linear)
//                                          //
/********************************************/
//qx = a1.px + b1.py + c1 - a3.px.qx - b3.py.qx
//qy = a2.px + b2.py + c2 - a3.px.qy - b3.py.qy
//////////////////////////////////////
class Projective_linear:public Transformation
{
public:
	Projective_linear() {}
	Projective_linear(KnownPoint& p);
	virtual ~Projective_linear() {}
	//qx = a1.px+b1.py+c1-a3.px.qx-b3.py.qx
	//qy = a2.px+b2.py+c2-a3.px.qy-b3.py.qy
	double a1,b1,c1,a2,b2,c2,a3,b3;//transformation parameter
	double I_a1,I_b1,I_c1,I_a2,I_b2,I_c2,I_a3,I_b3;//inverse transformation parameter
	
	BOOL Projective_linear_Transform(KnownPoint& p);
	Point2D<double> CoordTransform(Point2D<double> P);
	Point2D<double> InverseCoordTransform(Point2D<double> Q);
};

/*************************************************/
//                                               //
//class for projective transformation(non_Linear)
//                                               //
/*************************************************/
//qx = (a1.px+b1.py+c1)/(a3.px+b3.py+1)
//qy = (a2.px+b2.py+c2)/(a3.px+b3.py+1)
//////////////////////////////////////
class Projective_nonlinear : public Projective_linear
{
public:
	Projective_nonlinear() {}
	Projective_nonlinear(KnownPoint& p);
	virtual ~Projective_nonlinear() {}
	int iteration_num;
	double maxcorrection;
	//qx = (a1.px+b1.py+c1)/(a3.px+b3.py+1)
	//qy = (a2.px+b2.py+c2)/(a3.px+b3.py+1)
	BOOL Projective_nonlinear_Transform(KnownPoint& p);

};

/***********************************/
//                                 //
//class for bilinear transformation
//(pseudo affine transformation)
//                                 //
/***********************************/
//qx = a1.px+a2.py+a3.px.py+c1
//qy = b1.px+b2.py+b3.px.py+c2
//////////////////////////////////////
class Bilinear:public Transformation
{
public:
	Bilinear() {}
	Bilinear(KnownPoint& p);
	virtual ~Bilinear() {}
	double a1,a2,a3,b1,b2,b3,c1,c2;//transformation parameter
	double I_a1,I_a2,I_a3,I_b1,I_b2,I_b3,I_c1,I_c2;//inverse transformation parameter
	//qx = a1.px+a2.py+a3.px.py+c1
	//qy = b1.px+b2.py+b3.px.py+c2
	BOOL Bilinear_Transform(KnownPoint& p);	
	Point2D<double> CoordTransform(Point2D<double> P);
	Point2D<double> InverseCoordTransform(Point2D<double> Q);
};

/***********************************/
//                                 //
//class for rigid_bogy transformation
//(pseudo affine transformation)
//                                 //
/***********************************/
//qx = Sx.px+Tx
//qy = Sy.py+Ty
//////////////////////////////////////
class RigidBody:public Transformation
{
public:
	RigidBody() {}
	RigidBody(KnownPoint& p);
	virtual ~RigidBody() {}
	double Sx, Sy, Tx, Ty;//transformation parameter
	double I_Sx, I_Sy, I_Tx, I_Ty;//inverse transformation parameter
	BOOL RigidBody_Transform(KnownPoint& p);	
	Point2D<double> CoordTransform(Point2D<double> P);
	Point2D<double> InverseCoordTransform(Point2D<double> Q);
};

/**********************/
//class for known point
/**********************/
class KnownPoint
{
public:
	KnownPoint() {m_pPoint = NULL; m_qPoint = NULL;}
	KnownPoint(int num_point);
	virtual ~KnownPoint(); 
	
	int num;
	Point2D<double> *m_pPoint;
	Point2D<double> *m_qPoint;

	BOOL SetPointNum(int n);
	BOOL SetPoint(int index, double px, double py, double qx, double qy);
	BOOL SetPoint_p(int index, double px, double py);
	BOOL SetPoint_q(int index, double qx, double qy);
	inline Point2D<double> GetPoint_p(int n) {return m_pPoint[n];}
	inline Point2D<double> GetPoint_q(int n) {return m_qPoint[n];}
};

////////////////////////////////////////////////////////////////////
#endif