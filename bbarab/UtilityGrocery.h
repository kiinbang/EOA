/*
* Copyright (c) 2005-2006, KI IN Bang
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#if !defined(__INTERSOLUTION_BBARAB_UTILITY_GROCERY__)
#define __INTERSOLUTION_BBARAB_UTILITY_GROCERY__

// WGS84 Parameters
#define WGS84_A		6378137.0		// major axis
#define WGS84_B		6356752.31424518	// minor axis
#define WGS84_F		0.0033528107		// ellipsoid flattening
#define WGS84_E		0.0818191908		// first eccentricity
#define WGS84_EP	0.0820944379		// second eccentricity

// UTM Parameters
#define UTM_K0		0.9996			// scale factor
#define UTM_FE		500000.0		// false easting
#define UTM_FN_N	0.0           // false northing, northern hemisphere
#define UTM_FN_S	10000000.0    // false northing, southern hemisphere
#define UTM_E2		(WGS84_E*WGS84_E)	// e^2
#define UTM_E4		(UTM_E2*UTM_E2)		// e^4
#define UTM_E6		(UTM_E4*UTM_E2)		// e^6
#define UTM_EP2		(UTM_E2/(1-UTM_E2))	// e'^2

#include <fstream>
#include <math.h>
//#include <afx.h>
#include <string.h>
#include <stdlib.h>

using namespace std;

namespace SMATICS_BBARAB
{
	
#define MAX_LINE_LENGTH 512
#define PI GetPI() //3.141592654
	
	/**GetPI
	* Description	    : to get pi(3.14159265....)
	*@param void
	*@return inline double : PI
	*/
	inline double GetPI(void);
	
	/**Rad2Deg
	* Description	    : to convert the degree to the radian
	*@param double rad
	*@return inline double 
	*/
	inline double Rad2Deg(double rad);
	
	/**Deg2Rad
	* Description	    : to convert the radian to the degree
	*@param double deg
	*@return inline double 
	*/
	inline double Deg2Rad(double deg);
	
	/**RemoveCommentLine
	* Description	    : inline function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@return void 
	*/
	inline void RemoveCommentLine(fstream &file, char delimiter);
	
	/**RemoveCommentLine
	* Description	    : inline function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@param in num: number of delimiters
	*@return void 
	*/
	inline void RemoveCommentLine(fstream &file, char* delimiter, int num);
	
	/**RemoveCommentLine
	* Description	    : inline function for removing comments in the input file
	*@param FILE* file : FILE struct pointer
	*@param char delimiter: delimiter
	*@return void 
	*/
	inline void RemoveCommentLine(FILE* file, char delimiter);
	
	/**FindNumber
	* Description	    : inline function for removing delimiter in the input file
	*@param fstream file : file stream
	*@return void 
	*/
	inline void FindNumber(fstream &file);
	
	/**FindString
	* Description	    : inline function for finding given string in the input file
	*@param fstream file : file stream
	*@param char* target : target string
	*@return void 
	*/
	inline bool FindString(fstream &fin, char target[], int &result);
	
	/**FindString
	* Description	    : checking version of the given text file
	*@param fstream file : file stream
	*@return double: version number 
	*/
	inline double VersionCheck(fstream &fin);
	
	/**CalAzimuth
	* Description	    : calculate azimuth with 2 points
	*@param double Xa	: X coordinate
	*@param double Ya	: Y coordinate
	*@param double Xb	: X coordinate
	*@param double Yb	: Y coordinate
	*@return double: azimuth
	*/
	inline double CalAzimuth(double Xa, double Ya, double Xb, double Yb);
	
	/**MeridianConversion
	* Description	    : calculate azimuth with 2 points
	*@param double Lon	: longitude
	*@param double Lat	: latitude
	*@param double CenterLon	: longitude of center meridian
	*@return double: meridian conversion
	*/
	inline double MeridianConversion(double Lon, double Lat, double CenterLon);
	
	/**CentralMeridian
	* Description	    : calculate azimuth with 2 points
	*@param int zone	: UTM zone
	*@return double: central meridian (rad)
	*/
	inline double CentralMeridian(int zone);
	
	/**Line2DIntersect
	* Description	    :  find intersect point between two lines
	*@param double x1, y1 : given point of line
	*@param double x2, y2 : given point of line
	*@param double x3, y3 : given point of line
	*@param double x4, y4 : given point of line
	*@param double &x, &y : intersection point
	*@return int : -3 (parallel) -2(same) -1 (Not line) 0 (out of ranges) 1(intersection within ranges) 2 (end point)
	*/
	inline int Line2DIntersect(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double &x, double &y);
	
	/**Inside2DPolygon
	* Description	    :  topology between line and point
	*@param double *x, *y	: vertices
	*@param double n		  : number of vertices
	*@param double Tx, Ty	: target point
	*@return int	: -1 (not polygon) 0 (outside) 1 (inside)
	*/
	inline int Inside2DPolygon(double *x, double* y, int n, double Tx, double Ty);
	
	/**IntersectPlaneLine
	* Description	    :  intersection between a line and a plane
	*@param double *N: normal vector of a plane
	*@param double *V: a point on a plane
	*@param double *P1, P2: two point on a line
	*@param double *P: intersection point
	*@return bool: [false] //out of two point [true]
	*/
	inline bool IntersectPlaneLine(double *N, double *V, double *P1, double *P2, double *P);
	
	/**IntersectPointBetweenOnePointandOneLine
	* Description	    :  intersection between a line and a point
	*@param double *P0: given one point
	*@param double *PA, *PB: two end points of a line
	*@param double *P: intersection of P0 and a line
	*@return bool: false and true
	*/
	inline bool IntersectPointBetweenOnePointandOneLine(double *P0, double *Pa, double *Pb, double *P);
	
	/**Area_of_Polygon2D
	* Description	    :  area of a polygon
	*@param double *x: list of x coordinates
	*@param double *y: list of y coordinates
	*@param int n: number of vertices
	*@return double: area
	*/
	inline double Area_of_Polygon2D(double* x, double* y, int n);
	
	//Extract angle from normal vector
	/**Area_of_Polygon2D
	* Description	    :  area of a polygon
	*@param double *x: list of x coordinates
	*@param double *y: list of y coordinates
	*@param int n: number of vertices
	*@return double: area
	*/
	inline bool ExtractEulerAngles(double dX, double dY, double dZ, double &O, double &P);
	
	//Randomly select string lines from a txt file
	inline bool RandomTXTLIneExtraction(int numlines, char* ptspath, char* outpath);
	
	//File extention check
	inline bool ExtCheck(char *path, char* ext, bool bMatchCase);
	
	//Txt file open for reading
	inline bool ASCIIReadOpen(fstream &infile, char* path);
	
	//Txt file open for writing
	inline bool ASCIISaveOpen(fstream &outfile, char* path);
	
	//Binary file open for reading
	inline bool BINReadOpen(fstream &infile, char* path);
	
	//Binary file open for writing
	inline bool BINSaveOpen(fstream &outfile, char* path);
	
	//Precision of txt file setting
	inline void ASCIIFilePrecision(fstream &file, unsigned int precision);

	//WGS84 lat-lon to UTM East-North
	inline void UTM(double lat, double lon, double *x, double *y);

	//WGS84 lat-lon to UTM East-North
	inline void LLH2ENU_UTM(double *LLH_deg, double *ENU);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**GetPI
	* Description	    : to get pi(3.14159265....)
	*@param void
	*@return inline double : PI
	*/
	inline double GetPI(void)
	{
		return asin(1.0)*2;
	}

	/**Rad2Deg
	* Description	    : to convert the degree to the radian
	*@param double rad
	*@return inline double 
	*/
	inline double Rad2Deg(double rad)
	{
		double deg;
		deg = rad/GetPI()*180.0;
		if(deg>180.0) deg = deg - 360.0;
		else if(deg<-180.0) deg = deg + 360.0;
		return deg;
	}

	/**Deg2Rad
	* Description	    : to convert the radian to the degree
	*@param double deg
	*@return inline double 
	*/
	inline double Deg2Rad(double deg)
	{
		double rad;
		rad = deg/180.0*GetPI();
		if(rad>GetPI()) rad = rad - 2*GetPI();
		else if(rad<-GetPI()) rad = rad + 2*GetPI();
		return rad;
	}

	/**RemoveCommentLine
	* Description	    : inline function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@return void 
	*/
	inline void RemoveCommentLine(fstream &file, char delimiter)
	{
		char comment = delimiter;

		char line[MAX_LINE_LENGTH];
		
		int bstop = false; 
		do 
		{ 
			file>>ws;
			
			if ( file.peek() == comment )	//comment line
			{
				file.getline(line,MAX_LINE_LENGTH);  
				bstop = false;
			}
			else
				bstop = true;
			
			
			file>>ws;
			
		}while( (!file.eof()) &&  (bstop==false) ); 

		file>>ws;
	}

	/**RemoveCommentLine
	* Description	    : inline function for removing comments in the input file
	*@param fstream file : file stream
	*@param char delimiter: delimiter
	*@param in num: number of delimiters
	*@return void 
	*/
	inline void RemoveCommentLine(fstream &file, char* delimiter, int num)
	{
		char line[MAX_LINE_LENGTH];
		
		int bstop = true; 
		do 
		{
			bstop = true; 

			file>>ws;
			char target = file.peek();

			for(int i=0; i<num; i++)
			{
				char comment = delimiter[i];
				if (target == comment )	//comment line
				{
					file.getline(line,MAX_LINE_LENGTH);
					bstop = false;
				}
			}
			
			file>>ws;
			
		}while( (!file.eof()) &&  (bstop==false) ); 

		file>>ws;
	}

	/**RemoveCommentLine
	* Description	    : inline function for removing comments in the input file
	*@param FILE* file : FILE struct pointer
	*@param char delimiter: delimiter
	*@return void 
	*/
	inline void RemoveCommentLine(FILE* file, char delimiter)
	{
		char comment = delimiter;
		char line[MAX_LINE_LENGTH];
		
		bool bstop = false;
		char temp;
		do 
		{ 
			fscanf(file,"%c",&temp);
			if (temp == comment)//comment line
			{
				fgets(line,MAX_LINE_LENGTH,file);
				bstop = false;
			}
			//back space, tap, line feed, carriage return, Space
			else if((8 == temp)||(9 == temp)||(10 == temp)||(13 == temp)||(32 == temp))
				bstop = false;
			else if(temp == EOF)
				bstop = true;
			else
			{
				fseek(file,(long)-1, SEEK_CUR);
				bstop = true;
			}
			
		}while(bstop==false); 
	}

	/**FindNumber
	* Description	    : inline function for removing delimiter in the input file
	*@param fstream file : file stream
	*@return void 
	*/
	inline void FindNumber(fstream &file)
	{
		char temp;
		int bstop = false;
		do 
		{ 
			//48~57: number
			file>>ws;
			file>>temp;
			if ((48<=temp)&&(temp<=57))
			{
				file.seekg(-1,ios::cur);
				bstop = true;
			}
			//+ or - symbol
			else if((45==temp)||(43==temp))
			{
				file>>temp;
				//48~57: number
				if ((48<=temp)&&(temp<=57))
				{
					file.seekg(-2,ios::cur);
					bstop = true;
				}
			}
			else
				bstop = false;
			
		}while((!file.eof()) && (bstop==false)); 
	}

	/**FindString
	* Description	    : inline function for finding given string in the input file
	*@param fstream file : file stream
	*@param char* target : target string
	*@return void 
	*/
	inline bool FindString(fstream &fin, char target[], int &result)
	{
		char string[MAX_LINE_LENGTH];
		if(fin.getline(string, MAX_LINE_LENGTH))
		{
			char *pdest =  NULL;
			pdest = strstr(string, target);
			result = pdest - string + 1;
			if(pdest != NULL)
				return true;
			else
				return false;
		}
		else
			return false;

		
	}

	/**VersionCheck
	* Description	    : checking version of the given text file
	*@param fstream file : file stream
	*@return double: version number 
	*/
	inline double VersionCheck(fstream &fin)
	{
		//version check
		int pos;
		
		double ver_num = 1.0;
		
		bool bVersion = FindString(fin, "VER", pos);
		
		if(bVersion == true)
		{
			//To move to begin of file
			fin.seekg(0,ios::beg);
			char temp[256];//"VER"
			fin>>temp>>ver_num;//version
		}
		else
		{
			//To move to begin of file
			fin.seekg(0,ios::beg);//old version without version number(before ver 1.1)
		}
		
		return ver_num;
	}
	
	/**CalAzimuth
	* Description	    : calculate azimuth with 2 points
	*@param double Xa	: X coordinate
	*@param double Ya	: Y coordinate
	*@param double Xb	: X coordinate
	*@param double Yb	: Y coordinate
	*@return double: azimuth
	*/
	inline double CalAzimuth(double Xa, double Ya, double Xb, double Yb)
	{
		double Azi;
		
		double dX = Xb - Xa;
		double dY = Yb - Ya;
		
		Azi = atan2(dX, dY);
		
		return Azi;
	}

	/**MeridianConversion
	* Description	    : calculate meridian conversion
	*@param double Lon	: longitude
	*@param double Lat	: latitude
	*@param double CenterLon	: longitude of center meridian
	*@return double: meridian conversion
	*/
	inline double MeridianConversion(double Lon, double Lat, double CenterLon)
	{
		double MC;
		MC = (CenterLon - Lon)*sin(Lat);
		return MC;
	}

	/**CentralMeridian
	* Description	    : central meridian
	*@param int zone	: UTM zone
	*@return double: central meridian (rad)
	*/
	inline double CentralMeridian(int zone)
	{
		double centmeri;
		if(zone<=30)
			centmeri = (double)zone*6. + 180. - 3.;
		else
			centmeri = (double)zone*6. - 180. - 3.;
		
		return Deg2Rad(centmeri);
	}
	
	/**Line2DIntersect
	* Description	    :  find intersect point between two lines
	*@param double x1, y1 : given point of line
	*@param double x2, y2 : given point of line
	*@param double x3, y3 : given point of line
	*@param double x4, y4 : given point of line
	*@param double &x, &y : intersection point
	*@return int : -3 (parallel) -2(same) -1 (Not line) 0 (out of ranges) 1(intersection within ranges) 2 (end point)
	*/
	inline int Line2DIntersect(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double &x, double &y)
	{
		//1 = ax + by
		
		double a1=0, b1=0, a2=0, b2=0;
		
		double dx12 = x2-x1;
		double dy12 = y2-y1;
		double dx34 = x4-x3;
		double dy34 = y4-y3;
		
		//
		//Specific lines (No intersection)
		/////////////////////////////////////
		
		//-3 (parallel) -2(same) -1 (Not line) 
		
		if((dx12 == 0)&&(dy12 == 0))
			return -1;//Not line
		
		if((dx34== 0)&&(dy34 == 0))
			return -1;//Not line
		
		if((dx12 == 0)&&(dx34 == 0))//(parallel to y axis)
		{
			if(x1 == x3)
				return -2;//same lines
			else
				return -3;//parallel lines
		}
		
		if((dy12 == 0)&&(dy34 == 0))//(parallel to x axis)
		{
			if(y1 == y3)
				return -2;//same lines
			else
				return -3;//parallel lines
		}
		
		//
		//Specific lines (intersection)
		/////////////////////////////////////
		
		if((dx12 == 0)&&(dy34 == 0))
		{
			x = x1; y = y2;
		}
		
		if((dy12 == 0)&&(dx34 == 0))
		{
			x = x3; y = y1;
		}
		
		if((dx12 != 0)&&(dy12 != 0))
		{
			a1 = dy12/dx12;
			b1 = y1 - a1*x1;
			
			if(dx34 == 0)
			{
				x = x3;
				y = a1*x3 + b1;
			}
			else if(dy34 == 0)
			{
				y = y3;
				x = (y3 - b1)/a1;
			}
		}
		
		if((dx34 != 0)&&(dy34 != 0))
		{
			a2 = dy34/dx34;
			b2 = y3 - a2*x3;
			
			if(dx12 == 0)
			{
				x = x1;
				y = a2*x1 + b2;
			}
			else if(dy12 == 0)
			{
				y = y1;
				x = (y1 - b2)/a2;
			}
		}
		
		//
		//Normal lines (intersection)
		/////////////////////////////////////
		
		//-3 (parallel) -2(same) -1 (Not line)
		
		if((dx12 != 0)&&(dy12 != 0)&&(dx34 != 0)&&(dy34 != 0))
		{
			if(a1==a2)
			{
				if(b1==b2)
					return -2;//same lines
				else
					return -3;//parallel lines
			}
			
			x = (b2-b1)/(a1-a2);
			y = a1*x + b1;
		}
		
		//
		//Location of the intersection point
		/////////////////////////////////////
		double dx1, dx2, dx3, dx4;
		double dy1, dy2, dy3, dy4;
		
		bool on_line_1 = false;
		bool on_line_2 = false;
		bool end_point = false;
		
		dx1 = x1 - x;
		dx2 = x2 - x;
		dx3 = x3 - x;
		dx4 = x4 - x;
		
		dy1 = y1 - y;
		dy2 = y2 - y;
		dy3 = y3 - y;
		dy4 = y4 - y;
		
		if(dx1*dx2 <= 0 && dy1*dy2 <= 0)
			on_line_1 = true;
		
		if(dx3*dx4 <= 0 && dy3*dy4 <= 0)
			on_line_2 = true;
		
		if( (dx1 == 0 && dy1 == 0) || (dx2 == 0 && dy2 == 0) || (dx3 == 0 && dy3 == 0) || (dx4 == 0 && dy4 == 0))
			end_point = true;
		
		//0 (out of ranges) 
		//1 (intersection within ranges)
		//2 (end point)
		
		if(on_line_1==true && on_line_2==true)
		{
			if(end_point == true)
				return 2;
			else
				return 1;
		}
		else
			return 0;

	};

	/**Inside2DPolygon
	* Description	    :  topology between line and point
	*@param double *x, *y	: vertices
	*@param double n		  : number of vertices
	*@param double Tx, Ty	: target point
	*@return int	: -1 (not polygon) 0 (outside) 1 (inside)
	*/
	inline int Inside2DPolygon(double *x, double* y, int n, double Tx, double Ty)
	{
		if(n < 3) return -1;

		int i;
		int count = 0;
		
		double min_x, same_y;	
		same_y   = Ty;
		min_x = -1.0e20;

		int n_end_point = 0;
		
		for(i=0;i<n;i++)
		{
			double x3, y3, x4, y4;
			
			if(i != n-1)
			{
				x3 = x[i]; 
				y3 = y[i];
				x4 = x[i+1]; 
				y4 = y[i+1];
			}
			else
			{
				x3 = x[i]; 
				y3 = y[i];
				x4 = x[0]; 
				y4 = y[0];
			}
			
			double x, y;
			int res= Line2DIntersect(Tx, Ty, min_x, same_y, x3, y3, x4, y4, x, y);
			if(0 < res)
				count++;
			if(2 == res)
				n_end_point ++;
		}

		if((count - n_end_point/2)%2 == 0) return 0;
		else return 1;
	}
	
	inline bool IntersectPlaneLine(double *N, double *V, double *P1, double *P2, double *P)
	{
		//Normal vector: N
		//a plane including point V

		//�������Ͱ� N�̰� �� P3�� ������ ������ �� P�� ������ ����� ������
		//N.(P-P3)=0
		
		//��P1�� P2�� ������ ������ ������
		//P=P1+u(P2-P1)
		
		//���� ������ ����
		//u = ((P3-P1).N)/((P2-P1).N)
		//u�� ������ �Ŀ� �����Ͽ� ã�´�.
		
		//Point3D<double> N(patch.coeff[0], patch.coeff[1], patch.coeff[2]);
		//Point3D<double> P3(patch.Vertex[0], patch.Vertex[1], patch.Vertex[2]);

		double VP1[3];
		VP1[0] = V[0] - P1[0];
		VP1[1] = V[1] - P1[1];
		VP1[2] = V[2] - P1[2];
		
		double P21[3];
		P21[0] = P2[0] - P1[0];
		P21[1] = P2[1] - P1[1];
		P21[2] = P2[2] - P1[2];

		double numerator = VP1[0]*N[0] + VP1[1]*N[1] + VP1[2]*N[2];
		double denominator = P21[0]*N[0] + P21[1]*N[1] + P21[2]*N[2];

		double ZERO = 10e-99;
		if(fabs(denominator) < ZERO) return false;
		
		double u = numerator / denominator;

		// (P1)---------(P2)-------------(P)
		if( fabs(u) > 1.0 ) return false; //out of two points
		
		P[0] = P1[0] + P21[0]*u;
		P[1] = P1[1] + P21[1]*u;
		P[2] = P1[2] + P21[2]*u;

		// (P)----------(P1)-------------(P2)
		double P1P[3];
		P1P[0] = P1[0] - P[0];
		P1P[1] = P1[1] - P[1];
		P1P[2] = P1[2] - P[2];
		double P1P2[3];
		P1P2[0] = P1[0] - P2[0];
		P1P2[1] = P1[1] - P2[1];
		P1P2[2] = P1[2] - P2[2];
		double temp = P1P[0]*P1P2[0] + P1P[1]*P1P2[1] + P1P[2]*P1P2[2];
		if( temp<0.0 ) return false; //out of two points 
		
		return true;
	}

	inline bool IntersectPointBetweenOnePointandOneLine(double *P0, double *Pa, double *Pb, double *P)
	{
		double Pba[3];
		double P0a[3];

		//Shift
		Pba[0] = Pb[0] - Pa[0];
		Pba[1] = Pb[1] - Pa[1];
		Pba[2] = Pb[2] - Pa[2];

		P0a[0] = P0[0] - Pa[0];
		P0a[1] = P0[1] - Pa[1];
		P0a[2] = P0[2] - Pa[2];

		double numerator	= Pba[0]*P0a[0] + Pba[1]*P0a[1] + Pba[2]*P0a[2];
		double denominator	= Pba[0]*Pba[0] + Pba[1]*Pba[1] + Pba[2]*Pba[2];
		
		if(fabs(denominator) < 1.0e-20)
			return false;

		double n = numerator/denominator;

		P[0] = Pa[0] + n*Pba[0];
		P[1] = Pa[1] + n*Pba[1];
		P[2] = Pa[2] + n*Pba[2];
		
		return true;
	}

	//return: area of polygon
	inline double Area_of_Polygon2D(double* x, double* y, int n)
	{
		double area = 0;
		int i;
		
		for (i=1;i<n-1;i++)
		{
			area += x[i]* (y[i+1] - y[i-1]);
		}
		area += x[n-1] * (y[0] - y[n-2]); 
		area += x[0]   * (y[1] - y[n-1]);
		
		return area / 2.0;
	}

	//Extract angle from normal vector
	inline bool ExtractEulerAngles(double dX, double dY, double dZ, double &O, double &P)
	{	
		if(dX == 0 && dY == 0 && dZ == 0) return false;

		double dYZ = sqrt(dY*dY + dZ*dZ);
		O = -atan2(dY, dZ);
		P = atan2(dX, dYZ);

		return true;
	}

	inline bool RandomTXTLIneExtraction(int numlines, char* ptspath, char* outpath)
	{
		//
		//File open
		//
		fstream PTSFile;//PTS file 
		PTSFile.open(ptspath, ios::in);
		
		//
		//Output file
		//
		fstream outfile;
		outfile.open(outpath, ios::out);
		
		int count = 0;
		char line[512];

		while(!PTSFile.eof())
		{
			int rLine = rand();
			rLine += int (rLine/RAND_MAX*numlines + 0.5);

			//Skip lines
			for(int i=0; i<rLine; i++)
			{
				if(!PTSFile.eof())
				{
					PTSFile.getline(line, 512);
					PTSFile>>ws;
				}
				else
				{
					break;
				}
			}
			
			if(!PTSFile.eof())
			{
				PTSFile.getline(line, 512);
				PTSFile>>ws;
				outfile<<line<<endl;
				outfile.flush();
				count++;
			}
			else
			{
				break;
			}
		}
		
		outfile.close();
		
		PTSFile.close();
		
		return true;
	}

	inline bool ExtCheck(char *path, char* ext, bool bMatchCase)
	{
		char *ext_temp = new char[512];// = _strupr(ext);
		ext_temp = strcpy(ext_temp, ext);
		if(bMatchCase == false) ext_temp = _strupr(ext_temp);
		char *path_temp = new char[512];
		path_temp = strcpy(path_temp, path);
		if(bMatchCase == false) path_temp = _strupr(path_temp);
		
		int length = strlen(path_temp);
		char ext_path[5];
		strncpy(ext_path, &path_temp[length+1-5], 5);
		int res = strcmp(ext_path,ext_temp);
		
		if(res == 0) return true;
		else return false;
	}

	inline bool ASCIIReadOpen(fstream &infile, char* path)
	{
		infile.open(path, ios::in);
		if(!infile)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	inline bool ASCIISaveOpen(fstream &outfile, char* path)
	{
		outfile.open(path, ios::out);
		if(!outfile)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	inline bool BINReadOpen(fstream &infile, char* path)
	{
		infile.open(path, ios::in|ios::binary);
		if(!infile)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	inline bool BINSaveOpen(fstream &outfile, char* path)
	{
		outfile.open(path, ios::out|ios::binary);
		if(!outfile)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	inline void ASCIIFilePrecision(fstream &file, unsigned int precision)
	{
		file.setf(ios::fixed, ios::floatfield);

		file.precision(precision);
	}

	void UTM(double lat, double lon, double *x, double *y)
	{
		double RADIANS_PER_DEGREE = acos(-1.0)/180.0;
		// constants
		const static double m0 = (1 - UTM_E2/4 - 3*UTM_E4/64 - 5*UTM_E6/256);
		const static double m1 = -(3*UTM_E2/8 + 3*UTM_E4/32 + 45*UTM_E6/1024);
		const static double m2 = (15*UTM_E4/256 + 45*UTM_E6/1024);
		const static double m3 = -(35*UTM_E6/3072);

		// compute the central meridian
		int cm = ((lon >= 0.0)
			? ((int)lon - ((int)lon)%6 + 3)
			: ((int)lon - ((int)lon)%6 - 3));

		// convert degrees into radians
		double rlat = lat * RADIANS_PER_DEGREE;
		double rlon = lon * RADIANS_PER_DEGREE;
		double rlon0 = cm * RADIANS_PER_DEGREE;

		// compute trigonometric functions
		double slat = sin(rlat);
		double clat = cos(rlat);
		double tlat = tan(rlat);

		// decide the false northing at origin
		double fn = (lat > 0) ? UTM_FN_N : UTM_FN_S;

		double T = tlat * tlat;
		double C = UTM_EP2 * clat * clat;
		double A = (rlon - rlon0) * clat;
		double M = WGS84_A * (m0*rlat + m1*sin(2*rlat)
			+ m2*sin(4*rlat) + m3*sin(6*rlat));
		double V = WGS84_A / sqrt(1 - UTM_E2*slat*slat);

		// compute the easting-northing coordinates
		*x = UTM_FE + UTM_K0 * V * (A + (1-T+C)*pow(A,3)/6
			+ (5-18*T+T*T+72*C-58*UTM_EP2)*pow(A,5)/120);
		*y = fn + UTM_K0 * (M + V * tlat * (A*A/2
			+ (5-T+9*C+4*C*C)*pow(A,4)/24
			+ ((61-58*T+T*T+600*C-330*UTM_EP2)
			* pow(A,6)/720)));

		return;
	};

	void LLH2ENU_UTM(double *LLH_deg, double *ENU)
	{
		ENU[2] = LLH_deg[2];

		UTM(LLH_deg[0], LLH_deg[1], &ENU[0], &ENU[1]);
	};

}//namespace SMATICS_BBARAB

#endif // !defined(__INTERSOLUTION_BBARAB_UTILITY_GROCERY__)