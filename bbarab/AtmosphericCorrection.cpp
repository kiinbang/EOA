#include "AtmosphericCorrection.h"

AtmosphericCorrectionModel::AtmosphericCorrectionModel(const double temperatureH0, const double h0/*Height given temperature*/, const double pMSL, const double waveLength) :
	m_groundTemperature(temperatureH0),
	m_temperatureKlvMSL(temperatureH0 + 273.15 + tempGradient*h0),
	m_airPressureMSL(pMSL),
	m_waveLength(waveLength),
	m_waveNum(1.0 / (waveLength*1.0e-7)),
	m_C2((273.2 + 526.3*v1*v1 / (v1*v1 - m_waveNum*m_waveNum) + 11.69*v2*v2 / (v2*v2 - m_waveNum*m_waveNum)))
{
};

AtmosphericCorrectionModel::~AtmosphericCorrectionModel() {};

Eigen::Vector3d AtmosphericCorrectionModel::calRefractiveVectorNED(const double travelTime, const double FH, const double latitudeRad, const double& lasDirVecN, const double& lasDirVecE, const double& lasDirVecD)
{
	Eigen::Vector3d posENU = calRefractiveVector(travelTime, FH, latitudeRad, lasDirVecE, lasDirVecN, -lasDirVecD);
	Eigen::Vector3d posNED(posENU(1), posENU(0), -posENU(2));
	return posNED;
}

Eigen::Vector3d AtmosphericCorrectionModel::calRefractiveVector(const double travelTime, const double FH, const double latitudeRad, const double& lasDirVecE, const double& lasDirVecN, const double& lasDirVecU)
{
	Eigen::Vector3d laserDirectionVecENU(lasDirVecE, lasDirVecN, lasDirVecU);

	//gravity
	const double gphi = 9.780356*(1 + 0.0052885*(sin(latitudeRad)*sin(latitudeRad)) - 0.0000059*(sin(2.*latitudeRad)*sin(2.*latitudeRad)));
	const double C1 = gphi*molecularWeight / (univGasConst*tempGradient);
	// initial temperature
	double T = m_temperatureKlvMSL + tempGradient * FH;
	// initial air pressure
	double pressure = m_airPressureMSL * pow((m_temperatureKlvMSL / T), C1);
	//initial refraction index
	double n1 = 1.0 + m_C2*pressure / T*1e-6;
	//half of speed of light, travelTime(round trip time)
	double range0 = vt0 * travelTime;

	//Eigen::Vector3d s1 = laserENUVec;
	//position calculated using the nominal index, na0
	Eigen::Vector3d position0 = laserDirectionVecENU*range0 + Eigen::Vector3d(0, 0, FH);
	Eigen::Vector3d N(0., 0., 1.);

	//start the iteration
	double t = travelTime;
	Eigen::Vector3d laserFiringPoint(0., 0., FH);
	Eigen::Vector3d position = laserFiringPoint;
	while (t > 0)
	{
		double dt;

		if (t > deltat)
			dt = deltat;
		else
			dt = t;

		//get the mid point at the current slice
		Eigen::Vector3d midPosition;//calculate the mid point of the current slice
		midPosition = position + laserDirectionVecENU * (dt / 2 * mHalfSpeedOfLight / n1);
		//update the refraction index for the current slice
		double T = m_temperatureKlvMSL + tempGradient * midPosition(2); //temperature at the height of H
		pressure = m_airPressureMSL*pow((m_temperatureKlvMSL / T), C1);
		double n2 = 1.0 + m_C2*pressure / T*1e-6;
		//updated laser beam vector in time dt/2
		double n1n2 = n1 / n2;
		Eigen::Vector3d Nci = N.cross(laserDirectionVecENU);
		laserDirectionVecENU = n1n2 * (N.cross(-Nci)) - N * sqrt(1.0 - (n1n2*n1n2)*(Nci.dot(Nci)));
		//move to the next slice
		t = t - dt;
		n1 = n2;
		//update the position
		position = position + laserDirectionVecENU * (dt * mHalfSpeedOfLight / n2);
	}

	Eigen::Vector3d laserVec = position - laserFiringPoint;

	return laserVec;
}