//////////////////////
//Transformation.cpp
//for Transformation
//made by Bang Ki In
//revision date 2000/05/27
//revision date 2000/08/03
//revision date 2001/02/22
//revision date 2001/03/13
//////////////////////
//Inverse변환 미완성....
//conformal, affine만 인버스 구현되있음...
//////////////////////////
//revision date 2001/07/5
//////////////////////
//Inverse변환 완성....
//모든 인버스 구현되있음...
//conformal, affine는 direct 계수를 이용한 계산이고
//나머지는 inverse 계수를 구하여 사용함.
//////////////////////////////
//revision date 2001/08/17
//////////////////////////
//인버스 변환: 모두 역변환 계수를 별도로 구하여 사용함...
///////////////////////////////////////////////////////////
//////////////////////////////
//revision date 2002/01/04
//////////////////////////
//비선형 투영변환 모듈에서 버그 수정함.
//반복계산 과정중에서 미지수를(X(i)) 갱신하기 위한
//순환문이 잘못된 인덱스를 사용함으로 오류를 수정함.
///////////////////////////////////////////////////////////


#include"stdafx.h"
#include<math.h>
#include"Transformation.h"

//Conformal
/////////////////////////////////////////////////////////////////////////
Conformal::Conformal(KnownPoint& p)
{
	Conformal_Transform(p);
}

BOOL Conformal::Conformal_Transform(KnownPoint& p)
{
	//A_trans
	CSMMatrix<double> AT;
	//(A_trans * A)_inverse
	CSMMatrix<double> ATAinv;
	//A_trans * L
	CSMMatrix<double> ATL;
	//AX = A * X
	CSMMatrix<double> AX;
	
	int i;
	
	//matrix(A, L) resizing
	A.Resize(p.num*2,4);
	L.Resize(p.num*2);
	
	//A matrix
	for(i=0;i<p.num;i++)
	{
		double x,y;
		x = p.GetPoint_p(i).x;
		y = p.GetPoint_p(i).y;
		
		//A matrix
		A(i*2,0) =x;
		A(i*2,1) =y;
		A(i*2,2) = 1;
		A(i*2,3) = 0;
		A(i*2+1,0) =y;
		A(i*2+1,1) =-x;
		A(i*2+1,2) =0;
		A(i*2+1,3) =1;
	}
	//L matrix
	for(i=0;i<p.num;i++)
	{
		double x,y;
		x = p.GetPoint_q(i).x;
		y = p.GetPoint_q(i).y;
		L(i*2) = x;
		L(i*2+1) = y;
	}
	
	AT = A.Transpose();
	ATAinv = (AT%A).Inverse();
	ATL = AT%L;
	X = ATAinv%ATL;
	
	a = X(0);
	b = X(1);
	c = X(2);
	d = X(3);
	
	AX = A%X;
	V = L - AX;
	VTV = V.Transpose()%V;
	DF = p.num*2 - 4;
	variance = VTV(0)/(double)DF;
	Ninv = ATAinv;
	VarCov = Ninv*variance;
	Posteriori_La = (A%Ninv%AT)*variance;

	//////////////////////////////////////////////////////////////////////////
	//Inverse Transform
	//////////////////////////////////////////////////////////////////////////
	//A_trans
	CSMMatrix<double> I_AT;
	CSMMatrix<double> I_ATAinv;
	CSMMatrix<double> I_ATL;
	CSMMatrix<double> I_AX;
	
	//matrix(A, L) resizing
	I_A.Resize(p.num*2,4);
	I_L.Resize(p.num*2);
	
	//A matrix
	for(i=0;i<p.num;i++)
	{
		double x,y;
		x = p.GetPoint_q(i).x;
		y = p.GetPoint_q(i).y;
		
		//A matrix
		I_A(i*2,0) =x;
		I_A(i*2,1) =y;
		I_A(i*2,2) = 1;
		I_A(i*2,3) = 0;
		I_A(i*2+1,0) =y;
		I_A(i*2+1,1) =-x;
		I_A(i*2+1,2) =0;
		I_A(i*2+1,3) =1;
	}
	//L matrix
	for(i=0;i<p.num;i++)
	{
		double x,y;
		x = p.GetPoint_p(i).x;
		y = p.GetPoint_p(i).y;
		I_L(i*2) = x;
		I_L(i*2+1) = y;
	}
	
	I_AT = I_A.Transpose();
	I_ATAinv = (I_AT%I_A).Inverse();
	I_ATL = I_AT%I_L;
	I_X = I_ATAinv%I_ATL;
	
	I_a = I_X(0);
	I_b = I_X(1);
	I_c = I_X(2);
	I_d = I_X(3);
	
	I_AX = I_A%I_X;
	I_V = I_L - I_AX;
	I_VTV = I_V.Transpose()%I_V;
	I_variance = I_VTV(0)/(double)DF;
	I_Ninv = I_ATAinv;
	I_VarCov = I_Ninv*I_variance;
	I_Posteriori_La = (I_A%I_Ninv%I_AT)*I_variance;
	
	return TRUE;
}

Point2D<double> Conformal::CoordTransform(Point2D<double> P)
{
	Point2D<double> Q;
	Q.x =  a*P.x + b*P.y + c;
	Q.y = -b*P.x + a*P.y + d;
	return Q;
}

Point2D<double> Conformal::InverseCoordTransform(Point2D<double> Q)
{
	Point2D<double> P;
	//Q.x =  a*P.x + b*P.y + c;
	//Q.y = -b*P.x + a*P.y + d;
	if((a*a+b*b) == 0.0)
	{
		P.x = (a*Q.x - b*Q.y + b*d - a*c)/(0.1e99);
	}
	else
	{
		P.x = (a*Q.x - b*Q.y + b*d - a*c)/(a*a + b*b);
		//P.y = (1/a - b*b/(a*a*a + a*b*b))*Q.y + (a*b/(a*a*a+a*b*b))*Q.x + (b*b*d-a*b*c)/(a*a*a+a*b*b) - d/a;
	}

	if(b == 0)
	{
		P.y = (Q.x-a*P.x-c)/(0.1e99);
	}
	else
	{
		P.y = (Q.x-a*P.x-c)/b;
	}
	
	return P;
}

//Affine
/////////////////////////////////////////////////////////////////////////
Affine::Affine(KnownPoint& p)
{
	Affine_Transform(p);
}

BOOL Affine::Affine_Transform(KnownPoint& p)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATAinv;
	CSMMatrix<double> ATL;
	
	int i;
	
	//matrix(A, L) resizing
	A.Resize(p.num*2,6);
	L.Resize(p.num*2);
	
	//A matrix
	for(i=0;i<p.num;i++)
	{
		A(i*2,0) = p.GetPoint_p(i).x;
		A(i*2,1) = p.GetPoint_p(i).y;
		A(i*2,2) = 0;
		A(i*2,3) = 0;
		A(i*2,4) = 1;
		A(i*2,5) = 0;
		A(i*2+1,0) =0;
		A(i*2+1,1) =0;
		A(i*2+1,2) =p.GetPoint_p(i).x;
		A(i*2+1,3) =p.GetPoint_p(i).y;
		A(i*2+1,4) =0;
		A(i*2+1,5) =1;
	}
	
	//L matrix
	for(i=0;i<p.num;i++)
	{
		L(i*2) = p.GetPoint_q(i).x;
		L(i*2+1) = p.GetPoint_q(i).y;
	}
	
	AT = A.Transpose();
	ATAinv = (AT%A).Inverse();
	ATL = AT%L;
	X = ATAinv%ATL;
	
	a1 = X(0);
	a2 = X(1);
	b1 = X(2);
	b2 = X(3);
	c1 = X(4);
	c2 = X(5);
	
	AX = A%X;
	V = L - AX;
	VTV = V.Transpose()%V;
	DF = p.num*2 - 6;
	variance = VTV(0)/(double)DF;
	Ninv = ATAinv;
	VarCov = Ninv*variance;
	Posteriori_La = A%Ninv%AT;
	Posteriori_La = Posteriori_La*variance;

	//////////////////////////////////////////////////////////////////////////
	//Inverse Transform
	//////////////////////////////////////////////////////////////////////////
	CSMMatrix<double> I_AX;
	CSMMatrix<double> I_AT;
	CSMMatrix<double> I_ATAinv;
	CSMMatrix<double> I_ATL;
	
	//matrix(A, L) resizing
	I_A.Resize(p.num*2,6);
	I_L.Resize(p.num*2);
	
	//A matrix
	for(i=0;i<p.num;i++)
	{
		I_A(i*2,0) = p.GetPoint_q(i).x;
		I_A(i*2,1) = p.GetPoint_q(i).y;
		I_A(i*2,2) = 0;
		I_A(i*2,3) = 0;
		I_A(i*2,4) = 1;
		I_A(i*2,5) = 0;
		I_A(i*2+1,0) =0;
		I_A(i*2+1,1) =0;
		I_A(i*2+1,2) =p.GetPoint_q(i).x;
		I_A(i*2+1,3) =p.GetPoint_q(i).y;
		I_A(i*2+1,4) =0;
		I_A(i*2+1,5) =1;
	}
	
	//L matrix
	for(i=0;i<p.num;i++)
	{
		I_L(i*2) = p.GetPoint_p(i).x;
		I_L(i*2+1) = p.GetPoint_p(i).y;
	}
	
	I_AT = I_A.Transpose();
	I_ATAinv = (I_AT%I_A).Inverse();
	I_ATL = I_AT%I_L;
	I_X = I_ATAinv%I_ATL;
	
	I_a1 = I_X(0);
	I_a2 = I_X(1);
	I_b1 = I_X(2);
	I_b2 = I_X(3);
	I_c1 = I_X(4);
	I_c2 = I_X(5);
	
	I_AX = I_A%X;
	I_V = I_L - I_AX;
	I_VTV = I_V.Transpose()%I_V;
	I_variance = I_VTV(0)/(double)DF;
	I_Ninv = I_ATAinv;
	I_VarCov = I_Ninv*I_variance;
	I_Posteriori_La = I_A%I_Ninv%I_AT;
	I_Posteriori_La = I_Posteriori_La*I_variance;
	
	return TRUE;
}

Point2D<double> Affine::CoordTransform(Point2D<double> P)
{
	Point2D<double> Q;
	Q.x = a1*P.x + a2*P.y + c1;
	Q.y = b1*P.x + b2*P.y + c2;
	return Q;
}

Point2D<double> Affine::InverseCoordTransform(Point2D<double> Q)
{
	Point2D<double> P;
	P.x = I_a1*Q.x + I_a2*Q.y + I_c1;
	P.y = I_b1*Q.x + I_b2*Q.y + I_c2;

	return P;
}

//Projective_linear
/////////////////////////////////////////////////////////////////////////
Projective_linear::Projective_linear(KnownPoint& p)
{
	Projective_linear_Transform(p);
}

BOOL Projective_linear::Projective_linear_Transform(KnownPoint& p)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATAinv;
	CSMMatrix<double> ATL;
	
	int i;
	//matrix(A, L) resizing
	A.Resize(p.num*2,8);
	L.Resize(p.num*2);
	
	//A matrix
	for(i=0;i<p.num;i++)
	{
		A(i*2,0) = p.GetPoint_p(i).x;
		A(i*2,1) = p.GetPoint_p(i).y;
		A(i*2,2) = 1;
		A(i*2,3) = 0;
		A(i*2,4) = 0;
		A(i*2,5) = 0;
		A(i*2,6) = -p.GetPoint_p(i).x*p.GetPoint_q(i).x;
		A(i*2,7) = -p.GetPoint_p(i).y*p.GetPoint_q(i).x;
		
		A(i*2+1,0) =0;
		A(i*2+1,1) =0;
		A(i*2+1,2) =0;
		A(i*2+1,3) =p.GetPoint_p(i).x;
		A(i*2+1,4) =p.GetPoint_p(i).y;
		A(i*2+1,5) =1;
		A(i*2+1,6) =-p.GetPoint_p(i).x*p.GetPoint_q(i).y;
		A(i*2+1,7) =-p.GetPoint_p(i).y*p.GetPoint_q(i).y;
	}
	
	//L matrix
	for(i=0;i<p.num;i++)
	{
		L(i*2) = p.GetPoint_q(i).x;
		L(i*2+1) = p.GetPoint_q(i).y;
	}
	
	AT = A.Transpose();
	ATAinv = (AT%A).Inverse();
	ATL = AT%L;
	X = ATAinv%ATL;
	
	a1 = X(0);
	b1 = X(1);
	c1 = X(2);
	a2 = X(3);
	b2 = X(4);
	c2 = X(5);
	a3 = X(6);
	b3 = X(7);
	
	AX = A%X;
	V = L - AX;
	VTV = V.Transpose()%V;
	DF = p.num*2 - 8;
	variance = VTV(0)/(double)DF;
	Ninv = ATAinv;
	VarCov = Ninv*variance;
	Posteriori_La = A%Ninv%AT;
	Posteriori_La = Posteriori_La*variance;


	//////////////////////////////////////////////////////////////////////////
	//Inverse Transform
	//////////////////////////////////////////////////////////////////////////
	CSMMatrix<double> I_AX;
	CSMMatrix<double> I_AT;
	CSMMatrix<double> I_ATAinv;
	CSMMatrix<double> I_ATL;
	
	//matrix(A, L) resizing
	I_A.Resize(p.num*2,8);
	I_L.Resize(p.num*2);
	
	//A matrix
	for(i=0;i<p.num;i++)
	{
		I_A(i*2,0) = p.GetPoint_q(i).x;
		I_A(i*2,1) = p.GetPoint_q(i).y;
		I_A(i*2,2) = 1;
		I_A(i*2,3) = 0;
		I_A(i*2,4) = 0;
		I_A(i*2,5) = 0;
		I_A(i*2,6) = -p.GetPoint_q(i).x*p.GetPoint_p(i).x;
		I_A(i*2,7) = -p.GetPoint_q(i).y*p.GetPoint_p(i).x;
		
		I_A(i*2+1,0) =0;
		I_A(i*2+1,1) =0;
		I_A(i*2+1,2) =0;
		I_A(i*2+1,3) =p.GetPoint_q(i).x;
		I_A(i*2+1,4) =p.GetPoint_q(i).y;
		I_A(i*2+1,5) =1;
		I_A(i*2+1,6) =-p.GetPoint_q(i).x*p.GetPoint_p(i).y;
		I_A(i*2+1,7) =-p.GetPoint_q(i).y*p.GetPoint_p(i).y;
	}
	
	//L matrix
	for(i=0;i<p.num;i++)
	{
		I_L(i*2) = p.GetPoint_p(i).x;
		I_L(i*2+1) = p.GetPoint_p(i).y;
	}
	
	I_AT = I_A.Transpose();
	I_ATAinv = (I_AT % I_A).Inverse();
	I_ATL = I_AT % I_L;
	I_X = I_ATAinv % I_ATL;
	
	I_a1 = I_X(0);
	I_b1 = I_X(1);
	I_c1 = I_X(2);
	I_a2 = I_X(3);
	I_b2 = I_X(4);
	I_c2 = I_X(5);
	I_a3 = I_X(6);
	I_b3 = I_X(7);
	
	I_AX = I_A % I_X;
	I_V = I_L - I_AX;
	I_VTV = I_V.Transpose() % I_V;
	I_variance = I_VTV(0)/(double)DF;
	I_Ninv = I_ATAinv;
	I_VarCov = I_Ninv*variance;
	I_Posteriori_La = I_A % I_Ninv % I_AT;
	I_Posteriori_La = I_Posteriori_La * I_variance;
	
	return TRUE;
}

Point2D<double> Projective_linear::CoordTransform(Point2D<double> P)
{
	Point2D<double> Q;
	Q.x = (a1*P.x+b1*P.y+c1)/(a3*P.x+b3*P.y+1);
	Q.y = (a2*P.x+b2*P.y+c2)/(a3*P.x+b3*P.y+1);
	return Q;
}

Point2D<double> Projective_linear::InverseCoordTransform(Point2D<double> Q)
{
	Point2D<double> P;
	P.x = (I_a1*Q.x+I_b1*Q.y+I_c1)/(I_a3*Q.x+I_b3*Q.y+1);
	P.y = (I_a2*Q.x+I_b2*Q.y+I_c2)/(I_a3*Q.x+I_b3*Q.y+1);
	return P;
}

//Projective_nonlinear
/////////////////////////////////////////////////////////////////////////
Projective_nonlinear::Projective_nonlinear(KnownPoint& p)
//:Projective_linear(p)
{
	Projective_nonlinear_Transform(p);
}

BOOL Projective_nonlinear::Projective_nonlinear_Transform(KnownPoint& p)
{
	Projective_linear_Transform(p);

	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATAinv;
	CSMMatrix<double> ATL;
	CSMMatrix<double> Lo(p.num*2);
	
	int i;
	
	A.Resize(p.num*2,8);
	L.Resize(p.num*2);
	
	iteration_num = 0;
	maxcorrection = 0.0;
	do
	{
		iteration_num++;
		for(i=0;i<p.num;i++)
		{
			
			double X = p.GetPoint_p(i).x;
			double Y = p.GetPoint_p(i).y;
			A(i*2,0) = X/(a3*X+b3*Y+1);
			A(i*2,1) = Y/(a3*X+b3*Y+1);
			A(i*2,2) = 1/(a3*X+b3*Y+1);
			A(i*2,3) = 0;
			A(i*2,4) = 0;
			A(i*2,5) = 0;
			A(i*2,6) = -X*(a1*X+b1*Y+c1)/pow((a3*X+b3*Y+1),2);
			A(i*2,7) = -Y*(a1*X+b1*Y+c1)/pow((a3*X+b3*Y+1),2);
			
			A(i*2+1,0) =0;
			A(i*2+1,1) =0;
			A(i*2+1,2) =0;
			A(i*2+1,3) =X/(a3*X+b3*Y+1);
			A(i*2+1,4) =Y/(a3*X+b3*Y+1);
			A(i*2+1,5) =1/(a3*X+b3*Y+1);
			A(i*2+1,6) =-X*(a2*X+b2*Y+c2)/pow((a3*X+b3*Y+1),2);
			A(i*2+1,7) =-Y*(a2*X+b2*Y+c2)/pow((a3*X+b3*Y+1),2);
			
			Lo(i*2) = (a1*X+b1*Y+c1)/(a3*X+b3*Y+1);
			Lo(i*2+1) = (a2*X+b2*Y+c2)/(a3*X+b3*Y+1);
		}
		
		for(i=0;i<p.num;i++)
		{
			L(i*2) = p.GetPoint_q(i).x - Lo(i*2);
			L(i*2+1) = p.GetPoint_q(i).y - Lo(i*2+1);
		}
		
		AT = A.Transpose();
		ATAinv = AT%A;
		ATAinv = ATAinv.Inverse();
		ATL = AT%L;
		X = ATAinv%ATL;
		maxcorrection = fabs(X(0));
		for(i=1;i<(int)X.GetRows();i++)
		{
			if(maxcorrection<fabs(X(i)))
			{
				maxcorrection = fabs(X(i));
			}
		}
		a1 += X(0);
		b1 += X(1);
		c1 += X(2);
		a2 += X(3);
		b2 += X(4);
		c2 += X(5);
		a3 += X(6);
		b3 += X(7);
	}while(maxcorrection>0.00000001 && iteration_num<10);
	
	AX = A%X;
	V = L - AX;
	CSMMatrix<double> VT;
	VT = V.Transpose();
	VTV = VT%V;
	DF = p.num*2 - 8;
	variance = VTV(0)/(double)DF;
	Ninv = ATAinv;
	VarCov = Ninv*variance;
	Posteriori_La = A%Ninv%AT;
	Posteriori_La = Posteriori_La*variance;

	//////////////////////////////////////////////////////////////////////////
	//Inverse Transform
	//////////////////////////////////////////////////////////////////////////
	CSMMatrix<double> I_AX;
	CSMMatrix<double> I_AT;
	CSMMatrix<double> I_ATAinv;
	CSMMatrix<double> I_ATL;
	CSMMatrix<double> I_Lo(p.num*2);
	
	//matrix(A, L) resizing
	I_A.Resize(p.num*2,8);
	I_L.Resize(p.num*2);
	
	iteration_num = 0;
	maxcorrection = 0.0;
	do
	{
		iteration_num++;
		for(i=0;i<p.num;i++)
		{
			
			double X = p.GetPoint_q(i).x;
			double Y = p.GetPoint_q(i).y;
			I_A(i*2,0) = X/(I_a3*X+I_b3*Y+1);
			I_A(i*2,1) = Y/(I_a3*X+I_b3*Y+1);
			I_A(i*2,2) = 1/(I_a3*X+I_b3*Y+1);
			I_A(i*2,3) = 0;
			I_A(i*2,4) = 0;
			I_A(i*2,5) = 0;
			I_A(i*2,6) = -X*(I_a1*X+I_b1*Y+I_c1)/pow((I_a3*X+I_b3*Y+1),2);
			I_A(i*2,7) = -Y*(I_a1*X+I_b1*Y+I_c1)/pow((I_a3*X+I_b3*Y+1),2);
			
			I_A(i*2+1,0) =0;
			I_A(i*2+1,1) =0;
			I_A(i*2+1,2) =0;
			I_A(i*2+1,3) =X/(I_a3*X+I_b3*Y+1);
			I_A(i*2+1,4) =Y/(I_a3*X+I_b3*Y+1);
			I_A(i*2+1,5) =1/(I_a3*X+I_b3*Y+1);
			I_A(i*2+1,6) =-X*(I_a2*X+I_b2*Y+I_c2)/pow((I_a3*X+I_b3*Y+1),2);
			I_A(i*2+1,7) =-Y*(I_a2*X+I_b2*Y+I_c2)/pow((I_a3*X+I_b3*Y+1),2);
			
			I_Lo(i*2) = (I_a1*X+I_b1*Y+I_c1)/(I_a3*X+I_b3*Y+1);
			I_Lo(i*2+1) = (I_a2*X+I_b2*Y+I_c2)/(I_a3*X+I_b3*Y+1);
		}
		
		for(i=0;i<p.num;i++)
		{
			I_L(i*2) = p.GetPoint_p(i).x - I_Lo(i*2);
			I_L(i*2+1) = p.GetPoint_p(i).y - I_Lo(i*2+1);
		}
		
		I_AT = I_A.Transpose();
		I_ATAinv = I_AT%I_A;
		I_ATAinv = I_ATAinv.Inverse();
		I_ATL = I_AT%I_L;
		I_X = I_ATAinv%I_ATL;
		maxcorrection = fabs(I_X(0));
		for(i=1;i<(int)I_X.GetRows();i++)
		{
			if(maxcorrection<fabs(I_X(i)))
			{
				maxcorrection = fabs(I_X(i));
			}
		}
		I_a1 += I_X(0);
		I_b1 += I_X(1);
		I_c1 += I_X(2);
		I_a2 += I_X(3);
		I_b2 += I_X(4);
		I_c2 += I_X(5);
		I_a3 += I_X(6);
		I_b3 += I_X(7);

	}while(maxcorrection>0.00000001 && iteration_num<10);
	
	I_AX = I_A % I_X;
	I_V = I_L - I_AX;
	I_VTV = I_V.Transpose() % I_V;
	I_variance = I_VTV(0)/(double)DF;
	I_Ninv = I_ATAinv;
	I_VarCov = I_Ninv*variance;
	I_Posteriori_La = I_A % I_Ninv % I_AT;
	I_Posteriori_La = I_Posteriori_La * I_variance;
	
	return TRUE;
}

//Bilinear
/////////////////////////////////////////////////////////////////////////
Bilinear::Bilinear(KnownPoint& p)
{
	Bilinear_Transform(p);
}

BOOL Bilinear::Bilinear_Transform(KnownPoint& p)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATAinv;
	CSMMatrix<double> ATL;
	
	int i;
	
	//matrix(A, L) resizing
	A.Resize(p.num*2,8);
	L.Resize(p.num*2);
	
	//matrix A
	for(i=0;i<p.num;i++)
	{
		double X = p.GetPoint_p(i).x;
		double Y = p.GetPoint_p(i).y;
		A(i*2,0) = X;
		A(i*2,1) = Y;
		A(i*2,2) = X*Y;
		A(i*2,3) = 0;
		A(i*2,4) = 0;
		A(i*2,5) = 0;
		A(i*2,6) = 1;
		A(i*2,7) = 0;
		
		A(i*2+1,0) =0;
		A(i*2+1,1) =0;
		A(i*2+1,2) =0;
		A(i*2+1,3) =X;
		A(i*2+1,4) =Y;
		A(i*2+1,5) =X*Y;
		A(i*2+1,6) =0;
		A(i*2+1,7) =1;
	}
	
	//matrix L
	for(i=0;i<p.num;i++)
	{
		L(i*2) = p.GetPoint_q(i).x;
		L(i*2+1) = p.GetPoint_q(i).y;
	}
	
	AT = A.Transpose();
	ATAinv = (AT%A).Inverse();
	ATL = AT%L;
	X = ATAinv%ATL;
	
	a1 = X(0);
	a2 = X(1);
	a3 = X(2);
	b1 = X(3);
	b2 = X(4);
	b3 = X(5);
	c1 = X(6);
	c2 = X(7);
	
	AX = A%X;
	V = L - AX;
	VTV = V.Transpose()%V;
	DF = p.num*2 - 8;
	variance = VTV(0)/(double)DF;
	Ninv = ATAinv;
	VarCov = Ninv*variance;
	Posteriori_La = A%Ninv%AT;
	Posteriori_La = Posteriori_La*variance;

	//////////////////////////////////////////////////////////////////////////
	//Inverse Transform
	//////////////////////////////////////////////////////////////////////////
	CSMMatrix<double> I_AX;
	CSMMatrix<double> I_AT;
	CSMMatrix<double> I_ATAinv;
	CSMMatrix<double> I_ATL;
	
	//matrix(I_A, I_L) resizing
	I_A.Resize(p.num*2,8);
	I_L.Resize(p.num*2);
	
	//matrix I_A
	for(i=0;i<p.num;i++)
	{
		double X = p.GetPoint_q(i).x;
		double Y = p.GetPoint_q(i).y;
		I_A(i*2,0) = X;
		I_A(i*2,1) = Y;
		I_A(i*2,2) = X*Y;
		I_A(i*2,3) = 0;
		I_A(i*2,4) = 0;
		I_A(i*2,5) = 0;
		I_A(i*2,6) = 1;
		I_A(i*2,7) = 0;
		
		I_A(i*2+1,0) =0;
		I_A(i*2+1,1) =0;
		I_A(i*2+1,2) =0;
		I_A(i*2+1,3) =X;
		I_A(i*2+1,4) =Y;
		I_A(i*2+1,5) =X*Y;
		I_A(i*2+1,6) =0;
		I_A(i*2+1,7) =1;
	}
	
	//matrix L
	for(i=0;i<p.num;i++)
	{
		I_L(i*2) = p.GetPoint_p(i).x;
		I_L(i*2+1) = p.GetPoint_p(i).y;
	}
	
	I_AT = I_A.Transpose();
	I_ATAinv = (I_AT % I_A).Inverse();
	I_ATL = I_AT % I_L;
	I_X = I_ATAinv % I_ATL;
	
	I_a1 = I_X(0);
	I_a2 = I_X(1);
	I_a3 = I_X(2);
	I_b1 = I_X(3);
	I_b2 = I_X(4);
	I_b3 = I_X(5);
	I_c1 = I_X(6);
	I_c2 = I_X(7);
	
	I_AX = I_A%I_X;
	I_V = I_L - I_AX;
	I_VTV = I_V.Transpose() % I_V;
	I_variance = I_VTV(0) / (double)DF;
	I_Ninv = I_ATAinv;
	I_VarCov = I_Ninv*I_variance;
	I_Posteriori_La = I_A%I_Ninv % I_AT;
	I_Posteriori_La = I_Posteriori_La * I_variance;

	
	return TRUE;
}

Point2D<double> Bilinear::CoordTransform(Point2D<double> P)
{
	Point2D<double> Q;
	Q.x = a1*P.x+a2*P.y+a3*P.x*P.y+c1;
	Q.y = b1*P.x+b2*P.y+b3*P.x*P.y+c2;
	return Q;
}

Point2D<double> Bilinear::InverseCoordTransform(Point2D<double> Q)
{
	Point2D<double> P;
	P.x = I_a1*Q.x+I_a2*Q.y+I_a3*Q.x*Q.y+I_c1;
	P.y = I_b1*Q.x+I_b2*Q.y+I_b3*Q.x*Q.y+I_c2;

	return P;
}

//RigidBody
/////////////////////////////////////////////////////////////////////////
RigidBody::RigidBody(KnownPoint& p)
{
	RigidBody_Transform(p);
}

BOOL RigidBody::RigidBody_Transform(KnownPoint& p)
{
	CSMMatrix<double> AX;
	CSMMatrix<double> AT;
	CSMMatrix<double> ATAinv;
	CSMMatrix<double> ATL;
	
	int i;
	
	//matrix(A, L) resizing
	A.Resize(p.num*2,4);
	L.Resize(p.num*2);
	
	//matrix A
	for(i=0;i<p.num;i++)
	{
		double X = p.GetPoint_p(i).x;
		double Y = p.GetPoint_p(i).y;
		A(i*2,0) = X;
		A(i*2,1) = 1;
		A(i*2,2) = 0;
		A(i*2,3) = 0;
				
		A(i*2+1,0) =0;
		A(i*2+1,1) =0;
		A(i*2+1,2) =Y;
		A(i*2+1,3) =1;
	}
	
	//matrix L
	for(i=0;i<p.num;i++)
	{
		L(i*2) = p.GetPoint_q(i).x;
		L(i*2+1) = p.GetPoint_q(i).y;
	}
	
	AT = A.Transpose();
	ATAinv = (AT%A).Inverse();
	ATL = AT%L;
	X = ATAinv%ATL;
	
	Sx = X(0);
	Tx = X(1);
	Sy = X(2);
	Ty = X(3);
	
	AX = A%X;
	V = L - AX;
	VTV = V.Transpose()%V;
	DF = p.num*2 - 4;
	variance = VTV(0)/(double)DF;
	Ninv = ATAinv;
	VarCov = Ninv*variance;
	Posteriori_La = A%Ninv%AT;
	Posteriori_La = Posteriori_La*variance;

	//////////////////////////////////////////////////////////////////////////
	//Inverse Transform
	//////////////////////////////////////////////////////////////////////////
	CSMMatrix<double> I_AX;
	CSMMatrix<double> I_AT;
	CSMMatrix<double> I_ATAinv;
	CSMMatrix<double> I_ATL;
	
	//matrix(I_A, I_L) resizing
	I_A.Resize(p.num*2,4);
	I_L.Resize(p.num*2);
	
	//matrix I_A
	for(i=0;i<p.num;i++)
	{
		double X = p.GetPoint_q(i).x;
		double Y = p.GetPoint_q(i).y;
		I_A(i*2,0) = X;
		I_A(i*2,1) = 1;
		I_A(i*2,2) = 0;
		I_A(i*2,3) = 0;
		
		I_A(i*2+1,0) =0;
		I_A(i*2+1,1) =0;
		I_A(i*2+1,2) =Y;
		I_A(i*2+1,3) =1;
	}
	
	//matrix L
	for(i=0;i<p.num;i++)
	{
		I_L(i*2) = p.GetPoint_p(i).x;
		I_L(i*2+1) = p.GetPoint_p(i).y;
	}
	
	I_AT = I_A.Transpose();
	I_ATAinv = (I_AT % I_A).Inverse();
	I_ATL = I_AT % I_L;
	I_X = I_ATAinv % I_ATL;
	
	I_Sx = I_X(0);
	I_Tx = I_X(1);
	I_Sy = I_X(2);
	I_Ty = I_X(3);
	
	I_AX = I_A%I_X;
	I_V = I_L - I_AX;
	I_VTV = I_V.Transpose() % I_V;
	I_variance = I_VTV(0) / (double)DF;
	I_Ninv = I_ATAinv;
	I_VarCov = I_Ninv*I_variance;
	I_Posteriori_La = I_A%I_Ninv % I_AT;
	I_Posteriori_La = I_Posteriori_La * I_variance;
	
	return TRUE;
}

Point2D<double> RigidBody::CoordTransform(Point2D<double> P)
{
	Point2D<double> Q;
	Q.x = Sx*P.x+Tx;
	Q.y = Sy*P.y+Ty;
	return Q;
}

Point2D<double> RigidBody::InverseCoordTransform(Point2D<double> Q)
{
	Point2D<double> P;
	P.x = I_Sx*Q.x+I_Tx;
	P.y = I_Sy*Q.y+I_Ty;

	return P;
}

//KnownPoint
/////////////////////////////////////////////////////////////////////////
KnownPoint::KnownPoint(int num_point)
{
	m_pPoint = NULL;
	m_qPoint = NULL;

	SetPointNum(num_point);
}

BOOL KnownPoint::SetPointNum(int n)
{
	if(m_pPoint != NULL)
	{
		delete[] m_pPoint;
		m_pPoint = NULL;
	}

	if(m_qPoint != NULL)
	{
		delete[] m_qPoint;
		m_qPoint = NULL;
	}
	
	if(n > 0)
	{
		num = n;
		m_pPoint = new Point2D<double> [num];
		m_qPoint = new Point2D<double> [num];
	}
	else
	{
		return FALSE;
	}
	
	return TRUE;
}

KnownPoint::~KnownPoint()
{
	if(m_pPoint!=NULL)
	{
		delete[] m_pPoint;
		m_pPoint = NULL;
	}
	if(m_qPoint!=NULL)
	{
		delete[] m_qPoint;
		m_qPoint = NULL;
	}
}

BOOL KnownPoint::SetPoint(int index, double px, double py, double qx, double qy)
{
	if(index < num)
	{
		m_pPoint[index].x = px;
		m_pPoint[index].y = py;
		
		m_qPoint[index].x = qx;
		m_qPoint[index].y = qy;
	}
	else
	{
		return FALSE;
	}
	
	return TRUE;
}

BOOL KnownPoint::SetPoint_p(int index, double px, double py)
{
	if(index < num)
	{
		m_pPoint[index].x = px;
		m_pPoint[index].y = py;
	}
	else
	{
		return FALSE;
	}
	
	return TRUE;
}

BOOL KnownPoint::SetPoint_q(int index, double qx, double qy)
{
	if(index < num)
	{
		m_qPoint[index].x = qx;
		m_qPoint[index].y = qy;
	}
	else
	{
		return FALSE;
	}
	
	return TRUE;
}