/*
 * Copyright (c) 1999-2007, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// SSMaticsBundle.cpp: implementation of the CSSMaticsBundle class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SSMaticsBundle.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSSMaticsBundle::CSSMaticsBundle()
{

}

CSSMaticsBundle::~CSSMaticsBundle()
{

}

void CSSMaticsBundle::Init()
{
	m_MinVar = 1.0e-6;
	m_MaxVar = 1.0e+6;
	m_Threshold_S = 1.0e-10;
	max_iteration = 10;
	m_Camera.RemoveAll();
	m_Photo.RemoveAll();
	m_GCPs.RemoveAll();
}

bool CSSMaticsBundle::ReadPrjFile(char* stPrjFile)
{
	//Initialize this module
	Init();

	char line[MAXLINE];
	char camerapath[MAXLINE];
	char photopath[MAXLINE];
	char gcppath[MAXLINE];
	
	fstream prjfile;
	
	prjfile.open(stPrjFile, ios::in|ios::nocreate);

	if(!prjfile)
	{
		AfxMessageBox(strcat(stPrjFile, "file open is failed"));
		return false;
	}

	prjfile.getline(line, MAXLINE); prjfile.eatwhite();
	prjfile>>m_MinVar; prjfile.eatwhite();
	prjfile.getline(line, MAXLINE); prjfile.eatwhite();
	prjfile>>m_MaxVar; prjfile.eatwhite();
	prjfile.getline(line, MAXLINE); prjfile.eatwhite();
	prjfile.getline(camerapath, MAXLINE); prjfile.eatwhite();
	prjfile.getline(line, MAXLINE); prjfile.eatwhite();
	prjfile.getline(photopath, MAXLINE); prjfile.eatwhite();
	prjfile.getline(line, MAXLINE); prjfile.eatwhite();
	prjfile.getline(gcppath, MAXLINE); prjfile.eatwhite();
	prjfile.getline(line, MAXLINE); prjfile.eatwhite();
	prjfile>>m_Threshold_S; prjfile.eatwhite();
	prjfile.getline(line, MAXLINE); prjfile.eatwhite();
	prjfile>>max_iteration; prjfile.eatwhite();

	prjfile.close();

	if(false == ReadCamera(camerapath)) return false;

	if(false == ReadPhoto(photopath)) return false;

	if(false == ReadGCP(gcppath)) return false;
	
	return true;
}

bool CSSMaticsBundle::ReadCamera(char* stCameraFile)
{
	char line[MAXLINE];

	fstream camerafile;
	camerafile.open(stCameraFile, ios::in|ios::nocreate);
	if(!camerafile)
	{
		AfxMessageBox(strcat(stCameraFile, "file open is failed"));
		return false;
	}

	while(!camerafile.eof())
	{
		CSSMCamera Camera;
		
		camerafile.getline(line, MAXLINE); camerafile.eatwhite();//camera ID
		camerafile>>Camera.m_ID; camerafile.eatwhite();
		camerafile.getline(line, MAXLINE); camerafile.eatwhite();//principal distance
		camerafile>>Camera.m_c; camerafile.eatwhite();
		camerafile.getline(line, MAXLINE); camerafile.eatwhite();//xp, yp
		camerafile>>Camera.m_xp>>Camera.m_yp; camerafile.eatwhite();
		camerafile.getline(line, MAXLINE); camerafile.eatwhite();//dispersion
		for(int i=0; i<NUM_CAMERA_PARAM; i++)
		{
			for(int j=0; j<NUM_CAMERA_PARAM; j++)
			{
				camerafile>>Camera.m_Dispersion(i,j);
			}
			
			camerafile.eatwhite();
		}

		camerafile.getline(line, MAXLINE); camerafile.eatwhite();//SMAC parameters
		camerafile>>Camera.k0>>Camera.k1>>Camera.k2>>Camera.k3>>Camera.p1>>Camera.p2>>Camera.p3; camerafile.eatwhite();

		m_Camera.AddTail(Camera);
	}

	camerafile.close();

	//Given camera data backup
	m_CopyCamera = m_Camera;

	return true;
}

bool CSSMaticsBundle::ReadPhoto(char* stPhotoFile)
{
	char line[MAXLINE];
	int i, j;

	fstream photofile;
	
	photofile.open(stPhotoFile, ios::in);

	if(!photofile)
	{
		AfxMessageBox(strcat(stPhotoFile, "file open is failed"));
		return false;
	}

	while(!photofile.eof())
	{
		CSSMPhoto Photo;
		
		photofile.getline(line, MAXLINE); photofile.eatwhite();
		photofile>>Photo.m_ID; photofile.eatwhite();
		
		photofile.getline(line, MAXLINE); photofile.eatwhite();
		photofile>>Photo.m_CameraID; photofile.eatwhite();
		
		photofile.getline(line, MAXLINE); photofile.eatwhite();
		photofile>>Photo.m_X0>>Photo.m_Y0>>Photo.m_Z0;
		photofile>>Photo.m_Omega0>>Photo.m_Phi0>>Photo.m_Kappa0; photofile.eatwhite();
		
		Photo.m_Omega0 = Deg2Rad(Photo.m_Omega0);
		Photo.m_Phi0 = Deg2Rad(Photo.m_Phi0);
		Photo.m_Kappa0 = Deg2Rad(Photo.m_Kappa0);
		
		photofile.getline(line, MAXLINE); photofile.eatwhite();
		for(i=0; i<NUM_PHOTO_PARAM; i++)
		{
			for(j=0; j<NUM_PHOTO_PARAM; j++)
			{
				photofile>>Photo.m_Dispersion(i,j);
			}
			
			photofile.eatwhite();
		}
		
		photofile.getline(line, MAXLINE); photofile.eatwhite();
		CSSMPoint point(2);
		
		while(!photofile.eof())
		{
			photofile>>point.m_ID;
			
			if(0 == strcmp(point.m_ID, "ENDOFIMAGEPOINT"))
			{
				photofile.eatwhite();
				break;
			}

			photofile>>point[0]>>point[1];
			for(i=0; i<2; i++)
			{
				for(j=0; j<2; j++)
				{
					photofile>>point.m_Dispersion(i,j);
				}
			}
			
			//Photo.m_ImagePoints.AddTail(point);
			Photo.AddImagePoint(point);
			
			photofile.eatwhite();
		}

		m_Photo.AddTail(Photo);
		
	}

	//Given photo data backup
	m_CopyPhoto = m_Photo;

	photofile.close();

	return true;
}

bool CSSMaticsBundle::ReadGCP(char* stGCPFile)
{
	m_GCPs.RemoveAll();

	char line[MAXLINE];
	char attribute[MAXLINE];
	int i, j;

	fstream gcpfile;
	
	gcpfile.open(stGCPFile, ios::in|ios::nocreate);

	if(!gcpfile)
	{
		AfxMessageBox(strcat(stGCPFile, "file open is failed"));
		return false;
	}

	gcpfile.getline(line, MAXLINE); 
	gcpfile.eatwhite();

	while(!gcpfile.eof())
	{
		gcpfile>>attribute;
		gcpfile.eatwhite();

		if(0 == strcmp(attribute, "LINE"))
		{
			CSSMPoint pointA(3); //line end point A
			CSSMPoint pointB(3); //line end point B
			
			gcpfile>>pointA.m_ID>>pointA.m_p[0]>>pointA.m_p[1]>>pointA.m_p[2];
			gcpfile.eatwhite();

			for(i=0; i<3; i++)
			{
				for(j=0; j<3; j++)
				{
					gcpfile>>pointA.m_Dispersion(i,j);
					gcpfile.eatwhite();
				}
			}

			gcpfile>>pointB.m_ID>>pointB.m_p[0]>>pointB.m_p[1]>>pointB.m_p[2];
			gcpfile.eatwhite();

			for(i=0; i<3; i++)
			{
				for(j=0; j<3; j++)
				{
					gcpfile>>pointB.m_Dispersion(i,j);
					gcpfile.eatwhite();
				}
			}

			//
			//Calculate dispersion for a line
			//
			double dX, dY, dZ;
			dX = pointB[0] - pointA[0];
			dY = pointB[1] - pointA[1];
			dZ = pointB[2] - pointA[2];
			double O, P, K=0;
			if(false == ExtractEulerAngles(dX, dY, dZ, O, P))
			{
				char message[MAXLINE];
				strcpy(message, "To calculate a linear vector is impossible.\t");
				strcat(message, pointA.m_ID);
				strcat(message, "\t");
				strcat(message, pointB.m_ID);
				AfxMessageBox(message);
				return false;
			}

			//New dispersion
			// M*XYZ = UVW
			// Sigma_UVW = M*Sigma_XYZ*M_T
			//Sigma_XYZ = R*Sigma_UVW*R_T = M_T*Sigma_UVW*M
			_Mmat_ M(O, P, K);
			CSMMatrix<double> dispersion;

			//A
			dispersion = M.Mmatrix%pointA.m_Dispersion%M.Mmatrix.Transpose();
			dispersion(2,2) = m_MaxVar*0.1;
			pointA.m_Dispersion = M.Mmatrix.Transpose()%dispersion%M.Mmatrix;
			//B			
			dispersion = M.Mmatrix%pointB.m_Dispersion%M.Mmatrix.Transpose();
			dispersion(2,2) = m_MaxVar*0.1;
			pointB.m_Dispersion = M.Mmatrix.Transpose()%dispersion%M.Mmatrix;
			//
			//end of calculation of line dispersion
			//

			try
			{
				CSMMatrix<double> temp;
				temp = pointA.m_Dispersion.Inverse();
				temp = pointB.m_Dispersion.Inverse();
			}
			catch(CSMMatrixError e)
			{
				char message[MAXLINE];
				strcpy(message, "Error in given dispersion : ");
				char ID[MAXLINE];
				strcpy(ID, pointA.m_ID); strcat(ID, " "); strcat(ID, pointB.m_ID);
				strcat(message, ID);
				AfxMessageBox(e.WhichError());
				AfxMessageBox(message);
				return false;
			}

			m_GCPs.AddTail(pointA);
			m_GCPs.AddTail(pointB);
		}
		else if(0 == strcmp(attribute, "POINT"))
		{
			CSSMPoint point(3); //point

			gcpfile>>point.m_ID>>point.m_p[0]>>point.m_p[1]>>point.m_p[2];
			gcpfile.eatwhite();
			
			for(i=0; i<3; i++)
			{
				for(j=0; j<3; j++)
				{
					gcpfile>>point.m_Dispersion(i,j);
					gcpfile.eatwhite();
				}
			}
			
			m_GCPs.AddTail(point);
		}
		else
		{
			AfxMessageBox("Point attribute tag is wrong!");
			return false;
		}
		
		gcpfile.eatwhite();
	}

	gcpfile.close();

	//Given GCP data backup
	m_CopyGCPs = m_GCPs;

	return true;
}

bool CSSMaticsBundle::DataCheck(fstream &report)
{
	bool retval=true;

	report<<"[Input Data check]"<<endl<<endl;
	report.flush();

	int i, j, k;
	int num_photos = m_Photo.GetNumItem(); //number of photos
	int num_points = m_GCPs.GetNumItem(); //Number of object points

	///////////////////////////////////////
	//Check photo file
	//
	CSMList<int> UnavailablePoint;

	for(i=0; i<num_photos; i++)
	{
		CSSMPhoto* photo = m_Photo.GetHandleAt(i);
		CSSMCamera camera, copycamera;
		if(false == FindCamera(photo->m_CameraID, camera, copycamera)) 
		{
			report<<"Check Camera ID : "<<photo->m_CameraID<<endl;
			report.flush();
			return false;
		}

		UnavailablePoint.RemoveAll();

		//int num_photo_points = photo->m_ImagePoints.GetNumItem();
		int num_photo_points = photo->GetNumberofPoints();
		
		for(j=0; j<num_photo_points; j++)
		{
			int obs_point_index;
			CSSMPoint GP;
			CSSMPoint copyGP;
			//CSSMPoint IP = photo->m_ImagePoints.GetAt(j);
			CSSMPoint IP = photo->GetImagePoint(camera, j);

			//Find unavailable image points
			if(true != FindGCP(IP.m_ID, GP, copyGP, obs_point_index))
			{
				report<<"Check unavailable image point["<<IP.m_ID<<"] "<<endl;
				report.flush();
				UnavailablePoint.AddTail(j);
				retval = false;
			}
		}
	}
	
	///////////////////////////////////////
	//Check GCP file
	//
	int pos = num_photos*NUM_PHOTO_PARAM;
	CSMList<int> UnavailableGCPPoint;
	for(i=0; i<num_points; i++)
	{
		CSSMPoint  GP = m_GCPs.GetAt(i);
		
		bool bUnknownCheck = false;

		for(j=0; j<3; j++)
		{
			for(k=0; k<3; k++)
			{
				if(m_MaxVar <= GP.m_Dispersion(j, k))
				{
					bUnknownCheck = true;
				}
			}
		}

		int count=0;
		for(j=0; j<num_photos; j++)
		{
			int count_i;
			CSSMPhoto photo = m_Photo.GetAt(j);
			if(count_i = CheckImagePoint(GP.m_ID, photo))
			{
				if(count_i >= 2)
				{
					report<<"[Reson of return false]Same ID["<<GP.m_ID<<"] in ["<<photo.m_ID<<"]"<<endl;
					report.flush();

					retval = false;
				}
			}
			
			count += count_i;
		}

		//Delete unavailable unknown object points
		if(bUnknownCheck == true)
		{
			if(count < 2)
			{
				report<<"Check unknown object point["<<GP.m_ID<<"] : not enought observations"<<endl;
				report.flush();
				UnavailableGCPPoint.AddTail(i);
				retval = false;
			}
		}
	}

	if(retval == true) report<<endl<<"Data check is done!"<<endl<<endl;
	report.flush();

	return retval;
}

bool CSSMaticsBundle::Solve(char* stReportFile)
{
	fstream reportfile;
	reportfile.open(stReportFile, ios::out);
	reportfile.setf(ios::fixed, ios::floatfield);

	//Check input data
	if(false == DataCheck(reportfile))
	{
		AfxMessageBox("Take a look at the report file and check Input data!!!");
		return false;
	}
	
	int num_photos = m_Photo.GetNumItem(); //number of photos
	int num_points = m_GCPs.GetNumItem(); //Number of object points

	bool bStop = false;
	double old_var=0, old_sigma=0;
	int num_iteration = 0;

	do{
		//Increase iteration number
		num_iteration ++;

		int i, j, k;		
		
		CSMMatrix<double> FixedEOP(6,1,1.0), FreeEOP(6,1,1.0);
		CSMMatrix<double> FixedPoint(3,1,1.0), FreePoint(3,1,1.0);
		
		CLeastSquare LS; //Least square method
		LS.SetDataConfig(num_photos, NUM_PHOTO_PARAM, num_points, 0, 0);//Least square configuration
		
		///////////////////////////////////////////////////////
		//[1] Obs of image point measurements
		///////////////////////////////////////////////////////		
		
		double** b; //coefficients of partial derivatives of collinearity equations
		b = new double*[2];
		b[0] = new double[6];
		b[1] = new double[6];
		
		CSMMatrix<double> a1(2,6), a2(2,3), l(2,1), q(2,2), we;//sub-matrices
				
		CSMMatrix<double> B(2,2);//coefficient matrix of image obs residual
		B(0,0) = 1;	B(0,1) = 0;
		B(1,0) = 0;	B(1,1) = 1;
		
		for(i=0; i<num_photos; i++)
		{	
			CSSMPhoto photo = m_Photo.GetAt(i);
			CSSMPhoto copyphoto = m_CopyPhoto.GetAt(i);
			
			CSSMCamera camera, copycamera; if(false == FindCamera(photo.m_CameraID, camera, copycamera)) continue;
			
			//int num_photo_points = photo.m_ImagePoints.GetNumItem();
			int num_photo_points = photo.GetNumberofPoints();
			
			//Find fixed EOPs
			FindFixedEOP(copyphoto, FixedEOP, FreeEOP);
			
			for(j=0; j<num_photo_points; j++)
			{
				int obs_point_index;
				double J, K;
				//CSSMPoint IP = photo.m_ImagePoints.GetAt(j);
				CSSMPoint IP = photo.GetImagePoint(camera, j);
				CSSMPoint GP, copyGP; if(true != FindGCP(IP.m_ID, GP, copyGP, obs_point_index)) continue;
				
				//Find fixed coordinates
				FindFixedPoint(copyGP, FixedPoint, FreePoint);
				
				//Weight matrix
				q = IP.m_Dispersion; //Variance of image point measurement
				we = (B%q%B.Transpose()).Inverse();
				
				//Partial derivatives
				Make_b_J_K(photo, camera, GP, IP, b, J, K);
				
				//x
				a1(0,0)  = -b[0][3];
				a1(0,1)  = -b[0][4];
				a1(0,2)  = -b[0][5];
				a1(0,3)  =  b[0][0];
				a1(0,4)  =  b[0][1];
				a1(0,5)  =  b[0][2];
				
				a2(0,0)  = b[0][3];
				a2(0,1)  = b[0][4];
				a2(0,2)  = b[0][5];
				
				l(0,0) = J;
				//end of x
				
				//y
				a1(1,0)  = -b[1][3];
				a1(1,1)  = -b[1][4];
				a1(1,2)  = -b[1][5];
				a1(1,3)  =  b[1][0];
				a1(1,4)  =  b[1][1];
				a1(1,5)  =  b[1][2];
				
				a2(1,0)  = b[1][3];
				a2(1,1)  = b[1][4];
				a2(1,2)  = b[1][5];
				
				l(1,0) = K;
				//end of y
				
				//Fix EOPs
				for(k=0; k<NUM_PHOTO_PARAM; k++)
				{
					a1(0,k) = a1(0,k)*FixedEOP(k,0);
					a1(1,k) = a1(1,k)*FixedEOP(k,0);
				}
				
				//Fix coordinates
				for(k=0; k<3; k++)
				{
					a2(0,k) = a2(0,k)*FixedPoint(k,0);
					a2(1,k) = a2(1,k)*FixedPoint(k,0);
				}
				
				LS.Fill_Nmat_Data_Point(a1, a2, we, l, obs_point_index, i);
				
			} //end of for(j=0; j<num_photo_points; j++)
			
	} //end of for(i=0; i<num_photos; i++)
	//
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	//[2] Obs of EOPs
	////////////////////////////////////////////////////////////////////////////
	CSMMatrix<double> a_eop(6,6), l_eop(6,1), q_eop(6,6), w_eop;
	
	for(i=0; i<num_photos; i++)
	{
		CSSMPhoto photo = m_Photo.GetAt(i);
		CSSMPhoto copyphoto = m_CopyPhoto.GetAt(i);
		
		//Find fixed EOPs
		FindFixedEOP(copyphoto, FixedEOP, FreeEOP);
		
		//Check free and stochastic EOP
		a_eop.Identity(1.0);
		
		l_eop(0,0) = copyphoto.m_X0 - photo.m_X0;
		l_eop(1,0) = copyphoto.m_Y0 - photo.m_Y0;
		l_eop(2,0) = copyphoto.m_Z0 - photo.m_Z0;
		l_eop(3,0) = copyphoto.m_Omega0 - photo.m_Omega0;
		l_eop(4,0) = copyphoto.m_Phi0 - photo.m_Phi0;
		l_eop(5,0) = copyphoto.m_Kappa0 - photo.m_Kappa0;
		
		//Fix EOPs
		for(j=0; j<NUM_PHOTO_PARAM; j++)
		{
			//Free or fixed EOPs
			l_eop(j,0) = l_eop(j,0)*FixedEOP(j,0)*FreeEOP(j,0);
			
			//Free EOPs
			a_eop(j,j) = a_eop(j,j)*FreeEOP(j,0);
		}
		
		q_eop = copyphoto.m_Dispersion;
		w_eop = q_eop.Inverse();
		
		LS.Fill_Nmat_EOP(a_eop, w_eop, l_eop, i);
		
	}//end of for(i=0; i<num_points; i++)
	//
	////////////////////////////////////////////////////////////////////////////
	
	////////////////////////////////////////////////////////////////////////////
	//[3] Obs of object point measurements
	////////////////////////////////////////////////////////////////////////////
	CSMMatrix<double> a_gp(3,3), l_gp(3,1), q_gp(3,3), w_gp;
	
	for(i=0; i<num_points; i++)
	{
		CSSMPoint GP = m_GCPs.GetAt(i);
		CSSMPoint copyGP = m_CopyGCPs.GetAt(i);
		
		//Find fixed coordinates
		FindFixedPoint(copyGP, FixedPoint, FreePoint);
		
		a_gp.Identity(1.0);
		
		l_gp(0,0) = copyGP[0] - GP[0];
		l_gp(1,0) = copyGP[1] - GP[1];
		l_gp(2,0) = copyGP[2] - GP[2];
		
		//Fix coordinates
		for(j=0; j<3; j++)
		{
			//Free or fixed coordinates
			l_gp(j,0) = l_eop(j,0)*FixedPoint(j,0)*FreePoint(j,0);
			
			//Free coordinates
			a_gp(j,j) = a_gp(j,j)*FreePoint(j,0);
		}
		
		q_gp = copyGP.m_Dispersion;
		w_gp = q_gp.Inverse();
		
		LS.Fill_Nmat_Data_XYZ(a_gp, w_gp, l_gp, i);
		
	}//end of for(i=0; i<num_points; i++)
	//
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	//Solve unknowns
	double var;
	CSMMatrix<double> N_dot, C_dot;
	CSMMatrix<double> X = LS.RunLeastSquare_RN(var, N_dot, C_dot);
	double sigma = sqrt(var);

	//Update unknown parameters
	ParameterUpdate(X);

	//Update report file
	MakeReport(reportfile, X, sigma, num_iteration);

	////////////////////////////////////////////////////////////////////////////
	//Check iteration stop
	if(num_iteration > 1)
	{
		if(fabs(sigma - old_sigma) < m_Threshold_S)
		{
			bStop = true;
			reportfile.precision(10);
			reportfile<<"Iteration stop because"<<endl;
			reportfile<<"sigma("<<sigma<<") - old sigma("<<old_sigma
				      <<") is smaller than threshold("<<m_Threshold_S<<")"<<endl;
			reportfile.flush();
		}
	}

	//updata sigma and variance
	old_var = var;
	old_sigma = sigma;

	if(num_iteration >= max_iteration)
	{
		bStop = true;
		reportfile<<"Iteration stop because"<<endl;
		reportfile<<"iteration exceed "<<max_iteration<<endl;
		reportfile.flush();
	}
	
	}while(bStop == false);

	reportfile.close();

	return true;
}

void CSSMaticsBundle::GetCameraDistortion(CSSMCamera camera, CSSMPoint IP, CSSMPoint &Dist)
{
	Dist.SetDimension(2);
	Dist[0] = 0;
	Dist[1] = 0;
}


void CSSMaticsBundle::CollinearityEQ(CSSMPhoto photo, CSSMCamera camera, CSSMPoint GP, CSSMPoint &IP)
{
	//x = -f(r/q)
	//y = -f(s/q)

	IP.SetDimension(2);

	_Mmat_ M(photo.m_Omega0, photo.m_Phi0, photo.m_Kappa0);
	double q, r, s;
	double dX, dY, dZ;
	
	dX = GP[0] - photo.m_X0;
	dY = GP[1] - photo.m_Y0;
	dZ = GP[2] - photo.m_Z0;

	q = M.Mmatrix(2,0)*dX + M.Mmatrix(2,1)*dY + M.Mmatrix(2,2)*dZ;
	r = M.Mmatrix(0,0)*dX + M.Mmatrix(0,1)*dY + M.Mmatrix(0,2)*dZ;
	s = M.Mmatrix(1,0)*dX + M.Mmatrix(1,1)*dY + M.Mmatrix(1,2)*dZ;

	IP[0] = -camera.m_c*(r/q);
	IP[1] = -camera.m_c*(s/q);
}

bool CSSMaticsBundle::Make_b_J_K(CSSMPhoto photo, CSSMCamera camera, CSSMPoint GP, CSSMPoint p, double** b, double &J, double &K)
{
	double Omega = photo.m_Omega0;
	double Phi = photo.m_Phi0;
	double Kappa = photo.m_Kappa0;
	double f = camera.m_c;
	double xp = camera.m_xp;
	double yp = camera.m_yp;

	_Mmat_ M(Omega, Phi, Kappa);
	double q, r, s;
	double dX, dY, dZ;
	double fqq;
	
	dX = GP[0] - photo.m_X0;
	dY = GP[1] - photo.m_Y0;
	dZ = GP[2] - photo.m_Z0;

	q = M.Mmatrix(2,0)*dX + M.Mmatrix(2,1)*dY + M.Mmatrix(2,2)*dZ;
	r = M.Mmatrix(0,0)*dX + M.Mmatrix(0,1)*dY + M.Mmatrix(0,2)*dZ;
	s = M.Mmatrix(1,0)*dX + M.Mmatrix(1,1)*dY + M.Mmatrix(1,2)*dZ;
	
	fqq = f/(q*q);

	b[0][0] = fqq*(r*(-M.Mmatrix(2,2)*dY+M.Mmatrix(2,1)*dZ) - q*(-M.Mmatrix(0,2)*dY+M.Mmatrix(0,1)*dZ));
	b[0][1] = fqq*(r*(cos(Phi)*dX+sin(Omega)*sin(Phi)*dY-cos(Omega)*sin(Phi)*dZ)
		- q*(-sin(Phi)*cos(Kappa)*dX+sin(Omega)*cos(Phi)*cos(Kappa)*dY-cos(Omega)*cos(Phi)*cos(Kappa)*dZ));
	b[0][2] = -fqq*q*(M.Mmatrix(1,0)*dX+M.Mmatrix(1,1)*dY+M.Mmatrix(1,2)*dZ);
	b[0][3] = fqq*(r*M.Mmatrix(2,0)-q*M.Mmatrix(0,0));
	b[0][4] = fqq*(r*M.Mmatrix(2,1)-q*M.Mmatrix(0,1));
	b[0][5] = fqq*(r*M.Mmatrix(2,2)-q*M.Mmatrix(0,2));
	
	J = p[0] - xp + f*r/q;

	b[1][0] = fqq*(s*(-M.Mmatrix(2,2)*dY+M.Mmatrix(2,1)*dZ) - q*(-M.Mmatrix(1,2)*dY+M.Mmatrix(1,1)*dZ));
	b[1][1] = fqq*(s*(cos(Phi)*dX+sin(Omega)*sin(Phi)*dY-cos(Omega)*sin(Phi)*dZ)
		- q*(sin(Phi)*sin(Kappa)*dX-sin(Omega)*cos(Phi)*sin(Kappa)*dY+cos(Omega)*cos(Phi)*sin(Kappa)*dZ));
	b[1][2] = fqq*q*(M.Mmatrix(0,0)*dX+M.Mmatrix(0,1)*dY+M.Mmatrix(0,2)*dZ);
	b[1][3] = fqq*(s*M.Mmatrix(2,0)-q*M.Mmatrix(1,0));
	b[1][4] = fqq*(s*M.Mmatrix(2,1)-q*M.Mmatrix(1,1));
	b[1][5] = fqq*(s*M.Mmatrix(2,2)-q*M.Mmatrix(1,2));

	K = p[1] - yp + f*s/q;

	return TRUE;
}

bool CSSMaticsBundle::FindGCP(char* ID, CSSMPoint &G, CSSMPoint &copyG, int &index)
{
	int num_points = m_GCPs.GetNumItem();

	for(int i=0; i<num_points; i++)
	{
		CSSMPoint groundP;
		groundP = m_GCPs.GetAt(i);
		if(0 == strcmp(ID, groundP.m_ID))
		{
			G = groundP;
			copyG = m_CopyGCPs.GetAt(i);
			index = i;
			return true;
		}
	}
	
	return false;
}

bool CSSMaticsBundle::FindCamera(char* ID, CSSMCamera &Camera, CSSMCamera &copyCamera)
{
	int num_Cameras = m_Camera.GetNumItem();

	for(int i=0; i<num_Cameras; i++)
	{
		CSSMCamera cam = m_Camera.GetAt(i);
		
		if(0 == strcmp(ID, cam.m_ID))
		{
			Camera = cam;
			copyCamera = m_CopyCamera.GetAt(i);
			return true;
		}
	}
	
	return false;
}

void CSSMaticsBundle::FindFixedEOP(CSSMPhoto photo, CSMMatrix<double> &FixedEOP, CSMMatrix<double> &FreeEOP)
{
	for(int k=0; k<NUM_PHOTO_PARAM; k++)
	{
		double dispersion = photo.m_Dispersion(k,k);
		
		if(dispersion <= m_MinVar)
		{
			FixedEOP(k,0) = 0.0;
			FreeEOP(k,0) = 1.0;
		}
		else if(m_MinVar < dispersion && dispersion < m_MaxVar)
		{
			FixedEOP(k,0) = 1.0;
			FreeEOP(k,0) = 1.0;
		}
		else if(dispersion >= m_MaxVar)
		{
			FixedEOP(k,0) = 1.0;
			FreeEOP(k,0) = 0.0;
		}
	}
}

void CSSMaticsBundle::FindFixedPoint(CSSMPoint GP, CSMMatrix<double> &FixedPoint, CSMMatrix<double> &FreePoint)
{
	for(int k=0; k<3; k++)
	{
		double dispersion = GP.m_Dispersion(k,k);
		
		if(dispersion <= m_MinVar)
		{
			FixedPoint(k,0) = 0.0;
			FreePoint(k,0) = 1.0;
		}
		else if(m_MinVar < dispersion && dispersion < m_MaxVar)
		{
			FixedPoint(k,0) = 1.0;
			FreePoint(k,0) = 1.0;
		}
		else if(dispersion >= m_MaxVar)
		{
			FixedPoint(k,0) = 1.0;
			FreePoint(k,0) = 0.0;
		}
	}
}

void CSSMaticsBundle::ParameterUpdate(CSMMatrix<double> X)
{
	int i;
	int num_photos = m_Photo.GetNumItem(); //number of photos
	int num_points = m_GCPs.GetNumItem(); //Number of object points

	for(i=0; i<num_photos; i++)
	{
		CSSMPhoto* photo = m_Photo.GetHandleAt(i);
		
		photo->m_X0		+= X(i*NUM_PHOTO_PARAM + 0, 0);
		photo->m_Y0		+= X(i*NUM_PHOTO_PARAM + 1, 0);
		photo->m_Z0		+= X(i*NUM_PHOTO_PARAM + 2, 0);

		photo->m_Omega0	+= X(i*NUM_PHOTO_PARAM + 3, 0);
		photo->m_Phi0	+= X(i*NUM_PHOTO_PARAM + 4, 0);
		photo->m_Kappa0	+= X(i*NUM_PHOTO_PARAM + 5, 0);
	}

	int pos = num_photos*NUM_PHOTO_PARAM;
	
	for(i=0; i<num_points; i++)
	{
		CSSMPoint *GP = m_GCPs.GetHandleAt(i);

		GP->m_p[0] += X(pos + i*3 + 0, 0);
		GP->m_p[1] += X(pos + i*3 + 1, 0);
		GP->m_p[2] += X(pos + i*3 + 2, 0);
	}
}

void CSSMaticsBundle::MakeReport(fstream &report, CSMMatrix<double> X, double sigma, int iteration)
{
	report<<"-----------------------------------"<<endl;
	report<<"Iteration "<<iteration<<endl;
	report<<"-----------------------------------"<<endl;

	report.precision(6);
	report<<endl;
	report<<"posteriori sigma: "<<sigma<<endl<<endl;

	report.flush();

	int i;
	int num_photos = m_Photo.GetNumItem(); //number of photos
	int num_points = m_GCPs.GetNumItem(); //Number of object points

	report<<"EO Parameters"<<endl<<endl;
	report.flush();

	for(i=0; i<num_photos; i++)
	{
		CSSMPhoto* photo = m_Photo.GetHandleAt(i);
		report<<"Photo ID : "<<photo->m_ID<<endl;
		report<<"(X, Y, Z, Omega, Phi, and Kappa)"<<endl;
		report.precision(3);
		report<<photo->m_X0<<"\t"<<photo->m_Y0<<"\t"<<photo->m_Z0<<endl;
		report.precision(6);
		report<<Rad2Deg(photo->m_Omega0)<<"\t"<<Rad2Deg(photo->m_Phi0)<<"\t"<<Rad2Deg(photo->m_Kappa0)<<endl;
		report<<endl;
		report.flush();
	}

	int pos = num_photos*NUM_PHOTO_PARAM;
	report<<"Object point coordinates"<<endl<<endl;
	report<<"X, Y, and Z"<<endl;
	report.flush();

	for(i=0; i<num_points; i++)
	{
		CSSMPoint *GP = m_GCPs.GetHandleAt(i);

		report<<GP->m_ID<<endl;		
		report.precision(3);
		report<<GP->m_p[0]<<"\t"<<GP->m_p[1]<<"\t"<<GP->m_p[2]<<endl;
		report<<endl;
		report.flush();
	}
}

int CSSMaticsBundle::CheckImagePoint(char* ID, CSSMPhoto photo)
{
	CSSMCamera camera, copycamera;
	if(false == FindCamera(photo.m_CameraID, camera, copycamera)) return -1;
	
	int count=0;
	//int num_points = photo.m_ImagePoints.GetNumItem();
	int num_points = photo.GetNumberofPoints();

	for(int i=0; i<num_points; i++)
	{
		//CSSMPoint IP = photo.m_ImagePoints.GetAt(i);
		CSSMPoint IP = photo.GetImagePoint(camera, i);

		if(0 == strcmp(IP.m_ID, ID))
		{
			count++;
		}
	}

	return count;
}