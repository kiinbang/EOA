#ifndef __KDTREE_H__
#define __KDTREE_H__

//max KD tree points number
#define KD_MAX_POINTS 1000000    //if this number > 1.0^6, project setting should be adjusted.
#include "SMList.h"

inline double distance2(double* x1, double* x2, int dim)
{
	double S = 0;
	for(int k=0; k < dim; k++)
		S+= (x1[k]-x2[k])*(x1[k]-x2[k]);
	return S;
}

inline bool IsEqual(double* x1, double* x2, int dim)
{
	for(int k=0; k < dim; k++)
	{
		if(x1[k] != x2[k])
			return false;
	}
	
	return true ;
}

//KDTreeNode template class implementation

class KDNode
{
	//member functions
public:
	int axis ;
	double *x;
	unsigned int id ;
	bool checked ;
	bool orientation ;
	int sd;
	
	KDNode(double* x0, int axis0, unsigned int SD)
	{
		if(SD < 1)
		{
			x = NULL;
			::MessageBox(NULL, "SD < 1", "Worning", MB_OK);
			return;
		}
		else
		{
			sd = SD;
			x = new double[SD];
			axis = axis0;
			for(int k=0; k<(int)SD; k++)
				x[k] = x0[k];
			
			Left = Right = Parent = NULL ;
			checked = false ;
			id = 0;
		}
	}

	virtual ~KDNode()
	{
		if(x != NULL) delete[] x;
	}
	
	inline KDNode*	Insert(double* p, unsigned int SD)
	{
		KDNode* parent = FindParent(p);
		if(IsEqual(p, parent->x, SD))
			return NULL ;
		
		KDNode* newNode = new KDNode(p, parent->axis +1 < (int)SD? parent->axis+1:0, SD);
		newNode->Parent = parent ;
		
		if(p[parent->axis] > parent->x[parent->axis])
		{
			parent->Right = newNode ;
			newNode->orientation = 1 ;
		}
		else
		{
			parent->Left = newNode ;
			newNode->orientation = 0 ;
		}
		
		return newNode ;
	}

	inline KDNode*	FindParent(double* x0)
	{
		KDNode* parent ;
		KDNode* next = this ;
		int split ;
		while(next)
		{
			split = next->axis  ;
			parent = next ;
			if(x0[split] > next->x[split])
				next = next->Right ;
			else
				next = next->Left ;
		}
		return parent ;
	}
	
	KDNode* Parent ;
	KDNode* Left ;
	KDNode* Right ;
};


class KDTree
{
public:
	
	KDTree()
	{
		x_min = NULL;
		x_max = NULL; 
		max_boundary = NULL; 
		min_boundary = NULL;
	}

	KDTree(unsigned int sd)
	{
		x_min = NULL;
		x_max = NULL; 
		max_boundary = NULL; 
		min_boundary = NULL;

		InitialSetup(sd);		
	}

	inline void InitialSetup(unsigned int sd)
	{
		if(sd < 1)
		{
			sd = 1;
			::MessageBox(NULL, "SD = 1", "Worning", MB_OK);
		}
		
		if(sd < 1) sd = 1;
		
		SD = sd;
		Root = NULL ;
		KD_id = 1;
		nList = 0;
		
		x_min = new double[SD];
		x_max = new double[SD]; 
		
		max_boundary = new bool[SD];
		min_boundary = new bool[SD];	
	}
	
	virtual ~KDTree()
	{
		RemoveAll();
	}

	inline void RemoveAll()
	{
		nList  = 0;

		if(x_min != NULL) delete[] x_min;
		if(x_max != NULL) delete[] x_max;
		
		if(max_boundary != NULL) delete[] max_boundary;
		if(min_boundary != NULL) delete[] min_boundary;

		x_min = NULL;
		x_max = NULL; 
		max_boundary = NULL; 
		min_boundary = NULL;
	}
	
	unsigned int SD; //space dimension
	
	inline bool add(double* x)
	{
		if(nList >= KD_MAX_POINTS-1)
			return false; //can't add more points
		
		if(!Root)
		{
			Root =  new KDNode(x, 0, SD);
			Root->id = KD_id++ ;
			List[nList++] = Root ;
		}
		else
		{
			KDNode* pNode ;
			if((pNode=Root->Insert( x, SD)))
			{
				pNode->id = KD_id++ ;
				List[nList++] = pNode;
			}
		}
		
		return true ;
	}
	
	inline KDNode* find_nearest(double* x)
	{
		if(!Root)
			return NULL ;
		
		checked_nodes = 0;
		
		KDNode* parent = Root->FindParent(x);
		nearest_neighbour = parent ;
		d_min = distance2(x, parent->x, SD); ;
		
		if(IsEqual(x, parent->x, SD))
			return nearest_neighbour ;
		
		search_parent(parent, x);
		uncheck();
		
		return nearest_neighbour;
	}

	inline int search(CONST double *low, const double *high, KDNode *pParent, int level)
	{
		if(pParent == NULL) return 0;

		int comparison = 1;

		bool bSuitable =  true;
		for(int i=0; i<(int)SD; i++)
		{
			if( pParent->x[i] < low[i] || high[i] < pParent->x[i] )
			{
				bSuitable = false;
				break;
			}
		}
		
		if(bSuitable)
		{
			pParent->x;//selected
		}
		
		if( low[level] <= pParent->x[level] )
			comparison += search(low, high, pParent->Left, (level+1)%SD);
		if( pParent->x[level] <= high[level] )
			comparison += search(low, high, pParent->Right, (level+1)%SD);
		
		return comparison;
	}
	
	//sequential nearest neighbor search
	inline KDNode* find_nearest_brute(double* x)
	{
		if(!Root)
			return NULL;
		
		KDNode* nearest = Root ;
		double d ;
		d_min = distance2(Root->x, x, SD);
		for(int n=1; n<nList; n++)
		{
			d =  distance2(List[n]->x, x, SD);
			if(d < d_min)
			{
				nearest = List[n];
				d_min = d;
			}
		}
		
		return nearest ;
	}

	inline KDNode* GetAt(int n)
	{		
		if( n < 0 || n >= nList)
			return NULL;

		KDNode *node;

		node = List[n];
		return node;
	}
	
	inline	void check_subtree(KDNode* node, double* x)
	{
		if(!node || node->checked)
			return ;
		
		CheckedNodes[checked_nodes++] = node ;
		node->checked = true ;
		set_bounding_cube(node, x);
		
		int dim = node->axis ;
		double d= node->x[dim] - x[dim];
		
		if (d*d > d_min) {
			if (node->x[dim] > x[dim])
				check_subtree(node->Left, x);
			else 
				check_subtree(node->Right, x);
		}
		// If the distance from the key to the current value is 
		// less than the nearest distance, we still need to look
		// in both directions.
		else {
			check_subtree(node->Left, x);
			check_subtree(node->Right, x);
		}
	}
	
	inline  void set_bounding_cube(KDNode* node, double* x)
	{
		if(!node)
			return ;
		
		double d = (double)0;
		double dx;

		for(int k=0; k<(int)SD; k++)
		{
			dx = node->x[k]-x[k];
			if(dx > 0)
			{
				dx *= dx ;
				if(!max_boundary[k])
				{
					if(dx > x_max[k])
						x_max[k] = dx;
					if(x_max[k]>d_min)
					{
						max_boundary[k] =true;
						n_boundary++ ;
					}
				}
			}
			else 
			{
				dx *= dx ;
				if(!min_boundary[k])
				{
					if(dx > x_min[k])
						x_min[k] = dx;
					if(x_min[k]>d_min)
					{
						min_boundary[k] =true;
						n_boundary++ ;
					}
				}
			}
			d += dx;
			if(d>d_min)
				return ;
		}
		
		if(d<d_min)
		{
			d_min = d;
			nearest_neighbour = node ;
		}
	}
	
	inline KDNode* search_parent(KDNode* parent, double* x)
	{
		for(int k=0; k<(int)SD; k++)
		{
			x_min[k] = x_max[k] = 0;
			max_boundary[k] = min_boundary[k] = 0;
		}
		n_boundary = 0;
		
		KDNode* search_root = parent ;
		while(parent && n_boundary != 2*(int)SD)
		{	
			check_subtree(parent, x);
			search_root = parent ;
			parent = parent->Parent ;
		}
		
		return search_root ;
	}
	
	inline void uncheck()
	{
		for(int n=0; n<checked_nodes; n++)
			CheckedNodes[n]->checked = false;
	}
	
public:	
	KDNode*  Root ;
	double d_min ;
	KDNode* nearest_neighbour ;
	
	int KD_id  ;
	
	KDNode* List[KD_MAX_POINTS] ;
	int nList ;
	
	KDNode* CheckedNodes[KD_MAX_POINTS] ;
	int checked_nodes ;
	
	double *x_min, *x_max; 
	bool *max_boundary, *min_boundary;
	int n_boundary ;	
};

#endif