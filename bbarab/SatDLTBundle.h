// SatDLTBundle.h: interface for the CSatDLTBundle class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SATDLTBUNDLE_H__6BB46F00_075D_4AF7_BDC4_EA00782D17C0__INCLUDED_)
#define AFX_SATDLTBUNDLE_H__6BB46F00_075D_4AF7_BDC4_EA00782D17C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMaticsDLT.h"

class CSatDLTBundle  
{
public:
	CSatDLTBundle();
	virtual ~CSatDLTBundle();

//Member variables
private:
	//DLTParam *pParam;
	DLTParam LParam, RParam;
	//CICPManage *ICP;
	CICPManage LICP;
	CICPManage RICP;
	CGCPManage GCP;
//Member functions
public:
	bool DLTRO(CICPManage Licp, CICPManage Ricp);
	bool CheckTiePoint(CICPManage Licp, CICPManage Ricp);
	bool InitDLTROParam();

};

#endif // !defined(AFX_SATDLTBUNDLE_H__6BB46F00_075D_4AF7_BDC4_EA00782D17C0__INCLUDED_)
