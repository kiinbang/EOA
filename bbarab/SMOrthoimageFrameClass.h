/*
 * Copyright (c) 2002-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#if !defined(_SPACEMATICS_ORTHOIMAGE_FRAME_BASECLASS_)
#define _SPACEMATICS_ORTHOIMAGE_FRAME_BASECLASS_      
                                      
#if _MSC_VER > 1000                   
#pragma once                          
#endif // _MSC_VER > 1000             
                                      
#include <SMCoordinateClass.h>
#include <SMGrid.h>

using namespace SMATICS_BBARAB;

//////////////////////////////////////////////////////////////////////////////////////////////
//
//2006: Add CFrameOrthoClass Class <--Ortho gen with rigorous param for frame imagery
//          1)New ortho-photo class for frame imagery
//          2)Derived form CSatOrthoImage class
//			3)Completely new source code without modifying old codes
//
//
//
//
//
//
// SMOrthoimageFrame.h : header file
// Created by BANG, KI IN

/**
* @class CFrameOrthoClass
* @brief Revision: 2006-03-29\n
* This class is derived form CSatOrthoImage class.\n
* @author Bang, Ki In
* @version 1.0
* @Since 2006~
* @bug N/A.
* @warning N/A.
*
*/
class CFrameOrthoClass// : public CSatOrthoImage
{
	friend class CFrameOrthoImage;

	CFrameOrthoClass(){};
	virtual ~CFrameOrthoClass(){};

protected: //member variable
	double xp, yp, f; /*<xp, yp, focal length*/
	unsigned int LD_tag; /*<Lens distortion availablility info*/
	double k1, k2, d1, d2, a1, a2;/*<Lens distortion parameters*/
	double AffineParam[6];/*<Affine parameters for IO*/
	unsigned int MethodTag;/*<Tag of ortho generation method. 0: Simple differential rectification, 1: Slope method*/
	unsigned int ResamplingTag;/*<Tag of resampling method*/
	double Xo, Yo, Zo, Oo, Po, Ko;/*<6 EOPs*/
	CSMGrid<BYTE> *m_Source;/**<Source image */
	CSMGrid<BYTE> m_Ortho;/**<Ortho image */
	CSMGridDEM<double> *m_RefDEM;
	double m_NullDEM; /**<Null DEM value*/
	double MinZ;/**<Minimum value of DEM*/
	double MaxZ;/**<Maximum value of DEM*/

public: //member function

private:

	/**ColorResampleOrtho_Slope_DiffZ
	* Description	    : Color ortho image resampling by slope method
	*@return bool 
	*/
	bool ResampleOrtho_Slope_DiffZ();

	/**GroundCoord2SceneCoord
	* Description	    : To transform ground coordinates to scene coordinates
	*@param double X: ground X coordinate
	*@param double Y: ground Y coordinate
	*@param double Z: ground Z coordinate
	*@param double &row: image coordinate (row)
	*@param double &col: image coordinate (col)
	*@return bool 
	*/
	bool GroundCoord2SceneCoord(double X, double Y, double Z, Point2D<double> &point);

	/**CheckOcclusion_DiffZ
	* Description	    : to estimate occlusion area 
	*@param double Xl : perspective center coordinates(Xo)
	*@param double Yl : perspective center coordinates(Yo)
	*@param double Zl : perspective center coordinates(Zo)
	*@param int indexX : DEM index (for X direction)
	*@param int indexY : DEM index (for Y direction)
	*@param double MAXDIFFHEIGHT : serching limit
	*@return bool 
	*/
	bool CheckOcclusion_DiffZ(double Xl, double Yl, double Zl, int indexX, int indexY, double MAXDIFFHEIGHT);
	
	
	/**SetConfig
	* Description						: set configuration and data
	*@param CSMGrid<BYTE> &source		: source image data
	*@param CSMGridDEM<double> &dem		: dem
	*@param double focal					: focal length
	*@param double ppx					: xp
	*@param double ppy					: yp
	*@param double *LDParam				: parametrers of lens distortion
	*@param double *Affine				: affine parameters for image 2 photo transform
	*@param double *Pos					: perspective center(X,Y,Z)
	*@param double *Ori					: attitudes(O,P,K)
	*@param int nMethod					: rectification method (normal, slope method)
	*@param int nResample				: resampling method (neareast, bilinear)
	*@return void 
	*/
	void SetConfig(CSMGrid<BYTE> &source, CSMGridDEM<double> &dem, double focal, double ppx, double ppy, double *LDParam, double *Affine, double *Pos, double *Ori, int nMethod, int nResample);

};


/////////////////////////////////////////////////////////////////////////////

#endif // !defined(_SPACEMATICS_ORTHOIMAGE_FRAME_BASECLASS_)