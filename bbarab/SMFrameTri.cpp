/////////////////////////////////////////////////////////
//PushbroomTri.cpp
//made by bbarab
//revision: 2000-9-29
//revision: 2000-10-4
//인하대학교 토목공학과 지형정보연구실
//phshbroom image의 triangulation
///////////////////////////////////////////////////////////////
//revision: 2001-11-12
//ETRI: 위성영상 에서 항공사진용으로 개조
//Aerial Triangulation Program
///////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SMFrameTri.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////
//class CFrameBundle
/////////////////////////////
CFrameBundle::CFrameBundle()
{
	gcp = NULL;
	check = NULL;
	check_T = NULL;
	pass = NULL;
	frame = NULL;
}

bool CFrameBundle::OpenDataFile(char *fname)
{
	//Data Reset
	ResetData();

	FILE* indata;
	unsigned long i,j;
	const int MAX_SIZE = 256;
	char st[MAX_SIZE];

	//file open
	indata = fopen(fname,"r");

	fgets(st, MAX_SIZE, indata);

	//num_frame, num_GCP, num_Check, num_Pass
	fscanf(indata,"%d",&num_GCP);
	fscanf(indata,"%d",&num_Check);
	fscanf(indata,"%d",&num_Pass);
	fscanf(indata,"%d",&num_Frame);
	fgets(st,MAX_SIZE,indata);

	if((num_GCP)>0)
			gcp = new CGroundPoint[num_GCP];
	if((num_Check)>0)
			check = new CGroundPoint[num_Check];
	if((num_Check)>0)
			check_T = new CGroundPoint[num_Check];
	if((num_Pass)>0)
			pass = new CGroundPoint[num_Pass];
	if((num_Frame)>0)
			frame = new CFrame[num_Frame];
	
	fgets(st,MAX_SIZE,indata);
	//Frame ID, Position, Attitude, num of frame point, point data
	for(i=0;i<num_Frame;i++)
	{
		fscanf(indata,"%d",&frame[i].FrameID);
		fscanf(indata,"%lf",&frame[i].f);
		fscanf(indata,"%lf",&frame[i].PC.x);
		fscanf(indata,"%lf",&frame[i].PC.y);
		fscanf(indata,"%lf",&frame[i].PC.z);
		fscanf(indata,"%lf",&frame[i].Omega);
		fscanf(indata,"%lf",&frame[i].Phi);
		fscanf(indata,"%lf",&frame[i].Kappa);
		fscanf(indata,"%d",&frame[i].num_point);

		frame[i].framepoint = new CScenePoint[frame[i].num_point];
		for(j=0;j<frame[i].num_point;j++)
		{
			fscanf(indata,"%d",&frame[i].framepoint[j].PointID);
			fscanf(indata,"%d",&frame[i].framepoint[j].PointProperty);
			fscanf(indata,"%lf",&frame[i].framepoint[j].coord.x);
			fscanf(indata,"%lf",&frame[i].framepoint[j].coord.y);
		}

	}
	
	fgets(st,MAX_SIZE,indata);

	fgets(st,MAX_SIZE,indata);
	//Point ID, GCP(x,y,z)
	for(i=0;i<num_GCP;i++)
	{
		fscanf(indata,"%d",&gcp[i].PointID);
		fscanf(indata,"%lf",&gcp[i].coord.x);
		fscanf(indata,"%lf",&gcp[i].coord.y);
		fscanf(indata,"%lf",&gcp[i].coord.z);
	}
	fgets(st,MAX_SIZE,indata);

	fgets(st,MAX_SIZE,indata);
	//Point ID, Check(x,y,z)
	for(i=0;i<num_Check;i++)
	{
		fscanf(indata,"%d",&check[i].PointID);
		check_T[i].PointID = check[i].PointID;
		fscanf(indata,"%lf",&check[i].coord.x);
		check_T[i].coord.x = check[i].coord.x;
		fscanf(indata,"%lf",&check[i].coord.y);
		check_T[i].coord.y = check[i].coord.y;
		fscanf(indata,"%lf",&check[i].coord.z);
		check_T[i].coord.z = check[i].coord.z;
	}
	fgets(st,MAX_SIZE,indata);

	fgets(st,MAX_SIZE,indata);
	//Pass Point ID
	for(i=0;i<num_Pass;i++)
	{
		fscanf(indata,"%d",&pass[i].PointID);
		fscanf(indata,"%lf",&pass[i].coord.x);
		fscanf(indata,"%lf",&pass[i].coord.y);
		fscanf(indata,"%lf",&pass[i].coord.z);
	}
	fgets(st,MAX_SIZE,indata);

	fgets(st,MAX_SIZE,indata);
	//max iteration
	fscanf(indata,"%d",&maxiteration);
	fgets(st,MAX_SIZE,indata);

	fgets(st,MAX_SIZE,indata);
	//max correction
	fscanf(indata,"%lf",&maxcorrection);
	fgets(st,MAX_SIZE,indata);
		
	fclose(indata);
	
	return true;
}

CFrameBundle::~CFrameBundle()
{
	if(gcp != NULL)
	{
		delete[] gcp;
		gcp = NULL;
	}
	
	if(check != NULL)
	{
		delete[] check;
		check = NULL;
	}

	if(check_T != NULL)
	{
		delete[] check_T;
		check_T = NULL;
	}

	if(pass != NULL)
	{
		delete[] pass;
		pass = NULL;
	}

	if(frame != NULL)
	{
		delete[] frame;
		frame = NULL;
	}
}

void CFrameBundle::ResetData()
{
	if(gcp != NULL)
	{
		delete[] gcp;
		gcp = NULL;
	}
	
	if(check != NULL)
	{
		delete[] check;
		check = NULL;
	}

	if(check_T != NULL)
	{
		delete[] check_T;
		check_T = NULL;
	}

	if(pass != NULL)
	{
		delete[] pass;
		pass = NULL;
	}

	if(frame != NULL)
	{
		for(unsigned int i=0; i<num_Frame; i++)
			frame[i].DeleteData();
		delete[] frame;
		frame = NULL;
	}
}

bool CFrameBundle::Solve(void)
{
	CSMStr temp;

	unsigned long i;
	unsigned long j, iteration=0;
	unsigned long sum = 0; //number of image point
	bool stop = false;	//iteration stop

	Matrix<double> AT, N, Ninv, VTV;
	double Posteriori_Var;
	double SD;
	Matrix<double>Var_Cov;
	Matrix<double>Var;

	double maxX;
	
	//output file head
	result = "[Frame Image Bundle Adjustment Ver 1.0]\r\n";
	result += "-Made by Ki-In Bang-\r\n";
	result += "kiinbang@etri.re.kr\r\n";

	for(i=0;i<num_Frame;i++)
	{
		sum += frame[i].num_point;
	}
	DF = sum*2 - 6*num_Frame - (num_Pass+num_Check)*3;

	do
	{
		iteration ++;
		temp.Format("--[%d]--\r\n",iteration);
		result += temp;
		//A & L matrix
		if(FALSE == Make_A_L_Matrix())
		{ return FALSE; }

		AT = A.Transpose();
		N = AT%A;
		
		Ninv = N.Inverse();
		
		X = Ninv%AT%L;
		V = A%X - L;

		if(DF>0)
		{
			VTV = V.Transpose()%V;
			Posteriori_Var = VTV(0,0)/DF;
			SD = sqrt(Posteriori_Var);
			Var_Cov = Ninv*Posteriori_Var;
			Var.Resize((int)Var_Cov.GetRows(),1);
			for(int j=0;j<(int)Var_Cov.GetRows();j++)
			{
				Var(j,0) = Var_Cov(j,j);
			}
			
		}
		
		maxX = fabs(X(0));

		for(i=1;i<(int)X.GetRows();i++)
		{
			if(maxX<fabs(X(i)))
				maxX = fabs(X(i));
		}
		
		for(i=0;i<num_Frame;i++)
		{
			frame[i].Omega += X(i*6+0);
			frame[i].Phi   += X(i*6+1);
			frame[i].Kappa += X(i*6+2);
			frame[i].PC.x  += X(i*6+3);
			frame[i].PC.y  += X(i*6+4);
			frame[i].PC.z  += X(i*6+5);
		}

		for(j=0;j<num_Pass;j++)
		{
			pass[j].coord.x += X(6*num_Frame+3*j);
			pass[j].coord.y += X(6*num_Frame+3*j+1);
			pass[j].coord.z += X(6*num_Frame+3*j+2);
		}

		for(j=0;j<num_Check;j++)
		{
			check[j].coord.x += X(6*num_Frame+3*num_Pass+3*j);
			check[j].coord.y += X(6*num_Frame+3*num_Pass+3*j+1);
			check[j].coord.z += X(6*num_Frame+3*num_Pass+3*j+2);
		}

		result += "--[A Matrix]--\r\n";
		result += A.matrixout();
		result += "--[X Matrix]--\r\n";
		result += X.matrixout();
		result += "--[V Matrix]--\r\n";
		result += V.matrixout();
		result += "--[L Matrix]--\r\n";
		result += L.matrixout();
		temp.Format("SD: [%lf]\r\n",SD);
		result += temp;
		temp.Format("Max Correction(X): [%lf]\r\n",maxX);
		result += temp;

		for(unsigned int count=0; count<num_Frame; count++)
		{
			temp.Format("[%d]Omega: %lf\r\n",count,frame[count].Omega);
			result += temp;
			temp.Format("[%d]Phi: %lf\r\n",count,frame[count].Phi);
			result += temp;
			temp.Format("[%d]Kappa: %lf\r\n",count,frame[count].Kappa);
			result += temp;
			temp.Format("[%d]X: %lf\r\n",count,frame[count].PC.x);
			result += temp;
			temp.Format("[%d]Y: %lf\r\n",count,frame[count].PC.y);
			result += temp;
			temp.Format("[%d]Z: %lf\r\n",count,frame[count].PC.z);
			result += temp;
		}

		if(iteration == maxiteration)
		{
			stop = true;
			result += "!!! Execss Max Iteration\r\n";
		}
		
		if(maxX < maxcorrection)
		{
			stop = TRUE;
			result += "!!! Max Correction < Limit Correction\r\n";
		}

	}while(stop == FALSE);

	result += "\r\n\r\n\r\n";
	result += "Iteration = ";
	temp.Format("%d\r\n",iteration);
	result += temp;

	return true;
}

bool CFrameBundle::Make_A_L_Matrix(void)
{
	unsigned long i,j,k;
	short offset;
	unsigned long sort_check;
	unsigned long sort_pass;
	unsigned long sum_imagepoint = 0;

	for(i=0;i<num_Frame;i++)
		sum_imagepoint += frame[i].num_point;
	
	A.Resize(2*sum_imagepoint,6*num_Frame+3*(num_Pass+num_Check),0.0);
	L.Resize(2*sum_imagepoint,1,0.0);
	
	double omega, phi, kappa;
	Point2D<double> photo_point;
	Point3D<double> camera_position;
	Point3D<double> ground_point;
	
	offset = -1;
	for(i=0;i<num_Frame;i++)
	{
		for(j=0;j<frame[i].num_point;j++)
		{
			offset ++;
			camera_position.x = frame[i].PC.x;
			camera_position.y = frame[i].PC.y;
			camera_position.z = frame[i].PC.z;

			photo_point.x = frame[i].framepoint[j].coord.x;
			photo_point.y = frame[i].framepoint[j].coord.y;

			omega = frame[i].Omega;
			phi   = frame[i].Phi  ;
			kappa = frame[i].Kappa;
			
			if(1==frame[i].framepoint[j].PointProperty)//GCP
			{
				for(k=0;k<num_GCP;k++)
				{
					if(frame[i].framepoint[j].PointID == gcp[k].PointID)
					{
						ground_point = gcp[k].coord;
						break;
					}
				}
				
				if(!Make_b_J_K(camera_position,ground_point,photo_point,omega,phi,kappa,frame[i].f))
				{
					return FALSE;
				}

				Manipulate_A_L(offset,i);

			}
			
			else if(2==frame[i].framepoint[j].PointProperty)//Check
			{
				for(k=0;k<num_Check;k++)
				{
					if(frame[i].framepoint[j].PointID == check[k].PointID)
					{
						ground_point = check[k].coord;
						sort_check = k;
						break;
					}
				}
				
				if(!Make_b_J_K(camera_position,ground_point,photo_point,omega,phi,kappa,frame[i].f))
				{
					return FALSE;
				}

				Manipulate_A_L(offset,i);

				A(offset*2,  6*num_Frame+3*num_Pass+sort_check*3+0) =  b14;
				A(offset*2,  6*num_Frame+3*num_Pass+sort_check*3+1) =  b15;
				A(offset*2,  6*num_Frame+3*num_Pass+sort_check*3+2) =  b16;
				
				A(offset*2+1,6*num_Frame+3*num_Pass+sort_check*3+0) =  b24;
				A(offset*2+1,6*num_Frame+3*num_Pass+sort_check*3+1) =  b25;
				A(offset*2+1,6*num_Frame+3*num_Pass+sort_check*3+2) =  b26;

			}
			
			else if(3==frame[i].framepoint[j].PointProperty)//Pass
			{
				for(k=0;k<num_Pass;k++)
				{
					if(frame[i].framepoint[j].PointID == pass[k].PointID)
					{
						ground_point = pass[k].coord;
						sort_pass = k;
						break;
					}
				}
				
				if(!Make_b_J_K(camera_position,ground_point,photo_point,omega,phi,kappa,frame[i].f))
				{
					return FALSE;
				}
				
				Manipulate_A_L(offset,i);

				A(offset*2,6*num_Frame+sort_pass*3+0) =  b14;
				A(offset*2,6*num_Frame+sort_pass*3+1) =  b15;
				A(offset*2,6*num_Frame+sort_pass*3+2) =  b16;
				
				A(offset*2+1,6*num_Frame+sort_pass*3+0) =  b24;
				A(offset*2+1,6*num_Frame+sort_pass*3+1) =  b25;
				A(offset*2+1,6*num_Frame+sort_pass*3+2) =  b26;
				
			}
			else
			{
				return FALSE;
			}

		} //for(j) 문
			
	} //for(i) 문

	return true;
}

bool CFrameBundle::InitEOParameters(void)
{
	unsigned long i, j;
	long k = -1;

	Point2D<double> *totalphoto;
	totalphoto = new Point2D<double>[num_ScenePoint];
	
	//Initialize each photo's attitude
	for(i=0; i<num_Frame; i++)
	{
		frame[i].Omega = 0.0;
		frame[i].Phi = 0.0;
		//frame[i].Kappa = 0.0;
	}

	//Initialize each lens's position
	for(i=0; i<num_Frame; i++)
	{
		for(j=0; j<frame[i].num_point; j++)
		{
			k ++;
			totalphoto[k].x = frame[i].framepoint[j].coord.x;
			totalphoto[k].y = frame[i].framepoint[j].coord.y;
		}
	}
	return true;
}

bool CFrameBundle::Make_b_J_K(Point3D<double> PC, Point3D<double> GP, Point2D<double> p, double Omega, double Phi, double Kappa, double f)
{
	CRotationcoeff M(Omega,Phi,Kappa);
	double q, r, s;
	double dX, dY, dZ;
	double fqq;

	dX = GP.x - PC.x;
	dY = GP.y - PC.y;
	dZ = GP.z - PC.z;

	q = M.Rmatrix(2,0)*dX + M.Rmatrix(2,1)*dY + M.Rmatrix(2,2)*dZ;
	r = M.Rmatrix(0,0)*dX + M.Rmatrix(0,1)*dY + M.Rmatrix(0,2)*dZ;
	s = M.Rmatrix(1,0)*dX + M.Rmatrix(1,1)*dY + M.Rmatrix(1,2)*dZ;
	
	fqq = f/(q*q);

	b11 = fqq*(r*(-M.Rmatrix(2,2)*dY+M.Rmatrix(2,1)*dZ) - q*(-M.Rmatrix(0,2)*dY+M.Rmatrix(0,1)*dZ));
	b12 = fqq*(r*(cos(Phi)*dX+sin(Omega)*sin(Phi)*dY-cos(Omega)*sin(Phi)*dZ)
		- q*(-sin(Phi)*cos(Kappa)*dX+sin(Omega)*cos(Phi)*cos(Kappa)*dY-cos(Omega)*cos(Phi)*cos(Kappa)*dZ));
	b13 = -fqq*q*(M.Rmatrix(1,0)*dX+M.Rmatrix(1,1)*dY+M.Rmatrix(1,2)*dZ);
	b14 = fqq*(r*M.Rmatrix(2,0)-q*M.Rmatrix(0,0));
	b15 = fqq*(r*M.Rmatrix(2,1)-q*M.Rmatrix(0,1));
	b16 = fqq*(r*M.Rmatrix(2,2)-q*M.Rmatrix(0,2));
	
	J = p.x + f*r/q;

	b21 = fqq*(s*(-M.Rmatrix(2,2)*dY+M.Rmatrix(2,1)*dZ) - q*(-M.Rmatrix(1,2)*dY+M.Rmatrix(1,1)*dZ));
	b22 = fqq*(s*(cos(Phi)*dX+sin(Omega)*sin(Phi)*dY-cos(Omega)*sin(Phi)*dZ)
		- q*(sin(Phi)*sin(Kappa)*dX-sin(Omega)*cos(Phi)*sin(Kappa)*dY+cos(Omega)*cos(Phi)*sin(Kappa)*dZ));
	b23 = fqq*q*(M.Rmatrix(0,0)*dX+M.Rmatrix(0,1)*dY+M.Rmatrix(0,2)*dZ);
	b24 = fqq*(s*M.Rmatrix(2,0)-q*M.Rmatrix(1,0));
	b25 = fqq*(s*M.Rmatrix(2,1)-q*M.Rmatrix(1,1));
	b26 = fqq*(s*M.Rmatrix(2,2)-q*M.Rmatrix(1,2));

	K = p.y + f*s/q;

	return TRUE;
}

void CFrameBundle::Manipulate_A_L(unsigned long offset1, unsigned long offset2)
{
	A(offset1*2,offset2*6+0)  =  b11;
	A(offset1*2,offset2*6+1)  =  b12;
	A(offset1*2,offset2*6+2)  =  b13;
	A(offset1*2,offset2*6+3)  = -b14;
	A(offset1*2,offset2*6+4)  = -b15;
	A(offset1*2,offset2*6+5)  = -b16;
				
	L(offset1*2,0) = J;

	A(offset1*2+1,offset2*6+0)  =  b21;
	A(offset1*2+1,offset2*6+1)  =  b22;
	A(offset1*2+1,offset2*6+2)  =  b23;
	A(offset1*2+1,offset2*6+3)  = -b24;
	A(offset1*2+1,offset2*6+4)  = -b25;
	A(offset1*2+1,offset2*6+5)  = -b26;
		
	L(offset1*2+1,0) = K;
}





/*********************************************************/
/*                                                       */
/*항공사진 스트립을 위한 항공삼각 측량 프로그램 2002 버전*/
/*                                                       */
/*********************************************************/

CFrameBundle2002::CFrameBundle2002()
{
	m_PhotoData = NULL;
	m_CameraData = NULL;

	MAX_ITERATION = 10;
	MAX_CORRECTION = 0.000001;
}

CFrameBundle2002::~CFrameBundle2002()
{
	ResetData();
}

bool CFrameBundle2002::Approximation(int IO_option)
{
	int i;

	//
	//Interior orientation
	//
	RunIO(IO_option);

	//
	//Block coordinate system
	//
	for(i=1; i<num_Photo; i++)
	{
		Affine affine;
		
		KnownPoint PQ;
	}

	return true;
}

bool CFrameBundle2002::RunIO(int option)
{
	int i;
	
	for(i=0; i<num_Photo; i++)
	{
		//IO processing
		m_PhotoData[i].IO(option);
	}
	return true;
}


//////////////////////
//Cameras Data Setting
//////////////////////

bool CFrameBundle2002::SetCamera(int index, CCamera camera)
{
	if(index < num_Camera)
	{
		m_CameraData[index] = camera;
		return true;
	}
	else
	{
		return false;
	}
}

bool CFrameBundle2002::GetCamera(int index, CCamera *camera)
{
	if(index < num_Camera)
	{
		*camera = m_CameraData[index];
		return true;
	}
	else
	{
		return false;
	}
}

//////////////////////
//Photos Data Setting
//////////////////////

void CFrameBundle2002::SetNumPhoto(int num)
{
	num_Photo = num;
	if(NULL != m_PhotoData)
	{
		m_PhotoData = new CPhoto[num_Photo];
	}
	else
	{
		delete[] m_PhotoData;
		m_PhotoData = new CPhoto[num_Photo];
	}
}

bool CFrameBundle2002::SetPhoto(int index, CPhoto photo)
{
	if(index < num_Photo)
	{
		m_PhotoData[index] = photo;
		return true;
	}
	else
	{
		return false;
	}
} 

bool CFrameBundle2002::GetPhoto(int index, CPhoto *photo)
{
	if(index < num_Photo)
	{
		*photo = m_PhotoData[index];
		return true;
	}
	else
	{
		return false;
	}
} 

bool CFrameBundle2002::SetCameraToPhoto(int camera_index, int photo_index)
{
	if(camera_index < num_Camera)
	{
		if(photo_index < num_Photo)
		{
			m_PhotoData[photo_index].SetCamera(m_CameraData[camera_index]);
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool CFrameBundle2002::GetCameraOnPhoto(int index, CCamera *camera)
{
	if(index < num_Photo)
	{
		*camera = m_PhotoData[index].GetCamera();
		return true;
	}
	else
	{
		return false;
	}
}

bool CFrameBundle2002::SetPhotoFMImageCoord(int photo_index, int fm_index, Point2D<double> coord)
{
	if(photo_index < num_Photo)
	{
		if(fm_index < m_PhotoData[photo_index].GetCamera().GetNumFM())
		{
			m_PhotoData[photo_index].SetFiducialImageCoord(fm_index,coord);
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool CFrameBundle2002::GetPhotoFMImageCoord(int photo_index, int fm_index, Point2D<double> *coord)
{
	if(photo_index < num_Photo)
	{
		if(fm_index < m_PhotoData[photo_index].GetCamera().GetNumFM())
		{
			*coord = m_PhotoData[photo_index].GetFiducial(fm_index);
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool CFrameBundle2002::GetImage2Photo(int photo_index, Point2D<double> Icoord, Point2D<double> *Pcoord)
{
	if(photo_index < num_Photo)
	{
		*Pcoord = m_PhotoData[photo_index].GetImageToPhotoCoord(Icoord);
		return true;
	}
	else
	{
		return false;
	}
}

bool CFrameBundle2002::IsIOComplete(int photo_index, bool *check)
{
	if(photo_index < num_Photo)
	{
		*check = m_PhotoData[photo_index].IsIOComplete();
		return true;
	}
	else
	{
		return false;
	}
}

bool CFrameBundle2002::OpenDataFile(char *fname)
{
	//Data Reset
	ResetData();

	FILE* indata;
	int i,j;
	const int MAX_SIZE = 256;
	char st[MAX_SIZE];

	//file open
	indata = fopen(fname,"r");

	//Camera Data
	fgets(st, MAX_SIZE, indata);
	fscanf(indata,"%d",&num_Camera);
	SetNumCamera(num_Camera);
	
	for(i=0; i<num_Camera; i++)
	{
		int id;
		double FL;
		Point2D<double> PPA, PPBS;
		int num_FM;
		fscanf(indata,"%d %lf %lf %lf %lf %lf %d", &id, &FL, &PPA.x, &PPA.y, 
			                                                   &PPBS.x, &PPBS.y, &num_FM);

		CCamera camera;
		camera.ID = id;
		camera.f = FL;
		camera.PPA = PPA;
		camera.PPBS = PPBS;
		camera.SetNumFM(num_FM);

		for(j=0; j<num_FM; j++)
		{
			Point2D<double> fm;
			fscanf(indata, "%lf %lf", &fm.x, &fm.y);
			camera.fiducial[j].x = fm.x;
			camera.fiducial[j].y = fm.y;
		}

		if(!SetCamera(i, camera)) return false;
	}
	fgets(st, MAX_SIZE, indata);

	//Photo Data
	fgets(st, MAX_SIZE, indata);
	fscanf(indata,"%d",&num_Photo);
	SetNumPhoto(num_Photo);

	for(i=0; i<num_Photo; i++)
	{
		CPhoto photo;
		int id;
		int camera_id;

		fscanf(indata, "%d %d", &id, &camera_id);

		photo.SetPhotoID(id);
		SetCameraToPhoto(camera_id, i);

		for(j=0; j<photo.GetCamera().GetNumFM(); j++)
		{
			Point2D<double> coord;
			fscanf(indata, "%lf %lf", &coord.x, &coord.y);
			SetPhotoFMImageCoord(i, j, coord);
		}

		int num_point;
		fscanf(indata, "%d", &num_point);
		photo.SetNumImagePoint(num_point);
		
		for(j=0; j<num_point; j++)
		{
			int point_id, property;
			double col, row;
			fscanf(indata, "%d %d %lf %lf", &point_id, &property, &col, &row);
			photo.SetImagePoint(j, point_id, property, col, row);
			m_Pass.InsertGCP(-999.0, -999.0, -999.0, point_id);
		}
	}
	fgets(st, MAX_SIZE, indata);

	//GCP data
	fgets(st, MAX_SIZE, indata);

	fscanf(indata, "%d", &num_GCP);
	for(i=0; i<num_GCP; i++)
	{
		int id;
		double x, y, z;
		fscanf(indata, "%d %lf %lf %lf", &id, &x, &y, &z);
		m_GCP.InsertGCP(x, y, z, id);
	}
	fgets(st, MAX_SIZE, indata);

	//Check point data
	fgets(st, MAX_SIZE, indata);

	fscanf(indata, "%d", &num_Check);
	for(i=0; i<num_Check; i++)
	{
		int id;
		double x, y, z;
		fscanf(indata, "%d %lf %lf %lf", &id, &x, &y, &z);
		m_Check.InsertGCP(x, y, z, id);
	}
	fgets(st, MAX_SIZE, indata);

	//Iteration condition
	fgets(st, MAX_SIZE, indata);
	fscanf(indata, "%d", &MAX_ITERATION);
	fgets(st, MAX_SIZE, indata);

	fgets(st, MAX_SIZE, indata);
	fscanf(indata, "%lf", &MAX_CORRECTION);
	fgets(st, MAX_SIZE, indata);

	fclose(indata);
	
	return true;
}

void CFrameBundle2002::ResetData()
{
	if(m_CameraData != NULL)
	{
		delete[] m_CameraData;
		m_CameraData = NULL;
	}

	if(m_PhotoData != NULL)
	{
		delete[] m_PhotoData;
		m_PhotoData = NULL;
	}

}
void test(void)
{
	CPhoto temp;
//	temp.SetCamera(CCamera c);
	//temp.SetFiducialImageCoord(index,Point2D);
	//temp.GetImageToPhotoCoord(point2D imagecoord);
	temp.IsIOComplete();
//	temp.SetPhotoID(int ID);

}