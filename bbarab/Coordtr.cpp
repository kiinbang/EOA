/******************************************************************
*                                                                 *
*   coordtr.cpp                                                     *
*                                                                 *
*******************************************************************
*                                                                 *
*   Header file for Coordinate Transformation.                    *
*                                                                 *
*   This header file contains functions as follows,               *
*                                                                 *
*     void geodet2geocen(lambda, phi, h, &x, &y, &z, a, b)        *
*     {  geodetic coord. -> geocentric coord. }                   *
*                                                                 *
*     void geocen2geodet(x, y, z, &lambda, &phi, &h, a, b)        *
*     {  geocentric coord. -> geodetic coord. }                   *
*                                                                 *
*     void wgs2bessel(xw,yw,zw,xb,yb,zb)                          *
*     {  WGS84 geocen. coord. -> Bessel geocen. coord. }          *
*                                                                 *
*     void bessel2wgs(xb,yb,zb,xw,yw,zw)                          *
*     {  Bessel geocen. coord. -> WGS84 geocen. coord. }          *
*                                                                 *
*     int tmproj(lambda, phi, xnorth, yeast)                      *
*     {  geodetic coord. -> plane coord.                          *
*        (using the TM projection for the Korean datum)  }        *
*                                                                 *
*     int utmproj(lambda, phi, north, east, a, b)                 *
*     {  geodetic coord. -> UTM coord. }                          *
*                                                                 *
*     void gauss_kruger(lambda, phi, x, y, centmeri, a, b)        *
*     {  geodetic coord. -> TM projected plane coord. }           *
*                                                                 *
*     int inv_tmproj(xnorth, yeast, lambda, phi, oflag)           *
*     {  plane coord. -> geodetic coord.                          *
*        (using the TM projection for the Korean datum)           *
*                                                                 *
*     int inv_utmproj(lambda, phi, north, east, a, b)             *
*     {  UTM coord. -> geodetic coord. }                          *
*                                                                 *
*     void inv_gauss_kruger(lambda, phi, x, y, centmeri, a, b)    *
*     {  TM projected plane coord. -> geodetic coord. }           *
*                                                                 *
*******************************************************************
*                                                                 *
*        Programmed by Jeong, Soo                                 *
*        Date : FEB. 1996.                                        *
*                                                                 *
******************************************************************/

#include "coordtr.h"


/******************************************************************
*  Function to convert                                            *
*    geodetic coord.(lambda,phi,h) -> geocentric coord.(x,y,z)    *
*                                                                 *
*   In this fuction,  a : radius of major axis                    *
*                     b : radius of minor axis                    *

*   And, the unit of lambda, phi is radian.                       *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/ 
void geodet2geocen(double lambda, double phi, double h, double* x, double* y, double* z, double a, double b)
//double lambda, phi, h, a, b;
//double *x, *y, *z;
{
   double e, n;

   /* e : first eccentricity
      n : radius of curvature in the prime vertical */

   e = sqrt(a*a - b*b) / a;
   n = a / sqrt(1. - e*e*sin(phi)*sin(phi));
  
   *x = (n+h)*cos(phi)*cos(lambda);
   *y = (n+h)*cos(phi)*sin(lambda);
   *z = ( n*(1.-e*e)+h )*sin(phi);

}



/******************************************************************
*  Function to convert                                            *
*    geocentric coord.(x,y,z) -> geodetic coord.(lambda,phi,h)    *
*                                                                 *
*   In this fuction,  a : radius of major axis                    *
*                     b : radius of minor axis                    *
*   And, the unit of lambda, phi is radian.                       *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/ 

void geocen2geodet(double x, double y, double z, double* lambda, double* phi, double* h, double a, double b)
//double x, y, z, a, b;
//double *phi, *lambda, *h;

{
   double e, n;
   double p_i, h_e, p_e;

   /* e : first eccentricity
      n : radius of curvature in the prime vertical 
      p_i : initial value of phi
      p_e : estimated value of phi
      h_e : estimated value of height
   */ 

   e = sqrt(a*a - b*b) / a;

   *lambda = ARCTAN(y,x);

   p_i = atan(z/((1.-e*e)*sqrt(x*x + y*y)));
 
   h_e = z / sin(p_i) - a*(1.-e*e)/sqrt(1-e*e*sin(p_i)*sin(p_i));
 
   n = a / sqrt(1.- e*e*sin(p_i)*sin(p_i));
   p_e = atan( (n+h_e)*z / ( ((1.-e*e)*n+h_e)*sqrt(x*x+y*y) ) );
  
   while(p_i!=p_e)
   {
      h_e = z / sin(p_e) - a*(1.-e*e)/sqrt(1-e*e*sin(p_e)*sin(p_e));
 
      p_i=p_e;

      n = a / sqrt(1.- e*e*sin(p_i)*sin(p_i));  
      p_e = atan( (n+h_e)*z / ( ((1.-e*e)*n+h_e)*sqrt(x*x+y*y) ) );
  }

   *phi = p_e;
   *h = h_e;
}



/******************************************************************
*   Function to convert                                           *
*      WGS84 geocentric coord.(xw,yw,zw)                          *
*                  -> Bessel geocentric coord.(xb,yb,zb)          *
*                                                                 *
*      This fucntion uses 7-parameter transformation method.      *
*          Xb = s*R*Xw + dX                                       *
*                   where, Xb : Bessel geocentric coord.          *
*                          Xw : WGS84 geocentric coord.           *
*                          R  : rotation matrix                   *
*                          dX : datum shifts(Xb-Xw)               *
*                          s  : scale                             *
*      The parameters are provided by Dr. Lee, Yongchang          *
*      (PHD Thesis of Chungnam Univ., 1992. 2.,pp104-105)         *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/ 
void wgs2bessel(double xw,double yw,double zw,double* xb,double* yb, double* zb)
//double xw,yw,zw;
//double *xb,*yb,*zb;
{
   double dx, dy, dz, dk, dp, dw, s;
   double r[3][3];

   /*   dX = [ dx, dy, dz ]^T
        dk, dp, dw : rotation angle for z, y, x axis
        s : scale factor  */

   dx = 125.853;
   dy = -478.221;
   dz = -659.941;
   dk = (-2.195/3600.)*D2R;
   dp = (-2.333/3600.)*D2R;
   dw = (1.860/3600.)*D2R;
   s = 0.999998503;
   
   r[0][0] = cos(dp)*cos(dk);
   r[0][1] = cos(dw)*sin(dk)+sin(dw)*sin(dp)*cos(dk);
   r[0][2] = sin(dw)*sin(dk)-cos(dw)*sin(dp)*cos(dk);
   r[1][0] = -cos(dp)*sin(dk);
   r[1][1] = cos(dw)*cos(dk)-sin(dw)*sin(dp)*sin(dk);
   r[1][2] = sin(dw)*cos(dw)+cos(dw)*sin(dp)*sin(dk);
   r[2][0] = sin(dp);
   r[2][1] = -sin(dw)*cos(dp);
   r[2][2] = cos(dw)*cos(dp);

   *xb = s*(r[0][0]*xw+r[0][1]*yw+r[0][2]*zw) + dx;
   *yb = s*(r[1][0]*xw+r[1][1]*yw+r[1][2]*zw) + dy;  
   *zb = s*(r[2][0]*xw+r[2][1]*yw+r[2][2]*zw) + dz;
}



/******************************************************************
*   Function to convert                                           *
*      Bessel geocentric coord.(xb,yb,zb)                         *
*                  -> WGS84 geocentric coord.(xw,yw,zw)           *
*                                                                 *
*      This fucntion uses 7-parameter transformation method.      *
*          Xw = s*R*Xb + dX                                       *
*                   where, Xb : Bessel geocentric coord.          *
*                          Xw : WGS84 geocentric coord.           *
*                          R  : rotation matrix                   *
*                          dX : datum shifts(Xw-Xb)               *
*                          s  : scale                             *
*      The parameters are provided by Dr. Lee, Yongchang          *
*      (PHD Thesis of Chungnam Univ., 1992. 2.,pp104-105)         *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/ 
void bessel2wgs(double xb,double yb,double zb,double* xw,double* yw,double* zw)
//double xb,yb,zb;
//double *xw,*yw,*zw;
{
   double dx, dy, dz, dk, dp, dw, s;
   double r[3][3];

   /*   dX = [ dx, dy, dz ]^T
        dk, dp, dw : rotation angle for z, y, x axis
        s : scale factor  */

   dx = -125.853;
   dy = 478.221;
   dz = 659.941;
   dk = (2.195/3600.)*D2R;
   dp = (2.333/3600.)*D2R;
   dw = (-1.860/3600.)*D2R;
   s = 1.000001497;
   
   r[0][0] = cos(dp)*cos(dk);
   r[0][1] = cos(dw)*sin(dk)+sin(dw)*sin(dp)*cos(dk);
   r[0][2] = sin(dw)*sin(dk)-cos(dw)*sin(dp)*cos(dk);
   r[1][0] = -cos(dp)*sin(dk);
   r[1][1] = cos(dw)*cos(dk)-sin(dw)*sin(dp)*sin(dk);
   r[1][2] = sin(dw)*cos(dw)+cos(dw)*sin(dp)*sin(dk);
   r[2][0] = sin(dp);
   r[2][1] = -sin(dw)*cos(dp);
   r[2][2] = cos(dw)*cos(dp);

   *xw = s*(r[0][0]*xb+r[0][1]*yb+r[0][2]*zb) + dx;
   *yw = s*(r[1][0]*xb+r[1][1]*yb+r[1][2]*zb) + dy;  
   *zw = s*(r[2][0]*xb+r[2][1]*yb+r[2][2]*zb) + dz;
}



/******************************************************************
*   Function for the TM Projection in Korea                       *
*         (lambda,phi) -> (xnorth,yeast)                          *
*                                                                 *
*     The return value of this fuction is as follows,             *
*                 0 : out of the projection range in logitude     *
*                 1 : the projection zone uses western origin     *
*                 2 : the projection zone uses middle origin      *
*                 3 : the projection zone uses eastern origin     *
*                                                                 *
*     This function calls the fuction,                            *
*        void gauss_kruger(lambda, phi, x, y, centmeri, a, b)     *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/
int tmproj(double lambda, double phi, double* xnorth, double* yeast)
//double lambda, phi;
//double *xnorth, *yeast;
{
   double a, b;
   double olat, olong, olong1, olong2, olong3;
   double x, y, xo, yo;
   int zone;

   a = 6377397.2;
   b = 6356079.0;

   /* Set the origin of TM projection for Korea
         origin        longitude   latitude      x(m)     y(m)
      western origin   125 00 00   38 00 00    500,000  200,000
      middle origin    127 00 00   38 00 00    500,000  200,000
      eastern origin   129 00 00   38 00 00    500,000  200,000  */

   olat = 38. * D2R;
   olong1 = (125. /* + 10.405/3600. */ )*D2R;
   olong2 = (127. /* + 10.405/3600. */ )*D2R;
   olong3 = (129. /* + 10.405/3600. */ )*D2R;
 
  /* return -1 if the longitude is out of the range(124E - 130E)  */  

   if(lambda < olong1-1.*D2R || lambda >= olong3+1.*D2R)
      return(0);


   if(lambda < olong1+1.*D2R) 
   {
      olong = olong1; 
      zone = 1;
   }

   if(lambda >= olong1+1.*D2R && lambda < olong2+1.*D2R)
   {
      olong = olong2;
      zone = 2;
   }

   if(lambda >= olong2+1.*D2R)
   {
      olong = olong3;
      zone = 3;
   }

   /* compute xo coordinate for latitude origin(38 00 00)  */

   gauss_kruger(olong, olat, &yo, &xo, olong, a, b);

   /* compute projection coordinate */

   gauss_kruger(lambda, phi, &y, &x, olong, a, b);

   /*  reduce the projection coordinate to the distance from origin */

   *xnorth = (x - xo) /* + 500000 */ ;
   *yeast = y /* + 200000 */ ;

   return(zone);
}



/******************************************************************
*   Function for the UTM Projection.                              *
*         (lambda,phi) -> (north, east )                          *
*     In this fuction,  a : radius of major axis                  *
*                       b : radius of minor axis                  *
*                                                                 *
*     The return value of this fuction is the zone number         *
*     of the UTM Projection, except                               *
*        0 : out of the projection range in latitude(84N - 80S)   *
*                                                                 *
*     This function calls the fuction,                            *
*        void gauss_kruger(lambda, phi, x, y, centmeri, a, b)     *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/
int utmproj(double lambda, double phi, double* north, double* east, double a, double b)
//double lambda, phi, a, b;
//double *north, *east;
{
   double e, n, centmeri;
   int zl, zone;

   /* return -1 if the latitude is out of the range(84N - 80S)  */

   if(phi*R2D > 84. || phi*R2D <-80) return(0);

   /* determine the zone and central meridian */

   zl = (int)(lambda*R2D);
   if(zl>=180)
      zl= (int)(lambda*R2D)-360;

   if(zl >= 0)
      zone = (int)(zl/6) + 31;
   else 
      zone = (int)(zl/6) + 30;
   
   if(zone > 30) 
      centmeri = (double)(zone*6 - 180 - 3) * D2R;
   else
      centmeri = (double)(zone*6 + 180 - 3) * D2R;

   gauss_kruger(lambda, phi, &e, &n, centmeri, a, b);

   /* correction the coord. for the scale factor of central meridan
      and virtual coord. of origin */   

   *east = e*0.9996 + 500000.;
   *north = n*0.9996;
   if(phi<0.) *north = *north + 10000000.;

   return(zone);
};

bool utmproj(double lambda, double phi, double* north, double* east, double a, double b, int given_zone)
//double lambda, phi, a, b;
//double *north, *east;
{
   double e, n, centmeri;
   int zl;
   //int zone;

   /* return -1 if the latitude is out of the range(84N - 80S)  */

   if(phi*R2D > 84. || phi*R2D <-80) return false;

   /* determine the zone and central meridian */

   zl = (int)(lambda*R2D);
   if(zl>=180)
      zl= (int)(lambda*R2D)-360;

   if(given_zone > 30) 
      centmeri = (double)(given_zone*6 - 180 - 3) * D2R;
   else
      centmeri = (double)(given_zone*6 + 180 - 3) * D2R;

   gauss_kruger(lambda, phi, &e, &n, centmeri, a, b);

   /* correction the coord. for the scale factor of central meridan
      and virtual coord. of origin */   

   *east = e*0.9996 + 500000.;
   *north = n*0.9996;
   if(phi<0.) *north = *north + 10000000.;

   return true;;
}



/******************************************************************
*   Function for Gauss-Kruger Projection.                         *
*         (lambda,phi) -> (x,y)                                   *
*     Note, x axis parallels to equator and y axis to meridian.   *
*     (In Korea, the notations are vice versa.)                   *
*     In this function,  a : radius of major axis                 *
*                        b : radius of minor axis                 *
*                        centmeri : logitude of central meridian  *
*                                  (radian)                       *
*                        the unit of lambda and phi is radian.    *
*     Note, the scale factor of central meridian is 1.            *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/
void gauss_kruger(double lambda, double phi, double* x, double* y, double centmeri, double a, double b)
//double lambda, phi, centmeri, a, b;
//double *x, *y;
{
   double e, e2, n, ns, t, eta, bl, dl;
   double a0, a2, a4, a6, a8;
   double xterm1, xterm2, xterm3;
   double yterm1, yterm2, yterm3;

   /* e : first eccentricity
      e2 : second eccentricity
      n : radius of curvature in the prime vertical  */   

   e = sqrt(a*a - b*b) / a;
   e2 = sqrt(a*a - b*b) / b;
   n = a / sqrt(1. - e*e*sin(phi)*sin(phi));
   
   ns = (a-b)/(a+b);

   /* compute the distance(bl) of the ellipsiodal meridian
      from the equator to the latitude phi  */

   a0 = 1. + pow(ns,2.)/4. + pow(ns,4.)/64.;
   a2 = (3./2.)*(ns - pow(ns,3.)/8.);
   a4 = (15./16.)*(pow(ns,2.) - pow(ns,4.)/4.);
   a6 = (35./48.)*pow(ns,3.);
   a8 = (315./512.)*pow(ns,4.);

   bl = ((a+b)/2.)*(a0*phi - a2*sin(2.*phi) + a4*sin(4.*phi) 
       - a6*sin(6.*phi) + a8*sin(8.*phi));

   t = tan(phi);
   eta = e2 * cos(phi);
   dl = lambda - centmeri;

   /* compute x coord. */

   xterm1 = dl*cos(phi);
   xterm2 = pow(dl,3.)*pow(cos(phi),3.)*(1-t*t+eta*eta)/6.;
   xterm3 = pow(dl,5.)*pow(cos(phi),5.)
            *(5.-18.*t*t+pow(t,4.)+14.*eta*eta-58.*t*t*eta*eta)/120.;
   *x = n*(xterm1 + xterm2 + xterm3);

   /* compute y coord. */ 

   yterm1 = dl*dl*sin(phi)*cos(phi)/2.;
   yterm2 = pow(dl,4.)*sin(phi)*pow(cos(phi),3.)
            *(5.-t*t+9.*eta*eta+4.*pow(eta,4.))/24.;
   yterm3 = pow(dl,6.)*sin(phi)*pow(cos(phi),5.)
            *(61.-58.*t*t+pow(t,4.)+270.*eta*eta-330.*t*t*eta*eta)/720.;

   *y = bl + n*(yterm1 + yterm2 + yterm3); 

}



/******************************************************************
*   Function for the Inverse TM Projection in Korea               *
*          (xnorth,yeast) -> (lambda,phi)                         *
*                                                                 *
*     <oflag> value means the zone as like,                       *
*             1 : western zone(central meridian = 125 00 00)      *
*             2 : middle zone(central meridian = 127 00 00)       *
*             3 : eastern zone(central meridian = 129 00 00)      *
*                                                                 *
*     Return value == 0, when oflag is not the above values.      *
*     Otherwise, return value == oflag                            *
*                                                                 *
*     This function calls two functions,                          *
*       void gauss_kruger(lambda, phi, x, y, centmeri, a, b)      *
*       void inv_gauss_kruger(x, y, lambda, phi, centmeri, a, b)  *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/
int inv_tmproj(double xnorth, double yeast, double* lambda, double* phi, int oflag)
//double xnorth, yeast;
//double *lambda, *phi;
//int oflag;
{
   double a, b;
   double olat, olong;
   double xo, yo, l, p;
//   int zone;

   a = 6377397.2;
   b = 6356079.0;

   /* set the origin of TM projection by the value of <oflag>  */

   switch(oflag)
   {
      case 1 :
         olong = 125.*D2R; break; 
      case 2 :
         olong = 127.*D2R; break;
      case 3 :
         olong = 129.*D2R; break;
      default :
         return(0);
   }

   olat = 38.*D2R;
 
   /* compute xo coordinate of latitude origin(38 00 00)from equator  */
   gauss_kruger(olong, olat, &yo, &xo, olong, a, b);

   /* correct the xnorth for the distance from equator */
   xnorth = xnorth + xo;

   /* compute inverse projection coordinate */
   inv_gauss_kruger(yeast, xnorth, &l, &p, olong, a, b);

   *lambda = l;
   *phi = p;

   return(oflag);
}



/******************************************************************
*   Function for the Inverse UTM Projection.                      *
*          (north, east) -> (lambda,phi)                          *
*     In this fuction,  a : radius of major axis                  *
*                       b : radius of minor axis                  *
*                                                                 *
*     <zone> value means the UTM zone.                            *
*     <hemi> value,  1 : northern hemisphere                      *
*                    2 : southern hemisphere                      *
*     The return value of this fuction is the zone number         *
*     of the UTM Projection, except                               *
*        0 : <zone> value is wrong                                *
*                                                                 *
*     This function calls the fuction,                            *
*        void gauss_kruger(lambda, phi, x, y, centmeri, a, b)     *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/
int inv_utmproj( double north, double east, double* lambda, double* phi, double a, double b, int zone, int hemi)
//double north, east, a, b;
//double *lambda, *phi;
//int zone, hemi;
{
   double l, p, centmeri;

   /* set central meridian by the value of <zone>  */

   if(zone<=30)
      centmeri = (double)zone*6. + 180. - 3.;
   else
      centmeri = (double)zone*6. - 180. - 3.;

   centmeri = centmeri*D2R;

   /* correction the coord. for the scale of central meridian
      and virtual coord. of origin */

   if(hemi < 0) north = (north - 10000000.)/0.9996;
   else north = north / 0.9996;

   east = (east - 500000.)/0.9996;
   
   /* compute latitude and phi */
   inv_gauss_kruger(east, north, &l, &p, centmeri, a, b);

   *lambda = l;
   *phi = p;
   if(hemi < 0) *phi = -1.*p;

   return(zone);
}



/******************************************************************
*   Function for Inverse Gauss-Kruger Projection.                 *
*          (x,y) -> (lambda,phi)                                  *
*     Note, x axis parallels to equator and y axis to meridian.   *
*     (In Korea, the notations are vice versa.)                   *
*     In this function,  a : radius of major axis                 *
*                        b : radius of minor axis                 *
*                        centmeri : logitude of central meridian  *
*                                  (radian)                       *
*                        the unit of lambda and phi is radian.    *
*     Note, the scale factor of central meridian is 1.            *
*                                                                 *
*       Programmed by Jeong, Soo                                  *
*       Date : Feb. 1996                                          *
******************************************************************/
void inv_gauss_kruger(double x, double y, double* lambda, double *phi, double centmeri, double a, double b)
//double x, y, centmeri, a, b;
//double *lambda, *phi;
{
   double e, e2, n, ns, eta, t; 
   double pf, y_e;
//   double flag1, flag2;
   double a0, a2, a4, a6, a8;
   double pterm1, pterm2, pterm3, lterm1, lterm2, lterm3;

   /*  e : first eccentricity
       e2 : second eccentricity  */

   e = sqrt(a*a - b*b) / a;
   e2 = sqrt(a*a - b*b) / b;

   ns = (a-b)/(a+b);
   
   /* estimate the footpoint latitude(pf), which is a point of the
      central meridian obtained by drawing a parallel to the x-axis
      through the point P(x,y). */

   a0 = 1. + pow(ns,2.)/4. + pow(ns,4.)/64.;
   a2 = (3./2.)*(ns - pow(ns,3.)/8.);
   a4 = (15./16.)*(pow(ns,2.) - pow(ns,4.)/4.);
   a6 = (35./48.)*pow(ns,3.);
   a8 = (315./512.)*pow(ns,4.);

   /* set initial value of footpoint latitude */
   pf = (2.*y)/(a0*(a+b));
   
   /* solve first estimated value of y coord(y_e) */
   y_e = ((a+b)/2.)*a0*pf
        + ((a+b)/2.)*( -a2*sin(2.*pf)
                      +a4*sin(4.*pf)-a6*sin(6.*pf)+a8*sin(8.*pf) );

   /* iteratively solve the <pf> value while the value of <y_e> from 
      estimated <pf> equals y coord. */
   while(y_e < y)
   {
      pf = pf + (2.*(y-y_e))/(a0*(a+b));
      y_e = ((a+b)/2.)*a0*pf
           + ((a+b)/2.)*( -a2*sin(2.*pf)
                         +a4*sin(4.*pf)-a6*sin(6.*pf)+a8*sin(8.*pf) );
   }

   /*  n : radius of curvature in the prime vertical  */   
   n = a / sqrt(1. - e*e*sin(pf)*sin(pf));
   eta = e2 * cos(pf);
   t = tan(pf);

   /* compute phi coord. */

   pterm1 = t*(1.+eta*eta)*(x/n)*(x/n)/2.;
   pterm2 = t*(5.+3.*t*t+6.*eta*eta-6.*eta*eta*t*t
               -3.*pow(eta,4.)-9.*t*t*pow(eta,4.))*pow(x/n,4.)/24.;
   pterm3 = t*(61.+90.*t*t+45.*pow(t,4.)+107.*eta*eta
               -162.*t*t*eta*eta-45.*pow(t,4.)*eta*eta)*pow(x/n,6.)/720.;

   *phi = pf - pterm1 + pterm2 - pterm3;

   /* compute lambda coord. */

   lterm1 = x/n;
   lterm2 = pow(x/n,3.)*(1.+2.*t*t+eta*eta)/6.;
   lterm3 = pow(x/n,5.)*(5.+28.*t*t+24.*pow(t,4.)
                         +6.*eta*eta+8.*t*t*eta*eta)/120.;

   *lambda = (1./cos(pf))*(lterm1-lterm2+lterm3);
   *lambda = *lambda+centmeri;
}



/**************************************************************
*                                                             *
*   This function returns arc tangent value of a/b            *
*   i.e. theta = arctan(a/b)                                  *
*                                                             *
*   The range of theta is                                     *
*         0 < return value < 2 Pi                             *
*                                                             *
*       Programmed by Jeong, Soo                              *
*       Date : Feb. 1996                                      *
*                                                             *
**************************************************************/
double ARCTAN(double a, double b)
//double a,b;
{
   double theta, pi;

   pi = 4.*atan(1.);
   
   if(b==0.)
   {
      if(a==0.) theta = 0.;
      if(a>0.)  theta = pi / 2.;
      if(a<0.)  theta = pi * 3. / 2.;
   }

   if(b>0.)
   {
      if(a>=0.) theta = atan(a/b);
      if(a<0.)  theta = 2*pi + atan(a/b);
   }

   if(b<0.) theta = pi + atan(a/b);
   
   return theta;
}



/**************************************************************
*                                                             *
*   This function converts                                    *
*          Degree -> Deg.MinSec                               *
*      for example,  12.3456 -> 12.204416                     *
*                               (12deg 20min 44.16sec)        *
*                                                             *
*       Programmed by Jeong, Soo                              *
*       Date : Feb. 1996                                      *
*                                                             *
**************************************************************/
double deg2dms(double degree)
//double degree;
{
   int deg, min;
   double sec, dmin,dms;

   deg = (int)degree;
   dmin = (degree -(double)deg)*60.;
   min = (int)dmin;
   sec = (dmin - (double)min)*60.;

   dms = (double)deg + (double)min/100. + sec/10000.;
 
   return(dms);
}



/**************************************************************
*                                                             *
*   This function converts                                    *
*          Deg.MinSec -> Degree                               *
*      for example,        12.345678      ->    12.582439     *
*                   (12deg 34min 56.78sec)                    *
*                                                             *
*       Programmed by Jeong, Soo                              *
*       Date : Feb. 1996                                      *
*                                                             *
**************************************************************/
double dms2deg(double dms)
//double dms;
{
   int deg, min;
   double sec, dmin, degree;

   deg = (int)dms;
   dmin = (dms-(double)deg)*100.;
   min = (int)dmin;
   sec = (dmin-(double)min)*100.;

   degree = (double)deg + (double)min/60. + sec/3600.;

   return(degree);
}
