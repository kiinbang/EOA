#pragma once

namespace tobj
{
	static const char* _TObjLoad_ = "TObjLoad"; /// Use this one

	struct Meta
	{
		double tx = 0.0, ty = 0.0, tz = 0.0;
		double rx = 0.0, ry = 0.0, rz = 0.0;
		double sx = 1.0, sy = 1.0, sz = 1.0;
	};

	template <typename T>
	class VTX
	{
	public:
		VTX() {}
		VTX(const T val0, const T val1, const T val2) { this->x[0] = val0; this->x[1] = val1; this->x[2] = val2; }

		T& operator[](const size_t i) { return x[i]; }

		const T operator[](const size_t i) const { return x[i]; }

		VTX<T> operator + (const VTX<T>& inVal)
		{
			VTX<T> newVal;
			for (size_t i = 0; i < 3; ++i) newVal[i] = this->x[i] + inVal[i];
			return newVal;
		}

		VTX<T> operator - (const VTX<T>& inVal)
		{
			VTX<T> newVal;
			for (size_t i = 0; i < 3; ++i) newVal[i] = this->x[i] - inVal[i];
			return newVal;
		}

		VTX<T> operator * (const VTX<T>& inVal)
		{
			VTX<T> newVal;
			for (size_t i = 0; i < 3; ++i) newVal[i] = this->x[i] * inVal[i];
			return newVal;
		}

		VTX<T> operator * (const double inVal)
		{
			VTX<T> newVal;
			for (size_t i = 0; i < 3; ++i) newVal[i] = static_cast<T>(this->x[i] * inVal);
			return newVal;
		}

		/// cross product
		VTX<T> operator ^ (const VTX<T>& p2) const
		{
			VTX<T> newVal;
			newVal[0] = x[1] * p2[2] - x[2] * p2[1];
			newVal[1] = x[2] * p2[0] - x[0] * p2[2];
			newVal[2] = x[0] * p2[1] - x[1] * p2[0];

			return newVal;
		};

		/// inner product
		double operator & (const VTX<T>& p) const
		{
			return this->x[0] * p.x[0] + this->x[1] * p.x[1] + this->x[2] * p.x[2];
		}

	private:
		T x[3];
	};

	using Vertex = VTX<double>;
	using FaceVtxIdx = VTX<int>;
	using FaceNormIdx = VTX<int>;

	using Normal = Vertex;

	class Face
	{
	public:
		Face() { this->idx = 0; }

		Face(const Face& f) { copy(f); }

		Face& operator=(const Face& f) { copy(f);  return *this; }

		void copy(const Face& f)
		{
			for (size_t i = 0; i < 3; ++i)
				this->vertices[i] = f.vertices[i];

			for (size_t i = 0; i < 3; ++i)
				this->vtxNormals[i] = f.vtxNormals[i];

			this->idx = f.idx;
		}

		Vertex& operator[](const size_t i) { return vertices[i]; }

		const Vertex& operator[](const size_t i) const { return vertices[i]; }

		void setIdx(const size_t i) { idx = i; }

		size_t getIdx() const { return idx; }

		Normal& norm(const size_t i) { return vtxNormals[i]; }

		const Normal& norm(const size_t i) const { return vtxNormals[i]; }

	private:
		Vertex vertices[3];
		Normal vtxNormals[3];
		size_t idx;
	};
}
