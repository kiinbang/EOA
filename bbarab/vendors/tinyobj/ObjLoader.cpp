#pragma once
#include "ObjLoader.h"

#include <array>
#include <assert.h>
#include <iostream>

#include "tiny_obj_loader.h"

#include <boost/algorithm/string/replace.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace bpt = boost::property_tree;

namespace tobj /// Use this one
{
	const wchar_t* _tx = L"Softgraphy.GIX.SceneNode.Transform.tx";
	const wchar_t* _ty = L"Softgraphy.GIX.SceneNode.Transform.ty";
	const wchar_t* _tz = L"Softgraphy.GIX.SceneNode.Transform.tz";
	const wchar_t* _rx = L"Softgraphy.GIX.SceneNode.Transform.rx";
	const wchar_t* _ry = L"Softgraphy.GIX.SceneNode.Transform.ry";
	const wchar_t* _rz = L"Softgraphy.GIX.SceneNode.Transform.rz";
	const wchar_t* _sx = L"Softgraphy.GIX.SceneNode.Transform.sx";
	const wchar_t* _sy = L"Softgraphy.GIX.SceneNode.Transform.sy";
	const wchar_t* _sz = L"Softgraphy.GIX.SceneNode.Transform.sz";

	const double deg2rad = acos(-1.0) / 180.0;
	const double rad2deg = 180.0 / acos(-1.0);

	class TObjLoad : public ObjLoad /// Use this one
	{
	public:
		TObjLoad() {}

		/// Load an obj file
		bool load(const char* fileName, const Meta& meta) override
		{
			tinyobj::ObjReaderConfig readConfig; /// keep its default values

			tinyobj::ObjReader reader;
			if (!reader.ParseFromFile(fileName, readConfig))
				return false;

			const double omega = meta.rx;
			const double phi = meta.ry;
			const double kappa = meta.rz;

			//R-rotation-matrix
			double rot[3][3];

			const double cosomega = cos(omega);
			const double sinomega = sin(omega);
			const double cosphi = cos(phi);
			const double sinphi = sin(phi);
			const double coskappa = cos(kappa);
			const double sinkappa = sin(kappa);

			rot[0][0] = cosphi * coskappa;
			rot[0][1] = -cosphi * sinkappa;
			rot[0][2] = sinphi;

			rot[1][0] = sinomega * sinphi * coskappa + cosomega * sinkappa;
			rot[1][1] = -sinomega * sinphi * sinkappa + cosomega * coskappa;
			rot[1][2] = -sinomega * cosphi;

			rot[2][0] = -cosomega * sinphi * coskappa + sinomega * sinkappa;
			rot[2][1] = cosomega * sinphi * sinkappa + sinomega * coskappa;
			rot[2][2] = cosomega * cosphi;

			auto attribs = reader.GetAttrib();

			size_t nVtx = attribs.vertices.size() / 3;
			this->vertices.resize(nVtx);

			for (size_t i = 0; i < nVtx; ++i)
			{
				double x = attribs.vertices[i * 3 + 0];
				double y = attribs.vertices[i * 3 + 1];
				double z = attribs.vertices[i * 3 + 2];

				double vtx[3];

				/// rotate
				if (meta.rx != 0.0 || meta.ry != 0.0 || meta.rz != 0.0)
				{
					vtx[0] = (rot[0][0] * x + rot[0][1] * y + rot[0][2] * z);
					vtx[1] = (rot[1][0] * x + rot[1][1] * y + rot[1][2] * z);
					vtx[2] = (rot[2][0] * x + rot[2][1] * y + rot[2][2] * z);
				}
				else
				{
					vtx[0] = x;
					vtx[1] = y;
					vtx[2] = z;
				}

				/// scale
				vtx[0] = (vtx[0] * meta.sx);
				vtx[1] = (vtx[1] * meta.sy);
				vtx[2] = (vtx[2] * meta.sz);

				/// translation
				this->vertices[i][0] = (vtx[0] + meta.tx);
				this->vertices[i][1] = (vtx[1] + meta.ty);
				this->vertices[i][2] = (vtx[2] + meta.tz);
			}

			size_t nNorm = attribs.normals.size() / 3;
			this->vtxNormals.resize(nNorm);

			for (size_t i = 0; i < nNorm; ++i)
			{
				double nx = attribs.normals[i * 3 + 0];
				double ny = attribs.normals[i * 3 + 1];
				double nz = attribs.normals[i * 3 + 2];

				/// rotate
				if (meta.rx != 0.0 || meta.ry != 0.0 || meta.rz != 0.0)
				{
					this->vtxNormals[i][0] = (rot[0][0] * nx + rot[0][1] * ny + rot[0][2] * nz);
					this->vtxNormals[i][1] = (rot[1][0] * nx + rot[1][1] * ny + rot[1][2] * nz);
					this->vtxNormals[i][2] = (rot[2][0] * nx + rot[2][1] * ny + rot[2][2] * nz);
				}
				else
				{
					this->vtxNormals[i][0] = nx;
					this->vtxNormals[i][1] = ny;
					this->vtxNormals[i][2] = nz;
				}
			}

			/// Shapes
			auto& Shapes = reader.GetShapes();

			/// Faces
			faceVtxIdxs.clear();
			faceVtxNormalIdxs.clear();
			//faceVtxPtrs.clear();
			//faceVtxNormalPtrs.clear();

			for (auto& _shape : Shapes)
			{
				auto NumFaces = _shape.mesh.indices.size() / 3;

				for (size_t i = 0; i < NumFaces; ++i)
				{
					FaceVtxIdx faceVtxIdx;
					faceVtxIdx[0] = -1;
					faceVtxIdx[1] = -1;
					faceVtxIdx[2] = -1;

					FaceNormIdx faceVtxNormalIdx;
					faceVtxNormalIdx[0] = -1;
					faceVtxNormalIdx[1] = -1;
					faceVtxNormalIdx[2] = -1;

					auto& v0 = _shape.mesh.indices[i * 3 + 0];
					auto& v1 = _shape.mesh.indices[i * 3 + 1];
					auto& v2 = _shape.mesh.indices[i * 3 + 2];

					auto id0 = v0.vertex_index;
					auto id1 = v1.vertex_index;
					auto id2 = v2.vertex_index;

					if (id0 >= 0 || id1 >= 0 || id2 >= 0 || id0 < nVtx || id1 < nVtx || id2 < nVtx)
					{
						faceVtxIdx[0] = id0;
						faceVtxIdx[1] = id1;
						faceVtxIdx[2] = id2;
					}

					auto n0 = v0.normal_index;
					auto n1 = v1.normal_index;
					auto n2 = v2.normal_index;

					if (n0 >= 0 || n1 >= 0 || n2 >= 0 || n0 < nNorm || n1 < nNorm || n2 < nNorm)
					{
						faceVtxNormalIdx[0] = n0;
						faceVtxNormalIdx[1] = n1;
						faceVtxNormalIdx[2] = n2;
					}

					faceVtxIdxs.push_back(faceVtxIdx);
					faceVtxNormalIdxs.push_back(faceVtxNormalIdx);
				}
			}

			return true;
		}

		/// Compute a face normal vector
		Normal computeFaceNormal(const size_t faceIdx) const override
		{
			auto v0 = vertices[faceVtxIdxs[faceIdx][0]];
			auto v1 = vertices[faceVtxIdxs[faceIdx][1]];
			auto v2 = vertices[faceVtxIdxs[faceIdx][2]];

			Vertex v01;
			v01[0] = v1[0] - v0[0];
			v01[1] = v1[1] - v0[1];
			v01[2] = v1[2] - v0[2];

			Vertex v02;
			v02[0] = v2[0] - v0[0];
			v02[1] = v2[1] - v0[1];
			v02[2] = v2[2] - v0[2];

			/// cross product
			Normal cross;
			cross[0] = v01[1] * v02[2] - v01[2] * v02[1];
			cross[1] = v01[2] * v02[0] - v01[0] * v02[2];
			cross[2] = v01[0] * v02[1] - v01[1] * v02[0];

			auto len = sqrt(cross[0] * cross[0] + cross[1] * cross[1] + cross[2] * cross[2]);

			if (len > std::numeric_limits<float>::epsilon())
			{
				cross[0] /= len;
				cross[1] /= len;
				cross[2] /= len;
			}

			return cross;
		}

		/// Get a number of faces
		size_t numFaces() const override
		{
			return faceVtxIdxs.size();
		}
		
		/// Get a face
		Face getFace(const size_t faceIdx) const override
		{
			/// [Todo] Prepare all face data before asking
			Face face;

			face.setIdx(faceIdx);

			int v0 = faceVtxIdxs[faceIdx][0];
			int v1 = faceVtxIdxs[faceIdx][1];
			int v2 = faceVtxIdxs[faceIdx][2];

			if (v0 >= 0 || v1 >= 0 || v2 >= 0 || v0 < vertices.size() || v1 < vertices.size() || v2 < vertices.size())
			{
				face[0] = vertices[v0];
				face[1] = vertices[v1];
				face[2] = vertices[v2];
			}

			int n0 = faceVtxNormalIdxs[faceIdx][0];
			int n1 = faceVtxNormalIdxs[faceIdx][1];
			int n2 = faceVtxNormalIdxs[faceIdx][2];

			if (n0 >= 0 || n1 >= 0 || n2 >= 0 || n0 < vtxNormals.size() || n1 < vtxNormals.size() || n2 < vtxNormals.size())
			{
				face.norm(0) = vtxNormals[n0];
				face.norm(1) = vtxNormals[n1];
				face.norm(2) = vtxNormals[n2];
			}

			return face;
		}

		/// Get number of vertices
		size_t numVertices() const override
		{
			return this->vertices.size();
		}

		/// Get number of vertex normals
		size_t numNorms() const override
		{
			return this->vtxNormals.size();
		}

		/// Get vertex
		Vertex getVertex(const size_t vtxIdx) const override
		{
			return this->vertices[vtxIdx];
		}

		/// Get norm vectors
		Normal getNorm(const size_t normIdx) const override
		{
			return this->vtxNormals[normIdx];
		}

		/// Get face vertex indices
		FaceVtxIdx getVtxIdx(const size_t faceIdx) const override
		{
			FaceVtxIdx vid = this->faceVtxIdxs[faceIdx];
			return vid;
		}

		/// Get face vertex normal vector indices
		FaceNormIdx getNormIdx(const size_t faceIdx) const override
		{
			FaceNormIdx nid = this->faceVtxNormalIdxs[faceIdx];
			return nid;
		}

	private:
		std::vector<Vertex> vertices;
		std::vector<Normal> vtxNormals;
		std::vector<FaceVtxIdx> faceVtxIdxs;
		std::vector<FaceNormIdx> faceVtxNormalIdxs;
	};

	Meta readModelConfigWithBOM(const char* xmlPath)
	{
		Meta meta;
#ifdef _DEBUG
		//std::clog << "Model config file:\t" << xmlPath << std::endl;
#endif

		try
		{
			boost::property_tree::ptree pt;
			std::fstream file(xmlPath, std::ios::in);
			if (file.is_open())
			{
				///skip BOM
				wchar_t buffer[8];
				buffer[0] = 255;
				while (file.good() && buffer[0] > 127)
					file.read((char*)buffer, 1);

				std::fpos_t pos = file.tellg();
				if (pos > 0)
					file.seekg(pos - 1);

				///parse rest stream
				/// set locale
				//xmlPath.imbue(std::locale("kor"));
				setlocale(LC_ALL, "");
				file.imbue(std::locale(setlocale(LC_ALL, NULL)));

				/// Read a project config file (xml)
				bpt::ptree reader;
				bpt::read_xml(file, reader);

				BOOST_FOREACH(const bpt::ptree::value_type & v, reader.get_child("Softgraphy.GIX.SceneNode.Transform"))
				{
					BOOST_FOREACH(const bpt::ptree::value_type & var, v.second.get_child(""))
					{
						if (var.first == "tx") meta.tx = var.second.get_value<double>();
						else if (var.first == "ty") meta.ty = var.second.get_value<double>();
						else if (var.first == "tz") meta.tz = var.second.get_value<double>();

						else if (var.first == "rx") meta.rx = var.second.get_value<double>();
						else if (var.first == "ry") meta.ry = var.second.get_value<double>();
						else if (var.first == "rz") meta.rz = var.second.get_value<double>();

						else if (var.first == "sx") meta.sx = var.second.get_value<double>();
						else if (var.first == "sy") meta.sy = var.second.get_value<double>();
						else if (var.first == "sz") meta.sz = var.second.get_value<double>();
					}
				}

				file.close();
			}
			else
			{
				std::wcout << L"Error in opening a meta.gxxml" << std::endl;
			}
		}
		catch (...)
		{
			std::wcout << L"Error in reading a meta.gxxml" << std::endl;
		}

		/// Degrees to radians
		meta.rx *= deg2rad;
		meta.ry *= deg2rad;
		meta.rz *= deg2rad;

		return meta;
	}

	/// Compute a face normal vector
	Normal computeFaceNormal(const Vertex& v0, const Vertex& v1, const Vertex& v2)
	{
		Vertex v01;
		v01[0] = v1[0] - v0[0];
		v01[1] = v1[1] - v0[1];
		v01[2] = v1[2] - v0[2];

		Vertex v02;
		v02[0] = v2[0] - v0[0];
		v02[1] = v2[1] - v0[1];
		v02[2] = v2[2] - v0[2];

		/// cross product
		Normal cross;
		cross[0] = v01[1] * v02[2] - v01[2] * v02[1];
		cross[1] = v01[2] * v02[0] - v01[0] * v02[2];
		cross[2] = v01[0] * v02[1] - v01[1] * v02[0];

		auto len = sqrt(cross[0] * cross[0] + cross[1] * cross[1] + cross[2] * cross[2]);

		if (len > std::numeric_limits<float>::epsilon())
		{
			cross[0] /= len;
			cross[1] /= len;
			cross[2] /= len;
		}

		return cross;
	}

	std::shared_ptr<ObjLoad> createObjLoad(const char* coreClassName)
	{
		std::shared_ptr<ObjLoad> obj;
		std::string name(coreClassName);

		if (name == std::string(_TObjLoad_))
		{
			obj.reset(new TObjLoad);
		}
		else
			nullptr;

		return obj;
	}
}