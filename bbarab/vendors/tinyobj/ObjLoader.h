#pragma once

#include <memory>
#include <vector>

#include "ObjData.h"

namespace tobj
{
	class ObjLoad
	{
	public:
		/// Load an obj file
		virtual bool load(const char* fileName, const Meta& meta) = 0;
		/// Compute a face normal vector
		virtual Normal computeFaceNormal(const size_t faceIdx) const = 0;
		/// Get a number of faces
		virtual size_t numFaces() const = 0;
		/// Get a face
		virtual Face getFace(const size_t faceIdx) const = 0;
		/// Get number of vertices
		virtual size_t numVertices() const = 0;
		/// Get number of vertex normals
		virtual size_t numNorms() const = 0;
		/// Get face vertex indices
		virtual FaceVtxIdx getVtxIdx(const size_t faceIdx) const = 0;
		/// Get face vertex normal vector indices
		virtual FaceNormIdx getNormIdx(const size_t faceIdx) const = 0;
		/// Get vertex
		virtual Vertex getVertex(const size_t vtxIdx) const = 0;
		/// Get norm vectors
		virtual Normal getNorm(const size_t normIdx) const = 0;
	};

	/// Load an obj meta file
	Meta readModelConfigWithBOM(const char* xmlPath);

	/// Create an ObjLoad interface
	std::shared_ptr<ObjLoad> createObjLoad(const char* coreClassName);

	/// Compute a face normal vector
	Normal computeFaceNormal(const Vertex& v0, const Vertex& v1, const Vertex& v2);
}