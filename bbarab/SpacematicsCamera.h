/*
 * Copyright (c) 2000-2001, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//////////////////////////
//SpacematicsCamera.h
//made by BbaraB
//revision date 2001-03-09
//카메라 클레스를 단독 파일로 독립시킴
//////////////////////////
//revision date 2001-07-22
//lens distortions 추가
//////////////////////////
#ifndef __SPACEMATICS_CAMERA_CLASS_H__
#define __SPACEMATICS_CAMERA_CLASS_H__

#include "SMCoordinateClass.h"
#include "SPaceMaticsStr.h"
#include <fstream.h>

#include <afxtempl.h>

class Radial_Distortion
{
public:
	Radial_Distortion() {};
	Radial_Distortion(Radial_Distortion& copy) {radius = copy.radius; distortion = copy.distortion;}
	~Radial_Distortion() {};
	void operator = (Radial_Distortion& copy) {radius = copy.radius; distortion = copy.distortion;}
	double radius;
	double distortion;
};

typedef struct SMAC_Coefficient{	//Simultaneous Multi-camera Analytical Calibration
public:
	double k0, k1, k2, k3, k4;	//Radial Distortion
	double p1, p2, p3, p4;		//Decentering Distortion
} SMAC;

//class for camera
class CCamera
{
public:
	CCamera();
	CCamera(CCamera& copy);
	virtual ~CCamera();
	
private:
	int num_fiducial;			//number of fiducial mark

public:
	//property
	int ID;						//Camera ID
	CSMStr Name;				//Camera Name
	
	double f;					//focal length
	Point2D<double> *fiducial;	//ficucial mark
	Point2D<double> PPA;		//principal point autocollimation
	Point2D<double> PPBS;		//principal point best symmetry
	//Average
	double k1, k2, k3, k4;		//lens distortions polynomial function coefficients (positive values denote image displacement away from center(PPBS))
	//Quadrant
	double k1_1, k1_2, k1_3, k1_4;
	double k2_1, k2_2, k2_3, k2_4;
	double k3_1, k3_2, k3_3, k3_4;
	double k4_1, k4_2, k4_3, k4_4;

	CArray<Radial_Distortion,Radial_Distortion&> radial_distortion;

	SMAC smac;

	bool LensDistortionEnable;	//Lens Distortion Enable Check
	bool SMACEnable;			//SMAC Enable Check

public:

	//operator
	void operator = (CCamera& copy);
	
	//set number of fiducial mark
	inline void SetNumFM(int num)
	{
		if(fiducial!=NULL)
		{
			delete[] fiducial;
			fiducial = NULL;
		}

		num_fiducial = num;

		fiducial = new Point2D<double>[num_fiducial];
	}

	//Get number of fiducial mark
	inline int GetNumFM(void)
	{
		return num_fiducial;
	}

	//Read camera file(TXT file)
	bool ReadCameraFile(CString fname);
};

#endif