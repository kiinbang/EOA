// SpacematicsEpipolar.h: interface for the CEpipolar class.
// maed by BbaraB
// revision date 2001/03/09
//함수는 아래와 같은 순서로 진행되어야 한다.
//0. 내부표정과 상호표정이 선행되 있어야 한다.
//1. fn, fp 세팅
//2.SetEOParameter()
//3. LOriImage.Load()
//4. ROriImage.Load()
//5. Make_L_RTMatrix()
//6. Make_R_RTMatrix()
//7. MakeRBMatrix()
//8. Make_L_RNMatrix()
//9. Make_R_RNMatrix()
//10.NormalImageSize()
//11.LPixelDepth
//12.RPixelDepth
//13.Resampling
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EPIPOLAR_H__029A98C5_141B_11D5_8D02_0000F803A11E__INCLUDED_)
#define AFX_EPIPOLAR_H__029A98C5_141B_11D5_8D02_0000F803A11E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Matrix_.h"
#include "image.h"
#include "imagepixel.h"
#include "Collinearity.h"

class CEpipolar  
{
public:
	CEpipolar();
	virtual ~CEpipolar();
	
	//member variable
	//Medel
	CModel model;
	//focal length (normal image, photo)
	double fn, fp;
	//Exterior Orientation Parameter
	//Left Image
	double LO, LP, LK;
	double LX, LY, LZ;
	//Right Image
	double RO, RP, RK;
	double RX, RY, RZ;
	//Left Original Image
	CImage LOriImage;
	//Right Original Image
	CImage ROriImage;
	//Left Epipolar Image
	CImage LEpiImage;
	int LEpiWidth;
	int LEpiHeight;
	int LPixelDepth;
	//Right Epipolar Image
	CImage REpiImage;
	int REpiWidth;
	int REpiHeight;
	int RPixelDepth;
	//epipolar(normalized) image resolution and width(larger)
	double resolution, dmax;
	//epipolar(normalized) image max y photo coordinate, min x photo coordinate
	double max_y, min_y, min_Lx, max_Lx, min_Rx, max_Rx;
	//epipolar option(1: RO_independent, 2: RO_dependent, 3: AO, deafult: AO)
	int EPIPOLAR_OPTION;
	
public:
	//member function
	//Set EO Parameter
	BOOL SetEOParameter(double Lo, double Lp, double Lk,
		double Lx, double Ly, double Lz,
		double Ro, double Rp, double Rk,
		double Rx, double Ry, double Rz);
	BOOL SetEOParameter(int option);
	
	Matrix<double> Get_L_RTMatrix(void);//(Left)Get inverse rotation matrix
	Matrix<double> Get_L_RNMatrix(void);//(Left)Get normalize rotation matirx

	Matrix<double> Get_R_RTMatrix(void);//(Right)Get inverse rotation matrix
	Matrix<double> Get_R_RNMatrix(void);//(Right)Get normalize rotation matrix

	Matrix<double> GetRBMatrix(void);//base-line rotation matrix
	
	
	COORD2D<double> Get_L_NormalCoord_Collinearity(COORD2D<double> OCoord);//(Left)Get normalized photo coord From original photo coord by collinearity condition
	COORD2D<double> Get_L_NormalCoord_Projective(COORD2D<double> OCoord);//(Left)Get normalized photo coord From original photo coord by projective transform
	COORD2D<double> Get_L_OriginalCoord_NormalCoord(COORD2D<double> NCoord);//(Left)Get Original photo coord From normalized photo coord by projective transform
	COORD2D<double> Get_L_OriginalCoord_NormalCoord2(COORD2D<double> NCoord);//(Left)Get Original photo coord From normalized photo coord by collinearity condition

	COORD2D<double> Get_R_NormalCoord_Collinearity(COORD2D<double> OCoord);//(Right)Get normalized photo coord From original photo coord by collinearity condition
	COORD2D<double> Get_R_NormalCoord_Projective(COORD2D<double> OCoord);//(Right)Get normalized photo coord From original photo coord by projective transform
	COORD2D<double> Get_R_OriginalCoord_NormalCoord(COORD2D<double> NCoord);//(Right)Get Original photo coord From normalized photo coord by projective transform
	COORD2D<double> Get_R_OriginalCoord_NormalCoord2(COORD2D<double> NCoord);//(Right)Get Original photo coord From normalized photo coord by collinearity condition

	BOOL NormalImageSize();//determine normalized image size
	
	COORD2D<double> Get_L_NormalPhotoCoord_NormalImageCoord(COORD2D<double> NImage);//(Left)Normalized image coord from normalized photo coord
	COORD2D<double> Get_R_NormalPhotoCoord_NormalImageCoord(COORD2D<double> NImage);//(Right)Normalized image coord from normalized photo coord

	COORD2D<double> Get_L_NormalImageCoord_NormalPhotoCoord(COORD2D<double> NPhoto);//(Left)Normalized photo coord from normalized image coord
	COORD2D<double> Get_R_NormalImageCoord_NormalPhotoCoord(COORD2D<double> NPhoto);//(Right)Normalized photo coord from normalized image coord

	BOOL ResamplingL(CString Lepiname);//left epipolar image resampling
	BOOL ResamplingR(CString Repiname);//right epipolar image resampling
	

	//private:
	//Left
	Matrix<double> LRT;
	Matrix<double> RRT;
	//Right
	Matrix<double> LRN;
	Matrix<double> RRN;
	//base-line
	Matrix<double> RB;
	
	double BX, BY, BZ;
	double base_phi;
	double base_kappa;
	double base_omega;

	struct coefficient_c
	{
		//projective parameters
		double c11, c12, c13;
		double c21, c22, c23;
		double c31, c32;
		
		double c11_, c12_, c13_;
		double c21_, c22_, c23_;
		double c31_, c32_;
	};
	
	coefficient_c Lc;
	coefficient_c Rc;
	
	
	//private:
	//Make Normalized Rotation Matrix
	BOOL Make_L_RTMatirx();//original -> vertical
	BOOL Make_L_RNMatrix();//original -> normal

	BOOL Make_R_RTMatirx();//original -> vertical
	BOOL Make_R_RNMatrix();//original -> normal

	BOOL MakeRBMatrix();	//vertical -> normal

	BOOL Make_L_ProjParam();//Make projective 8 parameter(direct, inverse)
	BOOL Make_R_ProjParam();//Make projective 8 parameter(direct, inverse)
		
};

#endif // !defined(AFX_EPIPOLAR_H__029A98C5_141B_11D5_8D02_0000F803A11E__INCLUDED_)
