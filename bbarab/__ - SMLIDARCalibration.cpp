// LIDARBiasCalibration.cpp: implementation of the CLIDARBiasCalibration class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "SMLIDARCalibration.h"
#include "Coordtr.h"
#include "UtilityGrocery.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include <iomanip.h>

using namespace SMATICS_BBARAB;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMLIDARCalibration::CSMLIDARCalibration()
{
	bICPoint = false;
	bICPatch = false;
	bControlPoint = false;
	prj_ver_num = 1.0;
}

CSMLIDARCalibration::~CSMLIDARCalibration()
{
}

bool CSMLIDARCalibration::ReadRecord(fstream &infile, CSMList<int>& index, CLIDARCalibrationPoint &point, CLIDARCalibrationPoint &point_old, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid)
{
	RemoveCommentLine(infile, "#!",2);

	int num_field = index.GetNumItem();
	if(num_field < 1) return false;

	point.PlaneAttitude[0] = point.PlaneAttitude[1] = point.PlaneAttitude[2] = 0.0;

	for(int i=0; i<num_field; i++)
	{
		switch(index.GetAt(i))
		{
		case 0:
			infile>>point.GPS_X;
			break;
		case 1:
			infile>>point.GPS_Y;
			break;
		case 2:
			infile>>point.GPS_Z;
			break;
		case 3:
			infile>>point.time;
			break;
		case 4:
			infile>>point.INS_O;//pitch
			point.INS_O = Deg2Rad(point.INS_O);
			break;
		case 5:
			infile>>point.INS_P;//roll
			point.INS_P = Deg2Rad(point.INS_P);
			break;
		case 6:
			infile>>point.INS_K;//yaw
			point.INS_K = Deg2Rad(point.INS_K);
			break;
		case 7:
			infile>>point.alpha;
			point.alpha = Deg2Rad(point.alpha);
			break;
		case 8:
			infile>>point.beta;
			point.beta = Deg2Rad(point.beta);
			break;
		case 9:
			infile>>point.dist;
			break;
		case 10:
			infile>>point.X;
			break;
		case 11:
			infile>>point.Y;
			break;
		case 12:
			infile>>point.Z;
			break;
		case 13:
			infile>>point.intensity;
			break;
		case 14://Weight matrix
			{
				for(int i=0; i<3; i++)
					for(int j=0; j<3; j++)
						infile>>point.P(i, j);
			}
			break;
		case 15://Co-varance matrix
			{
				for(int i=0; i<3; i++)
					for(int j=0; j<3; j++)
						infile>>point.P(i, j);
					//cov2weg
					point.P = point.P.Inverse();
			}
			break;
		case 16://OMEGA_PLANE
			infile>>point.PlaneAttitude[0];
			break;
			case 17://PHI_PLANE
			infile>>point.PlaneAttitude[1];
			break;
			case 18://KAPPA_PLANE
			infile>>point.PlaneAttitude[2];
			break;
		case 19://N/A
			infile.eatwhite();
			char line[MAX_LINE_LENGTH];
			infile.getline(line, MAX_LINE_LENGTH);
			break;
		default:
			AfxMessageBox("Input file parsing error");
			return false;
		}
	}

	////////////////2007.09.25
	//Check for etpe
	//
	//weight matrix
	//
	CSMMatrix<double> Puvw(3, 3, 0.0);
	Puvw = point.P;
	
	_Rmat_ R(Deg2Rad(point.PlaneAttitude[0]), Deg2Rad(point.PlaneAttitude[1]), Deg2Rad(point.PlaneAttitude[2]));
	
	CSMMatrix<double> Pxyz;
	
	Pxyz = R.Rmatrix%Puvw%R.Rmatrix.Transpose();
	
	point.P = Pxyz;
	//
	//
	///////////////////////////////////////////////////////////////////////////////////

	infile.eatwhite();

	point_old = point;

	if(bMeridianConversion == true)
	{
		ApplyMeridianConversion(point, ellipsoid);
	}

	switch(EQtype)
	{
	case 0://Terrapoint
		point.INS_K = - point.INS_K;
		point.beta = Deg2Rad(90) - point.beta;
		break;
	case 1://Terrapoint(scan_angle plus 90deg)
		point.INS_K = - point.INS_K;
		point.beta = - point.beta;
		break;
	case 2://UofC
		point.beta = - point.beta;
		point.dist = - point.dist;
		break;
	case 3://etc
		//point.beta = - point.beta;
		//point.dist = - point.dist;
		break;
	default:
		AfxMessageBox("option fail");
		break;
	}

	return true;
}

void CSMLIDARCalibration::ApplyMeridianConversion(CLIDARCalibrationPoint& point, ReferenceEllipsoid ellipsoid)
{
	int num_p = PointList.GetNumItem();
	if(num_p > 0)
	{
		CLIDARCalibrationPoint previous_p = PointList.GetAt(PointList.GetNumItem() - 1);
		double azi = CalAzimuth(previous_p.GPS_X, previous_p.GPS_Y, point.GPS_X, point.GPS_Y);
	}

	double lambda, phi;
	::inv_utmproj(point.GPS_Y, point.GPS_X, &lambda, &phi, ellipsoid.SemiMajor, ellipsoid.SemiMinor, ellipsoid.zone, ellipsoid.hemi);

	if(lambda > PI) lambda -= 2*PI;
	else if(lambda < -PI) lambda += 2*PI;

	//double deglambda = Rad2Deg(lambda);
	//double degphi = Rad2Deg(phi);

	//double north, east;
	//utmproj(lambda, phi, &north, &east, ellipsoid.SemiMajor, ellipsoid.SemiMinor, ellipsoid.zone);
	
	double centralmeridian = CentralMeridian(ellipsoid.zone);

	if(centralmeridian > PI) centralmeridian -= 2*PI;
	else if(centralmeridian < -PI) centralmeridian += 2*PI;
	
	double MC = MeridianConversion(lambda, phi, centralmeridian);

	if(MC > PI) MC -= 2*PI;
	else if(MC < -PI) MC += 2*PI;

	point.INS_K = point.INS_K + MC;
}

bool CSMLIDARCalibration::ReadIndexFile(CString fileinfopath, CSMList<int>& index)
{
	int num_tag = 20;
	CString* TagList = new CString[num_tag];
	
	int count = -1;
	TagList[++count] = "NAVIGATION_X";//0
	TagList[++count] = "NAVIGATION_Y";//1
	TagList[++count] = "NAVIGATION_Z";//2
	TagList[++count] = "TIME";//3
	TagList[++count] = "NAVIGATION_PITCH";//4
	TagList[++count] = "NAVIGATION_ROLL";//5
	TagList[++count] = "NAVIGATION_YAW";//6
	TagList[++count] = "SCAN_ANGLE_ALPHA";//7
	TagList[++count] = "SCAN_ANGLE_BETA";//8
	TagList[++count] = "RANGE";//9
	TagList[++count] = "GROUND_X";//10
	TagList[++count] = "GROUND_Y";//11
	TagList[++count] = "GROUND_Z";//12
	TagList[++count] = "INTENSITY";//13
	TagList[++count] = "WEIGHT_MATRIX";//14
	TagList[++count] = "COV_MATRIX";//15
	TagList[++count] = "OMEGA_PLANE";//16
	TagList[++count] = "PHI_PLANE";//17
	TagList[++count] = "KAPPA_PLANE";//18
	TagList[++count] = "N/A";//19

	fstream InfoFile;
	InfoFile.open(fileinfopath, ios::in|ios::nocreate);
	if(!InfoFile) return false;

	char temp_st[512];

	while(!InfoFile.eof())
	{
		InfoFile>>temp_st;
		CString TagName = temp_st;
		
		bool bCheck = false;
		
		for(int i=0; i<num_tag; i++)
		{
			if(temp_st == TagList[i])
			{
				bCheck = true;
				index.AddTail(i);
				break;
			}
		}
		
		if(bCheck == false)
		{
			return false;
		}

		InfoFile.eatwhite();

	}
	
	InfoFile.close();

	return true;
}

bool CSMLIDARCalibration::ReadInputData(CString patchpath, CString fileinfopath, CSMList<CLIDARCalibrationPoint> &PointList, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid)
{
	Control_PointList.RemoveAll();

	fstream LidarFile;
	LidarFile.open(patchpath, ios::in);

	CSMList<int> index;	
	if(false == ReadIndexFile(fileinfopath, index)) 
	{
		AfxMessageBox("Error: ReadIndexFile");
		return false;
	}

	while(!LidarFile.eof())
	{
		RemoveCommentLine(LidarFile, '!');
		CLIDARCalibrationPoint point, point_old;
		if(false == ReadRecord(LidarFile, index, point, point_old, EQtype, bMeridianConversion, ellipsoid)) return false;

		Control_PointList.AddTail(point);

	}
	
	LidarFile.close();
	
	return true;
}

CSMMatrix<double> CSMLIDARCalibration::LIDAREQ_Point_Terrapoint(CLIDARCalibrationPoint point, CSMLIDARConfig config, bool bplus90)
{
	CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Ranging(3,1,0.);
	
	X0(0,0) = point.GPS_X; X0(1,0) = point.GPS_Y; X0(2,0) = point.GPS_Z;

	P(0,0) = config.Xb; P(1,0) = config.Yb; P(2,0) = config.Zb;

	double SA;
	if(bplus90 == true)
		SA = Deg2Rad(90) - (point.beta + config.Beta_b) - Deg2Rad(90);
	else
		SA = Deg2Rad(90) - (point.beta + config.Beta_b);
	
	CRotationcoeff2 Swing((point.alpha+config.Alpha_b), SA, 0.);
	
	Ranging(0,0) = 0; 
	Ranging(1,0) = 0; 
	Ranging(2,0) = -(config.RangeS*point.dist + config.Rangeb);

	CSMMatrix<double> LU = Swing.Rmatrix%Ranging;
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//
	
	_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, -point.INS_K);//rotation matrix (yaw)
	
	//
	//Attitude matrix
	//
	CSMMatrix<double> INS; INS = (Yawmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix);

	//
	//Rotational offset
	//
	CRotationcoeff2 ROffset;
	_Rmat_ Rmat	(0,		config.Pb,		0);//rotation matrix (roll)
	_Rmat_ Pmat	(config.Ob,	0,		0);//rotation matrix (pitch)
	_Rmat_ Ymat	(0,		0,		-config.Kb);//rotation matrix (yaw)
	ROffset.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;
	
	CSMMatrix<double> XG = X0 + INS%(P + ROffset.Rmatrix%Swing.Rmatrix%Ranging);

	return XG;
}

CSMMatrix<double> CSMLIDARCalibration::LIDAREQ_Point_Terrapoint_etc(CLIDARCalibrationPoint point, CSMLIDARConfig config, CSMMatrix<double> dumy)
{
	CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Ranging(3,1,0.);
	
	X0(0,0) = point.GPS_X; X0(1,0) = point.GPS_Y; X0(2,0) = point.GPS_Z;

	P(0,0) = config.Xb; P(1,0) = config.Yb; P(2,0) = config.Zb;
	
	CRotationcoeff2 Swing((point.alpha+config.Alpha_b), (point.beta + config.Beta_b), 0.);
	
	Ranging(0,0) = 0; 
	Ranging(1,0) = 0; 
	Ranging(2,0) = -(config.RangeS*point.dist + config.Rangeb);

	CSMMatrix<double> LU = Swing.Rmatrix%Ranging;
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//
	
	_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, point.INS_K);//rotation matrix (yaw)
	
	//
	//Attitude matrix
	//
	CSMMatrix<double> INS; INS = dumy%(Yawmat.Rmatrix)
													%	(Pitchmat.Rmatrix)
													%	(Rollmat.Rmatrix);

	//
	//Rotational offset
	//
	CRotationcoeff2 ROffset;
	_Rmat_ Rmat	(0,		config.Pb,		0);//rotation matrix (roll)
	_Rmat_ Pmat	(config.Ob,	0,		0);//rotation matrix (pitch)
	_Rmat_ Ymat	(0,		0,		config.Kb);//rotation matrix (yaw)

	ROffset.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;
	
	CSMMatrix<double> XG = X0 + INS%(P + ROffset.Rmatrix%Swing.Rmatrix%Ranging);

	return XG;
}

CSMMatrix<double> CSMLIDARCalibration::LIDAREQ_Point_BasicEQ(CLIDARCalibrationPoint point, CSMLIDARConfig config, CSMMatrix<double> dumy)
{
	CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Ranging(3,1,0.);
	
	X0(0,0) = point.GPS_X; X0(1,0) = point.GPS_Y; X0(2,0) = point.GPS_Z;

	P(0,0) = config.Xb; P(1,0) = config.Yb; P(2,0) = config.Zb;
	
	CRotationcoeff2 Swing((point.alpha+config.Alpha_b), (point.beta + config.Beta_b), 0.);
	
	Ranging(0,0) = 0; 
	Ranging(1,0) = 0; 
	Ranging(2,0) = -(config.RangeS*point.dist + config.Rangeb);

	CSMMatrix<double> LU = Swing.Rmatrix%Ranging;
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//
	
	_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, point.INS_K);//rotation matrix (yaw)
	
	//
	//Attitude matrix
	//
	CSMMatrix<double> INS; INS = dumy%(Yawmat.Rmatrix)
													%	(Rollmat.Rmatrix)
													%	(Pitchmat.Rmatrix);
	//
	//Rotational offset
	//
	CRotationcoeff2 ROffset;
	_Rmat_ Rmat	(0,		config.Pb,		0);//rotation matrix (roll)
	_Rmat_ Pmat	(config.Ob,	0,		0);//rotation matrix (pitch)
	_Rmat_ Ymat	(0,		0,		config.Kb);//rotation matrix (yaw)

	ROffset.Rmatrix = Ymat.Rmatrix%Rmat.Rmatrix%Pmat.Rmatrix;
	
	CSMMatrix<double> XG = X0 + INS%(P + ROffset.Rmatrix%Swing.Rmatrix%Ranging);

	return XG;
}

CSMMatrix<double> CSMLIDARCalibration::LIDAREQ_Point_Terrapoint_source_plus90(CLIDARCalibrationPoint point, CSMLIDARConfig config)
{
	//
	///Terrapoint source
	//
	
	double rawR = point.dist;
	//double x;//e
	//double y;//n
	//double z;//h
	double rawscan_angle = point.beta;
	
	double rawpitch = point.INS_O;//pitch
	double rawroll = point.INS_P;//roll
	double rawyaw = point.INS_K;//yaw
	
	//	dy = offset.z * RAD_FACTOR;
	//	dp = offset.y * RAD_FACTOR;
	//	dr = offset.x * RAD_FACTOR;
	
	double dy = Config.Kb;
	double dp = Config.Ob;
	double dr = Config.Pb;

	double lx = Config.Xb;
	double ly = Config.Yb;
	double lz = Config.Zb;
	
	// Compute Ground Point Vector (rx, ry and rz) in Laser Frame
	double alpha = rawscan_angle;
	alpha = PI/2.0 + rawscan_angle;
	double rx = -rawR*cos(alpha);
	double ry = 0;
	double rz = -rawR*sin(alpha);
	
	// Compute X Coordinate
	double coordx = (cos(rawyaw)*sin(rawroll) - sin(rawyaw)*sin(rawpitch)*cos(rawroll))*(ry*sin(dp) + rz*cos(dp)*cos(dr) - rx*cos(dp)*sin(dr)) + lx*(cos(rawyaw)*cos(rawroll) + sin(rawyaw)*sin(rawpitch)*sin(rawroll)) + lz*(cos(rawyaw)*sin(rawroll) - sin(rawyaw)*sin(rawpitch)*cos(rawroll)) + (cos(rawyaw)*cos(rawroll) + sin(rawyaw)*sin(rawpitch)*sin(rawroll))*(rx*(cos(dr)*cos(dy) + sin(dp)*sin(dr)*sin(dy)) + rz*(cos(dy)*sin(dr) - cos(dr)*sin(dp)*sin(dy)) + ry*cos(dp)*sin(dy)) + ly*sin(rawyaw)*cos(rawpitch) - sin(rawyaw)*cos(rawpitch)*(rx*(cos(dr)*sin(dy) - cos(dy)*sin(dp)*sin(dr)) + rz*(sin(dr)*sin(dy) + cos(dr)*cos(dy)*sin(dp)) - ry*cos(dp)*cos(dy));
	// Compute Y Coordinate
	double coordy = - (sin(rawyaw)*sin(rawroll) + cos(rawyaw)*sin(rawpitch)*cos(rawroll))*(ry*sin(dp) + rz*cos(dp)*cos(dr) - rx*cos(dp)*sin(dr)) - lx*(sin(rawyaw)*cos(rawroll) - cos(rawyaw)*sin(rawpitch)*sin(rawroll)) - lz*(sin(rawyaw)*sin(rawroll) + cos(rawyaw)*sin(rawpitch)*cos(rawroll)) - (sin(rawyaw)*cos(rawroll) - cos(rawyaw)*sin(rawpitch)*sin(rawroll))*(rx*(cos(dr)*cos(dy) + sin(dp)*sin(dr)*sin(dy)) + rz*(cos(dy)*sin(dr) - cos(dr)*sin(dp)*sin(dy)) + ry*cos(dp)*sin(dy)) + ly*cos(rawyaw)*cos(rawpitch) - cos(rawyaw)*cos(rawpitch)*(rx*(cos(dr)*sin(dy) - cos(dy)*sin(dp)*sin(dr)) + rz*(sin(dr)*sin(dy) + cos(dr)*cos(dy)*sin(dp)) - ry*cos(dp)*cos(dy));
	// Compute Z Coordinate
	double coordz = ly*sin(rawpitch) - sin(rawpitch)*(rx*(cos(dr)*sin(dy) - cos(dy)*sin(dp)*sin(dr)) + rz*(sin(dr)*sin(dy) + cos(dr)*cos(dy)*sin(dp)) - ry*cos(dp)*cos(dy)) - cos(rawpitch)*sin(rawroll)*(rx*(cos(dr)*cos(dy) + sin(dp)*sin(dr)*sin(dy)) + rz*(cos(dy)*sin(dr) - cos(dr)*sin(dp)*sin(dy)) + ry*cos(dp)*sin(dy)) + cos(rawpitch)*cos(rawroll)*(ry*sin(dp) + rz*cos(dp)*cos(dr) - rx*cos(dp)*sin(dr)) + lz*cos(rawpitch)*cos(rawroll) - lx*cos(rawpitch)*sin(rawroll);
	
	//plh.y = toProcess->lat * RAD_FACTOR; 
	//plh.x = toProcess->lon * RAD_FACTOR; 
	//plh.z = toProcess->h;
	
	double plhy = point.GPS_Y;
	double plhx = point.GPS_X;
	double plhz = point.GPS_Z;
	
	CSMMatrix<double> XG(3,1);
	XG(0,0) = coordx + plhx;
	XG(1,0) = coordy + plhy;
	XG(2,0) = coordz + plhz;
	
	return XG;
}

CSMMatrix<double> CSMLIDARCalibration::LIDAREQ_Point_Terrapoint_source(CLIDARCalibrationPoint point, CSMLIDARConfig config)
{
	//
	///Terrapoint source
	//
	
	double rawR = point.dist;
	//double x;//e
	//double y;//n
	//double z;//h
	double rawscan_angle = point.beta;
	
	double rawpitch = point.INS_O;//pitch
	double rawroll = point.INS_P;//roll
	double rawyaw = point.INS_K;//yaw
	
	//	dy = offset.z * RAD_FACTOR;
	//	dp = offset.y * RAD_FACTOR;
	//	dr = offset.x * RAD_FACTOR;
	
	double dy = Config.Kb;
	double dp = Config.Ob;
	double dr = Config.Pb;

	double lx = Config.Xb;
	double ly = Config.Yb;
	double lz = Config.Zb;
	
	// Compute Ground Point Vector (rx, ry and rz) in Laser Frame
	double alpha = rawscan_angle;
	//alpha = PI/2.0 + rawscan_angle;
	double rx = -rawR*cos(alpha);
	double ry = 0;
	double rz = -rawR*sin(alpha);
	
	// Compute X Coordinate
	double coordx = (cos(rawyaw)*sin(rawroll) - sin(rawyaw)*sin(rawpitch)*cos(rawroll))*(ry*sin(dp) + rz*cos(dp)*cos(dr) - rx*cos(dp)*sin(dr)) + lx*(cos(rawyaw)*cos(rawroll) + sin(rawyaw)*sin(rawpitch)*sin(rawroll)) + lz*(cos(rawyaw)*sin(rawroll) - sin(rawyaw)*sin(rawpitch)*cos(rawroll)) + (cos(rawyaw)*cos(rawroll) + sin(rawyaw)*sin(rawpitch)*sin(rawroll))*(rx*(cos(dr)*cos(dy) + sin(dp)*sin(dr)*sin(dy)) + rz*(cos(dy)*sin(dr) - cos(dr)*sin(dp)*sin(dy)) + ry*cos(dp)*sin(dy)) + ly*sin(rawyaw)*cos(rawpitch) - sin(rawyaw)*cos(rawpitch)*(rx*(cos(dr)*sin(dy) - cos(dy)*sin(dp)*sin(dr)) + rz*(sin(dr)*sin(dy) + cos(dr)*cos(dy)*sin(dp)) - ry*cos(dp)*cos(dy));
	// Compute Y Coordinate
	double coordy = - (sin(rawyaw)*sin(rawroll) + cos(rawyaw)*sin(rawpitch)*cos(rawroll))*(ry*sin(dp) + rz*cos(dp)*cos(dr) - rx*cos(dp)*sin(dr)) - lx*(sin(rawyaw)*cos(rawroll) - cos(rawyaw)*sin(rawpitch)*sin(rawroll)) - lz*(sin(rawyaw)*sin(rawroll) + cos(rawyaw)*sin(rawpitch)*cos(rawroll)) - (sin(rawyaw)*cos(rawroll) - cos(rawyaw)*sin(rawpitch)*sin(rawroll))*(rx*(cos(dr)*cos(dy) + sin(dp)*sin(dr)*sin(dy)) + rz*(cos(dy)*sin(dr) - cos(dr)*sin(dp)*sin(dy)) + ry*cos(dp)*sin(dy)) + ly*cos(rawyaw)*cos(rawpitch) - cos(rawyaw)*cos(rawpitch)*(rx*(cos(dr)*sin(dy) - cos(dy)*sin(dp)*sin(dr)) + rz*(sin(dr)*sin(dy) + cos(dr)*cos(dy)*sin(dp)) - ry*cos(dp)*cos(dy));
	// Compute Z Coordinate
	double coordz = ly*sin(rawpitch) - sin(rawpitch)*(rx*(cos(dr)*sin(dy) - cos(dy)*sin(dp)*sin(dr)) + rz*(sin(dr)*sin(dy) + cos(dr)*cos(dy)*sin(dp)) - ry*cos(dp)*cos(dy)) - cos(rawpitch)*sin(rawroll)*(rx*(cos(dr)*cos(dy) + sin(dp)*sin(dr)*sin(dy)) + rz*(cos(dy)*sin(dr) - cos(dr)*sin(dp)*sin(dy)) + ry*cos(dp)*sin(dy)) + cos(rawpitch)*cos(rawroll)*(ry*sin(dp) + rz*cos(dp)*cos(dr) - rx*cos(dp)*sin(dr)) + lz*cos(rawpitch)*cos(rawroll) - lx*cos(rawpitch)*sin(rawroll);
	
	//plh.y = toProcess->lat * RAD_FACTOR; 
	//plh.x = toProcess->lon * RAD_FACTOR; 
	//plh.z = toProcess->h;
	
	double plhy = point.GPS_Y;
	double plhx = point.GPS_X;
	double plhz = point.GPS_Z;
	
	CSMMatrix<double> XG(3,1);
	XG(0,0) = coordx + plhx;
	XG(1,0) = coordy + plhy;
	XG(2,0) = coordz + plhz;
	
	return XG;
}

CSMMatrix<double> CSMLIDARCalibration::LIDAREQ_Point_Terrapoint_UofC(CLIDARCalibrationPoint point, CSMLIDARConfig config)
{
	CSMMatrix<double> X0(3,1,0.), P(3,1,0.), Ranging(3,1,0.);
	
	X0(0,0) = point.GPS_X; X0(1,0) = point.GPS_Y; X0(2,0) = point.GPS_Z;
	P(0,0) = config.Xb; P(1,0) = config.Yb; P(2,0) = config.Zb;
	
	Ranging(0,0) = 0; Ranging(1,0) = 0; Ranging(2,0) = (config.RangeS*point.dist + config.Rangeb);
	
	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//
	
	_Rmat_ Rollmat	(0, point.INS_P,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(point.INS_O,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, point.INS_K);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(0,Deg2Rad(180),0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CSMMatrix<double> INS = INS2G.Rmatrix	%	(Yawmat.Rmatrix)
														%	(Pitchmat.Rmatrix)
														%	(Rollmat.Rmatrix);

	CRotationcoeff2 Swing((point.alpha+config.Alpha_b), -(point.beta+config.Beta_b), 0.);

	//
	//Rotational offset
	//
	CRotationcoeff2 ROffset;
	_Rmat_ Rmat	(0,		(config.Pb),		0);//rotation matrix (roll)
	_Rmat_ Pmat	((config.Ob),	0,		0);//rotation matrix (pitch)
	_Rmat_ Ymat	(0,		0,		(config.Kb));//rotation matrix (yaw)

	ROffset.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;
	
	CSMMatrix<double> XG = X0 + INS%(P + ROffset.Rmatrix%Swing.Rmatrix%Ranging);
	
	return XG;
}

void CSMLIDARCalibration::Cal_PDsofLIDAREQ_Terrapoint(CLIDARCalibrationPoint Point, CSMLIDARConfig Config, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_Point, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat, _Rmat_& Offset_R, CSMMatrix<double> &Pvector, _Rmat_& dOffsetR_dO, _Rmat_& dOffsetR_dP, _Rmat_& dOffsetR_dK)
{
	CSMMatrix<double> X0 = LIDAREQ_Point_Terrapoint(Point, Config, true);

	double XP, YP, ZP;

	double Beta_b = -Config.Beta_b;
	double Alpha_b = Config.Alpha_b;
	//beta = 90deg - scan_angle
	double beta = Deg2Rad(90) - Point.beta - Deg2Rad(90);
	double alpha = Point.alpha;
		
	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	double Rangeb = -Config.Rangeb;
	//ranging distance + distance bias
	double dist = -Point.dist*Config.RangeS + Rangeb;

	CSMMatrix<double> Rangevector(3,1);
	Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= dist;

	//INS angle
	double INS_Pitch=Point.INS_O;
	double INS_Roll=Point.INS_P;
	double INS_Yaw=-Point.INS_K;

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//

	_Rmat_ Rollmat	(0, INS_Roll,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(INS_Pitch,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, INS_Yaw);//rotation matrix (-yaw)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS_R; INS_R.Rmatrix = (Yawmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix);

	//partial derivatives for INS angle
	CRotationcoeff2 dR_dPitch;
	dR_dPitch.Partial_dRdO(INS_Pitch, 0, 0);
	dR_dPitch.Rmatrix = Yawmat.Rmatrix % dR_dPitch.Rmatrix % Rollmat.Rmatrix;

	CRotationcoeff2 dR_dRoll;
	dR_dRoll.Partial_dRdP(0, INS_Roll, 0);
	dR_dRoll.Rmatrix = Yawmat.Rmatrix % Pitchmat.Rmatrix % dR_dRoll.Rmatrix;

	CRotationcoeff2 dR_dYaw;
	dR_dYaw.Partial_dRdK(0, 0, INS_Yaw);
	dR_dYaw.Rmatrix = dR_dYaw.Rmatrix % Pitchmat.Rmatrix % Rollmat.Rmatrix;
	dR_dYaw.Rmatrix = dR_dYaw.Rmatrix;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(beta + Beta_b);
	SwingR(1,0) = -sin(alpha + Alpha_b)*cos(beta + Beta_b);
	SwingR(2,0) = cos(alpha + Alpha_b)*cos(beta + Beta_b);

	//partial derivatives for scan angle
	CRotationcoeff2 AlphaR(alpha,0,0);
	CRotationcoeff2 AlphaOffsetR(Alpha_b,0,0);
	CRotationcoeff2 BetaR(0,beta,0);
	CRotationcoeff2 BetaOffsetR(0, Beta_b,0);
	CRotationcoeff2 dAlphaR_dAlpha; dAlphaR_dAlpha.Partial_dRdO(alpha, 0, 0);
	CRotationcoeff2 dBetaR_dBeta; dBetaR_dBeta.Partial_dRdP(0, beta, 0);
	CRotationcoeff2 dAlphaBiasR_dAlphaOffset; dAlphaBiasR_dAlphaOffset.Partial_dRdO(Alpha_b,0, 0);
	CRotationcoeff2 dBetaBiasR_dBetaOffset; dBetaBiasR_dBetaOffset.Partial_dRdP(0, Beta_b,0);

	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
	CSMMatrix<double> R2 = BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R3 = R1%AlphaOffsetR.Rmatrix%BetaR.Rmatrix;
	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
	CSMMatrix<double> R5 = AlphaOffsetR.Rmatrix%BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R6 = R4%AlphaR.Rmatrix%AlphaOffsetR.Rmatrix;

	CSMMatrix<double> df1_dAlpha_offset = R1%dAlphaBiasR_dAlphaOffset.Rmatrix%R2%Rangevector;
	CSMMatrix<double> df2_dBeta_offset = R3%dBetaBiasR_dBetaOffset.Rmatrix%Rangevector;
	CSMMatrix<double> df3_dAlpha = R4%dAlphaR_dAlpha.Rmatrix%R5%Rangevector;
	CSMMatrix<double> df4_dBeta = R6%dBetaR_dBeta.Rmatrix%BetaOffsetR.Rmatrix%Rangevector;
	
	CSMMatrix<double> Lmat = R4%SwingR;

	CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%SwingR)*(dist));
	CSMMatrix<double> dFdPitch	= dR_dPitch.Rmatrix	%temp;
	CSMMatrix<double> dFdRoll	= dR_dRoll.Rmatrix%temp;
	CSMMatrix<double> dFdYaw	= dR_dYaw.Rmatrix%temp;

	/////////////////////////////
	//B Matrix
	/////////////////////////////
	//GNSS obs
	bmat(0,0) = 1.0;
	bmat(0,1) = 0.0;
	bmat(0,2) = 0.0;

	bmat(1,0) = 0.0;
	bmat(1,1) = 1.0;
	bmat(1,2) = 0.0;

	bmat(2,0) = 0.0;
	bmat(2,1) = 0.0;
	bmat(2,2) = 1.0;

	//INS obs
	bmat(0,3) = dFdPitch(0,0);
	bmat(1,3) = dFdPitch(1,0);
	bmat(2,3) = dFdPitch(2,0);

	bmat(0,4) = dFdRoll(0,0);
	bmat(1,4) = dFdRoll(1,0);
	bmat(2,4) = dFdRoll(2,0);

	bmat(0,5) = dFdYaw(0,0);
	bmat(1,5) = dFdYaw(1,0);
	bmat(2,5) = dFdYaw(2,0);

	//swing angle
	bmat(0,6) = df3_dAlpha(0,0);
	bmat(1,6) = df3_dAlpha(1,0);
	bmat(2,6) = df3_dAlpha(2,0);

	bmat(0,7) = df4_dBeta(0,0);
	bmat(1,7) = df4_dBeta(1,0);
	bmat(2,7) = df4_dBeta(2,0);
	//ranging distance
	bmat(0,8) = (Lmat*Config.RangeS)(0,0);
	bmat(1,8) = (Lmat*Config.RangeS)(1,0);
	bmat(2,8) = (Lmat*Config.RangeS)(2,0);

	/////////////////////////////
	//J Matrix
	/////////////////////////////
	//bore-sight factors (bias_X, bias_Y, bias_Z)
	//Partial derivatives
	jmat_lidar.Insert(0,0,INS_R.Rmatrix);
	
	//rotational alignment (bias_O, bias_P, bias_K)
	temp = ((INS_R.Rmatrix%dOffsetR_dO.Rmatrix%SwingR)*dist);
	jmat_lidar.Insert(0,3,temp);

	temp = ((INS_R.Rmatrix%dOffsetR_dP.Rmatrix%SwingR)*dist);
	jmat_lidar.Insert(0,4,temp);

	temp = ((INS_R.Rmatrix%dOffsetR_dK.Rmatrix%SwingR)*dist);
	jmat_lidar.Insert(0,5,temp);

	//scan angle offset
	jmat_lidar.Insert(0,6,(df1_dAlpha_offset));

	jmat_lidar.Insert(0,7,(df2_dBeta_offset));

	//laser ranging offset
	jmat_lidar.Insert(0,8,Lmat);

	//laser ranging scale
	jmat_lidar.Insert(0,9, Lmat*dist);

	//points
	jmat_Point(0,0) = -1.0;
	jmat_Point(1,1) = -1.0;
	jmat_Point(2,2) = -1.0;

	kmat(0,0) = Point.X - X0(0, 0);
	kmat(1,0) = Point.Y - X0(1, 0);
	kmat(2,0) = Point.Z - X0(2, 0);
}

void CSMLIDARCalibration::Cal_PDsofLIDAREQ_UofC(CLIDARCalibrationPoint Point, CSMLIDARConfig config, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_Point, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat, _Rmat_& Offset_R, CSMMatrix<double> &Pvector, _Rmat_& dOffsetR_dO, _Rmat_& dOffsetR_dP, _Rmat_& dOffsetR_dK)
{
	double XP, YP, ZP;
	
	CSMMatrix<double> X0 = LIDAREQ_Point_Terrapoint_UofC(Point, Config);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	//ranging distance + distance bias
	double dist = Point.dist*config.RangeS + config.Rangeb;

	CSMMatrix<double> Rangevector(3,1);
	Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= dist;

	//INS angle
	double INS_Pitch = Point.INS_O;
	double INS_Roll = Point.INS_P;
	double INS_Yaw = Point.INS_K;

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//

	double alpha = Point.alpha;
	double beta = Point.beta * -1.0;
		
	_Rmat_ Rollmat	(0, INS_Roll,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(INS_Pitch,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, INS_Yaw);//rotation matrix (yaw)
	
	//
	//INS2Ground
	//
	_Rmat_ INS2G(0, Deg2Rad(180), 0);//rotation matrix (INS2Ground)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS_R; INS_R.Rmatrix = INS2G.Rmatrix	%	(Yawmat.Rmatrix)
															%	(Pitchmat.Rmatrix)
															%	(Rollmat.Rmatrix);

	//partial derivatives for INS angle
	CRotationcoeff2 dR_dPitch;
	dR_dPitch.Partial_dRdO(INS_Pitch, 0, 0);
	dR_dPitch.Rmatrix = Yawmat.Rmatrix % dR_dPitch.Rmatrix % Rollmat.Rmatrix;

	CRotationcoeff2 dR_dRoll;
	dR_dRoll.Partial_dRdP(0, INS_Roll, 0);
	dR_dRoll.Rmatrix = Yawmat.Rmatrix % Pitchmat.Rmatrix % dR_dRoll.Rmatrix;

	CRotationcoeff2 dR_dYaw;
	dR_dYaw.Partial_dRdK(0, 0, INS_Yaw);
	dR_dYaw.Rmatrix = dR_dYaw.Rmatrix % Pitchmat.Rmatrix % Rollmat.Rmatrix;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(beta + config.Beta_b);
	SwingR(1,0) = -sin(alpha + config.Alpha_b)*cos(beta + config.Beta_b);
	SwingR(2,0) = cos(alpha + config.Alpha_b)*cos(beta + config.Beta_b);

	//partial derivatives for scan angle
	CRotationcoeff2 AlphaR(alpha, 0, 0);
	CRotationcoeff2 AlphaOffsetR(config.Alpha_b, 0, 0);
	CRotationcoeff2 BetaR(0, beta,0);
	CRotationcoeff2 BetaOffsetR(0,config.Beta_b,0);
	CRotationcoeff2 dAlphaR_dAlpha; dAlphaR_dAlpha.Partial_dRdO(alpha, 0, 0);
	CRotationcoeff2 dBetaR_dBeta; dBetaR_dBeta.Partial_dRdP(0, beta, 0);
	CRotationcoeff2 dAlphaBiasR_dAlphaOffset; dAlphaBiasR_dAlphaOffset.Partial_dRdO(config.Alpha_b,0, 0);
	CRotationcoeff2 dBetaBiasR_dBetaOffset; dBetaBiasR_dBetaOffset.Partial_dRdP(0,config.Beta_b,0);

	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
	CSMMatrix<double> R2 = BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R3 = R1%AlphaOffsetR.Rmatrix%BetaR.Rmatrix;
	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
	CSMMatrix<double> R5 = AlphaOffsetR.Rmatrix%BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R6 = R4%AlphaR.Rmatrix%AlphaOffsetR.Rmatrix;

	CSMMatrix<double> df1_dAlpha_offset = R1%dAlphaBiasR_dAlphaOffset.Rmatrix%R2%Rangevector;
	CSMMatrix<double> df2_dBeta_offset = R3%dBetaBiasR_dBetaOffset.Rmatrix%Rangevector;
	CSMMatrix<double> df3_dAlpha = R4%dAlphaR_dAlpha.Rmatrix%R5%Rangevector;
	CSMMatrix<double> df4_dBeta = R6%dBetaR_dBeta.Rmatrix%BetaOffsetR.Rmatrix%Rangevector;
	
	CSMMatrix<double> Lmat = R4%SwingR;

	CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%SwingR)*(dist));
	CSMMatrix<double> dFdPitch	= dR_dPitch.Rmatrix	%temp;
	CSMMatrix<double> dFdRoll	= dR_dRoll.Rmatrix%temp;
	CSMMatrix<double> dFdYaw	= dR_dYaw.Rmatrix%temp;

	/////////////////////////////
	//B Matrix
	/////////////////////////////
	//GNSS obs
	bmat(0,0) = 1.0;
	bmat(0,1) = 0.0;
	bmat(0,2) = 0.0;

	bmat(1,0) = 0.0;
	bmat(1,1) = 1.0;
	bmat(1,2) = 0.0;

	bmat(2,0) = 0.0;
	bmat(2,1) = 0.0;
	bmat(2,2) = 1.0;

	//INS obs
	bmat(0,3) = dFdPitch(0,0);
	bmat(1,3) = dFdPitch(1,0);
	bmat(2,3) = dFdPitch(2,0);

	bmat(0,4) = dFdRoll(0,0);
	bmat(1,4) = dFdRoll(1,0);
	bmat(2,4) = dFdRoll(2,0);

	bmat(0,5) = dFdYaw(0,0);
	bmat(1,5) = dFdYaw(1,0);
	bmat(2,5) = dFdYaw(2,0);

	//swing angle
	bmat(0,6) = df3_dAlpha(0,0);
	bmat(1,6) = df3_dAlpha(1,0);
	bmat(2,6) = df3_dAlpha(2,0);

	bmat(0,7) = df4_dBeta(0,0);
	bmat(1,7) = df4_dBeta(1,0);
	bmat(2,7) = df4_dBeta(2,0);
	//ranging distance
	bmat(0,8) = (Lmat*config.RangeS)(0,0);
	bmat(1,8) = (Lmat*config.RangeS)(1,0);
	bmat(2,8) = (Lmat*config.RangeS)(2,0);

	/////////////////////////////
	//J Matrix
	/////////////////////////////
	//bore-sight factors (bias_X, bias_Y, bias_Z)
	//Partial derivatives
	jmat_lidar.Insert(0,0,INS_R.Rmatrix);
	
	//rotational alignment (bias_O, bias_P, bias_K)
	temp = ((INS_R.Rmatrix%dOffsetR_dO.Rmatrix%SwingR)*dist);
	jmat_lidar.Insert(0,3,temp);

	temp = ((INS_R.Rmatrix%dOffsetR_dP.Rmatrix%SwingR)*dist);
	jmat_lidar.Insert(0,4,temp);

	temp = ((INS_R.Rmatrix%dOffsetR_dK.Rmatrix%SwingR)*dist);
	jmat_lidar.Insert(0,5,temp);

	//scan angle offset
	jmat_lidar.Insert(0,6,(df1_dAlpha_offset));

	jmat_lidar.Insert(0,7,(df2_dBeta_offset));

	//laser ranging offset
	jmat_lidar.Insert(0,8,Lmat);

	//laser ranging scale
	jmat_lidar.Insert(0,9, Lmat*dist);

	//points
	jmat_Point(0,0) = -1.0;
	jmat_Point(1,1) = -1.0;
	jmat_Point(2,2) = -1.0;

	kmat(0,0) = Point.X - X0(0, 0);
	kmat(1,0) = Point.Y - X0(1, 0);
	kmat(2,0) = Point.Z - X0(2, 0);
}

void CSMLIDARCalibration::Cal_PDsofLIDAREQ_Basic(CLIDARCalibrationPoint Point, CSMLIDARConfig config, CSMMatrix<double> &jmat_lidar, CSMMatrix<double> &jmat_Point, CSMMatrix<double> &bmat, CSMMatrix<double> &kmat, _Rmat_& Offset_R, CSMMatrix<double> &Pvector, _Rmat_& dOffsetR_dO, _Rmat_& dOffsetR_dP, _Rmat_& dOffsetR_dK, CSMMatrix<double> dumy)
{
	double XP, YP, ZP;
	
	CSMMatrix<double> X0 = LIDAREQ_Point_BasicEQ(Point, Config, dumy);

	XP = X0(0,0);
	YP = X0(1,0);
	ZP = X0(2,0);

	//ranging distance + distance bias
	double dist = Point.dist*config.RangeS + config.Rangeb;

	CSMMatrix<double> Rangevector(3,1);
	Rangevector(0,0) = 0;	Rangevector(1,0)=0;	Rangevector(2,0)= -dist;

	//INS angle
	double INS_Pitch = Point.INS_O;
	double INS_Roll = Point.INS_P;
	double INS_Yaw = Point.INS_K;

	////////////////////////////////////////////////////////////////////////////////////////
	//
	//INS configuration
	//

	double alpha = Point.alpha;
	double beta = Point.beta;
		
	_Rmat_ Rollmat	(0, INS_Roll,	0);//rotation matrix (roll)
	_Rmat_ Pitchmat	(INS_Pitch,	0, 0);//rotation matrix (pitch)
	_Rmat_ Yawmat	(0, 0, INS_Yaw);//rotation matrix (yaw)
	
	//
	//Attitude matrix
	//
	CRotationcoeff2 INS_R; 
	INS_R.Rmatrix =			dumy%(Yawmat.Rmatrix)%(Rollmat.Rmatrix)%(Pitchmat.Rmatrix);

	//partial derivatives for INS angle
	CRotationcoeff2 dR_dPitch;
	dR_dPitch.Partial_dRdO(INS_Pitch, 0, 0);
	dR_dPitch.Rmatrix = Yawmat.Rmatrix % Rollmat.Rmatrix % dR_dPitch.Rmatrix;

	CRotationcoeff2 dR_dRoll;
	dR_dRoll.Partial_dRdP(0, INS_Roll, 0);
	dR_dRoll.Rmatrix = Yawmat.Rmatrix % dR_dRoll.Rmatrix % Pitchmat.Rmatrix;

	CRotationcoeff2 dR_dYaw;
	dR_dYaw.Partial_dRdK(0, 0, INS_Yaw);
	dR_dYaw.Rmatrix = dR_dYaw.Rmatrix % Rollmat.Rmatrix % Pitchmat.Rmatrix;

	/////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

	//matrix(3X1) derived from swing angle
	CSMMatrix<double> SwingR(3,1,0.);
	SwingR(0,0) = sin(beta + config.Beta_b);
	SwingR(1,0) = -sin(alpha + config.Alpha_b)*cos(beta + config.Beta_b);
	SwingR(2,0) = cos(alpha + config.Alpha_b)*cos(beta + config.Beta_b);

	//partial derivatives for scan angle
	CRotationcoeff2 AlphaR(alpha, 0, 0);
	CRotationcoeff2 AlphaOffsetR(config.Alpha_b, 0, 0);
	CRotationcoeff2 BetaR(0, beta,0);
	CRotationcoeff2 BetaOffsetR(0,config.Beta_b,0);
	CRotationcoeff2 dAlphaR_dAlpha; dAlphaR_dAlpha.Partial_dRdO(alpha, 0, 0);
	CRotationcoeff2 dBetaR_dBeta; dBetaR_dBeta.Partial_dRdP(0, beta, 0);
	CRotationcoeff2 dAlphaBiasR_dAlphaOffset; dAlphaBiasR_dAlphaOffset.Partial_dRdO(config.Alpha_b,0, 0);
	CRotationcoeff2 dBetaBiasR_dBetaOffset; dBetaBiasR_dBetaOffset.Partial_dRdP(0,config.Beta_b,0);

	CSMMatrix<double> R1 = INS_R.Rmatrix%Offset_R.Rmatrix%AlphaR.Rmatrix;
	CSMMatrix<double> R2 = BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R3 = R1%AlphaOffsetR.Rmatrix%BetaR.Rmatrix;
	CSMMatrix<double> R4 = INS_R.Rmatrix%Offset_R.Rmatrix;
	CSMMatrix<double> R5 = AlphaOffsetR.Rmatrix%BetaR.Rmatrix%BetaOffsetR.Rmatrix;
	CSMMatrix<double> R6 = R4%AlphaR.Rmatrix%AlphaOffsetR.Rmatrix;

	CSMMatrix<double> df1_dAlpha_offset = R1%dAlphaBiasR_dAlphaOffset.Rmatrix%R2%Rangevector;
	CSMMatrix<double> df2_dBeta_offset = R3%dBetaBiasR_dBetaOffset.Rmatrix%Rangevector;
	CSMMatrix<double> df3_dAlpha = R4%dAlphaR_dAlpha.Rmatrix%R5%Rangevector;
	CSMMatrix<double> df4_dBeta = R6%dBetaR_dBeta.Rmatrix%BetaOffsetR.Rmatrix%Rangevector;
	
	CSMMatrix<double> Lmat = R4%SwingR;

	CSMMatrix<double> temp = (Pvector + (Offset_R.Rmatrix%SwingR)*(-dist));
	CSMMatrix<double> dFdPitch	= dR_dPitch.Rmatrix	%temp;
	CSMMatrix<double> dFdRoll	= dR_dRoll.Rmatrix%temp;
	CSMMatrix<double> dFdYaw	= dR_dYaw.Rmatrix%temp;

	/////////////////////////////
	//B Matrix
	/////////////////////////////
	//GNSS obs
	bmat(0,0) = 1.0;
	bmat(0,1) = 0.0;
	bmat(0,2) = 0.0;

	bmat(1,0) = 0.0;
	bmat(1,1) = 1.0;
	bmat(1,2) = 0.0;

	bmat(2,0) = 0.0;
	bmat(2,1) = 0.0;
	bmat(2,2) = 1.0;

	//INS obs
	bmat(0,3) = dFdPitch(0,0);
	bmat(1,3) = dFdPitch(1,0);
	bmat(2,3) = dFdPitch(2,0);

	bmat(0,4) = dFdRoll(0,0);
	bmat(1,4) = dFdRoll(1,0);
	bmat(2,4) = dFdRoll(2,0);

	bmat(0,5) = dFdYaw(0,0);
	bmat(1,5) = dFdYaw(1,0);
	bmat(2,5) = dFdYaw(2,0);

	//swing angle
	bmat(0,6) = df3_dAlpha(0,0);
	bmat(1,6) = df3_dAlpha(1,0);
	bmat(2,6) = df3_dAlpha(2,0);

	bmat(0,7) = df4_dBeta(0,0);
	bmat(1,7) = df4_dBeta(1,0);
	bmat(2,7) = df4_dBeta(2,0);
	//ranging distance
	bmat(0,8) = (-Lmat*config.RangeS)(0,0);
	bmat(1,8) = (-Lmat*config.RangeS)(1,0);
	bmat(2,8) = (-Lmat*config.RangeS)(2,0);

	/////////////////////////////
	//J Matrix
	/////////////////////////////
	//bore-sight factors (bias_X, bias_Y, bias_Z)
	//Partial derivatives
	jmat_lidar.Insert(0,0,INS_R.Rmatrix);
	
	//rotational alignment (bias_O, bias_P, bias_K)
	temp = ((INS_R.Rmatrix%dOffsetR_dO.Rmatrix%SwingR)*-dist);
	jmat_lidar.Insert(0,3,temp);

	temp = ((INS_R.Rmatrix%dOffsetR_dP.Rmatrix%SwingR)*-dist);
	jmat_lidar.Insert(0,4,temp);

	temp = ((INS_R.Rmatrix%dOffsetR_dK.Rmatrix%SwingR)*-dist);
	jmat_lidar.Insert(0,5,temp);

	//scan angle offset
	jmat_lidar.Insert(0,6,(df1_dAlpha_offset));

	jmat_lidar.Insert(0,7,(df2_dBeta_offset));

	//laser ranging offset
	jmat_lidar.Insert(0,8,-Lmat);

	//laser ranging scale
	jmat_lidar.Insert(0,9, Lmat*-dist);

	//points
	jmat_Point(0,0) = -1.0;
	jmat_Point(1,1) = -1.0;
	jmat_Point(2,2) = -1.0;

	kmat(0,0) = Point.X - X0(0, 0);
	kmat(1,0) = Point.Y - X0(1, 0);
	kmat(2,0) = Point.Z - X0(2, 0);
}

CSMMatrix<double> CSMLIDARCalibration::Correlation(CSMMatrix<double> VarCov)
{
	unsigned int i, j;
	//Correlation of Unknown Matrix
	CSMMatrix<double> Correlation;
	Correlation.Resize(VarCov.GetRows(),VarCov.GetCols(),0.);
	
	for(i=0; i<Correlation.GetRows(); i++)
	{
		double sigma_i, sigma_j;
		sigma_i = sqrt(VarCov(i,i));
		
		for(j=0; j<Correlation.GetCols();j++)
		{
			sigma_j = sqrt(VarCov(j,j));
			Correlation(i,j) = VarCov(i,j)/sigma_i/sigma_j;
		}
	}
	return Correlation;
}

void CSMLIDARCalibration::MatrixOut(fstream &file, CLeastSquareLIDAR &LS)
{
	int i, j;
	
	CSMMatrix<double> CMAT = LS.GetCmatrix();
	file<<setw(10)<<"[C MAT]"<<endl;
	for(i=0; i<(int)CMAT.GetRows(); i++)
	{
		for(j=0; j<(int)CMAT.GetCols(); j++)
			file<<setw(10)<<CMAT(i,j)<<"\t";
		file<<endl;
	}
	file<<endl;
	
	CSMMatrix<double> CorreMat;
	CSMMatrix<double> NMAT_dot = LS.GetNdot();
	CSMMatrix<double> NMAT_dot_inv = NMAT_dot.Inverse();

//	file<<setw(10)<<"[NMAT_dot Mat]"<<endl;
//	for(i=0; i<(int)NMAT_dot.GetRows(); i++)
//	{
//		for(j=0; j<(int)NMAT_dot.GetCols(); j++)
//			file<<setw(10)<<NMAT_dot(i,j)<<"\t";
//		file<<endl;
//	}
//	file.flush();
//	file<<endl;
	
	CorreMat = Correlation(NMAT_dot_inv);
	file<<setw(10)<<"[CorreMat]"<<endl;
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
			file<<setw(10)<<CorreMat(i,j)<<"\t";
		file<<endl;
	}
	file.flush();
	file<<endl;

	CSMList<MatrixData> V;
	LS.GetLIDARErrorMatwithPoint(V);
	int n_mat = V.GetNumItem();
	DATA<MatrixData> *pos = NULL;
	file<<setw(10)<<"[V]"<<endl;
	for(i=0; i<n_mat; i++)
	{
		pos = V.GetNextPos(pos);
		int nrows = pos->value.mat.GetRows();
		for(j=0; j<nrows; j++)
		{
			file<<setw(10)<<pos->value.mat(j,0)<<endl;
		}
	}
	file<<endl;
	file.flush();
}

bool CSMLIDARCalibration::IterationReport(int nIteration, int max_iter, fstream &outfile, fstream &csvfile, CString resultfile, double sigma, double &old_sigma, CSMLIDARConfig OutConfig, CSMMatrix<double> &NMAT_dot_inv, CSMList<CLIDARCalibrationPoint> &PointList, CSMList<CLIDARCalibrationPoint> &OriginalPointList, double limit_change_sd)
{
	bool bStop = true;

	outfile.setf(ios::scientific );
	outfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<"\t"<<sigma<<endl;
	
	csvfile.precision(10);
	csvfile.setf(ios::scientific );
	csvfile<<setw(10)<<nIteration<<endl<<" Posteriori standard deviation(unitless): "<<","<<sigma<<endl;
	
	outfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
	outfile<<setw(10)<<OutConfig.Xb<<"\t";
	outfile<<setw(10)<<OutConfig.Yb<<"\t";
	outfile<<setw(10)<<OutConfig.Zb<<"\t";
	outfile<<setw(10)<<Rad2Deg(OutConfig.Ob)<<"\t";
	outfile<<setw(10)<<Rad2Deg(OutConfig.Pb)<<"\t";
	outfile<<setw(10)<<Rad2Deg(OutConfig.Kb)<<"\t";
	outfile<<endl;
	
	outfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
	outfile<<setw(10)<<(Rad2Deg(OutConfig.Alpha_b))<<"\t";
	outfile<<setw(10)<<(Rad2Deg(OutConfig.Beta_b))<<"\t";
	outfile<<setw(10)<<(OutConfig.Rangeb)<<"\t";
	outfile<<setw(10)<<(OutConfig.RangeS)<<"\t";
	outfile<<endl;
				
	
	csvfile<<setw(10)<<"Xb(M), Yb(M), Zb(M), Ob(deg), Pb(deg), Kb(deg)"<<endl;
	csvfile<<setw(10)<<OutConfig.Xb<<",";
	csvfile<<setw(10)<<OutConfig.Yb<<",";
	csvfile<<setw(10)<<OutConfig.Zb<<",";
	csvfile<<setw(10)<<Rad2Deg(OutConfig.Ob)<<",";
	csvfile<<setw(10)<<Rad2Deg(OutConfig.Pb)<<",";
	csvfile<<setw(10)<<Rad2Deg(OutConfig.Kb)<<",";
	csvfile<<endl;
	
	csvfile<<setw(10)<<"Alpha_b(deg), Beta_b(deg), Range_b(M), Range_S"<<endl;
	csvfile<<setw(10)<<(Rad2Deg(OutConfig.Alpha_b))<<",";
	csvfile<<setw(10)<<(Rad2Deg(OutConfig.Beta_b))<<",";
	csvfile<<setw(10)<<(OutConfig.Rangeb)<<",";
	csvfile<<setw(10)<<(OutConfig.RangeS)<<",";
	csvfile<<endl;
	
	outfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
	outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<"\t";
	outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<"\t";
	outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<"\t";
	outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<"\t";
	outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<"\t";
	outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<"\t";
	outfile<<endl;
	
	outfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
	outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<"\t";
	outfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<"\t";
	outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<"\t";
	outfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<"\t";
	outfile<<endl<<endl;
	
	outfile.flush();
	
	csvfile<<setw(10)<<"s_Xb(mm), s_Yb(mm), s_Zb(mm), s_Ob(sec), s_Pb(sec), s_Kb(sec)"<<endl;
	csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))*1000<<",";
	csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))*1000<<",";
	csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))*1000<<",";
	csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))*3600<<",";
	csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))*3600<<",";
	csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))*3600<<",";
	csvfile<<endl;
	
	csvfile<<setw(10)<<"s_Alpha_b(sec), s_Beta_b(sec), s_Range_b(mm), s_Range_S"<<endl;
	csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))*3600<<",";
	csvfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))*3600<<",";
	csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(8,8))*1000<<",";
	csvfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(9,9))*1000<<",";
	csvfile<<endl<<endl;
	
	if(nIteration > 1)
	{
		if(fabs(old_sigma-sigma)<fabs(limit_change_sd))
		{
			outfile<<endl<<endl<<"##################################################################"<<endl;
			outfile<<"Iteration Stop(fabs(old_sigma-sigma)): "<<fabs(old_sigma - sigma)<<endl;
			outfile<<"##################################################################"<<endl<<endl;
			bStop = false;
		}
	}
	
	old_sigma = sigma;
	
	if(nIteration >= max_iter)
	{
		outfile<<endl<<endl<<"##################################################################"<<endl;
		outfile<<"Iteration Stop(nIteration): "<<nIteration<<endl;
		outfile<<"##################################################################"<<endl<<endl;
		bStop = false;
	}
	
	if(bStop == false)
	{
		fstream newconfigfile;
		CString configpath = resultfile;
		configpath.MakeLower();
		configpath.Replace(".result",".config");
		newconfigfile.open(configpath,ios::out);
		newconfigfile <<"VER 1.3"<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"#Spatial offsets(3EA, M)"<<endl;
		newconfigfile <<"#Sigma of spatial offsets(3EA, M)"<<endl;
		newconfigfile <<"#"<<endl;
		newconfigfile.precision(10);
		newconfigfile.setf(ios::scientific );
		newconfigfile<<setw(10)<<(OutConfig.Xb)<<"\t";
		newconfigfile<<setw(10)<<(OutConfig.Yb)<<"\t";
		newconfigfile<<setw(10)<<(OutConfig.Zb)<<endl;
		newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(0,0))<<"\t";
		newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(1,1))<<"\t";
		newconfigfile<<setw(10)<<sigma*sqrt(NMAT_dot_inv(2,2))<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"#Rotational offsets(3EA, deg)"<<endl;
		newconfigfile <<"#Biases in rotational offsets(3EA, deg)"<<endl;
		newconfigfile <<"#Sigma of rotational offsets biases(3EA, deg)"<<endl;
		newconfigfile <<"#"<<endl;
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.Ob)<<"\t";
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.Pb)<<"\t";
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.Kb)<<endl;
		newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(3,3)))<<"\t";
		newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(4,4)))<<"\t";
		newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(5,5)))<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"#Scan angle offsets(2EA, deg)"<<endl;
		newconfigfile <<"#Sigma of scan angle offset(2EA, deg)"<<endl;
		newconfigfile <<"#"<<endl;
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.Alpha_b)<<"\t";
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.Beta_b)<<endl;
		newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(6,6)))<<"\t";
		newconfigfile<<setw(10)<<Rad2Deg(sigma*sqrt(NMAT_dot_inv(7,7)))<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"#Ranging bias (1EA, M), Sigma of ranging bias(1EA, M)"<<endl;
		newconfigfile<<setw(10)<<(OutConfig.Rangeb)<<"\t";
		newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(8,8)))<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"#Scale of laser ranging(1EA, M), Sigma of scale of laser ranging(1EA)"<<endl;
		newconfigfile<<setw(10)<<(OutConfig.RangeS)<<"\t";
		newconfigfile<<setw(10)<<(sigma*sqrt(NMAT_dot_inv(9,9)))<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"#sigma of GPS signal[3EA(X,Y,Z), M]"<<endl;
		newconfigfile<<setw(10)<<OutConfig.sX<<"\t";
		newconfigfile<<setw(10)<<OutConfig.sY<<"\t";
		newconfigfile<<setw(10)<<OutConfig.sZ<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"#sigma of INS signal[3EA(O,P,K), rad]"<<endl;
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.sO)<<"\t";
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.sP)<<"\t";
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.sK)<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"#sigma of swing angle[2EA, rad]"<<endl;
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.sAlpha)<<"\t";
		newconfigfile<<setw(10)<<Rad2Deg(OutConfig.sBeta)<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"##sigma of laser ranging[1EA, M]"<<endl;
		newconfigfile<<setw(10)<<OutConfig.sRange<<endl;
		newconfigfile <<"####################################################################################################################"<<endl;
		newconfigfile <<"#ID"<<endl;
		newconfigfile <<OutConfig.ID<<endl;
		newconfigfile.close();
	}
	
	outfile<<"[Coordinates]"<<endl;
	
	outfile.setf(ios::fixed, ios::floatfield);
	outfile.precision(4);

	int num_Points = PointList.GetNumItem();
	DATA<CLIDARCalibrationPoint>*	point_pos = NULL;
	DATA<CLIDARCalibrationPoint>*	oripoint_pos = NULL;
	for(int i=0; i<(int)num_Points; i++)
	{
		point_pos = PointList.GetNextPos(point_pos);
		CLIDARCalibrationPoint point = point_pos->value;
		//SM3DPoint point = ControlPoints.GetAt(i);
		
		outfile<<setw(15)<<point.X<<"\t";
		outfile<<setw(15)<<point.Y<<"\t";
		outfile<<setw(15)<<point.Z<<"\t";
		
		oripoint_pos = OriginalPointList.GetNextPos(oripoint_pos);
		CLIDARCalibrationPoint oldpoint = oripoint_pos->value;
		//SM3DPoint oldpoint = OriginalControlPoints.GetAt(i);
		
		outfile<<setw(15)<<oldpoint.X<<"\t";
		outfile<<setw(15)<<oldpoint.Y<<"\t";
		outfile<<setw(15)<<oldpoint.Z<<endl;
	}
	
	outfile<<endl<<endl;

	return bStop;
}

bool CSMLIDARCalibration::SolveWithPlaneConstraints(const char resultfile[], int max_iter, double Small_QThreshold, double Big_QThreshold, int EQtype, double &sigma, double limit_diff_sd)
{
	int i, j;

	//number of unknown parameters
	const int num_unknown = 10;//3 spatial offset, 3 rotational offset, 2 scan angle offset, range bias, range scale

	//Number of patches
	int num_Points = PointList.GetNumItem();

	//original patches
	OriginalPointList = PointList;

	//matrix for areas of triangles
	CSMMatrix<double> NMAT_dot, CMAT_dot, NMAT_dot_inv;
	CSMMatrix<double> N2dot_point;
	CSMMatrix<double> N2dot_line;
	CSMMatrix<double> N2dot_tri;
	
	//result files
	fstream outfile;//txt file
	outfile.precision(10);
	fstream outfile_mat;//txt file
	outfile_mat.precision(10);
	fstream csvfile;//csv file
	fstream etpefile;//etpe file
	etpefile.precision(10);
	fstream duvwfile;//duvw file
	duvwfile.precision(10);

	CString csvpath = resultfile;
	csvpath.MakeLower();
	csvpath.Replace(".result",".csv");
	csvfile.open(csvpath, ios::out);
	
	CString resultname = resultfile;
	outfile.open(resultname,ios::out);
	outfile.setf(ios::fixed, ios::floatfield);

	CString resultname_mat = resultfile;
	resultname_mat.MakeLower(); resultname_mat.Replace(".result",".mat");
	outfile_mat.open(resultname_mat,ios::out);
	outfile_mat.setf(ios::fixed, ios::floatfield);

	CString etpename = resultfile;
	etpename.MakeLower(); etpename.Replace(".result", ".etpe");
	etpefile.open(etpename, ios::out);
	etpefile.setf(ios::fixed, ios::floatfield);

	CString duvwname = resultfile;
	duvwname.MakeLower(); duvwname.Replace(".result", ".duvw");
	duvwfile.open(duvwname, ios::out);
	duvwfile.setf(ios::fixed, ios::floatfield);

	bool bStop = true;
	int nIteration = 0;
	//number of observations
	const int num_obs = 9;
	double old_sigma;

	//Weight for parameters
	CSMMatrix<double> QParam(num_unknown,num_unknown,0.);
	QParam(0,0) = Config.sXb*Config.sXb;
	QParam(1,1) = Config.sYb*Config.sYb;
	QParam(2,2) = Config.sZb*Config.sZb;

	QParam(3,3) = Config.sOb*Config.sOb;
	QParam(4,4) = Config.sPb*Config.sPb;
	QParam(5,5) = Config.sKb*Config.sKb;
	
	QParam(6,6) = Config.sAlpha_b*Config.sAlpha_b;
	QParam(7,7) = Config.sBeta_b*Config.sBeta_b;

	QParam(8,8) = Config.sRangeb*Config.sRangeb;
	QParam(9,9) = Config.sRangeS*Config.sRangeS;
	
	//fixed unknown parameter list
	CSMMatrix<double> FixedParamIndex(1, num_unknown, 1.0);
	for(i=0; i<(int)num_unknown; i++)
	{
		if(QParam(i,i)<pow(Small_QThreshold,2))
		{
			FixedParamIndex(0,i) = 0.0;
		}
	}

	//weight for 9 observations
	CSMMatrix<double> qmat;
	qmat.Resize(num_obs,num_obs,0.);

	qmat(0,0) = Config.sX*Config.sX;
	qmat(1,1) = Config.sY*Config.sY;
	qmat(2,2) = Config.sZ*Config.sZ;

	qmat(3,3) = Config.sO*Config.sO;
	qmat(4,4) = Config.sP*Config.sP;
	qmat(5,5) = Config.sK*Config.sK;

	qmat(6,6) = Config.sAlpha*Config.sAlpha;
	qmat(7,7) = Config.sBeta*Config.sBeta;
	qmat(8,8) = Config.sRange*Config.sRange;

	CSMMatrix<double> kmat, jmat_lidar, jmat_Point, bmat;
	kmat.Resize(3,1,0.);
	jmat_lidar.Resize(3,num_unknown,0.);
	jmat_Point.Resize(3,3,0.);
	bmat.Resize(3,num_obs,0.);
	
	DATA<CLIDARCalibrationPoint> *point_pos = NULL;
	DATA<CLIDARCalibrationPoint> *oripoint_pos = NULL;
	
	
	CLeastSquareLIDAR LS;
	
	do
	{	//Increase iteration number from 0
		nIteration ++;
		
		outfile<<"Iteration "<<nIteration<<endl;
		outfile<<"-------------------------------------"<<endl;

		outfile_mat<<"Iteration "<<nIteration<<endl;
		outfile_mat<<"-------------------------------------"<<endl;

		etpefile<<"etpe in Iteration "<<nIteration<<endl;
		etpefile<<"-------------------------------------"<<endl;

		duvwfile<<"dU, dV, dW in Iteration "<<nIteration<<endl;
		duvwfile<<"-------------------------------------"<<endl;
		
		LS.SetDataConfig(1, num_unknown, num_Points, 0, 0);		
		
		//boresight vectior (spatial offset)
		CSMMatrix<double> Pvector;
		Pvector.Resize(3,1);
		Pvector(0,0) = Config.Xb;
		Pvector(1,0) = Config.Yb;
		Pvector(2,0) = Config.Zb;
		
		//Bore-sighting angles
		double dO, dP, dK;
		CRotationcoeff2 Offset_R;
		//partial derivatives for bore-sight angles
		CRotationcoeff2 dOffsetR_dO;
		CRotationcoeff2 dOffsetR_dP;
		CRotationcoeff2 dOffsetR_dK;

		dO = Config.Ob;
		dP = Config.Pb;
		dK =Config.Kb;
		
		//INS Rotation matrix: R matrix
		_Rmat_ Rmat	(0,		dP,		0);//rotation matrix (roll)
		_Rmat_ Pmat	(dO,	0,		0);//rotation matrix (pitch)
		_Rmat_ Ymat	(0,		0,		dK);//rotation matrix (yaw)

		Offset_R.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;

		if(EQtype == 3)
			Offset_R.Rmatrix = Ymat.Rmatrix%Rmat.Rmatrix%Pmat.Rmatrix;
		
		//partial derivatives for angle biases (omega)
		dOffsetR_dO.Partial_dRdO(dO,0,0);
		dOffsetR_dO.Rmatrix = Ymat.Rmatrix%dOffsetR_dO.Rmatrix%Rmat.Rmatrix;
		
		if(EQtype == 3)
			dOffsetR_dO.Rmatrix = Ymat.Rmatrix%Rmat.Rmatrix%dOffsetR_dO.Rmatrix;

		//partial derivatives for angle biases (phi)
		dOffsetR_dP.Partial_dRdP(0,dP,0);
		dOffsetR_dP.Rmatrix = Ymat.Rmatrix%Pmat.Rmatrix%dOffsetR_dP.Rmatrix;
		if(EQtype == 3)
			dOffsetR_dP.Rmatrix = Ymat.Rmatrix%dOffsetR_dP.Rmatrix%Pmat.Rmatrix;

		//partial derivatives for angle biases (kappa)
		dOffsetR_dK.Partial_dRdK(0,0,dK);
		dOffsetR_dK.Rmatrix = dOffsetR_dK.Rmatrix%Pmat.Rmatrix%Rmat.Rmatrix;
		if(EQtype == 3)
			dOffsetR_dK.Rmatrix = dOffsetR_dK.Rmatrix%Rmat.Rmatrix%Pmat.Rmatrix;
		
		////////////////////////////////////////////////////
		//
		//LIDAR Patches
		//
		////////////////////////////////////////////////////
		
		point_pos = NULL;
		oripoint_pos = NULL;
		for(i=0; i<num_Points; i++)
		{
			//To get control point
			//SM3DPoint point = ControlPoints.GetAt(i);

			//To get LIDAR raw point
			//CLIDARRawPoint rawpoint = RawPointList.GetAt(i);

			//To get original control point
			//SM3DPoint oripoint = OriginalControlPoints.GetAt(i);

			point_pos = PointList.GetNextPos(point_pos);
			oripoint_pos = OriginalPointList.GetNextPos(oripoint_pos);

			CLIDARCalibrationPoint point = point_pos->value;
			CLIDARCalibrationPoint oripoint = oripoint_pos->value;
			
			//Point fixing
			CSMMatrix<double> FixedParamPoint(1,3,1.0);
			double max_weight;
			if(fabs(Small_QThreshold) > 1.0e-99 )
			{
				max_weight = 1.0/pow(Small_QThreshold,2);

				for(int index=0; index<(int)3;index++)
				{
					if(point.P(index,index) > max_weight)
					{
						FixedParamPoint(0,index) = 0.0;
					}
				}
			}

			//Partial derivatives
			switch(EQtype)
			{
			case 0://Terrapoint
				//Cal_PDsofLIDAREQ_Terrapoint(point, Config, jmat_lidar,jmat_Point, bmat, kmat, Offset_R, Pvector, dOffsetR_dO, dOffsetR_dP, dOffsetR_dK);
				{
					_Rmat_ Rdumy(0, 0, 0); 
					CSMMatrix<double> dumy = Rdumy.Rmatrix;
					Cal_PDsofLIDAREQ_Basic(point, Config, jmat_lidar,jmat_Point, bmat, kmat, Offset_R, Pvector, dOffsetR_dO, dOffsetR_dP, dOffsetR_dK, dumy);
				}
				break;
			case 1://Terrapoint (plus 90 deg)
				{
					_Rmat_ Rdumy(0, 0, 0); 
					CSMMatrix<double> dumy = Rdumy.Rmatrix;
					Cal_PDsofLIDAREQ_Basic(point, Config, jmat_lidar,jmat_Point, bmat, kmat, Offset_R, Pvector, dOffsetR_dO, dOffsetR_dP, dOffsetR_dK, dumy);
				}
				break;
			case 2://UofC
				//Cal_PDsofLIDAREQ_UofC(point, Config, jmat_lidar,jmat_Point, bmat, kmat, Offset_R, Pvector, dOffsetR_dO, dOffsetR_dP, dOffsetR_dK);
				{
					_Rmat_ Rdumy(0, Deg2Rad(180), 0); 
					CSMMatrix<double> dumy = Rdumy.Rmatrix;
					Cal_PDsofLIDAREQ_Basic(point, Config, jmat_lidar,jmat_Point, bmat, kmat, Offset_R, Pvector, dOffsetR_dO, dOffsetR_dP, dOffsetR_dK, dumy);
				}
				break;
			case 3:
				{
					_Rmat_ Rdumy(0, Deg2Rad(0), 0); 
					CSMMatrix<double> dumy = Rdumy.Rmatrix;
					Cal_PDsofLIDAREQ_Basic(point, Config, jmat_lidar,jmat_Point, bmat, kmat, Offset_R, Pvector, dOffsetR_dO, dOffsetR_dP, dOffsetR_dK, dumy);
				}
				break;
			default:
				return false;
				break;
			}
			
 			outfile_mat<<"kma\t"<<kmat.matrixout()<<endl;
			outfile_mat<<"jmat_lidar\t"<<jmat_lidar.matrixout()<<endl;
			outfile_mat<<"jmat_Point\t"<<jmat_lidar.matrixout()<<endl;
			
			CSMMatrix<double> we_temp = bmat%qmat%bmat.Transpose();
			we_temp = we_temp.Inverse();

			//////////////////////////////////////////
			// dU, dV, dW
			CSMMatrix<double> luvw;
			_Mmat_ M(Deg2Rad(point.PlaneAttitude[0]), Deg2Rad(point.PlaneAttitude[1]), 0);
			luvw = M.Mmatrix%kmat;
			duvwfile<<luvw.matrixout();
			//
			//
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			//reduced normal matrix
			double etpe = LS.Fill_Nmat_Data_LIDARPointwithPoint(FixedParamIndex, FixedParamPoint, jmat_lidar, jmat_Point, we_temp, kmat, i);
			//
			etpefile<<etpe<<"\t";
			
			//Parameter observations(vertex coord)
			CSMMatrix<double> jmat_vtx_param(3,3);
			jmat_vtx_param.Identity(1.0);
			CSMMatrix<double> k_vtx(3,1);

			//CSMMatrix<double>& WVTX = P_Point;
			CSMMatrix<double>& WVTX = point.P;
			
			k_vtx(0,0) = oripoint.X - point.X;
			k_vtx(1,0) = oripoint.Y - point.Y;
			k_vtx(2,0) = oripoint.Z - point.Z;

			etpe = LS.Fill_Nmat_Data_XYZ(jmat_vtx_param,WVTX,k_vtx,i);

			etpefile<<etpe<<endl;

 			outfile_mat<<k_vtx.matrixout()<<endl;

		}//for(i=0; i<num_Points; i++)

	
		//Parameter observations(LIDAR param)
		CSMMatrix<double> WParam = QParam.Inverse();
		
		CSMMatrix<double> jmat_param(num_unknown,num_unknown,0.);
		jmat_param.Identity(1.0);
		CSMMatrix<double> jmat_lidar_k(num_unknown,1,0.);

		jmat_lidar_k(0,0) = OriginConfig.Xb - Config.Xb;
		jmat_lidar_k(1,0) = OriginConfig.Yb - Config.Yb;
		jmat_lidar_k(2,0) = OriginConfig.Zb - Config.Zb;
		
		jmat_lidar_k(3,0) = OriginConfig.Ob - Config.Ob;
		jmat_lidar_k(4,0) = OriginConfig.Pb - Config.Pb;
		jmat_lidar_k(5,0) = OriginConfig.Kb - Config.Kb;
		
		jmat_lidar_k(6,0) = OriginConfig.Alpha_b - Config.Alpha_b;
		jmat_lidar_k(7,0) = OriginConfig.Beta_b - Config.Beta_b;
		
		jmat_lidar_k(8,0) = OriginConfig.Rangeb - Config.Rangeb;
		jmat_lidar_k(9,0) = OriginConfig.RangeS - Config.RangeS;

		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!              !!!!!!!!!!!!!!!!!!!!!!!!!!!
		////////////////////////////////////////////////////////////////////////////
		//
		//Check this part in different functions
		//
		//////////////
		for(int param_index=0; param_index<num_unknown; param_index++)
		{
			if(sqrt(QParam(param_index,param_index)) <= fabs(Small_QThreshold))
			{
				jmat_lidar_k(param_index, 0) = 0.0; 
			}					
		}
		
		double etpe = LS.Fill_Nmat_LIDARParam(jmat_param,WParam,jmat_lidar_k);

		etpefile<<etpe<<endl;
		etpefile.flush();

 		//outfile<<jmat_lidar_k.matrixout()<<endl;
		
		double var;
		CSMMatrix<double> XMAT = LS.RunLeastSquare_RN(var, NMAT_dot, CMAT_dot);
		NMAT_dot_inv = NMAT_dot.Inverse();

		MatrixOut(outfile_mat, LS);
		
		outfile.precision(10);
		outfile.setf(ios::scientific);
		
		outfile_mat<<setw(10)<<"[XMAT]"<<endl;
		for(i=0; i<(int)XMAT.GetRows(); i++)
		{
			for(j=0; j<(int)XMAT.GetCols(); j++)
				outfile_mat<<setw(10)<<XMAT(i,j)<<"\t";
			outfile_mat<<endl;
		}
		outfile_mat.flush();
		

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		Config.Xb += XMAT(0,0);
		Config.Yb += XMAT(1,0);
		Config.Zb += XMAT(2,0);
		
		Config.Ob += XMAT(3,0);
		Config.Pb += XMAT(4,0);
		Config.Kb += XMAT(5,0);

		Config.Alpha_b += XMAT(6,0);
		Config.Beta_b  += XMAT(7,0);

		Config.Rangeb += XMAT(8,0);
		Config.RangeS += XMAT(9,0);

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		point_pos = NULL;
		for(i=0; i<(int)num_Points; i++)
		{
			point_pos = PointList.GetNextPos(point_pos);

			point_pos->value.X += XMAT(num_unknown+i*3+0,0);
			point_pos->value.Y += XMAT(num_unknown+i*3+1,0);
			point_pos->value.Z += XMAT(num_unknown+i*3+2,0);
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		sigma = sqrt(var);

		CSMLIDARConfig outConfig = Config;
		switch(EQtype)
		{
		case 0://Terrapoint
			outConfig.Beta_b = - outConfig.Beta_b;
			outConfig.Kb = - outConfig.Kb;
			break;
		case 1://Terrapoint(plus90)
			outConfig.Beta_b = - outConfig.Beta_b;
			outConfig.Kb = - outConfig.Kb;
			break;
		case 2://UofC
			outConfig.Beta_b = - outConfig.Beta_b;
			outConfig.Rangeb = - outConfig.Rangeb;
			break;
		case 3://etc
			//outConfig.Beta_b = - outConfig.Beta_b;
			//outConfig.Rangeb = - outConfig.Rangeb;
			break;
		default:
			AfxMessageBox("option fail");
			break;
		}
		
		bStop = IterationReport(nIteration, max_iter, outfile, csvfile, resultfile, sigma, old_sigma, outConfig, NMAT_dot_inv, PointList, OriginalPointList, limit_diff_sd);
		
	}while(bStop == true);

	CString nimgpath = resultfile;
	nimgpath.MakeLower();
	nimgpath.Replace(".result",".bmp");

// 	if(false == LS.MakeMatrixImg(0.9, nimgpath))
// 	{
// 		outfile<<endl<<endl<<"---------------------------------------"<<endl;
// 		outfile<<"Image Generation Fail: Size is too big"<<endl;
// 		outfile<<"---------------------------------------"<<endl;
// 	}

	CSMMatrix<double> CorreMat;
	CorreMat = Correlation(NMAT_dot_inv);
	outfile<<setw(10)<<"[CorreMat]"<<endl;
	for(i=0; i<(int)CorreMat.GetRows(); i++)
	{
		for(j=0; j<(int)CorreMat.GetCols(); j++)
			outfile<<setw(10)<<CorreMat(i,j)<<"\t";
		outfile<<endl;
	}
	outfile.flush();
	
	outfile.setf(ios::scientific);
	
	//
	///////////////////////////////////////////////////////////////////////
	
	outfile.close();
	outfile_mat.close();
	csvfile.close();
	etpefile.close();
	duvwfile.close();

	return true;
}

bool CSMLIDARCalibration::CalibrationWithPlaneConstraints(CString cfgfilepath, CString patchpath, CString fileinfopath, CString resultfile, 
		                                                double Small_threshold_sigam, double Large_threshold_sigam, int max_iter, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid)
{
	if(false == Config.ReadConfigFile(cfgfilepath, EQtype)) return false; //*<Read configuration file*/

	//*<copy original config data*/
	OriginConfig = Config;

	Config.CheckZero(1.0e-30); //*<Make Zero given threshold (default: 1.0e-30)

	//Data initialization
	PointList.RemoveAll();

	if(false == ReadInputData(patchpath, fileinfopath, PointList, EQtype, bMeridianConversion, ellipsoid))  /**<Read input data*/
	{
		AfxMessageBox("Error in ReadInputData()");
		return false;
	}

	double sd;
	if(true == SolveWithPlaneConstraints(resultfile, max_iter, Small_threshold_sigam, Large_threshold_sigam, EQtype, sd))
	{
		AfxMessageBox("Done: resultfile");
		return true;
	}
	else
	{
		AfxMessageBox("Done: Error in calibration (SolveWithPlaneConstraints)");
		return false;
	}
}

bool CSMLIDARCalibration::RunICPoint(int EQType, double &NDIST, KDTree &Ref_KD_Tree, double threshold, CSMList<CLIDARCalibrationPoint> &alllist, CSMList<CLIDARCalibrationPoint> &newlist)
{
	double sum_NDISTi = 0;
	DATA<CLIDARCalibrationPoint> *pos = NULL;
	
	sum_NDISTi = 0;
	
	for( unsigned long i=0; i<alllist.GetNumItem(); i++)
	{
		pos = alllist.GetNextPos(pos);
		
		GetGroundFootPrint(pos->value, EQType);
		
		double x_tar[3];
		
		x_tar[0] = pos->value.X;
		x_tar[1] = pos->value.Y;
		x_tar[2] = pos->value.Z;
		
		KDNode *nearest_ref = Ref_KD_Tree.find_nearest(x_tar);

		double NDISTi = sqrt( (x_tar[0]-nearest_ref->x[0])*(x_tar[0]-nearest_ref->x[0]) 
			+ (x_tar[1]-nearest_ref->x[1])*(x_tar[1]-nearest_ref->x[1]) 
			+ (x_tar[2]-nearest_ref->x[2])*(x_tar[2]-nearest_ref->x[2]) );

		if( NDISTi > threshold)
			continue;

		CLIDARCalibrationPoint p;
		p = pos->value;
		
		p.X = nearest_ref->x[0];
		p.Y = nearest_ref->x[1];
		p.Z = nearest_ref->x[2];

		newlist.AddTail(p);
		
		sum_NDISTi += NDISTi;
	}
	
	NDIST = sum_NDISTi / PointList.GetNumItem();

	return true;
}

bool CSMLIDARCalibration::RunICPatch(int EQType, double &NDIST, CSMList<CTriangle> &TIN, double threshold, CSMList<CLIDARCalibrationPoint> &alllist, CSMList<CLIDARCalibrationPoint> &newlist)
{
	double sum_NDISTi = 0;
	
	DATA<CLIDARCalibrationPoint> *pos = NULL;
	
	for( unsigned long i=0; i<alllist.GetNumItem(); i++)
	{
		pos = alllist.GetNextPos(pos);
		
		GetGroundFootPrint(pos->value, EQType);

		///////////////////////////////////////////////////////
		//Inside check
		int nIntersect = 0;//Number of intersection with sides

		double x, y;
		double x0, y0, x1, y1;

		DATA<CTriangle> *pos_TIN = NULL;

		CTriangle tri;

		bool bInside;
		
		for(int ntri=0; ntri<(int)TIN.GetNumItem(); ntri++)
		{
			bInside = false;

			pos_TIN = TIN.GetNextPos(pos_TIN);
			
			x = pos->value.X;
			y = pos->value.Y;			
			
			x0 = pos_TIN->value.vertex1[0];
			y0 = pos_TIN->value.vertex1[1];
			x1 = pos_TIN->value.vertex2[0];
			y1 = pos_TIN->value.vertex2[1];
			
			if ( ( y1<y && y0>=y ) || ( y0<y && y1>=y ) ) //check whether y is between two nodes' y coordinates 
			{
				if ( ( x1 + (y-y1) / (y0-y1) * (x0-x1) ) < x )//Check intersection (line is left side of given point) 
				{
					nIntersect ++;//Increase number of intersection
				}
				else if ( ( x1 + (y-y1) / (y0-y1) * (x0-x1) ) == x )//Check intersection (point is on the line)
				{
					nIntersect = 1;
					break;
				}
			}
			
			x0 = pos_TIN->value.vertex3[0];
			y0 = pos_TIN->value.vertex3[1];
			x1 = pos_TIN->value.vertex2[0];
			y1 = pos_TIN->value.vertex2[1];
			
			if ( ( y1<y && y0>=y ) || ( y0<y && y1>=y ) ) //check whether y is between two nodes' y coordinates 
			{
				if ( ( x1 + (y-y1) / (y0-y1) * (x0-x1) ) < x )//Check intersection (line is left side of given point) 
				{
					nIntersect ++;//Increase number of intersection
				}
				else if ( ( x1 + (y-y1) / (y0-y1) * (x0-x1) ) == x )//Check intersection (point is on the line)
				{
					nIntersect = 1;
					break;
				}
			}
			
			x0 = pos_TIN->value.vertex1[0];
			y0 = pos_TIN->value.vertex1[1];
			x1 = pos_TIN->value.vertex3[0];
			y1 = pos_TIN->value.vertex3[1];
			
			if ( ( y1<y && y0>=y ) || ( y0<y && y1>=y ) ) //check whether y is between two nodes' y coordinates 
			{
				if ( ( x1 + (y-y1) / (y0-y1) * (x0-x1) ) < x )//Check intersection (line is left side of given point) 
				{
					nIntersect ++;//Increase number of intersection
				}
				else if ( ( x1 + (y-y1) / (y0-y1) * (x0-x1) ) == x )//Check intersection (point is on the line)
				{
					nIntersect = 1;

					tri = pos_TIN->value;

					bInside = true;

					break;
				}
			}

			if( nIntersect%2 != 0 )
			{
				tri = pos_TIN->value;

				bInside = true;

				break;
			}
		}

		if( bInside == true )
		{	
			CLIDARCalibrationPoint p = pos->value;

			double X1, Y1, Z1, X2, Y2, Z2, X3, Y3, Z3;
			
			X1 = tri.vertex1[0];
			Y1 = tri.vertex1[1];
			Z1 = tri.vertex1[2];
			
			X2 = tri.vertex2[0];
			Y2 = tri.vertex2[1];
			Z2 = tri.vertex2[2];
			
			X3 = tri.vertex3[0];
			Y3 = tri.vertex3[1];
			Z3 = tri.vertex3[2];
			
			CSMMatrix<double> A(3,3),  X, L(3,1);
			A(0, 0) = X1; A(0, 1) = Y1; A(0, 2) = Z1; L(0,0) = 1;
			A(1, 0) = X2; A(1, 1) = Y2; A(1, 2) = Z2; L(1,0) = 1;
			A(2, 0) = X3; A(2, 1) = Y3; A(2, 2) = Z3; L(2,0) = 1;
			
			X = A.Inverse()%L;
			
			CSMMatrix<double> P(3,1);
			P(0,0) = p.X;
			P(1,0) = p.Y;
			P(2,0) = p.Z;
			
			CSMMatrix<double> XX;
			XX = X.Transpose()%X;
			double t = ( X(0,0)*P(0,0) + X(1,0)*P(1,0) + X(2,0)*P(2,0) - 1 ) / XX(0,0);
			
			CSMMatrix<double> OH(3,1);
			OH = X*t;
			OH = P + OH;
			
			CLIDARCalibrationPoint newp;
			newp = p;
			
			newp.X = OH(0,0);
			newp.Y = OH(1,0);
			newp.Z = OH(2,0);			

			double NDISTi = sqrt ( (p.X - newp.X)*(p.X - newp.X) + (p.Y - newp.Y)*(p.Y - newp.Y) + (p.Z - newp.Z)*(p.Z - newp.Z) );
			sum_NDISTi += NDISTi;
			
			double a = X(0,0);
			double b = X(1,0);
			double c = X(2,0);
			double dist = fabs(a*p.X + b*p.Y + c*p.Z - 1) / sqrt(a*a + b*b + c*c);
			
			if(NDISTi < threshold)
				newlist.AddTail(newp);
		}

	}

	NDIST = sum_NDISTi / newlist.GetNumItem();
	
	return true;
}

bool CSMLIDARCalibration::ReadICPoint(LIDAR_CALIBRATION_PRJ_STRUCT &prj, KDTree &Ref_KD_Tree, double &Dist_Threshold)
{
	bool bMeridianConversion;
	
	if(prj.MC == 1) bMeridianConversion = true;
	else bMeridianConversion = false;
	
	//Input: ICPoint data file
	fstream ICPointFile;
	ICPointFile.open(prj.ControlPatchICPointPath, ios::in|ios::nocreate);

	if(!ICPointFile) return false;
	
	CSMList<int> index;	
	if(false == ReadIndexFile(prj.ControlPatchICPointContentsPath, index)) 
	{
		AfxMessageBox("Error: ReadIndexFile (ICPoint)");
		return false;
	}
	
	CString Contents_dat="";
	char line[MAX_LINE_LENGTH];
		
	while(!ICPointFile.eof())
	{
		RemoveCommentLine(ICPointFile, '!');
		
		RemoveCommentLine(ICPointFile, '!');
		ICPointFile>>Dist_Threshold;//distance threshold for ICP
		ICPointFile.eatwhite();
		
		////////////////////////////////////////////////////////////////////////////////////////
		//Read target surface
		//
		
		char letter = ICPointFile.peek();
		if(letter != 'T') return false;
		
		ICPointFile.getline(line, MAX_LINE_LENGTH);//TARGET_SURF
		ICPointFile.eatwhite();
		
		int count = 0;
		
		while('R' != ICPointFile.peek())
		{
			RemoveCommentLine(ICPointFile, '!');
			
			CLIDARCalibrationPoint point, point_old;
			
			if(false == ReadRecord(ICPointFile, index, point, point_old, prj.EQType, bMeridianConversion, prj.ellipsoid)) return false;
			
			GetGroundFootPrint(point, prj.EQType);
			
			All_ICP_PointList.AddTail(point);
			
			count ++;
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Read reference surface
		//
		
		ICPointFile.getline(line, MAX_LINE_LENGTH);//REFERENCE_SURF
		ICPointFile.eatwhite();
		
		double x[3];
		
		Ref_KD_Tree.InitialSetup(3);
		
		while('E' != ICPointFile.peek())
		{
			RemoveCommentLine(ICPointFile, '!');
			
			ICPointFile>>x[0]>>x[1]>>x[2];
			ICPointFile.eatwhite();
			
			Ref_KD_Tree.add(x);
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//End of surface data set
		ICPointFile.getline(line, MAX_LINE_LENGTH);//END_SURF
		ICPointFile.eatwhite();
	}
	
	ICPointFile.close();
	
	return true;
}

CString CSMLIDARCalibration::CalibrationWithPlaneConstraints(CString prjpath, CString WorkFolder)
{
	LIDAR_CALIBRATION_PRJ_STRUCT prj;

	stPrj_Path = WorkFolder;

	KDTree Ref_KD_Tree;//KD-tree data

	CSMList<CTriangle> TIN;
	
	//read project file
	if(false == ReadProjectFile(prjpath, prj)) return "Error: ReadProjectFile";

	bool bMeridianConversion;
	
	if(prj.MC == 1) bMeridianConversion = true;
	else bMeridianConversion = false;

	//read LIDAR system config file
	bool retReadConfig = Config.ReadConfigFile(prj.ConfigPath, prj.EQType);
	if(false == retReadConfig) { return "Error: ReadConfigFile()";} //Read configuration file

	//copy original config data
	OriginConfig = Config;

	Config.CheckZero(1.0e-30); //minimum threshold (1.0e-30)

	//Data initialization
	PointList.RemoveAll();

	////////////////////////////////////
	//Read icpoint data
	////////////////////////////////////

	double Dist_Threshold;
	
	if(bICPoint == true)
	{
		All_ICP_PointList.RemoveAll();

		if(false == ReadICPoint(prj, Ref_KD_Tree, Dist_Threshold))
			return "false == ReadICPoint()";
	}

	////////////////////////////////////
	//Read icpatch data
	////////////////////////////////////

	double norm_dist;
	
	if(bICPatch == true)
	{
		All_ICPatch_PointList.RemoveAll();
		
		if( false == ReadICPatch(prj, TIN, norm_dist) )
			return "false == ReadICPatch()";
	}

	/////////////////////////////////////////
	//Read control point data
	/////////////////////////////////////////

	if(bControlPoint == true)
	{
		if(false == ReadInputData(prj.ControlPointsPath, prj.ControlPointsContentsPath, PointList, prj.EQType, bMeridianConversion, prj.ellipsoid))  //Read input data
		{
			return "Error in ReadInputData()";
		}
	}

	if(bICPoint == true || bICPatch == true)
	{
		int nICPoint=0;
		double oldsd;
		double newsd;

		double oldNDIST=0, newNDIST=0;
		bool bStop;
		
		while(nICPoint < prj.Num_Iter)
		{
			bStop = true;

			nICPoint ++;

			Selected_ICP_PointList.RemoveAll();
			Selected_ICPatch_PointList.RemoveAll();
			
			if(bICPoint == true)
			{				
				if( false == RunICPoint( prj.EQType, newNDIST, Ref_KD_Tree, Dist_Threshold, All_ICP_PointList, Selected_ICP_PointList))
					return "false == RunICPoint()";
			}

			if(bICPatch == true)
			{
				if( false == RunICPatch(prj.EQType, newNDIST, TIN, norm_dist, All_ICPatch_PointList, Selected_ICPatch_PointList))
					return "false == RunICPatch()";
			}

			PointList.RemoveAll();
			PointList += Selected_ICP_PointList;
			PointList += Selected_ICPatch_PointList;
			PointList += Control_PointList;

			//Solve
			CString result_path = prj.ResultPath;
			CString temp; temp.Format("_[%d].result", nICPoint);

			result_path.MakeLower(); result_path.Replace(".result", temp);

			if(false == SolveWithPlaneConstraints(result_path, prj.Num_Iter, prj.Small_Sigma, prj.Large_Sigma, prj.EQType, newsd, prj.Diff_Sigma))
			{
				CString tempst;
				tempst.Format("Iteration %d: ", nICPoint);
				return tempst + "Error in calibration (SolveWithPlaneConstraints)";
			}

			if(nICPoint > 1)
			{
				if( fabs (oldsd - newsd) > prj.Small_Sigma)
				{
					bStop = false;
				}
				
				oldsd = newsd;
			}
			else
			{
				bStop = false;
			}

			oldsd = newsd;

			if(bStop == true) break;
		}
	}
	else
	{
		PointList.RemoveAll();
		PointList += Control_PointList;
		//Solve
		double sd;
		if(false == SolveWithPlaneConstraints(prj.ResultPath, prj.Num_Iter, prj.Small_Sigma, prj.Large_Sigma, prj.EQType, sd))
		{
			return "Error in calibration (SolveWithPlaneConstraints)";
		}
	}

	return prj.ResultPath+ " is done";
}

bool CSMLIDARCalibration::GetGroundFootPrint(CString infilepath, CString fileinfopath, CString cfgfilepath, CString outpath, int EQtype, bool bMeridianConversion, ReferenceEllipsoid ellipsoid, CString outpts)
{
	if(outpts == "")
	{
		outpts = infilepath;
		outpts.MakeLower();
		outpts.Replace(".pts","_cal.pts");
	}

	if(false == Config.ReadConfigFile(cfgfilepath, EQtype)) return false; //*<Read configuration file*/

	//*<copy original config data*/
	OriginConfig = Config;

	Config.CheckZero(1.0e-30); //*<Make Zero given threshold (default: 1.0e-30)

	//Data initialization
	PointList.RemoveAll();

	fstream outfile;
	outfile.open(outpath, ios::out);
	outfile.setf(ios::fixed, ios::floatfield);

	fstream outfile_pts;
	outfile_pts.open(outpts, ios::out);
	outfile_pts.setf(ios::fixed, ios::floatfield);
		
	fstream LidarFile;
	LidarFile.open(infilepath, ios::in|ios::nocreate);
	if(!LidarFile) return false;

	CSMList<int> index;	
	if(false == ReadIndexFile(fileinfopath, index)) 
	{
		AfxMessageBox("Error: ReadIndexFile");
		return false;
	}

	CSMMatrix<double> dumy(3,3); dumy.Identity();
	CSMMatrix<double> P;
	CLIDARCalibrationPoint point, point_old;

	int count =0;

	double sumX2=0, sumY2=0, sumZ2=0;

	while(!LidarFile.eof())
	{
		if(false == ReadRecord(LidarFile, index, point, point_old, EQtype, bMeridianConversion, ellipsoid)) return false;
		
		switch(EQtype)
		{
		case 0://Terrapoint
			P = LIDAREQ_Point_BasicEQ(point, Config, dumy);
			break;
		case 1://Terrapoint(scan_angle + 90deg)
			P = LIDAREQ_Point_BasicEQ(point, Config, dumy);
			break;
		case 2://UofC
			{
				_Rmat_ Rdumy(0, Deg2Rad(180), 0); 
				dumy = Rdumy.Rmatrix;
				P = LIDAREQ_Point_BasicEQ(point, Config, dumy);
			}
			break;
		case 3://etc
			{
				_Rmat_ Rdumy(0, Deg2Rad(0), 0); 
				dumy = Rdumy.Rmatrix;
				P = LIDAREQ_Point_BasicEQ(point, Config, dumy);
			}
			break;
		default:
			AfxMessageBox("option fail");
			break;
		}

		double dx = P(0,0)-point.X;
		double dy = P(1,0)-point.Y;
		double dz = P(2,0)-point.Z;

		sumX2 += dx*dx;
		sumY2 += dy*dy;
		sumZ2 += dz*dz;

		outfile.precision(3);
		outfile<<setw(12)<<P(0,0)<<"\t"<<setw(12)<<P(1,0)<<"\t"<<setw(12)<<P(2,0)<<"\t";
		outfile<<setw(12)<<dx<<"\t"<<setw(12)<<dy<<"\t"<<setw(12)<<dz<<"\t";
		outfile.precision(6);
		outfile<<setw(12)<<point.time<<endl;
		outfile.flush();

		outfile_pts.precision(3);
		outfile_pts<<setw(12)<<P(0,0)<<"\t"<<setw(12)<<P(1,0)<<"\t"<<setw(12)<<P(2,0)<<"\t";
		outfile_pts.precision(6);
		outfile_pts<<point_old.time<<"\t";
		outfile_pts<<Rad2Deg(point_old.INS_P)<<"\t"<<Rad2Deg(point_old.INS_O)<<"\t"<<Rad2Deg(point_old.INS_K)<<"\t";
		outfile_pts<<point_old.intensity<<"\t";
		outfile_pts.precision(3);
		outfile_pts<<point_old.GPS_X<<"\t"<<point_old.GPS_Y<<"\t"<<point_old.GPS_Z<<"\t";
		outfile_pts.precision(6);
		outfile_pts<<Rad2Deg(point_old.beta)<<"\t";
		outfile_pts.precision(3);
		outfile_pts<<point_old.dist<<endl;
		outfile_pts.flush();

		count++;
	}

	LidarFile.close();

	double rmseX = sqrt(sumX2/count);
	double rmseY = sqrt(sumY2/count);
	double rmseZ = sqrt(sumZ2/count);
	outfile<<endl<<endl<<endl;
	outfile<<"[RMSE]"<<endl;
	outfile<<setw(12)<<rmseX<<"\t"<<setw(12)<<rmseY<<"\t"<<setw(12)<<rmseZ<<"\t"<<setw(12)<<sqrt(rmseX*rmseX + rmseY*rmseY + rmseZ*rmseZ)<<endl;

	outfile.close();
	outfile_pts.close();
	
	return true;
}

void CSMLIDARCalibration::GetGroundFootPrint(CLIDARCalibrationPoint &point, int EQtype)
{
	CSMMatrix<double> dumy(3,3); dumy.Identity();
	CSMMatrix<double> P;
	
	switch(EQtype)
	{
	case 0://Terrapoint
		P = LIDAREQ_Point_BasicEQ(point, Config, dumy);
		break;
	case 1://Terrapoint(scan_angle + 90deg)
		P = LIDAREQ_Point_BasicEQ(point, Config, dumy);
		break;
	case 2://UofC
		{
			_Rmat_ Rdumy(0, Deg2Rad(180), 0); 
			dumy = Rdumy.Rmatrix;
			P = LIDAREQ_Point_BasicEQ(point, Config, dumy);
		}
		break;
	case 3://etc
		{
			_Rmat_ Rdumy(0, Deg2Rad(0), 0); 
			dumy = Rdumy.Rmatrix;
			P = LIDAREQ_Point_BasicEQ(point, Config, dumy);
		}
		break;
	default:
		AfxMessageBox("option fail");
		break;
	}
	
	  point.X = P(0, 0);
	  point.Y = P(1, 0);
	  point.Z = P(2, 0);
}

bool CSMLIDARCalibration::ReadProjectFile_100(fstream &PrjFile, char* delimiter, int num_delimiter, LIDAR_CALIBRATION_PRJ_STRUCT &prj)
{
	const CString Tag_Config = "LIDAR_SYSTEM_CONFIG";
	const CString Tag_Control_Point = "CONTROL_POINT_DATA";
	const CString Tag_Control_Point_Contents = "CONTROL_POINT_DATA_CONTENTS";
	const CString Tag_Control_ICPoint = "CONTROL_PATCH_DATA_ICPOINTS";
	const CString Tag_Control_ICPoint_Contents = "CONTROL_PATCH_DATA_ICPOINTS_CONTENTS";
	const CString Tag_Control_ICPatch = "CONTROL_PATCH_DATA_ICPATCHES";
	const CString Tag_Control_ICPatch_Contents = "CONTROL_PATCH_DATA_ICPATCHES_CONTENTS";
	const CString Tag_EQ_Type = "EQUATION_TYPE";
	const CString Tag_Meridian_Conversion = "MERIDIAN_CONVERSION";
	const CString Tag_Ref_Ellipsoid_Name = "REFERENCE_ELLIPSOID_NAME";
	const CString Tag_Ref_Ellipsoid_Semimajor = "REFERENCE_ELLIPSOID_SEMIMAJOR";
	const CString Tag_Ref_Ellipsoid_Semiminor = "REFERENCE_ELLIPSOID_SEMIMINOR";
	const CString Tag_Ref_Ellipsoid_Flattening = "REFERENCE_ELLIPSOID_FLATTENING";
	const CString Tag_UTM_Zone_Num = "UTM_ZONE_NUMBER";
	const CString Tag_Hemisphere = "NORTH_SOUTH_HEMISPHERE";
	const CString Tag_Result_Path = "RESULT_PATH";
	const CString Tag_Small_Sigma = "SMALL_SIGMA";
	const CString Tag_Large_Sigma = "LARGE_SIGMA";
	const CString Tag_Diff_Sigma = "DIFFERENCE_SIGMA";
	const CString Tag_Max_Iter_Num = "MAX_ITERATION";
	
	char Temp[MAX_LINE_LENGTH];
	CString Tag;

	//Parse project file
	while(!PrjFile.eof())
	{
		RemoveCommentLine(PrjFile, delimiter, num_delimiter);
		PrjFile.getline(Temp, MAX_LINE_LENGTH);
		PrjFile.eatwhite();

		Tag = Temp;

		if (Tag == Tag_Config) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ConfigPath = Temp;}
		else if (Tag == Tag_Control_Point) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPointsPath = Temp;}
		else if (Tag == Tag_Control_Point_Contents) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPointsContentsPath = Temp;}
		else if (Tag == Tag_Control_ICPoint) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPatchICPointPath = Temp;}
		else if (Tag == Tag_Control_ICPoint_Contents) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPatchICPointContentsPath = Temp;}
		else if (Tag == Tag_Control_ICPatch) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPatchICPatchPath = Temp;}
		else if (Tag == Tag_Control_ICPatch_Contents) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPatchICPatchContentsPath = Temp;}
		else if (Tag == Tag_EQ_Type) { PrjFile>>prj.EQType; PrjFile.eatwhite(); }
		else if (Tag == Tag_Meridian_Conversion) { PrjFile>>prj.MC;  PrjFile.eatwhite(); }
		else if (Tag == Tag_Ref_Ellipsoid_Name) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ellipsoid.Name = Temp;}
		else if (Tag == Tag_Ref_Ellipsoid_Semimajor) {PrjFile>>prj.ellipsoid.SemiMajor; PrjFile.eatwhite();}
		else if (Tag == Tag_Ref_Ellipsoid_Semiminor) {PrjFile>>prj.ellipsoid.SemiMinor; PrjFile.eatwhite();}
		else if (Tag == Tag_Ref_Ellipsoid_Flattening) {PrjFile>>prj.ellipsoid.Flattening; PrjFile.eatwhite();}
		else if (Tag == Tag_UTM_Zone_Num) {PrjFile>>prj.ellipsoid.zone; PrjFile.eatwhite();}
		else if (Tag == Tag_Hemisphere) {PrjFile>>prj.ellipsoid.hemi; PrjFile.eatwhite();}
		else if (Tag == Tag_Result_Path) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ResultPath = Temp;}
		else if (Tag == Tag_Small_Sigma) {PrjFile>>prj.Small_Sigma; PrjFile.eatwhite();}
		else if (Tag == Tag_Large_Sigma) {PrjFile>>prj.Large_Sigma; PrjFile.eatwhite();}
		else if (Tag == Tag_Diff_Sigma) {PrjFile>>prj.Diff_Sigma; PrjFile.eatwhite();}
		else if (Tag == Tag_Max_Iter_Num) {PrjFile>>prj.Num_Iter; PrjFile.eatwhite();}
		else {}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Check availability of project file
	if(prj.ConfigPath == "") return false;
	else if(prj.ControlPointsPath == "" && prj.ControlPatchICPointPath == "" && prj.ControlPatchICPatchPath=="") return false;//No control data
	else if(prj.ControlPointsPath != "" && prj.ControlPointsContentsPath == "") return false;
	else if(prj.ControlPatchICPointPath != "" && prj.ControlPatchICPointContentsPath == "") return false;
	else if(prj.ControlPatchICPatchPath != "" && prj.ControlPatchICPatchContentsPath == "") return false;
	else if(prj.EQType < 0 || prj.EQType > 3) return false;

	if((prj.ControlPatchICPointPath != "" && prj.ControlPatchICPointContentsPath != "")) bICPoint = true;
	if((prj.ControlPatchICPatchPath != "" && prj.ControlPatchICPatchContentsPath != "")) bICPatch = true;
	if((prj.ControlPointsPath != "" && prj.ControlPointsContentsPath != "")) bControlPoint = true;
	
	return true;
}

bool CSMLIDARCalibration::ReadProjectFile_110(fstream &PrjFile, char* delimiter, int num_delimiter, LIDAR_CALIBRATION_PRJ_STRUCT &prj)
{
	const CString Tag_Config = "LIDAR_SYSTEM_CONFIG";
	const CString Tag_Control_Point = "CONTROL_POINT_DATA";
	const CString Tag_Control_Point_Contents = "CONTROL_POINT_DATA_CONTENTS";
	const CString Tag_Control_ICPoint = "CONTROL_PATCH_DATA_ICPOINTS";
	const CString Tag_Control_ICPoint_Contents = "CONTROL_PATCH_DATA_ICPOINTS_CONTENTS";
	const CString Tag_Control_ICPatch = "CONTROL_PATCH_DATA_ICPATCHES";
	const CString Tag_Control_ICPatch_Contents = "CONTROL_PATCH_DATA_ICPATCHES_CONTENTS";
	const CString Tag_EQ_Type = "EQUATION_TYPE";
	const CString Tag_Meridian_Conversion = "MERIDIAN_CONVERSION";
	const CString Tag_Ref_Ellipsoid_Name = "REFERENCE_ELLIPSOID_NAME";
	const CString Tag_Ref_Ellipsoid_Semimajor = "REFERENCE_ELLIPSOID_SEMIMAJOR";
	const CString Tag_Ref_Ellipsoid_Semiminor = "REFERENCE_ELLIPSOID_SEMIMINOR";
	const CString Tag_Ref_Ellipsoid_Flattening = "REFERENCE_ELLIPSOID_FLATTENING";
	const CString Tag_UTM_Zone_Num = "UTM_ZONE_NUMBER";
	const CString Tag_Hemisphere = "NORTH_SOUTH_HEMISPHERE";
	const CString Tag_Result_Path = "RESULT_PATH";
	const CString Tag_Small_Sigma = "SMALL_SIGMA";
	const CString Tag_Large_Sigma = "LARGE_SIGMA";
	const CString Tag_Diff_Sigma = "DIFFERENCE_SIGMA";
	const CString Tag_Max_Iter_Num = "MAX_ITERATION";
	
	char Temp[MAX_LINE_LENGTH];
	CString Tag;
	
	//Parse project file
	while(!PrjFile.eof())
	{
		RemoveCommentLine(PrjFile, delimiter, num_delimiter);
		PrjFile.getline(Temp, MAX_LINE_LENGTH);
		PrjFile.eatwhite();
		
		Tag = Temp;
		
		if (Tag == Tag_Config) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ConfigPath = stPrj_Path + Temp;}
		else if (Tag == Tag_Control_Point) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPointsPath = stPrj_Path + Temp;}
		else if (Tag == Tag_Control_Point_Contents) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPointsContentsPath = stPrj_Path + Temp;}
		else if (Tag == Tag_Control_ICPoint) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPatchICPointPath = stPrj_Path + Temp;}
		else if (Tag == Tag_Control_ICPoint_Contents) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPatchICPointContentsPath = stPrj_Path + Temp;}
		else if (Tag == Tag_Control_ICPatch) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPatchICPatchPath = stPrj_Path + Temp;}
		else if (Tag == Tag_Control_ICPatch_Contents) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ControlPatchICPatchContentsPath = stPrj_Path + Temp;}
		else if (Tag == Tag_EQ_Type) { PrjFile>>prj.EQType; PrjFile.eatwhite(); }
		else if (Tag == Tag_Meridian_Conversion) { PrjFile>>prj.MC;  PrjFile.eatwhite(); }
		else if (Tag == Tag_Ref_Ellipsoid_Name) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ellipsoid.Name = Temp;}
		else if (Tag == Tag_Ref_Ellipsoid_Semimajor) {PrjFile>>prj.ellipsoid.SemiMajor; PrjFile.eatwhite();}
		else if (Tag == Tag_Ref_Ellipsoid_Semiminor) {PrjFile>>prj.ellipsoid.SemiMinor; PrjFile.eatwhite();}
		else if (Tag == Tag_Ref_Ellipsoid_Flattening) {PrjFile>>prj.ellipsoid.Flattening; PrjFile.eatwhite();}
		else if (Tag == Tag_UTM_Zone_Num) {PrjFile>>prj.ellipsoid.zone; PrjFile.eatwhite();}
		else if (Tag == Tag_Hemisphere) {PrjFile>>prj.ellipsoid.hemi; PrjFile.eatwhite();}
		else if (Tag == Tag_Result_Path) {PrjFile.getline(Temp, MAX_LINE_LENGTH); PrjFile.eatwhite(); prj.ResultPath = stPrj_Path + Temp;}
		else if (Tag == Tag_Small_Sigma) {PrjFile>>prj.Small_Sigma; PrjFile.eatwhite();}
		else if (Tag == Tag_Large_Sigma) {PrjFile>>prj.Large_Sigma; PrjFile.eatwhite();}
		else if (Tag == Tag_Diff_Sigma) {PrjFile>>prj.Diff_Sigma; PrjFile.eatwhite();}
		else if (Tag == Tag_Max_Iter_Num) {PrjFile>>prj.Num_Iter; PrjFile.eatwhite();}
		else {}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Check availability of project file
	if(prj.ConfigPath == "") return false;
	else if(prj.ControlPointsPath == "" && prj.ControlPatchICPointPath == "" && prj.ControlPatchICPatchPath=="") return false;//No control data
	else if(prj.ControlPointsPath != "" && prj.ControlPointsContentsPath == "") return false;
	else if(prj.ControlPatchICPointPath != "" && prj.ControlPatchICPointContentsPath == "") return false;
	else if(prj.ControlPatchICPatchPath != "" && prj.ControlPatchICPatchContentsPath == "") return false;
	else if(prj.EQType < 0 || prj.EQType > 3) return false;
	
	if((prj.ControlPatchICPointPath != "" && prj.ControlPatchICPointContentsPath != "")) bICPoint = true;
	if((prj.ControlPatchICPatchPath != "" && prj.ControlPatchICPatchContentsPath != "")) bICPatch = true;
	if((prj.ControlPointsPath != "" && prj.ControlPointsContentsPath != "")) bControlPoint = true;
	
	return true;
}

bool CSMLIDARCalibration::ReadProjectFile(CString prjpath, LIDAR_CALIBRATION_PRJ_STRUCT &prj)
{
	fstream PrjFile;
	PrjFile.open(prjpath, ios::in);
	
	//version check
	int pos;
	bool bVersion = FindString(PrjFile, "VER", pos);
	if(bVersion == true)
	{
		//To move to begin of file
		PrjFile.seekg(0,ios::beg);
		char temp[256];//"VER"
		PrjFile>>temp>>prj_ver_num;//version
	}
	else
	{
		//To move to begin of file
		PrjFile.seekg(0,ios::beg);//old version without version number(before ver 1.1)
		prj_ver_num = 1.0;//default version number
	}

	//Delimiter
	int num_delimiter = 1;
	char* delimiter = new char[num_delimiter];
	delimiter[0] = '!';

	//Read ver 1.0 project file
	if( prj_ver_num >= 1.1)
	{
		if(stPrj_Path == CString(""))
			return false;

		return ReadProjectFile_110(PrjFile, delimiter, num_delimiter, prj);
	}
	else
		return ReadProjectFile_100(PrjFile, delimiter, num_delimiter, prj);
	
}

bool CSMLIDARCalibration::ReadICPatch(LIDAR_CALIBRATION_PRJ_STRUCT &prj, CSMList<CTriangle> &TIN, double &norm_dist)
{
	bool bMeridianConversion;
	
	if(prj.MC == 1) bMeridianConversion = true;
	else bMeridianConversion = false;
	
	//Input: ICPoint data file
	fstream ICPatchFile;
	ICPatchFile.open(prj.ControlPatchICPatchPath, ios::in);
	
	CSMList<int> index;	
	if(false == ReadIndexFile(prj.ControlPatchICPatchContentsPath, index)) 
	{
		AfxMessageBox("Error: ReadIndexFile (ICPatch)");
		return false;
	}
	
	CString Contents_dat="";
	char line[MAX_LINE_LENGTH];
	
	while(!ICPatchFile.eof())
	{
		RemoveCommentLine(ICPatchFile, '!');
		ICPatchFile>>norm_dist;//distance threshold for ICPatch
		ICPatchFile.eatwhite();
		////////////////////////////////////////////////////////////////////////////////////////
		//Read target surface
		//
		
		char letter = ICPatchFile.peek();
		if(letter != 'T') return false;
		
		ICPatchFile.getline(line, MAX_LINE_LENGTH);//TARGET_SURF
		ICPatchFile.eatwhite();
		
		int count = 0;
		
		while('R' != ICPatchFile.peek())
		{
			RemoveCommentLine(ICPatchFile, '!');
			
			CLIDARCalibrationPoint point, point_old;
			
			if(false == ReadRecord(ICPatchFile, index, point, point_old, prj.EQType, bMeridianConversion, prj.ellipsoid)) return false;
			
			GetGroundFootPrint(point, prj.EQType);
			
			All_ICPatch_PointList.AddTail(point);
			
			count ++;
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Read reference surface
		//
		
		ICPatchFile.getline(line, MAX_LINE_LENGTH);//REFERENCE_SURF
		ICPatchFile.eatwhite();
		
		CTriangle tri;
		
		TIN.RemoveAll();
		
		while('E' != ICPatchFile.peek())
		{
			RemoveCommentLine(ICPatchFile, '!');
			
			ICPatchFile >> tri.ID; 
			ICPatchFile >> tri.vertex1[0] >> tri.vertex1[1] >> tri.vertex1[2];
			ICPatchFile >> tri.vertex2[0] >> tri.vertex2[1] >> tri.vertex2[2];
			ICPatchFile >> tri.vertex3[0] >> tri.vertex3[1] >> tri.vertex3[2];

			ICPatchFile.eatwhite();
			
			TIN.AddTail(tri);
		}
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//End of surface data set
		ICPatchFile.getline(line, MAX_LINE_LENGTH);//END_SURF
		ICPatchFile.eatwhite();
	}
	
	ICPatchFile.close();
	
	return true;

}