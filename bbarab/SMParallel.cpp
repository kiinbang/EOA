// SMParallel.cpp: implementation of the CParallel class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SMParallel.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define PI 3.141592654

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSMParallel::CSMParallel()
{
	Init();
}

CSMParallel::~CSMParallel()
{

}

CString CSMParallel::DoModeling(unsigned int num_max_iter, double maximum_sd, double maximum_diff, CString outfilename, double pixelsize)
{
	unsigned int num_param = 8;

	CString retval;

	retval = "\r\n\r\n[Parallel Projection Modeling]\r\n";
	retval += "x = A1X + A2Y + A3Z + A4\r\n";
	retval += "y = (A5X + A6Y + A7Z + A8)\r\n";
		
	if(bNormalized == true) retval += "(***Data is normalized***)\r\n";
	
	/**
	*Calculate parameter approximation
	*/
	retval += "\r\n\r\n[Init_value of Parameters]\r\n";
	retval += "///////////////////////////////////////////////////////////////////\r\n";
	///////////////////////////////
	//Original Data backup
	///////////////////////////////
	retval += DoInitModeling(num_param);
	///////////////////////////////////////
	//Initial data copy for the iteration
	///////////////////////////////////////
	twin_SMData = SMData;
	///////////////////////////////////////
	//eachiterout += "[parallel projection]\r\n";
	//eachiterout += "x = A1X + A2Y + A3Z + A4\r\n";
	//eachiterout += "y = A5X + A6Y + A7Z + A8\r\n\r\n";
	//eachiterout += "\r\n\r\n[End of Init_value Calculation]\r\n";
	retval += "///////////////////////////////////////////////////////////////////\r\n";

	FileOut(outfilename,retval);
	
	retval += Run(num_param, num_max_iter, maximum_sd, maximum_diff, outfilename, pixelsize);
	
	::Beep(500,50);
	return retval;
}

CString CSMParallel::DoInitModeling(unsigned int num_param)
{
	CString retval;

	retval="[Parallel Projection Init_Params (BA)]\r\n";
	retval="[Approximate the parameters using Parallel Projection(A1~A8)]\r\n";
	
	retval += "[parallel projection]\r\n";
	retval += "x = A1X + A2Y + A3Z + A4\r\n";
	retval += "y = A5X + A6Y + A7Z + A8\r\n\r\n";
	
	/*********************************
	* Data Normalization
	*********************************/
	
	unsigned int i, j, k;
	
	if(bNormalized == true) 
	{
		retval += "(***Data is normalized***)\r\n";
		DataNormalization();	
	}
	else 
	{		
		for(i=0; i<SMData.SCENE_List.GetNumItem(); i++)
		{
			SMParam temp;
			param_list.AddTail(temp);
		}
	} 
	
	//**************************************
	//x = A1X + A2Y + A3Z + A4
	//y = A5X + A6Y + A7Z + A8
	//**************************************
	//Make A and L matrix
	//Matrix Group
	
	unsigned int SCENE_Num = SMData.SCENE_List.GetNumItem();
	
	
	CSMList<_GCP_> &gcp = SMData.GCP_List;
	unsigned int GCP_Num = gcp.GetNumItem();
	
	unsigned int cur_rows;
	unsigned int GP_position;
	
	Matrix<double> Amat, Lmat, Wmat;
	Amat.Resize(0,0,0.);
	Lmat.Resize(0,0,0.);
	Wmat.Resize(0,0,0.);
	
	Matrix<double> a, l, w;
	
	for(k=0; k<SCENE_Num; k++)
	{
		Matrix<double> a1, a2, l, w;
		
		_SCENE_ scene = SMData.SCENE_List[k];
		
		CSMList<_ICP_> &icp = scene.ICP_List;
		unsigned int ICP_Num = icp.GetNumItem();
		
		SMParam _param_ = param_list[k];
		
		//**************************************
		//Point Data
		//**************************************
		if(GCP_Num != 0)
		{
			for(i=0; i<GCP_Num; i++)
			{
				l.Resize(2,1,0.);
				a1.Resize(2,num_param,0.);
				a2.Resize(2,3,0.);
				w.Resize(2,2,0.);
				
				_GCP_ GP = gcp.GetAt(i);
				
				double s_sum = 0.;
				for(unsigned int index=0; index<9; index++)
					s_sum += GP.s[index];
				if((s_sum/3)>GCP_sd_threshold)
				{
					continue;
				}
				
				for(j=0; j<ICP_Num; j++)
				{
					_ICP_ IP = icp.GetAt(j);
					
					if(IP.PID == GP.PID)
					{
						GP_position = i;
						
						l(0,0) = IP.row;
						l(1,0) = IP.col;
						
						//u(row)
						a1(0,0) = GP.X;
						a1(0,1) = GP.Y;
						a1(0,2) = GP.Z;
						a1(0,3) = 1.;
						//v(col)
						a1(1,4) = GP.X;
						a1(1,5) = GP.Y;
						a1(1,6) = GP.Z;
						a1(1,7) = 1.;
						//X,Y,Z
						a2(0,0) = _param_.A[0];
						a2(0,1) = _param_.A[1];
						a2(0,2) = _param_.A[2];
						
						a2(1,0) = _param_.A[0];
						a2(1,1) = _param_.A[0];
						a2(0,2) = _param_.A[0];
						
						//weight
						w(0,0) = 1./IP.s[0]; //s(1,1)
						//w(0,1) = 1./IP.s[1]; //s(1,2)
						//w(1,0) = 1./IP.s[2]; //s(2,1)
						w(1,1) = 1./IP.s[3]; //s(2,2)
						
						cur_rows = Amat.GetRows();
						GP_position = SCENE_Num*num_param + GP_position*3;
						Amat.Insert(cur_rows,k*num_param,a1);
						//
						//일단...프로토타입에서는 초기값 설정할때 지상점은 계산에서 제외한다.
						//Matlab 데이터 파일에 지상점에 대한 초기값이 입력이 되고 있고...
						//간단하게 작성한 후 테스트를 거쳐서, 지상점에 대한 부분을 보완하는것은 추후로 미룬다.
						//Amat.Insert(cur_rows,GP_position,a2);
						//
						Lmat.Insert(cur_rows,0,l);
						Wmat.Insert(cur_rows,cur_rows,w);
						
						break;
					} //if(IP.PID == GP.PID)
				} //for(j=0; j<ICP_Num; j++)
				
			} //for(i=0; i<GCP_Num; i++)
		} //if(GCP_Num != 0)
		
		//
		//Linear Feature(ICL) Data
		//We can get the two eqs.(constraints) from one ICL data
		//(Xi,Yi,Zi) and (Xj,Yj,Zj) are the ground coord of ground control line
		//(ROWm,COLm) and (ROWn,COLn) are the image coord of linear feature on the image
		//[constraint 1]
		//k(A1(Xi-Xj)+A2(Yi-Yj)+A3(Zi-Zj))
		//-(A5(Xi-Xj)+A6(Yi-Yj)+A7(Zi-Zj))=0
		//where, k=(COLn-COLn)/(ROWn-ROWm)
		//[constraint 2]
		//k'-k(A1Xi+A2Yi+A3Zi+A4)-(A5Xi+A6Yi+A7Zi+A8)=0
		//where, k'=(COLn-COLn)/(ROWn-ROWm)*(-ROWm)+COLm
		//k'=k*(-ROWm)+COLm
		//
		
		CSMList<_GCL_> &gcl = SMData.GCL_List;
		unsigned int GCL_Num = gcl.GetNumItem();
		_GCL_ GL1, GL2;
		
		CSMList<_ICL_> &icl = scene.ICL_List;
		unsigned int ICL_Num = icl.GetNumItem();
		
		_ICL_ IL1, IL2;
		
		if(GCL_Num != 0)
		{
			a1.Resize(2,num_param,0.);
			l.Resize(2,1,0.);
			w.Resize(2,2,0.);
			
			for(i=0; i<GCL_Num; i++)
			{
				int data_check = 0;
				if(gcl[i].flag == "A")
				{
					GL1 = gcl[i];
					
					double s_sum = 0.;
					for(unsigned int index=0; index<9; index++)
						s_sum += GL1.s[index];
					
					if((s_sum/3)>GCP_sd_threshold)
					{
						continue;
					}					
					
					for(j=0; j<GCL_Num; j++)
					{
						if((GL1.LID==gcl[j].LID)&&(gcl[j].flag == "B"))
						{
							GL2 = gcl[j];
							
							double s_sum = 0.;
							for(unsigned int index=0; index<9; index++)
								s_sum += GL2.s[index];
							
							if((s_sum/3)>GCP_sd_threshold)
							{
								continue;
							}	
							data_check++; //data_check == 1
							break;
						} //if((GL1.LID==gcl[j].LID)&&(gcl[j].flag == "B"))
					} //for(j=i+1; j<GCL_Num; j++)
				} //if(gcl[i].flag == "A")
				
				if(data_check == 1)
				{
					for(j=0; j<ICL_Num; j++)
					{
						if(GL1.LID == icl[j].LID)
						{
							_ICL_ temp = icl[j];
							//if("C" == icl[j].flag)<-A,B도 수용하여 계산
							{
								IL1 = icl[j];
								data_check++;//data_check == 2
								for(unsigned int m=j+1; m<ICL_Num; m++)
								{
									if(GL1.LID == icl[m].LID)
									{
										//if("C" == icl[m].flag)<-A,B도 수용하여 계산
										{
											IL2 = icl[m];
											data_check++; //data_check == 3
											break;
										}
									}
									
								}
							}
							
							if(data_check == 3)
								break;
						} //if(IL1.LID == gcl[j].LID)
					} //for(j=0; j<GCL_Num; j++)
				} //if(data_check == 1)				
				
				if(data_check == 3)
				{					
				/**
				*k=(v2-v1)/(u2-u1)<-----------------------------------------------------(eq1)
				*0=k(A1(X2-X1)+A2(Y2-Y1)+A3(Z2-Z1))-(A5(X2-X1)+A6(Y2-Y1)+A7(Z2-Z1))<----(eq2)
					*/
					double k1 = (IL2.col-IL1.col)/(IL2.row-IL1.row);
					
					l(0,0) = 0;
					
					a1(0,0) = k1*(GL2.X-GL1.X);
					a1(0,1) = k1*(GL2.Y-GL1.Y);
					a1(0,2) = k1*(GL2.Z-GL1.Z);
					a1(0,3) = 0.;
					a1(0,4) = -(GL2.X-GL1.X);
					a1(0,5) = -(GL2.Y-GL1.Y);
					a1(0,6) = -(GL2.Z-GL1.Z);
					a1(0,7) = 0.;
					
					/**
					*k2=k1(-u1)+v1
					*k2=k1(A1(X2)+A2(Y2)+A3(Z2)+A4)-(A5(X2)+A6(Y2)+A7(Z2)+A8)
					*/
					double k2 = k1*(-IL1.row)+IL1.col;
					
					l(1,0) = k2;
					
					a1(1,0) = -k1*GL1.X;
					a1(1,1) = -k1*GL1.Y;
					a1(1,2) = -k1*GL1.Z;
					a1(1,3) = -k1;
					a1(1,4) = GL1.X;
					a1(1,5) = GL1.Y;
					a1(1,6) = GL1.Z;
					a1(1,7) = 1.;
					
					//weight(equavalent weignt로 수정해야함.)
					w(0,0) = ((1./IL1.s[0])+(1./IL2.s[0]))/2; //s(1,1)
					w(1,1) = ((1./IL1.s[3])+(1./IL2.s[3]))/2; //s(2,2)
					
					cur_rows = Amat.GetRows();
					Amat.Insert(cur_rows,k*num_param,a1);
					Lmat.Insert(cur_rows,0,l);
					Wmat.Insert(cur_rows,cur_rows,w);
					
				}//if(data_check == 2)
				} //for(i=0; i<GCL_Num; i++)
			} //if(GCL_Num != 0)
			
		} //for(k=0; k<SCENE_Num; k++)
		
		CLeastSquare LS;
		RETMAT res;
		
		res = LS.RunLeastSquare(Amat, Lmat);
		
		CString stParam;
		stParam.Format("[--INITIAL VALUE--]\r\n");
		//Unknown Matrix
		for(i=0; i<SCENE_Num; i++)
		{
			stParam.Format("---Scene\t[%d]---\r\n",i);
			retval += stParam;
			for(j=0; j<num_param; j++)
			{
				double value = res.X(i*num_param+j);
				param_list[i].A[j] = value;
			}
			
			stParam.Format("[A1]%le\t[A2]%le\t[A3]%le\t[A4]%le\r\n",param_list[i].A[0],param_list[i].A[1],param_list[i].A[2],param_list[i].A[3]);
			retval += stParam;
			stParam.Format("[A5]%le\t[A6]%le\t[A7]%le\t[A8]%le\r\n",param_list[i].A[4],param_list[i].A[5],param_list[i].A[6],param_list[i].A[7]);
			retval += stParam;
		}
		
	
	return retval;
}

/**
*Calculating initial value (Fo) using approximate parameters.
*/
double CSMParallel::Func_row(_GCP_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double row = param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3];
	return row;
}
double CSMParallel::Func_row(_GCL_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double row = param.A[0]*G.X + param.A[1]*G.Y + param.A[2]*G.Z + param.A[3];
	return row;
}
/**
*Calculating initial value (Go) using approximate parameters.
*/
double CSMParallel::Func_col(_GCP_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double col = param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7];
	return col;
}
double CSMParallel::Func_col(_GCL_ G, unsigned int index)
{
	SMParam param = param_list[index];
	double col = param.A[4]*G.X + param.A[5]*G.Y + param.A[6]*G.Z + param.A[7];
	return col;
}

void CSMParallel::Cal_PDs(_GCP_ G, double* ret, unsigned int index)
{
	SMParam param = param_list[index];

	//A1~A8, alpha
	ret[0] = G.X;
	ret[1] = G.Y;
	ret[2] = G.Z;
	ret[3] = 1.;
	ret[4] = G.X;
	ret[5] = G.Y;
	ret[6] = G.Z;
	ret[7] = 1.;

	//X,Y,Z
	ret[8]  = param.A[0];
	ret[9]  = param.A[1];
	ret[10] = param.A[2];

	ret[11] = param.A[4];
	ret[12] = param.A[5];
	ret[13] = param.A[6];
}

void CSMParallel::Cal_PDs(_GCP_ G, double* ret1, double* ret2, unsigned int index)
{
	SMParam param = param_list[index];

	//A1~A4
	ret1[0] = G.X;
	ret1[1] = G.Y;
	ret1[2] = G.Z;
	ret1[3] = 1.;
	//A5~A8
	ret1[4] = 0.;
	ret1[5] = 0.;
	ret1[6] = 0.;
	ret1[7] = 0.;
	//X,Y,Z(row)
	ret1[8]  = param.A[0];
	ret1[9]  = param.A[1];
	ret1[10] = param.A[2];

	//A1~A4
	ret2[0] = 0.;
	ret2[1] = 0.;
	ret2[2] = 0.;
	ret2[3] = 0.;
	//A5~A8
	ret2[4] = G.X;
	ret2[5] = G.Y;
	ret2[6] = G.Z;
	ret2[7] = 1.;
	//X,Y,Z(col)
	ret2[8]  = param.A[4];
	ret2[9]  = param.A[5];
	ret2[10] = param.A[6];
}

void CSMParallel::Cal_PDs_Line(_GCL_ GL1, _GCL_ GL2, _ICL_ IL, double* ret, unsigned int scene_index, unsigned int num_param)
{
	SMParam param = param_list[scene_index];

	_ICP_ icp1, icp2;
	_GCP_ gcp1, gcp2;
	
	gcp1.X = GL1.X;
	gcp1.Y = GL1.Y;
	gcp1.Z = GL1.Z;
	
	gcp2.X = GL2.X;
	gcp2.Y = GL2.Y;
	gcp2.Z = GL2.Z;
	
	icp1.col = Func_col(gcp1, scene_index);
	icp1.row = Func_row(gcp1, scene_index);
	
	icp2.col = Func_col(gcp2, scene_index);
	icp2.row = Func_row(gcp2, scene_index);
	
	//(u,v) = (row,col)
	double v_v2 = IL.col-icp2.col;
	double v_v1 = IL.col-icp1.col;
	ret[0] = gcp1.X*v_v2 - gcp2.X*v_v1;
	ret[1] = gcp1.Y*v_v2 - gcp2.Y*v_v1;
	ret[2] = gcp1.Z*v_v2 - gcp2.Z*v_v1;
	ret[3] = -(icp2.col-icp1.col);
	double u_u2 = IL.row-icp2.row;
	double u_u1 = IL.row-icp1.row;
	ret[4] = -gcp1.X*u_u2 + gcp2.X*u_u1;
	ret[5] = -gcp1.Y*u_u2 + gcp2.Y*u_u1;
	ret[6] = -gcp1.Z*u_u2 + gcp2.Z*u_u1;
	ret[7] = (icp2.row-icp1.row);

	//X1,Y1,Z1, X2,Y2,Z2
	//double* ret1 = new double[num_param+6];
	//double* ret2 = new double[num_param+6];
	//Cal_PDs(gcp1, ret1, scene_index);
	//Cal_PDs(gcp2, ret2, scene_index);
	ret[8]  = v_v2*param.A[0]-u_u2*param.A[4];
	ret[9]  = v_v2*param.A[1]-u_u2*param.A[5];
	ret[10] = v_v2*param.A[2]-u_u2*param.A[6];

	ret[11] = u_u1*param.A[4]-v_v1*param.A[0];
	ret[12] = u_u1*param.A[5]-v_v1*param.A[1];
	ret[13] = u_u1*param.A[6]-v_v1*param.A[2];
}

SMParam CSMParallel::ParamDenormalization(SMParam param, unsigned int num_param)
{
	//double* Nparam = SMData.DataNoramlization();
	//SHIFT_X[0], SCALE_X[1], SHIFT_Y[2], SCALE_Y[3], SHIFT_Z[4], SCALE_Z[5], SHIFT_col[6], SCALE_col[7], SHIFT_row[8], SCALE_row[9]
	
	double* A = new double[num_param];
	for(unsigned int i=0; i<num_param; i++) A[i] = param.A[i];
	
	param.A[0] = A[0]*param.SCALE_X/param.SCALE_row;
	param.A[1] = A[1]*param.SCALE_Y/param.SCALE_row;
	param.A[2] = A[2]*param.SCALE_Z/param.SCALE_row;
	param.A[3] = ((A[3] 
		+ A[0]*param.SHIFT_X*param.SCALE_X 
		+ A[1]*param.SHIFT_Y*param.SCALE_Y 
		+ A[2]*param.SHIFT_Z*param.SCALE_Z)/param.SCALE_row) 
		- param.SHIFT_row;
	param.A[4] = A[4]*param.SCALE_X/param.SCALE_col;
	param.A[5] = A[5]*param.SCALE_Y/param.SCALE_col;
	param.A[6] = A[6]*param.SCALE_Z/param.SCALE_col;
	param.A[7] = ((A[7] 
		+ A[4]*param.SHIFT_X*param.SCALE_X 
		+ A[5]*param.SHIFT_Y*param.SCALE_Y 
		+ A[6]*param.SHIFT_Z*param.SCALE_Z)/param.SCALE_col)
		- param.SHIFT_col;
	
	param.SHIFT_X = 0;
	param.SCALE_X = 1;
	param.SHIFT_Y = 0;
	param.SCALE_Y = 1;
	param.SHIFT_Z = 0;
	param.SCALE_Z = 1;
	param.SHIFT_col = 0;
	param.SCALE_col = 1;
	param.SHIFT_row = 0;
	param.SCALE_row = 1;

	return param;
}