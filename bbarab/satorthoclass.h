/*
 * Copyright (c) 2002-2006, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// satorthoclass.h: interface for the CSatOrthoClass class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SATORTHOCLASS_H__6F4D7B52_7EA5_4BCC_8266_040837BD319B__INCLUDED_)
#define AFX_SATORTHOCLASS_H__6F4D7B52_7EA5_4BCC_8266_040837BD319B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <SMGrid.h>
#include <SMCoordinateClass.h>
#include <Collinearity.h>
#include <SMList.h>

#define FIRST_RECORD -999
#define BOUNDARY_MARGIN 100.0
#define _MAXDIFFROW_ 0.5
#define DEFAULT_EMPTY_DEM -99999.999

class CSatOrthoClass
{
	typedef class __EOPofOrientationImage__
	{
public:
	double n_line;
	double O, P, K;
	double X, Y, Z;
	} OIEOP;

	typedef class __EOPofScene__
	{
public:
	int n_Order;
	CSMList<double> O;
	CSMList<double> P;
	CSMList<double> K;
	CSMList<double> X;
	CSMList<double> Y;
	CSMList<double> Z;
	} SEOP;

friend class CSatOrthoImage;

public:
	CSatOrthoClass();
	~CSatOrthoClass();

	
	void SetFailedDEM(double failed, double backval) {faileddem = failed; backdem = backval;}

protected:

	bool FindEOP(double row, double &O, double &P, double &K, double &X, double &Y, double &Z);
	bool CalSInterval(double &SInterval, double line_num, double col_num);
	
	/**GroundCoord2SceneCoord
	* Description	    : To transform ground coordinates to scene coordinates
	*@param double X: ground X coordinate
	*@param double Y: ground Y coordinate
	*@param double Z: ground Z coordinate
	*@param double &Xs: perspective center coordinates(Xt) at certain line
	*@param double &Ys: perspective center coordinates(Yt) at certain line
	*@param double &Zs: perspective center coordinates(Zt) at certain line
	*@param double &row: image coordinate (row)
	*@param double &col: image coordinate (col)
	*@param double init_r: Initial approximate line number
	*@return bool 
	*/
	bool GroundCoord2SceneCoord(double X, double Y, double Z, double &Xs, double &Ys, double &Zs, double &row, double &col, double init_r);
	
	/**CheckOcclusion_DiffZ
	* Description	    : to estimate occlusion area 
	*@param double Xl : perspective center coordinates(Xo)
	*@param double Yl : perspective center coordinates(Yo)
	*@param double Zl : perspective center coordinates(Zo)
	*@param int indexX : DEM index (for X direction)
	*@param int indexY : DEM index (for Y direction)
	*@return bool 
	*/
	bool CheckOcclusion_DiffZ(double Xl, double Yl, double Zl, int indexX, int indexY);

	/**CheckOcclusion_Angle
	* Description	    : to estimate occlusion area 
	*@param double Xl : perspective center coordinates(Xo)
	*@param double Yl : perspective center coordinates(Yo)
	*@param double Zl : perspective center coordinates(Zo)
	*@param int indexX : DEM index (for X direction)
	*@param int indexY : DEM index (for Y direction)
	*@return bool 
	*/
	bool CheckOcclusion_Angle(double Xl, double Yl, double Zl, int indexX, int indexY);

	/**AnalysisIndex
	* Description	    : to find image row/col coordinates and distance from index map
	*@param int w : width of index map
	*@param double scale : scale for calculating distance
	*@param double index : value from index map
	*@param int &row : row coordinate
	*@param int &col : col coordinate
	*@param double &dist : distance
	*@return void 
	*/
	void AnalysisIndex(int w, double scale, double index, int &row, int &col, double &dist);
	
	
	/**MakeIndex
	* Description	    : to calculate index value for the index map
	*@param int w : width of index map
	*@param double scale : scale for calculating distance
	*@param double &index : value from index map
	*@param int row : row coordinate
	*@param int col : col coordinate
	*@param double dist : distance
	*@return void 
	*/
	void MakeIndex(int w, double scale, double &index, int row, int col, double dist);

	/**GetTargetBoundary
	* Description	    : to read EO parameters
	*
	*image boundary for making z-buffer and index buffer
	*to reduce the needed memory, full image size is not effective.
	*For that reason, to calculate boundary is required.
	*BOUNDARY_MARGIN is required because a image(scene) has relief displacement
	*
	*@param unsigned int &h: height of image
	*@param unsigned int &w: width of image
	*@param unsigned int &sr: starting row index of image patch
	*@param unsigned int &sc: starting column index of image patch
	*@param unsigned int &er: ending row index of image patch
	*@param unsigned int &ec: ending column index of image patch
	*@return void 
	*/

protected:
	
	CSMGridDEM<double>* m_DEM;/**<DEM */
	CSMGrid<BYTE> *m_OriginImage;
	CSMGrid<BYTE> OrthoImage;
	
	double MinZ;
	double MaxZ;

	double m_CCDSize;/*<CCD pixel size. It affect the scale of other IO parameters. If its unit is mm, focal length and xp, yp and d have a unit of mm.*/
	double m_xp, m_yp, m_d;/*<xp, yp and d(scan line offset value for the three line scanners*/
	
	double IOParam[3];/**<IO parameters double[3]: width,height,c */
	
	double number_ver;/*version number of input file*/
	double EOParam[12];/**<EO parameters double[12]: Xo,K1,Yo,K2,...*/
	
	CSMList<OIEOP> m_OIEOP;/**<EO parameters for orientation images*/
	SEOP m_SEOP;/**<EO parameters with time function*/

	int m_nEOPOption; /*0: orientation images	1: eo parameters with time functions	2: frame image*/
	int num_OI;//number of orientation images
	
	DWORD ElapsedTime;

	double faileddem;//failed value
	double backdem;//back
};

#endif // !defined(AFX_SATORTHOCLASS_H__6F4D7B52_7EA5_4BCC_8266_040837BD319B__INCLUDED_)
