/*
 * Copyright (c) 1999-2007, KI IN Bang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// SSMaticsBundle.h: interface for the CSSMaticsBundle class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SSMATICSBUNDLE_H__0994D60F_8D16_4131_9D31_EEBEA4DF7250__INCLUDED_)
#define AFX_SSMATICSBUNDLE_H__0994D60F_8D16_4131_9D31_EEBEA4DF7250__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SMList.h"
#include "SSMPhoto.h"
#include "SSMCamera.h"
#include "UtilityGrocery.h"
#include "LeastSquare.h"
#include "Collinearity.h"
#include <fstream.h>

#define MAXLINE 512

class CSSMaticsBundle
{
public:
	CSSMaticsBundle();
	virtual ~CSSMaticsBundle();
	void Init();
	bool ReadPrjFile(char* stPrjFile);
	bool Solve(char* stReportFile);
private:
	double m_MinVar;
	double m_MaxVar;
	double m_Threshold_S;
	int max_iteration;
	CSMList<CSSMCamera> m_Camera, m_CopyCamera;
	CSMList<CSSMPhoto> m_Photo, m_CopyPhoto;
	CSMList<CSSMPoint> m_GCPs, m_CopyGCPs;
private:
	bool ReadCamera(char* stCameraFile);
	bool ReadPhoto(char* stPhotoFile);
	bool ReadGCP(char* stGCPFile);
	bool Approximation() {/*Not implemented*/}
	bool Make_b_J_K(CSSMPhoto photo, CSSMCamera camera, CSSMPoint GP, CSSMPoint p, double** b, double &J, double &K);
	void GetCameraDistortion(CSSMCamera camera, CSSMPoint IP, CSSMPoint &Dist);
	void CollinearityEQ(CSSMPhoto photo, CSSMCamera camera, CSSMPoint GP, CSSMPoint &IP);
	bool FindGCP(char* ID, CSSMPoint &G, CSSMPoint &copyG, int &index);
	int CheckImagePoint(char* ID, CSSMPhoto photo);
	bool FindCamera(char* ID, CSSMCamera &Camera, CSSMCamera &copyCamera);
	bool DataCheck(fstream &report);
	void FindFixedEOP(CSSMPhoto photo, CSMMatrix<double> &FixedEOP, CSMMatrix<double> &FreeEOP);
	void FindFixedPoint(CSSMPoint point, CSMMatrix<double> &FixedPoint, CSMMatrix<double> &FreePoint);
	void ParameterUpdate(CSMMatrix<double> X);
	void MakeReport(fstream &report, CSMMatrix<double> X, double sigma, int iteration);
};

#endif // !defined(AFX_SSMATICSBUNDLE_H__0994D60F_8D16_4131_9D31_EEBEA4DF7250__INCLUDED_)
