#include "pch.h"

#include "../ViewSimulation/ObjRender.h"

#include <freeformsurf.h>
#include <ssm/include/SsmFrameCamera.h>
#include <ssm/include/SSMSMACModel.h>

const double deg2rad = 3.14159265358979 / 180.0;
const int subSample = 4;

#define castint(a) static_cast<int>(a)

TEST(ObjRender, test3DDamage)
{
	///
	/// 미호천 damage 2D --> 3D teszt
	///
	

	//return;

	char strBuff[_MAX_PATH];
	const char* varName = "RFIMData";
	size_t len;
	getenv_s(&len, strBuff, 80, varName);
	std::string rfimFolder = strBuff;

	std::vector<std::string> paths(43);
	/// obj files
	{
		paths[0] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-01\\BR-D-00008-P-01-01.obj";
		paths[1] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-02\\BR-D-00008-P-02-01.obj";
		paths[2] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-03\\BR-D-00008-P-03-01.obj";
		paths[3] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-04\\BR-D-00008-P-04-01.obj";
		paths[4] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-05\\BR-D-00008-P-05-01.obj";
		paths[5] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-06\\BR-D-00008-P-06-01.obj";
		paths[6] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-07\\BR-D-00008-P-07-01.obj";
		paths[7] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-08\\BR-D-00008-P-08-01.obj";
		paths[8] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-09\\BR-D-00008-P-09-01.obj";
		paths[9] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-10\\BR-D-00008-P-10-01.obj";
		paths[10] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-11\\BR-D-00008-P-11-01.obj";
		paths[11] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-12\\BR-D-00008-P-12-01.obj";
		paths[12] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-13\\BR-D-00008-P-13-01.obj";
		paths[13] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-14\\BR-D-00008-P-14-01.obj";
		paths[14] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-15\\BR-D-00008-P-15-01.obj";
		paths[15] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-16\\BR-D-00008-P-16-01.obj";
		paths[16] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-17\\BR-D-00008-P-17-01.obj";
		paths[17] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-18\\BR-D-00008-P-18-01.obj";
		paths[18] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-19\\BR-D-00008-P-19-01.obj";
		paths[19] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-20\\BR-D-00008-P-20-01.obj";
		paths[20] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-21\\BR-D-00008-P-21-01.obj";

		paths[21] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-01\\BR-D-00008-S-01-01.obj";
		paths[22] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-02\\BR-D-00008-S-02-01.obj";
		paths[23] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-03\\BR-D-00008-S-03-01.obj";
		paths[24] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-04\\BR-D-00008-S-04-01.obj";
		paths[25] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-05\\BR-D-00008-S-05-01.obj";
		paths[26] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-06\\BR-D-00008-S-06-01.obj";
		paths[27] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-07\\BR-D-00008-S-07-01.obj";
		paths[28] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-08\\BR-D-00008-S-08-01.obj";
		paths[29] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-09\\BR-D-00008-S-09-01.obj";
		paths[30] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-10\\BR-D-00008-S-10-01.obj";
		paths[31] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-11\\BR-D-00008-S-11-01.obj";
		paths[32] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-12\\BR-D-00008-S-12-01.obj";
		paths[33] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-13\\BR-D-00008-S-13-01.obj";
		paths[34] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-14\\BR-D-00008-S-14-01.obj";
		paths[35] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-15\\BR-D-00008-S-15-01.obj";
		paths[36] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-16\\BR-D-00008-S-16-01.obj";
		paths[37] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-17\\BR-D-00008-S-17-01.obj";
		paths[38] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-18\\BR-D-00008-S-18-01.obj";
		paths[39] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-19\\BR-D-00008-S-19-01.obj";
		paths[40] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-20\\BR-D-00008-S-20-01.obj";
		paths[41] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-21\\BR-D-00008-S-21-01.obj";
		paths[42] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-22\\BR-D-00008-S-22-01.obj";
	}

	std::vector<std::string> metas(43);
	/// meta.gxxml files
	{
		metas[0] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-01\\meta.gxxml";
		metas[1] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-02\\meta.gxxml";
		metas[2] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-03\\meta.gxxml";
		metas[3] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-04\\meta.gxxml";
		metas[4] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-05\\meta.gxxml";
		metas[5] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-06\\meta.gxxml";
		metas[6] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-07\\meta.gxxml";
		metas[7] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-08\\meta.gxxml";
		metas[8] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-09\\meta.gxxml";
		metas[9] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-10\\meta.gxxml";
		metas[10] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-11\\meta.gxxml";
		metas[11] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-12\\meta.gxxml";
		metas[12] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-13\\meta.gxxml";
		metas[13] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-14\\meta.gxxml";
		metas[14] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-15\\meta.gxxml";
		metas[15] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-16\\meta.gxxml";
		metas[16] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-17\\meta.gxxml";
		metas[17] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-18\\meta.gxxml";
		metas[18] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-19\\meta.gxxml";
		metas[19] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-20\\meta.gxxml";
		metas[20] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-21\\meta.gxxml";

		metas[21] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-01\\meta.gxxml";
		metas[22] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-02\\meta.gxxml";
		metas[23] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-03\\meta.gxxml";
		metas[24] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-04\\meta.gxxml";
		metas[25] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-05\\meta.gxxml";
		metas[26] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-06\\meta.gxxml";
		metas[27] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-07\\meta.gxxml";
		metas[28] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-08\\meta.gxxml";
		metas[29] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-09\\meta.gxxml";
		metas[30] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-10\\meta.gxxml";
		metas[31] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-11\\meta.gxxml";
		metas[32] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-12\\meta.gxxml";
		metas[33] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-13\\meta.gxxml";
		metas[34] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-14\\meta.gxxml";
		metas[35] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-15\\meta.gxxml";
		metas[36] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-16\\meta.gxxml";
		metas[37] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-17\\meta.gxxml";
		metas[38] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-18\\meta.gxxml";
		metas[39] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-19\\meta.gxxml";
		metas[40] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-20\\meta.gxxml";
		metas[41] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-21\\meta.gxxml";
		metas[42] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-22\\meta.gxxml";
	}

	/// zero dist parameters
	double k[4], p[3];
	for (auto& temp : k)
		temp = 0.0;

	for (auto& temp : p)
		temp = 0.0;

	/// distortion interface instance
	std::shared_ptr<sensor::DistortionModel> distortion = (std::make_shared<sensor::SMACModel>(k, p));
	data::Point2D pp;
	pp(0) = 0.0; pp(1) = 0.0;

	data::EulerAngles bore;
	bore(0) = 90.0 / 180.0 * 3.14159265358979;
	bore(1) = 0.0;
	bore(2) = 0.0;

	data::Point3D larm;
	larm(0) = 0.0;
	larm(1) = 0.0;
	larm(2) = 0.0;

	/// main camera parameters
	double mainFl = 85.0;
	unsigned int mainWidth = 9504;
	unsigned int mainHeight = 6336;
	double mainPixPitch = 0.0038;

	std::shared_ptr<sensor::Camera> mainCam(std::make_shared<sensor::FrameCamera>(1, mainFl, pp, mainWidth, mainHeight, mainPixPitch, distortion));
	mainCam->setBoresight(bore);
	mainCam->setLeverArm(larm);

	/// create objspace instance
	auto img2obj = obj::createCompObjSpace();
	img2obj->setObjData(paths, metas);

	if (img2obj)
		EXPECT_TRUE(true);
	else
		EXPECT_TRUE(false);

	obj::DamageInfoGroup damage;
	damage.cam = mainCam;
	damage.damages.resize(3);

	std::vector<std::vector<double>> pos(3);
	std::vector<std::vector<double>> opk(3);
	
	///DSC00019_25_1.JPG
	std::vector<double> pos1(3);
	pos1[0] = 348711.394095;
	pos1[1] = 4049817.046232;
	pos1[2] = 54.202;
	pos[0] = pos1;

	std::vector<double> opk1(3);
	opk1[0] = -0.158332 / 180.0 * 3.14159265358979;
	opk1[1] = 0.122896 / 180.0 * 3.14159265358979;
	opk1[2] = -255.162205 / 180.0 * 3.14159265358979;
	opk[0] = opk1;
	
	double temp11[22] = {5267, 5076, 5264, 5076, 5264, 5095, 5263, 5096, 5260, 5096, 5260, 5099, 5263, 5099, 5263, 5096, 5264, 5095, 5267, 5095, 5267, 5076};
	double temp12[34] = {5484,5060,5484,5087,5487,5087,5488,5088,5488,5099,5491,5099,5491,5092,5492,5091,5495,5091,5495,5088,5492,5088,5491,5087,5491,5064,5488,5064,5487,5063,5487,5060,5484,5060};
	double temp13[36] = {6734,4990,6734,5017,6738,5018,6738,5025,6761,5025,6761,5022,6765,5021,6766,5017,6769,5017,6769,5010,6770,5009,6785,5009,6785,4998,6750,4998,6749,4994,6742,4994,6741,4990,6734,4990};
	double temp14[34] = {3694,2878,3694,2893,3717,2893,3718,2905,3730,2906,3730,2917,3813,2917,3814,2929,3841,2929,3841,2890,3830,2890,3829,2878,3782,2878,3781,2866,3742,2866,3741,2878,3694,2878};
	double temp15[58] = {3625,2782,3614,2782,3613,2770,3610,2770,3609,2766,3582,2766,3581,2774,3578,2774,3577,2770,3566,2770,3566,2773,3562,2774,3561,2782,3502,2782,3502,2793,3513,2793,3518,2801,3533,2801,3534,2817,3569,2817,3570,2801,3585,2801,3586,2809,3613,2809,3613,2802,3617,2801,3618,2797,3625,2797,3625,2782};
	double temp16[78] = {3406,2734,3394,2773,3406,2774,3406,2801,3422,2802,3422,2853,3410,2862,3397,2830,3374,2846,3370,2865,3358,2866,3358,2929,3393,2929,3394,2897,3465,2897,3490,2929,3513,2929,3518,2945,3549,2953,3550,2977,3617,2977,3617,2958,3601,2938,3538,2926,3521,2894,3489,2889,3505,2881,3506,2865,3585,2865,3577,2830,3550,2830,3549,2846,3490,2846,3489,2798,3426,2798,3421,2762,3433,2761,3433,2746,3406,2734};
	double temp17[26] = {9446,2494,9446,2509,9442,2510,9442,2517,9438,2518,9438,2525,9430,2526,9430,2545,9465,2545,9465,2522,9469,2521,9469,2494,9446,2494};
	double temp18[42] = {181,2262,170,2262,170,2273,166,2274,166,2281,165,2282,150,2282,150,2301,161,2301,161,2290,165,2289,165,2282,166,2281,169,2281,170,2285,177,2285,178,2281,185,2281,185,2266,182,2266,181,2262};

	damage.damages[0].pos = pos1;
	damage.damages[0].opk = opk1;

	damage.damages[0].plgns.resize(8);
	damage.damages[0].plgns[0].node2D.resize(22);
	damage.damages[0].plgns[1].node2D.resize(34);
	damage.damages[0].plgns[2].node2D.resize(36);
	damage.damages[0].plgns[3].node2D.resize(34);
	damage.damages[0].plgns[4].node2D.resize(58);
	damage.damages[0].plgns[5].node2D.resize(78);
	damage.damages[0].plgns[6].node2D.resize(26);
	damage.damages[0].plgns[7].node2D.resize(42);

	for (int i = 0; i < damage.damages[0].plgns[0].node2D.size(); ++i)
	{
		damage.damages[0].plgns[0].node2D[i](0) = temp11[i * 2];
		damage.damages[0].plgns[0].node2D[i](1) = temp11[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[0].plgns[1].node2D.size(); ++i)
	{
		damage.damages[0].plgns[1].node2D[i](0) = temp12[i * 2];
		damage.damages[0].plgns[1].node2D[i](1) = temp12[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[0].plgns[2].node2D.size(); ++i)
	{
		damage.damages[0].plgns[2].node2D[i](0) = temp13[i * 2];
		damage.damages[0].plgns[2].node2D[i](1) = temp13[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[0].plgns[3].node2D.size(); ++i)
	{
		damage.damages[0].plgns[3].node2D[i](0) = temp14[i * 2];
		damage.damages[0].plgns[3].node2D[i](1) = temp14[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[0].plgns[4].node2D.size(); ++i)
	{
		damage.damages[0].plgns[4].node2D[i](0) = temp15[i * 2];
		damage.damages[0].plgns[4].node2D[i](1) = temp15[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[0].plgns[5].node2D.size(); ++i)
	{
		damage.damages[0].plgns[5].node2D[i](0) = temp16[i * 2];
		damage.damages[0].plgns[5].node2D[i](1) = temp16[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[0].plgns[6].node2D.size(); ++i)
	{
		damage.damages[0].plgns[6].node2D[i](0) = temp17[i * 2];
		damage.damages[0].plgns[6].node2D[i](1) = temp17[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[0].plgns[7].node2D.size(); ++i)
	{
		damage.damages[0].plgns[7].node2D[i](0) = temp18[i * 2];
		damage.damages[0].plgns[7].node2D[i](1) = temp18[i * 2 + 1];
	}


	///DSC00020_25_1.JPG
	std::vector<double> pos2(3);
	pos2[0] = 348711.302671;
	pos2[1] = 4049816.936867;
	pos2[2] = 54.186604;
	pos[1] = pos2;

	std::vector<double> opk2(3);
	opk2[0] = -0.091603 / 180.0 * 3.14159265358979;
	opk2[1] = 0.323835 / 180.0 * 3.14159265358979;
	opk2[2] = -255.313414 / 180.0 * 3.14159265358979;
	opk[1] = opk2;

	double temp21[22] = { 5624,5096,5624,5099,5627,5099,5628,5100,5628,5107,5631,5107,5632,5108,5632,5119,5635,5119,5635,5096,5624,5096 };
	double temp22[28] = { 5608,5056,5608,5083,5611,5083,5612,5084,5612,5095,5619,5095,5619,5084,5616,5084,5615,5083,5615,5072,5612,5072,5611,5071,5611,5056,5608,5056 };
	double temp23[22] = { 225,3342,206,3342,206,3357,205,3358,190,3358,190,3377,209,3377,209,3362,210,3361,225,3361,225,3342 };
	double temp24[72] = { 3574,2878,3574,2893,3597,2897,3598,2905,3609,2905,3610,2913,3621,2913,3622,2921,3630,2922,3630,2929,3645,2929,3646,2941,3658,2942,3658,2953,3681,2953,3682,2965,3709,2965,3709,2946,3717,2945,3717,2938,3709,2933,3709,2926,3697,2925,3697,2914,3662,2914,3657,2909,3657,2902,3634,2902,3633,2894,3626,2894,3625,2890,3614,2890,3613,2886,3602,2886,3601,2878,3574,2878 };
	double temp25[18] = { 3442,2854,3442,2881,3454,2882,3454,2905,3481,2905,3481,2894,3493,2893,3493,2854,3442,2854 };
	double temp26[26] = { 3730,2830,3730,2869,3741,2869,3742,2881,3841,2881,3841,2842,3818,2842,3817,2830,3782,2830,3781,2818,3754,2818,3753,2830,3730,2830 };
	double temp27[50] = { 3601,2770,3562,2770,3562,2781,3550,2782,3550,2797,3561,2797,3562,2805,3526,2806,3526,2841,3514,2842,3513,2854,3502,2854,3502,2869,3577,2869,3577,2846,3601,2845,3601,2830,3578,2830,3577,2822,3601,2821,3601,2810,3613,2809,3613,2794,3601,2793,3601,2770 };
	double temp28[30] = { 3610,2758,3610,2773,3693,2773,3694,2785,3721,2785,3721,2746,3694,2746,3693,2750,3686,2750,3685,2746,3646,2746,3645,2750,3622,2750,3621,2758,3610,2758 };
	double temp29[34] = { 3502,2710,3502,2721,3490,2722,3489,2734,3478,2734,3478,2749,3489,2749,3490,2761,3502,2762,3502,2773,3517,2773,3517,2738,3529,2737,3529,2722,3518,2722,3517,2710,3502,2710 };

	damage.damages[1].pos = pos2;
	damage.damages[1].opk = opk2;

	damage.damages[1].plgns.resize(9);
	damage.damages[1].plgns[0].node2D.resize(22);
	damage.damages[1].plgns[1].node2D.resize(28);
	damage.damages[1].plgns[2].node2D.resize(22);
	damage.damages[1].plgns[3].node2D.resize(72);
	damage.damages[1].plgns[4].node2D.resize(18);
	damage.damages[1].plgns[5].node2D.resize(26);
	damage.damages[1].plgns[6].node2D.resize(50);
	damage.damages[1].plgns[7].node2D.resize(30);
	damage.damages[1].plgns[8].node2D.resize(34);

	for (int i = 0; i < damage.damages[1].plgns[0].node2D.size(); ++i)
	{
		damage.damages[1].plgns[0].node2D[i](0) = temp21[i * 2];
		damage.damages[1].plgns[0].node2D[i](1) = temp21[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[1].plgns[1].node2D.size(); ++i)
	{
		damage.damages[1].plgns[1].node2D[i](0) = temp22[i * 2];
		damage.damages[1].plgns[1].node2D[i](1) = temp22[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[1].plgns[2].node2D.size(); ++i)
	{
		damage.damages[1].plgns[2].node2D[i](0) = temp23[i * 2];
		damage.damages[1].plgns[2].node2D[i](1) = temp23[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[1].plgns[3].node2D.size(); ++i)
	{
		damage.damages[1].plgns[3].node2D[i](0) = temp24[i * 2];
		damage.damages[1].plgns[3].node2D[i](1) = temp24[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[1].plgns[4].node2D.size(); ++i)
	{
		damage.damages[1].plgns[4].node2D[i](0) = temp25[i * 2];
		damage.damages[1].plgns[4].node2D[i](1) = temp25[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[1].plgns[5].node2D.size(); ++i)
	{
		damage.damages[1].plgns[5].node2D[i](0) = temp26[i * 2];
		damage.damages[1].plgns[5].node2D[i](1) = temp26[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[1].plgns[6].node2D.size(); ++i)
	{
		damage.damages[1].plgns[6].node2D[i](0) = temp27[i * 2];
		damage.damages[1].plgns[6].node2D[i](1) = temp27[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[1].plgns[7].node2D.size(); ++i)
	{
		damage.damages[1].plgns[7].node2D[i](0) = temp28[i * 2];
		damage.damages[1].plgns[7].node2D[i](1) = temp28[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[1].plgns[8].node2D.size(); ++i)
	{
		damage.damages[1].plgns[8].node2D[i](0) = temp29[i * 2];
		damage.damages[1].plgns[8].node2D[i](1) = temp29[i * 2 + 1];
	}

	///DSC00021_25_1.JPG
	std::vector<double> pos3(3);
	pos3[0] = 348711.121774;
	pos3[1] = 4049816.829076;
	pos3[2] = 54.200345;
	pos[2] = pos3;

	std::vector<double> opk3(3);
	opk3[0] = -0.104094 / 180.0 * 3.14159265358979;
	opk3[1] = 0.283738 / 180.0 * 3.14159265358979;
	opk3[2] = -254.941596 / 180.0 * 3.14159265358979;
	opk[2] = opk3;
	
	double temp31[58] = { 3774,2910,3774,2921,3781,2921,3782,2929,3798,2930,3798,2937,3802,2938,3802,2945,3806,2946,3806,2953,3829,2953,3830,2961,3837,2961,3838,2965,3845,2965,3846,2969,3850,2970,3850,2977,3865,2977,3865,2966,3877,2965,3877,2938,3866,2938,3865,2926,3826,2926,3825,2918,3810,2918,3809,2910,3774,2910 };
	double temp32[22] = { 3838,2830,3838,2869,3849,2869,3850,2881,3865,2881,3865,2858,3877,2857,3878,2845,3889,2845,3889,2830,3838,2830 };
	double temp33[54] = { 3889,2758,3882,2758,3881,2750,3878,2750,3877,2746,3870,2746,3869,2750,3858,2750,3858,2753,3854,2754,3853,2758,3806,2758,3805,2766,3790,2766,3790,2777,3801,2777,3802,2785,3817,2785,3818,2777,3837,2777,3838,2785,3854,2786,3854,2797,3877,2797,3877,2786,3889,2785,3889,2758 };
	double temp34[10] = { 3658,2722,3658,2749,3673,2749,3673,2722,3658,2722 };
	double temp35[34] = { 254,2622,254,2637,238,2638,238,2673,269,2673,270,2705,305,2705,305,2690,313,2689,313,2662,306,2662,305,2654,290,2654,289,2638,273,2637,273,2622,254,2622 };

	damage.damages[2].pos = pos3;
	damage.damages[2].opk = opk3;

	damage.damages[2].plgns.resize(5);
	damage.damages[2].plgns[0].node2D.resize(58);
	damage.damages[2].plgns[1].node2D.resize(22);
	damage.damages[2].plgns[2].node2D.resize(54);
	damage.damages[2].plgns[3].node2D.resize(10);
	damage.damages[2].plgns[4].node2D.resize(34);

	for (int i = 0; i < damage.damages[2].plgns[0].node2D.size(); ++i)
	{
		damage.damages[2].plgns[0].node2D[i](0) = temp31[i * 2];
		damage.damages[2].plgns[0].node2D[i](1) = temp31[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[2].plgns[1].node2D.size(); ++i)
	{
		damage.damages[2].plgns[1].node2D[i](0) = temp32[i * 2];
		damage.damages[2].plgns[1].node2D[i](1) = temp32[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[2].plgns[2].node2D.size(); ++i)
	{
		damage.damages[2].plgns[2].node2D[i](0) = temp33[i * 2];
		damage.damages[2].plgns[2].node2D[i](1) = temp33[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[2].plgns[3].node2D.size(); ++i)
	{
		damage.damages[2].plgns[3].node2D[i](0) = temp34[i * 2];
		damage.damages[2].plgns[3].node2D[i](1) = temp34[i * 2 + 1];
	}
	for (int i = 0; i < damage.damages[2].plgns[4].node2D.size(); ++i)
	{
		damage.damages[2].plgns[4].node2D[i](0) = temp35[i * 2];
		damage.damages[2].plgns[4].node2D[i](1) = temp35[i * 2 + 1];
	}

	auto retval = img2obj->getObjPts(damage, 10.0);

	if (retval)
	{
		std::cout.precision(3);
		std::cout.setf(std::ios::floatfield, std::ios::fixed);
		int imgCount = 0;
		for (const auto& d : damage.damages)
		{
			std::cout << "[" << imgCount++ << "] img: " << d.plgns.size() << " damages" << std::endl;

			for(size_t i = 0; i<d.plgns.size(); ++i)
			{
				const auto& p = d.plgns[i];

				std::cout << "\tdamage: " << p.node2D.size() << " nodes " << p.objIdx << "th obj file" << std::endl;

				for (int n=0; n<p.node3D.size(); ++n)
				{
					if (!p.usage[n])
						continue;

					const auto xyz = p.node3D[n];
					std::cout << "poly#" << i << " " << "node#" << n << " " << xyz(0) << ", " << xyz(1) << ", " << xyz(2) << std::endl;
				}
			}
		}
	}

	/*
	std::vector<std::vector<data::Point2di>> imgColRow;
	for (int i = 0; i < damage.damages.size(); ++i)
	{
		for (int j = 0; j < damage.damages[i].plgns.size(); ++j)
		{
			for (int k = 0; k < damage.damages[k].plgns.size(); ++k)
			{
				damage.damages[i].plgns[j].node2D[k](0) = temp21[i * 2];
				damage.damages[i].plgns[j].node2D[k](1) = temp21[i * 2 + 1];
			}
		}
	}

	std::vector<std::vector<data::Point3D>> objPts;
	std::vector<std::vector<int>> result = img2obj->determineObj(imgColRow, pos, opk, mainCam, 10.0, objPts);

	for (const auto& d : damage.damages)
	{
		std::cout << "[" << imgCount++ << "] img: " << d.plgns.size() << " damages" << std::endl;

		for (const auto& p : d.plgns)
		{
			std::cout << "\tdamage: " << p.node2D.size() << " nodes " << p.objIdx << "th obj file" << std::endl;

			for (int n = 0; n < p.node3D.size(); ++n)
			{
				if (!p.usage[n])
					continue;

				const auto xyz = p.node3D[n];
				std::cout << xyz(0) << ", " << xyz(1) << ", " << xyz(2) << std::endl;
			}
		}
	}
	*/
}

TEST(ObjRender, testComputingSurfaceAspect)
{
	return;

	double ax = 0.0;
	double ay = 0.0;
	double az = 1.0;
	
	double bx = 0.0;
	double by = 1.0;
	double bz = 1.0;

	const bool acuteAngle = true;
	double ang = obj::computeAngleIn3Vectors(ax, ay, az, bx, by, bz, acuteAngle);
	std::cout << "angle: " << ang / 3.14159265358979 * 180.0 << std::endl;

	EXPECT_TRUE(fabs(ang - acos(-1.0) / 4.0) < std::numeric_limits<double>::epsilon());

}

TEST(ObjRender, testComputingArea)
{
	return;

	double ax = 0.0;
	double ay = 0.0;
	double az = 1.0;

	double bx = 0.0;
	double by = 1.0;
	double bz = 1.0;

	const bool acuteAngle = true;
	double ang = obj::computeAngleIn3Vectors(ax, ay, az, bx, by, bz, acuteAngle);
	std::cout << "angle: " << ang / 3.14159265358979 * 180.0 << std::endl;

	EXPECT_TRUE(fabs(ang - acos(-1.0) / 4.0) < std::numeric_limits<double>::epsilon());

}

TEST(ObjRender, testGetObjCoord)
{
	return;

	char strBuff[_MAX_PATH];
	const char* varName = "RFIMData";
	size_t len;
	getenv_s(&len, strBuff, 80, varName);
	std::string rfimFolder = strBuff;

	std::vector<std::string> paths(43);
	/// obj files
	{
		paths[0] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-01\\BR-D-00008-P-01-01.obj";
		paths[1] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-02\\BR-D-00008-P-02-01.obj";
		paths[2] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-03\\BR-D-00008-P-03-01.obj";
		paths[3] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-04\\BR-D-00008-P-04-01.obj";
		paths[4] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-05\\BR-D-00008-P-05-01.obj";
		paths[5] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-06\\BR-D-00008-P-06-01.obj";
		paths[6] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-07\\BR-D-00008-P-07-01.obj";
		paths[7] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-08\\BR-D-00008-P-08-01.obj";
		paths[8] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-09\\BR-D-00008-P-09-01.obj";
		paths[9] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-10\\BR-D-00008-P-10-01.obj";
		paths[10] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-11\\BR-D-00008-P-11-01.obj";
		paths[11] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-12\\BR-D-00008-P-12-01.obj";
		paths[12] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-13\\BR-D-00008-P-13-01.obj";
		paths[13] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-14\\BR-D-00008-P-14-01.obj";
		paths[14] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-15\\BR-D-00008-P-15-01.obj";
		paths[15] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-16\\BR-D-00008-P-16-01.obj";
		paths[16] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-17\\BR-D-00008-P-17-01.obj";
		paths[17] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-18\\BR-D-00008-P-18-01.obj";
		paths[18] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-19\\BR-D-00008-P-19-01.obj";
		paths[19] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-20\\BR-D-00008-P-20-01.obj";
		paths[20] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-21\\BR-D-00008-P-21-01.obj";

		paths[21] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-01\\BR-D-00008-S-01-01.obj";
		paths[22] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-02\\BR-D-00008-S-02-01.obj";
		paths[23] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-03\\BR-D-00008-S-03-01.obj";
		paths[24] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-04\\BR-D-00008-S-04-01.obj";
		paths[25] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-05\\BR-D-00008-S-05-01.obj";
		paths[26] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-06\\BR-D-00008-S-06-01.obj";
		paths[27] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-07\\BR-D-00008-S-07-01.obj";
		paths[28] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-08\\BR-D-00008-S-08-01.obj";
		paths[29] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-09\\BR-D-00008-S-09-01.obj";
		paths[30] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-10\\BR-D-00008-S-10-01.obj";
		paths[31] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-11\\BR-D-00008-S-11-01.obj";
		paths[32] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-12\\BR-D-00008-S-12-01.obj";
		paths[33] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-13\\BR-D-00008-S-13-01.obj";
		paths[34] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-14\\BR-D-00008-S-14-01.obj";
		paths[35] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-15\\BR-D-00008-S-15-01.obj";
		paths[36] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-16\\BR-D-00008-S-16-01.obj";
		paths[37] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-17\\BR-D-00008-S-17-01.obj";
		paths[38] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-18\\BR-D-00008-S-18-01.obj";
		paths[39] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-19\\BR-D-00008-S-19-01.obj";
		paths[40] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-20\\BR-D-00008-S-20-01.obj";
		paths[41] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-21\\BR-D-00008-S-21-01.obj";
		paths[42] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-22\\BR-D-00008-S-22-01.obj";
	}

	std::vector<std::string> metas(43);
	/// meta.gxxml files
	{
		metas[0] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-01\\meta.gxxml";
		metas[1] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-02\\meta.gxxml";
		metas[2] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-03\\meta.gxxml";
		metas[3] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-04\\meta.gxxml";
		metas[4] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-05\\meta.gxxml";
		metas[5] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-06\\meta.gxxml";
		metas[6] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-07\\meta.gxxml";
		metas[7] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-08\\meta.gxxml";
		metas[8] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-09\\meta.gxxml";
		metas[9] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-10\\meta.gxxml";
		metas[10] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-11\\meta.gxxml";
		metas[11] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-12\\meta.gxxml";
		metas[12] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-13\\meta.gxxml";
		metas[13] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-14\\meta.gxxml";
		metas[14] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-15\\meta.gxxml";
		metas[15] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-16\\meta.gxxml";
		metas[16] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-17\\meta.gxxml";
		metas[17] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-18\\meta.gxxml";
		metas[18] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-19\\meta.gxxml";
		metas[19] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-20\\meta.gxxml";
		metas[20] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-21\\meta.gxxml";

		metas[21] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-01\\meta.gxxml";
		metas[22] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-02\\meta.gxxml";
		metas[23] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-03\\meta.gxxml";
		metas[24] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-04\\meta.gxxml";
		metas[25] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-05\\meta.gxxml";
		metas[26] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-06\\meta.gxxml";
		metas[27] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-07\\meta.gxxml";
		metas[28] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-08\\meta.gxxml";
		metas[29] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-09\\meta.gxxml";
		metas[30] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-10\\meta.gxxml";
		metas[31] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-11\\meta.gxxml";
		metas[32] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-12\\meta.gxxml";
		metas[33] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-13\\meta.gxxml";
		metas[34] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-14\\meta.gxxml";
		metas[35] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-15\\meta.gxxml";
		metas[36] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-16\\meta.gxxml";
		metas[37] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-17\\meta.gxxml";
		metas[38] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-18\\meta.gxxml";
		metas[39] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-19\\meta.gxxml";
		metas[40] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-20\\meta.gxxml";
		metas[41] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-21\\meta.gxxml";
		metas[42] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-22\\meta.gxxml";
	}

	/// create objspace instance
	auto img2obj = obj::createCompObjSpace();
	img2obj->setObjData(paths, metas);

	if (img2obj)
		EXPECT_TRUE(true);
	else
		EXPECT_TRUE(false);

	/// zero dist parameters
	double k[4], p[3];
	for (auto& temp : k)
		temp = 0.0;

	for (auto& temp : p)
		temp = 0.0;

	/// distortion interface instance
	std::shared_ptr<sensor::DistortionModel> distortion = (std::make_shared<sensor::SMACModel>(k, p));
	data::Point2D pp;
	pp(0) = 0.0; pp(1) = 0.0;

	data::EulerAngles bore;
	bore(0) = 90.0 / 180.0 * 3.14159265358979;
	bore(1) = 0.0;
	bore(2) = 0.0;

	data::Point3D larm;
	larm(0) = 0.0;
	larm(1) = 0.0;
	larm(2) = 0.0;

	/// mapping camera parameters
	double mapFl = 18.0; /// focal length of a mapping camera
	unsigned int mapWidth = 5456; /// width
	unsigned int mapHeight = 3632; /// height
	double mapPixPitch = 0.0042; /// pixel pitch

	std::shared_ptr<sensor::Camera> mapCam(std::make_shared<sensor::FrameCamera>(0, mapFl, pp, mapWidth, mapHeight, mapPixPitch, distortion));
	mapCam->setBoresight(bore);
	mapCam->setLeverArm(larm);

	/// EOP of a sample main camera image
	std::vector<double> pos(3);
	pos[0] = 348711.3941;
	pos[1] = 4049817.046;
	pos[2] = 54.227;

	double roll, pitch, heading;
	roll = 0.122896 / 180.0 * 3.14159265358979;
	pitch = -0.158332 / 180.0 * 3.14159265358979;
	heading = 255.609205 / 180.0 * 3.14159265358979;

	std::vector<double> opk(3);
	opk[0] = pitch;
	opk[1] = roll;
	opk[2] = -heading;

	img2obj->setImgDataForObjCoord(pos, opk, mapCam);

	int nCols = 10;
	int nRows = 10;

	auto mapImgSize = mapCam->getSensorSize();
	auto gapW = mapImgSize(0) / nCols;
	auto halfGapW = gapW * 0.5;
	auto gapH = mapImgSize(1) / nRows;
	auto halfGapH = gapH * 0.5;

	std::cout.precision(3);
	std::cout.setf(std::ios::floatfield, std::ios::fixed);

	std::vector<double> Xa, Ya, Za;
	std::vector<int> cols, rows;
	std::cout << "Mapping camera grid coordiantes: " << std::endl;
	for (int r = 0; r < nRows; ++r)
	{
		int row = int(halfGapH + r * gapH + 0.5);

		for (int c = 0; c < nCols; ++c)
		{
			int col = int(halfGapW + c * gapW + 0.5);

			double x, y, z;
			if (img2obj->getObjPts(mapCam, col, row, x, y, z))
			{
				std::cout << "O\t";

				cols.push_back(col);
				rows.push_back(row);

				Xa.push_back(x);
				Ya.push_back(y);
				Za.push_back(z);
			}
			else
				std::cout << ".\t";
		}

		std::cout << std::endl;
	}

	std::cout << std::endl;
	std::cout << std::endl;

	for (int i = 0; i < Xa.size(); ++i)
	{
		std::cout << cols[i] << "\t" << rows[i] << "\t" << Xa[i] << "\t" << Ya[i] << "\t" << Za[i] << "\n";
	}

	std::cout << std::endl;
	std::cout << std::endl;

	/// main camera parameters
	double mainFl = 85.0;
	unsigned int mainWidth = 9504;
	unsigned int mainHeight = 6336;
	double mainPixPitch = 0.0038;

	std::shared_ptr<sensor::Camera> mainCam(std::make_shared<sensor::FrameCamera>(1, mainFl, pp, mainWidth, mainHeight, mainPixPitch, distortion));
	mainCam->setBoresight(bore);
	mainCam->setLeverArm(larm);

	/// EOP of a main camera [97]
	pos[0] = 348756.32844613539;
	pos[1] = 4049674.8750433391;
	pos[2] = 54.173183000000002;

	opk[0] = -0.0018225600713950769;
	opk[1] = 0.0051077758691314805;
	opk[2] = -4.4768511778678590;

	img2obj->setImgDataForObjCoord(pos, opk, mainCam);

	auto mainImgSize = mainCam->getSensorSize();
	gapW = mainImgSize(0) / nCols;
	halfGapW = gapW * 0.5;
	gapH = mainImgSize(1) / nRows;
	halfGapH = gapH * 0.5;

	Xa.clear();
	Ya.clear();
	Za.clear();
	cols.clear();
	rows.clear();

	std::cout << "Main camera grid coordiantes: " << std::endl;
	for (int r = 0; r < nRows; ++r)
	{
		int row = int(halfGapH + r * gapH);

		for (int c = 0; c < nCols; ++c)
		{
			int col = int(halfGapW + c * gapW);

			double x, y, z;
			if (img2obj->getObjPts(mapCam, col, row, x, y, z))
			{
				std::cout << "O\t";

				cols.push_back(col);
				rows.push_back(row);

				Xa.push_back(x);
				Ya.push_back(y);
				Za.push_back(z);
			}
			else
				std::cout << ".\t";
		}

		std::cout << std::endl;
	}

	std::cout << std::endl;
	std::cout << std::endl;

	for (int i = 0; i < Xa.size(); ++i)
	{
		std::cout << cols[i] << "\t" << rows[i] << "\t" << Xa[i] << "\t" << Ya[i] << "\t" << Za[i] << "\n";
	}
}

TEST(ObjRender, testDeterminePartMapCam)
{
	return;

	char strBuff[_MAX_PATH];
	const char* varName = "RFIMData";
	size_t len;
	getenv_s(&len, strBuff, 80, varName);
	std::string rfimFolder = strBuff;

	std::vector<std::string> paths(43);
	/// obj files
	{
		paths[0] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-01\\BR-D-00008-P-01-01.obj";
		paths[1] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-02\\BR-D-00008-P-02-01.obj";
		paths[2] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-03\\BR-D-00008-P-03-01.obj";
		paths[3] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-04\\BR-D-00008-P-04-01.obj";
		paths[4] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-05\\BR-D-00008-P-05-01.obj";
		paths[5] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-06\\BR-D-00008-P-06-01.obj";
		paths[6] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-07\\BR-D-00008-P-07-01.obj";
		paths[7] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-08\\BR-D-00008-P-08-01.obj";
		paths[8] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-09\\BR-D-00008-P-09-01.obj";
		paths[9] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-10\\BR-D-00008-P-10-01.obj";
		paths[10] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-11\\BR-D-00008-P-11-01.obj";
		paths[11] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-12\\BR-D-00008-P-12-01.obj";
		paths[12] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-13\\BR-D-00008-P-13-01.obj";
		paths[13] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-14\\BR-D-00008-P-14-01.obj";
		paths[14] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-15\\BR-D-00008-P-15-01.obj";
		paths[15] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-16\\BR-D-00008-P-16-01.obj";
		paths[16] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-17\\BR-D-00008-P-17-01.obj";
		paths[17] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-18\\BR-D-00008-P-18-01.obj";
		paths[18] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-19\\BR-D-00008-P-19-01.obj";
		paths[19] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-20\\BR-D-00008-P-20-01.obj";
		paths[20] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-21\\BR-D-00008-P-21-01.obj";

		paths[21] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-01\\BR-D-00008-S-01-01.obj";
		paths[22] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-02\\BR-D-00008-S-02-01.obj";
		paths[23] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-03\\BR-D-00008-S-03-01.obj";
		paths[24] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-04\\BR-D-00008-S-04-01.obj";
		paths[25] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-05\\BR-D-00008-S-05-01.obj";
		paths[26] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-06\\BR-D-00008-S-06-01.obj";
		paths[27] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-07\\BR-D-00008-S-07-01.obj";
		paths[28] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-08\\BR-D-00008-S-08-01.obj";
		paths[29] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-09\\BR-D-00008-S-09-01.obj";
		paths[30] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-10\\BR-D-00008-S-10-01.obj";
		paths[31] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-11\\BR-D-00008-S-11-01.obj";
		paths[32] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-12\\BR-D-00008-S-12-01.obj";
		paths[33] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-13\\BR-D-00008-S-13-01.obj";
		paths[34] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-14\\BR-D-00008-S-14-01.obj";
		paths[35] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-15\\BR-D-00008-S-15-01.obj";
		paths[36] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-16\\BR-D-00008-S-16-01.obj";
		paths[37] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-17\\BR-D-00008-S-17-01.obj";
		paths[38] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-18\\BR-D-00008-S-18-01.obj";
		paths[39] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-19\\BR-D-00008-S-19-01.obj";
		paths[40] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-20\\BR-D-00008-S-20-01.obj";
		paths[41] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-21\\BR-D-00008-S-21-01.obj";
		paths[42] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-22\\BR-D-00008-S-22-01.obj";
	}

	std::vector<std::string> metas(43);
	/// meta.gxxml files
	{
		metas[0] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-01\\meta.gxxml";
		metas[1] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-02\\meta.gxxml";
		metas[2] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-03\\meta.gxxml";
		metas[3] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-04\\meta.gxxml";
		metas[4] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-05\\meta.gxxml";
		metas[5] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-06\\meta.gxxml";
		metas[6] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-07\\meta.gxxml";
		metas[7] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-08\\meta.gxxml";
		metas[8] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-09\\meta.gxxml";
		metas[9] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-10\\meta.gxxml";
		metas[10] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-11\\meta.gxxml";
		metas[11] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-12\\meta.gxxml";
		metas[12] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-13\\meta.gxxml";
		metas[13] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-14\\meta.gxxml";
		metas[14] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-15\\meta.gxxml";
		metas[15] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-16\\meta.gxxml";
		metas[16] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-17\\meta.gxxml";
		metas[17] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-18\\meta.gxxml";
		metas[18] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-19\\meta.gxxml";
		metas[19] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-20\\meta.gxxml";
		metas[20] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-21\\meta.gxxml";

		metas[21] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-01\\meta.gxxml";
		metas[22] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-02\\meta.gxxml";
		metas[23] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-03\\meta.gxxml";
		metas[24] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-04\\meta.gxxml";
		metas[25] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-05\\meta.gxxml";
		metas[26] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-06\\meta.gxxml";
		metas[27] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-07\\meta.gxxml";
		metas[28] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-08\\meta.gxxml";
		metas[29] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-09\\meta.gxxml";
		metas[30] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-10\\meta.gxxml";
		metas[31] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-11\\meta.gxxml";
		metas[32] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-12\\meta.gxxml";
		metas[33] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-13\\meta.gxxml";
		metas[34] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-14\\meta.gxxml";
		metas[35] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-15\\meta.gxxml";
		metas[36] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-16\\meta.gxxml";
		metas[37] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-17\\meta.gxxml";
		metas[38] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-18\\meta.gxxml";
		metas[39] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-19\\meta.gxxml";
		metas[40] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-20\\meta.gxxml";
		metas[41] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-21\\meta.gxxml";
		metas[42] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-22\\meta.gxxml";
	}

	/// create objspace instance
	auto img2obj = obj::createCompObjSpace();
	img2obj->setObjData(paths, metas);

	if (img2obj)
		EXPECT_TRUE(true);
	else
		EXPECT_TRUE(false);

	/// zero dist parameters
	double k[4], p[3];
	for (auto& temp : k)
		temp = 0.0;

	for (auto& temp : p)
		temp = 0.0;

	/// distortion interface instance
	std::shared_ptr<sensor::DistortionModel> distortion = (std::make_shared<sensor::SMACModel>(k, p));
	data::Point2D pp;
	pp(0) = 0.0; pp(1) = 0.0;

	data::EulerAngles bore;
	bore(0) = 90.0 / 180.0 * 3.14159265358979;
	bore(1) = 0.0;
	bore(2) = 0.0;

	data::Point3D larm;
	larm(0) = 0.0;
	larm(1) = 0.0;
	larm(2) = 0.0;

	/// mapping camera parameters
	double mapFl = 18.0; /// focal length of a mapping camera
	unsigned int mapWidth = 5456; /// width
	unsigned int mapHeight = 3632; /// height
	double mapPixPitch = 0.0042; /// pixel pitch

	std::shared_ptr<sensor::Camera> mapCam(std::make_shared<sensor::FrameCamera>(0, mapFl, pp, mapWidth, mapHeight, mapPixPitch, distortion));
	mapCam->setBoresight(bore);
	mapCam->setLeverArm(larm);

	/// EOP of a sample main camera image
	std::vector<double> pos(3);
	pos[0] = 348711.3941;
	pos[1] = 4049817.046;
	pos[2] = 54.227;
	std::vector<std::vector<double>> poss(1);
	poss[0] = pos;

	double roll, pitch, heading;
	roll = 0.122896 / 180.0 * 3.14159265358979;
	pitch = -0.158332 / 180.0 * 3.14159265358979;
	heading = 255.609205 / 180.0 * 3.14159265358979;

	std::vector<double> opk(3);
	opk[0] = pitch;
	opk[1] = roll;
	opk[2] = -heading;
	std::vector<std::vector<double>> opks(1);
	opks[0] = opk;

	int nCols = 10;
	int nRows = 10;
	double maxDist = 15.0;

	std::vector<std::vector<data::Point3D>> objPts;
	auto objParts = img2obj->determineObj(nCols, nRows, poss, opks, mapCam, maxDist, objPts);

	std::cout.precision(3);
	std::cout.setf(std::ios::floatfield, std::ios::fixed);

	for (unsigned int i = 0; i < objParts.size(); ++i)
	{
		std::cout << "Obj file indices corresponding to " << i << "th image [Mapping Camera]: ";

		for (const auto& idx : objParts[i])
		{
			std::cout << idx << "\t";
		}

		std::cout << std::endl;
	}

	std::cout << "obj coordinates of grid cells" << std::endl;

	for (const auto& rows : objPts)
	{
		for (int i = 0; i < nRows; ++i)
		{
			for (int j = 0; j < nCols; ++j)
			{
				const auto& pt = rows[i * nCols + j];
				if (pt(0) == pt(1) && pt(1) == pt(2) && pt(0) < 0. && pt(1) < 0. && pt(2) < 0.)
					std::cout << ".\t";
				else
					std::cout << "O\t";
			}
			std::cout << std::endl;
		}

		std::cout << std::endl;
		std::cout << std::endl;

		for (int j = 0; j < rows.size(); ++j)
		{
			const auto& pt = rows[j];
			if (pt(0) == pt(1) && pt(1) == pt(2) && pt(0) < 0. && pt(1) < 0. && pt(2) < 0.)
				continue;
			else
				std::cout << pt(0) << "\t" << pt(1) << "\t" << pt(2) << "\n";
		}

		std::cout << std::endl;
		std::cout << std::endl;
	}

	/// main camera parameters
	double mainFl = 85.0;
	unsigned int mainWidth = 9504;
	unsigned int mainHeight = 6336;
	double mainPixPitch = 0.0038;

	std::shared_ptr<sensor::Camera> mainCam(std::make_shared<sensor::FrameCamera>(1, mainFl, pp, mainWidth, mainHeight, mainPixPitch, distortion));
	mainCam->setBoresight(bore);
	mainCam->setLeverArm(larm);

	/// EOP of a main camera [97]
	pos[0] = 348756.32844613539;
	pos[1] = 4049674.8750433391;
	pos[2] = 54.173183000000002;
	poss[0] = pos;

	opk[0] = -0.0018225600713950769;
	opk[1] = 0.0051077758691314805;
	opk[2] = -4.4768511778678590;
	opks[0] = opk;

	objParts = img2obj->determineObj(nCols, nRows, poss, opks, mainCam, maxDist, objPts);

	std::cout.precision(3);
	std::cout.setf(std::ios::floatfield, std::ios::fixed);

	for (unsigned int i = 0; i < objParts.size(); ++i)
	{
		std::cout << "Obj file indices corresponding to " << i << "th image [Main camera]: ";

		for (const auto& idx : objParts[i])
		{
			std::cout << idx << "\t";
		}

		std::cout << std::endl;
	}

	std::cout << "obj coordinates of grid cells" << std::endl;

	for (const auto& rows : objPts)
	{
		for (int i = 0; i < nRows; ++i)
		{
			for (int j = 0; j < nCols; ++j)
			{
				const auto& pt = rows[i * nCols + j];
				if (pt(0) == pt(1) && pt(1) == pt(2) && pt(0) < 0. && pt(1) < 0. && pt(2) < 0.)
					std::cout << ".\t";
				else
					std::cout << "O\t";
			}
			std::cout << std::endl;
		}
		
		std::cout << std::endl;
		std::cout << std::endl;

		for (int j = 0; j < rows.size(); ++j)
		{
			const auto& pt = rows[j];
			if (pt(0) == pt(1) && pt(1) == pt(2) && pt(0) < 0. && pt(1) < 0. && pt(2) < 0.)
				continue;
			else
				std::cout << pt(0) << "\t" << pt(1) << "\t" << pt(2) << "\n";
		}

		std::cout << std::endl;
		std::cout << std::endl;
	}
}

TEST(ObjRender, testDeterminePartMainCam)
{
	return;

	char strBuff[_MAX_PATH];
	const char* varName = "RFIMData";
	size_t len;
	getenv_s(&len, strBuff, 80, varName);
	std::string rfimFolder = strBuff;

	std::vector<std::string> paths(43);
	paths[0] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-01\\BR-D-00008-P-01-01.obj";
	paths[1] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-02\\BR-D-00008-P-02-01.obj";
	paths[2] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-03\\BR-D-00008-P-03-01.obj";
	paths[3] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-04\\BR-D-00008-P-04-01.obj";
	paths[4] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-05\\BR-D-00008-P-05-01.obj";
	paths[5] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-06\\BR-D-00008-P-06-01.obj";
	paths[6] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-07\\BR-D-00008-P-07-01.obj";
	paths[7] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-08\\BR-D-00008-P-08-01.obj";
	paths[8] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-09\\BR-D-00008-P-09-01.obj";
	paths[9] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-10\\BR-D-00008-P-10-01.obj";
	paths[10] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-11\\BR-D-00008-P-11-01.obj";
	paths[11] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-12\\BR-D-00008-P-12-01.obj";
	paths[12] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-13\\BR-D-00008-P-13-01.obj";
	paths[13] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-14\\BR-D-00008-P-14-01.obj";
	paths[14] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-15\\BR-D-00008-P-15-01.obj";
	paths[15] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-16\\BR-D-00008-P-16-01.obj";
	paths[16] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-17\\BR-D-00008-P-17-01.obj";
	paths[17] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-18\\BR-D-00008-P-18-01.obj";
	paths[18] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-19\\BR-D-00008-P-19-01.obj";
	paths[19] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-20\\BR-D-00008-P-20-01.obj";
	paths[20] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-21\\BR-D-00008-P-21-01.obj";

	paths[21] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-01\\BR-D-00008-S-01-01.obj";
	paths[22] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-02\\BR-D-00008-S-02-01.obj";
	paths[23] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-03\\BR-D-00008-S-03-01.obj";
	paths[24] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-04\\BR-D-00008-S-04-01.obj";
	paths[25] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-05\\BR-D-00008-S-05-01.obj";
	paths[26] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-06\\BR-D-00008-S-06-01.obj";
	paths[27] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-07\\BR-D-00008-S-07-01.obj";
	paths[28] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-08\\BR-D-00008-S-08-01.obj";
	paths[29] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-09\\BR-D-00008-S-09-01.obj";
	paths[30] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-10\\BR-D-00008-S-10-01.obj";
	paths[31] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-11\\BR-D-00008-S-11-01.obj";
	paths[32] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-12\\BR-D-00008-S-12-01.obj";
	paths[33] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-13\\BR-D-00008-S-13-01.obj";
	paths[34] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-14\\BR-D-00008-S-14-01.obj";
	paths[35] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-15\\BR-D-00008-S-15-01.obj";
	paths[36] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-16\\BR-D-00008-S-16-01.obj";
	paths[37] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-17\\BR-D-00008-S-17-01.obj";
	paths[38] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-18\\BR-D-00008-S-18-01.obj";
	paths[39] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-19\\BR-D-00008-S-19-01.obj";
	paths[40] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-20\\BR-D-00008-S-20-01.obj";
	paths[41] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-21\\BR-D-00008-S-21-01.obj";
	paths[42] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-22\\BR-D-00008-S-22-01.obj";

	std::vector<std::string> metas(43);
	metas[0] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-01\\meta.gxxml";
	metas[1] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-02\\meta.gxxml";
	metas[2] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-03\\meta.gxxml";
	metas[3] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-04\\meta.gxxml";
	metas[4] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-05\\meta.gxxml";
	metas[5] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-06\\meta.gxxml";
	metas[6] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-07\\meta.gxxml";
	metas[7] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-08\\meta.gxxml";
	metas[8] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-09\\meta.gxxml";
	metas[9] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-10\\meta.gxxml";
	metas[10] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-11\\meta.gxxml";
	metas[11] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-12\\meta.gxxml";
	metas[12] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-13\\meta.gxxml";
	metas[13] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-14\\meta.gxxml";
	metas[14] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-15\\meta.gxxml";
	metas[15] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-16\\meta.gxxml";
	metas[16] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-17\\meta.gxxml";
	metas[17] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-18\\meta.gxxml";
	metas[18] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-19\\meta.gxxml";
	metas[19] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-20\\meta.gxxml";
	metas[20] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-P-21\\meta.gxxml";

	metas[21] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-01\\meta.gxxml";
	metas[22] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-02\\meta.gxxml";
	metas[23] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-03\\meta.gxxml";
	metas[24] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-04\\meta.gxxml";
	metas[25] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-05\\meta.gxxml";
	metas[26] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-06\\meta.gxxml";
	metas[27] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-07\\meta.gxxml";
	metas[28] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-08\\meta.gxxml";
	metas[29] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-09\\meta.gxxml";
	metas[30] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-10\\meta.gxxml";
	metas[31] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-11\\meta.gxxml";
	metas[32] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-12\\meta.gxxml";
	metas[33] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-13\\meta.gxxml";
	metas[34] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-14\\meta.gxxml";
	metas[35] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-15\\meta.gxxml";
	metas[36] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-16\\meta.gxxml";
	metas[37] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-17\\meta.gxxml";
	metas[38] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-18\\meta.gxxml";
	metas[39] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-19\\meta.gxxml";
	metas[40] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-20\\meta.gxxml";
	metas[41] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-21\\meta.gxxml";
	metas[42] = rfimFolder + "\\Facility\\BR-D-1938-0000008\\Model\\Rev01\\BR-D-00008-S-22\\meta.gxxml";

	auto img2obj = obj::createCompObjSpace();
	img2obj->setObjData(paths, metas);

	if (img2obj)
		EXPECT_TRUE(true);
	else
		EXPECT_TRUE(false);

	double k[4], p[3];
	for (auto& temp : k)
		temp = 0.0;

	for (auto& temp : p)
		temp = 0.0;

	std::shared_ptr<sensor::DistortionModel> distortion = (std::make_shared<sensor::SMACModel>(k, p));
	data::Point2D pp;
	double f = 85.0;
	unsigned int w = 9504;
	unsigned int h = 6336;
	double pixPitch = 0.0038;

	pp(0) = 0.0; pp(1) = 0.0;
	std::shared_ptr<sensor::Camera> cam(std::make_shared<sensor::FrameCamera>(0, f, pp, w, h, pixPitch, distortion));

	data::EulerAngles bore;
	bore(0) = 90.0 / 180.0 * 3.14159265358979;
	bore(1) = 0.0;
	bore(2) = 0.0;
	cam->setBoresight(bore);

	data::Point3D larm;
	larm(0) = 0.0;
	larm(1) = 0.0;
	larm(2) = 0.0;
	cam->setLeverArm(larm);

	/// main camera [97]
	std::vector<double> pos(3);
	pos[0] = 348756.32844613539;
	pos[1] = 4049674.8750433391;
	pos[2] = 54.173183000000002;
	std::vector<std::vector<double>> poss(1);
	poss[0] = pos;

	/// main camera [97]
	std::vector<double> opk(3);
	opk[0] = -0.0018225600713950769;
	opk[1] = 0.0051077758691314805;
	opk[2] = -4.4768511778678590;

	std::vector<std::vector<double>> opks(1);
	opks[0] = opk;

	double maxDist = 15.0;

	std::vector<std::vector<data::Point3D>> objPts;
	auto objParts = img2obj->determineObj(10, 10, poss, opks, cam, maxDist, objPts);

	for (unsigned int i = 0; i < objParts.size(); ++i)
	{
		std::cout << "Obj file indices corresponding to " << i << "th image: ";

		for (const auto& idx : objParts[i])
		{
			std::cout << idx << "\t";
		}

		std::cout << std::endl;
	}
}

/*
TEST(ObjRender, DisplayRenderSample2)
{
	std::vector<std::string> paths;
	std::vector<std::string> metas;
	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.gxxml");

	objrend::loadObj(paths, metas);

	int imgW = 5456;
	int imgH = 3632;
	int winH = imgH / subSample;
	/// APSC (25.1×16.7 mm, 1.503:1.0) with 18mm, fov = 46.32;
	float fovHDeg = 46.32f;
	fovHDeg = 45.92798521;

	/// boresight
	double bore_o, bore_p, bore_k;
	bore_o = 90.0 * deg2rad;
	bore_p = 0.0 * deg2rad;
	bore_k = 0.0 * deg2rad;

	double pc[3];
	pc[0] = 348727.134;
	pc[1] = 4049759.916;
	pc[2] = 53.851;

	//double nx = pc[0], ny = pc[1], nz = pc[2];
	//objrend::normalizePt(nx, ny, nz);

	double o, p, k;
	o = 0.302124 * deg2rad;
	p = 0.05518472 * deg2rad;
	k = 108.298326 * deg2rad;

	objrend::simulatePhoto(imgW, imgH, fovHDeg,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_017_new.bmp",
		true);

	EXPECT_EQ(1, 1);
	EXPECT_TRUE(true);
}
*/

/*
TEST(ObjRender, DisplayRenderSample3)
{
	std::vector<std::string> paths;
	std::vector<std::string> metas;
	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.gxxml");

	objrend::loadObj(paths, metas);

	int imgW = 5456;
	int imgH = 3632;
	int winH = imgH / subSample;
	/// APSC (25.1×16.7 mm, 1.503:1.0) with 18mm, fov = 46.32;
	float fovH = 46.32f;
	fovH = 45.92798521;

	/// boresight
	double bore_o, bore_p, bore_k;
	bore_o = 90.0 * deg2rad;
	bore_p = 0.0 * deg2rad;
	bore_k = 0.0 * deg2rad;

	double nx = 0.0, ny = 0.0, nz = 10.0;
	objrend::deNormalizePt(nx, ny, nz);

	double o, p, k;
	o = 0 * deg2rad;
	p = 0 * deg2rad;
	k = 0 * deg2rad;

	objrend::simulatePhoto(imgW, imgH, fovH,
		o, p, k, nx, ny, nz,
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_017_new.bmp",
		true);

	EXPECT_EQ(1, 1);
	EXPECT_TRUE(true);
}

TEST(ObjRender, getCoords)
{
	std::vector<std::string> paths;
	std::vector<std::string> metas;
	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.gxxml");

	objrend::loadObj(paths, metas);

	int imgW = 5456;
	int imgH = 3632;
	int winH = imgH / subSample;
	/// APSC (25.1×16.7 mm, 1.503:1.0) with 18mm, fov = 46.32;
	float fovH = 46.32f;
	fovH = 45.92798521;

	/// boresight
	double bore_o, bore_p, bore_k;
	bore_o = 0.0 * deg2rad;
	bore_p = 0.0 * deg2rad;
	bore_k = 0.0 * deg2rad;

	double nx = 0.0, ny = 0.0, nz = 10.0;
	objrend::deNormalizePt(nx, ny, nz);

	double o, p, k;
	o = 0 * deg2rad;
	p = 0 * deg2rad;
	k = 0 * deg2rad;

	auto scale = float(double(winH) / double(imgH));

	std::vector<int> imgCol(5), imgRow(5);
	imgCol[0] = 0;		imgRow[0] = 0;
	imgCol[1] = castint(imgW * scale);	imgRow[1] = 0;
	imgCol[2] = 0;		imgRow[2] = castint(imgH * scale);
	imgCol[3] = castint(imgW * scale);	imgRow[3] = castint(imgH * scale);
	imgCol[4] = castint(imgW / 2 * scale);	imgRow[4] = castint(imgH / 2 * scale);

	std::vector<double> Xa, Ya, Za;
	std::vector<bool> ret = objrend::getImg2ObjCoords(imgW, imgH, fovH,
		o, p, k, nx, ny, nz, bore_o, bore_p, bore_k, 0., 0., 0., scale, imgCol, imgRow, Xa, Ya, Za);

	std::cout.setf(std::ios::floatfield, std::ios::fixed);
	std::cout.precision(5);

	for (int i = 0; i < imgCol.size(); ++i)
	{
		std::cout << imgCol[i] << "\t";
		std::cout << imgRow[i] << "\t";

		if (!ret[i])
		{
			std::cout << "FAIL" << "\n";
		}
		else
		{
			std::cout << Xa[i] << "\t";
			std::cout << Ya[i] << "\t";
			std::cout << Za[i] << "\n";
		}
	}

	EXPECT_EQ(1, 1);
	EXPECT_TRUE(true);
}
*/

TEST(ObjRender, FBOTest)
{
	//objrend::renderFBO();
}

TEST(ObjRender, DisplayRenderSample)
{
	return;

	//objrend::renderFBO();
	//objrend::displayFBO();
	//return;

	//*
	std::vector<std::string> paths;
	std::vector<std::string> metas;
	std::string temp = "../../";
	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.gxxml");

	objrend::loadObj(paths, metas);

	int imgW = 5456;
	int imgH = 3632;
	int winH = imgH / subSample;
	/// APSC (25.1×16.7 mm, 1.503:1.0) with 18mm, fov = 46.32;
	float fovH = 46.32f;
	fovH = 45.92798521f;

	/// boresight
	double bore_o, bore_p, bore_k;
	bore_o = 90.0 * deg2rad;
	bore_p = 0.0 * deg2rad;
	bore_k = 0.0 * deg2rad;

	double pc[3];
	pc[0] = 348727.134;
	pc[1] = 4049759.916;
	pc[2] = 53.851;

	//double nx = pc[0], ny = pc[1], nz = pc[2];
	//objrend::normalizePt(nx, ny, nz);

	double o, p, k;
	o = 0.302124 * deg2rad;
	p = 0.05518472 * deg2rad;
	k = 108.298326 * deg2rad;

	objrend::simulatePhoto(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_017_new.bmp",
		false);

	pc[0] = 348711.39;
	pc[1] = 4049817.05;
	pc[2] = 54.227;

	//nx = pc[0];
	//ny = pc[1];
	//nz = pc[2];

	//objrend::normalizePt(nx, ny, nz);

	o = -0.1583322 * deg2rad;
	p = 0.1228959 * deg2rad;
	k = -255.609205 * deg2rad;

	objrend::simulatePhoto(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_013.bmp",
		false);

	pc[0] = 348730.78;
	pc[1] = 4049738.69;
	pc[2] = 51.76;

	//nx = pc[0];
	//ny = pc[1];
	//nz = pc[2];

	//objrend::normalizePt(nx, ny, nz);

	o = -0.2541518 * deg2rad;
	p = 0.02611797 * deg2rad;
	k = -250.4444871 * deg2rad;

	objrend::simulatePhoto(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_085.bmp",
		false);

	pc[0] = 348713.8;
	pc[1] = 4049795.92;
	pc[2] = 53.58821559;

	//nx = pc[0];
	//ny = pc[1];
	//nz = pc[2];

	//objrend::normalizePt(nx, ny, nz);

	o = -0.1943272 * deg2rad;
	p = -0.2761481 * deg2rad;
	k = -250.1265522 * deg2rad;

	objrend::simulatePhoto(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_018.bmp",
		false);

	pc[0] = 330261.324;
	pc[1] = 4076994.374;
	pc[2] = 74.600;

	//nx = pc[0];
	//ny = pc[1];
	//nz = pc[2];

	//objrend::normalizePt(nx, ny, nz);

	o = -0.256748 * deg2rad;
	p = -0.986063 * deg2rad;
	k = -80.642832 * deg2rad;

	objrend::simulatePhoto(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_647.bmp",
		false);
	//*/

	EXPECT_EQ(1, 1);
	EXPECT_TRUE(true);
}

/*
TEST(ObjRender, SveRenderSample)
{
	std::vector<std::string> paths;
	std::vector<std::string> metas;
	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-P-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-12-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-13-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-14-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-15-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-16-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-17-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-18-01.gxxml");

	paths.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.obj");
	metas.push_back("../../testData/Model/미호천하행/BR-D-00008-S-19-01.gxxml");

	objrend::loadObj(paths, metas);

	int imgW = 5456;
	int imgH = 3632;
	int winH = imgH / subSample;
	/// APSC (25.1×16.7 mm, 1.503:1.0) with 18mm, fov = 46.32;
	float fovH = 46.32f;
	fovH = 45.92798521;

	/// boresight
	double bore_o, bore_p, bore_k;
	bore_o = 90.0 * deg2rad;
	bore_p = 0.0 * deg2rad;
	bore_k = 0.0 * deg2rad;

	double pc[3];
	pc[0] = 348727.134;
	pc[1] = 4049759.916;
	pc[2] = 53.851;

	//double nx = pc[0], ny = pc[1], nz = pc[2];
	//objrend::normalizePt(nx, ny, nz);

	double o, p, k;
	o = 0.302124 * deg2rad;
	p = 0.05518472 * deg2rad;
	k = 108.298326 * deg2rad;

	objrend::simulatePhoto2(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_017_new.bmp",
		false);

	pc[0] = 348711.39;
	pc[1] = 4049817.05;
	pc[2] = 54.227;

	//nx = pc[0];
	//ny = pc[1];
	//nz = pc[2];

	//objrend::normalizePt(nx, ny, nz);

	o = -0.1583322 * deg2rad;
	p = 0.1228959 * deg2rad;
	k = -255.609205 * deg2rad;

	objrend::simulatePhoto2(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_013.bmp",
		false);

	pc[0] = 348730.78;
	pc[1] = 4049738.69;
	pc[2] = 51.76;

	//nx = pc[0];
	//ny = pc[1];
	//nz = pc[2];

	//objrend::normalizePt(nx, ny, nz);

	o = -0.2541518 * deg2rad;
	p = 0.02611797 * deg2rad;
	k = -250.4444871 * deg2rad;

	objrend::simulatePhoto2(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_085.bmp",
		false);

	pc[0] = 348713.8;
	pc[1] = 4049795.92;
	pc[2] = 53.58821559;

	//nx = pc[0];
	//ny = pc[1];
	//nz = pc[2];

	//objrend::normalizePt(nx, ny, nz);

	o = -0.1943272 * deg2rad;
	p = -0.2761481 * deg2rad;
	k = -250.1265522 * deg2rad;

	objrend::simulatePhoto2(imgW, imgH, fovH,
		o, p, k, pc[0], pc[1], pc[2],
		bore_o, bore_p, bore_k, 0.0, 0.0, 0.0, float(double(winH) / double(imgH)), "RenderCaptureTest_018.bmp",
		false);

	EXPECT_EQ(1, 1);
	EXPECT_TRUE(true);
}

*/