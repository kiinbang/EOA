// PhotogrammetricSim.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#ifndef GOOGLE_GLOG_DLL_DECL
#define GOOGLE_GLOG_DLL_DECL
#endif

#ifndef GLOG_NO_ABBREVIATED_SEVERITIES
#define GLOG_NO_ABBREVIATED_SEVERITIES
#endif

#include <glog/logging.h>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

namespace bfs = boost::filesystem;
namespace bpo = boost::program_options;

#include <iostream>
#include <locale>
#include <string>

#include "ssm/SSMPhotoSimulation.h"

void initLog(char* argv0)
{
	char logFolderPath[_MAX_PATH];
	const char* varName = "glogfolder";
	size_t len;

	getenv_s(&len, logFolderPath, 80, varName);
	// Initialize Google's logging library.
	google::InitGoogleLogging(argv0);
	FLAGS_log_dir = logFolderPath;

	LOG(INFO) << "glog is initialized.";
}

std::string commandLineParser(const int argc, char** argv, const std::string& key)
{
	for (int i = 1; i < argc; ++i)
	{
		if (std::string(argv[i]).compare(key) == 0)
			return argv[i + 1];
	}

	return std::string("");
}

void onSimPath(std::string simPath)
{
	std::cout << "Simulation file path: " << simPath << '\n';
}

void onRetPath(std::string retPath)
{
	std::cout << "Simulation result file path: " << retPath << '\n';
}

int main(int argc, char* argv[])
{
	///glog initialization
	initLog(argv[0]);

	/// Simulation file path
	std::string simPath = commandLineParser(argc, argv, std::string("--sim"));
	/// Result file path
	std::string retPath = commandLineParser(argc, argv, std::string("--ret"));

	bpo::options_description desc{ "Options" };
	desc.add_options()
		("help", "Help screen")
		("simpath,s", bpo::value<std::string>(&simPath)->notifier(onSimPath), "*.sim_path")
		("retpath,r", bpo::value<std::string>(&retPath)->notifier(onRetPath), "*.ret_path");

	bpo::command_line_parser parser(argc, argv);
	parser.
		options(desc).
		allow_unregistered().
		style(
			bpo::command_line_style::default_style |
			bpo::command_line_style::allow_slash_for_short
		);

	bpo::parsed_options parsed_options = parser.run();

	bpo::variables_map vm;
	bpo::store(parsed_options, vm);
	bpo::notify(vm);

	if (vm.count("help"))
		std::cout << desc << '\n';

	std::cout << desc << '\n';

	if (argc < 3)
	{
		std::cout << desc << std::endl;
		return 0;
	}

	CSMPhotoSimulation sim;
	LOG(INFO) << "Run the simulator for " << simPath;
	try
	{
	
		if (!sim.RunSimulation(simPath, retPath))
		{
			std::cout << "!!! FAILURE:\nRunSimulation return false." << std::endl;
			LOG(INFO) << ("RunSimulation return false. \t" + simPath);
		}
	}
	catch (std::runtime_error err)
	{
		std::cout << "!!! FAILURE: " << std::endl;
		std::cout << err.what() << std::endl; 
		LOG(INFO) << (err.what() + std::string("\t") + simPath);
	}
	catch(...)
	{
		LOG(INFO) << ("Error from RunSimulation: \t" + simPath);
	}

	return 1;
}