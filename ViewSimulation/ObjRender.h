#pragma once

#ifdef GLRENDER_EXPORT
#define GLRENDER_API __declspec(dllexport)
#else
#define GLRENDER_API __declspec(dllimport)
#endif

#define API GLRENDER_API

#include <string>
#include <vector>

namespace objrend
{
	const double falseCoord = -999.999;
	bool API loadObj(const std::vector<std::string>& objPaths, const std::vector <std::string>& metaPaths);

	void API normalizePt(double& x, double& y, double& z);

	void API deNormalizePt(double& x, double& y, double& z);

	void API simulatePhoto(int imgW, const int imgH, const float fovH,
		double omg, const double phi, const double kap,
		const double x0, const double y0, const double z0,
		const double domg, const double dphi, const double dkap,
		const double dx, const double dy, const double dz,
		const float outImgScale,
		const std::string& outPath,
		const bool show);

	std::vector<bool> API getImg2ObjCoords(int imgW, const int imgH, const float fovH,
		double omg, const double phi, const double kap,
		const double x0, const double y0, const double z0,
		const double domg, const double dphi, const double dkap,
		const double dx, const double dy, const double dz,
		const float outImgScale,
		const std::vector<int>& imgCol,
		const std::vector<int>& imgRow,
		std::vector<double>& Xa,
		std::vector<double>& Ya,
		std::vector<double>& Za);

	std::vector<bool> API getObjCoord(const std::vector<int>& imgCol,
		const std::vector<int>& imgRow,
		std::vector<double>& Xa,
		std::vector<double>& Ya,
		std::vector<double>& Za);

	void API setNearFar(const double nearVal, const double farVal);

	void API renderFBO();
	void API displayFBO();
}