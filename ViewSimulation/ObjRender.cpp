#include "ObjRender.h"
#include <ssm/include/viewsim.hpp>

namespace objrend
{
	static globj::ViewSim viewRender;

	bool loadObj(const std::vector<std::string>& objPaths, const std::vector <std::string>& metaPaths)
	{
		return viewRender.load(objPaths, metaPaths);
	}

	void normalizePt(double& x, double& y, double& z)
	{
		tobj::Vertex pc;
		pc[0] = x;
		pc[1] = y;
		pc[2] = z;

		auto nPc = globj::getNormalizedVtx(pc);

		x = nPc[0];
		y = nPc[1];
		z = nPc[2];
	}

	void deNormalizePt(double& x, double& y, double& z)
	{
		tobj::Vertex nPc;
		nPc[0] = x;
		nPc[1] = y;
		nPc[2] = z;

		auto pc = globj::getDenormalizedVtx(nPc);

		x = pc[0];
		y = pc[1];
		z = pc[2];
	}

	void simulatePhoto(const int imgW, const int imgH, const float fovH,
		const double omg, const double phi, const double kap,
		const double x0, const double y0, const double z0,
		const double domg, const double dphi, const double dkap,
		const double dx, const double dy, const double dz,
		const float outImgScale,
		const std::string& outPath,
		const bool show)
	{
		/// geo-referencing with alignment (boresight(Rb2m), leverarm(T)) 
		/// Xa = X0 + Rb2m * T + S * Rb2m * Rc2b * (x, y, -f) 
		/// S * Rb2m * Rc2b * (x, y, -f) = Xa -  X0 - Rb2m * T 
		/// S *  (x, y, -f) = (Rb2c * Rm2b)(Xa -  X0) - (Rb2c * Rm2b) * Rb2m * T 
		/// S *  (x, y, -f) = (q, r, s) = (Rb2c * Rm2b)(Xa -  X0) - Rb2c * T 
		/// x = -f (r/q) 
		/// y = -f (s/q)

		/// EOP derivation 
		/// S* (x, y, -f) = (Rb2c * Rm2b)(Xa - X0) - Rb2c * T 
		/// S * (x, y, -f) = (Rb2c * Rm2b)((Xa - X0) - Rb2m * T) 
		/// S * (x, y, -f) = (Rb2c * Rm2b)(Xa - (X0 - Rb2m * T)) 
		/// Orientation Matrix: Rb2c * Rm2b 
		/// Perspective center: (X0 - Rb2m * T)

		try
		{
			double nx, ny, nz;
			globj::getNormalized(x0, y0, z0, nx, ny, nz);

			/// body rotation
			auto bodyR = math::rotation::getRMat(omg, phi, kap);
			/// lever-arm
			math::Matrix<double> T(3, 1);
			T(0, 0) = dx;
			T(1, 0) = dy;
			T(2, 0) = dz;
			/// perspective center
			math::Matrix<double> pc(3, 1);
			pc(0, 0) = nx;
			pc(1, 0) = ny;
			pc(2, 0) = nz;
			pc = pc + bodyR % T;
			/// boresight
			auto boreR = math::rotation::getRMat(domg, dphi, dkap);
			/// camera rotation w.r.t a mapping frame
			auto R = bodyR % boreR;
			
			math::Matrix<double> camYDir(3, 1);
			camYDir(0, 0) = 0.0;
			camYDir(1, 0) = 1.0;
			camYDir(2, 0) = 0.0;

			camYDir = R % camYDir;

			/// camera direction
			math::Matrix<double> camDir(3, 1);
			camDir(0, 0) = 0.0;
			camDir(1, 0) = 0.0;
			camDir(2, 0) = -1.0;

			camDir = R % camDir * 5.0;

			/// looking at
			auto tar = camDir + pc;

			/// set view direction
			globj::setupEye(pc(0, 0), pc(1, 0), pc(2, 0),
				tar(0, 0), tar(1, 0), tar(2, 0),
				camYDir(0, 0), camYDir(1, 0), camYDir(2, 0));

			/// set fov along the image height
			viewRender.setFov(fovH);

			double apsc_w_ratio = double(imgW) / double(imgH);
			int winH;
			winH = int(float(imgH) * outImgScale + 0.5);

			if (winH % 2 != 0)
				winH -= 1; /// even number

			int winW = int(apsc_w_ratio * double(winH) + 0.5);

			if (winW % 2 != 0)
				winW -= 1; /// even number

			if (show)
			{
				/// initialize glut
				viewRender.init(winW, winH, "glrender", false);

				viewRender.showObj();
			}
			else
			{
				/// initialize glut
				viewRender.init(winW, winH, "winHided", true);

				viewRender.saveRender(std::string(outPath));
			}
		}
		catch (...)
		{
			throw std::runtime_error("Error in simulatePhoto");
		}
	}

	std::vector<bool> getImg2ObjCoords(int imgW, const int imgH, const float fovH,
		double omg, const double phi, const double kap,
		const double x0, const double y0, const double z0,
		const double domg, const double dphi, const double dkap,
		const double dx, const double dy, const double dz,
		const float outImgScale,
		const std::vector<int>& imgCol,
		const std::vector<int>& imgRow,
		std::vector<double>& Xa,
		std::vector<double>& Ya,
		std::vector<double>& Za)
	{
		/// geo-referencing with alignment (boresight(Rb2m), leverarm(T)) 
		/// Xa = X0 + Rb2m * T + S * Rb2m * Rc2b * (x, y, -f) 
		/// S * Rb2m * Rc2b * (x, y, -f) = Xa -  X0 - Rb2m * T 
		/// S *  (x, y, -f) = (Rb2c * Rm2b)(Xa -  X0) - (Rb2c * Rm2b) * Rb2m * T 
		/// S *  (x, y, -f) = (q, r, s) = (Rb2c * Rm2b)(Xa -  X0) - Rb2c * T 
		/// x = -f (r/q) 
		/// y = -f (s/q)

		/// EOP derivation 
		/// S* (x, y, -f) = (Rb2c * Rm2b)(Xa - X0) - Rb2c * T 
		/// S * (x, y, -f) = (Rb2c * Rm2b)((Xa - X0) - Rb2m * T) 
		/// S * (x, y, -f) = (Rb2c * Rm2b)(Xa - (X0 - Rb2m * T)) 
		/// Orientation Matrix: Rb2c * Rm2b 
		/// Perspective center: (X0 - Rb2m * T)

		std::vector<bool> retVal;

		try
		{
			double nx, ny, nz;
			globj::getNormalized(x0, y0, z0, nx, ny, nz);

			/// body rotation
			auto bodyR = math::rotation::getRMat(omg, phi, kap);
			/// lever-arm
			math::Matrix<double> T(3, 1);
			T(0, 0) = dx;
			T(1, 0) = dy;
			T(2, 0) = dz;
			/// perspective center
			math::Matrix<double> pc(3, 1);
			pc(0, 0) = nx;
			pc(1, 0) = ny;
			pc(2, 0) = nz;
			pc = pc + bodyR % T;
			/// boresight
			auto boreR = math::rotation::getRMat(domg, dphi, dkap);
			/// camera rotation w.r.t a mapping frame
			auto R = bodyR % boreR;
			
			math::Matrix<double> camYDir(3, 1);
			camYDir(0, 0) = 0.0;
			camYDir(1, 0) = 1.0;
			camYDir(2, 0) = 0.0;

			camYDir = R % camYDir;

			/// camera direction
			math::Matrix<double> camDir(3, 1);
			camDir(0, 0) = 0.0;
			camDir(1, 0) = 0.0;
			camDir(2, 0) = -1.0;

			camDir = R % camDir * 5.0;

			/// looking at
			auto tar = camDir + pc;

			/// set view direction
			globj::setupEye(pc(0, 0), pc(1, 0), pc(2, 0),
				tar(0, 0), tar(1, 0), tar(2, 0),
				camYDir(0, 0), camYDir(1, 0), camYDir(2, 0));

			/// set fov along the image height
			viewRender.setFov(fovH);

			double apsc_w_ratio = double(imgW) / double(imgH);
			int winH;
			winH = int(float(imgH) * outImgScale + 0.5);

			if (winH % 2 != 0)
				winH -= 1; /// even number

			int winW = int(apsc_w_ratio * double(winH) + 0.5);

			if (winW % 2 != 0)
				winW -= 1; /// even number

			/// initialize glut
			viewRender.init(winW, winH, "winHided", true);

			retVal = viewRender.getObjCoords(imgCol, imgRow, Xa, Ya, Za);
		}
		catch (...)
		{
			throw std::runtime_error("Error in simulatePhoto");
		}

		return retVal;
	}

	std::vector<bool> getObjCoord(const std::vector<int>& imgCol,
		const std::vector<int>& imgRow,
		std::vector<double>& Xa,
		std::vector<double>& Ya,
		std::vector<double>& Za)
	{
		auto retVal = viewRender.getObjCoords(imgCol, imgRow, Xa, Ya, Za);
		return retVal;
	}

	void setNearFar(const double nearVal, const double farVal)
	{
		globj::setNearFar(nearVal, farVal);
	}
}