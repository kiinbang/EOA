#pragma once

#include <string>

#include <bitmap-master/bitmap_image.hpp>
#include <freeglut/include/GL/freeglut.h>

#include "datastructs.h"

/// old codes
namespace globj2
{
	/// texture id
	static GLuint texName;
	/// window width and height
	static int win_width, win_height;
	static int gButton;
	static int startx, starty;
	/// shade flag
	static int shadeFlag = 1;
	/// vertex ids of a face
	static std::vector<tobj::FaceVtxIdx> fVtxIds;
	/// face normal vectors
	static std::vector<tobj::Vertex> faceNormals;
	///	vertex normal vectors
	static std::vector<tobj::Vertex> vtxNormalV;
	/// vertices
	static std::vector<tobj::Vertex> vertices;
	/// vertex colors
	static std::vector<globj::VC> vtxColors;
	static bool initColor = false;
	/// number of vertices of each file
	static std::vector<size_t> nVertices;
	///	normalized vertices
	static std::vector<tobj::Vertex> normalizedVtxs;
	/// arcball object
	static globj::CArcball arcball;
	/// Camera looks at
	static globj::Pos3D eyeXyz(0, 0, 1);
	static globj::Pos3D centerXyz(0, 0, 0);
	static globj::Pos3D upXyz(0, 1, 0);
	/// zoom factor
	static float zoomFactor = 1.0f;
	/// field of view in degrees
	static double fov = 40.0;
	/// set light
	static bool setLight = false;
	/// screen capture path
	static std::string outImgPath;
	/// shift, center of vertices
	static tobj::Vertex center;
	/// translation
	static globj::Pos3D objTrans;
	/// quaternion
	static SSMATICS_EOA::CSMQuaternion objRot;

	/// save gl screen
	inline void saveScreen(const std::string& path)
	{
		// Image Writing	
		unsigned int nCh = 3;
		uint8_t* imageData = new uint8_t[win_width * win_width * nCh];

		//glPixelStorei(GL_PACK_ALIGNMENT, 1);
		glReadBuffer(GL_FRONT);
		glReadPixels(GLint(0), GLint(0), GLsizei(win_width), GLsizei(win_height), GL_RGB, GL_UNSIGNED_BYTE, imageData);

		//write 
		bitmap_image image(win_width, win_height);
		bitmap_image imageBack(win_width, win_height);
		//image_drawer draw(image);

		const auto H = image.height();
		const auto W = image.width();
		int idx = 0;
		for (unsigned int r = 0; r < H; ++r)
		{
			for (unsigned int c = 0; c < W; ++c)
			{
				auto rIdx = idx++;
				auto gIdx = idx++;
				auto bIdx = idx++;

				//auto A = *(imageData[c++]);
				unsigned int red = static_cast<unsigned int>(imageData[rIdx]);
				unsigned int green = static_cast<unsigned int>(imageData[gIdx]);
				unsigned int blue = static_cast<unsigned int>(imageData[bIdx]);

				image.set_pixel(c, (H - 1) - r, red, green, blue);
			}
		}

		if (image.save_image(path))
			std::cout << "Succeed in saving: " << path << std::endl;

		delete[] imageData;
	}

	///	Convert quoternion to OpenGL matrix
	inline void convert(const SSMATICS_EOA::CSMQuaternion& q, double(&m)[16])
	{
		double l = q * q;
		double s = 2.0 / l;
		double xs = q.x * s;
		double ys = q.y * s;
		double zs = q.z * s;

		double wx = q.w * xs;
		double wy = q.w * ys;
		double wz = q.w * zs;

		double xx = q.x * xs;
		double xy = q.x * ys;
		double xz = q.x * zs;

		double yy = q.y * ys;
		double yz = q.y * zs;
		double zz = q.z * zs;

		m[0 * 4 + 0] = 1.0 - (yy + zz);
		m[1 * 4 + 0] = xy - wz;

		m[2 * 4 + 0] = xz + wy;
		m[0 * 4 + 1] = xy + wz;
		m[1 * 4 + 1] = 1.0 - (xx + zz);
		m[2 * 4 + 1] = yz - wx;

		m[0 * 4 + 2] = xz - wy;
		m[1 * 4 + 2] = yz + wx;
		m[2 * 4 + 2] = 1.0 - (xx + yy);


		m[0 * 4 + 3] = 0.0;

		m[1 * 4 + 3] = 0.0;
		m[2 * 4 + 3] = 0.0;
		m[3 * 4 + 0] = m[3 * 4 + 1] = m[3 * 4 + 2] = 0.0;
		m[3 * 4 + 3] = 1.0;
	};

	/// setup light
	inline void setupLight(const globj::Pos3D& pos0, const globj::Pos3D& pos1)
	{
		GLfloat lightOnePosition[4] = { float(pos0[0]), float(pos0[1]), float(pos0[2]), 0 };
		glLightfv(GL_LIGHT1, GL_POSITION, lightOnePosition);
		GLfloat lightTwoPosition[4] = { float(pos0[0]), float(pos0[1]), float(pos0[2]), 0 };
		glLightfv(GL_LIGHT2, GL_POSITION, lightTwoPosition);
	}

	/// define view transform matrix
	/// transform from the eye coordinate system to the world system
	inline void setupEye(const double eyeX, const double eyeY, const double eyeZ,
		const double cX, const double cY, const double cZ,
		const double upX, const double upY, const double upZ)
	{
		eyeXyz[0] = eyeX;
		eyeXyz[1] = eyeY;
		eyeXyz[2] = eyeZ;

		centerXyz[0] = cX;
		centerXyz[1] = cY;
		centerXyz[2] = cZ;

		upXyz[0] = upX;
		upXyz[1] = upY;
		upXyz[2] = upZ;

		glLoadIdentity();
		gluLookAt(eyeXyz[0], eyeXyz[1], eyeXyz[2],
			centerXyz[0], centerXyz[1], centerXyz[2],
			upXyz[0], upXyz[1], upXyz[2]);

#ifdef _DEBUG
		//std::cout << "eyeXyz: " << eyeXyz[0] << "\t" << eyeXyz[1] << "\t" << eyeXyz[2] << std::endl;
		//std::cout << "centerXyz: " << centerXyz[0] << "\t" << centerXyz[1] << "\t" << centerXyz[2] << std::endl;
		//std::cout << "upXyz: " << upXyz[0] << "\t" << upXyz[1] << "\t" << upXyz[2] << std::endl;
#endif
	}

	inline void setupEye(const globj::Pos3D& newEyeXyz, const globj::Pos3D& newCenterXyz, const globj::Pos3D& newUpXyz)
	{
		setupEye(newEyeXyz[0], newEyeXyz[1], newEyeXyz[2],
			newCenterXyz[0], newCenterXyz[1], newCenterXyz[2],
			newUpXyz[0], newUpXyz[1], newUpXyz[2]);
	}

	/// setup the object, transform from the world to the object coordinate system
	inline void setupObject()
	{
		double rot[16];
		glTranslated(objTrans[0], objTrans[1], objTrans[2]);
		convert(objRot, rot);
		glMultMatrixd((GLdouble*)rot);
	}

	inline void draw_object()
	{
		if (!initColor)
		{
			vtxColors.clear();
			vtxColors.resize(normalizedVtxs.size());
			for (auto& c : vtxColors)
			{
				c.r = c.g = c.b = 0.0f;
			}
		}

		glBindTexture(GL_TEXTURE_2D, texName);

		for (size_t f = 0; f < faceNormals.size(); f++)
		{
			glBegin(GL_TRIANGLES);

			if (shadeFlag == 0)
			{
				glNormal3f(float(faceNormals[f][0]), float(faceNormals[f][1]), float(faceNormals[f][2]));
			}

			const tobj::FaceVtxIdx& vtxIds = fVtxIds[f];

			for (int v = 0; v < 3; v++)
			{
				const auto& vtxN = vtxNormalV[vtxIds[v]];

				if (shadeFlag == 1)
				{
					glNormal3f(GLfloat(vtxN[0]), GLfloat(vtxN[1]), GLfloat(vtxN[2]));
				}

				if (!initColor)
				{
					//glColor3f(235 / 255.0, 180 / 255.0, 173 / 255.0);
					float max = float(fabs(vtxN[0]) > fabs(vtxN[1]) ? fabs(vtxN[0]) : fabs(vtxN[1]));
					max = max > fabs(vtxN[2]) ? max : float(fabs(vtxN[2]));
					vtxColors[vtxIds[v]].r = GLfloat(fabs(vtxN[0])) / max;
					vtxColors[vtxIds[v]].g = GLfloat(fabs(vtxN[1])) / max;
					vtxColors[vtxIds[v]].b = GLfloat(fabs(vtxN[2])) / max;
				}

				glColor3f(vtxColors[vtxIds[v]].r, vtxColors[vtxIds[v]].g, vtxColors[vtxIds[v]].b);

				const auto& p = normalizedVtxs[vtxIds[v]];
				glVertex3d(p[0], p[1], p[2]);
			}

			glEnd();
		}

		if (!initColor)
			initColor = true;
	}

	/// display call back function
	inline void display()
	{
		/* clear frame buffer */
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/// lights
		if (setLight)
		{
			globj::Pos3D pos0, pos1;
			pos0[0] = 0.0;
			pos0[1] = 0.0;
			pos0[2] = 1.0;

			pos1[0] = centerXyz[0] - eyeXyz[0];
			pos1[1] = centerXyz[1] - eyeXyz[1];
			pos1[2] = centerXyz[2] - eyeXyz[2];

			auto l = sqrt(pos1[0] * pos1[0] + pos1[1] * pos1[1] + pos1[2] * pos1[2]);
			pos1[0] /= l;
			pos1[1] /= l;
			pos1[2] /= l;

			setupLight(pos0, pos1);
		}

		/* transform from the eye coordinate system to the world system */
		setupEye(eyeXyz, centerXyz, upXyz);
		glPushMatrix();

		/* transform from the world to the ojbect coordinate system */
		setupObject();
		/* draw the mesh */
		draw_object();
		//for(const auto& obj : objInstances)
		//	draw_object(obj);

		glPopMatrix();
		glutSwapBuffers();
	}

	/// Called when a "resize" event is received by the window.
	inline void reshape(int w, int h)
	{
		win_width = w;
		win_height = h;
		float ar = (float)(w) / h;
		/// set viewport
		glViewport(0, 0, w, h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		double zNear = 1.0;
		double zFar = 100.0;
		// magic imageing commands
		gluPerspective(fov, /* field of view in degrees */
			ar, /* aspect ratio */
			zNear, /* Z near */
			zFar /* Z far */);

		glMatrixMode(GL_MODELVIEW);

		glutPostRedisplay();
	}

	/// setup GL states
	inline void setupGLstate()
	{
		GLfloat lightOneColor[] = { 1, 1, 1, 1 };
		GLfloat globalAmb[] = { .1f, .1f, .1f, 1.f };
		GLfloat light1Position[] = { 0,  0, 1, 0 };

		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CCW);
		//glFrontFace(GL_CW);
		glEnable(GL_DEPTH_TEST);
		//glClearColor(0, 0, 0, 1); /// black bg
		glClearColor(1.0, 1.0, 1.0, 1.0); /// white bg
		glShadeModel(GL_SMOOTH);

		glEnable(GL_LIGHT1);
		glEnable(GL_LIGHT2);
		//glEnable(GL_LIGHT3);
		//glEnable(GL_LIGHT4);
		glEnable(GL_LIGHTING);
		glEnable(GL_NORMALIZE);
		glEnable(GL_COLOR_MATERIAL);

		glLightfv(GL_LIGHT1, GL_DIFFUSE, lightOneColor);
		glLightfv(GL_LIGHT2, GL_DIFFUSE, lightOneColor);
		//glLightfv(GL_LIGHT3, GL_DIFFUSE, lightOneColor);
		//glLightfv(GL_LIGHT4, GL_DIFFUSE, lightOneColor);
		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalAmb);
		glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

		//glColorMaterial(GL_BACK, GL_AMBIENT_AND_DIFFUSE);

		glLightfv(GL_LIGHT1, GL_POSITION, light1Position);
	}

	inline void getObjCoord(int col, int row, double& X, double& Y, double& Z)
	{
		/// The codes below are from lidarviewer
		/// Found issue: it works correctly in lidarviewer, but it produces wrong obj-coords after rotating or shifting object space.
		/// Without rotate or shift, it looks fine.
		GLint viewport[4];
		GLdouble modelview[16];
		GLdouble projection[16];
		GLfloat winX, winY, winZ;
		GLdouble posX, posY, posZ;

		//Once we have the viewport information, we can grab the Modelview information. The Modelview Matrix determines how the vertices of OpenGL primitives are transformed to eye coordinates.
		glGetDoublev(GL_MODELVIEW_MATRIX, modelview);// Retrieve The Modelview Matrix
		//After that, we need to get the Projection Matrix. The Projection Matrix transforms vertices in eye coordinates to clip coordinates. 	
		glGetDoublev(GL_PROJECTION_MATRIX, projection);// Retrieve The Projection Matrix
		//We need to grab the current viewport. The information we need is the starting X and Y position of our GL viewport along with the viewport width and height.
		//Once we get this information using glGetIntegerv(GL_VIEWPORT, viewport), viewport will hold the following information: 
		glGetIntegerv(GL_VIEWPORT, viewport);// Retrieves The Viewport Values (X, Y, Width, Height)

		winX = (float)col;
		winY = (float)viewport[3] - (float)row;

		//You may have noticed the missing z coordinate, well here is how to get it: 	
		glReadPixels(col, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

		//gluUnProject converts Windows screen coordinates to OpenGL coordinates.
		gluUnProject(winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);

		X = (double)posX + center[0];
		Y = (double)posY + center[1];
		Z = (double)posZ + center[2];
	}

	/// mouse click call back function
	inline void  mouseClick(int button, int state, int x, int y)
	{

#ifdef _DEBUG
		if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
		{
			double X, Y, Z;
			getObjCoord(x, y, X, Y, Z);
			std::cout.setf(std::ios::floatfield, std::ios::fixed);
			std::cout.precision(3);
			std::cout << "col, row, X0, Y0, Z0: " << x << "\t" << y << "\t" << X << "\t" << Y << "\t" << Z << std::endl;
			std::cout << "X, Y, Z: " << X - center[0] << "\t" << Y - center[1] << "\t" << Z - center[2] << std::endl;
		}
#endif

		/// set up an arcball around the Eye's center
		/// switch y coordinates to right handed system

		if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
		{
			gButton = GLUT_LEFT_BUTTON;
			arcball = globj::CArcball(win_width, win_height, x - win_width / 2, win_height - y - win_height / 2);
		}

		if (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN)
		{
			startx = x;
			starty = y;
			gButton = GLUT_MIDDLE_BUTTON;
		}

		if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
		{
			startx = x;
			starty = y;
			gButton = GLUT_RIGHT_BUTTON;
		}

		return;
	}

	//Define the callback function.The second parameter gives the direction of the scroll.Values of + 1 is forward, -1 is backward.
	inline void mouseWheel(int button, int dir, int x, int y)
	{
		double scale = 50. / win_height;

#ifdef _DEBUG
		std::cout << "eyeXyz[0] - centerXyz[0]: " << eyeXyz[0] - centerXyz[0] << std::endl;
#endif

		auto dx = (eyeXyz[0] - centerXyz[0]);
		auto dy = (eyeXyz[1] - centerXyz[1]);
		auto dz = (eyeXyz[2] - centerXyz[2]);
		auto l = sqrt(dx * dx + dy * dy + dz * dz);

		if (dir > 0)
		{
			eyeXyz[0] += 1.0 / 5.0 * dx / l;
			eyeXyz[1] += 1.0 / 5.0 * dy / l;
			eyeXyz[2] += 1.0 / 5.0 * dz / l;
		}
		else
		{
			eyeXyz[0] -= 1.0 / 5.0 * dx / l;
			eyeXyz[1] -= 1.0 / 5.0 * dy / l;
			eyeXyz[2] -= 1.0 / 5.0 * dz / l;
		}


		glLoadIdentity();

		gluLookAt(eyeXyz[0], eyeXyz[1], eyeXyz[2],
			centerXyz[0], centerXyz[1], centerXyz[2],
			upXyz[0], upXyz[1], upXyz[2]);

		glutPostRedisplay();

#ifdef _DEBUG
		std::cout << "eyeXyz[0]: " << eyeXyz[0] << std::endl;
		std::cout << "centerXyz[0]: " << centerXyz[0] << std::endl;
		std::cout << "eyeXyz[0] - centerXyz[0]: " << eyeXyz[0] - centerXyz[0] << std::endl;
		std::cout << "1.0 / 5.0 * dx / l: " << 1.0 / 5.0 * dx / l << std::endl;
#endif

		return;

		/// following codes are not available for now
		/// Just keep for reference
		/// 
		/*
		if (dir > 0)
		{
			objTrans = objTrans + globj::CPoint(0, 0, scale);
			glutPostRedisplay();
		}
		else
		{
			objTrans = objTrans + globj::CPoint(0, 0, -scale);
			glutPostRedisplay();
		}
		*/

		return;
	}

	/// mouse motion call back function
	inline void mouseMove(int x, int y)
	{
		globj::CPoint trans;
		SSMATICS_EOA::CSMQuaternion rot;

		/// rotation, call arcball
		if (gButton == GLUT_LEFT_BUTTON)
		{
			rot = arcball.update(x - win_width / 2, win_height - y - win_height / 2);
			objRot = rot % objRot;
			glutPostRedisplay();
		}

		/*xy translation */
		if (gButton == GLUT_MIDDLE_BUTTON)
		{
			double scale = 10. / win_height;
			trans = globj::CPoint(scale * (x - startx), scale * (starty - y), 0);
			startx = x;
			starty = y;
			objTrans = objTrans + trans;
			glutPostRedisplay();
		}

		/* zoom in and out */
		if (gButton == GLUT_RIGHT_BUTTON)
		{
			double scale = 10. / win_height;
			trans = globj::CPoint(0, 0, scale * (starty - y));
			startx = x;
			starty = y;
			objTrans = objTrans + trans;
			glutPostRedisplay();
		}

	}

	/// helper function to remind the user about commands, hot keys
	inline void help()
	{
		printf("w - Wireframe Display\n");

		printf("f  -  Flat Shading \n");
		printf("s  -  Smooth Shading\n");
		//printf("t  -  Texture Mapping\n");
		printf("r  -  Reset the view\n");
		//printf("g  -  Toggle Geometry and UV view\n");

		printf("o  -  Save frame buffer to screen_image.bmp\n");
		printf("? -  Help Information\n");
		printf("esc - quit\n");
	}

	// keyboard call back function
	inline void keyBoard(unsigned char key, int x, int y)
	{
		switch (key)
		{
		case 'f':
			//Flat Shading
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			shadeFlag = 0;
			break;
		case 's':
			//Smooth Shading
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			shadeFlag = 1;
			break;
		case 'w':
			//Wireframe mode
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			break;

			/*
			case 't':
				textureFlag = (textureFlag + 1) % 3;
				switch (textureFlag)
				{
				case 0:
					glDisable(GL_TEXTURE_2D);
					break;
				case 1:
					glEnable(GL_TEXTURE_2D);
					glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
					break;
				case 2:
					glEnable(GL_TEXTURE_2D);
					glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
					break;
				}
				break;
			*/

			/*
			case 'g':
				geometryFlag = (geometryFlag + 1) % 2;
				break;
			*/

		case 'r':
			objRot = SSMATICS_EOA::CSMQuaternion(1, 0, 0, 0);
			objTrans = globj::CPoint(0, 0, 0);
			break;

		case '?':
			help();
			break;

		case 27:
			exit(0);
			break;

		case 'o':
			saveScreen(outImgPath);
			break;
		}

		glutPostRedisplay();
	}

	class ViewSim
	{
	public:

		ViewSim()
		{
			/// translation
			objTrans[0] = 0.0;
			objTrans[1] = 0.0;
			objTrans[2] = 0.0;
			/// quaternion (rotation)
			objRot.w = 1.0;
			objRot.x = 0.0;
			objRot.y = 0.0;
			objRot.z = 0.0;

			startx = 0;
			starty = 0;

			/// set light
			setLight = false;
			/// screen capture path
			outImgPath = "temp.bmp";

			glutInitialization = false;
		}

		bool load(const std::vector<std::string>& objPaths, const std::vector <std::string>& metaPaths)
		{
			/// clear all drawing data
			fVtxIds.clear();
			faceNormals.clear();
			vtxNormalV.clear();
			vertices.clear();
			nVertices.clear();
			normalizedVtxs.clear();

			size_t nFiles = objPaths.size();
			nVertices.resize(nFiles);

			for (size_t i = 0; i < nFiles; ++i)
			{
				nVertices[i] = 0;

				std::string name(objPaths[i]);

				size_t pos = name.find_last_of(".");
				std::string type = name.substr(pos + 1);

				if (type != "obj")
				{
					std::cout << "Not obj file." << std::endl;
					continue;
				}

				std::shared_ptr<tobj::ObjLoad> obj = tobj::createObjLoad(tobj::_TObjLoad_);

				obj = tobj::createObjLoad(tobj::_TObjLoad_);
				tobj::Meta meta = tobj::readModelConfigWithBOM(metaPaths[i].c_str());
				obj->load(name.c_str(), meta);

				/// previous size of vertices
				auto oldSize = vertices.size();
				/// extend vertex vector
				vertices.reserve(oldSize + obj->numVertices());
				/// new added vertices
				std::vector<tobj::Vertex> newVertices(obj->numVertices());

				for (size_t v = 0; v < obj->numVertices(); ++v)
				{
					newVertices[v] = obj->getVertex(v);
				}

				/// save the number of vertices for each file
				nVertices[i] = obj->numVertices();
				/// insert new vertices
				vertices.insert(vertices.end(), newVertices.begin(), newVertices.end());

				/// compuate face normal vectors
				compute_normal(obj, i);
			}

			/// compute normalization parameters: center and scale(length)
			computeNormalization();

			/// normalized vertices
			normalizedVtxs.resize(vertices.size());
			for (size_t v = 0; v < vertices.size(); ++v)
			{
				normalizedVtxs[v] = getNormalizedVtx(vertices[v]);
			}

			return true;
		}

		void setupLoop(int argc, char* argv[],
			const int width, const int height,
			const char* winTitle)
		{
			shadeFlag = 1;

			/* glut stuff */
			/// initialize GLUT
			///	https://www.opengl.org/resources/libraries/glut/spec3/node10.html
			/// argc 
			///	A pointer to the program's unmodified argc variable from main. Upon return, the value pointed to by argcp will be updated, because glutInit extracts any command line options intended for the GLUT library.
			///	argv
			///	The program's unmodified argv variable from main. Like argcp, the data for argv will be updated because glutInit extracts any command line options understood by the GLUT library.

			glutInit(&argc, argv);

			glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
				GLUT_ACTION_GLUTMAINLOOP_RETURNS);

			glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH);
			glutInitWindowSize(width, height);
			std::cout << "width: " << width << std::endl;
			std::cout << "height: " << height << std::endl;
			/// Create window with given title
			glutCreateWindow(winTitle);
			/// vpX, vpY: lower left corner
			/// vpW, vpH: viewport widht and height
			int vpX = 0;
			int vpY = 0;
			int vpW = width;
			int vpH = height;
			glViewport(vpX, vpY, vpW, vpH);

			std::cout << "vpX: " << vpX << std::endl;
			std::cout << "vpY: " << vpY << std::endl;
			std::cout << "vpW: " << vpW << std::endl;
			std::cout << "vpH: " << vpH << std::endl;

			setupGLstate();

			///Set-up callback functions
			glutDisplayFunc(display);
			glutReshapeFunc(reshape);
			glutMouseFunc(mouseClick);
			glutMotionFunc(mouseMove);
			glutMouseWheelFunc(mouseWheel);
			glutKeyboardFunc(keyBoard);

			glutMainLoop();
		}

		void saveRender(int argc, char* argv[],
			const int width, const int height,
			const char* winTitle)
		{
			shadeFlag = 1;

			if (!this->glutInitialization)
			{
				/// initialize GLUT, refer to https://www.opengl.org/resources/libraries/glut/spec3/node10.html
				glutInit(&argc, argv);
				glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH);
				glutInitWindowSize(width, height);
				winidx = glutCreateWindow(winTitle);
				this->glutInitialization = true;
			}

			std::cout << "width: " << width << std::endl;
			std::cout << "height: " << height << std::endl;

			/// vpX, vpY: lower left corner
			/// vpW, vpH: viewport widht and height
			int vpX = 0;
			int vpY = 0;
			int vpW = width;
			int vpH = height;
			glViewport(vpX, vpY, vpW, vpH);

			std::cout << "vpX: " << vpX << std::endl;
			std::cout << "vpY: " << vpY << std::endl;
			std::cout << "vpW: " << vpW << std::endl;
			std::cout << "vpH: " << vpH << std::endl;

			setupGLstate();

			reshape(width, height);
			display();

			saveScreen(outImgPath);

			if (0)
			{
				//glutHideWindow();
				//glutLeaveMainLoop();
			}
		}

		void getCoords(int argc, char* argv[],
			const int width, const int height,
			const char* winTitle,
			std::vector<int>& x,
			std::vector<int>& y,
			std::vector<double>& X,
			std::vector<double>& Y,
			std::vector<double>& Z)
		{
			shadeFlag = 1;

			if (!this->glutInitialization)
			{
				/// initialize GLUT, refer to https://www.opengl.org/resources/libraries/glut/spec3/node10.html
				glutInit(&argc, argv);
				glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH);
				this->glutInitialization = true;
				glutInitWindowSize(width, height);
				//glutInitWindowSize(1, 1);
				/// Create window with given title
				winidx = glutCreateWindow(winTitle);
			}

			std::cout << "width: " << width << std::endl;
			std::cout << "height: " << height << std::endl;

			/// vpX, vpY: lower left corner
			/// vpW, vpH: viewport widht and height
			int vpX = 0;
			int vpY = 0;
			int vpW = width;
			int vpH = height;
			glViewport(vpX, vpY, vpW, vpH);

			std::cout << "vpX: " << vpX << std::endl;
			std::cout << "vpY: " << vpY << std::endl;
			std::cout << "vpW: " << vpW << std::endl;
			std::cout << "vpH: " << vpH << std::endl;

			setupGLstate();

			reshape(width, height);
			display();

			try
			{
				for (size_t i = 0; i < x.size(); ++i)
				{
					getObjCoord(x[i], y[i], X[i], Y[i], Z[i]);
				}
			}
			catch (...)
			{
				throw std::runtime_error("Error from getObjCoord in getCoords()");
			}
		}

		void setFov(const float newFov)
		{
			fov = newFov;
			std::cout << "new fov: " << fov << std::endl;
		}

		/// get normalized vertex coordinates
		tobj::Vertex getNormalizedVtx(const tobj::Vertex& vtx)
		{
			tobj::Vertex nVtx = vtx;

			for (int i = 0; i < 3; ++i)
			{
				nVtx[i] -= center[i];
			}

			return nVtx;
		}

		/// set light and export option and out img path
		void config(const bool light, const std::string& path)
		{
			/// set light
			setLight = light;
			/// screen capture export path
			outImgPath = path;
		}

	private:

		/// compute normalization parameters
		void computeNormalization()
		{
			double c[3];
			c[0] = c[1] = c[2] = 0.0;

			for (int j = 0; j < 3; ++j)
			{
				max[j] = std::numeric_limits<float>::min();
				min[j] = std::numeric_limits<float>::max();
			}

			for (size_t v = 0; v < vertices.size(); ++v)
			{
				for (int j = 0; j < 3; ++j)
				{
					c[j] += vertices[v][j];

					max[j] = (max[j] < vertices[v][j]) ? static_cast<float>(vertices[v][j]) : max[j];
					min[j] = (min[j] > vertices[v][j]) ? static_cast<float>(vertices[v][j]) : min[j];
				}
			}

			for (int j = 0; j < 3; ++j)
				center[j] = static_cast<float>(c[j] / vertices.size());
		}

		/// compute face normal vectors
		void compute_normal(const std::shared_ptr<tobj::ObjLoad>& obj, const size_t fileIdx)
		{
			size_t idxOffset = 0;
			for (int i = 0; i < fileIdx; ++i)
			{
				idxOffset += nVertices[i];
			}

			/// new face normal vectors
			std::vector<tobj::Vertex> newFaceNormals;
			newFaceNormals.reserve(obj->numFaces());
			/// new vertex normal vectors
			std::vector<tobj::Vertex> newVtxNormalV(obj->numVertices());
			/// initialize new vertex normals
			for (auto& n : newVtxNormalV)
			{
				n[0] = n[1] = n[2] = 0.0;
			}

			tobj::Vertex fVtx[3];
			std::vector<tobj::FaceVtxIdx> newFVtxIds;
			newFVtxIds.reserve(obj->numFaces());

			for (size_t f = 0; f < obj->numFaces(); ++f)
			{
				auto vtxIds = obj->getVtxIdx(f);

				for (int v = 0; v < 3; ++v)
					fVtx[v] = obj->getVertex(vtxIds[v]);

				tobj::Vertex p01, p02;
				p01 = fVtx[1] - fVtx[0];
				p02 = fVtx[2] - fVtx[0];

				/// cross product
				auto crossP = p01 ^ p02;

				/// area
				auto faceAreas = sqrt(crossP[0] * crossP[0] + crossP[1] * crossP[1] + crossP[2] * crossP[2]);

				if (faceAreas < std::numeric_limits<double>::epsilon())
				{
					continue;
				}

				/// face normal
				newFaceNormals.push_back(crossP * (1.0 / faceAreas));

				/// vertex normal vectors
				tobj::FaceVtxIdx fvtxid;
				for (int v = 0; v < 3; ++v)
				{
					newVtxNormalV[vtxIds[v]] = newVtxNormalV[vtxIds[v]] + crossP;
					/// save vertex indices of a face after adding an offset (previous number of vertices)
					fvtxid[v] = int(vtxIds[v] + idxOffset);
				}

				newFVtxIds.push_back(fvtxid);
			}

			/// previous number of faces
			auto nPreFace = faceNormals.size();
			/// extend vectors
			faceNormals.reserve(nPreFace + newFaceNormals.size());
			/// insert new face normlas
			faceNormals.insert(faceNormals.end(), newFaceNormals.begin(), newFaceNormals.end());

			/// normalize new vertex normals
			for (auto& n : newVtxNormalV)
			{
				auto size = sqrt(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
				if (size < std::numeric_limits<double>::epsilon())
					continue;
				n = n * (1.0 / size);
			}

			/// previous number of vertex normals
			auto nPreVNormals = vtxNormalV.size();
			/// extend
			vtxNormalV.reserve(nPreVNormals + newVtxNormalV.size());
			/// insert new vertex normlas
			vtxNormalV.insert(vtxNormalV.end(), newVtxNormalV.begin(), newVtxNormalV.end());

			/// previous number of face vertex Ids
			auto nPreFVtxIdx = fVtxIds.size();
			/// extend
			fVtxIds.reserve(fVtxIds.size() + newFVtxIds.size());
			/// insert new face vtx indices
			fVtxIds.insert(fVtxIds.end(), newFVtxIds.begin(), newFVtxIds.end());
		}

	public:
		/// min and max of vertex coordinates
		float max[3];
		float min[3];
		bool glutInitialization;
		int winidx;
	};
}